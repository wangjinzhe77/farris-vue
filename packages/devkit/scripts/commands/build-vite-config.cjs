const { resolve } = require("path");
const { defineConfig } = require("vite");
const vue = require("@vitejs/plugin-vue");
const vueJsx = require("@vitejs/plugin-vue-jsx");

module.exports = function (options) {
    const { lib, outDir, minify = false, plugins = [] } = options;
    return defineConfig({
        configFile: false,
        publicDir: false,
        plugins: [
            vue(),
            vueJsx(),
            ...plugins
        ],
        resolve: {
            alias: [{ find: "@", replacement: resolve(__dirname, "../../") }],
        },
        build: {
            lib,
            outDir,
            minify,
            rollupOptions: {
                // external: ["vue", "@vueuse/core", "@vue/shared", "dayjs"],
                external: (id) => {
                    const items = [
                        "vue",
                        "vue-router",
                        "axios"
                    ];
                    return items.find((item) => id.indexOf(item) === 0);
                },
                output: {
                    exports: "named",
                    globals: (id) => {
                        const map = {
                            "vue": "Vue",
                            "vue-router": "VueRouter",
                            "axios": "axios"
                        };
                        return map[id];
                    }
                },
            },
        },
    });
};
