const { omit } = require("lodash");
const { resolve } = require("path");
const fsExtra = require("fs-extra");
const package = require("../../package.json");

const CWD = process.cwd();

/**
 * 获取版本号
 */
const getVersion = () => {
    const versionNums = package.version.split(".");
    return versionNums
        .map((num, index) => (index === versionNums.length - 1 ? +num + 1 : num))
        .join(".");
};


/**
 * 创建package.json文件
 */
const createPackageJson = async (version) => {
    package.version = getVersion(version);
    package.main = "./devkit-vue.umd.js";
    package.module = "./devkit-vue.esm.js";
    package.dependencies = omit(package.dependencies, "");
    package.typings = "./types/index.d.ts";
    package.types = "./types/index.d.ts";
    package.private = false;
    const fileStr = JSON.stringify(
        omit(package, "scripts", "devDependencies"),
        null,
        2
    );
    await fsExtra.outputFile(
        resolve(CWD, "./package", `package.json`),
        fileStr,
        "utf-8"
    );
};

module.exports = createPackageJson;
