module.exports = function replace({ path }) {
    return {
        name: 'farris-replace',
        renderChunk(code, chunk) {
            const fileNames = chunk.fileName.split('.');
            const format = fileNames[fileNames.length - 2];
            return code.replace(/@\/components(\/\w+)/g, (...args) => {
                return path(format, args);
            });
        }
    };
};
