#!/usr/bin/env node
const { Command } = require('commander');
const { build } = require('./commands/build.cjs');

const program = new Command();
program.command('build').description('构建 Farris Devkit Vue').action(build);
program.parse();
