import { makePropMetadataDecorator } from '../../common/index';

/**
 * 状态访问器元数据类型
 */
interface GetterMeta {
    name?: string;
}

/**
 * 状态访问器元数据名称
 */
const GETTER_PROP_META_NAME = 'GETTER';

/**
 * 状态访问器元数据工厂方法
 */
const getterMetaFactory = (meta: GetterMeta) => {
    return meta;
};

/**
 * 状态访问器装饰器类型
 */
type GetterDecoratorFactory = (getterMeta?: GetterMeta) => MethodDecorator;

/**
 * 状态访问器装饰器工厂
 */
const FdGetter: GetterDecoratorFactory = makePropMetadataDecorator(GETTER_PROP_META_NAME, getterMetaFactory);

export { GetterMeta, GETTER_PROP_META_NAME, GetterDecoratorFactory, FdGetter };
