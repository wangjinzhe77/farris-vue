import { StaticProvider, Type, TypeDecorator, makeTypeMetadataDecorator } from '../../common/index';
import { CommandHandler } from '../../command/index';

/**
 * 视图模型元数据名称
 */
const VIEWMODEL_META_NAME = 'VIEWMODEL_META';

/**
 * ViewModel元数据
 */
interface ViewModelMeta {
    id: string;
    deps?: any[];
    providers?: StaticProvider[];

    entityStore?: string;
    uiStore?: string;
    stateMachine?: string;
    repository?: string;

    commandHandlers?: Type<CommandHandler>[]
}

/**
 * 视图模型元数据工厂
 */
const viewModelMetaFactory = (meta: ViewModelMeta): ViewModelMeta => {
    return meta;
};

/**
 * 视图模型装饰器工厂类型
 */
interface ViewModelDecoratorFactory {
    (meta: ViewModelMeta): TypeDecorator;
}

/**
 * 视图模型装饰器工厂
 */
function FdViewModel(meta: ViewModelMeta) {
    const decoratorFactory: ViewModelDecoratorFactory = makeTypeMetadataDecorator(VIEWMODEL_META_NAME, viewModelMetaFactory);
    return decoratorFactory(meta);
}

export { VIEWMODEL_META_NAME, ViewModelMeta, viewModelMetaFactory, ViewModelDecoratorFactory, FdViewModel }
