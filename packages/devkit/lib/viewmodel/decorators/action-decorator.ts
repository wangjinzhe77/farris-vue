import { makePropMetadataDecorator } from '../../common/index';

/**
 * 动作元数据名称
 */
const ACTION_META_NAME = 'ACTION_META';

/**
 * 动作元数据类型
 */
interface ActionMeta {
    name?: string;
}

/**
 * 动作元数据工厂方法
 */
const actionMetaFactory = (meta: ActionMeta) => {
    return meta;
};

/**
 * 动作装饰器类型
 */
type ActionDecoratorFactory = (actionMeta?: ActionMeta) => MethodDecorator;

/**
 * 动作装饰器工厂
 */
const FdAction: ActionDecoratorFactory = makePropMetadataDecorator(ACTION_META_NAME, actionMetaFactory);

export { ACTION_META_NAME, ActionMeta, ActionDecoratorFactory, FdAction };
