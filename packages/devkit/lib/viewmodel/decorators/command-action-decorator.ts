import { makePropMetadataDecorator } from '../../common/index';

/**
 * 命令动作元数据名称
 */
const COMMAND_ACTION_META_NAME = 'COMMAND_ACTION_META';

/**
 * 命令动作元数据类型
 */
interface CommandActionMeta {
    name: string;
    params?: { [key: string]: any };
}

/**
 * 命令动作元数据工厂方法
 */
const commandActionMetaFactory = (meta: CommandActionMeta) => {
    return meta;
};

/**
 * 命令动作装饰器类型
 */
type CommandActionDecoratorFactory = (commandactionMeta: CommandActionMeta) => MethodDecorator;

/**
 * 命令动作装饰器工厂
 */
const FdCommandAction: CommandActionDecoratorFactory = makePropMetadataDecorator(COMMAND_ACTION_META_NAME, commandActionMetaFactory);

export { COMMAND_ACTION_META_NAME, CommandActionMeta, CommandActionDecoratorFactory, FdCommandAction };
