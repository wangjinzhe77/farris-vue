import { provide } from 'vue';
import { StaticProvider, Injector, createInjector } from '../common/index';
import { Module } from '../module/index';
import { CommandHandlerConfig, COMMAND_HANDLERS_TOKEN, DynamicCommandHandler, commandBusProviders } from '../command/index';
import { ViewModelNode } from './viewmodel-node';
import { VIEWMODEL_INJECTION_TOKEN, ViewModelState, ViewModel, } from './viewmodel';
import { ViewModelConfig } from './configs';

/**
 * 获取命令处理器的Providers
 */
function getCommandHandlerProviders(commandHandlerConfigs: CommandHandlerConfig[]): StaticProvider[] {
    const providers: StaticProvider[] = [];
    commandHandlerConfigs.forEach((config) => {
        if (config.type) {

            // 静态CommandHandler
            const deps = config.deps || [];
            const provider: StaticProvider = {
                provide: COMMAND_HANDLERS_TOKEN,
                useClass: config.type,
                deps: deps,
                multi: true
            };
            providers.push(provider);
        } else {

            // 动态CommandHandler
            const provider: StaticProvider = {
                provide: COMMAND_HANDLERS_TOKEN,
                useFactory: () => {
                    const instance = new DynamicCommandHandler(config);
                    return instance;
                },
                multi: true,
                deps: []
            };
            providers.push(provider);
        }
    });

    return providers;
}

/**
 * 获取ViewModel的Providers
 */
function getViewModelProviders(viewModelConfig: ViewModelConfig): StaticProvider[] {
    const configProviders = viewModelConfig.providers || [];
    const commandHandlerProviders = getCommandHandlerProviders(viewModelConfig.commandHandlers)
    const providers = [
        ...configProviders,
        ...commandHandlerProviders,
        ...commandBusProviders
    ];

    if (viewModelConfig.type) {
        const deps = viewModelConfig.deps || [];
        providers.push({ provide: ViewModel, useClass: viewModelConfig.type, deps: deps });
    } else {
        providers.push({ provide: ViewModel, useClass: ViewModel, deps: [Injector] })
    }
    providers.push({ provide: ViewModelNode, useExisting: ViewModel });

    return providers;
}

/**
 * 创建ViewModel
 */
function createViewModel<VM extends ViewModel<ViewModelState>>(module: Module, viewModelId: string): VM {
    if (!module) {
        throw new Error('Module does not exist');
    }

    const moduleConfigManager = module.getConfigManager();
    const viewModelConfig = moduleConfigManager.getViewModelConfig(viewModelId);
    if (!viewModelConfig) {
        throw new Error(`ViewModelConfig(id=${viewModelId}) does not exist`);
    }

    let parentViewModel;
    if (viewModelConfig.parentId) {
        parentViewModel = module.getViewModel(viewModelConfig.parentId);
        if (!parentViewModel) {
            throw new Error(`ParentViewModel(id=${viewModelConfig.parentId}) does not exist`);
        }
    }
    const parentInjector = parentViewModel ? parentViewModel.getInjector() : module.getInjector();

    const providers = getViewModelProviders(viewModelConfig);
    const viewModelInjector = createInjector(providers, parentInjector);

    const viewModel = viewModelInjector.get(ViewModel);
    module.registerViewModel(viewModelConfig.id, viewModel);
    provide(VIEWMODEL_INJECTION_TOKEN, viewModel);

    viewModel.init(viewModelConfig);

    return viewModel as VM;
}

export { createViewModel };