import { Reactive } from 'vue';
import { Injector } from '../common/index';
import { Entity, EntityState, EntityStore, UIState, UIStore, StateMachineState, StateMachine } from '../store/index';
import { Repository } from '../repository/index';
import { Command, CommandBus } from '../command/index';
import { ViewModelConfig, ViewModelConfigManager } from './configs/index';
import { ViewModelNode } from './viewmodel-node';
import { ViewModelInitializer } from './viewmodel-initializer';

/**
 * 注入Token
 */
const VIEWMODEL_INJECTION_TOKEN = Symbol('VIEWMODEL');

/**
 * 视图模型状态
 */
class ViewModelState {

    /**
     * 实体状态
     */
    public entityState?: EntityState<Entity>;

    /**
     * UI状态
     */
    public uiState?: UIState;

    /**
     * 状态机状态
     */
    public stateMachine?: StateMachineState;
}


/**
 * 视图模型
 */
class ViewModel<S extends ViewModelState> extends ViewModelNode {

    /**
     * ID
     */
    public id!: string;

    /**
     * 配置管理
     */
    public configManager!: ViewModelConfigManager;

    /**
     * 实体曾库
     */
    public entityStore: EntityStore<EntityState<Entity>> | null = null;

    /**
     * UI仓库
     */
    public uiStore: UIStore<UIState> | null = null;

    /**
     * 状态机
     */
    public stateMachine: StateMachine<StateMachineState> | null = null;

    /**
     * 远程实体仓库
     */
    public repository: Repository<Entity>;

    /**
     * 视图模型状态
     */
    public state!: Reactive<S>;

    /**
     * 命令总线
     */
    public commandBus: CommandBus | null = null;
    

    /**
     * 构造函数
     */
    constructor(injector: Injector) {
        super(injector);
    }

    /**
     * 初始化
     */
    public init(config: ViewModelConfig) {
        this.id = config.id;
        this.bindingPath = config.bindingPath;
        this.componentId = config.componentId;
        this.configManager = new ViewModelConfigManager(config);
        const initializer = new ViewModelInitializer(this);
        initializer.init();
    }

    /**
     * 初始化命令总线
     */
    public dispatch(command: Command): any {
        if (!this.commandBus) {
            throw new Error('No CommandBus provided');
        }
        this.commandBus.dispatch(command);
    }
}

export { VIEWMODEL_INJECTION_TOKEN, ViewModelState, ViewModel };
