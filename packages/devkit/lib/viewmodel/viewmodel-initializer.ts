import { reactive } from 'vue';
import { Injector } from '../common/index';
import { Module } from '../module/index';
import { CommandBus } from '../command/index';
import { ViewModelConfig } from './configs/index';
import { ViewModelState, ViewModel } from './viewmodel';

/**
 * 视图模型初始化器
 */
class ViewModelInitializer {

    /**
     * 所属模块
     */
    private module: Module;

    /**
     * 注入器
     */
    private injector: Injector;

    /**
     * 视图模型
     */
    private viewModel: ViewModel<ViewModelState>;

    /**
     * 视图模型配置
     */
    private config: ViewModelConfig;

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        this.viewModel = viewModel;
        this.module = viewModel.getModule();
        this.injector = viewModel.getInjector();
        this.config = viewModel.configManager.getViewModelConfig();
    }

    /**
     * 初始化
     */
    public init(): void {
        this.initState();
        this.initEntityStore();
        this.initUIStore();
        this.initStateMachine();
        this.initRepository();
        this.initCommandBus();
        this.initCommandAction();
    }

    /**
     * 初始化状态
     */
    private initState() {
        this.viewModel.state = reactive({
            entityState: null,
            uiState: null,
        });
    }

    /**
     * 初始化命令总线
     */
    private initCommandBus() {
        this.viewModel.commandBus = this.injector.get(CommandBus) as CommandBus;
    }

    /**
     * 初始化实体仓库
     */
    private initEntityStore(): void {
        if (!this.config.entityStore) {
            return;
        }
        const id = this.config.entityStore;
        const entityStore = this.module.createEntityStore(id);

        // 更新状态
        this.viewModel.state.entityState = entityStore.getState();
        entityStore.watchChange(() => {
            this.viewModel.state.entityState = entityStore.getState();
        });

        this.viewModel.entityStore = entityStore;
    }

    /**
     * 初始化UI仓库
     */
    private initUIStore(): void {
        if (!this.config.uiStore) {
            return;
        }
        const id = this.config.uiStore;
        let uiStore = this.module.createUIStore(id);

        // 更新状态
        this.viewModel.state.uiState = uiStore.getState();
        uiStore.watchChange(() => {
            this.viewModel.state.uiState = uiStore.getState();
        });

        this.viewModel.uiStore = uiStore;
    }

    /**
     * 初始化状态机
     */
    private initStateMachine() {
        if (!this.config.stateMachine) {
            return;
        }
        const id = this.config.stateMachine;
        const stateMachine = this.module.createStateMachine(id);

        // 更新状态
        this.viewModel.state.stateMachine = stateMachine.getState();
        stateMachine.watchChange(() => {
            this.viewModel.state.stateMachine = stateMachine.getState();
        });

        this.viewModel.stateMachine = stateMachine;
        stateMachine.transitToInitState();
    }

    /**
     * 初始化远程实体仓库
     */
    private initRepository(): void {
        if (!this.config.repository) {
            return;
        }
        const id = this.config.repository;
        this.viewModel.repository = this.module.createRepository(id);
    }

    /**
     * 初始化命令动作
     */
    private initCommandAction() {
        const commandConfigs = this.config.commands;
        if (!Array.isArray(commandConfigs)) {
            return;
        }

        commandConfigs.forEach((commandConfig) => {
            const commandAction = (eventParams: any) => {
                const command = {
                    name: commandConfig.name,
                    params: commandConfig.params,
                    eventParams
                };
                return this.viewModel.dispatch(command);
            };

            Object.defineProperty(this.viewModel, commandConfig.name, {
                enumerable: true,
                configurable: true,
                get() {
                    return commandAction;
                }
            });
        });
    }
}

export { ViewModelInitializer };
