/* eslint-disable no-use-before-define */
import { Injector, InjectFlags } from '../common/index';
import { Module } from '../module';

/**
 * 视图模型树节点
 */
class ViewModelNode {

    /**
     * 注入器
     */
    protected injector: Injector;

    /**
     * 应用
     */
    protected module: Module;

    /**
     * 根节点
     */
    protected root!: ViewModelNode;

    /**
     * 父节点
     */
    protected parent: ViewModelNode | null;

    /**
     * 子节点集合
     */
    protected children: ViewModelNode[];

    public bindingPath: string;
    public componentId: string;

    /**
     * 构造函数
     */
    constructor(injector: Injector) {
        this.injector = injector;
        this.module = injector.get(Module) as Module;
        this.parent = null;
        this.children = [];
        this.registerToParent();
    }

    /**
     * 获取注入器
     */
    public getInjector(): Injector {
        return this.injector;
    }

    public getRoot() {
        return this.root;
    }
    public getParent() {
        return this.parent;
    }
    /**
     * 获取所属Module
     */
    public getModule() {
        return this.module;
    }

    /**
     * 注册到父节点
     */
    private registerToParent(): void {
        const parent = this.injector.get<ViewModelNode | null>(ViewModelNode, null, InjectFlags.SkipSelf) || null;
        if (parent) {
            this.parent = parent;
            this.root = this.parent.root;
            parent.registerChild(this);
        } else {
            this.parent = null;
            this.root = this;
        }
    }

    /**
     * 从父节点中注册中移除
     */
    private unregisterFromParent(): void {
        if (!this.parent) {
            return;
        }
        this.parent.unregisterChild(this);
    }

    /**
     * 注册子节点
     */
    public registerChild(childNode: ViewModelNode): void {
        if (!childNode) {
            return;
        }
        this.children.push(childNode);
    }

    /**
     * 移除子节点注册
     */
    public unregisterChild(childNodeToRemove: ViewModelNode): void {
        const index = this.children.findIndex((childNode) => {
            return childNode === childNodeToRemove;
        });
        if (index === -1) {
            return;
        }

        this.children.splice(index, 1);
    }

    /**
     * 注销
     */
    protected dispose(): void {
        this.unregisterFromParent();
    }
}

export { ViewModelNode };
