import { PropMetadataReader, SameNamePropsMetadatas, Type, TypeMetadataReader } from '../../common/index';
import {
    COMMAND_HANDLER_META_NAME, CommandHandlerMeta, CommandConfig, CommandParamConfig, CommandHandlerConfig, CommandHandler
} from '../../command/index';
import { COMMAND_ACTION_META_NAME, CommandActionMeta, VIEWMODEL_META_NAME, ViewModelMeta } from '../decorators/index';
import { ViewModelState, ViewModel } from '../viewmodel';
import { ViewModelConfig } from './viewmodel-config';

/**
 * ViewModel配置构造器
 */
class ViewModelConfigBuilder {

    /**
     * 构造ViewModel配置
     */
    public build(viewModelType: Type<ViewModel<ViewModelState>>): ViewModelConfig {
        const viewmodelMeta = this.getViewModelMeta(viewModelType);
        const commandConfigs = this.buildCommandConfigs(viewModelType);
        const commandHandlerTypes = viewmodelMeta.commandHandlers || [];
        const commandHandlerConfigs = this.buildCommandHandlerConfigs(commandHandlerTypes);

        const viewModelConfig: ViewModelConfig = {
            id: viewmodelMeta.id,
            type: viewModelType,
            deps: viewmodelMeta.deps || [],
            providers: viewmodelMeta.providers || [],
            entityStore: viewmodelMeta.entityStore,
            uiStore: viewmodelMeta.uiStore,
            stateMachine: viewmodelMeta.stateMachine,
            repository: viewmodelMeta.repository,

            commands: commandConfigs,
            commandHandlers: commandHandlerConfigs
        };

        return viewModelConfig;
    }


    /**
     * 构造Command配置
     */
    private buildCommandConfigs(viewModelType: Type<ViewModel<ViewModelState>>): CommandConfig[] {
        const commandActionMetas = this.getCommandActionMetas(viewModelType);
        const commandConfigs: CommandConfig[] = [];
        Object.keys(commandActionMetas).forEach((propName) => {
            const commandActionMeta = commandActionMetas[propName];
            const commandParamConfigs = this.buildCommandParamConfigs(commandActionMeta.params);
            const commandConfig = {
                name: propName,
                params: commandParamConfigs
            }
            commandConfigs.push(commandConfig);
        });

        return commandConfigs;
    }

    /**
     * 构造Command参数配置
     */
    private buildCommandParamConfigs(commandActionParams: any): CommandParamConfig[] {
        if (!commandActionParams) {
            return [];
        }

        const commandConfigs: CommandParamConfig[] = [];
        Object.keys(commandActionParams).forEach((name) => {
            const value = commandActionParams[name];
            const commandParamConfig = { name, value };
            commandConfigs.push(commandParamConfig);
        });

        return commandConfigs
    }

    /**
     * 构造CommandHandler配置
     */
    private buildCommandHandlerConfigs(commandHandlerTypes: Type<CommandHandler>[]): CommandHandlerConfig[] {
        const configs: CommandHandlerConfig[] = [];
        commandHandlerTypes.forEach((commandHandlerType) => {
            const meta = this.getCommandHandlerMeta(commandHandlerType);
            const deps = meta.deps || [];
            const config: CommandHandlerConfig = {
                commandName: meta.commandName,
                type: commandHandlerType,
                deps: [...deps],
            }
            configs.push(config);
        });

        return configs;
    }

    /**
     * 获取ViewModel元数据
     */
    private getViewModelMeta(viewModelType: Type<ViewModel<ViewModelState>>): ViewModelMeta {
        const viewModelMeta = TypeMetadataReader.getMataByName(viewModelType, VIEWMODEL_META_NAME);
        return viewModelMeta;
    }

    /**
     * 获取Command元数据
     */
    private getCommandActionMetas(viewModelType: Type<ViewModel<ViewModelState>>): SameNamePropsMetadatas<CommandActionMeta> {
        const metas = PropMetadataReader.getPropsMetasByName<CommandActionMeta>(viewModelType, COMMAND_ACTION_META_NAME);
        return metas;
    }

    /**
     * 获取CommandHandler元数据
     */
    private getCommandHandlerMeta(commandHandlerType: Type<CommandHandler>): CommandHandlerMeta {
        const meta = TypeMetadataReader.getMataByName(commandHandlerType, COMMAND_HANDLER_META_NAME);
        return meta;
    }
}

export { ViewModelConfigBuilder };