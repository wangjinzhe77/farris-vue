import { ViewModelConfig } from "./viewmodel-config";

/**
 * ViewModel配置管理
 */
class ViewModelConfigManager {

    /**
     * ViewModel配置
     */
    private config: ViewModelConfig;

    /**
     * 构造函数
     */
    constructor(config: ViewModelConfig) {
        this.config = config;
    }

    /**
     * 获取ViewModel配置
     */
    public getViewModelConfig() {
        return this.config;
    }

}

export { ViewModelConfigManager };
