import { StaticProvider, Type } from '../../common/index';
import { CommandConfig, CommandHandlerConfig } from '../../command/index';
import { ViewModelState, ViewModel } from '../viewmodel';

/**
 * Getter配置
 */
interface GetterConfig {
    name: string;
    expression?: string;
}

/**
 * Action配置
 */
interface ActionConfig {
    name: string;
    acitonFunc: any;
}

/**
 * CommandActon参数配置
 */
interface CommandActionParamConfig {
    name: string;
    value: any;
}

/**
 * CommandActon配置
 */
interface CommandActionConfig {
    name: string;
    params: CommandActionParamConfig[]
}

/**
 * ViewModel配置
 */
interface ViewModelConfig {
    id: string;
    parentId?: string;
    bindingPath?: string;
    componentId?: string;
    type?: Type<ViewModel<ViewModelState>>;
    deps?: any[];
    providers?: StaticProvider[],

    entityStore?: string;
    uiStore?: string;
    stateMachine?: string;
    repository?: string;

    getters?: GetterConfig[],
    actions?: ActionConfig[],
    commands?: CommandConfig[];
    commandHandlers: CommandHandlerConfig[];
}

export { GetterConfig, ActionConfig, CommandActionParamConfig, CommandActionConfig, ViewModelConfig };
