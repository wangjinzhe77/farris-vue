import { App } from 'vue';
import { StaticProvider, Injector, createInjector } from './common/index';
import { httpProviders } from './http/index';
import { variableProviders } from './variable/index';
import { Module } from './module/index';

/**
 * Devkit注入Token
 */
export const DEVKIT_INJECTION_TOKEN = Symbol('DEVKIT');

/**
 * Devkit配置
 */
export interface DevkitConfig {

    /**
     * 注入配置
     */
    providers: StaticProvider[];
}

/**
 * Devkit定义
 */
export class Devkit {

    /**
     * 配置对象
     */
    private config: DevkitConfig;

    /**
     * 注入器
     */
    private injector!: Injector;

    /**
     * 模块集合
     */
    private modules: Map<string, Module>;

    /**
     * 全局变量
     */
    public params: any = new Map<string, any>();

    /**
     * 构造函数
     */
    constructor(config: DevkitConfig) {
        this.config = config;
        this.modules = new Map<string, Module>();
        this.injector = this.createInjector();
    }

    /**
     * 安装Devkit
     */
    public install(app: App) {
        app.provide(DEVKIT_INJECTION_TOKEN, this);
    }

    /**
     * 获取Injector 
     */
    public getInjector(): Injector {
        return this.injector;
    }

    /**
     * 获取Module
     */
    public getModule(id: string): Module | undefined {
        return this.modules.get(id);
    }

    /**
     * 注册Module
     */
    public regitsterModule(id: string, module: Module) {
        this.modules.set(id, module);
    }

    /**
     * 创建Injector
     */
    private createInjector() {
        const providers = [
            { provide: Devkit, useValue: this },
            ...httpProviders,
            ...variableProviders,
            ...this.config.providers
        ]
        const injector = createInjector(providers);
        return injector;
    }
}

let devkit: Devkit;

/**
 * 创建Devkit
 */
export function createDevkit(config: DevkitConfig) {
    devkit = new Devkit(config);
    return devkit;
}

/**
 * 使用Devkit
 */
export function useDevkit(): Devkit {
    if (!devkit) {
        throw new Error('Please create Devkit before using it');
    }
    return devkit;
}
