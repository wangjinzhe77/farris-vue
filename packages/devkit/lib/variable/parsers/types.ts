import { createInjectionToken } from '../../common/index';
import { Module } from '../../module';
import { ViewModelState, ViewModel } from '../../viewmodel/index';

/**
 * 状态变量路径
 */
interface VariablePath {
    viewModelId: string;
    propPath: string,
    variable: string
}

/**
 * 变量解析器上下文
 */
interface VariableParseContext {

    /**
     * 模块
     */
    module: Module;

    /**
     * 视图模型
     */
    viewModel?: ViewModel<ViewModelState>;

    /**
     * 其他上下文
     */
    extraContext?: any;
}

/**
 * 变量解析器接口
 */
interface VariableParser {
    parse(expression: string, context: VariableParseContext): any;
}

/**
 * 变量解析器注入Token
 */
const VARIABLE_PARSERS_TOKEN = createInjectionToken('VARIABLE_PARSERS');

export { VariablePath, VariableParseContext, VariableParser, VARIABLE_PARSERS_TOKEN };