import { Entity, EntityState, EntityStore } from '../../store/index';
import { VariableParseContext, VariablePath } from './types';
import { BaseStateVariableParser } from './base-state-variable-parser';

/**
 * EntityState变量解析
 * @summary
 * 变量格式形如：
 * 1、简单字段：{DATA~/id}
 * 2、关联字段：{DATA~/departmentInfo/id}
 * 3、从表简单字段：{DATA~/eduInfos/id}
 * 4、从表关联字段：{DATA~/eduInfos/schoolInfo/id}
 */
class EntityVariableParser extends BaseStateVariableParser {

    protected VARIABLE_PATTERN_G: RegExp = /\{DATA~(\/#{\S+?})?\/(\S+?)\}/g;

    protected VARIABLE_PATTERN: RegExp = /\{DATA~(\/#{\S+?})?\/(\S+?)\}/;

    protected VARIABLE_PREFIX: string = 'DATA';

    /**
     * 获取实体变量值
     */
    protected getVariableValue(variablePath: VariablePath, context: VariableParseContext) {
        const { viewModelId, propPath } = variablePath;
        const entityStore = this.getEntityStore(viewModelId, context);
        const variableValue = entityStore.getValueByPath(propPath);

        return variableValue;
    }

    /**
     * 获取实体状态仓库
     */
    private getEntityStore(viewModelId: string, context: VariableParseContext): EntityStore<EntityState<Entity>> {
        let viewModel = this.getViewModel(viewModelId, context);
        const entityStore = viewModel.entityStore;
        if (!entityStore) {
            throw new Error('Can not find the EntityStore');
        }

        return entityStore;
    }
}

export { EntityVariableParser };
