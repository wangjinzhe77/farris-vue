import { StateMachineState, StateMachine } from '../../store/index';
import { VariableParseContext, VariablePath } from './types';
import { BaseStateVariableParser } from './base-state-variable-parser';

/**
 * 状态机变量解析
 * @summary
 * 变量的格式如下：
 * 当前页面状态：{STATEMACHINE~/currentPageState}
 * 可视化状态：{STATEMACHINE~/canSave}
 */
class StateMachineVariableParser extends BaseStateVariableParser {

    protected VARIABLE_PATTERN_G: RegExp = /\{STATEMACHINE~(\/#{\S+?})?\/(\S+?)\}/g;

    protected VARIABLE_PATTERN: RegExp = /\{STATEMACHINE~(\/#{\S+?})?\/(\S+?)\}/;

    protected VARIABLE_PREFIX: string = 'STATEMACHINE';

    /**
     * 获取状态机变量值
     */
    protected getVariableValue(variablePath: VariablePath, context: VariableParseContext) {
        const { viewModelId, propPath } = variablePath;
        const propName = propPath.slice(1);
        const stateMachine = this.getStateMachine(viewModelId, context);
        const variableValue = stateMachine.getValue(propName);

        return variableValue;
    }

    /**
     * 获取状态机
     */
    private getStateMachine(storeId: string, context: VariableParseContext): StateMachine<StateMachineState> {
        const viewModel = this.getViewModel(storeId, context);
        const stateMachine = viewModel.stateMachine;
        if (!stateMachine) {
            throw new Error('Can not find the StateMachine');
        }

        return stateMachine;
    }
}

export { StateMachineVariableParser };

