import { VariablePath, VariableParser, VariableParseContext } from './types';
import { StateVariableUtil } from './state-variable-util';

/**
 * 状态变量解析基类
 */
abstract class BaseStateVariableParser implements VariableParser {

    /**
     * 状态路径（全局）
     */
    protected abstract VARIABLE_PATTERN_G: RegExp;

    /**
     * 状态路径（局部）
     */
    protected abstract VARIABLE_PATTERN: RegExp;

    /**
     * 变量前缀
     */
    protected abstract VARIABLE_PREFIX: string;

    /**
     * 替换表达式中的变量
     */
    protected abstract getVariableValue(variablePath: VariablePath, context: VariableParseContext);

    /**
     * 解析变量 
     */
    public parse(expression: string, context: VariableParseContext): any {
        const variablePaths = this.extractVariablePaths(expression);

        // 单变量：返回原始值，保留原始类型
        if (variablePaths.length === 1 && expression === variablePaths[0].variable) {
            return this.getVariableValue(variablePaths[0], context);
        }

        // 多变量：进行字符串替换
        variablePaths.forEach((variablePath) => {
            const searchValue = variablePath.variable;
            const replaceValue = this.getVariableValue(variablePath, context);
            expression = expression.replace(searchValue, replaceValue);
        });

        return expression;
    }

    /**
     * 从表达式中提取变量路径
     */
    protected extractVariablePaths(expression: string): VariablePath[] {
        const variablePaths: any[] = [];

        // 查找变量字符串
        const variables = expression.match(this.VARIABLE_PATTERN_G);
        if (variables === null) {
            return [];
        }

        // 获取变量路径路径
        variables.forEach((variable: string) => {
            const variablePath = StateVariableUtil.getVariablePath(variable, this.VARIABLE_PREFIX);
            variablePaths.push(variablePath);
        });

        return variablePaths;
    }

    /**
     * 获取视图模型
     */
    protected getViewModel(viewModelId: string, context: VariableParseContext) {
        let viewModel = context.viewModel;
        if (viewModelId) {
            viewModel = context.module.getViewModel(viewModelId);
        }
        if (!viewModel) {
            throw new Error('Can not find the ViewModel');
        }

        return viewModel;
    }
}

export { BaseStateVariableParser };