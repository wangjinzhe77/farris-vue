import { UIState, UIStore } from '../../store/index';
import { VariableParseContext, VariablePath } from './types';
import { BaseStateVariableParser } from './base-state-variable-parser';

/**
 * UI状态变量解析
 * @summary
 * 变量的格式如下：
 * 1、简单变量：{UIState~/currentTab}、{UIState~/#{storeId}/currentTab}
 * 2、嵌套变量：{UIState~/filterState/keyword}、{UIState~/#{storeId}/filterState/keyword}
 * 3、其他变量：暂不支持数组的解析
 */
class UIVariableParser extends BaseStateVariableParser {

    protected VARIABLE_PATTERN_G: RegExp = /\{UISTATE~(\/#{\S+?})?\/(\S+?)\}/g;

    protected VARIABLE_PATTERN: RegExp = /\{UISTATE~(\/#{\S+?})?\/(\S+?)\}/;

    protected VARIABLE_PREFIX: string = 'UISTATE';

    /**
     * 获取UI变量值
     */
    protected getVariableValue(variablePath: VariablePath, context: VariableParseContext) {
        const { viewModelId, propPath } = variablePath;
        const uiStore = this.getUIStore(viewModelId, context);
        const variableValue = uiStore.getValueByPath(propPath);

        return variableValue;
    }

    /**
     * 获取UI状态仓库
     */
    private getUIStore(viewModelId: string, context: VariableParseContext): UIStore<UIState> {
        let viewModel = context.viewModel;
        if (viewModelId) {
            viewModel = context.module.getViewModel(viewModelId);
        }
        if (!viewModel) {
            throw new Error('Can not find the ViewModel');
        }

        const uiStore = viewModel.uiStore;
        if (!uiStore) {
            throw new Error('Can not find the UIStore');
        }

        return uiStore;
    }
}

export { UIVariableParser };
