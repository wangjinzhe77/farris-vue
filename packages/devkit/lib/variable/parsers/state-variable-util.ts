import { VariablePath } from './types';

/**
 * 状态变量工具类
 */
class StateVariableUtil {

    /**
     * 获取变量路径
     */
    public static getVariablePath(variable: string, variablePrefix: string): VariablePath {
        const variablePathString = this.trimVariablePrefixAndSuffix(variable, variablePrefix);
        const variablePath: VariablePath = {
            viewModelId: '',
            propPath: '',
            variable: variable
        };

        if (variablePathString.startsWith('/#{')) {
            let viewModelId = this.getViewModelId(variablePathString, variablePrefix);
            const viewModelIdLength = viewModelId ? viewModelId.length : -1;
            if (viewModelId && viewModelId.startsWith('#{') && viewModelId.endsWith('}')) {
                viewModelId = viewModelId.slice(2, viewModelId.length - 1);
            }
            variablePath.viewModelId = viewModelId;
            variablePath.propPath = variablePathString.slice(viewModelIdLength + 1);
        } else {
            variablePath.viewModelId = '';
            variablePath.propPath = variablePathString;
        }

        return variablePath;
    }

    /**
     * 移除变量的前缀和后缀
     */
    public static trimVariablePrefixAndSuffix(variable: string, variablePrefix: string): string {
        const prefix = `{${variablePrefix}~`;
        const suffix = '}';

        const startIndex = prefix.length;
        const endIndex = variable.length - suffix.length;
        const trimedVariable = variable.slice(startIndex, endIndex);

        return trimedVariable;
    }

    /**
     * 从变量字符串中获取视图模型ID
     */
    public static getViewModelId(variablePathString: string, variablePrefix: string): string {
        // data变量不需要视图模型ID
        if (variablePrefix === 'DATA') {
            return null;
        }
        const variablePathSegs = variablePathString.split('/');
        if (variablePathSegs.length <= 1 || variablePathSegs[0] !== '') {
            throw new Error('Variable path can not be empty and must start with a slash');
        }
        const viewModelId = variablePathSegs[1];


        return viewModelId;
    }
}

export { StateVariableUtil };