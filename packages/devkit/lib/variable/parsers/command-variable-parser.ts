import { TaskFlowContext } from '../../command/index';
import { VariableParseContext, VariableParser } from './types';

/**
 * 命令变量解析
 * @summary
 * 变量格式形如：
 * 1、命令参数变量：{Command~/params/key}
 * 2、操作执行结果变量：{Command~/results/taskName}
 */
class CommandVariableParser implements VariableParser {

    /**
     * 解析变量
     */
    public parse(expression: string, context: VariableParseContext): any {
        const taskFlowContext = this.getTaskFlowContext(context);
        if (!taskFlowContext) {
            return expression;
        }

        const variablePaths = this.extractPaths(expression);

        // 1、单个变量：直接求职
        if (variablePaths.length === 1 && expression === `{COMMAND~${variablePaths[0]}}`) {
            return this.getValue(variablePaths[0], taskFlowContext);
        }

        // 2、多个变量：字符串替换
        variablePaths.forEach(path => {
            const searchValue = `{COMMAND~${path}}`;
            const replaceValue = this.getValue(path, taskFlowContext);
            expression = expression.replace(searchValue, replaceValue);
        });

        return expression;
    }

    /**
     * 获取上下文
     */
    private getTaskFlowContext(context: VariableParseContext): TaskFlowContext {
        if (!context) {
            throw new Error('The context can not be empty');
        }

        return context.extraContext;
    }

    /**
     * 提取变量路径
     */
    private extractPaths(expression: string): string[] {
        const variablePaths: string[] = [];

        // 查找所有变量字符串
        const COMMAND_PATTERN_G = /\{COMMAND~(\S+?)\}/g;
        const variables = expression.match(COMMAND_PATTERN_G);
        if (variables === null) {
            return [];
        }

        // 提取所有变量路径
        const COMMAND_PATTERN = /\{COMMAND~(\S+?)\}/;
        variables.forEach((variable) => {
            const pathMatches = variable.match(COMMAND_PATTERN);
            if (pathMatches != null && pathMatches.length === 2) {
                variablePaths.push(pathMatches[1]);
            }
        });

        return variablePaths;
    }

    /**
     * 获取变量值
     */
    private getValue(path: string, taskFlowContext: TaskFlowContext): any {
        const pathSegments = path.split('/').slice(1);
        const [type, name] = pathSegments;
        if (type === 'params') {
            const params = taskFlowContext.commandContext.command.params;
            if (!params) {
                throw new Error('The params of command does not exist');
            }

            const targetParam = params.find((param) => {
                return param.name === name;
            });

            return targetParam ? targetParam.value : undefined;
        } else if (type === 'results') {
            const results = taskFlowContext.taskResults;
            return results.get(name);
        }
    }
}

export { CommandVariableParser };
