
import { StaticProvider } from '../common/index';
import { EntityVariableParser, UIVariableParser, StateMachineVariableParser, CommandVariableParser, VARIABLE_PARSERS_TOKEN } from './parsers/index';
import { VariableParseService } from './variable-parse-service';

const variableProviders: StaticProvider[] = [
    { provide: VARIABLE_PARSERS_TOKEN, useClass: EntityVariableParser, multi: true, deps: [] },
    { provide: VARIABLE_PARSERS_TOKEN, useClass: UIVariableParser, multi: true, deps: [] },
    { provide: VARIABLE_PARSERS_TOKEN, useClass: StateMachineVariableParser, multi: true, deps: [] },
    { provide: VARIABLE_PARSERS_TOKEN, useClass: CommandVariableParser, multi: true, deps: [] },
    { provide: VariableParseService, useClass: VariableParseService, deps: [VARIABLE_PARSERS_TOKEN] },
];

export { variableProviders };
