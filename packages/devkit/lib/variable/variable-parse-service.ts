import { VariableParseContext, VariableParser } from './parsers/index';

/**
 * 变量解析服务
 */
class VariableParseService {

    /**
     * 解析器集合
     */
    private parsers: VariableParser[];

    /**
     * 构造函数
     */
    constructor(parsers: VariableParser[]) {
        this.parsers = parsers;
    }

    /**
     * 变量解析
     */
    public parse(target: string | string[] | any, context: VariableParseContext): any {
        if (typeof target === 'string' && target.length > 0) {
            return this.parseExpression(target, context);

        } else if (Array.isArray(target)) {
            target.forEach((item, itemIndex) => {
                if (typeof item === 'string') {
                    target[itemIndex] = this.parseExpression(item, context);
                } else {
                    target[itemIndex] = this.parse(item, context);
                }
            });

        } else if (typeof target === 'object' && target !== null) {
            const keys = Object.keys(target);
            keys.forEach(key => {
                if (typeof target[key] === 'string') {
                    target[key] = this.parseExpression(target[key], context);
                } else {
                    target[key] = this.parse(target[key], context);
                }
            });
        }

        return target;
    }

    /**
     * 表达式求值
     */
    public evaluate(expression: string, context?: any): any {
        const parsedExpression = this.parse(expression, context);
        return (new Function('return ' + parsedExpression))();
    }

    /**
     * 表达式解析
     */
    private parseExpression(expression: string, context: any): string {
        if (expression === '') {
            return '';
        }

        this.parsers.forEach(parser => {
            if (typeof expression === 'string') {
                expression = parser.parse(expression, context);
            }
        });
        return expression;
    }
}

export { VariableParseService };
