import { Injector, StaticProvider } from '../common/index';
import { VariableParseService } from '../variable';
import { CommandBus } from './command-bus';
import { CommandHandlerFactory } from './command-handler-factory';
import { CommandHandlerRegistry } from './command-handler-registry';

const commandBusProviders: StaticProvider[] = [
  {
    provide: CommandHandlerRegistry,
    useClass: CommandHandlerRegistry,
    deps: [Injector]
  },
  {
    provide: CommandHandlerFactory,
    useClass: CommandHandlerFactory,
    deps: [Injector, CommandHandlerRegistry]
  },
  {
    provide: CommandBus,
    useClass: CommandBus,
    deps: [Injector,CommandHandlerFactory,VariableParseService]
  }
];

export { commandBusProviders };