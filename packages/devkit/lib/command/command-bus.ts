import { Injector } from '../common';
import { ViewModelState, ViewModel } from '../viewmodel';
import { VariableParseService } from '../variable/index';
import { Command } from './command';
import { CommandContext } from './command-context';
import { CommandHandlerFactory } from './command-handler-factory';

/**
 * 命令总线
 */
class CommandBus {

    /**
     * 视图模型
     */
    private viewModel: ViewModel<ViewModelState>;

    /**
     * 构造函数
     */
    constructor(private injector: Injector, private handlerFactory: CommandHandlerFactory) {
        this.viewModel = this.injector.get(ViewModel);
    }

    /**
     * 派发命令
     */
    public dispatch(command: Command): Promise<any> {
        return this.handleCommand(command);
    }

    /**
     * 执行命令
     */
    private async handleCommand(command: Command): Promise<any> {
        const commandName = command.name;
        const handler = this.handlerFactory.create(commandName);
        const context: CommandContext = this.buildCommandContext(command);
        return handler.execute(context);
    }

    /**
     * 构造命令上下文
     */
    private buildCommandContext(command: Command): CommandContext {
        const { eventParams = null } = command;
        delete command.eventParams;

        const commandToExecute = JSON.parse(JSON.stringify(command));
        commandToExecute.eventParams = eventParams;

        // 解析参数
        const variableParserService = this.injector.get<VariableParseService>(VariableParseService);
        const variableParseContext = {
            module: this.viewModel.getModule(),
            viewModel: this.viewModel
        };
        const parsedParams = variableParserService.parse(commandToExecute.params, variableParseContext);
        commandToExecute.params = parsedParams;

        const conext = new CommandContext(commandToExecute, this.viewModel);
        return conext;
    }
}

export { CommandBus };
