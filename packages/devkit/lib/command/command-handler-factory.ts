import { Injector } from '../common/index';
import { ViewModelState, ViewModel } from '../viewmodel';
import { CommandHandler } from './command-handler';
import { CommandHandlerRegistry } from './command-handler-registry';

/**
 * 命令处理器工厂
 */
class CommandHandlerFactory {

    /**
     * 视图模型
     */
    private viewModel: ViewModel<ViewModelState>;

    /**
     * 构造函数
     */
    constructor(private injector: Injector, private handlerRegistry: CommandHandlerRegistry) {
        this.viewModel = this.injector.get(ViewModel) as ViewModel<ViewModelState>;
    }

    /**
     * 创建命令处理器
     */
    public create(commandName: string): CommandHandler {
        const handler = this.handlerRegistry.get(commandName);
        return handler;
    }
}

export { CommandHandlerFactory };
