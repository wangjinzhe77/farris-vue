
import { TypeDecorator, makeTypeMetadataDecorator } from '../../common/index';

/**
 * CommandHandler元数据名称
 */
const COMMAND_HANDLER_META_NAME = 'COMMAND_HANDLER_META';

/**
 * CommandHandler元数据
 */
interface CommandHandlerMeta {
    commandName: string;
    deps: any[];
}

/**
 * CommandHandler元数据工厂
 */
const commandHandlerMetaFactory = (meta: CommandHandlerMeta): CommandHandlerMeta => {
    return meta;
};

/**
 * CommandHandler装饰器工厂类型
 */
interface CommandHandlerDecoratorFactory {
    (meta: CommandHandlerMeta): TypeDecorator;
}

/**
 * CommandHandler装饰器工厂
 */
function FdCommandHandler(meta: CommandHandlerMeta) {
    const decoratorFactory: CommandHandlerDecoratorFactory = makeTypeMetadataDecorator(COMMAND_HANDLER_META_NAME, commandHandlerMetaFactory);
    return decoratorFactory(meta);
}

export { COMMAND_HANDLER_META_NAME, CommandHandlerMeta, commandHandlerMetaFactory, CommandHandlerDecoratorFactory, FdCommandHandler }
