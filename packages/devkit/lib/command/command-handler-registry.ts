
import { InjectFlags, Injector } from '../common';
import { COMMAND_HANDLERS_TOKEN, CommandHandler } from './command-handler';

/**
 * 命令处理注册器
 */
class CommandHandlerRegistry {

    /**
     * CommandHandler集合
     */
    private handlerMap: Map<string, CommandHandler>;

    /**
     * 构造函数
     */
    constructor(private injector: Injector) {
        this.handlerMap = new Map<string, CommandHandler>();
        this.init();
    }

    /**
     * 初始化
     */
    private init() {
        const handlers = this.injector.get(COMMAND_HANDLERS_TOKEN, null, InjectFlags.Optional);
        if (handlers) {
            handlers.forEach((handler: CommandHandler) => {
                this.register(handler);
            });
        }
    }

    /**
     * 获取CommandHandler
     */
    public get(commandName: string): CommandHandler {
        const commandHandler = this.handlerMap.get(commandName);
        if (!commandHandler) {
            throw new Error('Can not find CommandHandler for' + commandName + '');
        }

        return commandHandler
    }

    /**
     * 注册CommandHandler
     */
    public register(commandHandler: CommandHandler) {
        const commandName = commandHandler.commandName;
        if (this.handlerMap.has(commandName)) {
            throw new Error(`CommandHandler(commandName=${commandName}) already exists`);
        }

        this.handlerMap.set(commandName, commandHandler);
    }

}

export { CommandHandlerRegistry }
