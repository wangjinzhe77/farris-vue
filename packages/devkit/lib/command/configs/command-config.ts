import { Type } from '../../common/index';
import { ConditionConfig } from '../../condition/index';
import { CommandHandler } from '../command-handler';

/**
 * Command参数配置
 */
interface CommandParamConfig {
    name: string;
    value: any;
}

/**
 * Command配置
 */
interface CommandConfig {
    name: string;
    params: CommandParamConfig[];
}

/**
 * Command任务参数配置
 */
interface CommandTaskParamConfig {
    name: string;
    value: any;
}

/**
 * 命令任务连接
 */
interface CommandTaskLinkConfig {
    conditions: boolean | string | ConditionConfig[];
    nextTaskName: string;
}

/**
 * Command任务配置
 */
interface CommandTaskConfig {
    name: string;
    service: string;
    serviceUrl?: string;
    serviceDeps?: any[];
    method: string;
    params: CommandTaskParamConfig[];
    links: CommandTaskLinkConfig[];
}

/**
 * CommandHandler配置
 */
interface CommandHandlerConfig {
    commandName: string;
    type?: Type<CommandHandler>;
    deps?: any[];
    tasks?: CommandTaskConfig[];
}

export { CommandParamConfig, CommandConfig, CommandTaskParamConfig, CommandTaskLinkConfig, CommandTaskConfig, CommandHandlerConfig };
