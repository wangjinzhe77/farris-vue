import { TaskFlowContext } from './task-flow-context';
import { TaskLink } from './task-link';

/**
 * 任务函数
 */
type TaskFunc = (context: TaskFlowContext) => any;

/**
 * 任务节点
 */
class TaskNode {

    /**
     * 任务名称
     */
    public name: string;

    /**
     * 任务方法
     */
    public func: TaskFunc;

    /**
     * 任务连接
     */
    public links: TaskLink[];

    /**
     * 构造函数
     */
    constructor(name: string, func: TaskFunc, links: TaskLink[]) {
        this.name = name;
        this.func = func;
        this.links = links;
    }

    /**
     * 执行任务
     */
    public execute(taskFlowContext: TaskFlowContext): any {
        const result = this.func(taskFlowContext);
        return result;
    }

    /**
     * 获取下一任务
     */
    public getNextTaskName(taskFlowContext: TaskFlowContext): string | null {
        if (!Array.isArray(this.links)) {
            return null;
        }

        const targetTaskLink = this.links.find((link: TaskLink) => {
            return link.canLink(taskFlowContext);
        });
        if (!targetTaskLink) {
            return null;
        }

        return targetTaskLink.getNextTaskName();
    }
}

export { TaskFunc, TaskNode };
