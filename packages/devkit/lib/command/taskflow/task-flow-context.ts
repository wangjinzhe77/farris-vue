import { CommandContext } from '../command-context';

/**
 * 任务流上下文
 */
class TaskFlowContext {

    /**
     * 命令上下文
     */
    public commandContext: CommandContext;

    /**
     * 任务执行结果
     */
    public taskResults: Map<string, any>;

    /**
     * 构造函数
     */
    constructor(commandContext: CommandContext) {
        this.commandContext = commandContext;
        this.taskResults = new Map<string, any>();
    }

    /**
     * 设置执行结果
     */
    public setTaskResult(taskName: string, taskResult: any) {
        this.taskResults.set(taskName, taskResult);
    }
}

export { TaskFlowContext };