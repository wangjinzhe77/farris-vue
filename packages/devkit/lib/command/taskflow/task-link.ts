/* eslint-disable no-case-declarations */
import { TaskFlowContext } from './task-flow-context';

/**
 * 连接函数类型
 */
type TaskLinkFunc = (context: TaskFlowContext) => boolean;

/**
 * 任务连接
 */
class TaskLink {

    /**
     * 下一个任务的名称
     */
    private nextTaskName: string;

    /**
     * 连接方法
     */
    private linkFunc: TaskLinkFunc;

    /**
     * 构造函数
     */
    constructor(nextTaskName: string, linkFunc: TaskLinkFunc) {
        this.nextTaskName = nextTaskName;
        this.linkFunc = linkFunc;
    }

    /**
     * 是否允许连接
     */
    public canLink(context: TaskFlowContext): boolean {
        return this.linkFunc(context);
    }

    /**
     * 获取下一个任务名称
     */
    public getNextTaskName() {
        return this.nextTaskName;
    }
}

export { TaskLinkFunc, TaskLink };
