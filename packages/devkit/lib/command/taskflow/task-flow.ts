import { CommandContext } from '../command-context';
import { TaskFlowContext } from './task-flow-context';
import { TaskFunc, TaskNode } from './task-node';
import { TaskLink } from './task-link';

/**
 * 任务流
 */
class TaskFlow {

    /**
     * 上下文
     */
    private context: TaskFlowContext

    /**
     * 任务节点
     */
    private taskNodes: TaskNode[] = [];

    /**
     * 构造函数
     */
    constructor(commandContext: CommandContext) {
        this.context = this.buildContext(commandContext);
    }

    /**
     * 执行任务流
     */
    public async execute() {
        let currentTask: TaskNode | null = this.getFistTask();
        while (currentTask) {
            const currentTaskResult = await currentTask.execute(this.context);
            this.context.setTaskResult(currentTask.name, currentTaskResult);
            currentTask = this.getNextTask(currentTask);
        }
    }

    /**
     * 添加任务
     */
    public addTask(taskName: string, taskFunc: TaskFunc, taskLinks: TaskLink[]) {
        const taskNode = new TaskNode(taskName, taskFunc, taskLinks);
        this.taskNodes.push(taskNode);
    }

    /**
     * 获取开始任务
     */
    private getFistTask(): TaskNode {
        return this.taskNodes[0];
    }

    /**
     * 获取下一个任务
     */
    private getNextTask(currentTask: TaskNode): TaskNode | undefined {
        const nextTaskName = currentTask.getNextTaskName(this.context);
        const nextTask = this.taskNodes.find((task) => {
            return task.name === nextTaskName;
        });

        return nextTask;
    }

    /**
     * 构造上下文
     */
    public buildContext(commandContext: CommandContext) {
        const context = new TaskFlowContext(commandContext);
        return context;
    }
}

export { TaskFlow };


