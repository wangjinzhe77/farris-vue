import { createInjectionToken } from '../common';
import { CommandContext } from './command-context';

/**
 * 命令处理抽象类
 */
abstract class CommandHandler {

    /**
     * 命令名称
     */
    abstract commandName: string;


    /**
     * 构造函数
     */
    constructor() {
    }

    /**
     * 命令执行
     */
    abstract execute(context: CommandContext): any;

}

/**
 * 命令处理器注入Token
 */
const COMMAND_HANDLERS_TOKEN = createInjectionToken('COMMAND_HANDLERS_TOKEN');

export { CommandHandler, COMMAND_HANDLERS_TOKEN };
