import { createInjector, StaticProvider, Injector, TypeUtil } from '../common/index';
import { VariableParseContext, VariableParseService } from '../variable/index';
import { ConditionEvaluator } from '../condition/index';
import { CommandTaskParamConfig, CommandHandlerConfig, CommandTaskLinkConfig, CommandTaskConfig } from './configs/command-config';
import { CommandContext } from './command-context';
import { CommandHandler } from './command-handler';
import { TaskLinkFunc, TaskLink, TaskFunc, TaskFlowContext, TaskFlow, } from './taskflow/index';

declare const System: any;

/**
 * 动态命令处理器
 */
class DynamicCommandHandler extends CommandHandler {

    /**
     * 配置信息
     */
    private config!: CommandHandlerConfig;

    /**
     * 命令名称
     */
    public commandName: string;

    /**
     * 构造函数
     */
    constructor(config: CommandHandlerConfig) {
        super();
        this.config = config;
        this.commandName = config.commandName;
    }

    /**
     * 执行方法
     */
    public execute(commandContext: CommandContext) {
        const taskFlow = this.createTaskFlow(commandContext);
        return taskFlow.execute();
    }

    /**
     * 创建TaskFlow
     */
    private createTaskFlow(context: CommandContext): TaskFlow {
        const taskFlow = new TaskFlow(context);
        const taskConfigs = this.config.tasks;
        if (!taskConfigs) {
            throw new Error('There must be at least one task.');
        }
        taskConfigs.forEach((taskConfig) => {
            const taskName = taskConfig.name;
            const taskFunc = this.createTaskFunc(taskConfig);
            const taskLinks = this.createTaskLinks(taskConfig.links);

            taskFlow.addTask(taskName, taskFunc, taskLinks);
        });

        return taskFlow;
    }

    /**
     * 创建任务函数
     */
    private createTaskFunc(taskConfig: CommandTaskConfig): TaskFunc {
        const taskFunc = (taskFlowContext: TaskFlowContext) => {
            const methodName = taskConfig.method;
            const methodParams = this.getMethodParams(taskConfig.params, taskFlowContext);

            const serviceInstancePromise = this.getServiceInstance(taskConfig, taskFlowContext.commandContext);
            if (TypeUtil.isPromise(serviceInstancePromise)) {
                serviceInstancePromise.then((serviceInstance) => {     
                    serviceInstance['context'] = taskFlowContext.commandContext;
                    return serviceInstance[methodName](...methodParams);
                });
            } else {
                const serviceInstance = serviceInstancePromise;
                serviceInstance['context'] = taskFlowContext.commandContext;
                return serviceInstance[methodName](...methodParams);
            }
        };

        return taskFunc;
    }

    /**
     * 获取服务实例
     */
    private getServiceInstance(taskConfig: CommandTaskConfig, commandContext: CommandContext): any | Promise<any> {
        const injector = commandContext.getViewModelInjector() as any;
        const serviceName = taskConfig.service;
        const serviceUrl = taskConfig.serviceUrl;
        const serviceDeps = taskConfig.serviceDeps || [];

        const serviceToken = this.getServiceToken(serviceName, commandContext);
        if (!serviceToken && serviceUrl) {
            return this.getExternalServiceInstance(serviceName, serviceUrl, serviceDeps, injector);
        }

        return injector.get(serviceToken);
    }

    /**
     * 获取外部服务
     */
    private getExternalServiceInstance(serviceName: string, serviceUrl: string, serviceDeps: any[], parentInjector: Injector) {
        const promise = System.import(serviceUrl).then((module) => {
            const serviceClass = module[serviceName];
            const provider: StaticProvider = {
                provide: serviceClass,
                useClass: serviceClass,
                deps: serviceDeps
            }
            const injector = createInjector([provider], parentInjector);
            const serviceInstance = injector.get(serviceClass);

            const serviceRecord = (injector as any)._records.get(serviceClass);
            (parentInjector as any)._records.set(serviceClass, serviceRecord);

            return serviceInstance;
        });

        return promise;
    }

    /**
     * 获取方法参数
     */
    private getMethodParams(taskParamConfigs: CommandTaskParamConfig[], taskFlowContext: TaskFlowContext) {
        const params = taskParamConfigs.map((taskParamConfig) => {
            return taskParamConfig.value;
        });

        const variableParserService = this.getVariableParseService(taskFlowContext);
        const variableParseContext = this.getVariableParseContext(taskFlowContext);
        const parsedParams = variableParserService.parse(params, variableParseContext);

        return parsedParams;
    }

    /**
     * 获取服务注入Token
     */
    private getServiceToken(serviceName: string, context: CommandContext) {
        const injector = context.getViewModelInjector() as any;
        const tokens = Array.from(injector._records.keys()) as any[];
        const serviceToken = tokens.find((token) => {
            return token && token.name === serviceName;
        });

        return serviceToken;
    }

    /**
     * 创建任务连接
     */
    public createTaskLinks(taskLinkConfigs: CommandTaskLinkConfig[]): TaskLink[] {
        if (!Array.isArray(taskLinkConfigs)) {
            return [];
        }

        const taskLinks = [];
        taskLinkConfigs.forEach((taskLinkConfig) => {
            const nextTaskName = taskLinkConfig.nextTaskName;
            const taskLinkFunc = this.createTaskLinkFunc(taskLinkConfig.conditions);
            const taskLink = new TaskLink(nextTaskName, taskLinkFunc);
            taskLinks.push(taskLink);
        })

        return taskLinks;
    }

    /**
     * 创建连接函数
     */
    public createTaskLinkFunc(conditions: any): TaskLinkFunc {
        let linkFunc: TaskLinkFunc;
        if (typeof conditions === 'boolean') {
            linkFunc = () => {
                return true;
            }
        } else if (typeof conditions === 'string') {
            linkFunc = (taskFlowContext: TaskFlowContext) => {
                const variableParserService = this.getVariableParseService(taskFlowContext);
                const variableParseContext = this.getVariableParseContext(taskFlowContext);
                return variableParserService.evaluate(conditions, variableParseContext);
            }
        } else if (Array.isArray(conditions)) {
            linkFunc = (taskFlowContext: TaskFlowContext) => {
                const variableParseContext = this.getVariableParseContext(taskFlowContext);
                const conditionEvaluator = new ConditionEvaluator(variableParseContext);

                return conditionEvaluator.evaluate(conditions);
            }
        } else {
            linkFunc = () => {
                return false;
            }
        }

        return linkFunc;
    }

    /**
     * 获取变量解析服务
     */
    private getVariableParseService(taskFlowContext: TaskFlowContext): VariableParseService {
        const commandContext = taskFlowContext.commandContext;
        const variableParserService = commandContext.getVariableParseService();

        return variableParserService;
    }

    /**
     * 获取变量解析上下文
     */
    private getVariableParseContext(taskFlowContext: TaskFlowContext) {
        const commandContext = taskFlowContext.commandContext;
        const viewModel = commandContext.viewModel;
        const variableParseContext: VariableParseContext = {
            module: viewModel.getModule(),
            viewModel: viewModel,
            extraContext: taskFlowContext
        };

        return variableParseContext;
    }
}

export { DynamicCommandHandler };