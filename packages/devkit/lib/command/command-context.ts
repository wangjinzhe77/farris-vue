import { Injector } from '../common';
import { VariableParseService } from '../variable/index';
import { ViewModelState, ViewModel } from '../viewmodel';
import { Command } from './command';

/**
 * 命令上下文
 */
class CommandContext {

    /**
     * 命令信息
     */
    public command: Command;

    /**
     * 视图模型
     */
    public viewModel: ViewModel<ViewModelState>;

    /**
     * 构造方法
     */
    constructor(command: Command, viewModel: ViewModel<ViewModelState>) {
        this.command = command;
        this.viewModel = viewModel;
    }

    /**
     * 获取参数
     */
    public getParam(name: string): any {
        return null;
    }

    /**
     * 获取事件参数
     */
    public getEventParam(): any {
        return null;
    }

    /**
     * 获取视图模型
     */
    public getViewModel(): ViewModel<ViewModelState> {
        return this.viewModel;
    }

    /**
     * 获取视图模型
     */
    public getViewModelInjector(): Injector {
        return this.viewModel.getInjector();
    }

    /**
     * 获取变量解析服务
     */
    public getVariableParseService() {
        const injecotr = this.viewModel.getInjector();
        const variableParseService = injecotr.get<VariableParseService>(VariableParseService);

        return variableParseService;
    }
}

export { CommandContext };
