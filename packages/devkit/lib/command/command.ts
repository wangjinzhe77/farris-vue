/**
 * 命令参数定义
 */
interface CommandParams {
    [key: string]: any;
}

/**
 * 命令定义
 */
interface Command {

    /**
     * 命令名称
     */
    name: string;

    /**
     * 命令参数
     */
    params?: CommandParams;

    /**
     * 事件参数
     */
    eventParams?: any;
}

export { CommandParams, Command };
