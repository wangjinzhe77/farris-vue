/* eslint-disable import/no-duplicates */
import axios from 'axios';
import { AxiosInstance, AxiosResponse } from 'axios';
import { HttpMethod, HttpRequestConfig } from './types';
import { HttpUtil } from './http-util';

/**
 * HttpClient
 */
class HttpClient {

    /**
     * axios实例
     */
    private axiosInstance: AxiosInstance;

    /**
     * 构造函数
     */
    constructor() {
        this.axiosInstance = axios.create();
    }

    /**
     * 发送GET请求
     */
    public get(url: string, requestConfig: HttpRequestConfig): Promise<any> {
        return this.request('GET', url, requestConfig);
    }

    /**
     * 发送POST请求
     */
    public post(url: string, body: any, requestConfig: HttpRequestConfig): Promise<any> {
        requestConfig = HttpUtil.appendBodyToRequestConfig(body, requestConfig);
        return this.request('POST', url, requestConfig);
    }

    /**
     * 发送PUT请求
     */
    public put(url: string, body: any, requestConfig: HttpRequestConfig): Promise<any> {
        requestConfig = HttpUtil.appendBodyToRequestConfig(body, requestConfig);
        return this.request('PUT', url, requestConfig);
    }

    /**
     * 发送PATCH请求
     */
    public patch(url: string, body: any, requestConfig: HttpRequestConfig): Promise<any> {
        requestConfig = HttpUtil.appendBodyToRequestConfig(body, requestConfig);
        return this.request('PATCH', url, requestConfig);
    }

    /**
     * 发送DELETE请求
     */
    public delete(url: string, requestConfig: HttpRequestConfig): Promise<any> {
        return this.request('DELETE', url, requestConfig);
    }

    /**
     * 发送请求
     */
    public request(method: HttpMethod, url: string, requestConfig: HttpRequestConfig): Promise<any> {
        const axiosRequestConfig = HttpUtil.buildAxiosRequestConfig(method, url, requestConfig);
        const responsePromise = this.axiosInstance.request(axiosRequestConfig).then((axiosResponse: AxiosResponse) => {
            const httpResponse = HttpUtil.buildHttpResponse(axiosResponse);
            return requestConfig.observe === 'response' ? httpResponse : axiosResponse.data;
        });

        return responsePromise;
    }
}

export { HttpClient };
