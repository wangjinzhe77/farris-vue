import { StaticProvider } from '../common/index';
import { HttpClient } from './http-client';

const httpProviders: StaticProvider[] = [
    { provide: HttpClient, useClass: HttpClient, deps: [] }
];

export { httpProviders };
