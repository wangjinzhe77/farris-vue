/**
 * 请求头信息
 */
interface HttpHeaders {
  [key: string]: string;
}

/**
 * Http参数
 */
interface HttpParams {
  [key: string]: string;
}

/**
 * 请求方法
 */
type HttpMethod = | 'GET' | 'DELETE' | 'HEAD' | 'OPTIONS' | 'POST' | 'PUT' | 'PATCH' | 'LINK' | 'UNLINK';

/**
 * HttpMethods
 */
class HttpMethods {
    public static GET: HttpMethod = 'GET';

    public static DELETE: HttpMethod = 'DELETE';

    public static HEAD: HttpMethod = 'HEAD';

    public static OPTIONS: HttpMethod = 'OPTIONS';

    public static POST: HttpMethod = 'POST';

    public static PUT: HttpMethod = 'PUT';

    public static PATCH: HttpMethod = 'PATCH';

    public static LINK: HttpMethod = 'LINK';

    public static UNLINK: HttpMethod = 'UNLINK';
}


/**
 * 返回值类型
 */
type HttpResponseType = | 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | 'stream';

/**
 * 返回值处理类型
 */
type ObserveType = 'body' | 'response';

/**
 * Http响应信息
 */
interface HttpResponse {
    headers: any;
    body: any;
    status: number;
    statusText: string;
}

/**
 * Http请求配置
 */
interface HttpRequestConfig {
    params?: HttpParams;
    body?: any;
    headers?: HttpHeaders;
    responseType?: HttpResponseType;
    observe?: 'body' | 'response';
}

export { HttpHeaders, HttpParams, HttpMethod, HttpMethods, ObserveType, HttpResponseType, HttpRequestConfig, HttpResponse };
