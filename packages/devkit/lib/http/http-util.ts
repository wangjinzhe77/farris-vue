import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { HttpMethod, HttpHeaders, HttpResponse, HttpRequestConfig } from './types';

class HttpUtil {

    /**
     * 追加Header
     */
    public static appendHeader(headers: HttpHeaders, key: string, value: string): HttpHeaders {
        headers = Object.assign({}, headers, { [key]: value });
        return headers;
    }

    /**
     * 向RequestConfig中追加body
     */
    public static appendBodyToRequestConfig(body: any, requestConfig: HttpRequestConfig) {
        if (!requestConfig) {
            requestConfig = {};
        }
        requestConfig = Object.assign({}, requestConfig, { body });

        return requestConfig;
    }

    /**
     * 构造AxiosReqeustConfig
     */
    public static buildAxiosRequestConfig(method: HttpMethod, url: string, requestConfig: HttpRequestConfig): AxiosRequestConfig {
        requestConfig = requestConfig || {};

        const axiosRequestConfig: AxiosRequestConfig = {
            url,
            method,
            params: requestConfig.params || null,
            headers: requestConfig.headers || null,
            responseType: requestConfig.responseType || 'json',
            data: requestConfig.body || null,
            withCredentials: true
        };
        return axiosRequestConfig;
    }

    /**
     * 构造Http响应信息
     */
    public static buildHttpResponse(axiosResponse: AxiosResponse) {
        const httpResponse: HttpResponse = {
            body: axiosResponse.data,
            headers: axiosResponse.headers,
            status: axiosResponse.status,
            statusText: axiosResponse.statusText
        };
        return httpResponse;
    }

}

export { HttpUtil };
