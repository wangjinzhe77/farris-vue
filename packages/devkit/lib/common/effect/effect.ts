/* eslint-disable @typescript-eslint/ban-types */

/**
 * 副作用
 */
class Effect<T> {

    /**
     * 副作用方法
     */
    private internalFunction: Function;

    /**
     * 副作用构造器
     * @param effectFunction 副作用方法
     */
    constructor(private effectFunction: Function) {
        this.internalFunction = this.effectFunction;
    }

    /**
     * 执行副作用
     * @param change 变更 
     */
    public run(change: T) {
        this.internalFunction(change);
    }

}

export { Effect }
