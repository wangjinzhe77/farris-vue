import { Effect } from "../effect/effect";

/**
 * 副作用管理
 */
class EffectManager<T> {

    /**
     * 副作用集合
     */
    private innerEffects: Effect<T>[] = [];

    /**
     * 构造函数
     */
    constructor() {
    }

    /**
     * 添加副作用
     */
    public addEffect(effect: Effect<T>): void {
        if (this.innerEffects.indexOf(effect) > -1) {
            return;
        }
        this.innerEffects.push(effect);
    }

    /**
     * 删除副作用
     */
    public removeEffect(effect: Effect<T>): void {
        this.innerEffects = this.innerEffects.filter(internalEffect => effect !== internalEffect);
    }

    /**
     * 获取全部副作用
     */
    public getEffects(): Effect<T>[] {
        return this.innerEffects;
    }

    /**
     * 执行全部副作用
     */
    public trigger(change: T) {
        this.innerEffects.forEach((effect: Effect<T>) => {
            effect.run(change);
        });
    }
}

export { EffectManager };
