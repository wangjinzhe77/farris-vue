export class Token {
    private static previous = 0;
    public static create(redix?: number) {
        const timestamp = Date.now().valueOf();
        let uuid = 0;
        if (timestamp > Token.previous) {
            Token.previous = timestamp;
            uuid = timestamp;
        } else {
            Token.previous = Token.previous + 100;
            uuid = Token.previous;
        }
        return uuid.toString(redix);
    }
}