/* eslint-disable @typescript-eslint/no-unused-expressions */
/* eslint-disable @typescript-eslint/no-unsafe-function-type */
/* eslint-disable @typescript-eslint/no-explicit-any */

import { Type } from '../types';
import { ANNOTATIONS, PROP_METADATA } from './consts';
import { TypeDecorator } from './types';

/**
 * 将元数据工厂方法转换为元数据构造函数
 * @param metaFactory 元数据工厂方法
 * @returns 元数据构造函数
 */
function makeMetaCtor(metaFactory?: (...args: any[]) => any): any {
    return function ctor(this: any, ...args: any[]) {
        if (metaFactory) {
            const values = metaFactory(...args);
            for (const propName in values) {
                this[propName] = values[propName];
            }
        }
    };
}

/**
 * 创建类型元数据装饰器
 */
export function makeTypeMetadataDecorator(
    name: string,
    props?: (...args: any[]) => any,
    parentClass?: any,
    chainFn?: (fn: Function) => void,
    typeFn?: (type: Type<any>, ...args: any[]) => void
): {
    new(...args: any[]): any;
    (...args: any[]): any;
    (...args: any[]): (cls: any) => any;
} {
    
    const metaCtor = makeMetaCtor(props);

    // 类型装饰器工厂
    function DecoratorFactory(this: unknown | typeof DecoratorFactory, ...args: any[]): (cls: any) => any {
        if (this instanceof DecoratorFactory) {
            metaCtor.call(this, ...args);
            return this as typeof DecoratorFactory;
        }

        const annotationInstance = new (<any>DecoratorFactory)(...args);

        // 类型装饰器
        const typeDecorator: TypeDecorator = <TypeDecorator>function createTypeDecorator(cls: Type<any>) {
            typeFn && typeFn(cls, ...args);
            const annotations = cls.hasOwnProperty(ANNOTATIONS) ?
                (cls as any)[ANNOTATIONS] :
                Object.defineProperty(cls as any, ANNOTATIONS, { value: [] })[ANNOTATIONS];
            annotations.push(annotationInstance);
            return cls;
        };

        if (chainFn) {
            chainFn(typeDecorator);
        }
        return typeDecorator;
    }

    if (parentClass) {
        DecoratorFactory.prototype = Object.create(parentClass.prototype);
    }

    DecoratorFactory.prototype.ngMetadataName = name;
    (<any>DecoratorFactory).annotationCls = DecoratorFactory;
    return DecoratorFactory as any;
}


/**
 * 获取所有属性的元数据集合
 * @param target 被装饰属性所属对象
 * @return 所有属性的原数据集合，格式形如：{ propaA: [], propB: []};
 */
function getAllPropMetas(target: any): { [key: string]: any[] } {
    const constructor = target.constructor;
    let allPropMetas = null;
    if (constructor.hasOwnProperty(PROP_METADATA)) {
        allPropMetas = (constructor as any)[PROP_METADATA]
    } else {
        allPropMetas = Object.defineProperty(constructor, PROP_METADATA, { value: {} })[PROP_METADATA];
    }

    return allPropMetas;
}

/**
 * 获取单个属性的元数据集合
 * @param target 被装饰属性所属对象
 * @param propName 被装饰属性的名称
 * @return 单个属性元数据集合
 */
function getSinglePropMetas(target: any, propName: string): any[] {
    const allPropMetas = getAllPropMetas(target);
    if (allPropMetas.hasOwnProperty(propName)) {
        return allPropMetas[propName];
    }

    return [];
}

/**
 * 创建属性注解工厂
 * @param name 元数据名称
 * @param metaFactory 元数据工厂方法
 * @param parentClass 父类
 */
function makePropMetadataDecorator(name: string, metaFactory?: (...args: any[]) => any, parentClass?: any): any {
    const metaCtor = makeMetaCtor(metaFactory);

    // 属性装饰器工厂
    function PropDecoratorFactory(this: unknown | typeof PropDecoratorFactory, ...args: any[]): any {
        if (this instanceof PropDecoratorFactory) {
            metaCtor.apply(this, args);
            return this;
        }

        const decoratorInstance = new (<any>PropDecoratorFactory)(...args);

        // 属性装饰器
        function PropDecorator(target: any, propName: string) {
            const allPropMetas = getAllPropMetas(target);
            allPropMetas[propName] = getSinglePropMetas(target, propName);
            allPropMetas[propName].unshift(decoratorInstance);
        };

        return PropDecorator;
    }

    if (parentClass) {
        PropDecoratorFactory.prototype = Object.create(parentClass.prototype);
    }

    PropDecoratorFactory.prototype.metadataName = name;
    PropDecoratorFactory.annotationClass = PropDecoratorFactory;

    return PropDecoratorFactory;
}

export { makePropMetadataDecorator };
