/* eslint-disable @typescript-eslint/no-explicit-any */
 

import { Type } from '../types';

/**
 * 类型装饰器方法
 */
export interface TypeDecorator {
    <T extends Type<any>>(type: T): T;
    (target: object, propertyKey?: string | symbol, parameterIndex?: number): void;
}

/**
 * 属性装饰器方法
 */
interface PropDecorator {
    (target: any, propName: string): void;
}

/**
 * 所有属性的所有元数据
 */
interface PropsMetadatas {
    [propName: string]: any[];
}

/**
 * 所有属性某个名称的元数据
 */
interface SameNamePropsMetadatas<T> {
    [propName: string]: T;
}

export { PropDecorator, PropsMetadatas, SameNamePropsMetadatas };
