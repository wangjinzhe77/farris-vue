/* eslint-disable @typescript-eslint/no-explicit-any */
import { ANNOTATIONS, PROP_METADATA } from './consts';
import { PropsMetadatas, SameNamePropsMetadatas } from './types';


/**
 * 类型元数据读取器
 */
class TypeMetadataReader {

    /**
     * 获取所有的元数据
     */
    public static getMetas(constructor: any): any  {
        const metas = constructor[ANNOTATIONS];
        if (!metas) {
            return {};
        }

        return metas;
    }

    /**
     * 根据名称获取元数据
     */
    public static getMataByName(constructor: any, metaName: string): any {
        const metas = this.getMetas(constructor);
        if (!metas) {
            return {};
        }

        const targetMeta = metas.find((meta: any) => {
            return meta.ngMetadataName = metaName;
        });

        return targetMeta;
    }
}


/**
 * 属性元数据读取器
 */
class PropMetadataReader {

    /**
     * 获取所有属性的所有类型元数据
     */
    public static getPropsMetas(constructor: any): PropsMetadatas {
        const propsMetas = constructor[PROP_METADATA];
        if (!propsMetas) {
            return {};
        }

        return propsMetas;
    }

    /**
     * 获取所有属性某一类型的元数据
     */
    public static getPropsMetasByName<T>(constructor: any, metaName: string): SameNamePropsMetadatas<T> {
        const propsMetas = this.getPropsMetas(constructor);
        if (!propsMetas) {
            return {};
        }

        const sameNamePropsMetas: SameNamePropsMetadatas<T> = {};
        Object.keys(propsMetas).forEach((propName: string) => {
            const propMetas = propsMetas[propName];
            const targetPropMeta = propMetas.find((propMeta: any) => {
                return propMeta.metadataName === metaName;
            });

            if (!targetPropMeta) {
                return;
            }

            sameNamePropsMetas[propName] = targetPropMeta;
        });

        return sameNamePropsMetas;
    }
}

export { TypeMetadataReader,  PropMetadataReader };
