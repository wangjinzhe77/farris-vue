/* eslint-disable @typescript-eslint/prefer-function-type */

export interface Type<T> extends Function {
    new(...args: any[]): T;
}

export const Type = Function;

export interface ExceptionHandler {
    handle(error: any);
}

export interface RenderEngine {
    /**
     * 获取组件实例
     * @param id
     * @param schema
     */
    getComponentById(id: string);
    /**
     * 调用组件方法
     * @param id
     * @param methodName
     * @param args
     */
    invokeMethod(id: string, methodName: string, ...args: any[]);
    /**
     * 重新渲染组件
     * @param id
     */
    rerender(id: string);
    /**
     * 更新组件属性
     * @param componentId 
     * @param props 
     */
    setProps(componentId: string, props: Record<string,any>);
    /**
     * 获取组件属性
     * @param componentId
     */
    getProps(componentId: string);
    /**
     * 更新组件schema
     * @param id 
     * @param schema 
     */
    setSchema(componentId: string, schema: Record<string, any>);
    /**
     * 获取组件schema
     * @param componentId 
     */
    getSchema(componentId: string): Record<string,any>;
}
