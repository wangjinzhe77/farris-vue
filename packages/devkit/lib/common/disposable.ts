/**
 * 资源释放接口
 */
interface IDisposable {
    dispose(): void;
}

export { IDisposable };
