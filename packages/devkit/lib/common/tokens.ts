import { InjectionToken } from "./di/injection-token";
import { ExceptionHandler, RenderEngine } from "./types";

export const EXCEPTION_HANDLER_TOKEN = new InjectionToken<ExceptionHandler>('@farris/exception_handler_token');

export const RENDER_ENGINE_TOKEN = new InjectionToken<RenderEngine>('@farris/render_engine_token');
