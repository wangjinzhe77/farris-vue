class TypeUtil {

    /**
     * 是否为Promsie对象
     */
    public static isPromise(obj: any) {
        return obj instanceof Promise && Object.getPrototypeOf(obj) === Promise.prototype;
    }
}

export { TypeUtil };