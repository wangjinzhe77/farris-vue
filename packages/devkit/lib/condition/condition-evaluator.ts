import { Module } from '../module/index';
import { VariableParseContext, VariableParseService } from '../variable/index';
import { ConditionConfig } from './types';

/**
 * 条件求值器
 */
class ConditionEvaluator {

    /**
     * 模块
     */
    private module: Module;

    /**
     * 上下文
     */
    private context: VariableParseContext;

    /**
     * 解析服务
     */
    private variableParseServce: VariableParseService;

    /**
     * 构造函数
     */
    constructor(context: VariableParseContext) {
        this.context = context;
        this.module = context.module;
        const moduleInjector = this.module.getInjector();
        this.variableParseServce = moduleInjector.get<VariableParseService>(VariableParseService);
    }

    /**
     * 求值
     */
    public evaluate(conditions: ConditionConfig[]): boolean {
        const tokens = this.mergeConditions(conditions);
        const result = this.evaluateExpression(tokens);

        return result;
    }

    /**
     * 解析条件
     */
    private mergeConditions(conditions: ConditionConfig[]) {
        let tokens: any[] = [];
        const conditionCount = conditions.length;
        conditions.forEach((condition: ConditionConfig, index: number) => {
            const leftBrackets = this.splitBracketString(condition.lBraceket);
            const rightBrackets = this.splitBracketString(condition.rBraceket);
            const comparisonResult = this.compareSourceAndTarget(condition.source, condition.target, condition.compare);
            const relation = (index === conditionCount - 1) ? [] : [condition.relation];

            tokens = tokens.concat(...leftBrackets, comparisonResult, ...rightBrackets, ...relation);
        });

        return tokens;
    }

    /**
     * 比较Source和Target的值
     */
    private compareSourceAndTarget(source: any, target: any, operator: string,) {
        const sourceValue = this.parseValue(source);
        const targetValue = this.parseValue(target);
        const result = this.compareValue(sourceValue, targetValue, operator);

        return result;
    }

    /**
     * 解析括号字符串
     */
    private splitBracketString(bracketString: string): string[] {
        if (!bracketString) {
            return [];
        }

        let brackets: string[] = [];
        if (bracketString.indexOf('(') !== -1) {
            brackets = bracketString.split('');
        } else {
            brackets = bracketString.split('');
        }

        return brackets;
    }

    /**
     * 解析值
     */
    private parseValue(expression: string) {
        const parsedExpression = this.variableParseServce.parse(expression, this.context);

        return parsedExpression;
    }

    /**
     * 比较值
     */
    private compareValue(sourceValue: any, targetValue: any, operator: string) {
        switch (operator) {
            case '===':
                return sourceValue === targetValue;
            case '!==':
                return sourceValue !== targetValue;
            case '>':
                return sourceValue > targetValue;
            case '>=':
                return sourceValue >= targetValue;
            case '<':
                return sourceValue < targetValue;
            case '<=':
                return sourceValue <= targetValue;
            default:
                throw new Error('Invalid operator');
        };
    }

    /**
     * 计算表达式
     */
    private evaluateExpression(tokens: any[]) {
        tokens = this.handleBracekets(tokens);
        tokens = this.handleLogicalAnd(tokens);
        tokens = this.handleLogicalOr(tokens);
        const result = tokens[0];
        return typeof result === 'string' ? parseFloat(result) : result;
    }

    /**
     * 处理括号
     */
    private handleBracekets(tokens: any[]): any[] {
        const result: any[] = [];
        let count = 0;
        let startIndex = 0;
        for (let i = 0; i < tokens.length; i++) {
            if (tokens[i] === '(') {
                count++;
                if (count === 1) {
                    startIndex = i;
                }
            } else if (tokens[i] === ')') {
                count--;
                if (count === 0) {
                    const subTokens = tokens.slice(startIndex + 1, i);
                    result.push(this.evaluateExpression(subTokens));
                    startIndex = i + 1;
                }
            } else if (count === 0) {
                result.push(tokens[i]);
            }
        }
        return result;
    }

    /**
     * 处理逻辑 与
     */
    private handleLogicalAnd(tokens: any[]): any[] {
        const result: any[] = [];
        for (let i = 0; i < tokens.length; i++) {
            result.push(tokens[i]);
            if (tokens[i] === 'and') {
                const leftOperand = result[result.length - 2];
                const rightOperand = tokens[i + 1];
                const value = leftOperand && rightOperand;
                result.pop();
                result.pop();
                result.push(value);
                i++;
            }
        }
        return result;
    }

    /**
     * 处理逻辑 或
     */
    private handleLogicalOr(tokens) {
        const result: any[] = [];
        for (let i = 0; i < tokens.length; i++) {
            result.push(tokens[i]);
            if (tokens[i] === 'or') {
                const leftOperand = result[result.length - 2];
                const rightOperand = tokens[i + 1];
                const value = leftOperand || rightOperand;
                result.pop();
                result.pop();
                result.push(value);
                i++;
            }
        }
        return result;
    }
}

export { ConditionEvaluator };
