/**
 * 条件配置
 */
interface ConditionConfig {
    lBraceket: string;
    source: string;
    compare: string;
    target: any;
    relation: string;
    rBraceket: string;
}

export { ConditionConfig }
