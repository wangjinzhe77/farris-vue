import { provide, inject } from 'vue';
import { StaticProvider, Injector, Type, createInjector } from '../common/index';
import { DEVKIT_INJECTION_TOKEN, Devkit } from '../devkit';
import { RepositoryConfig, getDynamicRepositoryToken } from '../repository/index';
import { MODULE_INJECTION_TOKEN, Module } from './module';
import { ModuleConfig, ModuleConfigBuilder } from './configs/index';

/**
 * 创建远程实体仓库注入
 */
function createRepositoryProviders(repositoryConfigs: RepositoryConfig[]) {
    if (!repositoryConfigs || !Array.isArray(repositoryConfigs)) {
        return [];
    }

    const providers: StaticProvider[] = [];
    repositoryConfigs.forEach((repositoryConfig) => {
        if (!repositoryConfig) {
            throw new Error('RepositoryConfig does not exist');

        }
        if (!repositoryConfig.type) {
            throw new Error(`The type property of  RepositoryConfig(id=${repositoryConfig.id}) can not be empty`);
        }
        const type = repositoryConfig.type;
        const deps = repositoryConfig.deps || [];

        let provider: StaticProvider;
        if (repositoryConfig.isDynamic) {
            const dynamicRepositoryToken = getDynamicRepositoryToken(repositoryConfig.id);
            provider = {
                provide: dynamicRepositoryToken,
                useClass: type,
                deps: [Module]
            };
        } else {
            provider = { provide: type, useClass: type, deps: [...deps] };
        }
        providers.push(provider);

    });

    return providers;
}

/**
 * 创建模块Providers
 */
function createModuleProviders(moduleConfig: ModuleConfig) {
    const extendProviders: StaticProvider[] = moduleConfig.providers || [];
    const repositoryProviders = createRepositoryProviders(moduleConfig.repositories);
    const providers = [
        ...extendProviders,
        ...repositoryProviders
    ];

    if (moduleConfig.type) {
        providers.push({ provide: Module, useClass: moduleConfig.type, deps: [Injector] });
    } else {
        providers.push({ provide: Module, useClass: Module, deps: [Injector] });
    }

    return providers;
}

/**
 * 创建动态模块
 */
function createDynamicModule(moduleConfig: ModuleConfig) {
    const devkit = inject(DEVKIT_INJECTION_TOKEN) as Devkit;
    if (!devkit) {
        throw new Error('Devkit can not be empty');
    }
    const devkitInjector = devkit.getInjector();

    const providers = createModuleProviders(moduleConfig);
    const moduleInjector = createInjector(providers, devkitInjector);
    const module = moduleInjector.get<Module>(Module);
    module.init(moduleConfig);

    devkit.regitsterModule(moduleConfig.id, module);
    provide(MODULE_INJECTION_TOKEN, module);

    return module;
}

/**
 * 创建静态模块
 */
function createStaticModule(moduleType: Type<Module>) {
    const moduleConfigBuilder = new ModuleConfigBuilder();
    const moduleConfig = moduleConfigBuilder.build(moduleType);
    const module = createDynamicModule(moduleConfig);

    return module;
}

export { createDynamicModule, createStaticModule };