import { Injector } from '../common/index';
import { useDevkit } from '../devkit';
import { Entity, EntityState, EntityStore, UIState, UIStore, StateMachineState, StateMachine } from '../store/index';
import { Repository, getDynamicRepositoryToken } from '../repository/index';
import { ViewModel, ViewModelState } from '../viewmodel/index';
import { ModuleConfig, ModuleConfigManager } from './configs/index';
import { Context } from '../context';

/**
 * 模块注入Token
 */
const MODULE_INJECTION_TOKEN = Symbol('Module');

/**
 * 模块定义
 */
class Module {
    /**
     * 模块ID
     */
    private id: string;

    /**
     * 注入器
     */
    private injector!: Injector;

    /**
     * 配置管理
     */
    private configManager!: ModuleConfigManager;

    /**
     * 实体仓库集合
     */
    private entityStores: Map<string, EntityStore<EntityState<Entity>>>;

    /**
     * UI仓库集合
     */
    private uiStores: Map<string, UIStore<UIState>>;

    /**
     * 状态机集合
     */
    private stateMachines: Map<string, StateMachine<StateMachineState>>;

    /**
     * 远程实体仓库集合
     */
    private repositories: Map<string, Repository<Entity>>;

    /**
     * 视图模型集合
     */
    private viewModels: Map<string, ViewModel<ViewModelState>>;

    /**
     * 上下文
     */
    private context: Context;

    /**
     * 构造函数
     */
    constructor(injector: Injector) {
        this.injector = injector;
        this.entityStores = new Map<string, EntityStore<EntityState<Entity>>>();
        this.uiStores = new Map<string, UIStore<UIState>>();
        this.stateMachines = new Map<string, StateMachine<StateMachineState>>();
        this.repositories = new Map<string, Repository<Entity>>;
        this.viewModels = new Map<string, ViewModel<ViewModelState>>();
        this.context = new Context();
        this.id = new Date().getTime().toString();
    }

    /**
     * 初始化
     */
    public init(config: ModuleConfig) {
        if (!config) {
            throw new Error('Module must be initialized using ModuleConfig');
        }
        this.configManager = new ModuleConfigManager(config);
    }

    /**
     * 获取注入器
     */
    public getInjector(): Injector {
        return this.injector;
    }

    /**
     * 获取配置管理器
     */
    public getConfigManager() {
        return this.configManager;
    }

    /**
     * 获取视图模型
     */
    public getViewModel(id: string): ViewModel<ViewModelState> | null {
        const viewModel = this.viewModels.get(id);
        if (!viewModel) {
            return null;
        }

        return viewModel;
    }


    /**
     * 获取全部视图模型
     */
    public getViewModels(): ViewModel<ViewModelState>[] {
        return Array.from(this.viewModels.values());
    }

    /**
     * 获取实体仓库
     */
    public getEntityStore(id: string): EntityStore<EntityState<Entity>> | null {
        const entityStore = this.entityStores.get(id);
        if (!entityStore) {
            return null;
        }
        return entityStore;
    }

    /**
     * 获取全部实体仓库
     */
    public getEntityStores(): EntityStore<EntityState<Entity>>[] {
        return Array.from(this.entityStores.values());
    }

    /**
     * 获取UI仓库
     */
    public getUIStore(id: string): UIStore<UIState> | null {
        const uiStore = this.uiStores.get(id);
        if (!uiStore) {
            return null;
        }

        return uiStore;
    }

    /**
     * 获取全部UI仓库
     */
    public getUIStores(): UIStore<UIState>[] {
        return Array.from(this.uiStores.values());
    }

    /**
     * 获取状态机
     */
    public getStateMachine(id: string): StateMachine<StateMachineState> | null {
        const stateMachine = this.stateMachines.get(id);
        if (!stateMachine) {
            return null;
        }

        return stateMachine;
    }

    /**
     * 获取全部状态机
     */
    public getStateMachines(): StateMachine<StateMachineState>[] {
        return Array.from(this.stateMachines.values());
    }

    /**
     * 获取远程实体仓库
     */
    public getRepository(id: string): Repository<Entity> | null {
        const repository = this.repositories.get(id);
        if (!repository) {
            return null;
        }

        return repository;
    }

    /**
     * 注册视图模型
     */
    public registerViewModel(id: string, viewModel: ViewModel<ViewModelState>) {
        this.viewModels.set(id, viewModel);
    }

    /**
     * 构造实体仓库
     */
    public createEntityStore(id: string): EntityStore<EntityState<Entity>> {
        let store = this.getEntityStore(id);
        if (store) {
            return store;
        }

        const storeConfig = this.configManager.getEntityStoreConfig(id);
        if (!storeConfig) {
            throw new Error(`StoreConfig(id=${id}) does not exist`);
        }

        if (storeConfig.type) {
            store = this.injector.get(storeConfig.type);
        } else {
            store = new EntityStore(this);
        }
        store.init(storeConfig);
        this.entityStores.set(id, store);

        return store;
    }

    /**
     * 创建UI仓库
     */
    public createUIStore(id: string): UIStore<UIState> {
        let store = this.getUIStore(id);
        if (store) {
            return store;
        }

        const storeConfig = this.configManager.getUIStoreConfig(id);
        if (!storeConfig) {
            throw new Error(`StoreConfig(id=${id}) does not exist`);
        }

        if (storeConfig.type) {
            store = this.injector.get(storeConfig.type);
        } else {
            store = new UIStore(this);
        }
        store.init(storeConfig);
        this.uiStores.set(id, store);

        return store;
    }
    /**
     * 创建状态机
     */
    public createStateMachine(id: string): StateMachine<StateMachineState> {
        let store = this.getStateMachine(id);
        if (store) {
            return store;
        }

        const storeConfig = this.configManager.getStateMachineConfig(id);
        if (!storeConfig) {
            throw new Error(`StoreConfig(id=${id}) does not exist`);
        }

        if (storeConfig.type) {
            store = this.injector.get(storeConfig.type);
        } else {
            store = new StateMachine(this);
        }
        store.init(storeConfig);
        this.stateMachines.set(id, store);

        return store;
    }

    /**
     * 创建模块
     */
    public createRepository(id: string): Repository<Entity> {
        let repository = this.getRepository(id);
        if (repository) {
            return repository;
        }

        const repositoryConfig = this.configManager.getRepositoryConfig(id);
        if (!repositoryConfig) {
            throw new Error(`RepositoryConfig(id=${id}) does not exist`);
        }

        if (repositoryConfig.isDynamic) {
            const dynamicRepositoryToken = getDynamicRepositoryToken(id);
            repository = this.injector.get(dynamicRepositoryToken);
        } else {
            repository = this.injector.get(repositoryConfig.type);
        }
        repository.init(repositoryConfig);
        this.repositories.set(id, repository);

        return repository;
    }
    /**
     * module上下文
     * @returns 
     */
    public getContext() {
        return this.context;
    }
    public getId(){
        return this.id;
    }
}

/**
 * 使用模块
 */
function useModule(id: string): Module {
    const devkit = useDevkit();
    const module = devkit.getModule(id) as Module;
    if (!module) {
        throw new Error('Please create Module before using it');
    }

    return module;
}

export { MODULE_INJECTION_TOKEN, Module, useModule }; 