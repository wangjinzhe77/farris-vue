/* eslint-disable @typescript-eslint/no-explicit-any */
import { StaticProvider, Type, TypeDecorator, makeTypeMetadataDecorator } from '../../common/index';
import { ViewModelState, ViewModel } from '../../viewmodel/index';
import { Repository } from '../../repository/index';
import { Entity, EntityState, EntityStore, UIState, UIStore, StateMachineState, StateMachine } from '../../store/index';

/**
 * 模块元数据名称
 */
const MODULE_META_NAME = 'MODULE';

/**
 * 模块元数据
 */
interface ModuleMeta {
    id: string;
    deps?: any[],
    providers?: StaticProvider[],

    repositorys?: Type<Repository<Entity>>[];
    entityStores?: Type<EntityStore<EntityState<Entity>>>[];
    uiStores?: Type<UIStore<UIState>>[];
    stateMachines?: Type<StateMachine<StateMachineState>>[];
    viewModels?: Type<ViewModel<ViewModelState>>[]
}

/**
 * 模块元数据工厂
 */
const moduleMetaFactory = (meta: ModuleMeta): ModuleMeta => {
    return meta;
};

/**
 * 模块装饰器工厂类型
 */
interface ModuleDecoratorFactory {
    (meta: ModuleMeta): TypeDecorator;
}

/**
 * 模块装饰器工厂类型
 */
function FdModule(meta: ModuleMeta) {
    const decoratorFactory: ModuleDecoratorFactory = makeTypeMetadataDecorator(MODULE_META_NAME, moduleMetaFactory);
    return decoratorFactory(meta);
}

export { MODULE_META_NAME, ModuleMeta, moduleMetaFactory, ModuleDecoratorFactory, FdModule};
