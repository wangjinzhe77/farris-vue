import { StaticProvider, Injector, Type, TypeMetadataReader } from '../../common/index';
import {
    StoreConfig, Entity, EntityState, EntityStore, EntityStoreConfig, EntityStoreConfigBuilder,
    UIState, UIStore, UIStoreConfig, UIStoreConfigBuilder,
    StateMachineState, StateMachine, StateMachineConfig, StateMachineConfigBuilder
} from '../../store/index';
import { ViewModelState, ViewModel, ViewModelConfig, ViewModelConfigBuilder } from '../../viewmodel/index';
import { RepositoryConfig, RepositoryConfigBuilder, Repository } from '../../repository/index'
import { ModuleMeta, MODULE_META_NAME } from '../decorators/index';
import { Module } from '../module';
import { ModuleConfig } from './module-config';

/**
 * 模块配置构造器
 */
class ModuleConfigBuilder {

    /**
     * 构造模块配置
     */
    public build(moduleType: Type<Module>): ModuleConfig {
        const moduleMeta = this.getModuleMeta(moduleType);
        const entityStoreConfigs = this.buildEntityStoreConfigs(moduleMeta.entityStores);
        const uiStoreConfigs = this.buildUIStoreConfigs(moduleMeta.uiStores);
        const stateMachineConfigs = this.buildStateMachineConfigs(moduleMeta.stateMachines);
        const repositoryConfigs = this.buildRepositoryConfigs(moduleMeta.repositorys);
        const viewModelConfigs = this.buildViewModelConfigs(moduleMeta.viewModels);

        const moduleConfig: ModuleConfig = {
            id: moduleMeta.id,
            type: moduleType,
            deps: moduleMeta.deps || [],
            providers: moduleMeta.providers || [],
            repositories: repositoryConfigs,
            entityStores: entityStoreConfigs,
            uiStores: uiStoreConfigs,
            stateMachines: stateMachineConfigs,
            viewModels: viewModelConfigs
        };
        this.appendModuleProviders(moduleConfig);

        return moduleConfig;
    }

    /**
     * 构造远程实体仓库配置
     */
    private buildRepositoryConfigs(repositoryTypes: Type<Repository<Entity>>[]): RepositoryConfig[] {
        if (!Array.isArray(repositoryTypes) || repositoryTypes.length === 0) {
            return [];
        }

        const repositoryConfigs: RepositoryConfig[] = [];
        const repositoryConfigBuilder = new RepositoryConfigBuilder();
        repositoryTypes.forEach((repositoryType) => {
            const repositoryConfig = repositoryConfigBuilder.build(repositoryType);
            repositoryConfigs.push(repositoryConfig);
        });

        return repositoryConfigs;
    }

    /**
     * 构造实体仓库配置
     */
    private buildEntityStoreConfigs(entityStoreTypes: Type<EntityStore<EntityState<Entity>>>[] | undefined): EntityStoreConfig[] {
        if (!Array.isArray(entityStoreTypes) || entityStoreTypes.length === 0) {
            return [];
        }

        const entityStoreConfigs: EntityStoreConfig[] = [];
        const entityStoreConfigBuilder = new EntityStoreConfigBuilder();
        entityStoreTypes.forEach((entityStoreType) => {
            const entityStoreConfig = entityStoreConfigBuilder.build(entityStoreType);
            entityStoreConfigs.push(entityStoreConfig);
        });

        return entityStoreConfigs;
    }

    /**
     * 构造UI仓库配置
     */
    private buildUIStoreConfigs(uiStoreTypes: Type<UIStore<UIState>>[] | undefined): UIStoreConfig[] {
        if (!Array.isArray(uiStoreTypes) || uiStoreTypes.length === 0) {
            return [];
        }

        const uiStoreConfigs: UIStoreConfig[] = [];
        const uiStoreConfigBuilder = new UIStoreConfigBuilder();
        uiStoreTypes.forEach((uiStoreType) => {
            const uiStoreConfig = uiStoreConfigBuilder.build(uiStoreType);
            uiStoreConfigs.push(uiStoreConfig);
        });

        return uiStoreConfigs;
    }

    /**
     * 构造状态机配置
     */
    private buildStateMachineConfigs(stateMachineTypes: Type<StateMachine<StateMachineState>>[] | undefined): StateMachineConfig[] {
        if (!Array.isArray(stateMachineTypes) || stateMachineTypes.length === 0) {
            return [];
        }

        const stateMachineConfigs: StateMachineConfig[] = [];
        const stateMachineConfigBuilder = new StateMachineConfigBuilder();
        stateMachineTypes.forEach((stateMachineType) => {
            const stateMachineConfig = stateMachineConfigBuilder.build(stateMachineType);
            stateMachineConfigs.push(stateMachineConfig);
        });

        return stateMachineConfigs;
    }

    /**
     * 构造视图模型配置
     */
    private buildViewModelConfigs(viewModelTypes: Type<ViewModel<ViewModelState>>[] | undefined): ViewModelConfig[] {
        if (!Array.isArray(viewModelTypes) || viewModelTypes.length === 0) {
            return [];
        }

        const viewModelConfigs: ViewModelConfig[] = [];
        const viewModelConfigBuilder = new ViewModelConfigBuilder();
        viewModelTypes.forEach((viewModelType) => {
            const viewModelConfig = viewModelConfigBuilder.build(viewModelType);
            viewModelConfigs.push(viewModelConfig);
        });

        return viewModelConfigs;
    }

    /**
     * 获取模块元数据
     */
    private getModuleMeta(moduleType: Type<Module>): ModuleMeta {
        if (!moduleType) {
            throw new Error('moduleType can not be empty');
        }

        const moduleMeta = TypeMetadataReader.getMataByName(moduleType, MODULE_META_NAME) as ModuleMeta;
        if (!moduleMeta) {
            throw new Error('moduleMeta can not be empty');
        }

        return moduleMeta;
    }

    /**
     * 追加Store对应的providers
     * @summary
     * 除了通过注解提供的providers，还需要追加Modlue、Repository、Store对应的Provider，以便静态模式下通过依赖注入创建实例
     */
    private appendModuleProviders(moduleConfig: ModuleConfig): void {
        const repositoryProviders = this.createRepositoryProviders(moduleConfig.repositories);
        const entityStoreProviders = this.createStoreProviders(moduleConfig.entityStores);
        const uiStoreProviders = this.createStoreProviders(moduleConfig.uiStores);

        const extraProvider = moduleConfig.providers || [];
        const providers: StaticProvider = [
            { provide: Module, useClass: moduleConfig.type, deps: [Injector] },
            ...repositoryProviders,
            ...entityStoreProviders,
            ...uiStoreProviders,
            ...extraProvider
        ];

        moduleConfig.providers = providers;
    }

    /**
     * 创建远程仓库的注入配置
     */
    private createRepositoryProviders(repositoryConfigs: RepositoryConfig[]) {
        if (!repositoryConfigs || !Array.isArray(repositoryConfigs)) {
            return [];
        }

        const providers: StaticProvider[] = [];
        repositoryConfigs.forEach((repositoryConfig) => {
            if (!repositoryConfig || !repositoryConfig.type) {
                throw new Error('StoreConfig for a static store must specify the type of store');
            }

            const deps = repositoryConfig.deps || [];
            const provider: StaticProvider = { provide: repositoryConfig.type, deps: [...deps] };
            providers.push(provider);
        });

        return providers;
    }

    /**
     * 创建实体仓库的注入配置
     */
    private createStoreProviders(storeConfigs: StoreConfig[]): StaticProvider[] {
        if (!storeConfigs || !Array.isArray(storeConfigs)) {
            return [];
        }

        const providers: StaticProvider[] = [];
        storeConfigs.forEach((storeConfig: StoreConfig) => {
            if (!storeConfig || !storeConfig.type) {
                throw new Error('StoreConfig for a static store must specify the type of store');
            }

            const deps = storeConfig.deps || [];
            const provider: StaticProvider = { provide: storeConfig.type, deps: [...deps] };
            providers.push(provider);
        });

        return providers;
    }
}

export { ModuleConfigBuilder };