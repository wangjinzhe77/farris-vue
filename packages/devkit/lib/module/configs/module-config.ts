/* eslint-disable @typescript-eslint/no-explicit-any */
import { Type, StaticProvider } from '../../common/index';
import { RepositoryConfig } from '../../repository';
import { EntityStoreConfig, StateMachineConfig, UIStoreConfig } from '../../store/index';
import { ViewModelConfig } from '../../viewmodel/index';
import { Module } from '../module';

/**
 * Module配置
 */
interface ModuleConfig {
    id: string;
    type?: Type<Module>;
    deps?: any[];
    providers?: StaticProvider[];

    repositories: RepositoryConfig[];
    entityStores: EntityStoreConfig[];
    uiStores: UIStoreConfig[];
    stateMachines: StateMachineConfig[];
    viewModels: ViewModelConfig[];
}

export { ModuleConfig };