import { StoreConfig, EntityStoreConfig, UIStoreConfig, StateMachineConfig } from '../../store/index';
import { RepositoryConfig } from '../../repository/index';
import { ViewModelConfig } from '../../viewmodel/index';
import { ModuleConfig } from './module-config';

/**
 * ModuleConfig管理器
 */
class ModuleConfigManager {

    /**
     * module配置
     */
    public config: ModuleConfig;

    /**
     * 构造函数
     */
    constructor(moduleConfig: ModuleConfig) {
        this.config = moduleConfig;
    }

    /**
     * 获取ViewModel配置
     */
    public getViewModelConfig(id: string): ViewModelConfig | undefined {
        const viewModelConfigs = this.config.viewModels;
        const targetViewModelConfig = viewModelConfigs.find((viewModelConfig) => {
            return viewModelConfig.id === id;
        });

        return targetViewModelConfig;
    }

    /**
     * 获取实体仓库配置
     */
    public getEntityStoreConfig(storeId: string): EntityStoreConfig | undefined {
        const storeConfigs = this.config.entityStores;
        const storeConfig = this.getStoreConfig(storeConfigs, storeId) as EntityStoreConfig;

        return storeConfig;
    }

    /**
     * 获取UI仓库配置
     */
    public getUIStoreConfig(storeId: string): UIStoreConfig | undefined {
        const storeConfigs = this.config.uiStores;
        const storeConfig = this.getStoreConfig(storeConfigs, storeId) as UIStoreConfig;

        return storeConfig;
    }

    /**
     * 获取状态机配置
     */
    public getStateMachineConfig(storeId: string): StateMachineConfig | undefined {
        const storeConfigs = this.config.stateMachines;
        const storeConfig = this.getStoreConfig(storeConfigs, storeId) as StateMachineConfig;

        return storeConfig;
    }

    /**
     * 获取远程实体仓库配置
     */
    public getRepositoryConfig(storeId: string): RepositoryConfig | undefined {
        const repositoryConfigs = this.config.repositories;
        const targetRepositoryConfig = repositoryConfigs.find((repositoryConfig) => {
            return repositoryConfig.id === storeId;
        });

        return targetRepositoryConfig;
    }

    /**
     * 获取StoreConfig
     */
    private getStoreConfig(storeConfigs: StoreConfig[], storeId: string): StoreConfig | undefined {
        const targetStoreConfig = storeConfigs.find((storeConfig) => {
            return storeConfig.id === storeId;
        });

        return targetStoreConfig;
    }
}

export { ModuleConfigManager };