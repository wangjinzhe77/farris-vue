import { StaticProvider, Type, TypeDecorator, makeTypeMetadataDecorator } from '../../common/index';

/**
 * 实体远程仓库元数据名称
 */
const REPOSITORY_META_NAME = 'REPOSITORY_META';

/**
 * 实体远程仓库元数据
 */
interface RepositoryMeta {
    id: string;
    deps?: any[];
    entityStore: string;
    [key: string]: any;
}

/**
 * 实体远程仓库元数据工厂
 */
const RepositoryMetaFactory = (meta: RepositoryMeta): RepositoryMeta => {
    return meta;
};

/**
 * 实体远程仓库装饰器工厂类型
 */
interface RepositoryDecoratorFactory {
    (meta: RepositoryMeta): TypeDecorator;
}

/**
 * 实体远程仓库装饰器工厂
 */
function FdRepository(meta: RepositoryMeta) {
    const decoratorFactory: RepositoryDecoratorFactory = makeTypeMetadataDecorator(REPOSITORY_META_NAME, RepositoryMetaFactory);
    return decoratorFactory(meta);
}

export { REPOSITORY_META_NAME , RepositoryMeta, RepositoryMetaFactory, RepositoryDecoratorFactory, FdRepository }
