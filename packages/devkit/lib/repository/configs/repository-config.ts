import { Type } from '../../common/index';
import { Entity } from '../../store/index';
import { Repository } from '../repository';

/**
 * Repository配置
 */
interface RepositoryConfig {
    id: string;
    type?: Type<Repository<Entity>>;
    deps?: any[];
    isDynamic: boolean;
    entityStore: string;

    [key: string]: any;
}

export { RepositoryConfig };
