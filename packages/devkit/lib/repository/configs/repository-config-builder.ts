
import { Type, TypeMetadataReader } from '../../common/index';
import { REPOSITORY_META_NAME, RepositoryMeta } from '../decorators/index';
import { Entity } from '../../store/index';
import { Repository } from '../repository';
import { RepositoryConfig } from './repository-config';

/**
 * 远程实体仓库配置构造器
 */
class RepositoryConfigBuilder {

    /**
     * 构造配置
     */
    public build(repositoryType: Type<Repository<Entity>>): RepositoryConfig {
        const repositoryMeta = this.getRepositoryMeta(repositoryType);
        const extendConfig = this.buildExtendConfig(repositoryMeta);

        const repositoryConfig: RepositoryConfig = {
            id:  repositoryMeta.id,
            type: repositoryType,
            deps: repositoryMeta.deps,
            isDynamic: false,
            entityStore: repositoryMeta.entityStore,
            ...extendConfig
        };

        return repositoryConfig;
    }

    /**
     * 追加扩展配置
     */
    private buildExtendConfig(repositoryMeta: RepositoryMeta): {[key: string]: any} {
        const buildInProps = ['id', 'type', 'deps', 'entityStore'];
        const extendConfig = {};
        Object.keys(repositoryMeta).forEach((propName: string) => {
            if (buildInProps.includes(propName)) {
                return;
            }
            extendConfig[propName] = repositoryMeta[propName]
        });

        return extendConfig;
    }

    /**
     * 获取元数据
     */
    private getRepositoryMeta(repositoryType: Type<Repository<Entity>>): RepositoryMeta {
        const repositoryMeta = TypeMetadataReader.getMataByName(repositoryType, REPOSITORY_META_NAME);
        return repositoryMeta;
    }
}

export { RepositoryConfigBuilder }