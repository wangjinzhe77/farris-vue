import { InjectionToken, createInjectionToken } from '../common/index';
import { Module } from '../module/index';
import { Entity, EntitySchema, createEntityBySchema, createEntitiesBySchema, EntityState, EntityStore } from '../store/index';
import { RepositoryConfig } from './configs/index';

const dynamicRepositoryTokens = new Map<string, InjectionToken<any>>();
function getDynamicRepositoryToken(id: string): InjectionToken<any> {
    let token = dynamicRepositoryTokens.get(id);
    if (token) {
        return token;
    }

    token = createInjectionToken(`DYNAMIC_REPOSITORIES_TOKEN_${id}`);
    dynamicRepositoryTokens.set(id, token);

    return token;
}

/**
 * 远程实体仓库
 */
abstract class Repository<T extends Entity> {

    /**
     * 所属模块
     */
    public module: Module;

    /**
     * 实体描述
     */
    public entitySchema: EntitySchema;

    /**
     * 实体仓库
     */
    public entityStore: EntityStore<EntityState<Entity>>;

    /**
     * ID
     */
    public id: string;

    /**
     * 配置
     */
    protected config: RepositoryConfig;

    /**
     * 构造函数
     */
    constructor(module: Module) {
        this.module = module;
    }

    /**
     * 初始化
     */
    public init(config: RepositoryConfig) {
        this.id = config.id;
        this.entityStore = this.module.createEntityStore(config.entityStore);
        this.entitySchema = this.entityStore.getEntitySchema();
    }

    /**
     * 批量创建实体
     */
    public buildEntites(entityDatas: any[]): T[] {
        const entities = createEntitiesBySchema<T>(this.entitySchema, entityDatas);
        return entities;
    }

    /**
     * 创建实体
     */
    public buildEntity(entityData: any): T {
        const entity = createEntityBySchema<T>(this.entitySchema, entityData);
        return entity;
    }

    /**
     * 获取实体仓库
     */
    public getEntityStore(): EntityStore<EntityState<Entity>> {
        return this.entityStore;
    }
}

export { getDynamicRepositoryToken, Repository };
