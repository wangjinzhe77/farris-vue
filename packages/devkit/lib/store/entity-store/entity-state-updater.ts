
import { EntityPath } from './entity-path/index';
import {
    LoadEntityChange, AppendEntityChange, EntityChangeType, RemoveEntityChange, UpdateEntityChange,
    ChangeValueChange, ChangeCurrentEntityChange
} from './entity-change/index';
import { Entity } from './entity';
import { EntityList } from './entity-list';
import { EntityStateQuery } from './entity-state-query';
import { EntityState, EntityStore } from './entity-store';

/**
 * 实体状态更新
 */
class EntityStateUpdater {

    /**
     * 实体类型
     */
    private entityStore: EntityStore<EntityState<Entity>>;

    /**
     * 实体集合
     */
    private entityList: EntityList<Entity>;

    /**
     * 实体查询器
     */
    private entityQuery: EntityStateQuery;

    /**
     * 构造函数
     */
    constructor(entityStore: EntityStore<EntityState<Entity>>) {
        this.entityStore = entityStore;
        this.entityList = entityStore.getEntityList();
        this.entityQuery = entityStore.entityQuery;
    }

    /**
     * 根据path加载实体集合
     */
    public loadEntitesByPath(path: EntityPath, entities: Entity[]) {
        const entityList = this.entityQuery.getEntityListByPath(path);
        entityList.loadEntities(entities);
        const pagination = this.entityStore.getPaginationByPath(path);
        const change: LoadEntityChange = {
            type: EntityChangeType.Load,
            path,
            entities,
            pagination
        };
        this.entityStore.triggerChange(change);
    }

    /**
     * 根据path追加实体集合
     */
    public appendEntitesByPath(path: EntityPath, entities: Entity[]) {
        const entityList = this.entityQuery.getEntityListByPath(path);
        entityList.appendEntities(entities);

        const change: AppendEntityChange = {
            type: EntityChangeType.Append,
            path,
            entities
        };

        this.entityStore.triggerChange(change);
    }

    /**
     * 根据Path批量删除实体
     */
    public removeEntitiesByPath(path: EntityPath, ids: string[]): void {
        const entityList = this.entityQuery.getEntityListByPath(path);
        const entities = entityList.getEntities();
        const entitiesToRemove = entities.filter((entity) => {
            return ids.includes(entity.idValue);
        });

        entityList.removeEntityByIds(ids);

        const change: RemoveEntityChange = {
            type: EntityChangeType.Remove,
            path,
            entities: entitiesToRemove
        };

        this.entityStore.triggerChange(change);
    }

    /**
     * 根据实体path，更新实体
     */
    public updateEntityByPath(path: EntityPath, newValues: any): void {
        const entity: Entity = this.entityQuery.getEntityByPath(path) as any;
        if (!entity) {
            return;
        }

        const changedOldValues: any = {};
        const changedNewValues: any = {};
        // Object.keys(newValues).forEach((propName: string) => {
        //     const oldValue = entity[propName];
        //     const newValue = newValues[propName];
        //     if (oldValue !== newValue) {
        //         changedOldValues[propName] = oldValue;
        //         changedNewValues[propName] = newValue;
        //         entity[propName] = newValue;
        //     }
        // });
        entity.loadData(newValues);
        const change: UpdateEntityChange = {
            type: EntityChangeType.Update,
            path,
            oldEntityData: changedOldValues,
            newEntityData: changedNewValues
        };

        this.entityStore.triggerChange(change);
    }

    /**
     * 根据属性Path，更新属性值
     */
    public setValueByPath(path: EntityPath, newValue: any): void {
        const parentPath = path.getParentEntityPath();
        const entity = this.entityQuery.getEntityByPath(parentPath) as any;

        const fieldPathNode = path.getLastNode();
        const fieldName = fieldPathNode.getNodeValue();

        const oldValue = entity[fieldName];
        if (oldValue === newValue) {
            return;
        }
        entity[fieldName] = newValue;

        const change: ChangeValueChange = {
            type: EntityChangeType.ValueChange,
            path,
            oldValue,
            newValue
        };
        this.entityStore.triggerChange(change);
    }

    /**
     * 根据Paht和ID改变当前实体
     */
    public changeCurrentEntityByPath(path: EntityPath, id: string): void {
        const entityList = this.entityQuery.getEntityListByPath(path);
        const newCurrentEntity = entityList.getEntityById(id);
        if (!newCurrentEntity) {
            return;
        }

        const oldCurrentEntity = this.entityQuery.getCurrentEntityByPath(path);
        if (oldCurrentEntity === newCurrentEntity) {
            return;
        }

        entityList.setCurrentId(id);

        const change: ChangeCurrentEntityChange = {
            type: EntityChangeType.CurrentChange,
            path,
            oldCurrentEntity,
            newCurrentEntity
        };
        this.entityStore.triggerChange(change);
    }
}

export { EntityStateUpdater };
