import { EntitySchema } from './entity-schema';
import { EntityNode } from './entity-node';
import { EntityDataLoader } from './entity-data-loader';
import { EntityDataPeeker } from './entity-data-peeker';

/**
 * 实体
 */
class Entity extends EntityNode {

    /**
     * 实体描述
     */
    private entitySchema: EntitySchema;

    /**
     * 数据加载器
     */
    private entityDataLoader: EntityDataLoader;

    /**
     * 数据提取器
     */
    private entityDataPeeker: EntityDataPeeker;

    /**
     * 主键
     */
    public get idKey(): string {
        return this.entitySchema.getIdKey();
    }

    /**
     * 主键值
     */
    public get idValue(): any {
        return (this as any)[this.idKey];
    }

    /**
     * 构造函数
     */
    constructor(schema: EntitySchema) {
        super();
        this.entitySchema = schema;
        this.entityDataLoader = new EntityDataLoader(this);
        this.entityDataPeeker = new EntityDataPeeker(this);
    }

    /**
     * 获取实体描述
     */
    public getSchema(): EntitySchema {
        return this.entitySchema;
    }

    /**
     * 加载数据
     */
    public loadData(data: any): void {
        this.entityDataLoader.load(data);
    }

    /**
     * 转换为JSON对象
     */
    public toJSON(): any {
        const data = this.entityDataPeeker.peek();
        return data;
    }
}

export { Entity };
