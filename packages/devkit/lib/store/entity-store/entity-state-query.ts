import { EntityNode } from './entity-node';
import { EntityPath, EntityPathNodeType } from './entity-path/index';
import { Entity } from './entity';
import { EntityList } from './entity-list';
import { EntityState, EntityStore } from './entity-store';


/**
 * 实体查询
 */
class EntityStateQuery {

    /**
     * 实体类型
     */
    private entityStore: EntityStore<EntityState<Entity>>;

    /**
     * 实体集合
     */
    private entityList: EntityList<Entity>

    /**
     * 构造函数
     */
    constructor(entityStore: EntityStore<EntityState<Entity>>) {
        this.entityStore = entityStore;
        this.entityList = entityStore.getEntityList();
    }

    /**
     * 获取实体数组
     */
    public getEntities(): Entity[] {
        return this.entityList.getEntities();
    }

    /**
     * 根据Path获取实体数组
     */
    public getEntitiesByPath(path: EntityPath): Entity[] {
        const entityList = this.getEntityNode(path) as EntityList<Entity>;
        const entities = entityList.getEntities();
        return entities;
    }

    /**
     * 获取实体列表
     */
    public getEntityList(): EntityList<Entity> {
        return this.entityList;
    }

    /**
     * 根据Path获取实体列表
     */
    public getEntityListByPath(path: EntityPath): EntityList<Entity> {
        const entityList = this.getEntityNode(path) as EntityList<Entity>;
        return entityList;
    }

    /**
     * 根据ID获取根实体
     */
    public getEntityById(id: string): Entity | undefined {
        const entity = this.entityList.getEntityById(id);
        return entity;
    }

    /**
     * 根据Path获取实体
     */
    public getEntityByPath(path: EntityPath): Entity | undefined {
        const entity = this.getEntityNode(path) as Entity;
        if (!entity) {
            return;
        }

        return entity;
    }

    /**
     * 获取根当前实体
     */
    public getCurrentEntity(): Entity {
        const currentEntity = this.entityList.getCurrentEntity();
        return currentEntity;
    }

    /**
     * 根据Path获取当前实体
     */
    public getCurrentEntityByPath(path: EntityPath): Entity {
        const entityList = this.getEntityListByPath(path);
        const currentEntity = entityList.getCurrentEntity();

        return currentEntity;
    }

    /**
     * 根据字段名获取根实体字段值
     */
    public getValue(name: string): any {
        const currentEntity = this.getCurrentEntity() as any;
        return currentEntity[name];
    }

    /**
     * 根据路径获取字段值
     */
    public getValueByPath(path: EntityPath) {
        const value = this.getEntityNode(path) as any;
        return value;
    }

    /**
     * 根据路径获取实体、实体集合、属性值
     */
    public getEntityNode(path: EntityPath): EntityNode | any {
        const entityPathNodes = path.getNodes();
        let currentEntityNode: any = this.entityList;
        entityPathNodes.forEach((entityPathNode) => {
            const pathNodeType = entityPathNode.getNodeType();
            const pathNodeValue = entityPathNode.getNodeValue();
            if (pathNodeType === EntityPathNodeType.IdValue) {
                currentEntityNode = currentEntityNode.getEntityById(pathNodeValue) || currentEntityNode.getCurrentEntity();
            } else {
                currentEntityNode = currentEntityNode[pathNodeValue];
            }
        });

        return currentEntityNode;
    }
}

export { EntityStateQuery };