 
import { Type, TypeMetadataReader, PropMetadataReader, SameNamePropsMetadatas } from '../../../common/index';
import { ENTITY_STORE_META_NAME, EntityStoreMeta, PRIMITIVE_FIELD_META_NAME, PrimitiveFieldMeta } from '../decorators/index';
import { PrimitiveFieldConfig, EntityConfig, EntityStoreConfig } from './entity-store-config';
import { Entity } from '../entity';
import { EntityState, EntityStore } from '../entity-store';

/**
 * EntityStore配置构造器
 */
class EntityStoreConfigBuilder {

    /**
     * 构造函数
     */
    constructor() {
    }

    /**
     * 构造EntityStore配置
     */
    public build(entityStoreType: Type<EntityStore<EntityState<Entity>>>): EntityStoreConfig {
        const entityStoreMeta = this.getEntityStoreMeta(entityStoreType);
        const id = entityStoreMeta.id;
        const entityConfig = this.buildEntityConfig(entityStoreMeta.entityType);
        const entityStoreConfig: EntityStoreConfig = {
            id,
            type: entityStoreType,
            state: {
                entity: entityConfig
            }
        };

        return entityStoreConfig;
    }

    /**
     * 构造Entity配置
     */
    private buildEntityConfig(entityType: Type<Entity>): EntityConfig {
        const entityConfig: EntityConfig = {
            idKey: '',
            fields: []
        };
        this.appendPrimitiveFieldConfigs(entityConfig, entityType);

        return entityConfig;
    }

    /**
     * 构造Primitive字段配置
     */
    private appendPrimitiveFieldConfigs(entityConfig: EntityConfig, entityType: Type<Entity>): PrimitiveFieldConfig[] {
        const fieldMetas = this.getPrimitiveFieldMetas(entityType);
        const fieldConfigs: PrimitiveFieldConfig[] = [];
        Object.keys(fieldMetas).forEach((propName: string) => {
            const fieldMeta = fieldMetas[propName];
            const fieldConfig: PrimitiveFieldConfig = {
                type: 'Primitive',
                name: propName
            };
            entityConfig.fields.push(fieldConfig);

            if (fieldMeta.primary) {
                entityConfig.idKey = propName;
            }
        });

        return fieldConfigs;
    }

     /**
     * 获取EntityStore元数据
     */
    private getEntityStoreMeta(entityStoreType: Type<EntityStore<EntityState<Entity>>>): EntityStoreMeta {
        const entityStoreMeta = TypeMetadataReader.getMataByName(entityStoreType, ENTITY_STORE_META_NAME);
        return entityStoreMeta;
    }

    /**
     * 获取PrimitiveField元数据
     */
    private getPrimitiveFieldMetas(entityType: Type<Entity>): SameNamePropsMetadatas<PrimitiveFieldMeta> {
        const fieldMetas = PropMetadataReader.getPropsMetasByName<PrimitiveFieldMeta>(entityType, PRIMITIVE_FIELD_META_NAME);
        return fieldMetas;
    }
}

export { EntityStoreConfigBuilder }