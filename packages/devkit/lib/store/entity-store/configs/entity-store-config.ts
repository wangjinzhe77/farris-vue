
import { StaticProvider, Type } from '../../../common/index';
import { StoreConfig } from '../../store/index';
import { Entity } from '../entity';
import { EntityState, EntityStore } from '../entity-store';

/**
 * 字段配置
 */
interface FieldConfig {
    type: string;
    name: string;
}

/**
 * 简单字段配置
 */
interface PrimitiveFieldConfig extends FieldConfig {
    type: 'Primitive';
}

/**
 * 实体字段配置
 */
interface EntityFieldConfig extends FieldConfig {
    type: 'Entity';
    entityConfig: EntityConfig;
}

/**
 * 实体数组字段配置
 */
interface EntityListFieldConfig extends FieldConfig {
    type: 'EntityList';
    childEntityConfig: EntityConfig;
}

/**
 * 实体配置
 */
interface EntityConfig {
    idKey: string;
    fields: FieldConfig[];
}

/**
 * 实体状态配置
 */
interface EntityStateConfig {
    entity: EntityConfig;
}

/**
 * EntityStore配置
 */
interface EntityStoreConfig extends StoreConfig {
    id: string;
    type?: Type<EntityStore<EntityState<Entity>>>;
    deps?: StaticProvider[];
    state: EntityStateConfig;
}

export { FieldConfig, PrimitiveFieldConfig, EntityFieldConfig, EntityListFieldConfig, EntityConfig, EntityStateConfig, EntityStoreConfig };

