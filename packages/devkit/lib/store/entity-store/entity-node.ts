/* eslint-disable no-use-before-define */

/**
 * 实体节点
 */
class EntityNode {

    /**
     * 父节点
     */
    protected parentEntityNode: EntityNode;

    /**
     * 设置父实体节点
     */
    public setParentEntityNode(parentEntityNode: EntityNode) {
        this.parentEntityNode = parentEntityNode;
    }

    /**
     * 获取父实体节点
     */
    public getParentEntityNode() {
        return this.parentEntityNode;
    }

}

export { EntityNode };