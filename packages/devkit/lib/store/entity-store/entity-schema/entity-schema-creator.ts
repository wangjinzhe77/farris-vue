/* eslint-disable no-use-before-define */
import { EntityConfig, PrimitiveFieldConfig, EntityFieldConfig, EntityListFieldConfig } from '../configs/index';
import { FieldType, PrimitiveFieldSchema, EntityFieldSchema, EntityListFieldSchema } from './entity-field-schema';
import { EntitySchema } from './entity-schema';

/**
 * 实体描述创建器
 */
class EntitySchemaCreator {

    /**
     * 实体配置
     */
    private entityConfig: EntityConfig;

    /**
     * 实体描述
     */
    private entitySchema: EntitySchema;

    /**
     * 构造函数
     */
    constructor(entityConfig: EntityConfig) {
        this.entityConfig = entityConfig;
        this.entitySchema = new EntitySchema();
    }

    /**
     * 创建实体描述
     */
    public create(): EntitySchema {
        this.addFieldSchemas();
        this.setIdKey();
        return this.entitySchema;
    }

    /**
     * 设置主键
     */
    private setIdKey() {
        const idKey = this.entityConfig.idKey;
        this.entitySchema.setIdKey(idKey);
    }

    /**
     * 添加实体
     */
    private addFieldSchemas() {
        const fieldConfigs = this.entityConfig.fields;
        fieldConfigs.forEach((fieldConfig) => {
            if (fieldConfig.type === 'Primitive') {
                this.addPrimitiveFieldSchema(fieldConfig as PrimitiveFieldConfig);
            } else if (fieldConfig.type === 'Entity') {
                this.addEntityFieldSchema(fieldConfig as EntityFieldConfig);
            } else if (fieldConfig.type === 'EntityList') {
                this.addEntityListFieldSchema(fieldConfig as EntityListFieldConfig);
            } else {
                throw new Error(`Unknow field type(${fieldConfig.type})`);
            }
        });
    }

    /**
     * 添加简单字段描述
     */
    private addPrimitiveFieldSchema(fieldConfig: PrimitiveFieldConfig): void {
        const fieldSchema: PrimitiveFieldSchema = {
            type: FieldType.Primitive,
            name: fieldConfig.name
        };
        this.entitySchema.addFieldSchema(fieldSchema);
    }

    /**
     * 添加实体字段描述
     */
    private addEntityFieldSchema(fieldConfig: EntityFieldConfig): void {
        const entitySchema = createEntitySchema(fieldConfig.entityConfig);
        const fieldSchema: EntityFieldSchema = {
            type: FieldType.Entity,
            name: fieldConfig.name,
            entitySchema: entitySchema
        };
        this.entitySchema.addFieldSchema(fieldSchema);
    }

    /**
     * 添加实体列表字段描述
     */
    private addEntityListFieldSchema(fieldConfig: EntityListFieldConfig) {
        const childEntitySchema = createEntitySchema(fieldConfig.childEntityConfig);
        const fieldSchema: EntityListFieldSchema = {
            type: FieldType.EntityList,
            name: fieldConfig.name,
            entitySchema: childEntitySchema
        };
        this.entitySchema.addFieldSchema(fieldSchema);
    }
}

/**
 * 创建实体描述
 */
function createEntitySchema(entityConfig: EntityConfig): EntitySchema {
    const schemaCreator = new EntitySchemaCreator(entityConfig);
    const schema = schemaCreator.create();

    return schema;
}

export { createEntitySchema };
