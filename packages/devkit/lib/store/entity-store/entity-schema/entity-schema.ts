import { EntityPath, EntityPathNode, EntityPathNodeType } from '../entity-path/index';
import { FieldType, FieldSchema, EntityFieldSchema, EntityListFieldSchema} from './entity-field-schema';

/**
 * 实体字段信息
 */
class EntitySchema {

    /**
     * 字段描述集合
     */
    private fieldSchemaMap: Map<string, FieldSchema>;

    /**
     * 主键值
     */
    private idKey: string;

    /**
     * 构造函数
     */
    constructor() {
        this.fieldSchemaMap = new Map<string, FieldSchema>();
    }

    /**
     * 获取全部字段描述
     */
    public getFieldSchemas(): FieldSchema[] {
        const fieldSchemas = Array.from(this.fieldSchemaMap.values());
        return fieldSchemas;
    }

    /**
     * 根据字段Type获取字段描述
     */
    public getFieldSchemasByType<T extends FieldSchema>(type: FieldType): T[] {
        const fieldSchemas = this.getFieldSchemas();
        const targetFieldSchemas = fieldSchemas.filter((fieldSchema: FieldSchema) => {
            return fieldSchema.type === type;
        });

        return targetFieldSchemas as T[];
    }

    /**
     * 根据字段Name获取字段描述
     */
    public getFieldSchemaByName(name: string): FieldSchema | undefined {
        if (!this.fieldSchemaMap.has(name)) {
            return;
        }

        const targetFieldSchema = this.fieldSchemaMap.get(name);
        return targetFieldSchema;
    }

    /**
     * 根据字段Path获取字段描述
     */
    public getFieldSchemaByPath(entityPath: EntityPath): FieldSchema | undefined {
        const entityPathNodes = entityPath.getNodes();
        let currentEntitySchema: EntitySchema = this;
        let currentFieldSchema: FieldSchema | undefined;
        entityPathNodes.forEach((entityPathNode: EntityPathNode) => {
            const pathNodeType = entityPathNode.getNodeType();
            const pathNodeValue = entityPathNode.getNodeValue();
            if (pathNodeType === EntityPathNodeType.IdValue) {
                return;
            }
            currentFieldSchema = currentEntitySchema.getFieldSchemaByName(pathNodeValue);
            if (!currentFieldSchema) {
                throw new Error(`FieldSchem(name=${pathNodeValue})`);
            }
            if (currentFieldSchema.type === FieldType.Entity) {
                currentEntitySchema = (currentFieldSchema as EntityFieldSchema).entitySchema;
            } else if (currentFieldSchema.type === FieldType.EntityList) {
                currentEntitySchema = (currentFieldSchema as EntityListFieldSchema).entitySchema;
            }
        });

        return currentFieldSchema;
    }

    /**
     * 添加字段信息
     */
    public addFieldSchema(fieldSchema: FieldSchema): void {
        this.fieldSchemaMap.set(fieldSchema.name, fieldSchema);
    }

    /**
     * 设置主键字段
     */
    public setIdKey(primaryKey: string): void {
        this.idKey = primaryKey;
    }

    /**
     * 获取主键字段
     */
    public getIdKey(): string {
        return this.idKey;
    }
}

export { EntitySchema };
