import { EntitySchema } from './entity-schema';

/**
 * 字段类型
 */
enum FieldType {
    /**
     * 简单字段
     */
    Primitive = 'Primitive',

    /**
     * 实体字段
     */
    Entity = 'Entity',

    /**
     * 实体数组字段
     */
    EntityList = 'EntityList'
}

/**
 * 字段信息
 */
interface FieldSchema {

    /**
     * 字段名称
     */
    name: string;

    /**
     * 字段类型
     */
    type: FieldType;
}

/**
 * 简单字段
 */
interface PrimitiveFieldSchema extends FieldSchema {

    /**
     * 字段类型
     */
    type: FieldType.Primitive;
}

/**
 * 实体字段
 */
interface EntityFieldSchema extends FieldSchema {

    /**
     * 字段类型
     */
    type: FieldType.Entity;

    /**
     * 类型信息
     */
    entitySchema: EntitySchema;
}


/**
 * 实体集合字段
 */

interface EntityListFieldSchema extends FieldSchema {

    /**
     * 字段类型
     */
    type: FieldType.EntityList;

    /**
     * 集合内元素类型信息
     */
    entitySchema: EntitySchema
}

export { FieldType, FieldSchema, PrimitiveFieldSchema, EntityFieldSchema, EntityListFieldSchema };
