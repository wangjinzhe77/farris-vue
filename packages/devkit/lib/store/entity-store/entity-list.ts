import { EntitySchema } from './entity-schema/index';
import { EntityNode } from './entity-node';
import { Entity } from './entity';
import { createEntityBySchema, createEntitiesBySchema } from './entity-creator';

/**
 * 实体列表
 */
class EntityList<T extends Entity> extends EntityNode {

    /**
     * 实体描述
     */
    private entitySchema: EntitySchema;

    /**
     * 实体集合
     */
    private entities: T[];

    /**
     * 当前实体ID
     */
    private currentId: string | null;

    /**
     * 构造函数
     */
    constructor(entitySchema: EntitySchema) {
        super();
        this.entitySchema = entitySchema;
        this.entities = [];
    }

    /**
     * 加载数据
     */
    public loadData(entityDatas: any[]): void {
        if (!Array.isArray(entityDatas)) {
            throw new Error('The entityDatas parameter should be an array');
        }
        const entities = createEntitiesBySchema<T>(this.entitySchema, entityDatas);
        this.loadEntities(entities);
    }

    /**
     * 加载实体
     */
    public loadEntities(entities: T[]): void {
        this.entities = [];
        entities.forEach((entity: T) => {
            this.appendEntity(entity, false);
        });

        if (entities.length === 0) {
            this.currentId = null;
        } else {
            let newCurrentId = null;
            const currentEntity = this.getCurrentEntity();
            if (currentEntity.idValue) {
                newCurrentId = currentEntity.idValue;
            } else {
                const firstEntity = this.entities[0];
                newCurrentId = firstEntity.idValue;
            }
            this.setCurrentId(newCurrentId);
        }
    }

    /**
     * 批量追加实体
     */
    public appendEntities(entities: T[]) {
        if (Array.isArray(entities) !== true) {
            return;
        }

        entities.forEach((entity: T) => {
            entity.setParentEntityNode(entity);
            this.entities.push(entity);
        });

        if (this.entities.length > 0) {
            const lastEntity = this.entities[this.entities.length - 1];
            this.setCurrentId(lastEntity.idValue);
        }
    }

    /**
     * 追加实体
     */
    public appendEntity(entity: T, changeCurrentId: boolean = true): void {
        entity.setParentEntityNode(entity);
        this.entities.push(entity);

        if (this.entities.length > 0 && changeCurrentId) {
            const lastEntity = this.entities[this.entities.length - 1];
            this.setCurrentId(lastEntity.idValue);
        }
    }

    /**
     * 获取全部实体
     */
    public getEntities() {
        return this.entities;
    }

    /**
     * 获取当前实体
     */
    public getCurrentEntity(): T {
        if (!this.currentId) {
            return this.createEmpltyEntity();
        }

        const currentEntity = this.getEntityById(this.currentId);
        if (!currentEntity) {
            return this.createEmpltyEntity();
        }

        return currentEntity;
    }

    /**
     * 获取实体
     */
    public setCurrentId(id: string | null): void {
        const currentEntity = this.entities.find((entity: T) => {
            return entity.idValue === id;
        });

        if (!currentEntity) {
            this.currentId = null;
            return;
        }

        this.currentId = currentEntity.idValue;
    }

    /**
     * 获取实体
     */
    public getEntityById(id: string): T | undefined {
        const targetEntity = this.entities.find((entity: T) => {
            return entity.idValue === id;
        });

        return targetEntity;
    }

    /**
     * 删除实体
     */
    public removeEntityById(id: string): void {
        const targetIndex = this.entities.findIndex((entity: T) => {
            return entity.idValue === id;
        });

        if (targetIndex === -1) {
            return;
        }

        // 切换当前行
        const currentEntity = this.getCurrentEntity();
        if (currentEntity.idValue === id) {
            let newCurrentIndex = null;
            if (targetIndex === this.entities.length - 1) {
                newCurrentIndex = targetIndex - 1;
            } else {
                newCurrentIndex = targetIndex + 1;
            }

            const newCurrentEntity = this.entities[newCurrentIndex];
            if (newCurrentEntity) {
                this.setCurrentId(newCurrentEntity.idValue);
            } else {
                this.setCurrentId(null);
            }
        }

        this.entities.splice(targetIndex, 1);

    }

    /**
     * 批量删除实体
     */
    public removeEntityByIds(ids: string[]) {
        if (!Array.isArray(ids)) {
            return;
        }

        ids.forEach((id: string) => {
            this.removeEntityById(id);
        });
    }

    /**
     * 转换为JSON对象
     */
    public toJSON() {
        const entityListData = this.entities.map((entity: T) => {
            return entity.toJSON();
        });
        return entityListData;
    }

    /**
     * 创建空实体
     */
    private createEmpltyEntity() {
        const emptyEntity = createEntityBySchema<T>(this.entitySchema, {});
        return emptyEntity;
    }
}

export { EntityList };