/* eslint-disable @typescript-eslint/ban-types */
import { Module } from '../../module/index';
import { Store } from '../store/index';

import { EntityStoreConfig } from './configs/index';
import { Entity } from './entity';
import { EntityList } from './entity-list';

import { EntitySchema, createEntitySchema } from './entity-schema/index';
import { EntityPath, EntityPathBuilder } from './entity-path/index';
import { EntityChangeHistory } from './entity-change/index';

import { createEntityListBySchema } from './entity-list-creator';
import { EntityStateQuery } from './entity-state-query';
import { EntityStateUpdater } from './entity-state-updater';
import { EntityPagination, Pagination } from './types';

/**
 * EntityState定义
 */
class EntityState<E extends Entity> {
    entities!: E[];
    currentEntity!: E | null;
}

/**
 * 实体状态
 */
class EntityStore<S extends EntityState<Entity>> extends Store<S> {

    /**
     * 分页信息
     */
    private entityPagination: EntityPagination;

    /**
     * 实体描述
     */
    protected entitySchema: EntitySchema;

    /**
     * 根实体列表
     */
    protected entityList: EntityList<Entity>;

    /**
   * 实体状态查询器
   */
    public entityQuery: EntityStateQuery;

    /**
     * 实体更新器
     */
    public entityUpdater: EntityStateUpdater;

    /**
     * 变更历史记录
     */
    public entityChangeHistory: EntityChangeHistory;

    /**
     * 构造函数
     */
    constructor(module: Module) {
        super(module);
    }

    /**
     * 初始化
     */
    public init(config: EntityStoreConfig): void {
        super.init(config);

        const entityConfig = config.state.entity;
        this.entitySchema = createEntitySchema(entityConfig);
        this.entityList = createEntityListBySchema<Entity>(this.entitySchema, []);

        this.entityQuery = new EntityStateQuery(this);
        this.entityUpdater = new EntityStateUpdater(this);
        this.entityChangeHistory = new EntityChangeHistory();

        this.refreshState();
    }

    /**
     * 获取实体列表
     */
    public getEntityList(): EntityList<Entity> {
        return this.entityList;
    }

    /**
     * 根据path获取实体列表
     */
    public getEntityListByPath(path: string | EntityPath): EntityList<Entity> {
        const entityPath = this.createPath(path, true);
        return this.entityQuery.getEntityListByPath(entityPath);
    }

    /**
     * 获取根实体集合
     */
    public getEntities(): Entity[] {
        return this.entityList.getEntities() as Entity[];
    }

    /**
     * 根据Path获取根实体集合
     */
    public getEntitiesByPath(path: string | EntityPath): Entity[] {
        const entityPath = this.createPath(path, true);
        return this.entityQuery.getEntitiesByPath(entityPath) as Entity[];
    }

    /**
     * 根据ID获取根实体
     */
    public getEntityById(id: string): Entity | undefined {
        return this.entityQuery.getEntityById(id) as Entity;
    }

    /**
     * 根据Path获取实体
     */
    public getEntityByPath(path: string | EntityPath): Entity | undefined {
        const entityPath = this.createPath(path);
        return this.entityQuery.getEntityByPath(entityPath) as Entity;
    }

    /**
     * 根据Path获取字段值
     */
    public getValueByPath(path: string | EntityPath) {
        const entityPath = this.createPath(path);
        return this.entityQuery.getValueByPath(entityPath);
    }

    /**
     * 根据ID获取根实体
     */
    public getCurrentEntity(): Entity {
        return this.entityQuery.getCurrentEntity() as Entity;
    }

    /**
     * 根据Path获取实体
     */
    public getCurrentEntityByPath(path: string | EntityPath): Entity {
        const entityPath = this.createPath(path, true);
        return this.entityQuery.getCurrentEntityByPath(entityPath);
    }

    /**
     * 加载根实体集合
     */
    public loadEntities(entities: Entity[]): void {
        const entityPath = this.createPath('/', true);
        this.entityUpdater.loadEntitesByPath(entityPath, entities);
    }

    /**
     * 根据Path加载实体集合
     */
    public loadEntitesByPath(path: string | EntityPath, entities: Entity[]): void {
        const entityPath = this.createPath(path, true);
        this.entityUpdater.loadEntitesByPath(entityPath, entities);
    }

    /**
     * 加载根实体集合
     */
    public appendEntities(entities: Entity[]): void {
        const entityPath = this.createPath('/', true);
        this.entityUpdater.appendEntitesByPath(entityPath, entities);
    }

    /**
     * 根据Path加载实体集合
     */
    public appendEntitesByPath(path: string | EntityPath, entities: Entity[]): void {
        const entityPath = this.createPath(path, true);
        this.entityUpdater.appendEntitesByPath(entityPath, entities);
    }

    /**
     * 根据ID删除根实体
     */
    public removeEntityById(id: string): void {
        const entityPath = this.createPath('/', true);
        this.entityUpdater.removeEntitiesByPath(entityPath, [id]);
    }

    /**
     * 根据父Paht和ID删除实体
     */
    public removeEntityByPath(path: string | EntityPath, id: string): void {
        const entityPath = this.createPath(path, true);
        this.entityUpdater.removeEntitiesByPath(entityPath, [id]);
    }

    /**
     * 根据ID批量删除根实体
     */
    public removeEntitiesByIds(ids: string[]): void {
        const entityPath = this.createPath('/', true);
        this.entityUpdater.removeEntitiesByPath(entityPath, ids);
    }

    /**
     * 根据path和ID批量删除实体
     */
    public removeEntitiesByPath(path: string | EntityPath, ids: string[]): void {
        const entityPath = this.createPath(path, true);
        this.entityUpdater.removeEntitiesByPath(entityPath, ids);
    }

    /**
     * 根据ID更新实体
     */
    public updateEntityById(id: string, values: any): void {
        const entityPath = this.createPath(`/[${id}]`);
        this.entityUpdater.updateEntityByPath(entityPath, values);
    }

    /**
     * 根据Path更新实体
     */
    public updateEntityByPath(path: string, values: any): void {
        const entityPath = this.createPath(path);
        this.entityUpdater.updateEntityByPath(entityPath, values);
    }

    /**
     * 根据path更新字段值
     */
    public setValueByPath(path: string | EntityPath, value: any): void {
        const entityPath = this.createPath(path);
        this.entityUpdater.setValueByPath(entityPath, value);
    }

    /**
     * 设置当前根实体
     */
    public changeCurrentEntityById(id: string): void {
        const entityListPath = this.createPath('/', true);
        this.entityUpdater.changeCurrentEntityByPath(entityListPath, id);
    }

    /**
     * 根据Path设置当前实体
     */
    public changeCurrentEntityByPath(path: string | EntityPath, id: string): void {
        const enityListPath = this.createPath(path, true);
        this.entityUpdater.changeCurrentEntityByPath(enityListPath, id);
    }

    /**
     * 构造实体路径
     */
    public createPath(path: string | string[] | EntityPath, isEntityListPath: boolean = false): EntityPath {
        const entityPathCreator = new EntityPathBuilder(this);
        const entityPath = entityPathCreator.build(path, isEntityListPath);

        return entityPath;
    }

    /**
     * 获取实体结构描述
     */
    public getEntitySchema() {
        return this.entitySchema;
    }

    /**
     * 更新实体分页配置
     */
    public setEntityPagination(entityPagination: EntityPagination) {
        this.entityPagination = entityPagination;
    }

    /**
     * 获取实体分页配置
     */
    public getEntityPagination() {
        return this.entityPagination;
    }

    /**
     * 更新指定实体分页信息
     */
    public setPaginationByPath(path: string | string[] | EntityPath, pagination: Partial<Pagination>) {
        if (Array.isArray(path)) {
            path = `/${path.join('/')}`;
        } else if (path instanceof EntityPath) {
            path = path.toShortPath();
        }
        const previousPagination = this.getPaginationByPath(path);
        this.entityPagination[path] = Object.assign({}, previousPagination, pagination);
    }

    /**
     * 获取分页信息
     */
    public getPaginationByPath(path: string | string[] | EntityPath): Pagination {
        if (Array.isArray(path)) {
            path = `/${path.join('/')}`;
        } else if (path instanceof EntityPath) {
            path = path.toShortPath();
        }
        return this.entityPagination && this.entityPagination[path];
    }

    /**
     * 刷新State
     */
    public refreshState() {
        const latestState = {
            entities: this.entityList.getEntities(),
            currentEntity: this.entityList.getCurrentEntity()
        } as S;
        this.state = latestState;
    }

    /**
     * 触发变更
     */
    public triggerChange(change: any) {
        this.refreshState();
        this.entityChangeHistory.addChange(change);
        super.triggerChange(change as any);
    }

    /**
     * 转换为JSON
     */
    public toJSON() {
        const entityStateJson = {
            entityList: this.entityList.toJSON(),
            currentEntity: this.entityList.getCurrentEntity().toJSON()
        };

        return entityStateJson;
    }
}

export { EntityState, EntityStore };
