 
import { Type, TypeDecorator, makeTypeMetadataDecorator } from '../../../common/index';
import { StoreMeta } from '../../store';
import { Entity } from '../entity';
import { EntityState } from '../entity-store';

/**
 * EntityStore元数据名称
 */
const ENTITY_STORE_META_NAME = 'ENITY_STORE_META';

/**
 * EntityStore元数据
 */
interface EntityStoreMeta extends StoreMeta  {
    stateType: Type<EntityState<Entity>>;
    entityType: Type<Entity>;
}

/**
 * EntityStore元数据工厂
 */
const entityStoreMetaFactory = (meta: EntityStoreMeta): EntityStoreMeta => {
    return meta;
};

/**
 * EntityStore装饰器工厂类型
 */
interface EntityStoreDecoratorFactory {
    (meta: EntityStoreMeta): TypeDecorator;
}

/**
 * EntityStore装饰器工厂
 */
function FdEntityStore(meta: EntityStoreMeta) {
    const decoratorFactory: EntityStoreDecoratorFactory = makeTypeMetadataDecorator(ENTITY_STORE_META_NAME, entityStoreMetaFactory);
    return decoratorFactory(meta);
}

export { ENTITY_STORE_META_NAME, EntityStoreMeta, entityStoreMetaFactory, EntityStoreDecoratorFactory, FdEntityStore }
