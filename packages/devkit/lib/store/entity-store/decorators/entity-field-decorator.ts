/* eslint-disable @typescript-eslint/prefer-function-type */
import { Type, PropDecorator, makePropMetadataDecorator } from '../../../common/index';
import { Entity } from '../entity';

/**
 * 实体字段元数据
 */
interface EntityFieldMeta {

    /**
     * 实体类型
     */
    entityType: Type<Entity>;
}

/**
 * 实体字段元数据名称
 */
const ENTITY_FIELD_META_NAME = 'ENTITY_FIELD';

/**
 * 实体字段元数据工厂方法
 */
const entityFieldMetaFactory = (meta: EntityFieldMeta) => {
    return meta;
}

/**
 * 实体字段装饰器类型
 */
interface EntityFieldDecoratorFactory {
    (meta?: EntityFieldMeta): PropDecorator
}

/**
 * 实体字段装饰器工厂
 */
const FdEntityField: EntityFieldDecoratorFactory = makePropMetadataDecorator(ENTITY_FIELD_META_NAME, entityFieldMetaFactory);

export { EntityFieldMeta, ENTITY_FIELD_META_NAME, entityFieldMetaFactory, EntityFieldDecoratorFactory, FdEntityField };
