
import { PropDecorator, makePropMetadataDecorator } from '../../../common/index';

/**
 * 简单字段元数据
 */
interface PrimitiveFieldMeta {

    /**
     * 是否为主键
     */
    primary?: boolean;
}

/**
 * 简单字段元数据名称
 */
const PRIMITIVE_FIELD_META_NAME = 'PRIMITIVE_FIELD';

/**
 * 简单字段元数据工厂方法
 */
const primitiveFieldMetaFactory = (meta: PrimitiveFieldMeta) => {
    return meta;
};

/**
 * 简单字段装饰器类型
 */
interface PrimitiveFieldDecoratorFactory {
    (meta?: PrimitiveFieldMeta): PropDecorator;
}

/**
 * 简单字段装饰器工厂
 */
const FdField: PrimitiveFieldDecoratorFactory = makePropMetadataDecorator(PRIMITIVE_FIELD_META_NAME, primitiveFieldMetaFactory);

export { PrimitiveFieldMeta, PRIMITIVE_FIELD_META_NAME, primitiveFieldMetaFactory, PrimitiveFieldDecoratorFactory, FdField };
