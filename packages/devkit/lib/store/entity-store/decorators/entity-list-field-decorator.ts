/* eslint-disable @typescript-eslint/prefer-function-type */
import { Type, PropDecorator, makePropMetadataDecorator } from '../../../common/index';
import { Entity } from '../entity';

/**
 * 实体列表字段元数据
 */
interface EntityListFieldMeta {

    /**
     * 列表内实体的类型
     */
    entityType: Type<Entity>;
}

/**
 * 实体列表字段元数据名称
 */
const ENTITY_LIST_FIELD_META_NAME = 'ENTITY_LIST_FIELD';

/**
 * 实体列表字段元数据工厂方法
 */
const entitylistFieldMetaFactory = (meta: EntityListFieldMeta) => {
    return meta;
};

/**
 * 实体列表字段装饰器类型
 */
interface EntityListFieldDecoratorFactory {
    (meta?: EntityListFieldMeta): PropDecorator;
}

/**
 * 实体列表字段装饰器工厂
 */
const FdEntityListField: EntityListFieldDecoratorFactory = makePropMetadataDecorator(ENTITY_LIST_FIELD_META_NAME, entitylistFieldMetaFactory);

export { EntityListFieldMeta, ENTITY_LIST_FIELD_META_NAME, entitylistFieldMetaFactory, EntityListFieldDecoratorFactory, FdEntityListField };
