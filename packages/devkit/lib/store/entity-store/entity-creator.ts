/* eslint-disable no-use-before-define */
import { EntitySchema } from './entity-schema/index';
import { Entity } from './entity';

/**
 * 创建实体
 */
function createEntityBySchema<T extends Entity>(entitySchema: EntitySchema, entityData: any): T {
    if (!entitySchema) {
        throw new Error(`The entitySchema parameter can't be empty`);
    }
    if (!entityData) {
        throw new Error(`The entityData parameter should be an array`);
    }

    const entity = new Entity(entitySchema) as T;
    entity.loadData(entityData);

    return entity;
}

/**
 * 批量创建实体
 */
function createEntitiesBySchema<T extends Entity>(entitySchema: EntitySchema, entityDatas: any[]): T[] {
    if (!entitySchema) {
        throw new Error(`The entitySchema parameter can't be empty`);
    }
    if (!Array.isArray(entityDatas)) {
        throw new Error(`The entityDatas parameter should be an array`);
    }

    const entities = entityDatas.map((entityData: any) => {
        const entity = createEntityBySchema<T>(entitySchema, entityData);
        return entity;
    });

    return entities;
}


export { createEntityBySchema, createEntitiesBySchema };