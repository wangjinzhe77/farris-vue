/**
 * 分页信息
 */
export interface Pagination {
    pageIndex: number;
    pageSize?: number;
    totalCount?: number;
    pageCount?: number;
}
/**
 * 实体分页信息
 */
export interface EntityPagination {
    [entityPath: string]: Pagination;
}