/* eslint-disable no-use-before-define */
/**
 * 实体路径节点类型
 */
enum EntityPathNodeType {

    /**
     * 主键值
     */
    IdValue = "IdValue",

    /**
     * 属性名称
     */
    PropName = "PropName"
}

/**
 * 实体路径节点
 */
class EntityPathNode {

    /**
     * 类型
     */
    private nodeType: EntityPathNodeType;

    /**
     * 节点值
     */
    private nodeValue: string;

    /**
     * 上一路径节点
     */
    private prevNode: EntityPathNode | null;

    /**
     * 下一路径节点
     */
    private nextNode: EntityPathNode | null;

    /**
     * 构造函数
     */
    constructor(nodeType: EntityPathNodeType, nodeValue: string) {
        this.nodeType = nodeType;
        this.nodeValue = nodeValue;
    }

    /**
     * 获取节点类型
     */
    public getNodeType(): EntityPathNodeType {
        return this.nodeType;
    }

    /**
     * 获取节点值
     */
    public getNodeValue() {
        return this.nodeValue;
    }

    /**
     * 设置节点值
     */
    public setNodeValue(nodeValue: any) {
        this.nodeValue = nodeValue;
    }

    /**
     * 获取上一节点
     */
    public getPrevNode(): EntityPathNode | null {
        return this.prevNode;
    }

    /**
     * 获取下一节点
     */
    public getNextNode(): EntityPathNode | null {
        return this.nextNode;
    }

    /**
     * 设置上一节点
     */
    public setPrevNode(prevNode: EntityPathNode | null): void {
        this.prevNode = prevNode;
    }

    /**
     * 设置下一节点
     */
    public setNextNode(nextNode: EntityPathNode | null): void {
        this.nextNode = nextNode;
    }
}

export { EntityPathNodeType, EntityPathNode };
