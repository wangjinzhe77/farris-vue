import { Entity } from '../entity';
import { EntityList } from '../entity-list';
import { EntityState, EntityStore } from '../entity-store';
import { FieldType } from '../entity-schema';
import { EntityPath } from './entity-path';
import { EntityPathNode, EntityPathNodeType } from './entity-path-node';

/**
 * 实体路径构造器
 */
class EntityPathBuilder {

    /**
     * 实体状态
     */
    private entityStore: EntityStore<EntityState<Entity>>;

    /**
     * 构造函数
     */
    constructor(entityStore: EntityStore<EntityState<Entity>>) {
        this.entityStore = entityStore;
    }

    /**
     * 构造实体路径
     */
    public build(path: string | string[] | EntityPath, isEntityListPath = false): EntityPath {
        if (path instanceof EntityPath) {
            return path;
        }

        this.checkPath(path);

        const entityList = this.entityStore.getEntityList();
        const pathArray = this.convertToPathArray(path);
        const longPathArray = this.convertToLongPath(pathArray, entityList, isEntityListPath);
        const entityPath = this.buildEntityPath(longPathArray);

        return entityPath;
    }

    /**
     * 根据长路径数组构造实体路径对象
     */
    private buildEntityPath(longPathArray: string[]) {
        const entityPath = new EntityPath();
        longPathArray.forEach((pathItem: string) => {
            const isIdPathItem = this.isIdPathItem(pathItem);
            const nodeType = isIdPathItem ? EntityPathNodeType.IdValue : EntityPathNodeType.PropName;
            let nodeValue;
            if (isIdPathItem) {
                nodeValue = pathItem.slice(1).slice(0, -1);
            } else {
                nodeValue = pathItem;
            }
            const pathNode = new EntityPathNode(nodeType, nodeValue);
            entityPath.appendNode(pathNode);
        });

        return entityPath;
    }

    /**
     * 转换为长路径
     * 短路径：/childs/grands/assoInfo/name
     * 长路径：/[id]/childs/[childId]/assoInfo/name
     */
    private convertToLongPath(pathArray: string[], entityList: EntityList<Entity>, isEntityListPath = false): string[] {
        if (this.isShortPath(pathArray) === false) {
            return pathArray;
        }
        const pathArrayLen = pathArray.length;

        const longPathArray: string[] = [];
        if (isEntityListPath && pathArray.length === 0) {
            return longPathArray;
        }
        let currentEntity: any = entityList.getCurrentEntity();
        const currentId = currentEntity.idValue;
        longPathArray.push(`[${currentId}]`);

        pathArray.forEach((pathItem: string, pathIndex: number) => {
            const entitySchema = currentEntity.getSchema();
            const fieldSchema = entitySchema.getFieldSchemaByName(pathItem);
            if (!fieldSchema) {
                throw new Error(`${pathItem} not exist`);
            }

            if (fieldSchema.type === FieldType.EntityList) {
                const childEntityList = currentEntity[pathItem] as EntityList<Entity>;
                const currentChildEntity = childEntityList.getCurrentEntity();
                longPathArray.push(pathItem);

                // 实体列表短路径，最后一段不拼接当前实体ID
                if (isEntityListPath && pathIndex === pathArrayLen - 1) {
                    return;
                }
                longPathArray.push(`[${currentChildEntity.idValue}]`);
                currentEntity = currentChildEntity;
            } else if (fieldSchema.type === FieldType.Entity) {
                longPathArray.push(pathItem);
                currentEntity = currentEntity[pathItem];
            } else {
                longPathArray.push(pathItem);
            }
        });

        return longPathArray;
    }

    /**
     * 转换为路径数据
     */
    private convertToPathArray(path: string | string[]) {
        if (Array.isArray(path)) {
            return path;
        }

        const pathArray = path.split('/').filter((pathItem: string) => {
            return pathItem;
        });

        return pathArray;
    }

    /**
     * 检查路径是否合法
     */
    private checkPath(path: string | string[]): void {
        if (!path) {
            throw new Error('path can not be empty');
        }

        if (Array.isArray(path) === true) {
            return;
        }

        if (typeof path !== 'string') {
            throw new Error('path must be an string or array');
        }

        if (path.startsWith('/') === false) {
            throw new Error('path must be starts with /');
        }
    }

    /**
     * 检查是否为长路径
     */
    private isShortPath(pathArray: string[]): boolean {
        const firstPathItem = pathArray[0];
        if (firstPathItem && firstPathItem.startsWith('[')) {
            return false;
        }

        return true;
    }

    /**
     * 检查是否为ID路径片段
     */
    private isIdPathItem(pathItem: string): boolean {
        return pathItem.startsWith('[');
    }
}

export { EntityPathBuilder };
