import { EntityPathNode, EntityPathNodeType } from './entity-path-node';

/**
 * 实体路径
 */
class EntityPath {

    /**
     * 路径节点集合
     */
    private nodes: EntityPathNode[];

    /**
     * 构造函数
     */
    constructor() {
        this.nodes = [];
    }

    /**
     * 获取路径节点
     */
    public getNodes(): EntityPathNode[] {
        return this.nodes;
    }

    /**
     * 获取第一个节点
     */
    public getFirstNode(): EntityPathNode {
        return this.nodes[0];
    }

    /**
     * 获取最后一个节点
     */
    public getLastNode(): EntityPathNode {
        return this.nodes[this.nodes.length - 1];
    }

    /**
     * 获取父实体路径
     */
    public getParentEntityPath(): EntityPath {
        const parentEntityPath = new EntityPath();
        const parentPathNodes = this.nodes.concat([]);
        parentPathNodes.pop();

        parentPathNodes.forEach((parentPathNode) => {
            parentEntityPath.appendNode(parentPathNode);
        });

        return parentEntityPath;
    }

    /**
     * 添加节点
     */
    public appendNode(node: EntityPathNode) {
        const prevNode = this.nodes.length === 0 ? null : this.nodes[0];
        if (prevNode) {
            prevNode.setNextNode(node);
        }
        node.setPrevNode(prevNode);
        this.nodes.push(node);
    }

    /**
     * 是否和当前路径相等
     */
    public isSame(entityPath: EntityPath): boolean {
        const targetNodes = entityPath.getNodes();
        if (this.nodes.length !== targetNodes.length) {
            return false;
        }

        const isDifferent = this.nodes.some((node, index) => {
            const targetNode = targetNodes[index];
            const isDifferentType = node.getNodeType() !== targetNode.getNodeType();
            const isDifferentValue = node.getNodeValue() !== targetNode.getNodeValue();
            if (isDifferentType || isDifferentValue) {
                return true;
            }
        });

        return !isDifferent;
    }

    /**
     * 是否为当前路径的祖先路径
     */
    public isAncestor(ancestorPath: EntityPath): boolean {
        const ancestorNodes = ancestorPath.getNodes();
        const descendantNodes = this.nodes;
        if (ancestorNodes.length >= descendantNodes.length) {
            return false;
        }

        const isDifferent = ancestorNodes.some((ancestorNode, index) => {
            const descendantNode = descendantNodes[index];
            const isDifferentType = ancestorNode.getNodeValue() !== descendantNode.getNodeType();
            const isDifferentValue = ancestorNode.getNodeValue() !== descendantNode.getNodeValue();
            if (isDifferentType || isDifferentValue) {
                return true;
            }
        });

        return !isDifferent;
    }

    /**
     * 是否为当前路径的父路径
     */
    public isParent(parentPath: EntityPath): boolean {
        const ancestorNodes = parentPath.getNodes();
        const descendantNodes = this.nodes;

        if (ancestorNodes.length !== descendantNodes.length - 1) {
            return false;
        }

        return this.isAncestor(parentPath);
    }

    /**
     * 克隆路径
     */
    public clone() {
        const clonedEntityPath = new EntityPath();

        this.nodes.forEach((node) => {
            const type = node.getNodeType();
            const value = node.getNodeValue();
            const clonedNode = new EntityPathNode(type, value);
            clonedEntityPath.appendNode(clonedNode);
        });

        return clonedEntityPath;
    }
    /**
     * 转换为短路径
     * @returns 
     */
    public toShortPath() {
        return '/' + this.nodes.filter((node: EntityPathNode) => node.getNodeType() === EntityPathNodeType.PropName).map((node: EntityPathNode) => node.getNodeValue()).join('/');
    }
}

export { EntityPath };
