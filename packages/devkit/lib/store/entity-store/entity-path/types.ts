/**
 * 路径字符串
 *
 * @summary
 * ----------------------------------------
 * 父实体相关
 * ----------------------------------------
 * 父实体集合: /
 * 父实体：/[parentId]
 * 父实体下的关联实体：/[parentId]/asso
 * 父实体下的二级关联实体：/[parentId]/asso/childAsso
 *
 * ----------------------------------------
 * 子实体相关
 * ----------------------------------------
 *
 * 子实体集合：/[parentId]/childs
 * 子实体：/[parentId]/childs/[childId]
 * 子实体下的关联实体：/[parentId]/childs/[childId]/asso
 * 子实体下的二级关联实体：/[parentId]/childs/[childId]/asso/childAsso
 *
 * ----------------------------------------
 * 孙实体相关
 * ----------------------------------------
 * 孙实体集合：/[parentId]/childs/[childId]/grands
 * 孙实体：/[parentId]/childs/[childId]/grands/[grandId]
 * 孙实体下的关联实体：/[parentId]/childs/[childId]/grands/[grandId]/asso
 * 孙实体下的二级关联实体：/[parentId]/childs/[childId]/grands/[grandId]/asso/childAsso
 */
export type EntityPathString = string;

/**
 * 实体路径数组
 */

export type EntityPathArray = Array<string>;
