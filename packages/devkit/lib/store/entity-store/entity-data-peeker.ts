import { FieldType, EntitySchema, EntityFieldSchema, PrimitiveFieldSchema, EntityListFieldSchema } from './entity-schema';
import { Entity } from './entity';

/**
 * 实体数据提取
 */
class EntityDataPeeker {

    /**
     * 实体
     */
    private entity: Entity;

    /**
     * 实体描述
     */
    private entitySchema: EntitySchema;

    /**
     * 构造函数
     */
    constructor(entity: Entity) {
        this.entity = entity;
        this.entitySchema = entity.getSchema();
    }

    /**
     * 提取数据
     */
    public peek(): any {
        const entityData = {};
        this.peekPrimitiveFields(entityData);
        this.peekEntityFields(entityData);
        this.peekEntityListFields(entityData);
        return entityData;
    }

    /**
     * 提取简单字段数据
     */
    private peekPrimitiveFields(entityData: any): void {
        const entity = this.entity as any;
        const fieldSchemas = this.entitySchema.getFieldSchemasByType<PrimitiveFieldSchema>(FieldType.Primitive);
        fieldSchemas.forEach((fieldSchema) => {
            const propName = fieldSchema.name;
            entityData[propName] = entity[propName];
        });
    }

    /**
     * 提取实体字段数据
     */
    private peekEntityFields(entityData: any): void {
        const entity = this.entity as any;
        const fieldSchemas = this.entitySchema.getFieldSchemasByType<EntityFieldSchema>(FieldType.Entity);
        fieldSchemas.forEach((fieldSchema) => {
            const propName = fieldSchema.name;
            const childEntity = entity[propName];
            const childEntityData = childEntity ? childEntity.toJSON() : {};
            entityData[propName] = childEntityData;
        });
    }

    /**
     * 提取实体列表字段数据
     */
    private peekEntityListFields(entityData: any): void {
        const entity = this.entity as any;
        const fieldSchemas = this.entitySchema.getFieldSchemasByType<EntityListFieldSchema>(FieldType.EntityList);
        fieldSchemas.forEach((fieldSchema) => {
            const propName = fieldSchema.name;
            const childEntityList = entity[propName];
            const childEntityListData = childEntityList ? childEntityList.toJSON() : [];
            entityData[propName] = childEntityListData;
        });
    }
}

export { EntityDataPeeker };