/* eslint-disable no-use-before-define */
import { EntitySchema } from './entity-schema/index';
import { Entity } from './entity';
import { EntityList } from './entity-list';

/**
 * 创建实体列表（按描述）
 */
function createEntityListBySchema<T extends Entity>(entitySchema: EntitySchema, entityDatas: any[]): EntityList<T> {
    if (!entitySchema) {
        throw new Error(`The entitySchema parameter can't be empty`);
    }
    if (!Array.isArray(entityDatas)) {
        throw new Error(`The entityDatas parameter should be an array`);
    }

    const entityList = new EntityList<T>(entitySchema);
    entityList.loadData(entityDatas);

    return entityList;
}

export { createEntityListBySchema };