/* eslint-disable @typescript-eslint/indent */
import { EntityPathNode, EntityPathNodeType } from '../entity-path/index';
import {
    ChangeValueChange, UpdateEntityChange, RemoveEntityChange,
    EntityChange, EntityChangeType, EntityChangeStatus
} from './entity-change';

/**
 * 实体变更合并器
 */
class EntityChangeMerger {

    /**
     * 合并前的变更
     */
    public changes: EntityChange[];

    /**
     * 合并后的变更
     */
    public mergedChagnes: EntityChange[];

    /**
     * 构造函数
     */
    constructor(changes: EntityChange[]) {
        this.changes = changes;
        this.mergedChagnes = [];
    }

    /**
     * 合并变更
     */
    public mergeChanges() {
        this.changes.forEach((change: EntityChange) => {
            this.mergeChange(change);
        });
        return this.mergedChagnes;
    }

    private mergeChange(change: EntityChange) {
        if (change.status !== EntityChangeStatus.New) {
            return;
        }
        const clonedChange = Object.assign({}, change);
        switch (clonedChange.type) {
            case EntityChangeType.ValueChange:
                this.mergeChangeValueChange(change as ChangeValueChange);
                break;
            case EntityChangeType.Update:
                this.mergeUpdateChange(change as UpdateEntityChange);
                break;
            case EntityChangeType.Remove:
                this.mergeRemoveChange(change as RemoveEntityChange);
                break;
            default:
                break;
        }
    }

    /**
     * 合并值变更
     */
    public mergeChangeValueChange(change: ChangeValueChange): void {
        const existedMergedChange = this.mergedChagnes.find((mergedChange: EntityChange) => {
            const isSameType = mergedChange.type === EntityChangeType.ValueChange;
            const isSamePath = mergedChange.path.isSame(change.path);
            if (isSameType && isSamePath) {
                return true;
            }

            return false;
        }) as ChangeValueChange;

        if (existedMergedChange) {
            existedMergedChange.newValue = change.newValue;
        } else {
            this.mergedChagnes.push(change);
        }
    }

    /**
     * 合并更新变更
     */
    public mergeUpdateChange(change: UpdateEntityChange): void {
        Object.keys(change.newEntityData).forEach((propName: string) => {
            const changeValueChange = this.convertToChangeValueChange(change, propName);
            this.mergeChangeValueChange(changeValueChange);
        });
    }

    /**
     * 合并删除变更
     */
    public mergeRemoveChange(change: RemoveEntityChange) {
        this.mergedChagnes = this.mergedChagnes.filter((mergedChange) => {
            return mergedChange.path.isAncestor(change.path) === false
        });
    }

    /**
     * 转换为值变更
     */
    private convertToChangeValueChange(change: UpdateEntityChange, propName: string) {
        const oldValue = change.oldEntityData[propName];
        const newValue = change.newEntityData[propName];
        const path = change.path.clone();
        path.appendNode(new EntityPathNode(EntityPathNodeType.PropName, propName));

        const changeValueChange: ChangeValueChange = {
            type: EntityChangeType.ValueChange,
            path,
            oldValue,
            newValue,
            status: change.status,
            snapshot: change.snapshot
        };

        return changeValueChange;
    }
}

export { EntityChangeMerger };
