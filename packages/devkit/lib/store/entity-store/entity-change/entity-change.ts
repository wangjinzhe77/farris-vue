import { StateChange } from '../../store/index';
import { EntityPath } from '../entity-path/index';
import { Entity } from '../entity';
import { EntityState } from '../entity-store';
import { Pagination } from '../types';

/**
 * 实体状态变化类型
 */
enum EntityChangeType {

    /**
     * 加载
     */
    Load = 'Load',

    /**
     * 加载
     */
    Append = 'Append',

    /**
     * 删除
     */
    Remove = 'Remove',

    /**
     * 更新
     */
    Update = 'Update',

    /**
     * 值变化
     */
    ValueChange = 'ValueChange',

    /**
     * 当前实体切换
     */
    CurrentChange = 'CurrentChange'
}

/**
 * 变更状态
 */
enum EntityChangeStatus {

    /**
     * 新变更
     */
    New = 'New',

    /**
     * 已暂存
     */
    Staged = 'Staged',

    /**
     * 已提交
     */
    Committed = 'Commited',

    /**
     * 已取消
     */
    Canceled = 'Canceled'
}

/**
 * 实体状态变更
 */
interface EntityChange extends StateChange<EntityState<Entity>> {

    /**
     * 类型
     */
    type: EntityChangeType;

    /**
     * 路径
     */
    path: EntityPath;

    /**
     * 变更状态
     */
    status?: EntityChangeStatus;

    /**
     * 快照版本
     */
    snapshot?: string;
}

/**
 * 加载实体变更
 */
interface LoadEntityChange extends EntityChange {

    /**
     * 变更类型
     */
    type: EntityChangeType.Load,

    /**
     * 加载的实体集合
     */
    entities: Entity[];
    /**
     * 页码
     */
    pagination?: Pagination;

}

/**
 * 新增实体变更
 */
interface AppendEntityChange extends EntityChange {

    /**
     * 变更类型
     */
    type: EntityChangeType.Append;

    /**
     * 新增的实体集合
     */
    entities: Entity[];
}

/**
 * 删除实体变更
 */
interface RemoveEntityChange extends EntityChange {

    /**
     * 变更类型
     */
    type: EntityChangeType.Remove;

    /**
     * 新增的实体集合
     */
    entities: Entity[];
}

/**
 * 更新实体变更
 */
interface UpdateEntityChange extends EntityChange {

    /**
     * 变更类型
     */
    type: EntityChangeType.Update;

    /**
     * 原实体数据
     */
    oldEntityData: any;

    /**
     * 新实体数据
     */
    newEntityData: any;
}

/**
 * 修改值变更
 */
interface ChangeValueChange extends EntityChange {

    /**
     * 变更类型
     */
    type: EntityChangeType.ValueChange;

    /**
     * 原字段值
     */
    oldValue: any;

    /**
     * 新字段值
     */
    newValue: any;
}

/**
 * 当前实体切换变更
 */
interface ChangeCurrentEntityChange extends EntityChange {

    /**
     * 变更类型
     */
    type: EntityChangeType.CurrentChange;

    /**
     * 原当前实体
     */
    oldCurrentEntity: any;

    /**
     * 新当前实体
     */
    newCurrentEntity: any;
}

export {
    EntityChangeType, EntityChangeStatus, EntityChange,
    LoadEntityChange, AppendEntityChange, RemoveEntityChange, UpdateEntityChange, ChangeValueChange, ChangeCurrentEntityChange
};
