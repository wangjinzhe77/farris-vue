import { EntityChangeStatus, EntityChange } from './entity-change';
import { EntityChangeMerger } from './entity-change-merger';

/**
 * 实体状态变更历史
 */
class EntityChangeHistory {

    /**
     * 变更集合
     */
    private changes: EntityChange[];

    /**
     * 构造函数
     */
    constructor() {
        this.changes = [];
    }

    /**
     * 获取全部变更
     */
    public getChanges() {
        return this.changes;
    }

    /**
     * 获取合并
     */
    public getMergedChanges() {
        const merger = new EntityChangeMerger(this.changes);
        const mergedChanges = merger.mergeChanges();

        return mergedChanges;
    }

    /**
     * 添加变更
     */
    public addChange(change: EntityChange) {
        change.status = EntityChangeStatus.New;
        this.changes.push(change);
    }

    /**
     * 暂存变更
     */
    public stageChanges() {
        this.changes.forEach((change) => {
            if (change.status === EntityChangeStatus.New) {
                change.status = EntityChangeStatus.Staged;
            }
        });
    }

    /**
     * 提交变更
     */
    public commitChanges() {
        this.changes.forEach((change) => {
            if (change.status === EntityChangeStatus.New || change.status === EntityChangeStatus.Staged) {
                change.status = EntityChangeStatus.Committed;
            }
        });
    }

    /**
     * 取消变更
     */
    public cancelChanges() {
        this.changes.forEach((change) => {
            if (change.status === EntityChangeStatus.New || change.status === EntityChangeStatus.Staged) {
                change.status = EntityChangeStatus.Canceled;
            }
        });
    }
}

export { EntityChangeHistory };
