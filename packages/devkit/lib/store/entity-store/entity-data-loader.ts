import { FieldType, EntitySchema, EntityFieldSchema, PrimitiveFieldSchema, EntityListFieldSchema } from './entity-schema';
import { Entity } from './entity';
import { createEntityBySchema } from './entity-creator';
import { createEntityListBySchema } from './entity-list-creator';

/**
 * 实体数据加载器
 */
class EntityDataLoader {

    /**
     * 实体
     */
    private entity: Entity;

    /**
     * 实体描述
     */
    private entitySchema: EntitySchema;

    /**
     * 构造函数
     */
    constructor(entity: Entity) {
        this.entity = entity;
        this.entitySchema = entity.getSchema();
    }

    /**
     * 加载数据
     */
    public load(entityData: any) {
        this.loadPrimitiveFields(entityData);
        this.loadEntityFields(entityData);
        this.loadEntityListFields(entityData);
    }

    /**
     * 加载简单字段数据
     */
    private loadPrimitiveFields(entityData: any): void {
        const entity = this.entity as any;
        const fieldSchemas = this.entitySchema.getFieldSchemasByType<PrimitiveFieldSchema>(FieldType.Primitive);
        fieldSchemas.forEach((fieldSchema) => {
            const propName = fieldSchema.name;
            entity[propName] = entityData[propName];
        });
    }

    /**
     * 加载实体字段数据
     */
    private loadEntityFields(entityData: any): void {
        const entity = this.entity as any;
        const fieldSchemas = this.entitySchema.getFieldSchemasByType<EntityFieldSchema>(FieldType.Entity);
        fieldSchemas.forEach((fieldSchema) => {
            const propName = fieldSchema.name;
            const childEntitySchema = fieldSchema.entitySchema;
            const childEntityData = entityData[propName] || {};
            const childEntity = createEntityBySchema<Entity>(childEntitySchema, childEntityData);
            childEntity.setParentEntityNode(entity);

            entity[propName] = childEntity;
        });
    }

    /**
     * 加载实体列表字段数据
     */
    private loadEntityListFields(entityData: any): void {
        const entity = this.entity as any;
        const fieldSchemas = this.entitySchema.getFieldSchemasByType<EntityListFieldSchema>(FieldType.EntityList);
        fieldSchemas.forEach((fieldSchema) => {
            const propName = fieldSchema.name;
            const childEntitySchema = fieldSchema.entitySchema;
            const childEntityListData = entityData[propName] || [];
            const childEntityList = createEntityListBySchema(childEntitySchema, childEntityListData);
            childEntityList.setParentEntityNode(entity);

            entity[propName] = childEntityList;
        });
    }
}

export { EntityDataLoader };
