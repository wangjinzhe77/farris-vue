import { Type } from '../../common/index';

/**
 * Store元数据
 */
interface StoreMeta {

    /**
     * ID
     */
    id: string;

    /**
     * 静态Store依赖
     */
    deps?: any[];

    /**
     * 静态Store状态类型
     */
    stateType: Type<any>;
}

export { StoreMeta };

