/**
 * 状态变更
 */
interface StateChange<S> {

    /**
     * 老状态
     */
    oldState?: S;

    /**
     * 新状态
     */
    newState?: S;
}

export { StateChange }