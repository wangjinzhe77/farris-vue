/* eslint-disable @typescript-eslint/no-unsafe-function-type */
import { Effect, EffectManager, Injector} from '../../common/index';
import { Module } from '../../module/index'
import { StoreConfig } from './store-config';
import { StateChange } from './state-change';

/**
 * 状态仓库
 */
class Store<S> {

    /**
     * 所属Module
     */
    protected module: Module;

    /**
     * Store配置
     */
    protected config!: StoreConfig;

    /**
     * Store状态
     */
    protected state!: S;

    /**
     * 副作用管理
     */
    public effectManager: EffectManager<StateChange<S>>;

    /**
     * 构造函数
     */
    constructor(module: Module) {
        this.effectManager = new EffectManager<StateChange<S>>();
        this.module = module;
    }

    /**
     * 初始化
     */
    public init(storeConfig: StoreConfig) {
        this.config = storeConfig;
    }

    /**
     * 获取ID
     */
    public getId(): string {
        return this.config.id;
    }

    /**
     * 获取状态
     */
    public getState() {
        return this.state;
    }

    /**
     * 更新状态
     */
    public setState(newState: S) {
        const oldState = this.state;
        this.state = newState;

        const stateChange: StateChange<S> = {
            oldState,
            newState
        };
        this.triggerChange(stateChange)
    }

    /**
     * 获取模块
     */
    public getModule(): Module {
        return this.module;
    }

    /**
     * 获取模块注入器
     */
    public getModuleInjector(): Injector {
        return this.module.getInjector();
    }

    /**
     * 触发变更
     */
    public triggerChange(change: StateChange<S>) {
        this.effectManager.trigger(change);
    }

    /**
     * 监听变更
     */
    public watchChange(changeEffectFunc: (change: StateChange<S>) => void): Function {
        const changeEffect = new Effect<StateChange<S>>(changeEffectFunc);
        this.effectManager.addEffect(changeEffect);
        const stopWatch = () => {
            this.effectManager.removeEffect(changeEffect);
        };

        return stopWatch;
    }
}

export { Store }