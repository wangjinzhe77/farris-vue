import { Type } from '../../common/index';
import { Store } from './store';

/**
 * Store配置
 */
interface StoreConfig {

    /**
     * ID
     */
    id: string;

    /**
     * 静态Store类型
     */
    type?: Type<Store<any>>;

    /**
     * 静态Store类型
     */
    deps?: any[];

    /**
     * State配置
     */
    state: any;
}

export { StoreConfig };