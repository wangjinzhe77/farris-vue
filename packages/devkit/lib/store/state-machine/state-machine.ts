
import { Module } from '../../module/index';
import { Store, StateChange } from '../store';
import { StateMachineConfig, StateMachineConfigManager } from './configs/index';
import { StateMachineInitializer } from './state-machine-initializer';
import { StateMachineWatcher } from './state-machine-watcher';
import { StateMachineChange } from './change';

/**
 * 状态机状态定义
 */
class StateMachineState {

    /**
     * 当前页面状态
     */
    public currentPageState: string;

    /**
     * 其他可视化状态
     */
    [key: string]: boolean | string;
}

/**
 * 状态机定义
 */
class StateMachine<S extends StateMachineState> extends Store<S> {

    /**
     * 状态渲染器
     */
    private renders: Map<string, any>;

    /**
     * 配置管理器
     */
    public configManager: StateMachineConfigManager;

    /**
     * 外部状态监听器
     */
    public stateWatcher: StateMachineWatcher;

    /**
     * 构造函数
     */
    constructor(module: Module) {
        super(module);
        this.renders = new Map<string, any>;
    }

    /**
     * 初始化
     */
    public init(config: StateMachineConfig) {
        super.init(config);
        this.configManager = new StateMachineConfigManager(config);
        
        const initializer = new StateMachineInitializer(this);
        initializer.init();
    
        this.stateWatcher = new StateMachineWatcher(this);
        this.stateWatcher.watch();
    }

    /**
     * 获取状态值
     */
    public getValue(name: string): any {
        return this.state[name];
    }

    /**
     * 迁移到初始状态
     */
    public transitToInitState() {
        const initPageState = this.configManager.getInitPageState();
        this.transitTo(initPageState);
    }

    /**
     * 迁移状态
     */
    public transitTo(nextPageState: string): void {
        const nextPageStateConfig = this.configManager.getPageStateConfigByName(nextPageState);
        if (!nextPageStateConfig) {
            throw new Error(`PageStateConfig(name=${nextPageState}) does not exist`);
        }

        const oldPageState = this.state.currentPageState;
        if (oldPageState === nextPageState) {
            return;
        }

        this.refreshState(nextPageState);
    }

    /**
     * 注册渲染器
     */
    public registerRender(name: string, render: any): void {
        this.renders.set(name, render);
    }

    /**
     * 刷新状态
     */
    public refreshState(nextPageState?: string): void {
        const changedValues: any = {};

        const oldState: any = { ...this.state };
        const newState: any = {};
        const currentState: any = this.state;
        
        if (nextPageState) {
            newState.currentPageState = nextPageState;
            currentState.currentPageState = newState.currentPageState;

            if (newState.currentPageState !== oldState.currentPageState) {
                changedValues.currentPageState = newState.currentPageState;
            }
        } else {
            newState.currentPageState = oldState.currentPageState;
        }

        this.renders.forEach((render: any, renderStateName: string) => {
            newState[renderStateName] = render();
            currentState[renderStateName] = newState[renderStateName];

            if (newState[renderStateName] !== oldState[renderStateName]) {
                changedValues[renderStateName] = newState[renderStateName];
            }
        });

        this.state = newState;
        const stateChange: StateMachineChange = {
            oldState,
            newState,
            changedValues
        }
        this.triggerChange(stateChange as any);
    }
}

export { StateMachineState, StateMachine };



