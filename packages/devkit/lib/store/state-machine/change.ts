import { StateChange } from '../store/index';
import { StateMachineState } from './state-machine';

interface StateMachineChange extends StateChange<StateMachineState>  {
    changedValues: any;
}

export { StateMachineChange };

