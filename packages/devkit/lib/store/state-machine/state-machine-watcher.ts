import { Module } from '../../module/index';
import { StateVariableUtil } from '../../variable/index';
import { Entity, EntityState, EntityStore } from '../entity-store/index';
import { UIState, UIStore } from '../ui-store/index';
import { StateMachineConfigManager } from './configs/index';
import { StateMachine, StateMachineState } from './state-machine';

/**
 * 外部状态监听器
 */
class StateMachineWatcher {

    /**
     * 模块
     */
    private module: Module;

    /**
     * 状态机
     */
    private stateMachine: StateMachine<StateMachineState>;

    /**
     * 状态机配置管理器
     */
    public configManager: StateMachineConfigManager;

    /**
     * 构造函数
     */
    constructor(stateMachine: StateMachine<StateMachineState>) {
        this.stateMachine = stateMachine;
        this.module = stateMachine.getModule();
        this.configManager = stateMachine.configManager;
    }

    /**
     * 监听变量变化
     */
    public watch() {
        const variables = this.getVariables();
        this.watchEntityStores(variables);
        this.watchUIStores(variables);
    }

    /**
     * 获取条件中的所有变量
     */
    private getVariables(): string[] {
        const variableSet = new Set<string>();;
        const renderStateConfigs = this.configManager.getRenderStateConfigs();
        renderStateConfigs.forEach((renderStateConfig) => {
            const conditions = renderStateConfig.conditions;
            conditions.forEach((condition) => {
                variableSet.add(condition.source);
            });
        });

        return Array.from(variableSet);
    }

    /**
     * 监听关联的实体状态
     */
    public watchEntityStores(variables: string[]): void {
        const entityStores = new Set<EntityStore<EntityState<Entity>>>();
        const entityVariablePrefix = 'DATA';

        variables.forEach((variable: string) => {
            if (!variable.startsWith('{' + entityVariablePrefix)) {
                return;
            }

            const variablePath = StateVariableUtil.getVariablePath(variable, entityVariablePrefix);
            const viewModel = this.module.getViewModel(variablePath.viewModelId);
            if (viewModel && viewModel.entityStore) {
                entityStores.add(viewModel.entityStore);
            }
        });

        entityStores.forEach((entityStore) => {
            entityStore.watchChange(() => {
                this.stateMachine.refreshState();
            });
        });

    }

    /**
     * 监听关联的UI状态
     */
    private watchUIStores(variables): void {
        const uiStores = new Set<UIStore<UIState>>();
        const uiVariablePrefix = 'UISTATE';

        variables.forEach((variable: string) => {
            if (!variable.startsWith('{' + uiVariablePrefix)) {
                return;
            }

            const variablePath = StateVariableUtil.getVariablePath(variable, uiVariablePrefix);
            const viewModel = this.module.getViewModel(variablePath.viewModelId);
            if (viewModel && viewModel.uiStore) {
                uiStores.add(viewModel.uiStore);
            }
        });

        uiStores.forEach((entityStore) => {
            entityStore.watchChange(() => {
                this.stateMachine.refreshState();
            });
        });
    }
}

export { StateMachineWatcher };
