import { StateMachineConfigManager } from './configs/index';
import { StateMachineState, StateMachine } from './state-machine';
import { ConditionEvaluator } from '../../condition/index';
import { VariableParseContext } from '../../variable';

/**
 * 状态机初始化器
 */
class StateMachineInitializer {

    /**
     * 状态机实例
     */
    private stateMachine: StateMachine<StateMachineState>;

    /**
     * 配置管理器
     */
    public configManager: StateMachineConfigManager;

    /**
     * 渲染条件求值器
     */
    public conditionEvaluator: ConditionEvaluator;

    /**
     * 构造函数
     */
    constructor(stateMachine: StateMachine<StateMachineState>) {
        this.stateMachine = stateMachine;
        this.configManager = stateMachine.configManager;
        const module = this.stateMachine.getModule();
        const context: VariableParseContext = {
            module: module
        };
        this.conditionEvaluator = new ConditionEvaluator(context);
    }

    /**
     * 初始化
     */
    public init(): void {
        this.initState();
        this.initActions();
    }

    /**
     * 初始化状态
     */
    private initState() {
        const state = new StateMachineState();
        this.initRenderStates(state);
        this.initRenders();

        this.stateMachine.setState(state);
    }

    /**
     * 初始化可视化状态
     */
    private initRenderStates(state: StateMachineState) {
        const renderStateConfigs = this.configManager.getRenderStateConfigs();
        renderStateConfigs.forEach((renderStateConfig) => {
            const name = renderStateConfig.name;
            state[name] = false;
        });
    }

    /**
     * 初始化渲染方法
     */
    private initRenders() {
        const renderStateConfigs = this.configManager.getRenderStateConfigs();
        renderStateConfigs.forEach((renderStateConfig) => {
            const name = renderStateConfig.name;
            const conditions = renderStateConfig.conditions;
            const render = () => {
                return this.conditionEvaluator.evaluate(conditions);
            };

            this.stateMachine.registerRender(name, render);
        });
    }

    /**
     * 初始化迁移动作
     */
    private initActions() {
        const actionConfigs = this.configManager.getTransitionActionConfigs();
        actionConfigs.forEach((actionConfig) => {
            const actionName = actionConfig.name;
            const actionMethod = () => {
                const nextPageState = actionConfig.transitionTo;
                const nextPageStateConfig = this.configManager.getPageStateConfigByName(nextPageState);
                if (!nextPageStateConfig) {
                    throw new Error('PageStateConfig(name=${nextPageState}) does not exist');
                }

                this.stateMachine.transitTo(nextPageState);
            }
            this.stateMachine[actionName] = actionMethod;
        });
    }
}

export { StateMachineInitializer };