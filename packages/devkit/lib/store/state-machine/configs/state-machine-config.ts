import { StaticProvider, Type } from '../../../common/index';
import { ConditionConfig } from '../../../condition/index';
import { StoreConfig } from '../../store/index';
import { StateMachineState, StateMachine } from '../state-machine';

/**
 * 页面状态配置
 */
interface PageStateConfig {
    name: string;
};

/**
 * 可视化状态配置
 */
interface RenderStateConfig {
    name: string,
    conditions: ConditionConfig[];
};

/**
 * 状态机状态配置
 */
interface StateMachineStateConfig {
    renderStates: RenderStateConfig[];
}

/**
 * 迁移动作配置
 */
interface TransitionActionConfig {
    name: string;
    transitionTo: string;
}

/**
 * 状态机配置
 */
interface StateMachineConfig extends StoreConfig {
    id: string;
    type?: Type<StateMachine<StateMachineState>>;
    deps?: StaticProvider[],
    initPageState: string;
    pageStates: PageStateConfig[];
    transitionActions: TransitionActionConfig[];
    state: StateMachineStateConfig;
};

export { PageStateConfig, RenderStateConfig, StateMachineStateConfig, TransitionActionConfig, StateMachineConfig };