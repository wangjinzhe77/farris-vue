
import { PageStateConfig, RenderStateConfig, TransitionActionConfig, StateMachineConfig } from './state-machine-config';

/**
 * 状态机配置管理
 */

class StateMachineConfigManager {

    /**
     * 状态机配置
     */
    private config: StateMachineConfig

    /**
     * 构造函数
     */
    constructor(config: StateMachineConfig) {
        this.config = config;
    }

    /**
     * 获取全部配置
     */
    public getConfig() {
        return this.config;
    }

    /**
     * 获取初始化页面状态
     */
    public getInitPageState() {
        return this.config.initPageState;
    }

    /**
     * 根据name获取页面状态配置
     */
    public getPageStateConfigByName(name: string): PageStateConfig | undefined {
        if (!Array.isArray(this.config.pageStates)) {
            return;
        }

        const targetPageStateConfig = this.config.pageStates.find((pageStateConfig) => {
            return pageStateConfig.name === name;
        });

        return targetPageStateConfig;
    }

    /**
     * 根据全部可视化状态配置
     */
    public getRenderStateConfigs(): RenderStateConfig[] {
        if (!this.config.state || !Array.isArray(this.config.state.renderStates)) {
            return [];
        }
        return this.config.state.renderStates;
    }

    /**
     * 根据name获取可视化状态配置
     */
    public getRenderStateConfigByName(name: string): RenderStateConfig | undefined {
        if (!this.config.state || !Array.isArray(this.config.state.renderStates)) {
            return;
        }

        const targetRenderStateConfig = this.config.state.renderStates.find((renderStateConfig) => {
            return renderStateConfig.name === name;
        });

        return targetRenderStateConfig;
    }

    /**
     * 根据全部动作配置
     */
    public getTransitionActionConfigs(): TransitionActionConfig[] {
        if (!Array.isArray(this.config.transitionActions)) {
            return [];
        }
        return this.config.transitionActions;
    }

    /**
     * 根据name获取动作配置
     */
    public getTransitionActionConfigByName(name: string): TransitionActionConfig | undefined {
        if (!Array.isArray(this.config.transitionActions)) {
            return;
        }

        const targetActionsConfig = this.config.transitionActions.find((actionConfig) => {
            return actionConfig.name === name;
        });

        return targetActionsConfig;
    }
}

export { StateMachineConfigManager };
