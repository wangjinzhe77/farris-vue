import { Type, TypeMetadataReader, PropMetadataReader, SameNamePropsMetadatas } from '../../../common/index';
import {
    STATE_MACHINE_META_NAME, StateMachineMeta, RENDER_STATE_META_NAME, RenderStateMeta,
    PAGE_STATE_META_NAME, PageStateMeta, TRANSITION_ACTION_META_NAME, TransitionActionMeta
} from '../decorators/index';
import { PageStateConfig, TransitionActionConfig, RenderStateConfig, StateMachineStateConfig, StateMachineConfig } from './state-machine-config';
import { StateMachineState, StateMachine } from '../state-machine';

/**
 * 状态机配置构造器
 */
class StateMachineConfigBuilder {

    /**
     * 构造状态机配置 
     */
    public build(stateMachineType: Type<StateMachine<StateMachineState>>): StateMachineConfig {
        const stateMachineMeta = this.getStateMachineMeta(stateMachineType);
        const pageStateConfigs = this.buildPageStateConfigs(stateMachineType);
        const transitionActionConfigs = this.buildTransitionActionConfigs(stateMachineType);
        const stateConfig = this.buildStateConfig(stateMachineMeta.stateType);
        const stateMachineConfig: StateMachineConfig = {
            id: stateMachineMeta.id,
            type: stateMachineType,
            pageStates: pageStateConfigs,
            initPageState: stateMachineMeta.initPageState,
            transitionActions: transitionActionConfigs,
            state: stateConfig,
        };

        return stateMachineConfig;
    }

    /**
     * 构造页面状态配置
     */
    private buildPageStateConfigs(stateMachineType: Type<StateMachine<StateMachineState>>): PageStateConfig[] {
        const pageStateMetas = this.getPageStateMetas(stateMachineType);
        const pageStateConfigs: PageStateConfig[] = [];
        Object.keys(pageStateMetas).forEach((propName: string) => {
            const pageStateConfig: PageStateConfig = {
                name: propName,
            };
            pageStateConfigs.push(pageStateConfig);
        });

        return pageStateConfigs;
    }

    /**
     * 构造迁移动作配置
     */
    private buildTransitionActionConfigs(stateMachineType: Type<StateMachine<StateMachineState>>): TransitionActionConfig[] {
        const actionMetas = this.getTransitionActionMetas(stateMachineType);
        const actionConfigs: TransitionActionConfig[] = [];
        Object.keys(actionMetas).forEach((propName: string) => {
            const actionMeta = actionMetas[propName];
            const actionConfig: TransitionActionConfig = {
                name: propName,
                transitionTo: actionMeta.transitionTo

            };
            actionConfigs.push(actionConfig);
        });

        return actionConfigs;
    }

    /**
     * 构造状态机状态配置
     */
    private buildStateConfig(stateType: Type<StateMachineState>): StateMachineStateConfig {
        const renderStateMetas = this.getRenderStateMetas(stateType);
        const renderStateConfigs: RenderStateConfig[] = [];
        Object.keys(renderStateMetas).forEach((propName: string) => {
            const renderStateMeta = renderStateMetas[propName];
            const renderStateConfig: RenderStateConfig = {
                name: propName,
                conditions: renderStateMeta.conditions
            };
            renderStateConfigs.push(renderStateConfig);
        });

        const stateConfig: StateMachineStateConfig = {
            renderStates: renderStateConfigs
        }

        return stateConfig;
    }

    /**
     * 获取状态机元数据
     */
    private getStateMachineMeta(stateMachineType: Type<StateMachine<StateMachineState>>): StateMachineMeta {
        const stateMachineMeta = TypeMetadataReader.getMataByName(stateMachineType, STATE_MACHINE_META_NAME);
        return stateMachineMeta
    }

    /**
     * 获取页面状态元数据
     */
    private getPageStateMetas(stateMachineType: Type<StateMachine<StateMachineState>>): SameNamePropsMetadatas<PageStateMeta> {
        const pageStateMetas = PropMetadataReader.getPropsMetasByName<PageStateMeta>(stateMachineType, PAGE_STATE_META_NAME);
        return pageStateMetas;
    }

    /**
     * 获取迁移动作元数据
     */
    private getTransitionActionMetas(stateMachineType: Type<StateMachine<StateMachineState>>): SameNamePropsMetadatas<TransitionActionMeta> {
        const actionMetas = PropMetadataReader.getPropsMetasByName<TransitionActionMeta>(stateMachineType, TRANSITION_ACTION_META_NAME);
        return actionMetas;
    }

    /**
     * 获取可视化状态元数据
     */
    private getRenderStateMetas(stateType: Type<StateMachineState>): SameNamePropsMetadatas<RenderStateMeta> {
        const renderStateMetas = PropMetadataReader.getPropsMetasByName<RenderStateMeta>(stateType, RENDER_STATE_META_NAME);
        return renderStateMetas;
    }
}

export { StateMachineConfigBuilder };
