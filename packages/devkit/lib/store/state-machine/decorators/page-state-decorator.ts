
import { PropDecorator, makePropMetadataDecorator } from '../../../common/index';


/**
 * 页面状态元数据名称
 */
const PAGE_STATE_META_NAME = 'PAGE_STATE_META';

/**
 * 页面状态元数据类型
 */
interface PageStateMeta {

    isInit: boolean;
}

/**
 * 页面状态元数据工厂
 */
const PageStateMetaFactory = (meta: PageStateMeta) => {
    return meta;
}

/**
 * 页面状态装饰器工厂类型
 */
interface PageStateMetaDecoratorFactory {
    (meta?: PageStateMeta): PropDecorator
}

/**
 * 页面状态饰器工厂
 */
const FdPageState: PageStateMetaDecoratorFactory = makePropMetadataDecorator(PAGE_STATE_META_NAME, PageStateMetaFactory);

export { PageStateMeta, PAGE_STATE_META_NAME, PageStateMetaFactory, PageStateMetaDecoratorFactory, FdPageState };
