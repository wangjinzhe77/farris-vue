
import { PropDecorator, makePropMetadataDecorator } from '../../../common/index';

/**
 * 迁移动作元数据名称
 */
const TRANSITION_ACTION_META_NAME = 'TRANSITION_ACTION_META';

/**
 * 迁移动作元数据类型
 */
interface TransitionActionMeta {

    /**
     * 目标状态
     */
    transitionTo: string;
}

/**
 * 迁移动作元数据工厂
 */
const transitionActionMetaFactory = (meta: TransitionActionMeta) => {
    return meta;
}

/**
 * 迁移动作装饰器工厂类型
 */
interface TransitionActionMetaDecoratorFactory {
    (meta?: TransitionActionMeta): PropDecorator
}

/**
 * 迁移动作装饰器工厂
 */
const FdTransitionAction: TransitionActionMetaDecoratorFactory = makePropMetadataDecorator(TRANSITION_ACTION_META_NAME, transitionActionMetaFactory);

export { TransitionActionMeta, TRANSITION_ACTION_META_NAME, transitionActionMetaFactory, TransitionActionMetaDecoratorFactory, FdTransitionAction };
