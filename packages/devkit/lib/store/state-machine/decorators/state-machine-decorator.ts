import { Type, TypeDecorator, makeTypeMetadataDecorator } from '../../../common/index';
import { StoreMeta } from '../../store/index';
import { StateMachineState } from '../state-machine';

/**
 * 状态机元数据名称
 */
const STATE_MACHINE_META_NAME = 'STATE_MACHINE_META';

/**
 * 状态机元数据
 */
interface StateMachineMeta extends StoreMeta {
    stateType: Type<StateMachineState>;
    initPageState: string
}

/**
 * 状态机元数据工厂
 */
const stateMachineMetaFactory = (meta: StateMachineMeta): StateMachineMeta => {
    return meta;
};

/**
 * 状态机装饰器工厂类型
 */
interface StateMachineDecoratorFactory {
    (meta: StateMachineMeta): TypeDecorator;
}

/**
 * 状态机装饰器工厂
 */
function FdStateMachine(meta: StateMachineMeta) {
    const decoratorFactory: StateMachineDecoratorFactory = makeTypeMetadataDecorator(STATE_MACHINE_META_NAME, stateMachineMetaFactory);
    return decoratorFactory(meta);
}

export { STATE_MACHINE_META_NAME, StateMachineMeta, stateMachineMetaFactory, StateMachineDecoratorFactory, FdStateMachine }
