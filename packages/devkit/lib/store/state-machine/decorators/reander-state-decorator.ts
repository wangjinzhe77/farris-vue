
import { PropDecorator, makePropMetadataDecorator } from '../../../common/index';
import { ConditionConfig } from '../../../condition/index';

/**
 * 可视化状态元数据名称
 */
const RENDER_STATE_META_NAME = 'RENDER_STATE_META';

/**
 * 可视化状态元数据类型
 */
interface RenderStateMeta {

    /**
     * 求值条件
     */
    conditions: ConditionConfig[];
}

/**
 * 可视化状态元数据工厂
 */
const renderStateMetaFactory = (meta: RenderStateMeta) => {
    return meta;
}

/**
 * 可视化状态装饰器工厂类型
 */
interface renderStateMetaDecoratorFactory {
    (meta?: RenderStateMeta): PropDecorator
}

/**
 * 可视化状态饰器工厂
 */
const FdRenderState: renderStateMetaDecoratorFactory = makePropMetadataDecorator(RENDER_STATE_META_NAME, renderStateMetaFactory);

export { RenderStateMeta, RENDER_STATE_META_NAME, renderStateMetaFactory, renderStateMetaDecoratorFactory, FdRenderState };
