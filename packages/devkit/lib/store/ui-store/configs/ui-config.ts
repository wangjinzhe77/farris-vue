import { Type } from '../../../common/index';
import { StoreConfig } from '../../store/index';
import { UIState, UIStore } from '../ui-store';

/**
 * UIState配置
 */
interface UIStatePropConfig {
    name: string;
}

/**
 * UIState配置
 */
interface UIStateConfig {
    props: UIStatePropConfig[]
}

/**
 * UIStore配置
 */
interface UIStoreConfig extends StoreConfig {
    id: string;
    type?: Type<UIStore<UIState>>;
    deps?: [];
    state: UIStateConfig
}

export { UIStatePropConfig, UIStateConfig, UIStoreConfig };