
import { Type, TypeMetadataReader, PropMetadataReader, SameNamePropsMetadatas } from '../../../common/index';
import { UISTATE_META_NAME, UIStateMeta, UI_STORE_META_NAME, UIStoreMeta } from '../decorators/index';
import { UIState, UIStore } from '../ui-store';
import { UIStatePropConfig, UIStateConfig, UIStoreConfig } from './ui-config';

/**
 * UIStore配置构造器
 */
class UIStoreConfigBuilder {

    /**
     * 构造UIStore配置
     */
    public build(uiStoreType: Type<UIStore<UIState>>): UIStoreConfig {
        const uiStoreMeta = this.getUIStoreMeta(uiStoreType);
        const id = uiStoreMeta.id;
        const uiStateConfig = this.buildUIStateConfig(uiStoreMeta.stateType);
        const uiStoreConfig: UIStoreConfig = {
            id,
            state: uiStateConfig,
            type: uiStoreType
        };

        return uiStoreConfig;
    }

    /**
     * 构造UIState配置
     */
    private buildUIStateConfig(uiStateType: Type<UIState>): UIStateConfig {
        const propMetas = this.getUIStatePropMetas(uiStateType);

        const propConfigs: UIStatePropConfig[] = [];
        Object.keys(propMetas).forEach((propName: string) => {
            const propConfig: UIStatePropConfig = {
                name: propName
            };
            propConfigs.push(propConfig);
        });

        const uiStateConfig: UIStateConfig = {
            props: propConfigs
        }

        return uiStateConfig;
    }

    /**
     * 获取UIStore元数据
     */
    private getUIStoreMeta(uiStoreType: Type<UIStore<UIState>>): UIStoreMeta {
        const uiStoreMeta = TypeMetadataReader.getMataByName(uiStoreType, UI_STORE_META_NAME);
        return uiStoreMeta;
    }

    /**
     * 获取UIStateProp元数据
     */
    private getUIStatePropMetas(uiStateType: Type<UIState>): SameNamePropsMetadatas<UIStateMeta> {
        const propsMetas = PropMetadataReader.getPropsMetasByName<UIStateMeta>(uiStateType, UISTATE_META_NAME);
        return propsMetas;
    }
}

export { UIStoreConfigBuilder };