import { StateChange } from '../store/index';
import { UIState } from '../ui-store';

/**
 * UIState变更类型
 */
enum UIStateChangeType {

    /**
     * 值变更
     */
    ValueChange = 'ValueChange'
}

/**
 * UIState变更
 */
interface UIStateChange extends StateChange<UIState> {

    /**
     * 变更类型
     */
    type: UIStateChangeType;

    /**
     * 变更路径
     */
    path: string;
}

/**
 * UIState值变更
 */
interface UIStateValueChange extends UIStateChange {

    /**
     * 变更类型
     */
    type: UIStateChangeType.ValueChange;

    /**
     * 旧值
     */
    oldValue: any;

    /**
     * 新值
     */
    newValue: any;
}


export { UIStateChangeType, UIStateChange, UIStateValueChange };
