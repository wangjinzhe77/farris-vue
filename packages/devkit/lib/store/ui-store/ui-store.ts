import { Module } from '../../module/index';
import { Store } from '../store/index';
import { UIStoreConfig } from './configs/index';
import { UIStateChangeType, UIStateValueChange } from './uistate-change';

/**
 * UIState定义
 */
class UIState {
}

/**
 * UIStore定义
 */
class UIStore<S extends UIState> extends Store<S> {

    /**
     * 构造函数
     */
    constructor(module: Module) {
        super(module);
    }

    /**
     * 初始化
     */
    public init(config: UIStoreConfig): void {
        super.init(config);
        this.initState();
    }

    /**
     * 初始化属性
     */
    private initState(): void {
        this.state = {} as any;
    }

    /**
     * 根据name获取状态值
     */
    public getValue(name: string): any {
        return this.state[name];
    }

    /**
     * 根据path获取状态值
     */
    public getValueByPath(path: string): any {
        this.checkPath(path);

        const pathSegs = path.split('/').slice(1);
        const value = pathSegs.reduce((currentValue: any, pathSeg: string) => {
            if (!currentValue) {
                throw new Error(`Can not read property of empty object( reading ${pathSeg})`);
            }
            return currentValue[pathSeg];
        }, this.state);

        return value;
    }

    /**
     * 根据name设置状态值
     */
    public setValue(name: string, newValue: any): void {
        const oldValue = this.getValue(name);
        if (oldValue === newValue) {
            return;
        }

        this.state[name] = newValue;

        // 触发变更
        const change: UIStateValueChange = {
            type: UIStateChangeType.ValueChange,
            path: `/${name}`,
            oldValue,
            newValue
        };
        this.triggerChange(change as any);
    }

    /**
     * 根据path设置状态值
     */
    public setValueByPath(path: string, newValue: any): void {
        const oldValue = this.getValue(path);
        if (oldValue === newValue) {
            return;
        }

        const pathSegs = path.split('/').slice(1);
        pathSegs.reduce((previouseValue: any, currentPathSeg: string, currentPathSegIndex: number) => {
            if (!previouseValue) {
                throw new Error(`Can not read property of empty object( reading ${currentPathSeg})`);
            }
            previouseValue[currentPathSeg] = newValue;
        }, this.state);

        // 触发变更
        const change: UIStateValueChange = {
            type: UIStateChangeType.ValueChange,
            path: path,
            oldValue,
            newValue
        };
        this.triggerChange(change as any);
    }

    /**
     * 检查路径合法性
     */
    private checkPath(path: string): void {
        if (!path || typeof path !== 'string') {
            throw new Error('The path must be a non-empty string.');
        }

        if (path.startsWith('/') === false) {
            throw new Error('The path must be started with slash.');
        }

        if (path.length < 1) {
            throw new Error('It must contain at least one character other than a slash');
        }

        const pathSegs = path.split('/').slice(1);
        if (!pathSegs) {
            throw new Error('The path must be a non-empty string.');
        }

        const emptyPathSegIndex = pathSegs.findIndex((pathItem: string) => {
            return !pathItem;
        });
        if (emptyPathSegIndex > -1) {
            throw new Error('The path can not contain empty segment ');
        }
    }
}

export { UIState, UIStore };