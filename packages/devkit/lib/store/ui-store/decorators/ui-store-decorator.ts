import { Type, TypeDecorator, makeTypeMetadataDecorator } from '../../../common/index';
import { StoreMeta } from '../../store/index';
import { UIState } from '../ui-store';

/**
 * UIStore元数据名称
 */
const UI_STORE_META_NAME = 'UI_STORE_META';

/**
 * UIStore元数据
 */
interface UIStoreMeta extends StoreMeta {
    stateType: Type<UIState>
}

/**
 * UIStore元数据工厂
 */
const uiStoreMetaFactory = (meta: UIStoreMeta): UIStoreMeta => {
    return meta;
};

/**
 * UIStore装饰器工厂类型
 */
interface UIStoreDecoratorFactory {
    (meta: UIStoreMeta): TypeDecorator;
}

/**
 * UIStore装饰器工厂类型
 */
function FdUIStore(meta: UIStoreMeta) {
    const decoratorFactory: UIStoreDecoratorFactory = makeTypeMetadataDecorator(UI_STORE_META_NAME, uiStoreMetaFactory);
    return decoratorFactory(meta);
}

export { UI_STORE_META_NAME, UIStoreMeta, uiStoreMetaFactory, UIStoreDecoratorFactory, FdUIStore };