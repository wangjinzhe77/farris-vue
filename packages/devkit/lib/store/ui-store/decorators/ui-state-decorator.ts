
import { PropDecorator, makePropMetadataDecorator } from '../../../common/index';

/**
 * UI属性元数据名称
 */
const UISTATE_META_NAME = 'UISTATE_META';

/**
 * UI属性元数据
 */
interface UIStateMeta {

    /**
     * 属性名称
     */
    name?: string;
}

/**
 * UI属性工厂方法
 */
const uiStateMetaFactory = (meta: UIStateMeta) => {
    return meta;
}

/**
 * UI属性装饰器类型
 */
interface UIStateMetaDecoratorFactory {
    (meta?: UIStateMeta): PropDecorator
}

/**
 * UI属性饰器工厂
 */
const FdUIState: UIStateMetaDecoratorFactory = makePropMetadataDecorator(UISTATE_META_NAME, uiStateMetaFactory);

export { UIStateMeta, UISTATE_META_NAME, uiStateMetaFactory, UIStateMetaDecoratorFactory, FdUIState };
