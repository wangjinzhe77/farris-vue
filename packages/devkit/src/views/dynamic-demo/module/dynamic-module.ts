import { ModuleConfig, Module } from '../../../../lib/index';
import { serviceProviders } from './services/index';
import { DynamicRepository } from './dynamic-repository';

export const dynamicModuleConfig: ModuleConfig = {

    id: 'dynamic-module',

    viewModels: [
        {
            id: 'dynamic-viewmodel',
            entityStore: 'dynamic-entity-store',
            uiStore: 'dynamic-ui-store',
            stateMachine: 'dynamic-state-machine',
            repository: 'dynamic-repository',
            providers: serviceProviders,
            commands: [
                {
                    name: 'load',
                    params: [],

                },
                {
                    name: 'save',
                    params: []
                },
                {
                    name: 'test',
                    params: []
                }
            ],
            commandHandlers: [
                {
                    commandName: 'load',
                    tasks: [
                        {
                            name: 'load',
                            service: 'LoadDataService',
                            method: 'load',
                            params: [],
                            links: [
                                { nextTaskName: 'refresh', conditions: true }
                            ]
                        },
                        {
                            name: 'refresh',
                            service: 'SimpleCustomService',
                            serviceUrl: '/apps/services/dist/simple-custom-service.js',
                            method: 'refresh',
                            params: [],
                            links: [
                            ]

                        }
                    ]
                },
                {
                    commandName: 'save',
                    tasks: [
                        {
                            name: 'validate',
                            service: 'ValidationService',
                            method: 'validate',
                            params: [],
                            links: [
                                { nextTaskName: 'save', conditions: true }
                            ]
                        },
                        {
                            name: 'save',
                            service: 'SaveDataService',
                            method: 'save',
                            params: [],
                            links: []
                        }
                    ]
                },
                {
                    commandName: 'test',
                    tasks: [
                        {
                            name: 'testA',
                            service: 'TestService',
                            method: 'testA',
                            params: [],
                            links: [
                                { nextTaskName: 'testB', conditions: '{COMMAND~/results/testA}===true && "{DATA~/id}"==="0001"' },
                                { nextTaskName: 'testC', conditions: '{COMMAND~/results/testA}===true && "{DATA~/id}"==="0002"' }

                            ]
                        },
                        {
                            name: 'testB',
                            service: 'TestService',
                            method: 'testB',
                            params: [],
                            links: [
                                { nextTaskName: 'testD', conditions: '{COMMAND~/results/testB}===true' }
                            ]
                        },
                        {
                            name: 'testC',
                            service: 'TestService',
                            method: 'testC',
                            params: [],
                            links: [
                                { nextTaskName: 'testD', conditions: '{COMMAND~/results/testC}===true' }
                            ]
                        },
                        {
                            name: 'testD',
                            service: 'TestService',
                            method: 'testD',
                            params: [],
                            links: [
                                {
                                    nextTaskName: 'testE',
                                    conditions: [
                                        { lBraceket: '', source: '{COMMAND~/results/testD}', compare: '===', target: true, rBraceket: '', relation: 'and', },
                                        { lBraceket: '', source: '{DATA~/#{dynamic-viewmodel}/id}', compare: '===', target: '0001', rBraceket: '', relation: '', }
                                    ]
                                },
                                {
                                    nextTaskName: 'testF',
                                    conditions: [
                                        { lBraceket: '', source: '{COMMAND~/results/testD}', compare: '===', target: true, rBraceket: '', relation: 'and', },
                                        { lBraceket: '', source: '{DATA~/#{dynamic-viewmodel}/id}', compare: '===', target: '0002', rBraceket: '', relation: '', }
                                    ]
                                }
                            ]
                        },
                        {
                            name: 'testE',
                            service: 'TestService',
                            method: 'testE',
                            params: [],
                            links: []
                        },
                        {
                            name: 'testF',
                            service: 'TestService',
                            method: 'testF',
                            params: [],
                            links: []
                        }
                    ]
                }
            ]
        }
    ],

    repositories: [
        {
            id: 'dynamic-repository',
            type: DynamicRepository,
            deps: [Module],
            isDynamic: true,
            entityStore: 'dynamic-entity-store'
        }
    ],

    entityStores: [
        {
            id: "dynamic-entity-store",
            state: {
                entity: {
                    idKey: 'id',
                    fields: [
                        { name: 'id', type: 'Primitive' },
                        { name: 'code', type: 'Primitive' },
                        { name: 'name', type: 'Primitive' },
                        { name: 'status', type: 'Primitive' },
                    ]
                }
            }
        }
    ],

    uiStores: [
        {
            id: 'dynamic-ui-store',
            state: {
                props: [
                    { name: 'action' },
                    { name: 'allowDelete' }
                ]
            }
        },
    ],

    stateMachines: [
        {
            id: 'dynamic-state-machine',
            state: {
                renderStates: [
                    {
                        name: 'canEdit',
                        conditions: [
                            { lBraceket: '', source: '{STATEMACHINE~/#{dynamic-viewmodel}/currentPageState}', compare: '===', target: 'viewState', rBraceket: '', relation: 'and', },
                            { lBraceket: '', source: '{DATA~/#{dynamic-viewmodel}/status}', compare: '===', target: 'Billing', rBraceket: '', relation: '', }
                        ]
                    },
                    {
                        name: 'canSave',
                        conditions: [
                            { lBraceket: '', source: '{STATEMACHINE~/#{dynamic-viewmodel}/currentPageState}', compare: '===', target: 'editState', rBraceket: '', relation: 'and', },
                            { lBraceket: '', source: '{DATA~/#{dynamic-viewmodel}/status}', compare: '===', target: 'Billing', rBraceket: '', relation: '', }

                        ]
                    },

                    {
                        name: 'canSubmit',
                        conditions: [
                            { lBraceket: '', source: '{STATEMACHINE~/#{dynamic-viewmodel}/currentPageState}', compare: '===', target: 'viewState', rBraceket: '', relation: 'and', },
                            { lBraceket: '', source: '{DATA~/#{dynamic-viewmodel}/status}', compare: '===', target: 'Billing', rBraceket: '', relation: '', }

                        ]
                    },
                    {
                        name: 'canCancelSubmit',
                        conditions: [
                            { lBraceket: '', source: '{STATEMACHINE~/#{dynamic-viewmodel}/currentPageState}', compare: '===', target: 'viewState', rBraceket: '', relation: 'and', },
                            { lBraceket: '', source: '{DATA~/#{dynamic-viewmodel}/status}', compare: '===', target: 'SubmitApproval', rBraceket: '', relation: '', }

                        ]
                    },

                    {
                        name: 'canDelete',
                        conditions: [
                            { lBraceket: '', source: '{STATEMACHINE~/#{dynamic-viewmodel}/currentPageState}', compare: '===', target: 'viewState', rBraceket: '', relation: 'and', },
                            { lBraceket: '', source: '{UISTATE~/#{dynamic-viewmodel}/allowDelete}', compare: '===', target: true, rBraceket: '', relation: '', }

                        ]
                    },

                    {
                        name: 'canInput',
                        conditions: [
                            { lBraceket: '', source: '{STATEMACHINE~/#{dynamic-viewmodel}/currentPageState}', compare: '===', target: 'editState', rBraceket: '', relation: 'and', },
                            { lBraceket: '', source: '{DATA~/#{dynamic-viewmodel}/status}', compare: '===', target: 'Billing', rBraceket: '', relation: '', }

                        ]
                    },
                ]
            },
            initPageState: 'viewState',
            pageStates: [
                { name: 'viewState' },
                { name: 'editState' }
            ],
            transitionActions: [
                { name: 'editAction', transitionTo: 'editState' },
                { name: 'saveAction', transitionTo: 'viewState' }
            ]
        }
    ]
};
