import { Entity, Repository, Module } from '../../../../lib/index';

/**
 * 动态远程实体仓库
 */
class DynamicRepository extends Repository<Entity> {

    /**
     * 实体数据
     */
    private entityDatas = [
        { id: '0001', code: 'LiuYi', name: '刘一', status: 'Billing' },
        { id: '0002', code: 'ChenEr', name: '陈二', status: 'SubmitApproval' },
        { id: '0003', code: 'LiuYi', name: '张三', status: 'Billing'},
    ];

    /**
     * 构造函数
     */
    constructor(module: Module) {
        super(module);
    }

    /**
     * 获取实体集合
     */
    public getEntites(): Promise<Entity[]> {
        const entities = this.buildEntites(this.entityDatas);
        const entitesPromise = new Promise<Entity[]>((resolve, reject) => {
            setTimeout(() => {
                resolve(entities);
            });
        });

        return entitesPromise;
    }

    /**
     * 获取实体
     */
    public getEntityById(id: string): Promise<Entity | null> {
        const targetEntityData = this.entityDatas.find((entityData) => {
            return entityData.id === id;
        });
        const entityPromise = new Promise<Entity>((resolve, reject) => {
            setTimeout(() => {
                if (targetEntityData) {
                    const targetEntity = this.buildEntity(targetEntityData);
                    resolve(targetEntity);
                } else {
                    resolve(null);
                }
            });
        });

        return entityPromise;
    }
}

export { DynamicRepository };
