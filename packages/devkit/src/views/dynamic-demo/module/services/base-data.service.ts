import { ViewModelState, ViewModel, Entity, EntityState, EntityStore, UIState, UIStore } from '../../../../../lib/index';
import { DynamicRepository } from '../dynamic-repository';

/**
 * 基础数据服务
 */
class BaseDataService {

    /**
     * 视图模型
     */
    protected viewModel: ViewModel<ViewModelState>;

    /**
     * 实体状态仓库
     */
    protected entityStore: EntityStore<EntityState<Entity>>;

    /**
    * UI状态仓库
    */
    protected uiStore: UIStore<UIState>;

    /**
     * 远程实体仓库
     */
    protected repository: DynamicRepository;

    /**
    * 构造函数
    */
    constructor(viewModel: ViewModel<ViewModelState>) {
        this.viewModel = viewModel;
        this.entityStore = viewModel.entityStore;
        this.uiStore = viewModel.uiStore;
        this.repository = viewModel.repository as DynamicRepository;
    }

    /**
     * 获取服务实例
     */
    public getService<T>(token: any, defaultValue?: any): T {
        const injector = this.viewModel.getInjector();
        return injector.get<T>(token, defaultValue);
    }
}

export { BaseDataService };
