import { StaticProvider, ViewModel } from '../../../../../lib/index';
import { LoadDataService } from './load-data.service';
import { ValidationService } from './validation.service';
import { SaveDataService } from './save-data.service';
import { TestService } from './test.service';

const serviceProviders: StaticProvider[] = [
    { provide: LoadDataService, useClass: LoadDataService, deps: [ ViewModel ] },
    { provide: ValidationService, useClass: ValidationService, deps: [ ViewModel ] },
    { provide: SaveDataService, useClass: SaveDataService, deps: [ ViewModel ] },
    { provide: TestService, useClass: TestService, deps: [ ViewModel ] }
];

export { serviceProviders };
