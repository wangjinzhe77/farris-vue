import { ViewModelState, ViewModel } from '../../../../../lib/index';
import { BaseDataService } from './base-data.service';

/**
 * 数据保存服务
 */
class TestService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     * 测试方法
     */
    public test() {
        console.log('----------test----------');

        this.viewModel.entityStore.setValueByPath('/name', 'new-name');
        this.uiStore.setValue('/action', 'new-action');
    }

    public testA() {
        console.log('----------testA----------');
        return true;
    }

    public testB() {
        const promise = new Promise((resolve) => {
            console.log('----------testB----------');
            resolve(true);
        });

        return promise;
    }

    public testC() {
        console.log('----------testC----------');
        return true;
    }

    public testD() {
        console.log('----------testD----------');
        return true;
    }

    public testE() {
        console.log('----------testE----------');
        return true;
    }

    public testF() {
        console.log('----------testF----------');
        return true;
    }
}

export { TestService };
