import { FdUIStore, FdUIState, UIState, UIStore } from '../../../../lib/index';

class StaticUIState extends UIState {

    @FdUIState()
    public action!: string;
}

@FdUIStore({
    id: 'static-ui-store',
    stateType: StaticUIState
})
class StaticUIStore extends UIStore<StaticUIState> {
}

export { StaticUIState, StaticUIStore};
