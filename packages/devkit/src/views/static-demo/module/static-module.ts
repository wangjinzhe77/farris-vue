import { Injector, Module, FdModule } from '../../../../lib/index';
import { StaticAPIProxy, StaticRepository } from './static-repository';
import { StaticEntityStore } from './static-entity-store';
import { StaticUIStore } from './static-ui-store';
import { StaticViewModel } from './static-viewmodel';

@FdModule({
    id: 'static-module',
    deps: [Injector],
    providers: [
        { provide: StaticAPIProxy, useClass: StaticAPIProxy, deps: [] }
    ],

    entityStores: [StaticEntityStore],
    uiStores: [StaticUIStore],
    repositorys: [StaticRepository],

    viewModels: [StaticViewModel]
})
export class StaticModule extends Module {
}
