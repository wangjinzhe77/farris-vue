import { CommandContext, FdCommandHandler, CommandHandler } from '../../../../../lib/index';
import { ValidationService, SaveDataService } from '../services/index';

/**
 * SaveHandler定义
 */
@FdCommandHandler({
    commandName: 'save',
    deps: [ValidationService, SaveDataService]
})
class SaveHandler extends CommandHandler {

    /**
     * 对应命令名称
     */
    public commandName = 'save';

    /**
     * 构造函数
     */
    constructor(
        private validationService: ValidationService,
        private saveDataService: SaveDataService
    ) {
        super();
    }

    /**
     * 执行方法
     */
    public execute(context: CommandContext) {
        this.validationService.validate();
        this.saveDataService.save();
    }
}

export { SaveHandler };
