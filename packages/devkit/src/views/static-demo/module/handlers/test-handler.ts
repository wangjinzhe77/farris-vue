import { CommandContext, FdCommandHandler, CommandHandler } from '../../../../../lib/index';
import { TestService } from '../services/index';

/**
 * SaveHandler定义
 */
@FdCommandHandler({
    commandName: 'test',
    deps: [ TestService]
})
class TestHandler extends CommandHandler {

    /**
     * 对应命令名称
     */
    public commandName = 'test';

    /**
     * 构造函数
     */
    constructor(private testService: TestService) {
        super();
    }

    /**
     * 执行方法
     */
    public execute(context: CommandContext) {
        this.testService.test();
    }
}

export { TestHandler };
