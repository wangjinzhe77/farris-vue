import { FdEntityStore, FdField, EntityState, EntityStore, Entity } from '../../../../lib/index';

class StaticEntity extends Entity {

    @FdField({
        primary: true
    })
    public id!: string;

    @FdField()
    public code!: string;

    @FdField()
    public name!: string;
}

class StaticEntityState extends EntityState<StaticEntity>  {
}

@FdEntityStore({
    id: 'static-entity-store',
    stateType: StaticEntityState,
    entityType: StaticEntity
})
class StaticEntityStore  extends EntityStore<StaticEntityState> {
}

export { StaticEntity, StaticEntityState, StaticEntityStore };
