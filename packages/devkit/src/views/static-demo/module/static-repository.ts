import { FdRepository, RepositoryConfig, Repository, Module } from '../../../../lib/index';
import { StaticEntity } from './static-entity-store';

/**
 * 取数接口代理
 */
class StaticAPIProxy {

    /**
     * 基路径
     */
    private baseUrl: string;


    /**
     * 实体数据
     */
    private entityDatas = [
        { id: '0001', code: 'LiuYi', name: '刘一' },
        { id: '0001', code: 'ChenEr', name: '陈二' },
        { id: '0001', code: 'LiuYi', name: '张三' },
    ];

    /**
     * 构造函数
     */
    constructor() {
    }

    /**
     * 初始化
     */
    public init(baseUrl: string) {
        this.baseUrl = baseUrl;
        console.log(this.baseUrl);
    }

    /**
     * 获取实体数据集合
     */
    public getEntityDatas(): Promise<any[]> {
        const promise = new Promise<any[]>((resolve, reject) => {
            setTimeout(() => {
                resolve(this.entityDatas);
            }, 1000);
        });

        return promise;
    }


    /**
     * 获取实体数据
     */
    public getEntityData(id: string): Promise<any> {
        const targetEntityData = this.entityDatas.find((entityData) => {
            return entityData.id === id;
        });

        const promise = new Promise<any>((resolve, reject) => {
            setTimeout(() => {
                if (targetEntityData) {
                    resolve(targetEntityData);
                } else {
                    resolve(null);
                }
            }, 1000);
        });

        return promise;
    }
}

/**
 * 静态远程实体仓库
 */
@FdRepository({
    id: 'static-repository',
    deps: [Module, StaticAPIProxy],
    entityStore: 'static-entity-store',
    baseUrl: 'http://127.0.0.1/api/entity'
})
class StaticRepository extends Repository<StaticEntity> {

    /**
     * 数据服务
     */
    private apiProxy: StaticAPIProxy;

    /**
     * 构造函数
     */
    constructor(module: Module, dataService: StaticAPIProxy) {
        super(module);
        this.apiProxy = dataService;
    }

    /**
     * 初始化
     */
    public init(config: RepositoryConfig) {
        super.init(config);
        this.apiProxy.init(config.baseUrl);
    }

    /**
     * 获取实体集合
     */
    public getEntites(): Promise<StaticEntity[]> {
        const entitesPromise = this.apiProxy.getEntityDatas().then((entityDatas: any) => {
            const entities = this.buildEntites(entityDatas);
            return entities;
        });

        return entitesPromise;
    }

    /**
     * 获取实体
     */
    public getEntityById(id: string): Promise<StaticEntity | null> {
        const entityPromise = this.apiProxy.getEntityData(id).then((entityData) => {
            if (!entityData) {
                return null;
            }

            const entity = this.buildEntity(entityData);
            return entity;
        });

        return entityPromise;
    }
}

export { StaticAPIProxy, StaticRepository };
