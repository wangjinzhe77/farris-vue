import { ViewModelState, ViewModel } from '../../../../../lib/index';
import { BaseDataService } from './base-data.service';

/**
 * 验证服务
 */
class ValidationService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }


    /**
     * 验证
     */
    public validate() {
        const promise = new Promise((resolve, reject) => {
            setTimeout(() => {
                console.log('validated ...');
                resolve(true);
            }, 2000);
        });

        return promise;
    }
}

export { ValidationService };
