import { ViewModelState, ViewModel, Entity } from '../../../../../lib/index';
import { BaseDataService } from './base-data.service';

/**
 * 数据加载服务
 */
class LoadDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     * 加载数据
     */
    public load(): Promise<Entity[]> {
        const loadPromise: Promise<Entity[]> = this.repository.getEntites().then((entities) => {
            this.entityStore.loadEntities(entities);
            return entities;
        });

        return loadPromise;
    }
}

export { LoadDataService };
