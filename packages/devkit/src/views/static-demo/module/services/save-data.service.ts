import { ViewModelState, ViewModel } from '../../../../../lib/index';
import { BaseDataService } from './base-data.service';

/**
 * 数据保存服务
 */
class SaveDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     * 保存
     */
    public save() {
        const promise = new Promise((resolve, reject) => {
            setTimeout(() => {
                console.log('saved ...');
                resolve(true);
            }, 1000);
        });

        return promise;
    }
}

export { SaveDataService };
