import { ViewModelState, ViewModel } from '../../../../../lib/index';
import { BaseDataService } from './base-data.service';

/**
 * 数据保存服务
 */
class TestService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     * 测试方法
     */
    public test() {
        console.log('----------test----------');
        this.viewModel.entityStore.setValueByPath('/name', 'new-name');
        this.uiStore.setValue('/action', 'new-action');
    }
}

export { TestService };
