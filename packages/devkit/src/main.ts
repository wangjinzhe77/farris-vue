import { createApp } from 'vue';
import { createDevkit } from '../lib/index';
import App from './app.vue';

const app = createApp(App);
const devkit = createDevkit({
  providers: []
});
app.use(devkit);
app.mount('#app');
