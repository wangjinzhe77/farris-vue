# farris-themebuilder-site
主题生成站点Vue版
1. 支持在线修改变量调整样式，在线验证样式
2. 支持下载样式压缩包

#### 启动后端服务
1. 后端服务位置在farris-vue/packages/f-theme-editor-project/farris-themebuilder-service
2. 按照farris-vue工程的说明，安装依赖的包
3. 安装farris-themebuilder-service的说明，安装依赖的包
4. npm run farrisTheme，启动后端服务
```
npm run farrisTheme
```
#### 启用站点
1. npm install 安装依赖
```
npm install
```
2. npm run serve 启动服务
```
npm run serve
```
