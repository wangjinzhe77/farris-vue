import axios from "axios";
import {storeInstance} from '@/store/init';
import { useLoadingStore } from "@/store/index";
// 创建一个axios 实例
const api = axios.create({
  baseURL: "http://localhost:3000", // 基准地址
  timeout: 20000 // 超时时间
});

// const loadingStore = useLoadingStore(storeInstance);

// 添加请求拦截器
api.interceptors.request.use(
  function (config) {
    // loadingStore.updateState(true);
    // ---todo 在发送请求之前，loading开始
    return config;
  },
  function (error) {
    
    // loading 结束
    // ---todo 对请求错误给出错误提示框 notify
    return Promise.reject(error);
  }
);

// 添加响应拦截器
api.interceptors.response.use(
  function (response) {
    // loading 结束
    // 对响应数据做点什么
    // loadingStore.updateState(false);
    return response["data"];
  },
  function (error) {    
    // loading 结束
    // ---todo 给出错误提示框 notify
    return Promise.reject(error);
  }
);

// 最后导出
export default api;
