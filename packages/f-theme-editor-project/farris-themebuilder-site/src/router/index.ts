import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import FTLayout from "../views/Layout";
import FTPreview from "../views/Preview";
import FTPreviewPage from "../components/preview-page/index";
import FTPreviewWidget from "../components/preview-widget/index";
// import TestView from "../views/TestView";

const routes: Array<RouteRecordRaw> = [
    {
        path: "",
        name: "home",
        component: FTLayout
    },
    {
        path: "",
        name: "preview",
        component: FTPreview,
        children: [
            {
                path: "/preview-page/:themeName/:themeColorSize",
                component: FTPreviewPage
            },
            {
                path: "/preview-widget/:themeName/:themeColorSize",
                component: FTPreviewWidget
            }
        ]
    }
    // {
    //   path: "/about",
    //   name: "about",
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () =>
    //     import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
    // },
    // {
    //   path: "/test",
    //   name: "test",
    //   component: TestView
    // }
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
});
router.beforeEach((to, from, next) => {
    console.log("beforeEach");
    console.log(to);
    next();
});

router.afterEach((to, from) => {
    console.log("afterEach");
    console.log(from);
});
export default router;
