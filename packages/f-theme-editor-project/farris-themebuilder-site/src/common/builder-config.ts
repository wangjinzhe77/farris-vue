import { ExportedItem } from './exported-item';
/**
 * 变更主题变量，发起到服务器端生成主题的的结构
 */
export interface BuilderConfig {
    baseTheme?: string;
    makeSwatch?: boolean;
    widgets?: Array<string>;
    outputColorScheme?: string;
    outColorScheme?: string;
    items?: Array<ExportedItem>;
    noClean?: boolean;
    data?: string;
    inputFile?: string;
    outputFile?: string;
    assetsBasePath?: string;
    themeName?: string;
    colorScheme?: string;
    removeExternalResources?: boolean;
    bootstrapVersion?: number;
}
