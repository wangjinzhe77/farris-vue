export interface MetaItem {
    Name: string;
    Key: string;
    Value: string;
    Type?: string;
    TypeValues?: string;
    TypeValuesArray?: string[];
    Items?: MetaItem[];
}
export interface MetaValueChange{
    changeValue:any;// 修改后的值
    controlKey:string;// 控件编号
    variableType:string;// 变量修改的类型，基础变量还是高级变量
}
export interface ThemeMetaItem{
    [themeName:string]: Array<MetaItem>
}
