import { ThemeConfig } from './theme';
import { MetaItem } from './meta-item';
/**
 * 定义主题元数据项
 */
export interface Metadata extends MetaItemThemeType {
    baseParameters?: string[];
    themes: ThemeConfig[];
    version: string;
}
/**
 * 用于过typescript
 */
export type MetaItemThemeType = {
    generic: MetaItem[];
    material?: MetaItem[];
    farris?:MetaItem[];
}
