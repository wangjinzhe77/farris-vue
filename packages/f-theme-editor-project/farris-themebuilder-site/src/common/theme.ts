/**
 * 主题信息接口类
 */
export class Theme {
    name: string ='';
    colorScheme: string ='';
}

export interface ThemeConfig extends Theme {
    themeId: number;
    text: string;
    group: string;
}

export interface ThemeSetting{
    themeColor:Array<ThemeSettingItem>;
    themeSize:Array<ThemeSettingItem>;
}
export interface ThemeSettingItem{
    type: string;  
    color?:string; 
    size?:string; 
    name: string;    
}
