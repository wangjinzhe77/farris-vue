/**
 * 编译主题后服务器端返回的内容接口类
 */
export interface BuilderResult {
    css: string;
    compiledMetadata: { [key: string]: string };
    swatchSelector: string;
    unusedWidgets: string[];
    widgets: string[];
    version: string;
}
