import { themeMetadataUtils } from "./theme-metadata";

export function ExportUtils() {
  const { exportCSS } = themeMetadataUtils();
  /**
   * 导出css时候调用此处
   * @param outColorScheme
   * @param swatch
   * @param widgets
   * @param assetsBasePath
   * @param removeExternalResources
   */
  const exportThemeCSS = (themeName = ""): Promise<string> => {
    return exportCSS(themeName);
  };
  return{
    exportThemeCSS
  };
}
