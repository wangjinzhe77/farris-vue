export function StorageThemeCSSUtils() {
  /**
   * 保存指定主题的CSS
   * @param themeName
   */
  const saveCSS = (css: string, themeName: string) => {
    themeName=defaultName(themeName);
    clearCSS(themeName);
    localStorage.setItem(themeName, css);
    // 标记有哪些样式主题需要存储
    const themeTypes = localStorage.getItem("themeTypes");
    if (!themeTypes) {
      localStorage.setItem("themeTypes", JSON.stringify([themeName]));
    } else {
      const themeTypesArray = JSON.parse(themeTypes);
      if (themeTypesArray.indexOf(themeName)==-1) {
        themeTypesArray.push(themeName);
        localStorage.setItem("themeTypes", JSON.stringify(themeTypesArray));
      }
    }
  };
  /**
   * 获取指定样式
   * @param themeName 
   * @returns 
   */
  const getCSS = (themeName: string) => {

    return localStorage.getItem(defaultName(themeName));
  };
  /**
   * 删除指定样式
   * @param themeName 
   */
  const clearCSS = (themeName: string = "") => {
    let themeTypesArray = [];
    if (themeName) {      
      themeTypesArray=[themeName];
    }else{      
      const themeTypes = localStorage.getItem("themeTypes");
      if (themeTypes) {
        // 类型
        themeTypesArray = JSON.parse(themeTypes);
      } else {
        // 数组
        localStorage.setItem("themeTypes", "[]");
      }
      // 移除
      localStorage.removeItem("themeTypes");
    }
    themeTypesArray.forEach((name: string) => {
      localStorage.removeItem(name);
    });
  };
  /**
   * 设置指定
   * @param themeName 
   * @returns 
   */
  const defaultName=(themeName:string)=>{
    return themeName?themeName:"farris";
  };
  return {
    saveCSS,
    getCSS,
    clearCSS
  };
}
