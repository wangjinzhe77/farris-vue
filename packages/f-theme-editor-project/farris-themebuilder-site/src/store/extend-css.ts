import { defineStore } from "pinia";
/**
 * 处理在线编辑的css--------------暂时没有用到
 */
export const useExtendCSSStore = defineStore("extendCSS", {
  state: () => {
    return {
      css: "",
    };
  },
  actions: {
    updateCSS(newCSS: string) {
      // this.show =loadingState;
      this.css = newCSS;
    }
  },
});
