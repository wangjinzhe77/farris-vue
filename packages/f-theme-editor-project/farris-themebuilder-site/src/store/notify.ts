import { defineStore } from "pinia";

export const useNotifyStore = defineStore("notifyStore", {
  state: () => {
    const flag: any = null;
    return {
      show: false,
      type: "info",
      text: "正在进行中...",
      flag: flag
    };
  },
  actions: {
    updateState(showOrhide: boolean) {
      this.show = showOrhide;
    },
    updateType(type = "", text = "", timer = 5000) {
      this.show = true;
      this.type = type || "info";
      this.text = text || "正在进行中";
      if (this.flag) {
        clearTimeout(this.flag);
      }
      if (timer > 0) {
        this.flag = setTimeout(() => {
          this.show = false;
        }, timer);
      }
    }
  }
});
