import {
  defineComponent,
  toRefs
} from "vue";
import {
  EditorChildProps,
  editorChildProps,
  useEditorChildEvent
} from "./editor.types";

export default defineComponent({
  name: "FTInput",
  props: editorChildProps,
  inheritAttrs: false,
  emits: ["update:modelValue", "input", "change"],
  setup(props:EditorChildProps, ctx) {
    const { modelValue, disable, placeholder } = toRefs(props);
    //  const input = shallowRef<HTMLInputElement>();
    const { onInput, onChange } = useEditorChildEvent(ctx);

    return () => (
      <input class="form-control"   
        value={modelValue.value}
        disabled={disable.value}
        placeholder={placeholder.value}
        type="text"
        onInput={onInput}
        onChange={onChange}
      />
    );
  }
});
