import { ExtractPropTypes, PropType, Ref, SetupContext } from "vue";
import { MetaItem } from "../../common/meta-item";

const editorChildProps = {
    /**
 * 组件值，字符串或者数组
 * { type: String, default: false },
  { type: Array<String>, default: [] },
  author: {
      validator: function (value: Person) {
        return typeof value.name === "string" && typeof value.age === "number"
      }
    }
 */
    modelValue: { type: String, default: "" },
    /**
   * 禁用组件
   */
    disable: { type: Boolean, default: false },
    /**
   * 禁用组件
   */
    placeholder: { type: String, default: '' },
    /**
   * 查询
   */
    searchText: { type: String, default: '' },
};

type EditorChildProps = ExtractPropTypes<typeof editorChildProps>;
/**
 * 属性
 */
const editorProps = {
    /**
   * 组件值，字符串或者数组
   * { type: String, default: false },
    { type: Array<String>, default: [] },
   */
    item: { type: Object as () => MetaItem, default: null },
    /**
   * 禁用组件
   */
    disable: { type: Boolean, default: false },
    /**
   * 查询文本
   */
    searchText: { type: String, default: '' },
    /**
   * 记变量变更类型，如果是：快速配置 'rapidSettings', 普通配置 ''
  *  快速配置有特殊的颜色关系，需要在服务器端进行特殊处理，所以此处有差异
   */
    variableType: { type: String, default: '' }
};
type EditorProps = ExtractPropTypes<typeof editorProps>;

interface UseEditorChildEvent {
    onInput: (e: Event) => void;
    onChange: (e: Event) => void;
    // onClear: () => void;
}

/**
 * 事件
 * @param props
 * @param context
 * @param modelValue
 * @returns
 */
function useEditorChildEvent(ctx: any): UseEditorChildEvent {
    const onInput = (e: Event) => {
        ctx.emit("input", (e.target as HTMLInputElement).value);
        ctx.emit("update:modelValue", (e.target as HTMLInputElement).value);
    };

    const onChange = (e: Event) => {
        console.log("值改变" + (e.target as HTMLInputElement).value);
        ctx.emit("change", (e.target as HTMLInputElement).value);
    };
    return {
        onInput,
        onChange,
    };
}

export { EditorProps, editorProps, useEditorChildEvent, EditorChildProps, editorChildProps };
