import { defineComponent } from "vue";

export default defineComponent({
  name: "FTPageBulkEdit",
  setup() {
    return () => (<div class="f-page f-page-is-managelist f-page-has-scheme">
    <farris-section class="f-section-scheme f-section-in-managelist f-section">
      <div class="f-section-content">
        <app-scheme>
          <div class="query-solution">
            <div class="farris-panel position-relative" style="outline: none">
              <div class="solution-header">
                <div class="btn-group mr-3 dropdown">
                  <div class="solution-header-title" aria-haspopup="true">
                    <span style="
                      max-width: 288px;
                      overflow: hidden;
                      text-overflow: ellipsis;
                      white-space: nowrap;
                    " title="默认筛选方案">
                      默认筛选方案
                    </span>
                    <span class="f-icon f-accordion-expand"></span>
                  </div>
                  <div class="dropdown-menu solution-header-title-menu">
                    <div class="solution-header-title-menu-inner">
                      <div class="solution-header-title-menu-arrow"></div>
                      <li class="dropdown-item solution-header-dropdown-item solution-header-dropdown-item-active">
                        <span class="solution-header-dropdown-item-title" title="默认筛选方案">
                          默认筛选方案
                        </span>
                        <span class="solution-header-dropdown-item-tip" style="visibility: hidden">
                          默认
                        </span>
                      </li>
                      <li class="solution-header-dropdown-item-btns">
                        <span class="dropdown-item-btn">另存为</span>
                        <span class="dropdown-item-btn">管理</span>
                      </li>
                    </div>
                  </div>
                </div>
                <div class="solution-action">
                  <div class="btn-group">
                    <button class="btn btn-primary" type="button">
                      筛选
                    </button>
                  </div>
                  <div class="icon-group">
                    <span class="icon-group-remove" title="清空">
                      <span class="f-icon f-icon-remove"></span>
                    </span>
                    <span class="divide"></span>
                    <span class="icon-group-setup" title="配置">
                      <span class="f-icon f-icon-home-setup"></span>
                    </span>
                    <span class="icon-group-packup" title="收起">
                      <span class="f-icon f-icon-packup"></span>
                    </span>
                  </div>
                </div>
              </div>
              <div>
                <div
                  class="row f-utils-flex-row-wrap farris-form farris-form-controls-inline condition-with-fixed condition-div"
                  id="querycondition">
                  <div class="col-12 col-md-6 col-xl-3 col-el-2">
                    <div class="farris-group-wrap" id="2ff3dde3-bb7e-46f9-822a-ee9f14663049">
                      <div class="form-group farris-form-group common-group">
                        <label class="col-form-label" title="订单编号">
                          <span class="farris-label-text">订单编号</span>
                        </label>
                        <div class="farris-input-wrap">
                          <input class="form-control" placeholder="输入编号" type="text" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6 col-xl-3 col-el-2">
                    <div class="farris-group-wrap" id="3ff3dde3-bb7e-46f9-822a-ee9f14663049">
                      <div class="form-group farris-form-group common-group">
                        <label class="col-form-label" title="单据日期">
                          <span class="farris-label-text">单据日期</span>
                        </label>
                        <div class="farris-input-wrap">
                          <farris-datepicker placeholder="选择日期" _nghost-dka-c14="" ng-reflect-placeholder="选择日期"
                            class="f-cmp-datepicker f-cmp-inputgroup">
                            <div class="input-group f-state-editable" ng-reflect-ng-class="[object Object]">
                              <input autocomplete="off" class="form-control f-utils-fill " name="farris-date"
                                style="padding-right: 4px" type="text" ng-reflect-name="farris-date"
                                ng-reflect-is-disabled="false" ng-reflect-model="" ng-reflect-popover=""
                                ng-reflect-triggers="hover" ng-reflect-container="body" ng-reflect-pop-active="false"
                                ng-reflect-show-action="show2" placeholder="选择日期" />
                              <div class="input-group-append f-cmp-iconbtn-wrapper" style="position: relative">
                                <span class="input-group-text input-group-clear datepicker-clear"
                                  style="visibility: hidden">
                                  <i class="f-icon modal_close"></i>
                                </span>
                                <button class="btn f-cmp-iconbtn" type="button">
                                  <span class="f-icon f-icon-date"></span>
                                </button>
                              </div>
                            </div>
                          </farris-datepicker>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-6 col-xl-3 col-el-2">
                    <div class="farris-group-wrap" id="4ff3dde3-bb7e-46f9-822a-ee9f14663049">
                      <div class="form-group farris-form-group common-group">
                        <label class="col-form-label" title="客户名称">
                          <span class="farris-label-text">客户名称</span>
                        </label>
                        <div class="farris-input-wrap">
                          <farris-lookup-grid class="f-cmp-inputgroup">
                            <input-group class="f-cmp-inputgroup ">
                              <div class="lookupbox input-group f-state-editable">
                                <input class="form-control f-utils-fill text-left " name="input-group-value" type="text"
                                  placeholder="请选择" autocomplete="off" />
                                <div class="input-group-append" ng-reflect-klass="input-group-append"
                                  ng-reflect-ng-class="[object Object]">
                                  <span class="input-group-text input-group-clear" style="width: 24px; display: none">
                                    <i class="f-icon modal_close"></i>
                                  </span>
                                  <span class="input-group-text">
                                    <i class="f-icon f-icon-lookup"></i>
                                  </span>
                                </div>
                              </div>
                            </input-group>
                          </farris-lookup-grid>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </app-scheme>
      </div>
    </farris-section>
    <app-header _nghost-dka-c12="">
      <div _ngcontent-dka-c12="" class="f-page-header">
        <nav _ngcontent-dka-c12="" class="f-page-header-base">
          <div _ngcontent-dka-c12="" class="f-title">
            <span _ngcontent-dka-c12="" class="f-title-icon f-text-orna-manage">
              <i _ngcontent-dka-c12="" class="f-icon f-icon-page-title-administer" ng-reflect-klass="f-icon"
                ng-reflect-ng-class="[object Object]"></i>
            </span>
            <h4 _ngcontent-dka-c12="" class="f-title-text">
              销售列表
            </h4>
          </div>
          <f-response-toolbar class="f-toolbar col-7 f-response-toolbar position-relative">
            <div class="response-toolbar-hidden-element"></div>
            <div class="d-flex flex-nowrap justify-content-end" ng-reflect-klass="d-flex flex-nowrap"
              ng-reflect-ng-class="[object Object]">
              <div class="d-inline-block f-response-content" style="white-space: nowrap">
                <button class="btn f-rt-btn btn-primary f-btn-ml disabled" type="button" ng-reflect-klass="btn f-rt-btn"
                  ng-reflect-ng-class="btn-primary" id="toolbar-001">
                  新增
                </button>
                <button class="btn f-rt-btn btn-secondary f-btn-ml" type="button" ng-reflect-klass="btn f-rt-btn"
                  ng-reflect-ng-class="btn-secondary" id="toolbar-002">
                  编辑
                </button>
                <button class="btn f-rt-btn btn-secondary f-btn-ml" type="button" ng-reflect-klass="btn f-rt-btn"
                  ng-reflect-ng-class="btn-secondary" id="toolbar-003">
                  查看
                </button>
                <button class="btn f-rt-btn btn-secondary f-btn-ml disabled" type="button" ng-reflect-klass="btn f-rt-btn"
                  ng-reflect-ng-class="btn-secondary" id="toolbar-004">
                  删除
                </button>
                <button class="btn f-rt-btn btn-secondary f-btn-ml" type="button" ng-reflect-klass="btn f-rt-btn"
                  ng-reflect-ng-class="btn-secondary" id="toolbar-005">
                  关闭
                </button>
              </div>
            </div>
          </f-response-toolbar>
        </nav>
      </div>
    </app-header>
    <div class="f-page-main">
      <div class="f-struct-wrapper f-utils-fill-flex-column">
        <farris-section class="f-section-grid f-section-in-managelist f-section f-section-fill" _nghost-dka-c10=""
          ng-reflect-enable-maximize="false" ng-reflect-fill="true">
          <div class="f-section-header" ng-reflect-klass="f-section-header">
            <div class="f-title">
              <h4 class="f-title-text"></h4>
            </div>
          </div>
  
          <div class="f-section-extend" ng-reflect-klass="f-section-extend">
            <div class="f-section-extend-gridfilter">
              <span>
                <i class="f-icon f-icon-info"></i>
              </span>
              <p>
                已选择 <span class="text-info">2</span> 项
              </p>
              <p>支付申请总计：36.4 万</p>
              <button class="btn btn-link">清空</button>
            </div>
          </div>
          <div class="f-section-content" style="position: relative">
            <farris-datagrid class="f-component-grid f-datagrid-full" style="position: relative">
              <div class="f-datagrid f-datagrid-rowhover f-datagrid-strip f-datagrid-sm FarrisDataGrid1106c8fcb6"
                style="outline: 0px; width: 2118px; height: 910px" id="FarrisDataGrid1106c8fcb6">
                <datagrid-header class="d-flex flex-fill" style="position: relative; width: 100%; height: 35px; top: 0px"
                  ng-reflect-height="35" ng-reflect-columns="[object Object],[object Object"
                  ng-reflect-enable-drag-column="false" ng-reflect-wrap-header="false"
                  ng-reflect-enable-filter-row="false" ng-reflect-scrollbar-mode="auto" ng-reflect-fast="false"
                  ng-reflect-columns-group="[object Object]">
                  <div class="f-datagrid-header" style="width: 100%; min-height: 35px">
                    <div class="f-datagrid-header-fixed-left" style="height: 35px; width: 36px">
                      <table cellpadding="0" cellspacing="0" style="height: 100%">
                        <colgroup>
                          <col width="36px" />
                        </colgroup>
                        <thead>
                          <tr class="f-datagrid-header-row" ng-reflect-group-name="left">
                            <td class="f-datagrid-header-cell f-datagrid-cell-rownumber" rowspan="1">
                              <div class="f-datagrid-header-cell-content" style="padding: 0px; width: 36px">
                                <span class="f-datagrid-header-cell-title" style="width: 100%">
                                  序号
                                </span>
                              </div>
                            </td>
                          </tr>
                        </thead>
                      </table>
                    </div>
                    <div class="f-datagrid-header-center" style="
                      position: absolute;
                      height: 100%;
                      left: 36px;
                      width: 1700px;
                      transform: translate3d(0px, 0px, 0px);
                    ">
                      <table cellpadding="0" cellspacing="0" style="height: 100%">
                        <colgroup>
                          <col width="100px" />
  
                          <col width="200px" />
  
                          <col width="200px" />
  
                          <col width="200px" />
  
                          <col width="200px" />
  
                          <col width="200px" />
  
                          <col width="200px" />
  
                          <col width="100px" />
  
                          <col width="100px" />
  
                          <col width="200px" />
                        </colgroup>
                        <thead>
                          <tr class="f-datagrid-header-row">
                            <td class="f-datagrid-header-cell f-datagrid-header-cell-resize"
                              ng-reflect-col="[object Object]" title="加急">
                              <div class="f-datagrid-header-cell-content d-flex flex-row" style="width: 100px">
                                <span class="f-datagrid-header-cell-title" style="width: 80px">
                                  加急{" "}
                                </span>
                              </div>
                              <span class="column-resize-bar"></span>
                            </td>
  
                            <td class="f-datagrid-header-cell f-datagrid-header-cell-resize"
                              ng-reflect-col="[object Object]" title="单据日期">
                              <div class="f-datagrid-header-cell-content d-flex flex-row" style="width: 200px">
                                <span class="f-datagrid-header-cell-title" style="width: 180px">
                                  单据日期{" "}
                                </span>
                              </div>
                              <span class="column-resize-bar"></span>
                            </td>
  
                            <td class="f-datagrid-header-cell f-datagrid-header-cell-sort f-datagrid-header-cell-resize"
                              ng-reflect-col="[object Object]" title="订单编号">
                              <div class="f-datagrid-header-cell-content d-flex flex-row" style="width: 200px">
                                <span class="f-datagrid-header-cell-title" style="width: 100%">
                                  订单编号{" "}
                                </span>
                                <span class="f-datagrid-sort" style="cursor: pointer"></span>
                              </div>
                              <span class="column-resize-bar"></span>
                            </td>
  
                            <td class="f-datagrid-header-cell f-datagrid-header-cell-sort f-datagrid-header-cell-resize"
                              ng-reflect-col="[object Object]" title="客户名称">
                              <div class="f-datagrid-header-cell-content d-flex flex-row" style="width: 200px">
                                <span class="f-datagrid-header-cell-title" style="width: 100%">
                                  客户名称{" "}
                                </span>
                                <span class="f-datagrid-sort" style="cursor: pointer"></span>
                              </div>
                              <span class="column-resize-bar"></span>
                            </td>
  
                            <td class="f-datagrid-header-cell f-datagrid-header-cell-resize"
                              ng-reflect-col="[object Object]" title="客户别名">
                              <div class="f-datagrid-header-cell-content d-flex flex-row" style="width: 200px">
                                <span class="f-datagrid-header-cell-title" style="width: 180px">
                                  客户别名{" "}
                                </span>
                              </div>
                              <span class="column-resize-bar"></span>
                            </td>
  
                            <td class="f-datagrid-header-cell f-datagrid-header-cell-resize"
                              ng-reflect-col="[object Object]" title="部门">
                              <div class="f-datagrid-header-cell-content d-flex flex-row" style="width: 200px">
                                <span class="f-datagrid-header-cell-title" style="width: 180px">
                                  部门{" "}
                                </span>
                              </div>
                              <span class="column-resize-bar"></span>
                            </td>
  
                            <td class="f-datagrid-header-cell f-datagrid-header-cell-resize"
                              ng-reflect-col="[object Object]" title="业务员">
                              <div class="f-datagrid-header-cell-content d-flex flex-row" style="width: 200px">
                                <span class="f-datagrid-header-cell-title" style="width: 180px">
                                  业务员{" "}
                                </span>
                              </div>
                              <span class="column-resize-bar"></span>
                            </td>
  
                            <td class="f-datagrid-header-cell f-datagrid-header-cell-sort f-datagrid-header-cell-resize"
                              ng-reflect-col="[object Object]" title="总金额">
                              <div class="f-datagrid-header-cell-content d-flex flex-row" style="width: 100px">
                                <span class="f-datagrid-header-cell-title" style="width: 100%">
                                  总金额{" "}
                                </span>
                                <span class="f-datagrid-sort" style="cursor: pointer"></span>
                              </div>
                              <span class="column-resize-bar"></span>
                            </td>
  
                            <td class="f-datagrid-header-cell f-datagrid-header-cell-resize"
                              ng-reflect-col="[object Object]" title="币种">
                              <div class="f-datagrid-header-cell-content d-flex flex-row" style="width: 100px">
                                <span class="f-datagrid-header-cell-title" style="width: 80px">
                                  币种{" "}
                                </span>
                              </div>
                              <span class="column-resize-bar"></span>
                            </td>
  
                            <td class="f-datagrid-header-cell f-datagrid-header-cell-resize"
                              ng-reflect-col="[object Object]" title="管理">
                              <div class="f-datagrid-header-cell-content d-flex flex-row" style="width: 200px">
                                <span class="f-datagrid-header-cell-title" style="width: 180px">
                                  管理{" "}
                                </span>
                              </div>
                              <span class="column-resize-bar"></span>
                            </td>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </datagrid-header>
                <datagrid-body >
                  <div class="f-datagrid-body"  style="height: 798px; top: 38px; width: 2118px">
                    <div class="ps f-datagrid-body-wheel-area" style="width: 100%; height: 798px; padding-right: 0px"
                      ng-reflect-config="[object Object]">
                      <div class="f-datagrid-bg" ng-reflect-auto-height="false" ng-reflect-wheel-height="332"
                        style="height: 332px; width: 1736px">
                        <div style="width: 100%; height: 0px"></div>
                        <div class="f-datagrid-table f-datagrid-body-fixed-left" style="
                          z-index: 999;
                          width: 36px;
                          transform: translate3d(0px, 0px, 0px);
                        ">
                          <fixed-left-rows>
                            <table cellpadding="0" cellspacing="0" class="f-datagrid-rows" style="width: 100%">
                              <colgroup>
                                <col style="width: 36px" />
                              </colgroup>
                              <tbody>
                                <tr class="f-datagrid-body-row fixed-left-row" id="row_SQ201805010001_fixedleft">
                                  <td class="f-datagrid-cell f-datagrid-cell-rownumber" style="width: 36px">
                                    <div class="f-datagrid-cell-content">
                                      1
                                    </div>
                                  </td>
                                </tr>
                                <tr class="f-datagrid-body-row fixed-left-row" id="row_SQ201805010001_fixedleft">
                                  <td class="f-datagrid-cell f-datagrid-cell-rownumber" style="width: 36px">
                                    <div class="f-datagrid-cell-content">
                                      2
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </fixed-left-rows>
                        </div>
  
                        <div class="f-datagrid-table f-datagrid-body-center"
                          style="position: relative; width: 1700px; left: 36px">
                          <datagrid-rows ng-reflect-start-row- ng-reflect-data="[object Object],[object Object"
                            ng-reflect-columns="[object Object],[object Object">
                            <table cellpadding="0" cellspacing="0" class="f-datagrid-rows" row-dblclick=""
                              style="border: 0; width: 100%">
                              <colgroup>
                                <col width="100px" />
                                <col width="200px" />
                                <col width="200px" />
                                <col width="200px" />
                                <col width="200px" />
                                <col width="200px" />
                                <col width="200px" />
                                <col width="100px" />
                                <col width="100px" />
                                <col width="200px" />
                              </colgroup>
                              <tbody id="normal-rows-tbody_FarrisDataGrid1106c8fcb6">
                                <tr class="f-datagrid-body-row " id="row_SQ201805010001">
                                  <td class="f-datagrid-cell cell-readonly"
                                    id="row_SQ201805010001_0-farris-datagrid-column_0_0" style="vertical-align: middle">
                                    <grid-body-cell>
                                      <div class="f-datagrid-cell-content cell-text-align__farris-datagrid-column_0_0">
                                        <div class="text-center">
                                          <span class="f-icon f-icon-flag_urgent text-warning"></span>
                                        </div>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" id="row_SQ201805010001_0-DDRQ"
                                    style="vertical-align: middle">
                                    <grid-body-cell ng-reflect-width="200">
                                      <div class="f-datagrid-cell-content cell-text-align__DDRQ">
                                        <span class="cell-text-box grid-cell-nowrap">
                                          2018-04-21
                                        </span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" id="row_SQ201805010001_0-DDBH"
                                    style="vertical-align: middle">
                                    <grid-body-cell ng-reflect-width="200">
                                      <div class="f-datagrid-cell-content cell-text-align__DDBH">
                                        <span class="f-pretend-link mr-2">
                                          SQ201805010001
                                        </span>
                                        <span class="badge badge-arrow-left-info">
                                          通过
                                        </span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" id="row_SQ201805010001_0-KHMC"
                                    style="vertical-align: middle">
                                    <grid-body-cell ng-reflect-width="200">
                                      <div class="f-datagrid-cell-content cell-text-align__KHMC">
                                        <span class="cell-text-box grid-cell-nowrap">
                                          青峰集团
                                        </span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" id="row_SQ201805010001_0-KHBM"
                                    style="vertical-align: middle">
                                    <grid-body-cell ng-reflect-width="200">
                                      <div class="f-datagrid-cell-content cell-text-align__KHBM">
                                        <span class="cell-text-box grid-cell-nowrap">
                                          青峰集团
                                        </span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" id="row_SQ201805010001_0-BM"
                                    style="vertical-align: middle">
                                    <grid-body-cell ng-reflect-width="200">
                                      <div class="f-datagrid-cell-content cell-text-align__BM">
                                        <span class="cell-text-box grid-cell-nowrap">
                                          代理销售
                                        </span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" id="row_SQ201805010001_0-YWY"
                                    style="vertical-align: middle">
                                    <grid-body-cell ng-reflect-width="200">
                                      <div class="f-datagrid-cell-content cell-text-align__YWY">
                                        <span class="cell-text-box grid-cell-nowrap">
                                          张艳
                                        </span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" id="row_SQ201805010001_0-ZJE"
                                    style="vertical-align: middle">
                                    <grid-body-cell>
                                      <div class="f-datagrid-cell-content cell-text-align__ZJE">
                                        <span class="cell-text-box grid-cell-nowrap">
                                          10000
                                        </span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" id="row_SQ201805010001_0-BZ"
                                    style="vertical-align: middle">
                                    <grid-body-cell>
                                      <div class="f-datagrid-cell-content cell-text-align__BZ">
                                        <span class="cell-text-box grid-cell-nowrap">
                                          人民币
                                        </span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" align="center"
                                    id="row_SQ201805010001_0-farris-datagrid-column_0_9" style="vertical-align: middle">
                                    <grid-body-cell ng-reflect-width="200" ng-reflect-last-column="true">
                                      <div class="f-datagrid-cell-content cell-text-align__farris-datagrid-column_0_9">
                                        <span class="cell-text-box grid-cell-nowrap"></span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                </tr>
                                <tr class="f-datagrid-body-row " id="row_SQ201805010001">
                                  <td class="f-datagrid-cell cell-readonly"
                                    id="row_SQ201805010001_1-farris-datagrid-column_0_0" style="vertical-align: middle">
                                    <grid-body-cell>
                                      <div class="f-datagrid-cell-content cell-text-align__farris-datagrid-column_0_0">
                                        <div class="text-center">
                                          <span class="f-icon f-icon-flag_urgent text-warning"></span>
                                        </div>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" id="row_SQ201805010001_1-DDRQ"
                                    style="vertical-align: middle">
                                    <grid-body-cell ng-reflect-width="200">
                                      <div class="f-datagrid-cell-content cell-text-align__DDRQ">
                                        <span class="cell-text-box grid-cell-nowrap">
                                          2018-05-01
                                        </span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" id="row_SQ201805010001_1-DDBH"
                                    style="vertical-align: middle">
                                    <grid-body-cell ng-reflect-width="200">
                                      <div class="f-datagrid-cell-content cell-text-align__DDBH">
                                        <span class="f-pretend-link mr-2">
                                          SQ201805010001
                                        </span>
                                        <span class="badge badge-arrow-left-success">
                                          通过
                                        </span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" id="row_SQ201805010001_1-KHMC"
                                    style="vertical-align: middle">
                                    <grid-body-cell ng-reflect-width="200">
                                      <div class="f-datagrid-cell-content cell-text-align__KHMC">
                                        <span class="cell-text-box grid-cell-nowrap">
                                          金得利快餐
                                        </span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" id="row_SQ201805010001_1-KHBM"
                                    style="vertical-align: middle">
                                    <grid-body-cell ng-reflect-width="200">
                                      <div class="f-datagrid-cell-content cell-text-align__KHBM">
                                        <span class="cell-text-box grid-cell-nowrap">
                                          客户别名
                                        </span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" id="row_SQ201805010001_1-BM"
                                    style="vertical-align: middle">
                                    <grid-body-cell ng-reflect-width="200">
                                      <div class="f-datagrid-cell-content cell-text-align__BM">
                                        <span class="cell-text-box grid-cell-nowrap">
                                          食品销售部
                                        </span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" id="row_SQ201805010001_1-YWY"
                                    style="vertical-align: middle">
                                    <grid-body-cell ng-reflect-width="200">
                                      <div class="f-datagrid-cell-content cell-text-align__YWY">
                                        <span class="cell-text-box grid-cell-nowrap">
                                          夏文冬
                                        </span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" id="row_SQ201805010001_1-ZJE"
                                    style="vertical-align: middle">
                                    <grid-body-cell>
                                      <div class="f-datagrid-cell-content cell-text-align__ZJE">
                                        <span class="cell-text-box grid-cell-nowrap">
                                          10000
                                        </span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" id="row_SQ201805010001_1-BZ"
                                    style="vertical-align: middle">
                                    <grid-body-cell>
                                      <div class="f-datagrid-cell-content cell-text-align__BZ">
                                        <span class="cell-text-box grid-cell-nowrap">
                                          人民币
                                        </span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                  <td class="f-datagrid-cell cell-readonly" align="center"
                                    id="row_SQ201805010001_1-farris-datagrid-column_0_9" style="vertical-align: middle">
                                    <grid-body-cell ng-reflect-width="200" ng-reflect-last-column="true">
                                      <div class="f-datagrid-cell-content cell-text-align__farris-datagrid-column_0_9">
                                        <span class="cell-text-box grid-cell-nowrap"></span>
                                      </div>
                                    </grid-body-cell>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </datagrid-rows>
                        </div>
                      </div>
                    </div>
                  </div>
                </datagrid-body>
                <datagrid-footer>
                  <div class="f-datagrid-footer f-datagrid-footer-bottom" component-template="" ng-reflect-column=""
                    style="bottom: 40px; height: 36px">
                    <div class="f-grid-total-row">
                      <div class="f-title">当页合计</div>
                      <div class="f-content">
                        <dl class="f-grid-total-detail">
                          <dt>未还款金额：</dt>
                          <dd>¥ 15,236.00</dd>
                          <dt>申请金额：</dt>
                          <dd class="text-warning h3">¥ 35,236.00</dd>
                        </dl>
                      </div>
                    </div>
                  </div>
                </datagrid-footer>
                <datagrid-pager >
                  <div class="f-datagrid-pager" style="opacity: 1">
                    <pagination-controls >
                      <pagination-template ng-reflect-id="FarrisDataGrid1106c8fcb6-pager" ng-reflect-max-size="7">
                        <div class="pagination-container">
                          <div class="pager-overlay-container" style="position: fixed; z-index: 999999"></div>
                          <ul class="ngx-pagination pagination pager-viewmode-default" role="navigation" style="
                            position: relative;
                            align-items: center;
                            justify-content: start;
                          " aria-label="Pagination">
                            <li class="pagination-message text-truncate d-flex ml-auto flex-fill"
                              style="min-width: 70px; justify-content: right">
                              <div class="flex-fill pager-pos-right" style="left: 0px; position: relative">
                                <div class="d-flex flex-row" style="align-items: center"></div>
                              </div>
                              <div class="text-truncate">
                                <span class="pg-message-text">共</span>
                                <b class="pg-message-total">11</b>
                                <span class="pg-message-text">条</span>
                              </div>
                            </li>
                            <li class="pagination-pagelist">
                              <div class="dropup dropdown-right pg-pagelist"
                                ng-reflect-klass="dropup dropdown-right pg-pagel" ng-reflect-ng-class="[object Object]">
                                <div class="pg-pagelist-info">
                                  <span class="pagelist-text">每页</span>
                                  <b class="cur-pagesize">20</b>
                                  <span class="pagelist-text">条</span>
                                  <i class="f-icon f-icon-dropdown"></i>
                                </div>
                                <div class="dropdown-menu" style="margin-bottom: -1px" ng-reflect-klass="dropdown-menu"
                                  ng-reflect-ng-class="[object Object]">
                            <li class="dropdown-item" ng-reflect-klass="dropdown-item"
                              ng-reflect-ng-class="[object Object]">
                              <span>10</span>
                            </li>
                            <li class="dropdown-item active" ng-reflect-klass="dropdown-item"
                              ng-reflect-ng-class="[object Object]">
                              <span>20</span>
                            </li>
                            <li class="dropdown-item" ng-reflect-klass="dropdown-item"
                              ng-reflect-ng-class="[object Object]">
                              <span>30</span>
                            </li>
                            <li class="dropdown-item" ng-reflect-klass="dropdown-item"
                              ng-reflect-ng-class="[object Object]">
                              <span>50</span>
                            </li>
                            <li class="dropdown-item" ng-reflect-klass="dropdown-item"
                              ng-reflect-ng-class="[object Object]">
                              <span>100</span>
                            </li>
                        </div>
                  </div>
                  </li>
                  <li class="page-item disabled">
                    <span class="page-link">
                      <span class="f-icon f-page-first"></span>
                    </span>
                  </li>
                  <li class="page-item disabled">
                    <span class="page-link">
                      <span class="f-icon f-page-pre"></span>
                    </span>
                  </li>
  
                  <li class="page-item current">
                    <span class="page-link">1</span>
                  </li>
  
                  <li class="page-item disabled">
                    <span class="page-link">
                      <span class="f-icon f-page-next"></span>
                    </span>
                  </li>
                  <li class="page-item disabled">
                    <span class="page-link">
                      <span class="f-icon f-page-last"></span>
                    </span>
                  </li>
                  </ul>
              </div>
              </pagination-template>
              </pagination-controls>
          </div>
          </datagrid-pager>
          <div class="f-datagrid-resize-bg"></div>
          <div class="f-datagrid-resize-proxy"></div>
          <div style="
                  position: absolute;
                  left: -1000px;
                  top: -1000px;
                  visibility: hidden;
                "></div>
      </div>
      </farris-datagrid>
    </div>
    </farris-section>
  </div>
  </div>
  </div>);
  }
});
