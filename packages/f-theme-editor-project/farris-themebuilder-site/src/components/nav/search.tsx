import { SetupContext, defineComponent, ref, toRefs } from "vue";
import { useFilterMetaItemsStore } from "@/store/index";
import { debounce } from "lodash-es";
/**
 *  未添加
 */
export default defineComponent({
  name: "FTNavSearch",
  props: {
    variableName: { type: String, default: "基础变量" }
  },
  setup(props, ctx: SetupContext) {
    const filterMenu = useFilterMetaItemsStore();
    // 是否进入了查询
    const inSearch = ref(false);
    // 查询文本
    const searchText = ref("");
    // 变量分类名称
    const { variableName } = toRefs(props);
    // 切换查询状态
    const changeSearchState = (ev: MouseEvent) => {
      inSearch.value = !inSearch.value;
      searchText.value = "";
      if (inSearch.value) {
        // 确定在查询中，聚焦
        // setTimeout(() => this.searchInput.nativeElement.focus(), 100);
      } else {
        filterMenu.menuFilter(searchText.value);
      }
      ev.stopPropagation();
    };
    /**
     * 查询文本修改
     * @param ev
     */
    const searchTextChange = debounce(() => {
      filterMenu.menuFilter(searchText.value);
    }, 500);

    return () => (
      <div class="search-header-container">
        <span class="search-header--menu">
          <i class="f-icon f-icon-arrow-chevron-left"></i>
        </span>
        <span class="search-header--title">{variableName.value}</span>
        <div
          class={[
            "search-header--search",
            inSearch.value ? "f-state-expand" : ""
          ]}
          onClick={(ev) => {
            ev.stopPropagation();
          }}
        >
          <span class="search-icon">
            <i class="f-icon f-icon-search"></i>
          </span>
          <input
            type="text"
            class="form-control"
            placeholder="查询"
            onInput={searchTextChange}
            v-model={searchText.value}
          />
        </div>
        <div
          class="search-header--icons"
          onClick={(ev) => changeSearchState(ev)}
        >
          {inSearch.value ? (
            <span class="search-icon">
              <i class="f-icon f-icon-close"></i>
            </span>
          ) : (
            <span class="search-icon">
              <i class="f-icon f-icon-search"></i>
            </span>
          )}
        </div>
      </div>
    );
  }
});
