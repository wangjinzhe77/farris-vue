import router from "@/router";
import { defineComponent, ref } from "vue";
import FTIframe from "../iframe/iframe";

export default defineComponent({
  name: "FTLayoutMain",
  props: {},
  components: {
    "ft-iframe": FTIframe
  },
  setup() {
    const previewDatas = [
      {
        code: "preview-page",
        name: "模板"
      },
      {
        code: "preview-widget",
        name: "组件"
      }
    ];    
    // 记录预览类型
    const previewType = ref(previewDatas[0].code);
    const changePreviewType = (typeCode: string) => {
      previewType.value=typeCode;
    };
    // const goTo = () => {
    //   router.push({ path: "/preview-widget", query: {} }); //path写跳转的路由，同样可以传参
    // };
    // const goTo2 = () => {
    //   router.push({ path: "/preview-page", query: {} }); //path写跳转的路由，同样可以传参
    // };
    return () => (
      <div class="tool-layout-main">
        <div class="layout-main--header btn-group">
          {previewDatas.map((item) => {
            return (
              <button
                class={[
                  "btn btn-secondary",
                  item.code == previewType.value ? "f-state-selected" : ""
                ]}
                onClick={(e) => changePreviewType(item.code)}
              >
                {item.name}
              </button>
            );
          })}
        </div>      
        <div class="layout-main--content">
          <ft-iframe previewType={previewType}></ft-iframe>
        </div>
      </div>
    );
  }
});
