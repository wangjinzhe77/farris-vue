import { defineComponent, ref} from "vue";
import FTNavSearch from "../nav/search";
import FTNavRapidSettings from "../nav/rapid-settings";
import FTNavHeader from "../nav/header";
/**
 * 1. 未添加 monaco相关代码
 * 2. farris=list-nav 未使用,现在不支持拖拽
 */
export default defineComponent({
  name: "FTLayoutNav",
  components:{
    'ft-nav-search':FTNavSearch,
    'ft-nav-rapid-settings':FTNavRapidSettings,
    'ft-nav-header':FTNavHeader
  },
  setup() {
    // 配置模式
    const settingMode = [
      {
        code: "rapid",
        name: "快速配置",
        icon: "nav-icon-rapid"
      },
      // {
      //   code: 'advanced',
      //   name: '高级配置',
      //   icon: 'nav-icon-detailed'
      // },
      {
        code: "extend",
        name: "自定义",
        icon: "nav-icon-css"
      }
    ];
    // 标记当前模式
    const activeModeCode = ref(settingMode[0].code);
    /**
     * 标签点击
     * @param code
     */
    const navTabClickHandler = (code: string) => {
      console.log("点击Tab："+code);
      activeModeCode.value = code;
    };

    return () => (
      <div class="tool-layout-nav">
        <div class="layout-nav--tab">
          <ul class="tab--list">
            {settingMode.map((modeItem) => {
              return (
                <li
                  class={[
                    "tab--item",
                    "tab--item-" + modeItem.code,
                    modeItem.code == activeModeCode.value ? "active" : ""
                  ]}
                  title="tabItem.name"
                  v-on:click={() => navTabClickHandler(modeItem.code)}
                >
                  <i class={[modeItem.icon]}></i>
                </li>
              );
            })}
          </ul>  
        </div>               
        <div  class="f-list-nav f-list-nav-left">
            <div class="f-list-nav-in">
            <div class="f-list-nav-main">
                <div class="f-list-nav-header">
                    <ft-nav-header></ft-nav-header>
                    <ft-nav-search></ft-nav-search>
                </div>
                <div class="f-list-nav-content">
                    内容                    
                    <ft-nav-rapid-settings></ft-nav-rapid-settings>
                </div>
            </div>
            </div>
          </div>
      </div>
    );
  }
});

