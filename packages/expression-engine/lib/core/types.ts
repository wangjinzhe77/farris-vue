/**
 * 表达式扩展方法
 */
export type ExpressionCommonFunction = (...args: any) => any | void;

export type TypedFunction = (...args: any[]) => any;
/**
 * 表达式上下文
 */
export interface IExpressionContext {
    arguments: { [prop: string]: any };
    contexts: { [prop: string]: any };
    set(key: string, value: any): void;
}
/**
 * 表达式
 */
export interface IExpression {
    eval(context: IExpressionContext): any;
}
/**
* 表达式引擎
*/
export interface IExpressionEngine {
    eval(expression: string): any;
    getContext(): IExpressionContext;
    addFun(name: string, func: ExpressionCommonFunction): void;
}
/**
 * 表达式编译器
 */
export interface ICompiler {
    compile(expr: string, context: IExpressionContext): Function;
}
export interface IEvaler {
    eval(expr: string, context: IExpressionContext): TypedFunction;
}
