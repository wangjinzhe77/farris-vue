/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable no-new-func */
import { ICompiler, IExpressionContext } from "../core/index";
import { DefaultFunctions } from "../function/index";

export class Compiler implements ICompiler {
    public compile(expr: string, context?: IExpressionContext): Function {
        const contexts = context && context.contexts || {};
        const args = contexts.arguments || {};
        const signature = Object.keys(args).join(',');
        const defaultFunction = new DefaultFunctions(context);
        contexts.DefaultFunction = defaultFunction;
        const hasReturnStatement = expr.match(/([\s\r\n{;}]+return)|(^return)\s+/g);
        if (!hasReturnStatement) {
            expr = `return ${expr}`;
        }
        const scopeNames = Object.getOwnPropertyNames(contexts);
        const scopeVariable = `__scope__${new Date().valueOf()}`;
        return new Function(scopeVariable, `
        ${scopeNames.map((key: string) => `var ${key} = ${scopeVariable}['${key}'];`).join('\r\n')}
        return function anonymous(${signature}) {
            try{ \n${expr}\n }catch(e){console.error(e);}
        };`);
    }
}
