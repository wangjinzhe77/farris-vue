import { ExpressionCommonFunction, IExpressionContext, IExpressionEngine } from "../core/index";

export abstract class AbstractExpressionEngine implements IExpressionEngine {
    protected context: IExpressionContext | undefined;

    constructor(context?: IExpressionContext) {
        this.context = context;
    }

    abstract eval(expression: string): any;

    abstract getContext(): IExpressionContext;

    abstract addFun(name: string, func: ExpressionCommonFunction): void;

}
