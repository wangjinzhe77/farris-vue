import { ExpressionContext } from "../context/index";
import { ExpressionCommonFunction, IExpressionContext } from "../core/index";
import { Expression } from "../expression/index";
import { AbstractExpressionEngine } from "./abstract_expression_engine";

export class ExpressionEngine extends AbstractExpressionEngine {
    protected context: IExpressionContext | undefined;

    constructor(context?: IExpressionContext) {
        super(context);
    }

    /**
     * 计算表达式
     * @param expr 表达式
     * @returns
     */
    public eval(expr: string): any {
        const expression = new Expression(expr);
        return expression.eval(this.getContext());
    }

    /**
     * 获取表达式上下文
     * @returns
     */
    public getContext(): IExpressionContext {
        if (!this.context) {
            this.context = new ExpressionContext();
        }
        return this.context;
    }

    /**
     * 添加自定义方法
     * @param name 方法名
     * @param func 方法体
     */
    public addFun(name: string, func: ExpressionCommonFunction): void {
        if (name && typeof func === 'function') {
            this.getContext().set(name, func);
        }
    }
}
