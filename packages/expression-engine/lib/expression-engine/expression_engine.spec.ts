/* eslint-disable max-len */
import { ExpressionEngine } from "./expression_engine";
import { DefaultFunctions } from '../function';

describe('ExpressionEngine', () => {
    let service: ExpressionEngine;
    beforeEach(() => { service = new ExpressionEngine(); });

    it('# 2+3*4 = 14', () => {
        const result = service.eval("2+3*4");
        expect(result).toBe(14);
    });

    it('# 判断语句', () => {
        expect(service.eval("var x=1;var y;if(x>0){y=x;}else{y=0};return y;")).toBe(1);
    });

    it('# 带入参的判断语句', () => {
        const context = service.getContext();
        context.set("x", 10);
        context.set("y", undefined);
        expect(service.eval("if(x>0){y=x;}else{y=0} return y;")).toBe(10);
    });

    it('# 带对象类型的入参', () => {
        const context = service.getContext();
        context.set("debug", console);
        expect(service.eval("debug.log(\"success!\");return 10;")).toBe(10);
    });

    it('# DefaultFunction.Sum', () => {
        const context = service.getContext();
        context.set("x", [1, 10, 100, 1000]);
        expect(service.eval("DefaultFunction.Sum(x)")).toBe(1111);
    });

    it('# Entity.a+Entity.b', () => {
        const context = service.getContext();
        context.set("Entity", { a: 1, b: 5 });
        expect(service.eval("if(Entity.a+Entity.b > 10){return 'Tom';}")).toBe(undefined);
    });
    it('# DefaultFunction.SumByProp("Entity.items","score")', () => {
        const context = service.getContext();
        context.set("Entity", { items: [{ score: 100 }, { score: 99 }, { score: 1 }, { score: 88 }, { score: 12 }] });
        expect(service.eval("DefaultFunction.SumByProp(\"Entity.items\",\"score\")")).toBe(300);
    });
    it('# DefaultFunction.CountByProp("Entity.items","score")', () => {
        const context = service.getContext();
        context.set("Entity", { items: [{ score: 100 }, { score: 99 }, { score: 1 }, { score: 88 }, { score: 12 }] });
        expect(service.eval("DefaultFunction.CountByProp(\"Entity.items\",\"score\")")).toBe(5);
    });
    it('# DefaultFunction.AvgByProp("Entity.items","score")', () => {
        const context = service.getContext();
        context.set("Entity", { items: [{ score: 100 }, { score: 99 }, { score: 1 }, { score: 88 }, { score: 12 }, { score: 1 }] });
        expect(service.eval("DefaultFunction.AvgByProp(\"Entity.items\",\"score\")")).toBe(50.17);
    });
    it('# DefaultFunction.MaxByProp("Entity.items","score")', () => {
        const context = service.getContext();
        context.set("Entity", { items: [{ score: 100 }, { score: 99 }, { score: 1 }, { score: 88 }, { score: 12 }, { score: 1 }, { score: "0" }] });
        expect(service.eval("DefaultFunction.MaxByProp(\"Entity.items\",\"score\")")).toBe(100);
    });
    it('# DefaultFunction.MinByProp("Entity.items","score")', () => {
        const context = service.getContext();
        context.set("Entity", { items: [{ score: 100 }, { score: 99 }, { score: 88 }, { score: 12 }, { score: null }] });
        expect(service.eval("DefaultFunction.MinByProp(\"Entity.items\",\"score\")")).toBe(12);
    });
    it('# DefaultFunction.IsExistRecord("Entity.items","score",88)', () => {
        const context = service.getContext();
        context.set("Entity", { items: [{ score: 100 }, { score: 99 }, { score: 1 }, { score: 88 }, { score: 12 }, { score: 1 }] });
        expect(service.eval("DefaultFunction.IsExistRecord(\"Entity.items\",\"score\",88)")).toBe(true);
    });
    it('# DefaultFunction.ListContains("Entity.items","score","wor")', () => {
        const context = service.getContext();
        context.set("Entity", { items: [{ score: 100 }, { score: 99 }, { score: 1 }, { score: 88 }, { score: 12 }, { score: "hello world!" }] });
        expect(service.eval("DefaultFunction.ListContains(\"Entity.items\",\"score\",\"wor\")")).toBe(true);
    });
    it('# DefaultFunction.ListContains("Entity.items","score","ow")', () => {
        const context = service.getContext();
        context.set("Entity", { items: [{ score: 100 }, { score: 99 }, { score: 1 }, { score: 88 }, { score: 12 }, { score: "hello world!" }] });
        expect(service.eval("DefaultFunction.ListContains(\"Entity.items\",\"score\",\"ow\")")).toBe(false);
    });
    it('# DefaultFunction.ListGreaterThan("Entity.items","score",1000)', () => {
        const context = service.getContext();
        context.set("Entity", { items: [{ score: 100 }, { score: 99 }, { score: 1 }, { score: 88 }, { score: 12 }] });
        expect(service.eval("DefaultFunction.ListGreaterThan(\"Entity.items\",\"score\",1000)")).toBe(false);
    });
    it('# DefaultFunction.ListGreaterThan("Entity.items","score",9)', () => {
        const context = service.getContext();
        context.set("Entity", { items: [{ score: 100 }, { score: 99 }, { score: 1 }, { score: 88 }, { score: 12 }] });
        expect(service.eval("DefaultFunction.ListGreaterThan(\"Entity.items\",\"score\",9)")).toBe(true);
    });
    it('# DefaultFunction.ListLessThan("Entity.items","score",9)', () => {
        const context = service.getContext();
        context.set("Entity", { items: [{ score: 100 }, { score: 99 }, { score: 1 }, { score: 88 }, { score: 12 }] });
        expect(service.eval("DefaultFunction.ListLessThan(\"Entity.items\",\"score\",9)")).toBe(true);
    });
    it('# DefaultFunction.ListStartWith("Entity.items","score","jerry&")', () => {
        const context = service.getContext();
        context.set("Entity", { items: [{ score: 100 }, { score: 99 }, { score: 1 }, { score: 88 }, { score: "jerry&tom are good friends!" }] });
        expect(service.eval("DefaultFunction.ListStartWith(\"Entity.items\",\"score\",\"jerry&\")")).toBe(true);
    });
    it('# DefaultFunction.ListStartWith("Entity.items","score","erry&tom")', () => {
        const context = service.getContext();
        context.set("Entity", { items: [{ score: 100 }, { score: 99 }, { score: 1 }, { score: 88 }, { score: "jerry&tom are good friends!" }] });
        expect(service.eval("DefaultFunction.ListStartWith(\"Entity.items\",\"score\",\"erry&tom&\")")).toBe(false);
    });//
    it('# DefaultFunction.ListEndWith("Entity.items","score","ds!")', () => {
        const context = service.getContext();
        context.set("Entity", { items: [{ score: 100 }, { score: 99 }, { score: 1 }, { score: 88 }, { score: "jerry&tom are good friends!" }] });
        expect(service.eval("DefaultFunction.ListEndWith(\"Entity.items\",\"score\",\"ds!\")")).toBe(true);
    });
    it('# DefaultFunction.GetChainedPropertyValue("Entity.items.score")', () => {
        const context = service.getContext();
        context.set("Entity", { items: { score: 100 } });
        expect(service.eval("DefaultFunction.GetChainedPropertyValue(\"Entity.items.score\")")).toBe(100);
    });//
    it('# DefaultFunction.GetContextParameter("userId")', () => {
        const context = service.getContext();
        context.set("userId", "tom");
        expect(service.eval("DefaultFunction.GetContextParameter(\"userId\")")).toBe("tom");
    });
    it('# add function', () => {
        service.addFun("sum", (x, y) => { return x + y + x * y; });
        const context = service.getContext();
        context.set("x", 5);
        context.set("y", 4);
        expect(service.eval("sum(x,y)")).toBe(29);
    });
    it('# StringAdd', () => {
        expect(service.eval("DefaultFunction.Add('123','456','789')")).toBe('123456789');
    });
    it('# ToChineseMoney', () => {
        expect(service.eval("DefaultFunction.ToChineseMoney('996')")).toBe('玖佰玖拾陆元整');
    });
    it('# ToChineseMoney', () => {
        expect(service.eval("DefaultFunction.ToByte('100')")).toBe(100);
    });
    it('# GetBirthday', () => {
        expect(service.eval("DefaultFunction.GetBirthday('370784199012037014')")).toBe('1990-12-03');
    });
    it('# GetAge', () => {
        expect(service.eval("DefaultFunction.GetAge('370784192112037014')")).toBe(102);
    });
    it('# DateTimeAddDays', () => {
        expect(service.eval("DefaultFunction.FormatDefineDate('yyyy-MM-dd HH:mm:ss',DefaultFunction.DateTimeAddDays('2020-01-01 00:00:00',1))")).toBe('2020-01-02 00:00:00');
    });
    it('# Today', () => {
        expect(service.eval("DefaultFunction.Today('2021-05-26 00:00:00')")).toBe('2021-05-26');
    });
    it('# Yesterday', () => {
        expect(service.eval("DefaultFunction.Yesterday('2021-05-26 00:00:00')")).toBe('2021-05-25');
    });
    it('# ThisYear', () => {
        expect(service.eval("DefaultFunction.ThisYear('2021-05-26 00:00:00')")).toBe('2021');
    });
    it('# LastDayOfMonth', () => {
        expect(service.eval("DefaultFunction.LastDayOfMonth('2021-05-26 00:00:00')")).toBe('2021-05-31');
    });
    it('# StringToUpper', () => {
        expect(service.eval("DefaultFunction.ToUpper('abcDeFg')")).toBe('ABCDEFG');
    });
    it('# StringTrimStart', () => {
        expect(service.eval("DefaultFunction.TrimStart(' abcDeFg')")).toBe('abcDeFg');
    });
    it('# IncludedInList', () => {
        expect(service.eval("DefaultFunction.IncludedInList('tom',['a','b','tom'])")).toBe(true);
    });
    it('# NotIncludedInList', () => {
        expect(service.eval("DefaultFunction.IncludedInList('tom',['a','b','tom1'])")).toBe(false);
    });
    it('# ExistData = false', () => {
        const context = service.getContext();
        context.set("data", { arr: [] });
        expect(service.eval("DefaultFunction.ExistData('data.arr')")).toBe(false);
    });
    it('# ExistData= true', () => {
        const context = service.getContext();
        context.set("data", { arr: [1, 2, 3] });
        expect(service.eval("DefaultFunction.ExistData('data.arr')")).toBe(true);
    });
    it('# CountOfChild', () => {
        const context = service.getContext();
        context.set("data", { item: [{ p1: "1" }, { p1: "2" }, { p1: "3" }] });
        expect(service.eval("DefaultFunction.CountOfChild('data.item')")).toBe(3);
    });
    it('# SortChildData', () => {
        const context = service.getContext();
        context.set("data", { item: [{ p1: "3", p2: null }, { p1: "1", p2: undefined }, { p1: "2", p2: '' }] });
        expect(service.eval("DefaultFunction.SortChildData('data.item','p1','ESC')").map((item: any) => item.p1).join(',')).toBe("1,2,3");
    });
    it('# IsContainMatch', () => {
        const context = service.getContext();
        context.set("data", { item: [{ p1: 3, p2: null }, { p1: true, p2: undefined }, { p1: undefined, p2: '' }, { p1: 'hello', p2: '' }, { p1: null, p2: '' }] });
        expect(service.eval("DefaultFunction.IsContainMatch('data.item','p1',undefined)")).toBe(true);
    });
    it('# MinValueOfPeriod', () => {
        const context = service.getContext();
        context.set("data", { item: [{ p1: 2123, p2: '2021-09-10' }, { p1: 323, p2: '2021-09-11' }, { p1: 4121, p2: '2021-09-12' }, { p1: 78, p2: '2021-09-13' }, { p1: 908, p2: '2021-09-14' }] });
        expect(service.eval("DefaultFunction.MinValueOfPeriod('data.item','p2','p1','2021-09-11','2021-09-13')")).toBe(78);
    });
    it('# MaxValueOfPeriod', () => {
        const context = service.getContext();
        context.set("data", { item: [{ p1: 2123, p2: '2021-09-10' }, { p1: 323, p2: '2021-09-11' }, { p1: 4121, p2: '2021-09-12' }, { p1: 78, p2: '2021-09-13' }, { p1: 908, p2: '2021-09-14' }] });
        expect(service.eval("DefaultFunction.MaxValueOfPeriod('data.item','p2','p1','2021-09-11','2021-09-13')")).toBe(4121);
    });
    it('# AvgValueOfPeriod', () => {
        const context = service.getContext();
        context.set("data", { item: [{ p1: 2123, p2: '2021-09-10' }, { p1: 1, p2: '2021-09-11' }, { p1: 2, p2: '2021-09-12' }, { p1: 3, p2: '2021-09-13' }, { p1: 908, p2: '2021-09-14' }] });
        expect(service.eval("DefaultFunction.AvgValueOfPeriod('data.item','p2','p1','2021-09-11','2021-09-13')")).toBe(2);
    });
    it('# GetComputeJsonData', () => {
        const context = service.getContext();
        context.set("data", { item: { p1: 1 } });
        expect(service.eval("DefaultFunction.GetComputeJsonData('data.item','p1')")).toBe(1);
    });
    it('# ThisMonth', () => {
        expect(service.eval("DefaultFunction.ThisMonth('2021-12-09')")).toBe('12');
    });
    it('# CountByProp', () => {
        const context = service.getContext();
        context.set("data", { item: [{ p1: "1" }, { p1: "2" }, { p1: null }, { p1: undefined }] });
        expect(service.eval("DefaultFunction.CountByProp('data.item','p1')")).toBe(2);
    });
    it('# LastMonth', () => {
        expect(service.eval("DefaultFunction.LastMonth('2021-12-09')")).toBe('11');
    });
    it('# LastMonth-2', () => {
        expect(service.eval("DefaultFunction.LastMonth('2021-1-1 00:00:00')")).toBe('12');
    });
    it('# LastMonth-3', () => {
        expect(service.eval("DefaultFunction.LastMonth('2021-5-1 00:00:00')")).toBe('4');
    });
    it('# NextMonth', () => {
        expect(service.eval("DefaultFunction.NextMonth('2021-12-24')")).toBe('1');
    });
    it('# NextMonth-1', () => {
        expect(service.eval("DefaultFunction.NextMonth('2102-5-3')")).toBe('6');
    });
    it('# DayOfWeek-1', () => {
        expect(service.eval("DefaultFunction.DayOfWeek('2022-01-07')")).toBe('5');
    });
    it('# FirstDayOfNextMonth-1', () => {
        expect(service.eval("DefaultFunction.FirstDayOfNextMonth('2021-12-20')")).toBe('2022-01-01');
    });
    it('# LastDayOfNextMonth-1', () => {
        expect(service.eval("DefaultFunction.LastDayOfNextMonth('2021-12-01')")).toBe('2022-01-31');
    });
    it('# LastDayOfNextMonth-2', () => {
        expect(service.eval("DefaultFunction.LastDayOfNextMonth('2022-01-01')")).toBe('2022-02-28');
    });
    it('# LastDayOfYear', () => {
        expect(service.eval("DefaultFunction.LastDayOfYear('2021-09-19')")).toBe('2021-12-31');
    });
    it('# LastDayOfYear-1', () => {
        expect(service.eval("DefaultFunction.LastDayOfYear('2020-01-01')")).toBe('2020-12-31');
    });
    it('# GetFirstDayOfMonth', () => {
        expect(service.eval("DefaultFunction.GetFirstDayOfMonth('2020-01-23')")).toBe('2020-01-01');
    });
    it('# GetLastDayOfMonth', () => {
        expect(service.eval("DefaultFunction.GetLastDayOfMonth('2020-01-23')")).toBe('2020-01-31');
    });
    it('# GetDate', () => {
        expect(service.eval("DefaultFunction.GetDate('DD','2021-12-31 09:08:07')")).toBe('2021-12-31');
    });
    it('# FirstDayOfLastYear', () => {
        expect(service.eval("DefaultFunction.FirstDayOfLastYear('2021-12-12')")).toBe('2020-01-01');
    });
    it('# FirstDayOfLastYear-1', () => {
        expect(service.eval("DefaultFunction.FirstDayOfLastYear('2020-01-01')")).toBe('2019-01-01');
    });
    it('# LastDayOfLastYear', () => {
        expect(service.eval("DefaultFunction.LastDayOfLastYear('2020-01-01')")).toBe('2019-12-31');
    });
    it('# LastDayOfLastYear-1', () => {
        expect(service.eval("DefaultFunction.LastDayOfLastYear('2021-01-01')")).toBe('2020-12-31');
    });
    it('# FirstDayOfNextYear', () => {
        expect(service.eval("DefaultFunction.FirstDayOfNextYear('2021-09-09')")).toBe('2022-01-01');
    });
    it('# FirstDayOfNextYear-1', () => {
        expect(service.eval("DefaultFunction.FirstDayOfNextYear('2020-12-09')")).toBe('2021-01-01');
    });
    it('# LastDayOfNextYear', () => {
        expect(service.eval("DefaultFunction.LastDayOfNextYear('2020-12-09')")).toBe('2021-12-31');
    });
    it('# NextMonth', () => {
        expect(service.eval("DefaultFunction.NextMonth('2020-12-09')")).toBe('1');
    });
    it('# GetDate-1', () => {
        expect(service.eval("DefaultFunction.GetDate('DD','2020-09-09 09:59:59')")).toBe('2020-09-09');
    });
    it('# ToBoolean-True', () => {
        expect(service.eval("DefaultFunction.ToBoolean('True')")).toBe(true);
    });
    it('# ToBoolean-False', () => {
        expect(service.eval("DefaultFunction.ToBoolean('FalSE')")).toBe(false);
    });
    it('# GetSessionValue', () => {
        const context = service.getContext();
        context.set("userCode", "tom");
        expect(service.eval("DefaultFunction.GetSessionValue('userCode')")).toBe('tom');
    });
    it('# IsNullOrWhiteSpace', () => {
        const context = service.getContext();
        expect(service.eval("DefaultFunction.IsNullOrWhiteSpace('')")).toBe(true);
    });
    it('# IsNullOrWhiteSpace-1', () => {
        const context = service.getContext();
        expect(service.eval("DefaultFunction.IsNullOrWhiteSpace(undefined)")).toBe(false);
    });
    it('# IsNullOrWhiteSpace-2', () => {
        const context = service.getContext();
        expect(service.eval("DefaultFunction.IsNullOrWhiteSpace(null)")).toBe(true);
    });
    it('# MultiplyChildNumber', () => {
        const context = service.getContext();
        context.set("UserInfo", { records: [{ p1: 10, p2: 30 }, { p1: 5, p2: 6 }, { p1: 1, p2: 3 }] });
        expect(service.eval("DefaultFunction.MultiplyChildNumber('UserInfo.records','p1','p2')")).toBe(333);
    });
    it('# FormatDate', () => {
        const context = service.getContext();
        const defaultFunction = new DefaultFunctions(context);
        console.log(defaultFunction.FormatDate('yyyy-MM-dd'));
        expect(service.eval("DefaultFunction.FormatDate()")).toBe('2024-07-02');
    });
    // it('# GetDateTimeNow', () => {
    //     const context = service.getContext();
    //     const defaultFunction = new DefaultFunctions(context);
    //     console.log(defaultFunction.GetDateTimeNow());
    //     expect(service.eval("DefaultFunction.GetDateTimeNow()")).toBe('2024-01-23 19:05:20');
    // });
    it('# SubString', () => {
        const context = service.getContext();
        expect(service.eval("DefaultFunction.SubString('abcdefg',2,3)")).toBe('cde');
    });
    it('# ThisWeek', () => {
        const context = service.getContext();
        expect(service.eval("DefaultFunction.ThisWeek('2024-1-1')")).toBe(1);
        expect(service.eval("DefaultFunction.ThisWeek('2023-1-1')")).toBe(1);
        expect(service.eval("DefaultFunction.ThisWeek('2024-01-15')")).toBe(3);
        expect(service.eval("DefaultFunction.ThisWeek('2023-12-14')")).toBe(50);
        expect(service.eval("DefaultFunction.ThisWeek('2020-03-19')")).toBe(12);
    });
    //
    it('# DayDifference', () => {
        const context = service.getContext();
        expect(service.eval("DefaultFunction.DayDifference('2024-1-1','2024-1-11')")).toBe(10);
        expect(service.eval("DefaultFunction.DayDifference('2024-1-11','2024-1-1')")).toBe(-10);
        expect(service.eval("DefaultFunction.DayDifference('2024-1-1 00:00:00','2024-1-1 23:59:59')")).toBe(0);
        expect(service.eval("DefaultFunction.DayDifference('2024-1-1 09:00:00','2024-1-2 10:00:00')")).toBe(1);
        expect(service.eval("DefaultFunction.DayDifference('2024-1-2 10:00:00', '2024-1-1 09:00:00')")).toBe(-1);

        expect(service.eval("DefaultFunction.DayDifference('2024-02-28', '2024-03-01')")).toBe(2);
        expect(service.eval("DefaultFunction.DayDifference('2024-02-28', '2024/03/01')")).toBe(2);
        expect(service.eval("DefaultFunction.DayDifference('2024-02-28', '2024-03-01 00:00:00')")).toBe(2);

        expect(service.eval("DefaultFunction.DayDifference('2024-04-04', '2026-04-04')")).toBe(730);
        expect(service.eval("DefaultFunction.DayDifference('2026-04-04','2024-04-04')")).toBe(-730);

        expect(service.eval("DefaultFunction.DayDifference('2026-01-04','2024-01-04')")).toBe(-731);
        expect(service.eval("DefaultFunction.DayDifference('2024-01-04','2026-01-04')")).toBe(731);

        expect(service.eval("DefaultFunction.DayDifference('', '2024-1-1 09:00:00')")).toBe(null);
        expect(service.eval("DefaultFunction.DayDifference('2024-2-32 00:00:00', '2024-1-1 09:00:00')")).toBe(null);
        expect(service.eval("DefaultFunction.DayDifference('This is a date', '2024-1-1 09:00:00')")).toBe(null);
    });
});
