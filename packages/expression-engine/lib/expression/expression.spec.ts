import { ExpressionContext } from "../context";
import { Expression } from "./expression";

describe('Expression', () => {
    const expr = "2+3*4";
    const expression = new Expression(expr);
    it('# @Expression 2+3*4 = 14', () => {
        const result = expression.eval();
        expect(result).toBe(14);
    });

    it('# @Expression 判断语句', () => {
        const expr = "var x=1;var y;if(x>0){y=x;}else{y=0};return y;";
        const expression = new Expression(expr);
        expect(expression.eval()).toBe(1);
    });

    it('# @Expression 带入参的判断语句', () => {
        const expr = "if(x>0){y=x;}else{y=0};return y;";
        const expression = new Expression(expr);
        const context = new ExpressionContext();
        context.set("x", 10);
        context.set("y", undefined);
        expect(expression.eval(context)).toBe(10);
    });

    it('# @Expression 带对象类型的入参', () => {
        const expr = "debug.log(\"success!\");return 10;";
        const expression = new Expression(expr);
        const context = new ExpressionContext();
        context.set("debug", console);
        expect(expression.eval(context)).toBe(10);
    });

    it('# @Expression DefaultFunction.Sum', () => {
        const expr = "DefaultFunction.Sum(x)";
        const expression = new Expression(expr);
        const context = new ExpressionContext();
        context.set("x", [1, 10, 100, 1000]);
        expect(expression.eval(context)).toBe(1111);
    });

    it('# @Expression Entity.a+Entity.b', () => {
        const expr = "if(Entity.a+Entity.b > 10){return 'Tom';}";
        const expression = new Expression(expr);
        const context = new ExpressionContext();
        context.set("Entity", { a: 1, b: 5 });
        expect(expression.eval(context)).toBe(undefined);
    });
    it('# @commit', () => {
        const expr = "if(Entity.a+Entity.b > 10){return 'Tom';} // 这是注释啊";
        const expression = new Expression(expr);
        const context = new ExpressionContext();
        context.set("Entity", { a: 1, b: 5 });
        expect(expression.eval(context)).toBe(undefined);
    });
    it('# @compile', () => {
        const expr = "if(x>0){y=x;}else{y=0};return y;";
        const context = new ExpressionContext();
        context.set("x", 10);
        context.set("y", undefined);
        const expression = new Expression(expr, context);
        expect(expression.compile().eval()).toBe(10);
    });
    it('# @banchmark 1 @ compile', () => {
        console.time('compile');
        const expr = "if(x>0){y=x;}else{y=0};return y;";
        let factory;
        for (let i = 1; i < 100000; i++) {
            const context = new ExpressionContext();
            context.set("x", i);
            context.set("y", undefined);
            if (!factory) {
                const expression = new Expression(expr, context);
                factory = expression.compile();
            }
            expect(factory.eval(context)).toBe(i);
        }
        console.timeEnd('compile');
    });
    it('# @banchmark 1 @ eval', () => {
        console.time('eval');
        for (let i = 1; i < 100000; i++) {
            const expr = "if(x>0){y=x;}else{y=0};return y;";
            const context = new ExpressionContext();
            context.set("x", i);
            context.set("y", undefined);
            const expression = new Expression(expr, context);
            expect(expression.eval()).toBe(i);
        }
        console.timeEnd('eval');
    });
});
