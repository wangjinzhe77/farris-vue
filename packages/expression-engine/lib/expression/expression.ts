/* eslint-disable @typescript-eslint/ban-types */
import { Compiler } from "../compiler/index";
import { IExpression, IExpressionContext } from "../core/index";
import { Evaler } from "../evaler/index";
import { DefaultFunctions } from "../function/index";

export class Expression implements IExpression {
    private expr: string;

    private factory: Function | null = null;

    private context: IExpressionContext | undefined;

    constructor(expr: string, context?: IExpressionContext) {
        this.expr = expr;
        this.context = context;
    }

    /**
     * 执行表达式
     * @param context
     * @returns
     */
    public eval(context?: IExpressionContext) {
        context = context || this.context;
        const args = context && context.contexts && context.contexts.arguments || {};
        if (this.factory) {
            const ctx = this.buildContext(context);
            const result = this.factory(ctx?.contexts)(args);
            return result;
        }
        const evaler = new Evaler();
        const factory = evaler.eval(this.expr, context);
        const result = factory(...Object.values(args));
        return result;

    }

    /**
     * 编译表达式
     * @param context 上下文
     * @returns
     */
    public compile(context?: IExpressionContext): Expression {
        context = context || this.context;
        const compiler = new Compiler();
        this.factory = compiler.compile(this.expr, context);
        return this;
    }

    private buildContext(context?: IExpressionContext) {
        const contexts = context && context.contexts || {};
        const defaultFunction = new DefaultFunctions(context);
        contexts.DefaultFunction = defaultFunction;
        return context;
    }
}
