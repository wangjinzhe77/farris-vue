import { IExpressionContext } from "../core/index";

export class ExpressionContext implements IExpressionContext {

    public contexts: { [prop: string]: any } = {};

    public arguments: { [prop: string]: any } = {};

    constructor() {
        this.contexts = {};
    }

    public set(key: string, value: any): void {
        this.contexts[key] = value;
    }

    public setArgument(key: string, value: any) {
        this.arguments[key] = value;
    }

    public clear() {
        this.arguments = {};
    }
}
