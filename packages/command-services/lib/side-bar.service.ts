export class SidebarService {
    constructor() { }

    public openSidebar() { }

    public closeSidebar() { }

    public confirmBeforeClosingSidebar() { }

    public continueClosingSidebar() { }

    public stopClosingSidebar() { }
}
