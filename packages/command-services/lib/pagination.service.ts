import { ViewModel, ViewModelState } from "@farris/devkit-vue";

export class PaginationService {
    [propertyName: string]: any;
    constructor(private viewModel: ViewModel<ViewModelState>) { }
    /**
     * 设置页码
     * @param entityPath 
     */
    public setPageIndex(entityPath: string) {
        const eventParams = this['context'] && this['context']['eventParams'] || {};
        const pageIndex = eventParams;
        const pagination = this.viewModel.entityStore?.getPaginationByPath(entityPath);
        if (pagination) {
            pagination.pageIndex = pageIndex;
            this.viewModel.entityStore?.setPaginationByPath(entityPath, pagination);
        }
    }
    /**
     * 设置分页大小
     * @param entityPath 
     */
    public setPageSize(entityPath: string) {
        const eventParams = this['context'] && this['context']['eventParams'] || {};
        const pageSize = eventParams;
        const pagination = this.viewModel.entityStore?.getPaginationByPath(entityPath);
        if (pagination) {
            pagination.pageSize = pageSize;
            this.viewModel.entityStore?.setPaginationByPath(entityPath, pagination);
        }
    }
    public resetChildrenPagination(){
        
    }
}