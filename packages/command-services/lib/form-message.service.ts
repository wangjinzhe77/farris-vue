import { InjectionToken, Injector } from "@farris/devkit-vue";
import { MESSAGE_BOX_SERVICE_TOKEN } from "./tokens";

export class FormMessageService {
    private messageBoxService: any;
    constructor(private injector: Injector) {
        if (this.injector) {
            this.messageBoxService = this.injector.get(MESSAGE_BOX_SERVICE_TOKEN);
        }
    }
    public success(content: string) {
        if (this.messageBoxService) {
            this.messageBoxService.success(content);
        }
    }
    /**
     * 确认弹框
     * @param content 弹出内容提示
     */
    public confirm(content: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            if (this.messageBoxService) {
                this.messageBoxService.question(content, '', () => {
                    resolve(true);
                }, () => {
                    resolve(false);
                });
            } else {
                reject();
            }
        });
    }

    /**
     * 消息弹框
     */
    public info(content: string) {
        if (this.messageBoxService) {
            this.messageBoxService.info(content);
        }
    }

    /**
     * 错误弹框
     */
    public error(content: string) {
        if (this.messageBoxService) {
            this.messageBoxService.error(content);
        }
    }

    /**
     * 警告弹框
     */
    public warning(content: string) {
        if (this.messageBoxService) {
            this.messageBoxService.warning(content);
        }
    }
    /**
     * 异常
     * @param message 
     * @param detail 
     * @param date 
     */
    public exception(message: string, detail: string, date?: string) {
        if (this.messageBoxService) {
            this.messageBoxService.error(message, detail, date);
        }
    }
}