import { QuerystringService } from './querystring.service';
import { RuntimeFrameworkService } from './rtf.service';
// tslint:disable: no-string-literal
/**
 * 参数服务
 */
export class ParamService {
    constructor(
        private querystringService: QuerystringService,
        private runtimeFrameworkService: RuntimeFrameworkService
    ) { }
    /**
     * 适配获取所有参数
     */
    public get params(): Promise<{ [propName: string]: any }> {
        // 先从hash中获取参数
        const hash = window.location.hash;
        const params = this.querystringService.parse(hash);
        // 获取tabId，最新版本一定有tabId
        const tabId = params.tabId;
        if (!tabId) {
            return Promise.resolve(params);
        }
        return new Promise((resolve, reject) => {
            // 管道参数e可能为object、Map类型
            this.runtimeFrameworkService.addEventListener(tabId, (data: any) => {
                let map = {};
                let args = {};
                if (this.isMap(data)) {
                    map = this.parseMapParams(data);
                    args = new Map(data);
                } else {
                    args = Object.assign({}, data);
                }
                resolve(Object.assign({}, args, map, params));
            }, params);
        });
    }
    /**
     * 解析map类型的参数
     * @param params params
     */
    private parseMapParams(params: Map<any, any>) {
        const map: any = {};
        map['WEB_FORM_ROUTE_PARAMS'] = decodeURIComponent(params.get('WEB_FORM_ROUTE_PARAMS'));
        params.forEach((value, key) => {
            if (key !== 'WEB_FORM_ROUTE_PARAMS') {
                map[key] = value;
            }
        });
        return map;
    }
    /**
     * 获取param参数
     * @param param key
     */
    public get(param: string): Promise<any> {
        return this.params.then((options => {
            if (!!options && options.hasOwnProperty(param)) {
                return options.param;
            } else {
                return undefined;
            }
        })
        );
    }
    /**
     * 参数转为对象
     */
    public parse(): Promise<any> {
        return this.params;
    }
    private isMap(value: any): value is Map<any, any> {
        return Object.prototype.toString.call(value) === '[object Map]';
    }
}
