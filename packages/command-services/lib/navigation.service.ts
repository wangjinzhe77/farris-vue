import { RuntimeFrameworkService } from './rtf.service';
import { QuerystringService } from './querystring.service';
import { ViewModel, Injector, ViewModelState, CommandContext, Command } from '@farris/devkit-vue';
import { AppType } from './types';
import lodash from 'lodash';
import { NavigationEventService } from './navigation-event.service';
import { NavigationHistoryService } from './navigation-history.service';
/**
 * 导航服务
 */
export class NavigationService {
    private commandContext: CommandContext;
    /**
     * 命令上下文
     */
    constructor(
        private runtimeFrameworkService: RuntimeFrameworkService,
        private querystringService: QuerystringService,
        private viewModel: ViewModel<ViewModelState>,
        private injector: Injector,
        private navigationEventService: NavigationEventService,
        private navigationHistoryService: NavigationHistoryService
    ) {
    }
    public set context(commandContext: CommandContext) {
        this.commandContext = commandContext;
        this.navigationEventService.commandContext = commandContext;
    }
    private get querystrings() {
        let hash = window.location.hash;
        const params = this.querystringService.parse(hash);
        if (params) {
            params.formToken = this.runtimeFrameworkService.formToken;
        }
        return params;
    }
    // #region 接口

    /**
     * 打开菜单
     * @param tabId 根据TabId决定打开新标签页或定位之前打开的标签页
     * @param funcId 菜单Id
     * @param params 参数
     * @param enableRefresh 启用数据刷新
     * @param tabName tab标题
     * @param destructuring 是否解构参数
     */
    public openMenu(tabId: string, funcId: string, params: any, reload?: boolean, enableRefresh?: any, tabName?: string, destructuring?: any) {
        let queryStringParams = this.buildParamMap(params);
        destructuring = this.convertToBoolean(destructuring, false);
        if (destructuring === true) {
            queryStringParams = this.buildParam(params);
        }
        const currentTabId = this.querystrings.tabId || this.querystrings.funcId || this.querystrings.appId;
        const options: any = {
            tabId,
            funcId,
            appType: AppType.Menu,
            queryStringParams: queryStringParams,
            entityParams: queryStringParams,
            appId: undefined,
            appEntrance: undefined,
            tabName: tabName || null
        };
        // 启用数据刷新参数为true或者没有定义，则按刷新处理
        // 没有传递该参数或该参数为空，则认为按照之前的逻辑处理，默认刷新
        // null false "false" "true" undefined
        enableRefresh = this.convertToBoolean(enableRefresh, true);
        if (enableRefresh) {
            this.navigationHistoryService.add(currentTabId, tabId);
        }
        this.runtimeFrameworkService.openMenu(options);
    }
    /**
     * 打开菜单（流）
     * @param tabId 根据TabId决定打开新标签页或定位之前打开的标签页
     * @param funcId 菜单Id
     * @param params 参数
     * @param enableRefresh 启用数据刷新
     * @param tabName 页签标题
     * @param destructuring 解构参数
     */
    public openMenu$(tabId: string, funcId: string, params: any, reload?: boolean, enableRefresh?: any, tabName?: string, destructuring?: any) {
        let queryStringParams = this.buildParamMap(params);
        destructuring = this.convertToBoolean(destructuring, false);
        if (destructuring === true) {
            queryStringParams = this.buildParam(params);
        }
        const currentTabId = this.querystrings.tabId || this.querystrings.funcId || this.querystrings.appId;
        const options: any = {
            tabId,
            funcId,
            appType: AppType.Menu,
            queryStringParams: queryStringParams,
            entityParams: queryStringParams,
            appId: undefined,
            appEntrance: undefined,
            tabName: tabName || null
        };
        // 启用数据刷新参数为true或者没有定义，则按刷新处理
        enableRefresh = this.convertToBoolean(enableRefresh, true);
        if (enableRefresh) {
            this.navigationHistoryService.add(currentTabId, tabId);
        }
        return this.runtimeFrameworkService.openMenu$(options);
    }
    /**
     * 打开菜单(带维度)
     * @param tabId 根据TabId决定打开新标签页或定位之前打开的标签页
     * @param funcId 菜单Id
     * @param params 参数
     * @param enableRefresh 启用数据刷新
     * @param dim1 dim1
     * @param dim2 dim2
     * @param tabName 页签名称
     * @param metadataId 默认元数据id
     * @param destructuring 解构参数
     */
    public openMenuWithDimension(tabId: string, funcId: string, params: any, enableRefresh?: any, dim1?: any, dim2?: any, tabName?: string, metadataId?: string, destructuring?: any) {
        if (metadataId === undefined || metadataId === null) {
            metadataId = '';
        }
        let queryStringParams = this.buildParamMap(params);
        destructuring = this.convertToBoolean(destructuring, false);
        if (destructuring === true) {
            queryStringParams = this.buildParam(params);
        }

        // const paramsMap = this.buildParamMap(params);
        queryStringParams.set('dim1', dim1 ? dim1 : 'public');
        queryStringParams.set('dim2', dim2 ? dim2 : 'public');
        queryStringParams.set('metadataId', metadataId);
        queryStringParams.set('isRtc', '1');
        queryStringParams.set('isRootMetadata', 'true');
        const options: any = {
            tabId,
            funcId,
            appType: AppType.Menu,
            queryStringParams: queryStringParams,
            entityParams: queryStringParams,
            appId: undefined,
            appEntrance: undefined,
            isReload: false,
            tabName: tabName || null
        };
        // 启用数据刷新参数为true或者没有定义，则按刷新处理
        // 没有传递该参数或该参数为空，则认为按照之前的逻辑处理，默认刷新
        // null false "false" "true" undefined
        enableRefresh = this.convertToBoolean(enableRefresh, true);
        if (enableRefresh) {
            const currentTabId = this.querystrings.tabId || this.querystrings.funcId || this.querystrings.appId;
            this.navigationHistoryService.add(currentTabId, tabId);
        }
        this.runtimeFrameworkService.openMenu(options);
    }
    /**
     * 打开应用
     * @param tabId tabId 根据TabId决定打开新标签页或定位之前打开的标签页
     * @param appId 应用Id
     * @param appEntrance 应用入口
     * @param params 参数
     * @param tabName tab标题
     * @param enableRefresh 启用数据刷新
     * @param destructuring 解构参数
     */

    public openApp(tabId: string, appId: string, appEntrance: string, params: any, reload?: boolean, tabName?: string, enableRefresh?: any, destructuring?: any) {
        let queryStringParams = this.buildParamMap(params);
        destructuring = this.convertToBoolean(destructuring, false);
        if (destructuring === true) {
            queryStringParams = this.buildParam(params);
        }
        const options: any = {
            tabId,
            appId,
            appEntrance,
            funcId: undefined,
            appType: AppType.App,
            queryStringParams: queryStringParams,
            entityParams: queryStringParams,
            tabName: tabName || null
        };
        enableRefresh = this.convertToBoolean(enableRefresh, true);
        if (enableRefresh) {
            const currentTabId = this.querystrings.tabId || this.querystrings.funcId || this.querystrings.appId;
            this.navigationHistoryService.add(currentTabId, tabId);
        }
        this.runtimeFrameworkService.openMenu(options);
    }
    /**
     * 打开应用(流式)
     * @param tabId tabId 根据TabId决定打开新标签页或定位之前打开的标签页
     * @param appId 应用Id
     * @param appEntrance 应用入口
     * @param params 参数
     * @param tabName tab标题
     * @param enableRefresh 启用数据刷新
     * @param destructuring 解构参数
     */
    public openApp$(tabId: string, appId: string, appEntrance: string, params: any, reload?: boolean, tabName?: string, enableRefresh?: any, destructuring?: any) {
        let queryStringParams = this.buildParamMap(params);
        destructuring = this.convertToBoolean(destructuring, false);
        if (destructuring === true) {
            queryStringParams = this.buildParam(params);
        }
        const options: any = {
            tabId,
            appId,
            appEntrance,
            funcId: undefined,
            appType: AppType.App,
            queryStringParams: queryStringParams,
            entityParams: queryStringParams,
            tabName: tabName || null
        };
        enableRefresh = this.convertToBoolean(enableRefresh, true);
        if (enableRefresh) {
            const currentTabId = this.querystrings.tabId || this.querystrings.funcId || this.querystrings.appId;
            this.navigationHistoryService.add(currentTabId, tabId);
        }
        return this.runtimeFrameworkService.openMenu$(options);
    }
    /**
     * 关闭
     * @param onCloseing 关闭前事件处理器
     */
    public close() {
        const options = this.querystrings;
        options.token = options.formToken;
        this.runtimeFrameworkService.beforeCloseMenu(options);
    }
    /**
     * 强制关闭
     */
    public destory() {
        const options = this.querystrings;
        options.token = options.formToken;
        this.runtimeFrameworkService.closeMenu(options);
    }
    /**
     * 增加事件处理器
     * @param eventType 
     * @param handler 
     * @returns 
     */
    public addEventListener(eventType: string, handler: (options: any) => any): string | null {
        return this.navigationEventService.addEventListener(eventType, handler);
    }
    /**
     *
     * @param params params
     * @deprecated 待废弃，与buildParamMap重复
     */
    private parseParams(params: any) {
        if (typeof params === 'undefined' || params === null || (typeof params === 'string' && params.length < 1)) {
            params = {};
        }
        const paramMap = new Map<string, any>();
        if (typeof params === 'object') {
            params = JSON.stringify(params);
        }
        params = window['encodeURIComponent'](params);
        paramMap.set('WEB_FORM_ROUTE_PARAMS', params);
        return paramMap;
    }
    // #endregion

    // #region 私有方法

    /**
     * 封装路由参数
     * @param params 参数
     * @param options 配置参数
     */
    private buildParamMap(params: any, options?: any): Map<string, string> {
        if (typeof params === 'undefined' || params === null || (typeof params === 'string' && params.length < 1)) {
            params = {};
        }
        const paramMap = new Map<string, any>();
        if (options && Object.keys(options).length > 0) {
            if (typeof params !== 'object') {
                params = JSON.parse(params);
            }
            params = lodash.merge(params, options);
        }
        if (typeof params === 'object') {
            params = JSON.stringify(params);
        }
        const currentTabId = this.querystrings.tabId || this.querystrings.funcId || this.querystrings.appId;
        params = window['encodeURIComponent'](params);
        paramMap.set('WEB_FORM_ROUTE_PARAMS', params);
        paramMap.set('WEB_FORM_ROUTER_PARENT_ID', currentTabId);
        return paramMap;
    }
    private buildParam(params: any | null, options?: any): Map<string, string> {
        if (typeof params === 'undefined' || params === null || (typeof params === 'string' && params.length < 1)) {
            params = {};
        }
        const paramMap = new Map<string, any>();
        if (options && Object.keys(options).length > 0) {
            if (typeof params !== 'object') {
                params = JSON.parse(params);
            }
            params = lodash.merge(params, options);
        }
        if (typeof params !== 'object') {
            params = JSON.parse(params);
        }
        Object.keys(params).forEach(key => {
            paramMap.set(key, params[key]);
        });

        const currentTabId = this.querystrings.tabId || this.querystrings.funcId || this.querystrings.appId;
        params = window['encodeURIComponent'](JSON.stringify(params));
        paramMap.set('WEB_FORM_ROUTE_PARAMS', params);
        paramMap.set('WEB_FORM_ROUTER_PARENT_ID', currentTabId);
        return paramMap;
    }
    private convertToBoolean(value: any, defaultVal: boolean = false) {
        if (typeof value === 'undefined' || value === null) {
            value = defaultVal;
        }
        if (typeof value === 'string') {
            value = value || String(defaultVal);
            value = value === 'true' ? true : false;
        }
        return value;
    }
    // #endregion
}
