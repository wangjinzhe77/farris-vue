import { FormNotifyService } from "./form-notify.service";
import { LanguageService } from "./language.service";

export class DataCheckService {
    constructor(
        private languageService: LanguageService,
        private formNotifyService: FormNotifyService,
    ) { }
    public checkBeforeView(id: string) {
        if (id) {
            return true;
        }
        this.formNotifyService.warning(this.languageService.language.pleaseSelectViewData);
        return Promise.reject();
    }
    public checkBeforeEdit(id: string) {
        if (id) {
            return true;
        }
        this.formNotifyService.warning(this.languageService.language.pleaseSelectEditData);
        return Promise.reject();
    }
    public ifSkipCheck(id: string): boolean {
        return true;
    }
}