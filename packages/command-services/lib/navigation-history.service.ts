import { AtLeastOne, NavigationHistory } from "./types";

export class NavigationHistoryService {
    private readonly COMMAND_SERVICE_NAVIGATION_HISTORY = 'COMMAND_SERVICE_NAVIGATION_HISTORY';
    constructor() { }
    /**
     * 新增历史记录
     * @param from 
     * @param to 
     * @returns 
     */
    public add(from: string, to: string) {
        if (!from || !to) {
            console.warn(`History should has from and to property.`);
            return;
        }
        const history = this.deserialize();
        const item = this.find({ from, to });
        // 导航历史已经存在
        if (item) {
            return;
        }
        const navigationHistory: NavigationHistory = { from, to };
        history.push(navigationHistory);
        this.serialize(history);
    }
    /**
     * 移除历史记录
     * @param condition 条件
     * @returns 
     */
    public remove(condition: (item: NavigationHistory) => boolean) {
        const history = this.deserialize();
        const matchs = history.filter((value: NavigationHistory) => {
            return !condition(value);
        });
        this.serialize(matchs);
    }
    /**
     * 根据路由获取路由历史中是否存在该记录
     * @param target 
     * @returns 
     */
    public find(target: AtLeastOne<NavigationHistory>) {
        const history = this.deserialize();
        return history.find((item: NavigationHistory) => {
            if (target.from && target.from !== item.from) {
                return false;
            }
            if (target.to && target.to !== item.to) {
                return false;
            }
            return true;
        });
    }
    public findIndex(from: string, to: string) {
        const history = this.deserialize();
        return history.findIndex((item: NavigationHistory) => item.from === from && item.to === to);
    }
    /**
     * 持久化
     * @param history 
     */
    private serialize(history: any) {
        sessionStorage.setItem(this.COMMAND_SERVICE_NAVIGATION_HISTORY, JSON.stringify(history));
    }
    /**
     * 对象化
     * @returns 
     */
    private deserialize(): NavigationHistory[] {
        const storage = sessionStorage?.getItem(this.COMMAND_SERVICE_NAVIGATION_HISTORY);
        return storage ? JSON.parse(storage) : [];
    }
}
