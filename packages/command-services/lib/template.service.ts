import { compile, createVNode } from 'vue';
import { ViewModel, ViewModelState } from "@farris/devkit-vue";

export class TemplateService {
    constructor(private viewModel: ViewModel<ViewModelState>) { }

    public createTemplateFactory(template: string) {
        const compiledTemplate = compile(template);
        const viewModel = {
            stateMachine: this.viewModel.state.stateMachine,
            uiState: this.viewModel.state.uiState,
            bindingData: this.viewModel.state.entityState?.currentEntity,
            root: this.viewModel.getRoot(),
            parent: this.viewModel.getParent()
        };
        return (cell: any, data: any) => {
            return createVNode({ render: compiledTemplate, props: ['rowData', 'viewModel'] }, { viewModel, rowData: data });
        };
    }
}