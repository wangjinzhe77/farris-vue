import { ViewModel, ViewModelState } from '@farris/devkit-vue';
import { FormNotifyService } from '../form-notify.service';

export class AttachmentService {
    constructor(private viewModel: ViewModel<ViewModelState>, private formNotifyService: FormNotifyService) {
    }

    public uploadAndUpdateRow(attachmentInfoFieldPath: string, rootDirId?: string, parentDirName?: string, fileType?: string, id?: string){}

    public uploadAndUpdateRowWithPropertyName(attachmentInfoFieldPath: string, rootDirId?: string, parentDirName?: string, fileType?: string, id?: string){}

    public uploadAndBatchAddRows(attachmentInfoFieldPath: string, rootDirId?: string, parentDirName?: string, fileType?: string){}

    public uploadAndBatchAddRowsWithPropertyName(attachmentInfoFieldPath: string, rootDirId?: string, parentDirName?: string, fileType?: string, id?: string){}

    public download(attachId: string, rootId?: string){}

    public batchDownload(attachIds: string[], rootId: string){}

    public downloadByDataId(dataId: string, attachmentInfoFieldPath: string, rootId: string){}

    public batchDownloadByDataIds(dataIds: string[] | string, attachmentInfoFieldPath: string, rootId: string){}

    public previewByAttachmentInfoFieldPath(attachmentInfoFieldPath: string, rootDirId: string, ids?: any){}

    public previewByAttachmentInfoFieldPathWithIndex(attachmentInfoFieldPath: string, rootDirId: string){}

    public previewBySubDirName(subDirName: string, rootDirId: string){}

    public previewBySubDirNameWithIndex(attachmentInfoFieldPath: string, subDirName: string, rootDirId: string){}

    public previewByAttachmentIds(attachmentIds: string[], rootDirId: string){}

    public previewFileListWithIndex(attachmentIds: string[], rootDirId: string, attachmentId: string) {}

    public genVersion(versions: string[]){}

    public updateAttachmentVersion(versionField: string, historyField: string, attachmentFieldPath: string){}

    public isAttachmentCanDelete(historyField: string, attachmentFieldPath: string){}

    public updateAttachmentHistory(versionField: string, historyField: string, attachmentFieldPath: string){}

    public removeAttachment(attachmentInfoFieldPath: string, id: string, rootDirId: string){}
}
