import { ViewModel, ViewModelState } from "@farris/devkit-vue";
import { FormNotifyService } from "../form-notify.service";

export class FileService {
    constructor(private viewModel: ViewModel<ViewModelState>, private formNotifyService: FormNotifyService){}

    public addFileRows(fileInfoFieldPath: string){}

    public addFileRowsWithConfigs(fileInfoFieldPath: string, configs: string){}

    public removeFileRows(fileInfoFieldPath: string){}

    public updateOrder(attachmentInfoFieldPath: string, ids: string[]){}

    
}
