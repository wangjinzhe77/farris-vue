/**
 * 附件信息定义
 */
interface AttachmentInfo {

  /**
   * 附件id
   */
  attachmentId: string;

  /**
   * 附件名称
   */
  fileName: string;
}


/**
 * 服务器端所需的附件信息定义
 */
interface ServerAttachmentInfo {

  /**
   * 表名路径
   */
  NodeCodes: string[];

  /**
   * 主键路径
   */
  HiretryIds: string[];

  /**
   * 附件id数组（批量新增时使用）
   */
  AttachmentIds?: string[];

  /**
   * 附件id（更新时使用）
   */
  AttachmentId?: string;
}
/**
 * 附件udt排序字段
 */
const ATTACHMENT_ORDER_FIELD = 'fileSortOrder';

export { AttachmentInfo, ServerAttachmentInfo, ATTACHMENT_ORDER_FIELD };
