import { Module, ViewModel, ViewModelState } from '@farris/devkit-vue';
import { ParamService } from './param.service';
import { RuntimeFrameworkService } from './rtf.service';

/**
 * 应用参数服务
 */

export class ApplicationParamService {
    constructor(
        private paramService: ParamService,
        private runtimeFrameworkService: RuntimeFrameworkService,
        private viewModel: ViewModel<ViewModelState>
    ) {
    }
    /**
     * 解析参数
     */
    public parseParams(callback: () => void) {
        const highOrderInvoke = this.highOrderInvoke(callback);
        this.paramService.parse().then((params) => {
            this.setupParams(params, this.viewModel, highOrderInvoke);
        });
    }

    /**
     * 设置参数
     */
    private setupParams(params: any, viewModel: ViewModel<ViewModelState>, callback: () => void) {
        const queryParams = this.getParams(params);
        if (!queryParams) {
            callback();
            return;
        }

        // 先设置参数，保证普通路由也能正常执行。
        this.setQueryParams(queryParams, viewModel);
        const funcId = this.getFuncId(queryParams);
        const appId = this.getAppId(queryParams);
        if (!funcId && !appId) {
            callback();
            return;
        }
        // 保存上下文
        const app = this.viewModel.getInjector().get<Module>(Module);
        if (params.tabId) {
            // TODO: 缺少全局参数存储
            // app.params.set('tabId', params.tabId)
        }
        if (funcId) {
            this.setStaticParams(funcId, queryParams, viewModel, callback);
        } else {
            callback();
        }
    }
    /**
     * 设置查询参数
     */
    private setQueryParams(queryParams: any, viewModel: ViewModel<ViewModelState>): any {
        const parsedQueryParams: any = {};
        // 设置表单参数
        const uiState = viewModel && viewModel.uiStore || {};
        Object.keys(queryParams).forEach((paramName: string) => {

            if (!uiState.hasOwnProperty(paramName)) {
                parsedQueryParams[paramName] = queryParams[paramName];
            }
        });
        this.updateUIState(viewModel, parsedQueryParams);
    }

    /**
     * 设置静态参数
     */
    private setStaticParams(
        funcId: string, queryParams: any, viewModel: ViewModel<ViewModelState>, callback: () => void
    ) {
        this.runtimeFrameworkService.getMenuParams(funcId, (staicParams) => {
            const staticParamsObj = this.mapStaticParamsToObject(staicParams, queryParams, viewModel);
            if (!staticParamsObj) {
                callback();
                return;
            }
            this.updateUIState(viewModel, staticParamsObj);
            callback();
        });
    }

    /**
     * 将staticParams转换为普通对象
     * @param staticParams，形如：[{'name': 'key1', 'value': 'val1'}, {'name': 'key2', 'value': 'val2'}]
     * @return 形如：{key1: val1, key2: value2 }
     */
    private mapStaticParamsToObject(staticParams: Map<any, any>, queryParams: any, viewModel?: ViewModel<ViewModelState>): any {
        if (!staticParams) {
            return;
        }
        const uiState = viewModel && viewModel.uiStore || {};
        const result: any = {};
        staticParams.forEach((value, key, map) => {

            if (!queryParams.hasOwnProperty(key) && !uiState.hasOwnProperty(key)) {
                result[key] = value;
            }
        });
        return result;
    }
    /**
     * 更新UIState
     */
    private updateUIState(viewModel: ViewModel<ViewModelState>, params: any) {
        if (!viewModel || !params) {
            return;
        }
        const uiState = viewModel.uiStore;

        // 兼容使用string传递params对象的场景
        if (typeof params === 'string' && params !== '') {
            params = JSON.parse(params);
        }
        // 在UIState为参数创建属性
        Object.keys(params).forEach((propName: string) => {
            uiState?.setValue(propName, params[propName]);
        });
    }
    /**
     * 获取功能菜单id
     */
    private getFuncId(queryParams: any): any {
        if (!queryParams) {
            return;
        }
        return queryParams['funcId'];
    }

    /**
     * 获取应用id
     */
    private getAppId(queryParams: any): any {
        if (!queryParams) {
            return;
        }
        return queryParams['appId'];
    }
    private getTabId(queryParams: any) {
        if (!queryParams) {
            return;
        }
        return queryParams['tabId'];
    }
    /**
     * 获取url参数对象
     * @param queryParams url参数
     */
    private getParams(queryParams: any): { [propName: string]: any } {
        if (!queryParams) {
            return {};
        }
        let result: any = {};
        if (queryParams.hasOwnProperty('WEB_FORM_ROUTE_PARAMS')) {
            let webFormRouteParams: string = queryParams['WEB_FORM_ROUTE_PARAMS'];
            if (webFormRouteParams && webFormRouteParams.startsWith('{') && webFormRouteParams.endsWith('}')) {
                webFormRouteParams = decodeURI(encodeURI(webFormRouteParams).replace(/%0A/g, '\\n').replace(/%09/g, '\\t').replace(/%0D/g, '\\r'));
                result = JSON.parse(webFormRouteParams);
            }
            Object.keys(queryParams).forEach((prop) => {
                if (prop !== 'WEB_FORM_ROUTE_PARAMS') {
                    result[prop] = queryParams[prop];
                }
            });
            return result;
        }
        return queryParams;
    }
    private highOrderInvoke(callback: any) {
        return () => {
            if (callback && typeof callback === 'function') {
                callback();
            }
        };
    }
}
