import { InjectionToken } from '@farris/devkit-vue';

export const MESSAGE_BOX_SERVICE_TOKEN = new InjectionToken('@farris/command-services-message-box-service-token');
export const NOTIFY_SERVICE_TOKEN = new InjectionToken('@farris/command-services-notify-service-token');
export const LOADING_SERVICE_TOKEN = new InjectionToken('@farris/command-services-loading-service-token');
export const MODAL_SERVICE_TOKEN = new InjectionToken('@farris/command-services-modal-service-token');