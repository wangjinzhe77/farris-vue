import { BefRepository } from "@farris/bef-vue";
import { ViewModel, ViewModelState } from "@farris/devkit-vue";

export class EntityChangeService {
    constructor(private viewModel: ViewModel<ViewModelState>) { }
    public hasChanges(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const primaryValue = this.viewModel.entityStore?.getCurrentEntity().idValue;
            if (!primaryValue) {
                resolve(false);
            }
            // const changes = this.viewModel.entityState?.entityChangeHistory.getMergedChanges();
            // if (changes && changes.length > 0) {
            //     return resolve(true);
            // }
            const repository = this.viewModel.repository as unknown as BefRepository<any>;
            repository.hasChanges().then((result) => {
                resolve(result);
            })
        });
    }
}