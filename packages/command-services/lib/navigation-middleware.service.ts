import { Token, ViewModel, ViewModelState } from '@farris/devkit-vue';
import { NavigationService } from './navigation.service';
import { TAB_EVENT } from './types';
import { FormMessageService } from './form-message.service';
import { LanguageService } from './language.service';
import { CancelDataService } from './data-services';
import { EntityChangeService } from './entity-change.service';

export class NavigationMiddlewareService {
    constructor(
        private navigationService: NavigationService,
        private formMessageService: FormMessageService,
        private languageService: LanguageService,
        private cancelDataService: CancelDataService,
        private viewModel: ViewModel<ViewModelState>,
        private entityChangeService: EntityChangeService
    ) { }
    /**
     * 菜单关闭前
     */
    public onClosing() {
        this.navigationService.addEventListener(TAB_EVENT.onTabClosing, (options) => {
            return this.entityChangeService.hasChanges().then((changed: boolean) => {
                if (changed) {
                    // 如果需要用户确认就切换到当前tab
                    if (options && options.beforeCloseHandle && typeof options.beforeCloseHandle === 'function') {
                        options.beforeCloseHandle({ selectedChange: true });
                    }
                    const conform = this.formMessageService.confirm(this.languageService.language.confirmClosing);
                    return conform.then((result: boolean) => {
                        if (result) {
                            return this.cancelDataService.cancel(false).then(() => true);
                        }
                        return Promise.resolve(false);
                    }
                    );
                } else {
                    return Promise.resolve(true);
                }
            });
        });
    }
    /**
     * 获取tabid,如果targetId存在则直接使用targetId
     * @description 将用户要查看的数据id转换为运行框架需要的tabId
     * @param params - router参数
     */
    public getTabId(params: string | any): any {
        let paramsObj: any = null;
        if (typeof params == 'string' && params.startsWith('{') && params.endsWith('}')) {
            paramsObj = JSON.parse(params);
        } else {
            paramsObj = params;
        }
        let paramId: string;
        if (paramsObj && paramsObj.hasOwnProperty('id') && !!paramsObj.id) {
            paramId = paramsObj.id;
        } else {
            paramId = Token.create();
        }
        return paramId;
    }
}