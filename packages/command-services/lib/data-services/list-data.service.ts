import { Entity, ViewModel, ViewModelState } from "@farris/devkit-vue";
import { FormLoadingService } from "../form-loading.service";
import { BaseDataService } from "./base-data.service";
import { FormMessageService } from "../form-message.service";
import { LanguageService } from "../language.service";
import { CommandService } from '../command.service';
import { FormNotifyService } from "../form-notify.service";
import { EntityChangeService } from "../entity-change.service";

export class ListDataService extends BaseDataService {
    constructor(
        viewModel: ViewModel<ViewModelState>,
        private formLoadingService: FormLoadingService,
        private formMessageService: FormMessageService,
        private languageService: LanguageService,
        private formNotifyService: FormNotifyService,
        private entityChangeService: EntityChangeService,
    ) {
        super(viewModel);
    }

    public load(filter?: string, sort?: string) {
        const mergedFilterConditions = this.mergeFilterConditions(filter);
        const mergedSortConditions = this.mergeSortConditions(sort);
        const timerId = this.formLoadingService.showLoadingWithDelay();
        const loadPromise = this.repository.getEntities(mergedFilterConditions, mergedSortConditions).then((entities: Entity[]) => {
            this.entityState.loadEntities(entities);
            return entities;
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });
        return loadPromise;
    }
    public filter(filter?: string, sort?: string) {
        return this.load(filter, sort);
    }
    public query(filter: string, sort: string, pageSize: number, pageIndex: number) {
        // TODO: 支持分页
        // pageSize = this.parsePageSize(pageSize);
        // pageIndex = this.parsePageIndex(pageIndex);
        const mergedFilterConditions = this.mergeFilterConditions(filter);
        const mergedSortConditions = this.mergeSortConditions(sort);
        const timerId = this.formLoadingService.showLoadingWithDelay();
        const loadPromise = this.repository.getEntities(mergedFilterConditions, mergedSortConditions, pageSize, pageIndex).then((entities: Entity[]) => {
            this.entityState.loadEntities(entities);
            return entities;
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });
        return loadPromise;
    }
    public removeRows(ids: string[] | string, ifSave?: boolean | string, successMessage?: string) {
        if (typeof ids === 'string') {
            ids = ids.split(',').filter((item) => item);
        }
        if (!ids || ids.length < 1) {
            this.formNotifyService.warning(this.languageService.language.pleaseSelectDeleteData);
            return Promise.reject();
        }
        ifSave = this.parseBoolean(ifSave, true);
        return this.formMessageService.confirm(this.languageService.language.confirmDeletion).then((result: boolean | unknown) => {
            if (!result) {
                return Promise.reject();
            }
            const timerId = this.formLoadingService.showLoadingWithDelay();
            const removeMethod = ifSave ? 'removeEntitiesAndSave' : 'removeEntitiesByIds';
            return this.repository[removeMethod](ids).then(() => {
                this.entityState.removeEntitiesByIds(ids);
                const parsedMessage = this.parseSuccessMessage(successMessage);
                this.displayMessage(parsedMessage, this.languageService.language.deleteSuccess);
            }).finally(() => {
                this.formLoadingService.hideDelayLoading(timerId);
            });
        });
    }
    public remove(id: string, ifSave?: boolean | string, successMessage?: string, confirm: boolean | string = true, breakable: boolean | string = true) {
        if (!id) {
            this.formNotifyService.warning(this.languageService.language.pleaseSelectDeleteData);
            return Promise.reject();
        }
        return this.formMessageService.confirm(this.languageService.language.confirmDeletion).then((result: boolean | unknown) => {
            if (!result) {
                return Promise.reject();
            }
            const timerId = this.formLoadingService.showLoadingWithDelay();
            ifSave = (ifSave === false || ifSave === 'false') ? false : true;
            const parsedMessage = this.parseSuccessMessage(successMessage);
            const remove = ifSave ? this.repository.removeAndSaveEntityById(id) : this.repository.removeEntityById(id);
            const removePromsie = remove.then(() => {
                this.entityState.removeEntityById(id);
                this.displayMessage(parsedMessage, this.languageService.language.deleteSuccess);
                if(!ifSave){
                    return Promise.reject();
                }
            }).finally(() => {
                this.formLoadingService.hideDelayLoading(timerId);
            });;
            return removePromsie;
        });

    }
    public refreshAfterRemoving(loadCmdName: string, loadCmdFrameId: string) {
        if (this.viewModel && loadCmdName && loadCmdFrameId) {
            const commandService = this.viewModel.getInjector().get<CommandService>(CommandService, undefined);
            return commandService.execute(loadCmdName, loadCmdFrameId);
        }
        return this.load();
    }
    public refresh(loadCmdName: string, loadCmdFrameId: string) {
        if (this.viewModel && loadCmdName && loadCmdFrameId) {
            const commandService = this.viewModel.getInjector().get<CommandService>(CommandService, undefined);
            return commandService.execute(loadCmdName, loadCmdFrameId);
        }
        return this.load();
    }
    public append() {
        const timerId = this.formLoadingService.showLoadingWithDelay();
        const createPromise = this.repository.createEntity().then((entity: Entity) => {
            this.entityState.appendEntities([entity]);
            return entity;
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });
        return createPromise;
    }
    public insert(position: number | string) { }

    public queryChild(filter: string, sort: string) { }

    public save(successMessage?: string) {
        const timerId = this.formLoadingService.showLoadingWithDelay();
        return this.repository.saveEntityChanges().then(() => {
            const parsedMessage = this.parseSuccessMessage(successMessage);
            this.displayMessage(parsedMessage, this.languageService.language.saveSuccess);
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });
    }

    public cancel() {
        return this.entityChangeService.hasChanges().then((changed: boolean) => {
            const confirm = changed ? this.formMessageService.confirm(this.languageService.language.confirmCancel) : Promise.resolve(true);
            return confirm.then((result: boolean) => {
                if (result) {
                    const timerId = this.formLoadingService.showLoadingWithDelay();
                    return this.repository.cancelEntityChanges().finally(() => {
                        this.formLoadingService.hideDelayLoading(timerId);
                    });
                }
            });
        });
    }

    public revert() {
        const timerId = this.formLoadingService.showLoadingWithDelay();
        return this.repository.cancelEntityChanges().finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });
    }

    public refreshWhenConditionMet(condition: string, commandName: string, frameId: string) { }

    /**
     * 合并过滤条件
     * @param filters 
     * @returns 
     */
    private mergeFilterConditions(filters?: string | any[]): string[] {
        filters = !filters ? '[]' : filters;
        if (typeof filters === 'string') {
            filters = JSON.parse(filters) as string[];
        }
        if (filters.length > 0) {
            filters.at(filters.length - 1).Relation = 0;
        }
        return filters;
    }
    /**
     * 合并排序条件
     * @param sorts 
     * @returns 
     */
    private mergeSortConditions(sorts?: string | string[]): string[] {
        sorts = !sorts ? '[]' : sorts;
        if (typeof sorts === 'string') {
            sorts = JSON.parse(sorts) as string[];
        }
        return sorts;
    }
}