import { ViewModel, Entity, ViewModelState } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';
import { FormLoadingService } from '../form-loading.service';

/**
 * 更新数据服务
 */
class UpdateDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>, private formLoadingService: FormLoadingService) {
        super(viewModel);
    }

    /**
     * 加载实体
     */
    public update(id: string): Promise<void> {
        const timerId = this.formLoadingService.showLoadingWithDelay();
        const loadPromise = this.repository.getEntityById(id).then((newEntity: Entity) => {
            const newEntityData = newEntity.toJSON();
            this.viewModel.entityStore?.updateEntityById(id, newEntityData);
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });

        return loadPromise;
    }
}

export { UpdateDataService };
