import { ViewModel, Entity, EntityState, ViewModelState, EntityStore } from '@farris/devkit-vue';
import { BefRepository } from '@farris/bef-vue';
import { inject } from 'vue';
import { FormNotifyService } from '../form-notify.service';

/**
 * 基础数据服务
 */
class BaseDataService {
    /**
     * 视图模型
     */
    protected viewModel: ViewModel<ViewModelState>;

    /**
     * 数据仓库
     */
    protected repository: BefRepository<Entity>;

    /**
     * 实体状态
     */
    protected entityState: EntityStore<EntityState<Entity>>;

    /**
     * 表单
     */
    // protected form: Form;

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        this.viewModel = viewModel;
        this.repository = viewModel.repository as unknown as BefRepository<Entity>;
        this.entityState = viewModel.entityStore as EntityStore<EntityState<Entity>>;
        // this.form = viewModel.form as Form;
    }

    /**
     * 获取服务实例
     */
    public getService<T>(token: any, defaultValue?: any): T {
        const injector = this.viewModel.getInjector();
        return injector.get<T>(token, defaultValue);
    }
    /**
     * 转换成功消息
     * @param successMessage 
     * @returns 
     */
    protected parseSuccessMessage(successMessage?: string) {
        if (successMessage && successMessage.trim()) {
            let showMessage: boolean = true;
            if (successMessage.startsWith('{') && successMessage.endsWith('}')) {
                try {
                    const options = JSON.parse(successMessage);
                    if (options.showMessage === false) {
                        showMessage = false;
                    }
                } catch { }
            }
            return { hasMessage: true, showMessage, message: successMessage };
        }
        return { hasMessage: false };
    }
    protected parseBoolean(value?: string | boolean, defaultValue: boolean = false): boolean {
        if (typeof value === 'boolean') {
            return value;
        }
        if (typeof value !== 'string') {
            return defaultValue;
        }
        if (value.toLowerCase().trim() === 'true') {
            return true;
        } else if (value.toLowerCase().trim() === 'false') {
            return false;
        } else {
            return defaultValue;
        }
    }
    protected displayMessage(parsedMessage: any, defaultMessage: string) {
        const { hasMessage, showMessage = true, message = null } = parsedMessage;
        if (showMessage === false) {
            return;
        }
        const formNotifyService = this.viewModel.getInjector().get<FormNotifyService>(FormNotifyService);
        if (formNotifyService) {
            const text = hasMessage ? message : defaultMessage;
            formNotifyService.success(text);
        }
    }
}

export { BaseDataService };