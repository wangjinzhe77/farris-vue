import { ViewModel, Entity, ViewModelState } from '@farris/devkit-vue';
import { BefRepository } from '@farris/bef-vue';
import { BaseDataService } from './base-data.service';
import { FormLoadingService } from '../form-loading.service';

/**
 * 数据加载服务
 */
class LoadDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>, private formLoadingService: FormLoadingService) {
        super(viewModel);
    }

    /**
     * 加载数据
     * @param filters 过滤条件
     * @param sorts 排序条件
     */
    public load(filters?: string | string[], sorts?: string | string[], pageSize?: number | null, pageIndex?: number | null): Promise<Entity[]> {
        pageSize = this.parsePageSize(pageSize);
        pageIndex = this.parsePageIndex(pageIndex);
        const mergedFilterConditions = this.mergeFilterConditions(filters);
        const mergedSortConditions = this.mergeSortConditions(sorts);
        const timerId = this.formLoadingService.showLoadingWithDelay();
        // TODO: 支持分页
        const loadPromise = this.repository.getEntities(mergedFilterConditions, mergedSortConditions).then((entities: Entity[]) => {
            this.entityState.loadEntities(entities);
            return entities;
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });
        return loadPromise;
    }

    /**
     *加载实体
     */
    public loadById(id: string): Promise<Entity> {
        const timerId = this.formLoadingService.showLoadingWithDelay();
        const loadPromise = this.repository.getEntityById(id).then((entity: Entity) => {
            this.entityState.loadEntities([entity]);
            return entity;
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });

        return loadPromise;
    }
    /**
     * 转换分页大小
     * @param pageSize 分页大小
     * @returns 
     */
    private parsePageSize(pageSize: number | null | undefined) {
        const repository = this.viewModel.repository as unknown as BefRepository<any>;
        // if (pageSize !== 0) {
        //     pageSize = pageSize || repository.pageSize;
        // }
        return pageSize;
    }
    /**
     * 转换页码
     * @param pageIndex 页码
     * @returns 
     */
    private parsePageIndex(pageIndex: number | null | undefined) {
        const repository = this.viewModel.repository as unknown as BefRepository<any>;
        // if (!pageIndex) {
        //     pageIndex = repository.pageIndex;
        // }
        return pageIndex;
    }
    /**
     * 合并过滤条件
     * @param filters 
     * @returns 
     */
    private mergeFilterConditions(filters?: string | any[]): string[] {
        filters = !filters ? '[]' : filters;
        if (typeof filters === 'string') {
            filters = JSON.parse(filters) as string[];
        }
        if (filters.length > 0) {
            filters.at(0).Relation = 0;
        }
        return filters;
    }
    /**
     * 合并排序条件
     * @param sorts 
     * @returns 
     */
    private mergeSortConditions(sorts?: string | string[]): string[] {
        sorts = !sorts ? '[]' : sorts;
        if (typeof sorts === 'string') {
            sorts = JSON.parse(sorts) as string[];
        }
        return sorts;
    }

}

export { LoadDataService };
