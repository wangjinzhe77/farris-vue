import { ViewModel, Entity, ViewModelState } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';
import { FormLoadingService } from '../form-loading.service';
import { FormNotifyService } from '../form-notify.service';
import { LanguageService } from '../language.service';
import { EntityChangeService } from '../entity-change.service';
import { FormMessageService } from '../form-message.service';
import { UpdateDataService } from './update-data.service';

export class CardDataService extends BaseDataService {
    constructor(
        viewModel: ViewModel<ViewModelState>,
        private formLoadingService: FormLoadingService,
        private formNotifyService: FormNotifyService,
        private languageService: LanguageService,
        private entityChangeService: EntityChangeService,
        private formMessageService: FormMessageService,
    ) {
        super(viewModel);
    }

    public load(id: string) {
        const timerId = this.formLoadingService.showLoadingWithDelay();
        const loadPromise = this.repository.getEntityById(id).then((newEntity: Entity) => {
            // const newEntityData = newEntity.toJSON();
            this.viewModel.entityStore?.loadEntities([newEntity]);
            // this.viewModel.entityState?.getCurrentEntity().loadData(newEntityData);
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });

        return loadPromise;
    }
    public onLoading(transitionActionParamName: string) {

    }

    public add() {
        const timerId = this.formLoadingService.showLoadingWithDelay();
        const createPromise = this.repository.createEntity().then((entity: Entity) => {
            this.entityState.loadEntities([entity]);
            return entity;
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });
        return createPromise;
    }

    public cascadeAdd() {
        let bindingPaths: any[] = [];
        const viewModels = this.viewModel.getModule().getViewModels();
        viewModels.forEach((viewModel) => {
            if (viewModel.bindingPath === '/') {
                return;
            }
            bindingPaths.push(viewModel.bindingPath);
        });
        bindingPaths = bindingPaths.map((path) => path.split('/').filter((p: string) => p)).sort((a, b) => a.length - b.length);
        const timerId = this.formLoadingService.showLoadingWithDelay();
        return this.repository.createEntity().then((entity: Entity) => {
            this.entityState.appendEntities([entity]);
            const rid = entity.idValue;
            if (bindingPaths && bindingPaths.length > 0) {
                return bindingPaths.reduce((promiseChain, bindingPath) => {
                    return promiseChain.then(() => {
                        const fpath = this.getPath('/' + bindingPath.join('/'), rid);
                        return this.repository.createEntityByPath(fpath).then((entity: Entity) => {
                            this.entityState.appendEntitesByPath(bindingPath, [entity]);
                        });
                    });
                }, Promise.resolve());
            } else {
                return Promise.resolve([]);
            }
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });
    }

    public checkBeforeUpdate() {
        const currentEntity = this.entityState.getCurrentEntity();
        const id = currentEntity.idValue;
        if (!id) {
            this.formNotifyService.warning(this.languageService.language.noDataExist);
            return Promise.reject();
        }
        return Promise.resolve();
    }
    public edit() {
        return this.update();
    }
    public update() {
        const currentEntity = this.entityState.getCurrentEntity();
        const id = currentEntity.idValue;
        if (!id) {
            return Promise.reject();
        }
        const timerId = this.formLoadingService.showLoadingWithDelay();
        const loadPromise = this.repository.getEntityById(id).then((newEntity: Entity) => {
            const newEntityData = newEntity.toJSON();
            this.viewModel.entityStore?.updateEntityById(id, newEntityData);
            // this.viewModel.entityStore?.getCurrentEntity().loadData(newEntityData);
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });

        return loadPromise;
    }
    public updateWithoutEmpty() {
        const currentEntity = this.entityState.getCurrentEntity();
        const id = currentEntity.idValue;
        if (!id) {
            return Promise.resolve();
        } else {
            return this.update();
        }
    }

    public save(successMessage?: string) {
        const timerId = this.formLoadingService.showLoadingWithDelay();
        return this.repository.saveEntityChanges().then(() => {
            const parsedMessage = this.parseSuccessMessage(successMessage);
            this.displayMessage(parsedMessage, this.languageService.language.saveSuccess);
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });
    }

    public cancel(showConfirm: boolean = true) {
        return this.entityChangeService.hasChanges().then((changed: boolean) => {
            const confirm = changed && showConfirm ? this.formMessageService.confirm(this.languageService.language.confirmCancel) : Promise.resolve(true);
            return confirm.then((result: boolean) => {
                if (!result) {
                    return Promise.reject();
                }
                const timerId = this.formLoadingService.showLoadingWithDelay();
                return this.repository.cancelEntityChanges().then(() => {
                    const updateService = this.getService<UpdateDataService>(UpdateDataService);
                    const currentEntity = this.entityState.getCurrentEntity();
                    return updateService.update(currentEntity.idValue);
                }).finally(() => {
                    this.formLoadingService.hideDelayLoading(timerId);
                });
            });
        });
    }

    public cancelWithCheck() {
        return this.cancel();
    }
    public reload() {
        const currentEntity = this.entityState.getCurrentEntity();
        const id = currentEntity.idValue;
        return this.load(id);
    }

    public cancelWithoutCheck() {
        return this.revert();
    }

    public revert() {
        const timerId = this.formLoadingService.showLoadingWithDelay();
        return this.repository.cancelEntityChanges().finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });
    }

    public loadPaged() {

    }

    private getPath(bindingPath: string, rid: string): string {
        let path = '/' + rid;
        const currentPaths = [];
        const subPaths = bindingPath.split('/');
        if (subPaths.length > 0) {
            // eg:bindingPath形如/edus/grades,split后是['', 'edus', 'grades']
            // 因此index从1开始
            for (let index = 1; index < subPaths.length - 1; index++) {
                const subPath = subPaths[index];
                currentPaths.push(subPath);
                const subData = this.viewModel.entityStore?.getCurrentEntityByPath(`/${currentPaths.join('/')}`);
                const currentId = subData?.idValue;
                if (!currentId) {
                    throw Error(`获取子表完整路径出错，找不到${subData}对应的子表，或对应子表没有当前行。`);
                }
                path += `/${subPath}/${currentId}`;
            }
        }
        path += '/' + subPaths[subPaths.length - 1];

        return path;
    }
}
