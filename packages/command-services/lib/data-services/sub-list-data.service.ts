import { Entity, ViewModel, ViewModelState } from "@farris/devkit-vue";
import { BaseDataService } from "./base-data.service";
import { FormLoadingService } from "../form-loading.service";
import { FormMessageService } from "../form-message.service";
import { LanguageService } from "../language.service";
import { FormNotifyService } from "../form-notify.service";

export class SubListDataService extends BaseDataService {
    constructor(
        viewModel: ViewModel<ViewModelState>,
        private formLoadingService: FormLoadingService,
        private formMessageService: FormMessageService,
        private languageService: LanguageService,
        private formNotifyService: FormNotifyService,
    ) {
        super(viewModel);
    }
    public add() {
        const path = this.getPath();
        const primaryValue = this.viewModel.entityStore?.getCurrentEntity().idValue;
        if (!primaryValue) {
            console.error('缺少主表数据');
            return Promise.reject();
        }
        const timerId = this.formLoadingService.showLoadingWithDelay();
        this.repository.createEntityByPath(path, undefined).then((entity: Entity) => {
            this.entityState.appendEntitesByPath(this.viewModel.bindingPath, [entity]);
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });
    }
    public remove(id: string, successMessage?: string) {
        if (!id) {
            this.formNotifyService.warning(this.languageService.language.pleaseSelectDeleteData);
            return Promise.reject();
        }
        const path = this.getPath();
        return this.formMessageService.confirm(this.languageService.language.confirmDeletion).then((result: boolean | unknown) => {
            if (!result) {
                return Promise.reject();
            }
            const timerId = this.formLoadingService.showLoadingWithDelay();
            return this.repository.removeEntityByPath(path, id).then(() => {
                this.entityState.removeEntitiesByPath(this.viewModel.bindingPath, [id]);
                const parsedMessage = this.parseSuccessMessage(successMessage);
                this.displayMessage(parsedMessage, this.languageService.language.deleteSuccess);
            }).finally(() => {
                this.formLoadingService.hideDelayLoading(timerId);
            });
        });
    }
    public removeAndSave(id: string, successMessage?: string) {
        if(!id){
            this.formNotifyService.warning(this.languageService.language.pleaseSelectDeleteData);
            return Promise.reject();
        }
        const path = this.getPath();
        return this.formMessageService.confirm(this.languageService.language.confirmDeletion).then((result: boolean | unknown) => {
            if (!result) {
                return Promise.reject();
            }
            const timerId = this.formLoadingService.showLoadingWithDelay();
            return this.repository.removeEntityByPath(path, id).then(() => {
                return this.repository.saveEntityChanges().then(() => {
                    this.entityState.removeEntitiesByPath(this.viewModel.bindingPath, [id]);
                    const parsedMessage = this.parseSuccessMessage(successMessage);
                    this.displayMessage(parsedMessage, this.languageService.language.deleteSuccess);
                });
            }).finally(() => {
                this.formLoadingService.hideDelayLoading(timerId);
            });
        });
    }

    public insert(position: number | string) { }

    public removeChildrenByIds(ids: string[] | string, successMessage?: string) { }

    public move(direction: string, field: string, targets: string | string[]) { }

    private getPath(): string {
        const bindingPath = this.viewModel.bindingPath;
        const rid = this.viewModel.entityStore?.getCurrentEntity().idValue; // root表数据id
        let path = '/' + rid;
        const currentPaths = [];
        const subPaths = bindingPath.split('/');
        if (subPaths.length > 0) {
            // eg:bindingPath形如/edus/grades,split后是['', 'edus', 'grades']
            // 因此index从1开始
            for (let index = 1; index < subPaths.length - 1; index++) {
                const subPath = subPaths[index];
                currentPaths.push(subPath);
                const subData = this.viewModel.entityStore?.getEntityListByPath(`/${currentPaths.join('/')}`);
                const currentId = subData?.getCurrentEntity().idValue;
                if (!subData || !currentId) {
                    this.formNotifyService.warning(this.languageService.language.pleaseSelectDetailFormData);
                    throw Error(`获取子表完整路径出错，找不到${subData}对应的子表，或对应子表没有当前行。`);
                }
                path += `/${subPath}/${currentId}`;
            }
        }
        path += '/' + subPaths[subPaths.length - 1];

        return path;
    }
}
