import { ViewModel, Entity, ViewModelState } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';
import { FormNotifyService } from '../form-notify.service';
import { FormLoadingService } from '../form-loading.service';
import { LanguageService } from '../language.service';

/**
 * 数据保存服务
 */
class SaveDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(public viewModel: ViewModel<ViewModelState>,
        private formNotifyService: FormNotifyService,
        private formLoadingService: FormLoadingService,
        private languageService: LanguageService
    ) {
        super(viewModel);
    }

    /**
     * 保存成功
     */
    public save() {
        const timerId = this.formLoadingService.showLoadingWithDelay();
        return this.repository.saveEntityChanges().then(() => {
            this.formNotifyService.success(this.languageService.language.saveSuccess);
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });
    }
}

export { SaveDataService }