import { ViewModel, ViewModelState } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';
import { FormMessageService } from '../form-message.service';
import { FormLoadingService } from '../form-loading.service';
import { LanguageService } from '../language.service';

/**
 * 数据删除服务
 */
class RemoveDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(
        viewModel: ViewModel<ViewModelState>,
        private formMessageService: FormMessageService,
        private formLoadingService: FormLoadingService,
        private languageService: LanguageService
    ) {
        super(viewModel);
    }

    /**
     * 删除方法
     * @param id 实体主键
     * @param ifSave 是否保存
     * @param successMessage 删除成功提示消息
     */
    public remove(id?: string, ifSave?: boolean | string, successMessage?: string) {
        if (!id) {
            const currentEntity = this.entityState.getCurrentEntity();
            id = currentEntity.idValue;
        }
        if (!id) {
            // TODO: display error
            return;
        }
        if (!this.formMessageService) {
            return;
        }

        return this.formMessageService.confirm(this.languageService.language.confirmDeletion).then((result: boolean | unknown) => {
            if (!result) {
                return Promise.reject();
            }
            const timerId = this.formLoadingService.showLoadingWithDelay();
            ifSave = (ifSave === false || ifSave === 'false') ? false : true;
            const parsedMessage = this.parseSuccessMessage(successMessage);
            const removePromsie = this.repository.removeAndSaveEntityById(id).then(() => {
                this.entityState.removeEntityById(id);
                this.displayMessage(parsedMessage, this.languageService.language.deleteSuccess);
            }).finally(() => {
                this.formLoadingService.hideDelayLoading(timerId);
            });;
            return removePromsie;
        });
    }
    /**
     * 批量删除数据
     * @param ids 数据主键数组
     * @param ifSave 是否保存
     * @param successMessage 保存成功提示信息
     */
    public removeRows(ids: string[] | string, ifSave?: boolean | string, successMessage?: string) {
        if (typeof ids === 'string') {
            ids = ids.split(',').filter((item) => item);
        }
        ifSave = this.parseBoolean(ifSave, true);
        const parsedMessage = this.parseSuccessMessage(successMessage);
        const timerId = this.formLoadingService.showLoadingWithDelay();
        return this.repository.removeEntitiesByIds(ids).then(() => {
            this.entityState.removeEntitiesByIds(ids);
            this.displayMessage(parsedMessage, this.languageService.language.deleteSuccess);
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });
    }

    public removeById(id: string, ifSave: string | boolean) { }

    public removeAndSaveById(id: string) { }

    public refreshAfterRemoving(commandName: string, componentId: string) { }

    public removeAndSaveByIdForTree(id: string) { }



}

export { RemoveDataService };
