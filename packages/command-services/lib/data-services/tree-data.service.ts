import { Entity, EntityFieldSchema, FieldSchema, ViewModel, ViewModelState } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';
import { FormLoadingService } from '../form-loading.service';
import { HierarchyType, ViewState } from '../types';
import { BefParentTreeRepository, BefTreeRepository, BefPathTreeRepository } from '@farris/bef-vue';
import { FormNotifyService } from '../form-notify.service';
import { FormMessageService } from '../form-message.service';
import { LanguageService } from '../language.service';
import { StateService } from '../state.service';
import { EntityChangeService } from '../entity-change.service';
import { ParentTreeNodeService, PathTreeNodeService } from '../tree-node-services/index';
const LAST_MODIFIED_ID = '__LAST_MODIFIED_ID__';
export class TreeDataService extends BaseDataService {
    constructor(
        public viewModel: ViewModel<ViewModelState>,
        private formLoadingService: FormLoadingService,
        private formNotifyService: FormNotifyService,
        private formMessageService: FormMessageService,
        private languageService: LanguageService,
        private stateService: StateService,
        private entityChangeService: EntityChangeService,
    ) {
        super(viewModel);
    }
    public load(filters?: string, sorts?: string): Promise<any> {
        const filtersArray = this.parseConditions(filters);
        const sortsArray = this.parseConditions(sorts);
        const timerId = this.formLoadingService.showLoadingWithDelay();
        const loadPromise = this.repository.getEntities(filtersArray, sortsArray).then((entities: Entity[]) => {
            this.entityState.loadEntities(entities);
            return entities;
        }).finally(() => {
            this.lastModifiedId = null;
            this.formLoadingService.hideDelayLoading(timerId);
        });
        return loadPromise;
    }
    public loadByLevel(filters?: string, sorts?: string, frozenCurrentRow?: boolean | string) { }
    public loadFullTree(virtualPropertyName: string, fullTreeType: string, loadType: string, filters: string, frozenCurrentRow?: boolean | string) { }
    public addSibling(id: string) {
        id = id ? id : this.viewModel.entityStore?.getCurrentEntity().idValue;
        // 参数检查
        if (!id) {
            id = '';
        }

        // 获取分级方式
        const hierarchyType = this.getHierarchyType();
        if (!hierarchyType) {
            console.error(`Invalid hierarchy key!`);
            return;
        }
        const treeRepository = this.getTreeRepository(hierarchyType);
        // 执行取数
        const timerId = this.formLoadingService.showLoadingWithDelay();
        return treeRepository?.addSibling(id).finally(() => {
            this.lastModifiedId = id;
            this.stateService.transitTo(ViewState.Add);
            this.formLoadingService.hideDelayLoading(timerId);
        });
    }
    /**
     * 新增子级
     * @param id 
     * @returns 
     */
    public addChild(id: string) {
        id = id ? id : this.viewModel.entityStore?.getCurrentEntity().idValue;
        // 参数检查
        if (!id) {
            this.formNotifyService.warning(this.languageService.language.pleaseSelectParentNode);
            return Promise.reject();
        }
        const hierarchyType = this.getHierarchyType();
        if (!hierarchyType) {
            console.error(`Invalid hierarchy key!`);
            return Promise.reject();
        }
        const treeRepository = this.getTreeRepository(hierarchyType);
        const timerId = this.formLoadingService.showLoadingWithDelay();
        return treeRepository?.addChild(id).finally(() => {
            this.lastModifiedId = id;
            this.stateService.transitTo(ViewState.Add);
            this.formLoadingService.hideDelayLoading(timerId);
        });
    }
    public remove(id: string, successMessage?: string) {
        id = id ? id : this.viewModel.entityStore?.getCurrentEntity().idValue;
        if (!id) {
            this.formNotifyService.warning(this.languageService.language.pleaseSelectDeleteData);
            return Promise.reject();
        }
        const entity = this.viewModel.entityStore?.getEntityById(id);
        const treeService = this.getTreeService();
        const hasChild = treeService.hasChildNodes(entity, this.getHierarchyKey());
        if (hasChild) {
            this.formNotifyService.warning(this.languageService.language.deleteChildFirst);
            return Promise.reject();
        }
        return this.formMessageService.confirm(this.languageService.language.confirmDeletion).then((result: boolean) => {
            if (result) {
                return this.repository.removeAndSaveEntityById(id).then(() => {
                    this.updateCurrentRow(id);
                    this.entityState.removeEntityById(id);
                    const parsedMessage = this.parseSuccessMessage(successMessage);
                    this.displayMessage(parsedMessage, this.languageService.language.deleteSuccess);
                });
            }
        });
    }
    public save() {
        const timerId = this.formLoadingService.showLoadingWithDelay();
        return this.repository.saveEntityChanges().finally(() => {
            this.stateService.transitTo(ViewState.Initial);
            this.lastModifiedId = null;
            this.formLoadingService.hideDelayLoading(timerId);
        });
    }
    public cancel() {
        const currentId = this.viewModel.entityStore?.getCurrentEntity().idValue;
        if (!currentId) {
            return Promise.resolve();
        }
        return this.entityChangeService.hasChanges().then((changed: boolean) => {
            if (changed) {
                return this.formMessageService.confirm(this.languageService.language.confirmCancel).then((result: boolean) => {
                    if (!result) {
                        return Promise.reject();
                    }
                    const timerId = this.formLoadingService.showLoadingWithDelay();
                    return this.repository.cancelEntityChanges().then(() => {
                        if (this.stateService.is(ViewState.Add)) {
                            this.viewModel.entityStore?.removeEntityById(currentId);
                            // 删除后重新计算当前行
                            this.revertCurrentRow();
                            this.formLoadingService.hideDelayLoading(timerId);
                        } else {
                            return this.repository.getEntityById(currentId).then((newEntity: Entity) => {
                                const newEntityData = newEntity.toJSON();
                                this.viewModel.entityStore?.updateEntityById(currentId, newEntityData);
                                return Promise.resolve(true);
                            }).finally(() => {
                                this.formLoadingService.hideDelayLoading(timerId);
                            });
                        }
                        this.stateService.transitTo(ViewState.Initial);
                        return Promise.resolve(true);
                    });
                });
            } else {
                const timerId = this.formLoadingService.showLoadingWithDelay();
                return this.repository.cancelEntityChanges().finally(() => {
                    this.formLoadingService.hideDelayLoading(timerId);
                });
            }
        });
    }
    public setCurrentId(id: string) { }

    public selectFirstRow() { }
    /**
     * 获取树数据仓库
     * @param hierarchyType 
     * @returns 
     */
    private getTreeRepository(hierarchyType: HierarchyType): BefTreeRepository<Entity> | null {
        const injector = this.viewModel.getInjector();
        switch (hierarchyType) {
            case HierarchyType.parent:
                return injector.get(BefParentTreeRepository);
            case HierarchyType.path:
                return injector.get(BefPathTreeRepository);
        }
    }
    /**
     * 获取分级码信息
     * @returns 
     */
    private getHierarchyType(): HierarchyType | null {
        const hierarchyKey = this.getHierarchyKey();
        const hierarchyPaths = hierarchyKey.split('/').filter((item: any) => item);
        const entitySchema = this.viewModel.entityStore?.getEntitySchema();
        const entityPath = this.viewModel.entityStore?.createPath(hierarchyPaths);
        if (!entityPath) {
            return null;
        }
        const fieldSchema = entitySchema?.getFieldSchemaByPath(entityPath) as EntityFieldSchema;
        const fields = fieldSchema?.entitySchema.getFieldSchemas();
        const index = fields.findIndex((field: FieldSchema) => field.name === 'parentElement');
        if (index == -1) {
            return HierarchyType.path;
        } else {
            return HierarchyType.parent;
        }
    }
    private getHierarchyKey() {
        const injector = this.viewModel.getInjector();
        const rootViewModel = this.viewModel.getRoot() as ViewModel<ViewModelState>;
        // TODO: 需支持子表树，暂时短路
        return rootViewModel.uiStore?.getValue(`hierarchy_`);
    }
    private revertCurrentRow() {
        const result = this.selectLastModifyItem();
        if (!result) {
            this.updateCurrentRow();
        }
    }
    private selectLastModifyItem(): boolean {
        const entities = this.viewModel.entityStore?.getEntities();
        if (entities && entities.length > 0) {
            const index = entities.findIndex((entity: Entity) => entity.idValue === this.lastModifiedId);
            if (this.lastModifiedId && index !== -1) {
                this.viewModel.entityStore?.changeCurrentEntityByPath(this.viewModel.bindingPath, this.lastModifiedId);
                this.lastModifiedId = null;
                return true;
            }
        }
        this.lastModifiedId = null;
        return false;
    }
    private updateCurrentRow(previousId?: string) {
        const entities = this.viewModel.entityStore?.getEntities();
        const hierarchyKey: string = this.getHierarchyKey().split('/').filter((item: any) => item).join('');
        const treeService = this.getTreeService();
        if (!entities || entities.length < 1) {
            return;
        }
        let nodeId: string;
        if (!previousId) {
            // 将第一个根节点设为当前行
            nodeId = treeService.getFirstNodeId(entities, hierarchyKey);
        } else {
            // 找到该节点的兄弟节点或父节点
            nodeId = treeService.getNextNodeId(entities, hierarchyKey, previousId);
        }
        if (nodeId) {
            this.viewModel.entityStore?.changeCurrentEntityById(nodeId);
        }
    }
    private getTreeService() {
        const hierarchy = this.getHierarchyType();
        if (hierarchy === HierarchyType.parent) {
            return new ParentTreeNodeService();
        } else {
            return new PathTreeNodeService();
        }
    }
    private get lastModifiedId() {
        return this.viewModel.getModule().getContext().getParam(LAST_MODIFIED_ID);
    }
    private set lastModifiedId(id: string | null) {
        this.viewModel.getModule().getContext().setParam(LAST_MODIFIED_ID, id);
    }
    private parseConditions(conditions?: string): any[] {
        const conditionsString = (!conditions || conditions === '') ? '[]' : conditions;
        return JSON.parse(conditionsString);
    }
}