import { ViewModel, ViewModelState } from '@farris/devkit-vue';

export class SubTreeDataService {    
    constructor(private viewModel: ViewModel<ViewModelState>) { }

    public addSubSibling(){}
    public addSubChild(){}

    public remove(id: string, successMsg?: string){}
}