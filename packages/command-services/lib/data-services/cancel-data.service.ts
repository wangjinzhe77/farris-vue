import { ViewModel, ViewModelState } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';
import { UpdateDataService } from './update-data.service';
import { FormLoadingService } from '../form-loading.service';
import { EntityChangeService } from '../entity-change.service';
import { FormMessageService } from '../form-message.service';
import { LanguageService } from '../language.service';

/**
 * 数据取消服务
 */
class CancelDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(
        viewModel: ViewModel<ViewModelState>,
        private formLoadingService: FormLoadingService,
        private entityChangeService: EntityChangeService,
        private formMessageService: FormMessageService,
        private languageService: LanguageService
    ) {
        super(viewModel);
    }

    /**
     * 取消方法
     */
    public cancel(showConfirm: boolean = true) {
        return this.entityChangeService.hasChanges().then((changed: boolean) => {
            const confirm = changed && showConfirm ? this.formMessageService.confirm(this.languageService.language.confirmCancel) : Promise.resolve(true);
            return confirm.then((result: boolean) => {
                if (result) {
                    const timerId = this.formLoadingService.showLoadingWithDelay();
                    return this.repository.cancelEntityChanges().then(() => {
                        const updateService = this.getService<UpdateDataService>(UpdateDataService);
                        const currentEntity = this.entityState.getCurrentEntity();
                        return updateService.update(currentEntity.idValue);
                    }).finally(() => {
                        this.formLoadingService.hideDelayLoading(timerId);
                    });
                }
            });
        });
    }
}

export { CancelDataService };
