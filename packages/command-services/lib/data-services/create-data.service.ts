import { ViewModel, Entity, ViewModelState } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';
import { FormLoadingService } from '../form-loading.service';

/**
 * 数据新增服务
 */
class CreateDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>, private formLoadingService: FormLoadingService) {
        super(viewModel);
    }

    /**
     * 新增数据
     */
    public create(): Promise<Entity> {
        const timerId = this.formLoadingService.showLoadingWithDelay();
        const createPromise = this.repository.createEntity().then((entity: Entity) => {
            this.entityState.loadEntities([entity]);
            return entity;
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });
        return createPromise;
    }

    /**
     * 追加新数据
     */
    public append() {
        const timerId = this.formLoadingService.showLoadingWithDelay();
        const createPromise = this.repository.createEntity().then((entity: Entity) => {
            this.entityState.appendEntities([entity]);
            return entity;
        }).finally(() => {
            this.formLoadingService.hideDelayLoading(timerId);
        });
        return createPromise;
    }

}

export { CreateDataService }