import { RequestInfo } from '@farris/bef-vue';

export const AppType = {
    App: 'app',
    Menu: 'menu',
    Other: 'other'
};

export const TAB_EVENT = {
    /**
     * Tab关闭后
     */
    onTabClosed: 'FuncClosed',
    /**
     * Tab关闭前
     */
    onTabClosing: 'beforeFuncCloseEvent',
    /**
     * Tab切换
     */
    onTabSwitched: 'funcSwitchEvent',
    onTabRefresh: 'tabRefresh'
};

export const TAB_QUERY_STRING = {
    TabId: 'tabId',
    AppType: 'appType',
    AppId: 'appId',
    AppEntrance: 'appEntrance',
    FuncId: 'funcId'
};

/**
 * 带RequestInfo的body对象
 */
export interface BodyWithRequestInfo {
    requestInfo: RequestInfo;
    [key: string]: any;
}

export const WEB_FORM_ROUTE_PARAMS_KEY = 'WEB_FORM_ROUTE_PARAMS';
export const enum DataType {
    map = '[object Map]'
}
export namespace GspFramework {
    /**
     * 框架用户信息描述
     */
    export type UserInfo = {
        id: string;
        code: string;
        name: string;
        orgId: string;
        orgName: string;
        lanName: string;
        languageId: string;
        unitId: string;
        unitName: string;
    };
    /**
     * 框架用户服务
     */
    export type UserInfoService = {
        get: () => UserInfo
    };
    /**
     * 框架common服务
     */
    export type CommonService = {
        userInfos: UserInfoService
    };
    export type RuntimeFrameworkService = {
    };
    /**
     * 框架服务
     */
    export type FrameworkService = {
        common: CommonService,
        rtf: RuntimeFrameworkService
    };
}
/**
 * 导航历史
 */
export interface NavigationHistory {
    from: string;
    to: string;
}
export type AtLeastOne<T, K extends keyof T = keyof T> = {
    [P in K]: Required<Pick<T, P>> & Partial<Omit<T, P>>;
}[K];
/**
 * 分级类型
 */
export enum HierarchyType {
    /**
     * 父节点树
     */
    parent = 'parent',
    /**
     * 分级码树
     */
    path = 'path'
}

export enum ViewState {
    Initial = 'Initial',
    Add = 'Add',
    Edit = 'Edit'
}