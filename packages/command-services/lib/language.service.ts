import { Language, Locales } from "./languages";
import { LanguageFactory } from "./languages/language-factory";

export class LanguageService {
    /**
     * 默认中文
     */
    private locale: Locales;
    public language: Language;
    constructor() {
        this.locale = localStorage.getItem('languageCode') as Locales || Locales.zh;
        this.language = LanguageFactory.create(this.locale);
    }
}
