export class StateService {
    private state: any = null;
    constructor() {
    }
    public transitTo(state: any) {
        this.state = state;
    }
    public is(state: any) {
        return this.state === state;
    }
}