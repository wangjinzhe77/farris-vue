import { Injector, Module, ViewModel, ViewModelState } from '@farris/devkit-vue';

export class CommandService {
    constructor(private injector: Injector, private viewModel: ViewModel<ViewModelState>) { }
    public execute(commandName: string, componentId?: string) {
        if (!commandName) {
            console.warn('Invalid command name!');
            return;
        }
        if (componentId) {
            if (componentId.startsWith('#{')) {
                componentId = componentId.slice(2, componentId.length - 1);
            }
            const module = this.injector.get<Module>(Module);
            const viewModel = module.getViewModel(componentId);
            // const viewModel = viewModels.find((viewModel: ViewModel<ViewModelState>)=> viewModel.componentId=== componentId);
            if (!viewModel) {
                console.warn(`There is no viewmodel with id ${componentId}`);
                return;
            }
            return (viewModel as any)[commandName]();
        } else {
            return (this.viewModel as any)[commandName]();
        };

    }
    public suspendFrameContextRowSelectedEvent() { }

    public resumeFrameContextRowSelectedEvent() { }
}