import { QuerystringService } from './querystring.service';
import { GspFramework } from './types';
/**
 * RuntimeFrameworkService
 */
export class RuntimeFrameworkService {
    private rtfService: any;
    private gspFrameworkServiceInstance: GspFramework.FrameworkService;
    constructor(
        private queryStringService: QuerystringService
    ) {
        this.rtfService = this.getRuntimeFrameworkService();
    }
    /**
     * 获取rtf服务
     */
    private getRuntimeFrameworkService() {
        const frameworkService = this.gspFrameworkService;
        return frameworkService && frameworkService.rtf || {};
    }
    private get gspFrameworkService() {
        if (this.gspFrameworkServiceInstance) {
            return this.gspFrameworkServiceInstance;
        }
        let env: any = window;
        while (env.location.origin === env.parent.location.origin && env !== env.parent && !(env as any).isRTFTopWin) {
            env = env.parent;
        }
        this.gspFrameworkServiceInstance = env['gspframeworkService'];
        return this.gspFrameworkServiceInstance;
    }
    public get common(): GspFramework.CommonService {
        return this.gspFrameworkService && this.gspFrameworkService.common;
    }
    // #region common服务
    public get userInfo(): GspFramework.UserInfo {
        return this.common && this.common.userInfos && this.common.userInfos.get() || null;
    }
    // #endregion
    // #region 导航服务
    /**
     * 打开菜单或应用
     * @param options - options
     */
    openMenu(options: any) {
        if (this.rtfService && this.rtfService.hasOwnProperty('func') && typeof this.rtfService['func']['openMenu'] === 'function') {
            this.rtfService.func.openMenu(options);
        }
    }
    /**
     * 打开菜单或应用
     * @param options - options
     */
    openMenu$(options: any) {
        if (this.rtfService && this.rtfService.hasOwnProperty('func') && typeof this.rtfService['func']['openMenuByStream'] === 'function') {
            return this.rtfService.func.openMenuByStream(options);
        }
        return Promise.reject();
    }
    /**
     * 获取导航实体数据
     * @param tabId - tabid
     * @param callback - callback
     * @param once - once
     */
    getEntityParam(tabId: string, callback: any, once: boolean = true) {
        if (this.rtfService && this.rtfService.hasOwnProperty('func') && typeof this.rtfService['func']['getEntityParam'] === 'function') {
            this.rtfService.func.getEntityParam(tabId, callback, once);
        }
    }
    /**
     * 尝试关闭菜单或应用
     * @param options - optins
     */
    beforeCloseMenu(options: any) {
        if (this.rtfService && this.rtfService.hasOwnProperty('func') && typeof this.rtfService['func']['beforeClose'] === 'function') {
            this.rtfService.func.beforeClose(options);
        }
    }
    /**
     * 关闭菜单
     * @param options - options
     */
    closeMenu(options: any) {
        if (this.rtfService && this.rtfService.hasOwnProperty('func') && typeof this.rtfService['func']['close'] === 'function') {
            this.rtfService.func.close(options);
        }
    }
    /**
     * 获取菜单静态参数
     * @param funcId - 菜单id
     * @param callback - 回调
     */
    getMenuParams(funcId: string, callback: (params: any) => void) {
        if (this.rtfService && this.rtfService.hasOwnProperty('func') && typeof this.rtfService['func']['getMenuParams'] === 'function') {
            this.rtfService.func.getMenuParams(funcId, callback);
        }
    }
    /**
     * 添加事件监听
     * @param token
     * @param handler
     * @param options
     */
    addEventListener(token: string, handler: (value: any) => void, options: any) {
        if (this.rtfService && this.rtfService.hasOwnProperty('frmEvent') && typeof this.rtfService['frmEvent']['eventListener'] === 'function') {
            this.rtfService.frmEvent.eventListener(token, handler, options);
        }
    }
    // TODO: 框架菜单切换事件无法监听
    // #endregion

    // #region 适配层属性
    public get params() {
        return this.queryStringService.parse(window.location.hash);
    }
    /**
     * 获取tabId
     */
    public get tabId() {
        return this.params && this.params['tabId'] || null;
    }
    /**
     * 获取formToken
     */
    public get formToken() {
        return this.params && this.params['formToken'] || null;
    }
    /**
     * 获取funcId
     */
    public get funcId() {
        return this.params && this.params['funcId'] || null;
    }
    // #endregion
}
