/**
 * 查询参数服务
 */
export class QuerystringService {
    /**
     * 解码参数
     * @param queryString search|hash
     */
    public parse(queryString: string): { [propName: string]: any } {
        if (!queryString) {
            return {};
        }
        const hashes = queryString.slice(queryString.indexOf('?') + 1).split('&');
        return hashes.reduce((params, hash) => {
            const split = hash.indexOf('=');
            const key = hash.slice(0, split);
            const val = hash.slice(split + 1);
            return Object.assign(params, { [key]: decodeURIComponent(val) });
        }, {});
    }
}
