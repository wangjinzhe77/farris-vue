
import { LanguageService } from '../../language.service';
import { FormMessageService } from '../../form-message.service';

/**
 * 异常处理基类
 */
export class ExceptionStrategy {
    constructor(
        public messagerService: FormMessageService,
        public languageService: LanguageService) {
    }
}
