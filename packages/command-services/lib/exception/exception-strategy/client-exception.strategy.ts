import { FormMessageService } from "../../form-message.service";
import { LanguageService } from "../../language.service";
import { ClientException, IExceptionStrategy } from "../types";
import { ExceptionStrategy } from "./exception-strategy";

export class ClientExceptionStrategy extends ExceptionStrategy implements IExceptionStrategy {
    constructor(
        public messagerService: FormMessageService,
        public languageService: LanguageService
    ) {
        super(messagerService, languageService);
    }

    handleException(error: ClientException): void {
        this.messagerService.exception(error.message, error.stack);
    }
}