import { FormMessageService } from "../../form-message.service";
import { LanguageService } from "../../language.service";
import { IExceptionStrategy, ServerException } from "../types";
import { ExceptionStrategy } from "./exception-strategy";

/**
 * 致命错误级别异常处理
 */
export class FatalExceptionStrategy extends ExceptionStrategy implements IExceptionStrategy {
    constructor(
        public messagerService: FormMessageService,
        public languageService: LanguageService) {
        super(messagerService, languageService);
    }

    public handleException(error: ServerException): void {
        if (!error) {
            return;
        }
        this.messagerService.error(error.message);
    }
}