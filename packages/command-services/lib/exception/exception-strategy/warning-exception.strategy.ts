import { FormMessageService } from "../../form-message.service";
import { LanguageService } from "../../language.service";
import { IExceptionStrategy, ServerException } from "../types";
import { ExceptionStrategy } from "./exception-strategy";

/**
 * 警告级别异常处理
 */
export class WarningExceptionStrategy extends ExceptionStrategy implements IExceptionStrategy {
    constructor(
        public messagerService: FormMessageService,
        public languageService: LanguageService) {
        super(messagerService, languageService);
    }

    public handleException(error: ServerException): void {
        if (!error) {
            return;
        }
        this.messagerService.warning(error.message);
    }
}