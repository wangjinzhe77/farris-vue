// import moment from "moment";
import { FormMessageService } from "../../form-message.service";
import { LanguageService } from "../../language.service";
import { IExceptionStrategy, ServerException } from "../types";
import { ExceptionStrategy } from "./exception-strategy";

/**
 * 错误级别异常处理
 */
export class ErrorExceptionStrategy extends ExceptionStrategy implements IExceptionStrategy {
    constructor(
        public messagerService: FormMessageService,
        public languageService: LanguageService
    ) {
        super(messagerService, languageService);
    }

    public handleException(error: ServerException): void {
        if (!error) {
            return;
        }

        this.handleErrorLevel(error);
    }

    private handleErrorLevel(error: ServerException): void {
        const message = error.message || '';
        const date = error.date || new Date();
        // TODO: 支持日期格式化
        const localDate = '';//moment(date).format('YYYY-MM-DD HH:mm:ss');
        const detail = error.detail || error.innerMessage || '';

        this.messagerService.exception(message, detail, localDate);

        this.displayError(error);
    }
    /**
     * 在控制台中显示错误信息
     */
    private displayError(error: ServerException) {
        if (!error) {
            return;
        }
        if (!console || !console.error) {
            return;
        }
        console.error(error);
    }
}