import { FormMessageService } from "../../form-message.service";
import { LanguageService } from "../../language.service";
import { Exception, ExceptionLevel, IExceptionStrategy, ServerException } from "../types";
import { ClientExceptionStrategy } from "./client-exception.strategy";
import { ErrorExceptionStrategy } from "./error-exception.strategy";
import { FatalExceptionStrategy } from "./fatal-exception.strategy";
import { InfoExceptionStrategy } from "./info-exception.strategy";
import { UnauthorizedExceptionStrategy } from "./unauthorized-exception.strategy";
import { WarningExceptionStrategy } from "./warning-exception.strategy";

export class ExceptionStrategyFactory {
    public static create(exception: Exception, messagerService: FormMessageService, languageService: LanguageService) {
        if (this.isUnauthorizedException(exception)) {
            return new UnauthorizedExceptionStrategy(messagerService, languageService);
        } else if (this.isServerException(exception)) {
            const serverException = exception as ServerException;
            return this.getExceptionHandleStrategy(serverException.level, messagerService, languageService);
        } else {
            return new ClientExceptionStrategy(messagerService, languageService);
        }
    }
    private static isServerException(exception: Exception) {
        return 'code' in exception;
    }
    private static isUnauthorizedException(exception: Exception) {
        return 'status' in exception;
    }
    private static getExceptionHandleStrategy(exceptionLevel: ExceptionLevel, messagerService: FormMessageService, languageService: LanguageService): IExceptionStrategy {
        let exceptionStrategy: IExceptionStrategy;
        switch (exceptionLevel) {
            case ExceptionLevel.Info:
                exceptionStrategy = new InfoExceptionStrategy(messagerService, languageService);
                break;
            case ExceptionLevel.Warning:
                exceptionStrategy = new WarningExceptionStrategy(messagerService, languageService);
                break;
            case ExceptionLevel.Error:
                exceptionStrategy = new ErrorExceptionStrategy(messagerService, languageService);
                break;
            case ExceptionLevel.Fatal:
                exceptionStrategy = new FatalExceptionStrategy(messagerService, languageService);
                break;
            default:
                // 默认异常等级是错误
                exceptionStrategy = new ErrorExceptionStrategy(messagerService, languageService);
                break;
        }

        return exceptionStrategy;
    }
}