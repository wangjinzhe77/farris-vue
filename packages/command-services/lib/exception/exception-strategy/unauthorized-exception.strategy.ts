import { FormMessageService } from "../../form-message.service";
import { LanguageService } from "../../language.service";
import { Exception, IExceptionStrategy, UnAuthorizedException } from "../types";
import { ExceptionStrategy } from "./exception-strategy";

export class UnauthorizedExceptionStrategy extends ExceptionStrategy implements IExceptionStrategy {
    constructor(
        public messagerService: FormMessageService,
        public languageService: LanguageService) {
        super(messagerService, languageService);
    }
    handleException(error: UnAuthorizedException): void {
        this.messagerService.warning(this.languageService.language.unauthorized);
    }
}