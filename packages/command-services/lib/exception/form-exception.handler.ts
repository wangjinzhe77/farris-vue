import { ExceptionHandler } from "@farris/devkit-vue";
import { FormMessageService } from "../form-message.service";
import { ClientException, Exception, ServerException, UnAuthorizedException } from "./types";
import { LanguageService } from "../language.service";
import { ExceptionStrategyFactory } from "./exception-strategy/exception-strategy.factory";
import { FormLoadingService } from "../form-loading.service";

export class FormExceptionHandler implements ExceptionHandler {
    constructor(private formMessageService: FormMessageService, private languageService: LanguageService, private formLoadingService: FormLoadingService) { }
    public handle(error: any) {
        this.formLoadingService.clearAll();
        const exception = this.parse(error);
        const exceptionStrategy = ExceptionStrategyFactory.create(exception, this.formMessageService, this.languageService);
        exceptionStrategy.handleException(exception);
    }
    private parse(error: any): Exception {
        if (error.response) {
            // 请求成功发出且服务器也响应了状态码，但状态代码超出了 2xx 的范围
            if (error.response.status === 401) {
                return { status: error.response.status } as UnAuthorizedException;
            } else {
                let data = error.response.data;
                const { Code, Level, Message, Detail, date, innerMessage, RequestId } = data;
                return {
                    code: Code, detail: Detail, level: Level, message: Message, requestId: RequestId, date, innerMessage
                } as ServerException;
            }
        } else if (error.request) {
            // 请求已经成功发起，但没有收到响应
            const json = error.toJSON();
            const message = json && json.message;
            const stack = json && json.stack;
            return { message, stack } as ClientException;
        } else {
            // 发送请求时出了问题
            const message = error.message;
            const stack = error.stack;
            return { message, stack } as ClientException;
        }
    }
}