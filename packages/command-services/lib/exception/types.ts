interface Exception {
}
interface ClientException extends Exception {
    message: string;
    stack: string;
}
/**
 * 服务器端异常
 */
interface ServerException extends Exception {
    code: string;
    level: ExceptionLevel;
    message: string;
    detail: string;
    requestId: string;
    date?: string;
    innerMessage?: string;
}
interface UnAuthorizedException extends Exception {
    status: number;
}
/**
 * 服务器端异常等级
 */
const enum ExceptionLevel {
    /**
     * 提示信息
     */
    Info = 0,
    /**
     * 警告
     */
    Warning,
    /**
     * 错误
     */
    Error,
    /**
     * 致命错误
     */
    Fatal
}

/**
* 异常处理接口
*/
interface IExceptionStrategy {
    handleException(error: Exception): void;
}


export { ClientException, ServerException, ExceptionLevel, IExceptionStrategy, Exception, UnAuthorizedException };
