import { Language } from "./types";

export class EnglishLanguage implements Language {
    public yes = 'Yes';
    public no = 'No';
    public confirm = 'Confirm';
    public cancel = 'Cancel';
    public saveSuccess = 'Successfully saved!';
    public saveFailed = 'Save failed!';
    public deleteSuccess = 'Successfully deleted!';
    public deleteFaild = 'Failed to delete!';
    public confirmDeletion = 'Confirm deletion?';
    public confirmClosing = 'There is unsaved data. Do you want to continue closing?';
    public confirmCancel = 'Exist unsaved change,Confirm to cancel?';
    public unauthorized = 'Your login has expired, please login again.';
    public noDataExist = 'Data does not exist to access the edit state!';
    public pleaseSelectDeleteData = 'Please select the data to delete!';
    public pleaseSelectParentNode = 'Please select parent node!';
    public deleteChildFirst = 'Please delete the child nodes first!';
    public pleaseSelectDetailFormData = 'Please select a detail form data first!';
    public pleaseSelectEditData = 'Please select the data you want to edit!';
    public pleaseSelectViewData = 'Please select the data you want to view!';
}