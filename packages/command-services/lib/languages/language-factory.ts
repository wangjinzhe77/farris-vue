import { ChineseTraditionalLanguage } from "./cht";
import { EnglishLanguage } from "./en";
import { Language, Locales } from "./types";
import { ChineseLanguage } from "./zh";

export class LanguageFactory {
    public static create(locale: Locales): Language {

        let language: Language;
        switch (locale) {
            case Locales.zh:
                language = new ChineseLanguage();
                break;
            case Locales.en:
                language = new EnglishLanguage();
                break;
            case Locales.zht:
                language = new ChineseTraditionalLanguage();
                break;
            default:
                language = new ChineseLanguage();
                break;
        }
        return language;
    }
}