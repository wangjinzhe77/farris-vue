import { Language } from "./types";

export class ChineseTraditionalLanguage implements Language {
    public yes = '是';
    public no = '否';
    public confirm = '確定';
    public cancel = '取消';
    public saveSuccess = '保存成功！';
    public saveFailed = '保存失敗！';
    public deleteSuccess = '刪除成功！';
    public deleteFaild = '刪除失敗！';
    public confirmDeletion = '確認刪除？';
    public confirmClosing = '存在未保存的數據，是否繼續關閉？';
    public confirmCancel = '存在未保存的變更，確認取消？';
    public unauthorized = '用戶登錄信息已失效，請重新登錄。';
    public noDataExist = '要編輯的數據不存在，無法進入編輯狀態！';
    public pleaseSelectDeleteData = '請選擇要刪除的數據！';
    public pleaseSelectParentNode = '請選擇父節點！';
    public deleteChildFirst = '請先刪除子節點';
    public pleaseSelectDetailFormData = '請先選擇一條從表數據！';
    public pleaseSelectEditData = '請選擇要編輯的數據！';
    public pleaseSelectViewData = '請選擇要查看的數據！';
}