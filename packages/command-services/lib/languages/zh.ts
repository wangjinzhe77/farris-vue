import { Language } from "./types";

export class ChineseLanguage implements Language {
    public yes = '是';
    public no = '否';
    public confirm = '确定';
    public cancel = '取消';
    public saveSuccess = '保存成功！';
    public saveFailed = '保存失败！';
    public deleteSuccess = '删除成功！';
    public deleteFaild = '删除失败！';
    public confirmDeletion = '确认删除？';
    public confirmClosing = '存在未保存的数据，是否继续关闭？';
    public confirmCancel = '存在未保存的变更，确认取消？';
    public unauthorized = '用户登录信息已失效，请重新登录。';
    public noDataExist = '要编辑的数据不存在，无法进入编辑状态！';
    public pleaseSelectDeleteData = '请选择要删除的数据！';
    public pleaseSelectParentNode = '请选择父节点';
    public deleteChildFirst = '请先删除子节点';
    public pleaseSelectDetailFormData = '请先选择一条从表数据！';
    public pleaseSelectEditData = '请选择要编辑的数据！';
    public pleaseSelectViewData = '请选择要查看的数据！';
}