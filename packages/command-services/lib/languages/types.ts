export interface Language {
    yes: string;
    no: string;
    confirm: string;
    cancel: string;
    saveSuccess: string;
    saveFailed: string;
    deleteSuccess: string;
    deleteFaild: string;
    confirmDeletion: string;
    confirmClosing: string;
    confirmCancel: string;
    unauthorized: string;
    noDataExist: string;
    pleaseSelectDeleteData: string;
    pleaseSelectParentNode: string;
    deleteChildFirst: string;
    pleaseSelectDetailFormData: string;
    pleaseSelectEditData: string;
    pleaseSelectViewData: string;
}

export enum Locales {
    zh = 'zh-CHS',
    zht = 'zh-ZHT',
    en = 'en'
}