import { RuntimeFrameworkService } from './rtf.service';
import { QuerystringService } from './querystring.service';
import { TAB_EVENT } from './types';
import { CommandContext, Token } from '@farris/devkit-vue';
import { NavigationHistoryService } from './navigation-history.service';

export class NavigationEventService {
    public commandContext: CommandContext;
    /**
     * 关闭后事件处理器
     */
    private onClosedListeners: Map<string, (options?: any) => void>;
    /**
     * 关闭前处理器
     */
    private onClosingListeners: Map<string, (options?: any) => Promise<boolean>>;
    /**
     * 框架页签切换事件
     */
    private onTabSwitchListeners: Map<string, (options?: any) => void>;
    private onTabRefreshListeners: Map<string, (options?: any) => void>;
    private get querystrings() {
        const params = this.querystringService.parse(window.location.hash);
        // 修正formToken
        if (params) {
            params.formToken = this.runtimeFrameworkService.formToken;
        }
        return params;
    }
    constructor(
        private runtimeFrameworkService: RuntimeFrameworkService,
        private querystringService: QuerystringService,
        private navigationHistoryService: NavigationHistoryService
    ) {
        this.onClosedListeners = new Map<string, (options?: any) => void>();
        this.onClosingListeners = new Map<string, (options?: any) => Promise<boolean>>();
        this.onTabSwitchListeners = new Map<string, (options?: any) => void>();
        this.onTabRefreshListeners = new Map<string, (options?: any) => void>();
        this.registerEvent();
    }
    public registerEvent() {
        const options = this.querystrings;
        // 注册标签页切换事件
        this.runtimeFrameworkService.addEventListener(TAB_EVENT.onTabSwitched, (e) => this.handleTabSwitchEvent(e), options);
        // 注册标签页关闭后事件
        this.runtimeFrameworkService.addEventListener(TAB_EVENT.onTabClosed, (e) => this.handleTabClosedEvent(e), options);
        // 注册标签页关闭前事件
        this.runtimeFrameworkService.addEventListener(TAB_EVENT.onTabClosing, (e) => this.handleTabClosingEvent(e), options);
    }
    private handleTabSwitchEvent(e: any) {
        if (!e) {
            return;
        }
        // 选中的表单为系统表单，只能返回id，没有tabId
        const eventTabId = e.tabId || e.id || null;
        if (!eventTabId) {
            return;
        }
        const options = this.querystrings;
        const tabId = options.tabId || options.funcId || options.appId;
        // 获取当前页签是否打开过其他页签
        const match = this.navigationHistoryService.find({ from: eventTabId });
        if (!!tabId && tabId === eventTabId && match) {
            this.refresh();
        }
        this.fireTabSwitchEvent(e);
    }
    /**
     * 触发tab切换事件
     * @param e e
     */
    private fireTabSwitchEvent(e: any) {
        if (!this.onTabSwitchListeners || this.onTabSwitchListeners.size < 1) {
            return;
        }
        this.onTabSwitchListeners.forEach((handle, key, map) => {
            if (typeof handle === 'function') {
                handle(e);
            }
        });
    }
    /**
     * 标签页关闭前事件
     */
    private handleTabClosingEvent(event: any) {
        if (!event) {
            return;
        }
        // 要关闭的表单为系统表单，只能返回id，没有tabId
        const eventTabId = event.tabId || event.id || null;
        const options = this.querystrings;
        const tabId = options.tabId || options.funcId || options.appId;
        if (eventTabId && tabId && tabId === eventTabId) {
            this.fireTabClosingEvent(event).then((result) => {
                if (result) {
                    this.navigationHistoryService.remove((item) => item.from === eventTabId || item.to === eventTabId);
                    if (!(event && event.hasOwnProperty('token'))) {
                        event = Object.assign({}, event, { token: options.formToken });
                    }
                    this.runtimeFrameworkService.closeMenu(event);
                }
            });
        }
    }
    /**
     * 触发关闭前事件
     */
    private fireTabClosingEvent(e: any): Promise<boolean> {
        if (!this.onClosingListeners || this.onClosingListeners.size < 1) {
            return Promise.resolve(true);
        }
        const listeners = Array.from(this.onClosingListeners.values());
        // const result$ = from(listeners);
        // 用户拒绝
        let userRejected = false;
        const resultPromise = listeners.reduce((promiseChain: Promise<any>, handle) => {
            return promiseChain.then(chainResults => {
                // 如果用户已经拒绝，则跳过后续的处理
                if (userRejected) {
                    return chainResults;
                }
                // 处理当前的 handle，只取第一个结果
                return handle(e).then(result => {
                    // 如果用户拒绝，则设置标志
                    userRejected = !result;
                    // 将结果添加到结果链中
                    return [...chainResults, result];
                });
            });
        }, Promise.resolve([]));
        return resultPromise.then(chainResults => {
            // 检查是否所有的结果都是 true
            return chainResults.every((result: any) => result);
        });
    }
    /**
     * 标签页关闭后事件
     */
    private handleTabClosedEvent(e: any) {
        if (!e) {
            return;
        }
        const options = this.querystrings;
        const tabId = options.tabId || options.funcId || options.appId;
        const eventTabId = e.tabId || e.id || null;
        if (tabId === eventTabId) {
            return;
        }
        const match = this.navigationHistoryService.find({ from: eventTabId });
        if (eventTabId && match) {
            // 移除所有从当前页签打开或被当前页签打开的历史记录
            this.navigationHistoryService.remove((item) => item.from === eventTabId || item.to === eventTabId);
            this.refresh();
        }
        this.fireTabClosedEvent(e);
    }
    /**
     * 触发关闭后事件
     * @param e event
     */
    private fireTabClosedEvent(e: any) {
        if (!this.onClosedListeners || this.onClosedListeners.size < 1) {
            return;
        }
        this.onClosedListeners.forEach((handle, key, map) => {
            if (typeof handle === 'function') {
                handle(e);
            }
        });
    }
    private fireTabRefreshEvent() {
        if (!this.onTabRefreshListeners || this.onTabRefreshListeners.size < 1) {
            return;
        }
        this.onTabRefreshListeners.forEach((handle, key, map) => {
            if (typeof handle === 'function') {
                handle();
            }
        });
    }
    // #endregion
    /**
     * 注册事件监听器
     * @param eventType 事件类型 onTabClosed
     * @param handler 处理器
     */
    public addEventListener(eventType: string, handler: (options: any) => any): string | null {
        const key = Token.create();
        if (eventType === TAB_EVENT.onTabClosed) {
            this.onClosedListeners.set(key, handler);
            return key;
        } else if (eventType === TAB_EVENT.onTabClosing) {
            this.onClosingListeners.set(key, handler);
            return key;
        } else if (eventType === TAB_EVENT.onTabSwitched) {
            this.onTabSwitchListeners.set(key, handler);
            return key;
        } else if (eventType === TAB_EVENT.onTabRefresh) {
            this.onTabRefreshListeners.set(key, handler);
            return key;
        }
        return null;
    }
    /**
     * 移除事件监听器
     * @param eventType 事件类型
     * @param key 事件标识
     */
    public removeEventListener(eventType: string, key: string) {
        if (eventType === TAB_EVENT.onTabClosed) {
            return this.onClosedListeners.delete(key);
        } else if (eventType === TAB_EVENT.onTabClosing) {
            return this.onClosingListeners.delete(key);
        }
        return false;
    }
    /**
     * 清空事件监听器
     * @param eventType 事件类型
     */
    public clearEventListener(eventType: string): void {
        if (eventType === TAB_EVENT.onTabClosed) {
            this.onClosedListeners.clear();
        } else if (eventType === TAB_EVENT.onTabClosing) {
            this.onClosingListeners.clear();
        }
    }
    /**
     * 刷新数据
     */
    private refresh() {
        this.fireTabRefreshEvent();
    }
}