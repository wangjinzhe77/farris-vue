export class DialogService {
    constructor() { }

    public openModal(config: string | Record<string, any>, modalId: string, params: string | Record<string, any>, callback?: (modalRef: any) => any) { }

    public openHelpModal(frameId: string, params: string | Record<string, any>, configs?: string | Record<string, any>) { }

    public openCallbackableModal(frameId: string, params?: string | Record<string, any>, configs?: string | Record<string, any>) {

    }

    public confirm(){}

    public cancel(event?: any){}

    public closeModal(){}
}
