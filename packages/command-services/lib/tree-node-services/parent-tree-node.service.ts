export class ParentTreeNodeService {
    public getNextNodeId(treeNodesData: any[], hierarchyInfoKey: string, currentId: string): string {
        // 当前节点信息
        const currentNodeData = treeNodesData.find((itemData: any) => {
            return itemData['id'] === currentId;
        });
        const currentLayer = currentNodeData[hierarchyInfoKey]['layer'];
        // 父节点信息
        const fLayer = currentLayer - 1;
        const fParentElement = currentNodeData[hierarchyInfoKey]['parentElement'];
        // 查找兄弟节点
        const siblingtreeNodesData = this.getChildNodesData(treeNodesData, hierarchyInfoKey, fLayer, fParentElement);
        // 如果没有兄弟节点，向上查找
        if (siblingtreeNodesData.length === 1) {
            const parentData = treeNodesData.find((itemData: any) => {
                return itemData['id'] === fParentElement;
            });
            // 存在父节点，则设置父节点；
            // 不存在父节点，则设置第一个根节点。
            if (!parentData) {
                return this.getFirstNodeId(treeNodesData, hierarchyInfoKey);
            }
            return parentData['id'];
        } else {
            return this.getNextSiblingNodeId(siblingtreeNodesData, currentId);
        }
    }
    public getNextSiblingNodeId(siblingtreeNodesData: any[], currentId: string): string {
        if (siblingtreeNodesData.length <= 1) {
            return '';
        }
        const currentIndex = siblingtreeNodesData.findIndex((itemData: any) => {
            return itemData['id'] === currentId;
        });
        // 最后一行上移一行，其他下移一行
        let nextIndex = -1;
        if (currentIndex === siblingtreeNodesData.length - 1) {
            nextIndex = currentIndex - 1;
        } else {
            nextIndex = currentIndex + 1;
        }
        return siblingtreeNodesData[nextIndex]['id'];
    }
    public getChildNodesData(treeNodesData: any[], hierarchyInfoKey: string, fLayer: number, fParentElement: string): any[] {
        const childtreeNodesData = treeNodesData.filter((itemData) => {
            const layer = itemData[hierarchyInfoKey]['layer'];
            const parentElement = itemData[hierarchyInfoKey]['parentElement'];
            return (layer === fLayer + 1) && fParentElement == parentElement;
        });
        return childtreeNodesData;
    }
    public getFirstNodeId(treeNodesData: any[], hierarchyInfoKey: string): string {
        let rootData = treeNodesData.find((itemData: any) => {
            const layer = itemData[hierarchyInfoKey]['layer'];
            return layer === 1;
        });
        if (!rootData) {
            const rootLayer = this.getRootLayer(treeNodesData, hierarchyInfoKey);
            rootData = treeNodesData.find((itemData: any) => {
                const layer = itemData[hierarchyInfoKey]['layer'];
                return layer === rootLayer;
            });
        }
        return rootData ? rootData['id'] : '';
    }
    private getRootLayer(treeNodesData: any[], hierarchyInfoKey: string) {
        let layer = null;
        if (treeNodesData && Array.isArray(treeNodesData)) {
            const layers = treeNodesData.map(item => {
                const layer = item[hierarchyInfoKey]['layer'];
                return layer;
            });
            const minLayer = Math.min.apply(Math, layers);
            if (!isNaN(minLayer)) {
                layer = minLayer;
            }
        }
        return layer;
    }
    public getNodeDataById(treeNodesData: any[], id: string): any {
        const nodeData = treeNodesData.find((itemData: any) => {
          return itemData['id'] === id;
        });
        return nodeData ? nodeData : null;
      }
    public hasChildNodes(treeNode: any, hierarchyInfoKey: string): boolean {
        const fIsDetail = treeNode[hierarchyInfoKey]['isDetail'];
        // 非明细节点，返回true
        if (fIsDetail === false) {
          return true;
        }
        return false;
      }
}