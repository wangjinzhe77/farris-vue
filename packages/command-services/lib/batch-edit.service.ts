import { ViewModel, ViewModelState } from "@farris/devkit-vue";

export class BatchEditService {
    constructor(private viewModel: ViewModel<ViewModelState>) { }

    public openHiddenHelp(helpId: string) { }

    public beforeMultiSelectHelpOpen() { }

    public afterMultiSelectHelpClose(frameId: string, mappingFields: string) { }

    public openBatchEditDialog(frameId: string) { }

    public copyRow(frameId: string, ignoreField: string, repeat: number = 1) { }

    public copy(id: string) { }

    public batchAppendByPathBasedOnHelpSelections() { }

    public clearHelpSelections() { }

    public batchAppendBasedOnRowHelpSelections() { }

    public checkCurrentRow() { }

    public batchAppend() { }

    public clone(id: string, path: string) { }

    public batchAppendByPathWithAttachment() { }
}
