import { StateMachine, StateMachineState, ViewModel, ViewModelState } from '@farris/devkit-vue';
// TODO: 依赖状态机
export class StateMachineService {
    // 
    constructor(private viewModel: ViewModel<ViewModelState>) { }
    /**
     * 状态机切换
     * @param action 状态机动作
     */
    public transit(action: string) {
        (this.viewModel.stateMachine as any)[action]();
    }
}