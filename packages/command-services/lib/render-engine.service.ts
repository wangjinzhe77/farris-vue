import { Injector, RENDER_ENGINE_TOKEN, RenderEngine } from '@farris/devkit-vue';

export class RenderEngineService {
    private renderEngine: RenderEngine;
    constructor(private injector: Injector) {
        this.renderEngine = this.injector.get<RenderEngine>(RENDER_ENGINE_TOKEN);
    }
    /**
     * 使用组件Id获取组件实例
     * @param componentId 
     */
    public getComponentById(componentId: string) {
        return this.renderEngine.getComponentById(componentId);
    }
    /**
     * 更新组件属性
     * @param componentId 
     * @param props 
     */
    public setProps(componentId: string, props: Record<string, any>) {
        this.renderEngine.setProps(componentId, props);
    }
    /**
     * 获取组件属性
     * @param componentId 
     * @returns 
     */
    public getProps(componentId: string) {
        return this.renderEngine.getProps(componentId);
    }
    /**
     * 更新组件schema
     * @param componentId 
     * @param schema 
     */
    public setSchema(componentId: string, schema: Record<string, any>) {
        this.renderEngine.setSchema(componentId, schema);
    }
    /**
     * 获取组件schema
     * @param componentId 
     * @returns 
     */
    public getSchema(componentId: string) {
        return this.renderEngine.getSchema(componentId);
    }
}