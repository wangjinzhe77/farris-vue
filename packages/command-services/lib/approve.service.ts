export class ApproveService {
    public submitApproveWithInteraction(bizBillID: string){}

    public submitApprove(bizBillID: string, interactionResult?: any){}

    public submitApproveWithBizDefKey(bizBillID: string, bizDefKey: string, options?: any, interactionResult?: any){}

    public submitApproveWithBizDefKeyUseControl(bizBillID: string, bizDefKey: string, options: any = {}, variables?: any){}

    public childSubmitApproveWithBizDefKey(bizDefKey: string, bizId: string, childBizId: string, options: any = {}, variables?: any){}

    public cancelApprove(bizBillID: string){}

    public cancelSubmit(procInstId: string){}

    public viewProcess(procInstId: string){}
}
