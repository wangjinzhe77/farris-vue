import { ViewModel, ViewModelState } from "@farris/devkit-vue";
import { BaseDataService } from "./data-services/base-data.service";

export class EndEditService extends BaseDataService {
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }
    public endEdit() {
        
    }
}