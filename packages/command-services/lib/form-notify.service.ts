import { Injector } from '@farris/devkit-vue';
import { NOTIFY_SERVICE_TOKEN } from './tokens';

/**
 * 通知服务
 * @description
 * 成功使用success，失败使用error，警告使用warning，其他场景使用info
 */
export class FormNotifyService {
    private notifyService: any = null;
    /**
     * 构造函数
     */
    constructor(private injector: Injector) {
        if (this.injector) {
            this.notifyService = this.injector.get(NOTIFY_SERVICE_TOKEN);
            this.notifyService.globalConfig.position = 'top-center';
            this.notifyService.globalConfig.top = 150;
        }
    }

    /**
     * 信息提示
     * @param content 内容
     */
    public info(content: string) {
        const notifyOptions = {
            message: content,
            timeout: 3000
        };
        return this.notifyService.info(notifyOptions);
    }

    /**
     * 成功提示
     * @param content 内容
     */
    public success(content: string) {
        const notifyOptions = {
            message: content,
            timeout: 3000
        };
        this.notifyService.success(notifyOptions);
    }

    /**
     * 警告提示
     * @param content 内容
     */
    public warning(content: string) {
        const notifyOptions = {
            message: content,
            timeout: 3000
        };
        this.notifyService.warning(notifyOptions);
    }

    /**
     * 错误提示
     * @param content 内容
     */
    public error(content: string) {
        const notifyOptions = {
            message: content,
            timeout: 3000
        };
        this.notifyService.error(notifyOptions);
    }
}
