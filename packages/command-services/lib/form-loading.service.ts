import { Injector } from "@farris/devkit-vue";
import { LOADING_SERVICE_TOKEN } from "./tokens";

export class FormLoadingService {
    private loadingService: any;
    private loadingTimerRecord: Record<number, null | number> = {};
    constructor(private injector: Injector) {
        this.loadingService = this.injector.get(LOADING_SERVICE_TOKEN);
    }
    /**
     * 展示加载提示
     * @param configOrMessage 
     * @returns 
     */
    public show(configOrMessage: any): number | undefined | null {
        if (this.loadingService) {
            const config = this.buildConfig(configOrMessage);
            const loadingComponent = this.loadingService.show(config);
            return loadingComponent && loadingComponent.value.getLoadingId();
        }
    }
    /**
     * 延时展示加载提示
     * @param delayTime 延时时间，默认500毫秒
     * @param configOrMessage 消息或加载配置
     * @returns 
     */
    public showLoadingWithDelay(delayTime: number = 500, configOrMessage?: any): number {
        const timerId = window.setTimeout(() => {
            const loadingId = this.show(configOrMessage);
            if (typeof loadingId === 'number') {
                this.loadingTimerRecord[timerId] = loadingId;
            }
        }, delayTime);
        this.loadingTimerRecord[timerId] = null;
        return timerId;
    }
    public hideDelayLoading(timerId: any) {
        const loadingId = this.loadingTimerRecord[timerId];
        if (typeof loadingId === 'number') {
            this.hide(loadingId);
        } else {
            this.clearLoadingTimer(timerId);
        }
    }
    private clearLoadingTimer(timerId: number) {
        window.clearTimeout(timerId);
    }
    /**
     * 隐藏加载
     */
    public hide(loadingId: number) {
        if (this.loadingService) {
            this.loadingService.close(loadingId);
        }
    }
    public clearAll() {
        if (this.loadingService) {
            this.loadingService.clearAll();
        }
    }
    /**
     * 构造loading参数
     * @param configOrMessage 
     * @returns 
     */
    private buildConfig(configOrMessage: any) {
        let config: any;
        if (typeof configOrMessage === 'string') {
            config = { message: configOrMessage };
        } else {
            config = Object.assign({}, configOrMessage);
        }
        return config;
    }
}