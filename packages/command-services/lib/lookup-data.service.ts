import { HttpClient, ViewModel, ViewModelState } from '@farris/devkit-vue';
// import { LookupHttpResult, LookupHttpService, LookupRequestParams } from '@farris/ui-vue/types/lookup';
import { BaseDataService } from './data-services/index';
export class LookupDataService extends BaseDataService {

    private settingsUri = '/api/runtime/bcc/v1.0/datagrid/settings';

    constructor(public viewModel: ViewModel<ViewModelState>, public http: HttpClient) {
        super(viewModel);
    }
    public getData(uri: string, params: any): Promise<any> {
        const [tableName, labelId] = uri.split('.');
        return this.repository.apiProxy.elementhelps(labelId, tableName, params);
    }

    public getSettings(id: string): Promise<any> {
        return this.http.get(this.settingsUri + '/' + id, {});
    }

    public updateSettings(id: string, settings: any): Promise<any> {
        return this.http.post(this.settingsUri, settings, {});
    }
}