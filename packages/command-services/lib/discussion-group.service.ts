import { ViewModel, ViewModelState } from "@farris/devkit-vue";
import { FormLoadingService } from './form-loading.service';
import { RuntimeFrameworkService } from './rtf.service';

export class DiscussionGroupService {
  constructor(private viewModel: ViewModel<ViewModelState>, private loadingService: FormLoadingService, private runtimeFrameworkService: RuntimeFrameworkService) {
  }

  public addComment(id?: string, summary?: string, configId?: string, text?: string, visibility?: string, parentId?: string) {
    
  }
  /**
   * 查询评论
   * @param id id
   */
  public queryComments(id: string, configId: string, pageIndex?: number | null, pageSize?: number | null) {
   
  }
  /**
   * 查询所有部门信息
   */
  public queryAllOrgs() {
    
  }
  /**
   * 查询常用@用户
   * @param pageIndex
   * @param pageSize
   */
  public queryFrequentAtUsers(pageIndex?: number | null, pageSize?: number | null) {
    
  }
  /**
   * 获取at用户列表
   * @param user 用户编号或者用户名称（过滤使用）
   * @param pageIndex pageIndex
   * @param pageSize pageSize
   */
  public queryAtUsers(user?: string, pageIndex?: number | null, pageSize?: number | null) {
    
  }
  /**
   * 构造获取评论列表的url
   * @param id id
   */
  private buildQueryCommentsUrl(id: string, pageIndex: number | null, pageSize: number | null, configId: string) {
    
  }
}
