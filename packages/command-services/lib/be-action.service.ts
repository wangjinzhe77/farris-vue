import { BodyWithRequestInfo } from "@farris/bef-vue";

export class BeActionService {

    public invokeAction(actionUri: string, httpMethod: string, httpHeaders?: any, queryParams?: any, body?: BodyWithRequestInfo) { }

    public executeAction(actionUri: string, httpMethod: string, httpHeaders?: any, queryParams?: any, body?: any) { }

    public getRestService(){}

    public buildQueryParams(queryParams: string){}
}
