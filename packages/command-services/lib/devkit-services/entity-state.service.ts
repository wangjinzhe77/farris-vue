import { ViewModel, Entity, EntityState,ViewModelState, EntityStore } from '@farris/devkit-vue';

/**
 * 实体状体服务
 */
class EntityStateService {

    /**
     * 视图模型
     */
    private viewModel: ViewModel<ViewModelState>;

    /**
     * 实体状态
     */
    private entityState: EntityStore<EntityState<Entity>>;

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        this.viewModel = viewModel;
        this.entityState = viewModel.entityStore as EntityStore<EntityState<Entity>>;
    }

    /**
     * 改变当前行
     */
    public changeCurrentEntity(id: string): void {
        this.entityState.changeCurrentEntityById(id);
    }

    /**
     * 设置当前实体
     */
    public changeCurrentEntityByPath(path: string, id: string): void {
        this.entityState.changeCurrentEntityByPath(path, id);
    }
    /**
     * 获取实体变更
     * @returns 
     */
    public entityChanges(){
        return this.entityState.entityChangeHistory.getMergedChanges();
    }
}

export { EntityStateService };
