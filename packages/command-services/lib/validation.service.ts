import { ViewModel, ViewModelState } from "@farris/devkit-vue";
import { BaseDataService } from "./data-services/base-data.service";

export class ValidationService extends BaseDataService {
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    public validate() {

    }
    public validateCurrentRow() {
        return Promise.resolve();
    }

    public validateAll(){}

    public resetValidation(){
        return Promise.resolve();
    }

}