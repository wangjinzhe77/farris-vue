const { resolve } = require("path");
const fsExtra = require("fs-extra");
const { omit } = require("lodash");
const { build } = require("vite");
const dts = require("vite-plugin-dts");
const replace = require("../plugins/replace.cjs");
const getViteConfig = require("./vite.config.cjs");
const package = require("../../package.json");

const CWD = process.cwd();

const getVersion = () => {
    const versionNums = package.version.split(".");
    return versionNums
        .map((num, index) => (index === versionNums.length - 1 ? +num + 1 : num))
        .join(".");
};

const createPackageJson = async (version) => {
    package.version = getVersion(version);
    package.main = "./command-services-vue.umd.js";
    package.module = "./command-services-vue.esm.js";
    package.style = "./style.css";
    package.dependencies = omit(package.dependencies, "");
    package.typings = "./types/index.d.ts";
    package.types = "./types/index.d.ts";
    package.private = false;
    const fileStr = JSON.stringify(
        omit(package, "scripts", "devDependencies"),
        null,
        2
    );
    await fsExtra.outputFile(
        resolve(CWD, "./package", `package.json`),
        fileStr,
        "utf-8"
    );
};

exports.build = async () => {
    const lib = {
        entry: resolve(CWD, "./lib/index.ts"),
        name: "FarrisCommandServicesVue",
        fileName: "command-services-vue",
        formats: ["esm", "umd"],
    };

    const outDir = resolve(CWD, "./package");
    const plugins = [dts({
        entryRoot: "./lib",
        outputDir: resolve(CWD, "./package/types"),
        include: [
            "./lib/**/*.ts",
            "./lib/**/*.tsx",
            "./lib/**/*.vue",
        ],
        noEmitOnError: false,
        skipDiagnostics: true,
    }), replace({ path: (format, args) => `.${args[1]}/index.${format}.js` })];
    const config = getViteConfig({ lib, outDir, plugins });

    await build(config);

    await createPackageJson();
};
