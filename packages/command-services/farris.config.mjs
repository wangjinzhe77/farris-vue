import { fileURLToPath, URL } from 'node:url';

export default {
    lib: {
        entry: fileURLToPath(new URL('./lib/index.ts', import.meta.url)),
        name: "farris-command-services-vue",
        fileName: "command-services-vue",
        formats: ["esm", 'umd', 'systemjs'],
    },
    systemjs: true,
    outDir: fileURLToPath(new URL('./package', import.meta.url)),
    externals: {
        include: [process.env.BUILD_TYPE === 'components' ? "@components" : ''],
        filter: (externals) => {
            return (id) => {
                return externals.find((item) => item && id.indexOf(item) === 0);
            };
        }
    },
    externalDependencies: true,
    minify: false,
    target: "es2015",
    alias: [
        { find: '@', replacement: fileURLToPath(new URL('./', import.meta.url)) },
        { find: '@components', replacement: fileURLToPath(new URL('./components', import.meta.url)) }
    ],
    css: {
        entry: './components/index.scss',
        outfile: './package/index.css'
    }
};
