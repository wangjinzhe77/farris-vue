import { createRouter, createWebHistory } from 'vue-router';
import List from '../views/list/list.vue';
import Card from '../views/card/card.vue';

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            component: List
        },
        {
            path: '/card',
            component: Card
        }
    ]
});

export default router;
