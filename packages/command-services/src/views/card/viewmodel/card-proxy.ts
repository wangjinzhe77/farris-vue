import { HttpClient } from '@farris/devkit-vue';
import { BefProxy } from '@farris/bef-vue';

/**
 * 代理
 */
export class CardProxy extends BefProxy {

    /**
     * 基路径地址
     */
    public baseUrl = '/api/webapp/formtemplate/v1.0/mainlist1_frm';

    /**
     * 构造函数
     */
    constructor(httpClient: HttpClient) {
        super(httpClient);
    }
}
