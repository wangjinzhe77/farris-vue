import { ViewModel, Type } from '@farris/devkit-vue';
import { BefProxy, BefRepository } from '@farris/bef-vue';
import { MainEntity } from './entities/index';
import { CardProxy } from './card-proxy';

export class CardRepository extends BefRepository<MainEntity> {

    public entityType = MainEntity;

    public apiProxyType: Type<BefProxy> = CardProxy;

    constructor(viewModel: ViewModel) {
        super(viewModel);
    }

}
