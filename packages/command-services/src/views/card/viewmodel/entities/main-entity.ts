import { Field, Entity } from '@farris/devkit-vue';

class MainEntity extends Entity {

    /**
     * id
     */
    @Field({
        primary: true
    })
    public id: string;

    /**
     * version
     */
    @Field()
    public version: string;

    /**
     * string1
     */
    @Field()
    public string1: string;

    /**
     * string2
     */
    @Field()
    public string2: string;

    /**
     * float1
     */
    @Field()
    public number1: any;

    /**
     * float1
     */
    @Field()
    public float1: any;

    /**
     * bollean1
     */
    @Field()
    public boolean1: any;

    /**
     * date1
     */
    @Field()
    public date1: any;

    /**
     * dateTime1
     */
    @Field()
    public dateTime1: string;

    /**
     * enum1
     */
    @Field()
    public enum1: any;
}

export { MainEntity };
