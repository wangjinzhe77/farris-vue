import { Form, ControlProp, BindingType, FormControl, ViewModel } from '@farris/devkit-vue';

export class CardForm extends Form {

    constructor(viewModel: ViewModel) {
        super(viewModel);
    }

    @ControlProp({
        bindingType: BindingType.EntityState,
        bindingPath: '/id'
    })
    public id: FormControl;

    @ControlProp({
        bindingType: BindingType.EntityState,
        bindingPath: '/version'
    })
    public version: FormControl;

    @ControlProp({
        bindingType: BindingType.EntityState,
        bindingPath: '/string1'
    })
    public string1: FormControl;

    @ControlProp({
        bindingType: BindingType.EntityState,
        bindingPath: '/string2'
    })
    public string2: FormControl;

    @ControlProp({
        bindingType: BindingType.EntityState,
        bindingPath: '/float1'
    })
    public float1: FormControl;

    @ControlProp({
        bindingType: BindingType.EntityState,
        bindingPath: '/number1'
    })
    public number1: FormControl;

    @ControlProp({
        bindingType: BindingType.EntityState,
        bindingPath: '/date1'
    })
    public date1: FormControl;

    @ControlProp({
        bindingType: BindingType.EntityState,
        bindingPath: '/dateTime1'
    })
    public dateTime1: FormControl;

    @ControlProp({
        bindingType: BindingType.EntityState,
        bindingPath: '/enum1'
    })
    public enum1: FormControl;

}
