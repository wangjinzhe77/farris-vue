import { ViewModel, CommandAction } from '@farris/devkit-vue';

export class CardViewModel extends ViewModel {
    public get inject() {
        return this.injector;
    }

    @CommandAction({
        name: 'add'
    })
    public add() { }

    @CommandAction({
        name: 'edit',
        params: {
            id: '02b88225-fb84-45da-9109-1f8ee03d3565',
            action: 'edit'
        }
    })
    public edit() { }

    @CommandAction({
        name: 'view',
        params: {
            id: '02b88225-fb84-45da-9109-1f8ee03d3565',
            action: 'view'
        }
    })
    public view() { }
}
