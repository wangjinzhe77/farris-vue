import { EntityState } from '@farris/devkit-vue';
import { MainEntity } from './entities/index';

export class CardEntityState extends EntityState<MainEntity> {
    constructor() {
        super(MainEntity);
    }
}
