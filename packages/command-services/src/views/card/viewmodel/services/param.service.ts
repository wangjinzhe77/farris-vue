import { useRoute } from 'vue-router';

export class ParamService {
    parseQuery() {
        const route = useRoute();
        return route.query;
    }
}
