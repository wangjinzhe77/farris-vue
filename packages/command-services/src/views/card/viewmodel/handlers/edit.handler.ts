import { CommandContext, CommandHandler } from '@farris/devkit-vue';
import { LoadDataService } from '../../../../../lib/index';

export class EditHandler extends CommandHandler {

    public name = 'edit';

    constructor(private loadDataService: LoadDataService) {
        super();
    }

    execute(context: CommandContext) {
        console.log('execute edit action');
        const { id = null } = context.command.params;
        this.loadDataService.loadById(id);
    }
}
