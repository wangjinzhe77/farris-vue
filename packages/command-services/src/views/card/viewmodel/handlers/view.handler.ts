import { CommandContext, CommandHandler } from '@farris/devkit-vue';
import { LoadDataService } from '../../../../../lib/index';

export class ViewHandler extends CommandHandler {

    public name = 'view';

    constructor(private loadDataService: LoadDataService) {
        super();
    }

    execute(context: CommandContext) {
        console.log('execute view action');
        const { id = null } = context.command.params;
        this.loadDataService.loadById(id);
    }
}
