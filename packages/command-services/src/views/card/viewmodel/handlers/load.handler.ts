import { CommandContext, CommandHandler } from '@farris/devkit-vue';
import { ParamService } from '../services/index';

export class LoadHandler extends CommandHandler {

    public name = 'load';

    constructor(private paramService: ParamService) {
        super();
    }

    execute(context: CommandContext) {
        const { id = null, action = null } = this.paramService.parseQuery();
        if (action) { /* empty */ }
    }
}
