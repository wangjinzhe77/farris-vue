import { CommandContext, CommandHandler } from '@farris/devkit-vue';
import { CreateDataService } from '../../../../../lib';

export class AddHandler extends CommandHandler {

    public name = 'add';

    constructor(private createDataService: CreateDataService) {
        super();
    }

    execute(context: CommandContext) {
        return this.createDataService.create();
    }
}
