import router from '../../../../router/index';

/**
 * 主实体服务
 */
export class ListService {
    public navigate(id: string, action: string) {
        const params: any = { action };
        if (id) {
            params.id = id;
        }
        router.push({ path: '/card', query: params });
    }

    public load() {

    }

    public loadData(content: string) {
        console.log('EmployeeService=>test】: ' + content);
    }
}
