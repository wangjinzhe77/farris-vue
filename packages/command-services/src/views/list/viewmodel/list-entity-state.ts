import { ref, Ref } from 'vue';
import { EntityState } from '@farris/devkit-vue';
import { MainEntity } from './entities/index';

/**
 * 列表页实体状态
 */
class ListEntityState extends EntityState<MainEntity> {

    /**
     * 数据源
     */
    public data: Ref<any[]>;

    /**
     * 当前实体
     */
    public current: Ref<any>;

    /**
     * 构造函数
     */
    constructor() {
        super(MainEntity);
        this.data = ref(this.entityList.toJSON());
        this.current = ref(null);
    }
}

export { ListEntityState };
