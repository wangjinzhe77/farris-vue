import { ViewModel } from '@farris/devkit-vue';
import {BefRepository} from '@farris/bef-vue';
import { MainEntity } from './entities/index';
import { ListProxy } from './list-proxy';

/**
 * 员工实体仓库
 */
class ListRepository extends BefRepository<MainEntity> {

    public apiProxyType = ListProxy;

    /**
     * Entity类型
     */
    public entityType = MainEntity;

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel) {
        super(viewModel);
    }

}

export { ListRepository };
