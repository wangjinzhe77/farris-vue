import { State, Getter, Action, CommandAction, ViewModel, EntityList } from '@farris/devkit-vue';

/**
 * 列表视图模型
 */
class ListViewModel extends ViewModel {
    /**
     * 部门信息
     */
    @State()
    public deptInfo: any = { id: '', name: '' };

    /**
     * 数据源
     */
    @State()
    public data: any[] = [];

    /**
     * 总数
     */
    @Getter()
    public get total(): number {
        return this.data.length;
    };

    /**
     * 加载数据
     */
    @Action()
    public load1() {
        // 加载部门数据
        this.deptInfo = {
            id: '0001',
            name: '产品研发部'
        };
    }

    @CommandAction({
        name: 'load',
        params: {}
    })
    public load() { }

    @CommandAction({
        name: 'add',
        params: {}
    })
    public add() { }

    /**
     * 打开卡片查看
     */
    @CommandAction({
        name: 'view',
        params: {
            id: '0001',
            action: 'view'
        }
    })
    public view() { }

    /**
     * 打开卡片编辑
     */
    @CommandAction({
        name: 'edit',
        params: {
            id: '0001',
            action: 'edit'
        }
    })
    public edit() { }

    /**
     * 打开卡片
     */
    @CommandAction({
        name: 'remove',
        params: {
            id: '0001'
        }
    })
    public remove() { }

}

export { ListViewModel };
