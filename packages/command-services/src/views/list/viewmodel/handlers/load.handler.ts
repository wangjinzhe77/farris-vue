import { CommandContext, CommandHandler } from '@farris/devkit-vue';
import { ListService } from '../services/index';
import {LoadDataService} from '../../../../../lib/index';

export class LoadHandler extends CommandHandler {

    public name = 'load';

    constructor(private loadDataService: LoadDataService) {
        super();
    }

    execute(context: CommandContext) {
        return this.loadDataService.load();
    }
}
