import { CommandContext, CommandHandler } from '@farris/devkit-vue';
import { ListService } from '../services/index';

export class AddHandler extends CommandHandler {

    public name = 'add';

    constructor(private listService: ListService) {
        super();
    }

    execute(context: CommandContext) {
        const args = [
            '',
            'add'
        ];
        return this.invoke(this.listService, 'navigate', args, context);
    }
}
