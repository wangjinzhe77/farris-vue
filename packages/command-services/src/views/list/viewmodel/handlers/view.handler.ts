import { CommandContext, CommandHandler } from '@farris/devkit-vue';
import { ListService } from '../services/index';

export class ViewHandler extends CommandHandler {

    public name = 'view';

    constructor(private listService: ListService) {
        super();
    }

    execute(context: CommandContext) {
        const args = [
            '02b88225-fb84-45da-9109-1f8ee03d3565',
            'view'
        ];
        return this.invoke(this.listService, 'navigate', args, context);
    }
}
