import { CommandContext, CommandHandler } from '@farris/devkit-vue';
import { ListService } from '../services/index';

export class EditHandler extends CommandHandler {

    public name = 'edit';

    constructor(private listService: ListService) {
        super();
    }

    execute(context: CommandContext) {
        const args = [
            '02b88225-fb84-45da-9109-1f8ee03d3565',
            'edit'
        ];
        return this.invoke(this.listService, 'navigate', args, context);
    }
}
