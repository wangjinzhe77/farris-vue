import { UIState } from '@farris/devkit-vue';

/**
 * 员工列表UI状态
 */
class ListUIState extends UIState {

    /**
     * 构造函数
     */
    constructor() {
        super();
    }
}

export { ListUIState };
