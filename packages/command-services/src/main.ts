import { createApp } from 'vue';
import FarrisUI from '@farris/ui-vue';
import { createDevkit } from '@farris/devkit-vue';
import '@farris/ui-vue/style.css';
import router from './router/index';
import './style.css';
import App from './app.vue';

const app = createApp(App);
app.use(router);
// use farris ui
app.use(FarrisUI);

// devkit
const devkit = createDevkit({
    providers: []
});
app.use(devkit);
app.mount('#app');
