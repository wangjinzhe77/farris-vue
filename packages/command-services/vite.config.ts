import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [vue()],
    server: {
        proxy: {
            "/api": {
                target: "http://10.110.87.52:5200",
                changeOrigin: true,
                secure: false,
                headers:{
                    Cookie:'caf_web_session=OTViOThmNDMtY2YwZC00YmJmLWFjM2EtZmM0NTgwNWM5OGU5'
                }
            }
        }
    },
    resolve: {
        alias: {
            "@farris/devkit-vue": path.resolve(__dirname, "../devkit/lib/index"),
            "@farris/bef-vue": path.resolve(__dirname, "../bef/lib/index")
        }
    }
});
