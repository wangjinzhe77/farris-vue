import { EntityChange, EntityChangeType, EntityPath, ViewModel, ViewModelState } from "@farris/devkit-vue";
import { Ref } from "vue";
/**
 * 元素实例
 */
export type ElementRef = Ref | string;

export interface UseDataGridBinding {
    data: Ref<any>;
    onClickRow: (index: number, data: any) => void;
    pagination: Ref<any>;
}
export interface UseTreeGridBinding {
    onClickRow: (action: Action) => (index: number, data: any) => void;
}
/**
 * 绑定配置
 */
export interface BindingOptions {
    viewModel: ViewModel<ViewModelState>;
    entityPath: string | EntityPath;
}

export interface TreeBuilderBindingOptions extends BindingOptions {
    hierarchyKey: string;
}

export interface TreeNode {
    data: Record<string, any>,
    children: TreeNode[];
}

export interface UseCommandExecutor {
    execute: (action: Action, payload: Arguments) => any;
}

export interface TreeBuildResult {
    data: TreeNode[];
    rootId: string | null;
}

export interface TreeBuilder {
    build: () => TreeBuildResult;
}

export type EntityChangeHandler = {
    [changeType in EntityChangeType | 'All']?: Callable<any, [EntityChange]>;
};

export type Callable<T = any, U extends any[] = any[]> = (...args: U) => T;
export type Action = string | Callable;
export type Arguments<T = any> = T[] | T;