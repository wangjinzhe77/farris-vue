import { Entity, EntityPath, ViewModel, ViewModelState } from "@farris/devkit-vue";
import { TreeBuilder, TreeBuildResult, TreeNode } from "../types";
import { useHierarchyInfo } from "./use-hierarchy-info";
import { useEntitySchema } from "./use-entity-schema";

export function usePathTreeBuilder(hierarchyKey: string, viewModel: ViewModel<ViewModelState>, entityPath: string | EntityPath): TreeBuilder {
    const levels: { [propertyName: string]: any[]; } = {};
    const { isLeaf, getLayer, getHierarchyInfo } = useHierarchyInfo(hierarchyKey);
    const childrenOf: any = {};
    const nodes: any = {};
    const entities = viewModel.entityStore?.getEntitiesByPath(entityPath) || [];
    entities.forEach((nodeData) => {
        const hierarchyInfo = getHierarchyInfo(nodeData);
        const key = `level${hierarchyInfo.layer}`;
        if (levels[key]) {
            levels[key].push(nodeData);
        } else {
            levels[key] = [nodeData];
        }
    });

    function build(): TreeBuildResult {
        const treeNodes: TreeNode[] = [];
        const entities = viewModel.entityStore?.getEntitiesByPath(entityPath) || [];
        entities.forEach(item => buildTree(item, treeNodes));
        const rootId = getRootId(treeNodes);
        return { rootId, data: treeNodes };
    }
    function buildTree(entity: Entity, treeNodes: TreeNode[]) {
        const id = entity.idValue;
        const parentId = getParentId(entity);
        // 构造树节点
        const node = buildNode(entity);
        nodes[id] = node;
        childrenOf[id] = childrenOf[id] || [];
        node.children = childrenOf[id];

        if (parentId) {
            childrenOf[parentId] = childrenOf[parentId] || [];
            childrenOf[parentId].push(node);
        } else {
            treeNodes.push(node);
        }
    }

    function getRootId(treeNodes: TreeNode[]): string | null {
        if (!treeNodes || treeNodes.length < 1) {
            return null;
        }
        const rootNode = treeNodes[0];
        const { getPrimaryKey } = useEntitySchema(viewModel, entityPath);
        const primaryKey = getPrimaryKey();
        if (!primaryKey) {
            return null;
        }
        const rootId = rootNode.data[primaryKey];
        return rootId;
    }
    function buildNode(entity: Entity) {
        return {
            data: entity.toJSON(),
            children: [],
        };
    }
    function getParentId(node: any) {
        const hierarchyInfo = getHierarchyInfo(node);
        const parentLevelGroup = levels[getLevelKey(hierarchyInfo.layer - 1)] || [];
        const parent = parentLevelGroup.find(nodeData => {
            const currentHierarchyInfo = getHierarchyInfo(nodeData);
            if (currentHierarchyInfo.isDetail === true) {
                return false;
            }
            return hierarchyInfo.path.startsWith(currentHierarchyInfo.path);
        });
        if (parent) {
            return parent.id;
        } else {
            return null;
        }
    }
    function getLevelKey(level: any) {
        return `level${level}`;
    }
    return {
        build
    };
}