import { EntityChangeType, ViewModel, ViewModelState } from "@farris/devkit-vue";
import { ElementRef } from "../types";
import { useElementRef } from "./use-element-ref";
import { useEntityPath } from "./use-entity-path";

export function useFormGroupBinding(elementRef: ElementRef, options: any, viewSchema: Record<string, any>) {
    const componentRef = useElementRef(elementRef);
    const viewModel: ViewModel<ViewModelState> = options.viewModel;
    const { entityPath, toShortPath } = useEntityPath(viewModel, options.entityPath, true);

    if (viewSchema.editor.type === 'lookup') {
        viewModel.entityStore?.watchChange((change: any) => {
            if (!componentRef.value) {
                return;
            }
            switch (change.type) {
                case EntityChangeType.CurrentChange:
                case EntityChangeType.Load:
                case EntityChangeType.Update:
                case EntityChangeType.ValueChange:
                    const entityStore = viewModel.entityStore;
                    const editorRef = componentRef.value.editorRef;
                    if (!editorRef) {
                        return;
                    }
                    const mappingFields = editorRef.mappingFields || null;
                    const idField = editorRef.idField;
                    if (!mappingFields || Object.keys(mappingFields).length < 1 || !idField) {
                        return;
                    }
                    const dataField = mappingFields[idField];
                    const entityPaths = toShortPath() || [];
                    const paths = entityPaths.concat(dataField.split('.'));
                    const dataFieldPath = '/' + paths.join('/');
                    const primaryValue = entityStore?.getValueByPath(dataFieldPath);
                    editorRef.updateIdValue(primaryValue);
                    break;
                default: break;
            }
        });
    }

    return {
    };
}