import { Action, Arguments, Callable, UseCommandExecutor } from "../types";

export function useActionExecutor(viewModel: any): UseCommandExecutor {
    function execute(action: Action, payload: Arguments) {
        if (typeof action === 'string') {
            const command: Callable = viewModel[action];
            return command.call(viewModel, payload);
        } else {
            return action.apply(viewModel, payload);
        }
    }
    return {
        execute
    };
}