import { EntityFieldSchema, EntityPath, EntitySchema, ViewModel, ViewModelState } from "@farris/devkit-vue";

export function useEntitySchema(viewModel: ViewModel<ViewModelState>, entityPath: string | EntityPath) {

    function getPrimaryKey(): string | null {
        const entityPathObject = getEntityPath();
        if (!entityPathObject) {
            return null;
        }
        let entitySchema: EntitySchema | undefined = viewModel.entityStore?.getEntitySchema();
        if (entityPathObject.getNodes().length !== 0) {
            const fieldSchema = viewModel.entityStore?.getEntitySchema().getFieldSchemaByPath(entityPathObject) as EntityFieldSchema;
            entitySchema = fieldSchema.entitySchema;
        }
        if (!entitySchema) {
            console.warn(`composition:useTreeGridBinding,invalid entity path!`);
            return null;
        }
        return entitySchema.getIdKey();
    }
    function getEntityPath(): EntityPath | undefined {
        return entityPath instanceof EntityPath ? entityPath : viewModel.entityStore?.createPath(entityPath, true);
    }

    return {
        getPrimaryKey
    };
}