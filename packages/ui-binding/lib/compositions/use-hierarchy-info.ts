import { Entity } from "@farris/devkit-vue";

export function useHierarchyInfo(hierarchyKey: string) {
    function isLeaf(item: any): boolean {
        const hierarchyInfo = getHierarchyInfo(item);
        const isDetail = hierarchyInfo['isDetail'];
        return isDetail;
    }
    function getLayer(item: any): number {
        const hierarchyInfo = getHierarchyInfo(item);
        return hierarchyInfo['layer'];
    }
    function getHierarchyInfo(entity: Entity) {
        return getValue(entity, hierarchyKey);
    }
    function getValue(item: any, path: string) {
        if (path.indexOf('/') === -1) {
            return item[path];
        }
        const paths: any = path.split('/').filter(p => p);
        if (paths.length < 1) {
            return item;
        }
        return paths.reduce((result: any, path: string) => {
            return result && result[path];
        }, item);
    }
    return {
        isLeaf,
        getLayer,
        getHierarchyInfo,
        getValue
    };
}