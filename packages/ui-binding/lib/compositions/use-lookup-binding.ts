import { EntityChangeType, ViewModel, ViewModelState } from "@farris/devkit-vue";
import { ElementRef } from "../types";
import { useElementRef } from "./use-element-ref";
import { useEntityPath } from "./use-entity-path";

export function useLookupBinding(elementRef: ElementRef, options: any) {
    const componentRef = useElementRef(elementRef);
    const viewModel: ViewModel<ViewModelState> = options.viewModel;
    const { entityPath, toShortPath } = useEntityPath(viewModel, options.entityPath, true);

    viewModel.entityStore?.watchChange((change: any) => {
        if (!componentRef.value) {
            return;
        }
        switch (change.type) {
            case EntityChangeType.CurrentChange:
            case EntityChangeType.Load:
            case EntityChangeType.Update:
            case EntityChangeType.ValueChange:
                const entityStore = viewModel.entityStore;
                const mappingFields = componentRef.value.mappingFields || null;
                const idField = componentRef.value.idField;
                if(!mappingFields || !idField){
                    return;
                }
                const dataField = mappingFields[idField];
                const entityPaths = toShortPath() || [];
                const paths = entityPaths.concat(dataField.split('.'));
                const dataFieldPath = '/' + paths.join('/');
                const primaryValue = entityStore?.getValueByPath(dataFieldPath);
                componentRef.value.updateIdValue(primaryValue);
                break;
            default: break;
        }
    });
    return {
    };
}