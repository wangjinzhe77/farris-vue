import { AppendEntityChange, ChangeCurrentEntityChange, Entity, EntityChange, EntityChangeType, EntityPath, ViewModel, ViewModelState } from "@farris/devkit-vue";
import { Action, ElementRef, TreeBuilder, TreeBuilderBindingOptions, UseTreeGridBinding } from "../types";
import { useElementRef } from "./use-element-ref";
import { usePathTreeBuilder } from "./use-path-tree-builder";
import { useHierarchyInfo } from "./use-hierarchy-info";
import { useParentTreeBuilder } from "./use-parent-tree-builder";
import { useEntitySchema } from "./use-entity-schema";
import { useActionExecutor } from "./use-command-executor";

export function useTreeGridBinding(elementRef: ElementRef, options: TreeBuilderBindingOptions): UseTreeGridBinding {
    const componentRef = useElementRef(elementRef);
    const viewModel: ViewModel<ViewModelState> = options.viewModel;
    const entityPath = options.entityPath instanceof EntityPath ? options.entityPath : viewModel.entityStore?.createPath(options.entityPath || '/', true);
    // 设置树udt到应用上下文，方便后续其他组件获取
    const rootViewModel = viewModel.getRoot() as ViewModel<ViewModelState>;
    const actionExecutor = useActionExecutor(viewModel);
    const hierarchyStoreKey = `hierarchy_${entityPath?.getNodes().join('/')}`;
    rootViewModel.uiStore?.setValue(hierarchyStoreKey, options.hierarchyKey);

    function changeCurrentRow(change: EntityChange) {
        if (change.path.getNodes().length === 0) {
            const currentEntityChange = change as ChangeCurrentEntityChange;
            const currentEntity = currentEntityChange.newCurrentEntity as Entity;
            const currentRow = componentRef.value.getSelectionRow && componentRef.value.getSelectionRow();
            const currentId = currentRow?.raw?.id;
            if (currentId === currentEntity.idValue) {
                return;
            }
            selectItemById(currentEntity.idValue);
        }
    };
    /**
     * 渲染数据
     */
    function render(change: EntityChange) {
        // const entities = viewModel.entityStore?.getEntityList().getEntities();
        const builder = getTreeBuilder();
        const { data = [], rootId } = builder?.build() || {};
        componentRef.value.updateDataSource(data);
        if (rootId && change.type === EntityChangeType.Load) {
            selectItemById(rootId);
            viewModel.entityStore?.changeCurrentEntityByPath(viewModel.bindingPath, rootId);
        } else if (change.type === EntityChangeType.Append) {
            const appendChange = change as AppendEntityChange;
            const primaryValue = appendChange.entities[0].idValue;
            selectItemById(primaryValue);
        }else if (change.type === EntityChangeType.Remove) {
            const newCurrentId = viewModel.entityStore?.getCurrentEntityByPath(options.entityPath).idValue;
            if (!newCurrentId) {
                return;
            }
            selectItemById(newCurrentId);
        }
    };
    function selectItemById(id: string){
        componentRef.value.clickRowItemById(id);
    }
    function getTreeBuilder(): TreeBuilder | null {
        const { getHierarchyInfo } = useHierarchyInfo(options.hierarchyKey);
        const currentEntity = viewModel.entityStore?.getCurrentEntity();
        if (!currentEntity) {
            return null;
        }
        const hierarchyInfo = getHierarchyInfo(currentEntity);
        if (!hierarchyInfo || !entityPath) {
            throw new Error(`useTreeGridBinding,invalid hierarchy key:${options.hierarchyKey}`);
        }
        let builder: TreeBuilder;
        if (hierarchyInfo.path) {
            builder = usePathTreeBuilder(options.hierarchyKey, viewModel, entityPath);
        } else {
            builder = useParentTreeBuilder(options.hierarchyKey, viewModel, entityPath);
        }
        return builder;
    }
    // 监听实体变化
    viewModel.entityStore?.watchChange((change: any) => {
        if (!componentRef.value) {
            return;
        }
        switch (change.type) {
            case EntityChangeType.CurrentChange:
                changeCurrentRow(change);
                break;
            case EntityChangeType.Append:
            case EntityChangeType.Remove:
            case EntityChangeType.Load:
            case EntityChangeType.Update:
            case EntityChangeType.ValueChange:
                render(change);
                break;
            default: break;
        }
    });
    function onClickRow(action: Action) {
        return function (index: number, data: any) {
            if (!entityPath) {
                return;
            }
            const { getPrimaryKey } = useEntitySchema(viewModel, entityPath);
            const primaryKey = getPrimaryKey();
            if (!primaryKey) {
                return;
            }
            const primaryValue = data[primaryKey];
            if (!primaryValue) {
                return;
            }
            viewModel.entityStore?.changeCurrentEntityByPath(entityPath, primaryValue);
            if (!action) {
                return;
            }
            actionExecutor.execute(action, [index, data]);
        };
    }
    return {
        onClickRow
    };
}