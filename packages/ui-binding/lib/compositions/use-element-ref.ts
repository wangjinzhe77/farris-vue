import { isRef, onMounted, Ref, ref, useTemplateRef, watch } from "vue";
import { ElementRef } from "../types";

export function useElementRef(elementRef: ElementRef): Ref {
    const componentRef = ref<ElementRef | null>(null);
    if (typeof elementRef === 'string') {
        const templateRef = useTemplateRef(elementRef);
        watch(templateRef, (elementRef) => {
            componentRef.value = elementRef;
        }, { immediate: true });
    } else if (isRef(elementRef)) {
        watch(elementRef, (ref) => {
            componentRef.value = ref;
        }, { immediate: true });
    } else {
        throw new Error(`Invalid useTreeGridBinding arguments.`);
    }
    return componentRef;
}