import { Entity, EntityPath, ViewModel, ViewModelState } from "@farris/devkit-vue";
import { TreeBuilder, TreeBuildResult, TreeNode } from "../types";
import { useHierarchyInfo } from "./use-hierarchy-info";
import { useEntitySchema } from "./use-entity-schema";

export function useParentTreeBuilder(hierarchyKey: string, viewModel: ViewModel<ViewModelState>, entityPath: string | EntityPath): TreeBuilder {
    const { isLeaf, getLayer, getHierarchyInfo } = useHierarchyInfo(hierarchyKey);
    const childrenOf: any = {};
    const nodes: any = {};

    function build(): TreeBuildResult {
        const entities = viewModel.entityStore?.getEntitiesByPath(entityPath) || [];
        const treeNodes: TreeNode[] = [];
        entities.forEach(item => buildTree(item, treeNodes));
        const rootId = getRootId(treeNodes);
        return { rootId, data: treeNodes };
    }
    function buildTree(entity: Entity, treeNodes: TreeNode[]) {
        const id = entity.idValue;
        const parentId = getParentId(entity);
        // 构造树节点
        const node = buildNode(entity);
        nodes[id] = node;
        childrenOf[id] = childrenOf[id] || [];
        node.children = childrenOf[id];
        if (parentId) {
            childrenOf[parentId] = childrenOf[parentId] || [];
            childrenOf[parentId].push(node);
        } else {
            treeNodes.push(node);
        }
    }
    function getRootId(treeNodes: TreeNode[]): string | null {
        if (!treeNodes || treeNodes.length < 1) {
            return null;
        }
        const rootNode = treeNodes[0];
        const { getPrimaryKey } = useEntitySchema(viewModel, entityPath);
        const primaryKey = getPrimaryKey();
        if (!primaryKey) {
            return null;
        }
        const rootId = rootNode.data[primaryKey];
        return rootId;
    }

    function buildNode(entity: Entity) {
        const treeNode = {
            data: entity.toJSON(),
            children: [],
        };
        return treeNode;
    }
    function getParentId(node: any) {
        const hierarchyInfo = getHierarchyInfo(node);
        return hierarchyInfo.parentElement;
    }
    return {
        build
    };
}