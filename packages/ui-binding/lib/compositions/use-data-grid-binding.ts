import { ref } from 'vue';
import { EntityChange, EntityChangeType, Entity, LoadEntityChange, EntitySchema, EntityFieldSchema, AppendEntityChange, ViewModelState, ViewModel, EntityPathNode, EntityPathNodeType, ChangeValueChange } from '@farris/devkit-vue';
import { BindingOptions, ElementRef, UseDataGridBinding } from '../types';
import { useElementRef } from './use-element-ref';
import { useEntityPath } from './use-entity-path';

export function useDataGridBinding(elementRef: ElementRef, options: BindingOptions): UseDataGridBinding {
    const componentRef = useElementRef(elementRef);
    const viewModel: ViewModel<ViewModelState> = options.viewModel;
    const { entityPath, toShortPath } = useEntityPath(viewModel, options.entityPath, true);
    const pagination = ref();
    const data = ref();
    /**
     * 渲染表格数据
     */
    function render(change: EntityChange) {
        const entityPath = toShortPath();
        const bindingPath = `/${entityPath?.join('/')}`;
        if (change.type === EntityChangeType.ValueChange || change.type === EntityChangeType.Update) {
            const changePaths = change.path.clone();
            const nodes = changePaths.getNodes();
            let node = nodes[nodes.length - 1];
            while (node.getNodeType() !== EntityPathNodeType.IdValue) {
                nodes.pop();
                node = nodes[nodes.length - 1];
            }
            nodes.pop();
            const changePath = '/' + nodes.filter((node: EntityPathNode) => node.getNodeType() !== EntityPathNodeType.IdValue).map((node: EntityPathNode) => node.getNodeValue()).join('/');
            if (!(changePath === bindingPath || bindingPath.startsWith(changePath))) {
                return;
            }
        } else {
            const changePath = change && change.path.toShortPath();
            if (!(changePath === bindingPath || bindingPath.startsWith(changePath))) {
                return;
            }
        }
        const entities = viewModel.entityStore?.getEntityListByPath(options.entityPath).toJSON();

        data.value = entities;
        if (change.type === EntityChangeType.Load) {
            const serverPagination = viewModel.entityStore?.getPaginationByPath(viewModel.bindingPath);
            // const serverPagination = (change as LoadEntityChange).pagination;
            if (serverPagination && serverPagination.pageSize !== 0) {
                pagination.value = {
                    index: serverPagination?.pageIndex,
                    size: serverPagination?.pageSize,
                    total: serverPagination?.totalCount,
                    mode: 'server',
                    enable: serverPagination?.pageSize !== 0
                };
                componentRef.value.updatePagination(pagination.value);
            }

            const entity = viewModel.entityStore?.getEntityListByPath(options.entityPath).getCurrentEntity();
            const primaryValue = entity?.idValue;
            componentRef.value.updateDataSource(entities);
            selectItemById(primaryValue);
        } else if (change.type === EntityChangeType.Append) {
            const appendEntityChange = change as AppendEntityChange;
            const entity = appendEntityChange.entities[0];
            const primaryValue = entity.idValue;
            componentRef.value.updateDataSource(entities);
            if (primaryValue && entities && entities.length > 0) {
                selectItemById(primaryValue);
            }
        } else if (change.type === EntityChangeType.CurrentChange) {
            const newCurrentEntity: Entity = (change as any).newCurrentEntity as Entity;
            let dataSourceCurrentId: any = newCurrentEntity.idValue;
            const changePath = change && change.path.toShortPath();
            // 上级表行切换
            if (changePath !== bindingPath && bindingPath.startsWith(changePath)) {
                componentRef.value.updateDataSource(entities);
                const entityList = viewModel.entityStore?.getEntityListByPath(options.entityPath);
                dataSourceCurrentId = entityList?.getCurrentEntity().idValue;
            }
            const dataGridCurrentRow = componentRef.value.getSelectionRow();
            const dataGridCurrentId = dataGridCurrentRow?.raw?.id;
            if (dataGridCurrentId === dataSourceCurrentId) {
                return;
            }
            selectItemById(dataSourceCurrentId);
        } else if (change.type === EntityChangeType.Remove) {
            componentRef.value.updateDataSource(entities);
            const newCurrentId = viewModel.entityStore?.getCurrentEntityByPath(options.entityPath).idValue;
            if (!newCurrentId) {
                return;
            }
            selectItemById(newCurrentId);
        } else if (change.type === EntityChangeType.ValueChange) {
            // componentRef.value.updateDataSource(entities);
            // 找到事件节点
            const changePath = change.path;
            const nodes = changePath.getNodes();
            let idValue: string | undefined | null = null;
            let index = -1;
            if (!entityPath || entityPath.length < 1) {
                idValue = nodes.at(0)?.getNodeValue();
            } else {
                index = nodes.findIndex((node: EntityPathNode) => node.getNodeType() === EntityPathNodeType.PropName && node.getNodeValue() === entityPath[entityPath.length - 1]);
                if (index === -1) {
                    return;
                }
                idValue = nodes.at(index + 1)?.getNodeValue();
            }
            if (!idValue) {
                return;
            }
            const rows = componentRef.value.getVisibleDataByIds([idValue]);
            if (!rows || rows.length < 1) {
                return;
            }
            const row = rows.at(0);

            const propertyNames = changePath.getNodes().slice(index + 1 + 1);
            const propertyName = propertyNames.map((node: EntityPathNode) => node.getNodeValue()).join('.');
            const valueChange = change as ChangeValueChange;
            if (!row.data[propertyName]) {
                return;
            }
            const patch: any = {};
            propertyNames.reduce((propertyObject: any, node: EntityPathNode, index: number) => {
                const propertyName = node.getNodeValue();
                if (index === propertyNames.length - 1) {
                    propertyObject[propertyName] = valueChange.newValue;
                    return propertyObject;
                } else {
                    propertyObject[propertyName] = {};
                    return propertyObject[propertyName];
                }
            }, patch);
            row.updateCell(patch, propertyName);
        } else if (change.type === EntityChangeType.Update) {
            componentRef.value.updateDataSource(entities);
            const entity = viewModel.entityStore?.getEntityListByPath(options.entityPath).getCurrentEntity();
            if (!entity || !entity.idValue) {
                return;
            }
            selectItemById(entity.idValue);
        }
    };
    function selectItemById(id: string) {
        if (!id) {
            return;
        }
        const currentRow = componentRef.value.getSelectionRow();
        const currentId = currentRow?.raw?.id;
        if (currentId === id) {
            return;
        }
        componentRef.value.clickRowItemById(id);
    }
    viewModel.entityStore?.watchChange((change: any) => {
        render(change);
    });
    // useEntityStateChange(viewModel, { [EntityChangeType.Append]: render, [EntityChangeType.Load]: render, [EntityChangeType.Remove]: render, [EntityChangeType.ValueChange]: render });
    /**
     * 行点击处理器
     * @param index index
     * @param data data
     * @returns 
     */
    function onClickRow(index: number, data: any) {
        if (!entityPath) {
            return;
        }
        let entitySchema: EntitySchema | undefined = viewModel.entityStore?.getEntitySchema();
        if (entityPath.getNodes().length !== 0) {
            const fieldSchema = viewModel.entityStore?.getEntitySchema().getFieldSchemaByPath(entityPath) as EntityFieldSchema;
            entitySchema = fieldSchema.entitySchema;
        }
        if (!entitySchema) {
            console.warn(`composition:useDataGridBinding,invalid entity path!`);
            return;
        }
        const primaryKey = entitySchema.getIdKey();
        const primaryValue = data[primaryKey];
        if (!primaryValue) {
            return;
        }
        viewModel.entityStore?.changeCurrentEntityByPath(entityPath, primaryValue);
    }
    return {
        data,
        onClickRow,
        pagination
    };
}