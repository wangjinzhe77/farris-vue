import { EntityChange, ViewModel, ViewModelState } from '@farris/devkit-vue';
import { EntityChangeHandler } from '../types';

export function useEntityStateChange(viewModel: ViewModel<ViewModelState>, entityChangeHandler: any) {
    viewModel.entityStore?.watchChange((change: any) => {
        const handler = entityChangeHandler[change.type];
        if (!handler) {
            return;
        }
        handler(change);
    });
}