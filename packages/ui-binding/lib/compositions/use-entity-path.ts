import { Entity, EntityList, EntityPath, FieldType, ViewModel, ViewModelState } from "@farris/devkit-vue";

export function useEntityPath(viewModel: ViewModel<ViewModelState>, entityPath: string | string[] | EntityPath, isEntityListPath: boolean = false) {
    const path = viewModel.entityStore?.createPath(entityPath || '/', isEntityListPath);
    function toShortPath(): string[] | null {
        if (path) {
            return path.toShortPath().split('/').filter((item)=> item);
        }
        return null;
    }
    function toLongPath() {
        const longPath: string[] = [];
        let currentEntity: any = viewModel.entityStore?.getCurrentEntity();
        const currentId = currentEntity.idValue;
        longPath.push(`${currentId}`);
        const shortPaths = toShortPath() || [];
        shortPaths.forEach((pathItem: string, pathIndex: number) => {
            const entitySchema = currentEntity.getSchema();
            const fieldSchema = entitySchema.getFieldSchemaByName(pathItem);
            if (!fieldSchema) {
                throw new Error(`${pathItem} not exist`);
            }

            if (fieldSchema.type === FieldType.EntityList) {
                const childEntityList = currentEntity[pathItem] as EntityList<Entity>;
                const currentChildEntity = childEntityList.getCurrentEntity();
                longPath.push(pathItem);

                // 实体列表短路径，最后一段不拼接当前实体ID
                if (isEntityListPath && pathIndex === shortPaths.length - 1) {
                    return;
                }
                longPath.push(`${currentChildEntity.idValue}`);
                currentEntity = currentChildEntity;
            } else if (fieldSchema.type === FieldType.Entity) {
                longPath.push(pathItem);
                currentEntity = currentEntity[pathItem];
            } else {
                longPath.push(pathItem);
            }
        });
        return longPath;
    }
    return {
        entityPath: path,
        toShortPath,
        toLongPath
    };
}