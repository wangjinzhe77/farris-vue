const { resolve } = require("path");
const { defineConfig } = require("vite");
const vue = require("@vitejs/plugin-vue");
const vueJsx = require("@vitejs/plugin-vue-jsx");

module.exports = function (options) {
    const { lib, outDir, minify = false, plugins = [] } = options;
    return defineConfig({
        configFile: false,
        publicDir: false,
        plugins: [
            vue(),
            vueJsx(),
            ...plugins
        ],
        resolve: {
            alias: [{ find: "@", replacement: resolve(__dirname, "../../") }],
        },
        build: {
            target:"es2015",
            lib,
            outDir,
            minify,
            rollupOptions: {
                // external: ["vue", "@vueuse/core", "@vue/shared", "dayjs"],
                external: (id) => {
                    const items = [
                        "vue",
                        "lodash",
                        "@vueuse/core",
                        "@vue/shared",
                        "dayjs",
                        "@/components",
                        "@farris/devkit-vue",
                        "@farris/bef"
                    ];
                    return items.find((item) => id.indexOf(item) === 0);
                },
                output: {
                    exports: "named",
                    globals: (id) => {
                        if (id.includes('@/components')) {
                            const name = id.split('/').pop();
                            return name.slice(0, 1).toUpperCase() + name.slice(1);
                        }
                        const map = {
                            vue: "Vue",
                            dayjs: "dayjs",
                        };
                        return map[id];
                    }
                },
            },
        },
    });
};
