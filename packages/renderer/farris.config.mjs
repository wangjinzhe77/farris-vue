import { fileURLToPath, URL } from 'node:url';
import banner from 'vite-plugin-banner';

export default {
    format: "system",
    // 输出目录  App模式默认值 './dist' Lib模式 './lib'
    // outDir: fileURLToPath(new URL('./dist', import.meta.url)),

    // 外部依赖排除项 默认值 { include: [], exclude: [] }

    // 是否排除 package.json 中 dependencies和 peerDependencies 依赖的包; App模式默认值 false Lib模式默认值 true
    externalDependencies: true,
    // 路径别名 默认值 null
    alias: [
        { find: '@', replacement: fileURLToPath(new URL('./src', import.meta.url)) }
    ],
    // 插件 默认值 [vue(), vueJsx()] 不要重复添加
    // plugins: [],
    // viteConfig 配置项
    viteConfig: {
        build: {
            outDir: "dist",
            assetsDir: "assets",
            rollupOptions: {
                output: {
                    entryFileNames: `renderer.js`,
                }
            },
            manifest: false,
            minify: 'terser',
            terserOptions: {
                compress: {
                    keep_classnames: true,
                    keep_fnames: true,
                    drop_console: true,
                    drop_debugger: true,
                },
                mangle: false,
                format: {
                    comments: /^!/
                }
            },
        }
    },
    plugins: [
        banner('Last Update Time: ' + new Date().toLocaleString())
    ]
};
