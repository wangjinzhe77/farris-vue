import { Injector } from "@farris/devkit-vue";
import { COMPONENT_CONFIG_DEPENDENCY_RESOLVER_TOKEN } from "../tokens";
import { ComponentConfigDependencyResolver } from "./component-config-dependency-resolver";

export class ComponentConfigDependencyResolverRegistry {
    public resolvers: ComponentConfigDependencyResolver[];

    constructor(private injector: Injector) {
        this.resolvers = this.injector.get<ComponentConfigDependencyResolver[]>(COMPONENT_CONFIG_DEPENDENCY_RESOLVER_TOKEN);
    }
}
