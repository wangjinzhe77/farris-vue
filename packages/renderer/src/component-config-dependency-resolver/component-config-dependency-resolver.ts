import { Config } from "../config";

export abstract class ComponentConfigDependencyResolver {
    public abstract resolve(schema: Record<string, any>): Config[] | null;
}
