import { Config } from "../config";
import { ConfigDependencyResolveService } from "../config-dependency-resolver";
import { ComponentConfigDependencyResolver } from "./component-config-dependency-resolver";

export class DataGridComponentConfigDependencyResolver extends ComponentConfigDependencyResolver {

    constructor(private configDepencencyResolveService: ConfigDependencyResolveService) {
        super();
    }

    public resolve(schema: Record<string, any>): Config[] | null {
        if (!schema || schema.type !== 'data-grid') {
            return null;
        }
        const { id, columns: fields, editable, disabled, pagination } = schema;
        const configs: Config[] = [];
        const editableConfigDeps = this.configDepencencyResolveService.resolve(editable);
        if (editableConfigDeps) {
            configs.push({ deps: editableConfigDeps, config: editable, path: '/editable' });
        }
        const disabledConfigDeps = this.configDepencencyResolveService.resolve(disabled);
        if (disabledConfigDeps) {
            configs.push({ deps: disabledConfigDeps, config: disabled, path: '/disabled' });
        }
        if (pagination) {
            const { disabled = false } = pagination;
            const paginationDisabledConfigDeps = this.configDepencencyResolveService.resolve(disabled);
            if (paginationDisabledConfigDeps) {
                configs.push({ deps: paginationDisabledConfigDeps, config: disabled, path: '/pagination/disabled' });
            }
        }

        if (!id || !fields || !Array.isArray(fields) || fields.length < 1) {
            return configs && configs.length > 0 ? configs : null;
        }
        return fields.reduce((configs: Config[], field: Record<string, any>) => {
            const { visible, editor, id } = field;
            const visibleConfigDeps = this.configDepencencyResolveService.resolve(visible);
            if (visibleConfigDeps) {
                configs.push({ deps: visibleConfigDeps, config: visible, path: `/columns/${id}:visible` });
            }
            if (editor) {
                const readonlyConfigDeps = this.configDepencencyResolveService.resolve(editor?.readonly);
                if (readonlyConfigDeps) {
                    configs.push({ deps: readonlyConfigDeps, config: editor?.readonly, path: `columns/${id}:editor/readonly` });
                }

                const disabledConfigDeps = this.configDepencencyResolveService.resolve(editor?.disabled);
                if (disabledConfigDeps) {
                    configs.push({ deps: disabledConfigDeps, config: editor?.disabled , path: `columns/${id}:editor/disabled`});
                }
            }
            return configs;
        }, configs);
    }
}
