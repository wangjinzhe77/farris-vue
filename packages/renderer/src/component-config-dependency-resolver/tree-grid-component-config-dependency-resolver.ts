import { Config } from "../config";
import { ConfigDependencyResolveService } from "../config-dependency-resolver";
import { ComponentConfigDependencyResolver } from "./component-config-dependency-resolver";

export class TreeGridComponentConfigDependencyResolver extends ComponentConfigDependencyResolver {

    constructor(private configDepencencyResolveService: ConfigDependencyResolveService) {
        super();
    }

    public resolve(schema: Record<string, any>): Config[] | null {
        if (!schema || schema.type !== 'tree-grid') {
            return null;
        }
        const { id, columns: fields, editable, disabled } = schema;
        const configs: Config[] = [];
        const editableConfigDeps = this.configDepencencyResolveService.resolve(editable);
        if (editableConfigDeps) {
            configs.push({ deps: editableConfigDeps, config: editable, path: '/editable' });
        }
        const disabledConfigDeps = this.configDepencencyResolveService.resolve(disabled);
        if (disabledConfigDeps) {
            configs.push({ deps: disabledConfigDeps, config: disabled, path: '/disabled' });
        }
        if (!id || !fields || !Array.isArray(fields) || fields.length < 1) {
            return configs && configs.length > 0 ? configs : null;
        }
        return fields.reduce((configs: Config[], field: Record<string, any>) => {
            const { visible, editor } = field;
            const visibleConfigDeps = this.configDepencencyResolveService.resolve(visible);
            if (visibleConfigDeps) {
                configs.push({ deps: visibleConfigDeps, config: visible, path: `/columns/${field.id}:visible` });
            }
            if (editor) {
                const readonlyConfigDeps = this.configDepencencyResolveService.resolve(editor?.readonly);
                if (readonlyConfigDeps) {
                    configs.push({ deps: readonlyConfigDeps, config: editor?.readonly, path: `/columns/${field.id}:editor/readonly` });
                }
            }
            return configs;
        }, configs);
    }
}
