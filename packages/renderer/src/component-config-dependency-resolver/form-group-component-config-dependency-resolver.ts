import { Config } from "../config";
import { ConfigDependencyResolveService } from "../config-dependency-resolver";
import { ComponentConfigDependencyResolver } from "./component-config-dependency-resolver";

export class FormGroupComponentConfigDependencyResolver extends ComponentConfigDependencyResolver {

    constructor(private configDepencencyResolveService: ConfigDependencyResolveService) {
        super();
    }

    public resolve(schema: Record<string, any>): Config[] | null {
        if (!schema || schema.type !== 'form-group') {
            return null;
        }
        const { id, editor } = schema;
        if (!id || !editor) {
            return null;
        }
        const { readonly, required, disabled } = editor;
        const configs = [];
        const readonlyConfigDeps = this.configDepencencyResolveService.resolve(readonly);
        if (readonlyConfigDeps) {
            configs.push({ deps: readonlyConfigDeps, config: readonly, path: '/editor/readonly' });
        }
        const disabledConfigDeps = this.configDepencencyResolveService.resolve(disabled);
        if (disabledConfigDeps) {
            configs.push({ deps: disabledConfigDeps, config: disabled, path: '/editor/disabled' });
        }
        const requiredConfigDeps = this.configDepencencyResolveService.resolve(required);
        if (requiredConfigDeps) {
            configs.push({ deps: requiredConfigDeps, config: required, path: '/editor/required' });
        }

        return configs;
    }
}
