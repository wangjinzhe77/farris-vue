 import { Config } from "../config";
import { ConfigDependencyResolveService } from "../config-dependency-resolver";
import { ComponentConfigDependencyResolver } from "./component-config-dependency-resolver";

export class ResponseToolbarComponentConfigDependencyResolver extends ComponentConfigDependencyResolver {

    constructor(private configDepencencyResolveService: ConfigDependencyResolveService) {
        super();
    }

    public resolve(schema: Record<string, any>): Config[] | null {
        if (!schema || schema.type !== 'response-toolbar') {
            return null;
        }
        const { id, buttons } = schema;
        if (!id || !buttons) {
            return null;
        }
        if (!buttons || !Array.isArray(buttons) || buttons.length < 1) {
            return null;
        }
        const configs: Config[] = [];
        buttons.forEach((button: Record<string, any>) => {
            const { disabled, visible } = button;
            const diabledConfigDeps = this.configDepencencyResolveService.resolve(disabled);
            if (diabledConfigDeps) {
                configs.push({ deps: diabledConfigDeps, config: disabled, path: `/buttons/${button.id}:disabled` });
            }
        });
        return configs;
    }
}
