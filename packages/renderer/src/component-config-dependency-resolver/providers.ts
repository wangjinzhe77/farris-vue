 import { Injector, StaticProvider } from "@farris/devkit-vue";
import { PageHeaderComponentConfigDependencyResolver } from "./page-header-component-config-dependency-resolver";
import { COMPONENT_CONFIG_DEPENDENCY_RESOLVER_TOKEN } from "../tokens";
import { FormGroupComponentConfigDependencyResolver } from "./form-group-component-config-dependency-resolver";
import { ConfigDependencyResolveService } from "../config-dependency-resolver";
import { DataGridComponentConfigDependencyResolver } from "./data-grid-component-config-dependency-resolver";
import { ComponentConfigDependencyResolveService } from "./component-config-dependency-resolve-service";
import { ComponentConfigDependencyResolverRegistry } from "./component-config-dependency-resolver-registry";
import { TabPageComponentConfigDependencyResolver } from "./tab-page-component-config-dependency-resolver";
import { ResponseToolbarComponentConfigDependencyResolver } from "./response-toolbar-component-config-dependency-resolver";
import { SectionComponentConfigDependencyResolver } from "./section-component-config-dependency-resolver";
import { TreeGridComponentConfigDependencyResolver } from "./tree-grid-component-config-dependency-resolver";

export const componentConfigDependencyResolverProviders: StaticProvider[] = [
    { provide: COMPONENT_CONFIG_DEPENDENCY_RESOLVER_TOKEN, useClass: PageHeaderComponentConfigDependencyResolver, deps: [ConfigDependencyResolveService], multi: true },
    { provide: COMPONENT_CONFIG_DEPENDENCY_RESOLVER_TOKEN, useClass: FormGroupComponentConfigDependencyResolver, deps: [ConfigDependencyResolveService], multi: true },
    { provide: COMPONENT_CONFIG_DEPENDENCY_RESOLVER_TOKEN, useClass: DataGridComponentConfigDependencyResolver, deps: [ConfigDependencyResolveService], multi: true },
    { provide: COMPONENT_CONFIG_DEPENDENCY_RESOLVER_TOKEN, useClass: TabPageComponentConfigDependencyResolver, deps: [ConfigDependencyResolveService], multi: true },
    { provide: COMPONENT_CONFIG_DEPENDENCY_RESOLVER_TOKEN, useClass: ResponseToolbarComponentConfigDependencyResolver, deps: [ConfigDependencyResolveService], multi: true },
    { provide: COMPONENT_CONFIG_DEPENDENCY_RESOLVER_TOKEN, useClass: SectionComponentConfigDependencyResolver, deps: [ConfigDependencyResolveService], multi: true },
    { provide: COMPONENT_CONFIG_DEPENDENCY_RESOLVER_TOKEN, useClass: TreeGridComponentConfigDependencyResolver, deps: [ConfigDependencyResolveService], multi: true },
    { provide: ComponentConfigDependencyResolverRegistry, useClass: ComponentConfigDependencyResolverRegistry, deps: [Injector] },
    { provide: ComponentConfigDependencyResolveService, useClass: ComponentConfigDependencyResolveService, deps: [ComponentConfigDependencyResolverRegistry] }
];
