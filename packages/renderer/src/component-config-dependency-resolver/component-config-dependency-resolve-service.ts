 import { Config } from "../config";
import { ComponentConfigDependencyResolver } from "./component-config-dependency-resolver";
import { ComponentConfigDependencyResolverRegistry } from "./component-config-dependency-resolver-registry";

export class ComponentConfigDependencyResolveService {
    constructor(private componentConfigDependencyResolveRegistry: ComponentConfigDependencyResolverRegistry) { }

    public resolve(schema: Record<string, any>): Config[] {
        return this.componentConfigDependencyResolveRegistry.resolvers.reduce((dependencies: Config[], resolver: ComponentConfigDependencyResolver) => {
            const configs = resolver.resolve(schema);
            if (configs) {
                dependencies.push(...configs);
            }
            return dependencies;
        }, []);
    }
}
