 import { Config } from "../config";
import { ConfigDependencyResolveService } from "../config-dependency-resolver";
import { ComponentConfigDependencyResolver } from "./component-config-dependency-resolver";

export class PageHeaderComponentConfigDependencyResolver extends ComponentConfigDependencyResolver {

    constructor(private configDepencencyResolveService: ConfigDependencyResolveService) {
        super();
    }

    public resolve(schema: Record<string, any>): Config[] | null {
        if (!schema || schema.type !== 'page-header') {
            return null;
        }
        const { id, toolbar } = schema;
        if (!id || !toolbar) {
            return null;
        }
        const { buttons } = toolbar;
        if (!buttons || !Array.isArray(buttons) || buttons.length < 1) {
            return null;
        }
        const configs: Config[] = [];
        buttons.forEach((button: Record<string, any>) => {
            const { disabled, visible } = button;
            const diabledConfigDeps = this.configDepencencyResolveService.resolve(disabled);
            if (diabledConfigDeps) {
                configs.push({ deps: diabledConfigDeps, config: disabled, path: `/toolbar/buttons/${button.id}:disabled` });
            }
        });
        return configs;
    }
}
