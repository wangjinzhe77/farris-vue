import { Injector } from "@farris/devkit-vue";
import { ConfigDependencyResolver } from "./types";
import { CONFIG_DEPENDENCY_RESOLVER_TOKEN } from "../tokens";

export class ConfigDependencyResolverRegistry {
    public resolvers: ConfigDependencyResolver[];
    constructor(private injector: Injector) {
        this.resolvers = this.injector.get<ConfigDependencyResolver[]>(CONFIG_DEPENDENCY_RESOLVER_TOKEN);
    }
}
