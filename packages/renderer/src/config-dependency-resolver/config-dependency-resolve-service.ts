 import { ConfigDependencyResolver } from "./types";
import { ConfigDependency } from "../config";
import { ConfigDependencyResolverRegistry } from "./config-dependency-resolver-registry";

export class ConfigDependencyResolveService {
    constructor(private configDependencyResolverRegistry: ConfigDependencyResolverRegistry) { }

    public resolve(config: string | boolean): ConfigDependency[] | null {
        const { resolvers } = this.configDependencyResolverRegistry;
        if (!resolvers) {
            return null;
        }
        return resolvers.reduce((dependencies: ConfigDependency[], resolver: ConfigDependencyResolver) => {
            const configs = resolver.resolve(config);
            if (configs) {
                dependencies.push(...configs);
            }
            return dependencies;
        }, []);
    }
}
