import { Injector, StaticProvider } from "@farris/devkit-vue";
import { ConfigDependencyResolveService } from "./config-dependency-resolve-service";
import { StateMachineConfigDependencyResolver } from "./state-machine-config-dependency-resolver";
import { CONFIG_DEPENDENCY_RESOLVER_TOKEN } from "../tokens";
import { UIStateConfigDependencyResolver } from "./ui-state-config-dependency-resolver";
import { StaticConfigDependencyResolver } from "./static-config-dependency-resolver";
import { EmptyConfigDependencyResolver } from "./empty-config-dependency-resolver";
import { EntityStateConfigDependencyResolver } from "./entity-state-config-dependency-resolver";
import { ConfigDependencyResolverRegistry } from "./config-dependency-resolver-registry";

export const configDependencyResolverProviders: StaticProvider[] = [
    { provide: CONFIG_DEPENDENCY_RESOLVER_TOKEN, useClass: StateMachineConfigDependencyResolver, deps: [], multi: true },
    { provide: CONFIG_DEPENDENCY_RESOLVER_TOKEN, useClass: UIStateConfigDependencyResolver, deps: [], multi: true },
    { provide: CONFIG_DEPENDENCY_RESOLVER_TOKEN, useClass: StaticConfigDependencyResolver, deps: [], multi: true },
    { provide: CONFIG_DEPENDENCY_RESOLVER_TOKEN, useClass: EmptyConfigDependencyResolver, deps: [], multi: true },
    { provide: CONFIG_DEPENDENCY_RESOLVER_TOKEN, useClass: EntityStateConfigDependencyResolver, deps: [], multi: true },
    { provide: ConfigDependencyResolverRegistry, useClass: ConfigDependencyResolverRegistry, deps: [Injector] },
    { provide: ConfigDependencyResolveService, useClass: ConfigDependencyResolveService, deps: [ConfigDependencyResolverRegistry] }
];
