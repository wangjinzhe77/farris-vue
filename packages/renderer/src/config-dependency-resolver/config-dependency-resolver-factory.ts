 
import { ConfigDependency } from "../config/types";
import { ConfigDependencyResolverRegistry } from "./config-dependency-resolver-registry";
import { ConfigDependencyResolver } from "./types";

export class ConfigDependencyResolverFactory {
    public static resolve(config: string) {
        return ConfigDependencyResolverRegistry.reduce((dependencies: ConfigDependency[], resolver: ConfigDependencyResolver) => {
            const rules = resolver.resolve(config);
            if (rules) {
                dependencies.push(...rules);
            }
            return dependencies;
        }, []);
    }
}
