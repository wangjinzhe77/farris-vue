import { ConfigDependency, DependencyType } from "../config";
import { ConfigDependencyResolver } from "./types";

 
export class StateMachineConfigDependencyResolver implements ConfigDependencyResolver {
    public resolve(config: string): ConfigDependency[] | null {
        if (typeof config !== 'string') {
            return null;
        }
        if (!config || config.length < 1) {
            return null;
        }
        config = config.trim().replace(/\[\s*(['"])([^\1]+?)\1\s*\]/g, '.$2');
        const regex = /^[!]?viewModel.stateMachine\.([a-zA-Z0-9_.]+)$/g;
        const results: string[] = [];
        let matches: RegExpExecArray | null;
        while ((matches = regex.exec(config)) !== null) {
            results.push(matches[1]);
        }
        if (results.length < 1) {
            return null;
        }
        return results.map((path: string) => {
            return { type: DependencyType.StateMachine, path };
        });
    }
}
