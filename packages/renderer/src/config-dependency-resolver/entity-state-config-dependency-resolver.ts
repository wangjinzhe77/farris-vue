import { ConfigDependency, DependencyType } from "../config";
import { ConfigDependencyResolver } from "./types";

export class EntityStateConfigDependencyResolver implements ConfigDependencyResolver {

    constructor() { }

    resolve(config: string): ConfigDependency[] | null {
        if (typeof config !== 'string') {
            return null;
        }
        if (!config || config.length < 1) {
            return null;
        }
        const regex = /(?:viewModel)\.entityState\.([a-zA-Z0-9_.-]+)/g;
        const results: string[] = [];
        let matches: RegExpExecArray | null;
        while ((matches = regex.exec(config)) !== null) {
            results.push(matches[1]);
        }
        if (results.length < 1) {
            return null;
        }
        return results.map((path: string) => {
            return { path, type: DependencyType.EntityState };
        });
    }
} 
