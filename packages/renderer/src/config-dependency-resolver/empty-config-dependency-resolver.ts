import { ConfigDependency, DependencyType } from '../config';
import { ConfigDependencyResolver } from './types';

export class EmptyConfigDependencyResolver implements ConfigDependencyResolver {
    resolve(config: string): ConfigDependency[] | null {
        if (config === '' || config === null || config === undefined) {
            return [{ type: DependencyType.Empty }];
        }
        return null;
    }
}
