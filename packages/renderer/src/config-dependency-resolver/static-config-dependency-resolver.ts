import { ConfigDependency, DependencyType } from "../config/types";
import { ConfigDependencyResolver } from "./types";

export class StaticConfigDependencyResolver implements ConfigDependencyResolver {
    public resolve(config: string): ConfigDependency[] | null {
        if (typeof config === 'boolean') {
            return [{ type: DependencyType.Static }];
        }
        if (typeof config === 'string' && config.match(/true|false/g)) {
            return [{ type: DependencyType.Static }];
        }
        return null;
    }
}
