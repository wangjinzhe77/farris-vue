import { ConfigDependency } from "../config";

export interface ConfigDependencyResolver {
    resolve(config: string | boolean): ConfigDependency[] | null;
}
