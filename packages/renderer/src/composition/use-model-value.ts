import { Injector, Module } from "@farris/devkit-vue";
import { MODULE_CONFIG_ID_TOKEN } from "../tokens";
import { useModelValueResolver } from "./use-model-value-resolver";
import { ref, Ref } from "vue";
import { ENTITY_STORE_SUFFIX } from "../types";

export function useModelValue(formInjector: Injector, module: Module) {
    const modelValue: Ref = ref({});
    const moduleConfigId = formInjector.get(MODULE_CONFIG_ID_TOKEN);
    const entityStore = module.getEntityStore(moduleConfigId + ENTITY_STORE_SUFFIX);
    entityStore?.watchChange((change: any) => {
        const modelValueResolver = useModelValueResolver(formInjector);
        modelValue.value = modelValueResolver.resolve();
    });
    return {
        modelValue
    };
}
