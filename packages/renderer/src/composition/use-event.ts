 
/* eslint-disable no-use-before-define */
import { Injector } from "@farris/devkit-vue";
import { useEventEmitter } from "./use-event-emitter";
import { createEventHandlerResolver, EventHandlerResolver, resolverMap } from '@farris/ui-vue';
import { useFormMetadataService } from "./use-form-metadata-service";
import { useModule } from "./use-module";

export function useEvent(injector: Injector) {
    const { emitter } = useEventEmitter(injector);
    const formMetadataService = useFormMetadataService(injector);
    const module = useModule(injector);

    function onEventHandler(event: any) {
        const { name, type, schema, token } = event;
        emitter.emit(name, event);
        const resolver = resolverMap[type];
        const eventHandlerResolver: EventHandlerResolver = resolver && resolver.eventHandlerResolver ? resolver.eventHandlerResolver : createEventHandlerResolver();
        const eventHandler: string = eventHandlerResolver.resolve(schema, event);
        if (!eventHandler) {
            return;
        }
        const { commandName, viewModelId } = extraEventHandler(eventHandler);
        const commandViewModelId = viewModelId || getElementViewModelId(token);
        if (!commandViewModelId) {
            return;
        }
        // viewModelId => componentId
        const componentId = formMetadataService.convertViewModelIdToComponentId(commandViewModelId);
        if (!componentId) {
            return;
        }
        const viewModel = module.getViewModel(componentId);
        if (!viewModel) {
            return;
        }
        const mappedViewModel = viewModel as any;
        mappedViewModel[commandName]();
    }
    function extraEventHandler(eventHandler: string): { viewModelId: string | null; commandName: string; } {
        let viewModelId: string | null = null;
        let commandName = eventHandler;
        if (eventHandler.indexOf('.') !== -1) {
            const chaindCommandName = eventHandler.split('.');
            viewModelId = chaindCommandName[chaindCommandName.length - 2];
            commandName = chaindCommandName[chaindCommandName.length - 1];
        }
        return {
            viewModelId, commandName
        };
    }

    function getElementViewModelId(token: string): string | null {
        const relatedComponent = formMetadataService.getRelatedComponent(token);
        if (!relatedComponent) {
            return null;
        }
        return relatedComponent.viewModel;
    }

    return {
        onEventHandler
    };
}
