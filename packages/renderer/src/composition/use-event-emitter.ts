import { Injector } from "@farris/devkit-vue";
import { EventEmitter } from "../common/index";
import { UseEventEmitter } from "../types";

export function useEventEmitter(injector: Injector): UseEventEmitter {
    const eventEmitter = injector.get<EventEmitter>(EventEmitter, undefined);
    return {
        emitter: eventEmitter
    };
}
