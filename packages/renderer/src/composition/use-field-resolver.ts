 
 
import { Injector } from "@farris/devkit-vue";
import { UseFieldResolver } from "../types";
import { FieldResolver } from "../resolvers";

export function useFieldResolver(injector: Injector): UseFieldResolver {
    const fieldResolver = injector.get<FieldResolver>(FieldResolver, undefined);
    return fieldResolver;
}
