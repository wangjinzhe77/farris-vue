import { Injector } from "@farris/devkit-vue";
import { ComponentConfigResolveService } from "../component-config-resolver";

export function useComponentConfigResolver(injector: Injector) {
    const componentConfigResolveService = injector.get<ComponentConfigResolveService>(ComponentConfigResolveService);
    return componentConfigResolveService;
}
