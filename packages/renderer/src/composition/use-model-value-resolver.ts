 
import { Injector } from "@farris/devkit-vue";
import { ModelValueResolver } from "../resolvers";

export function useModelValueResolver(injector: Injector) {
    const modelValueResolver = injector.get<ModelValueResolver>(ModelValueResolver);
    return modelValueResolver;
}
