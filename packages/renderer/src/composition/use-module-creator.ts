import { createDynamicModule, createViewModel, ModuleConfig } from "@farris/devkit-vue";
import { Ref } from "vue";

export function useModuleCreator(metadata: Ref, moduleConfig: ModuleConfig){
    const module = createDynamicModule(moduleConfig);
    const { components } = metadata.value.form?.content?.module || [];
    components.forEach((component: any) => {
        const viewModel = createViewModel(module, component.id);
        module.registerViewModel(component.id, viewModel);
    });
    return module;
}
