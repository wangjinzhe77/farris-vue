 
 
import { Injector } from "@farris/devkit-vue";
import { UseEntityResolver } from "../types";
import { EntityResolver } from "../resolvers";

export function useEntityResolver(injector: Injector): UseEntityResolver {
    const entityResolver = injector.get<EntityResolver>(EntityResolver, undefined);
    return entityResolver;
}
