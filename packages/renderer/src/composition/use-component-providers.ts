
import { MESSAGE_BOX_SERVICE_TOKEN, NOTIFY_SERVICE_TOKEN, LOADING_SERVICE_TOKEN, MODAL_SERVICE_TOKEN } from '@farris/command-services-vue';
import { StaticProvider } from '@farris/devkit-vue';
import { inject } from 'vue';

export function useComponentProviders() {
    const MessageBoxService: any = inject('FMessageBoxService');
    const NotifyService: any = inject('FNotifyService');
    const LoadingService = inject('FLoadingService');
    const ModalService = inject('FModalService');

    const providers: StaticProvider[] = [
        { provide: MESSAGE_BOX_SERVICE_TOKEN, useValue: MessageBoxService },
        { provide: NOTIFY_SERVICE_TOKEN, useValue: NotifyService },
        { provide: LOADING_SERVICE_TOKEN, useValue: LoadingService },
        { provide: MODAL_SERVICE_TOKEN, useValue: ModalService }
    ];
    return {
        providers
    };
}
