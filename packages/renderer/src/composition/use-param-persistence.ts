import { Injector, Module } from '@farris/devkit-vue';
import { ApplicationParamService } from '@farris/command-services-vue';
import { useFormMetadataService } from './use-form-metadata-service';
import { cloneDeep } from 'lodash-es';
import { FORM_METADATA_TOKEN } from '../tokens';
import { useFormEngine } from './use-form-engine';
import { ref, Ref } from 'vue';

export function useParamPersistence(injector: Injector, module: Module) {
    const schema: Ref = ref();
    const formMetadataService = useFormMetadataService(injector);
    const rootComponent = formMetadataService.getFrameComponent();
    if (!rootComponent) {
        return null;
    }
    const frameId = rootComponent.id;
    const viewModel = module.getViewModel(frameId);
    if (!viewModel) {
        return null;
    }
    const applicationParamService = viewModel.getInjector().get<ApplicationParamService>(ApplicationParamService, undefined);
    if (!applicationParamService) {
        return null;
    }
    const formMetadata = injector.get(FORM_METADATA_TOKEN);
    const formEngine = useFormEngine(injector);
    applicationParamService.parseParams(() => {
        const metadata = cloneDeep(formMetadata);
        formEngine.resolve(metadata.module.components);
        schema.value = metadata;
    });
    return {
        schema
    };
}
