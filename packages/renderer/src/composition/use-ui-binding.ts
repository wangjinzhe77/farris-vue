/* eslint-disable no-use-before-define */
import { Injector } from '@farris/devkit-vue';
import { useDataGridBinding, useFormGroupBinding, useLookupBinding, useTreeGridBinding } from '@farris/ui-binding-vue';
import { useFormMetadataService } from './use-form-metadata-service';
import { useEntityResolver } from './use-entity-resolver';
import { useModule } from './use-module';
import { useFieldResolver } from './use-field-resolver';

export function useUIBinding(injector: Injector) {
    const formMetadata = useFormMetadataService(injector);
    const entityResolver = useEntityResolver(injector);
    const fieldResolver = useFieldResolver(injector);
    const module = useModule(injector);

    function onComponentReady(payload: any) {
        const { ref, type, id } = payload;
        if (!id) {
            return;
        }
        const viewSchema = formMetadata.getMetadataById(id);
        if (type === 'data-grid' || type === 'tree-grid') {
            const { dataSource } = viewSchema || {};
            const entityPath = resolveEntityPath(dataSource);
            if (!entityPath) {
                return;
            }
            const viewModel = resolveViewModel(entityPath);
            if (!viewModel) {
                return;
            }
            if (type === 'data-grid') {
                useDataGridBinding(ref, { entityPath, viewModel });
            } else if (type === 'tree-grid') {
                const hierarchyKey = viewSchema.udtField;
                useTreeGridBinding(ref, { entityPath, viewModel, hierarchyKey });
            }
        } else if (type === 'form-group') {
            const { field } = viewSchema.binding || {};
            if (!field) {
                return;
            }
            const { dataSource } = fieldResolver.resolve(field) || {};
            if (!dataSource) {
                return;
            }
            const entityPath = resolveEntityPath(dataSource);
            if (!entityPath) {
                return;
            }
            const viewModel = resolveViewModel(entityPath);
            if (!viewModel) {
                return;
            }
            useFormGroupBinding(ref, { entityPath, viewModel }, viewSchema);
        }

    }
    function resolveEntityPath(dataSource: string) {
        if (!dataSource) {
            return null;
        }
        const entity = entityResolver.resolve(dataSource);
        if (!entity) {
            return null;
        }
        const entityPath = '/' + entity.bindingPaths.join('/');
        return entityPath;
    }

    function resolveViewModel(entityPath: string) {
        const viewModelSchema = formMetadata.getViewModels().find((viewModel) => viewModel.bindTo === entityPath);
        if (!viewModelSchema) {
            return null;
        }
        const componentId = formMetadata.convertViewModelIdToComponentId(viewModelSchema.id);
        const viewModel = module.getViewModel(componentId);
        if (!viewModel) {
            return null;
        }
        return viewModel;
    }
    return {
        onComponentReady
    };
}
