import { Injector } from "@farris/devkit-vue";
import { useFormMetadataService } from "./use-form-metadata-service";
import { useModule } from "./use-module";
import { ENTITY_STORE_SUFFIX, MetadataType } from "../types";
import { useEntityResolver } from "./use-entity-resolver";
import { MODULE_CONFIG_ID_TOKEN } from "../tokens";

export function usePagination(injector: Injector) {
    const formMetadata = useFormMetadataService(injector);
    const module = useModule(injector);
    const entityResolver = useEntityResolver(injector);

    const datagridComponents = formMetadata.getMetadataByType(MetadataType.DataGrid);
    datagridComponents.forEach((metadata: Record<string, any>) => {
        const { pagination = {}, dataSource = '' } = metadata.schema;
        const { enable, mode, size: pageSize = 20 } = pagination;
        if (enable && mode === 'server' && dataSource) {
            const { bindingPaths } = entityResolver.resolve(dataSource) || {};
            if (bindingPaths) {
                const moduleConfigId = injector.get<string>(MODULE_CONFIG_ID_TOKEN);
                const storeId = moduleConfigId + ENTITY_STORE_SUFFIX;
                const entityStore = module.getEntityStore(storeId);
                entityStore?.setPaginationByPath(bindingPaths, { pageSize });
            }
        }
    });
}
