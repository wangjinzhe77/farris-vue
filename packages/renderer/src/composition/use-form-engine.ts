import { Injector } from "@farris/devkit-vue";
import { FormEngine } from "../form-engine";

export function useFormEngine(injector: Injector) {
    return injector.get<FormEngine>(FormEngine);
}
