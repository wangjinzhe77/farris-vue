import { NavigationEventService, TAB_EVENT } from "@farris/command-services-vue";
import { Injector } from "@farris/devkit-vue";
import { useFormMetadataService } from "./use-form-metadata-service";
import { useModule } from "./use-module";

export function useNavigation(injector: Injector) {
    const navigationEventService = injector.get<NavigationEventService>(NavigationEventService);
    const formMetadata = useFormMetadataService(injector);
    const module = useModule(injector);

    navigationEventService.addEventListener(TAB_EVENT.onTabRefresh, () => {
        const components = formMetadata.getComponents();
        components.forEach((component: any) => {
            if (component.schema.onInit) {
                const viewModel: any = module.getViewModel(component.schema.id);
                viewModel[component.schema.onInit]();
            }
        });
    });
}
