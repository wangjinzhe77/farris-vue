import { Injector } from "@farris/devkit-vue";
import { CallbackHandlerRegistry } from "../callback-handler";

export function useCallbackHandlerRegistry(injector: Injector) {
    return injector.get(CallbackHandlerRegistry);
}
