import { Entity, Injector } from "@farris/devkit-vue";
import { useModule } from "./use-module";
import { MODULE_CONFIG_ID_TOKEN } from "../tokens";
import { BefRepository } from "@farris/bef-vue";
import { RuntimeFrameworkService } from "@farris/command-services-vue";

export function useSession(injector: Injector) {
    const module = useModule(injector);
    const moduleConfigId = injector.get<string>(MODULE_CONFIG_ID_TOKEN);
    const repository: BefRepository<Entity> = module.getRepository(`${moduleConfigId}-repository`) as BefRepository<Entity>;
    if (!repository) {
        return;
    }
    const runtimeFrameworkService = injector.get<RuntimeFrameworkService>(RuntimeFrameworkService);
    if (!runtimeFrameworkService) {
        return;
    }
    const { tabId } = runtimeFrameworkService;
    const runtimeContext: any = {};
    if (tabId) {
        runtimeContext.tabId = tabId;
    }
    repository.sessionService.clearBeSessionId(runtimeContext);
}
