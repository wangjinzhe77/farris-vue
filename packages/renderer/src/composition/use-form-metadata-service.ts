import { Injector } from "@farris/devkit-vue";
import { FormMetadataService } from "../service";

export function useFormMetadataService(injector: Injector){
    const formMetadataService = injector.get<FormMetadataService>(FormMetadataService);
    return formMetadataService;
}
