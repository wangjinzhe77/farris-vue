import { befProviders, befRootProviders } from "@farris/bef-vue";
import { commandServiceProviders, commandServiceRootProviders } from "@farris/command-services-vue";
import { Injector, ModuleConfig, RENDER_ENGINE_TOKEN, StaticProvider } from "@farris/devkit-vue";
import { Ref } from "vue";
import { ModuleConfigBuilder } from "../config-builders";
import { RenderEngineImpl } from "../render-engine";
import { RENDER_TOKEN } from "../tokens";

export function useModuleConfig(metadata: Ref, uiProviders: StaticProvider[], render: Ref): ModuleConfig {
    const moduleMetaContext: any = {
        form: metadata?.value.form.content,
        webcmds: metadata?.value.commands,
        stateMachines: metadata?.value.stateMachines,
        webComponents: metadata?.value.webComponents,
        moduleProviders: [
            ...uiProviders,
            ...commandServiceRootProviders,
            ...befRootProviders,
            { provide: RENDER_TOKEN, useValue: render, deps: [] },
            { provide: RENDER_ENGINE_TOKEN, useClass: RenderEngineImpl, deps: [Injector] }
        ],
        viewModelProviders: [
            ...befProviders,
            ...commandServiceProviders,
        ],
    };
    const moduleConfigBuilder = new ModuleConfigBuilder(moduleMetaContext);
    const moduleConfig = moduleConfigBuilder.build();
    return moduleConfig;
}
