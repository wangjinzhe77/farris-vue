import { createInjector, Module, StaticProvider } from "@farris/devkit-vue";
import { MODULE_CONFIG_ID_TOKEN, FORM_METADATA_TOKEN } from "../tokens";
import { configProviders } from "../config";
import { configDependencyResolverProviders } from "../config-dependency-resolver";
import { componentConfigResolverProviders } from "../component-config-resolver";
import { componentConfigDependencyResolverProviders } from "../component-config-dependency-resolver";
import { changeObserverProviders } from "../change-observer";
import { changeResolverProviders } from "../change-resolver";
import { renderEngineProviders } from "../render-engine";
import { formEngineProviders } from "../form-engine";
import { eventEmitterProviders } from "../common/providers";
import { serviceProviders } from "../service";
import { eventHanderProviders } from "../event-handler";
import { resolverProviders } from "../resolvers";
import { Ref } from "vue";
import { callbackHandlerProviders } from "../callback-handler";

export function useFormInjector(metadata: Ref, module: Module, moduleConfigId: string) {
    const providers: StaticProvider[] = [
        { provide: FORM_METADATA_TOKEN, useValue: metadata?.value.form.content, deps: [] },
        { provide: MODULE_CONFIG_ID_TOKEN, useValue: moduleConfigId, deps: [] },
        ...configProviders,
        ...configDependencyResolverProviders,
        ...componentConfigResolverProviders,
        ...componentConfigDependencyResolverProviders,
        ...changeObserverProviders,
        ...changeResolverProviders,
        ...renderEngineProviders,
        ...formEngineProviders,
        ...eventEmitterProviders,
        ...serviceProviders,
        ...eventHanderProviders,
        ...resolverProviders,
        ...callbackHandlerProviders
    ];
    const formInjector = createInjector({
        providers,
        parent: module.getInjector()
    });
    return formInjector;
}
