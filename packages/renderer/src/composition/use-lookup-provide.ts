import { LookupDataService } from "@farris/command-services-vue";
import { Injector, Module } from "@farris/devkit-vue";
import { F_LOOKUP_HTTP_SERVICE_TOKEN } from "@farris/ui-vue";
import { getCurrentInstance } from "vue";
import { useFormMetadataService } from "./use-form-metadata-service";

export function useLookupProvide(injector: Injector, module: Module) {
    const formMetadataService = useFormMetadataService(injector);
    const frameComponent = formMetadataService.getFrameComponent();
    if (!frameComponent) {
        return;
    }
    const frameId = frameComponent.id;
    const viewModel = module.getViewModel(frameId);
    if (!viewModel) {
        return;
    }
    const lookupDataService = viewModel.getInjector().get<LookupDataService>(LookupDataService, undefined);
    const currentApp = getCurrentInstance()?.appContext.app;
    currentApp?.provide(F_LOOKUP_HTTP_SERVICE_TOKEN, lookupDataService);
}
