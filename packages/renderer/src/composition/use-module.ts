import { Injector, Module } from "@farris/devkit-vue";

export function useModule(injector: Injector){
    return injector.get<Module>(Module);
}
