import { ViewModelMeta } from "@farris/devkit-vue";

export function useMetadataRefine(metadata: any) {
    if (!metadata) {
        return;
    }
    const formMetadata = metadata.form?.content;
    if (!formMetadata) {
        return;
    }
    const stateMachines = formMetadata.module?.stateMachines;
    const viewModels = formMetadata.module?.viewmodels;
    if (!stateMachines || !viewModels || !Array.isArray(stateMachines) || stateMachines.length < 1 || !Array.isArray(viewModels) || viewModels.length < 1) {
        return;
    }
    const { id } = stateMachines.at(0);
    viewModels.forEach((viewModel: ViewModelMeta) => {
        viewModel.stateMachine = id;
    });
}
