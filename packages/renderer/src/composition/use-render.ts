import { Injector } from "@farris/devkit-vue";
import { RENDER_TOKEN } from "../tokens";
import { Ref } from "vue";

export function useRender(injector: Injector) {
    return injector.get<Ref>(RENDER_TOKEN);
}
