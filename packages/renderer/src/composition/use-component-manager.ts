import { Injector } from "@farris/devkit-vue";
import { ComponentService } from "../service";

export function useComponentManager(injector: Injector) {
    return injector.get<ComponentService>(ComponentService);
}
