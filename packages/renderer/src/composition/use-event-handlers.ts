import { Injector } from "@farris/devkit-vue";
import { EVENT_HANDLERS_TOKEN } from "../tokens";
import { EventHandler } from "../event-handler";

export function useEventHandlers(injector: Injector) {
    const eventHandlers = injector.get<EventHandler[]>(EVENT_HANDLERS_TOKEN, []);
    eventHandlers.forEach((eventHandler: EventHandler) => {
        eventHandler.bind();
    });
}
