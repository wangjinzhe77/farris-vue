import { Injector } from "@farris/devkit-vue";
import { ChangeResolver } from "./change-resolver";
import { CHANGE_RESOLVER_TOKEN } from "../tokens";

export class ChangeResolverRegistry {
    public registry: ChangeResolver<any>[];

    constructor(private injector: Injector) {
        this.registry = this.injector.get<ChangeResolver<any>[]>(CHANGE_RESOLVER_TOKEN);
    }
}
