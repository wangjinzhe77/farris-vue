 import { StateMachine, StateMachineState } from "@farris/devkit-vue";
import { Change, ChangeSource } from "../change-observer";
import { ComponentConfigDependencyRegistry, Config, ConfigDependency, DependencyType } from "../config";
import { ChangeResolver } from "./change-resolver";

export class StateMachineChangeResolver extends ChangeResolver<StateMachine<StateMachineState>> {

    constructor(private componentConfigDependencyRegistry: ComponentConfigDependencyRegistry) {
        super();
    }

    public resolve(change: Change<StateMachine<StateMachineState>>): Map<string, Config[]> | null{
        if (change.source !== ChangeSource.StateMachine) {
            return null;
        }
        const registry = this.componentConfigDependencyRegistry.getComponents();
        const result: Map<string, Config[]> = new Map();
        registry.entries().forEach(([id, configs]) => {
            const matchedConfigs = configs.filter((config: Config) => {
                const match = config.deps.find((dependency: ConfigDependency) => {
                    return dependency.type === DependencyType.StateMachine;
                });
                return !!match;
            });
            if (matchedConfigs && matchedConfigs.length > 0) {
                result.set(id, matchedConfigs);
            }
        });
        return result;
    }
}
