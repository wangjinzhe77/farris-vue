import { Change } from "../change-observer";
import { Config } from "../config";

export abstract class ChangeResolver<T> {
    public abstract resolve(change: Change<T>): Map<string, Config[]> | null;
}
