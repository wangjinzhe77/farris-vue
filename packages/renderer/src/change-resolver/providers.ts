import { Injector, StaticProvider } from "@farris/devkit-vue";
import { StateMachineChangeResolver } from "./state-machine-change-resolver";
import { CHANGE_RESOLVER_TOKEN } from "../tokens";
import { ComponentConfigDependencyRegistry } from "../config";
import { ChangeResolverRegistry } from "./change-resolver-registry";
import { ChangeResolveService } from "./change-resolve-service";
import { EntityStoreChangeResolver } from "./entity-store-change-resolver";
import { UIStateChangeResolver } from "./ui-state-change-resolver";

export const changeResolverProviders: StaticProvider[] = [
    { provide: CHANGE_RESOLVER_TOKEN, useClass: StateMachineChangeResolver, deps: [ComponentConfigDependencyRegistry], multi: true },
    { provide: CHANGE_RESOLVER_TOKEN, useClass: EntityStoreChangeResolver, deps: [ComponentConfigDependencyRegistry], multi: true },
    { provide: CHANGE_RESOLVER_TOKEN, useClass: UIStateChangeResolver, deps: [ComponentConfigDependencyRegistry], multi: true },
    { provide: ChangeResolverRegistry, useClass: ChangeResolverRegistry, deps: [Injector] },
    { provide: ChangeResolveService, useClass: ChangeResolveService, deps: [ChangeResolverRegistry] }
];
