import { Entity, EntityState } from "@farris/devkit-vue";
import { ChangeResolver } from "./change-resolver";
import { Change, ChangeSource } from "../change-observer";
import { ComponentConfigDependencyRegistry, Config, ConfigDependency, DependencyType } from "../config";

export class EntityStoreChangeResolver extends ChangeResolver<EntityState<Entity>> {

    constructor(private componentConfigDependencyRegistry: ComponentConfigDependencyRegistry) {
        super();
    }

    public resolve(change: Change<EntityState<Entity>>): Map<string, Config[]> | null {
        if (change.source !== ChangeSource.EntityState) {
            return null;
        }
        const registry = this.componentConfigDependencyRegistry.getComponents();
        const result: Map<string, Config[]> = new Map();
        registry.entries().forEach(([id, configs]) => {
            const matchedConfigs = configs.filter((config: Config) => {
                const match = config.deps.find((dependency: ConfigDependency) => {
                    return dependency.type === DependencyType.EntityState;
                });
                return !!match;
            });
            if (matchedConfigs && matchedConfigs.length > 0) {
                result.set(id, matchedConfigs);
            }
        });
        return result;
    }
}
