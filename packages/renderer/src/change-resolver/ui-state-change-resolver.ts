import { UIState } from "@farris/devkit-vue";
import { ComponentConfigDependencyRegistry, Config, ConfigDependency, DependencyType } from "../config";
import { ChangeResolver } from "./change-resolver";
import { Change, ChangeSource } from "../change-observer";

export class UIStateChangeResolver extends ChangeResolver<UIState> {

    constructor(private componentConfigDependencyRegistry: ComponentConfigDependencyRegistry) {
        super();
    }

    public resolve(change: Change<UIState>): Map<string, Config[]> | null {
        if (change.source !== ChangeSource.UIState) {
            return null;
        }
        const registry = this.componentConfigDependencyRegistry.getComponents();
        const result: Map<string, Config[]> = new Map();
        registry.entries().forEach(([id, configs]) => {
            const matchedConfigs = configs.filter((config: Config) => {
                const match = config.deps.find((dependency: ConfigDependency) => {
                    return dependency.type === DependencyType.UIState;
                });
                return !!match;;
            });
            if (matchedConfigs && matchedConfigs.length > 0) {
                result.set(id, matchedConfigs);
            }
        });
        return result;
    }
}
