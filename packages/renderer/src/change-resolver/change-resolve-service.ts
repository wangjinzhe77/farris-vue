import { Change } from "../change-observer";
import { Config } from "../config";
import { ChangeResolver } from "./change-resolver";
import { ChangeResolverRegistry } from "./change-resolver-registry";

export class ChangeResolveService {
    constructor(private changeResolverRegistry: ChangeResolverRegistry) { }

    public resolve(change: Change<any>): Map<string, Config[]> {
        return this.changeResolverRegistry.registry.reduce((results: Map<string, Config[]>, resolver: ChangeResolver<any>) => {
            const result = resolver.resolve(change);
            if (result && result.size > 0) {
                result.keys().forEach((componentId: string)=>{
                    if (results.has(componentId)) {
                        results.get(componentId)!.push(...result.get(componentId)!);
                    } else {
                        results.set(componentId, result.get(componentId)!);
                    }
                });
            }
            return results;
        }, new Map());
    }
}
