 export abstract class ComponentConfigResolver {
    public abstract type: string;

    public abstract resolve(metadata: Record<string, any>): Record<string, any>;
}
