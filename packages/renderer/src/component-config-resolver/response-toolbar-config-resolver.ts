import { ConfigResolver } from "../config";
import { FormMetadataService } from "../service";
import { ComponentConfigResolver } from "./component-config-resolver";

export class ResponseToolbarConfigResolver extends ComponentConfigResolver {
    public type: string = 'response-toolbar';
    constructor(private formMetadataService: FormMetadataService, private configResolver: ConfigResolver) {
        super();
    }

    public resolve(metadata: Record<string, any>): Record<string, any> {
        const { buttons } = metadata;
        if (!buttons || !Array.isArray(buttons) || buttons.length < 1) {
            return metadata;
        }
        buttons.forEach((button: Record<string, any>) => {
            const { disabled, visible } = button;
            button.disabled = this.configResolver.resolve(disabled);
        });
        return metadata;
    }
}
