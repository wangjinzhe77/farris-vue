import { FormMetadataService } from "../service";
import { ComponentConfigResolverRegistry } from "./component-config-resolver-registry";

export class ComponentConfigResolveService {
    constructor(private componentConfigResolverRegistry: ComponentConfigResolverRegistry, private formMetadataService: FormMetadataService) { }

    public resolve(metadata: Record<string, any>): Record<string, any> {
        const resolver = this.componentConfigResolverRegistry.getResolverByType(metadata.type);
        if (!resolver) {
            return metadata;
        }
        return resolver.resolve(metadata);
    }
}
