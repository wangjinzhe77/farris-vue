import { FormMetadataService } from "../service";
import { ComponentConfigResolver } from "./component-config-resolver";
import { ConfigResolver } from "../config";

export class FormGroupComponentConfigResolver extends ComponentConfigResolver {
    public type: string='form-group';

    constructor(private formMetadataService: FormMetadataService, private configResolver: ConfigResolver) {
        super();
    }

    public resolve(metadata: Record<string, any>): Record<string, any> {
        const { visible, editor = null } = metadata;
        metadata.visible = this.configResolver.resolve(visible);
        if (!editor) {
            return metadata;
        }
        const { readonly, required, disabled } = editor;
        editor.readonly = this.configResolver.resolve(readonly);
        editor.disabled = this.configResolver.resolve(disabled);
        editor.required = this.configResolver.resolve(required);
        return metadata;
    }
}
