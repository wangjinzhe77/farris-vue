 import { Injector, StaticProvider } from "@farris/devkit-vue";
import { COMPONENT_CONFIG_RESOLVER_TOKEN } from "../tokens";
import { FormGroupComponentConfigResolver } from "./form-group-component-config-resolver";
import { FormMetadataService } from "../service";
import { ConfigResolver } from "../config";
import { DataGridComponentConfigResolver } from "./data-grid-component-config-resolver";
import { PageHeaderComponentConfigResolver } from "./page-header-component-config-resolver";
import { ComponentConfigResolveService } from "./component-config-resolve-service";
import { ComponentConfigResolverRegistry } from "./component-config-resolver-registry";
import { TabPageComponentConfigResolver } from "./tab-page-component-config-resolver";
import { ResponseToolbarConfigResolver } from "./response-toolbar-config-resolver";
import { SectionComponentConfigResolver } from "./section-component-config-resolver";
import { TreeGridComponentConfigResolver } from "./tree-grid-component-config-resolver";

export const componentConfigResolverProviders: StaticProvider[] = [
    { provide: COMPONENT_CONFIG_RESOLVER_TOKEN, useClass: FormGroupComponentConfigResolver, deps: [FormMetadataService, ConfigResolver], multi: true },
    { provide: COMPONENT_CONFIG_RESOLVER_TOKEN, useClass: DataGridComponentConfigResolver, deps: [FormMetadataService, ConfigResolver], multi: true },
    { provide: COMPONENT_CONFIG_RESOLVER_TOKEN, useClass: PageHeaderComponentConfigResolver, deps: [FormMetadataService, ConfigResolver], multi: true },
    { provide: COMPONENT_CONFIG_RESOLVER_TOKEN, useClass: TabPageComponentConfigResolver, deps: [FormMetadataService, ConfigResolver], multi: true },
    { provide: COMPONENT_CONFIG_RESOLVER_TOKEN, useClass: ResponseToolbarConfigResolver, deps: [FormMetadataService, ConfigResolver], multi: true},
    { provide: COMPONENT_CONFIG_RESOLVER_TOKEN, useClass: SectionComponentConfigResolver, deps: [FormMetadataService, ConfigResolver], multi: true},
    { provide: COMPONENT_CONFIG_RESOLVER_TOKEN, useClass: TreeGridComponentConfigResolver, deps: [FormMetadataService, ConfigResolver], multi: true},
    { provide: ComponentConfigResolverRegistry, useClass: ComponentConfigResolverRegistry, deps: [Injector] },
    { provide: ComponentConfigResolveService, useClass: ComponentConfigResolveService, deps: [ComponentConfigResolverRegistry, FormMetadataService] }
];
