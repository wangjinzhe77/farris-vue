 import { Injector } from "@farris/devkit-vue";
import { ComponentConfigResolver } from "./component-config-resolver";
import { COMPONENT_CONFIG_RESOLVER_TOKEN } from "../tokens";

export class ComponentConfigResolverRegistry {
    public resolvers: ComponentConfigResolver[];

    constructor(private injector: Injector) {
        this.resolvers = this.injector.get<ComponentConfigResolver[]>(COMPONENT_CONFIG_RESOLVER_TOKEN);
    }

    public getResolverByType(type: string){
        return this.resolvers.find((resolver: ComponentConfigResolver)=> resolver.type === type);
    }
}
