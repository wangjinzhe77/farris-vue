 import { FormMetadataService } from "../service";
import { ComponentConfigResolver } from "./component-config-resolver";
import { ConfigResolver } from "../config";

export class TabPageComponentConfigResolver extends ComponentConfigResolver {
    public type: string='tab-page';

    constructor(private formMetadataService: FormMetadataService, private configResolver: ConfigResolver) {
        super();
    }

    public resolve(metadata: Record<string, any>): Record<string, any> {
        const { toolbar } = metadata;
        if (!toolbar) {
            return metadata;
        }
        const { buttons } = toolbar;
        if (!buttons || !Array.isArray(buttons) || buttons.length < 1) {
            return metadata;
        }
        buttons.forEach((button: Record<string, any>) => {
            const { disabled, visible } = button;
            button.disabled = this.configResolver.resolve(disabled);
        });
        return metadata;
    }
}
