import { FormMetadataService } from "../service";
import { ComponentConfigResolver } from "./component-config-resolver";
import { ConfigResolver } from "../config";

export class TreeGridComponentConfigResolver extends ComponentConfigResolver {
    public type: string = 'tree-grid';

    constructor(private formMetadataService: FormMetadataService, private configResolver: ConfigResolver) {
        super();
    }

    public resolve(metadata: Record<string, any>): Record<string, any> {
        const { disabled, columns, editable } = metadata;
        metadata.editable = this.configResolver.resolve(editable);
        metadata.disabled = this.configResolver.resolve(disabled);
        if (!columns || !Array.isArray(columns) || columns.length < 1) {
            return metadata;
        }
        columns.forEach((column: Record<string, any>) => {
            const { editor } = column;
            if (!editor) {
                return;
            }
            const { readonly, required } = editor;
            editor.readonly = this.configResolver.resolve(readonly);
            editor.required = this.configResolver.resolve(required);
        });
        return metadata;
    }
}
