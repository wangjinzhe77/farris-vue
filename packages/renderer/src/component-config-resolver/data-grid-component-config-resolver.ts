import { FormMetadataService } from "../service";
import { ComponentConfigResolver } from "./component-config-resolver";
import { ConfigResolver } from "../config";

export class DataGridComponentConfigResolver extends ComponentConfigResolver {
    public type: string = 'data-grid';

    constructor(private formMetadataService: FormMetadataService, private configResolver: ConfigResolver) {
        super();
    }

    public resolve(metadata: Record<string, any>): Record<string, any> {
        const { disabled, columns, editable, pagination } = metadata;
        metadata.editable = this.configResolver.resolve(editable);
        metadata.disabled = this.configResolver.resolve(disabled);
        if (pagination) {
            const { disabled = false } = pagination;
            pagination.disabled = this.configResolver.resolve(disabled);
        }
        if (!columns || !Array.isArray(columns) || columns.length < 1) {
            return metadata;
        }
        columns.forEach((column: Record<string, any>) => {
            const { editor, columnTemplate } = column;
            // if (columnTemplate) {
            //     const compiledTemplate = compile(columnTemplate);
            //     const viewModel = {
            //         stateMachine: this.viewModel.state.stateMachine,
            //         uiState: this.viewModel.state.uiState,
            //         bindingData: this.viewModel.state.entityState?.currentEntity,
            //         root: this.viewModel.getRoot(),
            //         parent: this.viewModel.getParent()
            //     };
            //     const columnTemplateFactory = (context: Record<string, any> = {}) => {
            //         const props = Object.keys(context) || [];
            //         props.push('viewModel');
            //         return createVNode({ render: compiledTemplate, props }, { viewModel, ...context });
            //     };
            //     column.colTemplate = columnTemplateFactory;
            // }
            if (!editor) {
                return;
            }
            const { readonly, required, disabled } = editor;
            editor.readonly = this.configResolver.resolve(readonly);
            editor.required = this.configResolver.resolve(required);
            editor.disabled = this.configResolver.resolve(disabled);
        });
        return metadata;
    }
}
