import { Module } from "@farris/devkit-vue";
import { CallbackHandler } from "./callback-handler";
import { get } from 'lodash-es';
import { FormMetadataService } from "../service";
import { EntityResolver, FieldResolver } from "../resolvers";
import { ENTITY_STORE_SUFFIX } from "../types";

export class BeforeEditCallCallbackHandler extends CallbackHandler {
    public callbackType: string | null = 'beforeEditCell';
    constructor(
        private module: Module,
        private formMetadataService: FormMetadataService,
        private entityResolver: EntityResolver,
        private fieldResolver: FieldResolver,
    ) {
        super();
    }
    public handle(callbackType: string, args: any[]): undefined | boolean | Promise<boolean> {
        const { column, rawData } = args.at(0);
        const { editor, binding } = column || {};
        if (!editor || !binding) {
            return true;
        }
        const { type, mappingFields, dataSource } = editor;
        if (type !== 'lookup' || !dataSource) {
            return true;
        }
        const { idField } = dataSource;
        const mappingFieldsObject = typeof mappingFields === 'string' ? JSON.parse(mappingFields) : mappingFields;
        if (!mappingFieldsObject || Object.keys(mappingFieldsObject).length < 1) {
            return true;
        }
        const valueField = get(mappingFieldsObject, idField);
        const entityStoreId = this.formMetadataService.getModuleCode() + ENTITY_STORE_SUFFIX;
        const entityStore = this.module.getEntityStore(entityStoreId);
        if (!entityStore) {
            return true;
        }
        const { field } = binding;
        if (!field) {
            return true;
        }
        const { dataSource: entitySource } = this.fieldResolver.resolve(field) || {};
        if (!entitySource) {
            return true;
        }
        const { bindingPaths = [], primaryKey = 'id' } = this.entityResolver.resolve(entitySource) || {};
        const entityListPath = `/${bindingPaths.join('/')}`;
        const entityList = entityStore?.getEntityListByPath(entityListPath);
        const primaryValue = rawData[primaryKey];
        const entity = entityList?.getEntityById(primaryValue);
        if (!entity) {
            return true;
        }
        editor.idValue = get(entity, valueField);
        return true;
    }
}
