import { Injector, Module, StaticProvider } from "@farris/devkit-vue";
import { CALLBACK_HANDLER_TOKEN } from "../tokens";
import { BeforeEditCallCallbackHandler } from "./before-edit-cell-callback-handler";
import { CallbackHandlerRegistry } from "./callback-handler-registry";
import { FormMetadataService } from "../service";
import { EntityResolver, FieldResolver } from "../resolvers";


export const callbackHandlerProviders: StaticProvider[] = [
    { provide: CALLBACK_HANDLER_TOKEN, useClass: BeforeEditCallCallbackHandler, deps: [Module, FormMetadataService, EntityResolver, FieldResolver], multi: true },
    { provide: CallbackHandlerRegistry, useClass: CallbackHandlerRegistry, deps: [Injector] }
];
