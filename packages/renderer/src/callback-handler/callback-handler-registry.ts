import { Injector } from "@farris/devkit-vue";
import { CallbackHandler } from "./callback-handler";
import { CALLBACK_HANDLER_TOKEN } from "../tokens";

export class CallbackHandlerRegistry {
    public handlers: CallbackHandler[];
    constructor(private injector: Injector) {
        this.handlers = injector.get<CallbackHandler[]>(CALLBACK_HANDLER_TOKEN);
    }
    public getHandler(callbackType: string): CallbackHandler | null {
        return this.handlers.find((handler: CallbackHandler) => handler.callbackType === callbackType) || null;
    }
}
