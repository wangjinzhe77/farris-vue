export abstract class CallbackHandler {
    public callbackType: string | null = null;
    public abstract handle(type: string, args: any[]): undefined | boolean | Promise<boolean>;
}
