import { createApp } from 'vue';
import FarrisVue from '@farris/ui-vue';
import App from './app.vue';
import { createDevkit } from '@farris/devkit-vue';
import router from './router';

const devkit = createDevkit({
    providers: []
});

const app = createApp(App);
app.use(router);
app.use(devkit);
app.use(FarrisVue);
app.mount('#app');
