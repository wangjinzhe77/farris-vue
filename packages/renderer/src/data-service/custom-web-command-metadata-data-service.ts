import { MetadataRepository } from "../repository";
import { Metadata } from "../types";

export class CustomWebCommandMetadataDataService {
    constructor(private metadataRepository: MetadataRepository) { }

    public loadByType(metadataPath: string) {
        return this.metadataRepository.loadMetadatasByType(metadataPath, ".webcmd");
    }
    public loadByFullPath(metadataPath: string, metadataId: string): Promise<Metadata> {
        return this.metadataRepository.relied(metadataId, metadataPath).then((response: any) => {
            return this.createMetadata(response);
        });
    }

    public loadByProjectPath(projectPath: string, metadataId: string) {
        return this.metadataRepository.relied(metadataId, projectPath).then((response: any) => {
            return this.createMetadata(response);
        });
    }

    public loadById(metadataId: string) {
        return this.metadataRepository.retrieve(metadataId).then((response: any) => {
            return this.createMetadata(response);
        });
    }

    private createMetadata(response: any) {
        const metadata = {
            id: response.id,
            content: JSON.parse(response.content),
            refs: JSON.parse(response.refs)
        };
        return metadata;
    }
}
