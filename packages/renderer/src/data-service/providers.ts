import { StaticProvider } from "@farris/devkit-vue";
import { FormMetadataDataService } from "./form-metadata-data-service";
import { MetadataRepository } from "../repository";
import { StateMachineMetadataDataService } from "./state-machine-metadata-data-service";
import { CommonWebCommandMetadataDataService } from "./common-web-command-metadata-data-service";
import { ResourceMetadataDataService } from "./resource-metadata-data-service";
import { MetadataDataService } from "./metadata-data-service";
import { CustomWebComponentMetadataDataService } from "./custom-web-component-metadata-data-service";
import { CustomWebCommandMetadataDataService } from "./custom-web-command-metadata-data-service";

export const metadataDataServiceProviders: StaticProvider[] = [
    { provide: FormMetadataDataService, useClass: FormMetadataDataService, deps: [MetadataRepository] },
    { provide: StateMachineMetadataDataService, useClass: StateMachineMetadataDataService, deps: [MetadataRepository] },
    { provide: CommonWebCommandMetadataDataService, useClass: CommonWebCommandMetadataDataService, deps: [MetadataRepository] },
    { provide: ResourceMetadataDataService, useClass: ResourceMetadataDataService, deps: [MetadataRepository] },
    { provide: CustomWebComponentMetadataDataService, useClass: CustomWebComponentMetadataDataService, deps: [MetadataRepository] },
    { provide: CustomWebCommandMetadataDataService, useClass: CustomWebCommandMetadataDataService, deps: [MetadataRepository] },
    { provide: MetadataDataService, useClass: MetadataDataService, deps: [FormMetadataDataService, StateMachineMetadataDataService, CommonWebCommandMetadataDataService, CustomWebComponentMetadataDataService, CustomWebCommandMetadataDataService] }
];
