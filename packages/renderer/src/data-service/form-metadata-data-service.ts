 
import { MetadataRepository } from "../repository";
import { Metadata } from "../types";

export class FormMetadataDataService {
    constructor(private repository: MetadataRepository) { }

    public loadByFullPath(metadataFullPath: string): Promise<Metadata> {
        return this.repository.load(metadataFullPath).then((response: any) => {
            return this.createMetadata(response);
        });
    }

    public loadById(metadataId: string) {
        return this.repository.retrieve(metadataId).then((response: any) => {
            return this.createMetadata(response);
        });
    }

    private createMetadata(response: any) {
        const { Contents: content } = JSON.parse(response.content);
        const refs = JSON.parse(response.refs);
        const metadata = { id: response.id, content, refs };
        return metadata;
    }
}
