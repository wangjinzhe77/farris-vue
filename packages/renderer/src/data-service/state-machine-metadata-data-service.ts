 
import { MetadataRepository } from "../repository";

export class StateMachineMetadataDataService {
    constructor(private metadataRepository: MetadataRepository) { }

    public loadByFullPath(metadataFullPath: string): Promise<any> {
        return this.metadataRepository.load(metadataFullPath).then((response: any) => {
            return this.createMetadata(response);
        });
    }

    public loadByProjectPath(projectPath: string, metadataId: string) {
        return this.metadataRepository.relied(metadataId, projectPath).then((response: any) => {
            return this.createMetadata(response);
        });
    }

    public loadById(metadataId: string) {
        return this.metadataRepository.retrieve(metadataId).then((response: any) => {
            return this.createMetadata(response);
        });
    }

    private createMetadata(response: any) {
        const metadata = {
            content: JSON.parse(response.content),
            refs: JSON.parse(response.refs),
            id: response.id
        };
        return metadata;
    }
}
