import { CommonWebCommandMetadataDataService } from "./common-web-command-metadata-data-service";
import { CustomWebComponentMetadataDataService } from "./custom-web-component-metadata-data-service";
import { FormMetadataDataService } from './form-metadata-data-service';
import { StateMachineMetadataDataService } from "./state-machine-metadata-data-service";
import { CustomWebCommandMetadataDataService } from "./custom-web-command-metadata-data-service";
import { FormMetadata, Metadata, StateMachineRefMetadata, WebCommandRefMetadata } from "../types";

export class MetadataDataService {
    constructor(
        private formMetadataDataService: FormMetadataDataService,
        private stateMachineMetadataDataService: StateMachineMetadataDataService,
        private webCommandMetadataDataService: CommonWebCommandMetadataDataService,
        private customWebComponentMetadataDataService: CustomWebComponentMetadataDataService,
        private customWebCommandMetadataDataService: CustomWebCommandMetadataDataService
    ) { }

    public loadMetadataByPath(projectPath: string, metadataPath: string) {
        return this.formMetadataDataService.loadByFullPath(metadataPath).then((formMetadata: Metadata) => {
            const schema = formMetadata.content as FormMetadata;
            const { stateMachines = [] } = schema.module;
            // const stateMachineRefSchema = stateMachines;
            const stateMachineMetadataPromise = stateMachines && stateMachines.length > 0 ? stateMachines.map((stateMachine: StateMachineRefMetadata) => {
                return this.stateMachineMetadataDataService.loadByProjectPath(projectPath, stateMachine.uri);
            }) : [Promise.resolve(null)];
            // const stateMachineMetadataPromise = stateMachines ? this.stateMachineMetadataDataService.loadByProjectPath(projectPath, stateMachineRefSchema.uri) : Promise.resolve(null);
            const commandsMetadataPromise = schema.module.webcmds.map((webCommand: WebCommandRefMetadata) => {
                const { id } = webCommand;
                return this.webCommandMetadataDataService.loadByProjectPath(projectPath, id);
            });
            const customWebComponentMetadataPromise = this.customWebComponentMetadataDataService.loadByType(projectPath);
            // 资源元数据, 暂不加载
            // const resourceMetadatas = formMetadata.refs && formMetadata.refs.filter((ref: any) => {
            //     return ref.DependentMetadata.Type === "ResourceMetadata";
            // }).map((ref: any) => ref.DependentMetadata.ID) || [];
            // const resourceMetadataPromise = resourceMetadatas.map((id: string) => {
            //     return useResourceMetadata(id, useCache);
            // });
            // , Promise.all(resourceMetadataPromise)
            const promises = [Promise.all(stateMachineMetadataPromise), Promise.all(commandsMetadataPromise), customWebComponentMetadataPromise];
            return Promise.all(promises).then(([stateMachineSchema, commandSchemas, customWebComponents]) => {
                return {
                    form: formMetadata,
                    stateMachines: stateMachineSchema as Metadata[],
                    commands: commandSchemas as Metadata[],
                    webComponents: customWebComponents as Metadata[]
                };
            });
        });
    }

    public loadMetadataById(metadataId: string) {
        return this.formMetadataDataService.loadById(metadataId).then((formMetadata: Metadata) => {
            const schema = formMetadata.content as FormMetadata;
            const { stateMachines } = schema.module;
            // const stateMachineRefSchema = stateMachines[0];
            const stateMachineMetadataPromise = stateMachines && stateMachines.length > 0 ? stateMachines.map((stateMachine: StateMachineRefMetadata) => {
                return this.stateMachineMetadataDataService.loadById(stateMachine.uri);
            }) : [Promise.resolve(null)];
            // const stateMachineMetadataPromise = stateMachines ? this.stateMachineMetadataDataService.loadById(stateMachineRefSchema.uri) : Promise.resolve(null);

            const commandsMetadataPromise = schema.module.webcmds.map((webCommand: WebCommandRefMetadata) => {
                const { id } = webCommand;
                return this.webCommandMetadataDataService.loadById(id);
            });
            const promises = [Promise.all(stateMachineMetadataPromise), Promise.all(commandsMetadataPromise)];
            return Promise.all(promises).then(([stateMachineSchema, commandSchemas]) => {
                const customWebComponents: string[] = [];
                if (commandSchemas && commandSchemas.length > 0) {
                    commandSchemas.forEach((commandSchema: Metadata | null) => {
                        if (commandSchema && commandSchema.content && commandSchema.content.Extends && commandSchema.content.Extends.IsCommon === false) {
                            const commands = commandSchema.content.Commands as any[];
                            commands.forEach((command: any) => {
                                const items = command.Items as any[];
                                items.forEach((item: any) => {
                                    if (!item.ComponentPath.startsWith('Gsp/')) {
                                        const componentId = item.ComponentId;
                                        if (!customWebComponents.includes(componentId)) {
                                            customWebComponents.push(componentId);
                                        }
                                    }
                                });
                            });
                        }
                    });
                }
                const customWebComponentMetadataPromise = customWebComponents.length > 0 ? Promise.all(customWebComponents.map((id: string) => {
                    return this.customWebComponentMetadataDataService.loadById(id);
                })) : Promise.resolve(null);
                return customWebComponentMetadataPromise.then((customWebComponents: Metadata[] | null) => {
                    return {
                        form: formMetadata,
                        stateMachines: stateMachineSchema as Metadata[],
                        commands: commandSchemas as Metadata[],
                        webComponents: customWebComponents as Metadata[]
                    };
                });
            });
        });
    }
}
