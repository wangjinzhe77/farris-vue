import { HttpClient, HttpParams, HttpRequestConfig } from "@farris/devkit-vue";

export class MetadataRepository {
    constructor(private httpClient: HttpClient) {
    }

    /**
     * 加载设计时元数据
     * @param metadataPath 元数据工程路径
     * @returns Promise<any>
     */
    public load(metadataPath: string): Promise<any> {
        const httpParams: HttpParams = {
            metadataFullPath: metadataPath
        };
        return this.httpClient.get(`/api/dev/main/v1.0/metadatas/load`, { params: httpParams });
    }

    /**
     * 根据元数据类型获取表单定义的其他元数据
     * @param metadataPath 元数据工程路径
     * @param metadataType 元数据类型
     * @returns Promise<any>
     */
    public loadMetadatasByType(metadataPath: string, metadataType: string): Promise<any> {
        const params: HttpParams = {
            path: metadataPath,
            metadataTypeList: metadataType
        };
        const requestConfig: HttpRequestConfig = { params, headers: { accept: 'application/json' } };
        return this.httpClient.get('/api/dev/main/v1.0/mdservice', requestConfig);
    }

    /**
     * 获取表单依赖的元数据
     * @param metadataId 元数据id
     * @param metadataPath 元数据工程路径
     * @returns Promise<any>
     */
    public relied(metadataId: string, metadataPath: string): Promise<any> {
        const params: HttpParams = {
            metadataPath,
            metadataID: metadataId
        };
        const requestConfig: HttpRequestConfig = { params };
        return this.httpClient.get('/api/dev/main/v1.0/metadatas/relied', requestConfig);
    }

    /**
     * 检索元数据
     * @param metadataId 元数据id
     * @returns
     * @description 用于获取运行时元数据
     */
    public retrieve(metadataId: string): Promise<any> {
        return this.httpClient.get(`/api/runtime/lcm/v1.0/rt-metadatas/${metadataId}`, {});
    }
}
