import { HttpClient, StaticProvider } from "@farris/devkit-vue";
import { MetadataRepository } from "./metadata-repository";

export const repositoryProviders: StaticProvider[] = [
    { provide: MetadataRepository, useClass: MetadataRepository, deps: [HttpClient] }
];
