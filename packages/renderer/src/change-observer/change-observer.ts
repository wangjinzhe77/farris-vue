import { Callable } from "../common";

export abstract class ChangeObserver {
    public abstract observe(callback: Callable): void;
}
