import { Injector, Module, StaticProvider } from "@farris/devkit-vue";
import { StateMachineObserver } from "./state-machine-observer";
import { CHANGE_OBSERVER_TOKEN } from "../tokens";
import { ChangeObserverRegistry } from "./change-observer-registry";
import { EntityStateChangeObserver } from "./entity-state-change-observer";
import { UIStateChangeObserver } from "./ui-state-change-observer";

export const changeObserverProviders: StaticProvider[] = [
    { provide: CHANGE_OBSERVER_TOKEN, useClass: StateMachineObserver, deps: [Module], multi: true },
    { provide: CHANGE_OBSERVER_TOKEN, useClass: EntityStateChangeObserver, deps: [Module, Injector], multi: true },
    { provide: CHANGE_OBSERVER_TOKEN, useClass: UIStateChangeObserver, deps: [Module], multi: true },
    { provide: ChangeObserverRegistry, useClass: ChangeObserverRegistry, deps: [Injector] }
];
