import { Module, StateChange, UIState, UIStore } from "@farris/devkit-vue";
import { Callable } from "../common";
import { ChangeObserver } from "./change-observer";
import { Change, ChangeSource } from "./types";

export class UIStateChangeObserver extends ChangeObserver {

    constructor(private module: Module) {
        super();
    }

    public observe(callback: Callable): void {
        if (!this.module) {
            return;
        }
        const uiStateStores = this.module.getUIStores();
        if (!uiStateStores || uiStateStores.length < 1) {
            return;
        }
        uiStateStores.forEach((uiStore: UIStore<UIState>) => {
            uiStore.watchChange((change: StateChange<UIState>) => {
                const stateMachineChange: Change<StateChange<UIState>> = {
                    source: ChangeSource.UIState,
                    detail: change
                };
                callback(stateMachineChange);
            });
        });

    }
}
