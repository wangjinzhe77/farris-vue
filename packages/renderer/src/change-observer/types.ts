import { StateChange } from "@farris/devkit-vue";

export enum ChangeSource {
    StateMachine = 'StateMachine',
    UIState = 'UIState',
    EntityState = 'EntityState'
}

export interface Change<T> {
    source: ChangeSource;
    detail: StateChange<T>;
}
