import { Injector } from "@farris/devkit-vue";
import { ChangeObserver } from "./change-observer";
import { CHANGE_OBSERVER_TOKEN } from "../tokens";

export class ChangeObserverRegistry {
    public observers: ChangeObserver[];

    constructor(private injector: Injector) {
        this.observers = this.injector.get<ChangeObserver[]>(CHANGE_OBSERVER_TOKEN);
    }
}
