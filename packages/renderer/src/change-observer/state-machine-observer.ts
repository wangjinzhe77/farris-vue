 import { Module, StateChange, StateMachine, StateMachineState, ViewModel, ViewModelState } from "@farris/devkit-vue";
import { ChangeObserver } from "./change-observer";
import { Change, ChangeSource } from "./types";
import { Callable } from "../common";

export class StateMachineObserver extends ChangeObserver {

    constructor(private module: Module) {
        super();
    }

    public observe(callback: Callable) {
        if (!this.module) {
            return;
        }
        const stateMachine = this.module.getStateMachines()[0];
        if(!stateMachine){
            return;
        }
        stateMachine.watchChange((change: StateChange<StateMachineState>) => {
            const stateMachineChange: Change<StateMachineState> = {
                source: ChangeSource.StateMachine,
                detail: change
            };
            callback(stateMachineChange);
        });
    }
}
