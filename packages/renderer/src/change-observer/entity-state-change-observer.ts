import { Entity, EntityState, Injector, Module, StateChange } from "@farris/devkit-vue";
import { Callable } from "../common";
import { ChangeObserver } from "./change-observer";
import { Change, ChangeSource } from "./types";
import { MODULE_CONFIG_ID_TOKEN } from "../tokens";
import { ENTITY_STORE_SUFFIX } from "../types";

export class EntityStateChangeObserver extends ChangeObserver {
    constructor(private module: Module, private injector: Injector) {
        super();
    }
    public observe(callback: Callable): void {
        if (!this.module) {
            return;
        }
        const moduleConfigId = this.injector.get<string>(MODULE_CONFIG_ID_TOKEN);
        const entityStore = this.module.getEntityStore(`${moduleConfigId}${ENTITY_STORE_SUFFIX}`);
        if (!entityStore) {
            return;
        }
        entityStore.watchChange((change: StateChange<EntityState<Entity>>) => {
            const stateMachineChange: Change<EntityState<Entity>> = {
                source: ChangeSource.EntityState,
                detail: change
            };
            callback(stateMachineChange);
        });
    }
}
