import { StaticProvider, Module } from "@farris/devkit-vue";
import { ComponentConfigDependencyRegistry } from "./component-config-dependency-registry";
import { ConfigResolver } from './config-resolver';

export const configProviders: StaticProvider[] = [
    { provide: ComponentConfigDependencyRegistry, useClass: ComponentConfigDependencyRegistry, deps: [] },
    { provide: ConfigResolver, useClass: ConfigResolver, deps: [Module] }
];
