import { Config } from "./types";

export class ComponentConfigDependencyRegistry {
    private registry: Map<string, Config[]>;

    constructor() {
        this.registry = new Map<string, Config[]>();
    }

    public setConfigs(componentId: string, configs: Config[]) {
        this.registry.set(componentId, configs);
    }

    public getComponents() {
        return this.registry;
    }
}
