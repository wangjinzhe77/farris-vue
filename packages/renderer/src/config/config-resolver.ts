 
import { Module, ViewModel, ViewModelState } from "@farris/devkit-vue";

export class ConfigResolver {

    constructor(private module: Module, private viewModel: ViewModel<ViewModelState>) {
    }

    public resolve(config: string | boolean) {
        if (typeof config === 'boolean') {
            return config;
        }
        // TODO: 应获取指定的viewModel
        const viewModels = this.module.getViewModels();
        const currentViewModel = viewModels[0];
        const context = {
            stateMachine: currentViewModel.state.stateMachine,
            uiState: currentViewModel.state.uiState,
            entityState: currentViewModel.state.entityState?.currentEntity
        };
        return new Function('viewModel', `return ${config}`)(context);
    }
}
