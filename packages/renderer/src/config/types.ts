export enum DependencyType {
    StateMachine = 'StateMachine',
    EntityState = 'EntityState',
    UIState = 'UIState',
    Static = 'Static',
    Empty = 'Empty'
}
export interface ConfigDependency {
    type: DependencyType;
    path?: string;
}

export interface Config {
    config: string | boolean;
    deps: ConfigDependency[];
    path: string;
}
