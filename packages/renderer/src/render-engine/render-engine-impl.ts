import { Injector, RenderEngine } from "@farris/devkit-vue";
import { Ref } from "vue";
import { RENDER_TOKEN } from "../tokens";

export class RenderEngineImpl implements RenderEngine {
    private renderer: Ref;

    constructor(private injector: Injector) {
        const renderRef = this.injector.get<Ref>(RENDER_TOKEN);
        this.renderer = renderRef;
    }

    public render(componentId: string, type: string,schema: Record<string, any>) {
        const props = this.renderer.value.convertPartialSchemaToProps(type,schema);
        this.renderer.value.setProps(componentId, props);
    }

    public rerender(componentId: string) {
        this.renderer.value.rerender(componentId);
    }

    public getComponentById(componentId: string) {
        return this.renderer.value.componentManager.get(componentId);
    }

    public setProps(componentId: string, props: Record<string, any>) {
        return this.renderer.value.setProps(componentId, props);
    }

    public getProps(componentId: string) {
        return this.renderer.value.getProps(componentId);
    }

    public invokeMethod(componentId: string, methodName: string, ...args: any[]) {
        this.renderer.value.invoke(componentId, methodName, ...args);
    }

    public getSchema(componentId: string) {
        return this.renderer.value.getSchema(componentId);
    }

    public setSchema(componentId: string, schema: Record<string, any>) {
        this.renderer.value.setSchema(componentId, schema);
    }
}
