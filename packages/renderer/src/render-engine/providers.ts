import { Injector, StaticProvider } from "@farris/devkit-vue";
import { RenderEngineImpl } from "./render-engine-impl";

export const renderEngineProviders: StaticProvider[] = [
    { provide: RenderEngineImpl, useClass: RenderEngineImpl, deps: [Injector] }
];
