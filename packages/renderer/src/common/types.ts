export type Callable<T = any, U extends any[] = any[]> = (...args: U) => T;
