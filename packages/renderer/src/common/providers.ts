import { StaticProvider } from "@farris/devkit-vue";
import { EventEmitter } from "./event-emitter";

export const eventEmitterProviders: StaticProvider[] = [
    { provide: EventEmitter, useClass: EventEmitter, deps: [] }
];
