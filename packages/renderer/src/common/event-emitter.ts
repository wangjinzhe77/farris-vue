import { Reactive, reactive, toRefs } from 'vue';
import { Callable } from './types';

export class EventEmitter {
    private events: Reactive<Record<string, Callable[]>>;

    constructor() {
        this.events = reactive({});
    }

    on(event: string, callback: Callable) {
        if (!this.events[event]) {
            this.events[event] = [];
        }
        this.events[event].push(callback);
    }

    off(event: string, callback?: Callable) {
        if (!this.events[event]) {
            return;
        }
        const callbacks = this.events[event];
        if (callback) {
            const index = callbacks.indexOf(callback);
            if (index !== -1) {
                callbacks.splice(index, 1);
            }
        } else {
            callbacks.length = 0;
        }
    }

    emit(event: string, args: any[]) {
        if (!this.events[event]) {return;}
        this.events[event].forEach((callback) => {
            callback(args);
        });
    }
}
