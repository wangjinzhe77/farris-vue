import { createRouter, createWebHashHistory } from 'vue-router';
import PreviewView from './preview.vue';
import DebugView from './debug.vue';
import RendererView from './renderer.vue';

const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/preview',
            name: 'preview',
            component: PreviewView
        },
        {
            path: '/debug',
            name: 'debug',
            component: DebugView
        },
        {
            path: '/',
            name: 'renderer',
            component: RendererView
        }
    ]
});

export default router;
