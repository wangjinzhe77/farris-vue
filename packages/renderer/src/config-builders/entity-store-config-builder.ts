import {
    FieldConfig, PrimitiveFieldConfig, EntityFieldConfig, EntityListFieldConfig, EntityConfig,
    EntityStateConfig, EntityStoreConfig
} from '@farris/devkit-vue';
import { ENTITY_STORE_SUFFIX } from '../types';

/**
 * 实体仓库配置构造器
 */
class EntityStoreConfigBuilder {

    /**
     * 表单元数据
     */
    private formMeta: any;

    /**
     * 构造函数
     */
    constructor(formMeta: any) {
        this.formMeta = formMeta;
    }

    /**
     * 构造实体仓库配置
     */
    build(schemaNode: any) {
        const id = this.formMeta.module.code + ENTITY_STORE_SUFFIX;
        const parentEntitySchema = schemaNode.entities[0];
        const entityStateConfig = this.buildEntityStateConfig(parentEntitySchema);
        const entityStoreConfig: EntityStoreConfig = {
            id,
            state: entityStateConfig
        };

        return entityStoreConfig;
    }

    /**
     * 创建实体状态配置
     */
    private buildEntityStateConfig(entitySchemaNode: any): EntityStateConfig {
        const entityConfig = this.buildEntityConfig(entitySchemaNode);
        const entityStateConfig = {
            entity: entityConfig
        };

        return entityStateConfig;
    }

    /**
     * 构造实体配置
     */
    private buildEntityConfig(entitySchemaNode: any): EntityConfig {
        const idKey = entitySchemaNode.type.primary;
        const { fields, entities } = entitySchemaNode.type;
        const fieldConfigs = this.buildEntityFieldConfigs(fields, entities);
        const entityConfig = {
            idKey,
            fields: fieldConfigs
        };
        return entityConfig;
    }

    /**
     * 构造实体字段配置集合
     */
    private buildEntityFieldConfigs(fieldSchemaNodes: any[], childEntitySchemaNodes: any[]): FieldConfig[] {
        const fieldConfigs: FieldConfig[] = [];

        // 简单字段和关联字段
        fieldSchemaNodes = fieldSchemaNodes || [];
        fieldSchemaNodes.forEach((fieldSchemaNode: any) => {
            let fieldConfig: FieldConfig;
            if (fieldSchemaNode.$type === 'SimpleField') {
                fieldConfig = this.buildPrimitiveFieldConfig(fieldSchemaNode);
            } else {
                fieldConfig = this.buildEntityFieldConfig(fieldSchemaNode);
            }
            fieldConfigs.push(fieldConfig);
        });

        // 子实体字段
        childEntitySchemaNodes = childEntitySchemaNodes || [];
        childEntitySchemaNodes.forEach((childEntitySchemaNode: any) => {
            const fieldConfig = this.buildEntityListFieldConfig(childEntitySchemaNode);
            fieldConfigs.push(fieldConfig);
        });

        return fieldConfigs;
    }

    /**
     * 构造简单字段配置
     */
    private buildPrimitiveFieldConfig(fieldSchemaNode: any): PrimitiveFieldConfig {
        const fieldConfig: PrimitiveFieldConfig = {
            type: 'Primitive',
            name: fieldSchemaNode.label
        };

        return fieldConfig;
    }

    /**
     * 构造实体字段配置
     */
    private buildEntityFieldConfig(fieldSchemaNode: any): EntityFieldConfig {
        const name = fieldSchemaNode.label;

        const entityConfig = this.buildEntityConfig(fieldSchemaNode);
        const fieldConfig: EntityFieldConfig = {
            type: 'Entity',
            name,
            entityConfig
        };

        return fieldConfig;
    }

    /**
     * 构造实体列表字段配置
     */
    private buildEntityListFieldConfig(childEntitySchemaNode: any): EntityListFieldConfig {
        const type = 'EntityList';
        const name = childEntitySchemaNode.label;
        const childEntityConfig = this.buildEntityConfig(childEntitySchemaNode);
        const fieldSchema: EntityListFieldConfig = {
            type, name, childEntityConfig
        };

        return fieldSchema;
    }
}

export { EntityStoreConfigBuilder };
