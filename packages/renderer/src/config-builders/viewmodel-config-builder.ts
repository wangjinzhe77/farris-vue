import { CommandParamConfig, CommandConfig, ViewModelConfig, CommandHandlerConfig } from '@farris/devkit-vue';
import { CommandHandlerConfigBuilder } from './command-handler-builder';
import { ENTITY_STORE_SUFFIX } from '../types';

/**
 * 视图模型配置构造器
 */
class ViewModelConfigBuilder {

    /**
     * 表单元数据
     */
    private formMeta: any;

    /**
     * 表单元数据
     */
    private webCmdMetas: any[];

    private webComponentMetadatas: any[];

    /**
     * 视图模型注入配置
     */
    private viewModelProviders: any[];

    /**
     * 构造函数
     */
    constructor(formMeta: any, webCmdMetas: any[], webComponentMetadatas: any[],viewModelProviders: any[]) {
        this.formMeta = formMeta;
        this.webCmdMetas = webCmdMetas;
        this.webComponentMetadatas = webComponentMetadatas;
        this.viewModelProviders = viewModelProviders;
    }

    /**
     * 构造配置
     */
    public build(viewModelNode: any) {
        const componentNode = this.getComponentNode(viewModelNode.id);
        if (!componentNode) {
            return;
        }

        const moduleCode = this.formMeta.module.code;
        const commandConfigs = this.buildCommandConfigs(viewModelNode.commands);
        const commandHandlerConfigs = this.buildCommandHandlerConfigs(viewModelNode.commands);

        const parentViewModelId = viewModelNode.parent;
        let parentComponent = null;
        if (parentViewModelId) {
            parentComponent = this.getComponentNode(parentViewModelId);
        }
        this.getComponentNode(parentViewModelId);

        const viewModelConfig: ViewModelConfig = {
            id: componentNode.id,
            entityStore: moduleCode + ENTITY_STORE_SUFFIX,
            uiStore: viewModelNode.code + '-uistore',
            stateMachine: viewModelNode.stateMachine,
            repository: moduleCode + '-repository',
            providers: this.viewModelProviders,
            commands: commandConfigs,
            commandHandlers: commandHandlerConfigs,
            bindingPath: viewModelNode.bindTo,
            parentId: parentComponent ? parentComponent.id : null
        };

        return viewModelConfig;
    }

    /**
     * 构造命令配置
     */
    private buildCommandConfigs(commandNodes: any[]): CommandConfig[] {
        const commandConfigs: CommandConfig[] = [];
        commandNodes.forEach((commandNode) => {
            const paramConfigs = this.buildCommandParamConfigs(commandNode.params);
            const commandConfig: CommandConfig = {
                name: commandNode.code,
                params: paramConfigs
            };
            commandConfigs.push(commandConfig);
        });

        return commandConfigs;
    }

    /**
     * 构造命令参数配置
     */
    private buildCommandParamConfigs(paramNodes: any[]): CommandParamConfig[] {
        const paramsConfigs: CommandParamConfig[] = [];
        paramNodes.forEach((paramNode) => {
            const paramConfig: CommandParamConfig = {
                name: paramNode.name,
                value: paramNode.value
            };
            paramsConfigs.push(paramConfig);
        });

        return paramsConfigs;
    }

    /**
     * 构造命令处理器配置
     */
    private buildCommandHandlerConfigs(commandNodes: any[]): CommandHandlerConfig[] {
        const commandHandlerConfigBuilder = new CommandHandlerConfigBuilder(this.formMeta, this.webCmdMetas, this.webComponentMetadatas);
        const commandHandlerConfigs: CommandHandlerConfig[] = [];
        commandNodes.forEach((commandNode) => {
            // const webCmdId = commandNode.cmpId;
            // const commandName = commandNode.code;
            // const commandHandlerNode = this.getCommandHandlerNode(webCmdId, commandNode.handlerName);

            // const taskConfigs = this.buildTaskConfigs(commandHandlerNode.Items);
            // const commandHandlerConfig: CommandHandlerConfig = {
            //     commandName,
            //     tasks: taskConfigs
            // };
            // commandHandlerConfigs.push(commandHandlerConfig);
            const commandHandlerConfig = commandHandlerConfigBuilder.build(commandNode);
            commandHandlerConfigs.push(commandHandlerConfig);
        });

        return commandHandlerConfigs;
    }
    /**
     * 获取视图模型对应组件
     */
    private getComponentNode(viewModelId: string): any | undefined {
        const componentNodes = this.formMeta.module.components;
        const targetComponentNode = componentNodes.find((componentNode: any) => {
            return componentNode.viewModel === viewModelId;
        });

        return targetComponentNode;
    }

}

export { ViewModelConfigBuilder };
