import { PageStateConfig, TransitionActionConfig, RenderConditionConfig, RenderStateConfig, StateMachineConfig } from '@farris/devkit-vue';

/**
 * 状态机配置
 */
class StateMachineConfigBuilder {

    /**
     * 表单元数据
     */
    private formMeta: any;

    /**
     * 构造函数
     */
    constructor(formMeta: any) {
        this.formMeta = formMeta;
    }

    /**
     * 构造配置
     */
    public build(smMeta: any): StateMachineConfig | undefined {
        if(!smMeta){
            return;
        }
        // 未被引用的不处理
        const viewModelNode = this.getViewModelNode(smMeta);
        const componentNode = this.getComponentNode(viewModelNode);
        if (!componentNode) {
            return;
        }

        const smMetaContent = smMeta.content;

        const statesNodes = smMetaContent.state;
        const pageStateConfigs = this.buildPageStateConfigs(statesNodes);

        const actionsObjNode = smMetaContent.action;
        const actionConfigs = this.buildActionConfigs(actionsObjNode);

        const renderStatesObjNode = smMetaContent.renderState;
        const renderStateConfigs = this.buildRenderStateConfigs(renderStatesObjNode, componentNode);

        const smConfig: StateMachineConfig = {
            id: viewModelNode.stateMachine,
            initPageState: smMetaContent.initialState,
            pageStates: pageStateConfigs,
            transitionActions: actionConfigs,
            state: {
                renderStates: renderStateConfigs
            }
        };

        return smConfig;
    }

    /**
     * 页面状态配置
     */
    private buildPageStateConfigs(stateNodes: any[]): PageStateConfig[] {
        if (!Array.isArray(stateNodes)) {
            return [];
        }

        const pageStateConfigs: PageStateConfig[] = stateNodes.map((stateNode) => {
            const pageStateconfig: PageStateConfig = {
                name: stateNode.state
            };
            return pageStateconfig;
        });

        return pageStateConfigs;
    }

    /**
     * 构造迁移动作
     */
    private buildActionConfigs(actionsObjNode: any): TransitionActionConfig[] {
        if (!actionsObjNode) {
            return [];
        }

        const actionConfigs = Object.keys(actionsObjNode).map((actionName: string) => {
            const actionNode = actionsObjNode[actionName];
            const actionConfig: TransitionActionConfig = {
                name: actionName,
                transitionTo: actionNode.transitTo
            };

            return actionConfig;
        });

        return actionConfigs;
    }

    /**
     * 构造可视化状态配置
     */
    private buildRenderStateConfigs(renderStatesObjNode: any, componentNode: any) {
        if (!renderStatesObjNode) {
            return [];
        }

        const renderStateConfigs = Object.keys(renderStatesObjNode).map((stateName: string) => {
            const renderStateNode = renderStatesObjNode[stateName];
            const conditions = this.buildRenderConditionConfigs(renderStateNode.condition, componentNode);
            const renderStateConfig: RenderStateConfig = {
                name: stateName,
                conditions
            };

            return renderStateConfig;
        });

        return renderStateConfigs;
    }

    /**
     * 构造渲染条件配置
     */
    private buildRenderConditionConfigs(conditionNodes: any[], componentNode: any): RenderConditionConfig[] {
        if (!Array.isArray(conditionNodes)) {
            return [];
        }

        const conditionConfigs: RenderConditionConfig[] = conditionNodes.map((conditionNode) => {
            const conditionConfig: RenderConditionConfig = { ...conditionNode };
            conditionConfig.source = this.getSource(conditionNode.source, componentNode);
            conditionConfig.target = this.getTarget(conditionNode.target);

            return conditionConfig;
        });

        return conditionConfigs;
    }

    /**
     * 获取源值
     */
    private getSource(source: string, componentNode: any): string {
        const sourceLen = source.length;
        if (source.startsWith('getData') === true) {
            const startIndex = 9;
            const endIndex = sourceLen - 2;
            return source.slice(startIndex, endIndex);
        }

        if (source.startsWith('getUIState') === true) {
            const startIndex = 12;
            const endIndex = sourceLen - 2;
            return source.slice(startIndex, endIndex);
        }

        if (source.startsWith('state') === true) {
            return `{STATEMACHINE~/#{${componentNode.id}}/currentPageState}`;
        }

        return source;
    }

    /**
     * 获取目标值
     */
    private getTarget(target: any) {
        if (target.startsWith("'") || target.startsWith('"')) {
            const startIndex = 1;
            const endIndex = target.length -1;
            return target.slice(startIndex, endIndex);
        }
        return JSON.parse(target);
    }

    /**
     * 获取状态机对应的视图模型节点
     */
    private getViewModelNode(smMeta: any): any | undefined {
        const smNodes = this.formMeta.module.stateMachines;
        const viewModelNodes = this.formMeta.module.viewmodels;

        const smMetaId = smMeta.id;
        const targetSmNode = smNodes.find((smNode: any) => {
            return smNode.uri === smMetaId;
        });

        if (!targetSmNode) {
            return;
        }

        const targetViewModelNode = viewModelNodes.find((viewModelNode: any) => {
            return viewModelNode.stateMachine === targetSmNode.id;
        });

        return targetViewModelNode;
    }

    /**
     * 获取状态机对应组件节点
     */
    private getComponentNode(viewModelNode: any): any | undefined {
        const componentNodes = this.formMeta.module.components;
        const targetComponentNode = componentNodes.find((componentNode: any) => {
            return componentNode.viewModel === viewModelNode.id;
        });

        return targetComponentNode;
    }
}

export { StateMachineConfigBuilder };
