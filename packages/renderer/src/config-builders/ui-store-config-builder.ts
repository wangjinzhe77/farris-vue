import { UIStatePropConfig, UIStateConfig, UIStoreConfig, UIState } from '@farris/devkit-vue';

/**
 * UI状态仓库配置构造器
 */
class UIStoreConfigBuilder {

    /**
     * 表单元数据
     */
    private formMeta: any;

    /**
     * 构造函数
     */
    constructor(formMeta: any) {
        this.formMeta = formMeta;
    }

    /**
     * 构造配置
     */
    public build(viewModelNode: any): UIStoreConfig {
        const stateConfig = this.getUIStateConfig(viewModelNode.states);
        const storeConfig: UIStoreConfig = {
            id: viewModelNode.code + '-uistore',
            state: stateConfig
        };

        return storeConfig;
    }

    /**
     * 获取状态配置
     */
    private getUIStateConfig(stateNodes: any[]): UIStateConfig {
        const propConfigs = this.getUIStatePropConfigs(stateNodes);
        const stateConfig: UIStateConfig = {
            props: propConfigs
        };

        return stateConfig;
    }

    /**
     * 获取属性设置
     */
    private getUIStatePropConfigs(stateNodes: any[]): UIStatePropConfig[] {
        const propConfigs: UIStatePropConfig[] = [];
        stateNodes.forEach((stateNode) => {
            const propConfig: UIStatePropConfig = {
                name: stateNode.code
            };
            propConfigs.push(propConfig);
        });

        return propConfigs;
    }
}

export { UIStoreConfigBuilder };
