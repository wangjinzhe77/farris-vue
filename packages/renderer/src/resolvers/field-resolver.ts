 
 
import { FormMetadataService } from "../service";
import { ResolvedEntityField } from "../types";

export class FieldResolver {
    constructor(private formMetadataService: FormMetadataService) { }

    public resolve(fieldId: string): ResolvedEntityField | null {
        const entity = this.formMetadataService.getEntity();
        const fieldInfo = this.resolveField(entity, fieldId);
        return fieldInfo;
    }

    private resolveField(entitySchema: Record<string, any>, fieldId: string): ResolvedEntityField | null {
        const { fields } = entitySchema.type;
        for (const field of fields) {
            if (field.id === fieldId) {
                return {
                    id: field.id,
                    bindingPath: field.bindingPath,
                    required: field.required,
                    readonly: field.readonly,
                    multiLanguage: field.multiLanguage,
                    label: field.label,
                    dataSource: entitySchema.label
                };
            }
            if (field.type && field.type.fields) {
                const result = this.resolveField({ type: field.type, label: entitySchema.label }, fieldId);
                if (result) {
                    return result;
                }
            }
        }
        const { entities } = entitySchema.type;
        if (!entities || entities.length < 1) {
            return null;
        }
        for (const entity of entities) {
            const result = this.resolveField(entity, fieldId);
            if (result) {
                return result;
            }
        }
        return null;
    }
}
