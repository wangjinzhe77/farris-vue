 
 
import { Injector, Module } from "@farris/devkit-vue";
import { FormMetadataService } from "../service";
import { EntityResolver } from "./entity-resolver";
import { FieldResolver } from "./field-resolver";
import { MODULE_CONFIG_ID_TOKEN } from "../tokens";
import { ENTITY_STORE_SUFFIX } from "../types";

export class ModelValueResolver {
    private modelValue: Record<string, any> = {};

    constructor(private entityResolver: EntityResolver, private fieldResolver: FieldResolver, private formMetadataService: FormMetadataService, private module: Module, private injector: Injector) { }

    public resolve() {
        this.modelValue = {};
        const components = this.formMetadataService.getComponents();
        if (!components || components.length < 1) {
            return this.modelValue;
        }
        components.forEach((component) => {
            this.resolveModelValue(component.schema);
        });
        return this.modelValue;
    }

    public resolveModelValue(viewSchema: Record<string, any>) {
        const { binding, dataSource } = viewSchema;
        const { id } = viewSchema;
        if (binding) {
            const value = this.resolveBindingValue(viewSchema);
            this.modelValue[id] = value;
        } else if (dataSource) {
            const value = this.resolveDataSourceValue(viewSchema);
            this.modelValue[id] = value;
        }
        if (!viewSchema.contents) {
            return;
        }
        if (typeof viewSchema.contents === 'string') {
            return;
        }
        viewSchema.contents.forEach((schema: Record<string, any>) => this.resolveModelValue(schema));
    }

    public resolveBindingValue(viewSchema: Record<string, any>) {
        const { binding } = viewSchema;
        const { field } = binding;
        const resolvedField = this.fieldResolver.resolve(field);
        if (!resolvedField) {
            console.warn(`Invalid field: ${field}`);
            return null;
        }
        const { dataSource, bindingPath } = resolvedField;
        const resolvedEntity = this.entityResolver.resolve(dataSource);
        if (!resolvedEntity) {
            console.warn(`Invalid entity dataSource: ${dataSource}`);
            return null;
        }
        const { bindingPaths } = resolvedEntity;
        const fieldBindingPaths = bindingPath.split('.');
        const fieldBindingPath = bindingPaths.concat(...fieldBindingPaths).join('/');
        let value = null;
        try {
            value = this.entityStore?.getValueByPath('/' + fieldBindingPath);
        }
        catch (e) {
            console.error(e);
        }
        return value;
    }

    public resolveDataSourceValue(viewSchema: Record<string, any>) {
        const { dataSource } = viewSchema;
        const resolvedEntity = this.entityResolver.resolve(dataSource);
        if (!resolvedEntity) {
            console.warn(`Invalid entity dataSource: ${dataSource}`);
            return null;
        }
        const { bindingPaths } = resolvedEntity;
        const entityList = this.entityStore?.getEntityListByPath('/' + bindingPaths.join('/'));
        const value: any[] = entityList?.toJSON() || [];
        return value;
    }

    private get entityStore() {
        const configId = this.injector.get<string>(MODULE_CONFIG_ID_TOKEN, undefined);
        return this.module.getEntityStore(`${configId}${ENTITY_STORE_SUFFIX}`);
    }
}
