 
 
import { FormMetadataService } from "../service";
import { EntityMetadata } from "../types";
import { ResolvedEntity } from "./types";

export class EntityResolver {
    constructor(private formMetadataService: FormMetadataService) { }

    public resolve(dataSource: string): ResolvedEntity | null {
        const entity = this.formMetadataService.getEntity();
        const entityInfo = this.resolveEntity(entity, dataSource);
        return entityInfo;
    }

    public resolveEntities(entitySchema: EntityMetadata): ResolvedEntity[] {
        const result: ResolvedEntity[] = [];
        function traverse(entity: EntityMetadata, path: string[]) {
            const currentPath = [...path, entity.label];
            const primary = entitySchema?.type?.primary || null;
            if (primary) {
                result.push({
                    bindingPaths: currentPath,
                    primaryKey: entity.type.primary
                });
            }
            entity.type.entities.forEach(childEntity => {
                traverse(childEntity, currentPath);
            });
        }
        const primary = entitySchema?.type?.primary || null;
        if (!primary) {
            return result;
        }
        result.push({
            bindingPaths: [],
            primaryKey: entitySchema.type.primary
        });
        const childEntities = entitySchema?.type?.entities || [];
        childEntities.forEach((entity) => {
            traverse(entity, []);
        });
        return result;
    }

    private resolveEntity(entity: Record<string, any>, entityLabel: string, isRoot: boolean = true): ResolvedEntity | null {
        const currentPath = isRoot ? [] : [entity.label];
        if (entity.label === entityLabel) {
            return {
                bindingPaths: currentPath,
                primaryKey: entity.type.primary
            };
        }
        for (const childEntity of entity.type.entities) {
            const result = this.resolveEntity(childEntity, entityLabel, false);
            if (result) {
                return {
                    bindingPaths: [...currentPath, ...result.bindingPaths],
                    primaryKey: result.primaryKey
                };
            }
        }
        return null;
    }
}
