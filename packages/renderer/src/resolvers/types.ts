export interface ResolvedEntity {
    bindingPaths: string[];
    primaryKey: string;
}
