 
import { Injector, Module, StaticProvider } from "@farris/devkit-vue";
import { EntityResolver } from "./entity-resolver";
import { FormMetadataService } from "../service";
import { FieldResolver } from "./field-resolver";
import { ModelValueResolver } from "./model-value-resolver";

export const resolverProviders: StaticProvider[] = [
    { provide: EntityResolver, useClass: EntityResolver, deps: [FormMetadataService] },
    { provide: FieldResolver, useClass: FieldResolver, deps: [FormMetadataService] },
    { provide: ModelValueResolver, useClass: ModelValueResolver, deps: [EntityResolver, FieldResolver, FormMetadataService, Module, Injector] }
];
