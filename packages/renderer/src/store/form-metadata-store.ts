 
import { ComponentConfigResolverFactory } from "../component-config-resolver";
import { ComponentMetadata, EntityMetadata, FormMetadata, MetadataType, ViewModelMetadata } from "../types";

export class FormMetadataStore {
    private metadataMap: Map<string, any> = new Map<string, any>();

    constructor(private metadata: FormMetadata) {
        const components = this.metadata?.module?.components || [];
        this.traverseComponent(components);
    }

    public getModuleId(): string {
        return this.metadata.module.id;
    }

    public getComponents(): any[] {
        return Array.from(this.metadataMap.values()).filter((item: any) => item.type === MetadataType.Component);
    }

    public getMetadataById(id: string): any {
        const content = this.metadataMap.get(id);
        return content ? content.schema : null;
    }

    public getEntity(): EntityMetadata {
        return this.metadata?.module?.entity[0]?.entities[0] || {} as any;
    }

    public getFrameComponent(): ComponentMetadata | null {
        const { components } = this.metadata?.module || {};
        if (!components || components.length < 1) {
            return null;
        }
        const frameComponent = components.find((component: ComponentMetadata) => component.componentType && component.componentType.toLowerCase() === 'frame');
        if (!frameComponent) {
            return null;
        }
        return frameComponent;
    }

    public getViewModels(): ViewModelMetadata[] {
        const { viewmodels } = this.metadata?.module || {};
        return viewmodels;
    }

    public getViewModelById(id: string) {
        const { viewmodels } = this.metadata?.module || {};
        return viewmodels && viewmodels.find((viewModel) => viewModel.id === id) || null;
    }

    public getMetadataByChange(change: any) {
    }

    public getRelatedComponent(metadataId: string) {
        let currentMetadata = this.metadataMap.get(metadataId);
        while (currentMetadata.type !== 'component' && currentMetadata.parentId) {
            const { parentId } = currentMetadata;
            currentMetadata = this.metadataMap.get(parentId);
        }
        return currentMetadata.schema;
    }

    // #region utility function
    public traverseComponent(components: any[], parentId?: string) {
        components.forEach((component) => {
            if (component?.id) {
                this.metadataMap.set(component.id, {
                    schema: component,
                    type: component.type,
                    parentId
                });
                const resolver = ComponentConfigResolverFactory.getResolver(component.type);
                const configs = resolver?.resolve(component);
                if (configs) {
                    // componentConfigs.set(component.id, configs);
                }
            }
            if (component.contents) {
                this.traverseComponent(component.contents, component.id);
            }
        });
    }
}
