import { httpProviders, StaticProvider } from "@farris/devkit-vue";
import { repositoryProviders } from "./repository";
import { metadataDataServiceProviders } from "./data-service";
import { serviceProviders } from "./service";

export const appProviders: StaticProvider[] = [
    ...httpProviders,
    ...repositoryProviders,
    ...metadataDataServiceProviders,
    ...serviceProviders
];
