 
import { Injector, StaticProvider } from "@farris/devkit-vue";
import { FormMetadataService } from "./form-metadata-service";
import { ComponentService } from "./component-service";
import { ComponentConfigDependencyRegistry } from "../config";
import { ComponentConfigDependencyResolveService } from "../component-config-dependency-resolver";

export const serviceProviders: StaticProvider[] = [
    { provide: FormMetadataService, useClass: FormMetadataService, deps: [Injector, ComponentConfigDependencyRegistry, ComponentConfigDependencyResolveService] },
    { provide: ComponentService, useClass: ComponentService, deps: [Injector] },
];
