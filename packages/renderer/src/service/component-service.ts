import { Injector } from "@farris/devkit-vue";
import { Ref } from "vue";
import { RENDER_TOKEN } from "../tokens";

export class ComponentService {
    private render: Ref | undefined;

    constructor(private injector: Injector) {
        this.render = this.injector.get<Ref>(RENDER_TOKEN);
    }

    public getComponentById(id: string) {
        return this.render && this.render.value.componentManager.get(id);
    }
}
