 
import { Injector } from "@farris/devkit-vue";
import { ComponentMetadata, EntityMetadata, FormMetadata, MetadataType, ViewModelMetadata } from "../types";
import { FORM_METADATA_TOKEN } from "../tokens";
import { ComponentConfigDependencyResolveService } from "../component-config-dependency-resolver";
import { ComponentConfigDependencyRegistry } from "../config";

export class FormMetadataService {
    private metadata: FormMetadata;

    private metadataMap: Map<string, any> = new Map<string, any>();

    constructor(private injector: Injector, private componentConfigDependencyRegistry: ComponentConfigDependencyRegistry, private componentConfigDependencyResolveService: ComponentConfigDependencyResolveService) {
        this.metadata = this.injector.get<FormMetadata>(FORM_METADATA_TOKEN);
        const components = this.metadata?.module?.components || [];
        this.traverseComponent(components);
    }

    public getModuleId(): string {
        return this.metadata.module.id;
    }

    public getModuleCode(): string{
        return this.metadata.module.code;
    }

    public getComponents(): { schema: Record<string, any>, type: string, parentId: string; }[] {
        return Array.from(this.metadataMap.values()).filter((item: any) => item.type === MetadataType.Component);
    }

    public getMetadataByType(type: string | MetadataType) {
        return Array.from(this.metadataMap.values()).filter((item: any) => item.type === type);
    }

    public getMetadataById(id: string): any {
        const content = this.metadataMap.get(id);
        return content ? content.schema : null;
    }

    public getEntity(): EntityMetadata {
        return this.metadata?.module?.entity[0]?.entities[0] || {} as any;
    }

    public getFrameComponent(): ComponentMetadata | null {
        const { components } = this.metadata?.module || {};
        if (!components || components.length < 1) {
            return null;
        }
        const frameComponent = components.find((component: ComponentMetadata) => component.componentType && component.componentType.toLowerCase() === 'frame');
        if (!frameComponent) {
            return null;
        }
        return frameComponent;
    }

    public getViewModels(): ViewModelMetadata[] {
        const { viewmodels } = this.metadata?.module || {};
        return viewmodels;
    }

    public getViewModelById(id: string) {
        const { viewmodels } = this.metadata?.module || {};
        return viewmodels && viewmodels.find((viewModel) => viewModel.id === id) || null;
    }

    public convertViewModelIdToComponentId(viewModelId: string) {
        const component = this.getComponentByViewModelId(viewModelId);
        return component ? component.id : null;
    }

    public getComponentByViewModelId(viewModelId: string) {
        const components = this.getComponents();
        const component = components.find((component: any) => component.schema.viewModel === viewModelId);
        return component ? component.schema : null;
    }

    public getRelatedComponent(metadataId: string) {
        let currentMetadata = this.metadataMap.get(metadataId);
        while (currentMetadata.type !== 'component' && currentMetadata.parentId) {
            const { parentId } = currentMetadata;
            currentMetadata = this.metadataMap.get(parentId);
        }
        return currentMetadata.schema;
    }

    private traverseComponent(components: any[], parentId?: string) {
        components.forEach((component) => {
            if (component?.id) {
                this.metadataMap.set(component.id, {
                    schema: component,
                    type: component.type,
                    parentId
                });
                const configs = this.componentConfigDependencyResolveService.resolve(component);
                if (configs) {
                    this.componentConfigDependencyRegistry.setConfigs(component.id, configs);
                }
            }
            if (component.contents) {
                this.traverseComponent(component.contents, component.id);
            }
        });
    }
}
