 import { StaticProvider } from "@farris/devkit-vue";
import { FormEngine } from "./form-engine";
import { ChangeObserverRegistry } from "../change-observer";
import { ChangeResolveService } from "../change-resolver";
import { ComponentConfigResolveService } from "../component-config-resolver";
import { RenderEngineImpl } from "../render-engine";
import { FormMetadataService } from "../service";

export const formEngineProviders: StaticProvider[] = [
    { provide: FormEngine, useClass: FormEngine, deps: [ChangeObserverRegistry, ChangeResolveService, ComponentConfigResolveService, RenderEngineImpl, FormMetadataService] }
];
