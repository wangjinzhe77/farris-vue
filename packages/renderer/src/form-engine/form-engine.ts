import { cloneDeep, merge, mergeWith } from "lodash-es";
import { Change, ChangeObserver, ChangeObserverRegistry } from "../change-observer";
import { ChangeResolveService } from "../change-resolver";
import { ComponentConfigResolveService } from "../component-config-resolver";
import { RenderEngineImpl } from "../render-engine";
import { FormMetadataService } from "../service";
import { Config } from "../config";

export class FormEngine {
    constructor(
        private changeObserverRegistry: ChangeObserverRegistry,
        private changeResolveService: ChangeResolveService,
        private componentConfigResolveService: ComponentConfigResolveService,
        private renderEngineImpl: RenderEngineImpl,
        private formMetadataService: FormMetadataService
    ) {
        this.changeObserverRegistry.observers.forEach((observer: ChangeObserver) => {
            observer.observe((change: Change<any>) => {
                const results: Map<string, Config[]> = this.changeResolveService.resolve(change);
                if (results && results.size > 0) {
                    results.forEach((configs: Config[], componentId: string) => {
                        const metadata = this.formMetadataService.getMetadataById(componentId);
                        if (!metadata) {
                            return;
                        }
                        const schema = cloneDeep(metadata);
                        const resolvedSchema = this.componentConfigResolveService.resolve(schema);
                        // 过滤schema属性，仅解析发生变化的属性
                        const changedSchema = {};
                        configs.forEach((config: Config) => {
                            const filteredSchema = this.filterSchema(resolvedSchema, config);
                            mergeWith(changedSchema, filteredSchema, this.mergeArray);
                        });
                        this.renderEngineImpl.render(componentId, resolvedSchema.type, changedSchema);
                    });
                }
            });
        });
    }

    public resolve(components: any[]) {
        components.forEach((component) => {
            this.componentConfigResolveService.resolve(component);
            if (component.contents) {
                this.resolve(component.contents);
            }
        });
    }
    filterSchema(schema: any, config: Config): Record<string, any> {
        const filteredSchema: Record<string, any> = {};

        // configs.forEach((config: Config) => {
        const { path } = config;
        const paths = path.split('/').filter((p: string) => p);
        if (paths.length < 1) {
            return {};
        }
        let currentSchema = schema;
        let currentFilteredSchema = filteredSchema;

        paths.forEach((pathSegment: string, index: number) => {
            const [key, value] = pathSegment.split(':');
            const isLast = index === paths.length - 1;
            if (value) {
                if (!Array.isArray(currentSchema)) {
                    throw new Error(`Expected array at path ${pathSegment}`);
                }
                // eslint-disable-next-line eqeqeq
                const itemIndex = currentSchema.findIndex((item) => item.id == key);
                if (itemIndex === -1) {
                    throw new Error(`Item with id ${value} not found at path ${pathSegment}`);
                }
                currentSchema = currentSchema[itemIndex];// object
                const item = { id: key, [value]: currentSchema[value] };
                currentFilteredSchema.push(item);
                currentFilteredSchema = item[value];
                currentSchema = currentSchema[value];
            } else {
                if (isLast) {
                    currentFilteredSchema[key] = currentSchema[key];
                }
                else {
                    currentSchema = currentSchema[key];
                    currentFilteredSchema[key] = currentFilteredSchema[key] || paths[index + 1].indexOf(':') !== -1 ? [] : {};
                    currentFilteredSchema = currentFilteredSchema[key];
                }
            }
        });
        // });
        return filteredSchema;
    }
    private mergeArray(target: any, source: any) {
        if (Array.isArray(target) && Array.isArray(source)) {
            const mergedMap = new Map();
            target.forEach(item => {
                if (item.id != null) {
                    mergedMap.set(item.id, cloneDeep(item));
                }
            });

            // 合并源数组元素到Map，存在则深度合并
            source.forEach(item => {
                if (item.id != null) {
                    const existing = mergedMap.get(item.id);
                    if (existing) {
                        // 合并现有元素和当前元素
                        mergedMap.set(item.id, merge(existing, item));
                    } else {
                        mergedMap.set(item.id, cloneDeep(item));
                    }
                }
            });
            const combined = Array.from(mergedMap.values());
            return combined;
        }
    }
}
