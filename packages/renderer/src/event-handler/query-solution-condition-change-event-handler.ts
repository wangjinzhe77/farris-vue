import { Injector, Module } from "@farris/devkit-vue";
import { EventEmitter } from "../common";
import { FormMetadataService } from "../service";
import { ViewEvent } from "../types";
import { EventHandler } from "./types";

export class QuerySolutionConditionChangeEventHandler implements EventHandler {
    constructor(private emitter: EventEmitter, private formMetadataService: FormMetadataService, private module: Module, private injector: Injector){}
    bind(): void {
        this.emitter.on('conditionChange', (payload: ViewEvent) => this.onConfigionChange(payload));
    }
    dispose(): void {
        this.emitter.on('conditionChange', (payload: ViewEvent) => this.onConfigionChange(payload));
    }

    private onConfigionChange(payload: ViewEvent){
        if (!payload) {
            return;
        }
        const event: ViewEvent = payload;
        const { token, type } = event;
        if ((type !== 'query-solution') || !token) {
            return;
        }
        const relatedComponent = this.formMetadataService.getRelatedComponent(token);
        if (!relatedComponent) {
            return;
        }
        const viewModel = this.module.getViewModel(relatedComponent.id);
        if (!viewModel) {
            return;
        }
        const conditions = event.payloads[0];
        viewModel.uiStore?.setValue('originalFilterConditionList', JSON.stringify(conditions));
    }

}
