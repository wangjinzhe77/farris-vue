export interface EventHandler {
    bind(): void;
    dispose(): void;
}
