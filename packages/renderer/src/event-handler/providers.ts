import { Injector, Module, StaticProvider } from "@farris/devkit-vue";
import { EventEmitter } from "../common";
import { ComponentService, FormMetadataService } from "../service";
import { EVENT_HANDLERS_TOKEN } from "../tokens";
import { LookupDataMappingEventHandler } from "./lookup-data-mapping-event-handler";
import { EntityResolver, FieldResolver } from "../resolvers";
import { ModelValueUpdateEventHandler } from "./model-value-update-event-handler";
import { EndEditCellEventHandler } from "./end-edit-cell-event-handler";
import { DataViewRowClickEventHandler } from "./data-view-row-click-event-handler";
import { LookupClearEventHandler } from "./lookup-clear-event-handler";
import { DataGridPageIndexChangeEventHandler } from "./data-grid-page-index-change-event-handler";
import { DataGridPageSizeChangeEventHandler } from "./data-grid-page-size-change-event-handler";
import { DataGridSelectionChangeEventHandler } from "./data-grid-selection-change-event-handler";
import { QuerySolutionConditionChangeEventHandler } from "./query-solution-condition-change-event-handler";

export const eventHanderProviders: StaticProvider[] = [
    { provide: EVENT_HANDLERS_TOKEN, useClass: LookupDataMappingEventHandler, deps: [EventEmitter, FormMetadataService, Module, Injector], multi: true },
    { provide: EVENT_HANDLERS_TOKEN, useClass: ModelValueUpdateEventHandler, deps: [EventEmitter, FormMetadataService, Module, EntityResolver, FieldResolver, Injector], multi: true },
    { provide: EVENT_HANDLERS_TOKEN, useClass: EndEditCellEventHandler, deps: [EventEmitter, FormMetadataService, Module, EntityResolver, FieldResolver, ComponentService, Injector], multi: true },
    { provide: EVENT_HANDLERS_TOKEN, useClass: DataViewRowClickEventHandler, deps: [EventEmitter, FormMetadataService, Module, EntityResolver, FieldResolver, Injector], multi: true },
    { provide: EVENT_HANDLERS_TOKEN, useClass: LookupClearEventHandler, deps: [EventEmitter, FormMetadataService, Module, EntityResolver, FieldResolver, Injector], multi: true },
    { provide: EVENT_HANDLERS_TOKEN, useClass: DataGridPageIndexChangeEventHandler, deps: [EventEmitter, FormMetadataService, Module, EntityResolver, FieldResolver, Injector], multi: true },
    { provide: EVENT_HANDLERS_TOKEN, useClass: DataGridPageSizeChangeEventHandler, deps: [EventEmitter, FormMetadataService, Module, EntityResolver, FieldResolver, Injector], multi: true },
    { provide: EVENT_HANDLERS_TOKEN, useClass: DataGridSelectionChangeEventHandler, deps: [EventEmitter, FormMetadataService, Module, EntityResolver, FieldResolver, Injector], multi: true },
    { provide: EVENT_HANDLERS_TOKEN, useClass: QuerySolutionConditionChangeEventHandler, deps: [EventEmitter, FormMetadataService, Module, EntityResolver, FieldResolver, Injector], multi: true }
];
