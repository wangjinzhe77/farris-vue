import { EntityPathBuilder, EntityPathNode, EntityPathNodeType, Injector, Module } from "@farris/devkit-vue";
import { EventEmitter } from "../common";
import { ComponentService, FormMetadataService } from "../service";
import { EventHandler } from "./types";
import { EntityResolver, FieldResolver } from "../resolvers";
import { ENTITY_STORE_SUFFIX, ViewEvent } from "../types";
import { get } from "lodash-es";

export class EndEditCellEventHandler implements EventHandler {
    constructor(
        private emitter: EventEmitter,
        private formMetadataService: FormMetadataService,
        private module: Module,
        private entityResolver: EntityResolver,
        private fieldResolver: FieldResolver,
        private componentService: ComponentService,
        private injector: Injector) {
    }

    private onEndEditCell(payload: ViewEvent) {
        if (!payload) {
            return;
        }
        const event: ViewEvent = payload;
        const { token, type, payloads } = event;
        const eventArgs = payloads[0] || {};
        const component = this.formMetadataService.getMetadataById(token);
        if (!component) {
            return;
        }
        const { newValue, column, row, editor } = eventArgs;

        const field = column?.binding?.field || null;
        if (!field) {
            return;
        }
        const { bindingPath, dataSource } = this.fieldResolver.resolve(field) || {};
        if (!dataSource || !bindingPath) {
            return;
        }
        const { bindingPaths = [], primaryKey = 'id' } = this.entityResolver.resolve(dataSource) || {};
        const primaryValue = row?.raw[primaryKey];
        const fieldPath = bindingPath.split('.');

        const entityStoreId = this.formMetadataService.getModuleCode() + ENTITY_STORE_SUFFIX;
        const entityStore = this.module.getEntityStore(entityStoreId);
        if (!entityStore) {
            return;
        }
        const entityListPath = `/${bindingPaths.join('/')}`;
        const entityList = entityStore?.getEntityListByPath(entityListPath);
        const entity = entityList?.getEntityById(primaryValue);
        if (!entity) {
            return;
        }
        const entityPathCreator = new EntityPathBuilder(entityStore);
        const entityPaths = entityPathCreator.build(entityListPath, true);
        entityPaths.appendNode(new EntityPathNode(EntityPathNodeType.IdValue, primaryValue));
        fieldPath.forEach((path: string) => {
            entityPaths.appendNode(new EntityPathNode(EntityPathNodeType.PropName, path));
        });
        entityStore?.setValueByPath(entityPaths, newValue);
        // 继续处理帮助场景
        if (!column || !column.editor) {
            return;
        }
        const { type: editorType, separator = ',' } = column.editor;
        if (editorType === 'lookup' && editor) {
            const { mappingFields, items } = editor;
            if (!mappingFields || Object.keys(mappingFields).length < 1) {
                return;
            }
            Object.keys(mappingFields).forEach((key: string) => {
                const fieldPath = mappingFields[key];
                const value = (items && items.length > 0) ? items.map((item: any) => {
                    return get(item, key);
                }).join(separator) : null;
                const fieldPaths = fieldPath.split('.');
                const paths = bindingPaths.concat(fieldPaths);
                const path = '/' + paths.join('/');
                entityStore?.setValueByPath(path, value);
                // update row data
                const field = fieldPaths.join('.');
                if (row.data[field]) {
                    row.data[field].data = value;
                }
            });
            const datagrid = this.componentService.getComponentById(token);
            if (datagrid) {
                datagrid.acceptDataItem(row);
            }
        }
    }

    bind(): void {
        this.emitter.on('endEditCell', (payload: ViewEvent) => this.onEndEditCell(payload));
    }

    dispose(): void {
        this.emitter.off('endEditCell', (payload: ViewEvent) => this.onEndEditCell(payload));
    }
}
