

import { EventHandler } from "./types";
import { Injector, Module } from "@farris/devkit-vue";
import { FormMetadataService } from "../service";
import { ViewEvent } from "../types";
import { EventEmitter } from "../common";
import { EntityResolver, FieldResolver } from "../resolvers";

export class ModelValueUpdateEventHandler implements EventHandler {

    constructor(private emitter: EventEmitter, private formMetadataService: FormMetadataService, private module: Module, private entityResolver: EntityResolver, private fieldResolver: FieldResolver, private injector: Injector) {
    }

    private onModelValueUpdate(payload: any) {
        if (!payload) {
            return;
        }
        const { field: fieldId, value } = payload;
        if (!fieldId) {
            return;
        }
        const field = this.fieldResolver.resolve(fieldId);
        if (!field) {
            return;
        }
        const { dataSource, bindingPath } = field;
        if (!dataSource || !bindingPath) {
            console.error(`Invalid field: ${JSON.stringify(field)}`);
            return;
        }
        const entity = this.entityResolver.resolve(dataSource);
        if (!entity) {
            console.error(`Invalid datasource: ${dataSource}`);
            return;
        }
        const fieldBindingPaths = bindingPath.split('.').filter((item: string) => item);
        const paths = entity.bindingPaths.concat(fieldBindingPaths);
        const entityPath = `/${paths.join('/')}`;
        // use root component
        const frameComponent = this.formMetadataService.getFrameComponent();
        if (!frameComponent) {
            return;
        }
        const viewModel = this.module.getViewModel(frameComponent.id);
        const currentEntity = viewModel?.entityStore?.getCurrentEntity();
        if (!currentEntity || !currentEntity.idValue) {
            return;
        }
        viewModel?.entityStore?.setValueByPath(entityPath, value);
    }

    public bind() {
        this.emitter.on('update:modelValue', (payload: ViewEvent) => this.onModelValueUpdate(payload));
    }

    public dispose() {
        this.emitter.off('update:modelValue', (payload: ViewEvent) => this.onModelValueUpdate(payload));
    }
}
