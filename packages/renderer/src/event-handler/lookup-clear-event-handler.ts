import { EventHandler } from "./types";
import { Injector, Module } from "@farris/devkit-vue";
import { FormMetadataService } from "../service";
import { ENTITY_STORE_SUFFIX, ViewEvent } from "../types";
import { EventEmitter } from "../common";

export class LookupClearEventHandler implements EventHandler {

    constructor(private emitter: EventEmitter, private formMetadataService: FormMetadataService, private module: Module, private injector: Injector) {
    }

    private onClearMapping(payload: ViewEvent) {
        if (!payload) {
            return;
        }
        const event: ViewEvent = payload;
        const { token, type, payloads } = event;
        const eventArgs = payloads[0] || '';
        const component = this.formMetadataService.getMetadataById(token);
        if (!component) {
            return;
        }
        const { mappingFields } = eventArgs;
        const mapFields = typeof mappingFields === 'string' ? JSON.parse(mappingFields) : mappingFields;
        const { id } = component;
        const relatedComponent = this.formMetadataService.getRelatedComponent(id);
        if (!relatedComponent) {
            return;
        }
        const { viewModel: viewModelId } = relatedComponent;
        if (!viewModelId) {
            return;
        }
        const viewModelMetadata = this.formMetadataService.getViewModelById(viewModelId);
        if (!viewModelMetadata) {
            return;
        }
        const { bindTo: bindingPath = '' } = viewModelMetadata;
        const bindingPaths = bindingPath.split('/').filter((path: string) => path);
        const entityStoreId = this.formMetadataService.getModuleCode() + ENTITY_STORE_SUFFIX;
        const entityStore = this.module.getEntityStore(entityStoreId);
        Object.keys(mapFields).forEach((key: string) => {
            const fieldPath = mapFields[key];
            const value = null;
            const fieldPaths = fieldPath.split('.');
            const paths = bindingPaths.concat(fieldPaths);
            const path = '/' + paths.join('/');
            entityStore?.setValueByPath(path, value);
        });
    }

    bind(): void {
        this.emitter.on('clear', (payload: ViewEvent) => this.onClearMapping(payload));
    }

    dispose(): void {
        this.emitter.off('clear', (payload: ViewEvent) => this.onClearMapping(payload));
    }
}
