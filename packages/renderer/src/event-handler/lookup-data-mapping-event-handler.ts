

import { EventHandler } from "./types";
import { Injector, Module } from "@farris/devkit-vue";
import { FormMetadataService } from "../service";
import { ENTITY_STORE_SUFFIX, ViewEvent } from "../types";
import { EventEmitter } from "../common";
import { get } from "lodash-es";

export class LookupDataMappingEventHandler implements EventHandler {

    constructor(private emitter: EventEmitter, private formMetadataService: FormMetadataService, private module: Module, private injector: Injector) {
    }

    public onMappingData(payload: ViewEvent) {
        if (!payload) {
            return;
        }
        const event: ViewEvent = payload;
        const { token, type, payloads } = event;
        const eventArgs = payloads[0] || {};
        const { items } = eventArgs;
        const component = this.formMetadataService.getMetadataById(token);
        if (!component) {
            return;
        }
        const mapFields = JSON.parse(component.editor?.mappingFields || component.editor?.mapFields || {});
        const { separator = ',' } = component.editor || {};
        const { id } = component;
        const relatedComponent = this.formMetadataService.getRelatedComponent(id);
        if (!relatedComponent) {
            return;
        }
        const { viewModel: viewModelId } = relatedComponent;
        if (!viewModelId) {
            return;
        }
        const viewModelMetadata = this.formMetadataService.getViewModelById(viewModelId);
        if (!viewModelMetadata) {
            return;
        }
        const { bindTo: bindingPath = '' } = viewModelMetadata;
        const bindingPaths = bindingPath.split('/').filter((path: string) => path);
        const entityStoreId = this.formMetadataService.getModuleCode() + ENTITY_STORE_SUFFIX;
        const entityStore = this.module.getEntityStore(entityStoreId);
        Object.keys(mapFields).forEach((key: string) => {
            const fieldPath = mapFields[key];
            const value = items.map((item: any) => {
                return get(item, key);
            }).join(separator);
            const fieldPaths = fieldPath.split('.');
            const paths = bindingPaths.concat(fieldPaths);
            const path = '/' + paths.join('/');
            entityStore?.setValueByPath(path, value);
        });
    }

    public bind() {
        this.emitter.on('update:dataMapping', (payload: ViewEvent) => this.onMappingData(payload));
    }

    public dispose() {
        this.emitter.off('update:dataMapping', (payload: ViewEvent) => this.onMappingData(payload));
    }
}
