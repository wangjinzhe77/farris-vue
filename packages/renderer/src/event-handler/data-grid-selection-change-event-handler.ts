import { Injector, Module } from "@farris/devkit-vue";
import { EventEmitter } from "../common";
import { FormMetadataService } from "../service";
import { ViewEvent } from "../types";

/**
 * 勾选事件处理器
 */
export class DataGridSelectionChangeEventHandler {

    constructor(private emitter: EventEmitter, private formMetadataService: FormMetadataService, private module: Module, private injector: Injector) {
    }

    bind(): void {
        this.emitter.on('selectionChange', (payload: ViewEvent) => this.onSelectionChange(payload));
    }

    dispose(): void {
        this.emitter.on('selectionChange', (payload: ViewEvent) => this.onSelectionChange(payload));
    }

    private onSelectionChange(payload: ViewEvent) {
        if (!payload) {
            return;
        }
        const event: ViewEvent = payload;
        const { token, type } = event;
        if ((type !== 'data-grid' && type !== 'tree-grid') || !token) {
            return;
        }
        const component = this.formMetadataService.getMetadataById(token);
        if (!component) {
            return;
        }
        const { id } = component;
        if (!id) {
            return;
        }
        const relatedComponent = this.formMetadataService.getRelatedComponent(id);
        if (!relatedComponent) {
            return;
        }
        const viewModel = this.module.getViewModel(relatedComponent.id);
        if (!viewModel) {
            return;
        }
        const { payloads } = event;
        const items = payloads[0];
        const ids = items.map((item: any) => item.id);
        viewModel.uiStore?.setValue('ids', ids);
    }

}
