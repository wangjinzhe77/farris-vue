import { Injector, Module } from "@farris/devkit-vue";
import { EventEmitter } from "../common";
import { FormMetadataService } from "../service";
import { ViewEvent } from "../types";
import { EventHandler } from "./types";

export class DataGridPageSizeChangeEventHandler{
    constructor(private emitter: EventEmitter, private formMetadataService: FormMetadataService, private module: Module, private injector: Injector) {
    }

    bind(): void {
        this.emitter.on('pageSizeChanged', (payload: ViewEvent) => this.onPageSizeChange(payload));
    }

    dispose(): void {
        this.emitter.on('pageSizeChanged', (payload: ViewEvent) => this.onPageSizeChange(payload));
    }

    private onPageSizeChange(payload: ViewEvent) {
        if (!payload) {
            return;
        }
        const event: ViewEvent = payload;
        const { token, type } = event;
        if ((type !== 'data-grid' && type !== 'tree-grid') || !token) {
            return;
        }
        const component = this.formMetadataService.getMetadataById(token);
        if (!component) {
            return;
        }
        const { id } = component;
        if (!id) {
            return;
        }
        const relatedComponent = this.formMetadataService.getRelatedComponent(id);
        if (!relatedComponent) {
            return;
        }
        const viewModel = this.module.getViewModel(relatedComponent.id);
        if (!viewModel) {
            return;
        }
        const { payloads } = event;
        const { pageSize } = payloads[0];
        viewModel.entityStore?.setPaginationByPath(viewModel.bindingPath, { pageSize });
    }
}
