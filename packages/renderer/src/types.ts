import { EventEmitter } from "./common";

/* eslint-disable no-use-before-define */
// #region form schema
export interface EntityFieldMetadata {
    id: string;
    required: any;
    multiLanguage: boolean;
    defaultValue: any;
    readonly: any;
    bindingPath: string;
    label: string;
    type: Record<string, any>;
    name: string;
}
export interface EntityTypeMetadata {
    entities: EntityMetadata[];
    primary: string;
    displayName: string;
    fields: EntityFieldMetadata[];
    name: string;
}
export interface EntityMetadata {
    id: string;
    label: string;
    code: string;
    type: EntityTypeMetadata;
    name: string;
}
export interface ViewModelFieldMetadata {
    id: string;
    type: string;
    fieldName: string;
    groupId: string;
    groupName: string;
}
export interface CommandParamMetadata {
    name: string;
    shownName: string;
    value: string;
}
export interface CommandRefMetadata {
    id: string;
    code: string;
    name: string;
    params?: CommandParamMetadata[];
    handlerName: string;
    cmpId: string;
    isInvalid: boolean;
}
export interface StateMetadata {
    id: string;
    code: string;
    name: string;
    type: string;
    category: string;
}
export interface PaginationMetadata {
    enable: boolean;
    pageSize: number;
    pageList: string;
}
export interface ViewModelMetadata {
    id: string;
    code: string;
    name: string;
    fields?: ViewModelFieldMetadata[];
    commands?: CommandRefMetadata[];
    states?: StateMetadata[];
    bindTo?: string;
    parent?: string;
    enableUnifiedSession?: boolean;
    enableValidation?: boolean;
    pagination?: PaginationMetadata;
    serviceRefs?: any[];
}
export interface StateMachineRefMetadata {
    /**
     * 状态机标识，非元数据id
     */
    id: string;
    name: string;
    /**
     * 状态机元数据id
     */
    uri: string;
    code: string;
    nameSpace: string;
}
export interface RefedHandlerMetadata {
    host: string;
    handler: string;
}
export interface WebCommandRefMetadata {
    id: string;
    path: string;
    name: string;
    refedHandlers: RefedHandlerMetadata[];
    code: string;
    nameSpace: string;
}
export interface ComponentAppearanceMetadata {
    class: string;
    style?: string;
}
export interface ComponentMetadata {
    id: string;
    type: string;
    appearance?: ComponentAppearanceMetadata | null;
    componentType: string;
    viewModel: string;
    onInit: string;
    afterViewInit: string;
    contents: ComponentMetadata[];
    items?: any[];
    visible: string | boolean;
    component?: ComponentMetadata;
    [prop: string]: any;
}
export interface VariableMetadata {
    id: string;
    name: string;
    path: string;
    defaultValue: any;
    readonly: boolean;
    bindingField: string;
    bindingPath: string;
    code: string;
    label: string;
    type: any;
}
export interface EntityModelMetadata {
    id: string;
    code: string;
    name: string;
    extendProperties: any;
    entities: EntityMetadata[];
    sourceUri: string;
    sourceType: string;
    voPath: string;
    eapiNameSpace: string;
    eapiName: string;
    eapiCode: string;
    eapiId: string;
    variables: VariableMetadata[];
}
export interface ModuleMetadata {
    id: string;
    code: string;
    name: string;
    bootstrap: string;
    entity: EntityModelMetadata[];
    stateMachines: StateMachineRefMetadata[];
    viewmodels: ViewModelMetadata[];
    components: ComponentMetadata[];
    webcmds: WebCommandRefMetadata[];
    expressions: any[];
    actions: any[];
    [prop: string]: any;
}
export interface OptionsMetadata {
    renderMode: string;
    formRulePushMode: string;
}
export interface FormMetadata {
    module: ModuleMetadata;
    options: OptionsMetadata;
}
// #endregion

// #region stateMachine schema
export interface StateMachineStateMetadata {
    name: string;
    state: string;
    description: string;
}
export interface StateMachineActionMetadata {
    [actionName: string]: {
        name: string;
        transitTo: string;
        description: string;
    };
}
export interface StateMachineRenderStateMetadata {
    [render: string]: {
        name: string;
        description: string;
        condition: any[];
    };
}
export interface StateMachineMetadata {
    action: StateMachineActionMetadata;
    initialState: string;
    renderState: StateMachineRenderStateMetadata;
    state: StateMachineStateMetadata[];
}

// #endregion

// #region resource schema
export interface StringResourceMetadata {
    id: string;
    resourceType: string;
    value: string;
}
export interface ResourceMetadata {
    originalLanguage: string;
    stringResources: StringResourceMetadata[];
    resourceType: string;
}
// #endregion

export interface ResolvedEntity {
    bindingPaths: string[];
    primaryKey: string;
}

export interface ResolvedEntityField {
    id: string;
    bindingPath: string;
    required: any;
    readonly: any;
    multiLanguage: boolean;
    label: string;
    dataSource: string;
}

export interface UseEntityResolver {
    resolve(dataSource: string): ResolvedEntity | null;
}

export interface UseFieldResolver {
    resolve(fieldId: string): ResolvedEntityField | null;
}

export interface UseParamPersistence {
    persist(): Promise<void>;
}
export interface UseEventEmitter {
    emitter: EventEmitter;
}

export interface UseEventHandler {
    bind(): void;
    destroy(): void;
}
export interface Metadata {
    id: string;
    content: any;
    refs: any[];
}
export interface UseMetadata {
    setup(): void;
    getViewModels(): ViewModelMetadata[];
    getFrameComponent(): ComponentMetadata | null;
    getEntity(): EntityMetadata;
    getMetadataById(id: string): any;
    getComponents(): any[];
    getRelatedComponent(metadataId: string): Record<string,any>;
    getViewModelById(id: string): ViewModelMetadata | null;
    getModuleId(): string;
}

export interface UseSession {
    sessionId: string;
    metadataId: string;
    version: string;
    mode: string;
}

export interface UseCache {
    get(key: string): any;
    set(key: string, value: any): void;
    remove(key: string): boolean;
}

export interface UseSchemas {
    form: Metadata;
    stateMachine: Metadata;
    webcmd: Metadata[];
    resource: Metadata[];
}
export enum MetadataType {
    Component = 'component',
    DataGrid = 'data-grid',
    TreeGrid = 'tree-grid'
}

export interface ViewEvent {
    token: string;
    eventName: string;
    type: string;
    payloads: any[];
}

export const ENTITY_STORE_SUFFIX = '-entitystore';
