 import { InjectionToken } from "@farris/devkit-vue";
import { FormMetadata } from "./types";
import { ComponentConfigResolver } from "./component-config-resolver";
import { ComponentConfigDependencyResolver } from "./component-config-dependency-resolver";
import { ConfigDependencyResolver } from "./config-dependency-resolver";
import { ChangeObserver } from "./change-observer";
import { ChangeResolver } from "./change-resolver";
import { Ref } from "vue";
import { CallbackHandler } from "./callback-handler";

export const FORM_METADATA_TOKEN = new InjectionToken<FormMetadata>('@farris/form_metadata');
export const EVENT_HANDLERS_TOKEN = new InjectionToken<FormMetadata>('@farris/event_handlers_token');
export const MODULE_CONFIG_ID_TOKEN = new InjectionToken<string>('@farris/module_config_id_token');
export const RENDER_TOKEN = new InjectionToken<Ref>('@farris/render_token');
export const CONFIG_DEPENDENCY_RESOLVER_TOKEN = new InjectionToken<ConfigDependencyResolver[]>('@farris/config_dependency_resolver_token');
export const COMPONENT_CONFIG_RESOLVER_TOKEN = new InjectionToken<ComponentConfigResolver[]>('@farris/component_config_resolver_token');
export const COMPONENT_CONFIG_DEPENDENCY_RESOLVER_TOKEN = new InjectionToken<ComponentConfigDependencyResolver[]>('@farris/component_config_dependency_resolver_token');

export const CHANGE_OBSERVER_TOKEN = new InjectionToken<ChangeObserver[]>('@farris/change_observer_token');
export const CHANGE_RESOLVER_TOKEN = new InjectionToken<ChangeResolver<any>[]>('@farris/change_resolver_token');
export const CALLBACK_HANDLER_TOKEN = new InjectionToken<CallbackHandler[]>('@farris/callback_handler_token');
