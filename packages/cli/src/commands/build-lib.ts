import ora from 'ora';
import { buildCommon, BuildCommonOptions } from "./build.js";
import { generateTypes } from "../plugins/dts.js";
import { existsSync } from 'node:fs';
// import { buildCss } from './build-css.js';
import { createPackageJson } from '../plugins/create-package-json.js';

export type BuildLibOptions = BuildCommonOptions & {
  dts?: boolean;
  emitPackage?: boolean;
  format: 'es' | 'umd' | 'systemjs';
};

export async function buildLib(options: BuildLibOptions) {

  const { dts = true, emitPackage } = options;
  const config = {
    publicDir: '',
    plugins: [
      dts && generateTypes("./components", "./package/types"),
      emitPackage && createPackageJson()
    ]
  };
  const spinner = ora(`build lib begin`).start();

  spinner.text = `building lib`;

  await buildCommon({ ...options, config, type: 'lib' });

  // const { css } = libConfig.build;

  // if (existsSync(css.entry)) {
  //   spinner.text = `building css`;
  //   await buildCss({ entry: css.entry, outfile: css.outfile });
  // }

  spinner.succeed(`build lib success`);
};
