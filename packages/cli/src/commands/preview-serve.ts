import { preview, mergeConfig } from 'vite';
import { getViteConfig } from '../common/get-vite-config.js';

type PreviewOptions = {
  port?: number;
  host?: string;
};

export const previewServe = async (options: PreviewOptions) => {
  const { port = 4137, host = true } = options;

  const config = await getViteConfig();
  const previewConfig = mergeConfig({
    configFile: false,
    preview: {
      port,
      host,
      open: true
    }
  }, config);

  const server = await preview(previewConfig);

  server.printUrls();
};
