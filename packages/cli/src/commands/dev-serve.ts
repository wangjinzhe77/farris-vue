import { createServer, mergeConfig } from 'vite';
import { getViteConfig } from '../common/get-vite-config.js';

type ServeOptions = {
  port?: number;
  host?: string;
};

export const devServe = async (options: ServeOptions) => {
  const { port = 1337, host = true } = options;

  const config = await getViteConfig();

  const devConfig = mergeConfig(config, {
    configFile: false,
    mode: 'dev',
    server: {
      port,
      host,
      hmr: true
    }
  });

  const server = await createServer(devConfig);
  await server.listen();

  server.printUrls();
};
