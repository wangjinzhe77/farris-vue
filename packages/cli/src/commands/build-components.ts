import { readdirSync, lstatSync, existsSync } from "node:fs";
import { resolve } from "node:path";
import ora from 'ora';
import { BuildCommonOptions, buildCommon } from "./build.js";
import { buildCss } from "./build-css.js";
import { CWD } from "../common/constant.js";

const excludeComponents = ['property-panel', 'dynamic-resolver', 'dynamic-view'];

const BaseEntry = './components';
const BaseOutDir = './package/components';

export async function buildComponents(options: BuildCommonOptions) {

  const components = readdirSync("./components").filter((name) => {
    const componentDir = resolve(CWD, `./components`, name);
    const isDir = lstatSync(componentDir).isDirectory();

    return isDir && readdirSync(componentDir).includes("index.ts");
  }).filter(name=> !excludeComponents.includes(name));

  const spinner = ora(`build components begin`).start();

  components.forEach(async component=>{
    const entry = resolve(CWD, `${BaseEntry}/${component}/index.ts`);
    const outDir = resolve(CWD, `${BaseOutDir}/${component}`);
    const config = {
      publicDir: '',
      build: {
        lib:{
          entry
        },
        outDir
      }
    };

    spinner.text = `building ${component}`;

    await buildCommon({ ...options, config, type: 'component'  });

    spinner.succeed(`build ${component} success`);

    const cssEntry = `${BaseEntry}/${component}/src/${component}.scss`;
    if(!existsSync(cssEntry)) {
      return;
    }
    await buildCss({...options,entry: cssEntry, outfile: `${BaseOutDir}/${component}/index.css`, showSpinner: false });
  });

};
