import { generateApp } from "../common/generate-app.js";
import inquirer from "inquirer";

const prompt = inquirer.createPromptModule();

const questions = [
  {
    type: "input",
    name: "name",
    message: "What is your project name?",
    default: "my-project",
  },
  {
    type: "list",
    name: "type",
    message: "What type of project is this?",
    choices: ["app", "lib"],
  },
  {
    type: "list",
    name: "platform",
    message: "What platform of project is this?",
    choices: ["web", "mobile"],
    when: (response: any) => {
      const { type } = response;
      return type === 'app';
    }
  },
  {
    type: "checkbox",
    name: "formats",
    message: "What output format of project is this?",
    choices: ["es", "umd", "systemjs"],
    default: ["es", "umd"],
    when: (response: any) => {
      const { type } = response;
      return type === 'lib';
    }
  },
  {
    type: "list",
    name: "format",
    message: "What output format of project is this?",
    choices: ["es", "umd", "systemjs"],
    default: ["es"],
    when: (response: any) => {
      const { type } = response;
      return type === 'app';
    }
  }
];

export async function createApp() {
  const response = await prompt(questions);

  generateApp(response);
};
