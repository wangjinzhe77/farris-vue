import { build } from "esbuild";
import { sassPlugin } from 'esbuild-sass-plugin';
import path from 'node:path';
import { CWD } from "../common/constant.js";
import ora from "ora";

const replaceUrl = (source: string, pathname: string, entry: string)=> {
  const basedir = path.dirname(pathname);
  const entrydir = path.resolve(CWD, path.dirname(entry));
  const relativedir = path.relative(entrydir, basedir);
  const regExp = /(url\(['"]?)(\.\.?\/[^'")]+['"]?)(\))/g;
  return source.replace(regExp, (substring, ...args)=> `url(./${path.join(relativedir, args[1]).replaceAll('\\', '/')})`);
};

export async function buildCss(options: any) {
  const { entry, outfile, minify = false, showSpinner = true } = options;

  const spinner = ora(`build css begin`).start();
  spinner.text = `building css`;

  if(!showSpinner) {
    spinner.stop();
  }


  await build({
    entryPoints: [entry],
    outfile,
    minify,
    bundle: true,
    logLevel: 'error',
    plugins: [sassPlugin({
      precompile(source, pathname) {
        return replaceUrl(source, pathname, entry);
      }
    })],
    loader: {
      '.ttf': 'dataurl'
    }
  });

  if(showSpinner) {
    spinner.succeed(`build css success`);
  }
}
