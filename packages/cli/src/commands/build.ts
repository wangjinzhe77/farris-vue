import { build, mergeConfig, loadConfigFromFile, UserConfig } from 'vite';
import { getViteConfig } from '../common/get-vite-config.js';

export type BuildCommonOptions = {
  configFile?: string;
  viteConfigFile?: string;
  config?: Record<string, any>;
  type: 'lib' | 'app' | 'component';
};

export async function buildCommon(options: BuildCommonOptions, loadFile = true) {
  const { configFile, viteConfigFile, config = {}, type } = options;

  if(!loadFile) {
    await build(config);
    return config;
  }

  const viteConfig = await loadConfigFromFile({command: 'build', mode: 'production'}, viteConfigFile);

  const defaultConfig = await getViteConfig(type, configFile);

  const libConfig =mergeConfig(mergeConfig(defaultConfig, viteConfigFile && viteConfig?.config ? viteConfig.config : {}), config);

  await build(libConfig);

  return libConfig;
};
