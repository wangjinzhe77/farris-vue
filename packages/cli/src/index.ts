#!/usr/bin/env node
import { Command } from 'commander';
import { getVersion } from "./common/get-version.js";

const program = new Command();

program.version(`@farris/cli ${getVersion()}`);

program
  .command('build')
  .description('构建')
  .option('-c --configFile [configFile]', 'config file path')
  .option('-vc --viteConfigFile [viteConfigFile]', 'vite config file path')
  .action(async (options) => {
    process.env.BUILD_TYPE = 'app';
    const { buildCommon } = await import('./commands/build.js');
    buildCommon(options);
  });

program
  .command('build-lib')
  .description('构建')
  .option('-c --configFile [configFile]', 'config file path')
  .option('-vc --viteConfigFile [viteConfigFile]', 'vite config file path')
  .option('-f --format [format]', 'lib format')
  .option('-d --dts', 'dts')
  .option('-ep --emitPackage', 'createPackage')
  .action(async (options) => {
    process.env.BUILD_TYPE = 'lib';
    const { buildLib } = await import('./commands/build-lib.js');
    return buildLib(options);
  });

program
  .command('build-components')
  .description('构建')
  .option('-c --configFile [configFile]', 'config file path')
  .option('-vc --viteConfigFile [viteConfigFile]', 'vite config file path')
  .action(async (options) => {
    process.env.BUILD_TYPE = 'components';
    const { buildComponents } = await import('./commands/build-components.js');
    return buildComponents(options);
  });
program
  .command('build-css')
  .description('构建')
  .option('-e --entry [entry]', 'entry file')
  .option('-o --outfile [outfile]', 'outfile')
  .option('-m --minify [minify]', 'minify')
  .action(async (options) => {
    process.env.BUILD_TYPE = 'css';
    const { buildCss } = await import('./commands/build-css.js');
    return buildCss(options);
  });

program
  .command('create-app')
  .description('新建 App 模板')
  .action(async () => {
    const { createApp } = await import('./commands/create-app.js');
    return createApp();
  });

program
  .command('dev')
  .description('创建 Dev Server')
  .option('-p --port [port]', 'Server Port')
  .action(async (options) => {
    process.env.BUILD_TYPE = 'app';
    const { devServe } = await import('./commands/dev-serve.js');
    return devServe(options);
  });

program
  .command('preview')
  .description('创建 Preview Server')
  .option('-p --port [port]', 'Server Port')
  .action(async (options) => {
    process.env.BUILD_TYPE = 'app';
    const { previewServe } = await import('./commands/preview-serve.js');
    return previewServe(options);
  });

program.parse();
