import { mergeConfig } from 'vite';
import CommonConfig from "./vite-common.js";
import { replace } from "../plugins/replace.js";
import { createComponentStyle } from "../plugins/create-component-style.js";

export default mergeConfig(CommonConfig, {
  logLevel: 'silent',
  plugins: [createComponentStyle(), replace((format, args)=> `..${args[1]}/index.${format}.js`)],
  build:{
    lib: {
      fileName: "index",
      formats: ["esm"],
    },
    rollupOptions: {
      output: {
        exports: "named",
        globals: (id: string)=>{
          if(id.includes('@components')) {
            const name = id.split('/').pop() || '';
            return name.slice(0,1).toUpperCase() + name.slice(1);
          }
          const map: Record<string, string> = {
            vue: "Vue"
          };
          return map[id];
        }
      }
    }
  }
});
