import { mergeConfig } from 'vite';
import { resolve } from "node:path";
import { CWD } from '../common/constant.js';
import { systemjsBundle } from '../plugins/systemjs-bundle.js';
import CommonConfig from "./vite-common.js";

export default mergeConfig(CommonConfig, {
  logLevel: 'silent',
  plugins: [systemjsBundle()],
  build:{
    lib: {
      entry: resolve(CWD, `./src/main.ts`),
      fileName: "index",
    },
    target: 'es2019',
    outDir: resolve(CWD, `./lib`),
    rollupOptions: {
      output: {
        exports: "named",
        globals: {
          vue: "Vue",
          '@farris/mobile-ui-vue': 'FarrisVue'
        }
      }
    },
  }
});
