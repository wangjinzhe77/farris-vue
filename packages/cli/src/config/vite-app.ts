import { mergeConfig } from 'vite';
import { htmlSystemPlugin } from '../plugins/html-system.js';
import CommonConfig from "./vite-common.js";

export default mergeConfig(CommonConfig, {
  define: {
    __VUE_OPTIONS_API__: 'true',
    __VUE_PROD_DEVTOOLS__: 'true',
    __VUE_PROD_HYDRATION_MISMATCH_DETAILS__: 'true'
  },
  plugins: [
    process.env.FARRIS_FORMAT === 'systemjs' && htmlSystemPlugin()
  ]
});
