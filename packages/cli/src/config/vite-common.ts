import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
import { CWD } from '../common/constant.js';

export default {
  root: CWD,
  base: './',
  plugins: [
    vue(),
    vueJsx()
  ],
  configFile: false,
  css: {
    preprocessorOptions: {
      scss: {
        api: 'modern-compiler', // 或 "modern"，"legacy",
        silenceDeprecations: ['global-builtin', 'legacy-js-api']
      },
    }
  }
};
