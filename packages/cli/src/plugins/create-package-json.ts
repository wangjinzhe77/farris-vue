import { readFileSync } from "node:fs";
import { resolve } from "node:path";
import { CWD } from "../common/constant.js";

export const createPackageJson = (): any => {
  return {
    name: 'create-package-json',
    generateBundle(){
      const pkg = readFileSync(resolve(CWD, 'package.json'), 'utf-8');
      const packageJson = JSON.parse(pkg);
      const result: Record<string, any> = {
        name: '',
        version: '',
        private: false,
        main: './index.umd.js',
        module: './index.esm.js',
        types: './types/index.d.ts',
        style: './index.css',
      };
      const exclude = ['scripts', 'devDependencies', 'private'];
      Object.keys(packageJson).forEach(key => {
        if (exclude.includes(key)) {
          return;
        }
        result[key] = packageJson[key];
      });

      this.emitFile({
        type: 'asset',
        fileName: 'package.json',
        source: JSON.stringify(result, null, 2)
      });
    }
  };
};
