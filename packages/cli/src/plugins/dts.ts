import dts from "vite-plugin-dts";

export function generateTypes(entry: string, outDir: string){
  return dts({
    entryRoot: entry,
    outDir
  });
};
