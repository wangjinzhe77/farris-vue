export function replace(path: (format: string, args: string[]) => string){
  return {
    name: 'farris-replace',
    renderChunk(code: string, chunk: any) {
      const fileNames = chunk.fileName.split('.');
      const format = fileNames[fileNames.length-2];
      return code.replace(/@components(\/\w+)/g, (...args)=>{
        return path(format, args);
      });
    }
  };
};
