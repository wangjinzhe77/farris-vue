const NotStyleComponents = ['designer-canvas', 'dynamic-view', 'dynamic-resolver', 'common'];

const getComponentStyle = (imports: string[]) => {

  const components = imports
    .filter(path => {
      return path.startsWith('@components/') && !NotStyleComponents.find(notStylecomponent => path.includes(notStylecomponent));
    })
    .filter((item, index, array) => {
      const targetIndex = array.filter(e=> e!==item).findIndex(e => item.includes(e));
      return targetIndex < 0;
    })
    .map(name => {
      const paths = name.split('/');
      return {
        name: paths[1],
        path: `../${paths[1]}/style.js`
      };
    });

  components.push({
    name: 'components',
    path: `./index.css`
  });

  const template = `${components.map(component => {
    const { path } = component;
    return `import '${path}';`;
  }).join('\n')}
`;

  return template;
};

export function createComponentStyle(): any{
  return {
    name: 'farris-gen-component-style',
    renderChunk(code: string, chunk: any) {
      const { imports } = chunk;

      const template = getComponentStyle(imports);

      template && this.emitFile({
        type: 'asset',
        fileName: 'style.js',
        source: template
      });
      return code;
    }
  };
};
