export const htmlSystemPlugin = () => {
  return {
    name: 'html-transform-system',
    transformIndexHtml(html: string) {
      const regexp = /<script.*type="module".*src="(.*?)"><\/script>/;
      return html.replace(regexp, (str: string, p1: string)=> `
    <script src="https://cdn.bootcdn.net/ajax/libs/systemjs/6.15.1/system.js"></script>
    <script >System.import("${p1}");</script>`);
    }
  };
};
