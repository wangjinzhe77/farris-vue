export function systemjsBundle () {
  return {
    name: 'systemjs-bundle',
    outputOptions(options: any){
      if(options.format === 'systemjs') {
        return {
          ...options,
          name: undefined
        };
      }
      return options;
    }
  };
};
