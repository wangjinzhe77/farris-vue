import { AliasOptions, LibraryOptions, PluginOption, mergeConfig } from 'vite';
import { getFarrisConfigAsync } from './get-farris-config.js';
import { getDependencies } from '../common/get-dependencies.js';

type FarrisConfig = {
  lib: LibraryOptions | false;
  format: 'es' | 'umd' | 'system';
  minify: boolean | 'terser' | 'esbuild';
  externals: { include: string[]; exclude: string[]; filter?: (externals: string[]) => boolean };
  externalDependencies: boolean;
  plugins?: PluginOption[];
  alias?: AliasOptions;
  outDir?: string;
  target: string | string[];
};

const formatFarrisConfig = (farrisConfig: FarrisConfig, type: string) => {
  const {
    lib,
    format,
    target,
    minify = true,
    outDir,
    externals = { include: [], exclude: [] },
    externalDependencies,
    plugins,
    alias
  } = farrisConfig;

  const dependencies = externalDependencies ? getDependencies() : [];

  let external = externals.include ? [...externals.include, ...dependencies] : dependencies;

  external = external.filter((item) => !externals.exclude?.includes(item));

  format && (process.env.FARRIS_FORMAT = format);

  const viteConfig = {
    build: {
      target,
      minify,
      lib: type === 'lib' ? lib : undefined,
      rollupOptions: {
        output: {
          format
        },
        external: externals.filter ? externals.filter(external) : external
      },
      outDir
    },
    plugins,
    resolve: {
      alias
    }
  };
  return viteConfig;
};

const formatViteConfig = async (type: string, configFile?: string) => {
  const { viteConfig = {}, ...farrisConfig } = await getFarrisConfigAsync(configFile);
  const customViteConfig = formatFarrisConfig(farrisConfig, type);

  return mergeConfig(customViteConfig, viteConfig);
};

const getViteConfig = async (type = 'app', configFile?: string) => {
  const viteConfig = await formatViteConfig(type, configFile);
  const { default: defaultViteConfig } = await import(`../config/vite-${type}.js`);

  return mergeConfig(defaultViteConfig, viteConfig);
};

export { getViteConfig };
