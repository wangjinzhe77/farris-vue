import { existsSync } from 'node:fs';
import { fileURLToPath } from 'node:url';
import { join, dirname } from 'node:path';

function findRootDir(dir: string): string {
  if (existsSync(join(dir, 'farris.config.js'))) {
    return dir;
  }

  const parentDir = dirname(dir);
  if (dir === parentDir) {
    return dir;
  }

  return findRootDir(parentDir);
}
export const getDirname = (url: string)=> fileURLToPath(new URL('.', url));

// Root paths

export const CWD = process.cwd();
export const ROOT = findRootDir(CWD);
export const FAARIS_CONFIG_FILE = join(CWD, 'farris.config.mjs');
