import { pathToFileURL } from 'node:url';
import { FAARIS_CONFIG_FILE } from './constant.js';

async function getFarrisConfigAsync(configFile= FAARIS_CONFIG_FILE) {
  try {
    return (await import(pathToFileURL(configFile).href)).default;
  } catch (err) {
    return {};
  }
}

export { getFarrisConfigAsync };
