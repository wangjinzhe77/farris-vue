import { readFileSync } from "node:fs";
import { resolve } from 'node:path';
import { CWD } from "../common/constant.js";

export const getDependencies = () => {
  const pkg = readFileSync(resolve(CWD, 'package.json'), 'utf-8');
  const packageJson = JSON.parse(pkg);
  const { dependencies, peerDependencies } = packageJson;

  return [...Object.keys(dependencies || {}), ...Object.keys(peerDependencies || {})];
};
