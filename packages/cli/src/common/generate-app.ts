import { resolve } from 'path';
import { createRequire } from "node:module";
import { CWD, getDirname } from './constant.js';
import { getVersion } from "./get-version.js";

const require = createRequire(import.meta.url);

const { copySync, readFileSync, writeFileSync } = require("fs-extra");

const Array2String = (arr: any[]) => {
  return `[${arr.reduce((acc, cur, index) => {
    acc = index === 1 ? "'" + acc + "'" : acc;
    acc += `,'${cur}'`;
    return acc;
  })}]`;
};

const copyTemplate = (template: string, target: string) => {
  const templatePath = resolve(getDirname(import.meta.url), '../../templates', template);
  const targetPath = resolve(CWD, target);
  console.log(templatePath, targetPath);
  copySync(templatePath, targetPath);
};

const replaceContent = (path: string, args: Record<string, any>) => {
  let content: string = readFileSync(path, 'utf-8');

  Object.keys(args).forEach((key) => {
    const regexp = new RegExp(`<%= ${key} %>`, 'g');
    const value = Array.isArray(args[key]) ? Array2String(args[key]) : args[key];
    content = content.replace(regexp, value);
  });

  writeFileSync(path, content);

};

type GenerateAppOptions = {
  name: string;
  type: string;
  formats: string[];
  format: string;
  platform: string;
};

export const generateApp = (options: GenerateAppOptions) => {
  const { name, type, formats, format, platform } = options;
  const template = type === 'lib' ? 'lib' : platform;
  copyTemplate(template, name);

  const replaceFiles = [
    'package.json',
    'farris.config.mjs',
    'index.html'
  ];

  const cliVersion = getVersion();
  replaceFiles.forEach(replaceFile => {
    replaceContent(resolve(CWD, name, replaceFile), { name, formats, format, platform, cliVersion });
  });
  console.log(`cd ${name}`);
  console.log(`npm install or yarn`);
};
