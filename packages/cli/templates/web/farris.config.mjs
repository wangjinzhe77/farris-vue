import { fileURLToPath, URL } from 'node:url';

export default {
  format: "<%= format %>",
  // 输出目录  App模式默认值 './dist' Lib模式 './lib'
  // outDir: fileURLToPath(new URL('./dist', import.meta.url)),
  // 最小化 默认值 true
  minify: true,
  // 外部依赖排除项 默认值 { include: [], exclude: [] }
  // externals: {
  //   include: [],
  //   exclude: []
  // },
  // 是否排除 package.json 中 dependencies和 peerDependencies 依赖的包; App模式默认值 false Lib模式默认值 true
  externalDependencies: false,
  // 路径别名 默认值 null
  alias: [
    { find: '@', replacement: fileURLToPath(new URL('./src', import.meta.url)) }
  ],
  // 插件 默认值 [vue(), vueJsx()] 不要重复添加
  // plugins: [],
  // viteConfig 配置项
  viteConfig: {}
};
