import { createApp } from 'vue';
import FarrisVue from '@farris/ui-vue';
import App from './App.vue';
import router from './router';

const app = createApp(App);

app.use(router).use(FarrisVue);

app.mount('#app');
