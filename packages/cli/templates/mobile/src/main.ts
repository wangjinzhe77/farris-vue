import { createApp } from 'vue';
import FarrisVue from '@farris/mobile-ui-vue';
import App from './App.vue';
import router from './router';

import '@farris/mobile-ui-vue/style.css';

const app = createApp(App);

app.use(router).use(FarrisVue);

app.mount('#app');
