const { resolve } = require("path");
const { build } = require("vite");
const dts = require("vite-plugin-dts");

const replace = require("../plugins/replace.cjs");
const buildViteConfig = require("./build-vite-config.cjs");
const createPackageJson = require("./create-package.cjs");

const CWD = process.cwd();

exports.build = async () => {

    // 类库配置
    const lib = {
        entry: resolve(CWD, "./lib/index.ts"),
        name: "FarrisBefVue",
        fileName: "bef-vue",
        formats: ["esm", "umd"],
    };

    // 输出配置
    const outDir = resolve(CWD, "./package");

    // 插件配置
    const plugins = [
        dts({
            entryRoot: "./lib",
            outputDir: resolve(CWD, "./package/types"),
            include: [
                "./lib/**/*.ts",
                "./lib/**/*.tsx",
                "./lib/**/*.vue",
            ],
            noEmitOnError: false,
            skipDiagnostics: true,
        }),
        replace({ path: (format, args) => `.${args[1]}/bef-vue.${format}.js` })
    ];

    // 执行打包
    const config = buildViteConfig({ lib, outDir, plugins });
    await build(config);

    // 输出package.json
    await createPackageJson();
};
