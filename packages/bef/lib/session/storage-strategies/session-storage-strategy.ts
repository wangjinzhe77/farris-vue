import { StorageStrategy } from './types';

/**
 * 基于浏览器SessionStorage的BeSession缓存
 * @summary
 * 1、SeparatedSession模式下：
 *  {
 *    BE_SESSION_ID: {
 *      frmSessionId1_beSessionUri1: beSessionId-1,
 *      frmSessionId2_beSessionUri2: beSessionId-2,
 *    }
 *  }
 * 2、UnifiedSession模式下（在Debug状态，同模式1）：
 * {
 *    BE_SESSION_ID: {
 *      frmSessionId1: beSessionId-1,
 *      frmSessionId2: beSessionId-2,
 *    }
 *  }
 */
class SessionStorageStrategy implements StorageStrategy {

    /**
     * 缓存Token
     */
    private sessionStorageKey = 'BE_SESSION_ID';

    /**
     * 获取值
     */
    public getItem(beSessionKey: string): string {
        const beSessions = this.getAllBeSessions();
        return beSessions[beSessionKey];
    }

    /**
     * 设置值
     */
    public setItem(beSessionKey: string, beSessionId: string) {
        const beSessions = this.getAllBeSessions();
        beSessions[beSessionKey] = beSessionId;
        this.setAllBeSessions(beSessions);
    }

    /**
     * 删除值
     */
    public removeItem(beSessionKey: string) {
        const beSessions = this.getAllBeSessions();
        if (beSessions[beSessionKey]) {
            delete beSessions[beSessionKey];
        }
        this.setAllBeSessions(beSessions);
    }

    /**
     * 根据scope删除值
     */
    public removeItemsByScope(beSessionScope: string) {
        const beSessions = this.getAllBeSessions();
        Object.keys(beSessions).forEach((beSessionKey: string) => {
            if (beSessionKey.startsWith(beSessionScope) === true) {
                delete beSessions[beSessionKey];
            }
        });
        this.setAllBeSessions(beSessions);
    }

    /**
     * 清空所有会话
     */
    public clear(): void {
        window.sessionStorage.removeItem(this.sessionStorageKey);
    }

    /**
     * 从SessionStorage中获取全部BeSessions
     */
    private getAllBeSessions(): any {
        const beSessionsJson = window.sessionStorage.getItem(this.sessionStorageKey);
        if (!beSessionsJson) {
            return {};
        }
        return JSON.parse(beSessionsJson);
    }

    /**
     * 设置全部BeSessions到SessionStorage
     */
    setAllBeSessions(beSessions: any): void {
        const beSessionsString = JSON.stringify(beSessions);
        window.sessionStorage.setItem(this.sessionStorageKey, beSessionsString);
    }
}

export { SessionStorageStrategy };
