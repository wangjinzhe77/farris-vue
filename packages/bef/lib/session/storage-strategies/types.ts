/**
 * 缓存策略
 */
interface StorageStrategy {
    getItem(key: string): string;
    setItem(key: string, value: string): void;
    removeItem(key: string): void;
    removeItemsByScope(scope: string): void;
    clear(): void;
}

export { StorageStrategy };
