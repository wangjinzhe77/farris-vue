import { RuntimeContext } from '../types';
import { SessionHandlingStrategy } from './handling-strategies/index';

/**
 * BE会话服务
 */
class BefSessionService {

    /**
     * Session处理策略类
     */
    private handlingStrategy: SessionHandlingStrategy;

    /**
     * 构造函数
     */
    constructor(handlingStrategy: SessionHandlingStrategy) {
        this.handlingStrategy = handlingStrategy;
    }

    /**
     * 获取token
     */
    public get token() {
        return this.handlingStrategy.getFrameworkSessionId();
    }

    /**
     * 获取BeSessionId
     */
    public getBeSessionId(runtimeContext?: RuntimeContext): Promise<string> {
        return this.handlingStrategy.getSessionId(runtimeContext);
    }

    /**
     * 设置sessionId
     * @param sessionId sessionId
     */
    public setBeSessionId(sessionId: string, runtimeContext?: RuntimeContext) {
        this.handlingStrategy.setSessionId(sessionId, runtimeContext);
    }

    /**
     * 清空BeSessionId
     */
    public clearBeSessionId(runtimeContext?: RuntimeContext) {
        this.handlingStrategy.clearSessionId(runtimeContext);
    }

    /**
     * 扩展请求header
     */
    public extendRequestHeaders(headers: any, runtimeContext?: RuntimeContext): any {
        return this.handlingStrategy.handleRequestHeaders(headers, runtimeContext);
    }

    /**
     * 处理响应header
     */
    public handleResponseHeaders(headers: any): void {
        return this.handlingStrategy.handleReponseHeaders(headers);
    }
}

export { BefSessionService };
