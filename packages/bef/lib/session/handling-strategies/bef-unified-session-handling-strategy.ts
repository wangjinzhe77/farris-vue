import { HttpClient, HttpHeaders, Injector, Devkit } from '@farris/devkit-vue';
import { SessionHandlingStrategy } from './handling-strategy';
import { SessionStorageStrategy } from '../storage-strategies';
import { FrameworkSessionService } from '../../framework-session.service';
import { RuntimeContext } from '../../types';
import { BefHttpUtil } from '../../utils';

/**
 * 会话复用场景下会话处理策略
 */
export class BefUnifiedSessionHandlingStrategy extends SessionHandlingStrategy {
    /**
     * 构造函数
     */
    constructor(
        storageStrategy: SessionStorageStrategy,
        frameworSessionService: FrameworkSessionService,
        httpClient: HttpClient,
        baseUrl: string,
        protected injector: Injector
    ) {
        super(storageStrategy, frameworSessionService, httpClient, baseUrl, injector);
    }
    /**
     * 获取会话复用的session id
     * @param runtimeContext runtime context
     */
    public getSessionId(runtimeContext: RuntimeContext): Promise<string> {
        const sessionKey = this.getSessionStorageKey(runtimeContext);
        const sessionId = this.storageStrategy.getItem(sessionKey);
        return Promise.resolve(sessionId);
    }
    /**
     * 设置session id
     * @param sessionId session id
     * @param runtimeContext  runtime context
     */
    public setSessionId(sessionId: string, runtimeContext: RuntimeContext): void {
        const sessionKey = this.getSessionStorageKey(runtimeContext);
        this.storageStrategy.setItem(sessionKey, sessionId);
    }
    /**
     * 清空session id
     * @param runtimeContext runtime context
     */
    public clearSessionId(runtimeContext: RuntimeContext): void {
        const sessionKey = this.getSessionStorageKey(runtimeContext);
        // this.lastTimeUsedSessionId = this.getSessionIdFromStorage(runtimeContext);
        this.storageStrategy.removeItem(sessionKey);
    }
    /**
     * 处理请求header
     * @param headers 
     * @param runtimeContext runtime context
     */
    public handleRequestHeaders(headers: HttpHeaders, runtimeContext?: RuntimeContext): HttpHeaders {
        const frmSessionId = this.getFrameworkSessionId();
        const beSessionId = this.getSessionIdFromStorage(runtimeContext);
        const devkit = this.injector.get<Devkit>(Devkit, undefined);
        if (devkit) {
            // 使用应用id作为function instance id
            const funcInstId = '';
            headers = BefHttpUtil.appendFuncInstId(headers, funcInstId);
        }
        headers = BefHttpUtil.appendCafRuntimeCommonVariable(headers, frmSessionId);
        if (beSessionId) {
            headers = BefHttpUtil.appendCafRuntimeContext(headers, beSessionId);
        }
        return headers;
    }
    /**
     * 处理返回header
     * @param headers 
     */
    public handleReponseHeaders(headers: HttpHeaders): void {
    }
    /**
     * 构造session存储的key
     * @param runtimeContext runtime context
     */
    protected getSessionStorageKey(runtimeContext?: RuntimeContext): string {
        let sessionId = this.frameworkSessionId;
        if (runtimeContext) {
            sessionId = this.getFrameworkSessionId();
        }
        const tabId = runtimeContext && runtimeContext.tabId;
        if (tabId) {
            return `${sessionId}_${tabId}_${this.baseUrl}`;
        }
        return `${sessionId}_${this.baseUrl}`;
    }

}