import { HttpHeaders, HttpClient, Injector } from '@farris/devkit-vue';
import { FrameworkSessionService } from '../../framework-session.service';
import { SessionStorageStrategy } from '../storage-strategies/index';
import { RuntimeContext } from '../../types';

/**
 * 会话处理策略类
 */
abstract class SessionHandlingStrategy {

    /**
     * 存储策略
     */
    protected storageStrategy: SessionStorageStrategy;

    /**
     * 框架Session服务
     */
    protected frameworkSessionService: FrameworkSessionService;

    /**
     * Http客户端
     */
    protected httpClient: HttpClient;

    /**
     * 创建Session的的EAPI地址
     */
    protected baseUrl: string;

    /**
     * 创建Session接口地址
     */
    protected createSessionUrl: string;

    /**
     * 关闭Session接口地址
     */
    protected closeSessionUrl: string;
    /**
     * 上次使用的会话id
     */
    protected lastTimeUsedSessionId: string;
    /**
     * injector
     */
    protected injector: Injector;

    /**
     * 构造函数
     */
    constructor(
        storageStrategy: SessionStorageStrategy,
        frameworkSessionService: FrameworkSessionService,
        httpClient: HttpClient,
        baseUrl: string,
        injector: Injector
    ) {
        this.storageStrategy = storageStrategy;
        this.frameworkSessionService = frameworkSessionService;
        this.httpClient = httpClient;
        this.baseUrl = baseUrl;
        this.createSessionUrl = `${baseUrl}/service/createsession`;
        this.closeSessionUrl = `${baseUrl}/service/closesession`;
        this.injector = injector;
    }

    public abstract getSessionId(runtimeContext?: RuntimeContext): Promise<string>;
    public abstract setSessionId(sessionId: string, runtimeContext?: RuntimeContext): void;
    public abstract clearSessionId(runtimeContext?: RuntimeContext): void;
    public abstract handleRequestHeaders(headers: HttpHeaders, runtimeContext?: RuntimeContext): HttpHeaders;
    public abstract handleReponseHeaders(headers: HttpHeaders): void;
    protected abstract getSessionStorageKey(runtimeContext?: RuntimeContext): string;

    /**
     * 获取框架SessionId
     * TODO: 暂不支持runtimeContext
     */
    public getFrameworkSessionId() {
        return this.frameworkSessionId;
    }

    /**
     * 从缓存中获取BeSession
     */
    protected getSessionIdFromStorage(runtimeContext?: RuntimeContext) {
        const sessionStorageKey = this.getSessionStorageKey(runtimeContext);
        const beSessionId = this.storageStrategy.getItem(sessionStorageKey);
        return beSessionId;
    }
    /**
     * 框架SessionId（用户的或者功能菜单的）
     */
    protected get frameworkSessionId(): string {
        return this.frameworkSessionService.getCurrentSessionId();
    }

}

export { SessionHandlingStrategy };
