import { HttpClient, Injector } from '@farris/devkit-vue';
import { FrameworkSessionService } from '../../framework-session.service';
import { SessionStorageStrategy } from '../storage-strategies/index';
import { SessionHandlingStrategy } from './handling-strategy';
import { BefSeparatedSessionHandlingStrategy } from './bef-separated-session-handling-strategy';
import { BefUnifiedSessionHandlingStrategy } from './bef-unified-session-handling-strategy';

/**
 * BeSession处理策略工厂
 */
class BefSessionHandlingStrategyFactory {
    /**
     * 创建BeSession处理策略
     */
    public create(
        handlingStrategyName: string,
        frmSessionService: FrameworkSessionService,
        beBaseUrl: string,
        httpClient: HttpClient,
        injector: Injector
    ): SessionHandlingStrategy {
        const storageStrategy = this.createStorageStrategy();
        if (handlingStrategyName === 'UnifiedSession') {
            return new BefUnifiedSessionHandlingStrategy(storageStrategy, frmSessionService, httpClient, beBaseUrl, injector);
        } else {
            return new BefSeparatedSessionHandlingStrategy(storageStrategy, frmSessionService, httpClient, beBaseUrl, injector);
        }
    }

    /**
     * 创建BeSession缓存策略
     */
    private createStorageStrategy(): SessionStorageStrategy {
        return new SessionStorageStrategy();
    }
}

export { BefSessionHandlingStrategyFactory };
