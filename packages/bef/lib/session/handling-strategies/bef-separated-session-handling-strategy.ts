import { HttpHeaders, HttpClient, HttpRequestConfig, Injector } from '@farris/devkit-vue';
import { BefHttpUtil, BefEnvUtil } from '../../utils/index';
import { FrameworkSessionService } from '../../framework-session.service';
import { SessionStorageStrategy } from '../storage-strategies/index';
import { SessionHandlingStrategy } from './handling-strategy';
import { RuntimeContext } from '../../types';

/**
 * 隔离的BeSession处理策略
 */
class BefSeparatedSessionHandlingStrategy extends SessionHandlingStrategy {
    /**
     * 构造函数
     */
    constructor(
        storageStrategy: SessionStorageStrategy,
        frameworkSessionService: FrameworkSessionService,
        httpClient: HttpClient,
        baseUrl: string,
        injector: Injector
    ) {
        super(storageStrategy, frameworkSessionService, httpClient, baseUrl, injector);
    }

    /**
     * 获取BeSessionId
     */
    public getSessionId(runtimeContext?: RuntimeContext): Promise<string> {
        const beSessionId = this.getSessionIdFromStorage(runtimeContext);
        if (beSessionId) {
            return Promise.resolve(beSessionId)
        }
        const sessionIdPromise = this.closeLastTimeUsedSession().then(() => {
            return this.createSession(runtimeContext);
        });
        return sessionIdPromise;
    }
    /**
     * 创建BeSessionId
     */
    protected createSession(runtimeContext?: RuntimeContext): Promise<string> {
        const requestConfig: HttpRequestConfig = {
            responseType: 'text',
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const frmSessionId = this.frameworkSessionId;
        if (frmSessionId) {
            requestConfig.headers = BefHttpUtil.appendCafRuntimeCommonVariable(requestConfig.headers, this.frameworkSessionId);
        }

        return this.httpClient.post(this.createSessionUrl, null, requestConfig).then((beSessionId: string) => {
            this.setSessionId(beSessionId, runtimeContext);
            return beSessionId;
        });
    }

    /**
     * 关闭上次使用的会话
     */
    protected closeLastTimeUsedSession(): Promise<boolean> {
        if (!this.lastTimeUsedSessionId) {
            return Promise.resolve(true);
        }

        const requestConfig: HttpRequestConfig = {
            responseType: 'text',
            headers: {
                'Content-Type': 'application/json'
            }
        };

        // headers处理
        requestConfig.headers = BefHttpUtil.appendCafRuntimeContext(requestConfig.headers, this.lastTimeUsedSessionId);
        if (this.frameworkSessionId) {
            requestConfig.headers = BefHttpUtil.appendCafRuntimeCommonVariable(requestConfig.headers, this.frameworkSessionId);
        }

        // 无论是否成功，统一置空cleardBeSessionId
        return this.httpClient.post(this.closeSessionUrl, null, requestConfig).then(() => {
            return true;
        });
    }
    /**
     * 设置BeSessionId
     */
    public setSessionId(sessionId: string, runtimeContext?: RuntimeContext): void {
        const sessionKey = this.getSessionStorageKey(runtimeContext);
        this.storageStrategy.setItem(sessionKey, sessionId);
    }

    /**
     * 清空BeSessionId
     */
    public clearSessionId(runtimeContext?: RuntimeContext) {
        if (BefEnvUtil.isInFramework() === true) {
            this.storageStrategy.removeItemsByScope(this.frameworkSessionId);
        } else {
            const sessionKey = this.getSessionStorageKey(runtimeContext);
            this.lastTimeUsedSessionId = this.getSessionIdFromStorage(runtimeContext);
            this.storageStrategy.removeItem(sessionKey);
        }
    }

    /**
     * 扩展BeSessionId相关头信息
     */
    public handleRequestHeaders(headers: HttpHeaders, runtimeContext?: RuntimeContext): HttpHeaders {
        const frmSessionId = this.getFrameworkSessionId();
        const beSessionId = this.getSessionIdFromStorage(runtimeContext);

        if (frmSessionId) {
            headers = BefHttpUtil.appendCafRuntimeCommonVariable(headers, frmSessionId);
        }

        if (beSessionId) {
            headers = BefHttpUtil.appendCafRuntimeContext(headers, beSessionId);
            headers = BefHttpUtil.appendSessionId(headers, beSessionId);
        }

        return headers;
    }

    /**
     * 处理服务器端返回的headers
     */
    public handleReponseHeaders(headers: any): void {
    }

    /**
     * 获取某个Repository对应的BeSession的唯一key
     */
    protected getSessionStorageKey(runtimeContext?: RuntimeContext): string {
        let sessionId: string = this.frameworkSessionId;
        if (runtimeContext) {
            sessionId = this.getFrameworkSessionId();
        }
        const tabId = runtimeContext && runtimeContext.tabId;
        if (tabId) {
            return `${sessionId}_${tabId}_${this.baseUrl}`;
        }
        return `${sessionId}_${this.baseUrl}`;
    }
}

export { BefSeparatedSessionHandlingStrategy };