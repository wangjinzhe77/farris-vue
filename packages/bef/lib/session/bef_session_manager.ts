import { RuntimeContext } from '../types';
import { BefSessionService } from './bef_session_service';

export class BefSessionManager {
    private static history: string[] = [];
    public static getSessionId(moduleId: string, sessionService: BefSessionService, beBaseUri: string, runtimeContext?: RuntimeContext): Promise<any> {
      const key = `${moduleId}_${beBaseUri}`;
      const createSessionIsInvoked = this.history.includes(key);
      if (createSessionIsInvoked) {
        return Promise.resolve(null);
      } else {
        this.history.push(key);
        return sessionService.getBeSessionId(runtimeContext);
      }
    }
  }