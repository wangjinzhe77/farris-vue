import { Entity, Repository, ViewModel, ViewModelState } from "@farris/devkit-vue";
import { BefRepository } from "./bef-repository";

export abstract class BefTreeRepository<T extends Entity> {
    protected repository: BefRepository<T>;
    constructor(protected viewModel: ViewModel<ViewModelState>) {
        this.repository =this.viewModel.repository as BefRepository<T>;
    }
    /**
     * 新增同级
     * @param id 
     */
    public abstract addSibling(id: string): Promise<T>;
    /**
     * 新增子级
     * @param parentId 
     */
    public abstract addChild(parentId: string): Promise<T>;
}
