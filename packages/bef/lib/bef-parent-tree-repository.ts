import { Entity, ViewModel, ViewModelState } from "@farris/devkit-vue";
import { BefTreeRepository } from "./bef-tree-repository";

export class BefParentTreeRepository<T extends Entity> extends BefTreeRepository<T> {
    constructor(protected viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }
    /**
     * 父节点分级方式新增同级
     * @param id 
     * @returns 
     */
    public addSibling(id: string): Promise<T> {
        const apiProxy = this.repository.apiProxy;
        return apiProxy.parentHierarchyCreateSibling(id).then((returnValue: any) => {
            const entity = this.repository.buildEntity(returnValue);
            this.repository.entityStore.appendEntities([entity]);
            return entity;
        });
    }
    /**
     * 父节点分级方式新增子级
     * @param parentId 
     * @returns 
     */
    public addChild(parentId: string): Promise<T> {
        const apiProxy = this.repository.apiProxy;
        return apiProxy.parentHierarchyCreateChildLayer(parentId).then((returnValue: any) => {
            const entity = this.repository.buildEntity(returnValue);
            this.repository.entityStore.appendEntities([entity]);
            return entity;
        });
    }
}