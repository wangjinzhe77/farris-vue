import { Entity, EntityFieldSchema, EntitySchema, EntityState, EntityStore, FieldType } from '@farris/devkit-vue';
import { BefRepository } from "../bef-repository";
import { ChangeDetail, ChangeDetailType } from './types';

/**
 * 应用后端变更
 */
class BefChangeDetailHandler {

    /**
     * 实体仓库
     */
    private repository: BefRepository<Entity>;

    /**
     * 实体状态
     */
    private entityStore: EntityStore<EntityState<Entity>>;


    /**
     * 构造函数
     */
    constructor(repository: BefRepository<Entity>) {
        this.repository = repository;
        this.entityStore = repository.getEntityStore();
    }

    /**
     * 构造后端变更集合
     */
    public handle(changeDetails: ChangeDetail[]): void {
        this.handleChangeDetails(changeDetails);
    }
    private handleChangeDetails(changeDetails: ChangeDetail[], parentPaths: string[] = []) {
        changeDetails.forEach((changeDetail: ChangeDetail) => {
            const id = (changeDetail.ChangeInfo.dataId || changeDetail.ChangeInfo.DataId) as string;
            const currentPaths = parentPaths.concat(`[${id}]`);
            const path = '/' + currentPaths.join('/');
            const entity = this.entityStore.getEntityByPath(path);
            if (!entity || entity.idValue !== id) {
                return;
            }
            this.handleChangeDetail(entity, changeDetail, currentPaths);
        });
    }
    private handleChangeDetail(entity: Entity, changeDetail: ChangeDetail, parentPaths: string[]) {
        if (!changeDetail) {
            return;
        }
        // 处理修改类型的变更
        if (changeDetail.ChangeType !== ChangeDetailType.Modify) {
            return;
        }
        const changeInfo = changeDetail.ChangeInfo;
        Object.keys(changeInfo).forEach((propName: string) => {
            if (propName.toLowerCase() === 'dataid') {
                return;
            }

            const entitySchema = entity.getSchema();
            const fieldSchema = entitySchema.getFieldSchemaByName(propName);
            const currentPaths = parentPaths.concat([propName]);
            const path = '/' + currentPaths.join('/');
            if (fieldSchema?.type === FieldType.Primitive) {
                const value: any = changeInfo[propName];
                this.entityStore.setValueByPath(path, value);
            } else if (fieldSchema?.type === FieldType.Entity) {
                const entityFieldSchema = fieldSchema as EntityFieldSchema;
                const primaryKey = entityFieldSchema.entitySchema.getIdKey();
                if (primaryKey) {
                    // 关联字段
                    const value: any = changeInfo[propName];
                    this.entityStore.updateEntityByPath(path, value);
                } else {
                    // udt字段
                    const currentEntity = this.entityStore.getEntityByPath(path);
                    if (!currentEntity) {
                        return;
                    }
                    const changeDetail = changeInfo[propName] as ChangeDetail;
                    this.handleChangeDetail(currentEntity, changeDetail, currentPaths);
                }

            } else if (fieldSchema?.type === FieldType.EntityList) {
                const childChangeDetails = changeInfo[propName] as ChangeDetail[];
                this.handleChangeDetails(childChangeDetails, currentPaths);
            }
        });
    }
}

export { BefChangeDetailHandler };