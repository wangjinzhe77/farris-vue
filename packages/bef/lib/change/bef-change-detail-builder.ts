import { FieldType, Entity, EntityList, EntityState, EntityStore, EntityChange, ChangeValueChange, EntityPathNodeType, EntityFieldSchema } from '@farris/devkit-vue';
import { BefRepository } from "../bef-repository";
import { ChangeDetailType, ChangeDetail, ChangeDetailInfo } from './types';

/**
 * 变更构造器
 */
class BefChangeDetailBuilder {

    /**
     * 实体状态
     */
    private entityStore: EntityStore<EntityState<Entity>>;


    /**
     * 构造函数
     */
    constructor(repository: BefRepository<Entity>) {
        this.entityStore = repository.getEntityStore();
    }

    /**
     * 构造后端变更集合
     */
    public build(): ChangeDetail[] {
        const rootChengeDetails = [];
        const rootEntities = this.entityStore.getEntities();
        const entityChanges = this.entityStore.entityChangeHistory.getMergedChanges();
        entityChanges.forEach((entityChange) => {
            this.buildChangeDetail(rootChengeDetails, rootEntities, entityChange);
        });

        return rootChengeDetails;
    }

    /**
     * 构造后端变更
     * id/prop
     * id/udt/prop
     * id/ref/ref_value
     * id/ref/ref_udt/prop
     * id/udt/udt_prop/prop
     */
    public buildChangeDetail(rootChangeDetails: ChangeDetail[], rootEntities: Entity[], entityChange: EntityChange) {
        let currentChangeDetails: ChangeDetail[] | null = rootChangeDetails;
        let currentEntities: Entity[] | null = rootEntities;
        let currentChangeDetail: ChangeDetail | null;
        let currentEntity: Entity | null;
        let parentChangeDetail: ChangeDetailInfo | null | { [propertyName: string]: any; };

        const pathNodes = entityChange.path.getNodes();
        pathNodes.forEach((pathNode) => {
            const pathNodeType = pathNode.getNodeType();
            const pathNodeValue = pathNode.getNodeValue();

            if (pathNodeType === EntityPathNodeType.IdValue) {
                if (!Array.isArray(currentChangeDetails) || !Array.isArray(currentEntities)) {
                    throw new Error('currentChangeDetails and currentEntities must be array');
                }

                const entityId = pathNodeValue;
                currentEntity = currentEntities.find((entity) => {
                    return entity.idValue === entityId;
                }) || null;
                if (!currentEntity) {
                    throw new Error(`Entity(id=${entityId}) can not be found`);
                }

                currentChangeDetail = currentChangeDetails.find((changeDetail) => {
                    return changeDetail.ChangeInfo.DataId === entityId;
                }) || null;
                if (!currentChangeDetail) {
                    currentChangeDetail = this.createEmptyChangeDetail(entityId);
                    currentChangeDetails.push(currentChangeDetail);
                }
                parentChangeDetail = currentChangeDetail.ChangeInfo;
            } else {
                if (!currentEntity || !currentChangeDetail) {
                    throw new Error('currentEntity and currentChangeDetail must be array');
                }

                const currentEntitySchema = currentEntity.getSchema();
                const fieldName = pathNodeValue;
                const fieldSchema = currentEntitySchema.getFieldSchemaByName(fieldName);
                const fieldValue = currentEntity[fieldName];
                if (!fieldSchema) {
                    throw new Error(`Field(name=${fieldName}) can not be found`);
                }
                if (fieldSchema.type === FieldType.EntityList) {
                    let childChangeDetails = currentChangeDetail.ChangeInfo[fieldName] as ChangeDetail[];
                    if (!childChangeDetails) {
                        childChangeDetails = [];
                    }
                    currentChangeDetail.ChangeInfo[fieldName] = childChangeDetails;

                    const childEntityList = currentEntity[fieldName] as EntityList<Entity>;
                    const childEntities = childEntityList.getEntities();

                    currentChangeDetails = childChangeDetails;
                    currentEntities = childEntities;
                    currentEntity = null;
                    currentChangeDetail = null;

                } else if (fieldSchema.type === FieldType.Entity) {
                    // Entity场景包含UDT及关联，需要分开处理
                    const entityFieldSchema = fieldSchema as EntityFieldSchema;
                    const idKey = entityFieldSchema.entitySchema.getIdKey();
                    if (!parentChangeDetail) {
                        throw new Error('parent change detail can not be null');
                    }
                    if (idKey) {
                        let associatedChangeDetail = parentChangeDetail && parentChangeDetail[fieldName];
                        if (!associatedChangeDetail) {
                            associatedChangeDetail = fieldValue ? fieldValue.toJSON() : {};
                            parentChangeDetail[fieldName] = associatedChangeDetail;
                        }
                        parentChangeDetail = associatedChangeDetail as { [propertyName: string]: any; };
                    } else {
                        let unifiedChangeDetail = parentChangeDetail && parentChangeDetail[fieldName];
                        if (!unifiedChangeDetail) {
                            unifiedChangeDetail = this.createEmptyChangeDetail();
                            parentChangeDetail[fieldName] = unifiedChangeDetail;
                        }
                        parentChangeDetail = unifiedChangeDetail.ChangeInfo ? unifiedChangeDetail.ChangeInfo : unifiedChangeDetail;
                    }

                    currentChangeDetails = null;
                    currentEntities = null;
                    currentEntity = fieldValue as Entity;
                } else {
                    if (!parentChangeDetail) {
                        throw new Error(`Field(name=${fieldName}) can not be found`);
                    }
                    parentChangeDetail[fieldName] = (entityChange as ChangeValueChange).newValue;

                    currentChangeDetails = null;
                    currentEntities = null;
                    currentChangeDetail = null;
                    currentEntity = null;
                    parentChangeDetail = null;
                }
            }

        });
    }

    /**
     * 创建空的ChangeDetail
     */
    private createEmptyChangeDetail(dataId?: string): ChangeDetail {
        const changeDetail: ChangeDetail = {
            ChangeType: ChangeDetailType.Modify,
            ChangeInfo: {}
        };
        if (dataId) {
            changeDetail.ChangeInfo.DataId = dataId;
        }
        return changeDetail;
    }
}


export { BefChangeDetailBuilder };