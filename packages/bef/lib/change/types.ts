/* eslint-disable no-use-before-define */

/**
 * 变更类型
 */
export class ChangeDetailType {

    /**
     * 修改
     */
    static Modify = 'Modify';

}

/**
 * 变更信息
 * 必须包含：
 * 1、DataId   => 主键值；
 * 2、属性名   => 新的属性值；
 * 3、子表名+s => 子表行的ChangeDetail数组
 */
export interface ChangeDetailInfo {
    DataId?: string;
    [key: string]: number | string | boolean | null | undefined | ChangeDetail | ChangeDetail[] | { [key: string]: any };
}

/**
 * 变更详情
 */
export interface ChangeDetail {
    ChangeType: ChangeDetailType;
    ChangeInfo: ChangeDetailInfo;
}