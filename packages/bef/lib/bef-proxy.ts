import { HttpParams, HttpMethod, HttpMethods, HttpRequestConfig, HttpClient } from '@farris/devkit-vue';
import { ContentType, ResponseInfo } from './types';
import { IProxyExtend } from './bef-proxy-extend';
import { ChangeDetail } from './change/index';

/**
 * Bef API代理类
 */
export class BefProxy {

    /**
     * 基路径
     */
    public baseUrl: string;

    /**
     * 代理扩展
     */
    protected proxyExtend: IProxyExtend;

    /**
     * 构造函数
     */
    constructor(public httpClient: HttpClient) {
    }

    /**
     * 初始化
     */
    public init(baseUrl: string) {
        this.baseUrl = baseUrl;
    }

    /**
     * 设置扩展策略
     */
    public setProxyExtend(proxyExtend: IProxyExtend) {
        this.proxyExtend = proxyExtend;
    }

    /**
     * 列表数据查询（扩展）
     */
    public extendQuery(entityFilter: any): Promise<ResponseInfo> {
        const url = `${this.baseUrl}/extension/query`;
        const params: HttpParams = {};
        if (entityFilter) {
            const entityFilterString = JSON.stringify(entityFilter);
            params.entityFilter = entityFilterString;
        }
        const bodyWithRequestInfo = this.proxyExtend.extendBody({});
        const requestConfig: HttpRequestConfig = {
            params,
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 查询数据
     * @param entityFilter 过滤、排序、分页信息
     * @returns 
     * @description 和extendQuery类似，区别在于将查询参数放到body中
     */
    public filter(entityFilter: any): Promise<ResponseInfo> {
        let url = `${this.baseUrl}/extension/filter`;
        const body: any = {};
        if (entityFilter) {
            body.entityFilter = entityFilter;
        }
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.POST, url, requestConfig);
    }
    /**
     * 单条数据检索（扩展）
     */
    public extendRetrieve(id: string): Promise<ResponseInfo> {
        const url = `${this.baseUrl}/extension/retrieve/${encodeURIComponent(id)}`;
        const bodyWithRequestInfo = this.proxyExtend.extendBody({});
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 加锁编辑数据
     * @param id 
     * @returns 
     */
    public edit(id: string): Promise<ResponseInfo> {
        const url = `${this.baseUrl}/service/edit/${encodeURIComponent(id)}`;
        const bodyWithRequestInfo = this.proxyExtend.extendBody({});
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 新增数据
     */
    public create(defaultValue?: any): Promise<ResponseInfo> {
        const body = {
            defaultValue
        };
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.POST, this.baseUrl, requestConfig);
    }
    /**
     * 批量新增主表数据
     * @param defaultValues 字段默认值，keyvalue格式
     * @returns 
     */
    public batchCreate(defaultValues: Array<any>): Promise<ResponseInfo> {
        const url = `${this.baseUrl}/batch`;
        const body: any = {};
        if (defaultValues && defaultValues.length > 0) {
            body.retrieveDefaultParam = {
                defaultValues
            };
        }
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.POST, url, requestConfig);
    }
    /**
     * 新增从表或从从表数据
     * @param fpath 实体路径
     * @param defaultValues 默认值，keyvalue格式
     * @returns 
     */
    public createByPath(fpath: string, defaultValues?: Array<any>): Promise<ResponseInfo> {
        const pathUrl = this.convertPathToUrl(fpath);
        const url = `${this.baseUrl}${pathUrl}`;
        const body: any = {};
        if (defaultValues && defaultValues.length > 0) {
            body.retrieveDefaultParam = {
                defaultValues
            };
        }
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.POST, url, requestConfig);
    }
    /**
     * 批量新增从表或从从表数据
     * @param path 
     * @param defaultValues 
     * @returns 
     */
    public batchCreateByPath(path: string, defaultValues: Array<any>): Promise<ResponseInfo> {
        const pathUrl = this.convertPathToUrl(path);
        const url = `${this.baseUrl}${pathUrl}/batch`;
        const body: any = {};
        if (defaultValues && defaultValues.length > 0) {
            body.retrieveDefaultParam = {
                defaultValues
            };
        }
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.POST, url, requestConfig);
    }
    public update(changeDetails: ChangeDetail[]): Promise<ResponseInfo> {
        const body = {
            changeDetails
        };
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PATCH, this.baseUrl, requestConfig);
    }
    /**
     * 保存
     */
    public save(): Promise<any> {
        const bodyWithRequestInfo = this.proxyExtend.extendBody({});
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, this.baseUrl, requestConfig);
    }

    /**
     * 删除
     */
    public extendDelete(id: string): Promise<any> {
        const url = `${this.baseUrl}/extension/delete/${id}`;
        const bodyWithRequestInfo = this.proxyExtend.extendBody({});
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 删除从表或从从表数据
     * @param fpath 实体路径
     * @param id 实体id
     * @returns 
     */
    public extendDeletByPath(fpath: string, id: string): Promise<any> {
        const pathUrl = this.convertPathToUrl(fpath);
        const url = `${this.baseUrl}/extension${pathUrl}/${encodeURIComponent(id)}`;
        const bodyWithRequestInfo = this.proxyExtend.extendBody({});
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 批量删除从表或从从表数据
     * @param fPath 实体路径
     * @param ids 要删除的实体id
     * @returns 
     */
    public extendBatchDeleteByPath(fPath: string, ids: string[]): Promise<any> {
        const pathUrl = this.convertPathToUrl(fPath);
        const pathArray = pathUrl.split('/');
        if (pathArray.length < 3) {
            throw Error(`根据path删除实体数据出错了。传入的path[${fPath}]格式不对`);
        }
        const url = `${this.baseUrl}/extension${pathUrl}/batch`;
        const body = {
            ids
        };
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 删除并保存
     */
    public deleteAndSave(id: any): Promise<any> {
        const url = `${this.baseUrl}/service/delete/${id}`;
        const bodyWithRequestInfo = this.proxyExtend.extendBody({});
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 批量删除并保存
     * @param ids 
     * @returns 
     */
    public batchDeleteAndSave(ids: string[]) {
        const url = `${this.baseUrl}/batchdeleteandsave`;
        const body = {
            ids
        };
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo,
        };
        return this.request(HttpMethods.POST, url, requestConfig);
    }
    /**
     * 是否有未保存变更
     * @returns 
     */
    public hasChanges(): Promise<any> {
        const url = `${this.baseUrl}/haschanges`;
        const bodyWithRequestInfo = this.proxyExtend.extendBody({});
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }

    /**
     * 批量删除实体
     * @param ids 实体id数组
     * @returns 
     */
    public extensionBatchDeletion(ids: string[]): Promise<any> {
        const url = `${this.baseUrl}/extension/batchdeletion`;
        const body = {
            ids
        };
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 取消
     */
    public cancel(): Promise<any> {
        const url = `${this.baseUrl}/service/cancel`;
        const requestConfig: HttpRequestConfig = {
            headers: { 'Content-Type': 'application/json' }
        };
        return this.request(HttpMethods.POST, url, requestConfig);
    }
    /**
     * 父节点分级方式新增同级
     * @param id 
     * @returns 
     */
    public parentHierarchyCreateSibling(id: string) {
        const url = `${this.baseUrl}/service/parenthierarchycreatesibling`;
        const body = {
            dataId: id
        };
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 父节点分级方式新增子级
     * @param id 
     * @returns 
     */
    public parentHierarchyCreateChildLayer(id: string) {
        const url = `${this.baseUrl}/service/parenthierarchycreatechildlayer`;
        const body = {
            dataId: id
        };
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 子对象父节点分级方式新增同级
     * @param ids 数据id顺序列表
     * @param nodes 节点编号列表
     * @returns 
     */
    public childNodeParentHierarchyCreateSibling(ids: string[], nodes: string[]) {
        const url = `${this.baseUrl}/service/childnodeparenthierarchycreatesibling`;
        const body = {
            ids,
            nodes
        };
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 子对象父节点分级方式新增子级
     * @param ids 数据id顺序列表
     * @param nodes 节点编号列表
     * @returns 
     */
    public childNodeParentHierarchyCreateChildLayer(ids: string[], nodes: string[]) {
        const url = `${this.baseUrl}/service/childnodeparenthierarchycreatechildlayer`;
        const body = {
            ids,
            nodes
        };
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 分级码分级方式新增同级
     * @param id 
     * @returns 
     */
    public pathHierarchyCreateSibling(id: string) {
        const url = `${this.baseUrl}/service/pathhierarchycreatesibling`;
        const body = {
            dataId: id
        };
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 分级码分级方式新增子级
     * @param id 
     * @returns 
     */
    public pathHierarchyCreateChildLayer(id: string) {
        const url = `${this.baseUrl}/service/pathhierarchycreatechildlayer`;
        const body = {
            dataId: id
        };
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 子对象分级码分级方式新增同级
     * @param ids 数据id顺序列表
     * @param nodes 节点编号列表
     * @returns 
     */
    public childNodePathHierarchyCreateSibling(ids: string[], nodes: string[]) {
        const url = `${this.baseUrl}/service/childnodepathhierarchycreatesibling`;
        const body = {
            ids,
            nodes
        };
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 子对象分级码分级方式新增子级
     * @param ids 数据id顺序列表
     * @param nodes 节点编号列表
     * @returns 
     */
    public childNodePathHierarchyCreateChildLayer(ids: string[], nodes: string[]) {
        const url = `${this.baseUrl}/service/childnodepathhierarchycreatechildlayer`;
        const body = {
            ids,
            nodes
        };
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 查询全部树信息
     * @param id 
     * @param virtualPropertyName 
     * @param fullTreeType 
     * @param loadType 
     * @param filter 
     * @returns 
     */
    public parentIDFullTreeQuery(id: string, virtualPropertyName: string, fullTreeType: string, loadType: string, filter: any[]) {
        const url = `${this.baseUrl}/service/parentidfulltreequery`;
        const entityFilter = this.buildEntityFilter(filter, [], 0, 0);
        const body = {
            dataId: id,
            isUsePagination: false,
            virtualPropertyName: virtualPropertyName,
            pagination: {},
            fullTreeType,
            loadType,
            filter: entityFilter,
        };
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 获取帮助数据
     * @param labelId 
     * @param nodeCode 
     * @param queryParam 
     * @returns 
     */
    public elementhelps(labelId: string, nodeCode: string, queryParam: any): Promise<any> {
        const url = `${this.baseUrl}/extension/elementhelps`;
        const body = {
            labelId,
            nodeCode,
            queryParam
        };
        const bodyWithRequestInfo = this.proxyExtend.extendBody(body);
        const requestConfig: HttpRequestConfig = {
            body: bodyWithRequestInfo
        };
        return this.request(HttpMethods.PUT, url, requestConfig);
    }
    /**
     * 发送请求
     */
    public request(method: HttpMethod, url: string, requestConfigs: HttpRequestConfig = {}, ignoreHandlingChanges = false, selfHandError: boolean = false, ignoreError: boolean = false): Promise<any> {
        this.setContentType(requestConfigs);
        // 扩展headers
        const resultPromise = this.proxyExtend.extendHeaders(requestConfigs.headers).then((headers: any) => {
            requestConfigs.headers = headers;
            requestConfigs.observe = 'response';
            return this.httpClient.request(method, url, requestConfigs).then((result: any) => {
                if (ignoreHandlingChanges === true) {
                    return result && result.body && result.body.returnValue ? result.body.returnValue : result;
                }
                return this.proxyExtend.onResponse(result);
            }).catch((error: any) => {
                return this.proxyExtend.onError(error, selfHandError, ignoreError);
            });
        });

        return resultPromise;
    }
    private setContentType(requestConfigs: HttpRequestConfig = {}) {
        // 开发以及配置过content-type
        if (requestConfigs.headers != null && (requestConfigs.headers['Content-Type'] != null || requestConfigs.headers['content-type'] != null)) {
            return;
        }
        const detectedContentType = this.detectContentType(requestConfigs);
        switch (detectedContentType) {
            case ContentType.NONE:
                break;
            case ContentType.JSON:
                requestConfigs.headers = Object.assign({}, requestConfigs.headers, { 'Content-Type': 'application/json' });
                break;
            case ContentType.FORM:
                requestConfigs.headers = Object.assign({}, requestConfigs.headers, { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' });
                break;
            case ContentType.TEXT:
                requestConfigs.headers = Object.assign({}, requestConfigs.headers, { 'Content-Type': 'text/plain' });
                break;
            case ContentType.BLOB:
                const blob = requestConfigs.body as Blob;
                if (blob && blob.type) {
                    requestConfigs.headers = Object.assign({}, requestConfigs.headers, { 'Content-Type': blob.type });
                }
                break;
        }
    }
    private detectContentType(requestConfigs: HttpRequestConfig = {}): ContentType {
        const body = requestConfigs.body || null;
        if (body == null) {
            return ContentType.NONE;
        }
        else if (body instanceof URLSearchParams) {
            return ContentType.FORM;
        }
        else if (body instanceof FormData) {
            return ContentType.FORM_DATA;
        }
        else if (body instanceof Blob) {
            return ContentType.BLOB;
        }
        else if (body instanceof ArrayBuffer) {
            return ContentType.ARRAY_BUFFER;
        }
        else if (body && typeof body === 'object') {
            return ContentType.JSON;
        }
        else {
            return ContentType.TEXT;
        }
    }
    /**
     * 将路径转换为url
     * @param path 
     * @returns 
     */
    private convertPathToUrl(path: string): string {
        const paths = path.split('/').filter((p) => p);
        for (let index = paths.length - 1; index >= 0; index--) {
            if (index % 2 === 0) {
                paths[index] = encodeURIComponent(paths[index]);
            }
            if (paths[index] && paths[index].endsWith('s') && index % 2 !== 0) {
                paths[index] = paths[index].substring(0, paths[index].length - 1).toLowerCase();
            }
        }
        return '/' + paths.join('/');
    }
    private buildEntityFilter(filter: any[], sort: any[], pageSize: number, pageIndex: number) {
        if (!filter && !sort && !pageSize && !pageIndex) {
            return null;
        }
        if (!filter) {
            filter = [];
        }
        if (!sort) {
            sort = [];
        }
        if (filter && filter.length > 0) {
            filter[filter.length - 1].Relation = 0;
        }
        const entityFilter = {
            FilterConditions: filter,
            SortConditions: sort,
            IsUsePagination: pageSize === 0 ? false : true,
            Pagination: {
                PageIndex: pageIndex,
                PageSize: pageSize,
                PageCount: 0,
                TotalCount: 0
            }
        };
        return entityFilter;
    }
}
