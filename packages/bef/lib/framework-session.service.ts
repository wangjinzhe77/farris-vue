/**
 * 框架Session服务
 */
class FrameworkSessionService {
    private rtfService: any;
    private frameworkService: any = null;
    /**
     * 构造函数
     */
    constructor() {
        this.rtfService = this.getRuntimeFrameworkService();
    }

    /**
     * 获取当前会话id
     */
    getCurrentSessionId(): string {
        // TODO: 存在页签未激活获取token错误的问题
        return this.formToken || 'default';
    }
    public get tabId() {
        return this.params && this.params['tabId'] || null;
    }
    /**
     * 获取formToken
     */
    public get formToken() {
        return this.params && this.params['cvft'] || null;
    }
    public get params() {
        // if (this.rtfService && this.rtfService.hasOwnProperty('session') && typeof this.rtfService['session']['getCommonVariable'] === 'function') {
        //     return this.rtfService['session']['getCommonVariable']();
        // }
        // return null;
        // 从url中获取
        const hash = window.location.hash;
        const params = this.parseQueryString(hash);
        return params;
    }
    private getRuntimeFrameworkService() {
        const frameworkService = this.gspFrameworkService;
        return frameworkService && frameworkService.rtf || {};
    }
    private get gspFrameworkService() {
        if (this.frameworkService) {
            return this.frameworkService;
        }
        let env: Window = window;
        while (!env['gspframeworkService'] && env !== window.top && this.isSameOrigin(env)) {
            env = env.parent;
        }
        this.frameworkService = env['gspframeworkService'];
        return this.frameworkService;
    }
    private isSameOrigin(environment: Window) {
        const host = window.location.host;
        try {
            if (environment && environment.location && typeof environment.location.host !== 'undefined') {
                return environment.location.host === host;
            }
        } catch (e) {
            return false;
        }

        return false;
    }
    private parseQueryString(queryString: string): { [propName: string]: any } {
        if (!queryString) {
            return {};
        }
        const hashes = queryString.slice(queryString.indexOf('?') + 1).split('&');
        return hashes.reduce((params, hash) => {
            const split = hash.indexOf('=');
            const key = hash.slice(0, split);
            const val = hash.slice(split + 1);
            return Object.assign(params, { [key]: decodeURIComponent(val) });
        }, {});
    }
}

export { FrameworkSessionService };
