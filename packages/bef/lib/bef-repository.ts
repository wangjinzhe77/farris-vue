import {
    Type, Injector, HttpClient, Entity, RepositoryConfig, Repository, createEntitiesBySchema, EntityListFieldSchema, Module
} from '@farris/devkit-vue';
import { FrameworkSessionService } from './framework-session.service';
import { BefSessionHandlingStrategyFactory, BefSessionService } from './session/index';
import { BefProxy } from './bef-proxy';
import { BefProxyExtend } from './bef-proxy-extend';

/**
 * Bef实体仓库
 */
class BefRepository<T extends Entity> extends Repository<T> {
    /**
     * Bef代理
     */
    public apiProxyType: Type<BefProxy>;

    /**
     * 注入器
     */
    private injector: Injector;

    /**
     * Bef会话管理
     */
    public sessionService: BefSessionService;

    /**
     * API代理
     */
    public apiProxy: BefProxy;

    /**
     * 分页信息
     */
    public paginationInfo: any = null;

    /**
     * 构造函数
     */
    constructor(module: Module) {
        super(module);
        this.injector = module.getInjector();
    }

    /**
     * 初始化
     */
    public init(config: RepositoryConfig) {
        super.init(config);
        this.initApiProxy(config);
        this.initSessionService();
        this.initPaginationInfo();
    }

    /**
     * 初始化Proxy
     */
    private initApiProxy(config: RepositoryConfig) {
        if (config.isDynamic) {
            this.apiProxy = this.injector.get(BefProxy);
            this.apiProxy.init(config.baseUrl);
            
        } else {
            this.apiProxy = this.injector.get(config.apiProxyType);
        }
        const apiProxyExtend = new BefProxyExtend(this);
        this.apiProxy.setProxyExtend(apiProxyExtend);
    }

    /**
     * 初始化SessionService
     */
    private initSessionService() {
        const handlingStrategyName = 'SeparatedSession';
        const frmSessionService = new FrameworkSessionService();
        const httpClient = this.injector.get<HttpClient>(HttpClient);
        const baseUrl = `${this.apiProxy.baseUrl}`;
        const sessionHandlingStrategyFactory = new BefSessionHandlingStrategyFactory();
        const sessionHandlingStrategy = sessionHandlingStrategyFactory.create(handlingStrategyName, frmSessionService, baseUrl, httpClient, this.injector);
        this.sessionService = new BefSessionService(sessionHandlingStrategy);
    }
    /**
     * 初始化分页信息
     */
    private initPaginationInfo() {
        const pagination = this.paginationInfo || {};
        this.entityStore.setEntityPagination(pagination);
    }

    /**
     * 查询实体
     */
    public getEntities(filters: any[], sorts: any[], pageSize?: number, pageIndex?: number): Promise<T[]> {
        const currentPagination = this.entityStore.getPaginationByPath('/');
        if (currentPagination) {
            pageIndex = typeof pageSize === 'number' ? pageIndex : (currentPagination.pageIndex || 1);
            pageSize = typeof pageSize === 'number' ? pageSize : (currentPagination.pageSize || 0);
        }
        const entityFilter = this.buildEntityFilter(filters, sorts, pageSize, pageIndex);
        const queryPromise = this.apiProxy.filter(entityFilter).then((returnValue: any) => {
            const entityDatas = returnValue.result;
            const entities = this.buildEntites(entityDatas);
            // 更新分页信息
            const serverPagination = returnValue.pagination;
            if (serverPagination) {
                this.entityStore.setPaginationByPath('/', serverPagination);
            }
            return entities;
        });
        return queryPromise;
    }
    /**
     * 获取实体
     */
    public getEntityById(id: string): Promise<T> {
        const retrievePromise = this.apiProxy.extendRetrieve(id).then((returnValue: any) => {
            const entityData = returnValue || {};
            const entity = this.buildEntity(entityData);
            return entity;
        });

        return retrievePromise;
    }
    /**
     * 加锁编辑数据
     * @param id 数据id
     * @returns 
     */
    public editEntityById(id: string): Promise<T | null> {
        const entity = this.entityStore.getEntityById(id) as T;
        if (!entity) {
            return Promise.resolve(null);
        }
        const editPromise = this.apiProxy.edit(id).then((returnValue: any) => {
            const entityData = returnValue.data;
            entity.loadData(entityData);
            return entity;
        });
        return editPromise;
    }
    /**
     * 创建新实体
     */
    public createEntity(defaultValue?: any): Promise<T> {
        const createPromise = this.apiProxy.create(defaultValue).then((returnValue: any) => {
            const entityData = returnValue;
            const entity = this.buildEntity(entityData);
            return entity;
        });
        return createPromise;
    }
    /**
     * 批量创建实体
     * @param defaultValues 实体字段默认值，keyvalue格式
     * @returns 实体数组
     */
    public createEntities(defaultValues: Array<any>): Promise<T[]> {
        const createEntitiesPromise = this.apiProxy.batchCreate(defaultValues).then((returnValue: any) => {
            const entityDatas = returnValue || [];
            const entities = this.buildEntites(entityDatas);
            return entities;
        });
        return createEntitiesPromise;
    }
    /**
     * 创建从表或从从表实体
     * @param path 实体路径
     * @param defaultValues 默认值，keyvalue格式
     * @returns 
     */
    public createEntityByPath(path: string, defaultValues?: Array<any>): Promise<T> {
        const createEntityByPathPromise = this.apiProxy.createByPath(path, defaultValues).then((returnValue: any) => {
            const entityData = returnValue;
            const paths = path.split('/').filter((item) => item).filter((item, index) => index % 2 !== 0);
            const entityPath = this.entityStore.createPath(paths);
            const entityListFieldSchema = this.entityStore.getEntitySchema().getFieldSchemaByPath(entityPath) as EntityListFieldSchema;
            const entitySchema = entityListFieldSchema.entitySchema;
            const entities = createEntitiesBySchema(entitySchema, [entityData]);
            // this.entityState.appendEntitesByPath(entityPath, entities);
            return entities.pop() as T;
        });
        return createEntityByPathPromise;
    }
    /**
     * 批量创建从表或从从表实体
     * @param path 实体路径，如/1/educations
     * @param defaultValues 默认值，keyvalue格式
     * @returns 实体数组
     */
    public createEntitiesByPath(path: string, defaultValues: Array<any>): Promise<T[]> {
        const createEntitiesByPathPromise = this.apiProxy.batchCreateByPath(path, defaultValues).then((returnValue: any) => {
            const entityDatas = returnValue || [];
            const paths = path.split('/').filter((item) => item).filter((item, index) => index % 2 !== 0);
            const entityPath = this.entityStore.createPath(paths);
            const entityListFieldSchema = this.entityStore.getEntitySchema().getFieldSchemaByPath(entityPath) as EntityListFieldSchema;
            const entitySchema = entityListFieldSchema.entitySchema;
            return createEntitiesBySchema(entitySchema, entityDatas) as T[];
        });
        return createEntitiesByPathPromise;
    }

    /**
     * 删除实体
     */
    public removeEntityById(id: string): Promise<boolean> {
        const resultPromsie = this.apiProxy.extendDelete(id).then(() => {
            return true;
        });

        return resultPromsie;
    }
    /**
     * 批量删除仓库数据
     * @param ids 实体id数组
     * @returns 
     */
    public removeEntitiesByIds(ids: string[]): Promise<boolean> {
        const removeEntitiesByIdsPromise = this.apiProxy.extensionBatchDeletion(ids).then((returnValue: any) => {
            return true;
        });
        return removeEntitiesByIdsPromise;
    }
    /**
     * 删除从表或从从表实体
     * @param fpath 实体路径
     * @param id 实体id
     * @returns 
     */
    public removeEntityByPath(fpath: string, id: string): Promise<boolean> {
        const removeEntityByPathPromise = this.apiProxy.extendDeletByPath(fpath, id).then((returnValue: any) => {
            return true;
        });
        return removeEntityByPathPromise;
    }
    /**
     * 批量删除从表或从从表数据
     * @param fPath 实体路径
     * @param ids 实体id数组
     * @returns 
     */
    public removeEntitiesByPath(fPath: string, ids: string[]): Promise<boolean> {
        const removeEntitiesByPathPromise = this.apiProxy.extendBatchDeleteByPath(fPath, ids).then((returnValue: any) => {
            return true;
        });
        return removeEntitiesByPathPromise;
    }
    /**
     * 删除实体
     */
    public removeAndSaveEntityById(id: string): Promise<boolean> {
        const resultPromsie = this.apiProxy.deleteAndSave(id).then(() => {
            return true;
        });

        return resultPromsie;
    }
    /**
     * 批量删除并保存
     * @param ids 
     * @returns 
     */
    public removeEntitiesAndSave(ids: string[]){
        const removePromise = this.apiProxy.batchDeleteAndSave(ids).then(()=>{
            return true;
        });
        return removePromise;
    }
    /**
     * 保存实体变更
     */
    public saveEntityChanges(): Promise<boolean> {
        const savePromise = this.apiProxy.save().then(() => {
            this.entityStore.entityChangeHistory.commitChanges();
            return true;
        });

        return savePromise;
    }

    /**
     * 撤销实体变更
     */
    public cancelEntityChanges(): Promise<boolean> {
        const resultPromise = this.apiProxy.cancel().then(() => {
            this.entityStore.entityChangeHistory.cancelChanges();
            return true;
        });

        return resultPromise;
    }
    /**
     * 是否有未保存的变更
     * @returns 
     */
    public hasChanges() {
        const hasChangesPromise = this.apiProxy.hasChanges().then((returnValue: any) => {
            return returnValue;
        });
        return hasChangesPromise;
    }
    /**
     * 构造EntityFilter
     */
    private buildEntityFilter(filter: any[], sort: any[], pageSize?: number, pageIndex?: number) {
        if (!pageIndex) {
            pageIndex = 1;
        }
        if (!pageSize) {
            pageSize = 0;
        }
        const entityFilter = {
            FilterConditions: filter,
            SortConditions: sort,
            IsUsePagination: pageSize !== 0,
            Pagination: {
                PageIndex: pageIndex,
                PageSize: pageSize,
                PageCount: 0,
                TotalCount: 0
            }
        };

        return entityFilter;
    }

}

export { BefRepository };
