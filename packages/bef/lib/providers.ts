import { StaticProvider, HttpClient, Repository, getDynamicRepositoryToken, ViewModel } from '@farris/devkit-vue';
import { BefParentTreeRepository } from './bef-parent-tree-repository';
import { BefProxy } from './bef-proxy';
import { BefPathTreeRepository } from './bef-path-tree-repository';

const befProviders: StaticProvider[] = [
    { provide: BefPathTreeRepository, useClass: BefPathTreeRepository, deps: [ViewModel] },
    { provide: BefParentTreeRepository, useClass: BefParentTreeRepository, deps: [ViewModel] }
];

const befRootProviders: StaticProvider[] = [
    { provide: BefProxy, useClass: BefProxy, deps: [HttpClient] },
];

export { befProviders, befRootProviders };