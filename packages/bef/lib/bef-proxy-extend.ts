/* eslint-disable no-prototype-builtins */
import { HttpHeaders, Entity, ExceptionHandler, EXCEPTION_HANDLER_TOKEN, Devkit } from '@farris/devkit-vue';
import { ResponseInfo } from './types';
import { RequestInfoUtil, ResponseInfoUtil } from './utils';
import { BefRepository } from './bef-repository';
import { FrameworkSessionService } from './framework-session.service';
import { BefSessionManager } from './session';

/**
 * 代理扩展
 */
export interface IProxyExtend {
    extendBody(body: any): any;
    extendHeaders(headers: any): Promise<any>;
    onResponse(response: ResponseInfo, ignoreChanges?: boolean);
    onError(error: any, selfHandError: boolean, ignoreError: boolean): Promise<any>;
}

/**
 * Bef代理扩展类
 */
export class BefProxyExtend implements IProxyExtend {
    private frameworkSessionService: FrameworkSessionService;
    /**
     * 构造函数
     */
    constructor(private repository: BefRepository<Entity>) {
        this.frameworkSessionService = new FrameworkSessionService();
    }
    /**
     * 返回结果处理
     */
    public onResponse(response: any): ResponseInfo {
        this.handleResponseHeaders(response.headers);
        this.handleEntityChanges(response.body);
        return this.handleResponseBody(response.body);
    }

    /**
     * 处理服务器端返回的headers数据
     */
    private handleResponseHeaders(headers: any): void {
        this.repository.sessionService.handleResponseHeaders(headers);
    }
    private handleEntityChanges(responseInfo: ResponseInfo) {
        const dataChanges = responseInfo && responseInfo.innerDataChange;
        if (!dataChanges || !Array.isArray(dataChanges) || dataChanges.length < 1) {
            return;
        }
        ResponseInfoUtil.handleEntityChanges(this.repository, dataChanges);
    }
    /**
     * 处理服务器端返回的的body数据
     */
    private handleResponseBody(responseInfo: ResponseInfo): ResponseInfo {
        // 重置变更状态
        const entityStore = this.repository.entityStore;
        const entityChangeHistory = entityStore.entityChangeHistory;
        entityChangeHistory.stageChanges();

        if (responseInfo && responseInfo.hasOwnProperty('returnValue')) {
            return responseInfo.returnValue;
        }
        return responseInfo;
    }

    /**
     * 错误处理
     */
    public onError(error: any, selfHandError: boolean, ignoreError: boolean): Promise<any> {
        if (selfHandError) {
            return Promise.reject(error);
        }
        if (ignoreError) {
            return Promise.resolve();
        }
        // 处理公共异常
        const injector = this.repository.module.getInjector();
        if (!injector) {
            return Promise.reject(error);
        }
        const exceptionHandler = injector.get<ExceptionHandler>(EXCEPTION_HANDLER_TOKEN);
        if (!exceptionHandler) {
            return Promise.reject(error);
        }
        exceptionHandler.handle(error);
        return Promise.reject(error);

    }

    /**
     * 扩展Headers
     */
    public extendHeaders(headers: HttpHeaders): Promise<any> {
        const tabId = this.frameworkSessionService.tabId;
        const runtimeContext: any = {};
        if (tabId) {
            runtimeContext.tabId = tabId;
        }
        const beBaseUri = this.repository.apiProxy.baseUrl;
        const moduleId = this.repository.module.getId();

        const sessionIdPromise = BefSessionManager.getSessionId(moduleId, this.repository.sessionService, beBaseUri, runtimeContext).then(() => {
            headers = this.repository.sessionService.extendRequestHeaders(headers, runtimeContext);
            return headers;
        });

        return sessionIdPromise;
    }

    /**
     * 扩展Body
     */
    public extendBody(body: any): any {
        return RequestInfoUtil.appendRequestInfoToBody(body, this.repository);
    }
}
