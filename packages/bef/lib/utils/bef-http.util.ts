import { HttpHeaders, HttpUtil } from '@farris/devkit-vue';

/**
 * BefHttp工具类
 */
class BefHttpUtil {

    /**
     * 追加SessionId头
     */
    public static appendSessionId(headers: HttpHeaders, sessionId: string): any {
        headers = HttpUtil.appendHeader(headers, 'SessionId', sessionId);
        return headers;
    }

    /**
     * 追加CommonVariable头
     * @summary
     * 框架会话token，等价于原来的SessionId
     */
    public static appendCafRuntimeCommonVariable(headers: any, commonVariable: string): any {
        headers = HttpUtil.appendHeader(headers, 'X-CAF-Runtime-CommonVariable', commonVariable);
        return headers;
    }

    /**
     * 追加X-CAF-Runtime-Context头
     * @summary
     * X-CAF-Runtime-Context等价于BeSessionId
     */
    public static appendCafRuntimeContext(headers: any, context: string): any {
        headers = HttpUtil.appendHeader(headers, 'X-CAF-Runtime-Context', context);
        return headers;
    }

    /**
     * 追加Content-Type头
     * @summary
     * 提交内容的MIME类型，默认为application/json
     */
    public static appendContextType(headers: any, contentType?: string): any {
        contentType = contentType || 'application/json';
        headers = HttpUtil.appendHeader(headers, 'Content-Type', contentType);
        return headers;
    }
    /**
     * 追加Func-Inst-Id
     * @param headers 
     * @param funcInstId 
     * @returns 
     */
    public static appendFuncInstId(headers: any, funcInstId: string) {
        return headers.append('Func-Inst-Id', funcInstId);
    }

}

export { BefHttpUtil };
