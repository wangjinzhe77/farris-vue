/**
 * Bef环境监测工具类
 */
class BefEnvUtil {

    /**
     * 是否在框架内运行
     */
    public static isInFramework(): boolean {
        const hashString = window.location.hash;
        if (!hashString) {
            return false;
        }
        return hashString.indexOf('formToken=') !== -1;
    }
}

export { BefEnvUtil };
