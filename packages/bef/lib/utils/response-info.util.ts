/* eslint-disable prefer-destructuring */
/* eslint-disable no-prototype-builtins */
import { Entity } from '@farris/devkit-vue';
import { BefRepository } from '../bef-repository';
import { BefChangeDetailHandler, ChangeDetail } from '../change';
class ResponseInfoUtil {

    /**
     * 将ResponseInfo转换为老接口的数据返回格式
     */
    public static unWrapResponseInfo(result: any) {

        // 兼容cancel没有返回值的情况
        if (!result) {
            return result;
        }

        // 没有returnValue的情况下，兼容query取数的的格式
        if (result.hasOwnProperty('returnValue') === false) {
            if (result.hasOwnProperty('result') && result.hasOwnProperty('pagination')) {

                // 兼容返回带分页的列表数据
                return result.result;
            }
            return result;
        }

        // 其他：返回RequestInfo.returnValue的情况
        const returnValue = result.returnValue;
        if (returnValue && returnValue.hasOwnProperty('result') && returnValue.hasOwnProperty('pagination')) {

            // 兼容返回带分页的列表数据
            return returnValue.result;
        }
        return result.returnValue;

    }
    public static handleEntityChanges(repository: BefRepository<Entity>, changeDetails: ChangeDetail[]) {
        const changeDetailHandler = new BefChangeDetailHandler(repository);
        changeDetailHandler.handle(changeDetails);
    }
}

export { ResponseInfoUtil };
