import { Entity } from '@farris/devkit-vue';
import { RequestInfo } from '../types';
import { BefRepository } from '../bef-repository';
import { BefChangeDetailBuilder } from '../change/index';

class RequestInfoUtil {

    /**
     * 构造RequestInfo
     */
    public static buildRequestInfo(repository: BefRepository<Entity>): RequestInfo {
        const changeDetailBuilder = new BefChangeDetailBuilder(repository);
        const changeDetails = changeDetailBuilder.build();
        const requestInfo: RequestInfo = {
            dataChange: changeDetails,
            variableChange: null
        };

        return requestInfo;
    }

    /**
     * 向body中添加RequestInfo
     */
    public static appendRequestInfoToBody(body: any, repository: BefRepository<Entity>): any {
        if (body.requestInfo) {
            return body;
        }
        const requestInfo = this.buildRequestInfo(repository);

        // body不存在时，body=requestInfo
        if (!body || Object.keys(body).length === 0) {
            return requestInfo;
        }

        const bodyWithRequestInfo = Object.assign({}, body, { requestInfo });
        return bodyWithRequestInfo;
    }
}

export { RequestInfoUtil };
