/* eslint-disable no-use-before-define */

import { ChangeDetail } from './change/index';
/**
 * 请求类型
 */
export interface RequestInfo {
    dataChange: ChangeDetail[];
    variableChange?: ChangeDetail | null;
}

/**
 * 分页信息
 */
export interface Pagination {
    pageSize: number;
    totalCount: number;
    pageCount: number;
    pageIndex: number;
}

/**
 * 查询结果
 */
export interface QueryResult {
    result: any[];
    pagination: Pagination;
}

/**
 * 返回结果类型
 */
export interface ResponseInfo {
    returnValue: any;
    message: any[];
    innerDataChange: ChangeDetail[];
    innerVariableChange: ChangeDetail;
    viewRule?: any;
}

/**
 * 带RequestInfo的body对象
 */
export interface BodyWithRequestInfo {
    requestInfo: RequestInfo;
    [key: string]: any;
}

/**
 * request option格式
 */
export interface RequestOption {
    body: BodyWithRequestInfo;
    [key: string]: any;
}

/**
 *  异常处理信息接口
 */
export interface IErrorServe {
    show(error: any): any;
}
/**
 * 运行上下文
 */
export interface RuntimeContext {
    tabId: string;
}

export enum ContentType {
    NONE = 0,
    JSON = 1,
    FORM = 2,
    FORM_DATA = 3,
    TEXT = 4,
    BLOB = 5,
    ARRAY_BUFFER = 6
}