import typescript from 'rollup-plugin-typescript2';
import vue from 'rollup-plugin-vue';
import resolve from "@rollup/plugin-node-resolve";
import commonjs from '@rollup/plugin-commonjs';
import babel from '@rollup/plugin-babel';
import {
    visualizer
} from "rollup-plugin-visualizer";
import terser from "@rollup/plugin-terser";
import json from '@rollup/plugin-json';
import { createRequire } from 'node:module';

const require = createRequire(import.meta.url);
const { name } = require('./package.json');

const packageName = name.split('/')[1];

export default {
    input: 'lib/index.ts',
    external: ['@farris/devkit-vue', "lodash", "vue", "tslib"],
    output: [{
        file: `dist-rollup/@farris/bef-vue.js`,
        format: 'system',
        exports: 'auto',
        banner: `/*! Last Update Time: ${new Date().toLocaleString()} */`
    }],
    plugins: [
        json(),
        vue(),
        resolve({
            extensions: ['.js', '.ts', '.vue'],
            preferBuiltins: true
        }),
        commonjs({
            include: 'node_modules/**'
        }),
        typescript({
            tsconfigOverride: {
                compilerOptions: {
                    module: 'esnext',
                    target: 'es5',
                    useDefineForClassFields: false
                }
            },
            exclude: ['./src/**/*']
        }),
        babel({
            presets: [
                [
                    "@babel/preset-env",
                    {
                        loose: true
                    }
                ]
            ],
            babelHelpers: 'bundled',
        }),
        visualizer({
            template: "sunburst",
            filename: process.cwd() + `/dist-rollup/@farris/bef-vue.html`,
        }),
        terser({
            keep_fnames: true,
            format: {
                comments: /^!/
            }
        })
    ],
};
