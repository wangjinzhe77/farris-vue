// / <reference types="vitest" />
import path from 'path';
import { defineConfig } from 'vite';
import type { InlineConfig } from 'vitest';
import type { UserConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';

interface VitestConfigExport extends UserConfig {
    test: InlineConfig;
}

export default defineConfig({
    plugins: [vue(), vueJsx()],
    test: {
        globals: true,
        environment: 'happy-dom',
        include: ['**/*.test.tsx']
    },
    resolve: {
        alias: {
            "@farris/devkit-vue": path.resolve(__dirname, "../devkit/lib/index"),
        }
    },
    server: {
        proxy: {
            "/api": {
                target: "http://127.0.0.1:5200",
                changeOrigin: true,
                secure: false
            }

        }
    }
} as VitestConfigExport);
