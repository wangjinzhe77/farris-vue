import { ViewModelState, ViewModel, Entity, EntityState, EntityStore, UIState, UIStore } from '@farris/devkit-vue';
import { StaticBefRepository } from '../static-repository';

/**
 * 基础数据服务
 */
class BaseDataService {

    /**
     * 视图模型
     */
    protected viewModel: ViewModel<ViewModelState>;

    /**
     * 实体状态仓库
     */
    protected entityStore: EntityStore<EntityState<Entity>>;

    /**
    * UI状态仓库
    */
    protected uiStore: UIStore<UIState>;

    /**
     * 远程实体仓库
     */
    protected repository: StaticBefRepository;

    /**
    * 构造函数
    */
    constructor(viewModel: ViewModel<ViewModelState>) {
        this.viewModel = viewModel;
        this.entityStore = viewModel.entityStore as EntityStore<EntityState<Entity>>;;
        this.uiStore = viewModel.uiStore as UIStore<UIState>;
        this.repository = viewModel.repository as StaticBefRepository;
    }

    /**
     * 获取服务实例
     */
    public getService<T>(token: any, defaultValue?: any): T {
        const injector = this.viewModel.getInjector();
        return injector.get<T>(token, defaultValue);
    }
}

export { BaseDataService };
