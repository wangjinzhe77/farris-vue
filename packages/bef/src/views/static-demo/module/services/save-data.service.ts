import { ViewModelState, ViewModel } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';

/**
 * 数据保存服务
 */
class SaveDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     * 保存
     */
    public save(): Promise<boolean> {
        const savePromise = this.repository.saveEntityChanges().then((result) => {
            return result;
        });

        return savePromise;
    }
}

export { SaveDataService };
