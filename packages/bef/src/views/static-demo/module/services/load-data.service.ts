import { Entity, ViewModelState, ViewModel } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';

/**
 * 数据加载服务
 */
class LoadDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     * 加载数据
     */
    public load(): Promise<Entity[]> {
        const loadPromise = this.repository.getEntities([], []).then((entities) => {
            this.entityStore.loadEntities(entities);
            return entities;
        });

        return loadPromise;
    }
}

export { LoadDataService };
