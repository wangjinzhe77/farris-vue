import { Injector, HttpClient, Module, FdModule } from '@farris/devkit-vue';
import { StaticBefProxy, StaticBefRepository } from './static-repository';
import { StaticEntityStore } from './static-entity-store';
import { StaticUIStore } from './static-ui-store';
import { StaticViewModel } from './static-viewmodel';

@FdModule({
    id: 'static-module',
    deps: [Injector],
    providers: [
        { provide: StaticBefProxy, useClass: StaticBefProxy, deps: [ HttpClient ] }
    ],

    entityStores: [StaticEntityStore],
    uiStores: [ StaticUIStore ],
    repositorys: [StaticBefRepository],

    viewModels: [StaticViewModel]
})
export class StaticModule extends Module {
}
