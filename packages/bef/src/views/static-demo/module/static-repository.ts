 
import { FdRepository, Module } from '@farris/devkit-vue';
import { BefProxy, BefRepository } from '../../../../lib/index';
import { StaticEntity } from './static-entity-store';

/**
 * 取数接口代理
 */
class StaticBefProxy extends BefProxy {

    /**
     * 基路径
     */
    public baseUrl: string = '/api/jiwt/jiwtsimplemodule/v1.0/jiwtsimplelistcard_mfrm';
}

/**
 * 静态远程实体仓库
 */
@FdRepository({
    id: 'static-repository',
    deps: [Module, ],
    entityStore: 'static-entity-store',
    apiProxyType: StaticBefProxy
})
class StaticBefRepository extends BefRepository<StaticEntity> {

    /**
     * 构造函数
     */
    constructor(module: Module) {
        super(module);
    }
}

export { StaticBefProxy, StaticBefRepository };
