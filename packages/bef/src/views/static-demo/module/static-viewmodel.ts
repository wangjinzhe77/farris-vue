 
import { Injector, FdViewModel, FdGetter, FdAction, FdCommandAction, ViewModelState, ViewModel } from '@farris/devkit-vue';
import { serviceProviders } from './services/index';
import { LoadHandler, SaveHandler, TestHandler  } from './handlers/index';

class StaticViewModelState extends ViewModelState {
}

@FdViewModel({
    id: 'static-viewmodel',
    deps: [ Injector ],
    providers: [ ...serviceProviders ],

    entityStore: 'static-entity-store',
    uiStore: 'static-ui-store',
    repository: 'static-repository',

    commandHandlers: [ LoadHandler, SaveHandler, TestHandler ]
})
class StaticViewModel extends ViewModel<StaticViewModelState>  {

    /**
     * 构造函数
     */
    constructor(injector: Injector) {
        super(injector);
    }

    @FdGetter()
    public get totalEntityCount() {
        return this.state.entityState?.entities.length;
    }

    @FdAction()
    public sort() {
        console.log(this.state.uiState);
    }

    @FdCommandAction({
        name: 'load',
        params: []
    })
    public load() {};

    @FdCommandAction({
        name: 'save',
        params: []
    })
    public save() {};

    @FdCommandAction({
        name: 'test',
        params: []
    })
    public test() {};
}

export { StaticViewModelState, StaticViewModel };
