import { CommandContext, FdCommandHandler, CommandHandler } from '@farris/devkit-vue';
import { SaveDataService } from '../services/index';

/**
 * SaveHandler定义
 */
@FdCommandHandler({
    commandName: 'save',
    deps: [SaveDataService]
})
class SaveHandler extends CommandHandler {

    /**
     * 对应命令名称
     */
    public commandName = 'save';

    /**
     * 构造函数
     */
    constructor(private saveDataService: SaveDataService) {
        super();
    }

    /**
     * 执行方法
     */
    public execute(context: CommandContext) {
        this.saveDataService.save();
    }
}

export { SaveHandler };
