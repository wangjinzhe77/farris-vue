import { CommandContext, FdCommandHandler, CommandHandler } from '@farris/devkit-vue';
import { LoadDataService } from '../services/index';

/**
 * LoadHandler定义
 */
@FdCommandHandler({
    commandName: 'load',
    deps: [LoadDataService]
})
class LoadHandler extends CommandHandler {

    /**
     * 对应命令名称
     */
    public commandName = 'load';

    /**
     * 构造函数
     */
    constructor(private loadDataService: LoadDataService) {
        super();
    }

    /**
     * 执行方法
     */
    public execute(context: CommandContext) {
        return this.loadDataService.load();
    }
}

export { LoadHandler };
