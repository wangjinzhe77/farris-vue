import { ViewModelState, ViewModel } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';

/**
 * 数据保存服务
 */
class TestService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     * 测试方法
     */
    public test() {
        const oldName = this.viewModel.entityStore?.getValueByPath('/name').split('-')[0];
        const random = Math.round(Math.random() * 10000);
        const newName = oldName + '-' + random;
        this.viewModel.entityStore?.setValueByPath('/name', newName);
    }
}

export { TestService };
