import { createApp } from 'vue';
import { createDevkit } from '@farris/devkit-vue';
import App from './app.vue';

const app = createApp(App);
const devkit = createDevkit({
    providers: []
});
app.use(devkit);

app.mount('#app');
