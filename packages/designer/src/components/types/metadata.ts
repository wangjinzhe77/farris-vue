import { FormComponent, FormExpression, FormStateMachine, FormWebCmd } from "./basic";
import { DesignViewModel } from "./design-viewmodel";
import { FormSchema, FormSchemaEntityField } from "./entity-schema";
import { FormViewModel, FormViewModelField } from "./view-model";

export interface FormOptions {
    /**
     * 启用静态文本
     */
    enableTextArea?: boolean;

    publishFormProcess?: boolean;

    /**
     * 界面渲染模式：编译（生成代码并编译），动态渲染（动态解析，本地不生成代码，不编译）
     */
    renderMode?: 'compile' | 'dynamic';

    /** 变更集提交策略 */
    changeSetPolicy?: 'entire' | 'valid';

    /** 启用服务器端变更检测：菜单或应用关闭前调用后端接口确认后端缓存中的数据是否已经保存并提示用户 */
    enableServerSideChangeDetection?: boolean;

    /** 生成表单代码时将之前的源码都删除 */
    enableDeleteSourceCode?: boolean;

    /** 表单是否可以被组合 */
    canBeComposed?: boolean;

    /** 表单是否启用业务流 */
    enableAif?: boolean;

    /** 表单是否启用数据类型转换 */
    paramTypeTransform?: boolean;
}

export interface FormMetaDataModule {
    id: string;

    code: string;

    name: string;

    caption: string;

    type: string;

    creator: string;

    creationDate: Date;

    updateVersion: string;

    // 实体
    entity: Array<FormSchema>;

    // 状态机
    stateMachines: Array<FormStateMachine>;

    // 视图模型
    viewmodels: Array<FormViewModel>;

    // 源组件-事件-命令-目标组件的映射关系
    actions: Array<any>;

    // 组件
    components: Array<FormComponent>;

    // 构件
    webcmds: Array<FormWebCmd>;

    serviceRefs: Array<any>;

    // 表单所属模板
    templateId: string;

    // 表单模板对应的拖拽控制规则
    templateRule?: string;

    // 是否组合表单
    isComposedFrm: boolean;

    // 表单所在的工程名
    projectName: string;

    // 自定义样式
    customClass: any;

    // 外部模块声明
    extraImports: Array<{ name: string; path: string }>;

    /** 表达式配置 */
    expressions: FormExpression[];

    // 当前表单的展示形式：modal|page|sidebar
    showType?: string;

    // 页面级按钮配置（目前用于Header和ModalFooter组件内部的工具栏按钮）
    toolbar: {
        items: { [viewModelId: string]: any };
        configs: { modal?: any; page?: any; sidebar?: any };
    };

    qdpInfo: any;

    /** 表单元数据id */
    metadataId?: string;

}

export interface FormMetadaDataDom {

    module: FormMetaDataModule;

    options?: FormOptions;
}

export interface MetadataDto {
    id: string;

    nameSpace: string;

    code: string;

    name: string;

    fileName: string;

    type: string;

    bizobjectID: string;

    relativePath: string;

    extendProperty: string;

    content: string;

    extendable: boolean;

    properties: { framework: string, [propsName: string]: any }

    nameLanguage: any;
}
export interface UseFormMetadata {
    /** 查询表单元数据 */
    queryMetadata: () => Promise<FormMetadaDataDom>;
    /** 保存表单元数据 */
    saveFormMetadata: () => Promise<any>;
    /** 查询表单模板的拖拽控制规则 */
    queryFormTemplateRule: (module: any) => Promise<void>;
    /** 发布表单 */
    publishFormMetadata: () => Promise<{ result: boolean, error?: string }>;
    /** 部署表单 */
    deployFrontFile: (metadataId: string, path: string) => Promise<any>
}
export interface UseFormSchema {

    /** 表单元数据基础信息（外层结构） */
    getFormMetadataBasicInfo: () => MetadataDto;

    setFormMetadataBasicInfo: (newMetadata: MetadataDto) => void;

    /** 获取表单元数据 */
    getFormSchema: () => FormMetadaDataDom;

    getSchemas: () => FormSchema | undefined;

    setFormSchema: (newSchema: FormMetadaDataDom) => void;

    /** 根据Comonent id 获取组件节点*/
    getComponentById: (targetComponentId: string) => FormComponent | undefined;

    /** 根据viewmodel id 获取组件节点*/
    getComponentByViewModelId: (targetViewModelId: string) => FormComponent | undefined;

    /** 获取页面模型版本 */
    getUpdateVersion: () => string;

    /** 根据viewmodel id获取模型节点 */
    getViewModelById: (targetViewModelId: string) => FormViewModel | undefined;

    /**
     * 根据指定条件获取元数据的节点
     * @param rootNode 根节点
     * @param predict 预设的判断逻辑
     */
    selectNode: (rootNode: any, predict: (item: any) => boolean) => any;

    /**
     * 根据指定条件获取元数据的节点以及其父节点
     * @param rootNode 根节点
     * @param predict 预设的判断逻辑
     * @param parentNode 初始父节点
     */

    selectNodeAndParentNode: (rootNode: any, predict: (item: any) => boolean, parentNode: any) => { node: any; parentNode: any } | undefined;

    getControlBasicInfoMap: () => Map<string, { showName: string, parentPathName: string }>;
    /**
     * 返回命令
     * @returns 
     */
    getCommands: () => FormWebCmd[];
    setCommands: (value: Array<FormWebCmd>) => void;
    getViewModels: () => FormViewModel[];
    getComponents: () => FormComponent[];
    setViewmodels: (value: any) => void;
    getModule: () => FormMetaDataModule;
    getViewModelIdByComponentId: (componentId: string) => string;
    deleteViewModelById: (componentId: string) => void;
    addViewModelField: (viewModelId, filedObject: FormViewModelField) => void;
    deleteViewModelFieldById: (viewModelId: string, fieldId: string) => void;
    clearViewModelFieldSchema: (viewModelId, fieldId) => void;
    modifyViewModelFieldById: (viewModelId, fieldId, changeObject, isMerge: boolean) => void;
    getControlClassByFormUnifiedLayout: (controlClass: string, componentId: string, formNode: any) => string;
    setFormTemplateRule: (rules: any) => void;
    getFormTemplateRule: () => any;
    getLocaleVariablesByViewModelId: (viewModelId: string) => any;
    getRemoteVariables: () => any;
    getFieldsByViewModelId: (viewModelId: string) => FormSchemaEntityField[] | undefined;
    getExpressions: () => FormExpression[];
    setExpressions: (value: FormExpression[]) => void;
    deleteComponent: (componentId: string) => void;
    getControlsInCmpWidthBinding: (viewModelId: string, fieldId: string) => any;

}

export interface UseSchemaService {
    convertViewObjectToEntitySchema: (viewObjectId: string, sessionId: string) => Promise<FormSchema | undefined>;
    getFieldByIDAndVMID: (id: string, viewModelId: string) => {
        schemaField: FormSchemaEntityField, isRefElement: boolean, refElementLabelPath: string
    } | undefined;
    getTableInfoByViewModelId(viewModelId: string): { id: string, code: string, name: string, label: string, type } | undefined;
    getFieldsByViewModelId: (viewModelId: string) => FormSchemaEntityField[];
}
export interface UseDesignViewModel {
    assembleDesignViewModel: () => void;
    getAllFields2TreeByVMId: (viewModelId: string) => any[];
    getDgViewModel: (viewModelId: string) => DesignViewModel | null;
    deleteViewModelById: (viewModelId: string) => void;
    getDgViewModels: () => any[]
}
export interface UseControlCreator {
    setFormFieldProperty: (field: FormSchemaEntityField, editorType: string, controlClass: string) => any;
    setGridFieldProperty: (gridType: string, field: FormSchemaEntityField, metadata: any, needInlineEditor: false) => any;
    createFormGroupWithoutField: (editorType: string, controlClass: string) => any;
}

export interface UseFormStateMachine {
    /** 请求获取状态机元数据 */
    queryStateMachineMetadata: () => void;
    /** 获取状态机元数据 */
    getStateMachineMetadata: () => any;
    /** 获取状态机元数据中的可视化状态 */
    getRenderStates: (value: any) => any[];
}
