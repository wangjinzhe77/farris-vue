import { MetadataService } from "../../metadata.service";
import { SchemaItem, SchemaRepositoryPagination, SchemaCategory } from "@farris/ui-vue/components";
import controllCategories from './categories';
import { getAllSupportedControllers } from "../../command/supported-controller";

export class ControllerSelectorSchemaService {
    private metadataType = '.webcmd';
    constructor(private metadataService: MetadataService) {
    }

    public getNavigationData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaCategory[] {
        return controllCategories;
    }

    public getRecentlyData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return [];
    }

    public getRecommandData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return [];
    }

    private metadata2SchemaItem(metadata: any[], category: string): SchemaItem[] {
        const supportedControllers = getAllSupportedControllers();
        return metadata.filter((metadataItem) => {
            // 移除移动控制器
            if (metadataItem.nameSpace.includes('Inspur.GS.Gsp.Mobile')) {
                return false;
            }
            // 支持自定义构件
            if (metadataItem.nameSpace.includes('.Front')) {
                return true;
            }
            // 移除暂不支持的内置控制器
            if (!supportedControllers[metadataItem.id]) {
                return false;
            }
            return {
                id: metadataItem.id,
                name: metadataItem.name,
                code: metadataItem.code,
                nameSpace: metadataItem.nameSpace,
                data: metadataItem,
                category
            };
        }

        );
    }

    public getSchemaData = async (searchingText: string, pagination: SchemaRepositoryPagination, editorParams): Promise<SchemaItem[]> => {
        const { relativePath } = editorParams.formBasicInfo;
        const allMetadataRes = await this.metadataService?.getAllMetadataList(relativePath, this.metadataType);
        const items = allMetadataRes.data.metadataIndexItems ? allMetadataRes.data.metadataIndexItems : allMetadataRes.data;
        return this.metadata2SchemaItem(items, 'all');
    };
}
