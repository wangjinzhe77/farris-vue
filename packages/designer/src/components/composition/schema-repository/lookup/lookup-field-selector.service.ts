import { MetadataService } from "../../metadata.service";

export class LookupFieldSelectorService {
    constructor(private metadataService: MetadataService) {
    }

    /**
     * schema字段集合组装成树
     * @param fields schema字段集合
     */
    private assembleFields2Tree(fields: any[]) {
        const treeData: any = [];
        fields.forEach(element => {
            // 关联表字段 / UDT字段
            let children = [];
            if (element.type && element.type.fields && element.type.fields.length > 0) {
                children = this.assembleFields2Tree(element.type.fields);

            }
            // 适配旧的帮助元数据没有bindingPath属性
            if (!element.bindingPath) {
                element.bindingPath = element['parentPath'] ? element['parentPath'] + '.' + element.label : element.label;
            }
            treeData.push({
                data: element,
                children,
                expanded: true,
                selectable: !children.length
            });
        });
        return treeData;
    }

    private buildTreeData(schema: any) {
        if (!schema || !schema.entities || schema.entities.length === 0) {
            return;
        }
        const mainTable = schema.entities[0];
        if (mainTable.type && mainTable.type.fields) {
            return this.assembleFields2Tree(mainTable.type.fields);
        }

    }

    getData(editorParams: any) {
        const { propertyData, formBasicInfo } = editorParams;
        const metadataPath = formBasicInfo?.relativePath;
        if (!metadataPath) {
            return;
        }
        return this.metadataService && this.metadataService.GetRefMetadata(metadataPath, propertyData?.helpId).then((res) => {
            const metadata = JSON.parse(res.data.content);
            return metadata && metadata.schema && metadata.schema.main && this.buildTreeData(metadata.schema.main);
        });
    }

}
