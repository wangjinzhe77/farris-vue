import { SchemaItem, SchemaRepositoryPagination } from "@farris/ui-vue/components";
import { MetadataService } from "../../metadata.service";

export class LookupSchemaService {

    private metadataType = '.hlp';

    constructor(private metadataService: MetadataService) {
    }

    private getLocalMetadata(metadataPath) {
        return this.metadataService?.getMetadataListInSu(metadataPath, this.metadataType);
    }

    private getRecentMetadata(metadataPath) {
        return this.metadataService?.getRecentMetadata(metadataPath, this.metadataType);
    }

    private metadata2SchemaItem(metadata: any[], category: string): SchemaItem[] {
        return metadata.map((metadataItem) => {
            return {
                id: metadataItem.id,
                name: metadataItem.name,
                code: metadataItem.code,
                nameSpace: metadataItem.nameSpace,
                hide: false,
                active: false,
                data: metadataItem,
                category
            };
        });
    }

    getRecommandData = async (searchingText: string, pagination: SchemaRepositoryPagination, editorParams): Promise<SchemaItem[]> => {
        const {relativePath} = editorParams.formBasicInfo;
        const recentRequest = await this.getRecentMetadata(relativePath);
        const recentData = this.metadata2SchemaItem(recentRequest.data, 'recent');

        const suMetadataRequest = await this.getLocalMetadata(relativePath);
        const localData = this.metadata2SchemaItem(suMetadataRequest.data, 'local');

        return recentData.concat(localData);
    };

    getSchemaData = async (searchingText: string, pagination: SchemaRepositoryPagination, editorParams): Promise<SchemaItem[]> => {
        const {relativePath} = editorParams.formBasicInfo;
        const allMetadataRes = await this.metadataService?.getAllMetadataList(relativePath, this.metadataType);
        const items = allMetadataRes.data.metadataIndexItems ? allMetadataRes.data.metadataIndexItems: allMetadataRes.data;
        return this.metadata2SchemaItem(items, 'all');
    };
}
