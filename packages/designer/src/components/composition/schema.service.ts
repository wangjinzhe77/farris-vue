import axios from "axios";
import { FormSchema, FormSchemaEntity, FormSchemaEntityField, FormSchemaEntityField$Type, FormSchemaEntityFieldType$Type, FormSchemaEntityFieldTypeName, UseFormMetadata, UseFormSchema, UseSchemaService } from "../types";
import { MetadataService } from "./metadata.service";
/**
 * 操作表单DOM Schema的工具类
 */
export function useSchemaService(
    metadataService: MetadataService,
    useFormSchema: UseFormSchema
): UseSchemaService {

    function getSchemaEntities(): FormSchemaEntity[] {
        const schema = useFormSchema.getSchemas();
        return schema?.entities || [];
    }

    /**
     * 获取表字段列表
     * @param entities 实体对象集合
     * @param bindTo 实体绑定路径
     */
    function getTableFieldsByBindTo(entities: FormSchemaEntity[], bindTo: string) {
        if (!entities || entities.length === 0) {
            return [];
        }
        const splitIndex = bindTo.indexOf('/');
        if (splitIndex > -1) {
            bindTo = bindTo.slice(splitIndex + 1, bindTo.length);
        }

        for (const entity of entities) {
            const entityType = entity.type;
            if (!entityType) {
                return [];
            }
            if (bindTo === '' || bindTo === entity.code || bindTo === entity.label) {
                return entityType.fields;
            }
            if (entityType.entities && entityType.entities.length > 0) {
                const fields = getTableFieldsByBindTo(entityType.entities, bindTo);
                if (fields) {
                    return fields;
                }
            }
        }
    }

    /**
     * 获取指定id的实体
     * @param entities 实体对象集合
     * @param id 实体id
     */
    function getEntityNodeById(entities: FormSchemaEntity[], id: string) {
        for (const entity of entities) {
            if (id === entity.id) {
                return entity;
            }
            const entityType = entity.type;
            if (!entityType) {
                return {};
            }
            if (entityType.entities && entityType.entities.length > 0) {
                const tagetEntity = getEntityNodeById(entityType.entities, id);
                if (tagetEntity) {
                    return tagetEntity;
                }
            }

        }

    }

    function extractFieldsFromEntityType(entityType: { fields?: FormSchemaEntityField[] }) {
        const fields: FormSchemaEntityField[] = [];
        if (entityType && entityType.fields && entityType.fields.length) {
            entityType.fields.forEach(field => {
                if (field.$type === 'SimpleField') {
                    fields.push(field);
                } else {
                    const extractedFields = extractFieldsFromEntityType(field.type);
                    if (extractedFields.length) {
                        extractedFields.forEach(extractedField => fields.push(extractedField));
                    }
                }
            });
        }
        return fields;
    }

    /**
     * 根据bindTo获取对应表信息
     * @param entities 实体
     * @param bindTo VM绑定
     */
    function _getTableBasicInfoByUri(entities: FormSchemaEntity[], bindTo: string, includeType?: boolean): any {
        if (!entities || entities.length === 0) {
            return;
        }
        const splitIndex = bindTo.indexOf('/');
        if (splitIndex > -1) {
            bindTo = bindTo.slice(splitIndex + 1, bindTo.length);
        }

        for (const entity of entities) {
            if (bindTo === '' || bindTo === entity.code || bindTo === entity.label) {
                const result = {
                    id: entity.id,
                    code: entity.code,
                    name: entity.name,
                    label: entity.label

                };
                if (includeType) {
                    result['type'] = entity.type;
                }
                return result;

            }
            const entityType = entity.type;

            if (entityType && entityType.entities && entityType.entities.length > 0) {
                const basicInfo = _getTableBasicInfoByUri(entityType.entities, bindTo, includeType);
                if (basicInfo) {
                    return basicInfo;
                }
            }
        }

    }

    /**
     * 根据bindTo获取对应表名
     * @param entities 实体对象集合
     * @param bindTo 绑定路径
     */
    function _getTableCodeByUri(entities: FormSchemaEntity[], bindTo: string): string | undefined {
        if (!entities || entities.length === 0) {
            return '';
        }
        const splitIndex = bindTo.indexOf('/');
        if (splitIndex > -1) {
            bindTo = bindTo.slice(splitIndex + 1, bindTo.length);
        }

        for (const entity of entities) {
            if (bindTo === '' || bindTo === entity.code || bindTo === entity.label) {
                return entity.code;
            }
            const entityType = entity.type;

            if (entityType && entityType.entities && entityType.entities.length > 0) {
                const label = _getTableCodeByUri(entityType.entities, bindTo);
                if (label) {
                    return label;
                }
            }
        }
    }

    /**
     * 根据VM id获取对应表名
     * @param viewModelId 实体模型标识
     */
    function getTableCodeByViewModelID(viewModelId) {
        const vm = useFormSchema.getViewModelById(viewModelId);

        const entities = getSchemaEntities();
        if (entities && entities.length > 0) {
            return _getTableCodeByUri(entities, vm?.bindTo || '');
        }
        return '';
    }

    /** entity.typ
     * 根据bindTo获取对应表名
     * @param entities 实体对象集合
     * @param bindTo 绑定路径
     */
    function _getTableLabelByUri(entities: FormSchemaEntity[], bindTo: string): string | undefined {
        if (!entities || entities.length === 0) {
            return;
        }
        const splitIndex = bindTo.indexOf('/');
        if (splitIndex > -1) {
            bindTo = bindTo.slice(splitIndex + 1, bindTo.length);
        }
        for (const entity of entities) {
            if (bindTo === '' || bindTo === entity.code || bindTo === entity.label) {
                return entity.label;
            }
            const entityType = entity.type;

            if (entityType && entityType.entities && entityType.entities.length > 0) {
                const label = _getTableLabelByUri(entityType.entities, bindTo);
                if (label) {
                    return label;
                }
            }
        }
    }

    /**
     * 根据bindTo获取对应表基本信息
     * @param viewModelId 数据模型标识
     */
    function getTableInfoByViewModelId(viewModelId: string): { id: string, code: string, name: string, label: string, type } | undefined {
        const vm = useFormSchema.getViewModelById(viewModelId);
        const entities = getSchemaEntities();
        if (entities && entities.length > 0) {
            return _getTableBasicInfoByUri(entities, vm?.bindTo || '');
        }
    }

    /**
     * 根据bindTo获取对应表信息
     * @param viewModelId 数据模型标识
     */
    function getTableByViewModelId(viewModelId: string): { id: string, code: string, name: string, label: string, type: any } | undefined {
        const vm = useFormSchema.getViewModelById(viewModelId);
        const entities = getSchemaEntities();
        if (entities && entities.length > 0) {
            return _getTableBasicInfoByUri(entities, vm?.bindTo || '', true);
        }
    }

    /**
     * 递归查询字段
     * @param fields 实体字段集合
     * @param refElementLabelPath 字段路径
     * @param id 字段标识
     */
    function getFieldInfoByID(fields: FormSchemaEntityField[], refElementLabelPath = '', id: string): {
        schemaField: FormSchemaEntityField, isRefElement: boolean, refElementLabelPath: string
    } {
        let element;
        let isRefElement = false;
        const parentLabel = refElementLabelPath ? refElementLabelPath + '.' : '';
        for (const field of fields) {
            if (field.id === id) {
                element = field;
                refElementLabelPath = parentLabel + field.label;
                isRefElement = parentLabel ? true : false;
                break;
            } else {
                // 关联字段/UDT字段
                if (field.type && field.type.fields && field.type.fields.length > 0) {
                    const childResult = getFieldInfoByID(field.type.fields, parentLabel + field.label, id);
                    if (childResult.schemaField) {
                        element = childResult.schemaField;
                        // eslint-disable-next-line prefer-destructuring
                        refElementLabelPath = childResult.refElementLabelPath;
                        // eslint-disable-next-line prefer-destructuring
                        isRefElement = childResult.isRefElement;
                        break;
                    }
                }
            }
        }
        return { schemaField: element, isRefElement, refElementLabelPath };
    }

    /**
     * 根据字段ID获取schema字段信息--用户旧表单适配
     */
    function getFieldByID(fieldId: string) {
        const entities = getSchemaEntities();
        if (!entities || entities.length === 0) {
            return;
        }
        const viewModels = useFormSchema.getViewModels();
        for (const viewModel of viewModels) {
            // if (viewModel.bindTo === '/' && !viewModel.parent) {
            //     continue;
            // }
            const fields = getTableFieldsByBindTo(entities, viewModel.bindTo);
            if (!fields) {
                continue;
            }
            const result = getFieldInfoByID(fields, '', fieldId);
            if (result && result.schemaField) {
                return result.schemaField;
            }
        }
    }

    /**
     * 递归查询字段
     * @param fields 实体字段集合
     * @param refElementLabelPath 字段路径
     * @param path 字段标识
     */
    function getFieldInfoByPath(fields: FormSchemaEntityField[], refElementLabelPath = '', path: string): {
        schemaField: FormSchemaEntityField, isRefElement: boolean, refElementLabelPath: string
    } {
        let element;
        let isRefElement = false;
        const parentLabel = refElementLabelPath ? refElementLabelPath + '.' : '';
        for (const field of fields) {
            if (field.path === path) {
                element = field;
                refElementLabelPath = parentLabel + field.label;
                isRefElement = parentLabel ? true : false;
                break;
            } else {
                // 关联字段/UDT字段
                if (field.type && field.type.fields && field.type.fields.length > 0) {
                    const childResult = getFieldInfoByID(field.type.fields, parentLabel + field.label, path);
                    if (childResult.schemaField) {
                        element = childResult.schemaField;
                        // eslint-disable-next-line prefer-destructuring
                        refElementLabelPath = childResult.refElementLabelPath;
                        // eslint-disable-next-line prefer-destructuring
                        isRefElement = childResult.isRefElement;
                        break;
                    }
                }
            }
        }
        return { schemaField: element, isRefElement, refElementLabelPath };
    }

    /**
    * 根据字段ID和ViewModelId获取字段信息（包括关联表字段）
    * 返回对象 {实体，是否关联字段，关联字段的dataField}
    * @param id 字段标识
    * @param viewModelId 视图模型标识
    */
    function getFieldByIDAndVMID(id: string, viewModelId: string): {
        schemaField: FormSchemaEntityField, isRefElement: boolean, refElementLabelPath: string
    } | undefined {
        const entities = getSchemaEntities();
        if (!entities || entities.length === 0) {
            return;
        }
        const vm = useFormSchema.getViewModelById(viewModelId);
        const fields = getTableFieldsByBindTo(entities, vm?.bindTo || '');
        if (!fields) {
            return;
        }
        return getFieldInfoByID(fields, '', id);
    }

    /**
     * 根据字段path和ViewModelId获取字段信息（包括关联表字段）
     * 返回对象 {实体，是否关联字段，关联字段的dataField}
     * @param path 字段标识
     * @param viewModelId 视图模型标识
     */
    function getFieldByPathAndVMID(path: string, viewModelId: string): {
        schemaField: FormSchemaEntityField, isRefElement: boolean, refElementLabelPath: string
    } | undefined {
        const entities = getSchemaEntities();
        if (!entities || entities.length === 0) {
            return;
        }
        const vm = useFormSchema.getViewModelById(viewModelId);
        const fields = getTableFieldsByBindTo(entities, vm?.bindTo || '');
        if (!fields) {
            return;
        }
        return getFieldInfoByPath(fields, '', path);
    }

    function getTableLabelByVMID(viewModelId: string) {
        const entities = getSchemaEntities();
        if (!entities || entities.length === 0) {
            return;
        }
        const vm = useFormSchema.getViewModelById(viewModelId);
        return _getTableLabelByUri(entities, vm?.bindTo || '');
    }


    /**
     *  定位分级码字段（返回第一个类型为HierarchyType的字段）
     * @param viewModelId 视图模型标识
     */
    function getTreeGridUdtField(viewModelId: string) {
        const entities = getSchemaEntities();
        if (!entities || entities.length === 0) {
            return;
        }
        const vm = useFormSchema.getViewModelById(viewModelId);
        const fields = getTableFieldsByBindTo(entities, vm?.bindTo || '');
        if (!fields) {
            return '';
        }
        for (const element of fields) {
            if (element.type && element.type.$type === 'HierarchyType') {
                return element.label;
            }
        }
        return '';
    }

    /**
     *  获取分级码字段（返回所有类型为HierarchyType的字段）
     * @param viewModelId VMID
     */
    function getTreeGridUdtFields(viewModelId: string): any[] {
        const entities = getSchemaEntities();
        if (!entities || entities.length === 0) {
            return [];
        }
        const viewModelDetail = useFormSchema.getViewModelById(viewModelId);
        const fields = getTableFieldsByBindTo(entities, viewModelDetail?.bindTo || '');
        const udtFields = [] as any;
        if (!fields) {
            return [];
        }
        for (const element of fields) {
            if (element.type && element.type.$type === 'HierarchyType') {
                udtFields.push({ key: element.label, value: element.name, field: element });
            }
        }
        return udtFields;
    }

    /**
     * schema字段集合组装成树
     * @param fields schema字段集合
     */
    function assembleFields2Tree(fields: FormSchemaEntityField[], expandRelateNode = true, displayedFieldsMap: Map<string, boolean> | null = null, label = '') {
        const treeData = [] as any;
        fields.forEach(element => {
            // 零代码：不展示id等属性
            // if (formBasicService && formBasicService.envType === DesignerEnvType.noCode &&
            //     ['id', "version", "processInstance", "parentID"].includes(element.bindingField)) {
            //     return;
            // }
            // 补充bindingPath属性
            if (!element.bindingPath) {
                element.bindingPath = (label ? label + '.' : '') + element.label;
            }

            // 处理字段类型的国际化
            // element.type.displayName = EntityFieldTypeDisplayNamei18n[element.type.name] || element.type.displayName;

            // 关联表字段 / UDT字段
            let children = [];
            if (element.type && element.type.fields && element.type.fields.length > 0) {
                children = assembleFields2Tree(element.type.fields, true, displayedFieldsMap, element.bindingPath);
            }

            treeData.push({
                data: element,
                children,
                expanded: expandRelateNode,
                selectable: children.length > 0 ? false : true,
                isUsed: displayedFieldsMap && displayedFieldsMap.has(element.id),
                tips: element.name + '[' + element.id + ']'
            });

        });
        return treeData;
    }

    /**
     * 国际化：schema字段类型
     * @param fields 实体下的字段集合
     */
    function localizeFormSchema(schemaEntity: FormSchemaEntity) {

        const localizeSchemaFields = (fields: FormSchemaEntityField[]) => {
            fields.forEach(element => {

                // 字段类型名称
                // eslint-disable-next-line no-self-assign
                element.type.displayName = element.type.displayName;

                // 枚举类型名称
                if (element.type.name === FormSchemaEntityFieldTypeName.Enum && element.type.valueType && element.type.valueType.displayName) {
                    // eslint-disable-next-line no-self-assign
                    element.type.valueType.displayName = element.type.valueType.displayName;
                }
                // 关联表字段 / UDT字段
                if (element.type && element.type.fields && element.type.fields.length > 0) {
                    localizeSchemaFields(element.type.fields);
                }
            });
        };

        if (schemaEntity.type && schemaEntity.type.fields && schemaEntity.type.fields.length > 0) {
            localizeSchemaFields(schemaEntity.type.fields);
        }

        // 子表
        if (schemaEntity.type.entities && schemaEntity.type.entities.length > 0) {
            for (const childEntity of schemaEntity.type.entities) {
                localizeFormSchema(childEntity);
            }

        }
    }

    /**
     * VO 转化为表单schema
     * @param viewObjectId 视图对象标识
     */
    function convertViewObjectToEntitySchema(viewObjectId: string, sessionId: string) {
        // 1、schema.id 查询VO实体
        return metadataService.GetRefMetadata('', viewObjectId).then(result => {
            if (result && result.data && result.data.content) {
                const viewObjectMetadataContent = JSON.parse(result.data.content);
                // 2、将VO实体转为schema
                const schemaUrl = '/api/dev/main/v1.0/designschema/create';
                return axios.post(schemaUrl, viewObjectMetadataContent)
                    .then((response: any) => {
                        const schema = response.data as FormSchema;
                        // 字段类型国际化
                        const mainSchemaEntity = schema.entities[0];
                        localizeFormSchema(mainSchemaEntity);
                        return schema;
                    });
            }
        });
    }

    /**
     * 获取指定VM下的所有字段
     * @param viewModelId 视图模型标识
     */
    function getFieldsByViewModelId(viewModelId: string): FormSchemaEntityField[] {
        const vm = useFormSchema.getViewModelById(viewModelId);
        if (!vm) {
            return [];
        }
        const entities = getSchemaEntities();
        if (!entities || entities.length === 0) {
            return [];
        }
        return getTableFieldsByBindTo(entities, vm.bindTo);
    }


    /**
     * 根据实体获取VM id，之前用于实体树的拖拽
     * @param entity 实体
     */
    function getViewModelIdByEntity(entity: FormSchemaEntity): string {
        if (!entity) {
            return '';
        }
        const viewModels = useFormSchema.getViewModels();
        const entities = getSchemaEntities();
        const mappingViewModel = viewModels.find(viewModel => {
            if (viewModel.id === 'root-viewmodel') {
                return false; // root viewmodel 不对应任何SchemaEntity
            }
            const info = _getTableBasicInfoByUri(entities, viewModel.bindTo);
            return info.id === entity.id;
        });
        // 找到对应的viewmodel返回id，找不到返回undefined。
        return mappingViewModel && mappingViewModel.id || '';
    }

    /**
     * 根据指定的字段key值，获取字段信息。例如获取key=bindingPath,值为xxx的字段
     * @param targetFieldKey 字段key
     * @param targetFieldValue 字段key值
     */
    function getSchemaField(targetFieldKey: string, targetFieldValue: string): FormSchemaEntityField | null {
        const entities = getSchemaEntities();
        let retField: FormSchemaEntityField | null = null;
        const getTargetFiled = (predicate: (element: FormSchemaEntityField) => boolean, fieldsArray: FormSchemaEntityField[]) => {
            fieldsArray.find((element: FormSchemaEntityField) => {
                const predicateResult = predicate(element);
                if (predicateResult) {
                    retField = element;
                    return true;
                } else {
                    if (element.type.fields) {
                        getTargetFiled(predicate, element.type.fields);
                    }
                }
                return false;
            });
        };
        entities.forEach((entitiesItem: FormSchemaEntity) => {
            getTargetFiled((field: FormSchemaEntityField) => field[targetFieldKey] === targetFieldValue, entitiesItem.type.fields);
        });

        return retField;
    }

    /**
     * 字段名称映射更改
     */
    function fieldsDisplayNameUpdate(entity: FormSchemaEntityField[]) {
        entity.forEach((item) => {
            if (item.$type === FormSchemaEntityField$Type.SimpleField) {
                switch (item.type.$type) {
                    case FormSchemaEntityFieldType$Type.NumericType:
                        if (item.type.precision === 0) {
                            item.type.displayName = '整数';
                        }
                        if (item?.type?.precision && item?.type?.precision > 0) {
                            item.type.displayName = '浮点数字';
                        }
                        break;
                    case FormSchemaEntityFieldType$Type.StringType:
                        item.type.displayName = '文本';
                        break;
                    case FormSchemaEntityFieldType$Type.TextType:
                        item.type.displayName = '备注';
                        break;

                    default:
                        break;
                }

            }
        });
    }

    /**
     * 删除schema实体
     * @param entityId 实体id
     */
    function deleteSchemaEntityById(entityId: string) {

        const schemaEntities = getSchemaEntities();
        if (!schemaEntities || schemaEntities.length < 1 || !schemaEntities[0]) {
            return;
        }
        const mainEntity = schemaEntities[0];
        const queue = [mainEntity];
        while (queue.length) {
            const current = queue.shift();

            if (current?.type.entities && current.type.entities.length) {
                if (current.type.entities.some(entity => entity.id === entityId)) {
                    current.type.entities = current.type.entities.filter(entity => entity.id !== entityId);
                    return;
                }
                queue.push(...current.type.entities);
            }

        }

    }

    /**
     * 根据字段id获取所属schema实体和字段
     * @param fieldId 字段id
     */
    function getFieldBelongedEntity(fieldId: string): {
        entity: FormSchemaEntity, schemaField: FormSchemaEntityField, isRefElement: boolean
    } | null {
        const schemaEntities = getSchemaEntities();
        if (!schemaEntities || schemaEntities.length < 1 || !schemaEntities[0]) {
            return null;
        }
        const mainEntity = schemaEntities[0];
        let queue = [mainEntity];
        while (queue.length) {
            const current = queue.shift();
            if (current?.type.fields && current?.type.fields.length) {

                const fieldInfo = getFieldInfoByID(current.type.fields, '', fieldId);
                if (fieldInfo) {
                    return {
                        entity: current,
                        schemaField: fieldInfo.schemaField,
                        isRefElement: fieldInfo.isRefElement
                    };
                }
            }
            if (current?.type.entities && current.type.entities.length) {
                queue = queue.concat(current.type.entities);
            }
        }
        return null;
    }

    return { convertViewObjectToEntitySchema, getFieldByIDAndVMID, getFieldsByViewModelId, getTableInfoByViewModelId };

}
