/* eslint-disable prefer-destructuring */
/* eslint-disable complexity */
/* eslint-disable no-use-before-define */
import { cloneDeep } from "lodash-es";
import axios, { AxiosResponse } from 'axios';
import { FLoadingService,FMessageBoxService } from "@farris/ui-vue/components";
import { MetadataService } from "./metadata.service";
import { FormWebCmd, UseFormSchema, UseFormStateMachine } from "../types";
import { CommandMetadataConvertor } from "./command/command-metadata";
import { UseCommandBuilderService, UseFormCommandService } from "../types/command";
import { IdService } from "../components/view-model-designer/method-manager/service/id.service";
import { useParameterEditorData } from "./use-parameter-editor-data";
import { useEventParameterData } from "./use-event-parameter-data";
import { getSupportedControllerMethods } from "./command/supported-controller";

export function useFormCommandService(formSchemaService: UseFormSchema, useFormStateMachineComposition: UseFormStateMachine, loadingService: FLoadingService, webCmpBuilderService: UseCommandBuilderService): UseFormCommandService {
    const metadataService = new MetadataService();
    const messageService: any = FMessageBoxService;
    /** 命令列表下的参数 */
    let propertyItem: any;
    /** 内置构件控制器 */
    let internalCommandList = [] as any;
    /** 已绑定事件列表 */
    let boundEventsList: any;
    /** dom中的webCmds节点 */
    let commands: any;
    /** dom中的视图模型数据 */
    let viewModelData: any;
    /** 单个内置构件控制器 */
    const internalCommandListItem = {
        controllerName: {
            label: '',
            name: '',
            id: '',
        },
        controllerList: [
        ] as any
    };
    /** 控制器下的命令列表 */
    const controllerListItem = {
        label: '',
        name: '',
        id: '',
        handlerName: '',
        /** 当前命令需要选择目标组件 */
        showTargetComponent: false,
        /** 当前命令选择的目标组件的id */
        cmpId: '',
        componentLists: [] as any,
        isNewGenerated: undefined,
        isRTCmd: undefined,
        isInvalid: false,
        property: [] as any
    };

    let newControllerMethodBindingInfo: {
        controlData: any,
        componentId: string,
        viewModelId: string,
        eventCode: string,
        methodCode: string,
        methodName: string,
        setPropertyRelates?: any
    } | null;

    const { getEventParameterData } = useEventParameterData(formSchemaService, useFormStateMachineComposition);
    const { assembleOutline, assembleSchemaFieldsByComponent,
        assembleStateVariables,
    } = useParameterEditorData(formSchemaService);
    function getCommands() {
        return commands;
    }
    /** 0.获取webcmd控制器（其参数为空）*/
    function checkCommands(): Promise<any> {
        // const loadingInstance = loadingService['show']({ message: '数据加载中，请稍候....' });
        return new Promise((resolve, reject) => {
            loadWebcmd().then((webCmds) => {
                commands = cloneDeep(webCmds);
                generateInternalCommandList();
                checkViewModelCommands(webCmds);
                syncActions();
                // loadingInstance.value.close();
                resolve([]);
            }).catch((error) => {
                messageService.warning(error);
                // loadingInstance.value.close();
                reject(error);
            });
        });
    }
    /**
     * 返回目前支持的控制器命令
     */
    function filterSupportedCommand(controllerContent: any) {
        const { Id: controllerId, Commands: allCommands } = controllerContent;
        return getSupportedControllerMethods(controllerId, allCommands);
    }

    /** 1.生成webcmd控制器列表 
     * loadWebcmd=>loadCommand
    */
    function loadWebcmd(): Promise<any> {
        return new Promise((resolve, reject) => {
            loadCommandsInCurrentPath().then(() => {
                const commandsInfos = formSchemaService.getCommands();
                const metadataInfo = formSchemaService.getFormMetadataBasicInfo();
                if (!metadataInfo || !commandsInfos) {
                    resolve([]);
                    return;
                }
                const requestCommandsMetadata = commandsInfos.map((webcmdInfo: FormWebCmd) => {
                    return metadataService.GetRefMetadata(metadataInfo.relativePath || '', webcmdInfo.id).then((response) => {
                        webcmdInfo.code = webcmdInfo.code ? webcmdInfo.code : response.data.code;
                        webcmdInfo.nameSpace = webcmdInfo.nameSpace ? webcmdInfo.nameSpace : response.data.nameSpace;
                        return response.data;
                    }).catch(() => {
                        if (webcmdInfo.refedHandlers && webcmdInfo.refedHandlers.length) {
                            const msg = '获取元数据XXX失败，是否移除表单的引用？'.replace('XXX', webcmdInfo.name);
                            messageService.question(msg);
                        }
                        formSchemaService.setCommands(formSchemaService.getCommands().filter(webCmd => webCmd.id !== webcmdInfo.id));
                    });
                });
                axios.all(requestCommandsMetadata).then(axios.spread((...metadataList) => {
                    // !!item进行转化
                    const metadataContentList = metadataList.filter(item => item !== undefined && item !== null).map(item => {
                        const content = new CommandMetadataConvertor().InitFromJobject(JSON.parse(item['content']));
                        const supportedCommands = item.nameSpace.includes('.Front') ? content.Commands : filterSupportedCommand(content);
                        content.Commands = supportedCommands;
                        return content;
                    });
                    resolve(metadataContentList);
                })).catch((error) => {

                    reject(error);

                });
            });
        });
    }
    /** 1-1.将当前表单相关的自定义构件追加到webCmds中 
     * private
     * loadWebCmdsInCurrentPath
    */
    function loadCommandsInCurrentPath(): Promise<any> {
        const commandsInfos = formSchemaService.getCommands();
        const metadataInfo = formSchemaService.getFormMetadataBasicInfo();
        return new Promise((resolve, reject) => {
            metadataService.GetMetadataList(metadataInfo.relativePath, '.webcmd').then((response: AxiosResponse<any[]>) => {
                if (!response.data) {
                    resolve(null);
                    return;
                }
                for (const item of response.data) {
                    let extendProperty: any;
                    try {
                        extendProperty = JSON.parse(item.extendProperty);
                        // eslint-disable-next-line no-empty
                    } catch (e) {
                    }

                    // 比对表单编号，若不匹配说明是其他表单关联的自定义构件
                    if (extendProperty && extendProperty.FormCode !== metadataInfo.code) {
                        continue;
                    }

                    // 只要是属于此表单的都需要加载，除非是已经添加到表单中
                    if (!commandsInfos.find(webcmdInfo => item.id === webcmdInfo.id)) {
                        // 画布渲染过程中，可能会在控件上补充属性，这种变更用户不感知，所以不需要通知IDE框架
                        window['suspendChangesOnForm'] = true;
                        const webcmd: FormWebCmd = {
                            id: item.id,
                            path: item.relativePath,
                            name: item.fileName,
                            refedHandlers: []
                        };
                        commandsInfos.push(webcmd);
                        window['suspendChangesOnForm'] = false;
                    }
                }
                resolve(null);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    /** 2.生成内置构件中的控制器列表数据 */
    function generateInternalCommandList() {
        commands = cloneDeep(getUniqueValue(commands, 'Id'));
        internalCommandList = [] as any;
        commands.forEach(controller => {
            internalCommandListItem.controllerName = {
                label: controller.Code,
                name: controller.Name,
                id: controller.Id,
            };
            if (!controller.Commands) {
                controller['Commands'] = [];
            }
            controller.Commands.forEach(command => {
                controllerListItem.label = command.Code;
                controllerListItem.name = command.Name;
                controllerListItem.id = command.Id;
                controllerListItem.handlerName = command.Code;
                if (command.Parameters) {
                    command.Parameters.forEach(params => {
                        propertyItem = {
                            id: params.Id,
                            name: cloneDeep(params.Code),
                            value: '',
                            shownName: params.Name,
                            description: params.Description,
                            editorType: cloneDeep(params.EditorType),
                            isRetVal: params.IsRetVal,
                            parameterType: params.parameterType,
                            origin: cloneDeep(params),
                            context: {
                                // 通用编辑器数据
                                generalData: {
                                    assembleOutline,
                                    assembleSchemaFieldsByComponent,
                                    assembleStateVariables
                                },
                                // 回调方法或状态机动作或目标组件数据列表
                                data: getEventParameterData(params.controlSource?.context?.data?.value) || [],
                            }
                        };
                        controllerListItem.property.push(cloneDeep(propertyItem));
                    });
                }
                else {
                    command['Parameters'] = [];
                    controllerListItem.property = [];
                }
                internalCommandListItem.controllerList.push(cloneDeep(controllerListItem));
                controllerListItem.property = [];
            });
            internalCommandList.push(cloneDeep(internalCommandListItem));
            internalCommandListItem.controllerList = [];
        });
        return internalCommandList;

    }
    /** 2-1.去重 */
    function getUniqueValue(itemWithSameValue, identifier) {
        const value = cloneDeep(itemWithSameValue);
        for (let i = 0; i < value.length; i++) {
            for (let j = i + 1; j < value.length; j++) {
                if (value[i][identifier] === value[j][identifier]) {
                    value.splice(j, 1);
                    j--;
                }
            }
        }
        itemWithSameValue = cloneDeep(value);
        return itemWithSameValue;
    }

    /** 3.检查视图模型的命令是否符合存储规范 */
    function checkViewModelCommands(webCmds) {

        // 设计器自动补充的属性，不记录变更
        window['suspendChangesOnForm'] = true;

        formSchemaService.getViewModels().forEach(viewModel => {
            if (!viewModel.commands) {
                return;
            }
            viewModel.commands.map(curCmd => {
                const webCmd = webCmds.find(item => item.Id === curCmd.cmpId);
                if (!webCmd) {
                    curCmd.isInvalid = true;
                    return;
                }
                const commandInWebCmd = webCmd.Commands.find(c => c.Code === curCmd.handlerName);

                if (commandInWebCmd) {
                    curCmd.isInvalid = false;

                    // 将表单中记录的参数名称更新为控制器中的参数名称
                    if (curCmd.params && curCmd.params.length && commandInWebCmd.Parameters && commandInWebCmd.Parameters.length) {
                        curCmd.params.forEach((curParam) => {
                            const paramInWebCmd = commandInWebCmd.Parameters.find(param => param.Code === curParam.name);
                            if (paramInWebCmd) {
                                curParam.shownName = paramInWebCmd.Name;
                                delete curParam.description;
                            }
                        });
                    }
                } else {
                    curCmd.isInvalid = true;
                }
            });

        });

        window['suspendChangesOnForm'] = false;
    }

    /** 4.渲染表单取出所有的已绑定事件，同步至actions节点 */
    function syncActions() {
        if (commands) {
            let components = cloneDeep(formSchemaService.getComponents());
            components = components.filter(c => !c.fakeDel);
            const findEvents = [];
            let allBoundEvents = findBoundEvent(components, findEvents, 'root-viewmodel');
            if (formSchemaService.getFormSchema().module.toolbar) {
                allBoundEvents = findBoundEventInToolbar(formSchemaService.getFormSchema().module.toolbar, allBoundEvents);
            }
            getUniqueEvent(allBoundEvents);
            const array = [];
            const viewModel = cloneDeep(formSchemaService.getFormSchema().module.viewmodels);
            if (allBoundEvents.length !== 0) {
                allBoundEvents = setActions(getComponentId(matchWebcmd(matchVM(allBoundEvents, viewModel))), 0, array);
            }
            formSchemaService.getFormSchema().module.actions = cloneDeep(allBoundEvents);
        }
    }
    /** 4-1-1.遍历components节点，找到所有事件 */
    function findBoundEvent(components, findEvents, viewModelId, excludedEvents?: string[]) {
        // ----------------------ToDo--------------------------------
        // controlService = injector.get(DesignerHostSettingService).controlService;
        components.forEach((componentsItem) => {
            if (componentsItem['viewModel']) { viewModelId = componentsItem['viewModel']; }
            if (componentsItem['contents']) {
                findBoundEvent(componentsItem['contents'], findEvents, viewModelId);
            }
            if (componentsItem['items']) {
                findBoundEvent(componentsItem['items'], findEvents, viewModelId);
            }
            if (componentsItem['children']) {
                findBoundEvent(componentsItem['children'], findEvents, viewModelId);
            }
            if (componentsItem['fields']) {
                findBoundEvent(componentsItem['fields'], findEvents, viewModelId);
            }
            if (componentsItem['editor']) {
                const editor = [] as any;
                editor.push(cloneDeep(componentsItem['editor']));
                findBoundEvent(editor, findEvents, viewModelId, ['linkedLabelClick']);
            }
            if (componentsItem['toolbar']) {
                if (componentsItem['toolbar']['contents']) {
                    findBoundEvent(componentsItem['toolbar']['contents'], findEvents, viewModelId);
                }
                // 移动：导航工具栏
                if (componentsItem['toolbar']['items']) {
                    findBoundEvent(componentsItem['toolbar']['items'], findEvents, viewModelId);
                }
            }
            // ListView
            if (componentsItem['type'] === "ListView") {
                if (componentsItem['contentTemplate']) {
                    const contentTemplateDom = componentsItem['contentTemplate'];
                    if (contentTemplateDom['toolbar']) {
                        if (contentTemplateDom['toolbar']['contents']) {
                            findBoundEvent(contentTemplateDom['toolbar']['contents'], findEvents, viewModelId);
                        }
                    }
                }
                // 移动：列表滑动
                if (componentsItem['swipeToolbar'] && componentsItem['swipeToolbar']['items']) {
                    findBoundEvent(componentsItem['swipeToolbar']['items'], findEvents, viewModelId);
                }

            }
            // table类型
            if (componentsItem['type'] === "Table" && componentsItem['rows']) {
                const rows = componentsItem['rows'];
                rows.forEach(rowsItem => {
                    if (rowsItem['columns']) {
                        const columns = rowsItem['columns'];
                        columns.forEach(columnsItem => {
                            if (columnsItem['editor']) {
                                findBoundEvent([columnsItem['editor']], findEvents, viewModelId);
                            }
                        });
                    }
                });
            }
            // 筛选方案
            if (componentsItem['fieldConfigs']) {
                const fieldConfigs = componentsItem['fieldConfigs'];
                fieldConfigs.forEach(fieldConfigsItem => {
                    if (fieldConfigsItem['control']) {
                        findBoundEvent([fieldConfigsItem['control']], findEvents, viewModelId);
                    }
                });
            }
            // 筛选条
            if (componentsItem['filterList']) {
                const fieldConfigs = componentsItem['filterList'];
                fieldConfigs.forEach(fieldConfigsItem => {
                    if (fieldConfigsItem['control']) {
                        findBoundEvent([fieldConfigsItem['control']], findEvents, viewModelId);
                    }
                });
            }

            // 侧边栏
            if (componentsItem['type'] === "Sidebar" && componentsItem['toolbar'] && componentsItem['toolbar']['items']) {
                findBoundEvent(componentsItem['toolbar']['items'], findEvents, viewModelId);
            }

            // 智能输入框的弹出表单模式
            if (componentsItem['modalConfig'] && componentsItem['modalConfig']['footerButtons'] && componentsItem['modalConfig']['footerButtons'].length) {
                findBoundEvent(componentsItem['modalConfig']['footerButtons'], findEvents, viewModelId);
            }

            // 列表右键菜单中的按钮
            if (componentsItem['type'] === "DataGrid" && componentsItem['contextMenuItems'] && componentsItem['contextMenuItems'].length) {
                findBoundEvent(componentsItem['contextMenuItems'], findEvents, viewModelId);
            }

            // 查询结果工具栏中的按钮
            if (componentsItem['type'] === "QdpFramework" && componentsItem['inputTemplate'] && componentsItem['inputTemplate'].length) {
                findBoundEvent(componentsItem['inputTemplate'], findEvents, viewModelId);
            }
            // ----------------------ToDo--------------------------------
            // const controlEventPropertyIDList = controlService.getControlEventPropertyIDList();
            const controlEventPropertyIDList = [];
            const eventKeys = Object.keys(controlEventPropertyIDList);
            for (let i = 0; i < eventKeys.length; i++) {
                if (excludedEvents && excludedEvents.length && excludedEvents.includes(eventKeys[i])) {
                    continue;
                }
                const exist = Object.prototype.hasOwnProperty.call(componentsItem, eventKeys[i]);
                if (exist && componentsItem[eventKeys[i]]) {
                    // 判定三段式路径
                    const paths = componentsItem[eventKeys[i]].includes('.') ? componentsItem[eventKeys[i]].split('.') : undefined;
                    const vmId = paths !== undefined ? paths[1] : viewModelId;
                    const cmdLabel = paths !== undefined ? paths[2] : componentsItem[eventKeys[i]];
                    const findEventsItem = {
                        id: componentsItem['id'],
                        eventLabel: eventKeys[i],
                        eventName: controlEventPropertyIDList[eventKeys[i]],
                        commandLabel: cmdLabel,
                        viewModelId: vmId
                    };
                    findEvents.push(cloneDeep(findEventsItem));
                }
            }
        });
        return findEvents;
    }
    /** 4-1-2.遍历查看是否存在toolbar节点，若有，则找到所有事件 */
    function findBoundEventInToolbar(toolbar, findEvents) {
        if (toolbar['items']) {
            const items = toolbar['items'];
            const vmKeys = Object.keys(items);
            for (let j = 0; j < vmKeys.length; j++) {
                const vmValue = items[vmKeys[j]];
                vmValue.forEach(vmValueItem => {
                    // const controlEventPropertyIDList = controlService.getControlEventPropertyIDList();
                    // ----------------------ToDo--------------------
                    const controlEventPropertyIDList = [];
                    const eventKeys = Object.keys(controlEventPropertyIDList);
                    for (let i = 0; i < eventKeys.length; i++) {
                        const exist = Object.prototype.hasOwnProperty.call(vmValueItem, eventKeys[i]);
                        if (exist && vmValueItem[eventKeys[i]]) {
                            // 判定三段式路径
                            const paths = vmValueItem[eventKeys[i]].includes('.') ? vmValueItem[eventKeys[i]].split('.') : undefined;
                            const vmId = paths !== undefined ? paths[1] : vmKeys[j];
                            const cmdLabel = paths !== undefined ? paths[2] : vmValueItem[eventKeys[i]];
                            const findEventsItem = {
                                id: vmValueItem['id'],
                                eventLabel: eventKeys[i],
                                eventName: controlEventPropertyIDList[eventKeys[i]],
                                commandLabel: cmdLabel,
                                viewModelId: vmId
                            };
                            findEvents.push(cloneDeep(findEventsItem));
                        }

                        // 识别二级按钮
                        if (vmValueItem.items && vmValueItem.items.length) {
                            vmValueItem.items.forEach(childItem => {
                                if (!childItem[eventKeys[i]]) {
                                    return;
                                }
                                const paths = childItem[eventKeys[i]].includes('.') ? childItem[eventKeys[i]].split('.') : undefined;
                                const vmId = paths !== undefined ? paths[1] : vmKeys[j];
                                const cmdLabel = paths !== undefined ? paths[2] : childItem[eventKeys[i]];
                                const findEventsItem = {
                                    id: childItem['id'],
                                    eventLabel: eventKeys[i],
                                    eventName: controlEventPropertyIDList[eventKeys[i]],
                                    commandLabel: cmdLabel,
                                    viewModelId: vmId
                                };
                                findEvents.push(cloneDeep(findEventsItem));
                            });

                        }
                    }
                });
            }
        }
        return findEvents;
    }
    /** 4-2.去重-去除特殊情况下存储的相同事件及命令 */
    function getUniqueEvent(allBoundEvents) {
        const value = cloneDeep(allBoundEvents);
        for (let i = 0; i < value.length; i++) {
            for (let j = i + 1; j < value.length; j++) {
                if (value[i]['id'] === value[j]['id'] && value[i]['eventLabel'] === value[j]['eventLabel']) {
                    value.splice(j, 1);
                    j--;
                }
            }
        }
        allBoundEvents = cloneDeep(value);
        return allBoundEvents;
    }
    /** 4-3.根据遍历components和toolbar得出的所有事件，结合viewmodel和webcmd,得到参数值 */
    function matchVM(allBoundEvents, viewModel) {
        const updatedBoundEvents = [] as any;
        allBoundEvents.forEach(boundEvenItem => {
            viewModel.forEach(viewModelItem => {
                if (viewModelItem.id === boundEvenItem.viewModelId) {
                    viewModelItem.commands.forEach(commandItem => {
                        if (commandItem.code === boundEvenItem.commandLabel) {
                            const event = {
                                id: boundEvenItem.id,
                                eventLabel: boundEvenItem.eventLabel,
                                eventName: boundEvenItem.eventName,
                                viewModelId: boundEvenItem.viewModelId,
                                commandId: commandItem.id,
                                commandLabel: boundEvenItem.commandLabel,
                                commandName: commandItem.name,
                                handlerName: commandItem.handlerName,
                                cmpId: commandItem.cmpId,
                                isNewGenerated: commandItem.isNewGenerated || false,
                                isRTCmd: commandItem['isRTCmd'],
                                isInvalid: commandItem.isInvalid || false,
                                params: cloneDeep(commandItem.params),
                                controllerId: '',
                                controllerLabel: '',
                                controllerName: '',
                            };
                            if (commandItem['targetComponent']) {
                                event['targetComponent'] = commandItem.targetComponent;
                            }
                            updatedBoundEvents.push(cloneDeep(event));
                        }
                    });
                }
            });
        });
        return updatedBoundEvents;
    }
    /** 4-4.根据遍历components得出的所有事件，结合webcmd,得到控制器值 */
    function matchWebcmd(allBoundEvents) {
        commands.forEach(commandItem => {
            allBoundEvents.forEach(allBoundEventsItem => {
                if (commandItem.Id === allBoundEventsItem.cmpId) {
                    commandItem.Commands.forEach(item => {
                        if (item.Code === allBoundEventsItem.handlerName) {
                            allBoundEventsItem.controllerId = commandItem.Id;
                            allBoundEventsItem.controllerLabel = commandItem.Code;
                            allBoundEventsItem.controllerName = commandItem.Name;
                        }
                    });
                }
            });
        });
        allBoundEvents.forEach(function (allBoundEventsItem, index) {
            // 失效命令
            if (allBoundEventsItem.controllerId === '' && allBoundEventsItem.controllerLabel === '' && allBoundEventsItem.controllerName === '') {
                allBoundEvents.splice(index, 1);
            }
        });
        return allBoundEvents;
    }
    /** 4-5.根据遍历components得出的事件的viewmodelId,得到componentId值 */
    function getComponentId(allBoundEvents) {
        const components = formSchemaService.getComponents();
        const viewModel = formSchemaService.getViewModels();
        for (let i = 0; i < components.length; i++) {
            allBoundEvents.forEach(allBoundEventsItem => {
                if (viewModel[i] && viewModel[i].id === allBoundEventsItem.viewModelId) {
                    allBoundEventsItem.componentId = components[i].id;
                }
            });
        }
        return allBoundEvents;
    }
    /** 4-6.标准样式化actions */
    function setActions(allBoundEvents, num, array) {
        const action = {
            sourceComponent: {
                id: '',
                viewModelId: '',
                map: [] as any
            }
        };
        if (!allBoundEvents[num]) {
            return;
        }
        action.sourceComponent.id = allBoundEvents[num].id;
        action.sourceComponent.viewModelId = allBoundEvents[num].viewModelId;
        let i = num;
        while (i < allBoundEvents.length) {
            if (allBoundEvents[i].id === action.sourceComponent.id) {
                const mapItem = {
                    event: {
                        label: allBoundEvents[i].eventLabel,
                        name: allBoundEvents[i].eventName
                    },
                    targetComponent: {
                        id: allBoundEvents[i].componentId,
                        viewModelId: allBoundEvents[i].viewModelId
                    },
                    command: {
                        id: allBoundEvents[i].commandId,
                        label: allBoundEvents[i].commandLabel,
                        name: allBoundEvents[i].commandName,
                        handlerName: allBoundEvents[i].handlerName,
                        params: cloneDeep(allBoundEvents[i].params),
                        cmpId: allBoundEvents[i].cmpId,
                        isNewGenerated: allBoundEvents[i].isNewGenerated || false,
                        isRTCmd: allBoundEvents[i]['isRTCmd'],
                        isInvalid: allBoundEvents[i].isInvalid || false,
                    },
                    controller: {
                        id: allBoundEvents[i].controllerId,
                        label: allBoundEvents[i].controllerLabel,
                        name: allBoundEvents[i].controllerName
                    }
                };
                if (allBoundEvents[i]['componentId']) {
                    mapItem.targetComponent.id = allBoundEvents[i].componentId;
                }
                action.sourceComponent.map.push(cloneDeep(mapItem));
                i++;
                if (i === allBoundEvents.length) {
                    array.push(cloneDeep(action));
                }
            }
            else {
                array.push(cloneDeep(action));
                setActions(allBoundEvents, i, array);
                break;
            }
        }
        return array;
    }

    /**
     *  更新事件编辑器获取到的commands
     * webCmdsChanged=>commandsChanged
    */
    function commandsChanged(newController) {
        newController.forEach(newControllerItem => {
            const item = new CommandMetadataConvertor().InitFromJobject(newControllerItem);
            commands.push(cloneDeep(item));
        });
        checkViewModelCommands(commands);
    }

    /** 
     * 事件面板调用-1
     * a. 合并视图模型带参数的命令&控制器中无参数的命令
     * b. 生成绑定事件列表
     *  */
    function findParamtersPosition(propertyData: any, events: any, viewModelId: string, allComponentList: any) {
        boundEventsList = [];
        const viewModelData = cloneDeep(formSchemaService.getViewModels());
        // 1. 遍历当前container的组件dom结构的所有事件的值
        events.forEach(event => {
            if (propertyData[event.label]) {
                // 三段式path：root-viewmodel.items-component-viewmodel.itemsAddItem1 或 一段path：itemsAddItem1
                const paths = propertyData[event.label].includes('.') ? propertyData[event.label].split('.') : undefined;
                const { recordCommand, recordController } = handleParams(viewModelData, paths, propertyData[event.label], viewModelId, allComponentList);
                generateBoundEventsList(event, events, recordController, recordCommand);
            }
        });
        return boundEventsList;
    }
    /** 1-a.在视图模型中找到命令，并填充参数值*/
    function handleParams(viewModelData, paths, propCmdLabel, viewModelId, allComponentList) {
        const vmId = paths !== undefined ? paths[1] : viewModelId;
        const cmdLabel = paths !== undefined ? paths[2] : propCmdLabel;
        let cmpId;
        if (allComponentList.length && vmId !== undefined) {
            const componentList = allComponentList.find(componentListsItem => componentListsItem.viewModelId === vmId);
            cmpId = componentList === undefined ? undefined : componentList.componentId;
        }
        // 若命令不存在，则提示！，并允许用户绑定其他命令
        let recordCommand = {
            id: 'abandoned',
            code: cmdLabel,
            label: cmdLabel,
            name: cmdLabel,
            params: [],
            handlerName: cmdLabel,
            cmpId: '',
            shortcut: {},
            extensions: [],
            isInvalid: false,
            isNewGenerated: undefined,
            showTargetComponent: false,
            targetComponent: cmpId,
            isRTCmd: undefined,
        };
        let recordController = {
            controllerName: {
                label: '',
                name: '',
                id: '',
            },
            controllerList: {
            },
            boundEvents: {
            }
        };
        viewModelData.forEach(element => {
            // 5. 寻找命令-确认itemsAddItem1的位置
            if (element.code.includes(vmId)) {
                element.commands.forEach(viewModelcommand => {
                    // 6-1. 若命令存在，且参数不为空
                    if (viewModelcommand.code === cmdLabel && viewModelcommand.params.length !== 0) {
                        // 判断是否为已失效命令
                        if (!viewModelcommand['isInvalid']) {
                            // 7-1. 若存在参数，则合并该命令在VM与控制器中的参数值
                            viewModelcommand.params.forEach(param => {
                                // 7-1-1.handlerName唯一，code为带前后缀的命令英文名，name为中文名，param是当前待合并的参数（有值）
                                const combinedParamtersResult = combinedParamters(param, viewModelcommand, recordController, recordCommand, cmpId);
                                recordController = combinedParamtersResult.recordController;
                                recordCommand = combinedParamtersResult.recordCommand;
                            });
                        }
                        else {
                            // 命令已被删除
                            recordCommand.id = 'deleted';
                        }
                    }
                    // 6-2 若命令存在，且参数为空的处理
                    else if (viewModelcommand.code === cmdLabel && viewModelcommand.params.length === 0) {
                        // 判断是否为已失效命令
                        if (!viewModelcommand['isInvalid']) {
                            // 7-2. 不需要进行内置构件的处理，仅记录了命令所在的控制器及带前后缀的命令名称
                            internalCommandList.forEach(controller => {
                                if (controller.controllerName.id === viewModelcommand.cmpId) {
                                    controller.controllerList.forEach(command => {
                                        if (command.handlerName === viewModelcommand.handlerName) {
                                            recordController = cloneDeep(controller);
                                            recordCommand = getRecordController(command, viewModelcommand, recordCommand, cmpId);
                                        }
                                    });
                                }
                            });
                        } else {
                            // 命令已被删除
                            recordCommand.id = 'deleted';
                        }
                    }
                });

            }
        });
        return {
            recordController,
            recordCommand
        };
    }
    /** 1-a.合并视图模型带参数的命令&控制器中无参数的命令 */
    function combinedParamters(paramWithValue: any, viewModelcommand, recordController, recordCommand, cmpId) {
        // 7-1-2. handlerName唯一，code为带前后缀的命令英文名，paramWithValue是当前待合并的参数（有值）
        internalCommandList.forEach(controller => {
            // 控制器相同
            if (controller.controllerName.id === viewModelcommand.cmpId) {
                controller.controllerList.forEach(command => {
                    // 7-1-3. command.handlerName来自内置构件；handlerName来自视图模型
                    if (command.handlerName === viewModelcommand.handlerName) {
                        command.property.forEach(paramWithoutValue => {
                            if (paramWithoutValue.name === paramWithValue.name) {
                                paramWithoutValue.value = cloneDeep(paramWithValue.value);
                            }
                        });
                        recordController = cloneDeep(controller);
                        recordCommand = getRecordController(command, viewModelcommand, recordCommand, cmpId);
                    }
                });
            }
        });
        return {
            recordController,
            recordCommand
        };
    }
    /** 1-a.存储绑定事件对应的控制器参数值 */
    function getRecordController(command, viewModelcommand, recordCommand, cmpId) {
        // 1-a-1. 由于vm未记录控制器，此处根据命令找到控制器，并将有值的参数对应的命令command记录
        recordCommand = cloneDeep(command);
        // 1-a-2.记录带前后缀的英文名、中文名/参数
        recordCommand.label = viewModelcommand.code;
        recordCommand.name = viewModelcommand.name;
        recordCommand.id = viewModelcommand.id;
        recordCommand.targetComponent = cmpId;
        recordCommand['isRTCmd'] = viewModelcommand['isRTCmd'];
        return recordCommand;
    }
    /** 1-b.生成已绑定的事件列表 */
    function generateBoundEventsList(event, events, recordController, recordCommand) {
        if (recordController !== undefined) {
            const boundEventsListItem = {
                command: {
                },
                controller: {
                },
                boundEvents: {
                }
            };
            events.forEach(each => {
                if (each.label === event.label) { boundEventsListItem.boundEvents = cloneDeep(each); }
            });
            boundEventsListItem.controller = cloneDeep(recordController.controllerName);
            boundEventsListItem.command = cloneDeep(recordCommand);
            boundEventsList.push(cloneDeep(boundEventsListItem));
        }
    }


    /** 事件面板调用-2
     * 2-a.找到视图模型，在视图模型中增删改命令及参数 */
    function viewModelDomChanged(propertyData: any, events: any, viewModelId: string, domActions: any) {
        const viewModelData = cloneDeep(formSchemaService.getViewModels());
        events.forEach(event => {
            // 1. 遍历当前container的dom结构的所有事件的值
            if (propertyData[event.label]) {
                // 三段式path：root-viewmodel.items-component-viewmodel.itemsAddItem1 或 一段path：itemsAddItem1
                const paths = propertyData[event.label].includes('.') ? propertyData[event.label].split('.') : undefined;
                handleViewModel(viewModelData, paths, propertyData[event.label], viewModelId, domActions, propertyData);
            }
        });
        formSchemaService.setViewmodels(cloneDeep(viewModelData));
    }
    /** 2-a.在视图模型中增删改命令及参数 */
    function handleViewModel(viewModelData, paths, propCmdLabel, viewModelId, domActions, propertyData) {
        const newCommand = {
            id: '',
            code: '',
            name: '',
            params: [],
            handlerName: '',
            cmpId: '',
            shortcut: {},
            extensions: [],
            isInvalid: false,
            isNewGenerated: undefined,
            targetComponent: undefined,
            isRTCmd: undefined,
        };
        const vmId = paths !== undefined ? paths[1] : viewModelId;
        const cmdLabel = paths !== undefined ? paths[2] : propCmdLabel;
        // 4. 遍历找到存储命令的第二层path——path[1]，viewModel的items-component-viewmodel
        viewModelData.forEach(element => {
            if (element.code === vmId) {
                let commandExist = false;
                // 5. 借助整体dom中的domActions节点，寻找命令-确认itemsAddItem1的位置
                domActions.forEach(action => {
                    // 6. 匹配domActions，取出所有需要的值，包括命令名/事件名/参数/id等等；
                    if (action.sourceComponent.id === propertyData.id) {
                        action.sourceComponent.map.forEach(mapItem => {
                            if (mapItem.command.label === cmdLabel) {
                                newCommand.id = cloneDeep(mapItem.command.id);
                                newCommand.code = mapItem.command.label;
                                newCommand.name = mapItem.command.name;
                                newCommand.params = serializeParameter(mapItem.command.params);
                                newCommand.handlerName = mapItem.command.handlerName;
                                newCommand.cmpId = cloneDeep(mapItem.controller.id);
                                newCommand.shortcut = mapItem.command.shortcut;
                                newCommand.isRTCmd = mapItem.command['isRTCmd'];
                                newCommand.isNewGenerated = mapItem.command.isNewGenerated || false;
                                newCommand.isInvalid = mapItem.command.isInvalid || false;
                                newCommand.extensions = mapItem.command.extensions;
                                if (mapItem.command.isInvalid) {
                                    newCommand.isInvalid = mapItem.command.isInvalid;
                                }
                                newCommand.targetComponent = mapItem.targetComponent.id;
                            }
                        });
                    }
                });
                // 7-1. 匹配viewModel中存储的是否有该命令：itemsAddItem1，若存在该命令，则更新新的值
                element.commands.forEach(function (command, index) {
                    if (command.code === cmdLabel) {
                        commandExist = true;
                        element.commands.splice(index, 1, cloneDeep(newCommand));
                    }
                });
                // 7-2. 若没有该命令，则进行添加;并且不为已删除的命令，则添加
                if (!commandExist && newCommand.id !== 'abandoned') {
                    element.commands.push(cloneDeep(newCommand));
                }
            }
            // 8-1. 剪切掉其他同名称的命令
            else {
                // 8-1. 匹配其他viewModel中是否有同名称的命令，若有，则删除；
                element.commands.forEach(function (command, index) {
                    if (command.code === cmdLabel) {
                        element.commands.splice(index, 1);
                    }
                });
            }
        });
    }


    /** 事件面板调用-3
     * 3-a.存储事件编辑器「视图模型」中所有的viewModel数据 */
    function viewModelDisplay() {
        let savedViewModel = [] as any;
        const savedViewModelItem = {
            controllerName: {
                label: '',
                name: '',
                id: '',
            },
            controllerList: [
            ]
        };
        /** 控制器下的命令列表 */
        const controllerListItem = {
            label: '',
            name: '',
            id: '',
            handlerName: '',
            showTargetComponent: false,
            isNewGenerated: undefined,
            isRTCmd: undefined,
            isInvaild: false,
            cmpId: '',
            componentLists: [],
            targetComponent: undefined,
            property: [

            ]
        };
        if (!formSchemaService.getModule().actions) {
            return savedViewModel;
        }

        formSchemaService.getModule().actions.forEach(actionItem => {
            actionItem.sourceComponent.map.forEach(mapItem => {
                savedViewModelItem.controllerName.id = mapItem.controller.id;
                savedViewModelItem.controllerName.name = mapItem.controller.name;
                savedViewModelItem.controllerName.label = mapItem.controller.label;
                savedViewModel.push(cloneDeep(savedViewModelItem));
                savedViewModel = getUniqueController(savedViewModel);
            });
        });

        savedViewModel.forEach(savedViewModelItem => {
            formSchemaService.getModule().actions.forEach(actionItem => {
                actionItem.sourceComponent.map.forEach(mapItem => {
                    if (mapItem.controller.id === savedViewModelItem.controllerName.id) {
                        controllerListItem.label = mapItem.command.label;
                        controllerListItem.name = mapItem.command.name;
                        controllerListItem.id = mapItem.command.id;
                        controllerListItem.handlerName = mapItem.command.handlerName;
                        controllerListItem.property = cloneDeep(mapItem.command.params);
                        controllerListItem.cmpId = mapItem.controller.id;
                        controllerListItem.isNewGenerated = mapItem.isNewGenerated || false;
                        controllerListItem.isRTCmd = mapItem['isRTCmd'];
                        controllerListItem.isInvaild = mapItem.inInvalid || false;
                        if (mapItem.targetComponent.id) {
                            controllerListItem.targetComponent = mapItem.targetComponent.id;
                        } else {
                            controllerListItem.targetComponent = undefined;
                        }
                        savedViewModelItem.controllerList.push(cloneDeep(controllerListItem));
                    }
                });
            });
        });
        return savedViewModel;
    }

    /** 去重 */
    function getUniqueController(itemWithSameValue) {
        const value = cloneDeep(itemWithSameValue);
        for (let i = 0; i < value.length; i++) {
            for (let j = i + 1; j < value.length; j++) {
                if (value[i]['controllerName']['id'] === value[j]['controllerName']['id']) {
                    value.splice(j, 1);
                    j--;
                }
            }
        }
        itemWithSameValue = cloneDeep(value);
        return itemWithSameValue;
    }

    /** 触发新增控制器方法 */
    function addControllerMethod(propertyData: any, viewModelId: string, parameters: any) {
        const { methodCode, methodName } = resolveNewMethodForCodeEditor(propertyData, viewModelId, parameters);
        webCmpBuilderService.addControllerMethod(methodCode, methodName);
    }
    /**
     * private
     * 序列化视图模型中的命令参数
     * @param parameters 参数
     * @returns 
     */
    function serializeParameter(parameters: any[]) {
        const results = [] as any;
        if (parameters && Array.isArray(parameters) && parameters.length > 0) {
            parameters.forEach(parameter => {
                const item: any = {};
                if (Object.prototype.hasOwnProperty.call(parameter, 'name')) {
                    item.name = parameter.name;
                }
                if (Object.prototype.hasOwnProperty.call(parameter, 'shownName')) {
                    item.shownName = parameter.shownName;
                }
                if (Object.prototype.hasOwnProperty.call(parameter, 'value')) {
                    item.value = parameter.value;
                }
                results.push(item);
            });
        }
        return results;
    }

    /**
     * 交互面板跳转到代码视图前，收集控件信息，用于自动绑定新增的方法
     * @param propertyData 控件schema值
     * @param viewModelId 控件所属视图模型id
     * @param parameters 点击添加新方法后，交互面板返回的参数
     * @param controlTypeName 控件类型名称
     */
    function resolveNewMethodForCodeEditor(propertyData: any, viewModelId: string, parameters: any): { methodCode: string, methodName: string } {
        const eventCode = parameters.newFuncEvents && parameters.newFuncEvents.label || '';
        const eventName = parameters.newFuncEvents && parameters.newFuncEvents.name || '';
        const upperEventCode = eventCode.length > 1 ? (eventCode.slice(0, 1).toUpperCase() + eventCode.slice(1)) : eventCode;

        let methodCode = 'method';
        let methodName = '方法';

        // 方法编号：控件类型+事件编号；方法名称：控件名称+事件名称
        const { controlInfo } = parameters;
        if (controlInfo) {
            const controlType = controlInfo.type;
            const controlName = propertyData.title || propertyData.text || propertyData.caption || controlInfo.name;
            if (controlType && controlType.length > 1 && controlName) {
                const lowerControlType = controlType.slice(0, 1).toLowerCase() + controlType.slice(1);
                methodCode = `${lowerControlType}${upperEventCode}`.replace(/_/g, '').replace(/-/g, '');

                const joinChar = '';
                methodName = `${controlName}${joinChar}${eventName}`.replace(/_/g, '').replace(/-/g, '');

            }
        }

        newControllerMethodBindingInfo = {
            controlData: propertyData,
            viewModelId,
            componentId: formSchemaService.getComponentByViewModelId(viewModelId)?.id || '',
            eventCode,
            methodCode,
            methodName,
            setPropertyRelates: parameters.setPropertyRelates
        };


        return {
            methodCode,
            methodName
        };
    }

    /**
     * 代码视图返回新增方法的编号、名称后，由设计器将方法添加到控件上
     * @param methodCode 新增方法的编号
     * @param methodName 新增方法的名称
     */
    function bindNewMethodToControl(methodCode: string, methodName: string) {
        if (!newControllerMethodBindingInfo) {
            return;
        }
        const commandBuildInfo = webCmpBuilderService.getBuildInfo();
        if (!commandBuildInfo || !commandBuildInfo.webCmdId) {
            return;
        }
        const { eventCode, viewModelId } = newControllerMethodBindingInfo;
        const viewModel = formSchemaService.getViewModelById(viewModelId);

        // 1、控件绑定命令
        newControllerMethodBindingInfo.controlData[eventCode] = methodCode;

        // 2、控件绑定命令后，可能会需要联动其他属性的变更
        if (newControllerMethodBindingInfo.setPropertyRelates) {
            newControllerMethodBindingInfo.setPropertyRelates(newControllerMethodBindingInfo.controlData, null);
        }

        // 3、视图模型添加命令
        let commandId = new IdService().generate();
        const viewModelCommand = viewModel?.commands.find(co => co.code === methodCode && co.handlerName === methodCode && co.cmpId === commandBuildInfo.webCmdId);
        if (!viewModelCommand) {
            viewModel?.commands.push(
                {
                    id: commandId,
                    code: methodCode,
                    name: methodName,
                    params: [],
                    handlerName: methodCode,
                    cmpId: commandBuildInfo.webCmdId,
                    shortcut: {},
                    extensions: []
                }
            );
        } else {
            commandId = viewModelCommand.id;
        }

        // 4、webCmd添加构件
        if (formSchemaService.getCommands()) {
            let customCmd = formSchemaService.getCommands().find(cmd => cmd.id === commandBuildInfo.webCmdId);
            if (!customCmd) {
                customCmd = {
                    id: commandBuildInfo.webCmdId,
                    path: commandBuildInfo.relativePath,
                    name: `${commandBuildInfo.code}.webcmd`,
                    refedHandlers: []
                };
                formSchemaService.getCommands().push(customCmd);
            }
            if (!customCmd?.refedHandlers?.find(handler => handler.host === commandId && handler.handler === methodCode)) {
                customCmd?.refedHandlers?.push(
                    {
                        host: commandId,
                        handler: methodCode
                    }
                );
            }
        }

        newControllerMethodBindingInfo = null;
    }
    /**
     * 根据参数获取新控制器元数据信息，然后过滤出支持的方法，返回新的控制器元数据
     */
    function getSupportedControllerMetadata(controller: any): Promise<any> {
        const metadataInfo = formSchemaService.getFormMetadataBasicInfo();
        return new Promise((resolve, reject) => {
            metadataService.getPickMetadata(metadataInfo.relativePath, controller).then((response: any) => {
                const { content, code, nameSpace } = response?.metadata || {};
                if (content) {
                    const newController = JSON.parse(content);
                    // 筛选支持的方法
                    const supportedList = nameSpace.includes('.Front') ? newController.Commands : getSupportedControllerMethods(newController.Id, newController.Commands);
                    newController.Commands = supportedList;
                    resolve({ controller: newController, code, nameSpace });
                } else {
                    resolve(null);
                }
            }).catch((error) => {
                reject(error);
            });
        });
    }
    /**
     * 根据控制器元数据构造内置控制器
     * @param controller 
     * @param code 
     * @param nameSpace 
     * @returns 
     */
    function getInternalControllerFromControllerMetadata(controller: any, code, nameSpace = ''): any {
        // 构造内置控制器
        const importData = {
            /** 控制器名称 */
            controllerName: {
                id: controller.Id,
                /** 控制器编号 */
                label: controller.Code,
                /** 控制器中文名 */
                name: controller.Name,
                code: code,
                nameSpace: nameSpace
            },
            /** 当前控制器下的方法列表 */
            controllerList: [] as any
        };
        if (controller.Commands) {
            controller.Commands.forEach(commandsItem => {
                const controllerListItem = {
                    label: commandsItem.Code,
                    name: commandsItem.Name,
                    id: commandsItem.Id,
                    handlerName: commandsItem.Code,
                    targetComponent: undefined,
                    hasPath: false,
                    cmpId: '',
                    componentLists: [],
                    shortcut: {},
                    // true：表明为新增项，可以编辑
                    isRTCmd: commandsItem['isRTCmd'],
                    isNewGenerated: commandsItem['isNewGenerated'] || false,
                    extensions: [],
                    isInvalid: false,
                    property: [] as any
                };
                if (!commandsItem.Parameters) {
                    commandsItem['Parameters'] = [];
                    controllerListItem.property = [] as any;
                }
                commandsItem.Parameters.forEach(parameterItem => {
                    if (controllerListItem.property) {
                        controllerListItem.property.push({
                            name: parameterItem.Code,
                            value: '',
                            shownName: parameterItem.Name,
                            description: parameterItem.Description,
                            id: '',
                            editorType: '',
                            isRetVal: false,
                            parameterType: '',
                            origin: cloneDeep(parameterItem),
                            context: {
                                // 通用编辑器数据
                                generalData: {
                                    assembleOutline,
                                    assembleSchemaFieldsByComponent,
                                    assembleStateVariables
                                },
                                // 回调方法或状态机动作或目标组件数据列表
                                data: getEventParameterData(parameterItem.controlSource?.context?.data?.value) || [],
                            }
                        });
                    }
                });
                importData.controllerList.push(controllerListItem);
            });
        }
        return importData;
    }

    return { checkCommands, commandsChanged, generateInternalCommandList, viewModelDisplay, findParamtersPosition, addControllerMethod, viewModelDomChanged, getCommands, bindNewMethodToControl, getInternalControllerFromControllerMetadata, getSupportedControllerMetadata };
}
