export function useLocation() {
    function getUrlParam(key: string) {
        const URL = new URLSearchParams(location.search);
        return decodeURI(URL.get(key)|| '');
    }

    return {
        getUrlParam
    };
}
