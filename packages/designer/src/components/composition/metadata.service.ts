import axios from 'axios';
import { MetadataDto } from '../types';
import { useLocation } from './use-location';

export class MetadataService {

    private metadataBasePath = '/api/dev/main/v1.0/metadatas';

    /**
     * /api/dev/main/v1.0/mdservice
     */
    private metadataServicePath = '/api/dev/main/v1.0/mdservice';

    public getMetadataPath() {
        const { getUrlParam } = useLocation();
        let metadataPath = getUrlParam('id') || '';
        if (metadataPath && metadataPath.startsWith('/')) {
            metadataPath = metadataPath.slice(1, metadataPath.length);
        }
        if (metadataPath && metadataPath.endsWith('/')) {
            metadataPath = metadataPath.slice(0, metadataPath.length - 1);
        }
        return metadataPath;
    }

    /**
     * 根据元数据Id查询元数据
     * @param relativePath 相对路径
     * @param metadataId 元数据id
     * @returns
     */
    public queryMetadataById(relativePath: string, metadataId: string): Promise<any> {
        const url = this.metadataBasePath + '/relied?metadataPath=' + relativePath + '&metadataID=' + metadataId;
        return axios.get(url);
    }

    /**
     * 根据元数据类型查询元数据
     * @param relativePath
     * @param metadataType
     * @returns
     */
    public GetMetadataListByType(relativePath: string, metadataType: string): Promise<any> {
        const url = this.metadataBasePath + '?path=' + relativePath + '&metadataTypeList=' + metadataType;
        return axios.get(url);
    }

    /**
     * 获取当前工程下或者其他元数据包中依赖的元数据
     * @param {?} path
     * @param {?} metadataId
     * @param {?} sessionId
     * @return {?}
     */
    public GetRefMetadata(path, metadataId): Promise<any> {
        // gsp.cache.get('sessionId');
        const url = this.metadataBasePath + '/relied?metadataPath=' + path + '&metadataID=' + metadataId;
        return axios.get(url);
    }

    /**
     * 获取当前工程下所有的元数据包
     * @param {?} spacePath
     * @param {?} typeName
     * @return {?}
     */
    public GetMetadataList(spacePath, typeName) {
        // var headers = new HttpHeaders().set('SessionId', this.sessionId);
        const url = this.metadataServicePath + '?path=' + spacePath + '&metadataTypeList=' + typeName;
        return axios.get(url);
    };

    public getMetadataListInSu(relativePath: string, metaddataType: string) {
        const url = this.metadataServicePath + '/metadataListInSu?path=' + relativePath + '&metadataTypeList=' + metaddataType;
        return axios.get(url);
    }

    /**
     * 获取最近使用的元数据
     * @param relativePath
     * @param metaddataType
     * @returns
     */
    public getRecentMetadata(relativePath: string, metaddataType: string) {
        const url = this.metadataServicePath + '/getmdrecentuse?path=' + relativePath + '&metadataTypeList=' + metaddataType;
        return axios.get(url);
    }

    /**
     * 获取所有元数据
     * @param relativePath
     * @param metaddataType
     * @param pageSize
     * @returns
     */
    public getAllMetadataList(relativePath: string, metaddataType: string, pageSize = 1000) {
        const url = this.metadataServicePath + '/unionmdlist?pageIndex=1&pageSize=1000' +
            '&path=' + relativePath + '&metadataTypeList=' + metaddataType;

        return axios.get(url).then((res: any) => {
            const totalNum = res.data['page']['total'] || 0;
            if (totalNum > 1000) {
                return this.getAllMetadataList(relativePath, metaddataType, totalNum);
            }
            return res;
        });
    }

    public getPickMetadata(relativePath: string, data: any) {
        const url = this.metadataServicePath + '/pickMetadata?currentPath=' + relativePath;
        return axios.post(url, data).then((res: any) => {
            return res.data;
        });
    }

    public saveMetadata(metadataDto: any) {
        return axios.put(this.metadataBasePath, metadataDto);
    }

    public validateRepeatName(path: string, fileName: string) {
        const url = this.metadataBasePath + '/validation?path=' + path + '&fileName=' + fileName;
        return axios.get(url).then((res: any) => {
            return res.data;
        });
    }
    // Load元数据（外部调用）
    public loadMetadata(fullName: string, path: string) {
        const metadataFullPath = path.replace(/\\/g, '/') + '/' + fullName;
        const encMetadataFullPath = encodeURIComponent(metadataFullPath);
        const url = this.metadataBasePath + '/load?metadataFullPath=' + encMetadataFullPath;
        return axios.get(url).then((res: any) => {
            return res.data;
        });
    }
    // 初始化元数据实体
    public initializeMetadataEntity(metadataDto: MetadataDto) {
        let name_chs = metadataDto.name;
        let name_en = "";
        let name_cht = "";
        if (metadataDto.nameLanguage) {
            name_chs = metadataDto.nameLanguage['zh-CHS'] ? metadataDto.nameLanguage['zh-CHS'] : "";
            name_en = metadataDto.nameLanguage['en'] ? metadataDto.nameLanguage['en'] : "";
            name_cht = metadataDto.nameLanguage['zh-CHT'] ? metadataDto.nameLanguage['zh-CHT'] : "";
        }
        // tslint:disable-next-line:max-line-length
        const url = this.metadataBasePath + '/initialized?nameSpace=' + metadataDto.nameSpace + '&code=' + metadataDto.code + '&name=' + name_chs + '&name_cht=' + name_cht + '&name_en=' + name_en + '&type=' + metadataDto.type + '&bizObjectID=' + metadataDto.bizobjectID + '&metadataPath=' + metadataDto.relativePath + '&extendProperty=' + metadataDto.extendProperty;

        return axios.get(url).then((res: any) => {
            return res.data;
        });
    }
    // 新建元数据
    public createMetadata(metadataDto: MetadataDto) {
        const content = {
            'ID': metadataDto.id,
            'NameSpace': metadataDto.nameSpace,
            'Code': metadataDto.code,
            'Name': metadataDto.name,
            'FileName': metadataDto.fileName,
            'RelativePath': metadataDto.relativePath,
            'Content': metadataDto.content,
            'Type': metadataDto.type,
            'BizobjectID': metadataDto.bizobjectID,
            'ExtendProperty': metadataDto.extendProperty,
            'NameLanguage': !metadataDto.nameLanguage ? null : metadataDto.nameLanguage,
        };
        const url = this.metadataBasePath;

        return axios.post(url, content, {
            params: {}, // 可以指定请求的URL参数
            responseType: 'stream'
        }
        ).then((res: any) => {
            return { ok: res.status === 204 };
        });
    }
}
