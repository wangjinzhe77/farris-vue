/**
 * 将现有表单元数据转换成vue设计器要求的结构
 */
export class FormMetadataConverter {

    private componentTypeMapper: Record<string, string> = {
        Tab: 'tabs',
        ToolBar: 'response-toolbar',
        QueryScheme: 'query-solution',
        Form: 'response-form',
        Header: 'page-header',
        GridField: 'data-grid-column',
        TreeGridField: 'data-grid-column',
        TabToolbarItem: 'tab-toolbar-item',
        TabPage: 'tab-page',
        ResponseToolbarItem: 'response-toolbar-item'
    };

    private formGroupMapper: Record<string, string> = {
        EnumField: 'combo-list',
        LookupEdit: 'input-group',
        TextBox: 'input-group',
        NumericBox: 'number-spinner',
        DateBox: 'date-picker',
        SwitchField: 'switch',
        RadioGroup: 'radio-group',
        CheckBox: 'check-box',
        Avatar: 'avatar',
        CheckGroup: 'check-group',
        MultiTextBox: 'textarea',
        RichTextBox: 'textarea',
        OrganizationSelector: 'input-group',
        PersonnelSelector: 'input-group'
    };
    /** 将表单schema字段中的editor类型转为vue控件类型 */
    public getRealEditorType(type: string) {
        return this.formGroupMapper[type || 'TextBox'] || type;
    }
    public convertDesignerMetadata(formSchema: any) {
        this.convertEntity(formSchema);
        this.convertComponents(formSchema.module.components);
    }

    /**
     * 转换控件类型
     * @param component
     * @returns
     */
    private convertComponentType(component: any) {
        const originalComponentType = component.type;

        // 输入类控件
        if (this.formGroupMapper[originalComponentType]) {
            component.type = 'form-group';
            component.label = component.title;
            component.editor = {
                type: this.formGroupMapper[originalComponentType]
            };

            const properties = Object.keys(component);

            // 如果属性不是id等属性，则将其移到editor对象中
            for (const prop of properties) {
                if (!["id", "type", "editor", "label", "appearance", "style", "binding", "visible", "path"].includes(prop)) {
                    component.editor[prop] = component[prop];
                    // 删除原对象上的该属性
                    delete component[prop];
                }
            }
        }

        // 类型转换
        if (this.componentTypeMapper[originalComponentType]) {
            component.type = this.componentTypeMapper[component.type];
            return;
        }
        // 统一处理：将camel-case 转为kebab-case
        component.type = component.type.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
    }

    /**
     * 转换子组件的componentType值
     */
    private convertChildComponentType(component: any) {
        if (component && component.type === 'component' && component.componentType) {
            component.componentType = component.componentType.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
        }
    }

    private convertComponents(components: any[]) {
        components.forEach((component: any) => {
            this.convertComponentType(component);
            this.convertChildComponentType(component);
            if (component.contents && component.contents.length) {
                this.convertComponents(component.contents);
            }
            if (component.type === 'tab-page' && component.toolbar) {
                this.convertComponentType(component.toolbar);
                if (component.toolbar.contents && component.toolbar.contents.length) {
                    this.convertComponents(component.toolbar.contents);
                }
            }

        });
    }

    /**
     * 转换实体节点
     */
    private convertEntity(domMetadata: any) {
        if (domMetadata.module.schemas) {
            domMetadata.module.entity = domMetadata.module.schemas;
        }
    }

}
