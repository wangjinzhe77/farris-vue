import { inject, reactive, ref } from "vue";
import { ComponentSchema } from "@farris/ui-vue/components";
import { FormComponent, FormSchemaEntity, FormSchemaEntityField, UseFormSchema } from "../types";

export interface ComponentTreeNode {
    data: Partial<ComponentSchema>,
    id: string;
    code: string;
    name: string;
    layer: number;
    parent: any;
    parentId?: string | null;
    hasChildren: boolean;
}

export interface ViewModelTreeNode {
    children: ViewModelTreeNode[];
    data: { id: string, code: string, name: string, statePath: string };
    id?: string;
    code?: string;
    selectable?: boolean;
    expanded?: boolean;
}
export function useParameterEditorData(useFormSchemaComposition: UseFormSchema) {
    // 参数编辑器左侧组件配置
    /**
     * 将实体内的字段组装为树结构
     */
    function resolveFieldNodesInEntity(fields: FormSchemaEntityField[], layer: number, parentNode: any, treeViewData: any[] = []) {
        fields.forEach(field => {
            const fieldTreeData = {
                data: field,
                id: field.id,
                name: field.name,
                expanded: true,
                nodeType:'field',
                layer,
                parent: parentNode && parentNode.id,
                parentNode,
                hasChildren: false
            };
            treeViewData.push(fieldTreeData);
            // 关联表字段 / UDT字段
            if (field.type && field.type.fields && field.type.fields.length > 0) {
                fieldTreeData.hasChildren = true;
                resolveFieldNodesInEntity(field.type.fields, layer + 1, fieldTreeData, treeViewData);
            }
        });
    }

    /**
     * 组装实体树绑定数据
     */
    function resolveEntityTreeData(entity: FormSchemaEntity, layer: number, parentNode: any, treeViewData: any[] = []) {
        const entityTreeData = {
            data: entity,
            id: entity.id,
            name: entity.name,
            expanded: true,
            nodeType: 'entity',
            layer,
            parent: parentNode && parentNode.id,
            parentNode,
            hasChildren: true
        };
        treeViewData.push(entityTreeData);

        if (entity.type && entity.type.fields && entity.type.fields.length > 0) {
            resolveFieldNodesInEntity(entity.type.fields, layer + 1, entityTreeData, treeViewData);
        }

        if (entity.type.entities && entity.type.entities.length > 0) {
            const childentityTreeData = {
                id: `childEntity_${entity.id}`,
                name: '子表',
                layer: layer + 1,
                parent: entity.id,
                hasChildren: true,
                parentNode: entityTreeData
            };
            treeViewData.push(childentityTreeData);

            entity.type.entities.forEach((childEntity: any) => {
                resolveEntityTreeData(childEntity, layer + 2, childentityTreeData, treeViewData);

            });
        }
    }
    // const useFormSchemaComposition: any = inject('useFormSchema');
    function schemaFieldsToTree(treeData: any[], schemaFields: any[],
        path: string, layer: number, parent: string | null, bindTo: string) {
        if (!schemaFields || !schemaFields.length) {
            return treeData; // 传入的参数为空或长度为0，直接返回空数组
        }
        schemaFields.forEach(field => {
            const isComplexField = field.$type === 'ComplexField' && field.type && field.type.fields && field.type.fields.length;
            const treeItem = {
                data: { ...field, path, bindTo }, id: field.id, code: field.code,
                name: field.name, layer: layer + 1, parent, hasChildren: false
            };
            // 列卡表单等场景中字段会在不同的组件中重复展示，id会重复，故将树节点id重置。
            treeItem.data.id = treeItem.data.id + '_' + path;
            treeItem.id = treeItem.id + '_' + path;
            if (isComplexField) {
                treeItem.hasChildren = true;
                schemaFieldsToTree(treeData, field.type.fields,
                    `${path}/${field.bindingPath}`, treeItem.layer, treeItem.id, bindTo);
            }
            treeData.push(treeItem);
        });
        return treeData;
    }
    // 表单组件组装数据
    function assembleOutline() {
        const formSchema = useFormSchemaComposition.getFormSchema();
        const allComponents = formSchema.module.components;
        //  表单组件
        const rootComponentId = 'root-component';
        let rootComponent = allComponents.find(item => item.id === rootComponentId);
        if (!rootComponent) {
            rootComponent = allComponents[0];
        }
        const data: ComponentTreeNode[] = [];
        allComponents.forEach((cmp: FormComponent) => {
            const cmpTreeData: ComponentTreeNode = {
                data: cmp,
                id: cmp?.id,
                code: cmp?.code || cmp?.id,
                name: cmp?.name || cmp?.id,
                layer: 0,
                parent: null,
                hasChildren: true
            };
            data.push(cmpTreeData);
        });
        return data;
    }

    function assembleSchemaFieldsByComponent() {
        const formSchema = useFormSchemaComposition.getFormSchema();
        const entity = formSchema.module.entity[0];
        const data: Omit<ComponentTreeNode, 'children'>[] = [];
        entity.entities.forEach(entity => {
            resolveEntityTreeData(entity, 1, null, data);
        });
        // allComponents.forEach(component => {
        //     const cmp = useFormSchemaComposition.getComponentById(component.id);
        //     const { bindTo } = useFormSchemaComposition.getViewModelById(cmp?.viewModel || '') || { bindTo: '' };
        //     const cmpTreeNode: Omit<ComponentTreeNode, 'children'> = {
        //         data: { ...component, name: component.id, bindTo },
        //         id: component.id,
        //         code: component.code || component.id,
        //         name: component.name || component.id,
        //         layer: 0,
        //         parent: null,
        //         parentId: null,
        //         hasChildren: true
        //     };
        //     data.push(cmpTreeNode);
        //     const schemaFields = useFormSchemaComposition.getFieldsByViewModelId(component.viewModel) || [];
        //     // schemaFields.
        //     schemaFieldsToTree(data,
        //         schemaFields, component.id, 0, cmpTreeNode.id, bindTo);
        // });
        return data;
    }

    function assembleStateVariables() {
        const formSchema = useFormSchemaComposition.getFormSchema();
        const { viewmodels } = formSchema.module;
        if (!viewmodels || !viewmodels.length) {
            return;
        }
        const data: ViewModelTreeNode[] = [];
        viewmodels.forEach(viewModel => {
            if (!viewModel || !viewModel.states || !viewModel.states.length) {
                return;
            }
            const vmNode: ViewModelTreeNode = {
                data: { id: viewModel.id, code: viewModel.code, name: viewModel.name + '视图模型', statePath: viewModel.id },
                id: viewModel.id,
                code: viewModel.code || viewModel.id,
                children: [] as ViewModelTreeNode[],
                selectable: false,
                expanded: true
            };
            data.push(vmNode);
            const componentId = useFormSchemaComposition.getComponentByViewModelId(viewModel.id)?.id;
            viewModel.states.forEach(state => {
                vmNode.children.push({
                    data: { ...state, statePath: `/${componentId}/${state.code}` },
                    selectable: true
                } as any);
            });
        });
        return data;
    }

    return {
        assembleOutline,
        assembleSchemaFieldsByComponent,
        assembleStateVariables,
    };
}
