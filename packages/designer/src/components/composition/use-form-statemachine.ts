import { UseFormSchema, UseFormStateMachine } from "../types";
import { MetadataService } from "./metadata.service";

export default function (useFormSchemaComposition: UseFormSchema): UseFormStateMachine {
    let stateMachineMetadata: any;

    let renderStateData: any = [];

    const metadataService = new MetadataService();

    /**
     * 获取状态机中的可视化状态数据
     */
    function getRenderStates() {
        return renderStateData || [];
    }

    /**
     * 提取状态机中的可视化状态数据
     */
    function resolveRenderStateData(stateMachineCode: string) {
        renderStateData = [];
        Object.keys(stateMachineMetadata.renderState).forEach(item => {
            const state = stateMachineMetadata.renderState[item];
            renderStateData.push({
                id: item,
                name: state.name,
                exist: '是',
                stateMachineId: stateMachineCode
            });
        });
    }
    /**
     * 获取状态机元数据
     */
    function queryStateMachineMetadata(): void {
        const formSchema = useFormSchemaComposition.getFormSchema();
        const formBasicInfo = useFormSchemaComposition.getFormMetadataBasicInfo();
        if (!formSchema?.module || !formBasicInfo) {
            return;
        }
        const { stateMachines } = formSchema.module;
        if (stateMachines && stateMachines.length) {
            const { uri: stateMachineID, id: stateMachineCode } = stateMachines[0];
            const { relativePath } = formBasicInfo;
            metadataService.queryMetadataById(relativePath, stateMachineID).then(result => {
                if (result?.data?.content) {
                    stateMachineMetadata = JSON.parse(result.data.content);
                }
                resolveRenderStateData(stateMachineCode);
            });
        }
        return;
    }
    function getStateMachineMetadata() {
        return stateMachineMetadata;
    }

    return { getStateMachineMetadata, queryStateMachineMetadata, getRenderStates };
}
