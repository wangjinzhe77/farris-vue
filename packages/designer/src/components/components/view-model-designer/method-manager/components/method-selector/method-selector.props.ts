import { ExtractPropTypes } from "vue";

export const methodSelectorProps = {

} as Record<string, any>;

export type MethodSelectorProps = ExtractPropTypes<typeof methodSelectorProps>;
