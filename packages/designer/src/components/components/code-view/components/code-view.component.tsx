/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, inject, provide, ref, SetupContext } from 'vue';
import { codeViewProps, CodeViewProps } from '../props/code-view.props';
import FNavTreeDesign from './nav-tree.component';
import { FSplitter, FSplitterPane, FNotifyService, FLoadingService } from '@farris/ui-vue/components';
import './code-view.scss';
import FDesignCodeTabs from './code-tabs.component';
import { EditorController } from '../composition/handler/editor.controller';
import FEditorPanelsDesign from './editor-panels.component';
import { throttle } from 'lodash-es';
import { CommonEvent } from '../composition/event-bus/lib/event-bus';

export default defineComponent({
    name: 'FCodeViewDesign',
    props: codeViewProps,
    emits: ['changeView', 'saveAll'] as (string[] & ThisType<void>) | undefined,
    setup(props: CodeViewProps, context: SetupContext) {
        const notifyService: any = new FNotifyService();
        const loadingService: any = inject<FLoadingService>('FLoadingService');
        // 顶部标签页
        const tabInstance = ref();
        // 编辑器面板
        const editorPanelsInstance = ref();
        const treeInstance = ref();
        const editorController = new EditorController();
        const eventBusId = ref(editorController.getEventBusId());
        const filePath = ref('');
        provide('editorService', editorController);
        function switchViewChangeHandler(ev) {
            context.emit('changeView', 'designer');
        }
        function detectFileDirtyHandler(info) {
            context.emit('detectFileDirty', info)
        }
        editorController.init(tabInstance, editorPanelsInstance, detectFileDirtyHandler);
        /**
         * 处理保存按钮点击事件
         */
        async function handleSaveBtnClicked(): Promise<void> {
            const results = await editorController.doSaveAllFile(props.directlyNotifySaveAllResults);
            context.emit('saveAll', results);
        }

        /**
         *
         * @returns
         */
        function renderSwitchButton() {
            return (
                <div class="switch-btn">
                    <div class="view-type-panel">
                        <div onClick={(event) => switchViewChangeHandler(event)}>
                            <div>
                                <span class="f-icon f-icon-perspective_view"></span>设计器
                            </div>
                        </div>
                        <div class="active">
                            <div>
                                <span class="f-icon f-icon-source-code"></span>代码
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        function selectRowHandler(data) {
            editorController.openFile(data.path);
            filePath.value = data.path;
        }

        function refreshNavTree(tsFilePath) {
            treeInstance.value.reloadTreeData(tsFilePath);
        }
        function open(path: string) {
            editorController.openFile(path);
            filePath.value = path;
        }
        function saveHandler(event) {
            event && event.stopPropagation();
            throttle(() => {
                handleSaveBtnClicked();
            }, 600)();
        }
        /**
         * 关闭标签页
         * @param tab 
         */
        function beforeCloseTab(tab) {
            editorController.closeFile(tab.id);
        }
        /**
         * 选中标签页
         * @param tab 
         */
        function selectTab(tab) {
            editorController.handleTabSelected(tab.id);
        }
        /**
         * 向标签页发送新通知
         * @param path 标签页路径
         * @param event 通知事件
         * @returns 被通知的标签页针对该事件反馈的结果（如果标签页不存在则返回null）
         */
        function sendNotification(path: string, event: CommonEvent) {
            return editorController.sendNotification(path, event);
        }
        context.expose({ refreshNavTree, open, sendNotification });

        return () => {
            return (
                <div class="code-editor-wrapper">
                    <div class="tab-bar">
                        {renderSwitchButton()}
                        <div class="tab-bar-divider"></div>
                        <FDesignCodeTabs ref={tabInstance} onBeforeClose={(tab) => beforeCloseTab(tab)} onSelected={(tab) => selectTab(tab)}></FDesignCodeTabs>
                        <div class="btn-tool-bar ml-auto">
                            <button class="btn btn-primary app-custom-save-btn" onClick={(event) => saveHandler(event)}>保存</button>
                        </div>
                    </div>
                    <div class="f-utils-fill-flex-column">
                        <FSplitter>
                            <FSplitterPane position={'left'} resizable={true} width={300}>
                                <FNavTreeDesign ref={treeInstance} entryFilePath={props.entryFilePath} onSelectRow={(data) => selectRowHandler(data)}></FNavTreeDesign>
                            </FSplitterPane>
                            <div class="editor-area f-utils-fill">
                                <FEditorPanelsDesign ref={editorPanelsInstance} eventBusId={eventBusId.value}></FEditorPanelsDesign>
                            </div>
                        </FSplitter>
                    </div>
                </div>
            );
        };
    }
});
