/**
 * 去除标识串中作为id的非法字符，仅保留大小写英文字母、数字、中划线、下划线
 * @param id 标识串
 */
export function getValidId(id: string): string {
    return id.split('').filter((char) => {
        const regExp = /^[0-9a-zA-Z-_]{1}$/g;
        return regExp.test(char);
    }).join('');
}
