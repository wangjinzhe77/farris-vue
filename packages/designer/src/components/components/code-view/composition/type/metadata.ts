/**
 * 元数据类型 MetadataDto.type
 */
export enum MetadataType {
  Form = "Form",
  ResourceMetadata = "ResourceMetadata",
  StateMachine = "StateMachine",
  GSPViewModel = "GSPViewModel",
  ExternalApi = "ExternalApi",
  PageFlowMetadata = "PageFlowMetadata",
  WebCommand = "WebCommand",
  WebComponent = "WebComponent",
  BEMgrComponent = "BEMgrComponent",
  GSPBusinessEntity = "GSPBusinessEntity"
}
