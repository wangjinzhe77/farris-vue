import { Ref, computed, defineComponent, inject, onMounted, provide, ref, nextTick } from "vue";
import { ComponentSchema, DesignerComponentInstance, FDesignerCanvas, FTabs, FTabPage, propertyConfigSchemaMap, FSplitter, FSplitterPane, FDesignerToolbox, FPropertyPanel, FDesignerOutline } from "@farris/ui-vue/components";
import { FormDesignerProps, formDesignerProps } from "./form-designer.props";
import { useComponentSchemaService } from '../../composition/component-schema.service';
import MonacoEditor from '../monaco-editor/monaco-editor.component';
import modulePropertyConfig from '../../types/form-property-config.json';
import FEntityTreeView from '../entity-tree-view/components/entity-tree-view.component';
import { afterPropeControlPropertyChangedService } from "../../composition/control-property-changed.service";
import { UseControlCreator, UseDesignViewModel, UseFormSchema, UseSchemaService } from "../../types";

export default defineComponent({
    name: 'FFormDesigner',
    props: formDesignerProps,
    emits: [],
    setup(props: FormDesignerProps, context) {
        const propertyPanelInstance = ref();
        const schema = ref<any>(props.schema);
        const componentSchema = schema.value.module ? ref(schema.value.module.components[0]) : ref(schema.value);
        const componentId = ref(componentSchema['id'] || 'root-component');
        const dragulaCompostion = ref();
        const fillTabs = ref(true);
        const controlTreeRef = ref();
        const entityTreeRef = ref();

        function onCanvasInitialized(dragula: any) {
            dragulaCompostion.value = dragula;
        }

        const propertyConfig = ref();
        const propertyName = ref();
        const focusingSchema = ref();
        const schemaService = useComponentSchemaService();
        schemaService.load(componentSchema.value);
        provide('SchemaService', schemaService);
        const designViewModelUtils = inject('designViewModelUtils') as UseDesignViewModel;
        const schemaUtil = inject('schemaService') as UseSchemaService;

        const useFormSchema: any = inject('useFormSchema') as UseFormSchema;
        function onDesignItemClicked(schemaType: string, schemaValue: ComponentSchema, componentId: string, componentInstance: DesignerComponentInstance) {
            propertyName.value = schemaType;
            propertyPanelInstance?.value?.updateDesignerItem(componentInstance, componentId);
            focusingSchema.value = schemaValue;

            if (controlTreeRef.value && controlTreeRef.value.selectControlTreeNode) {
                controlTreeRef.value.selectControlTreeNode(schemaValue);
            }
        }

        function onCanvasChanged() {
            if (entityTreeRef.value && entityTreeRef.value.refreshEntityTree) {
                entityTreeRef.value.refreshEntityTree();
            }
            if (controlTreeRef.value && controlTreeRef.value.refreshControlTree) {
                controlTreeRef.value.refreshControlTree();
            }
        }

        function onPropertyChanged(event: any) {
            const { changeObject, designerItem } = event;
            if (changeObject.needRefreshControlTree && controlTreeRef.value && controlTreeRef.value.refreshControlTree) {
                controlTreeRef.value.refreshControlTree();
            }
            const afterPropeControlPropertyChanged = afterPropeControlPropertyChangedService(useFormSchema, designViewModelUtils, schemaUtil);
            afterPropeControlPropertyChanged.afterPropertyChanged(event);

            if (designerItem?.onPropertyChanged) {
                designerItem?.onPropertyChanged(event);
            }
        }

        const activeDesignerView = ref('formDesigner');

        /** 代码编辑器的显示文本 */
        const formSchemaCodes = ref('');
        function onChangeDesignerView(viewName: string) {
            activeDesignerView.value = viewName;
            if (viewName === 'formDesignerCode') {
                formSchemaCodes.value = JSON.stringify(schema.value, null, 4);
            }
        }

        const showDesignerView = computed(() => (itemType: string) => {
            return itemType !== activeDesignerView.value;
        });

        const formDesignerViewClass = computed(() => {
            return {
                'pl-2 pr-2 mr-2 d-flex': true,
                'f-designer-view-tabs-item': true,
                'active': activeDesignerView.value === 'formDesigner'
            };
        });

        const formDesignerCodeViewClass = computed(() => {
            return {
                'pl-2 pr-2 d-flex': true,
                'f-designer-view-tabs-item': true,
                'active': activeDesignerView.value === 'formDesignerCode'
            };
        });

        propertyConfigSchemaMap['Module'] = modulePropertyConfig;
        function onOutlineChanged(selectionSchema: any) {
            if (selectionSchema?.type === 'Module') {
                propertyName.value = 'Module';
                propertyPanelInstance?.value?.updateDesignerItem(null, selectionSchema.id);
                focusingSchema.value = selectionSchema;
            }
        }

        function reloadPropertyPanel() {
            propertyPanelInstance?.value.reloadPropertyPanel();
        }
        context.expose({ reloadPropertyPanel });

        function onEntityUpdated() {
            // focusingSchema.value = null;
            onCanvasChanged();
            nextTick(()=>{
                propertyPanelInstance?.value.updatePropertyConfig();
            });
        }
        return () => {
            return (
                <FSplitter class="f-designer-page-content">
                    <FSplitterPane class="f-designer-page-content-nav" width={300} position="left" resizable={true}>
                        <div class="f-utils-fill-flex-column">
                            <FTabs tabType='pills' justify-content='center' fill={fillTabs.value} customClass="f-designer-left-area">
                                <FTabPage id="outline" title="大纲">
                                    <FDesignerOutline ref={controlTreeRef} data={props.schema} onSelectionChanged={onOutlineChanged}></FDesignerOutline>
                                </FTabPage>
                                <FTabPage id="tools" title="工具箱">
                                    <FDesignerToolbox dragula={dragulaCompostion.value}></FDesignerToolbox>
                                </FTabPage>
                                {/* <FTabPage id="entity" title="实体">
                                    <FEntityTreeView ref={entityTreeRef} data={props.schema} dragula={dragulaCompostion.value} onEntityUpdated={onEntityUpdated}></FEntityTreeView>
                                </FTabPage> */}
                            </FTabs>
                        </div>
                    </FSplitterPane>
                    <FSplitterPane class="f-designer-page-content-main" position="center">
                        <div class="f-utils-fill-flex-column">
                            <div class="f-utils-fill">
                                <div class="h-100 form-designer-view" hidden={showDesignerView.value('formDesigner')}>
                                    <FDesignerCanvas
                                        v-model={componentSchema.value}
                                        onInit={onCanvasInitialized}
                                        onSelectionChange={onDesignItemClicked}
                                        onCanvasChanged={onCanvasChanged}
                                        componentId={componentId.value}
                                    ></FDesignerCanvas>
                                    <FPropertyPanel
                                        ref={propertyPanelInstance}
                                        propertyConfig={propertyConfig.value}
                                        propertyName={propertyName.value}
                                        schema={focusingSchema.value}
                                        onPropertyChanged={onPropertyChanged}
                                    ></FPropertyPanel>
                                </div>
                                <div hidden={showDesignerView.value('formDesignerCode')} class="h-100" style="padding-left: 10px;">
                                    <MonacoEditor v-model={formSchemaCodes.value} language={"json"} readOnly={true}></MonacoEditor>
                                </div>
                            </div>
                            <div class="d-flex flex-row" style="height: 40px;padding: 0 10px;align-items: center;background: white;z-index:850;">
                                <div onClick={() => onChangeDesignerView('formDesigner')}
                                    class={formDesignerViewClass.value}>可视化设计器</div>
                                <div onClick={() => onChangeDesignerView('formDesignerCode')}
                                    class={formDesignerCodeViewClass.value}>设计时代码</div>
                            </div>
                        </div>
                    </FSplitterPane>
                </FSplitter>
            );
        };
    }
});
