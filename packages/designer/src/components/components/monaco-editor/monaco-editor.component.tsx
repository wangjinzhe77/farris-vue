import { defineComponent, watch, onBeforeUnmount, onMounted, ref } from "vue";
import loader from "@monaco-editor/loader";
import { MonacoEditorProps, monacoEditorProps } from "./monaco-editor.props";

export default defineComponent({
    name: 'FMonacoEditor',
    props: monacoEditorProps,
    setup(props: MonacoEditorProps, context) {
        const editorContainer = ref();
        let editorInstance: any = null;
        const codeValues = ref(props.modelValue);

        async function getMonacoEditorConfig() {
            return fetch('assets/monaco-editor.config.json').then((config: Record<string, any>) => {
                return config.json();
            });
        }

        async function initMonacoEditor() {
            if (editorContainer.value && !editorInstance) {
                const config = await getMonacoEditorConfig();
                const { vsPath } = config;
                loader.config({ paths: { vs: window.location.origin + vsPath } });
                loader.config({ "vs/nls": { availableLanguages: { "*": "zh-cn" } } });

                loader.init().then((monaco) => {
                    editorInstance = monaco.editor.create(editorContainer.value, {
                        value: codeValues.value,
                        language: props.language,
                        theme: props.theme,
                        folding: true,
                        readOnly: props.readOnly
                    });
                });
            }
        }

        watch(() => props.modelValue, (newValue) => {
            codeValues.value = newValue;
            editorInstance?.setValue(newValue);
        });
        const resizeObserver = new ResizeObserver(entries => {
            editorInstance?.layout();
        });

        function getContent() {
            return editorInstance?.getValue();
        }

        onMounted(() => {
            initMonacoEditor();
            resizeObserver.observe(editorContainer.value);
        });

        onBeforeUnmount(() => {
            if (editorInstance) {
                editorInstance.dispose();
            }

            if (resizeObserver) {
                resizeObserver.unobserve(editorContainer.value);
                resizeObserver.disconnect();
            }
        });
        context.expose({ getContent });

        return () => {
            return <div class="monaco-editor h-100 w-100" ref={editorContainer}></div>;
        };
    }
});
