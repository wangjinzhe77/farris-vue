/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, inject, ref, SetupContext, watch, nextTick } from 'vue';
import { FButton } from '@farris/ui-vue/components';
import { FSearchBox } from "@farris/ui-vue/components";
import { FTreeView } from '@farris/ui-vue/components';
import { UseEntityTreeData } from '../composition/use-entity-tree-data';
import { entityTreeProps, EntityTreeProps } from './entity-tree-view.props';
import { useUpdateEntitySchema } from '../composition/use-update-entity-schema';
import { UseControlCreator, UseDesignViewModel, UseFormSchema, UseSchemaService } from '../../../../components/types';
import { cloneDeep } from 'lodash-es';
import '../composition/entity-tree-view.css';
import './entity-tree-view.scss';
import { RowOptions, VisualData } from '@farris/ui-vue/components';
import { FLoadingService } from '@farris/ui-vue/components';

export default defineComponent({
    name: 'FEntityTreeView',
    props: entityTreeProps,
    emits: ['entityUpdated'] as (string[] & ThisType<void>) | undefined,
    setup(props: EntityTreeProps, context: SetupContext) {
        const entityTreeRef = ref();
        const dragularCompostion = ref(props.dragula);
        const useFormSchema: any = inject('useFormSchema');
        const { treeViewData, resolveEntityTreeData, assignExpandState, setTreeDraggable, appendTreeToDragulaContainer } = UseEntityTreeData(useFormSchema);
        const schemaService = inject('schemaService') as UseSchemaService;
        const designViewModelUtils = inject('designViewModelUtils') as UseDesignViewModel;
        const controlCreatorUtils = inject('controlCreatorUtils') as UseControlCreator;
        const useUpdateEntitySchemaComposition = useUpdateEntitySchema(schemaService, useFormSchema, designViewModelUtils, controlCreatorUtils, context);
        const loadingService: FLoadingService | any = inject<FLoadingService>('FLoadingService');

        /** 原始实体数据 */
        const entityData = ref(props.data.module?.entity);

        /** 树节点图标 */
        const treeNodeIconsData = {
            fold: 'f-icon f-icon-file-folder-close text-primary mr-1',
            unfold: 'f-icon f-icon-file-folder-open text-primary mr-1',
            leafnodes: 'f-icon f-icon-preview mr-1'
        };

        /** 获取树表绑定数据 */
        function getTreeViewData() {
            if (!entityData.value) {
                return;
            }
            const oldTreeViewData = cloneDeep(treeViewData.value);
            treeViewData.value = [];
            const mainEntity = entityData.value[0].entities[0];
            resolveEntityTreeData(mainEntity, 0, null);

            assignExpandState(treeViewData.value, oldTreeViewData);
        }

        /** 刷新实体树 */
        function refreshEntityTree() {
            getTreeViewData();
            if (entityTreeRef.value && entityTreeRef.value.updateDataSource) {
                entityTreeRef.value.updateDataSource(treeViewData.value);

                nextTick(() => {
                    setTreeDraggable();
                });
            }
        }
        function onClick(payload: MouseEvent) {
            useUpdateEntitySchemaComposition.synchronizeFromViewObject(loadingService);
        }

        context.expose({ refreshEntityTree });

        getTreeViewData();

        // 配置行样式、行收折等特性
        const rowOptions: Partial<RowOptions> = {
            customRowStyle: (dataItem: any) => {
                return {
                    'occupied': dataItem?.isOccupied,
                    'no-drag': !dataItem?.draggable,
                    'drag-copy': dataItem?.draggable,
                    [`id=${dataItem.id}`]: true
                };
            },
            customRowStatus: (visualData: VisualData) => {
                if (visualData.collapse === undefined) {
                    visualData.collapse = visualData.raw.collapse;
                }
                return visualData;
            }
        };

        watch(
            () => props.dragula,
            (newValue: any) => {
                dragularCompostion.value = newValue;
                if (dragularCompostion.value?.getDragulaInstance) {
                    appendTreeToDragulaContainer(dragularCompostion.value.getDragulaInstance());
                }
            }
        );

        function onTreeNodeExpanded() {
            nextTick(() => {
                setTreeDraggable();
            });
        }
        function renderHeader() {
            return <div class="designer-schema-tree-header">
                {/* <FSearchBox style="width:100%"></FSearchBox> */}
                <div class="input-group-prepend d-flex toolbar-actions ml-1">
                    {/* <FButton customClass={{ "toolbar-action": true }} type="'link'" icon="fd-i-Family fd_pc-zhedieshouqi" title="收起"></FButton>
                    <FButton customClass={{ "toolbar-action": true }} type="'link'" icon="fd-i-Family fd_pc-xianshiID" title="显示标识"></FButton> */}
                    <FButton customClass={{ "toolbar-action": true }} type="'link'" icon="f-icon-recurrence" title="刷新" onClick={onClick}></FButton>
                </div>
            </div>;
        }

        return () => {
            return <div class="designer-schema-tree">
                <FTreeView ref={entityTreeRef} data={treeViewData.value} selectable={true} showTreeNodeIcons={true} treeNodeIconsData={treeNodeIconsData}
                    row-option={rowOptions}
                    onExpandNode={onTreeNodeExpanded}>
                    {{ header: renderHeader }}
                </FTreeView>
            </div>;
        };
    }
});
