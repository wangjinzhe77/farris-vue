
import { FLoadingService,FTooltipDirective, FMessageBoxService, F_MODAL_SERVICE_TOKEN, FModalService, LookupSchemaRepositoryToken, FieldSelectorRepositoryToken, F_NOTIFY_SERVICE_TOKEN, FNotifyService, ControllerSchemaRepositorySymbol } from "@farris/ui-vue/components";
import { App } from "vue";
import { MetadataService } from "./components/composition/metadata.service";
import { MetadataPathToken, MetadataServiceToken } from "./components/types";
import { LookupFieldSelectorService, LookupSchemaService } from "./components/composition/schema-repository";
import { ControllerSelectorSchemaService } from "./components/composition/schema-repository/controller/controller-selector.service";

export default {
    install(app: App): void {
        app.provide(F_MODAL_SERVICE_TOKEN, new FModalService(app));
        app.provide('FLoadingService', FLoadingService);

        const metadataService = new MetadataService();
        app.provide(MetadataServiceToken, metadataService);

        const metadataPath = metadataService.getMetadataPath();
        app.provide(MetadataPathToken, metadataPath);

        app.provide('FMessageBoxService', FMessageBoxService);

        app.provide(LookupSchemaRepositoryToken, new LookupSchemaService(metadataService));
        app.provide(FieldSelectorRepositoryToken, new LookupFieldSelectorService(metadataService));
        app.provide(F_NOTIFY_SERVICE_TOKEN, new FNotifyService());
        app.provide(ControllerSchemaRepositorySymbol, new ControllerSelectorSchemaService(metadataService));

        app.directive('tooltip', FTooltipDirective);
    }
};
