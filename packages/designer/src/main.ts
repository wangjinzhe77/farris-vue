import { createApp } from 'vue';
import './style.css';
import App from './app.vue';
import Designer from './components/index';

import Providers from './app-providers';
createApp(App).use(Designer).use(Providers).mount('#app');
