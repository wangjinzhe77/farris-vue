import { fileURLToPath, URL } from 'node:url';
import banner from 'vite-plugin-banner';

import { formatDate } from 'date-fns';

const currentTime = () => formatDate(new Date(), 'yyyy-MM-dd HH:mm:ss');

export default {
    lib: {
        entry: fileURLToPath(new URL('./components/index.ts', import.meta.url)),
        name: "farris-ui-vue",
        fileName: "ui-vue",
        formats: ['systemjs'],
    },
    systemjs: true,
    outDir: fileURLToPath(new URL('./dist', import.meta.url)),
    externals: {
        include: [''],
        filter: (externals) => {
            return (id) => {
                return externals.find((item) => item && id.indexOf(item) === 0);
            };
        }
    },
    externalDependencies: true,
    minify: 'terser',
    alias: [
        { find: '@', replacement: fileURLToPath(new URL('./', import.meta.url)) },
        { find: '@/components', replacement: fileURLToPath(new URL('./components', import.meta.url)) },
        { find: '@farris/ui-vue/components', replacement: fileURLToPath(new URL('./components', import.meta.url)) },
        { find: '@farris/mobile-ui-vue', replacement: fileURLToPath(new URL('./components', import.meta.url)) }
    ],
    plugins: [
        banner('Last Update Time: ' + currentTime())
    ]
};
