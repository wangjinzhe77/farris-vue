# Switch 开关

Switch 组件用来切换数据状态。

## 基本用法
 
:::vdemo

```vue
{demos/switch/basic.vue}
```
:::

## 禁用
 
:::vdemo

```vue
{demos/switch/disable.vue}
```
:::

## 尺寸
 
:::vdemo

```vue
{demos/switch/size.vue}
```
:::

## 颜色
 
:::vdemo

```vue
{demos/switch/color.vue}
```
:::

## 文字
 
:::vdemo

```vue
{demos/switch/text.vue}
```
:::

## 事件
 
:::vdemo

```vue
{demos/switch/events.vue}
```
:::
## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| modelValue     | `boolean` | --     | 绑定值 |
| disable     | `boolean` | false     | 禁用状态 |
| onBackground     | `string` | --     | 开启时背景色 |
| offBackground     | `string` | --     | 关闭时背景色 |
| onColor     | `string` | --     | 开启时文本色 |
| offColor     | `string` | --     | 关闭时文本色 |
| onLabel     | `string` | --     | 开启时文字 |
| offLabel     | `string` | --     | 关闭时文字 |
| size     | `'small' | 'medium' | 'large'` | medium中号     | 切换标签的尺寸，small小号，medium中号，large大号 |

## 事件


| 事件名 | 参数                | 说明         |
| :----- | :------------------ | :----------- |
| update:modelValue  | boolean 当前状态| 更新开关状态事件 |
| onModelValueChanged  | -| 开关值变化事件 |



## 插槽

::: tip
暂无内容
:::
