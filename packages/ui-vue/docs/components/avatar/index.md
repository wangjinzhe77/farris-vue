# Avatar 头像

Avatar 组件用来展示人员头像。

## 基本用法

:::vdemo

```vue
{demos/avatar/basic.vue}
```
  
:::

## 头像形状

:::vdemo

```vue
{demos/avatar/type.vue}
```
  
:::
## 头像状态

:::vdemo

```vue
{demos/avatar/status.vue}
```
  
:::

## 属性

| 属性名        | 类型                  | 默认值     | 说明     |
| :-----        | :-------             | :-----     | :------- |
| avatarWidth   | `number`             | 100        | 头像宽度 |
| avatarHeight  | `number`             | 100        | 头像高度 |
| cover         | `string`             | 100        | 组件标识 |
| readonly      | `boolean`            | false      | 只读     |
| shape         | `'square' 、 'circle` | `'circle'` | 头像形状 |
| maxSize       | `number`             | 1          | 头像最大尺寸, 单位MB |
| modelValue    | `string`             | `''`       | 组件值    |  
| title         | `string`             | `''`       | 组件值    |
| imageType     | `string`             | `['jpeg']` | 组件值    |


## 插槽

::: tip
暂无内容
:::
