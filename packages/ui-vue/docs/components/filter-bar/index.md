# Filter Bar 筛选条

Filter Bar 组件用来进行简单筛选。

## 基本用法

:::vdemo

```vue
{demos/filter-bar/basic.vue}
```

:::

## 是否允许重置

:::vdemo

```vue
{demos/filter-bar/show_reset.vue}
```

:::

## 修改重置文字

:::vdemo

```vue
{demos/filter-bar/reset_text.vue}
```

:::

## 是否可选择

:::vdemo

```vue
{demos/filter-bar/mode.vue}
```

:::

## 类型

```typescript
type FilterMode = 'editable' | 'display-only'
type EditorType =
    | 'button-edit'
    | 'check-box'
    | 'combo-list'
    | 'combo-lookup'
    | 'date-picker'
    | 'date-range'
    | 'datetime-picker'
    | 'datetime-range'
    | 'month-picker'
    | 'month-range'
    | 'year-picker'
    | 'year-range'
    | 'input-group'
    | 'lookup'
    | 'number-range'
    | 'number-spinner'
    | 'radio-group'
    | 'text'
```

## 属性

| 属性名    | 类型                   | 默认值                                                                                                           | 说明         |
| :-------- | :--------------------- | :--------------------------------------------------------------------------------------------------------------- | :----------- |
| data      | `array<Condition>`     | '[{id: 'name',fieldCode: 'name',fieldName: '名称',compareType: '0',valueType: 0,value: '示例 1',relation: '1'}]' | 被绑定数据   |
| fields    | `array<FieldConfig>`   | '[{ id: 'name', code: 'name', labelCode:'name', name: '名称', editor: { type: 'text' } }]'                       | 筛选分类     |
| mode      | `string as FilterMode` | 'editable'                                                                                                       | 是否可选择   |
| resetText | `string`               | '清空已选'                                                                                                       | 清空值文字   |
| showReset | `boolean`              | false                                                                                                            | 是否显示重置 |

## FieldConfig

data 字段配置

| 属性名    | 类型      | 默认值              | 说明                         |
| :-------- | :-------- | :------------------ | :--------------------------- |
| id        | `string`  | ''                  | 字段 ID                      |
| code      | `string`  | ''                  | 字段标签，字段编号的唯一标识 |
| labelCode | `string ` | ''                  | 字段名称                     |
| name      | `string`  | ''                  | 名称                         |
| editor    | `string`  | '{type:EditorType}' | 编辑器类型                       |

## Condition

fields 字段配置

| 属性名    | 类型        | 默认值 | 说明       |
| :-------- | :---------- | :----- | :--------- |
| id        | `string`    | ''     | 唯一性标识 |
| fieldCode | `string `   | ''     | 字段编号   |
| fieldName | `string`    | ''     | 字段名称   |
| valueType | `ValueType` | ''     | 值类型     |
| value     | `string`    | ''     | 字段名称   |
