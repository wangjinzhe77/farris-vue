# Tree View 树列表组件

Tree View 组件以树形式展现层级结构数据。

## 基本用法

:::vdemo
```vue
{demos/tree-view/basic.vue}
```
:::

## 勾选树节点

:::vdemo
```vue
{demos/tree-view/selectable.vue}
```
:::

## 自动勾选节点

:::vdemo
```vue
{demos/tree-view/auto-check.vue}
```
:::

## 显示图标

:::vdemo
```vue
{demos/tree-view/show-icon.vue}
```
:::

## 显示动态图标
树节点图标来源于绑定数据，由属性iconField指定。
:::vdemo
```vue
{demos/tree-view/show-dynamic-icon.vue}
```
:::

## 显示连接线

:::vdemo
```vue
{demos/tree-view/show-lines.vue}
```
:::

## 获取返回值

:::vdemo
```vue
{demos/tree-view/get-selected-value.vue}
```
:::

## 合并返回值
当选择多条数据时，将具有相同父节点的返回值合并，仅返回共有的父节点
:::vdemo
```vue
{demos/tree-view/merge-cascade-value.vue}
```
:::

## 增加和删除树节点
:::vdemo
```vue
{demos/tree-view/add-remove.vue}
```
:::

## 选中数据变化事件
:::vdemo
```vue
{demos/tree-view/on-selection-change.vue}
```
:::


## 属性

| 属性名                   | 类型                          | 默认值                             | 说明                         |
| :---------------------- | :----------------------------| :-------------------------------- | :--------------------------- |
| columns                 | `Array<DataColumn>`          | `[{field:'name'}]`                | 显示列                        |
| data                    | `Objecte`                    | `[]`                              | 绑定数据                      |
| treeNodeIconsData       | `[Object, String]`           | `{}`                              | 树节点图标数据                 |
| selectable              | `Boolean`                    | `false`                           | 树表是否支持可选状态            |
| autoCheckChildren       | `Boolean`                    | `true`                            | 选中父节点会自动选中子节点       |
| mergeCascadeValues      | `Boolean`                    | `true`                            | 返回值展示父节点和子节点         |
| showTreeNodeIcons       | `Boolean`                    | `false`                           | 是否显示图标集                 |
| showLines               | `Boolean`                    | `false`                           | 是否显示连接线                 |
| newDataItem             | `Function`                   | `() => { }`                       | 新增值                        |
| lineColor               | `String`                     | '#9399a0'                         | 连接线颜色                    |
| cellHeight              | `Number`                     | 28                                | 单元格高度                    |
| idField                 | `String`                     | 'id'                              | 标识字段                      |
| displayField            | `String`                     | 'name'                            | 显示字段                    |
| hierarchy               | `Object`                     | `{ hasChildrenField: 'hasChildren', layerField: 'layer', parentIdField: 'parent' }` | 分级信息             |

## 事件

| 事件名 | 类型                | 说明         |
| :----- | :------------------ | :----------- |
| outputValue      | `EventEmitter<string>` | 选择结果事件    ｜
| selectionChange  | `EventEmitter<string>` | 以选数据切换事件 |

## 插槽

::: tip
暂无内容
:::
