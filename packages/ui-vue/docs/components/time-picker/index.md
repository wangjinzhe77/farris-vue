# Time Picker 选择时间组件

Time Picker 组件用来选择时间。

## 基本用法

:::vdemo

```vue
{demos/time-picker/basic.vue}
```

:::

## 禁用

:::vdemo

```vue
{demos/time-picker/disable.vue}
```

:::

## 只读

:::vdemo

```vue
{demos/time-picker/readonly.vue}
```

:::

## 可编辑

:::vdemo

```vue
{demos/time-picker/editable.vue}
```

:::

## 默认展开显示值

:::vdemo

```vue
{demos/time-picker/default-open-value.vue}
```

:::

## 启用十二小时制

:::vdemo

```vue
{demos/time-picker/use-hours.vue}
```

:::

## 格式化

:::vdemo

```vue
{demos/time-picker/format.vue}
```

:::

## 显示标题

:::vdemo

```vue
{demos/time-picker/show-header.vue}
```

:::

## 指定步长

:::vdemo

```vue
{demos/time-picker/step.vue}
```

:::

## 禁止选择的小时

:::vdemo

```vue
{demos/time-picker/disabled-hours.vue}
```

:::

## 禁止选择分钟

:::vdemo

```vue
{demos/time-picker/disabled-minutes.vue}
```

:::

## 禁止选择秒

:::vdemo

```vue
{demos/time-picker/disabled-seconds.vue}
```

:::

## 事件

:::vdemo

```vue
{demos/time-picker/events.vue}
```

:::

## 属性

| 属性名               | 类型                                       | 默认值     | 说明                                 |
| :------------------- | :----------------------------------------- | :--------- | :----------------------------------- |
| modelValue           | string                                     | 空         | 绑定值，格式要与指定的 format 一致   |
| readonly             | boolean                                    | false      | 只读                                 |
| disabled             | boolean                                    | false      | 禁用                                 |
| editable             | boolean                                    | true       | 可编辑                               |
| placeholder          | string                                     | 空         | 提示文字                             |
| defaultOpenValue     | date                                       | new Date() | 默认时间控件下拉面板，展开时显示的值 |
| showHeader           | boolean                                    | false      | 时间下拉面板是否显示标题             |
| use12Hours           | boolean                                    | false      | 是否使用 12 小时制                   |
| format               | string                                     | 'HH:mm:ss' | 显示格式                             |
| hourStep             | number                                     | 1          | 小时步长值                           |
| minuteStep           | number                                     | 1          | 分钟步长值                           |
| secondStep           | number                                     | 1          | 秒步长值                             |
| disabledHours        | () => number[]                             |            | 禁止选择的小时                       |
| disabledMinutes      | (hour: number) => number[]                 |            | 禁止选择的分钟                       |
| disabledSeconds      | (hour: number, minute: number) => number[] |            | 禁止选择的秒                         |
| hideDisabledElements | boolean                                    | false      | 是否隐藏被禁用的元素                 |

## 事件

| 事件名      | 参数         | 说明       |
| :---------- | :----------- | :--------- |
| valueChange | 当前显示的值 | 值变更事件 |
| clear       | --           | 清除事件   |

## 插槽

::: tip
暂无内容
:::
