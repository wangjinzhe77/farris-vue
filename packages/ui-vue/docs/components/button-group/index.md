# Button Group 按钮组

Button Group 组件为不同使用场景提供了一组按钮展示样式。

## 基本用法

:::vdemo

```vue
{demos/button-group/basic.vue}
```

:::


## 禁用

:::vdemo

```vue
{demos/button-group/disabled.vue}
```

:::

## 展示模式

:::vdemo

```vue
{demos/button-group/mode.vue}
```

:::

## 尺寸

:::vdemo

```vue
{demos/button-group/size.vue}
```

:::

## 展示类型

:::vdemo

```vue
{demos/button-group/type.vue}
```

:::

## 指定显示个数

:::vdemo

```vue
{demos/button-group/count.vue}
```

:::

## 事件

:::vdemo

```vue
{demos/button-group/events.vue}
```

:::

## 类型

```typescript
type PlacementDirection = 'top' | 'bottom' | 'left' | 'right' | 'auto' | 'bottom-left'
type ButtonType = 'primary' | 'success' | 'info' | 'warning' | 'danger' | 'link' | 'secondary'
type ButtonSize = 'large' | 'small' | 'middle'
type ButtonGroupType = 'default' | 'group';
type ButtonItem = { id: string; text: string; type?: string; disable?: boolean; icon?: string }
```

## 属性

| 属性名    | 类型                 | 默认值    | 说明           |
| :-------- | :------------------- | :-------- | :------------- |
| data      | `Array<ButtonItem>`  | []        | 按钮信息       |
| count     | `number`             | 2         | 显示的按钮数量 |
| placement | `PlacementDirection` | 'bottom'  | 按钮展示位置   |
| type      | `ButtonType`         | 'primary' | 按钮的展示类型 |
| size      | `ButtonSize`         | 'middle'  | 按钮的展示尺寸 |
| mode      | `ButtonGroupType`         | 'default'  | 按钮的展示模式 |

## 事件

| 事件名      | 参数      | 说明                                           |
| :---------- | :-------- | :--------------------------------------------- |
| changeState | `boolean` | 下拉框展开或者收起的回调，参数是下拉框是否展开 |
| change      | `string`  | 触发按钮的回调，参数是触发按钮的 id            |
| click       | `ButtonItem`  | 触发按钮的回调，参数是触发按钮的项           |

## 插槽

::: tip
暂无内容
:::
