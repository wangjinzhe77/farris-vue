# Response Toolbar 响应式工具条

Response Toolbar 是一个可以根据屏幕尺寸自定义调整显示按钮个数的工具条组件。

## 基本用法

:::vdemo

```vue
{demos/response-toolbar/basic.vue}
```

:::


## 显示图标

:::vdemo

```vue
{demos/response-toolbar/useicon.vue}
```

:::

## 自定义样式

:::vdemo

```vue
{demos/response-toolbar/custom-class.vue}
```

:::

## 类型

```typescript
// 工具条对齐方式
type ButtonAlignment = 'left' | 'right'

// 按钮属性
interface ResponseToolbarItem {
    /** 工具栏项标识 */
    id: string;
    /** 图标 */
    icon: string;
    /** 文本 */
    text: string;
    /** 是否可用 */
    enable: boolean;
    /** 是否可见 */
    visible: boolean;
    /** 点击事件
     * $event: 鼠标点击事件对象
     * itemId: 当前点击的按钮标识 */ 
    onClick: ($event: MouseEvent, itemId: string) => void = () => { };
}
```

## 属性

| 属性名    | 类型                                                                                  | 默认值       | 说明                              |
| :-------- | :------------------------------------------------------------------------------------ | :----------- | :-------------------------------- |
| customClass | `String`                                                 |         | 工具条自定义样式                      |
| alignment | `String as PropType<ButtonAlignment>`                                                 | right        | 工具条对齐方式                        |
| items     | `Array<ResponseToolbarItem>`                                                          | []           | 按钮列表                         |



## 插槽

::: tip
暂无内容
:::
