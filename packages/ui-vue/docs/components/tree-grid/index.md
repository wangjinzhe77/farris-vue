# Tree Grid 树列表组件

Tree View 组件以树形式展现层级结构数据。


## 基本用法

:::vdemo

```vue
{demos/tree-grid/basic.vue}
```

:::

## 绑定树结构数据

:::vdemo

```vue
{demos/tree-grid/tree_node_data.vue}
```

:::

## 禁用

:::vdemo

```vue
{demos/tree-grid/disable.vue}
```

:::

## 尺寸

:::vdemo

```vue
{demos/tree-grid/change_size.vue}
```

:::

## 行号

:::vdemo

```vue
{demos/tree-grid/row_number.vue}
```

:::

## 启用选择行

:::vdemo

```vue
{demos/tree-grid/select_on_click.vue}
```

:::

## 勾选复选框选择行

:::vdemo

```vue
{demos/tree-grid/select_on_check.vue}
```

:::

## 勾选树节点

:::vdemo

```vue
{demos/tree-grid/multi_select.vue}
```

:::

## 自动勾选节点

:::vdemo

```vue
{demos/tree-grid/auto_check.vue}
```

:::

## 多选模式

:::vdemo

```vue
{demos/tree-grid/selection_mode.vue}
```

:::

## 选择数据范围

:::vdemo

```vue
{demos/tree-grid/selection_range.vue}
```

:::

## 设置选中数据

:::vdemo

```vue
{demos/tree-grid/set_selection_values.vue}
```

:::

## 显示全选

:::vdemo

```vue
{demos/tree-grid/show_select_all.vue}
```

:::


## 显示边框

:::vdemo

```vue
{demos/tree-grid/show_border.vue}
```

:::

## 显示配置按钮

:::vdemo

```vue
{demos/tree-grid/show_setting.vue}
```

:::

## 显示滚动条

:::vdemo

```vue
{demos/tree-grid/fit.vue}
```

:::

## 显示条纹

:::vdemo

```vue
{demos/tree-grid/show_stripe.vue}
```

:::

## 显示图标

:::vdemo

```vue
{demos/tree-grid/show_icon.vue}
```

:::

## 显示连接线

:::vdemo

```vue
{demos/tree-grid/show_lines.vue}
```

:::

## 新建和删除

:::vdemo

```vue
{demos/tree-grid/add_remove.vue}
```

:::

## 行编辑

:::vdemo

```vue
{demos/tree-grid/edit_local_data.vue}
```

:::

## 单元格编辑

:::vdemo

```vue
{demos/tree-grid/edit_local_data.vue}
```

:::

## 绑定数据

:::vdemo

```vue
{demos/tree-grid/binding_data.vue}
```

:::

## 设置列

:::vdemo

```vue
{demos/tree-grid/column.vue}
```

:::

## 自适应列宽

:::vdemo

```vue
{demos/tree-grid/fit_columns.vue}
```

:::

## 空数据模板

:::vdemo

```vue
{demos/tree-grid/empty.vue}
```

:::

## 属性

| 属性名             | 类型                | 默认值                                                                              | 说明                       |
| :----------------- | :------------------ | :---------------------------------------------------------------------------------- | :------------------------- |
| columns            | `Array<DataColumn>` | `[{field:'name'}]`                                                                  | 显示列                     |
| data               | `Objecte`           | `[]`                                                                                | 绑定数据                   |
| treeNodeIconsData  | `[Object, String]`  | `{}`                                                                                | 树节点图标数据             |
| selectable         | `Boolean`           | `false`                                                                             | 树表是否支持可选状态       |
| autoCheckChildren  | `Boolean`           | `true`                                                                              | 选中父节点会自动选中子节点 |
| mergeCascadeValues | `Boolean`           | `true`                                                                              | 返回值展示父节点和子节点   |
| showTreeNodeIcons  | `Boolean`           | `false`                                                                             | 是否显示图标集             |
| showLines          | `Boolean`           | `false`                                                                             | 是否显示连接线             |
| newDataItem        | `Function`          | `() => { }`                                                                         | 新增值                     |
| lineColor          | `String`            | '#9399a0'                                                                           | 连接线颜色                 |
| cellHeight         | `Number`            | 28                                                                                  | 单元格高度                 |
| idField            | `String`            | 'id'                                                                                | 标识字段                   |
| displayField       | `String`            | 'name'                                                                              | 显示字段                   |
| hierarchy          | `Object`            | `{ hasChildrenField: 'hasChildren', layerField: 'layer', parentIdField: 'parent' }` | 分级信息                   |

## 事件

| 事件名          | 类型                   | 说明             |
| :-------------- | :--------------------- | :--------------- |
| outputValue     | `EventEmitter<string>` | 选择结果事件 ｜  |
| selectionChange | `EventEmitter<string>` | 以选数据切换事件 |

## 插槽

::: tip
暂无内容
:::
