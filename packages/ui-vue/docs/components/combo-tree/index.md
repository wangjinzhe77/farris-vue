# Combo Tree 下拉树输入框

Combo Tree 组件提供下拉框形式选择树形数据。

## 基本用法

:::vdemo

```vue
{demos/combo-tree/basic.vue}
```

:::


## 类型

```typescript
type Placement = 'top' | 'bottom' | 'auto';
type ViewType = 'text' | 'tag';

```
## 属性 

| 属性名    | 类型      | 默认值   | 说明     |
| :-----   | :-------  | :-----  | :------- |
| id       | `string`  |  | 组件标识  |
| data | `Options` | `[]` | 数据源 |
| displayText | `String` | `` | 可选，展示文本 |
| editable | `boolean` | `false` | 是否可编辑|
| disabled | `boolean` | `false` | 是否禁用  |
| readonly | `boolean` | `false` | 是否只读      |
| dropDownIcon | `string` |  | 可选，下拉按钮图标 |
| enableClear | `boolean` | `true` | 可选，是否启用清空,单选且允许编辑下可用 |
| enableSearch | `boolean` | `false` | 可选，是否启用搜索 |
| enableTitle | `boolean` | `true` | 可选，鼠标悬停时是否显示控件值 |
| fitEditor | `boolean` | `false` | 自适应宽度 |
| forcePlaceholder | `boolean` | `false` | 可选，是否强制显示占位符 |
| hidePanelOnClear | `boolean` | `true` | 可选，清空值时隐藏面板 |
| idField | `string` | `id` | 可选，数据源id字段 |
| mapFields | `object` |  | 可选，字段映射 |
| maxHeight | `number` | `350` | 可选，下拉面板最大高度 |
| maxLength | `number` | `` | 最大输入长度 |
| multiSelect | `boolean` | `false` | 可选，是否支持多选 |
| placeholder | `string` | '' | 占位符 | 
| placement | `String as Placement` |  | 可选，下拉面板展示位置 | 
| remote | `object` | `null` | 可选，远端数据源信息 |
| remoteSearch | `boolean` | `false` | 可选，是否支持远端过滤 |
| separator | `string` | `,` | 可选，多选时值分隔符 |
| tabIndex | `number` | `-1` | tab index |
| valueField | `string` | `id` | 可选，数据源值字段 |
| textField | `string` | `name` | 可选，数据源显示字段 |
| titleField | `string` | `name` | 可选，数据源的title |
| viewType | `string as ViewType` | `name` | 可选，数据源的title |


## 事件 

值变化后对象属性 ChangeItemObject
| 属性名    | 类型      | 说明     |
| :-----   | :-------  | :------- |
| checked   | `boolean`  | 是否选中  |
| disabled | `boolean` | 是否禁用 |
| textField | `string` | 显示字段 |
| valueField | `string` |  值字段 |

| 事件名        | 类型                      | 说明                       |
| :-----------  | :------------------------ | :-------------------------|
| clear          | `null`                   | 清空方法                 |
| change       | `Array<ChangeItemObject> `        | 值变化回调                 |
## 插槽

::: tip
暂无内容
:::
