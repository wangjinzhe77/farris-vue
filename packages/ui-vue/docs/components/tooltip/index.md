# Tooltip 提示信息

Tooltip 组件用来实现鼠标移动至目标组件时，在其上显示提示信息。常见用于给按钮、图标设置少量提示的场景。

## 基本用法

:::vdemo

```vue
{demos/tooltip/basic.vue}
```

:::

## 自定义内容模板

:::vdemo

```vue
{demos/tooltip/custom.vue}
```

:::

## 触发方式

:::vdemo

```vue
{demos/tooltip/trigger.vue}
```

:::

## 提示内容模板

:::vdemo

```vue
{demos/tooltip/template.vue}
```

:::

## 指定提示消息的边界区域

:::vdemo

```vue
{demos/tooltip/reference.vue}
```

:::

## 指令属性

| 属性名             | 类型     | 默认值 | 说明                                                                                                                                                                                                                                                                                             |
| :----------------- | :------- | :----- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| id                 | `string` | --     | 组件标识                                                                                                                                                                                                                                                                                         |
| content            | `string` | --     | 提示内容，可以是字符串，可以是带 HTML 标签的字符串                                                                                                                                                                                                                                               |
| contentTemplate           | `HTMLElement` | --     | 模版提示内                                                                                                                                                                                                                                              |
| width              | `string` | --     | 给提示框设置固定的宽度，比如'200px'。<br>设置的值可以解除最大宽度的限制，只按照设置的值显示提示。                                                                                                                                                                                                |
| placement          | `string` | top    | 控制提示的朝向。目前支持的类型：<br>'top-left'朝上-偏左  \| 'top'朝上  \|'top-right'朝上-偏右  \|<br>'left-top:朝左-偏上  \|'left'朝左  \|'left-bottom'朝左-偏下  \|<br>'right-top'朝右-偏上  \|'right'朝右  \|'right-bottom'朝右-偏下 \|<br>'bottom-left'朝下-偏左 \|'bottom'朝下 \|'bottom-right'朝下-偏右 |
| trigger            | `string` | --     | 提示框的触发方式，'hover'鼠标滑过 \|'click'鼠标点击时。                                                                                                                                                                                                                                           |
| horizontalRelative | `string` | --     | 设置提示框显示位置的横向边界。提示框显示如果超出边界，就会转向。                                                                                                                                                                                                                                 |
| verticalRelative   | `string` | --     | 设置提示框显示位置的纵向边界。提示框显示如果超出边界，就会转向。                                                                                                                                                                                                                                 |

## 插槽

::: tip
暂无内容
:::
