# Button 按钮

Button 组件为不同使用场景提供了多种展示样式。

## 基本用法

:::demo

```vue
{demos/button/basic.vue}
```

:::

## 状态

:::vdemo

```vue
{demos/button/status.vue}
```

:::

## 尺寸

:::demo

```vue
{demos/button/size.vue}
```

:::


## 类型

```typescript
type ButtonType = 'primary' | 'warning' | 'danger' | 'success' | 'link' | 'secondary';
type SizeType = 'small' | 'middle' | 'large';
```
## 属性

| 属性名     | 类型                   | 默认值    | 说明                 |
| :--------- | :--------------------- | :-------- | :------------------- |
| id         | `string`               | --        | 标记组件的唯一标识   |
| type | `string as ButtonType` | 'primary' | 按钮显示样式         |
| disabled    | `boolean`              | false     | 将按钮设置为禁用状态 |
| size       | `string as SizeType`   | 'middle'   | 按钮尺寸             |

## 事件

| 事件名 | 类型                | 说明         |
| :----- | :------------------ | :----------- |
| click  | `EventEmitter<any>` | 点击按钮事件 |

## 插槽

::: tip
暂无内容
:::
