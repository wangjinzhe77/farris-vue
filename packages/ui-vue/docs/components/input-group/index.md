# InputGroup 智能输入框

增强型文本输入框，可自定义右侧按钮及文本齐方式

## 基本用法

:::vdemo

```vue
{demos/input-group/basic.vue}
```

:::


## 占位符

:::vdemo

```vue
{demos/input-group/placeholder.vue}
```

:::

## 状态

:::vdemo

```vue
{demos/input-group/state.vue}
```

:::

## 扩展区域

:::vdemo

```vue
{demos/input-group/extend.vue}
```

:::

## 样式

:::vdemo

```vue
{demos/input-group/style.vue}
```

:::

## 事件

:::vdemo

```vue
{demos/input-group/events.vue}
```

:::

## 属性

| 属性名                 | 类型              | 默认值      | 说明                                                                        |
| :--------------------- | :---------------- | :---------- | :-------------------------------------------------------------------------- |
| autocomplete           | `off，on`         | off         | 针对 txt 类型的输入框，是否启用自动完成.off 不启用自动完成，on 启用自动完成 |
| customClass            | `string`          | --          | 自定义样式                                                                  |
| disable                | `boolean`         | false       | 禁用输入功能，true 是禁用。                                                 |
| readonly               | `boolean`         | false       | 只读                                                                        |
| editable               | `boolean`         | true        | 是否允许编辑，true 是允许编辑                                               |
| enableClear            | `boolean`         | true        | 是否启用清空按钮，true 是当有文本时显示清空按钮                             |
| enableTitle            | `boolean`         | true        | 鼠标移动到组件内时显示组件内的文本，true 是启用提示                         |
| enableViewPassword     | `boolean`         | true        | 是否启用查看密码明文功能，true 是启用                                       |
| forcePlaceholder       | `boolean`         | false       | 是否始终显示占位符文本。true 始终显示占位符                                 |
| groupText              | `string`          | --          | 扩展按钮的文本                                                              |
| groupTextTemplate      | `templateRef`     | --          | 扩展按钮的模版                                                              |
| isPassword             | `boolean`         | false       | 是否启用密码模式                                                            |
| maxLength              | `number`          | --          | 设置文本最大长度                                                            |
| minLength              | `number`          | --          | 设置文本最小宽度                                                            |
| modelValue             | `string，boolean` | --          | 组件值                                                                      |
| noborder               | `boolean`         | false       | 是否隐藏边线，false 显示边线                                                |
| placeholder            | `string`          | --          | 启用提示信息文本                                                            |
| showButtonWhenDisabled | `boolean`         | --          | 当组件禁用或只读时显示后边的按钮                                            |
| tabIndex               | `number`          | --          | tab 索引                                                                    |
| textAlign              | `string`          | left 左对齐 | 文本在输入框中的对齐方式                                                    |
| useExtendInfo          | `boolean`         | false       | 是否启用前置扩展信息；鼠标滑过后在输入框前面 显示 ① 图标                    |
| extendInfo             | `string`          | --          | 前置扩展信息文本                                                            |
| value                  | `string`          | --          | 输入值                                                                      |

## 事件

| 事件名            | 参数                   | 说明                  |
| :---------------- | :--------------------- | :-------------------- |
| clear             | --                     | 点击清空按钮事件      |
| change            | 新值                   | 值变化事件            |
| blur              | MouseEvent             | 失去焦点事件          |
| click             | MouseEvent             | 输入框点击事件        |
| clickHandle       | {originalEvent:原事件} | 点击图标触发的事件    |
| focus             | MouseEvent             | 得到焦点事件          |
| input             | 新值                   | 值变化事件---         |
| keydown           | MouseEvent             | 键盘按下事件          |
| keyup             | MouseEvent             | 键盘弹起事件          |
| iconMouseEnter    | MouseEvent             | 进入图标区域事件      |
| iconMouseLeave    | MouseEvent             | 离开图标区域事件      |
| update:modelValue | --                     | modelValue 值更新事件 |
| update:value      | --                     | value 值更新事件      |
| updateExtendInfo  | --                     | 展示前置扩展信息事件  |

## 插槽

| 插槽名            | 说明               |
| :---------------- | :----------------- |
| groupTextTemplate | 扩展区域自定义模板 |
