# Popover 气泡提示

Tooltip 组件用来实现鼠标移动至目标组件时，或者点击目标时，在其上显示提示丰富的信息。

## 基本用法

:::vdemo
 
```vue
{demos/popover/basic.vue}
```

:::

## 设置标题

:::vdemo
 
```vue
{demos/popover/title.vue}
```

:::

## 设置最小宽度

:::vdemo
 
```vue
{demos/popover/min-width.vue}
```

:::

## 自适应宽度

:::vdemo
 
```vue
{demos/popover/fit-content.vue}
```

:::

## 自定义样式

:::vdemo
 
```vue
{demos/popover/custom-class.vue}
```

:::

## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| id     | `string` | --     | 组件标识 |
| title | `string` | -- | 可选，气泡标题 |
| placement | `PopoverPlacement` | 'bottom' | 可选，气泡显示的位置 |
| visible     | `boolean` | false     | 可选，弹框的初始化弹出状态 |
| zIndex     | `number` | -1    | 可选，z-index 值，用于手动控制层高 |
| fitContent | `boolean` | false | 是否自适应内容 |
| minWidth | `number` | -1 | 最小宽度 |
| class | `string` | -- | 可选，附加样式表 |
| reference | `object` | -- | 可选，目标元素 |
| keepWidthWithReference | `boolean` | false | 气泡宽度与目标元素宽度保持一致 |

## 事件

| 事件名 | 类型                | 说明         |
| :----- | :------------------ | :----------- |
| shown  | `EventEmitter<void>` | 气泡显示事件 |
| hidden  | `EventEmitter<void>` | 气泡隐藏事件 |


## 插槽

::: tip
暂无内容
:::
