# Uploader 附件上传组件

Uploader 选择文件上传，支持批量选择文件，通过列表方式展示已选择的附件、附件的进度。

-   上传功能，主要处理选择附件上传、展示附件上传状态、已上传附件、控制上传功能的启用禁用
-   预览功能，主要展示指定的附件数据，通过预览、下载、编辑这样预置的功能按钮，扩展对应的功能

## 上传

:::vdemo

```vue
{demos/uploader/upload.vue}
```

:::

## 上传配置

:::vdemo

```vue
{demos/uploader/upload-settings.vue}
```

:::

## 上传提示说明

:::vdemo

```vue
{demos/uploader/custom-info.vue}
```

:::

## 上传事件

:::vdemo

```vue
{demos/uploader/upload-events.vue}
```

:::

## 预览

:::vdemo

```vue
{demos/uploader/preview.vue}
```

:::

## 预览事件

:::vdemo

```vue
{demos/uploader/preview-events.vue}
```

:::

## 自定义显示列

:::vdemo

```vue
{demos/uploader/preview-columns.vue}
```

:::

## 禁用

:::vdemo

```vue
{demos/uploader/disabled.vue}
```

:::

## previewColumns 属性默认值

```typescript
[
    { field: 'name', width: 200, title: '文件名', checkbox: true },
    { field: 'size', width: 100, title: '大小' },
    { field: 'createTime', width: 100, title: '日期' },
    { field: 'state', width: 100, title: '状态' },
    { field: 'action', width: 100, title: '操作' }
]
```

## uploadOptions 属性默认值

```typescript
{
    concurrency: Number.POSITIVE_INFINITY,
    allowedContentTypes: ['*'],
    maxUploads: 0,
    maxFileSize: 12,
    uploadedCount:0
}
```

## FUploadFileExtend

| 属性名        | 类型                  | 说明               |
| :------------ | :-------------------- | :----------------- |
| id            | `string`              | 必有属性，标识字段 |
| name          | `string`              | 必有属性，附件名称 |
| disabled      | `boolean`             | 禁用状态           |
| checked       | `boolean`             | 选中状态           |
| size          | `number \| undefined` | 文件大小           |
| createTime    | `string\| undefined`  | 创建日期           |
| type          | `string`              | 类型               |
| extend        | `any\|null`           | 记录返回的额外数据 |
| extendHeaders | `any\|null`           | 扩展头部数据       |

## FUploadPreviewColumn

| 属性名   | 类型      | 说明                                       |
| :------- | :-------- | :----------------------------------------- |
| field    | `string`  | 必有属性，预览字段                         |
| width    | `number`  | 必有属性，预览该字段的宽度                 |
| title    | `string`  | 必有属性，预览字段的标题                   |
| checkbox | `boolean` | 是否启用选中，如果启用该字段左侧会有多选框 |

## UploaderOptions

| 属性名              | 类型       | 说明                                           |
| :------------------ | :--------- | :--------------------------------------------- |
| concurrency         | `string`   | 并发的个数,Number.POSITIVE_INFINITY 表示不限制 |
| allowedContentTypes | `string[]` | 允许上传的文件类型，\[\*]表示不限制类型        |
| maxUploads          | `number`   | 允许附件上传的个数 ,0 表示不限制个数           |
| maxFileSize         | `number`   | 允许的单个文件的大小，以 MB 为单位             |
| uploadedCount       | `number`   | 已上传的个数                                   |

## 属性

| 属性名                | 类型                          | 默认值     | 说明                                             |
| :-------------------- | :---------------------------- | :--------- | :----------------------------------------------- |
| contentFill           | `boolean`                     | false      | 是否填充父容器的区域                             |
| orderField            | `string`                      | createTime | 指定排序字段，默认是按照 createTime 上传时间排序 |
| previewReadonly       | `boolean`                     | true       | 预览功能的只读状态，如果只读则删除按钮不可见     |
| previewColumns        | `Array<FUploadPreviewColumn>` | 见下方     | 指定预览的字段、宽度、标题，是否可选中           |
| previewVisible        | `boolean`                     | true       | 预览是否可见                                     |
| previewEnableMulti    | `boolean`                     | true       | 预览是否启用批量操作                             |
| previewDefaultRename  | `string`                      | --         | 批量下载预览文件指定新的名称                     |
| noDownload            | `boolean`                     | false      | 预览列表中，禁止下载按钮                         |
| noPreview             | `boolean`                     | false      | 预览列表中，禁止预览按钮                         |
| uploadDisabled        | `boolean`                     | false      | 上传功能是否禁用                                 |
| uploadVisible         | `boolean`                     | true       | 上传功能是否显示                                 |
| uploadSelectText      | `string`                      | 选择文件   | 选择上传按钮的文本                               |
| uploadEnableMulti     | `boolean`                     | true       | 是否支持批量上传                                 |
| uploadedCount         | `number`                      | 0          | 已上传文件的个数                                 |
| downloadButtonDisable | `boolean`                     | false      | 下载按钮禁用                                     |
| previewButtonDisable  | `boolean`                     | false      | 预览按钮禁用                                     |
| deleteButtonDisable   | `boolean`                     | false      | 删除按钮禁用                                     |
| disabled              | `boolean`                     | false      | 整个控件的禁用状态                               |
| customInfo            | `string`                      | false      | 自定义提示信息                                   |
| fileInfos             | `Array<FUploadFileExtend>`    | 见上       | 处理传递预览的数据                               |
| uploadOptions         | `UploaderOptions`             | 见上       | 上传配置                                         |
| extendConfig          | `Object`                      | {}         | 发起服务器端请求，传递的特殊参数                 |
| uploadServerToken     | `string`                      | --         | 附件服务器端服务注入的 Token                     |
| notifyServiceToken    | `string`                      | --         | 提示服务注入的 Toke                              |

## 事件

| 事件名                    | 参数                                          | 说明                   |
| :------------------------ | :-------------------------------------------- | :--------------------- |
| filePreviewEvent          | FUploadFileExtend                             | 预览附件时的事件       |
| fileDownloadEvent         | \{fileInfos:[FUploadFileExtend],name:''\}     | 附件下载时事件         |
| previewMultiSelectedEvent | Array\<FUploadFileExtend\>                    | 勾选附件批量预览时事件 |
| selectedEvent             | FUploadFileExtend                             | 选中当前行时的事件     |
| fileRemoveEvent           | Array\<FUploadFileExtend\>\|FUploadFileExtend | 点击删除按钮时的事件参数是FUploadFileExtend对象,界面上没有数据变更，主要用于自定义处理删除逻辑。如果是批量预览下，勾选附件点击右上角的删除按钮，抛出此事件，参数是FUploadFileExtend类型的数组        |
| fileRemovedEvent           |FUploadFileExtend | 点击删除按钮调用服务器端删除接口，界面上更新数据，参数是FUploadFileExtend对象|
| uploadDoneEvent          | Array\<FUploadFileExtend\>                    | 附件上传完成后事件     |

## 插槽

::: tip
暂无内容
:::