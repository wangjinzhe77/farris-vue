# EnumEditor 枚举数据编辑器

Enum Editor 组件用来管理枚举类型的数据

## 基本用法

:::vdemo

```vue
{demos/enum-editor/basic.vue}
```

:::

## 只读&禁用

:::vdemo

```vue
{demos/enum-editor/readonly.vue}
```

:::

<!-- ## 动态更新数据源

:::vdemo

```vue
{demos/enum-editor/datasource.vue}
```

::: -->


## 属性 

| 属性名    | 类型      | 默认值   | 说明     |
| :-----   | :-------  | :-----  | :------- |
| items       | `Array<any>`  | `[]` | 数据源  |
| valueField | `String` | `value` | 编码字段 |
| textField | `String` | `name` | 文本字段 |
| disabled | `boolean` | `false` | 是否禁用  |
| readonly | `boolean` | `false` | 是否只读      |
| beforeOpen | `Function` | `() => true` | 编辑器打开前事件。在事件事可以动态更改数据源     |

## 事件 


| 事件名        | 类型                      | 说明                       |
| :-----------  | :------------------------ | :-------------------------|
| change       | `Array<Record<string, any>> `       | 值变化事件，返回修改后的数据源|