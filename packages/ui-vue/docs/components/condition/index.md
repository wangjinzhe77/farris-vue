# Condition 条件列表

Condition条件列表，主要用与展示、编辑用于筛选的条件集合。

## 基本用法

:::vdemo

```vue
{demos/condition/basic.vue}
```

:::


## 类型

```typescript
//0:无，1：与，2：或
type RelationType = 0 | 1 | 2;
enum CompareType {
    Equal = '0',
    NotEqual = '1',
    Greater = '2',
    GreaterOrEqual = '3',
    Less = '4',
    LessOrEqual = '5',
    Like = '6',
    LikeStartWith = '7',
    LikeEndWith = '8'
}
enum ValueType {
    //值类型
    Value = 0,
    //帮助类型
    SmartHelp = 1,
    //枚举类型
    Enum = 2,
    //表达式
    Express = 3
}
interface ConditionValue {

    //清空值方法
    clear(): void;
    //控件类型
    editorType: EditorType;
    //控件值
    value?: any;
    //控件值类型
    valueType: string;
    //获取控件值方法
    getValue(): any;
    
    setValue(value: any): void;

    isEmpty(): boolean;
}
interface EditorConfig {
    /** 编辑器类型 */
    type: EditorType;
    /** 自定义样式 */
    customClass?: string;
    /** 禁用 */
    disable?: boolean;
    /** 只读 */
    readonly?: boolean;
    /** 必填 */
    required?: boolean;
    /** 提示文本 */
    placeholder?: string;
    /** 其他属性 */
    [key: string]: any;
}

```
## 属性

| 属性名        | 类型                         | 默认值   | 说明            |
| :------------ | :-------------------------- | :--------| :-------------- |
| conditions    | `Array<Condition>`          | []       | 筛选数据         |
| fields        | `Array<FieldConfig>`        | []       | 筛选配置项       |
| key           | `string `                   | ''       | 组件唯一标识     |

## Condition属性

| 属性名        | 类型                         | 默认值   | 说明             |
| :------------ | :-------------------------- | :--------| :--------------- |
| id            | `string`                    | ''       | 唯一标识         |
| fieldCode     | `string`                    | ''       | 字段编号         |
| fieldName     | `string `                   | ''       | 字段名称         |
| compareType   | `CompareType `              | ''       | 高级模式下的值关系|
| valueType     | `ValueType `                | ''       | 值类型           |
| value         | `ConditionValue`            | ''       | 值               |
| relation      | `RelationType`              | ''       | 与下个条件的关系  |
| lBracket      | `string `                   | ''       | 左括号           |
| rBracket      | `string `                   | ''       | 右括号           |
| conditionId   | `number `                   | ''       | 组件唯一标识     |

## FieldConfig属性

| 属性名        | 类型                         | 默认值   | 说明             |
| :------------ | :-------------------------- | :--------| :--------------- |
| id            | `string`                    | ''       | 字段ID         |
| labelCode     | `string`                    | ''       | 字段标签         |
| name          | `string `                   | ''       | 字段名称         |
| editor        | `EditorConfig `             |          | 组件配置项，见各组件文档中可传参数  |
| defaulValue   | `ConditionValue `           |          |                  |


## 事件

| 事件名                | 类型                           | 说明                   |
| :----------           | :----------------------       |  :-------------------- |
| valueChange           | `EventEmitter<Condition>`     | 筛选值变化事件                      |
| labelCodeChange       | `EventEmitter<Condition>`     | 筛选条件类型切换事件（高级模式下）   |
| compareTypeChange     | `EventEmitter<Condition>`     | 筛选条件匹配值变化事件（高级模式下） |

## 插槽

::: tip
暂无内容
:::
