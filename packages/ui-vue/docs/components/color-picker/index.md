# ColorPicker 颜色选择器

ColorPicker 提供了一个选择与展示颜色的组件。

## 基本用法

:::vdemo

```vue
{demos/color-picker/basic.vue}
```

:::

## 属性

| 属性名         | 类型            | 默认值      | 说明            |
| :------------- | :-------------- | :---------- | :-------------- |
| color          | `string`        | `'e1e2e3'` | 颜色值          |
| disabled       | `boolean`       | false       | 是否禁用        |
| presets        | `Array<string>` | []          | 预置色彩组      |
| allowColorNull | `boolean`       | false       | 允许颜色为 null |

## 事件

| 事件名       | 类型                 | 说明       |
| :----------- | :------------------- | :--------- |
| valueChanged | `EventEmitter<any> ` | 值变化回调 |

