# Date Picker 选择日期组件

Date Picker 组件用来选择日期。

## 基本用法

:::vdemo

```vue
{demos/date-picker/basic.vue}
```

:::

## 日期格式化

:::vdemo

```vue
{demos/date-picker/date_format.vue}
```

:::

## 设置最大最小年限

:::vdemo

```vue
{demos/date-picker/date_boundary.vue}
```

:::

## 高亮周六或周日

:::vdemo

```vue
{demos/date-picker/highlight.vue}
```

:::

## 修改月份名称

:::vdemo

```vue
{demos/date-picker/name_of_months.vue}
```

:::

## 高亮日期

:::vdemo

```vue
{demos/date-picker/highlight_dates.vue}
```

:::


## 禁用周末

:::vdemo

```vue
{demos/date-picker/disable_weekends.vue}
```

:::


## 禁用特定日

:::vdemo

```vue
{demos/date-picker/disable_weekdays.vue}
```

:::

## 日期范围

:::vdemo

```vue
{demos/date-picker/date-range.vue}
```

:::


## 类型

```typescript
type defaultNameOfMonths = {
    1: 'Jan'
    2: 'Feb'
    3: 'Mar'
    4: 'Apr'
    5: 'May'
    6: 'Jun'
    7: 'Jul'
    8: 'Aug'
    9: 'Sep'
    10: 'Oct'
    11: 'Nov'
    12: 'Dec'
}
```

## 属性

| 属性名            | 类型      | 默认值              | 说明         |
| :---------------- | :-------- | :------------------ | :----------- |
| displayFormat     | `string`  | 'yyyy-MM-dd'        | 显示日期格式 |
| valueFormat       | `string`  | 'yyyy-MM-dd'        | 储存日期格式 |
| highlightSaturday | `boolean` | false               | 高亮周六     |
| highlightSunday   | `boolean` | false               | 高亮周日     |
| maxYear           | `number`  | 2500                | 最大年限     |
| minYear           | `number`  | 1                   | 最小年限     |
| nameOfMonths      | `Object`  | defaultNameOfMonths | 月名称缩写   |

<!-- | disableDates     | `Array<DateObject>` | []     | 组件标识 |
| disablePeriod     | `Array<Period>` | []     | 组件标识 |
| disableSince     | `Object` | { year: 0, month: 0, day: 0 }     | 组件标识 |
| disableWeekdays     | `Array<string>` | []     | 组件标识 |
| disableWeekends     | `Boolean` | false     | 组件标识 |
| disableUntil     | `Object` | { year: 0, month: 0, day: 0 }     | 组件标识 |
| displayFormat     | `string` | 'yyyyMMdd'     | 组件标识 |
| displayTime     | `Boolean` | false     | 组件标识 |
| highlightDates     | `Array<DateObject>` | []     | 组件标识 |
| periodDelimiter     | `string` | ''     | 组件标识 |
| selectMode     | `string` | 'day'     | 组件标识 |
 -->
