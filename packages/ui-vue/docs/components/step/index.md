# Step 步骤条

Step 组件用展现多步骤操作界面。

## 基本用法

:::vdemo

```vue
{demos/step/basic.vue}
```

:::

## 基本竖向步骤条

:::vdemo

```vue
{demos/step/vertical.vue}
```

:::

## 横向平分步骤条

:::vdemo

```vue
{demos/step/horizontal_fill.vue}
```

:::

## 竖向平分步骤条

:::vdemo

```vue
{demos/step/vertical_fill.vue}
```

:::

## 切换上一步或下一步

:::vdemo

```vue
{demos/step/step_changed.vue}
```

:::

## 类型

```typescript
type StepDirectionType = 'horizontal' | 'vertical'
type StepStatus = 'active' | 'finish' | ''
```

## 属性

| 属性名      | 类型                                                                                  | 默认值       | 说明                              |
| :---------- | :------------------------------------------------------------------------------------ | :----------- | :-------------------------------- |
| direction   | `string as StepDirectionType`                                                         | 'horizontal' | 步骤条方向                        |
| fill        | `boolean`                                                                             | false        | 是否平铺                          |
| height      | `number`                                                                              | 0            | 竖向步骤条，fill 时需要传递的高度 |
| clickable   | `boolean`                                                                             | true         | 是否支持点击                      |
| activeIndex | `number`                                                                              | 0            | 当前 active 步骤的索引            |
| steps       | ` [{disable: boolean;status: StepStatus;class: string;icon: string; title: string;}]` | --           | 步骤条的具体参数                  |

## 事件

| 事件名 | 类型                | 说明           |
| :----- | :------------------ | :------------- |
| click  | `EventEmitter<any>` | 点击步骤条事件 |
