# FieldSlector 绑定字段选择器

Field Slector 组件用来在表单设计器中选择字段。

## 基本用法

:::vdemo

```vue
{demos/field-selector/basic.vue}
```

:::

## 仅选择数据字段

:::vdemo

```vue
{demos/field-selector/only-datafield.vue}
```

:::

## 类型
```ts
export interface BindingTypeOption {
    /** 是否启用 */
    enable: boolean;
    /** 
     * 枚举数据源
     */
    data: Array<{ [key: string]: string }>;
    /** 默认选项值 */
    value: string;
    /** 文本字段 */
    textField: string;
    /** 值字段 */
    valueField: string;
}




```

## 属性 

| 属性名    | 类型      | 默认值   | 说明     |
| :-----   | :-------  | :-----  | :------- |
| data       | `Array<any>`  | `[]` | 数据源  |
| bindingData | `String` | `value` | 绑字数据 |
| bindingType | `BindingTypeOption` |  | 绑定类型 |
| disabled | `boolean` | `false` | 是否禁用  |
| readonly | `boolean` | `false` | 是否只读      |
| modalWidth | `number` | `600` | 弹窗宽度      |
| modalHeight | `number` | `500` | 弹窗高度      |
| title | `string` | `字段选择器` | 弱窗标题     |


`bindingType` 默认值：
```js
{
        enable: true,
        data: [
            { value: 'Form', text: '绑定字段'},
            { value: 'LocaleVariable', text: '绑定组件变量' },
            { value: 'RemoteVariable', text: '绑定表单变量' }
        ],
        value: 'Form',
        textField: 'text',
        valueField: 'value'
    }
```

## 事件 


| 事件名        | 类型                      | 说明                       |
| :-----------  | :------------------------ | :-------------------------|
| bindingTypeChanged   | `string `       | 绑定类型变化事件|
| selected       | `Array<any>`       | 字段选择事件|