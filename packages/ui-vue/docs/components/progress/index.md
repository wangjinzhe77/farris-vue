# Progress 进度条

Progress 进度条组件用来展示进度百分比。

## 基本用法

:::vdemo

```vue
{demos/progress/basic.vue}
```

:::

## 进度条颜色
 
:::vdemo

```vue
{demos/progress/stroke-color.vue}
```

:::



## 进度条宽度
 
:::vdemo

```vue
{demos/progress/stroke-width.vue}
```

:::

## 进度条尺寸
 
:::vdemo

```vue
{demos/progress/size.vue}
```

:::

## 隐藏进度信息
 
:::vdemo

```vue
{demos/progress/show-info.vue}
```

:::

## 格式化进度信息
 
:::vdemo

```vue
{demos/progress/format.vue}
```

:::

## 定义已完成进度
 
:::vdemo

```vue
{demos/progress/success-percent.vue}
```

:::

## 定义端点形状
 
:::vdemo

```vue
{demos/progress/stroke-line-cap.vue}
```

:::

## 定义进度条类型
 
:::vdemo

```vue
{demos/progress/progress-type.vue}
```

:::


## 定义进度状态
 
:::vdemo

```vue
{demos/progress/progress-status.vue}
```

:::

## 定义仪表盘缺口位置
 
:::vdemo

```vue
{demos/progress/gap-position.vue}
```

:::

## 定义成功分段背景图片
 
:::vdemo

```vue
{demos/progress/background-img.vue}
```

:::

## 类型

```typescript
type ProgressType = 'line' | 'circle' | 'dashboard';
type ProgressSize = 'default' | 'small';
type ProgressStatus = 'default' | 'success' | 'error' | 'active';
type ProgressStrokeLinecapType = 'round' | 'square';
type GapPosition = 'top' | 'bottom' | 'left' | 'right';
```

## 属性


| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| progressType | `string` | 'line'     | 可选，进度条类型 |
| strokeColor | `string` | '#2A87FF' | 可选，进度条颜色，支持渐变色 |
| strokeWidth | `number` | 0     | 可选，进度条线的宽度或圆形进度条宽度 |
| size     | `ProgressSize` | 'default'    | 可选，进度条尺寸 |
| strokeLinecap | `ProgressStrokeLinecapType` | 'round' | 可选，进度条端点形状 |
| width | `number` | 0 | 可选，仪表盘进度条画布宽度，单位px |
| percent | `number` | 20 | 进度 |
| format | `function` | (percent: number,success:number) => `${percent} %` | 可选，进度信息格式化函数 |
| showInfo | `boolean` | true | 可选，是否显示进度信息 |
| successPercent | `number` | 0 | 可选，已完成分段 |
| progressStatus | `ProgressStatus` | 'default' | 进度状态 |
| gapPosition | `GapPosition` | 'top' | 仪表盘进度条缺口位置 |
| gapDegree | `number` | 0 | 仪表盘进度条缺口角度 |

## 插槽

::: tip
暂无内容
:::
