# QuerySolution 筛选方案

QuerySolution 组件用来筛选过滤数据。

## 基本用法

:::vdemo

```vue
{demos/query-solution/basic.vue}
```

:::



## 类型

```typescript

```

## 属性

| 属性名    | 类型                   | 默认值 | 说明         |
| :-----    | :-------              | :----- | :-------     |
| fields    | `Array<FieldConfig>`  | []     | 筛选方案筛选项配置，具体可见`condition`组件     |
| solutions | `Array<QuerySolution>`| []     | 筛选方案列表     |
## QuerySolution属性

| 属性名        | 类型                   | 默认值 | 说明                                        |
| :-----        | :-------              | :----- | :-------                                    |
| id            | `string`              | ''     | 筛选方案标识                                 |
| code          | `string`              | ''     | 筛选方案编号                                 |
| name          | `string`              | ''     | 筛选方案名称                                 |
| conditions    | `Array<Condition>`    | []     | 筛选方案筛选项, Condition见`Condition组件`   |
| isDefault     | `boolean`             | false  | 是否为默认                                   |
| mode          | `string`              | []     | 模式新增字段，1为标准模式，2为高级模式         |


## 事件

| 事件名             | 类型                | 说明         |
| :-----             | :------------------ | :----------- |
| save               | `QuerySolution` | 保存方案    |
| saveAs             | `QuerySolution` | 另存为方案   |
| delete             | `QuerySolution` | 删除方案   |
| change             | `Condition` | 另存为方案   |

## 插槽

::: tip
暂无内容
:::
