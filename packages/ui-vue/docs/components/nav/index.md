# Nav 导航条

导航条，用于展示指定的导航信息

## 基本用法

:::vdemo

```vue
{demos/nav/basic.vue}
```

:::
## 最大数字

:::vdemo

```vue
{demos/nav/max-num.vue}
```

:::
## 指定当前

:::vdemo

```vue
{demos/nav/active.vue}
```

:::
## 垂直展示

:::vdemo

```vue
{demos/nav/vertical.vue}
```

:::

## 事件

:::vdemo

```vue
{demos/nav/events.vue}
```

:::

## 属性

| 属性名      | 类型             | 默认值 | 说明                                                |
| :---------- | :--------------- | :----- | :-------------------------------------------------- |
| maxNum      | `number`         | 99     | 显示未读信息的最大值，超过此数值会显示加号          |
| horizontal  | `boolean`        | true   | 水平还是垂直方向显示，ture 水平展示，false 垂直展示 |
| activeNavId | `string`         | true   | 指定当前激活的 id                                   |
| navPicking  | `function`       | --     | 切换前事件,用来控制是否可以点击                     |
| navData     | `Array<navItem>` |        | 导航数据                                            |

## navItem

| 属性名  | 类型      | 说明                     |
| :------ | :-------- | :----------------------- |
| id      | `string`  | 必有属性，导航的标识     |
| text    | `string`  | 必有属性，导航的展示文本 |
| num     | `number`  | 导航未读消息的个数       |
| disable | `boolean` | 导航是否禁用             |

## 事件

| 事件名 | 参数                      | 说明         |
| :----- | :------------------------ | :----------- |
| nav    | `navItem，当前导航的信息` | 点击标签事件 |

## 插槽

::: tip
暂无内容
:::
