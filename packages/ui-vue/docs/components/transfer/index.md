# Transfer 穿梭框
Transfer 穿梭框组件用于实现数据选择

## 基本用法

:::vdemo

```vue
{demos/transfer/basic.vue}
```

:::

## 展示树结构

:::vdemo

```vue
{demos/transfer/tree.vue}
```

:::


## 属性
| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| dataSource     | `Array<object>` | []     | 数据源 |
| displayType     | `string` | List     | 显示类型 |
| enableSearch     | `boolean` | true     | 启用搜索 |
| identifyField     | `string` | id     | 标识字段 |
| textField     | `string` | name     | 显示文本字段 |