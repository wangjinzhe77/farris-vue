# Message Box 消息弹窗

Message Box 消息弹窗采用模态对话框形式展示消息，用于提示消息、确认消息和提交内容。

## 基本用法

:::vdemo

```vue
{demos/message-box/types.vue}
```

:::

## 询问

:::vdemo

```vue
{demos/message-box/question.vue}
```

:::

## 异常

:::vdemo

```vue
{demos/message-box/errors.vue}
```

:::

## 提交内容

:::vdemo

```vue
{demos/message-box/prompt.vue}
```

:::
## 自定义用法

:::vdemo

```vue
{demos/message-box/basic.vue}
```

:::

```typescript
type MessageType = 'info' | 'warning' | 'success' | 'error' | 'prompt' | 'question';
type PromptEditorType = 'text-area' | 'number-spinner' | 'date-picker' | 'combo-list';
```
## Message Box 属性

| 属性名              | 类型                        | 默认值    | 说明          |
| :----------------- | :----------------------     | :-----     | :-------      |
| type               | `string as MessageType`     | `info`     | 类型          |
| title              | `string`                    |`''`        | 标题          |
| detail             | `string`                    | `''`       | 详细内容      |
| okButtonText       | `string`                    | `确定`     | 确定按钮文案  |
| cancelButtonText   | `string`                    | `取消`     | 取消按钮文案  |
| exceptionInfo      | `Object as ExceptionInfo`   | `null`     | 拓展信息      |
| promptEditorType   | `string as PromptEditorType`| `text-area`| 组件标识      |
## ExceptionInfo属性

| 属性名              | 类型        | 默认值 | 说明     |
| :----------------- | :---------  | :----- | :------- |
| date               | `string`    | `'`     | 日期 |
| message            | `string`    |`''`     | 消息内容 |
| detail             | `string`    | `''`    | 具体信息 |


## MessageService option属性


| 属性名              | 类型                        | 默认值        | 说明          |
| :----------------- | :----------------------     | :-----         | :-------      |
| type               | `string as MessageType`     | `info`         | 类型          |
| title              | `string`                    |`''`            | 标题          |
| detail             | `string`                    | `''`           | 详细内容,存在exceptionInfo时失效|
| okButtonText       | `string`                    | `确定`         | 确定按钮文案  |
| cancelButtonText   | `string`                    | `取消`         | 取消按钮文案  |
| exceptionInfo      | `ExceptionInfo`             | `null`         | 拓展信息      |
| acceptCallback     | `() => void`                | `() => void`   | 点击确定时回调，仅询问模式下可用      |
| rejectCallback     | `() => void`                | `() => void`   | 点击取消是回调，仅询问模式下可用      |



## 插槽

::: tip
暂无内容
:::
