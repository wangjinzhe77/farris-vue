# Notify 通知消息

Notify 组件用来显示全局消息。
1. 轻量级的提示，不影响用户继续执行其他操作
2. 有默认类提示、信息类提示、成功类提示、警告类提示、错误类提示
3. 可以设置提示显示位置
4. 提示可以只有内容，也可以带标题

## 基本用法
通过调用`NotifyService`中的方法使用
:::vdemo

```vue
{demos/notify/basic.vue}
```

:::
## 消息类型

通过调用`NotifyService`中不同的方法展示不同的通知类型
:::vdemo

```vue
{demos/notify/notify-type.vue}
```

:::

## 弹出位置
通过`globalConfig`中`position`属性设置弹出位置
:::vdemo

```vue
{demos/notify/notify-position.vue}
```

:::

## 延时关闭
:::vdemo

```vue
{demos/notify/notify-timeout.vue}
```

:::

## 禁止显示关闭按钮
通过`globalConfig`中的`showCloseButton`属性禁止显示关闭按钮
:::vdemo

```vue
{demos/notify/notify-close-button.vue}
```

:::


## 不显示标题

:::vdemo

```vue
{demos/notify/notify-no-title.vue}
```

:::


## 关闭所有提示框
调用`notifyService.closeAll()`方法关闭所有提示框

:::vdemo

```vue
{demos/notify/notify-closeall.vue}
```

:::

## 类型

```typescript
/** 弹出位置 */
export type NotifyPosition = 'bottom-right' | 'bottom-left' | 'top-right' | 'top-left' | 'top-center' | 'bottom-center';

/** 显示通知参数 */
export interface ShowNotifyParams {
    /** 通知内容 */
    message: string;
    /** 标题 */
    title?: string;
    /** 是否显示关闭按钮 */
    showCloseButton?: boolean;
    /** 通知位置 */
    position?: NotifyPosition;
    /** 延时关闭时间，默认 3000ms */
    timeout?: number;
}
```
## Service调用方法

| 属性名 | 参数 |  说明     |
| :----- | :------- | :------- |
| info     |ShowNotifyParams \| string   | 弹出info类型通知;`当参数为字符串时，仅显示通知内容不显示标题` |
| success     |ShowNotifyParams \| string   | 弹出success类型通知;`当参数为字符串时，仅显示通知内容不显示标题`  |
| warning     |ShowNotifyParams \| string    | 弹出warning类型通知;`当参数为字符串时，仅显示通知内容不显示标题`  |
| error     | ShowNotifyParams \| string   | 弹出error类型通知;`当参数为字符串时，仅显示通知内容不显示标题`  |
| closeAll     |    | 关闭所有通知 |



## 插槽

::: tip
暂无内容
:::
