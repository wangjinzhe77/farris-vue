# Tags 标签

Tags 组件用来标记事物的属性和维度，对事物进行分类。

## 显示类型

:::vdemo

```vue
{demos/tags/basic.vue}
```

:::


## 颜色类型

:::vdemo

```vue
{demos/tags/tag-type.vue}
```

:::

## 指定当前标签

:::vdemo

```vue
{demos/tags/active.vue}
```

:::


## 标签折行

:::vdemo

```vue
{demos/tags/wrap-text.vue}
```

:::
## 自定义样式

:::vdemo

```vue
{demos/tags/custom.vue}
```
::: 

## 支持选中

:::vdemo

```vue
{demos/tags/select.vue}
```
:::

## 显示添加按钮

:::vdemo

```vue
{demos/tags/add-button.vue}
```

:::
## 指定添加按钮文本

:::vdemo

```vue
{demos/tags/button-text.vue}
```

:::

## 禁用

:::vdemo

```vue
{demos/tags/disable.vue}
```

:::
## 事件

:::vdemo

```vue
{demos/tags/events.vue}
```

:::
## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| activeTag     | `string` | --     | 指定当前活动的标签 |
| addButtonText    | `string` | --      | 添加标签的文本 |
| customClass     | `string` | --      | 为标签添加自定义的样式名 |
| customStyle     | `string` |      | 为标签添加自定义的样式定义 |
| data     | `Array` | --     | 标签的数组 |
| enableAddButton     | `boolean` | false不启用添加标签按钮     | 是否启用添加标签按钮  |
| selectable     | `boolean` | false     | 标签是否可选中 |
| showAddButton     | `boolean` | false    | 是否显示添加标签按钮 |
| showClose     | `boolean` | true显示关闭按钮     | 标签是否显示关闭按钮 |
| showColor     | `boolean` | --     | 是否使用自定义的颜色 |
| tagType     | `string` | --     | 标签支持的颜色类型 |
| tagStyle     | `'default' | 'capsule'` |  default,默认展示方式   | 指定标签是按默认方式还是胶囊方式展示|
| wrapText     | `boolean` | false 不折行    | 标签文字是否自动折行 |
| disable     | `boolean` |false   | 组件整体的启用禁用状态，默认是启用 |


## 事件


| 事件名 | 参数                | 说明         |
| :----- | :------------------ | :----------- |
| remove  | 对象，包含当前标签数组、删除标签、删除标签的索引 | 删除标签 |
| selectionChange  |  当前选中标签| 选择标签事件 |
| change  | 当前标签数组 | 当删除标签、新增标签、选择标签时的变化事件 |


## 插槽

::: tip
暂无内容
:::
