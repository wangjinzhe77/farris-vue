# Dropdown 下拉组件

Dropdown 组件用来展示下拉内容。

## 基本用法

:::vdemo

```vue
{demos/dropdown/basic.vue}
```

:::

## 下拉组件类型

:::vdemo

```vue
{demos/dropdown/type.vue}
```

:::

## 下拉值状态

:::vdemo

```vue
{demos/dropdown/state.vue}
```

:::

## 下拉组件大小

:::vdemo

```vue
{demos/dropdown/size.vue}
```

:::

## 下拉是否分开展示

:::vdemo

```vue
{demos/dropdown/split.vue}
```

:::

## 禁用状态

:::vdemo

```vue
{demos/dropdown/disable.vue}
```

:::

## 图标下拉

:::vdemo

```vue
{demos/dropdown/icon.vue}
```

:::

## 下拉内容

:::vdemo

```vue
{demos/dropdown/content.vue}
```

:::

## 下拉方向

:::vdemo

```vue
{demos/dropdown/direction.vue}
```

:::

## 属性

| 属性名      | 类型                      | 默认值                                                                 | 说明                 |
| :---------- | :------------------------ | :--------------------------------------------------------------------- | :------------------- |
| title       | `string`                  | '下拉示例'                                                             | 下拉按钮对应文字     |
| size        | `string`                  | ''                                                                     | 下拉按钮大小         |
| type        | `string`                  | 'primary'                                                              | 下拉按钮类型         |
| iconClass   | `string`                  | ''                                                                     | 图标样式             |
| splitButton | `boolean`                 | false                                                                  | 下拉按钮是否分开展示 |
| position    | `string`                  | 'bottom'                                                               | 下拉框展示方向       |
| disabled    | `boolean`                 | false                                                                  | 下拉按钮是否禁用     |
| show        | `boolean`                 | false                                                                  | 默认展开或折叠       |
| model       | `array as DropdownItem[]` | [{ label: '项目一', value: 'XM1' }, { label: '项目二', value: 'XM2' }] | 下拉框内容           |
| active      | `boolean`                 | false                                                                  | 下拉框内容是否被选中 |

## 事件

| 事件名   | 类型                 | 说明       |
| :------- | :------------------- | :--------- |
| onSelect | `EventEmitter<any> ` | 返回选中值 |
