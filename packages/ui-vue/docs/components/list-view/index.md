# List View 列表

List View 列表组件用来以摘要信息的形式展示数据。

## 基本用法

:::vdemo

```vue
{demos/list-view/basic.vue}
```

:::

## 摘要视图

:::vdemo

```vue
{demos/list-view/common.vue}
```

:::

## 卡片视图

:::vdemo

```vue
{demos/list-view/card.vue}
```

:::

## draggable属性

:::vdemo

```vue
{demos/list-view/draggable.vue}
```

:::

## keepOrder属性

:::vdemo

```vue
{demos/list-view/keepOrder.vue}
```

:::

## placeholder属性

:::vdemo

```vue
{demos/list-view/placeholder.vue}
```

:::


## disableField属性

:::vdemo

```vue
{demos/list-view/disableField.vue}
```

:::

## multiSelect属性

:::vdemo

```vue
{demos/list-view/multiSelect.vue}
```

:::

## multiSelectMode属性

:::vdemo

```vue
{demos/list-view/multiSelectMode.vue}
```

:::

## 分组

:::vdemo

```vue
{demos/list-view/group.vue}
```

:::

## change事件

:::vdemo

```vue
{demos/list-view/change-event.vue}
```

:::

## selectionChange事件

:::vdemo

```vue
{demos/list-view/selectionChange-event.vue}
```

:::

## clickItem事件

:::vdemo

```vue
{demos/list-view/clickItem-event.vue}
```

:::

## removeItem事件

:::vdemo

```vue
{demos/list-view/removeItem-event.vue}
```

:::


## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| data     | `Array` | []     | 列表数组数据 |
|draggable | `boolean` | false     | 是否可拖拽 |
|multiSelect | `boolean` | false     | 是否支持多选 |
|multiSelectMode | `string as MultiSelectMode` | 'OnCheck'     | 多选模式 |
|idField | `string` | 'id'     | 定义列表的唯一标识字段 |
|valueField | `string` | 'id'    | 值字段 |
|textField | `string`  | 'name'      | 文本字段 |
|titleField | `string`  | 'name'      | 标题字段 |
|view | `string as ListViewType` | 'ContentView'    | 视图 |
|size | `string as ListViewSize` |  'default'     | 尺寸 |
|placeholder | `string ` |  ''     | 占位符 |
|header | `string as ListViewHeaderType` |   'ContentHeader'   | 列表头 |
|headerClass | `string ` |  ''     | 列表头类 |
|itemClass | `string ` |  ''     | 项目类 |
|selectionValues | `string as ListViewSize` |  'default'     | 已选列 |
|keepOrder | `boolean ` |  false     | 是否保持排序 |
|disableField | `string ` | 'disabled'   | 不可操作字段 |


## 事件

| 事件名       | 参数              | 说明             |
| :----------- | :---------------- | :--------------- |
| clickItem |`string `  | 点击项事件 |
| selectionChange | `string ` | 选择项改变事件 |
| removeItem | `string ` | 删除项 |
| change | `string ` | 拖动鼠标改变事件 |

## 插槽

::: tip
暂无内容
:::
