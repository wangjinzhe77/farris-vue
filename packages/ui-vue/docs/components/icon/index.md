# Icon
- 图库内容丰富，涵盖通用场景
- 兼容性好，支持 IE6+，及所有现代浏览器。
- 支持按字体的方式去动态调整图标大小，颜色等

## 基本用法

:::vdemo

```vue
{demos/icon/basic.vue}
```
:::
## 常用图标

:::vdemo

```vue
{demos/icon/common.vue}
```
:::
## 其他图标

:::vdemo

```vue
{demos/icon/others.vue}
```
:::