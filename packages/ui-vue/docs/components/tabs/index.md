# Tabs 标签页

Tabs 是标签页容器组件，通过切换标签，显示不同内容。内容区域可放置表格、Form、ListView等。Tabs纵向划分为：头部区域、内容区域。头部区域横向划分为：标题区、工具栏区。

## 基本用法
:::vdemo

```vue

{demos/tabs/basic.vue}
```

:::

## 下拉面板展示多标签
:::vdemo

```vue

{demos/tabs/more-tabs.vue}
```

:::


## 展示类型-胶囊001
:::vdemo

```vue
{demos/tabs/pill.vue}
```

:::
## 展示类型-填充
:::vdemo

```vue
{demos/tabs/tab-type.vue}
```

:::
## 展示类型-OnePage
:::vdemo

```vue
{demos/tabs/one-page.vue}
```

:::
## 填充内容区域
:::vdemo

```vue
{demos/tabs/tab-content-fill.vue}
```

:::
## 头部区域扩展
:::vdemo

```vue
{demos/tabs/header-extend.vue}
```
:::
## 头部标题宽度
:::vdemo

```vue
{demos/tabs/header.vue}
```
:::
## 子标签属性001
:::vdemo

```vue
{demos/tabs/tab-page.vue}
```


:::
## 工具栏按钮
:::vdemo

```vue
{demos/tabs/toolbar.vue}
```


:::
## 禁用标签

:::vdemo

```vue
{demos/tabs/disable.vue}
```

:::

## 隐藏标签

:::vdemo

```vue
{demos/tabs/hide.vue}
```

:::

## 删除标签

:::vdemo

```vue
{demos/tabs/close.vue}
```

:::

## 事件

:::vdemo

```vue
{demos/tabs/events.vue}
```

:::

## 类型

```typescript
export type TabType = 'fill' | 'pills' | 'default';
export type TabPosition = 'left' | 'right' | 'top' | 'bottom';
```

## Tabs属性

| 属性名           | 类型                    | 默认值    | 说明                     |
| :--------------- | :---------------------- | :-------- | :----------------------- |
| tabType          | `string as TabType`     | 'default' | 标签页显示样式          |
| autoTitleWidth   | `boolean`               | false     | 是否自动调整标题宽度     |
| titleLength      | `number`                | 7         | 标题宽度                 |
| position         | `string as TabPosition` | 'top'     | 显示页签的位置           |
| showDropDwon     | `boolean`               | false     | 是否显示页签导航下拉按钮 |
| showTooltips     | `boolean`               | false     | 是否显示标题提示信息     |
| scrollStep       | `number`                | 10         | 通过滚动鼠标切换页签的步长，默认是10px   |
| autoResize       | `boolean`               | false     | 是否允许自动调整页签高度 |
| removeable      | `boolean`                | false        | 是否支持关闭标签页           |
| width            | `number`                | --        | 组件宽度                 |
| height           | `number`                | --        | 组件高度                 |
| searchBoxVisible | `boolean`               | false     | 是否显示页签搜索框       |
| titleWidth       | `number`                | --        | 标题宽度                 |
| customClass      | `string`                | --        | 标签自定义样式           |
| activeId         | `string`                | --        | 指定选中的页签（值为页签标识）         |
| fill         | `boolean`                | false不填充      | 是否启用填充占满外层容器         |
| justifyContent   | `left,center,right`     | left左对齐  |left左对齐，center居中对齐，right右对齐          |
## TabPage属性
| 属性名           | 类型                    | 默认值    | 说明                     |
| :--------------- | :---------------------- | :-------- | :----------------------- |
| tabWidth          | `number`     | -1不限定宽度 | 指定标签的宽度          |
| id          | `string`     | -- | 指定标签的唯一标识         |
| title          | `string`     | -- | 指定标签的唯一标识         |
| selected          | `string`     | -- | 指定标签的唯一标识         |
| disabled         | `string`     | -- | 指定标签的唯一标识         |
| removeable          | `string`     | -- | 指定标签的唯一标识         |
| show          | `string`     | -- | 指定标签的唯一标识         |
| toolbar         | `Array<ToolbarContentsConf>`     | -- | 指定标签的工具栏    |
| toolbarPosition         | `inHead，inContent`     | inHead在标题栏 | 指定按钮工具栏的位置，inHead在标题栏区域，inContent在内容区域     |

## ToolbarContentsConf
| 属性名           | 类型                    | 默认值    | 说明                     |
| :--------------- | :---------------------- | :-------- | :----------------------- |
| id          | `number`     | -1不限定宽度 | 指定标签的宽度          |
| enable          | `boolean`     | true | 按钮是否可用         |
| title          | `string`     | -- | 按钮文案         |
| onClick          | `Function`     | -- | 按钮的点击事件         |
| appearance          | `{class:''}`     | -- | 按钮样式        |

## 事件


| 事件名 | 参数                | 说明         |
| :----- | :------------------ | :----------- |
| tabRemove  |`{"removeIndex":删除标签页的索引,"removeId":删除标签页的ID","activeId":删除后当前标签页的ID}` | 删除标签页时触发 |
| tabChange  |  `{"prevId":前标签页ID,"nextId":现在标签页ID}`| 切换标签页时触发 |

## 插槽

::: tip

```vue
<f-tabs>
    <template #headerPrefix>
        <div class="modal-title">
            <span class="modal-title-label">标题栏左侧</span>
        </div>
    </template>
     <template #headerSuffix>
        <div class="f-btn-icon f-bare">
          标题栏右侧
        </div>
   </template>
    <f-tab-page id="tab1" title="显示列">可以在此处放置其他控件、内容，标签页 1</f-tab-page>
    <f-tab-page id="tab2" title="排序列">可以在此处放置其他控件、内容，标签页 2</f-tab-page>
    <f-tab-page id="tab3" title="更多配置">可以在此处放置其他控件、内容，标签页 3</f-tab-page>  

 </f-tabs>
```
:::
