# Video 视频播放器

Video 用于在页面中展示视频。

## 基本用法

:::vdemo

```vue
{demos/video/basic.vue}
```

:::

## 禁用视频控件

:::vdemo

```vue
{demos/video/controls.vue}
```

:::

## 禁止自动播放

:::vdemo

```vue
{demos/video/autoplay.vue}
```

:::

## 静音

:::vdemo

```vue
{demos/video/muted.vue}
```

:::

## 禁止循环播放

:::vdemo

```vue
{demos/video/loop.vue}
```

:::

## 预加载视频信息

:::vdemo

```vue
{demos/video/preload.vue}
```

:::

## 设置视频封面

:::vdemo

```vue
{demos/video/poster.vue}
```

:::

## 类型

```typescript
type preload = 'none' | 'metadata' | 'auto';
```

## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| id     | `string` | --     | 组件标识 |
| src     | `string` | --     | 规定视频文件的 URL。 |
| autoplay | `boolean` | true | 可选，是否自动开始播放视频。 |
| controls | `boolean` | true | 可选，是否显示控制面板，允许用户控制视频的播放，包括音量、拖动进度、暂停等。 |
| loop | `boolean` | false | 可选，启用后会在视频播放结束的时候，自动返回视频开始的地方，继续播放。 |
| muted | `boolean` | false | 可选，是否静音 |
| preload | `string` | none | 可选，预加载内容 |
| poster | `string` | -- | 可选，规定在下载视频期间或在用户点击播放按钮之前显示的图像 |
| width | `number` | 300 | 可选，设置视频播放器的宽度。 |
| height | `number` | 168 | 可选，设置视频播放器的高度。 |

## 插槽

::: tip
暂无内容
:::
