# Radio Group 单选组

Radio Group 是一个单选一组枚举值的输入组件。

## 基本用法

:::vdemo

```vue
{demos/radio-group/basic.vue} 
```

:::

## 禁用状态

:::vdemo

```vue
{demos/radio-group/disable.vue}
```

:::

## 排列方式

:::vdemo

```vue
{demos/radio-group/alignment.vue}
```

:::

## 单选模板用法

:::vdemo

```vue
{demos/radio-group/radio_template.vue}
```

:::

## 按钮样式

:::vdemo

```vue
{demos/radio-group/radio_button.vue}
```

:::


## 类型

```typescript
export interface Radio {
    /**
     * 枚举值
     */
    value: ComputedRef<any>;
    /**
     * 枚举展示文本
     */
    name: ComputedRef<any>;
}

export interface ChangeRadio {
    enumData: ComputedRef<Array<Radio>>;

    /**
     * 获取枚举值
     */
    getValue(item: Radio): any;
    /**
     * 获取枚举文本
     */
    getText(item: Radio): any;

    /**
     * 切换单选按钮事件
     */
    onClickRadio: (item: Radio, $event: Event) => void;
}
```

## 属性

| 属性名     | 类型                  | 默认值  | 说明               |
| :--------- | :-------------------- | :------ | :----------------- |
| id         | `string`              | --      | 组件标识           |
| name       | `string`              | --      | 组件名称           |
| enumData   | `object[] as Radio[]` | --      | 废弃    |
| data   | `object[] as Radio[]` | []      | 单选组数据源       |
| textField  | `string`              | `'name'`  | 可选，默认为name，显示文本字段       |
| valueField | `string`              | `'value'` | 可选，默认为value，值字段             |
| direction | `string`             | `'horizontal'`   | 可选，枚举项的排列方向，默认为横向 |
| disabled   | `boolean`             | false   | 可选，是否禁用组件       |
| tabIndex   | `number`              | --      | 可选，Tab 键索引         |

## 事件

| 事件名 | 类型                | 说明         |
| :----- | :------------------ | :----------- |
| changeValue  | `EventEmitter<string>` | 单选值变化事件 |

## 插槽

::: tip
暂无内容
:::
