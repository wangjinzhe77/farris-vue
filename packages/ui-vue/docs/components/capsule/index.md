# Capsule 胶囊按钮

Capsule提供了一个具有视图切换功能的胶囊按钮 。

## 基本用法

:::vdemo

```vue
{demos/capsule/basic.vue}
```

:::


## 类型

```typescript
type CapsuleColerType = 'primary' | 'secondary';

```
## 属性

| 属性名      | 类型                         | 默认值   | 说明             |
| :---------- | :-------------------------- | :-------- | :-------------- |
| items      | `Array<CapsuleItem>`          | ''       | 展示项数组      |
| modelValue | `string`                      | ''       | 选中值          |
| type       | `string as CapsuleColerType`  | 'primary'| 按钮显示类型    |
## CapsuleItem属性

| 属性名         | 类型        | 默认值 | 说明     |
| :----------    | :----------| :----- | :--------|
| name          | `string`    | ''     | 选项名称 |
| value         | `any`       | ''     | 选项值   |


## 事件

| 事件名      | 类型                    | 说明                   |
| :---------- | :---------------------- | :-------------------- |
| change      | `EventEmitter<any>`     | 点击展示项事件         |

## 插槽

::: tip
暂无内容
:::
