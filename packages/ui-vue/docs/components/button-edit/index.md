# Button Edit 按钮输入框

Button Edit 是允许在其编辑器内部嵌入按钮的文本输入框。开发者可以自定义输入框的按钮，以便于实现丰富的录入交互形式。

## 基本用法

:::vdemo 使用`v-model`对输入值做双向绑定。

```vue
{demos/button-edit/basic.vue}
```

:::
## 输入框类型

:::vdemo 

```vue
{demos/button-edit/inputType.vue}
```

:::

## 对齐方式

:::vdemo

```vue
{demos/button-edit/alignment.vue}
```

:::

## 状态

:::vdemo

```vue
{demos/button-edit/status.vue}
```

:::

## 按钮

:::vdemo

```vue
{demos/button-edit/button.vue}
```

:::

## 清空按钮

:::vdemo 使用清空按钮可以快速置空文本框。

```vue
{demos/button-edit/clear.vue}
```

:::

## 文本标签

:::vdemo

```vue
{demos/button-edit/label.vue}
```

:::

## 提示信息

:::vdemo 使用清空按钮可以快速置空文本框。

```vue
{demos/button-edit/placeholder.vue}
```

:::
## 输入值长度限制

:::vdemo 

```vue
{demos/button-edit/range.vue}
```

:::
## 弹出面板交互设置

:::vdemo 弹出面板的展开与收起主要依赖popupOnInput、popupOnFocus两个属性，具体如下

```vue
{demos/button-edit/popover.vue}
```

:::

## 类型

```typescript
type TextAlignment = 'left' | 'center' | 'right';
```

## 属性

| 属性名                 | 类型                      | 默认值 | 说明                                                                          |
| :--------------------- | :------------------------ | :----- | :---------------------------------------------------------------------------- |
| id                     | `string`                  | --     | 标记组件的唯一标识                                                            |
| autoComplete           | `boolean`                 | false  | 是否启用自动完成录入文本功能                                                  |
| buttonContent          | `string`                  | --     | 按钮内容，缺省为点击按钮图标                                                  |
| customClass            | `string`                  | --     | 组件自定义样式                                                                |
| disable                | `boolean`                 | false  | 将组件设置为禁用状态                                                          |
| editable               | `boolean`                 | true   | 是否允许在输入框编辑文本                                                      |
| enableClear            | `boolean`                 | false  | 是否启用清空按钮                                                              |
| readonly               | `boolean`                 | false  | 将组件输入框设置为只读                                                        |
| textAlign              | `string as TextAlignment` | 'left' | 设置输入框对齐方式，缺省状况为左对齐，可选择的值包括：'left','center','right' |
| showButtonWhenDisabled | `boolean`                 | false  | 是否允许在禁用组件时仍然显示组件按钮                                          |
| enableTitle            | `boolean`                 | false  | 是否启用输入框的的提示文本                                                    |
| inputType              | `'text' \| 'tag'`         | 'text' | 设置输入框类型，缺省为文本输入框，可以选择设为标签输入框                      |
| forcePlaceholder       | `boolean`                 | false  | 是否始终显示输入框提示文本                                                    |
| placeholder            | `string`                  | --     | 设置输入框提示文本                                                            |
| minLength              | `number`                  | --     | 设置输入框文本最小长度                                                        |
| maxLength              | `number`                  | --     | 设置输入框文本最大长度                                                        |
| tabIndex               | `number`                  | --     | 设置组件 Tab 键索引                                                           |
| multiSelect            | `boolean`                 | false  | 是否支持多选                                                                 |
| separator              | `string`                  | ,      | 多选分隔符                                                                   |
| popupHost              | `Object as PropType<any>` | ,      | popover组件所依附的元素                                                      |
| popupRightBoundary     | `Object as PropType<any>` | ,      |                                                                             |
| popupOffsetX           | `Object as PropType<any>` | ,      |                                                                             |
| popupOnInput           | `boolean`                 | false  |开启后，输入时自动弹出选择面板，点击回车时收起面板                              |
| popupOnFocus           | `boolean`                 | false  |开启后，点击输入框自动弹出选择面板，点击回车时收起面板                           |
| popupMinWidth          | `number`                 | 320    | 弹出面板最小宽度                                                              |
| wrapText               | `boolean`                  | false  |                                                                            |

## 事件


```typescript
type ClickButtonEvent = {origin: PointerEvent, value:string};
```

| 事件名         | 类型                | 说明                 |
| :------------- | :------------------ | :------------------- |
| clear          | `EventEmitter<[]>` | 点击清空按钮事件     |
| click          | `EventEmitter<Array<PointerEvent>>` | 点击输入框事件       |
| clickButton    | `EventEmitter<Array<ClickButtonEvent>>` | 点击按钮事件         |
| blur           | `EventEmitter<Array<FocusEvent>>` | 输入框失去焦点事件   |
| focus          | `EventEmitter<Array<FocusEvent>>` | 输入框获得焦点事件   |
| mouseEnterIcon | `EventEmitter<Array<MouseEvent>>` | 鼠标移入按钮事件     |
| mouseLeaveIcon | `EventEmitter<Array<MouseEvent>>` | 数据移出按钮事件     |
| keyup          | `EventEmitter<Array<KeyboardEvent>>` | 抬起键盘按键事件     |
| keydown        | `EventEmitter<Array<KeyboardEvent>>` | 按下键盘按键事件     |
| input          | `EventEmitter<Array<string>>` | 在输入框录入字符事件 |

## 插槽

::: tip
暂无内容
:::
