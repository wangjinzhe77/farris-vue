# VerifyDetail 验证信息

VerifyDetail 用于展示表单验证结果。
- 通过组件使用，可以在多个容器内展示
- 通过服务调用，全局只能有一个验证信息的展示界面

## 使用组件

:::vdemo

```vue
{demos/verify-detail/basic.vue}
```

:::
## 指定展示类别

:::vdemo

```vue
{demos/verify-detail/show-type.vue}
```

:::
## 限定展示高度

:::vdemo

```vue
{demos/verify-detail/max-height.vue}
```

:::
## 事件

:::vdemo

```vue
{demos/verify-detail/events.vue}
```

:::
## 通过服务

:::vdemo

```vue
{demos/verify-detail/service.vue}
```

:::
## 属性

| 属性名 | 类型    | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| showType     | `string` | --     | 默认显示的表单验证分类，值分类的type |
| showList     | `boolean` | --     | 是否默认打开验证信息列表 |
| verifyList     | `validatorListItem` | --     | 表单验证列表信息 |
| verifyType     | `tabListItem` | --     |表单验证信息分类，默认为all error empty 三种|

## 事件
|名称|参数|说明|
| :----- | :------- | :----- | 
|validatorClick|validatorListItem	|验证信息点击|
|listshowChange|boolean	|验证详情显示隐藏事件,true是打开，false是隐藏|

## validatorListItem
| 名称| 类型| 说明| 
| :----- | :------- | :----- | 
|id|string|验证信息id|
|title|string|验证信息标题|
|msg|string|验证信息描述|
|type|string|验证表单对应类型|

## tabListItem
| 名称| 类型| 说明| 
| :----- | :------- | :----- | 
|id|string|验证信息id|
|type|string|验证表单对应类型|
|title|string|验证信息标题|
|active|boolean|是否默认显示|
|length|number|当前验证信息分类下长度|

## VerifyDetailService方法
|名称|参数|说明|
| :----- | :------- | :----- | 
|show|(props: VerifyDetailProps,parent:HTMLElement\|null):props指定组件属性，parent，指定验证消息所在位置，如果不指定则在body内插入验证消息|打开验证消息|
|clear|--	|移除所有验证消息|
## 插槽

::: tip
暂无内容
:::
