# Loading 加载

Loading 组件用于在需要等待的时候显示。

## 基本用法

:::vdemo

```vue
{demos/loading/basic.vue}
```

:::

## 显示图标

:::vdemo

```vue
{demos/loading/icon.vue}
```

:::

## 容器内展示

:::vdemo

```vue
{demos/loading/container.vue}
```

:::

## 延迟显示

:::vdemo

```vue
{demos/loading/delay.vue}
```

:::

## 使用组件

:::vdemo

```vue
{demos/loading/component.vue}
```

:::

## 属性

| 属性名         | 类型      | 默认值              | 说明                                                                                        |
| :------------- | :-------- | :------------------ | :------------------------------------------------------------------------------------------ |
| showMessage    | `boolean` | true                | 是否展示文案                                                                                |
| message        | `string`  | '加载中，请稍后...' | 展示信息                                                                                    |
| isActive       | `boolean` | false               | 默认展示状态                                                                                |
| width          | `number`  | '30'                | 图标宽度                                                                                    |
| type           | `number`  | '0'                 | 图标样式 ,0 是旋转图标，1 是彩色点旋转图标，2 是咖啡杯图标                                  |
| target         | Element   | null                | 加载范围 ,不指定默认是在 body 里                                                            |
| targetPosition | String    | ''                  | 需要给父元素设置的 position 值，如果父元素 position 的值是 absolute 或者是 fiex，不需要设置 |
| delay          | number    | 300                 | 延迟 300 毫秒显示加载,避免异步加载出现一闪而逝的问题                                        |

## 事件

| 事件名 | 参数                                     | 说明                                  |
| :----- | :--------------------------------------- | :------------------------------------ |
| closed | `{loadingId:加载标识,callback:调用函数}` | 常用在通过加载服务关闭 loading 的时候 |

## 组件实例的方法

| 事件名       | 参数                | 说明                                       |
| :----------- | :------------------ | :----------------------------------------- |
| close        | `callback:调用函数` | 关闭对应加载组件                           |
| getLoadingId | -                   | 获取 loadingId，提供给加载服务中的关闭方法 |

## 服务

| 事件名   | 参数                                       | 返回值             | 说明                                   |
| :------- | :----------------------------------------- | :----------------- | :------------------------------------- |
| show     | `LoadingProps`                             | Loading 组件的实例 | 根据指定属性显示加载组件，返回组件实例 |
| close    | `loadingId加载组件标识，callback:调用函数` | void               | 关闭指定 Id 对应的加载组件             |
| clearAll | -                                           | void               | 清空所有加载组件                       |
