# Search Box 组件

Search Box 组件为开发者提供了一个具有搜索功能的组件。


## 基本用法

:::vdemo

```vue
{demos/search-box/basic.vue}
```

:::

## 字段相关属性

:::vdemo

```vue
{demos/search-box/field.vue}
```

:::

## 指定下拉面板的模版

:::vdemo

```vue
{demos/search-box/slot.vue}
```

:::


## 正在加载

:::vdemo

```vue
{demos/search-box/loading.vue}
```

:::

## 禁用

:::vdemo

```vue
{demos/search-box/disable.vue}
```

:::
## 自定义样式

:::vdemo

```vue
{demos/search-box/class.vue}
```

:::

## 指定下拉面板最大高度

:::vdemo

```vue
{demos/search-box/max-height.vue}
```

:::

## 弹出相关属性

:::vdemo

```vue
{demos/search-box/popup.vue}
```

:::

## 事件

:::vdemo

```vue
{demos/search-box/events.vue}
```

:::
## 展示下拉结果面板

:::vdemo

```vue
{demos/search-box/show.vue}
```

:::



## 属性

| 属性名      | 类型         | 默认值         | 说明                                                                                 |
| :---------- | :----------- | :------------- | :----------------------------------------------------------------------------------- |
| data        | `array<any>` | --             | 搜索下拉面板绑定的列表数据                                                           |
| idField     | `string`     | id             | 指定绑定的列表数据中的标识字段                                                       |
| textField   | `string`     | name           | 指定绑定的列表数据中的名称字段                                                       |
| modelValue  | `string`     | ''             | 绑定搜索框的文本                                                                     |
| customClass | `string`     | ''             | 指定自定义的样式                                                                     |
| placeholder | `string`     | '请输入关键词' | 搜索框的占位文本                                                                     |
| maxHeight   | `number`     | 350            | 指定搜索下拉面板的最大高度                                                           |
| loading     | `boolean`    | false          | 是否处于加载状态，加载状态下不能输入，不可点击搜索按钮，下拉面板中显示正在加载的图标 |
| disable     | `boolean`    | false          | 是否处于禁用状态，禁用状态下，不能输入文本，不显示清空按钮，搜索图标正常显示 |
| popupHost   | `Object`     | --             | 搜索下拉面板面板代码插入的位置，默认是在 body 里，可以指定放置到其他元素             |

## 事件

| 事件名            | 参数                                                               | 说明                   |
| :---------------- | :----------------------------------------------------------------- | :--------------------- |
| update:modelValue | `searchText:string` 搜索框内的文本                                 | 搜索框内文本变更时触发 |
| change            | `searchText:string` 搜索框内的文本                                 | 搜索框内文本变更时触发 |
| selectedValue     | `item:Object` 选中的下拉项                                         | 点击下拉框列表时触发   |
| clickButton       | `{origin:Event,value:string}`origin:鼠标单机事件，value 搜索框的值 | 点击搜索图标按钮时触发 |

## 插槽

::: tip

```vue
<f-search-box>
    <template #default="listItemProps">
      <div>{{ listItemProps.item.name}}<span class="badge badge-border-info ml-3">{{ listItemProps.item.position }}</span></div>
   </template>
 </f-search-box>
```
