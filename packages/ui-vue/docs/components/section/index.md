# Section 面板

Section 组件为开发者提供了一个具有标题的组件容器。

## 基本用法

:::vdemo

```vue
{demos/section/basic.vue}
```

:::

## 是否显示头部区域

:::vdemo

```vue
{demos/section/show-header.vue}
```

:::

## 收折内容区域

默认收折：点击收折按钮后，隐藏整个内容区域；<br>
自定义收折：点击收折按钮后，显示内容区域中指定的部分节点。常用于内容区域收折后仍然想要显示部分关键信息的场景。<br><strong>请注意</strong>，自定义收折特性需要配合以下样式才能实现：<br>
1、为 Section 面板增加自定义样式`f-section-form`<br>
2、用一个容器包裹整个内容区域，并且容器的样式需要包含`f-form-layout`<br>
3、对于收折后仍需要显示的层级节点，增加样式`f-state-visible`
:::vdemo

```vue
{demos/section/collapse-expand.vue}
```

:::

## 最大化

:::vdemo

```vue
{demos/section/maximize.vue}
```

:::

## 按钮

:::vdemo

```vue
{demos/section/toolbar.vue}
```

:::

## 填充

内容区域是否占满 Section 父级容器的剩余空间。
:::vdemo

```vue
{demos/section/fill.vue}
```

:::

## 自定义头部模板

通过插槽`header`可以自定义整个头部区域，传入`headerClass`属性可以为头部区域添加自定义样式。

:::vdemo

```vue
{demos/section/header-template.vue}
```

:::

## 自定义头部标题模板

通过插槽`headerTitle`可以自定义头部区域中的标题部分。

:::vdemo

```vue
{demos/section/title-template.vue}
```

:::

## 自定义头部内容区域模板

通过插槽`headerContent`可以自定义头部区域的中间内容部分。

:::vdemo

```vue
{demos/section/header-content-template.vue}
```

:::

## 自定义扩展区域模板

头部和内容区域之间为扩展区域，通过插槽`extend`可以自定义扩展区域模板。传入`extendClass`属性可以为扩展区域添加自定义样式。

:::vdemo

```vue
{demos/section/extend-area-template.vue}
```

:::

<!-- ## 类型

```typescript
export interface ButtonAppearance {
    class: string
    class: string
}

export interface ButtonConfig {
    id: string
    disable: boolean
    title: string
    click: any
    appearance: ButtonAppearance
    visible?: boolean
    id: string
    disable: boolean
    title: string
    click: any
    appearance: ButtonAppearance
    visible?: boolean
}

export interface ToolbarConfig {
    position: string
    contents: ButtonConfig[]
    position: string
    contents: ButtonConfig[]
}
``` -->

## 属性

| 属性名             | 类型      | 默认值 | 说明                                                                              |
| :----------------- | :-------- | :----- | :-------------------------------------------------------------------------------- |
| customClass        | `string`  | --     | 组件自定义样式                                                                    |
| mainTitle          | `string`  | --     | 面板主标题                                                                        |
| subTitle           | `string`  | --     | 面板副标题                                                                        |
| showHeader         | `boolean` | true   | 是否显示标题面板                                                                  |
| enableAccordion    | `string` | ''     | 是否启用收折，支持：空字符串，不启用收折 \| default 默认收折 \| custom 自定义收折 |
| expandStatus       | `boolean` | true   | 组件默认的展开状态                                                                |
| headerClass        | `string`  | ''     | 头部区域的自定义样式。配置头部自定义模板后，此属性有效                            |
| headerContentClass | `string`  | ''     | 头部扩展区域的自定义样式。配置头部扩展区域模板后，此属性有效。                    |
| extendClass        | `string`  | ''     | 扩展区域的自定义样式。配置扩展区域模板后，此属性有效。                            |
| enableMaximize     | `boolean` | false  | 是否启用最大化                                                                    |
| fill               | `boolean` | false  | 是否允许填充内容区域                                                                  |

<!-- | toolbarButtons | `object[]` | [] | 工具栏按钮 | -->
<!-- | context       | `object`               | --                        | 组件上下文         | -->
<!--           | enableCollapse        | `boolean`                 | true               | 是否允许收折                  | -->
<!--           | maxStatus             | `boolean`                 | false              | 组件最大化状态               | -->
<!--           | clickThrottleTime     | `number`                  | 350                | 鼠标重复点击按钮触发事件的阈值 | -->
<!--           | showToolbarMoreButton | `boolean`                 | true               | 是否显示工具栏的更多按钮       | -->
<!--           | toolbar               | `object as ToolbarConfig` | {}                 | 工具栏配置对象                 | -->
<!-- | toolbarPosition | `string` | -- | 工具栏位置 | -->
<!-- | index | `number` | -- | 索引号 | -->

## 插槽

| 插槽名        | 说明                 |
| :------------ | :------------------- |
| header        | 整个头部区域模板     |
| headerTitle   | 头部标题区域模板     |
| headerContent | 头部中间内容区域模板 |
| extend        | 扩展区域模板         |
