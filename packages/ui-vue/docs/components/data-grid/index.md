# Data Grid 表格

Data Grid 是展示数据的表格组件，提供分页展示数据，在单元格中编辑数据的功能。

## 基本用法

:::vdemo

```vue
{demos/data-grid/basic.vue}
```

:::

## 禁用

:::vdemo

```vue
{demos/data-grid/disable.vue}
```

:::

## 尺寸

:::vdemo

```vue
{demos/data-grid/change_size.vue}
```

:::

## 行号

:::vdemo

```vue
{demos/data-grid/row_number.vue}
```

:::

## 分页

:::vdemo

```vue
{demos/data-grid/pagination.vue}
```

:::


## 按钮操作列

:::vdemo

```vue
{demos/data-grid/command_option.vue}
```

:::

## 自定义行列

:::vdemo

```vue
{demos/data-grid/cell_custom.vue}
```

:::

## 单元格模板

:::vdemo

```vue
{demos/data-grid/cell_template.vue}
```

:::

## 合计行

:::vdemo

```vue
{demos/data-grid/summary.vue}
```

:::

## 固定列

:::vdemo

```vue
{demos/data-grid/fixed_column.vue}
```

:::

## 表头分组

:::vdemo

```vue
{demos/data-grid/header-group.vue}
```

:::

## 数据分组

:::vdemo

```vue
{demos/data-grid/group_local_data.vue}
```

:::


## 启用选择行

:::vdemo

```vue
{demos/data-grid/select_on_click.vue}
```

:::

## 勾选复选框选择行

:::vdemo

```vue
{demos/data-grid/select_on_check.vue}
```

:::

## 多选模式

:::vdemo

```vue
{demos/data-grid/selection_mode.vue}
```

:::

## 设置选中数据

:::vdemo

```vue
{demos/data-grid/set_selection_values.vue}
```

:::

## 显示全选

:::vdemo

```vue
{demos/data-grid/show_select_all.vue}
```

:::


## 显示边框

:::vdemo

```vue
{demos/data-grid/show_border.vue}
```

:::

## 显示配置按钮

:::vdemo

```vue
{demos/data-grid/show_setting.vue}
```

:::

## 显示滚动条

:::vdemo

```vue
{demos/data-grid/fit.vue}
```

:::

## 显示条纹

:::vdemo

```vue
{demos/data-grid/show_stripe.vue}
```

:::

## 行编辑

:::vdemo

```vue
{demos/data-grid/edit_local_data.vue}
```

:::

## 单元格编辑

:::vdemo

```vue
{demos/data-grid/cell_edit.vue}
```

:::

## 绑定数据

:::vdemo

```vue
{demos/data-grid/binding_data.vue}
```

:::

## 设置列

:::vdemo

```vue
{demos/data-grid/column.vue}
```

:::

## 自适应列宽

:::vdemo

```vue
{demos/data-grid/fit_columns.vue}
```

:::

## 加载中

:::vdemo

```vue
{demos/data-grid/loading.vue}
```

:::

## 空数据

:::vdemo

```vue
{demos/data-grid/empty_content.vue}
```

:::


## DataGrid属性

| 属性名     | <div style="width:200px">类型</div>                 | 默认值    | 说明                 |
| :---------------------- | :------------------------------- | :--------: | :------------------- |
| data         | `array`               | --        | 绑定的数据源   |
| columns      | `array<DataGridColumn>`               | --        | 列集合        |
| disable    | `boolean`              | false     | 禁用 |
| editable       | `boolean`   | false   | 允许编辑             |
| fit       | `boolean`   | false   | 适配父组件尺寸             |
| focusOnEditingCell       | `boolean`   | false   | 编辑单元格时默认获得焦点             |
| selectOnEditingCell       | `boolean`   | false   | 编辑单元格时默认选中单元格文本             |
| height       | `number`   | -1   | 高度             |
| width       | `number`   | -1   | 宽度             |
| minHeight       | `number`   | 300   | 最小高度             |
| minWidth       | `number`   | 400   | 最小宽度             |
| id       | `string`   | ''   | DataGrid组件唯一标识             |
| idField       | `string`   | id   | 被绑定数据的标识字段             |
| mergeCell       | `boolean`   | 400   | 纵向合并具有相同值的单元格    |
| showFooter       | `boolean`   | false   | 显示底部面板             |
| showHeader       | `boolean`   | true   | 显示顶部面板             |
| showScrollBar       | `boolean`   | auto   | 显示滚动条             |
| showSetting       | `boolean`   | false   | 显示设置按钮             |
| virtualized       | `boolean`   | false   | 启用虚拟渲染             |
| commandOption  | `CommandOptions`   | `{enable: false,commands: []}`         |      列配置         |
| editOption  | `EditOption`   | `{ selectOnEditing: false, editMode:'cell' }`         |      编辑配置，selectOnEditing表示编辑时选中文本，editMode表示 编辑模式； row：整行编辑，cell: 单元格编辑        |
| pagination       | `PaginatonOptions`   | `{enable:false,size:20}`   | 分页配置            |
| rowNumber       | `RowNumberOptions`   | `{enable: true,width: 32,heading:'序号'}`   | 行号配置,enable表示是否显示行号，heading表示行号列表头标题，width表示行号宽度  |
| rowOption       | `RowOptions`   | `{wrapContent: false}`   | 行配置             |
| selection       | `SelectionOptions`   | `{enableSelectRow: true,multiSelect: false,multiSelectMode: 'DependOnCheck',showCheckbox: false,showSelectAll: false,showSelection: true}`   | 选中行配置             |
| sort       | `SortOptions`   | --   | 排序设置，enable标识启用排序，fields标识排序字段集合，mode标识排序交互模式，分为'client'和'server'两种，multiSort表示多列排序             |
| summary       | `SummaryOptions`   | `{enable: true,groupFields: ['numericField1', 'numericField2']}`   | 合计配置     |


## DataGridColumn属性

| 属性名     | 类型                   | 默认值    | 说明                 |
| :---------------------- | :------------------------------- | :-------- | :------------------- |
| dataType         | `string`               | --        | 数据类型   |
| editor      | `EditorConfig`               | --        | 编辑器配置        |
| field    | `string`              | --     | 列标识 |
| title       | `string`   | ''   | 列标题             |
| width       | `number,string`   | --   | 列宽度             |
| halign       | `string`   | --   | 标题水平对齐方式，分为left,center,right三种             |
| align       | `string`   | --   | 文本水平对齐方式，分为left,center,right三种            |
| valign       | `string`   | --   | 文本垂直对齐方式，分为top,middle,bottom三种             |
| fixed       | `string`   | --   | 是否固定列，分为left和right两种             |
| visible       | `boolean`   | --   | 是否显示             |
| readonly       | `boolean`   | --   | 是否只读             |
| showTips       | `boolean`   | --   | 鼠标移动至单元格后，显示悬浮消息             |
| tipMode       | `string`   | --   | 单元格提示模式：allways： 鼠标滑过即显示,   auto: 单元格宽度不够时才会显示           |
| sortable       | `boolean`   | --   | 是否排序             |
| resizable       | `boolean`   | --   | 是否伸缩             |
| filterable       | `boolean`   | --   | 是否筛选             |
| formatter(cell: VisualDataCell, visualDataRow: VisualData)     | `function`   | 无默认值，可选属性   | 格式化函数,函数返回值为VNode 或者string类型            |


## 分页属性(pagination)

| 属性 | 类型                | 默认值         |说明         |
| :----- | :------------------ | :----------- |:-------------------------- |
| enable  | `boolean` | false | 启用分页 |
| index  | `number` | 1 | 当前页码 |
| mode  | `string` | 'server' | 分页模式,'server'和'client'两种 |
| showGoto  | `boolean` | false | 显示页码输入框 |
| showIndex  | `boolean` | true | 显示页码 |
| showLimits  | `boolean` | false | 显示每页记录数 |
| showPageInfo  | `boolean` | false | 显示分页汇总信息 |
| size  | `number` | 20 | 默认每页记录数 |
| sizeLimits  | `array<number>` | [10, 20, 30, 50, 100] | 可选择的每页记录数据 |
| total  | `number` | 0 | 总记录数 |


## 行配置属性(rowOption)

| 属性 | 类型                | 默认值         |说明         |
| :----- | :------------------ | :----------- |:----------- |
| customRowStyle(data:any)  | `function` | 空函数 | 自定义行样式函数，参数为行数据 |
| height  | `boolean` | 28 | 行高度 |
| showHovering  | `boolean` | true | 鼠标滑过行效果 |
| wrapContent  | `boolean` | false | 禁止数据折行 |

## 列配置属性(columnOption)

| 属性 | 类型                | 默认值         |说明         |
| :----- | :------------------ | :----------- |:----------- |
| fitColumns  | `boolean` | false | 自动列宽。设为true后，所有列将填满表格并不会出现横向滚动条。 |
| fitMode  | `string` | average | 自动适配列宽度模式,'none'或'average'或'expand'或'percentage' |
| groups  | `array<ColumnGroupItem>` | [] | 分组, ColumnGroupItem结构是field（列标识）,title（标题）和group（ColumnGroupItem结构）|
| reorderColumn  | `boolean` | false | 允许拖动表头改变列显示顺序 |
| resizeColumn  | `boolean` | true | 允许拖动改变列宽度 |
| resizeColumnOnDoubleClick  | `boolean` | true | 双击表头列自适应内容宽度 |


## 选中行配置属性(selection)

| 属性 | 类型                | 默认值         |说明         |
| :----- | :------------------ | :----------- |:----------- |
| checkOnSelect  | `boolean` | false | 用多选且显示checkbox, 选中行后勾选前面的checkbox |
| clearSelectionOnEmpty  | `boolean` | true | 当数据源为空时，清空已选记录 |
| keepSelectingOnClick  | `boolean` | true | 允许重复点击行是保留选中状态 |
| keepSelectingOnPaging  | `boolean` | true | 允许跨页多选 |
| selectOnCheck  | `boolean` | false | 启用多选且显示checkbox, 勾选后并且选中行 |
| enableSelectRow  | `boolean` | true | 允许选中行 |
| multiSelect  | `boolean` | false | 启用多选 |
| showCheckbox  | `boolean` | false | 每行前边显示复选框 |
| showSelectAll  | `boolean` | false | 显示全选复选框 |

## 事件

| 事件名 | 回调参数                | 说明         |
| :----- | :------------------ | :----------- |
| click-row  | `Function(index,dataItem)` | 点击行事件 |
| double-click-row  | `Function(index,dataItem)` | 双击行事件 |
| selection-change  | `Function(selectedItems)` | 勾选事件 |
| enter-up-in-last-cell  | `Function()` | 进入最后一个单元格事件 |
| page-index-changed  | `Function(newPageIndex)` | 页码切换事件 |
| page-size-changed  | `Function(newPageSize)` | 每页数量切换事件 |



## 插槽

::: tip
暂无内容
:::



