# Rate 评分

用于评分。

## 基本用法
 
:::vdemo
   
```vue
{demos/rate/basic.vue}
```
:::

## 大小

:::vdemo

```vue
{demos/rate/size.vue} 
```

:::

## 图标

:::vdemo

```vue
{demos/rate/icon.vue} 
```

:::

## 禁用

:::vdemo

```vue
{demos/rate/disabled.vue}
```

:::

## 半选模式

:::vdemo

```vue
{demos/rate/enable-half.vue}
```

:::

## 设置颜色

:::vdemo

```vue
{demos/rate/color.vue}
```

:::

## 设置星星个数

:::vdemo

```vue
{demos/rate/num-of-star.vue}
```

:::

## 显示满意度

:::vdemo

```vue
{demos/rate/enable-satisfaction.vue}
```

:::

## 自定义满意度文案

:::vdemo

```vue
{demos/rate/tool-tip-contents.vue}
```

:::

## 启用清除

:::vdemo

```vue
{demos/rate/enable-clear.vue}
```

:::

## 类型

```typescript
type size = 'small' | 'middle' | 'large' | 'extraLarge';
```
## 属性

| 属性名     | 类型                   | 默认值    | 说明                 |
| :--------- | :--------------------- | :-------- | :------------------- |
| size         | `string`               | 'large'        | 可选，星星大小，默认为`large`   |
| enableHalf | `boolean` | false | 可选，是否启用半选模式         |
| enableClear    | `boolean`              | false     | 可选，是否启用再次点击后清除 |
| disabled       | `boolean`   | false   | 可选，是否禁用             |
| pointSystem | `number` | 5 | 可选，分制系统，默认为5分制 |
| lightColor | `string` | -- | 可选，星星亮色 |
| darkColor | `string` | -- | 可选，星星暗色 |
| iconClass | `string` | 'f-icon-star' | 可选，评分图标样式 |
| numOfStar | `number` | 5 | 可选，星星个数 |
| toolTipContents | `string[]` | ['很不满意', '不满意', '一般', '满意', '非常满意'] | 默认满意度文案 |
| enableSatisfaction | `boolean` | false | 是否显示满意度信息 |
| value | `number` | 0 | 绑定值 |

## 事件

| 事件名 | 类型                | 说明         |
| :----- | :------------------ | :----------- |
| selectedValue  | `EventEmitter<number>` | 选定分值 |

## 插槽

::: tip
暂无内容
:::
