# Page Footer 页脚

页脚组件常用于页面的尾部区域。

## 应用样例

:::vdemo

```vue
{demos/page-footer/basic.vue}
```

:::

## 内容区模板

组件默认插槽将被展示到内容区域，内容区域支持通过属性`contentClass`配置自定义样式。

:::vdemo

```vue
{demos/page-footer/content.vue}
```

:::

## 上方扩展区域

通过插槽`#header`，可以配置上方扩展区域模板。上方扩展区域支持通过属性`headerClass`配置自定义样式。<br>
配置上方扩展区域后，上方扩展区域左侧内容插槽`#headerContent`和上方扩展区域右侧按钮插槽`#headerToolbar`将失效。

:::vdemo

```vue
{demos/page-footer/header.vue}
```

:::

## 上方扩展区域左侧、右侧模板

页脚组件支持将上方扩展区域拆分为两个部分，左侧为内容区，右侧为按钮区。<br>
通过插槽`#headerContent`，可以配置上方扩展区域左侧内容区域模板。通过属性`headerContentClass`配置此区域的自定义样式。<br>
通过插槽`#headerToolbar`，可以配置上方扩展区域右侧按钮区域模板。通过属性`headerToolbarClass`配置此区域的自定义样式。<br>
:::vdemo

```vue
{demos/page-footer/header-content.vue}
```

:::

## 收折

组件支持将内容区域收起或者展开<br>

:::vdemo

```vue
{demos/page-footer/collapse.vue}
```

:::


## 属性

| 属性名             | 类型      | 默认值 | 说明                       |
| :----------------- | :-------- | :----- | :------------------------- |
| enableCollapse     | `Boolean` | false  | 是否启用内容区收折         |
| collapsed          | `Boolean` | true   | 是否默认收折内容区         |
| collapsedText      | `String`  | 'More' | 收折按钮文本               |
| expandedText       | `String`  | 'More'   | 展开按钮文本               |
| contentClass       | `String`  |        | 内容区域样式               |
| headerClass        | `String`  |        | 页脚组件样式               |
| headerContentClass | `String`  |        | 上方扩展区域左侧内容区样式 |
| headerToolbarClass | `String`  |        | 上方扩展区域右侧按钮区样式 |
| showHeader         | `Boolean` | false  | 是否显示上方扩展区域       |


## 插槽

| 插槽名 | 说明         |
| :----- | :----------- |
| header  | 上方扩展区域 |
| headerContent  | 上方扩展区域左侧内容区域 |
| headerToolbar  | 上方扩展区域右侧按钮区域 |
|   | 默认插槽：内容区域 |

