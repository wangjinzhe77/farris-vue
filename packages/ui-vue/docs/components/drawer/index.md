# Drawer 抽屉


## 基本用法

:::vdemo

```vue
{demos/drawer/basic.vue}
```

:::

## 打开位置

:::vdemo

```vue
{demos/drawer/position.vue}
```

:::


## 属性

| 属性 | 类型                | 默认值         |说明         |
| :----- | :------------------ | :----------- |:----------- |
| backgroundColor  | `string` | #fff(白色) | 背景色 |
| height  | `number 或 string` | 300 | 高度 |
| modelValue  | `boolean` | false | 开启或者关闭 |
| position  | `string` | right | 弹出位置 |
| showClose  | `boolean` | true | 是否展示关闭按钮 |
| showMask  | `boolean` | true | 是否展示遮罩层 |
| title  | `string` | -- | 标题 |
| width  | `number 或 string` | 300 | 宽度 |

## 插槽

::: tip
暂无内容
:::



