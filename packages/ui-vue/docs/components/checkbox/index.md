# CheckBox 多选组

 CheckBox组件用来进行多选。

## 基本用法 

:::vdemo

```vue
{demos/checkbox/basic.vue}
```

:::
## 组件状态 

:::vdemo

```vue
{demos/checkbox/status.vue}
```

:::
## 数据值类型 

:::vdemo

```vue
{demos/checkbox/valueType.vue}
```

:::

## 自定义字段 

:::vdemo

```vue
{demos/checkbox/field.vue}
```

:::

## 复选框模板用法 

:::vdemo

```vue
{demos/checkbox/checkbox_template.vue}
```

:::

## 按钮样式 

:::vdemo

```vue
{demos/checkbox/checkbox_button.vue}
```

:::


## 单个复选框 

:::vdemo

```vue
{demos/checkbox/checkbox.vue}
```

:::

## f-checkbox-group属性
```typescript
type Checkbox = {
    [key: string]: any;
    /**
     * 枚举值
     */
    value: any;
    /**
     * 枚举展示文本
     */
    name: string
};
```

| 属性名        | 类型                      | 默认值                | 说明                                                  |
| :-----------  | :---------                | :------------------- | :-------------------------|
| id            | `string`                  | ''                   | 组件标识                   |
| disable       | `boolean`                 | false                | 是否禁用                  |
| enumData      | `Array<Checkbox>`         | --                 | 废弃            |
| data      | `Array<Checkbox>`         | []                   | 枚举项数组             |
| direction    | `string`                 | `'horizontal'`             | 枚举项的排列方向，默认为横向           |
| isStringValue | `boolean`                 | true                 | 值类型是否为字符串          |
| loadData      | `number`                  | --                   | 异步获取枚举数组方法        |
| modelValue    | `[String, Array<string>]` | --                   | 组件值，字符串或者数组      |
| name          | `number`                  | `''`                 | 组件名称,用以区分同时引用多个组件的情况|
| separator     | `string`                  | `','`                | 分隔符， 默认逗号           |
| tabIndex      | `number`                  | 0                    | 输入框Tab键索引            |
| textField     | `string`                  | `'name'`             | 枚举数组中展示文本的key值   |
| valueField    | `string`                  | `'value'`            | 枚举数组中枚举值的key值     |


## 事件


| 事件名        | 类型                      | 说明                       |
| :-----------  | :------------------------ | :-------------------------|
| changeValue   | `EventEmitter<any> `        | 值变化回调                 |


## 插槽

::: tip
暂无内容
:::
