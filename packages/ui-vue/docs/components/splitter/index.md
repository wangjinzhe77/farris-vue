# Splitter 布局容器

Splitter 是一种布局容器组件，与结构类样式结合，通常应用在左右结构或上下结构中。支持内容自定义、収折。

## 基本用法

:::vdemo

```vue
{demos/splitter/basic.vue}
```

:::

## 指定方向

:::vdemo

```vue
{demos/splitter/direction.vue}
```

:::

## 面板用法

:::vdemo

```vue
{demos/splitter/pane.vue}
```

:::

## 面板启用拖拽

:::vdemo

```vue
{demos/splitter/resizable.vue}
```

:::

## 面板指定位置

:::vdemo

```vue
{demos/splitter/position.vue}
```

:::

## 面板指定宽度

:::vdemo

```vue
{demos/splitter/width.vue}
```

:::

## 面板指定最小宽度

:::vdemo

```vue
{demos/splitter/min-width.vue}
```

:::

## 面板指定高度

:::vdemo

```vue
{demos/splitter/height.vue}
```

:::

## 面板指定最小高度

:::vdemo

```vue
{demos/splitter/min-height.vue}
```

:::

## 属性

| 属性名      | 类型     | 默认值       | 说明                                                    |
| :---------- | :------- | :----------- | :------------------------------------------------------ |
| customClass | `string` | --           | 组件自定义样式                                          |
| customStyle | `string` | --           | 组件自定义 style                                        |
| direction   | `string` | row 左右排列 | 指定区域内的内容排列方向，column 上下排列，row 左右排列 |

## 面板属性

| 属性名      | 类型     | 默认值 | 说明             |
| :---------- | :------- | :----- | :--------------- |
| customClass | `string` | --     | 组件自定义样式   |
| customStyle | `string` | --     | 组件自定义 style |
| width       | `number` | --     | 指定面板初始宽度 |
| height      | `number` | --     | 指定面板初始高度 |
| position      | `SplitterPanePosition` | left 在容器左侧    | 面板在容器中的位置,left:容器左侧，center:容器中间，right：容器右侧，top：容器顶部，bottom:容器底部。如果启用了拖拽，position如果指定left，拖拽条在右侧；指定right,拖拽条在左侧；指定top,拖拽条在底部；指定bottom，拖拽条在顶部，指定main，没有拖拽条。 |
| resizable      | `boolean` | false没有启用拖拽     | 是否启用拖拽 |
| resizeHandle      | `string` | --    |指拖拽的方向，e向东,n向北,s向南,w向西 |
| minWidth       | `number` | 0     | 指定拖拽面板最小宽度 |
| minHeight      | `number` | 0    | 指定拖拽面板最小高度 |

## SplitterPanePosition
```
type SplitterPanePosition = 'left' | 'center' | 'right' | 'top' | 'bottom';

```