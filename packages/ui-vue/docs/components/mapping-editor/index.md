# MappingEditor 枚举数据编辑器

Mapping Editor 组件用来管理枚举类型的数据

## 基本用法

:::vdemo

```vue
{demos/mapping-editor/basic.vue}
```

:::



## 属性 

| 属性名    | 类型      | 默认值   | 说明     |
| :-----   | :-------  | :-----  | :------- |
| mappingFields       | `Record<string, string>`  | `{}` | 映射字段对象  |
| disabled | `boolean` | `false` | 是否禁用  |
| readonly | `boolean` | `false` | 是否只读      |
| modalWidth | `boolean` | `false` | 弹窗宽度      |
| modalHeight | `boolean` | `false` | 弹窗高度      |
| title | `Function` | `字段选择器` | 弱窗标题     |

`mappingFields`: 映射字段对象， key 为数据源字段， value 为目标字段；

如： { code: 'targetCode' }  
`code`: 数据源字段
`targetCode`: 目标字段

## 事件 


| 事件名        | 类型                      | 说明                       |
| :-----------  | :------------------------ | :-------------------------|
| submitModal   | `Record<string, string> `       | 窗口确定按钮点击事件，返回新的映射字段对象|
