# Accordion 手风琴

Accordion 组件为开发者提供了一种可收折的导航面板。

## 基本用法

:::vdemo

```vue
{demos/accordion/basic.vue}
```

:::
## 设置默认展开

:::vdemo

```vue
{demos/accordion/active.vue}
```

:::
## 设置组件禁用

:::vdemo

```vue
{demos/accordion/disable.vue}
```

:::

## f-accordion属性

| 属性名      | 类型       | 默认值 | 说明                                                  |
| :---------- | :--------- | :----- | :--------------------------------------------------- |
| customClass | `string`   | ''     | 组件自定义样式，多个时使用空格分隔,如'class1 class2'   |
| enableFold  | `boolean`  | true   | 是否允许收折                                          |
| expanded    | `boolean`  | false  | 是否展开所有面板                                      |
| height      | `number`   | --     | 设置组件的高度                                        |
| width       | `number`   | --     | 设置组件的宽度                                        |

## f-accordion-item属性

| 属性名      | 类型       | 默认值 | 说明             |
| :---------- | :--------- | :----- | :-------------- |
| disable     | `boolean`  | false  | 是否禁用       |
| title       | `string`   | ''     | 标题            |
| height      | `number`   | --     | 设置组件的高度   |
| width       | `number`   | --     | 设置组件的宽度   |
| active      | `boolean`  | false  | 是否默认展开   |
## 插槽

::: tip
暂无内容
:::
