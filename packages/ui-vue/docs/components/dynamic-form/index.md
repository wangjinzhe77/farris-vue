# Dynamic Form 动态输入组件

Dynamic Form 组件用来输入文本。

## 基本用法

:::vdemo

```vue
{demos/dynamic-form/basic.vue}
```

:::

## 动态组件渲染
可以代替form表单使用

:::vdemo

```vue
{demos/dynamic-form/dynamic-component.vue}
```

:::

## 设置 id

:::vdemo

```vue
{demos/dynamic-form/id.vue}
```

:::

## 显示标签 label

:::vdemo

```vue
{demos/dynamic-form/show_label.vue}
```

:::

## 显示默认值

:::vdemo

```vue
{demos/dynamic-form/show_value.vue}
```

:::


## 填充内容不允许为空

:::vdemo

```vue
{demos/dynamic-form/required.vue}
```

:::

## 属性

| 属性名      | 类型      | 默认值 | 说明               |
| :---------- | :-------- | :----- | :----------------- |
| id          | `string`  | ''     | 组件标识            |
| label       | `string`  | ''     | 标签名           |
| showLabel   | `boolean` | true   | 是否显示组件 label |
| modelValue  | `string`  | ''     | 组件默认值         |
| customClass | `string`  | ''     | 组件自定义样式     |
| editor    | `EditorConfig` | --  | 编辑器配置属性 |


## EditorConfig

| 属性名      | 类型      | 默认值 | 说明               |
| :---------- | :-------- | :----- | :----------------- |
| type          | `EditorType`  | --     | 组件类型 |
| customClass       | `string`  | ''     | 自定义类名           |
| readonly   | `boolean` | false   | 是否只读 |
| disable   | `boolean` | false   | 是否禁用 |
| required  | `boolean`  | false     | 是否必填         |
| placeholder | `string`  | ''     | 提示文本     |
| [key: string]    | `--` | --  | 其他属性，可以配置相应组件的属性 |

```ts
type EditorType = 'button-edit' | 'check-box' | 'check-group' | 'combo-list' | 'combo-lookup'
    | 'date-picker' | 'date-range' | 'datetime-picker' | 'datetime-range' | 'events-editor' | 'month-picker' | 'month-range' |
    'year-picker' | 'year-range' | 'input-group' | 'lookup' | 'number-range' | 'number-spinner' | 'radio-group' | 'text' |
    'response-layout-editor-setting' | 'switch';
```