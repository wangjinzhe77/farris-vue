# Pagination 分页条

Pagination 组件用来展示分页信息。

## 基本用法

:::vdemo

```vue
{demos/pagination/basic.vue}
```

:::

## 精简模式

:::vdemo

```vue
{demos/pagination/simple.vue}
```
:::


## 类型

```typescript
type PaginationMode = 'default' | 'simple';
```

## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| id     | `string` | --     | 组件标识 |
| currentPage | `number` | 1 | 当前页码 |
| mode | `PaginationMode` | 'default' | 模式 |
| pageSize | `number` | 20 | 分页大小 |
| totalItems | `number` | 0 | 数据条数 |

## 事件

| 事件名 | 类型                | 说明         |
| :----- | :------------------ | :----------- |
| pageIndexChanged  | `EventEmitter<number>` | 页码切换事件 |
| pageSizeChanged  | `EventEmitter<number>` | 分页大小切换事件 |

## 插槽

::: tip
暂无内容
:::
