# Page Header 页头

页头组件用于展示标题和操作按钮等内容，常用于页面的头部区域。

## 标题

:::vdemo

```vue
{demos/page-header/title.vue}
```

:::

## 图标

:::vdemo

```vue
{demos/page-header/icon.vue}
```

:::

## 按钮

按钮区域为响应式工具栏，支持根据屏幕尺寸自动调整显示按钮的个数。
通过属性`buttonClass`可以指定按钮区的自定义样式，例如宽度占比`col-5`。

:::vdemo

```vue
{demos/page-header/buttons.vue}
```

:::

## 翻页
 
页头组件支持向前翻页和向后翻页的图标按钮，常用于卡片类的单据页面。
:::vdemo

```vue
{demos/page-header/pagination.vue}
```

:::

## 自定义模板
组件提供以下三个区域的自定义插槽模板： <br>
1、左侧标题区域模板，插槽`titleContent`，自定义样式属性`titleContentClass`  <br>
2、中间内容区域模板，插槽`content`，自定义样式属性`contentClass` <br>
3、下方扩展区域模板，插槽`downContent`，自定义样式属性`downContentClass`  <br>

:::vdemo

```vue
{demos/page-header/template.vue}
```

:::

## 属性

| 属性名      | 类型                         | 默认值                     | 说明                             |
| :---------- | :--------------------------- | :------------------------- | :------------------------------- |
| customClass    | `string`                    |                        | 组件自定义样式                     |
| showIcon    | `boolean`                    | true                       | 是否显示图标                     |
| icon        | `string`                     | 'f-icon-page-title-record' | 图标样式名称                     |
| iconClass   | `string`                     | 'f-text-orna-bill'         | 图标自定义样式，例如颜色、背景等 |
| title   | `string`                     |                            | 主标题文本                       |
| subTitle    | `string`                     |                            | 副标题文本                       |
| buttons     | `Array<ResponseToolbarItem>` |                            | 按钮组                           |
| buttonClass | `string`                     | 'col-6'                    | 按钮组自定义样式                 |
| titleContentClass | `string`                     |                    | 左侧标题区域自定义样式                 |
| contentClass | `string`                     |                    | 中间内容区域自定义样式                 |
| downContentClass | `string`                     |                    | 下方扩展区域自定义样式                 |
| showPagination | `boolean`                     |     false               | 是否显示翻页图标                 |
| prePaginationDisabled | `boolean`                     |     true               | 向前翻页是否禁用                 |
| nextPaginationDisabled | `boolean`                     |      false              | 向后翻页是否禁用                 |

### 按钮类型

```typescript
interface ResponseToolbarItem {
    /** 工具栏项标识 */
    id: string
    /** 图标 */
    icon: string
    /** 文本 */
    text: boolean
    /** 是否可用 */
    enable: boolean
    /** 是否可见 */
    visible: boolean
    /** 点击事件
     * $event: 鼠标点击事件对象
     * itemId: 当前点击的按钮标识 */
    onClick: ($event: MouseEvent, itemId: string) => void = () => {}
}
```

## 事件

| 事件名 | 说明         |
| :----- | :----------- |
| click  | 按钮点击事件 |
