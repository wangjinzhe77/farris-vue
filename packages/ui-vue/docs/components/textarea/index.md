# Textarea 多行文本框

Textarea 组件用来输入多行文本。

## 基础用法

:::vdemo

```vue
{demos/textarea/basic.vue}
```

:::

## 只读禁用

:::vdemo

```vue
{demos/textarea/disabled.vue}
```

:::