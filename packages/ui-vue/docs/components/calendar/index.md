# Calendar 日历组件

Calendar 日历组件以日、周、月三种视图提供管理日程操作。

## 基本用法 

:::vdemo

```vue
{demos/calendar/basic.vue}
```
:::
## 类型

```typescript
type weekDays = 'Sun' | 'Mon' | 'Tue' | 'Wed' | 'Thu' | 'Fri' | 'Sat' ;

```


## 属性

| 属性名              | 类型               | 默认值       | 说明              |
| :----------------- | :--------------------- | :----------- | :--------------- |
| events             | `Array<ScheduleEvent>` | []           | 事件列表         |
| firstDayOfTheWeek  | `String as weekDays `  | `Sun`        | 周内起始日       |


## ScheduleEvent属性
| 属性名              | 类型               | 默认值       | 说明           |
| :----------------- | :------------------| :----------- | :--------------|
| starts             | `DateObject`       | `null`       | 开始时间       |
| ends               | `DateObject`       | `null`       | 结束时间       |
| title              | `String `          | `''`        | 事项描述       |
## DateObject
| 属性名              | 类型               | 默认值     | 说明      |
| :----------------- | :------------------| :----------| :--------|
| year               | `Number`           | `0`        | 年       |
| month              | `Number`           | `0`        | 月       |
| day                | `Number `          | `0`        | 日       |
| hour               | `Number `          | `0`        | 小时     |
| minute             | `Number `          | `0`        | 分钟     |
| second             | `Number `          | `0`        | 秒       |
## 插槽

::: tip
暂无内容
:::
