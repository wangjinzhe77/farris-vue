# Lookup 参照数据选择组件

Lookup 组件方便用户选择已有数据

## 基本用法

:::vdemo

```vue
{demos/lookup/basic.vue}
```

:::

## 树表模式

:::vdemo

```vue
{demos/lookup/lookup-tree.vue}
```

:::

## 多选

:::vdemo

```vue
{demos/lookup/multi-select.vue}
```

:::


## 双列表 

:::vdemo

```vue
{demos/lookup/list-list.vue}
```

:::

## 左树右列表 

:::vdemo

```vue
{demos/lookup/tree-list.vue}
```

:::


## 打开隐藏帮助 

:::vdemo

```vue
{demos/lookup/open-hidden-lookup.vue}
```

:::

## 类型定义
```ts
export type ShouldContinueResult = {continue: boolean; message?: string} | boolean;
export type BeforeOpenDialogFunction = (params: any) => ShouldContinueResult;
export type BeforeSubmitFunction = (params: any) => ShouldContinueResult;

/** 搜索参数 */
export interface SearchParams {
    field: string;
    value: string;
    isNavigation: boolean;
}

/** 分页页码改变时，分页大小改变时 触发 */
export interface PagerChangeParams{
    pageIndex?: number;
    pageSize?: number;
    isNavigation: boolean;
}

```


## 属性

| 属性名         | 类型      | 默认值              | 说明                               |
| :------------- | :-------- | :------------------ | :-------------------------------- |
| id            | `string` | '' | 组件标识ID  |
| readonly        | `boolean`  | false | 是否只读   |
| disabled       | `boolean` | false   | 是否禁用   |
| editable       | `boolean`  | false  | 是否允编                    |
| fitColumns | `boolean`  | true       | 是否自动调整列宽，默认为 true  |
| columns         | `Array<>`   | [] |  显示列配置，参考表格组件的 columns 属性 |
| idField | `string`    | 'id' | 标识字段 |
| textField | `string`    | 'name'| 显示文本字段 |
| data | `Array<any>` | [] |  数据源 |
| displayType| `string` | 'LIST' | 显示类型，可选值：LIST(普通表格)、TREELIST(树表)、NAVLIST(双列表)、NAVTREELIST(左树右列表) |
| multiSelect | `boolean` | false | 是否多选 |
| separator | `string` | ',' | 多选时，分隔符，默认为 ',' |
| dialog| `object` | {} | 弹窗配置，参考弹窗组件的 props 属性 |
| enableSearchBar | `boolean` | false | 是否显示搜索框 |
| pagination | `object` | `{ enable: false, size: 20,index: 1, list: [10, 20,30 50, 100] }` | 分页配置，参考表格组件的 pagination 属性 |
| enableClear | `boolean` | true | 是否显示清除按钮 |
| idValue | `string` | '' | 默认值 |
| searchFields | `Array<SearchField>` | [] | 搜索字段，默认为空数组，即不搜索 |
| showAllSearchColumns | `boolean` | false | 是否显示搜索全部字段 |
| navigation | `NavigationOptions` | null | 导航配置，参考表格组件的 navigation 属性 |
| dictPicking | `Function` | null | 窗口打开前事件 |
| beforeSelectData | `Function` | null | 确认选中数据前触发 |
| mappingData | `Function` | null | 数据映射回调函数 |
| dictPicked | `Function` | null | 确认选中数据后触发 |


## 事件

| 事件名 | 参数                                     | 说明                                  |
| :----- | :--------------------------------------- | :------------------------------------ |
| dialogClosed | `` | 窗口关闭后触发 |
| search | `params:<SearchParams>` | 搜索时触发，返回搜索参数 |
| pageIndexChanged | `params:<PagerChangeParams>` | 分页页码改变时触发 |
| pageSizeChanged | `params:<PagerChangeParams>` | 分页大小改变时触发 |
| navSelectionsChanged| `params: <{items: Array<any>, ids: Array<string>}>` | 导航选中项改变时触发 |

## 实例方法
| 方法名 | 参数 | 说明 |
| :----- | :--- | :--- |
| openDialog| - | 打开弹窗 |