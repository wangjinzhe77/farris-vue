# Number Spinner 数字输入框

Number Spinner 数字输入框用来展示和录入数字类数据。

<!-- ## 基本用法1

:::vdemo

```vue
{demos/number-spinner/basic.vue}
```
::: -->

## 基本用法

:::vdemo

```vue
{demos/number-spinner/appearance.vue}
```

:::

## 禁用

:::vdemo

```vue
{demos/number-spinner/disabled.vue}
```

:::

## 只读

:::vdemo

```vue
{demos/number-spinner/readonly.vue}
```

:::

## 可编辑

:::vdemo

```vue
{demos/number-spinner/editable.vue}
```

:::

## 最大值 最小值

:::vdemo

```vue
{demos/number-spinner/max-min-value.vue}
```

:::

## 格式类

:::vdemo

```vue
{demos/number-spinner/format.vue}
```

:::

## 空值 0值

:::vdemo

```vue
{demos/number-spinner/zero-null.vue}
```

:::

## 属性

| 属性名     | 类型             | 默认值 | 说明                              |
| :--------- | :--------------- | :----- | :-------------------------------- |
| modelValue      | `number/string`         |      | 组件值                          |
| disabled      | `boolean`         | false     | 是否禁用                          |
| readonly      | `boolean`         | false    | 是否只读                          |
| editable     | `boolean`         | true    | 是否可编辑                          |
| placeholder    | `string` | 请输入数字     | 空白提示文本                          |
| showButton | `boolean`        | true  | 是否显示微调按钮 |
| textAlign     | `string`         | left    | 文本对齐方式 : 左对齐left; 居中对齐center ;右对齐 right                         |
| max | `number`        | 1  | 最大值 |
| min | `number`        | 1  | 最小值 |
| useThousands | `boolean`        | true  | 是否使用千分值 |
| groupSeparator | `string`        | ,  | 千分位符号 |
| groupSize | `number`        | 3  | 使用千分位时，每组显示的字符数 |
| precision | `number`        | 0  | 精度 |
| prefix | `string`        |   | 前缀 |
| suffix | `string`        |   | 后缀 |
| decimalSeparator | `string`        |  . | 小数点符号 |
| nullable | `boolean`        | false  | 是否允许为空 |
| showZero | `boolean`        |  true | 是否显示0值 |
| formatter | `Function`        |   | 自定义格式化方法 (val: number) => string |
| parser | `Function`        |   | 自定义反格式化方法  (val: string \| number) => number|


