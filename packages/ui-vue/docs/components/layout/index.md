# Layout 布局

导航条，用于展示指定的导航信息

## 基本用法

:::vdemo

```vue
{demos/layout/basic.vue}
```

:::
## 垂直布局

:::vdemo

```vue
{demos/layout/vertical.vue}
```

:::
## 个性化配置

:::vdemo

```vue
{demos/layout/advanced.vue}
```

:::

## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| customClass     | `string` | --    | 自定义样式|

## LayoutPane

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| customClass     | `string` | --    | 自定义样式|
| width     | `number` | 100, 默认宽度 `100px`    | 设置初始宽度|
| height     | `number` | 100, 默认高度 `100px` | 设置初始高度|
| position     | `LayoutPanePosition` | left    | 面板所处的位置|
| visible     | `boolean` | true   | 面板是否可见|
| resizable     | `boolean` | true    | 是否可拖拽改变大小|
| minWidth     | `number` | 100   | 设置最小宽度，当拖拽改变宽度不能少于此宽度|
| minHeight     | `number` | 100   | 设置最小高度，当拖拽改变宽度不能少于此高度|


## 插槽

::: tip
暂无内容
:::
