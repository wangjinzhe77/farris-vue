# Combo List 选择输入框

Combo List 组件提供下拉框形式选择枚举数据。

## 基本用法

:::vdemo

```vue
{demos/combo-list/basic.vue}
```

:::

## 绑定字段

:::vdemo

```vue
{demos/combo-list/field.vue}
```

:::
## 占位符

:::vdemo

```vue
{demos/combo-list/placeholder.vue}
```

:::
## 展示相关

:::vdemo

```vue
{demos/combo-list/interface.vue}
```

:::

## 组件状态

:::vdemo

```vue
{demos/combo-list/status.vue}
```

:::

## 可编辑

:::vdemo

```vue
{demos/combo-list/editable.vue}
```

:::

## 支持多选

:::vdemo

```vue
{demos/combo-list/multi.vue}
```

:::

## 下拉面板

:::vdemo

```vue
{demos/combo-list/search.vue}
```

:::

## 设置下拉面板最大高度

:::vdemo

```vue
{demos/combo-list/height.vue}
```

:::

## 打开前方法

:::vdemo

```vue
{demos/combo-list/before-open.vue}
```

:::

## 数据来自远端

:::vdemo

```vue
{demos/combo-list/remote.vue}
```

:::
## 事件

:::vdemo

```vue
{demos/combo-list/event.vue}
```

:::

## 类型

```typescript
type Placement = 'top' | 'bottom' | 'auto'
type ViewType = 'text' | 'tag'
type BeforeOpenFunction = (params: any) => boolean | Promise<boolean>
interface Remote {
    url: string;
    method?: 'GET' | 'POST' | 'PUT';
    headers?: any;
    body?: any;
}
```

## 属性

| 属性名           | 类型                  | 默认值  | 说明                                                                                  |
| :--------------- | :-------------------- | :------ | :------------------------------------------------------------------------------------ |
| id               | `string`              |         | 组件标识                                                                              |
| data             | `Options`             | `[]`    | 数据源                                                                                |
| editable         | `boolean`             | `false` | 是否可编辑                                                                            |
| disabled         | `boolean`             | `false` | 是否禁用                                                                              |
| readonly         | `boolean`             | `false` | 是否只读                                                                              |
| dropDownIcon     | `string`              |         | 可选，下拉按钮图标                                                                    |
| enableClear      | `boolean`             | `true`  | 可选，是否启用清空按钮                                          |
| enableSearch     | `boolean`             | `false` | 可选，是否启用搜索                                                                    |
| enableTitle      | `boolean`             | `true`  | 可选，鼠标悬停时是否显示控件值                                                        |
| fitEditor        | `boolean`             | `false` | 自适应宽度                                                                            |
| forcePlaceholder | `boolean`             | `false` | 可选，是否在禁用或者只读状态，强制显示指定的的占位符                                                              |
| hidePanelOnClear | `boolean`             | `true`  | 可选，清空值时隐藏面板                                                                |
| idField          | `string`              | `id`    | 可选，数据源 id 字段                                                                  |
| mapFields        | `object`              |         | 可选，字段映射                                                                        |
| maxHeight        | `number`              | `350`   | 可选，下拉面板最大高度                                                                |
| maxLength        | `number`              | ``      | 最大输入长度                                                                          |
| multiSelect      | `boolean`             | `false` | 可选，是否支持多选                                                                    |
| placeholder      | `string`              | ''      | 值为空时，输入框的占位符                                                                                |
| placement        | `String as Placement` |         | 可选，下拉面板展示位置                                                                |
| remote           | `Remote`              | `null`  | 可选，远端数据源信息                                                                  |
| remoteSearch     | `boolean`             | `false` | 可选，是否支持远端过滤                                                                |
| separator        | `string`              | `,`     | 可选，多选时值分隔符                                                                  |
| tabIndex         | `number`              | `-1`    | tab index                                                                             |
| valueField       | `string`              | `id`    | 可选，数据源值字段                                                                    |
| textField        | `string`              | `name`  | 可选，数据源显示字段                                                                  |
| titleField       | `string`              | `name`  | 可选，数据源的 title                                                                  |
| viewType         | `text 按照文本查看|tag按照标签查看`  | ``  | 可选，多选情况下，按照哪种方式查看                                                             |
| beforeOpen       | `BeforeOpenFunction`  | `null`  | 可选，在弹出下拉面板前的调用的方法。返回 false 或者异步返回 false，就阻止打开下拉面板 |

## 事件

| 事件名            | 参数                       | 说明                   |
| :---------------- | :------------------------- | :--------------------- |
| clear             | -                          | 清空方法               |
| update:modelValue | 当前组件的值               | 绑定的值变化时触发     |
| input             | 输入框中的值               | 启动编辑后，输入时触发 |
| change            | `Array<ChangeItemObject> ` | 值变化回调             |

#### ChangeItemObject

| 属性名     | 类型      | 说明     |
| :--------- | :-------- | :------- |
| checked    | `boolean` | 是否选中 |
| disabled   | `boolean` | 是否禁用 |
| textField  | `string`  | 显示字段 |
| valueField | `string`  | 值字段   |

## 插槽

::: tip
暂无内容
:::
