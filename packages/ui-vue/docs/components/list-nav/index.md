# List Nav 列表导航组件

List Nav 组件常用作界面布局中的导航结构。

-   支持指定导航的位置，可以页面在左侧、右侧、顶部、底部
-   支持展开、收起导航
-   支持指定导航初始的展开、收起状态

## 基本用法

:::vdemo

```vue
{demos/list-nav/basic.vue}
```

:::

## 设置尺寸

:::vdemo

```vue
{demos/list-nav/size.vue}
```

:::

## 插槽

:::vdemo

```vue
{demos/list-nav/template.vue}
```

:::

## 不启用收折

:::vdemo

```vue
{demos/list-nav/collapse.vue}
```

:::

## 禁用收折

:::vdemo

```vue
{demos/list-nav/disabled.vue}
```

:::

## 初始收折状态

:::vdemo

```vue
{demos/list-nav/init-collapse.vue}
```

:::

## 设置导航位置

:::vdemo

```vue
{demos/list-nav/position.vue}
```

:::

## 事件

:::vdemo

```vue
{demos/list-nav/event.vue}
```

:::

:::

## 类型

```typescript
type position = 'top' | 'left' | 'bottom' | 'right'
```

## 属性

| 属性名       | 类型      | 默认值 | 说明                                                                                                             |
| :----------- | :-------- | :----- | :--------------------------------------------------------------------------------------------------------------- |
| title        | `string`  | ''     | 导航标题                                                                                                         |
| size         | `number`  | 218    | 指定宽度或者高度，如果导航的位置是左侧、右侧，size 指定的就是宽度，如果导航位置是顶部和底部，size 指定的就是高度 |
| collapsible     | `boolean` | true   | 是否启用收折功能，true 是启用收折，显示收折按钮，false 是禁不启用收折功能，不显示收折按钮                        |
| folded | `boolean` | false  | 初始收折状态，false 导航展开，true 导航收折是                                                                    |
| disabled     | `boolean` | false  | 是否禁用收折功能，false 是不禁用收折，收折按钮不可点击，true 是禁用收折，点击收折按钮可以收折展开                |

## 事件

| 事件名   | 参数                                                | 说明                 |
| :------- | :-------------------------------------------------- | :------------------- |
| collapse | `state:boolean` 收折状态，true 是收折，false 是展开 | 点击收折按钮触发事件 |

## 插槽

::: tip

```vue
<f-list-nav>
    <template #navHeader>
       导航标题区域
    </template>
    <template #navContent>
        导航内容区域
    </template>
    <template #navFooter>
        导航底部区域
   </template> 
 </f-list-nav>
```

:::
