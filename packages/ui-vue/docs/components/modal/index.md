# Modal 弹出窗口

Modal 组件用来已弹出模态窗口的形式展示页面内容。

## 基本用法

:::vdemo

```vue
{demos/modal/basic.vue}
```
 
:::

## 动态弹出
:::vdemo

```vue
{demos/modal/modal-service.vue} 
```

:::

## 自定义按钮-普通调用

:::vdemo

```vue
{demos/modal/default-modal-custom-buttons.vue}
```

:::

## 自定义按钮-函数式调用

:::vdemo

```vue
{demos/modal/modal-custom-buttons.vue}
```

:::

## 窗口尺寸

:::vdemo

```vue
{demos/modal/modal-custom-size.vue}
```

:::


## iframe

:::vdemo

```vue
{demos/modal/modal-iframe.vue}
```

:::


## 类型

```typescript
export interface ModalButton {
    class: string;
    focusedByDefault?: boolean;
    disable?: boolean;
    iconClass?: string;
    handle: ($event: MouseEvent) => void;
    text: string;
}
```

## 属性

| 属性名     | 类型             | 默认值 | 说明                              |
| :--------- | :--------------- | :----- | :-------------------------------- |
| title      | `string`         | --     | 窗口标题                          |
| width      | `number`         | 500    | 窗口宽度                          |
| height     | `number`         | 320    | 窗口高度                          |
| buttons     | `ModalButtton[]` | --     | 底部按钮                          |
| modelValue | `boolean`        | false  | 使用 v-model 绑定是否显示窗口属性 |

## 插槽

::: tip
暂无内容
:::
