# ImageCopper 图像裁剪

图像裁剪，通过拖拽裁剪器调整图像大小，选定裁剪的图像

## 基本用法

:::vdemo

```vue
 {demos/image-cropper/basic.vue} 
```

:::
## 禁用

:::vdemo

```vue
 {demos/image-cropper/disabled.vue} 
```

:::
## 图像对齐方式

:::vdemo

```vue
 {demos/image-cropper/align.vue} 
```

:::
## 设置图像的方式

:::vdemo

```vue
 {demos/image-cropper/file-type.vue} 
```

:::

## 设置裁剪器

:::vdemo

```vue
 {demos/image-cropper/cropper.vue} 
```

:::
## 画布

:::vdemo

```vue
 {demos/image-cropper/canvas.vue} 
```

:::
## 事件

:::vdemo

```vue
 {demos/image-cropper/events.vue} 
```

:::
## 类型

```typescript
type FormatValue = 'png' | 'jpeg' | 'bmp' | 'webp' | 'ico';
type AlignImageValue = 'left' | 'center';
```
## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| format     | `FormatValue` | png    | 生成的图片格式 (png, jpeg, webp, bmp, ico) 默认png |
| maintainAspectRatio     | `boolean` |--    | 是否保持长宽比例 默认true |
| transform     | `ImageTransform` | \{scale: 0, rotate: 0, flipH: false, flipV: false\} | 设置翻转、旋转和缩放图像 |
|  aspectRatio    | `number` |  1 | 裁剪长宽比例 width / height |
|  resizeToWidth    | `number` |  0 | 裁剪后图片被调整为的宽度 |
|  resizeToHeight    | `number` |  0 | 裁剪后图片被调整为的高度 |
|  cropperMinWidth    | `number` |  0 | 裁剪器的最小宽度 |
|  cropperMinHeight    | `number` |  0 | 裁剪器的最小高度 (如果设置maintainAspectRatio保持比例为true不生效) |
|  canvasRotation    | `number` |  0 | 旋转画布角度 (1 = 90deg, 2 = 180deg...) 默认0|
|  roundCropper    | `boolean` |  true |裁剪器展示形状是否为圆形 默认true|
|  onlyScaleDown    | `boolean` |  false |启用这个选项将确保较小的图像不会被放大 默认false|
|  imageQuality    | `number` |  92 |适用于使用jpeg或webp作为输出格式时。输入0到100之间的数字将决定输出图像的质量|
|  autoCrop    | `boolean` | true |是否每次修改裁剪器的位置或大小时，裁剪器都会发出一个图像|
|  backgroundColor    | `string` |  --|设置背景色|
|  containWithinAspectRatio    | `boolean` |  false|是否在图像周围添加填充以使其适合长宽比|
|  loadImageErrorText    | `string` |  图片加载错误|图片加载错误的提示|
|  alignImage    | `AlignImageValue` |  center|图片对齐方式 |
|  disabled    | `boolean` |  false|是否禁用 默认false |
|  imageURL    | `string` |  ./src/assets/image01.png|图片路径 |
|  imageChangedEvent    | `FileEvent` | -- |input file文件上传后对应的值 |
|  imageBase64    | `string` | -- |base64格式的图片值 |
|  imageFile    | `Blob(File)` | -- |文件 |
|  cropper    | `CropperPosition` | \{x1: -100,y1: -100,x2: 10000,y2: 10000\} |裁剪器的坐标 |

## 事件


| 事件名 | 参数                | 说明         |
| :----- | :------------------ | :----------- |
| imageCropped  |ImageCroppedEvent |图片被裁剪后事件 |
| imageLoaded  |-- |图片加载后事件 |
| cropperReady  |Dimensions |裁剪器显示后事件 |
| loadImageFailed  |-- |加载图片失败后事件 |

## ImageTransform

| 属性名 | 类型     |  说明     |
| :----- | :------- | :------- |
| scale     | `number` |  缩放比例|
| rotate     | `number` | 旋转角度 |
| flipH    | `boolean` | 是否水平翻转，true 水平翻转 |
| flipV     | `boolean` | 是否垂直翻转，true垂直翻转|

## CropperPosition

| 属性名 | 类型     |  说明     |
| :----- | :------- | :------- |
| x1    | `number` | 必有属性，x轴第一个坐标|
| y1     | `number` |必有属性，y轴第一个坐标 |
| x2    | `number` | 必有属性，x轴第二个坐标 |
| yw    | `number` | 必有属性，y轴第二个坐标|

## ImageCroppedEvent

| 属性名 | 类型     |  说明     |
| :----- | :------- | :------- |
| base64   | `string,null` |  裁剪后图片的base64值|
| file     | `blob,null` | 裁剪后图片对应的文件内容 |
| width    | `number` |必有属性，裁剪后图片宽度 |
| height    | `number` | 必有属性，裁剪后图片高度|
| cropperPosition    | `CropperPosition` | 裁剪后裁剪区相对于图片位置|
| imagePosition    | `CropperPosition` |裁剪后相对于原图片位置|
| offsetImagePosition    | `CropperPosition` | containWithinAspectRatio为真时，裁剪器相对于原始图像的位置|

## Dimensions

| 属性名 | 类型     |  说明     |
| :----- | :------- | :------- |
| width   | `number` | 必有属性，宽度|
| height    | `number` |必有属性，高度|


## 插槽

::: tip
暂无内容
:::
