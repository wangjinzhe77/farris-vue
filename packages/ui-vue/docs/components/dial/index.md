# Dial 仪表盘

Dial 提供了一个仪表盘组件。

## 基本用法

:::vdemo

```vue
{demos/dial/basic.vue}
```
  
:::

## 属性

| 属性名           | 类型                  | 默认值     | 说明            |
| :----------------| :-------             | :-----     | :-------------- |
| modelValue       | `number`             | null       | 传入数值         |
| min              | `number`             | 0          | 最小值          |
| max              | `number`             | 100        | 最大值          |
| valueTemplate    | `string`             | `{value}`  | 字符串模板      |
| fontColor        | `string`             | `#000000'` | 字体颜色        |
| strokeWidth      | `number`             | 8          | 边框整体大小    |
| size             | `number`             | 100        | 刻度盘大小（直径 |  
| valueColor       | `string`             | `#eff2f4`  | 刻度盘默认颜色   |
| rangeColor       | `string`             | `#116fee`  | 刻度盘刻度颜色   |


## 插槽

::: tip
暂无内容
:::
