# Text 静态文本

Text 组件用来展示只读态的页面数据。

1. 可以根据指定的类型，按照对应的规则展示数据
2. 可以指定不同的对齐方式
3. 可以指定高度或者最大、最小高度，适应不同的场景

## 基础用法

:::vdemo

```vue
{demos/text/basic.vue}
```

:::

## 高度

:::vdemo

```vue
{demos/text/height.vue}
```

:::

## 对齐

:::vdemo

```vue
{demos/text/text-align.vue}
```

:::

## 布尔

:::vdemo

```vue
{demos/text/bool.vue}
```

:::

## 枚举

:::vdemo

```vue
{demos/text/enum.vue}
```

:::

## 数值

:::vdemo

```vue
{demos/text/number.vue}
```

:::

## 数值参数

:::vdemo

```vue
{demos/text/number-options.vue}
```

:::

## 日期

:::vdemo

```vue
{demos/text/date.vue}
```

:::

## 日期区间

:::vdemo

```vue
{demos/text/date-range.vue}
```

:::

## 事件

:::vdemo

```vue
{demos/text/events.vue}
```

:::

## 属性

| 属性名              | 类型                | 默认值  | 说明                                                                                 |
| :------------------ | :------------------ | :------ | :----------------------------------------------------------------------------------- |
| modelValue          | `string`            | ''      | 组件值                                                                               |
| password            | `boolean`           | false   | 是否是密码                                                                           |
| textarea            | `boolean`           | false   | 是否多行文本                                                                         |
| autoSize            | `boolean`           | false   | 是否自动尺寸                                                                         |
| maxHeight           | `number`            | 0       | 设置最大高度，且自动高度 autoSize 为 true 时才生效                                   |
| height              | `number`            | 0       | 指定高度,且自动高度 autoSize 为 false 时，按照高度。如果 autoSize 为 true,是最小高度 |
| type                | `TextType`          | string  | 指定类型,默认指定为字符串 string 类型                                                |
| enumConvertedDatas  | `any`               |         | 转换类型用到的数据                                                                   |
| textField           | `string`            | 'name'  | 指定枚举类型时数据的文本字段                                                         |
| valueField          | `string`            | 'value' | 指定枚举类型时数据的值字段                                                           |
| enumDelimiter       | `string`            | ','     | 枚举下类型下的分隔符                                                                 |
| groupSeparator      | `string`            | ','     | 千分位符号                                                                           |
| decimalSeparator    | `string`            | '.'     | 小数点处符号                                                                         |
| decimalFilledSymbol | `string`            | '0'     | 十进制补位符                                                                         |
| numberOptions       | `FormNumberOptions` | '0'     | 格式化数字的参数                                                                     |
| amountExpression    | `string`            | '%s%v'  | 金额表达式                                                                           |
| textAlign           | `TextAlignment`         | ''      | 文本方向,默认左对齐                                                                  |
| dateRange           | `boolean`           | false   | 是否是日期范围                                                                       |
| format              | `string`            | ''      | 指定格式化模式                                                                       |
| dateRangeDelimiter  | `string`            | '~'     | 日期范围模式下值的显示分隔符                                                         |

## TextType

```typescript
type TextType = 'string' | 'date' | 'datetime' | 'number' | 'enum' | 'boolean'
type TextAlignment = 'left' | 'center' | 'right'
```

## 事件

| 事件名            | 参数       | 说明                   |
| :---------------- | :--------- | :--------------------- |
| update:modelValue | 新变更的值 | 绑定的值更新时触发     |
| valueChange       | 新变更的值 | 控件显示的值更新时触发 |

## 插槽

::: tip
暂无内容
:::
