import 'esbuild-register'
import config from './config/index'

const defaultConfig = config.default

export default config;
