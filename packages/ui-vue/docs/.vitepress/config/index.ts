import { defineConfig } from 'vitepress';
import head from './head';
import nav from './nav';
import sidebar from './sidebar';
import markdown from './markdown';
import { MarkdownTransform } from '../plugins/markdown-transform';

const config = defineConfig({
    base: '/farris-docs',
    title: 'Farris Vue',
    description: '基于 Farris Design 的前端组件库',
    head,
    markdown,
    themeConfig: {
        nav,
        sidebar,
        logo: {
            dark: '/assets/farris_design_dark.png',
            light: '/assets/farris_design_light.png'
        },
        footer: {
            message: '使用 Apache-2.0 开源许可协议',
            copyright: '© 版权所有 Copyright 2023 | 浪潮数字企业',
            address: '山东省济南市高新区浪潮路1036号'
        }
    }
});

export default config;
