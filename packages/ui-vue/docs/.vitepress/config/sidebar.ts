const sidebar = [
    {
        text: '介绍',
        items: [{ text: '快速开始', link: '/guide/quick-start/' }]
    },
    {
        text: '通用',
        items: [
            { text: 'Button 按钮', link: '/components/button/' },
            { text: 'Button Group 按钮组', link: '/components/button-group/' },
            { text: 'Icon 图标', link: '/components/icon/' },
            { text: 'Response Toolbar 响应式工具条', link: '/components/response-toolbar/' }
        ]
    },
    {
        text: '导航',
        items: [
            { text: 'Nav 导航条', link: '/components/nav/' },
            { text: 'Accordion 手风琴', link: '/components/accordion/' },
            { text: 'Step 步骤条', link: '/components/step/' },
            { text: 'Pagination 分页条', link: '/components/pagination/' }
        ]
    },
    {
        text: '布局',
        items: [
            {
                text: 'Section 面板',
                link: '/components/section/'
            },
            { text: 'Tabs 标签页', link: '/components/tabs/' },
            { text: 'Layout 布局', link: '/components/layout/' },
            { text: 'Splitter 布局', link: '/components/splitter/' }
        ]
    },
    {
        text: '录入数据',
        items: [
            { text: 'Button Edit 按钮输入框', link: '/components/button-edit/' },
            { text: 'Input Group 智能输入框', link: '/components/input-group/' },
            { text: 'Textarea 多行文本框', link: '/components/textarea/' },
            { text: 'Combo List 选择输入框', link: '/components/combo-list/' },
            { text: 'Combo Tree 下拉树输入框', link: '/components/combo-tree/' },
            { text: 'Dropdown 选择输入框', link: '/components/dropdown/' },
            { text: 'Date Picker 日期选择框', link: '/components/date-picker/' },
            { text: 'Radio Group 单选组', link: '/components/radio-group/' },
            { text: 'Checkbox Group 多选组', link: '/components/checkbox/' },
            { text: 'Color Picker 颜色选择器', link: '/components/color-picker/' },
            { text: 'Switch 开关', link: '/components/switch/' },
            { text: 'Text 静态文本', link: '/components/text/' },
            { text: 'Time Picker 时间选择框', link: '/components/time-picker/' },
            { text: 'Tags 标签', link: '/components/tags/' },
            { text: 'Rate 评分', link: '/components/rate/' },
            { text: 'Search Box 搜索框', link: '/components/search-box/' },
            { text: 'Dynamic Form 动态输入组件', link: '/components/dynamic-form/' },
            { text: 'Number Spinner 数字输入框', link: '/components/number-spinner/' },
            { text: 'Number Range 数字区间', link: '/components/number-range/' },
            { text: 'Order 排序', link: '/components/order/' },
            { text: 'Condition 条件列表', link: '/components/condition/' },
            { text: 'QuerySolution 条件列表', link: '/components/query-solution/' },
            { text: 'EnumEditor 枚举数据编辑器', link: '/components/enum-editor/' },
            { text: 'FieldSelector 字段选择器', link: '/components/field-selector/' },
            { text: 'MappingEditor 映射字段编辑器', link: '/components/mapping-editor/' },
            { text: 'Lookup 参照数据选择', link: '/components/lookup/' },
        ]
    },
    {
        text: '展示数据',
        items: [
            { text: 'Avatar 头像', link: '/components/avatar/' },
            { text: 'Image Cropper 图像裁剪', link: '/components/image-cropper/' },
            { text: 'Calendar 日历', link: 'components/calendar/' },
            { text: 'Capsule 胶囊按钮', link: '/components/capsule/' },
            { text: 'Data Grid 表格', link: '/components/data-grid/' },
            { text: 'List Nav 列表导航', link: '/components/list-nav/' },
            { text: 'List View 列表', link: '/components/list-view/' },
            { text: 'Modal 模态窗口', link: '/components/modal/' },
            { text: 'Progress 进度条', link: '/components/progress/' },
            { text: 'Tree Grid 树表格', link: '/components/tree-grid/' },
            { text: 'Tree View 树列表', link: '/components/tree-view/' },
            { text: 'Dial 仪表盘', link: '/components/dial/' },
            { text: 'Transfer 穿梭框', link: '/components/transfer/' },
            { text: 'Calculator 计算器', link: '/components/calculator/' },
            { text: 'Filter Bar 筛选条', link: '/components/filter-bar/' },
            { text: 'Video 视频播放器', link: '/components/video/' },
            { text: 'Page Header 页头', link: '/components/page-header/' },
            { text: 'Page Footer 页脚', link: '/components/page-footer/' }
        ]
    },
    {
        text: '反馈',
        items: [
            { text: 'Drawer 抽屉', link: '/components/drawer/' },
            { text: 'Message Box 消息弹窗', link: '/components/message-box/' },
            { text: 'Notify 通知消息', link: '/components/notify/' },
            { text: 'Popover 气泡提示', link: '/components/popover/' },
            { text: 'Tooltip 提示信息', link: '/components/tooltip/' },
            { text: 'VerifyDetail 验证信息', link: '/components/verify-detail/' },
            { text: 'Loading 加载', link: '/components/loading/' }
        ]
    },
    {
        text: '快捷指令',
        items: [
            { text: 'areaResponse 区域响应', link: '/components/area-response/' }
        ]
    },
    {
        text: '操作',
        items: [
            { text: 'Uploader 附件上传', link: '/components/uploader/' }
        ]
    }
];

export default sidebar;
