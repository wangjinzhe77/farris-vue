import { defineConfig } from 'vite';
import vueJsx from '@vitejs/plugin-vue-jsx';
import svgLoader from 'vite-svg-loader';
import { MarkdownTransform } from './.vitepress/plugins/markdown-transform';
import { resolve } from 'path';

export default defineConfig({
    plugins: [vueJsx({}), svgLoader(), MarkdownTransform()],
    server: {
        fs: {
            strict: false
        }
    },
    resolve: {
        alias: {
            '@': resolve(__dirname, '../'),
            '@components': resolve(__dirname, '../components'),
            '@farris/ui-vue': resolve(__dirname, '..')
        }
    }
});
