#!/usr/bin/env node
import { Command, Option } from 'commander';
import { buildLibs } from './commands/build.js';

const program = new Command();

program.command('build').description('构建 Farris UI Vue 组件库').action(buildLibs);

program.parse();
