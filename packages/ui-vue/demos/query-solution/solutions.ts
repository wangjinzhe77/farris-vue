/* eslint-disable max-len */

const fieldConfigs = [{
    "id": "f8ce2369-c420-4570-9337-f5bf742a4c8f",
    "code": "ID",
    "labelCode": "ID",
    "name": "主键",
    "controlType": "input-group",
    "editor": {
        "required": true,
        "type": "input-group"
    }
},
{
    "id": "b7ca02cc-e117-4bc0-aa63-b4178c8a234c",
    "code": "Version",
    "labelCode": "Version",
    "name": "版本",
    "controlType": "date-picker",
    "editor": {
        "required": true,
        "displayFormat": "yyyy-MM-dd",
        "type": "date-picker"
    }
},
{
    "id": "b59672e4-917c-4105-9886-4143af213733",
    "code": "inputgroup",
    "labelCode": "inputgroup",
    "name": "文本输入",
    "controlType": "input-group",
    "editor": {
        "required": true,
        "type": "input-group"
    }
},
{
    "id": "8bd7e862-197c-4e4c-abab-a305a2bc8c4e",
    "code": "datepicker",
    "labelCode": "datepicker",
    "name": "日期-日期格式",
    "controlType": "date-picker",
    "editor": {
        "required": true,
        "displayFormat": "yyyy-MM-dd",
        "type": "date-picker"
    }
},
{
    "id": "047ca48a-cba8-4fda-b971-d9c1ecd177e3",
    "code": "datepicker2",
    "labelCode": "datepicker2",
    "name": "日期-文本格式",
    "controlType": "input-group",
    "editor": {
        "required": true,
        "type": "input-group"
    }
},
{
    "id": "7a7c9c71-6c19-4d8d-b225-0846ab64cdc3",
    "code": "numberspinner",
    "labelCode": "numberspinner",
    "name": "数字",
    "controlType": "number-range",
    "editor": {
        "precision": 2,
        "isBigNumber": false,
        "required": true,
        "type": "number-range"
    }
},
{
    "id": "4d287311-b353-4977-8ed5-357edd496c68",
    "code": "numberrange",
    "labelCode": "numberrange",
    "name": "数字区间",
    "controlType": "number-range",
    "editor": {
        "precision": 2,
        "isBigNumber": false,
        "required": true,
        "type": "number-range"
    }
},
{
    "id": "96381c2a-32b7-4cf3-934c-64a76171343e",
    "code": "radiogroup",
    "labelCode": "radiogroup",
    "name": "单选",
    "controlType": "combo-list",
    "editor": {
        "required": true,
        "enumValueType": "string",
        "data": [
            {
                "disabled": false,
                "name": "啊啊啊",
                "value": "a"
            },
            {
                "disabled": false,
                "name": "不不不",
                "value": "b"
            },
            {
                "disabled": false,
                "name": "错错错",
                "value": "c"
            }
        ],
        "idField": "value",
        "textField": "name",
        "valueField": "value",
        "type": "combo-list"
    }
},
{
    "id": "9d2a5c8c-6766-4a74-933c-5f96cf273139",
    "code": "combolist",
    "labelCode": "combolist",
    "name": "下拉面板",
    "controlType": "combo-list",
    "editor": {
        "required": true,
        "enumValueType": "string",
        "data": [
            {
                "disabled": false,
                "name": "去去去",
                "value": "qqq"
            },
            {
                "disabled": false,
                "name": "汪汪汪",
                "value": "www"
            },
            {
                "disabled": false,
                "name": "饿饿饿",
                "value": "eee"
            }
        ],
        "idField": "value",
        "textField": "name",
        "valueField": "value",
        "type": "combo-list"
    }
},
{
    "id": "77575792-ed5c-453c-a925-06b8ab67f549",
    "code": "isboolean",
    "labelCode": "isboolean",
    "name": "布尔字段",
    "controlType": "combo-list",
    "editor": {
        "required": true,
        "enumValueType": "boolean",
        "data": [
            {
                "value": true,
                "name": "true"
            },
            {
                "value": false,
                "name": "false"
            }
        ],
        "idField": "value",
        "textField": "name",
        "valueField": "value",
        "type": "combo-list"
    }
},
{
    "id": "ea4d4779-2928-4d0b-bc39-79a32d63fead",
    "code": "lookup",
    "labelCode": "lookup",
    "name": "帮助",
    "controlType": "lookup",
    "editor": {
        "class": "",
        "required": false,
        "helpId": "915a0b20-975a-4df1-8cfd-888c3dda0009",
        "uri": "lookup.915a0b20_975a_4df1_8cfd_888c3dda0009",
        "textField": "code",
        "valueField": "name",
        "idField": "id",
        "mapFields": "",
        "multiSelect": true,
        "panelHeight": null,
        "displayName": "系统用户帮助",
        "displayType": "List",
        "dataSource": {
            "displayName": "系统用户帮助",
            "idField": "id",
            "uri": "lookup.915a0b20_975a_4df1_8cfd_888c3dda0009"
        },
        "type": "lookup"
    }
}]
const solutions = []


export {
    fieldConfigs,
    solutions
};
