/* eslint-disable no-use-before-define */
export interface InitialState {
    /** 列宽编辑器当前显示状态及所在分组*/
    defaultState: DefaultState;
    /** 列宽编辑器列宽及所在行数 */
    importData: ImportData[];
}

export interface DefaultState {
    /** 当前控件所在分组 */
    defaultGroupNumber: number;
    /** 当前模式：standard或customize */
    mode: string;
}
export interface ImportData {
    /** 小屏幕上占几列 */
    columnInSM: number;
    /** 中等屏幕上占几列 */
    columnInMD: number;
    /** 大屏幕上占几列 */
    columnInLG: number;
    /** 超大屏幕上占几列 */
    columnInEL: number;
    /** 小屏幕上占几列 */
    displayWidthInSM: number;
    /** 中等屏幕上占几列 */
    displayWidthInMD: number;
    /** 大屏幕上占几列 */
    displayWidthInLG: number;
    /** 超大屏幕上占几列 */
    displayWidthInEL: number;
    /** 控件名称 */
    label: string;
    /** 标注该控件在第几行 */
    componentRow: number;
    /** 最上方是否展示第X行 */
    showTopBorder: number;
    /** 表示当前分组 */
    group: number;
}
