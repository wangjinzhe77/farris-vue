import { LookupHttpResult, LookupHttpService, LookupRequestParams } from '../../../ui-vue/components/lookup/src/composition/types';

// 单数据列表帮助
function useSingleList() {
    return fetch('/farris-docs/assets/datas/lookup/lookup-griddata.json').then((e) => {
        return e.json();
    });
}

function useDoubleList() {
    return fetch('/farris-docs/assets/datas/lookup/lookup-double-list.json').then((e) => {
        return e.json();
    });
}

function useTreeList() {
    return fetch('/farris-docs/assets/datas/lookup/lookup-treedata.json').then((e) => {
        return e.json();
    })
}

export class LookupHttpRequest implements LookupHttpService  {
    getSettings(id: string): Promise<any> {
        throw new Error('Method not implemented.');
    }
    updateSettings(id: string, settings: any): Promise<any> {
        throw new Error('Method not implemented.');
    }
    displayType = 'list';

    getData(uri: string, params: Partial<LookupRequestParams>): Promise<LookupHttpResult> {
        // useTreeList();
        switch (this.displayType) {
            case 'tree':
                return useTreeList();
            case 'list-list':
                return useDoubleList();
            case 'list':
            default:
                return useSingleList();
        }
    }
}
