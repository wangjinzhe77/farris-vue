export const dataFields = [
    {
        "data": {
            "$type": "SimpleField",
            "refElementId": null,
            "defaultValue": "",
            "editor": {
                "$type": "TextBox"
            },
            "multiLanguage": false,
            "require": true,
            "readonly": false,
            "label": "id",
            "code": "ID",
            "originalId": "23c9c4c6-fbb6-4d17-af00-d62e954804af",
            "bindingPath": "id",
            "bindingField": "id",
            "name": "主键",
            "id": "23c9c4c6-fbb6-4d17-af00-d62e954804af",
            "type": {
                "$type": "StringType",
                "length": 36,
                "name": "String",
                "displayName": "字符串"
            },
            "path": "ID"
        },
        "children": [],
        "expanded": true,
        "selectable": true
    },
    {
        "data": {
            "$type": "SimpleField",
            "refElementId": null,
            "defaultValue": "",
            "editor": {
                "$type": "DateBox",
                "format": "'yyyy-MM-dd'"
            },
            "multiLanguage": false,
            "require": false,
            "readonly": false,
            "label": "version",
            "code": "Version",
            "originalId": "69c06cac-5e66-4045-af79-94d0bd4f2d04",
            "bindingPath": "version",
            "bindingField": "version",
            "name": "版本",
            "id": "69c06cac-5e66-4045-af79-94d0bd4f2d04",
            "type": {
                "$type": "DateTimeType",
                "name": "DateTime",
                "displayName": "日期时间"
            },
            "path": "Version"
        },
        "children": [],
        "expanded": true,
        "selectable": true
    },
    {
        "data": {
            "$type": "ComplexField",
            "label": "billStatus",
            "code": "BillStatus",
            "originalId": "774c1247-dd0e-4f07-9ef6-d52b132c722c",
            "bindingPath": "billStatus",
            "bindingField": "billStatus",
            "name": "状态",
            "id": "774c1247-dd0e-4f07-9ef6-d52b132c722c",
            "type": {
                "$type": "ObjectType",
                "name": "BillState774c",
                "fields": [
                    {
                        "$type": "SimpleField",
                        "refElementId": null,
                        "defaultValue": "",
                        "editor": {
                            "$type": "EnumField"
                        },
                        "multiLanguage": false,
                        "require": false,
                        "readonly": false,
                        "label": "billState",
                        "code": "BillState",
                        "originalId": "a0b19650-0101-468f-ae3f-40c76c0f06b0",
                        "bindingPath": "billStatus.billState",
                        "bindingField": "billStatus_BillState",
                        "name": "状态",
                        "id": "774c1247-0101-468f-ae3f-40c76c0f06b0",
                        "type": {
                            "$type": "EnumType",
                            "valueType": {
                                "$type": "StringType",
                                "length": 36,
                                "name": "String",
                                "displayName": "字符串"
                            },
                            "enumValues": [
                                {
                                    "name": "制单",
                                    "value": "Billing"
                                },
                                {
                                    "name": "提交审批",
                                    "value": "SubmitApproval"
                                },
                                {
                                    "name": "审批通过",
                                    "value": "Approved"
                                },
                                {
                                    "name": "审批不通过",
                                    "value": "ApprovalNotPassed"
                                }
                            ],
                            "name": "Enum",
                            "displayName": "枚举"
                        },
                        "path": "BillStatus.BillState"
                    }
                ],
                "displayName": "状态"
            },
            "path": "BillStatus"
        },
        "children": [
            {
                "data": {
                    "$type": "SimpleField",
                    "refElementId": null,
                    "defaultValue": "",
                    "editor": {
                        "$type": "EnumField"
                    },
                    "multiLanguage": false,
                    "require": false,
                    "readonly": false,
                    "label": "billState",
                    "code": "BillState",
                    "originalId": "a0b19650-0101-468f-ae3f-40c76c0f06b0",
                    "bindingPath": "billStatus.billState",
                    "bindingField": "billStatus_BillState",
                    "name": "状态",
                    "id": "774c1247-0101-468f-ae3f-40c76c0f06b0",
                    "type": {
                        "$type": "EnumType",
                        "valueType": {
                            "$type": "StringType",
                            "length": 36,
                            "name": "String",
                            "displayName": "字符串"
                        },
                        "enumValues": [
                            {
                                "name": "制单",
                                "value": "Billing"
                            },
                            {
                                "name": "提交审批",
                                "value": "SubmitApproval"
                            },
                            {
                                "name": "审批通过",
                                "value": "Approved"
                            },
                            {
                                "name": "审批不通过",
                                "value": "ApprovalNotPassed"
                            }
                        ],
                        "name": "Enum",
                        "displayName": "枚举"
                    },
                    "path": "BillStatus.BillState"
                },
                "children": [],
                "expanded": true,
                "selectable": true
            }
        ],
        "expanded": true,
        "selectable": false
    },
    {
        "data": {
            "$type": "ComplexField",
            "label": "processInstance",
            "code": "ProcessInstance",
            "originalId": "066f0dad-ba6f-499f-aec3-1ce6d77e1320",
            "bindingPath": "processInstance",
            "bindingField": "processInstance",
            "name": "流程实例",
            "id": "066f0dad-ba6f-499f-aec3-1ce6d77e1320",
            "type": {
                "$type": "ObjectType",
                "name": "ProcessInstance066f",
                "fields": [
                    {
                        "$type": "SimpleField",
                        "refElementId": null,
                        "defaultValue": "",
                        "editor": {
                            "$type": "TextBox"
                        },
                        "multiLanguage": false,
                        "require": false,
                        "readonly": false,
                        "label": "processInstance",
                        "code": "ProcessInstance",
                        "originalId": "2e1beb7d-ad8f-4da3-a430-c8a7f2162135",
                        "bindingPath": "processInstance.processInstance",
                        "bindingField": "processInstance_ProcessInstance",
                        "name": "流程实例",
                        "id": "066f0dad-ad8f-4da3-a430-c8a7f2162135",
                        "type": {
                            "$type": "StringType",
                            "length": 36,
                            "name": "String",
                            "displayName": "字符串"
                        },
                        "path": "ProcessInstance.ProcessInstance"
                    }
                ],
                "displayName": "流程实例"
            },
            "path": "ProcessInstance"
        },
        "children": [
            {
                "data": {
                    "$type": "SimpleField",
                    "refElementId": null,
                    "defaultValue": "",
                    "editor": {
                        "$type": "TextBox"
                    },
                    "multiLanguage": false,
                    "require": false,
                    "readonly": false,
                    "label": "processInstance",
                    "code": "ProcessInstance",
                    "originalId": "2e1beb7d-ad8f-4da3-a430-c8a7f2162135",
                    "bindingPath": "processInstance.processInstance",
                    "bindingField": "processInstance_ProcessInstance",
                    "name": "流程实例",
                    "id": "066f0dad-ad8f-4da3-a430-c8a7f2162135",
                    "type": {
                        "$type": "StringType",
                        "length": 36,
                        "name": "String",
                        "displayName": "字符串"
                    },
                    "path": "ProcessInstance.ProcessInstance"
                },
                "children": [],
                "expanded": true,
                "selectable": true
            }
        ],
        "expanded": true,
        "selectable": false
    },
    {
        "data": {
            "$type": "ComplexField",
            "label": "employeeID",
            "code": "EmployeeID",
            "originalId": "b309e877-4e52-489c-9239-0cdb069bd760",
            "bindingPath": "employeeID",
            "bindingField": "employeeID",
            "name": "报销人员",
            "id": "b309e877-4e52-489c-9239-0cdb069bd760",
            "type": {
                "$type": "EntityType",
                "primary": "employeeID",
                "entities": [],
                "fields": [
                    {
                        "$type": "SimpleField",
                        "refElementId": null,
                        "defaultValue": "",
                        "editor": {
                            "$type": "TextBox"
                        },
                        "multiLanguage": false,
                        "require": false,
                        "readonly": false,
                        "label": "employeeID",
                        "code": "EmployeeID",
                        "originalId": "a60b34bd-4383-4710-bda4-d38222c15c30",
                        "bindingPath": "employeeID.employeeID",
                        "bindingField": "employeeID",
                        "name": "报销人员",
                        "id": "a60b34bd-4383-4710-bda4-d38222c15c30",
                        "type": {
                            "$type": "StringType",
                            "length": 36,
                            "name": "String",
                            "displayName": "字符串"
                        },
                        "path": "EmployeeID.EmployeeID"
                    },
                    {
                        "$type": "SimpleField",
                        "refElementId": "82e52824-f8e1-46a1-841f-0f65582eafac",
                        "defaultValue": "",
                        "editor": {
                            "$type": "TextBox"
                        },
                        "multiLanguage": false,
                        "require": false,
                        "readonly": false,
                        "label": "employeeID_ID",
                        "code": "ID",
                        "originalId": "c389cdd7-d9ca-48c7-81e3-c0abf6eeaa4e",
                        "bindingPath": "employeeID.employeeID_ID",
                        "bindingField": "employeeID_EmployeeID_ID",
                        "name": "ID",
                        "id": "c389cdd7-d9ca-48c7-81e3-c0abf6eeaa4e",
                        "type": {
                            "$type": "StringType",
                            "length": 36,
                            "name": "String",
                            "displayName": "字符串"
                        },
                        "path": "EmployeeID.EmployeeID_ID"
                    },
                    {
                        "$type": "SimpleField",
                        "refElementId": "9d25e0d7-0fb7-435f-a2de-438d371b97e4",
                        "defaultValue": "",
                        "editor": {
                            "$type": "TextBox"
                        },
                        "multiLanguage": false,
                        "require": false,
                        "readonly": false,
                        "label": "employeeID_Code",
                        "code": "Code",
                        "originalId": "a4380f77-64d1-4c9b-8d85-2e71242f7584",
                        "bindingPath": "employeeID.employeeID_Code",
                        "bindingField": "employeeID_EmployeeID_Code",
                        "name": "编号",
                        "id": "a4380f77-64d1-4c9b-8d85-2e71242f7584",
                        "type": {
                            "$type": "StringType",
                            "length": 36,
                            "name": "String",
                            "displayName": "字符串"
                        },
                        "path": "EmployeeID.EmployeeID_Code"
                    },
                    {
                        "$type": "SimpleField",
                        "refElementId": "94fd1572-613c-4973-bf79-12e33c16a9ec",
                        "defaultValue": "",
                        "editor": {
                            "$type": "TextBox"
                        },
                        "multiLanguage": false,
                        "require": false,
                        "readonly": false,
                        "label": "employeeID_Name",
                        "code": "Name",
                        "originalId": "58bc1512-c731-4109-bd90-a6c0a24bc12f",
                        "bindingPath": "employeeID.employeeID_Name",
                        "bindingField": "employeeID_EmployeeID_Name",
                        "name": "名称",
                        "id": "58bc1512-c731-4109-bd90-a6c0a24bc12f",
                        "type": {
                            "$type": "StringType",
                            "length": 36,
                            "name": "String",
                            "displayName": "字符串"
                        },
                        "path": "EmployeeID.EmployeeID_Name"
                    }
                ],
                "name": "GspUserA60b",
                "displayName": "用户"
            },
            "path": "EmployeeID"
        },
        "children": [
            {
                "data": {
                    "$type": "SimpleField",
                    "refElementId": null,
                    "defaultValue": "",
                    "editor": {
                        "$type": "TextBox"
                    },
                    "multiLanguage": false,
                    "require": false,
                    "readonly": false,
                    "label": "employeeID",
                    "code": "EmployeeID",
                    "originalId": "a60b34bd-4383-4710-bda4-d38222c15c30",
                    "bindingPath": "employeeID.employeeID",
                    "bindingField": "employeeID",
                    "name": "报销人员",
                    "id": "a60b34bd-4383-4710-bda4-d38222c15c30",
                    "type": {
                        "$type": "StringType",
                        "length": 36,
                        "name": "String",
                        "displayName": "字符串"
                    },
                    "path": "EmployeeID.EmployeeID"
                },
                "children": [],
                "expanded": true,
                "selectable": true
            },
            {
                "data": {
                    "$type": "SimpleField",
                    "refElementId": "82e52824-f8e1-46a1-841f-0f65582eafac",
                    "defaultValue": "",
                    "editor": {
                        "$type": "TextBox"
                    },
                    "multiLanguage": false,
                    "require": false,
                    "readonly": false,
                    "label": "employeeID_ID",
                    "code": "ID",
                    "originalId": "c389cdd7-d9ca-48c7-81e3-c0abf6eeaa4e",
                    "bindingPath": "employeeID.employeeID_ID",
                    "bindingField": "employeeID_EmployeeID_ID",
                    "name": "ID",
                    "id": "c389cdd7-d9ca-48c7-81e3-c0abf6eeaa4e",
                    "type": {
                        "$type": "StringType",
                        "length": 36,
                        "name": "String",
                        "displayName": "字符串"
                    },
                    "path": "EmployeeID.EmployeeID_ID"
                },
                "children": [],
                "expanded": true,
                "selectable": true
            },
            {
                "data": {
                    "$type": "SimpleField",
                    "refElementId": "9d25e0d7-0fb7-435f-a2de-438d371b97e4",
                    "defaultValue": "",
                    "editor": {
                        "$type": "TextBox"
                    },
                    "multiLanguage": false,
                    "require": false,
                    "readonly": false,
                    "label": "employeeID_Code",
                    "code": "Code",
                    "originalId": "a4380f77-64d1-4c9b-8d85-2e71242f7584",
                    "bindingPath": "employeeID.employeeID_Code",
                    "bindingField": "employeeID_EmployeeID_Code",
                    "name": "编号",
                    "id": "a4380f77-64d1-4c9b-8d85-2e71242f7584",
                    "type": {
                        "$type": "StringType",
                        "length": 36,
                        "name": "String",
                        "displayName": "字符串"
                    },
                    "path": "EmployeeID.EmployeeID_Code"
                },
                "children": [],
                "expanded": true,
                "selectable": true
            },
            {
                "data": {
                    "$type": "SimpleField",
                    "refElementId": "94fd1572-613c-4973-bf79-12e33c16a9ec",
                    "defaultValue": "",
                    "editor": {
                        "$type": "TextBox"
                    },
                    "multiLanguage": false,
                    "require": false,
                    "readonly": false,
                    "label": "employeeID_Name",
                    "code": "Name",
                    "originalId": "58bc1512-c731-4109-bd90-a6c0a24bc12f",
                    "bindingPath": "employeeID.employeeID_Name",
                    "bindingField": "employeeID_EmployeeID_Name",
                    "name": "名称",
                    "id": "58bc1512-c731-4109-bd90-a6c0a24bc12f",
                    "type": {
                        "$type": "StringType",
                        "length": 36,
                        "name": "String",
                        "displayName": "字符串"
                    },
                    "path": "EmployeeID.EmployeeID_Name"
                },
                "children": [],
                "expanded": true,
                "selectable": true
            }
        ],
        "expanded": true,
        "selectable": false
    },
    {
        "data": {
            "$type": "ComplexField",
            "label": "domainID",
            "code": "DomainID",
            "originalId": "862ac245-7a22-4918-9c5e-ae74eee651ae",
            "bindingPath": "domainID",
            "bindingField": "domainID",
            "name": "所属部门",
            "id": "862ac245-7a22-4918-9c5e-ae74eee651ae",
            "type": {
                "$type": "EntityType",
                "primary": "domainID",
                "entities": [],
                "fields": [
                    {
                        "$type": "SimpleField",
                        "refElementId": null,
                        "defaultValue": "",
                        "editor": {
                            "$type": "TextBox"
                        },
                        "multiLanguage": false,
                        "require": false,
                        "readonly": false,
                        "label": "domainID",
                        "code": "DomainID",
                        "originalId": "970bcb08-b16e-49f6-9763-29e95c0636ae",
                        "bindingPath": "domainID.domainID",
                        "bindingField": "domainID",
                        "name": "所属部门",
                        "id": "970bcb08-b16e-49f6-9763-29e95c0636ae",
                        "type": {
                            "$type": "StringType",
                            "length": 36,
                            "name": "String",
                            "displayName": "字符串"
                        },
                        "path": "DomainID.DomainID"
                    },
                    {
                        "$type": "SimpleField",
                        "refElementId": "0d45aea4-1327-403f-bda2-c9b5a933587b",
                        "defaultValue": "",
                        "editor": {
                            "$type": "TextBox"
                        },
                        "multiLanguage": false,
                        "require": false,
                        "readonly": false,
                        "label": "domainID_ID",
                        "code": "ID",
                        "originalId": "c3ae54d9-5351-4b6f-9f6d-f8dbb9664c79",
                        "bindingPath": "domainID.domainID_ID",
                        "bindingField": "domainID_DomainID_ID",
                        "name": "ID",
                        "id": "c3ae54d9-5351-4b6f-9f6d-f8dbb9664c79",
                        "type": {
                            "$type": "StringType",
                            "length": 36,
                            "name": "String",
                            "displayName": "字符串"
                        },
                        "path": "DomainID.DomainID_ID"
                    },
                    {
                        "$type": "SimpleField",
                        "refElementId": "9834de85-fbd9-4f84-b31d-f97bb05b43e5",
                        "defaultValue": "",
                        "editor": {
                            "$type": "TextBox"
                        },
                        "multiLanguage": false,
                        "require": false,
                        "readonly": false,
                        "label": "domainID_code",
                        "code": "code",
                        "originalId": "391bde2f-a350-4ec8-8206-0fe4f15e4bfc",
                        "bindingPath": "domainID.domainID_code",
                        "bindingField": "domainID_DomainID_code",
                        "name": "编号",
                        "id": "391bde2f-a350-4ec8-8206-0fe4f15e4bfc",
                        "type": {
                            "$type": "StringType",
                            "length": 100,
                            "name": "String",
                            "displayName": "字符串"
                        },
                        "path": "DomainID.DomainID_code"
                    },
                    {
                        "$type": "SimpleField",
                        "refElementId": "a824ff6d-b08f-4b5c-b707-997d56760fb4",
                        "defaultValue": "",
                        "editor": {
                            "$type": "TextBox"
                        },
                        "multiLanguage": false,
                        "require": false,
                        "readonly": false,
                        "label": "domainID_name",
                        "code": "name",
                        "originalId": "06454a56-89b3-42c0-be4a-a8f273c403e2",
                        "bindingPath": "domainID.domainID_name",
                        "bindingField": "domainID_DomainID_name",
                        "name": "名称",
                        "id": "06454a56-89b3-42c0-be4a-a8f273c403e2",
                        "type": {
                            "$type": "StringType",
                            "length": 100,
                            "name": "String",
                            "displayName": "字符串"
                        },
                        "path": "DomainID.DomainID_name"
                    }
                ],
                "name": "SysOrg970b",
                "displayName": "系统组织"
            },
            "path": "DomainID"
        },
        "children": [
            {
                "data": {
                    "$type": "SimpleField",
                    "refElementId": null,
                    "defaultValue": "",
                    "editor": {
                        "$type": "TextBox"
                    },
                    "multiLanguage": false,
                    "require": false,
                    "readonly": false,
                    "label": "domainID",
                    "code": "DomainID",
                    "originalId": "970bcb08-b16e-49f6-9763-29e95c0636ae",
                    "bindingPath": "domainID.domainID",
                    "bindingField": "domainID",
                    "name": "所属部门",
                    "id": "970bcb08-b16e-49f6-9763-29e95c0636ae",
                    "type": {
                        "$type": "StringType",
                        "length": 36,
                        "name": "String",
                        "displayName": "字符串"
                    },
                    "path": "DomainID.DomainID",
                    "groupId": "",
                    "groupName": "",
                    "valueChanging": "",
                    "valueChanged": "",
                    "updateOn": "blur"
                },
                "children": [],
                "expanded": true,
                "selectable": true
            },
            {
                "data": {
                    "$type": "SimpleField",
                    "refElementId": "0d45aea4-1327-403f-bda2-c9b5a933587b",
                    "defaultValue": "",
                    "editor": {
                        "$type": "TextBox"
                    },
                    "multiLanguage": false,
                    "require": false,
                    "readonly": false,
                    "label": "domainID_ID",
                    "code": "ID",
                    "originalId": "c3ae54d9-5351-4b6f-9f6d-f8dbb9664c79",
                    "bindingPath": "domainID.domainID_ID",
                    "bindingField": "domainID_DomainID_ID",
                    "name": "ID",
                    "id": "c3ae54d9-5351-4b6f-9f6d-f8dbb9664c79",
                    "type": {
                        "$type": "StringType",
                        "length": 36,
                        "name": "String",
                        "displayName": "字符串"
                    },
                    "path": "DomainID.DomainID_ID"
                },
                "children": [],
                "expanded": true,
                "selectable": true
            },
            {
                "data": {
                    "$type": "SimpleField",
                    "refElementId": "9834de85-fbd9-4f84-b31d-f97bb05b43e5",
                    "defaultValue": "",
                    "editor": {
                        "$type": "TextBox"
                    },
                    "multiLanguage": false,
                    "require": false,
                    "readonly": false,
                    "label": "domainID_code",
                    "code": "code",
                    "originalId": "391bde2f-a350-4ec8-8206-0fe4f15e4bfc",
                    "bindingPath": "domainID.domainID_code",
                    "bindingField": "domainID_DomainID_code",
                    "name": "编号",
                    "id": "391bde2f-a350-4ec8-8206-0fe4f15e4bfc",
                    "type": {
                        "$type": "StringType",
                        "length": 100,
                        "name": "String",
                        "displayName": "字符串"
                    },
                    "path": "DomainID.DomainID_code"
                },
                "children": [],
                "expanded": true,
                "selectable": true
            },
            {
                "data": {
                    "$type": "SimpleField",
                    "refElementId": "a824ff6d-b08f-4b5c-b707-997d56760fb4",
                    "defaultValue": "",
                    "editor": {
                        "$type": "TextBox"
                    },
                    "multiLanguage": false,
                    "require": false,
                    "readonly": false,
                    "label": "domainID_name",
                    "code": "name",
                    "originalId": "06454a56-89b3-42c0-be4a-a8f273c403e2",
                    "bindingPath": "domainID.domainID_name",
                    "bindingField": "domainID_DomainID_name",
                    "name": "名称",
                    "id": "06454a56-89b3-42c0-be4a-a8f273c403e2",
                    "type": {
                        "$type": "StringType",
                        "length": 100,
                        "name": "String",
                        "displayName": "字符串"
                    },
                    "path": "DomainID.DomainID_name"
                },
                "children": [],
                "expanded": true,
                "selectable": true
            }
        ],
        "expanded": true,
        "selectable": false
    },
    {
        "data": {
            "$type": "SimpleField",
            "refElementId": null,
            "defaultValue": "",
            "editor": {
                "$type": "TextBox"
            },
            "multiLanguage": false,
            "require": false,
            "readonly": false,
            "label": "billCode",
            "code": "BillCode",
            "originalId": "1e216bad-87fe-4bc4-ad67-1e91e71f2d5c",
            "bindingPath": "billCode",
            "bindingField": "billCode",
            "name": "单据编号",
            "id": "1e216bad-87fe-4bc4-ad67-1e91e71f2d5c",
            "type": {
                "$type": "StringType",
                "length": 36,
                "name": "String",
                "displayName": "字符串"
            },
            "path": "BillCode",
            "groupId": null,
            "groupName": null,
            "updateOn": "blur"
        },
        "children": [],
        "expanded": true,
        "selectable": true
    },
    {
        "data": {
            "$type": "SimpleField",
            "refElementId": null,
            "defaultValue": "",
            "editor": {
                "$type": "NumericBox"
            },
            "multiLanguage": false,
            "require": false,
            "readonly": false,
            "label": "totalSum",
            "code": "TotalSum",
            "originalId": "65d73d35-afb9-479d-b033-0ec370b73b31",
            "bindingPath": "totalSum",
            "bindingField": "totalSum",
            "name": "报帐金额",
            "id": "65d73d35-afb9-479d-b033-0ec370b73b31",
            "type": {
                "$type": "NumericType",
                "precision": 2,
                "length": 18,
                "name": "Number",
                "displayName": "数字"
            },
            "path": "TotalSum",
            "groupId": null,
            "groupName": null,
            "updateOn": "blur"
        },
        "children": [],
        "expanded": true,
        "selectable": true
    },
    {
        "data": {
            "$type": "SimpleField",
            "refElementId": null,
            "defaultValue": "",
            "editor": {
                "$type": "EnumField"
            },
            "multiLanguage": false,
            "require": false,
            "readonly": false,
            "label": "billType",
            "code": "BillType",
            "originalId": "5b1d0fee-d7d4-4867-b862-a95a88aa5e5b",
            "bindingPath": "billType",
            "bindingField": "billType",
            "name": "报销类型",
            "id": "5b1d0fee-d7d4-4867-b862-a95a88aa5e5b",
            "type": {
                "$type": "EnumType",
                "valueType": {
                    "$type": "StringType",
                    "length": 36,
                    "name": "String",
                    "displayName": "字符串"
                },
                "enumValues": [
                    {
                        "name": "交通费",
                        "value": "JT"
                    },
                    {
                        "name": "差旅费",
                        "value": "CL"
                    },
                    {
                        "name": "手机费",
                        "value": "SJ"
                    }
                ],
                "name": "Enum",
                "displayName": "枚举"
            },
            "path": "BillType",
            "groupId": null,
            "groupName": null,
            "updateOn": "change"
        },
        "children": [],
        "expanded": true,
        "selectable": true
    },
    {
        "data": {
            "$type": "SimpleField",
            "refElementId": null,
            "defaultValue": "",
            "editor": {
                "$type": "DateBox",
                "format": "yyyy-MM-dd"
            },
            "multiLanguage": false,
            "require": false,
            "readonly": false,
            "label": "billDate",
            "code": "BillDate",
            "originalId": "8927b349-44cc-4851-b7b8-be10e07e4632",
            "bindingPath": "billDate",
            "bindingField": "billDate",
            "name": "制单日期",
            "id": "8927b349-44cc-4851-b7b8-be10e07e4632",
            "type": {
                "$type": "DateTimeType",
                "name": "DateTime",
                "displayName": "日期时间"
            },
            "path": "BillDate",
            "groupId": null,
            "groupName": null,
            "updateOn": "blur"
        },
        "children": [],
        "expanded": true,
        "selectable": true
    },
    {
        "data": {
            "$type": "SimpleField",
            "refElementId": null,
            "defaultValue": "",
            "editor": {
                "$type": "TextBox"
            },
            "multiLanguage": false,
            "require": false,
            "readonly": false,
            "label": "secID",
            "code": "SecID",
            "originalId": "bf5f94f2-b691-49d1-9ad9-4139b2ee7e54",
            "bindingPath": "secID",
            "bindingField": "secID",
            "name": "密级ID",
            "id": "bf5f94f2-b691-49d1-9ad9-4139b2ee7e54",
            "type": {
                "$type": "StringType",
                "length": 36,
                "name": "String",
                "displayName": "字符串"
            },
            "path": "SecID"
        },
        "children": [],
        "expanded": true,
        "selectable": true
    },
    {
        "data": {
            "$type": "SimpleField",
            "refElementId": null,
            "defaultValue": "",
            "editor": {
                "$type": "NumericBox"
            },
            "multiLanguage": false,
            "require": false,
            "readonly": false,
            "label": "secLevel",
            "code": "SecLevel",
            "originalId": "c4e51a6d-47a5-4fdb-a372-01aa94f16491",
            "bindingPath": "secLevel",
            "bindingField": "secLevel",
            "name": "密级",
            "id": "c4e51a6d-47a5-4fdb-a372-01aa94f16491",
            "type": {
                "$type": "NumericType",
                "precision": 0,
                "length": 0,
                "name": "Number",
                "displayName": "数字"
            },
            "path": "SecLevel"
        },
        "children": [],
        "expanded": true,
        "selectable": true
    },
    {
        "data": {
            "$type": "SimpleField",
            "refElementId": null,
            "defaultValue": "",
            "editor": {
                "$type": "TimePicker"
            },
            "multiLanguage": false,
            "require": false,
            "readonly": false,
            "label": "projectID",
            "code": "ProjectID",
            "originalId": "f79e78c3-1a82-49ce-9dab-8744b7c0dd26",
            "bindingPath": "projectID",
            "bindingField": "projectID",
            "name": "所属项目",
            "id": "f79e78c3-1a82-49ce-9dab-8744b7c0dd26",
            "type": {
                "$type": "StringType",
                "length": 36,
                "name": "String",
                "displayName": "字符串"
            },
            "path": "ProjectID",
            "groupId": "",
            "groupName": "",
            "valueChanging": "",
            "valueChanged": "",
            "updateOn": "blur"
        },
        "children": [],
        "expanded": true,
        "selectable": true
    },
    {
        "data": {
            "$type": "SimpleField",
            "refElementId": null,
            "defaultValue": "",
            "editor": {
                "$type": "MultiTextBox"
            },
            "multiLanguage": false,
            "require": false,
            "readonly": false,
            "label": "billNote",
            "code": "BillNote",
            "originalId": "a2f15300-0cb6-40aa-b59b-a37d2b1e9516",
            "bindingPath": "billNote",
            "bindingField": "billNote",
            "name": "报销说明",
            "id": "a2f15300-0cb6-40aa-b59b-a37d2b1e9516",
            "type": {
                "$type": "TextType",
                "length": 2000,
                "name": "Text",
                "displayName": "文本"
            },
            "path": "BillNote",
            "groupId": null,
            "groupName": null,
            "updateOn": "blur"
        },
        "children": [],
        "expanded": true,
        "selectable": true
    },
    {
        "data": {
            "$type": "ComplexField",
            "label": "projectMrg",
            "code": "ProjectMrg",
            "originalId": "f4bb0ba7-0aca-4eca-a3e7-066d851312b5",
            "bindingPath": "projectMrg",
            "bindingField": "projectMrg",
            "name": "项目经理",
            "id": "f4bb0ba7-0aca-4eca-a3e7-066d851312b5",
            "type": {
                "$type": "EntityType",
                "primary": "projectMrg",
                "entities": [],
                "fields": [
                    {
                        "$type": "SimpleField",
                        "refElementId": null,
                        "defaultValue": "",
                        "editor": {
                            "$type": "TextBox"
                        },
                        "multiLanguage": false,
                        "require": false,
                        "readonly": false,
                        "label": "projectMrg",
                        "code": "ProjectMrg",
                        "originalId": "4f498a88-e4f6-4eb2-a68a-670ccb1b17ee",
                        "bindingPath": "projectMrg.projectMrg",
                        "bindingField": "projectMrg",
                        "name": "项目经理",
                        "id": "4f498a88-e4f6-4eb2-a68a-670ccb1b17ee",
                        "type": {
                            "$type": "StringType",
                            "length": 36,
                            "name": "String",
                            "displayName": "字符串"
                        },
                        "path": "ProjectMrg.ProjectMrg"
                    },
                    {
                        "$type": "SimpleField",
                        "refElementId": "94fd1572-613c-4973-bf79-12e33c16a9ec",
                        "defaultValue": "",
                        "editor": {
                            "$type": "TextBox"
                        },
                        "multiLanguage": false,
                        "require": false,
                        "readonly": false,
                        "label": "projectMrg_Name",
                        "code": "Name",
                        "originalId": "91773518-6792-4222-8b28-1e90794ff802",
                        "bindingPath": "projectMrg.projectMrg_Name",
                        "bindingField": "projectMrg_ProjectMrg_Name",
                        "name": "名称",
                        "id": "91773518-6792-4222-8b28-1e90794ff802",
                        "type": {
                            "$type": "StringType",
                            "length": 36,
                            "name": "String",
                            "displayName": "字符串"
                        },
                        "path": "ProjectMrg.ProjectMrg_Name"
                    }
                ],
                "name": "GspUser4f49",
                "displayName": "用户"
            },
            "path": "ProjectMrg"
        },
        "children": [
            {
                "data": {
                    "$type": "SimpleField",
                    "refElementId": null,
                    "defaultValue": "",
                    "editor": {
                        "$type": "LookupEdit",
                        "dataSource": {
                            "uri": "ROBXDJ.projectMrg",
                            "displayName": "系统组织帮助",
                            "idField": "id",
                            "type": "ViewObject"
                        },
                        "textField": "name",
                        "valueField": "id",
                        "displayType": "TreeList",
                        "helpId": "b524a702-7323-4d46-998e-5ba0c6abcd49",
                        "mapFields": ""
                    },
                    "multiLanguage": false,
                    "require": false,
                    "readonly": false,
                    "label": "projectMrg",
                    "code": "ProjectMrg",
                    "originalId": "4f498a88-e4f6-4eb2-a68a-670ccb1b17ee",
                    "bindingPath": "projectMrg.projectMrg",
                    "bindingField": "projectMrg",
                    "name": "项目经理",
                    "id": "4f498a88-e4f6-4eb2-a68a-670ccb1b17ee",
                    "type": {
                        "$type": "StringType",
                        "length": 36,
                        "name": "String",
                        "displayName": "字符串"
                    },
                    "path": "ProjectMrg.ProjectMrg",
                    "groupId": "",
                    "groupName": "",
                    "valueChanging": "",
                    "valueChanged": "",
                    "updateOn": "blur"
                },
                "children": [],
                "expanded": true,
                "selectable": true
            },
            {
                "data": {
                    "$type": "SimpleField",
                    "refElementId": "94fd1572-613c-4973-bf79-12e33c16a9ec",
                    "defaultValue": "",
                    "editor": {
                        "$type": "TextBox"
                    },
                    "multiLanguage": false,
                    "require": false,
                    "readonly": false,
                    "label": "projectMrg_Name",
                    "code": "Name",
                    "originalId": "91773518-6792-4222-8b28-1e90794ff802",
                    "bindingPath": "projectMrg.projectMrg_Name",
                    "bindingField": "projectMrg_ProjectMrg_Name",
                    "name": "名称",
                    "id": "91773518-6792-4222-8b28-1e90794ff802",
                    "type": {
                        "$type": "StringType",
                        "length": 36,
                        "name": "String",
                        "displayName": "字符串"
                    },
                    "path": "ProjectMrg.ProjectMrg_Name"
                },
                "children": [],
                "expanded": true,
                "selectable": true
            }
        ],
        "expanded": true,
        "selectable": false
    },
    {
        "data": {
            "$type": "SimpleField",
            "refElementId": null,
            "defaultValue": "",
            "editor": {
                "$type": "EnumField"
            },
            "multiLanguage": false,
            "require": false,
            "readonly": false,
            "label": "auditStatus",
            "code": "AuditStatus",
            "originalId": "b1e5816e-e37e-4d66-983f-6047e70535a5",
            "bindingPath": "auditStatus",
            "bindingField": "auditStatus",
            "name": "稽核状态",
            "id": "b1e5816e-e37e-4d66-983f-6047e70535a5",
            "type": {
                "$type": "EnumType",
                "valueType": {
                    "$type": "StringType",
                    "length": 36,
                    "name": "String",
                    "displayName": "字符串"
                },
                "enumValues": [
                    {
                        "name": "未稽核",
                        "value": "None"
                    },
                    {
                        "name": "稽核通过",
                        "value": "Passed"
                    },
                    {
                        "name": "稽核未通过",
                        "value": "Reject"
                    }
                ],
                "name": "Enum",
                "displayName": "枚举"
            },
            "path": "AuditStatus",
            "groupId": "",
            "groupName": "",
            "valueChanging": "",
            "valueChanged": "",
            "updateOn": "change"
        },
        "children": [],
        "expanded": true,
        "selectable": true
    },
    {
        "data": {
            "$type": "SimpleField",
            "defaultValue": "",
            "readonly": false,
            "editor": {
                "$type": "DateBox",
                "format": "'yyyy-MM-dd'"
            },
            "require": false,
            "multiLanguage": false,
            "id": "ef5e768d-f43d-4fe9-b0fe-33bf733b5990",
            "path": "shortdate",
            "type": {
                "$type": "DateType",
                "displayName": "日期",
                "name": "Date"
            },
            "bindingField": "shortdate",
            "bindingPath": "shortdate",
            "label": "shortdate",
            "code": "shortdate",
            "originalId": "ef5e768d-f43d-4fe9-b0fe-33bf733b5990",
            "name": "短日期",
            "groupId": "",
            "groupName": "",
            "valueChanging": "",
            "valueChanged": "",
            "updateOn": "blur"
        },
        "children": [],
        "expanded": true,
        "selectable": true
    }
]