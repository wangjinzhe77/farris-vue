export default [
    {
        id: 'SecurityLevel',
        nameSpace: 'Inspur.iGIX.common',
        fileName: 'SecurityLevel.udt',
        type: 'UDT',
        content: null,
        name: '密级',
        code: 'SecurityLevel',
        hide: false,
        active: false,
        svg: 'SecurityLevel',
        category: '密级'
    },
    {
        id: 'Amount',
        nameSpace: 'Inspur.iGIX.common',
        fileName: 'Amount.udt',
        type: 'UDT',
        content: null,
        name: '金额',
        code: 'Amount',
        hide: false,
        active: false,
        svg: 'Amount',
        category: '常用'
    },
    {
        id: 'IdentificationCard',
        nameSpace: 'Inspur.iGIX.common',
        fileName: 'IdentificationCard.udt',
        type: 'UDT',
        content: null,
        name: '身份证',
        code: 'IdentificationCard',
        hide: false,
        active: false,
        svg: 'IdentificationCard',
        category: '常用'
    },
    {
        id: 'Sequence',
        nameSpace: 'Inspur.iGIX.common',
        fileName: 'Sequence.udt',
        type: 'UDT',
        content: null,
        name: '顺序',
        code: 'Sequence',
        hide: false,
        active: false,
        svg: 'Sequence',
        category: '常用'
    }
];
