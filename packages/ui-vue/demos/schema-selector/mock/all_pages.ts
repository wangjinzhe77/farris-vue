export default [
    {
        id: '1e9c35f0-88a2-4735-8f15-0e614b40f3f2',
        nameSpace: 'Inspur.GS.PM.IPM.procard.procard.Front',
        code: 'SubProjectList',
        name: '子项目列表',
        fileName: 'SubProjectList.frm',
        type: 'Form',
        content: null,
        category: '最近使用'
    },
    {
        id: '581d2fde-d554-4508-bf0c-04ca74349857',
        nameSpace: 'Inspur.GS.PM.IPM.procard.procard.Front',
        code: 'SidebarList',
        name: '侧边栏导航列表',
        fileName: 'TestCE.frm',
        type: 'Form',
        content: null,
        category: '最近使用'
    },
    {
        id: 'f4d6379e-6c75-48a4-95ed-beee23901f70',
        nameSpace: 'Inspur.GS.PM.IPM.procard.procard.Front',
        code: 'AddProcardForm',
        name: '新增项目信息',
        fileName: 'AddProcardForm.frm',
        type: 'Form',
        content: null,
        category: '本地元数据'
    },
    {
        id: '3feeb0a2-556f-47f4-a78b-92fb6298ff67',
        nameSpace: 'Inspur.GS.PM.IPM.procard.procard.Front',
        code: 'ChildList',
        name: '子项目列表',
        fileName: 'ChildList.frm',
        type: 'Form',
        content: null,
        category: '本地元数据'
    },
    {
        id: '54a1edb5-9ef9-47f0-be3f-565456000f7f',
        nameSpace: 'Inspur.GS.PM.IPM.procard.procard.Front',
        code: 'ProcardForm',
        name: '项目详细信息',
        fileName: 'ProcardForm.frm',
        type: 'Form',
        content: null,
        category: '本地元数据'
    },
    {
        id: '59974210-106a-49ab-9983-e96a96f49f7b',
        nameSpace: 'Inspur.GS.PM.IPM.proinfo.proinfo.Front',
        code: 'promonitor',
        name: '项目监控',
        fileName: 'promonitor.frm',
        type: 'Form',
        content: null,
        category: '本地元数据'
    },
    {
        id: '930f6084-020b-42d5-b68d-5ab605ba5836',
        nameSpace: 'Inspur.GS.PM.IPM.proinfo.proinfo.Front',
        code: 'proinfoForm',
        name: '项目信息管理',
        fileName: 'proinfoForm.frm',
        type: 'Form',
        content: null,
        category: '本地元数据'
    }
];
