/* eslint-disable max-len */
export default [
    {
        code: "all",
        name: "全部",
        active: true,
        contains: []
    }, {
        code: "dict",
        name: "字典",
        active: false,
        contains: ['ListCardController', 'AdvancedListCardController', 'AdvancedListCardWithSidebarController', 'TreeCardController']
    },
    {
        code: "list",
        name: "列表",
        active: false,
        contains: ['ListListController', 'ListController', 'TreeListController', 'EditableTreeController', 'EditableListController', 'DatagridController']
    },
    {
        code: "document",
        name: "单据",
        active: false,
        contains: ['CardController']
    },
    {
        code: "data",
        name: "数据",
        active: false,
        contains: ['RemoveCommands', 'EditCommands', 'BatchEditCommands', 'LoadCommands']
    },
    {
        code: "flow",
        name: "流程",
        active: false,
        contains: ['ApproveController']
    },
    {
        code: "impExp",
        name: "导入导出",
        active: false,
        contains: ['DataImportExportCommand', 'ListDataImportExportCommand']
    },
    {
        code: "file",
        name: "附件",
        active: false,
        contains: ['FileController', 'AttachmentController']
    },
    {
        code: "print",
        name: "打印",
        active: false,
        contains: ['PrintService']
    },
    {
        code: "others",
        name: "其他",
        active: false,
        contains: ['DialogController', 'DiscussionGroupController', 'WizardCommands', 'CommandController', 'PopController']
    }
];
