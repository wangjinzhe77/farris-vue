export default [
    {
        id: '1e9c35f0-88a2-4735-8f15-0e614b40f3f2',
        nameSpace: 'Inspur.GS.PM.IPM.procard.procard.Front',
        code: 'SubProjectList',
        name: '子项目列表',
        fileName: 'SubProjectList.frm',
        type: 'Form',
        content: null,
        category: '最近使用'
    },
    {
        id: '581d2fde-d554-4508-bf0c-04ca74349857',
        nameSpace: 'Inspur.GS.PM.IPM.procard.procard.Front',
        code: 'SidebarList',
        name: '侧边栏导航列表',
        fileName: 'TestCE.frm',
        type: 'Form',
        content: null,
        category: '最近使用'
    },
    {
        id: 'f4d6379e-6c75-48a4-95ed-beee23901f70',
        nameSpace: 'Inspur.GS.PM.IPM.procard.procard.Front',
        code: 'AddProcardForm',
        name: '新增项目信息',
        fileName: 'AddProcardForm.frm',
        type: 'Form',
        content: null,
        category: '本地元数据'
    },
    {
        id: '3feeb0a2-556f-47f4-a78b-92fb6298ff67',
        nameSpace: 'Inspur.GS.PM.IPM.procard.procard.Front',
        code: 'ChildList',
        name: '子项目列表',
        fileName: 'ChildList.frm',
        type: 'Form',
        content: null,
        category: '本地元数据'
    }
];
