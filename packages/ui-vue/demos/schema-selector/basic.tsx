import { defineComponent, inject, provide, ref } from 'vue';
import { FButton } from '../../components/button';
import { F_MODAL_SERVICE_TOKEN, FModalService } from '../../components/modal';
import { FSchemaSelector, FSchemaSelectorEditor, ViewOption } from '../../components/schema-selector';
import {
    controllerSchemaRepositorySymbol,
    entitySchemaRepositorySymbol,
    pageSchemaRepositorySymbol,
    valueObjectSchemaRepositorySymbol
} from './mock_repository';

export default defineComponent({
    name: 'FSchemaSelectorBasic',
    setup() {

        const modalService: any = inject(F_MODAL_SERVICE_TOKEN);

        const selectPageViewOptions = ref<ViewOption[]>([
            { id: 'recommend', title: '推荐', type: 'Card', dataSource: 'Recommand', pagination: false },
            { id: 'total', title: '全部', type: 'Card', dataSource: 'Total' }
        ]);

        const selectEntityViewOptions = ref<ViewOption[]>([
            { id: 'recommend', title: '推荐', type: 'List', dataSource: 'Recommand', enableGroup: true, groupField: 'category' },
            { id: 'total', title: '全部', type: 'List', dataSource: 'Total' }
        ]);

        const selectValueObjectViewOptions = ref<ViewOption[]>([
            { id: 'recent', title: '最近', type: 'Icon', dataSource: 'Recent' },
            { id: 'recommend', title: '推荐', type: 'Icon', dataSource: 'Recommand', enableGroup: true, groupField: 'category' },
            { id: 'total', title: '全部', type: 'Icon', dataSource: 'Total' }
        ]);

        function renderPages() {
            return <FSchemaSelector injectSymbolToken={pageSchemaRepositorySymbol} viewOptions={selectPageViewOptions.value}>
            </FSchemaSelector>;

        }

        function renderEntities() {
            return <FSchemaSelector injectSymbolToken={entitySchemaRepositorySymbol} viewOptions={selectEntityViewOptions.value}>
            </FSchemaSelector>;
        }

        function renderValueObject() {
            return <FSchemaSelector injectSymbolToken={valueObjectSchemaRepositorySymbol} viewOptions={selectValueObjectViewOptions.value}>
            </FSchemaSelector>;
        }

        function renderController() {
            return <FSchemaSelector injectSymbolToken={controllerSchemaRepositorySymbol} view-type="NavList"></FSchemaSelector>;
        }

        function toggleSelectPageSchema() {
            modalService.open({ title: '选择页面', width: 950, render: renderPages });
        }

        function toggleSelectEntitySchema() {
            modalService.open({ title: '选择实体', width: 950, render: renderEntities });
        }

        function toggleSelectValueObjectSchema() {
            modalService.open({ title: '选择业务字段', width: 950, render: renderValueObject });
        }

        function toggleSelectControllerSchema() {
            modalService.open({ title: '选择控制器', width: 950, render: renderController });
        }

        return () => {
            return <>
                <FButton onClick={toggleSelectPageSchema} style="margin: 10px">选择页面</FButton>
                <FButton onClick={toggleSelectEntitySchema} style="margin: 10px">选择实体</FButton>
                <FButton onClick={toggleSelectValueObjectSchema} style="margin: 10px">选择业务字段</FButton>
                <FButton onClick={toggleSelectControllerSchema} style="margin: 10px">选择控制器</FButton>
            </>;
        };
    }
});
