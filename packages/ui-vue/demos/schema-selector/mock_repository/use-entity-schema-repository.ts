import { SchemaCategory, SchemaItem, SchemaRepositoryPagination, UseSchemaRepository } from "../../../components/schema-selector";
import allEntities from "../mock/all_entities";
import recommendEntities from '../mock/recommend_entities';

export function useEntitySchemaRepository(): UseSchemaRepository {

    function getNavigationData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaCategory[] {
        return [];
    }

    function getRecentlyData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return [];
    }

    function getRecommandData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return recommendEntities;
    }

    function getSchemaData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return allEntities;
    }

    return { getNavigationData, getRecentlyData, getRecommandData, getSchemaData };
}
