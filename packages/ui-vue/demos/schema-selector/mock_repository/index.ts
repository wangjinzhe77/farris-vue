import { useControllerSchemaRepository } from './use-controller-schema-repository';
import { useEntitySchemaRepository } from "./use-entity-schema-repository";
import { usePageSchemaRepository } from "./use-page-schema-repository";
import { useValueObjectSchemaRepository } from "./use-value-object-repository";

const entitySchemaRepositorySymbol = Symbol('entity schema repository inject token');
const pageSchemaRepositorySymbol = Symbol('page schema repository inject token');
const valueObjectSchemaRepositorySymbol = Symbol('value object schema repository inject token');
const controllerSchemaRepositorySymbol = Symbol('controller schema repository inject token');

export {
    controllerSchemaRepositorySymbol,
    entitySchemaRepositorySymbol,
    pageSchemaRepositorySymbol,
    valueObjectSchemaRepositorySymbol,
    useControllerSchemaRepository,
    useEntitySchemaRepository,
    usePageSchemaRepository,
    useValueObjectSchemaRepository
};
