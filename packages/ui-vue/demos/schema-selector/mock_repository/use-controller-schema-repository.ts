import { SchemaCategory, SchemaItem, SchemaRepositoryPagination, UseSchemaRepository } from "../../../components/schema-selector";
import allControllers from "../mock/all_controllers";
import controllCategories from '../mock/controller_categories';

export function useControllerSchemaRepository(): UseSchemaRepository {

    function getNavigationData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaCategory[] {
        return controllCategories;
    }

    function getRecentlyData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return [];
    }

    function getRecommandData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return [];
    }

    function getSchemaData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return allControllers;
    }

    return { getNavigationData, getRecentlyData, getRecommandData, getSchemaData };
}
