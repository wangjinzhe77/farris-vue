import { SchemaCategory, SchemaItem, SchemaRepositoryPagination, UseSchemaRepository } from "../../../components/schema-selector";
import allValueObject from '../mock/all_value_objects';
import recentValueObject from '../mock/recent_value_objects';
import recommendValueObjects from "../mock/recommend_value_objects";

export function useValueObjectSchemaRepository(): UseSchemaRepository {

    function getNavigationData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaCategory[] {
        return [];
    }

    function getRecentlyData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return recentValueObject;
    }

    function getRecommandData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return recommendValueObjects;
    }

    function getSchemaData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return allValueObject;
    }

    return { getNavigationData, getRecentlyData, getRecommandData, getSchemaData };
}
