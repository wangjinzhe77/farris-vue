import { SchemaCategory, SchemaItem, SchemaRepositoryPagination, UseSchemaRepository } from "../../../components/schema-selector";
import allPages from '../mock/all_pages';
import recommendPages from "../mock/recommend_pages";

export function usePageSchemaRepository(): UseSchemaRepository {

    function getNavigationData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaCategory[] {
        return [];
    }

    function getRecentlyData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return [];
    }

    function getRecommandData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return recommendPages;
    }

    function getSchemaData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return allPages;
    }

    return { getNavigationData, getRecentlyData, getRecommandData, getSchemaData };
}
