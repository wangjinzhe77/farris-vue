import { createApp } from 'vue';
import './style.css';
import App from './app.vue';
import Farris from '../components';
import Plugins from './app-plugin';

const app = createApp(App);

app.use(Farris).use(Plugins).mount('#app');
