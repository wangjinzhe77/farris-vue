import {
    controllerSchemaRepositorySymbol, entitySchemaRepositorySymbol, pageSchemaRepositorySymbol, useEntitySchemaRepository,
    useControllerSchemaRepository, usePageSchemaRepository, useValueObjectSchemaRepository, valueObjectSchemaRepositorySymbol
} from "../demos/schema-selector/mock_repository";
import { App } from "vue";

export default {
    install(app: App): void {
        app.provide(controllerSchemaRepositorySymbol, useControllerSchemaRepository())
            .provide(entitySchemaRepositorySymbol, useEntitySchemaRepository())
            .provide(pageSchemaRepositorySymbol, usePageSchemaRepository())
            .provide(valueObjectSchemaRepositorySymbol, useValueObjectSchemaRepository());
    }
};
