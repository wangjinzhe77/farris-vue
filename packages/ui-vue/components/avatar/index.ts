 
 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import FAvatar from './src/avatar.component';
import { propsResolver } from './src/avatar.props';
import FAvatarDesign from './src/designer/avatar.design.component';
import { withInstall } from '@farris/ui-vue/components/common';

export * from './src/avatar.props';

export { FAvatar };

// export default {
//     install(app: App): void {
//         app.component(Avatar.name as string, Avatar);
//     },
//     register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
//         componentMap.avatar = Avatar;
//         propsResolverMap.avatar = propsResolver;
//     },
//     registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
//         componentMap.avatar = FAvatarDesign;
//         propsResolverMap.avatar = propsResolver;
//     }
// };

FAvatar.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap.avatar = FAvatar;
    propsResolverMap.avatar = propsResolver;
};
FAvatar.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.avatar = FAvatarDesign;
    propsResolverMap.avatar = propsResolver;
};

export default withInstall(FAvatar);
