import { mount } from '@vue/test-utils';
import { FAvatar } from '..';

describe('avatar', () => {
    describe('properties', () => { });
    describe('render', () => {
        test('it should work', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FAvatar></FAvatar>;
                    };
                }
            });
            expect(wrapper.find('div').classes('f-avatar')).toBeTruthy();
            expect(wrapper.find('div').classes('f-avatar-circle')).toBeTruthy();
        });
        test('it should show error image', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    const url = '123';
                    return () => {
                        return <FAvatar modelValue={url}></FAvatar>;
                    };
                }
            });
            expect(wrapper.find('div').classes('f-avatar')).toBeTruthy();
            expect(wrapper.find('div').classes('f-avatar-circle')).toBeTruthy();
        });
    });
    describe('methods', () => { });
    describe('events', () => { });
    describe('behavior', () => {
        const wrapper = mount({
            setup(props, ctx) {
                return () => {
                    return <FAvatar></FAvatar>;
                };
            }
        });
        wrapper.find('div').trigger('click');
    });
});
