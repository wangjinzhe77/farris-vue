/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, SetupContext, inject, onMounted } from 'vue';
import { avatarProps, AvatarProps } from '../avatar.props';
import { useImage } from '../composition/use-image';

import { DesignerItemContext, useDesignerComponent } from '@farris/ui-vue/components/designer-canvas';
import { useAvatarDesignerRules } from './use-rules';

export default defineComponent({
    name: 'FAvatarDesign',
    props: avatarProps,
    emits: ['change', 'update:modelValue'] as (string[] & ThisType<void>) | undefined,
    setup(props: AvatarProps, context: SetupContext) {
        const elementRef = ref();
        const designerHostService = inject('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useAvatarDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        const avatarClass = computed(() => ({
            'f-avatar': true,
            'f-avatar-readonly': props.readonly,
            'f-avatar-circle': props.shape === 'circle',
            'f-avatar-square': props.shape === 'square'
        }));

        const modelValue = ref(props.modelValue);

        const avatarStyle = computed(() => ({
            width: props.avatarWidth + 'px',
            height: props.avatarHeight + 'px'
        }));
        const showLoading = false;
        const imgSrc = '';

        const currentImgType = ['image/image', 'image/webp', 'image/png', 'image/svg', 'image/gif', 'image/jpg', 'image/jpeg', 'image/bmp'];

        function errorSrc() {
            return '';
        }

        function getfiledata() { }

        const file = ref(null);

        const { acceptTypes, imageSource, onClickImage } = useImage(props, context, file, modelValue);

        return () => {
            return (
                <div ref={elementRef} class={avatarClass.value} style={avatarStyle.value} onClick={onClickImage}>
                    {showLoading && (
                        <div class="f-avatar-upload-loading">
                            <div class="loading-inner">加载中</div>
                        </div>
                    )}
                    <img title={props.title} class="f-avatar-image" src={imageSource.value} onError={errorSrc} />
                    {!props.readonly && (
                        <div class="f-avatar-icon">
                            <span class="f-icon f-icon-camera"></span>
                        </div>
                    )}
                    <input
                        ref="file"
                        name="file-input"
                        type="file"
                        class="f-avatar-upload"
                        accept={acceptTypes.value}
                        onChange={getfiledata}
                        style="display: none;"
                    />
                </div>
            );
        };
    }
});
