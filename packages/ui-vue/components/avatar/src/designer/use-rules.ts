 
import { UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema, DesignerComponentInstance, DesignerItemContext } from "../../../designer-canvas/src/types";
import { AvatarProperty } from "../property-config/avatar.property-config";

export function useAvatarDesignerRules(designItemContext: DesignerItemContext, designerHostService): UseDesignerRules {

    const schema = designItemContext.schema as ComponentSchema;

    // 构造属性配置方法
    function getPropsConfig(componentId: string, componentInstance: DesignerComponentInstance) {
        const inputGroupProps = new AvatarProperty(componentId, designerHostService);
        return inputGroupProps.getPropertyConfig(schema, componentInstance);
    }

    return {
        getPropsConfig
    } as UseDesignerRules;

}
