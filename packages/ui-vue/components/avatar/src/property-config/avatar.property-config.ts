import { InputBaseProperty } from "../../../property-panel/src/composition/entity/input-base-property";

export class AvatarProperty extends InputBaseProperty {

    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }

    getEditorProperties(propertyData) {
        return {
            "description": "编辑器",
            "title": "编辑器",
            "type": "avatar",
            "$converter": "/converter/property-editor.converter",
            "properties": {
                "readonly": {
                    "description": "",
                    "title": "只读",
                    "type": "boolean"
                },
                "avatarWidth": {
                    "description": "",
                    "title": "头像宽度",
                    "type": "number"
                },
                "avatarHeight": {
                    "description": "",
                    "title": "头像高度",
                    "type": "number"
                },
                "cover": {
                    "description": "",
                    "title": "封面",
                    "type": "string"
                }
            }
        };
    }
}
