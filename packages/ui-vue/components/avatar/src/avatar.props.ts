 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver/src/props-resolver';
import { schemaResolver } from './schema/schema-resolver';
import { schemaMapper } from './schema/schema-mapper';
import avatarSchema from './schema/avatar.schema.json';

type AvatarShap = 'square' | 'circle';

export const avatarProps = {
    /**
     * 头像宽度
     */
    avatarWidth: { type: Number, default: 100 },
    /**
     * 头像高度
     */
    avatarHeight: { type: Number, default: 100 },
    /**
     * 组件标识
     */
    cover: { type: String },
    /**
     * 只读
     */
    readonly: { type: Boolean, default: false },
    /**
     * 头像形状
     */
    shape: { type: String as PropType<AvatarShap>, default: 'circle' },
    /**
     * 头像最大尺寸, 单位MB
     */
    maxSize: { type: Number, default: 1 },
    /**
     * 组件值
     */
    modelValue: { type: String, default: '' },
    /**
     * 头像标题
     */
    title: { type: String, default: '' },
    /**
     * 支持的头像类型
     */
    imageType: { type: Array<string>, default: ['jpeg'] }
} as Record<string, any>;

export type AvatarProps = ExtractPropTypes<typeof avatarProps>;

export const propsResolver = createPropsResolver<AvatarProps>(avatarProps, avatarSchema, schemaMapper, schemaResolver);
