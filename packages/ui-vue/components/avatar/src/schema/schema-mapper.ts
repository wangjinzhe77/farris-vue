import { MapperFunction, resolveAppearance } from '@farris/ui-vue/components/dynamic-resolver';

export const schemaMapper = new Map<string, string | MapperFunction>([
    ['appearance', resolveAppearance]
]);
