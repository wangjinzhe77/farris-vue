
import type { App, Plugin } from 'vue';
import FEventParameter from './src/event-parameter.component';
export * from './src/composition/type';
export * from './src/event-parameter.props';

FEventParameter.install = (app: App) => {
    app.component(FEventParameter.name as string, FEventParameter);
};
FEventParameter.register =
    (componentMap: Record<string, any>, propsResolverMap: Record<string, any>,
        configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
        componentMap['event-parameter'] = FEventParameter;
        propsResolverMap['event-parameter'] = FEventParameter;
    };
FEventParameter.registerDesigner = (componentMap: Record<string, any>,
    propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['event-parameter'] = FEventParameter;
    propsResolverMap['event-parameter'] = FEventParameter;
};

export { FEventParameter };

export default FEventParameter as typeof FEventParameter & Plugin;
