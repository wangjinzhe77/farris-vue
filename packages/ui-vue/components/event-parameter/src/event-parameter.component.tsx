import { App, computed, defineComponent, inject, reactive, Ref, ref, SetupContext, watch } from 'vue';
import { FDynamicFormGroup } from '@farris/ui-vue/components/dynamic-form';
import { F_MODAL_SERVICE_TOKEN } from '@farris/ui-vue/components/modal';
import FTabs, { FTabPage } from '@farris/ui-vue/components/tabs';
import FTreeView from '@farris/ui-vue/components/tree-view';
import FTextarea from '@farris/ui-vue/components/textarea';
import { VisualData, RowOptions } from '@farris/ui-vue/components/data-view';
import FLayout, { FLayoutPane } from '@farris/ui-vue/components/layout';
import { eventParameterProps, EventParameterProps } from './event-parameter.props';
import useEditor from './composition/use-editor';
import useEditorInput from './composition/use-editor-input';
import { useGeneralEditor } from './composition/use-general-editor';
import './event-parameter.scss';

export default defineComponent({
    name: 'FEventParameter',
    props: eventParameterProps,
    emits: [
        'valueChange',
        'onBlur',
        'applicationSelectionChange',
        'componentSelectionChange',
        'parameterChanged',
        'update:modelValue',
        'update:parameterValue',
        'confirm'
    ] as (string[] & ThisType<void>) | undefined,
    setup(props: EventParameterProps, context: SetupContext) {
        const id = ref(props.id);
        const label = ref(props.label);
        const showLabel = ref(props.showLabel);
        // 输入组件modelValue
        const modelValue = ref(props.modelValue);
        // textarea modelValue
        const textareaModelValue = ref(props.modelValue);
        // 初始化子组件
        const { getEditorConfig } = useEditor(props);
        const { shouldRenderAppendButton } = useEditorInput(props);
        const editorConfig = getEditorConfig();
        const parameterEditorRef = ref();
        const textareaRef = ref();
        const tabId = ref('tabField');

        const modalService = inject(F_MODAL_SERVICE_TOKEN) as any;

        const rowNumberOption = ref({
            enable: false,
        });
        const rowOption: Ref<Partial<RowOptions>> = ref({
            customRowStatus: (visualData: VisualData) => {
               if(visualData.collapse === undefined){
                if (!visualData.raw.parent || !visualData.raw.parentId) {
                    visualData.collapse = false;
                }
                if (visualData.raw.data?.$type === 'ComplexField') {
                    visualData.collapse = true;
                }
               }
                if (visualData.raw.hasChildren) {
                    visualData.disabled = true;
                }
                return visualData;
            }
        });

        // 编辑器配置 //
        const editor = computed(() => {
            return {
                // 编辑器类型
                type: editorConfig?.type,
                /** 根据不同编辑器  设置不同编辑器的属性 */
                ...editorConfig?.componentProps
            };
        });

        const { tabs } = useGeneralEditor(props);
        // 值变化事件
        function onValueChange($event: any) {
            context.emit('update:modelValue', $event);
            context.emit('valueChange', $event);
        }
        function processFormOutlineData(data: any, textareaRef: HTMLTextAreaElement, result?: string) {
            const selectionStart = textareaRef.selectionStart || 0;
            const { selectionEnd } = textareaRef;
            const selections = selectionEnd - selectionStart;
            const hasSelection = selections > 0;
            const results = result && result.split('') || [];
            if (hasSelection) {
                results.splice(selectionStart, selections, data);
            } else {
                results.splice(selectionStart, 0, data);
            }
            return results.join('');
        }
        function processFormFieldData(rowData: any, textareaRef: HTMLTextAreaElement) {
            if (!rowData.data) {
                return;
            }
            // 子表双击没效果
            // if (rowData.parent?.includes('childEntity')) {
            //     return;
            // }
            // if (rowData.hasChildren) {
            //     return;
            // }
            const { data } = rowData;
            // this.result = `{FIELD~${data.path}/${data.bindingPath}}`;
            let componentNodeId;
            let node = rowData.parentNode;
            let { label } = data;
            while (node && node.parentNode) {
                if (node.data) {
                    label = node.data.label + '/' + label;
                }
                node = node.parentNode;
                // if (node.nodeType === 'field') {
                //     // 帮助、udt的带出节点，父节点仍是一个field
                //     label = node.data.label + '/' + label;
                //     node = node.parentNode;
                // } else {
                //     // component
                //     componentNodeId = node;
                //     if (componentNodeId.includes('_')) {
                //         componentNodeId = componentNodeId.slice(componentNodeId.indexOf('_') + 1);
                //     }
                //     break;
                // }
            }
            return processFormOutlineData(`{DATA~/${label}}`, textareaRef);
        }

        function processFormVariableData(data: any, textareaRef: HTMLTextAreaElement) {
            const { statePath } = data;
            let result = textareaRef.value;
            if (statePath) {
                const paths: string[] = statePath.split('/');
                result = statePath;
                if (paths.length === 3) {
                    const frameId = paths[1];
                    const relativeFrameId = `#{${frameId}}`;
                    paths.splice(1, 1, relativeFrameId);
                    result = paths.join('/');
                }
                result = `{UISTATE~${result}}`;
            }
            return processFormOutlineData(result, textareaRef);
        }
        function doubleClickRowHandler(tabId: string, index: number, raw: any) {
            switch (tabId) {
                case 'tabForm':
                    textareaModelValue.value = processFormOutlineData(`#{${raw.data.id}}`,
                        textareaRef.value.elementRef.value, '');
                    break;
                case 'tabField':
                    textareaModelValue.value = processFormFieldData(raw, textareaRef.value.elementRef.value);
                    break;
                case 'tabVar':
                    textareaModelValue.value = processFormVariableData(raw.data, textareaRef.value.elementRef.value);
                    break;
            }
            context.emit('paramChanged', { tabId, index, raw, value: modelValue.value });
        }

        function renderParameterEditor() {
            return () =>
                <FLayout class="nav-tree-panel"  >
                    <FLayoutPane position="left" width={300} resizable={false}>
                        <FTabs v-model:activeId={tabId.value}>
                            {tabs.map(item =>
                                <FTabPage key={item.id} id={item.id}
                                    title={item.title}>
                                    {item.id === 'tabField' && <FTreeView
                                        rowNumber={rowNumberOption}
                                        rowOption={rowOption.value}
                                        key={item.treeConfigs.id}
                                        data={item.treeConfigs.data}
                                        fit={true}
                                        height={340}
                                        onDoubleClickRow={function doubleClickRow(index: number, raw: any) {
                                            doubleClickRowHandler(item.id, index, raw);
                                        }}
                                    ></FTreeView>}
                                    {item.id !== 'tabField' && <FTreeView
                                        rowNumber={rowNumberOption}
                                        // rowOption={rowOption.value}
                                        key={item.treeConfigs.id}
                                        data={item.treeConfigs.data}
                                        fit={true}
                                        height={340}
                                        onDoubleClickRow={function doubleClickRow(index: number, raw: any) {
                                            doubleClickRowHandler(item.id, index, raw);
                                        }}
                                    ></FTreeView>}
                                </FTabPage>)}
                        </FTabs>
                    </FLayoutPane>
                    <FLayoutPane position="center">
                        <FTextarea
                            ref={textareaRef}
                            v-model={textareaModelValue.value}
                            rows={10}
                            autoHeight={true}
                        >
                        </FTextarea>
                    </FLayoutPane>
                </FLayout>;
        }
        // 扩展按钮展示参数编辑器
        function onClickAppend() {
            textareaModelValue.value = modelValue.value;
            const parameterEditorModal: any = modalService.open({
                title: '参数编辑器',
                width: 900,
                height: 500,
                fitContent: false,
                showHeader: true,
                showCloseButton: true,
                draggable: true,
                render: renderParameterEditor(),
                buttons: [
                    {
                        class: 'btn btn-secondary',
                        text: '取消',
                        handle: () => {
                            parameterEditorModal.destroy();
                        }
                    },
                    {
                        class: 'btn btn-primary',
                        text: '确定',
                        handle: () => {
                            modelValue.value = textareaModelValue.value;
                            context.emit('update:modelValue', modelValue.value);
                            context.emit('confirm', modelValue.value);
                            parameterEditorModal.destroy();
                        }
                    }
                ]
            });
        }
        // 渲染扩展按钮
        function renderInputAppendButton() {
            return editor.value.type === 'input-group' ?
                <span class="f-icon f-icon-lookup" onClick={onClickAppend} style="
                margin-top: 2px;margin-right: 5px;color: rgba(0, 0, 0, .25)"></span> : <></>;
        }
        watch(() => props.modelValue, (newValue, oldValue) => {
            if (newValue !== oldValue) {
                modelValue.value = newValue;
            }
        });
        const onClear = () => {
            context.emit('update:modelValue', '');
        };

        const getInputRef = () => {
            return textareaRef.value.elementRef;
        };

        context.expose({ getInputRef });

        return () => {
            return <FDynamicFormGroup
                id={id.value}
                editor={editor.value}
                v-model={modelValue.value}
                showLabel={false}
                onChange={onValueChange}
                onClear={onClear}
            >
                {{
                    groupTextTemplate: () => renderInputAppendButton(),
                }}
            </FDynamicFormGroup>;
        };
    }
});
