import { ComboListProps } from "@farris/ui-vue/components/combo-list";
import { ComboTreeProps } from "@farris/ui-vue/components/combo-tree";
import { EditorType as DynamicFormEditorType } from "@farris/ui-vue/components/dynamic-form";
import { InputGroupProps } from "@farris/ui-vue/components/input-group";
import { ExtractPropTypes, PropType } from "vue";

export interface EditorConfig {
    type?: DynamicFormEditorType;
    componentProps?: Partial<ComboListProps | InputGroupProps | ComboTreeProps>;
    context?: any;
}

export interface EditorMap {
    [key: string]: EditorConfig;
}
export enum EditorType {
    Default = 'Default',
    Textbox = 'Textbox',
    Numberbox= 'Numberbox',
    Combolist= 'Combolist',
    Select= 'Select',
    Checkbox= 'Checkbox',
    Switch= 'Switch',
    Tags='Tags',
    SortEditor= 'SortEditor',
    FilterEditor= 'FilterEditor',
    DataEditor= 'DataEditor',
    MenuIdSelector= 'MenuIdSelector',
    AppIdSelector= 'AppIdSelector',
    ComboLookup= 'ComboLookup',
    ConfigurationParameterEditor= 'ConfigurationParameterEditor',
    FieldMappingEditor= 'FieldMappingEditor'
};

export const editorMap: EditorMap = {};

export const eventParameterProps = {
    /** 编辑器类型 */
    data: { type: Object as PropType<Array<any>>, default: [] },
    /** 复选框组件id  */
    id: { type: String, default: '' },
    /** 视图模型 id */
    editable: { type: Boolean, default: true },
    /** 参数编辑器类型 */
    editorType: {type:String, default: ''},
    /** 筛选组件配置器，具体配置项可查看各组件文档 */
    // editor: { type: Object as PropType<EditorConfig>, default: {} },
    // 组件标题
    label: { type: String, default: '' },
    /** 组件值 */
    modelValue: { type: [String, Boolean, Array, Number], default: '' },
    paramData : { type: Object as PropType<Array<any>>, default: []},
    // 是否展示标题
    showLabel: { type: Boolean, default: true },
    idField: { type: String, default: ''},
    textField: { type: String, default: ''},
    // 只读
    readonly: { type: Boolean, default: false },
    // 必填
    disabled: { type: Boolean, default: false },
    // 通用参数编辑器字段数据
    fieldData: { type: Object, default: []},
    // 通用参数编辑器变量数据
    varData: { type: Object, default: []},
    // 通用参数编辑器表单数据
    formData: { type: Object, default: []},
} as Record<string, any>;

export type EventParameterProps = ExtractPropTypes<typeof eventParameterProps>;
