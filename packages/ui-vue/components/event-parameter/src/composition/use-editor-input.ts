import { computed, ref } from "vue";
import { editorMap, EditorType, EventParameterProps } from "../event-parameter.props";
import {  UseEditorInput } from "./type";

export default function (
    props: EventParameterProps,
): UseEditorInput {
    const editorType = ref(props.editorType);
    const shouldRenderAppendButton = computed(()=> editorType.value === EditorType.Default);
    // 构造组件属性
    function createEditorProps(): void {
        editorMap[editorType.value] = {
            type: 'input-group',
            componentProps: {
            }
        };
    }

    function onValueChange() {
        
    }

    return { createEditorProps ,shouldRenderAppendButton, onValueChange };
}
