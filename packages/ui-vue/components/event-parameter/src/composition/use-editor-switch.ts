import { ref } from "vue";
import { editorMap, EventParameterProps } from "../event-parameter.props";
import { UseBaseEditor } from "./type";

export default function (
    props: EventParameterProps
): UseBaseEditor {
    const editorType = ref(props.editorType);
    // 构造组件属性
    function createEditorProps(): void {
        editorMap[editorType.value] = {
            type: 'switch',
            componentProps: {

            }
        };

    }

    function onValueChange() {
        
    }
    return { createEditorProps,onValueChange };
}
