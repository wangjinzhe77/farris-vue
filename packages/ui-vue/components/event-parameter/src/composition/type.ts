import { ComputedRef, Ref } from "vue";
import { EditorConfig, EditorMap } from "../event-parameter.props";
import { ComponentSchema } from "@farris/ui-vue/components/designer-canvas";

export interface UseBaseEditor {
    /**
     * 参数组件值变化后的响应函数
     * @param $event 鼠标事件对象
     * @returns 空
     */
    onValueChange: (selectItems: any[], value: any) => void;
    /**
     * 构造属性
     * @returns
     */
    createEditorProps: () => void;
}

export interface UseEditorProxy {
    /**
     * 获取编辑器类型
     * @returns 
     */
    getEditorType: () => string;

    /**
     * 获取编辑器配置
     * @returns 
     */
    getEditorConfig: () => EditorConfig;

    /**
     * 获取编辑器映射
     * @returns 
     */
    getEditorMap: () => EditorMap;
}

// export interface UseEditorSelect extends UseBaseEditor {

// }

export interface UseEditorInput extends UseBaseEditor {
    shouldRenderAppendButton: ComputedRef<boolean>;
}

export interface UseParameterEditor {
    /**
     * 组件结构数据
     */
    tabs: Ref<any[]>;
    /**
     * 重组表单组件数据
     * @returns 
     */
    assembleOutline: () => void;
    /**
     * 重组字段数据
     * @returns 
     */
    assembleSchemaFieldsByComponent: () => void;

    /**
     * 重组变量数据
     * @returns
     */
    assembleStateVariables: () => void;

    /**
     * 处理表单组件的返回值
     * @param data
     * @param textareaRef
     * @returns
     */
    processFromOutlineData: (data: any, textareaRef: HTMLTextAreaElement, result?: string) => string;

    /**
     * 处理字段的返回值
     * @param data 
     * @param textareaRef 
     * @returns 
     */
    processFromFieldData: (data: any, textareaRef: HTMLTextAreaElement) => string | undefined;

    /**
     * 处理变量的返回值
     * @param data
     * @param textareaRef
     * @returns
     */
    processFromVariableData: (data: any, textareaRef: HTMLTextAreaElement) => string;
}

