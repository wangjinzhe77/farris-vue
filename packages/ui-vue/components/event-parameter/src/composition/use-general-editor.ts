import { computed, reactive, ref } from "vue";
import { editorMap, EditorType, EventParameterProps } from "../event-parameter.props";
import {  UseEditorInput } from "./type";

export function useGeneralEditor(
    props: EventParameterProps,
) {
    const tabs = reactive([

        {
            id: 'tabField',
            title: '字段',
            treeConfigs: {
                id: 'tabFieldTree',
                columns: [{ field: 'name' }],
                data: [] as any[],
                onSelectNode: () => { },
                componentRef: ref()
            }
        },
        {
            id: 'tabVar',
            title: '变量',
            treeConfigs: {
                id: 'tabVarTree',
                data: [] as Array<any>,
            }
        },
        {
            id: 'tabForm',
            title: '表单组件',
            treeConfigs: {
                id: 'tabFormTree',
                data: [] as Array<any>,
            }
        }
    ]);

    tabs[0].treeConfigs.data = props.fieldData;
    tabs[1].treeConfigs.data = props.varData;
    tabs[2].treeConfigs.data = props.formData;
    return { tabs };
}
