import { computed, ref } from "vue";
import { editorMap, EditorType, EventParameterProps } from "../event-parameter.props";
import { UseEditorInput } from "./type";

export default function (
    props: EventParameterProps,
): UseEditorInput {
    const editorType = ref(props.editorType);
    const shouldRenderAppendButton = computed(() => editorType.value === EditorType.Default);
    // 构造组件属性
    function createEditorProps(): void {
        editorMap[editorType.value] = {
            type: 'combo-tree',
            componentProps: {
                data: props.data,
                placeholder: '请选择',
                enableSearch: false,
                enableClear: true,
                editable: true,
                readonly: false,
                disabled: false,
                idField: props.idField || 'id',
                textField: props.textField || 'name',
                mapFields: { id: 'selectId' },
                multiSelect: false,
            }
        };
    }
    function onValueChange() {

    }

    return { createEditorProps, shouldRenderAppendButton, onValueChange };
}
