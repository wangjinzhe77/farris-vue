import { ref } from "vue";
import { editorMap, EditorType, EventParameterProps } from "../event-parameter.props";
import { UseEditorProxy } from "./type";
import useEditorSelect from './use-editor-select';
import useEditorSwitch from './use-editor-switch';
import useEditorInput from './use-editor-input';
import useComboLookup from './use-editor-combotree';

export default function (
    props: EventParameterProps,
): UseEditorProxy {
    const editorType = ref(props.editorType);

    // 下拉组件
    const useEditorSelectComposition = useEditorSelect(props);
    // 开关组件
    const useEditorSwitchComposition = useEditorSwitch(props);
    // 默认组件 input group组件
    const useEditorInputComposition = useEditorInput(props);
    // 
    const useEditorComboLookupComposition = useComboLookup(props);
    function getEditorConfig() {
        const editorTypeValue = editorType.value;
        if (editorTypeValue === EditorType.Combolist ||
            editorTypeValue === EditorType.Select
        ) {
            useEditorSelectComposition.createEditorProps();
        } else if (editorTypeValue === EditorType.Checkbox) {
            // 复选框组件
        } else if (
            editorTypeValue === EditorType.FilterEditor ||
            editorTypeValue === EditorType.DataEditor ||
            editorTypeValue === EditorType.SortEditor ||
            editorTypeValue === EditorType.MenuIdSelector ||
            editorTypeValue === EditorType.AppIdSelector

        ) {
            // 其他情况
        }
        else if (editorTypeValue === EditorType.ComboLookup) {
            // combo  tree
            useEditorComboLookupComposition.createEditorProps();

        } else if (editorTypeValue === EditorType.Numberbox) {
            // 数字
        } else if (editorTypeValue === EditorType.Switch) {
            useEditorSwitchComposition.createEditorProps();
        } else {
            useEditorInputComposition.createEditorProps();
        }
        return editorMap[editorType.value];
    }

    function getEditorType() {
        return 'input-group';
    }

    function getEditorMap() {
        return {};
    }
    return { getEditorConfig, getEditorType, getEditorMap };

}
