
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { PropType } from 'vue';
export type RadioCheckboxType = 'default' | 'button';
export const radioCheckboxCommonProps = {
    /**
     * 是否被选中
     */
    checked: { type: Boolean, default: false },
    /**
     * 展示方向  水平或者垂直
     */
    direction: { type: String as PropType<'vertical' | 'horizontal'>, default: 'horizontal' },
    /**
     * 单选组类型
     */
    // mode: { type: String as PropType<'default' | 'button' | 'tag'>, default: 'default' },
    /**
     * type为button或者tag时的颜色
     */
    type: { type: String as PropType<RadioCheckboxType>, default: 'default' },
    /**
     * type为button或者tag时的尺寸
     * 应该要弃用
     */
    size: { type: String as PropType<'small' | 'middle' | 'large'>, default: 'middle' },
    /** 
     * 选项列表
     */
    options: { type: Object, default: []}
};
