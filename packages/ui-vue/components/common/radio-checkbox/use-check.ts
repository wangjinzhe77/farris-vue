import { computed, ref, SetupContext, watch } from 'vue';
import { isUndefined } from 'lodash-es';
import { CheckBoxProps, CheckboxGroupProps } from '@farris/ui-vue/components/checkbox';
import { RadioProps, RadioGroupProps } from '@farris/ui-vue/components/radio-group';

export function useCheck(
    props: CheckBoxProps | RadioProps,
    context: SetupContext,
    parentProps: CheckboxGroupProps | RadioGroupProps,
    parentContext: SetupContext,
) {
    // name,如果相同，可以作为一组
    const name = computed(() => parentProps?.name);
    // 禁用
    const disabled = computed(() => props.readonly || props.disabled || parentProps?.readonly || parentProps?.disabled);
    // 展示按钮样式
    const shouldRenderButton = computed(() => parentProps?.type === 'button');
    // 展示原生单选框或者复选框
    const shouldRenderNative = computed(() => parentProps?.type === 'default' || isUndefined(parentProps?.type));

    // 如果是group
    const checked = computed(() => parentProps ?
        parentProps.modelValue === props.value || parentProps.modelValue.includes(props.value) :
        !!props.checked || !!props.modelValue);

    // 按钮样式
    const buttonClass = computed(() => {
        return {
            btn: true,
            'f-radio-button': true,
            active: checked.value,
            'f-radio-button-primary': true,
        };
    });


    const modelValue = ref(props.modelValue);
    const indeterminate = ref(props.indeterminate);
    watch(() => props.modelValue, (newValue: boolean) => {
        modelValue.value = newValue;
    });
    watch(() => props.indeterminate, (newValue: boolean) => {
        indeterminate.value = newValue;
    });

    // 点击单选框事件
    const onClickRadio = (e: MouseEvent) => {
        e.stopPropagation();
        if (disabled.value) {
            return;
        }
        if (parentProps) {
            // 父组件双向绑定
            if (!checked.value) {
                parentContext.emit('update:modelValue', props.value);
                // 值变化事件
                parentContext.emit('changeValue', props.value);
            }
        } else {
            context.emit('update:checked', !checked.value);
        }
    };

    // 点击复选框事件
    const onClickCheckBox = (e: MouseEvent) => {
        e.stopPropagation();
        if (disabled.value) {
            return;
        }
        if (parentProps) {
            // 父组件双向绑定
            if (!checked.value) {
                parentContext.emit('update:modelValue', [...parentProps.modelValue, props.value]);
                // 值变化事件
                parentContext.emit('changeValue', [...parentProps.modelValue, props.value]);
            } else {
                const valuesWithoutSelf = parentProps.modelValue?.filter(value => value !== props.value);
                parentContext.emit('update:modelValue', valuesWithoutSelf);
                // 值变化事件
                parentContext.emit('changeValue', valuesWithoutSelf);
            }
        } else {
            context.emit('update:checked', !checked.value);
            context.emit('update:modelValue', !checked.value);
            context.emit('changeValue', !checked.value);
            context.emit('change', { originalEvent: e, checked: !checked.value });
        }
    };
    return {
        buttonClass,
        checked,
        disabled,
        indeterminate,
        name,
        onClickCheckBox,
        onClickRadio,
        shouldRenderButton,
        shouldRenderNative
    };
}
