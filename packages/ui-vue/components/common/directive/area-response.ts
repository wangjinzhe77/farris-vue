import { addClass, removeClass } from "../utils/use-class";

const breakPoints = [
    { size: 'sm', width: 576 },
    { size: 'md', width: 768 },
    { size: 'lg', width: 888 },
    { size: 'xl', width: 1200 },
    { size: 'el', width: 1690 }
];

/**
 * 移除绑定
 * @param sharedObject 
 */
function deleteObserver(sharedObject) {
    if (sharedObject.resizeObserver) {
        sharedObject.resizeObserver.disconnect();
        sharedObject.sharedObject = null;
    }
}
/**
 * 宽度变更后，执行事件
 * @param sharedObject 
 * @param bindElement 
 * @param newWidth 
 * @returns 
 */
function afterWidthChange(sharedObject, bindElement, newWidth = 0) {
    const result = [] as any;
    if (!bindElement) {
        return;
    }
    const areaWidth = newWidth ? newWidth : bindElement.getBoundingClientRect().width;
    const width = parseFloat(areaWidth);
    for (let k = 0; k < breakPoints.length; k++) {
        if (breakPoints[k]['width'] <= width) {
            result.push(breakPoints[k]['size']);
        }
    }
    if (sharedObject.className.join(',') !== result.join(',')) {
        // 旧的比新的多，应该移除class
        const distance = sharedObject.className.length - result.length;
        if (distance > 0) {
            for (let m = result.length; m < sharedObject.className.length; m++) {
                removeClass(bindElement, 'f-area-response--' + sharedObject.className[m]);
            }
        } else {
            for (let m = sharedObject.className.length; m < result.length; m++) {
                addClass(bindElement, 'f-area-response--' + result[m]);
            }
        }
        sharedObject.className = [...result];
    }
}

/**
 * 绑定监听宽度变化事件
 * @param sharedObject 
 * @param bindElement 
 */
function bindObserver(sharedObject, bindElement) {
    if (sharedObject.enable && sharedObject.autoWidth) {
        if (!sharedObject.resizeObserver) {
            sharedObject.resizeObserver = new ResizeObserver(entries => {
                if (!entries || entries.length < 1 || !entries[0].contentRect) {
                    return;
                }
                const rect = entries[0].contentRect;
                if (Math.abs(parseInt(rect.width + '') - sharedObject.width) > sharedObject.threshold) {
                    afterWidthChange(sharedObject, bindElement, rect.width);
                    sharedObject.width = parseInt(rect.width + '');
                }
            });
            sharedObject.resizeObserver.observe(bindElement);
        }
    } else {
        deleteObserver(sharedObject);
    }
}

function supportResponse(sharedObject, bindElement) {
    // 启用响应
    if (sharedObject.enable) {
        addClass(bindElement, 'f-area-response');
        afterWidthChange(sharedObject, bindElement);
    } else {
        // 移除对应的样式
        removeClass(bindElement, 'f-area-response');
        for (let m = sharedObject.className.length; m > 0; m--) {
            addClass(bindElement, 'f-area-response--' + sharedObject.className[m]);
        }
        sharedObject.className = [];
    }
    bindObserver(sharedObject, bindElement);
}
function updateSharedObject(sharedObject, binding) {
    if (binding.value && Object.prototype.hasOwnProperty.call(binding.value, 'enable')) {
        sharedObject.enable = binding.value.enable;
    }
    if (binding.value && Object.prototype.hasOwnProperty.call(binding.value, 'autoWidth')) {
        sharedObject.autoWidth = binding.value.autoWidth;
    }
}

/**
 * enable:启用
 * autoWidth:自动宽度
 */
const areaResponseDirective = {
    // 在绑定元素的父组件
    // 及他自己的所有子节点都挂载完成后调用
    mounted: (bindElement, binding, vnode) => {
        vnode.sharedObject = {
            className: [],
            resizeObserver: null,
            enable: true,
            autoWidth: true,
            threshold: 10,
            width: 0
        };
        updateSharedObject(vnode.sharedObject, binding);
        supportResponse(vnode.sharedObject, bindElement);
    },
    // 在绑定元素的父组件
    // 及他自己的所有子节点都更新后调用
    updated: function (bindElement, binding, vnode, prevVnode) {
        vnode.sharedObject = prevVnode.sharedObject;
        updateSharedObject(vnode.sharedObject, binding);
        supportResponse(vnode.sharedObject, bindElement);
    },
    // 绑定元素的父组件卸载前调用
    beforeUnmount(bindElement, binding, vnode) {
        deleteObserver(vnode.sharedObject);
    }
};
export default areaResponseDirective;
