import { ComputedRef, Ref } from "vue";

export interface TextBoxProps {

    customClass: string;

    disabled: boolean;

    editable: boolean;

    enableClear: boolean;

    enableTitle: boolean;

    forcePlaceholder: boolean;

    modelValue: string;

    placeholder: string;

    readonly: boolean;

    textAlign: string;

    type: string;

    showBorder: boolean;

    updateOn: 'blur' | 'change';
}

export interface UseClear {

    clearButtonClass: ComputedRef<Record<string, boolean>>;

    clearButtonStyle: ComputedRef<Record<string, any>>;

    hasShownClearButton: Ref<boolean>;

    onClearValue: ($event: MouseEvent) => void;

    onMouseEnter: ($event: MouseEvent) => void;

    onMouseLeave: ($event: MouseEvent) => void;

    shouldShowClearButton: ComputedRef<boolean>;
}

export interface UseTextBox {

    changeTextBoxValue: (newValue: string, shouldEmitChangeEvent: boolean) => void;

    disabled?: Ref<boolean>;

    displayText?: Ref<string>;

    editable?: Ref<boolean>;

    hasFocused?: ComputedRef<boolean>;

    inputGroupClass: ComputedRef<Record<string, boolean>>;

    inputType: Ref<string>;

    isEmpty: ComputedRef<boolean>;

    modelValue?: Ref<string>;

    readonly?: ComputedRef<boolean>;

    onBlur?: (payload: FocusEvent) => boolean;

    onClick?: (payload: MouseEvent) => void;

    onFocus?: (payload: FocusEvent) => void;

    onInput?: (payload: Event) => void;

    onKeydown?: (payload: KeyboardEvent) => void;

    onKeyup?: (payload: KeyboardEvent) => void;

    onMousedown?: (payload: MouseEvent) => void;

    onTextBoxValueChange?: (payload: Event) => void;

    placeholder: ComputedRef<string>;

    textBoxClass: ComputedRef<Record<string, boolean>>;

    textBoxTitle: ComputedRef<string>;

    inputGroupStyle: ComputedRef<string>;
}

export interface UseDateFormat {
    formatTo: (date: string | Date, format: string) => string;
}
export type NumberType = string | number;
export interface NumberOption {
    prefix?: string;
    suffix?: string;
    decimalSeparator?: string;
    groupSeparator?: string;
    [key: string]: any;
}
export interface UseNumberFormat {
    formatTo: (value: NumberType, opts: any) => string;
    removeFormat: (value: NumberType | null | undefined, opts: any) => string;
    convertCurrency: (money: string) => string;
    toFixed: (value: string, precision: number) => string;
    toNumber: (value: string) => number;
    greaterThan: (firstValue: NumberType, secondValue: NumberType) => boolean;
    lessThan: (firstValue: NumberType, secondValue: NumberType) => boolean;
    equal: (firstValue: NumberType, secondValue: NumberType) => boolean;
    minus: (firstValue: NumberType, secondValue: NumberType) => string;
    multiplied: (firstValue: NumberType, secondValue: NumberType) => string;
    plus: (firstValue: NumberType, secondValue: NumberType) => string;
    average: (total: NumberType, len: number) => string;
    sum: (numberArray: NumberType[]) => string;
    min: (firstValue: Array<NumberType> | NumberType, secondValue: NumberType | null) => string;
    max: (firstValue: Array<NumberType> | NumberType, secondValue: NumberType | null) => string;
}

export type TimeAgoDate = Date | string | number;
export type TimeAgoOptions = {
    // 可能后期有其他属性
    /** 相对的日期 */
    readonly relativeDate?: TimeAgoDate;
};
export interface UseTimeAgoFormat {
    formatTo(date: TimeAgoDate, opts?: TimeAgoOptions): string
}
