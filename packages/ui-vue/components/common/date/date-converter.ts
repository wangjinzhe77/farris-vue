 
const MONTHNAMES_LOOKUP: Record<string, number> =
    { jan: 1, feb: 2, mar: 3, apr: 4, may: 5, jun: 6, jul: 7, aug: 8, sep: 9, oct: 10, nov: 11, dec: 12 };
const DAYNAMES_LOOKUP: Record<string, number> = { sun: 0, mon: 1, tue: 2, wed: 3, thu: 4, fri: 5, sat: 6 };

export class DateConverter {
    regexes: Record<string, string> = {
        YEAR: '[1-9]\\d{3}',
        MONTH: '1[0-2]|0?[1-9]',
        MONTH2: '1[0-2]|0[1-9]',
        MONTHNAME:
            'jan|january|feb|february|mar|march|apr|april|may|jun|june|jul|july|aug|august|sep|september|oct|october|nov|november|dec|december',
        DAYNAME: 'mon|monday|tue|tuesday|wed|wednesday|thu|thursday|fri|friday|sat|saturday|sun|sunday',
        DAY: '3[01]|[12]\\d|0?[1-9]',
        DAY2: '3[01]|[12]\\d|0[1-9]',
        TIMEZONE: '[+-][01]\\d\\:?[0-5]\\d',
        H24: '[01]\\d|2[0-3]',
        MIN: '[0-5]\\d',
        SEC: '[0-5]\\d',
        MS: '\\d{3,}',
        H12: '0?[1-9]|1[012]',
        AMPM: 'am|pm',
        UNIT: 'year|month|week|day|hour|minute|second|millisecond'
    };

    patterns = [
        // 2010-03-15
        ['iso_8601', this.makePattern('^(_YEAR_)-(_MONTH_)-(_DAY_)$'), '$2/$3/$1'],

        // 3-15-2010
        ['us', this.makePattern('^(_MONTH_)([\\/-])(_DAY_)\\2(_YEAR_)$'), '$1/$3/$4'],

        // 15.03.2010
        ['world', this.makePattern('^(_DAY_)([\\/\\.])(_MONTH_)\\2(_YEAR_)$'), '$3/$1/$4'],

        // 15-Mar-2010, 8 Dec 2011, "Thu, 8 Dec 2011"
        ['chicago', this.makePattern('^(?:(?:_DAYNAME_),? )?(_DAY_)([ -])(_MONTHNAME_)\\2(_YEAR_)$'), '$3 $1, $4'],

        // "March 4, 2012", "Mar 4 2012", "Sun Mar 4 2012"
        ['conversational', this.makePattern('^(?:(?:_DAYNAME_),? )?(_MONTHNAME_) (_DAY_),? (_YEAR_)$'), '$1 $2, $3'],

        // Tue Jun 22 17:47:27 +0000 2010
        [
            'month_day_time_year',
            this.makePattern('^(?:_DAYNAME_) (_MONTHNAME_) (_DAY_) ((?:_H24_)\\:(?:_MIN_)(?:\\:_SEC_)?) (_TIMEZONE_) (_YEAR_)$'),
            (dateInfoList: Array<any>) => {
                const month = (''+this.getMonthByName(dateInfoList[1])).padStart(2, '0');
                const day = (''+dateInfoList[2]).padStart(2, '0');
                const year = dateInfoList[5];
                const time = dateInfoList[3];
                const timezone = dateInfoList[4];
                const date: any = year + '-' + month + '-' + day + 'T' + time + timezone;
                if (isNaN(date)) {
                    return false;
                }
                return date;
            }
        ],

        // @123456789
        [
            'unix',
            /^@(-?\d+)$/,
            (match: Array<any>) => {
                return this.create(parseInt(match[1], 10) * 1000);
            }
        ],

        // 24-hour time (This will help catch Date objects that are casted to a string)
        [
            '24_hour',
            this.makePattern('^(?:(.+?)(?: |T))?(_H24_)\\:(_MIN_)(?:\\:(_SEC_)(?:\\.(_MS_))?)? ?(?:GMT)?(_TIMEZONE_)?(?: \\([A-Z]+\\))?$'),
            (match: Array<any>) => {
                let date: any;
                const datePart = match[1];
                if (datePart) {
                    date = this.create(datePart);
                    if (isNaN(date)) {
                        return false;
                    }
                } else {
                    date = new Date();
                    date.setMilliseconds(0);
                }
                const hour = match[2];
                const minute = match[3];
                const second = match[4];
                const milliseconds = match[5];
                (date as Date).setHours(parseFloat(hour), parseFloat(minute), parseFloat(second || 0));

                if (milliseconds) {
                    (date as Date).setMilliseconds(+String(milliseconds).slice(0, 3));
                }

                return date;
            }
        ],

        // 12-hour time
        [
            '12_hour',
            this.makePattern('^(?:(.+) )?(_H12_)(?:\\:(_MIN_)(?:\\:(_SEC_))?)? ?(_AMPM_)$'),
            (match: Array<any>) => {
                let date: any;
                const datePart = match[1];
                if (datePart) {
                    date = this.create(datePart);
                    if (isNaN(date)) {
                        return false;
                    }
                } else {
                    date = new Date();
                    date.setMilliseconds(0);
                }
                let hour = parseFloat(match[2]);
                hour = match[5].toLowerCase() === 'am' ? (hour === 12 ? 0 : hour) : hour === 12 ? 12 : hour + 12;
                const minute = match[3];
                const second = match[4];
                date.setHours(hour, parseFloat(minute || 0), parseFloat(second || 0));
                return date;
            }
        ]
    ];

    makePattern(code: string) {
        code = code.replace(/_([A-Z][A-Z0-9]+)_/g, ($0, $1) => {
            return this.regexes[$1];
        });
        return new RegExp(code, 'i');
    }

    getMonthByName(monthname: string) {
        return MONTHNAMES_LOOKUP[String(monthname).slice(0, 3).toLowerCase()];
    }

    getWeekdayByName(dayname: string) {
        return DAYNAMES_LOOKUP[String(dayname).slice(0, 3).toLowerCase()];
    }

    private parse(date: number|string) {
        // If the passed value is an integer, interpret it as ms past epoch
        if (!isNaN(Number(date))) {
            return new Date(date);
        }

        // trim the date
        date = String(date).replace(/^\s*(.*)\s*$/, '$1');
        // normalize whitespace
        date = date.replace(/\s{2,}/g, ' ');
        if (date === '') {
            return Date.now();
        }

        let i = 0;
        // try each of our patterns
        while(i < this.patterns.length) {
            const pattern = this.patterns[i];
            let callback;
            let regex: string| RegExp;
            if (typeof pattern[0] == 'string') {
                // pattern[0] is the name of the pattern
                regex = pattern[1] as RegExp;
                callback = pattern[2];
            } else {
                // backwards compatibility with version 3.1
                regex = pattern[0] as RegExp;
                callback = pattern[1];
            }
            const match = date.match(regex);
            if (!match) {
                i++;
                continue;
            }

            if (typeof callback == 'function') {
                const result = callback(match);
                if (result instanceof Date) {
                    return result;
                }
            } else {
                // fn is not a function but a string replace command
                const milliseconds  = Date.parse(date.replace(regex, callback as string));
                if (!isNaN(milliseconds)) {
                    return new Date(milliseconds);
                }
            }
            i++;
        }
        return NaN;
    }

    create(date?: Date | string | number) {
        // 0 arguments or date is undefined
        if (date == null) {
            return Date.now();
        }
        // If the passed value is already a date object, return it
        if (date instanceof Date) {
            return date;
        }

        return this.parse(date);
    }
}
