/* eslint-disable eqeqeq */
import { format, isValid, parseISO } from "date-fns";

import { UseDateFormat } from "../types";
import { DateConverter } from "./date-converter";

export function useDateFormat(): UseDateFormat {

    const dateConverter = new DateConverter();

    function formatTo(dateValue: string | Date, dateFormat: string): string {
        if (!dateValue) {
            return "";
        }

        if (typeof dateValue === "string" && dateValue.indexOf("0001") === 0) {
            return "";
        }

        if (dateValue instanceof Date) {
            return format(dateValue, dateFormat);
        }

        let dateObject: any = parseISO(dateValue);

        if (dateObject == "Invalid Date") {
            dateObject = dateConverter.create(dateValue) || new Date(dateValue);
        }

        // const d = parseISO(value);
        if (isValid(dateObject)) {
            dateObject = parseISO(format(dateObject, "yyyy-MM-dd HH:mm:ss"));
            return format(dateObject, dateFormat);
        }
        if (dateFormat.indexOf("HH") === 0 || dateFormat.indexOf("hh") === 0) {
            // 仅有时间部分
            // 提取时间
            const _time = dateValue.match(/\d*/g)?.filter((n) => n !== "").join(":");

            if (dateFormat === "HH" || dateFormat === "hh") {
                dateFormat += ":mm";
            }

            const fullDateTime = parseISO("2024-06-05 " + _time);
            return format(fullDateTime, dateFormat);
        }
        return "";

    }

    return {
        formatTo
    };
}
