import { TimeAgoDate, TimeAgoOptions, UseTimeAgoFormat } from "../types";
import { DateConverter } from "./date-converter";

const TEXTZH_CN = ['秒', '分钟', '小时', '天', '周', '个月', '年'];

const SECONDS_ARRAY = [
    60, // 60 seconds in 1 min
    60, // 60 mins in 1 hour
    24, // 24 hours in 1 day
    7, // 7 days in 1 week
    365 / 7 / 12, // 4.345238095238096 weeks in 1 month
    12, // 12 months in 1 year
];

export function useTimeAgo(): UseTimeAgoFormat {

    const dateConverter = new DateConverter();


    function timeAgoText(second: number, index: number): [string, string] {
        if (index === 0) {return ['刚刚', '片刻后'];}
        const unit = TEXTZH_CN[~~(index / 2)];
        return [`${second} ${unit}前`, `${second} ${unit}后`];
    }

    function toDate(date: TimeAgoDate): Date {
        if (typeof date == 'object') {
            return date;
        } else {
            const result = dateConverter.create(date);
            return typeof result == 'object' ? result : new Date();
        }

    }
    function secondDifference(date: TimeAgoDate, relativeDate: TimeAgoDate | undefined): number {
        const relDate = relativeDate ? toDate(relativeDate) : new Date();
        return (+relDate - +toDate(date)) / 1000;
    }


    function formatDifferent(second: number): string {
        const agoIn = second < 0 ? 1 : 0;
        second = Math.abs(second);
        // const totalSec = diff;
        let index = 0;
        for (; second >= SECONDS_ARRAY[index] && index < SECONDS_ARRAY.length; index++) {
            second /= SECONDS_ARRAY[index];
        }
        second = Math.floor(second);

        index *= 2;

        if (second > (index === 0 ? 9 : 1)) {index += 1;}

        return timeAgoText(second, index)[agoIn].replace('%s', second.toString());
    }
    function formatTo(date: TimeAgoDate, options?: TimeAgoOptions): string {
        // 计算差异秒
        const second = secondDifference(date, options && options.relativeDate);
        return formatDifferent(second);
    };
    return {
        formatTo
    };
}
