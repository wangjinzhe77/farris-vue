/* eslint-disable no-use-before-define */
export interface FormSchema {
    sourceUri: string;
    id: string;
    code: string;
    name: string;
    entities: FormSchemaEntity[];
    variables: FormSchemaEntityField[];
    eapiId: string;
    extendProperties: { enableStdTimeFormat: boolean };
    eapiCode?: string;
    eapiName?: string;
    eapiNameSpace?: string;
    voPath?: string;
    voNameSpace?: string;
}

/**
 * 实体
 */
export interface FormSchemaEntity {
    id: string;
    code: string;
    name: string;
    label: string;
    type: FormSchemaEntityType;
}

/**
 * 字段类型枚举
 */
export enum FormSchemaEntityField$Type {
    /**
     * 简单类型字段
     */
    SimpleField = "SimpleField",
    /**
     * 关联/UDT类型字段
     */
    ComplexField = "ComplexField"
}
/**
 * 字段编辑器对象
 */
export interface FormSchemaEntityFieldEditor {
    $type: string;
    [propName: string]: any;
}
export interface DesignViewModelField extends FormSchemaEntityField {
    valueChanging: string;
    valueChanged: string;
    groupId: string;
    groupName: string;
    isSchemaRemoved?: boolean;
    updateOn?: string;
}
/**
 * 字段
 */
export interface FormSchemaEntityField {
    $type: FormSchemaEntityField$Type;
    id: string;
    originalId: string;
    code: string;
    label: string;
    bindingField: string;
    name: string;
    defaultValue?: any;
    require?: boolean;
    readonly?: boolean;
    type: FormSchemaEntityFieldType;
    editor?: FormSchemaEntityFieldEditor;
    path?: string;
    bindingPath?: string;
    multiLanguage?: boolean;
    expression?: any;
}
/**
 * 字段类型对象中的类型枚举
 */
export enum FormSchemaEntityFieldType$Type {
    /**
     * 字符串
     */
    StringType = "StringType",
    /**
     * 备注
     */
    TextType = "TextType",
    /**
     * 数字（整数、浮点数）
     */
    NumericType = "NumericType",
    /**
     * 布尔
     */
    BooleanType = "BooleanType",
    /**
     * 日期
     */
    DateType = "DateType",
    /**
     * 日期时间
     */
    DateTimeType = "DateTimeType",
    /**
     * 枚举
     */
    EnumType = "EnumType",
    /**
     * 实体类
     */
    EntityType = "EntityType",
    /**
     * 分级码
     */
    HierarchyType = "HierarchyType",
    /**
     * 对象
     */
    ObjectType = "ObjectType",
    /**
     * 数字（大数据）
     */
    BigNumericType = "BigNumericType"
}
/**
 * 字段类型中的名称
 */
export enum FormSchemaEntityFieldTypeName {
    /**
     * 简单类型字段
     */
    String = "String",
    /**
     * 日期时间
     */
    DateTime = "DateTime",
    /**
     * 日期
     */
    Date = "Date",
    /**
     * 枚举
     */
    Enum = "Enum",
    /**
     * 布尔
     */
    Boolean = "Boolean",
    /**
     * 数字
     */
    Number = "Number",
    /**
     * 备注
     */
    Text = "Text",
    /**
     * 大数字
     */
    BigNumber = "BigNumber"
    /**
     * 人员
     */
}
/**
 * 枚举类型
 */
export interface EnumData {
    value: string;
    name: string;
}
/**
 * 实体
 */
export interface FormSchemaEntity {
    id: string;
    code: string;
    name: string;
    label: string;
    type: FormSchemaEntityType;
}
/**
 * 实体类型对象
 */
export interface FormSchemaEntityType {
    $type: string;
    name: string;
    primary: string;
    fields: FormSchemaEntityField[];
    entities?: FormSchemaEntity[];
    displayName?: string;
}
/**
 * 字段类型对象
 */
export interface FormSchemaEntityFieldType {
    $type: FormSchemaEntityFieldType$Type;
    name: FormSchemaEntityFieldTypeName | any;
    length?: number;
    precision?: number;
    valueType?: FormSchemaEntityFieldType;
    enumValues?: EnumData[];
    fields?: FormSchemaEntityField[];
    displayName?: string;
    primary?: string;
    entities?: FormSchemaEntity[];
    elementType?: GSPElementDataType;
    extendProperty?: any;
}
/**
 * 字段数据类型
 */
export enum GSPElementDataType {
    /**
     * 文本
     */
    String = "String",
    /**
     * 备注
     */
    Text = "Text",
    /**
     * 整数
     */
    Integer = "Integer",
    /**
     * 浮点数
     */
    Decimal = "Decimal",
    /**
     * 布尔型
     */
    Boolean = "Boolean",
    /**
     * 日期型
     */
    Date = "Date",
    /**
     * 日期时间型
     */
    DateTime = "DateTime",
    /**
     * 二进制
     */
    Binary = "Binary"
}
/**
 * dom Json ViewModel 节点中states实体
 */
export interface FormVariable {
    id: string;
    code: string;
    name: string;
    value?: any;
    type: string;
    category: string;
    fields?: any[];
    defaultValue?: any;
}
/**
 * 表单变量
 */
export interface DesignFormVariable extends FormVariable {
    /**
     * 分组ID
     */
    groupId: string;
    /**
     * 分组名称
     */
    groupName: string;
}
