import BigNumber from "bignumber.js";
import { NumberOption, NumberType, UseNumberFormat } from "../types";

export function useNumberFormat(): UseNumberFormat {
    /**
     * 获取最大值
     * @param n1 
     * @param n2 
     * @returns 
     */
    function max(firstValue: Array<NumberType> | NumberType, secondValue: NumberType | null = null): string {
        if (Array.isArray(firstValue)) {
            return BigNumber.max.apply(null, firstValue).toFixed();
        } else {
            if (firstValue && secondValue) {
                return BigNumber.maximum(firstValue, secondValue).toFixed();
            } else {
                console.error(`请提供比较的值firstValue、secondValue.`);
                return '';
            }
        }
    }

    /**
     * 最小值
     * @param firstValue 
     * @param secondValue 
     * @returns 
     */
    function min(firstValue: Array<NumberType> | NumberType, secondValue: NumberType | null = null): string {
        if (Array.isArray(firstValue)) {
            return BigNumber.min.apply(null, firstValue).toFixed();
        } else {
            if (firstValue && secondValue) {
                return BigNumber.minimum(firstValue, secondValue).toFixed();
            } else {
                console.error(`请提供比较的值firstValue、secondValue.`);
                return '';
            }
        }
    }

    /**
     * 求和
     * @param numberArray 
     * @returns 
     */
    function sum(numberArray: NumberType[]): string {
        return BigNumber.sum.apply(null, numberArray).toFixed();
    }

    /**
     * 平均数
     * @param total 
     * @param len 
     * @returns 
     */
    function average(total: NumberType, len: number): string {
        return new BigNumber(total).div(len).toFixed();
    }

    /**
     * 加法
     * @param firstValue 
     * @param secondValue 
     * @returns 
     */
    function plus(firstValue: NumberType, secondValue: NumberType): string {
        return new BigNumber(firstValue).plus(secondValue).toFixed();
    }

    /**
     * 乘法
     * @param firstValue 
     * @param secondValue 
     * @returns 
     */
    function multiplied(firstValue: NumberType, secondValue: NumberType): string {
        return new BigNumber(firstValue).times(secondValue).toFixed();
    }

    /**
     * 减法
     * @param firstValue 
     * @param secondValue 
     * @returns 
     */
    function minus(firstValue: NumberType, secondValue: NumberType): string {
        return new BigNumber(firstValue).minus(secondValue).toFixed();
    }

    /**
     * 是否相等
     * @param firstValue 
     * @param secondValue 
     * @returns 
     */
    function equal(firstValue: NumberType, secondValue: NumberType): boolean {
        return new BigNumber(firstValue).eq(secondValue);
    }
    /**
     * 是否小于
     * @param firstValue 
     * @param secondValue 
     * @returns 
     */
    function lessThan(firstValue: NumberType, secondValue: NumberType): boolean {
        return new BigNumber(firstValue).lt(secondValue);
    }
    /**
     * 是否大于
     * @param firstValue 
     * @param secondValue 
     * @returns 
     */
    function greaterThan(firstValue: NumberType, secondValue: NumberType): boolean {
        return new BigNumber(firstValue).gt(secondValue);
    }
    /**
     * 转成数字
     */
    /** */
    function toNumber(value: string): number {
        return new BigNumber(value).toNumber();
    }
    /**
     * 精度
     * @param value 
     * @param precision 
     * @returns 
     */
    function toFixed(value: string, precision: number = 0): string {
        return new BigNumber(value).toFixed(precision);
    }
    /**
     * 转换金额
     * @param money 
     * @returns 
     */
    function convertCurrency(money: string): string {
        // 汉字的数字
        const cnNums = ['零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖'];
        // 基本单位
        const cnIntRadice = ['', '拾', '佰', '仟'];
        // 对应整数部分扩展单位
        const cnIntUnits = ['', '万', '亿', '兆'];
        // 对应小数部分单位
        const cnDecUnits = ['角', '分', '毫', '厘'];
        // 整数金额时后面跟的字符
        const cnInteger = '整';
        // 整型完以后的单位
        const cnIntLast = '元';
        // 最大处理的数字
        // eslint-disable-next-line no-loss-of-precision
        const maxNum = 999999999999999.9999;
        // 金额整数部分
        let intPart;
        // 金额小数部分
        let decimalNum;
        // 输出的中文金额字符串
        let chineseStr = '';
        // 分离金额后用的数组，预定义
        let parts;
        if (money === '') { return ''; }
        const moneyNum = parseFloat(money);
        if (moneyNum >= maxNum) {
            // 超出最大处理数字
            return '';
        }
        if (moneyNum === 0) {
            chineseStr = cnNums[0] + cnIntLast + cnInteger;
            return chineseStr;
        }
        // 转换为字符串
        money = moneyNum.toString();
        if (money.indexOf('.') === -1) {
            intPart = money;
            decimalNum = '';
        } else {
            parts = money.split('.');
            intPart = parts[0];
            decimalNum = parts[1].substr(0, 4);
        }
        // 获取整型部分转换
        if (parseInt(intPart, 10) > 0) {
            let zeroCount = 0;
            const intPartLength = intPart.length;
            for (let i = 0; i < intPartLength; i++) {
                const intNumSub = intPart.substr(i, 1);
                const leftLength = intPartLength - i - 1;
                const divisionInt = leftLength / 4;
                const remainder = leftLength % 4;
                if (intNumSub === '0') {
                    zeroCount++;
                } else {
                    if (zeroCount > 0) {
                        chineseStr += cnNums[0];
                    }
                    // 归零
                    zeroCount = 0;
                    chineseStr += cnNums[parseInt(intNumSub, 10)] + cnIntRadice[remainder];
                }
                if (remainder === 0 && zeroCount < 4) {
                    chineseStr += cnIntUnits[divisionInt];
                }
            }
            chineseStr += cnIntLast;
        }
        // 小数部分
        if (decimalNum !== '') {
            const decLen = decimalNum.length;
            for (let i = 0; i < decLen; i++) {
                const n = decimalNum.substr(i, 1);
                if (n !== '0') {
                    chineseStr += cnNums[Number(n)] + cnDecUnits[i];
                }
            }
        }
        if (chineseStr === '') {
            chineseStr += cnNums[0] + cnIntLast + cnInteger;
        } else if (decimalNum === '') {
            chineseStr += cnInteger;
        }
        return chineseStr;
    }
    /**
     * 去除格式化
     * @param value 
     * @param options 
     * @returns 
     */
    function removeFormat(value: NumberType | null | undefined, options: NumberOption): string {
        let newValue = (value === null || value === undefined || typeof value == 'number' && isNaN(value)) ? '' : String(value);
        if (options) {
            // 去前缀
            if (options.prefix) {
                newValue = newValue.replace(new RegExp(options.prefix, 'g'), '');
            }

            // 去后缀
            if (options.suffix) {
                newValue = newValue.replace(new RegExp(options.suffix, 'g'), '');
            }

            // 去掉千分位分隔符
            newValue = newValue.replace(/\\,/g, '');
        }
        return newValue;
    }
    /**
     * 格式化数据
     * @param value 
     * @param options 
     * @returns 
     */
    function formatTo(value: NumberType, options: NumberOption): string {
        const bignum = new BigNumber(value);
        const fmt = {
            prefix: options.prefix || '',
            suffix: options.suffix || '',
            decimalSeparator: options.decimalSeparator || '.',
            groupSeparator: options.groupSeparator || '',
            groupSize: 3
        };

        if (bignum.isNaN()) {
            return '';
        }

        let precision = options.precision || 0;
        precision = Number(precision);

        return bignum.toFormat(precision, fmt);
    }
    return {
        formatTo, removeFormat, convertCurrency, toFixed, toNumber, greaterThan, lessThan, equal, minus, multiplied, plus, average, sum, min, max
    };
}
