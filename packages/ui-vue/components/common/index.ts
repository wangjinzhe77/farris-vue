import { App } from 'vue';
import areaResponseDirective from './directive/area-response';

export * from './types';
export * from './text-box/composition/use-text-box';
export * from './text-box/composition/use-text-box-design';
export * from './text-box/composition/use-clear';
export * from './date/date-converter';
export * from './date/use-date-format';
export * from './utils/use-guid';
export * from './number/use-number-format';
export * from './date/use-time-ago';
export * from './utils/use-request-animation';
export * from './utils/use-appearance';
export * from './utils/with-install';
export * from './utils/symbol-key';
export * from './radio-checkbox/use-check';
export * from './radio-checkbox/radio-checkbox.props';
export * from './utils/encrypt';
export * from './utils/resolve-field';
export * from './entity/entity-schema';

export default {
    install(app: App): void {
        app.directive('area-response', areaResponseDirective);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>,
        configResolverMap: Record<string, any>, resolverMap: Record<string, any>): void {
        componentMap['area-response'] = areaResponseDirective;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap['area-response'] = areaResponseDirective;
    }
};
