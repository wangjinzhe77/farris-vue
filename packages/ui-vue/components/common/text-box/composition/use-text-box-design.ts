/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, ref, Ref, SetupContext, watch } from 'vue';
import { TextBoxProps, UseTextBox } from '../../types';

export function useTextBoxDesign(
    props: TextBoxProps,
    context: SetupContext,
    modelValue: Ref<string>,
    displayText: Ref<string>
): UseTextBox {
    const disabled = ref(props.disabled);
    const focusStatus = ref(false);
    const inputType = ref('text');
    const showBorder = ref(props.showBorder);
    const textAlign = ref(props.textAlign);

    const canFocus = computed(() => props.editable || !props.readonly);
    const editable = computed(() => props.editable && !props.disabled && !props.readonly);
    const hasFocused = computed(() => !props.disabled && focusStatus.value);
    const isEmpty = computed(() => modelValue.value === '' || modelValue.value === null || modelValue.value === undefined);
    const placeholder = computed(() => (props.placeholder));
    const readonly = computed(() => props.readonly || !props.editable);
    const textBoxTitle = computed(() => (props.enableTitle ? modelValue.value : ''));

    const textBoxClass = computed(() => ({
        'form-control': true,
        // 'f-utils-fill': true,
        'text-left': textAlign.value === 'left',
        'text-center': textAlign.value === 'center',
        'text-right': textAlign.value === 'right',
    }));

    const inputGroupClass = computed(() => {
        const classObject = {
            'f-cmp-inputgroup': true,
            'input-group': true,
            'f-state-disabled': true,
            'f-state-editable': false,
            'f-state-readonly': true,
            // 'f-state-focus': hasFocused.value
        };
        // const customClassArray = (props.customClass || '').split(' ');
        // customClassArray.reduce<Record<string, unknown>>((classObject, classString) => {
        //     classObject[classString] = true;
        //     return classObject;
        // }, classObject);
        return classObject;
    });

    const inputGroupStyle = computed(() => {
        return !showBorder.value ? 'border-width : 0 ' : '';
    });

    function changeTextBoxValue(newValue: string, shouldEmitChangeEvent = true) {
    }


    return {
        changeTextBoxValue,
        disabled,
        displayText,
        editable,
        hasFocused,
        inputGroupClass,
        inputType,
        isEmpty,
        modelValue,
        readonly,
        // onBlur,
        // onClick,
        // onFocus,
        // onInput,
        // onKeydown,
        // onKeyup,
        // onMousedown,
        // onTextBoxValueChange,
        placeholder,
        textBoxClass,
        textBoxTitle,
        inputGroupStyle
    };
}
