import { Ref, SetupContext, WatchSource, computed, ref, watch } from "vue";
import { TextBoxProps, UseClear, UseTextBox } from "../../types";

export function useClear(
    props: TextBoxProps,
    context: SetupContext,
    useTextBoxComposition: UseTextBox
): UseClear {
    const hasShownClearButton = ref(false);
    const shouldShowClearButton = computed(() => props.enableClear && !props.readonly && !props.disabled);
    const { changeTextBoxValue, displayText, hasFocused, isEmpty } = useTextBoxComposition;

    function toggleClearIcon(isShow: boolean) {
        hasShownClearButton.value = isShow;
    }

    watch(displayText as WatchSource<string>, () => {
        if (hasFocused?.value) {
            toggleClearIcon(!!displayText?.value);
        } else {
            toggleClearIcon(false);
        }
    });

    const clearButtonClass = computed(() => ({
        'input-group-text': true,
        'input-group-clear': true
    }));

    const clearButtonStyle = computed(() => {
        const styleObject = {
            width: '24px',
            display: hasShownClearButton.value ? 'flex' : 'none'
        } as Record<string, any>;
        return styleObject;
    });

    /** 清空输入框中的值 */
    function onClearValue($event: MouseEvent) {
        $event.stopPropagation();
        if (shouldShowClearButton.value) {
            changeTextBoxValue('', true);
            toggleClearIcon(!hasShownClearButton.value);
            context.emit('clear');
        }
    }

    function onMouseEnter(event: MouseEvent) {
        if (shouldShowClearButton.value && !isEmpty.value) {
            toggleClearIcon(true);
        }
    }

    function onMouseLeave(event: MouseEvent) {
        if (shouldShowClearButton.value) {
            toggleClearIcon(false);
        }
    }

    return { clearButtonClass, clearButtonStyle, hasShownClearButton, onClearValue, onMouseEnter, onMouseLeave, shouldShowClearButton };
}
