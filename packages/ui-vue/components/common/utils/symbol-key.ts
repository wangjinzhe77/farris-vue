import { InjectionKey } from "vue";

export const CHECKBOX_CONTEXT: InjectionKey<{ [key: string]: any }> = Symbol('checkboxGroupContext');

/** 选择控制器 */
export const ControllerSchemaRepositorySymbol = Symbol('controller schema repository inject token');
