export const resolveField = (dataItem: any, field: Array<string> | string) => {
    if (!field) {
        // 空字符串或者undefined
        return '';
    }
    if (!dataItem) {
        return '';
    }
    const fieldList = typeof field === 'string' ? field.split('.') : field;
    const result = fieldList.reduce((total: any, next: string) => {
        return total[next];
    }, dataItem);
    return result;
};

export const setFieldValue = (newValue: any, dataItem: any, field: Array<string> | string) => {
    let fieldList: string[] = [];
    if (typeof field === 'string') {
        fieldList = [...field.split('.')];
    }
    if (Array.isArray(field)) {
        fieldList = field;
    }
    let value = dataItem;
    while (fieldList && fieldList.length > 1) {
        value = value[fieldList.shift()!];
    }
    value[fieldList.shift()!] = newValue;
};
