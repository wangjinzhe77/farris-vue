/**
 * 根据传递过来已有的类对象和自定义类名，构造新的样式对象
 * @param classObject 
 * @param customClass 
 * @returns 
 */
export function getCustomClass(classObject: Record<string, any>, customClass: string) {
    const customClassArray = customClass?.split(' ') || [];
    customClassArray.reduce((result: Record<string, boolean>, classString: string) => {
        if (classString) {
            result[classString] = true;
        }
        return result;
    }, classObject);
    return classObject;
}
/**
 * 根据传递过来已有的style对象和自定义style，构造新的style对象
 * @param styleObject 
 * @param customStyle 
 * @returns 
 */
export function getCustomStyle(styleObject: Record<string, any>, customStyle: string) {
    const styleArray = customStyle?.split(';') || [];
    styleArray.reduce((result: Record<string, any>, styleString: string) => {
        if (styleString) {
            const styles = styleString.split(':');
            result[styles[0]] = styles[1];
        }
        return result;
    }, styleObject);
    return styleObject;
}
