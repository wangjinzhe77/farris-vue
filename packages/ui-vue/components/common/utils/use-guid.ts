export function useGuid() {
    function guid() {
        const replaceCallback = (replaceChar: string) => {
            const randomValue = (Math.random() * 16) | 0;
            const result = replaceChar === 'x' ? randomValue : (randomValue & 0x3) | 0x8;
            return result.toString(16);
        };

        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, replaceCallback);
    }

    function uuid(uuidLength: number, radix?: number) {
        const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
        const uuid: string[] = [];
        let charIndex;
        radix = radix || chars.length;

        if (uuidLength) {
            // Compact form
            for (charIndex = 0; charIndex < uuidLength; charIndex++) {uuid[charIndex] = chars[0 | (Math.random() * radix)];}
        } else {
            // rfc4122, version 4 form
            let randomValue;

            // rfc4122 requires these characters
            uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
            uuid[14] = '4';

            // Fill in random data.  At i==19 set the high bits of clock sequence as
            // per rfc4122, sec. 4.1.5
            for (charIndex = 0; charIndex < 36; charIndex++) {
                if (!uuid[charIndex]) {
                    randomValue = 0 | (Math.random() * 16);
                    uuid[charIndex] = chars[charIndex === 19 ? (randomValue & 0x3) | 0x8 : randomValue];
                }
            }
        }

        return uuid.join('');
    }

    return { guid, uuid };
}
