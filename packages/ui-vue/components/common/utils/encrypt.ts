/*
 * Configurable variables. You may need to tweak these to be compatible with
 * the server-side, but the defaults work in most cases.
 */
const hexcase = 0; /* hex output format. 0 - lowercase; 1 - uppercase        */
const b64pad = ''; /* base-64 pad character. "=" for strict RFC compliance   */
const chrsz = 8; /* bits per input character. 8 - ASCII; 16 - Unicode      */

/*
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally
 * to work around bugs in some JS interpreters.
 */
function safeAdd(x, y) {
    const lsw = (x & 0xffff) + (y & 0xffff);
    const msw = (x >> 16) + (y >> 16) + (lsw >> 16);
    return (msw << 16) | (lsw & 0xffff);
}

/*
 * Bitwise rotate a 32-bit number to the left.
 */
function bitRol(num, cnt) {
    return (num << cnt) | (num >>> (32 - cnt));
}

/*
 * These functions implement the four basic operations the algorithm uses.
 */
function encryptCmn(q, a, b, x, s, t) {
    return safeAdd(bitRol(safeAdd(safeAdd(a, q), safeAdd(x, t)), s), b);
}

function encryptFf(a, b, c, d, x, s, t) {
    return encryptCmn((b & c) | (~b & d), a, b, x, s, t);
}

function encryptGg(a, b, c, d, x, s, t) {
    return encryptCmn((b & d) | (c & ~d), a, b, x, s, t);
}

function encryptHh(a, b, c, d, x, s, t) {
    return encryptCmn(b ^ c ^ d, a, b, x, s, t);
}

function encryptIi(a, b, c, d, x, s, t) {
    return encryptCmn(c ^ (b | ~d), a, b, x, s, t);
}

/*
 * Convert a string to an array of little-endian words
 * If chrsz is ASCII, characters >255 have their hi-byte silently ignored.
 */
function str2binl(str: string): any[] {
    const bin: any = [];
    const mask = (1 << chrsz) - 1;
    for (let i = 0; i < str.length * chrsz; i += chrsz) {
        bin[i >> 5] |= (str.charCodeAt(i / chrsz) & mask) << i % 32;
    }
    return bin;
}

/*
 * Convert an array of little-endian words to a string
 */
function binl2str(bin) {
    let str = '';
    const mask = (1 << chrsz) - 1;
    for (let i = 0; i < bin.length * 32; i += chrsz) {
        str += String.fromCharCode((bin[i >> 5] >>> i % 32) & mask);
    }
    return str;
}

/*
 * Convert an array of little-endian words to a hex string.
 */
function binl2hex(binarray) {
    // tslint:disable-next-line: variable-name
    const hexTab = hexcase ? '0123456789ABCDEF' : '0123456789abcdef';
    let str = '';
    for (let i = 0; i < binarray.length * 4; i++) {
        str +=
        hexTab.charAt((binarray[i >> 2] >> ((i % 4) * 8 + 4)) & 0xf) +
        hexTab.charAt((binarray[i >> 2] >> ((i % 4) * 8)) & 0xf);
    }
    return str;
}

/*
 * Convert an array of little-endian words to a base-64 string
 */
function binl2b64(binarray) {
    const tab = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
    let str = '';
    for (let i = 0; i < binarray.length * 4; i += 3) {
        const triplet =
            (((binarray[i >> 2] >> (8 * (i % 4))) & 0xff) << 16) |
            (((binarray[(i + 1) >> 2] >> (8 * ((i + 1) % 4))) & 0xff) << 8) |
            ((binarray[(i + 2) >> 2] >> (8 * ((i + 2) % 4))) & 0xff);
        for (let j = 0; j < 4; j++) {
            if (i * 8 + j * 6 > binarray.length * 32) {
                str += b64pad;
            } else {
                str += tab.charAt((triplet >> (6 * (3 - j))) & 0x3f);
            }
        }
    }
    return str;
}

/*
 * Calculate the encrypt of an array of little-endian words, and a bit length
 */
function coreEncrypt(x: any, len: number) {
    /* append padding */
    x[len >> 5] |= 0x80 << len % 32;
    x[(((len + 64) >>> 9) << 4) + 14] = len;

    let a = 1732584193;
    let b = -271733879;
    let c = -1732584194;
    let d = 271733878;

    for (let i = 0; i < x.length; i += 16) {
        const olda = a;
        const oldb = b;
        const oldc = c;
        const oldd = d;

        a = encryptFf(a, b, c, d, x[i + 0], 7, -680876936);
        d = encryptFf(d, a, b, c, x[i + 1], 12, -389564586);
        c = encryptFf(c, d, a, b, x[i + 2], 17, 606105819);
        b = encryptFf(b, c, d, a, x[i + 3], 22, -1044525330);
        a = encryptFf(a, b, c, d, x[i + 4], 7, -176418897);
        d = encryptFf(d, a, b, c, x[i + 5], 12, 1200080426);
        c = encryptFf(c, d, a, b, x[i + 6], 17, -1473231341);
        b = encryptFf(b, c, d, a, x[i + 7], 22, -45705983);
        a = encryptFf(a, b, c, d, x[i + 8], 7, 1770035416);
        d = encryptFf(d, a, b, c, x[i + 9], 12, -1958414417);
        c = encryptFf(c, d, a, b, x[i + 10], 17, -42063);
        b = encryptFf(b, c, d, a, x[i + 11], 22, -1990404162);
        a = encryptFf(a, b, c, d, x[i + 12], 7, 1804603682);
        d = encryptFf(d, a, b, c, x[i + 13], 12, -40341101);
        c = encryptFf(c, d, a, b, x[i + 14], 17, -1502002290);
        b = encryptFf(b, c, d, a, x[i + 15], 22, 1236535329);

        a = encryptGg(a, b, c, d, x[i + 1], 5, -165796510);
        d = encryptGg(d, a, b, c, x[i + 6], 9, -1069501632);
        c = encryptGg(c, d, a, b, x[i + 11], 14, 643717713);
        b = encryptGg(b, c, d, a, x[i + 0], 20, -373897302);
        a = encryptGg(a, b, c, d, x[i + 5], 5, -701558691);
        d = encryptGg(d, a, b, c, x[i + 10], 9, 38016083);
        c = encryptGg(c, d, a, b, x[i + 15], 14, -660478335);
        b = encryptGg(b, c, d, a, x[i + 4], 20, -405537848);
        a = encryptGg(a, b, c, d, x[i + 9], 5, 568446438);
        d = encryptGg(d, a, b, c, x[i + 14], 9, -1019803690);
        c = encryptGg(c, d, a, b, x[i + 3], 14, -187363961);
        b = encryptGg(b, c, d, a, x[i + 8], 20, 1163531501);
        a = encryptGg(a, b, c, d, x[i + 13], 5, -1444681467);
        d = encryptGg(d, a, b, c, x[i + 2], 9, -51403784);
        c = encryptGg(c, d, a, b, x[i + 7], 14, 1735328473);
        b = encryptGg(b, c, d, a, x[i + 12], 20, -1926607734);

        a = encryptHh(a, b, c, d, x[i + 5], 4, -378558);
        d = encryptHh(d, a, b, c, x[i + 8], 11, -2022574463);
        c = encryptHh(c, d, a, b, x[i + 11], 16, 1839030562);
        b = encryptHh(b, c, d, a, x[i + 14], 23, -35309556);
        a = encryptHh(a, b, c, d, x[i + 1], 4, -1530992060);
        d = encryptHh(d, a, b, c, x[i + 4], 11, 1272893353);
        c = encryptHh(c, d, a, b, x[i + 7], 16, -155497632);
        b = encryptHh(b, c, d, a, x[i + 10], 23, -1094730640);
        a = encryptHh(a, b, c, d, x[i + 13], 4, 681279174);
        d = encryptHh(d, a, b, c, x[i + 0], 11, -358537222);
        c = encryptHh(c, d, a, b, x[i + 3], 16, -722521979);
        b = encryptHh(b, c, d, a, x[i + 6], 23, 76029189);
        a = encryptHh(a, b, c, d, x[i + 9], 4, -640364487);
        d = encryptHh(d, a, b, c, x[i + 12], 11, -421815835);
        c = encryptHh(c, d, a, b, x[i + 15], 16, 530742520);
        b = encryptHh(b, c, d, a, x[i + 2], 23, -995338651);

        a = encryptIi(a, b, c, d, x[i + 0], 6, -198630844);
        d = encryptIi(d, a, b, c, x[i + 7], 10, 1126891415);
        c = encryptIi(c, d, a, b, x[i + 14], 15, -1416354905);
        b = encryptIi(b, c, d, a, x[i + 5], 21, -57434055);
        a = encryptIi(a, b, c, d, x[i + 12], 6, 1700485571);
        d = encryptIi(d, a, b, c, x[i + 3], 10, -1894986606);
        c = encryptIi(c, d, a, b, x[i + 10], 15, -1051523);
        b = encryptIi(b, c, d, a, x[i + 1], 21, -2054922799);
        a = encryptIi(a, b, c, d, x[i + 8], 6, 1873313359);
        d = encryptIi(d, a, b, c, x[i + 15], 10, -30611744);
        c = encryptIi(c, d, a, b, x[i + 6], 15, -1560198380);
        b = encryptIi(b, c, d, a, x[i + 13], 21, 1309151649);
        a = encryptIi(a, b, c, d, x[i + 4], 6, -145523070);
        d = encryptIi(d, a, b, c, x[i + 11], 10, -1120210379);
        c = encryptIi(c, d, a, b, x[i + 2], 15, 718787259);
        b = encryptIi(b, c, d, a, x[i + 9], 21, -343485551);

        a = safeAdd(a, olda);
        b = safeAdd(b, oldb);
        c = safeAdd(c, oldc);
        d = safeAdd(d, oldd);
    }
    return [a, b, c, d];
}

/*
 * Calculate the HMAC-encrypt, of a key and some data
 */
function coreHmacEncrypt(key, data) {
    let bkey = str2binl(key);
    if (bkey.length > 16) {
        bkey = coreEncrypt(bkey, key.length * chrsz);
    }

    const ipad = Array(16);
    const opad = Array(16);
    for (let i = 0; i < 16; i++) {
        ipad[i] = bkey[i] ^ 0x36363636;
        opad[i] = bkey[i] ^ 0x5c5c5c5c;
    }

    const hash = coreEncrypt(ipad.concat(str2binl(data)), 512 + data.length * chrsz);
    return coreEncrypt(opad.concat(hash), 512 + 128);
}

export function encrypt(s: string, type = 'hex') {
    if (type === 'hex') {
        return binl2hex(coreEncrypt(str2binl(s), s.length * chrsz));
    }

    if (type === 'b64') {
        return binl2b64(coreEncrypt(str2binl(s), s.length * chrsz));
    }

    return binl2str(coreEncrypt(str2binl(s), s.length * chrsz));
}

export function hmacEncrypt(key, data, type = 'hex') {
    if (type === 'hex') {
        return binl2hex(coreHmacEncrypt(key, data));
    }
    if (type === 'b64') {
        return binl2b64(coreHmacEncrypt(key, data));
    }
    return binl2str(coreHmacEncrypt(key, data));
}
