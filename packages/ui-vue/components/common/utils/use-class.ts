interface ElementRef {
    nativeElement?: any;
}
export function getElementWithClassList(elementRef: ElementRef) {
    const element = elementRef.nativeElement ? elementRef.nativeElement : elementRef;

    if (element.classList !== undefined && element.classList !== null) {
        return element;
    }

    return null;
}

export function removeClass(elementRef: ElementRef | any, className: string) {
    if (className === undefined) {
        return;
    }
    const element = getElementWithClassList(elementRef);

    if (element) {
        element.classList.remove(className);
    }
}
export function addClass(elementRef: ElementRef | any, className: string) {
    if (className === undefined) {
        return;
    }
    const element = getElementWithClassList(elementRef);

    if (element) {
        element.classList.add(className);
    }
}
