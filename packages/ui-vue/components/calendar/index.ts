 
import type { App, Plugin } from 'vue';
import FCalendar from './src/calendar.component';
import FCalendarDesign from './src/designer/calendar.design.component';
import FCalendarHeader from './src/components/header/header.component';
import FCalendarDayView from './src/components/day/day-view.component';
import FCalendarMonthView from './src/components/month/month-view.component';
import FCalendarWeekView from './src/components/week/week-view.component';
import { propsResolver } from './src/calendar.props';

export * from './src/calendar.props';
export * from './src/components/day/day-view.props';
export * from './src/components/header/header.props';
export * from './src/components/month/month-view.props';
export * from './src/components/week/week-view.props';

export * from './src/types/calendar';
export * from './src/types/common';
export * from './src/types/month';
export * from './src/types/month-view';
export * from './src/types/schedule';

export { FCalendar, FCalendarDayView, FCalendarHeader, FCalendarMonthView, FCalendarWeekView };

FCalendar.install = (app: App) => {
    app.component(FCalendar.name as string, FCalendar)
        .component(FCalendarDayView.name as string, FCalendarDayView)
        .component(FCalendarHeader.name as string, FCalendarHeader)
        .component(FCalendarMonthView.name as string, FCalendarMonthView)
        .component(FCalendarWeekView.name as string, FCalendarWeekView);
};
FCalendar.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap.calendar = FCalendar;
    propsResolverMap.calendar = propsResolver;
};
FCalendar.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.calendar = FCalendarDesign;
    propsResolverMap.calendar = propsResolver;
};

export default FCalendar as typeof FCalendar & Plugin;
