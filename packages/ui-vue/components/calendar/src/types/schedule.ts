import { DateObject, EventItem, MarkStatus, MonthTag } from './common';

export type TimePartInSchedule = 'upper' | 'lower';

/**
 * 日程表预定事项
 */
export interface ScheduleEvent {
    /** 开始时间 */
    starts: DateObject;
    /** 结束时间 */
    ends: DateObject;
    /** 事项描述 */
    title: string;
}

/**
 * 日程表项
 */
export interface ItemInSchedule {
    /** 日期 */
    day: DateObject;
    /** 预定事项 */
    events: ScheduleEvent[];
}

/**
 * 日程表时刻
 */
export interface TimeInSchedule {
    /** 时刻 */
    time: DateObject;
    /** 此时刻的预定事件 */
    events: Array<ItemInSchedule>;
    /** 标题 */
    title: string;
    /** 时刻分布，前半小时或者后半小时 */
    part: TimePartInSchedule;
}
