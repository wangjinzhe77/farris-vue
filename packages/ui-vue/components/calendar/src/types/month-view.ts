import { DateObject, EventItem, MarkStatus, MonthTag } from './common';

export interface CalenderMonthViewDayItem {
    date: DateObject;
    events?: EventItem[];
    isCurrent?: boolean;
    tag: MonthTag;
}

export interface CalendarMonthViewWeekItem {
    days: Array<CalenderMonthViewDayItem>;
    numberInTheYear: number;
    year: number;
}

export type CalendarItemPart = 'upper' | 'lower';

export interface CalendarWeekViewItemCell {
    date: DateObject;
    events: EventItem[];
}

export interface CalendarWeekViewItem {
    time: DateObject;
    cells: Array<CalendarWeekViewItemCell>;
    title: string;
    part: CalendarItemPart;
}
