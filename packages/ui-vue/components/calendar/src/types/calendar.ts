import { DateObject, MonthTag } from './common';
import { ScheduleEvent } from './schedule';

export interface DayInCalendar {
    date: DateObject;
    events?: ScheduleEvent[];
    isCurrent?: boolean;
    monthTag: MonthTag;
}

export interface WeekInCalendar {
    days: Array<DayInCalendar>;
    weekNumber: number;
    year: number;
}
