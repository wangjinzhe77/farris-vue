 
import { ExtractPropTypes } from 'vue';
import { ScheduleEvent } from './types/schedule';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import calendarSchema from './schema/calendar.schema.json';
import propertyConfig from './property-config/calendar.property-config.json';

export const calendarProps = {
    events: { Type: Array<ScheduleEvent>, default: [] },
    firstDayOfTheWeek: { type: String, default: 'Sun.' }
} as Record<string, any>;

export type CalendarPropsType = ExtractPropTypes<typeof calendarProps>;

export const propsResolver = createPropsResolver<CalendarPropsType>(calendarProps, calendarSchema, schemaMapper, schemaResolver, propertyConfig);
