import { DateObject, weekDays } from "../types/common";
import { UseDate } from "./types";

export function useDate(): UseDate {

    function emptyDate(): DateObject {
        return { year: 0, month: 0, day: 0 };
    }

    function getDate(date: DateObject): Date {
        return new Date(
            date.year || 0,
            date.month ? date.month - 1 : 0,
            date.day || 0,
            date.hour ? date.hour : 0,
            date.minute ? date.minute : 0,
            date.second ? date.second : 0,
            0
        );
    }

    function getDate2(date: DateObject): Date {
        const now = new Date();

        if (!date.year) {
            date.year = now.getFullYear();
        }

        if (!date.month) {
            date.month = now.getMonth() + 1;
        }

        if (!date.day) {
            date.day = 1;
        }

        return new Date(
            date.year,
            date.month - 1,
            date.day,
            date.hour ? date.hour : 0,
            date.minute ? date.minute : 0,
            date.second ? date.second : 0,
            0
        );
    }

    function getDayNumber(date: DateObject): number {
        return new Date(
            date.year || 1,
            (date.month || 1) - 1,
            date.day,
            date.hour ? date.hour : 0,
            date.minute ? date.minute : 0,
            date.second ? date.second : 0,
            0
        ).getDay();
    }

    function getWeekdayIndex(wd: string): number {
        return weekDays.indexOf(wd);
    }

    function getTimeInMilliseconds(date: DateObject): number {
        return getDate(date).getTime();
    }

    function getEpocTime(date: DateObject): number {
        return Math.round(getTimeInMilliseconds(date) / 1000.0);
    }

    function getNearDate(now: DateObject, min: DateObject, max: DateObject): DateObject {
        const minMilliseconds = getTimeInMilliseconds(min);
        const maxMilliseconds = getTimeInMilliseconds(max);
        const nowMilliseconds = getTimeInMilliseconds(now);
        if (maxMilliseconds - nowMilliseconds > nowMilliseconds - minMilliseconds) {
            return min;
        }
        return max;
    }

    function getToday(): DateObject {
        const date: Date = new Date();
        return {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate(),
            hour: date.getHours(),
            minute: date.getMinutes(),
            second: date.getSeconds()
        };
    }

    return { emptyDate, getDate, getDate2, getDayNumber, getEpocTime, getNearDate, getWeekdayIndex, getTimeInMilliseconds, getToday };
}
