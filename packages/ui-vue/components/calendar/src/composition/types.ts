import { WeekInCalendar } from '../types/calendar';
import { DateObject, Period } from '../types/common';
import { NameOfMonths } from '../types/month';
import { CalendarMonthViewWeekItem } from '../types/month-view';

export interface DateFormatInfo {
    value: string;
    format: string;
}

export interface UseCompare {
    isDateEarlier: (firstDate: DateObject, secondDate: DateObject) => boolean;

    equal: (firstDate: DateObject, secondDate: DateObject) => boolean;

    inPeriod: (date: DateObject, period: Period | null) => boolean;

    isPoint: (period: Period, date: DateObject) => boolean;

    equalOrEarlier: (firstDate: DateObject, secondDate: DateObject) => boolean;

    isInitializedDate: (date: DateObject) => boolean;

    isInitializedMonth: (date: DateObject) => boolean;

    isInitializedYear: (date: DateObject) => boolean;

    isMonthDisabledByDisableSince: (date: DateObject, disableSince: DateObject) => boolean;

    isMonthDisabledByDisableUntil: (date: DateObject, disableUntil: DateObject) => boolean;

    sameDay: (firstDate: DateObject, secondDate: DateObject) => boolean;

    sameTime: (firstDate: DateObject, secondDate: DateObject) => boolean;
}

export interface UseDate {
    emptyDate: () => DateObject;

    getDate: (date: DateObject) => Date;

    getDate2: (date: DateObject) => Date;

    getDayNumber: (date: DateObject) => number;

    getWeekdayIndex: (wd: string) => number;

    getTimeInMilliseconds: (date: DateObject) => number;

    getEpocTime: (date: DateObject) => number;

    getNearDate: (now: DateObject, min: DateObject, max: DateObject) => DateObject;

    getToday(): DateObject;
}

export interface UseEvent {
    getKeyCodeFromEvent: (event: KeyboardEvent) => number;
}

export interface UseMonth {
    daysInMonth: (month: number, year: number) => number;

    daysInPreMonth: (month: number, year: number) => number;

    getNextMonth: (month: number, year: number) => DateObject;

    getPreviousMonth: (month: number, year: number) => DateObject;
}

export interface UseCalendar {
    getMonthlyCalendar: (month: number, year: number, firstDayOfWeek: string) => WeekInCalendar[];

    getWeeklyCalendar: (day: number, month: number, year: number, firstDayOfWeek: string) => WeekInCalendar;

    getPreviousDay: (date: DateObject) => DateObject;

    getNextDay: (date: DateObject) => DateObject;

    getDayInPreviousWeek: (date: DateObject) => DateObject;

    getDayInNextWeek: (date: DateObject) => DateObject;

    getDayInPreviousMonth: (date: DateObject) => DateObject;

    getDayInNextMonth: (date: DateObject) => DateObject;
}

export interface UseNumber {
    getDayNumber: (date: DateObject) => number;

    getNumberByValue: (df: DateFormatInfo) => number;

    getMonthNumberByMonthName: (df: DateFormatInfo, monthLabels: NameOfMonths) => number;

    getWeekNumber: (date: DateObject) => number;
}
