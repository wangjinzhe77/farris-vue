import { defineComponent, onMounted, ref, watch } from 'vue';
import { WeekViewPropsType, weekViewProps } from './week-view.props';
import { useCompare } from '../../composition/use-compare';
import { DateObject, EventItem } from '../../types/common';
import { ItemInSchedule, ScheduleEvent, TimeInSchedule } from '../../types/schedule';
import { DayInCalendar, WeekInCalendar } from '../../types/calendar';

export default defineComponent({
    name: 'FCalendarWeekView',
    props: weekViewProps,
    emits: [],
    setup(props: WeekViewPropsType) {
        const weekViewContentRef = ref<any>();
        const primaryWeekViewContentRef = ref<any>();
        const daysInWeek = ref<string[]>(props.daysInWeek);
        const week = ref<WeekInCalendar>(props.week);
        const enableMarkCurrent = ref(props.enableMarkCurrent);
        const events = ref<ScheduleEvent[]>(props.events);

        const items = ref<TimeInSchedule[]>([]);

        const { equal } = useCompare();

        function buildWeekViewItemCell(time: DateObject) {
            const cells = week.value.days.map((day: DayInCalendar) => {
                const cellDate: DateObject = {
                    year: day.date.year,
                    month: day.date.month,
                    day: day.date.day,
                    hour: time.hour,
                    minute: time.minute,
                    second: time.second
                };
                const cellEvents = events.value.filter((eventItem: ScheduleEvent) => equal(eventItem.starts, cellDate)) as ScheduleEvent[];
                return {
                    day: cellDate,
                    events: cellEvents
                };
            });
            return cells;
        }

        function loadCalendarWeekViewItems() {
            const result: TimeInSchedule[] = [];
            for (let i = 0; i < 24; i++) {
                const oClock = { hour: i, minute: 0, second: 0 };
                const halfPast = { hour: i, minute: 30, second: 0 };
                const oClockCells = buildWeekViewItemCell(oClock);
                const halfPastCells = buildWeekViewItemCell(halfPast);
                result.push({
                    time: oClock,
                    events: oClockCells,
                    title: i > 0 ? `${i}:00` : '',
                    part: 'upper'
                });
                result.push({
                    time: halfPast,
                    events: halfPastCells,
                    title: `${i}:30`,
                    part: 'lower'
                });
            }
            items.value = result;
        }

        loadCalendarWeekViewItems();
        watch(
            () => props.week,
            () => {
                week.value = props.week;
                loadCalendarWeekViewItems();
            }
        );
        watch(
            () => props.events,
            () => {
                events.value = props.events;
                loadCalendarWeekViewItems();
            }
        );

        const dayClass = (currentDay: DayInCalendar) => {
            const shouldMarkCurrentDay = currentDay.isCurrent && enableMarkCurrent.value;
            const classObject = {
                'f-calendar-week-view-date': true,
                'f-calendar-week-view-current': shouldMarkCurrentDay
            } as Record<string, boolean>;
            return classObject;
        };

        function renderHeader() {
            return (
                <div class="f-calendar-week-view-header">
                    <div class="f-calendar-week-view-header-corner"></div>
                    <div class="f-calendar-week-view-header-primary">
                        <div class="f-calendar-week-view-header-columns">
                            {daysInWeek.value.map((day: string, index: number) => {
                                return (
                                    <div class="f-calendar-week-view-header-cell">
                                        <div class={dayClass(week.value.days[index])}>{week.value.days[index].date.day}</div>
                                        <span>{day}</span>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </div>
            );
        }

        function renderSidebar() {
            return (
                <div class="f-calendar-week-view-content-side">
                    <div class="f-calendar-side">
                        {items.value.map((weekViewItem: TimeInSchedule) => (
                            <div class="f-calendar-side-row">
                                <div class="f-calendar-side-row-number">{weekViewItem.part === 'upper' ? weekViewItem.title : ''}</div>
                            </div>
                        ))}
                    </div>
                </div>
            );
        }

        function weekItemClass(weekViewItem: TimeInSchedule) {
            const classObject = {
                'f-calendar-week-view-item': true,
                'f-calendar-week-view-item-upper': weekViewItem.part === 'upper',
                'f-calendar-week-view-item-lower': weekViewItem.part === 'lower'
            } as Record<string, boolean>;
            return classObject;
        }

        function eventItemStyle(eventItem: EventItem) {
            const height = 44;
            const marginBottom = 2;
            const marginTop = 2;
            const borderBottom = 1;
            const borderTop = 1;
            const start = (eventItem.starts.hour || 0) + (eventItem.starts.minute || 0) / 60;
            const end = (eventItem.ends.hour || 0) + (eventItem.ends.minute || 0) / 60;
            const actualHeight = height * (end - start) - marginTop - marginBottom - borderTop - borderBottom;
            const styleObject = {
                height: `${actualHeight}px`
            } as Record<string, any>;
            return styleObject;
        }

        function renderGridData() {
            return items.value.map((weekViewItem: TimeInSchedule) => {
                return (
                    <div class={weekItemClass(weekViewItem)}>
                        {weekViewItem.events.map((cell: ItemInSchedule) => {
                            return (
                                <div class="f-calendar-week-view-item-cell">
                                    {cell.events.length > 0 &&
                                        cell.events.map((eventItem: ScheduleEvent) => {
                                            return (
                                                <div class="f-calendar-event" style={eventItemStyle(eventItem)}>
                                                    {eventItem.title}
                                                </div>
                                            );
                                        })}
                                </div>
                            );
                        })}
                    </div>
                );
            });
        }

        function renderDataArea() {
            return (
                <div ref={primaryWeekViewContentRef} class="f-calendar-content-primary">
                    <div class="f-calendar-content-data">{renderGridData()}</div>
                </div>
            );
        }

        onMounted(() => {
            if (weekViewContentRef.value) {
                weekViewContentRef.value.scrollTo(0, 250);
            }
        });

        return () => {
            return (
                <div class="f-calendar-week-view">
                    {renderHeader()}
                    <div class="f-calendar-week-view-content" ref={weekViewContentRef}>
                        {renderSidebar()}
                        {renderDataArea()}
                    </div>
                </div>
            );
        };
    }
});
