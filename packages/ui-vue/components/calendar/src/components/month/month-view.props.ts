import { ExtractPropTypes } from 'vue';
import { weekDays } from '../../types/common';
import { WeekInCalendar } from '../../types/calendar';
import { ScheduleEvent } from '../../types/schedule';

export const monthViewProps = {
    dates: { Type: Array<WeekInCalendar>, default: [] },
    daysInWeek: { Type: Array<string>, default: weekDays },
    enableKeyboadNavigate: { Type: Boolean, default: true },
    enableMarkCurrent: { Type: Boolean, default: true },
    events: { Type: Array<ScheduleEvent>, default: [] },
    activeDay: { Type: Object, default: null }
};

export type MonthViewProps = ExtractPropTypes<typeof monthViewProps>;
