import { mount } from '@vue/test-utils';
import { FButton } from '..';

describe('f-button', () => {
    const mocks = {};

    beforeAll(() => { });

    describe('properties', () => {
        test('it should been disabled', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton disabled={true}>it should be work</FButton>;
                    };
                }
            });
            expect(wrapper.find('button').element.hasAttribute('disabled')).toBeTruthy();
        });
    });

    describe('render', () => {
        test('it should work', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton>it should be work</FButton>;
                    };
                }
            });
            expect(wrapper.find('button').exists()).toBeTruthy();
            expect(wrapper.find('button').text()).toEqual('it should be work');
            expect(wrapper.find('button').element.hasAttribute('disabled')).toBeFalsy();
        });
        test('it should render primary button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton>primary button</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-primary')).toBeTruthy();
        });
        test('it should render danger button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton type="danger">danger button</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-danger')).toBeTruthy();
        });
        test('it should render success button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton type="success">danger button</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-success')).toBeTruthy();
        });
        test('it should render warning button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton type="warning">danger button</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-warning')).toBeTruthy();
        });
        test('it should render secondary button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton type="secondary">danger button</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-secondary')).toBeTruthy();
        });
        test('it should render link button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton type="link">danger button</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-link')).toBeTruthy();
        });
        test('it should render small button be default', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton>default size</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-sm')).toBeTruthy();
        });
        test('it should render large button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton size="large">large size</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-lg')).toBeTruthy();
        });
        test('it should render small button', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton size="small">small size</FButton>;
                    };
                }
            });
            expect(wrapper.classes().includes('btn')).toBeTruthy();
            expect(wrapper.classes().includes('btn-sm')).toBeTruthy();
        });
        test('it should show icon', () => {
            const component = mount({
                setup(props, ctx) {
                    return () => {
                        return <FButton icon="f-icon f-icon-add"></FButton>;
                    };
                }
            });
            expect(component.find('i')).toBeTruthy();
            expect(component.find('i').classes().includes('f-icon')).toBeTruthy();
            expect(component.find('i').classes().includes('f-icon-add')).toBeTruthy();
        });
    });

    describe('methods', () => { });

    describe('events', () => {
        test('it should be clicked', () => {
            const onClick = jest.fn();
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return (
                            <FButton onClick={onClick}> small size </FButton>
                        );
                    };
                }
            });
            wrapper.find('button').trigger('click');
            expect(onClick).toBeCalled();
        });
        test('it should not be clicked', () => {
            const onClick = jest.fn();
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return (
                            <FButton disabled={true} onClick={onClick}>
                                small size
                            </FButton>
                        );
                    };
                }
            });
            wrapper.find('button').trigger('click');
            expect(onClick).toBeCalledTimes(0);
        });
    });

    describe('behaviors', () => { });
});
