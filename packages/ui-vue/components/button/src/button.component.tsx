/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, watch } from 'vue';
import type { SetupContext } from 'vue';
import { buttonProps, ButtonProps } from './button.props';
import { useButton } from './composition/use-button';
import { useIcon } from './composition/use-icon';

export default defineComponent({
    name: 'FButton',
    props: buttonProps,
    emits: ['click'] as (string[] & ThisType<void>) | undefined,
    setup(props: ButtonProps, context: SetupContext) {
        const { onClickButton } = useButton(props, context);
        const { iconClass, shouldShowIcon } = useIcon(props);
        const buttonClass = computed(() => {
            const classObject = {
                btn: true,
                'btn-lg': props.size === 'large',
                'btn-md': props.size !== 'large' && props.size !== 'small',
                'btn-sm': props.size === 'small',
                'btn-icontext': shouldShowIcon.value
            } as Record<string, any>;
            classObject[`btn-${props.type}`] = true;
            if (props.customClass) {
                Object.keys(props.customClass).reduce<Record<string, boolean>>((result: Record<string, boolean>, className: string) => {
                    result[className] = props.customClass[className];
                    return result;
                }, classObject);
            }
            return classObject;
        });

        return () => (
            <button class={buttonClass.value} disabled={props.disabled}
                onClick={(event: MouseEvent) => onClickButton(event)}>
                {shouldShowIcon.value && <i class={iconClass.value}></i>}
                {context.slots.default && context.slots.default()}
            </button>
        );
    }
});
