import { computed } from 'vue';
import { ButtonProps } from '../button.props';
import { UseIcon } from './types';

export function useIcon(props: ButtonProps): UseIcon {

    const iconClass = computed(() => {
        const classObject = {
            'f-icon': true
        } as Record<string, boolean>;
        if (props.icon) {
            const classNames = props.icon.trim().split(' ');
            if (classNames && classNames.length) {
                classNames.reduce((result: Record<string, boolean>, className: string) => {
                    result[className] = true;
                    return result;
                }, classObject);
            }
        }
        return classObject;
    });

    const shouldShowIcon = computed(() => {
        return !!(props.icon && props.icon.trim());
    });

    return { iconClass, shouldShowIcon };
}
