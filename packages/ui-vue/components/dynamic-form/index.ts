 
import FResponseForm from './src/response-form.component';
import FResponseFormDesign from './src/designer/response-form.design.component';
import FDynamicFormGroup from './src/component/dynamic-form-group/dynamic-form-group.component';
import FDynamicFromGroupDesign from './src/designer/form-group.design.component';
import FDynamicFormInput from './src/component/dynamic-form-input/dynamic-form-input.component';
import { editorResolver, formGroupPropsResolver } from './src/component/dynamic-form-group/dynamic-form-group.props';
import { formInputPropsResolver } from './src/component/dynamic-form-input/dynamic-form-input.props';
import { formPropsResolver } from './src/response-form.props';
import { responseFormLayoutPropsResolver } from './src/designer/response-form-layout-setting.props';
import { App } from 'vue';
import FResponseFormLayoutSetting from './src/designer/response-form-layout-setting.component';

export * from './src/types';
export * from './src/composition/types';
export * from './src/response-form.props';
export * from './src/component/dynamic-form-group/dynamic-form-group.props';
export * from './src/designer/response-form-use-designer-rules';

export { FResponseForm, FDynamicFormGroup, FDynamicFormInput };

export default {
    install(app: App): void {
        app.component(FResponseForm.name as string, FResponseForm)
            .component(FDynamicFormGroup.name as string, FDynamicFormGroup);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>): void {
        componentMap['form-group'] = FDynamicFormGroup;
        propsResolverMap['form-group'] = formGroupPropsResolver;
        componentMap['response-form'] = FResponseForm;
        propsResolverMap['response-form'] = formPropsResolver;
        componentMap['form-input'] = FDynamicFormInput;
        propsResolverMap['form-input'] = formInputPropsResolver;
        resolverMap['form-group'] = { editorResolver };
        componentMap['response-form-layout-setting'] =FResponseFormLayoutSetting ;
        propsResolverMap['response-form-layout-setting'] = responseFormLayoutPropsResolver;
       
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
        componentMap['form-group'] = FDynamicFromGroupDesign;
        propsResolverMap['form-group'] = formGroupPropsResolver;
        componentMap['response-form'] = FResponseFormDesign;
        propsResolverMap['response-form'] = formPropsResolver;
    }
};
