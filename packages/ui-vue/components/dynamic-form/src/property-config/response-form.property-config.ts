 
import { DesignerComponentInstance } from "@farris/ui-vue/components/designer-canvas";
import { BaseControlProperty } from "../../../../components/property-panel/src/composition/entity/base-property";
import { useResponseFormLayoutSetting } from "../composition/use-response-form-layout-setting";

export class ResponseFormProperty extends BaseControlProperty {

    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }
    public getPropertyConfig(propertyData: any, componentInstance: DesignerComponentInstance) {
        const self = this;
        // 基本信息
        this.propertyConfig.categories['basic'] = this.getBasicPropConfig(propertyData);
        // 外观
        const { checkIsInFormComponent, assembleUnifiedLayoutContext, changeFormControlsByUnifiedLayoutConfig } = useResponseFormLayoutSetting(this.formSchemaUtils, this.componentId);
        const appearanceCategory = this.getAppearanceConfig(propertyData);

        appearanceCategory.properties['unifiedLayout'] = {
            title: "统一布局配置",
            description: "统一配置卡片区域内所有控件的宽度，只支持标准模式",
            visible: checkIsInFormComponent(this.componentId),
            refreshPanelAfterChanged: true,
            editor: {
                type: "response-form-layout-setting",
                initialState: assembleUnifiedLayoutContext(propertyData)
            }
        };
        appearanceCategory.setPropertyRelates = function (changeObject, prop) {
            if (!changeObject) {
                return;
            }
            switch (changeObject && changeObject.propertyID) {
                case 'unifiedLayout': {
                    changeFormControlsByUnifiedLayoutConfig(null, changeObject.propertyValue, propertyData.id);
                    self.updateElementByParentContainer(propertyData.id, componentInstance);
                    break;
                }
            }
        };

        this.propertyConfig.categories['appearance'] = appearanceCategory;
        return this.propertyConfig;
    }
}
