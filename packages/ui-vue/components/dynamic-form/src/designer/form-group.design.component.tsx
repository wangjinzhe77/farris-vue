 
import { SetupContext, defineComponent, inject, onMounted, ref } from 'vue';
import { DynamicFormGroupPropsType, dynamicFormGroupProps } from '../component/dynamic-form-group/dynamic-form-group.props';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import FDynamicFormGroupDesign from '../component/dynamic-form-group/dynamic-form-group.design.component';
import { propertyConfigSchemaMap } from '../../../../components/dynamic-resolver/src/property-config-resolver';
import { DesignerHostService } from '../../../../components/designer-canvas/src/composition/types';
import { useDesignerRulesForFormGroup } from './form-group-use-designer-rules';

export default defineComponent({
    name: 'FFormGroupDesign',
    props: dynamicFormGroupProps,
    emits: [],
    setup(props: DynamicFormGroupPropsType, context) {
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerHostService = inject<DesignerHostService>('designer-host-service');
        const designerRulesComposition = useDesignerRulesForFormGroup(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;

            componentInstance.value.getPropConfig = (...args) => {
                let propertyConfigSchema = propertyConfigSchemaMap[props.editor.type];
                if (propertyConfigSchema && Object.keys(propertyConfigSchema).length === 0 && elementRef.value?.editorRef && elementRef.value?.editorRef?.getPropConfig) {
                    propertyConfigSchema = elementRef.value.editorRef.getPropConfig(...args, componentInstance.value);
                }
                return propertyConfigSchema;
            };
        });


        context.expose(componentInstance.value);

        return () => {
            return <FDynamicFormGroupDesign {...props} ref={elementRef}></FDynamicFormGroupDesign>;
        };
    }
});
