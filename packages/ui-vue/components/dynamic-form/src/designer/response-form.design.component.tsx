import { SetupContext, computed, defineComponent, inject, onMounted, ref } from 'vue';
import { DynamicFormGroupPropsType, dynamicFormGroupProps } from '../component/dynamic-form-group/dynamic-form-group.props';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { useDesignerRules } from './response-form-use-designer-rules';
import areaResponseDirective from '../../../common/directive/area-response';
import { DesignerHostService } from '../../../designer-canvas/src/composition/types';

export default defineComponent({
    name: 'FResponseFormDesign',
    directives: {
        "area-response":areaResponseDirective,
      },
    props: dynamicFormGroupProps,
    emits: [],
    setup(props: DynamicFormGroupPropsType, context) {
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerHostService = inject<DesignerHostService>('designer-host-service');
        const designerRulesComposition = useDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        const responseFormClass = computed(() => {
            const customClassArray = props.customClass.split(' ');
            const classObject = {
                'drag-container': true
            } as Record<string, boolean>;
            customClassArray.reduce((result: Record<string, boolean>, classString: string) => {
                result[classString] = true;
                return result;
            }, classObject);
            return classObject;
        });
        

        return () => {
            return (
                <div
                    ref={elementRef}
                    class={responseFormClass.value}
                    style={props.customClass}
                    data-dragref={`${designItemContext.schema.id}-container`}
                    data-associate={`${designItemContext.schema.id}-component`}  v-area-response>
                    {context.slots.default && context.slots.default()}
                </div>
            );
        };
    }
});
