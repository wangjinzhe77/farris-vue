import { defineComponent, onMounted, ref, SetupContext } from 'vue';
import FComboList from '@farris/ui-vue/components/combo-list';
import { FMessageBoxService } from '../../../message-box';
import { ResponseFormLayoutSettingProps, responseFormLayoutSettingProps } from './response-form-layout-setting.props';
import './response-form-layout-setting.css';
export default defineComponent({
    name: 'FResponseFormLayoutSetting',
    props: responseFormLayoutSettingProps,
    emits: ['change'] as (string[] & ThisType<void>) | undefined,
    setup(props: ResponseFormLayoutSettingProps, context: SetupContext) {

        /** 小屏幕：列数只支持1 */
        const allowedSMColumns = [1];

        /** 中等屏幕，列数支持1,2 */
        const allowedMDColumns = [1, 2];

        /** 大屏幕，列数支持1,2, 3, 4 */
        const allowedLGColumns = [1, 2, 3, 4];

        /** 超大等屏幕，列数支持1,2, 3, 4, 6 */
        const allowedELColumns = [1, 2, 3, 4, 6];

        // 各屏幕尺寸的枚举项
        let smColumnEnumData = [] as any;
        let mdColumnEnumData = [] as any;
        let lgColumnEnumData = [] as any;
        let elColumnEnumData = [] as any;

        /** 当前卡片控件的column样式值 ，例如col-md-6，则uniqueColumnClass为6 */
        const uniqueColumnClass = ref(props.initialState);

        /** 当前卡片控件每行控件个数 */
        const smControlCountInRow = ref(0);
        const mdControlCountInRow = ref(0);
        const lgControlCountInRow = ref(0);
        const elControlCountInRow = ref(0);

        function initLayoutSetting() {
            const customText = '自定义';

            // 小屏幕：列数只支持1
            const controlCountInSMRow = 12 / uniqueColumnClass.value.uniqueColClassInSM;
            if (!allowedSMColumns.includes(controlCountInSMRow)) {
                smColumnEnumData.push({ value: 0, name: customText });
                smControlCountInRow.value = 0;
            } else {
                smControlCountInRow.value = controlCountInSMRow;
            }

            // 中等屏幕，列数支持1,2
            const controlCountInMDRow = 12 / uniqueColumnClass.value.uniqueColClassInMD;
            if (!allowedMDColumns.includes(controlCountInMDRow)) {
                mdColumnEnumData.push({ value: 0, name: customText });
                mdControlCountInRow.value = 0;
            } else {
                mdControlCountInRow.value = controlCountInMDRow;
            }

            // 大屏幕，列数支持1,2, 3, 4
            const controlCountInLGRow = 12 / uniqueColumnClass.value.uniqueColClassInLG;
            if (!allowedLGColumns.includes(controlCountInLGRow)) {
                lgColumnEnumData.push({ value: 0, name: customText });
                lgControlCountInRow.value = 0;
            } else {
                lgControlCountInRow.value = controlCountInLGRow;
            }

            // 超大等屏幕，列数支持1,2, 3, 4, 6
            const controlCountInELRow = 12 / uniqueColumnClass.value.uniqueColClassInEL;
            if (!allowedELColumns.includes(controlCountInELRow)) {
                elColumnEnumData.push({ value: 0, name: customText });
                elControlCountInRow.value = 0;
            } else {
                elControlCountInRow.value = controlCountInELRow;
            }
            smColumnEnumData = [...smColumnEnumData];
            mdColumnEnumData = [...mdColumnEnumData];
            lgColumnEnumData = [...lgColumnEnumData];
            elColumnEnumData = [...elColumnEnumData];
        }

        function initStandardColumEnumData() {
            allowedSMColumns.forEach(c => {
                smColumnEnumData.push({ value: c, name: c + '' });
            });
            allowedMDColumns.forEach(c => {
                mdColumnEnumData.push({ value: c, name: c + '' });
            });
            allowedLGColumns.forEach(c => {
                lgColumnEnumData.push({ value: c, name: c + '' });
            });
            allowedELColumns.forEach(c => {
                elColumnEnumData.push({ value: c, name: c + '' });
            });
        }

        function changeValue() {
            FMessageBoxService.question('应用统一布局，将重置区域内部所有控件的宽度样式，确定应用?', '', () => {
                uniqueColumnClass.value.uniqueColClassInSM = smControlCountInRow.value === 0 ? null : 12 / smControlCountInRow.value;
                uniqueColumnClass.value.uniqueColClassInMD = mdControlCountInRow.value === 0 ? null : 12 / mdControlCountInRow.value;
                uniqueColumnClass.value.uniqueColClassInLG = lgControlCountInRow.value === 0 ? null : 12 / lgControlCountInRow.value;
                uniqueColumnClass.value.uniqueColClassInEL = elControlCountInRow.value === 0 ? null : 12 / elControlCountInRow.value;
                context.emit('change', uniqueColumnClass.value);
            }, () => { });
        }

        onMounted(() => {
            initStandardColumEnumData();
            initLayoutSetting();
        });

        return () => {
            return (
                <div>
                    <div class="f-form-layout farris-form farris-form-controls-inline">
                        <div class="form-group farris-form-group screenGroup">
                            <span class="screenName"> 小屏幕 </span>
                            <div class="d-flex ml-2 screen-input" >
                                <FComboList
                                    data={smColumnEnumData}
                                    idField={'value'}
                                    valueField={'value'}
                                    editable={false}
                                    enableClear={false}
                                    v-model={smControlCountInRow.value}
                                ></FComboList>
                                <span class="ml-2 columnText">列</span>
                            </div>
                        </div>
                        <div class="form-group farris-form-group screenGroup">
                            <span class="screenName">中等屏幕</span>
                            <div class="d-flex ml-2 screen-input"><FComboList
                                data={mdColumnEnumData}
                                idField={'value'}
                                valueField={'value'} 
                                editable={false}
                                enableClear={false}
                                v-model={mdControlCountInRow.value}
                            ></FComboList><span class="ml-2 columnText">列</span>
                            </div>
                        </div>
                        <div class="form-group farris-form-group screenGroup">
                            <span class="screenName">大屏幕</span>
                            <div class="d-flex ml-2 screen-input"><FComboList
                                data={lgColumnEnumData}
                                idField={'value'}
                                valueField={'value'}
                                editable={false}
                                enableClear={false}
                                v-model={lgControlCountInRow.value}
                            ></FComboList>
                                <span class="ml-2 columnText">列</span>
                            </div>
                        </div>
                        <div class="form-group farris-form-group screenGroup">
                            <span class="screenName">极大屏幕</span>
                            <div class="d-flex ml-2 screen-input">
                                <FComboList
                                    data={elColumnEnumData}
                                    idField={'value'}
                                    valueField={'value'}
                                    editable={false}
                                    enableClear={false}
                                    v-model={elControlCountInRow.value}
                                ></FComboList>
                                <span class="ml-2 columnText">列</span>
                            </div>
                        </div>
                    </div>
                    <div class="applyBtn">
                        <button class="btn f-rt-btn btn-primary" type="button" onClick={() => changeValue()}>应用</button>
                    </div>
                </div >
            );
        };
    }
});
