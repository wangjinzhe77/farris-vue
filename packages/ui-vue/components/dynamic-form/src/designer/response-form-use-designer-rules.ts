import { DesignerHostService, DesignerHTMLElement, DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { useDragulaCommonRule } from "../../../designer-canvas/src/composition/rule/use-dragula-common-rule";
import { ComponentSchema, DesignerItemContext } from "../../../designer-canvas/src/types";
import { getSchemaByType } from "../../../dynamic-resolver/src/schema-resolver";
import { UseTemplateDragAndDropRules } from "../../../designer-canvas/src/composition/rule/use-template-rule";
import { ResponseFormProperty } from "../property-config/response-form.property-config";
import { DgControl } from "../../../designer-canvas";
import { FormBindingType } from "../../../property-panel/src/composition/type";

export function useDesignerRules(designItemContext: DesignerItemContext, designerHostService?: DesignerHostService): UseDesignerRules {
    const schema = designItemContext.schema as ComponentSchema;

    const dragAndDropRules = new UseTemplateDragAndDropRules();
    const { canMove, canAccept, canDelete } = dragAndDropRules.getTemplateRule(designItemContext, designerHostService);

    /**
     * 跨Component移动输入类控件时，需要判断源卡片和目标卡片是否绑定同一实体。
     */
    function checkBindingSameEntity(draggingContext: DraggingResolveContext) {
        const { sourceElement } = draggingContext;
        const { componentInstance: sourceComponentInstance } = sourceElement;
        const formSchemaUtils = designerHostService?.formSchemaUtils;
        if (!formSchemaUtils) {
            return true;
        }
        const sourceComponent = formSchemaUtils.getComponentById(sourceComponentInstance.value.belongedComponentId);
        const sourceViewmodel = formSchemaUtils.getViewModelById(sourceComponent.viewModel);
        const currentComponent = formSchemaUtils.getComponentById(designItemContext.componentInstance.value.belongedComponentId);
        const currentViewmodel = formSchemaUtils.getViewModelById(currentComponent.viewModel);

        if (sourceViewmodel?.id && currentViewmodel?.id && sourceViewmodel.id !== currentViewmodel.id) {
            return sourceViewmodel.bindTo === currentViewmodel.bindTo;
        }
        return true;
    }
    /**
     * 接收实体树新字段，需要判断新字段和当前卡片是否绑定同一实体。
     */
    function checkFieldBelongedToCurrentEntity(draggingContext: DraggingResolveContext) {
        const { bindingTargetId } = draggingContext;
        if (!designerHostService) {
            return;
        }
        const { formSchemaUtils, schemaService } = designerHostService;
        const currentComponent = formSchemaUtils.getComponentById(designItemContext.componentInstance.value.belongedComponentId);
        const fieldInfo = schemaService.getFieldByIDAndVMID(bindingTargetId, currentComponent.viewModel);
        if (fieldInfo?.schemaField) {
            return true;
        }
        return false;

    }
    /**
      * 只接收form-group输入类控件
      */
    function checkFormContainerAcceptable(draggingContext: DraggingResolveContext) {
        const schemaContext = dragAndDropRules.getComponentContext(designItemContext);
        if (schemaContext && schemaContext.componentClassList && schemaContext.componentClassList.includes('f-form-layout') && schemaContext.componentClassList.includes('farris-form')) {
            // 从工具箱拖入的输入类控件
            if (draggingContext.sourceType === 'control' && draggingContext.componentCategory === 'input') {
                return true;
            }
            // 画布中移动位置的输入类控件
            if (draggingContext.sourceType === 'move' && draggingContext.componentType === 'form-group') {
                return checkBindingSameEntity(draggingContext);
            }
            // 从实体树拖入的字段，需要判断是否属于当前组件绑定的实体
            if (draggingContext.sourceType === 'field' && draggingContext.componentCategory === 'input') {
                return checkFieldBelongedToCurrentEntity(draggingContext);
            }
            return false;
        }
        return true;
    }

    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {
        const basalRule = useDragulaCommonRule().basalDragulaRuleForContainer(draggingContext, designerHostService);
        if (!basalRule) {
            return false;
        }
        if (!canAccept) {
            return false;
        }

        if (!checkFormContainerAcceptable(draggingContext)) {
            return false;
        }
        return true;
    }
    /**
     * 从将输入类控件从fieldSet移动到ResponseForm，需要移除VM中的分组信息
     */
    function changeGroupValueForViewModelField(sourceElement: DesignerHTMLElement) {
        const sourceComponentInstance = sourceElement.componentInstance;
        const sourceComponentSchema = sourceComponentInstance.value.schema;
        const useFormSchema = designerHostService?.formSchemaUtils;
        const dgViewModelUtil = designerHostService?.designViewModelUtils;

        const sourceViewModelId = useFormSchema.getViewModelIdByComponentId(sourceComponentInstance.value.belongedComponentId);
        const sourceDesignViewModel = dgViewModelUtil.getDgViewModel(sourceViewModelId);

        const sourceParentComponentInstance = sourceComponentInstance.value.parent;
        if (sourceParentComponentInstance && DgControl['field-set'] && sourceParentComponentInstance['schema']?.type === DgControl['field-set'].type) {
            if (sourceDesignViewModel) {
                const vmChangeSet = { groupId: null, groupName: null };
                const sourceBindingType = sourceComponentSchema.binding.type;
                switch (sourceBindingType) {
                    case FormBindingType.Form: {
                        sourceDesignViewModel.changeField(sourceComponentSchema.binding.field, vmChangeSet);
                        break;
                    }
                    case FormBindingType.Variable: {
                        useFormSchema.modifyViewModelFieldById(sourceViewModelId, sourceComponentSchema.binding.field, vmChangeSet, true);
                        break;
                    }
                }
            }
        }
    }
    /**
     * 若源控件绑定字段，需要将绑定信息移动到新VM
     * @param sourceBindingFieldId 源控件绑定的字段id
     * @param sourceViewModelId 源控件所属视图模型id
     * @param dgViewModelService 视图模型服务类
     */
    function moveFieldBetweenViewModel(sourceBindingFieldId: string, sourceViewModelId: string, currentViewmodelId: string) {
        const dgViewModelUtil = designerHostService?.designViewModelUtils;
        const sourceDgViewModel = dgViewModelUtil.getDgViewModel(sourceViewModelId);
        const sourceVMField = sourceDgViewModel.fields.find(f => f.id === sourceBindingFieldId);
        sourceDgViewModel.removeField([sourceBindingFieldId]);

        const currentDgViewModel = dgViewModelUtil.getDgViewModel(currentViewmodelId);
        currentDgViewModel.addField(sourceVMField);

    }
    /**
     * 源控件绑定变量，需要将绑定信息移动到新VM，并且将绑定变量拷贝到新VM
     * @param sourceBindingFieldId 源控件绑定的字段id
     * @param sourceViewModelId 源控件所属视图模型id
     * @param domService DOM结构服务类
     */
    function moveVariableBetweenViewModel(sourceComponentSchema: any, sourceViewModel: any, currentViewModel: any, copiedVariables: string[] = []) {
        const sourceViewModelId = sourceViewModel.id;
        const sourceBindingVariableId = sourceComponentSchema.binding.field;

        // 移动VM.fields
        const sourceVMFieldIndex = sourceViewModel.fields.findIndex(f => f.id === sourceBindingVariableId);
        if (sourceVMFieldIndex > -1) {
            currentViewModel.fields.push(sourceViewModel.fields[sourceVMFieldIndex]);
            sourceViewModel.fields.splice(sourceVMFieldIndex, 1);
        }

        // 1、从根组件向子组件移动：不拷贝变量，直接使用根组件的变量，只需要修改引用路径。
        if (sourceViewModelId === 'root-viewmodel' && currentViewModel.id !== 'root-viewmodel') {
            if (sourceComponentSchema.binding.path && !sourceComponentSchema.binding.path.includes('root-component.')) {
                sourceComponentSchema.binding.path = 'root-component.' + sourceComponentSchema.binding.path;
            }
            return;
        }

        // 2、从子组件向根组件移动，并且控件绑定的是根组件的变量：那么仍然使用根组件的变量，只需要修改引用路径
        if (currentViewModel.id === 'root-viewmodel' && sourceViewModelId !== 'root-viewmodel') {
            if (sourceComponentSchema.binding.path && sourceComponentSchema.binding.path.includes('root-component.')) {
                sourceComponentSchema.binding.path = sourceComponentSchema.binding.path.replace('root-component.', '');
                return;
            }
        }
        // 3、其他场景：拷贝绑定的变量（为了防止此变量在源VM其他地方被使用的情况，这里是拷贝变量，而不是剪切）
        const sourceState = sourceViewModel.states.find(state => state.id === sourceBindingVariableId);
        if (sourceState) {
            currentViewModel.states = currentViewModel.states ? currentViewModel.states : [];
            if (currentViewModel.states.findIndex(s => s.id === sourceState.id) < 0) {
                currentViewModel.states.push(sourceState);
                copiedVariables.push(sourceState.id);
            }
        }

    }
    /**
     * 若控件的某一个属性绑定的变量（例如控件的必填属性绑定变量），则需要将此变量拷贝的新VM中
     * @param sourceComponentSchema 源控件Schema结构
     * @param sourceViewModelId  源控件所属视图模型id
     * @param domService DOM结构服务类
     */
    function handleVaribleInPropertyBinding(sourceComponentSchema: ComponentSchema, sourceViewModel: any, currentViewModel: any, copiedVariables: string[] = []) {
        const isMovedFromRootToChildComponent = sourceViewModel.id === 'root-viewmodel' && currentViewModel.id !== 'root-viewmodel';
        const isMovedFromChildToRoot = currentViewModel.id === 'root-viewmodel' && sourceViewModel.id !== 'root-viewmodel';

        Object.keys(sourceComponentSchema).forEach(propertyID => {
            if (propertyID === 'binding') {
                return;
            }
            const propertyValue = sourceComponentSchema[propertyID];
            if (propertyValue && typeof (propertyValue) === 'object' && propertyValue.type === FormBindingType.Variable && propertyValue.field) {

                // 1、从根组件向子组件移动：不拷贝变量，直接使用根组件的变量，只需要修改引用路径。
                if (isMovedFromRootToChildComponent) {
                    if (propertyValue.path && !propertyValue.path.includes('root-component.')) {
                        propertyValue.path = 'root-component.' + propertyValue.path;
                    }
                    return;
                }

                // 2、从子组件向根组件移动，并且控件绑定的是根组件的变量：那么仍然使用根组件的变量，只需要修改引用路径
                if (isMovedFromChildToRoot && propertyValue.path && propertyValue.path.includes('root-component.')) {
                    propertyValue.path = propertyValue.path.replace('root-component.', '');
                    return;
                }

                // 3、其他场景：拷贝绑定的变量（为了防止此变量在源VM其他地方被使用的情况，这里是拷贝变量，而不是剪切）
                const sourceState = sourceViewModel.states.find(state => state.id === propertyValue.field);
                if (sourceState) {
                    currentViewModel.states = currentViewModel.states ? currentViewModel.states : [];
                    if (currentViewModel.states.findIndex(s => s.id === sourceState.id) < 0) {
                        currentViewModel.states.push(sourceState);
                        copiedVariables.push(propertyValue.field);
                    }

                }
            }
        });

    }

    /**
     * 根据指定的条件遍历查找节点
     */
    function searchInAllControls(rootNode: any, predict: (item: any, usedVariables: string[]) => void, allUsedVariables: string[]) {
        if (!rootNode) {
            return;
        }
        predict(rootNode, allUsedVariables);

        if (rootNode.contents) {
            for (const item of rootNode.contents) {
                searchInAllControls(item, predict, allUsedVariables);
            }
        }

    }
    /**
     * 移动控件后，判断控件涉及的变量在源组件中是否还有用到，若没有用到，删除变量
     */
    function handleVariablesInSourceViewModel(variableIds: string[] = [], sourceViewModelId: string) {
        if (!variableIds.length) {
            return;
        }
        const useFormSchema = designerHostService?.formSchemaUtils;
        const sourceComponent = useFormSchema.getComponentByViewModelId(sourceViewModelId);
        const allUsedVariables: string[] = [];
        const searchVariableInControl = ((control: any, usedVariables: string[]) => {
            if (!control) {
                return;
            }
            Object.keys(control).forEach(propertyID => {
                const propertyValue = control[propertyID];
                if (propertyValue && typeof (propertyValue) === 'object' && propertyValue.type === FormBindingType.Variable && variableIds.includes(propertyValue.field) && !usedVariables.includes(propertyValue.field)) {
                    usedVariables.push(propertyValue.field);
                }
            });
        });
        searchInAllControls(sourceComponent, searchVariableInControl, allUsedVariables);

        const sourceViewModel = useFormSchema.getViewModelById(sourceViewModelId);
        variableIds.forEach(variableId => {
            if (!allUsedVariables.includes(variableId)) {
                sourceViewModel.states = sourceViewModel.states.filter(state => state.id !== variableId);
            }
        });
    }
    /**
     * 跨组件移动输入类控件，需要同步视图模型中的字段和变量等信息
     */
    function moveInputBetweenComponent(sourceElement: DesignerHTMLElement) {
        const useFormSchema = designerHostService?.formSchemaUtils;

        const sourceComponentInstance = sourceElement.componentInstance;
        const sourceComponentSchema = sourceComponentInstance.value.schema;
        const sourceViewModelId = useFormSchema.getViewModelIdByComponentId(sourceComponentInstance.value.belongedComponentId);
        const sourceViewModel = useFormSchema.getViewModelById(sourceViewModelId);
        const currentComponent = useFormSchema.getComponentById(designItemContext.componentInstance.value.belongedComponentId);
        const currentViewmodel = useFormSchema.getViewModelById(currentComponent.viewModel);

        if (sourceViewModelId !== currentViewmodel.id) {
            // 源控件绑定的字段或变量id
            const sourceBindingType = sourceComponentSchema.binding.type;
            const sourceBindingFieldId = sourceComponentSchema.binding.field;

            const copiedVariables = [];
            switch (sourceBindingType) {
                case FormBindingType.Form: {
                    moveFieldBetweenViewModel(sourceBindingFieldId, sourceViewModelId, currentViewmodel.id);
                    break;
                }
                case FormBindingType.Variable: {
                    moveVariableBetweenViewModel(sourceComponentSchema, sourceViewModel, currentViewmodel, copiedVariables);
                    break;
                }
            }
            handleVaribleInPropertyBinding(sourceComponentSchema, sourceViewModel, currentViewmodel, copiedVariables);
            // handleEventsInProperty(sourceComponentSchema, sourceViewModelId, domService, dgViewModelService);

            if (sourceViewModelId !== 'root-viewmodel') {
                handleVariablesInSourceViewModel(copiedVariables, sourceViewModelId);
            }
        }
    }
    function onAcceptMovedChildElement(sourceElement: DesignerHTMLElement) {
        if (!sourceElement || !sourceElement.componentInstance) {
            return;
        }
        const sourceComponentInstance = sourceElement.componentInstance;
        const sourceComponentSchema = sourceComponentInstance.value.schema;
        if (!sourceComponentSchema.binding || !sourceComponentSchema.binding.field) {
            return;
        }

        // 若从fieldSet移动到Form，需要移除VM中的分组信息
        changeGroupValueForViewModelField(sourceElement);

        // 若是跨组件的移动，需要移动vm的字段
        moveInputBetweenComponent(sourceElement);

    }

    function getStyles() {
        const component = schema;
        if (component.componentType) {
            return 'display:inherit;flex-direction:inherit;margin-bottom:10px';
        }
        return '';
    }

    /**
     * 根据所属组件的componentType设置输入控件的样式
     * @param componentType 
     * @returns 
     */
    function resolveControlClassByComponentType(formColumns: number) {
        let className = '';
        switch (formColumns) {
            case 1: {
                className = 'col-12';
                break;
            }
            case 2: {
                className = 'col-12 col-md-6 col-xl-6 col-el-6';
                break;
            }
            case 3: {
                className = 'col-12 col-md-6 col-xl-4 col-el-4';
                break;
            }
            case 4: {
                className = 'col-12 col-md-6 col-xl-3 col-el-2';
                break;
            }
        }

        return className;
    }

    function resolveFormGroupAppearance(formGroupElementSchema: any) {
        const belongedComponentInstance = designItemContext.componentInstance.value.getBelongedComponentInstance(designItemContext.componentInstance);
        if (belongedComponentInstance && belongedComponentInstance.schema && belongedComponentInstance.schema.componentType) {
            const { formColumns } = belongedComponentInstance.schema;
            let appearanceClass = resolveControlClassByComponentType(formColumns);
            appearanceClass = designerHostService?.formSchemaUtils.getControlClassByFormUnifiedLayout(appearanceClass, "", designItemContext.schema);

            if (!formGroupElementSchema.appearance) {
                formGroupElementSchema.appearance = {};
            }
            formGroupElementSchema.appearance.class = appearanceClass;
        }
    }
    /**
     * 添加输入类控件后，将绑定信息同步到视图模型
     */
    function syncFieldToViewModel(resolveContext: DraggingResolveContext, editorType: string) {
        const { bindingSourceContext, parentComponentInstance } = resolveContext;
        if (bindingSourceContext?.entityFieldNode && parentComponentInstance) {
            const designViewModelUtils = designerHostService?.designViewModelUtils;
            const formSchemaUtils = designerHostService?.formSchemaUtils;
            const viewModelId = formSchemaUtils.getViewModelIdByComponentId(parentComponentInstance.belongedComponentId);

            const dgViewModel = designViewModelUtils.getDgViewModel(viewModelId);
            dgViewModel.removeField([bindingSourceContext.entityFieldNode.id]);
            dgViewModel.addField(bindingSourceContext.designViewModelField);
            if (editorType) {
                dgViewModel.changeField(bindingSourceContext.entityFieldNode.id, { editor: { $type: editorType } });
            }
        }

    }
    function onResolveNewComponentSchema(resolveContext: DraggingResolveContext, componentSchema: ComponentSchema): ComponentSchema {
        const component = schema;
        // 控件本身样式
        const componentClass = component.appearance && component.appearance.class || '';
        const componentClassList = componentClass.split(' ') as string[];
        if (componentClassList.includes('f-form-layout')) {
            const { label } = resolveContext;
            let formGroupElementSchema;
            // 控件若有绑定信息，则根据绑定信息创建控件
            if (resolveContext.bindingSourceContext?.entityFieldNode) {
                const controlCreatorUtils = designerHostService?.controlCreatorUtils;
                formGroupElementSchema = controlCreatorUtils.setFormFieldProperty(resolveContext.bindingSourceContext?.entityFieldNode, componentSchema?.type);
            } else {
                formGroupElementSchema = getSchemaByType('form-group') as ComponentSchema;
                formGroupElementSchema.editor = componentSchema;
                formGroupElementSchema.label = label;
            }

            resolveFormGroupAppearance(formGroupElementSchema);

            syncFieldToViewModel(resolveContext, formGroupElementSchema.editor?.type);

            return formGroupElementSchema;
        }
        return componentSchema;
    }

    function checkCanMoveComponent() {
        return canMove;
    }
    function checkCanDeleteComponent() {
        return canDelete;
    }

    function hideNestedPaddingInDesginerView() {
        return false;
    }
    // 构造属性配置方法
    function getPropsConfig(componentId: string) {
        const responseFormPops = new ResponseFormProperty(componentId, designerHostService);
        return responseFormPops.getPropertyConfig(schema, designItemContext.componentInstance.value);
    }

    return {
        canAccepts,
        checkCanDeleteComponent,
        checkCanMoveComponent,
        getStyles,
        getPropsConfig,
        hideNestedPaddingInDesginerView,
        onAcceptMovedChildElement,
        onResolveNewComponentSchema
    };
}
