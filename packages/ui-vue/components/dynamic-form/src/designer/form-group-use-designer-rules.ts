import { DesignerItemContext, DesignerHostService, UseDesignerRules } from "@farris/ui-vue/components/designer-canvas";

export function useDesignerRulesForFormGroup(designItemContext: DesignerItemContext, designerHostService?: DesignerHostService): UseDesignerRules {

    function checkCanMoveComponent() {
        return true;
    }
    function checkCanDeleteComponent() {
        return true;
    }

    function canAccepts() {
        return false;
    }

    function hideNestedPaddingInDesginerView() {
        return true;
    }
    /**
     * 若控件有绑定信息，删除控件时需要同步移除viewmodel中的记录
     */
    function removeBindingFromViewModel(bindingFieldId: string) {
        const designViewModelUtils = designerHostService?.designViewModelUtils;
        const formSchemaUtils = designerHostService?.formSchemaUtils;
        const belongedComponentId = designItemContext?.componentInstance.value?.belongedComponentId;
        if (!belongedComponentId || !designViewModelUtils || !formSchemaUtils) {
            return;
        }
        const belongedviewModelId = formSchemaUtils.getViewModelIdByComponentId(belongedComponentId);
        const dgViewModel = designViewModelUtils.getDgViewModel(belongedviewModelId);

        if (dgViewModel) {
            dgViewModel.removeField([bindingFieldId]);
        }
    }
    /**
     * 若控件配置了表达式，删除控件时需要同步移除表达式
     */
    function removeExpression(bindingFieldId: string) {
        const formSchemaUtils = designerHostService?.formSchemaUtils;

        if (formSchemaUtils.getExpressions().length) {
            const expFieldIndex = formSchemaUtils.getExpressions().findIndex(e => e.fieldId === bindingFieldId);
            if (expFieldIndex > -1) {
                formSchemaUtils.getExpressions().splice(expFieldIndex, 1);
            }

        }
    }
    /**
     * 控件删除后事件
     */
    function onRemoveComponent() {
        const { schema } = designItemContext;
        const bindingFieldId = schema.binding && schema.binding.field;
        if (bindingFieldId) {
            removeBindingFromViewModel(bindingFieldId);
            removeExpression(bindingFieldId);
        }
    }


    return {
        canAccepts,
        checkCanDeleteComponent,
        checkCanMoveComponent,
        hideNestedPaddingInDesginerView,
        onRemoveComponent
    };
}
