 
import { ExtractPropTypes, PropType } from 'vue';

import { layoutSettingSchemaMapper } from '../schema/schema-mapper';
import { layoutSettingSchemaResolver } from '../schema/response-form-schema-resolver';
import formSchema from '../schema/response-form-layout-setting.schema.json';
import { createPropsResolver } from '@farris/ui-vue/components/dynamic-resolver';
import { FormUnifiedColumnLayout } from '../types';

export const responseFormLayoutSettingProps = {
    initialState: {
        type: Object as PropType<FormUnifiedColumnLayout>, default: {
            uniqueColClassInSM: 12,
            uniqueColClassInMD: 6,
            uniqueColClassInLG: 3,
            uniqueColClassInEL: 2
        }
    }
} as Record<string, any>;

export type ResponseFormLayoutSettingProps = ExtractPropTypes<typeof responseFormLayoutSettingProps>;

export const responseFormLayoutPropsResolver = createPropsResolver<ResponseFormLayoutSettingProps>(responseFormLayoutSettingProps, formSchema, layoutSettingSchemaMapper, layoutSettingSchemaResolver);
