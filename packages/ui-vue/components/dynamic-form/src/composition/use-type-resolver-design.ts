import { componentMap, componentPropsConverter } from '../../../../components/designer-canvas/src/components/maps';
import FInputGroupDesign from '../../../input-group/src/designer/input-group.design.component';
import { EditorType, EditorConfig } from "../types";
import { UseTypeResolver } from "./types";

export function useTypeResolverDesign(): UseTypeResolver {

    function resolveEditorProps(type: EditorType, config: EditorConfig): Record<string, any> {
        const propsConverter = componentPropsConverter[type];
        const viewProps = propsConverter ? propsConverter(config) : {};
        return viewProps;
    }

    function resolveEditorType(type: EditorType) {
        return componentMap[type] || FInputGroupDesign;
    }

    function getChangeFunctionName(type: EditorType) {
    }

    function getClearFunctionName(type: EditorType) {
    }

    return { getChangeFunctionName, getClearFunctionName, resolveEditorProps, resolveEditorType };
}
