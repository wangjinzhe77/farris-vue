import FInputGroup from '../../../input-group/src/input-group.component';
import { EditorType, EditorConfig } from "../types";
import { UseTypeResolver } from "./types";
import { componentMap, componentPropsConverter, loadRegister } from '../../../dynamic-view/src/components/maps';

export function useTypeResolver(): UseTypeResolver {

    loadRegister();

    function resolveEditorType(type: EditorType) {
        const editorType = componentMap[type];
        if (!editorType) {
            throw new Error(`Could not find the editor of type '${type}'`);
        }
        return editorType;
    }

    function resolveEditorProps(type: EditorType, config: EditorConfig): Record<string, any> {
        const propsConverter = componentPropsConverter[type];
        const viewProps = propsConverter ? propsConverter(config) : {};
        // const Component = resolveEditorType(type);
        // const componentPropertyProps = Object.keys(Component?.props || {});
        // const componentEventProps = (Component?.emits as string[] || []).map((eventName: string) => `on${eventName.charAt(0).toUpperCase()}${eventName.slice(1)}`);
        // const currentComponentProps = componentPropertyProps.concat(componentEventProps);
        // const componentProps = Object.keys(viewProps).reduce((props: Record<string, any>, propertyName: string) => {
        //     if (currentComponentProps.indexOf(propertyName) !== -1) {
        //         props[propertyName] = viewProps[propertyName];
        //     }
        //     return props;
        // }, {});
        return viewProps;
    }

    // jumphere
    function getChangeFunctionName(type: EditorType) {

        switch (type) {
            case 'check-box':
                return 'onChangeValue';
            case 'switch':
                return 'onModelValueChanged';
            case 'check-group':
                return 'onChangeValue';
            case 'combo-list':
            case 'combo-tree':
                return 'onChange';
            case 'combo-lookup':
                return '';
            case 'date-picker':
                return 'onDatePicked';
            case 'date-range':
                return '';
            case 'datetime-picker':
                return '';
            case 'datetime-range':
                return '';
            case 'month-picker':
                return '';
            case 'month-range':
                return '';
            case 'year-picker':
                return '';
            case 'year-range':
                return '';
            case 'input-group':
                return 'onChange';
            case 'lookup':
                return 'onUpdate:modelValue';
            case 'number-range':
                return 'onValueChange';
            case 'number-spinner':
                return 'onValueChange';
            case 'radio-group':
                return 'onChangeValue';
            case 'text':
                return '';
            case 'response-layout-editor-setting':
                return 'onChange';
            case 'events-editor':
                return 'onSavedCommandListChanged';
            case 'grid-field-editor':
                return 'onChange';
            case 'item-collection-editor':
                return 'onChange';
            case 'response-form-layout-setting':
                return 'onChange';
            case 'field-selector': case 'binding-selector':
                return 'onFieldSelected';
            case 'schema-selector':
                return 'onSchemaSelected';
            case 'mapping-editor':
                return 'onMappingFieldsChanged';
            case 'textarea':
                return 'onValueChange';
            case 'query-solution-config':
            case 'solution-preset':
                return 'onFieldsChanged';


        }
    }

    function getClearFunctionName(type: EditorType) {
        switch (type) {
            case 'combo-list':
            case 'input-group':
            case 'textarea':
                return 'onClear';
        }
    }

    return { resolveEditorProps, resolveEditorType, getChangeFunctionName, getClearFunctionName };
}
