import { EditorConfig } from "../types";

export interface UseTypeResolver {

    resolveEditorProps(type: string, config: EditorConfig): Record<string, any>;

    resolveEditorType(type: string): any;

    getChangeFunctionName(type: string): any;

    getClearFunctionName(type: string): any;
}
