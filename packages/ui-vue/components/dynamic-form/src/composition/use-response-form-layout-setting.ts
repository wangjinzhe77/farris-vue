import { ResponseFormLayoutContext } from "@farris/ui-vue/components/response-layout-editor";
import { FormUnifiedColumnLayout, UseResponseFormLayoutSetting } from "../types";
import { DgControl } from "@farris/ui-vue/components/designer-canvas";
import { useResponseLayoutEditorSetting } from
    "../../../response-layout-editor/src/composition/converter/use-response-layout-editor-setting";

export function useResponseFormLayoutSetting(formShemaService, componentId: string): UseResponseFormLayoutSetting {
    function getFormNode(formSchemaId, formNode = null): any {
        if (formNode) { return formNode; }
        const componentNode = formShemaService.getComponentById(componentId);
        return formShemaService.selectNode(componentNode, item => item.id === formSchemaId);
    }
    function checkIsInFormComponent(componentId: string): boolean {
        const componentNode = formShemaService.getComponentById(componentId);
        if (!componentNode || !componentNode.componentType || !componentNode.componentType.startsWith('form')) {
            return false;
        }
        return true;

    }
    
    /**
     * 校验宽度样式值是否一致
     */
    function checkIsUniqueColumn(columnsInScreen: number[]) {
        const keySet = new Set(columnsInScreen);
        const exclusiveKeys = Array.from(keySet);

        if (exclusiveKeys.length === 1) {
            return true;
        }
        return false;
    }
    /**
     * 组装每种屏幕下的宽度样式值，例如col-md-6，则uniqueColClassInMD为6
     */
    function assembleUnifiedLayoutContext(propertyData: any): FormUnifiedColumnLayout {

        const formNode = getFormNode(propertyData.id);
        const responseLayoutService = useResponseLayoutEditorSetting(formShemaService);
        const responseLayoutConfig: ResponseFormLayoutContext[] = [];
        responseLayoutService.getResonseFormLayoutConfig(formNode, responseLayoutConfig, 1);


        // 收集每种屏幕下的列数
        const columnInSMArray = responseLayoutConfig.map(config => config.columnInSM);
        const columnInMDArray = responseLayoutConfig.map(config => config.columnInMD);
        const columnInLGArray = responseLayoutConfig.map(config => config.columnInLG);
        const columnInELArray = responseLayoutConfig.map(config => config.columnInEL);

        // 只有每个控件的宽度都一样时，才认为form上有统一宽度，否则认为是自定义的控件宽度，此处传递null
        const uniqueColClassInSM = checkIsUniqueColumn(columnInSMArray) ? columnInSMArray[0] : 0;
        const uniqueColClassInMD = checkIsUniqueColumn(columnInMDArray) ? columnInMDArray[0] : 0;
        const uniqueColClassInLG = checkIsUniqueColumn(columnInLGArray) ? columnInLGArray[0] : 0;
        const uniqueColClassInEL = checkIsUniqueColumn(columnInELArray) ? columnInELArray[0] : 0;

        return {
            uniqueColClassInSM,
            uniqueColClassInMD,
            uniqueColClassInLG,
            uniqueColClassInEL
        };
    }

    /**
     * 根据统一配置值，修改某一个控件的class样式
     */
    function changeControlClassInByColumn(controlClass: string, unifiedLayout: FormUnifiedColumnLayout) {
        let originColClass;
        let originColMDClass;
        let originColXLClass;
        let originColELClass;
        let otherClassItems = [] as any;
        if (controlClass) {
            const controlClassArray = controlClass.split(' ');
            const colClassItems = controlClassArray.filter(classItem => classItem.startsWith('col-'));
            originColClass = colClassItems.find(item => /^col-([1-9]|10|11|12)$/.test(item));
            originColMDClass = colClassItems.find(item => /^col-md-([1-9]|10|11|12)$/.test(item));
            originColXLClass = colClassItems.find(item => /^col-xl-([1-9]|10|11|12)$/.test(item));
            originColELClass = colClassItems.find(item => /^col-el-([1-9]|10|11|12)$/.test(item));
            otherClassItems = controlClassArray.filter(classItem => !classItem.startsWith('col-'));
        }
        const colClass = unifiedLayout.uniqueColClassInSM ? 'col-' + unifiedLayout.uniqueColClassInSM : originColClass;
        const colMDClass = unifiedLayout.uniqueColClassInMD ? 'col-md-' + unifiedLayout.uniqueColClassInMD : originColMDClass;
        const colXLClass = unifiedLayout.uniqueColClassInLG ? 'col-xl-' + unifiedLayout.uniqueColClassInLG : originColXLClass;
        const colELClass = unifiedLayout.uniqueColClassInEL ? 'col-el-' + unifiedLayout.uniqueColClassInEL : originColELClass;

        const newClassItems = [colClass, colMDClass, colXLClass, colELClass].concat(otherClassItems);

        return newClassItems.join(' ');
    }

    /**
     * 根据统一配置值，修改卡片区域内所有控件的class样式
     * @param formNode dom节点
     * @param unifiedLayout 统一配置值
     */
    function changeFormControlsByUnifiedLayoutConfig(formNode: any, unifiedLayout: FormUnifiedColumnLayout, formSchemaId) {
        formNode = getFormNode(formSchemaId, formNode);
        formNode.contents.forEach(control => {
            if (control.type === DgControl['fieldset'].type) {
                changeFormControlsByUnifiedLayoutConfig(control, unifiedLayout, control.id);
                return;
            }
            if (!control.appearance) {
                control.appearance = {};
            }
            const controlClass = control.appearance.class;
            control.appearance.class = changeControlClassInByColumn(controlClass, unifiedLayout);

        });
    }

    return { checkIsInFormComponent, assembleUnifiedLayoutContext, changeFormControlsByUnifiedLayoutConfig };
}
