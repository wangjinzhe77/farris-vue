import { inject } from 'vue';
import { DesignerHostService } from '../../../designer-canvas/src/composition/types';
import { DynamicResolver } from '../../../../components/dynamic-resolver';
import { ComponentBuildInfo } from '../../../component/src/composition/inner-component-build-info';
import { ComponentSchema } from '../../../../components/designer-canvas';
import { FormSchemaEntityFieldTypeName } from '../../../common/entity/entity-schema';
import { cloneDeep } from 'lodash-es';

const ROOT_VIEW_MODEL_ID = 'root-viewmodel';
/**
 * 创建卡片类组件服务类
 */
export class ResponseFormComponentCreatorService {

    private formSchemaUtils: any;
    private controlCreatorUtils: any;
    private designViewModelUtils: any;

    constructor(
        private resolver: DynamicResolver,
        private designerHostService: DesignerHostService
    ) {
        this.formSchemaUtils = this.designerHostService.formSchemaUtils;
        this.controlCreatorUtils = this.designerHostService.controlCreatorUtils;
        this.designViewModelUtils = this.designerHostService.designViewModelUtils;
    }

    public createComponent(buildInfo: ComponentBuildInfo) {
        const componentRefNode = this.createComponentRefNode(buildInfo);

        const componentNode = this.createComponentNode(buildInfo);

        const viewModelNode = this.createViewModeNode(buildInfo);

        const formSchema = this.formSchemaUtils.getFormSchema();
        formSchema.module.viewmodels.push(viewModelNode);
        formSchema.module.components.push(componentNode);

        this.designViewModelUtils.assembleDesignViewModel();

        return componentRefNode;
    }
    createComponentRefNode(buildInfo: ComponentBuildInfo): any {
        const componentRefNode = this.resolver.getSchemaByType('component-ref') as ComponentSchema;
        Object.assign(componentRefNode, {
            id: `${buildInfo.componentId}-component-ref`,
            component: `${buildInfo.componentId}-component`,
        });
        return componentRefNode;
    }

    createComponentNode(buildInfo: ComponentBuildInfo): any {
        const componentNode = this.resolver.getSchemaByType('component') as ComponentSchema;
        const contents = this.createFormComponentContents(buildInfo);
        Object.assign(componentNode, {
            id: `${buildInfo.componentId}-component`,
            viewModel: `${buildInfo.componentId}-component-viewmodel`,
            componentType: buildInfo.componentType,
            appearance: {
                class: this.getFormComponentClass()
            },
            formColumns: buildInfo.formColumns,
            contents
        });
        return componentNode;
    }


    /**
     * 获取卡片组件层级的class样式
     */
    private getFormComponentClass(): string {
        const {templateId} = this.formSchemaUtils.getFormSchema().module;

        // 双列表标签页模板中拖入卡片
        if (templateId === 'double-list-in-tab-template') {
            return 'f-struct-wrapper f-utils-fill-flex-column';
        }
        return 'f-struct-wrapper';

    }

    private createFormComponentContents(buildInfo: ComponentBuildInfo) {
        // 1、创建section
        const section = this.resolver.getSchemaByType('section') as ComponentSchema;
        Object.assign(section, {
            id: buildInfo.componentId + '-form-section',
            appearance: {
                class: 'f-section-form f-section-in-mainsubcard'
            },
            mainTitle: buildInfo.componentName
        });

        // 2、创建form(默认控件标签独占一列)
        const responseForm = this.resolver.getSchemaByType('response-form') as ComponentSchema;
        const controls: any[] = [];
        Object.assign(responseForm, {
            id: buildInfo.componentId + '-form',
            appearance: {
                class: 'f-form-layout farris-form farris-form-controls-inline'
            },
            contents: controls
        });
        section.contents = [responseForm];

        // 3、创建字段
        const { selectedFields } = buildInfo;
        selectedFields?.forEach(field => {
            const dgVMField = cloneDeep(field);
            const resolvedControlClass = this.resolveControlClassByFormColumns(buildInfo);
            const fieldMetadata = this.controlCreatorUtils.setFormFieldProperty(dgVMField, '', resolvedControlClass);

            if (fieldMetadata) {
                controls.push(fieldMetadata);
            }
        });

        // 双列表标签页模板中拖入卡片，要求卡片要填充
        const {templateId} = this.formSchemaUtils.getFormSchema().module;
        if (templateId === 'double-list-in-tab-template') {
            section.appearance.class = 'f-section-grid f-section-in-main px-0 pt-0';
            section.fill = true;
        }

        return [section];
    }

    private resolveControlClassByFormColumns(buildInfo: ComponentBuildInfo) {
        let className = '';
        switch (buildInfo.formColumns) {
            case 1: {
                className = 'col-12';
                break;
            }
            case 2: {
                className = 'col-12 col-md-6 col-xl-6 col-el-6';
                break;
            }
            case 3: {
                className = 'col-12 col-md-6 col-xl-4 col-el-4';
                break;
            }
            case 4: {
                className = 'col-12 col-md-6 col-xl-3 col-el-2';
                break;
            }
        }

        return className;
    }
    /**
     * 添加viewModel节点
     */
    createViewModeNode(buildInfo: ComponentBuildInfo): any {
        const viewModelNode = {
            id: `${buildInfo.componentId}-component-viewmodel`,
            code: `${buildInfo.componentId}-component-viewmodel`,
            name: buildInfo.componentName,
            bindTo: buildInfo.bindTo,
            parent: ROOT_VIEW_MODEL_ID,
            fields: this.assembleViewModelFields(buildInfo),
            commands: [],
            states: [],
            enableValidation: true
        };
        return viewModelNode;
    }

    /**
     * 组装viewModel fields 节点
     */
    private assembleViewModelFields(buildInfo: ComponentBuildInfo) {

        const vmFields: any[] = [];
        const { selectedFields } = buildInfo;
        selectedFields?.forEach(field => {
            let updateOn = 'blur';
            const type = field.type.name;
            if (type === FormSchemaEntityFieldTypeName.Enum || type === FormSchemaEntityFieldTypeName.Boolean) {
                updateOn = 'change';
            }

            vmFields.push({
                type: 'Form',
                id: field.id,
                fieldName: field.bindingField,
                groupId: null,
                groupName: null,
                updateOn,
                fieldSchema: {}
            });
        });
        return vmFields;
    }

}
