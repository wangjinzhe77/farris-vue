
import { ComponentBuildInfo } from "../../../component/src/composition/inner-component-build-info";
import { DesignerHostService } from "../../../designer-canvas/src/composition/types";
import { DesignerComponentInstance } from "../../../designer-canvas/src/types";
import { DynamicResolver } from "../../../dynamic-resolver";
import { ResponseFormComponentCreatorService } from "../composition/response-form-component-creator.service";

export function reponseFormSchemaResolver(resolver: DynamicResolver, schema: Record<string, any>, context: Record<string, any>, designerHostService?: DesignerHostService): Record<string, any> {
    const parentComponentInstance = context.parentComponentInstance as DesignerComponentInstance;
    if (parentComponentInstance && designerHostService) {
        const parentComponentType = parentComponentInstance.schema?.type;
        const radomNumber = Math.random().toString(36).slice(2, 6);
        const componentBuildInfo: ComponentBuildInfo = {
            componentId: `form-${radomNumber}`,
            componentName: context.bindingSourceContext?.entityTitle || context.bindingSourceContext?.bindingEntity?.name || `标题`,
            componentType: 'form',
            formColumns: parentComponentType === 'splitter-pane' ? 1 : 4,
            parentContainerId: parentComponentInstance.schema.id,
            bindTo: context.bindingSourceContext?.bindTo || '/',
            selectedFields: context.bindingSourceContext?.bindingEntityFields
        };
        const componentCreator = new ResponseFormComponentCreatorService(resolver, designerHostService);
        const componentRefNode = componentCreator.createComponent(componentBuildInfo);

        return componentRefNode;
    } else {
        return schema;
    }
}

export function layoutSettingSchemaResolver(resolver: DynamicResolver, schema: Record<string, any>, context: Record<string, any>): Record<string, any> {
    return schema;
}
