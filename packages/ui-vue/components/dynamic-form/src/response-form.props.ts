import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import { reponseFormSchemaResolver } from './schema/response-form-schema-resolver';
import formSchema from './schema/response-form.schema.json';

export const responseFormProps = {
    customClass: { type: String, default: '' },
    customStyle: { type: String, defaut: '' },
} as Record<string, any>;

export type ResponseFormPropsType = ExtractPropTypes<typeof responseFormProps>;

export const formPropsResolver = createPropsResolver<ResponseFormPropsType>(responseFormProps, formSchema, schemaMapper, reponseFormSchemaResolver);
