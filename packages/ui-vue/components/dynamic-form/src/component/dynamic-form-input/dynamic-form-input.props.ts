import { ExtractPropTypes, PropType } from 'vue';
import { EditorConfig } from '../../types';
import { createPropsResolver } from '../../../../dynamic-resolver';
import { schemaMapper } from '../../schema/schema-mapper';
import formGroupSchema from '../../schema/form-group.schema.json';

export const dynamicFormInputProps = {
    id: { type: String, default: '' },
    /** 筛选组件配置器，具体配置项可查看各组件文档 */
    editor: { type: Object as PropType<EditorConfig>, default: {} },
    /** 组件值 */
    modelValue: { type: [String, Boolean, Array, Number], default: '' },
    /**
     * 作为内嵌编辑器被创建后默认获得焦点
     */
    focusOnCreated: { type: Boolean, default: false },
    /**
     * 作为内嵌编辑器被创建后默认选中文本
     */
    selectOnCreated: { type: Boolean, default: false }
} as Record<string, any>;

export type DynamicFormInputProps = ExtractPropTypes<typeof dynamicFormInputProps>;

export const formInputPropsResolver = createPropsResolver<DynamicFormInputProps>(
    dynamicFormInputProps, formGroupSchema, schemaMapper, undefined
);
