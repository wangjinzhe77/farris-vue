import { SetupContext, computed, defineComponent, ref, watch } from 'vue';
import { useTypeResolver } from '../../composition/use-type-resolver';
import { DynamicFormInputProps, dynamicFormInputProps } from './dynamic-form-input.props';

export default defineComponent({
    name: 'FDynamicFormInput',
    props: dynamicFormInputProps,
    emits: ['change', 'update:modelValue'] as (string[] & ThisType<void>) | undefined,
    setup(props: DynamicFormInputProps, context: SetupContext) {
        const id = ref(props.id);
        const editor = ref(props.editor);
        const modelValue = ref(props.modelValue);

        const { resolveEditorProps, resolveEditorType, getChangeFunctionName } = useTypeResolver();

        function onChange(newValue: any, newModelValue: any) {
            modelValue.value = newModelValue !== undefined ? newModelValue : newValue;
            context.emit('update:modelValue', modelValue.value);
            context.emit('change', modelValue.value);
        }

        const renderConditionEditor = computed(() => {
            const Component = resolveEditorType(editor.value.type);
            const editorProps = resolveEditorProps(editor.value.type, editor.value);
            editorProps.focusOnCreated = props.focusOnCreated;
            editorProps.selectOnCreated = props.selectOnCreated;
            const changeFunctionName = getChangeFunctionName(editor.value.type);
            editorProps[changeFunctionName] = onChange;

            if (editorProps.id == null || editorProps.id === '') {
                editorProps.id = id.value;
            }

            return () => <Component {...editorProps} v-model={modelValue.value}></Component>;
        });

        watch(
            [
                () => props.id,
                () => props.editor,
                () => props.modelValue
            ],
            ([newId, newEditor, newModelValue]) => {
                id.value = newId;
                editor.value = newEditor;
                modelValue.value = newModelValue;
            }
        );

        return () => {
            return renderConditionEditor.value();
        };
    }
});
