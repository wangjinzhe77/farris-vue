import { ExtractPropTypes, PropType } from 'vue';
import { EditorType, EditorConfig } from '../../types';
import { createPropsResolver } from '../../../../dynamic-resolver';
import { schemaMapper } from '../../schema/schema-mapper';
import formGroupSchema from '../../schema/form-group.schema.json';
import { createFormGroupEditorResolver } from '../../../../dynamic-resolver/src/editor-resolver';

export const dynamicFormGroupProps = {
    id: { type: String, default: '' },
    customClass: { type: String, default: '' },
    customStyle: { type: String, default: '' },
    /** 筛选组件配置器，具体配置项可查看各组件文档 */
    editor: { type: Object as PropType<EditorConfig>, default: {} },
    label: { type: String, default: '' },
    /** 组件值 */
    modelValue: { type: [String, Boolean, Array, Number], default: '' },
    // readonly: { type: Boolean, default: false },
    visible: { type: Boolean, default: true },
    required: { type: Boolean, default: false },
    showLabel: { type: Boolean, default: true },
    type: { type: String as PropType<EditorType>, default: 'input-group' }
} as Record<string, any>;

export type DynamicFormGroupPropsType = ExtractPropTypes<typeof dynamicFormGroupProps>;

export const formGroupPropsResolver = createPropsResolver<DynamicFormGroupPropsType>(
    dynamicFormGroupProps, formGroupSchema, schemaMapper, undefined
);

export const editorResolver = createFormGroupEditorResolver();
