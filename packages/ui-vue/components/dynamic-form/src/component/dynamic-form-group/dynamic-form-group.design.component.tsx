import { SetupContext, computed, defineComponent, ref, watch } from 'vue';
import { DynamicFormGroupPropsType, dynamicFormGroupProps } from './dynamic-form-group.props';
import Label from '../dynamic-form-label/dynamic-form-label.component';
import { useTypeResolverDesign } from '../../composition/use-type-resolver-design';

export default defineComponent({
    name: 'FDynamicFormGroupDesign',
    props: dynamicFormGroupProps,
    emits: ['change', 'update:modelValue'] as (string[] & ThisType<void>) | undefined,
    setup(props: DynamicFormGroupPropsType, context: SetupContext) {
        const id = ref(props.id);
        const customClass = ref(props.customClass);
        const editor = ref(props.editor);
        const label = ref(props.label);
        const modelValue = ref(props.modelValue);
        const readonly = ref(true);
        const required = ref(props.editor?.required === true);
        const showLabel = ref(props.showLabel);
        const type = ref(props.type);
        const editorRef = ref();

        const { resolveEditorProps, resolveEditorType } = useTypeResolverDesign();
        const conditionItemClass = computed(() => {
            const classObject = {
                'form-group': true,
                'farris-form-group': true,
                'common-group': true,
                'q-state-readonly': readonly.value,
                'form-group-in-canvas': true
            } as Record<string, boolean>;
            return classObject;
        });

        const renderConditionEditor = computed(() => {
            const editorType = editor.value.type || 'input-group';
            const Component = resolveEditorType(editorType);
            const editorProps = resolveEditorProps(editorType, editor.value);
            return () => <Component ref={editorRef} {...editorProps} v-model={modelValue.value}></Component>;
        });
        watch(
            [
                () => props.id,
                () => props.customClass,
                () => props.editor,
                () => props.label,
                () => props.modelValue,
                () => props.readonly,
                () => props.showLabel
            ],
            ([newId, newCustomClass, newEditor, newLabel, newModelValue, newReadonly, newShowLabel]) => {
                id.value = newId;
                customClass.value = newCustomClass;
                editor.value = {...newEditor};
                label.value = newLabel;
                modelValue.value = newModelValue;
                readonly.value = newReadonly;
                required.value = editor.value?.required === true;
                showLabel.value = newShowLabel;
            }, { deep: true }
        );
        context.expose({ editorRef });

        return () => {
            return (
                <div id={`${id.value}-input-group`} class={customClass.value} design_test>
                    <div class="farris-group-wrap">
                        <div class={conditionItemClass.value}>
                            {showLabel.value && (
                                <Label id={`${id.value}-lable`} required={required.value} text={label.value} title={label.value}></Label>
                            )}
                            <div class="farris-input-wrap">{renderConditionEditor.value()}</div>
                        </div>
                    </div>
                </div>
            );
        };
    }
});
