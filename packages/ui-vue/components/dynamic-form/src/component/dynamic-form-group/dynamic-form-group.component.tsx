
import { SetupContext, computed, defineComponent, ref, watch } from 'vue';
import { DynamicFormGroupPropsType, dynamicFormGroupProps } from './dynamic-form-group.props';
import Label from '../dynamic-form-label/dynamic-form-label.component';
import { useTypeResolver } from '../../composition/use-type-resolver';

export default defineComponent({
    name: 'FDynamicFormGroup',
    props: dynamicFormGroupProps,
    emits: ['change', 'update:modelValue'] as (string[] & ThisType<void>) | undefined,
    setup(props: DynamicFormGroupPropsType, context: SetupContext) {
        const id = ref(props.id);
        const customClass = ref(props.customClass);
        const editor = ref(props.editor);
        const label = ref(props.label);
        const modelValue = ref(props.modelValue);
        const readonly = ref(props.readonly);
        const required = ref(props.editor?.required || props.required);
        const showLabel = ref(props.showLabel);
        const visible = ref(props.visible);
        const type = ref(props.type);
        const editorRef = ref();

        const { resolveEditorProps, resolveEditorType, getChangeFunctionName, getClearFunctionName } = useTypeResolver();

        const conditionItemClass = computed(() => {
            const classObject = {
                'form-group': true,
                'farris-form-group': true,
                'common-group': true,
                'q-state-readonly': readonly.value
            } as Record<string, boolean>;
            return classObject;
        });

        function onChange(newValue: any, newModelValue: any) {
            modelValue.value = newModelValue !== undefined ? newModelValue : newValue;
            context.emit('update:modelValue', modelValue.value);
            context.emit('change', modelValue.value);
        }

        function onClear() {
            context.emit('update:modelValue', '');
        }

        const renderConditionEditor = computed(() => {
            const editorType = editor.value.type || 'input-group';
            const Component = resolveEditorType(editorType);
            const editorProps = resolveEditorProps(editorType, editor.value);
            const changeFunctionName = getChangeFunctionName(editorType);
            const clearFunctionName = getClearFunctionName(editor.value.type);
            if (clearFunctionName) {
                editorProps[clearFunctionName] = onClear;
            }
            editorProps[changeFunctionName] = onChange;
            if (editorProps.id == null || editorProps.id === '') {
                editorProps.id = id.value;
            }
            if(editorType === 'number-range' && editor.value.onBeginValueChange && typeof editor.value.onBeginValueChange === 'function') {
                if(editor.value.onBeginValueChange && typeof editor.value.onBeginValueChange === 'function') {
                    editorProps.onBeginValueChange = editor.value.onBeginValueChange;
                }
                if(editor.value.onEndValueChange && typeof editor.value.onEndValueChange === 'function') {
                    editorProps.onEndValueChange = editor.value.onEndValueChange;
                }
            }else if(editorType === 'lookup' && editor.value['onUpdate:idValue'] && typeof editor.value['onUpdate:idValue'] === 'function') {
                editorProps['onUpdate:idValue'] = editor.value['onUpdate:idValue'];
            }
            return () => <Component ref={editorRef} {...editorProps} v-model={modelValue.value} v-slots={context.slots}></Component>;
        });

        function getDraggingDisplayText() {
            return label.value;
        }

        watch(
            [
                () => props.id,
                () => props.customClass,
                () => props.editor,
                () => props.label,
                () => props.modelValue,
                () => props.readonly,
                () => props.required,
                () => props.showLabel,
                () => props.visible
            ],
            ([newId, newCustomClass, newEditor, newLabel, newModelValue, newReadonly, newRequired, newShowLabel, newVisible]) => {
                id.value = newId;
                customClass.value = newCustomClass;
                editor.value = newEditor;
                label.value = newLabel;
                modelValue.value = newModelValue;
                readonly.value = newReadonly;
                required.value = editor.value?.required || newRequired;
                showLabel.value = newShowLabel;
                visible.value = newVisible;
            }
        );
        context.expose({ editorRef });

        return () => {
            return (
                visible.value && (
                    <div id={`${id.value}-input-group`} class={customClass.value} style={props.customStyle}>
                        <div class="farris-group-wrap">
                            <div class={conditionItemClass.value}>
                                {showLabel.value && (
                                    <Label id={`${id.value}-lable`} required={required.value} text={label.value} title={label.value}></Label>
                                )}
                                <div class="farris-input-wrap">{renderConditionEditor.value()}</div>
                            </div>
                        </div>
                    </div>
                )
            );
        };
    }
});
