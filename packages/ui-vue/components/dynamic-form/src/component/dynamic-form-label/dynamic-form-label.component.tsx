import { SetupContext, defineComponent, ref, watch } from "vue";
import { dynamicFormLabelProps, DynamicFormLabelProps } from "./dynamic-form-label.props";

export default defineComponent({
    name: 'FDynamicFormLabel',
    props: dynamicFormLabelProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: DynamicFormLabelProps, context: SetupContext) {
        const title = ref<string>(props.title || props.text);
        const required = ref<boolean>(props.required);
        const text = ref<string>(props.text);

        watch(() => props.text, () => {
            text.value = props.text;
        });

        watch(() => props.required, (newValue, oldValue) => {
            if (newValue !== oldValue) {
                required.value = newValue;
            }
        });

        return () => {
            return (
                text.value&&<label class="col-form-label" title={title.value}>
                    {required.value && <span class="farris-label-info text-danger" >*</span>}
                    <span class="farris-label-text">{text.value}</span>
                </label>
            );
        };
    }
});
