import { ExtractPropTypes } from "vue";

export const dynamicFormLabelProps = {
    id: { type: String },
    required: { type: Boolean, default: false },
    text: { type: String, default: '' },
    title: { type: String }
};

export type DynamicFormLabelProps = ExtractPropTypes<typeof dynamicFormLabelProps>;
