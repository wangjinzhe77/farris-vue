export type EditorType = 'button-edit' | 'check-box' | 'check-group' | 'combo-list' | 'combo-lookup' | 'combo-tree' |
    'date-picker' | 'date-range' | 'datetime-picker' | 'datetime-range' | 'events-editor' | 'month-picker' | 'month-range' |
    'year-picker' | 'year-range' | 'input-group' | 'lookup' | 'number-range' | 'number-spinner' | 'radio-group' | 'text' |
    'response-layout-editor-setting' | 'switch' | 'grid-field-editor' | 'field-selector' | 'schema-selector' | 'mapping-editor' |
    'textarea' | 'response-form-layout-setting'|'binding-selector' | 'query-solution-config' | 'solution-preset' | 'item-collection-editor';

export interface EditorConfig {
    /** 编辑器类型 */
    type: EditorType;
    /** 自定义样式 */
    customClass?: string;
    /** 禁用 */
    disabled?: boolean;
    /** 只读 */
    readonly?: boolean;
    /** 必填 */
    required?: boolean;
    /** 提示文本 */
    placeholder?: string;
    /** 其他属性 */
    [key: string]: any;
}
export interface FormUnifiedColumnLayout {
    uniqueColClassInSM: number;
    uniqueColClassInMD: number;
    uniqueColClassInLG: number;
    uniqueColClassInEL: number;
}
export interface UseResponseFormLayoutSetting {
    checkIsInFormComponent: (componentId: string) => boolean;
    assembleUnifiedLayoutContext: (propertyData: any) => FormUnifiedColumnLayout;
    changeFormControlsByUnifiedLayoutConfig: (formNode: any, unifiedLayout: FormUnifiedColumnLayout, formSchemaId: string) => void;
}
