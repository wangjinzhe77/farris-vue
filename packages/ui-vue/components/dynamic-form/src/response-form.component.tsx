import { SetupContext, computed, defineComponent, ref } from 'vue';
import { ResponseFormPropsType, responseFormProps } from './response-form.props';

export default defineComponent({
    name: 'FResponseForm',
    props: responseFormProps,
    emits: [],
    setup(props: ResponseFormPropsType, context) {
        const elementRef = ref();

        const responseFormClass = computed(() => {
            const customClassArray = props.customClass.split(' ');
            const classObject = {
                'drag-container': true
            } as Record<string, boolean>;
            customClassArray.reduce((result: Record<string, boolean>, classString: string) => {
                result[classString] = true;
                return result;
            }, classObject);
            return classObject;
        });
        /**
         * 运行时增加 v-area-response指令，支持在拖拽改变区域大小时输入控件的响应式
         */
        return () => {
            return (
                <div ref={elementRef} class={responseFormClass.value} style={props.customStyle} v-area-response>
                    {context.slots.default && context.slots.default()}
                </div>
            );
        };
    }
});
