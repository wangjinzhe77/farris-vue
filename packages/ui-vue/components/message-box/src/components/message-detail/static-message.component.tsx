import { ComputedRef } from "vue";

export default function (safeMessageDetail: ComputedRef<string>) {
    return () => {
        return <p class="toast-msg-detail" v-html={safeMessageDetail.value}></p>;
    };
}
