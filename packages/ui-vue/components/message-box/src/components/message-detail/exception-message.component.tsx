import { Ref, computed, ref } from "vue";
import { ExceptionInfo } from "../../message-box.props";

export default function (exception: Ref<ExceptionInfo | null>) {

    // const showLines = ref(3);
    // const exceptionMessageMaxHeight = ref(480);

    const shouldShowExceptionDate = computed(() => {
        return !!exception.value && !!exception.value.date;
    });

    const exceptionDateMessage = computed(() => {
        const exceptionDate = (exception.value && exception.value.date) || '';
        return `发生时间 : ${exceptionDate}`;
    });

    const shouldShowExceptionMessage = computed(() => {
        return !!exception.value && !!exception.value.detail;
    });

    const safeExceptionMessage = computed(() => {
        const safeMessage = (exception.value && exception.value.detail) || '';
        return '详细信息 : ' + safeMessage;
    });

    const exceptionMessageId = 'exp_switch_' + new Date().getTime();

    // const exceptionMessageStyle = computed(() => {
    //     const maxHeight = `${exceptionMessageMaxHeight.value}px`;
    //     const styleObject = {
    //         overflow: 'hidden',
    //         'text-overflow': 'ellipsis',
    //         display: '-webkit-box',
    //         '-webkit-box-orient': 'vertical',
    //         '-webkit-line-clamp': showLines.value,
    //         'max-height': maxHeight
    //     };
    //     return styleObject;
    // });

    // const shouldShowExpandHandle = computed(() => {
    //     return true;
    // });

    // const expandExceptionMessage = ref(false);
    // const expandText = ref('展开');
    // const collapseText = ref('收起');
    // const expandHandleStyle = computed(() => {
    //     const styleObject = {
    //         display: 'block',
    //         color: '#2A87FF'
    //     } as Record<string, any>;
    //     styleObject['text-align'] = expandExceptionMessage.value ? '' : 'right';
    //     return styleObject;
    // });

    // function toggalExceptionMessage(expand: boolean, $event: Event) {
    //     expandExceptionMessage.value = !expandExceptionMessage.value;
    //     showLines.value = expandExceptionMessage.value ? 20 : 3;
    // }

    // function onClickExpand(payload: MouseEvent) {
    //     return toggalExceptionMessage(true, payload);
    // }

    // function onClickCollapse(payload: MouseEvent) {
    //     return toggalExceptionMessage(true, payload);
    // }

    return () => {
        return (
            <div class="toast-msg-detail">
                {shouldShowExceptionDate.value && <div>{exceptionDateMessage.value}</div>}
                {shouldShowExceptionMessage.value && (
                    /* <div id="exception_error_msg" class="exception_error_msg" ref="exceptionMessageRef" style={exceptionMessageStyle.value}>
                        <input type="checkbox" id="exp_switch" class="d-none exp_switch" />
                        <div>详细信息:
                            <label class="swith" for="exp_switch"></label>
                            <span v-html={safeExceptionMessage.value}></span>
                        </div>
                    </div> */

                    <div class="exception_wrap">
                        <input type="checkbox" id={exceptionMessageId} class="d-none exp_switch" />
                        <div class="exception_error_msg">
                            <label class="swith" htmlFor={exceptionMessageId}></label>
                            <div v-html={safeExceptionMessage.value}></div>
                        </div>
                    </div>


                )}
                {/* {shouldShowExpandHandle.value && (
                    <span style={expandHandleStyle.value}>
                        {expandExceptionMessage.value && (
                            <span onClick={onClickExpand} style="cursor: pointer;">
                                {collapseText.value}
                            </span>
                        )}
                        {!expandExceptionMessage.value && (
                            <span onClick={onClickCollapse} style="cursor: pointer;">
                                {expandText.value}
                            </span>
                        )}
                    </span>
                )} */}
            </div>
        );
    };

}
