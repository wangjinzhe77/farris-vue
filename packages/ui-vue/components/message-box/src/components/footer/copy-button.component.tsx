import { computed, ref } from "vue";
import { UseCopy, UseFeedback } from "../../composition/types";
import { ExceptionInfo, MessageBoxProps } from "../../message-box.props";

export default function (
    props: MessageBoxProps,
    useCopyComposition: UseCopy,
    useFeedbackComposition: UseFeedback
) {

    const { onCopy } = useCopyComposition;
    const { toShowFeedback } = useFeedbackComposition;
    const copyFeedback = ref('复制成功');
    const copyButtonText = ref('复制详细信息');
    const exception = ref<ExceptionInfo>(props.exceptionInfo || { date: '', message: '', detail: '' });

    const safeExceptionMessage = computed(() => {
        const safeMessage = (exception.value && exception.value.detail) || '';
        return safeMessage;
    });

    function onClickCopyButton($event: MouseEvent) {
        onCopy(safeExceptionMessage.value)
            .catch((reason: any) => {
                copyFeedback.value = '复制失败';
            })
            .finally(() => {
                toShowFeedback.value = true;
                setTimeout(() => {
                    toShowFeedback.value = false;
                }, 700);
            });
    }

    return () => {
        return (
            <span style="width: 100%;color: #2A87FF;padding-left: 37px;">
                <span onClick={onClickCopyButton} style="cursor: pointer;">
                    {copyButtonText.value}
                </span>
            </span>
        );
    };
}
