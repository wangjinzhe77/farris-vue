/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { getCurrentInstance, reactive } from 'vue';
import { FModalService } from '../../modal';
import { ExceptionInfo } from './message-box.props';
import FMessageBox from './message-box.component';

export interface MessageBoxOption {
    width?: number;
    type: string;
    title?: string;
    detail?: string;
    okButtonText?: string;
    cancelButtonText?: string;
    exceptionInfo?: ExceptionInfo;
    acceptCallback?: () => void;
    rejectCallback?: () => void;
    buttons?: Array<any>;
}

export default class MessageBoxService {
    static show(options: MessageBoxOption) {

        const props: MessageBoxOption = reactive({
            ...options
        });
        const showButtons = false;
        const showHeader = props.type === 'error' || props.type === 'prompt';
        const title = props.type === 'error' ? '错误提示' : props.type === 'prompt' ? (props.title || '') : '';
        const acceptCallback = props.acceptCallback || (() => { });
        const rejectCallback = props.rejectCallback || (() => { });

        let modalApp: any = null;
        let modalService: FModalService | null = new FModalService(modalApp);
        const onClose = () => {
            if (modalApp) {
                modalApp?.destroy();
                modalService = null;
            }
        };

        modalApp = modalService?.open({
            class: 'modal-message modal-message-type-info',
            title,
            showButtons,
            showHeader,
            width: props.width || 400,
            fitContent: true,
            showMaxButton: false,
            draggable: true,
            render: () => {
                return <FMessageBox {...props} onAccept={acceptCallback} onReject={rejectCallback} onClose={onClose}></FMessageBox>;
            }
        });
        // 增加返回值，方便自定义的命令关闭窗口
        return modalService?.getCurrentModal();
    }

    static info(message: string, detail: string) {
        const props: MessageBoxOption = reactive({
            type: 'info',
            title: message,
            detail,
            okButtonText: '知道了',
            cancelButtonText: ''
        });
        MessageBoxService.show(props);
    }

    static warning(message: string, detail: string) {
        const props: MessageBoxOption = reactive({
            type: 'warning',
            title: message,
            detail,
            okButtonText: '知道了',
            cancelButtonText: ''
        });
        MessageBoxService.show(props);
    }

    static success(message: string, detail: string) {
        const props: MessageBoxOption = reactive({
            type: 'success',
            title: message,
            detail,
            okButtonText: '关闭',
            cancelButtonText: ''
        });
        MessageBoxService.show(props);
    }

    static error(message: string, detail: string, date?: string): any {
        const props: MessageBoxOption = reactive({
            width: 500,
            type: 'error',
            okButtonText: '关闭',
            cancelButtonText: '',
            exceptionInfo: { date, message, detail } as ExceptionInfo
        });
        MessageBoxService.show(props);
    }

    static prompt(message: string, detail: string) {
        const props: MessageBoxOption = reactive({
            type: 'prompt',
            title: message,
            detail,
            okButtonText: '确定',
            cancelButtonText: '取消'
        });
        MessageBoxService.show(props);
    }

    static question(message: string, detail: string, acceptCallback: () => void, rejectCallback: () => void) {
        const props: MessageBoxOption = reactive({
            type: 'question',
            title: message,
            detail,
            okButtonText: '确定',
            cancelButtonText: '取消',
            acceptCallback,
            rejectCallback
        });
        MessageBoxService.show(props);
    }
}
