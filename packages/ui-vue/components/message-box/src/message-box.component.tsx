/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, ref, SetupContext } from 'vue';
import { MessageBoxProps, messageBoxProps } from './message-box.props';
import { useCopy } from './composition/use-copy';
import { useEditor } from './composition/use-editor';
import { useFeedfack } from './composition/use-feedback';
import getReactiveContentRender from './components/message-content/reactive-content.component';
import getStaticContentRender from './components/message-content/static-content.component';
import getFooter from './components/footer/footer.component';

export default defineComponent({
    name: 'FMessageBox',
    props: messageBoxProps,
    emits: ['accept', 'reject', 'close'] as (string[] & ThisType<void>) | undefined,
    setup(props: MessageBoxProps, context: SetupContext) {
        const messageType = ref(props.type);
        const messageTitle = ref(props.title);
        const messageDetail = ref(props.detail);

        const messageBoxContainerClass = computed(() => {
            const classObject = {
                'modal-tips': true,
                'd-flex': true,
                'flex-row': true
            } as Record<string, boolean>;
            const messageBoxTypeClass = `messager-type-${messageType.value}`;
            classObject[messageBoxTypeClass] = true;
            return classObject;
        });

        const messageBoxContainerStyle = computed(() => {
            const styleObject = {} as Record<string, any>;
            if (messageType.value === 'prompt') {
                styleObject.padding = '0.5rem 0.5rem 1rem 1.5rem';
            } else if (messageType.value === 'error') {
                styleObject.padding = '0.5rem 1.5rem 1rem 1.5rem';
            }
            return styleObject;
        });

        const useCopyComposition = useCopy();
        const useEditorComposition = useEditor(props, messageType, messageTitle, messageDetail);
        const useFeedbackComposition = useFeedfack();
        const { feedbackStyle, feedbackMessage } = useFeedbackComposition;

        function renderFeedback() {
            return <div style={feedbackStyle.value}>{feedbackMessage.value}</div>;
        }

        function getContentRender() {
            switch (messageType.value) {
                case 'prompt':
                    return getReactiveContentRender(props, useEditorComposition);
                default:
                    return getStaticContentRender(props, messageType, messageTitle, messageDetail);
            }
        }

        const renderContent = getContentRender();
        const renderFooter = getFooter(props, context, useCopyComposition, useFeedbackComposition);

        return () => {
            return (
                <div class="farris-messager">
                    <section class={messageBoxContainerClass.value} style={messageBoxContainerStyle.value}>
                        {renderContent()}
                    </section>
                    {renderFooter()}
                    {renderFeedback()}
                </div>
            );
        };
    }
});
