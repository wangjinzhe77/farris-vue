/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import propertyConfig from './property-config/text.property-config.json';
import { createPropsResolver } from '../../dynamic-resolver/src/props-resolver';
import textSchema from './schema/text.schema.json';
import { TextType, FormNumberOptions, TextAlignment } from './composition/types';


export const textProps = {
    // 组件值 
    modelValue: { type: [String,Number,Boolean], default: '' },
    // 是否是密码
    password: { type: Boolean, default: false },
    // 是否多行文本
    textarea: { type: Boolean, default: false },
    // 是否自动尺寸
    autoSize: { type: Boolean, default: false },
    // 是否是日期范围
    dateRange: { type: Boolean, default: false },
    // 最大高度
    maxHeight: { type: Number, default: 0 },
    // 指定高度
    height: { type: Number, default: 0 },
    // 控件类型
    type: { type: String as PropType<TextType>, default: 'string' },
    // 格式化
    format: { type: String, default: '' },
    // 转换类型用到的数据
    enumConvertedDatas: { default: undefined },
    // 千分位符号
    groupSeparator: { type: String, default: ',' },
    // 小数点处符号
    decimalSeparator: { type: String, default: '.' },
    // 十进制补位符
    decimalFilledSymbol: { type: String, default: '0' },
    // 数字参数
    numberOptions: { type: Object as PropType<FormNumberOptions>, default: undefined },
    // 金额表达式
    amountExpression: { type: String, default: '%s%v' },
    // 文本字段
    textField: { type: String, default: 'name' },
    // 值字段
    valueField: { type: String, default: 'value' },
    // 文本方向
    textAlign: { type: String as PropType<TextAlignment> , default: '' },
    // 日期范围模式下input的显示分割符合
    dateRangeDelimiter: { type: String, default: '~' },
    // 枚举项的分隔符
    enumDelimiter: { type: String, default: ',' }
} as Record<string, any>;

export type TextProps = ExtractPropTypes<typeof textProps>;

export const propsResolver = createPropsResolver<TextProps>(textProps, textSchema, schemaMapper, schemaResolver, propertyConfig);
