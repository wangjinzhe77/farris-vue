import { useDateFormat, useNumberFormat, useTimeAgo } from "../../../common";
import { FormNumberOptions } from "./types";

/**
 * 处理年，Y=>y, D=>d
 * @param format
 * @returns
 */
export function dateToUpperCase(format: string): string {
    let newFormat: string;
    if (/Y/g.test(format) === true) {
        newFormat = format.replace(/Y/g, 'y');
    } else {
        newFormat = format;
    }
    if (/D/g.test(newFormat) === true) {
        newFormat = newFormat.replace(/D/g, 'd');
    }

    return newFormat;
}
/**
* 枚举类型处理
*/
export function getFormatEnumValue(
    value: string | undefined | null,
    data: Array<any>,
    delimiter: string,
    textField: string,
    valueField: string
): string {
    if (value === undefined || value === null || value === '') {
        return '';
    }
    if (data && data.length) {
        // 可能存在多选枚举，作为分隔符
        // let curVals = ('' + value).split(',');
        const splitValues = ('' + value).split(delimiter);
        const nameResult: any[] = [];
        for (let k = 0; k < splitValues.length; k++) {
            const objectFound = data.find(item => {
                return item[valueField] === splitValues[k];
            });
            if (objectFound) {
                nameResult.push(objectFound[textField]);
            }
            else {
                nameResult.push(splitValues[k]);
            }
        }
        if (nameResult.length > 0) {
            return nameResult.join(delimiter);
        }
        return '';
    }
    return value;

}
/*
* 日期格式化
*/
export function dateformat(value: any, format: string): string {
    let newValue;
    if (!format) {
        format = 'YYYY-MM-DD';
    }

    if (format === 'timeago') {
        // 需要再验证
        return useTimeAgo().formatTo(value);
    }

    if (format === 'hh:mm:ss' || format === 'HH:mm:ss') {
        // 格式化时间  临时处理
        newValue = value;
    } else {
        newValue = useDateFormat().formatTo(value, dateToUpperCase(format));
    }
    return newValue;
}

/**
     * C货币
     * D十进制
     * F浮点数
     * N数字，带千分位
     * P百分比
     */

export function getNumberFormat(format: string, options: FormNumberOptions | null | undefined) {
    if (!format && !options) {
        return format;
    }
    if (options && options.type && options.type === 'number') {
        if (!options.formatter) {
            if (options.useThousands) {
                if (options.precision !== undefined) {
                    format = 'n' + options.precision;
                } else {
                    format = 'n2';
                }
            } else if (options.precision !== undefined) {
                format = 'f' + options.precision;
            } else {
                format = 'f2';
            }
        }
    }
    return format;
}

/**
 * 将货币符号首字母转换成大写
 */
function currencyToUpperCase(value: string) {
    return value.replace(/[a-z]+/g, (word: any) => {
        return word.toUpperCase();
    });
}

/**
 * @param value 转换成十进制的数字
 * @param precision 十进制数字的长度
 */
function toDecimal(value: any, precision: number, decimalFilledSymbol: string): string {
    if (value.toString().indexOf('.') > -1) {
        console.warn('十进制转换仅限整型类型');
        return '';
    }
    return (Array(precision).join(decimalFilledSymbol) + value).slice(-precision);
}

/**
 * @param value 转换成百分数的数字
 * @param decimal 小数点保留几位
 */
function toPercent(value: any, precision: any) {
    return Number(value * 100).toFixed(Number(precision)) + '%';
}

/**
 *
 * @param value 待格式化的数值
 * @param format 指定的格式化
 * @param options 类型
 * @param thousand 千分位符号
 * @param decimal 小数点处符号
 * @param expression 货币符号及值展现形式
 * @returns
 */
export function getFormatNumberValue(
    value: any,
    format: string,
    options: FormNumberOptions | null | undefined,
    thousand: string,
    decimal: string,
    expression: string,
    decimalFilledSymbol: string
) {
    if (!format && !options) {
        return value.toString();
    }
    if (options && options.type && options.type === 'number') {
        if (options.formatter) {
            return options.formatter(value);
        }
    }
    const firstLetter = currencyToUpperCase(format.substring(0, 1));
    const precision = Number(format.substring(1));
    const config = {};
    let result: any;
    if (!/C|D|F|N|P/g.test(firstLetter)) {
        // console.log(`不支持format为 ${format} 类型的数字格式化`);
        return;
    }
    if (thousand) {
        Object.assign(config, { groupSeparator: thousand });
    }
    if (decimal) {
        Object.assign(config, { decimalSeparator: decimal });
    }
    if (expression) {
        Object.assign(config, { format: expression });
    }
    const { formatTo } = useNumberFormat();
    switch (firstLetter) {
        case 'C':
            // 此处前缀
            Object.assign(config, { prefix: '￥', precision });
            result = formatTo(value, config);
            break;
        case 'D':
            result = toDecimal(value, precision, decimalFilledSymbol);
            break;
        case 'F':
            Object.assign(config, { prefix: '', groupSeparator: '', precision });
            result = formatTo(value, config);
            break;
        case 'N':
            Object.assign(config, { prefix: '', precision });
            result = formatTo(value, config);
            break;
        case 'P':
            result = toPercent(value, precision);
            break;
    }
    return result;
}

export function getFormatCheckBoxValue(value: boolean) {
    if (value === true) {
        return '是';
    } if (value === false || value == null || value === '' || typeof value === 'undefined') {
        // 因为默认值指定了默认值，当变量为undefined时，此时转换为空串
        return '否';
    }
    return value;

}
