export type TextType = "string" | "date" | "datetime" | "number" | "enum" | "boolean";
export type TextAlignment ="left"|"center"|"right";
export type TextValueType=string|number|boolean;

export interface FormNumberOptions {
    type?: string;
    formatter?: (val: number) => string;
    useThousands?: boolean;
    precision?: number | undefined;
    [key: string]: any;
}
