 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, onMounted, ref, SetupContext, watch } from 'vue';
import { TextProps, textProps } from './text.props';
import { dateformat, getFormatCheckBoxValue, getFormatEnumValue, getFormatNumberValue, getNumberFormat } from './composition/utils';

export default defineComponent({
    name: 'FText',
    props: textProps,
    emits: ['update:modelValue', 'valueChange'] as (string[] & ThisType<void>) | undefined,
    setup(props: TextProps, context: SetupContext) {
        // 是否多行文本
        const isTextarea = ref(props.textarea);
        // 是否密码
        const isPassword = ref(props.password);
        // 能否自动尺寸
        const canAutoSize = ref(props.autoSize);
        // 是否日期范围
        const isDateRange = ref(props.dateRange);
        // 类型
        const textType = ref(props.type);
        // 格式化
        const format = ref(props.format);
        const storeOldValue = ref();
        // 文本框元素
        const textElement = ref();
        // 显示标题
        const textTitle = ref('');

        const textClass = computed(() => ({
            'f-form-control-text': !isTextarea.value,
            'f-form-context-textarea': isTextarea,
            'f-component-text-auto-size': canAutoSize.value
        }));

        // 对齐
        const textStyle = computed(() => {
            return {
                'text-align': props.textAlign,
                'height': !canAutoSize.value && props.height > 0 ? `${props.height}px` : '',
                'min-height': canAutoSize.value && props.height > 0 ? `${props.height}px` : '',
                'max-height': canAutoSize.value && props.maxHeight > 0 ? `${props.maxHeight}px` : ''
            };
        });

        onMounted(() => {
            if (isTextarea.value) {
                const clsName = textElement.value.parentElement.className;
                textElement.value.parentElement.className = clsName + ' f-cmp-text-is-textarea';
            }
        });

        function formatText(textValue) {
            if (isPassword.value) {
                return textValue ? '******' : '';
            }
            if (textType.value !== 'boolean' && (textValue == null || typeof textValue === 'undefined')) {
                return '';
            }
            let result = '';
            switch (textType.value) {
                case 'string':
                    result = textValue;
                    break;
                case 'date':
                case 'datetime':
                    if (!isDateRange.value) {
                        result = dateformat(textValue, format.value);
                    } else {
                        const delimiter = props.dateRangeDelimiter;
                        const rangeDates = textValue.split(delimiter);
                        const dateStrArray: string[] = [];
                        if (rangeDates && rangeDates.length) {
                            rangeDates.forEach((date: string) => {
                                dateStrArray.push(dateformat(date, format.value));
                            });
                        }
                        result = dateStrArray.join(delimiter);
                    }

                    break;
                case 'number':
                    format.value = getNumberFormat(format.value, props.numberOptions);
                    result = getFormatNumberValue(textValue, format.value, props.numberOptions, props.groupSeparator, props.decimalSeparator, props.amountExpression, props.decimalFilledSymbol);
                    break;
                case 'enum':
                    result = getFormatEnumValue(textValue, props.enumConvertedDatas, props.enumDelimiter, props.textField, props.valueField);
                    break;
                case 'boolean':
                    result = getFormatCheckBoxValue(textValue);
                    break;
                default:
                    break;
            }
            return result;
        }

        // 记录真实的显示文本
        const realDisplayValue = computed(() => {
            const newValue = formatText(props.modelValue);
            if (storeOldValue.value !== newValue) {
                context.emit("valueChange", newValue);
                // eslint-disable-next-line vue/no-side-effects-in-computed-properties
                storeOldValue.value = newValue;
            }
            return newValue;
        });

        /**
         * 修改标题
         */
        function changeTitle() {
            const textEle = textElement.value;
            if (Math.abs(textEle.scrollWidth - textEle.clientWidth) > 2 || Math.abs(textEle.scrollHeight - textEle.clientHeight) > 2) {
                textTitle.value = realDisplayValue.value;
            } else {
                textTitle.value = '';
            }
        };

        watch(() => props.modelValue, (newValue, oldValue) => {
            if (newValue !== oldValue) {
                context.emit('update:modelValue', newValue);
            }
        });
        return () => {
            return (
                <span class={textClass.value} style={textStyle.value} ref={textElement} onMouseenter={() => changeTitle()} title={textTitle.value}>
                    {realDisplayValue.value}
                </span>
            );
        };

    }
});
