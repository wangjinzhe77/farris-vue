/* eslint-disable @typescript-eslint/no-unsafe-function-type */
/* eslint-disable no-use-before-define */
import { SetupContext, defineComponent, ref, watch, inject, createVNode, VNode, reactive, Reactive } from 'vue';
import { cloneDeep, merge, mergeWith } from 'lodash-es';
import { dynamicViewProps, DynamicViewProps } from './dynamic-view.props';
import { componentMap, componentPropsConverter, loadRegister, resolverMap } from './components/maps';
import { createEventsResolver, createFormBindingResolver, EditorResolver, SelectionItemResolver, UpdateColumnsResolver } from '../../dynamic-resolver';
import { createEventDispatcher } from './event-dispatcher';
import { useComponentManager } from './composition/use-component-manager';
import { useBindingData } from './composition/use-binding-data';
import { useEntityState } from './composition/use-entity-state';
import { createCallbackDeliver } from './callback-deliver';

const FDynamicView = defineComponent({
    name: 'FDynamicView',
    props: dynamicViewProps,
    emits: ['update:modelValue', 'event', 'componentReady'],
    setup(props: DynamicViewProps, setupContext: SetupContext) {
        const schema = ref(props.schema);
        const modelValue = ref(props.modelValue);
        const callback = ref(props.callback);
        const schemaMap: Map<string, any> = new Map<string, any>();
        const dataSourceMap: Map<string, any> = new Map<string, any>();
        loadRegister();
        const componentManager = useComponentManager();
        const bindingData = useBindingData(modelValue, setupContext);
        const entityState = useEntityState(schema.value);
        entityState.setup();
        // const componentState = new Map<string, Reactive<any>>();
        const state = new Map<string, Reactive<{ props: Record<string, any>; }>>();

        function resolveModels(viewSchema: Record<string, any>) {
            const componentType = viewSchema.type;
            const { dataSource, binding } = viewSchema;
            if (!dataSource && !binding) {
                return {};
            }
            if (dataSource) {
                dataSourceMap.set(dataSource, viewSchema);
            }
            const resolver = resolverMap[componentType];
            const bindingResolver = resolver && resolver.bindingResolver ? resolver.bindingResolver : createFormBindingResolver();
            return bindingResolver.resolve(viewSchema, bindingData);
        }

        function renderSlots(slots?: Record<string, any>) {
            const result: Record<string, Function> = {};
            if (!slots) {
                return result;
            }
            Object.entries(slots).forEach(([name, schema]) => {
                result[name] = () => {
                    if (Array.isArray(schema)) {
                        return schema.map((schema: Record<string, any>) => render(schema));
                    }
                    return render(schema);
                };
            });
            return result;
        }

        function resolveCallbacks(viewSchema: Record<string, any>) {
            const componentKey = viewSchema.type;
            const resolvers = resolverMap[componentKey];
            if (!resolvers || Object.keys(resolvers).length < 1) {
                return {};
            }
            const { callbackResolver } = resolvers;
            if (!callbackResolver) {
                return {};
            }
            const callbackDeliver = createCallbackDeliver(callback.value);
            return callbackResolver.resolve(viewSchema, callbackDeliver);
        }
        function resolveEvents(viewSchema: Record<string, any>) {
            const componentKey = viewSchema.type;
            const eventDispatcher = createEventDispatcher(setupContext, viewSchema);
            const Component = componentMap[componentKey];
            const eventResolver = createEventsResolver();
            const resolver = resolverMap[componentKey];
            const editorResolver: EditorResolver = resolver ? resolver.editorResolver : null;
            if (editorResolver) {
                const editor = editorResolver.resolve(viewSchema);
                const componentType = editor.type;
                const Editor = componentMap[componentType];
                const eventProps = eventResolver ? eventResolver(Editor, viewSchema, eventDispatcher) : {};
                return eventProps;
            }
            const eventProps = eventResolver ? eventResolver(Component, viewSchema, eventDispatcher) : {};
            return eventProps;
        }

        function resolveExtraProps(viewSchema: Record<string, any>) {
            // const componentKey = viewSchema.type;
            // const resolver = resolverMap[componentKey];
            // const extraPropsResolver: ExtraPropsResolver | null = resolver ? resolver.extraPropsResolver : null;
            // if (!extraPropsResolver) {
            //     return {};
            // }
            // return extraPropsResolver.resolve(viewSchema);
            return {};
        }

        function resolveProps(viewSchema: Record<string, any>) {
            const componentKey = viewSchema.type;
            const propsConverter = componentPropsConverter[componentKey];
            const componentProps: Record<string, any> = propsConverter ? propsConverter(viewSchema) : {};
            // const Component = componentMap[componentKey];
            // const componentPropertyProps = Object.keys(Component?.props || {});
            // const componentEventProps = (Component?.emits as string[] || []).map((eventName: string) => `on${eventName.charAt(0).toUpperCase()}${eventName.slice(1)}`);
            // const currentComponentProps = componentPropertyProps.concat(componentEventProps);
            // const componentProps = Object.keys(componentSchema).reduce((props: Record<string, any>, propertyName: string) => {
            //     if (currentComponentProps.indexOf(propertyName) !== -1) {
            //         props[propertyName] = componentSchema[propertyName];
            //     }
            //     return props;
            // }, {});
            const eventProps = {
                ...resolveEvents(viewSchema)
            };
            const viewProps = {
                ...componentProps,
                ...resolveModels(viewSchema),
                ...resolveExtraProps(viewSchema),
                ...resolveCallbacks(viewSchema)
            };
            const props = {
                ...viewProps,
                key: viewSchema.id,
                ref: (componentRef: any) => {
                    if (componentRef && viewSchema.id && !componentManager.has(viewSchema.id)) {
                        componentManager.register(viewSchema.id, componentRef);
                        setupContext.emit('componentReady', { ref: ref(componentRef), id: viewSchema.id, type: viewSchema.type });
                    }
                }
            };
            return { props, eventProps };
        }

        function render(viewSchema: Record<string, any>) {
            const componentKey = viewSchema.type;

            if (componentKey === 'component-ref') {
                const componentSchema = schema.value?.module?.components
                    .find((component: any) => component.id === viewSchema.component);
                if (componentSchema) {
                    return render(componentSchema);
                }
            }

            if (viewSchema.id) {
                schemaMap.set(viewSchema.id, viewSchema);
            }
            const Component = componentMap[componentKey];
            if (!Component) {
                return null;
            }
            // const { props, eventProps } = resolveProps(viewSchema);

            const renderChildren = () => {
                if (!viewSchema.contents) {
                    return null;
                }
                if (typeof viewSchema.contents === 'string') {
                    return viewSchema.contents;
                }
                return viewSchema.contents.map((schema: Record<string, any>) => render(schema));
            };
            // const resolver = resolverMap[componentKey];
            // const editorResolver: EditorResolver = resolver ? resolver.editorResolver : null;
            // if (editorResolver) {
            //     const editor = editorResolver.resolve(viewSchema);
            //     Object.assign(editor, eventProps);
            // } else {
            //     Object.assign(props, eventProps);
            // }
            const createNode = (componentType: any, props: Record<string, any>, children?: null | undefined | any[]) => {
                if (children && children.length > 0) {
                    return createVNode(componentType, { ...props }, children);
                } else {
                    return createVNode(componentType, { ...props }, null);
                }
            };
            // componentState.set(viewSchema.id, reactive({ props }));
            // const reactivedProps = componentState.get(viewSchema.id).props;
            const props = state.get(viewSchema.id)?.props || {};
            if (viewSchema.contents && viewSchema.contents.length > 0) {
                return createNode(Component, props, [renderChildren()]);
            } else if (viewSchema.slots) {
                return createNode(Component, props, [...Object.values(renderSlots(viewSchema.slots))]);
            } else {
                return createNode(Component, props);
            }
        }

        function rerender(component: any) {
            if (component.$forceUpdate) {
                component.$forceUpdate();
            }
        }

        function getSchema(id: string) {
            return schemaMap.get(id);
        }

        function setSchema(id: string, partialSchema: Record<string, any>) {
            const elementSchema = schemaMap.get(id);
            if (!elementSchema) {
                return;
            }
            // const componentInstance = componentManager.get(id);
            // if (!componentInstance) {
            //     return;
            // }
            merge(elementSchema, partialSchema);
            convertSchemaToProps(elementSchema);
            // TODO: 此处不能再更新表格组件了，因为表格组件的列是动态的
            // const componentKey = elementSchema.type;
            // const resolver = resolverMap[componentKey];
            // const updateColumnsResolver: UpdateColumnsResolver = resolver ? resolver.updateColumnsResolver : null;
            // if (updateColumnsResolver) {
            //     updateColumnsResolver.updateColumns(componentInstance, elementSchema);
            // }
            // rerender(componentInstance);
        }

        function getProps(id: string): Record<string, any> {
            const instance = componentManager.get(id);
            return instance.$props || {};
        }

        function setProps(id: string, props: Record<string, any>) {
            const currentState = state.get(id);
            mergeWith(currentState?.props, props, mergeArray);
            // state.set(id, reactive({ props: { ...currentProps, ...props } }));
        }

        function invoke(id: string, method: string, ...args: any[]): any {
            const componentInstance = componentManager.get(id);
            if (!componentInstance) {
                return;
            }
            if (!componentInstance || typeof componentInstance[method] !== 'function') {
                throw new Error(`Method ${method} not found on instance ${id}`);
            }
            return componentInstance[method](...args);
        }

        function selectItemById(bindingPath: string, id: string) {
            const bindingEntityState = entityState.get(bindingPath);
            if (!bindingEntityState) {
                return;
            }
            const entityLabel = bindingEntityState.label;
            const viewSchema = dataSourceMap.get(entityLabel);
            if (!viewSchema) {
                return;
            }
            const componentType = viewSchema.type;
            const resolver = resolverMap[componentType];
            const selectionMethodResolver: SelectionItemResolver | null = resolver ? resolver.selectionItemResolver : null;
            if (selectionMethodResolver) {
                const Component = componentManager.get(viewSchema.id);
                selectionMethodResolver.selectItemById(Component, id);
            }
        }
        function convertSchemaToProps(viewSchema: Record<string, any>) {
            const componentKey = viewSchema.type;
            if (componentKey === 'component-ref') {
                const componentSchema = schema.value?.module?.components
                    .find((component: any) => component.id === viewSchema.component);
                if (componentSchema) {
                    return convertSchemaToProps(componentSchema);
                }
            }
            const Component = componentMap[componentKey];
            if (!Component) {
                return;
            }
            const { props, eventProps } = resolveProps(viewSchema);
            const resolver = resolverMap[componentKey];
            const editorResolver: EditorResolver = resolver ? resolver.editorResolver : null;
            if (editorResolver) {
                const editor = editorResolver.resolve(viewSchema);
                Object.assign(editor, eventProps);
            } else {
                Object.assign(props, eventProps);
            }
            if (props && Object.keys(props).length > 0) {
                const currentState = state.get(viewSchema.id);
                if (!currentState) {
                    state.set(viewSchema.id, reactive({ props }));
                } else {
                    merge(currentState?.props, props);
                }

                // state.set(viewSchema.id, reactive({ props: { ...currentProps, ...props } }));
            }
            if (!viewSchema.contents || !Array.isArray(viewSchema.contents)) {
                return;
            }
            viewSchema.contents.forEach((schema: Record<string, any>) => convertSchemaToProps(schema));
        }

        function convertPartialSchemaToProps(type: string, viewSchema: Record<string, any>) {
            const propsConverter = componentPropsConverter[type];
            const componentProps: Record<string, any> = propsConverter ? propsConverter(viewSchema, false) : {};
            return componentProps;
        }

        function convertModelValueToProps(viewSchema: Record<string, any>) {
            const componentKey = viewSchema.type;
            if (componentKey === 'component-ref') {
                const componentSchema = schema.value?.module?.components
                    .find((component: any) => component.id === viewSchema.component);
                if (componentSchema) {
                    return convertModelValueToProps(componentSchema);
                }
            }
            const Component = componentMap[componentKey];
            if (!Component) {
                return;
            }
            const modelProps = resolveModels(viewSchema);
            if (modelProps && Object.keys(modelProps).length > 0) {
                const currentState = state.get(viewSchema.id);
                if (!currentState) {
                    state.set(viewSchema.id, reactive({ props: modelProps }));
                } else {
                    merge(currentState?.props, modelProps);
                }
                // state.set(viewSchema.id, reactive({ props: { ...currentProps, ...modelProps } }));
            }
            if (!viewSchema.contents || !Array.isArray(viewSchema.contents)) {
                return;
            }
            viewSchema.contents.forEach((schema: Record<string, any>) => convertModelValueToProps(schema));
        }
        function getFrameComponentSchema(): Record<string, any> | null {
            const components: Record<string, any>[] = schema.value?.module?.components;
            if (!components || components.length < 1) {
                return null;
            }
            const frameComponent = components.find((component: Record<string, any>) => component.componentType && component.componentType.toLowerCase() === 'frame');
            if (!frameComponent) {
                return null;
            }
            return frameComponent;
        }

        function mergeArray(target: any, source: any) {
            if (Array.isArray(target) && Array.isArray(source)) {
                const mergedMap = new Map();
                target.forEach(item => {
                    if (item.id != null) {
                        mergedMap.set(item.id, cloneDeep(item));
                    }
                });

                // 合并源数组元素到Map，存在则深度合并
                source.forEach(item => {
                    if (item.id != null) {
                        const existing = mergedMap.get(item.id);
                        if (existing) {
                            // 合并现有元素和当前元素
                            mergedMap.set(item.id, merge(existing, item));
                        } else {
                            mergedMap.set(item.id, cloneDeep(item));
                        }
                    }
                });
                const combined = Array.from(mergedMap.values());
                return combined;
            }
        }

        watch(() => props.modelValue, (newModelValue) => {
            modelValue.value = newModelValue;
            const viewSchema = getFrameComponentSchema();
            if (!viewSchema) {
                return;
            }
            convertModelValueToProps(viewSchema);
        });

        watch(() => props.schema, (newSchema) => {
            schema.value = newSchema;
            const viewSchema = getFrameComponentSchema();
            if (!viewSchema) {
                return;
            }
            convertSchemaToProps(viewSchema);
        });

        setupContext.expose({ componentManager, rerender, getProps, invoke, setProps, getSchema, setSchema, selectItemById, convertPartialSchemaToProps });

        return () => {
            const components: Record<string, any>[] = schema.value?.module?.components;
            if (!components || components.length < 1) {
                return null;
            }
            const frameComponent = components.find((component: Record<string, any>) => component.componentType && component.componentType.toLowerCase() === 'frame');
            if (!frameComponent) {
                return null;
            }
            return render(frameComponent);
        };
    }
});
export default FDynamicView;
