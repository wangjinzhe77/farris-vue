/* eslint-disable no-use-before-define */
export interface EntityState {
    primaryKey: string;
    bindingPath: string;
    currentId: string | null;
    label: string;
}

export interface UseBindingContext {
    setup();
    get(bindingPath: string | string[]): EntityState;
}

export interface EntityFieldSchema {
    id: string;
    require: any;
    multiLanguage: boolean;
    defaultValue: any;
    readonly: any;
    bindingPath: string;
    label: string;
    type: Record<string, any>;
    name: string;
}

export interface EntityTypeSchema {
    entities: EntitySchema[];
    primary: string;
    displayName: string;
    fields: EntityFieldSchema[];
    name: string;
}
export interface EntitySchema {
    id: string;
    label: string;
    code: string;
    type: EntityTypeSchema;
    name: string;
}

export interface ResolvedEntity {
    bindingPaths: string[];
    primaryKey: string;
    label: string;
}

export interface ResolvedEntityField {
    id: string;
    bindingPath: string;
    require: any;
    readonly: any;
    multiLanguage: boolean;
    label: string;
    dataSource: string;
}

export interface UseEntityResolver {
    resolveEntityByDataSource(dataSource: string): ResolvedEntity | null;
    resolveEntities(entitySchema: EntitySchema): ResolvedEntity[];
    resolveEntity(entity: Record<string, any>, entityLabel: string, isRoot?: boolean): ResolvedEntity | null;
}

export interface UseFieldResolver {
    resolveFieldById(fieldId: string): ResolvedEntityField | null;
}

// export interface UseBindingData {
//     currentId: ComputedRef<string | null>;
//     primaryKey: ComputedRef<string>;
//     bindingList: ComputedRef<any[]>;
//     currentItem: ComputedRef<any | null>;
//     setValueByPath(bindingPath: string, value: any);
// }

export interface UseComponentInstanceManager {
    register(id: string, instance: any): void;
    get(id: string): any | null;
    remove(id: string): void;
    update(id: string, instance: any): void;
    has(id: string): boolean;
    getAll(): Map<string, any>;
    clear(): void;
}
