import { UseComponentInstanceManager } from "../types";

export function useComponentManager(): UseComponentInstanceManager {
    const instances: Map<string, any> = new Map<string, any>();

    function register(id: string, instance: any) {
        instances.set(id, instance);
    }

    function get(id: string) {
        const instance = instances.get(id);
        if (!instance) {
            console.warn(`Instance with id ${id} not found`);
            return null;
        }
        return instance;
    }

    function remove(id: string) {
        instances.delete(id);
    }

    function update(id: string, instance: any) {
        if (!instances.has(id)) {
            console.warn(`Instance with id ${id} not found`);
            return;
        }
        instances.set(id, instance);
    }

    function has(id: string): boolean {
        return instances.has(id);
    }

    function getAll(): Map<string, any> {
        return new Map(instances);
    }

    function clear() {
        instances.clear();
    }
    return {
        register,
        get,
        remove,
        update,
        has,
        getAll,
        clear
    };
}
