import { BindingData } from "../../../dynamic-resolver";
import { Ref, SetupContext } from "vue";

export function useBindingData(modelValue: Ref<Record<string, any>>, setupContext: SetupContext): BindingData {
    function getValue(elementId: string) {
        return modelValue.value && modelValue.value[elementId];
    }

    function setValue(elementId: string, field: string, value: any) {
        if (modelValue.value) {
            modelValue.value[elementId] = value;
        }
        setupContext.emit('update:modelValue', { elementId, field, value, modelValue: modelValue.value });
    }

    return {
        getValue,
        setValue
    };
}
