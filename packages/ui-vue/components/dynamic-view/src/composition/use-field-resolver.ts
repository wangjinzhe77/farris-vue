/* eslint-disable no-use-before-define */
 
import { ResolvedEntityField, UseFieldResolver } from "../types";
import { useFormSchema } from "./use-form-schema";

export function useFieldResolver(schema: Record<string, any>): UseFieldResolver {
    const { getSchemaEntity } = useFormSchema(schema);
    function resolveFieldById(fieldId: string): ResolvedEntityField | null {
        const entity = getSchemaEntity();
        const fieldInfo = resolveField(entity, fieldId);
        return fieldInfo;
    }

    function resolveField(entitySchema: Record<string, any>, fieldId: string): ResolvedEntityField | null {
        const { fields } = entitySchema.type;
        for (const field of fields) {
            if (field.id === fieldId) {
                return {
                    id: field.id,
                    bindingPath: field.bindingPath,
                    require: field.require,
                    readonly: field.readonly,
                    multiLanguage: field.multiLanguage,
                    label: field.label,
                    dataSource: entitySchema.label
                };
            }
            if (field.type && field.type.fields) {
                const result = resolveField({ type: field.type, label: entitySchema.label }, fieldId);
                if (result) {
                    return result;
                }
            }
        }
        const { entities } = entitySchema.type;
        if (!entities || entities.length < 1) {
            return null;
        }
        for (const entity of entities) {
            const result = resolveField(entity, fieldId);
            if (result) {
                return result;
            }
        }
        return null;
    }
    return {
        resolveFieldById
    };
}
