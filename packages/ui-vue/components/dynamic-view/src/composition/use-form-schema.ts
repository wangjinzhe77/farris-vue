import { EntitySchema } from "../types";

export function useFormSchema(schema: Record<string, any>) {
    function getSchemaEntity(): EntitySchema {
        return schema?.module?.entity[0]?.entities[0] || {};
    }

    return {
        getSchemaEntity
    };
}
