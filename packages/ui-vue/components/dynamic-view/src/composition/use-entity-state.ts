import { EntityState, ResolvedEntity } from "../types";
import { useEntityResolver } from "./use-entity-resolver";
import { useFormSchema } from "./use-form-schema";

export function useEntityState(schema: Record<string, any>) {
    const bindingContexts: Record<string, EntityState> = {};
    const { getSchemaEntity } = useFormSchema(schema);
    const { resolveEntities } = useEntityResolver(schema);

    function setup() {
        const schemaEntity = getSchemaEntity();
        const entities = resolveEntities(schemaEntity);
        entities.forEach((entity: ResolvedEntity) => {
            const bindingPath = '/' + entity.bindingPaths.join('/');
            const { primaryKey } = entity;
            const bindingContext: EntityState = { primaryKey, bindingPath, label: entity.label, currentId: null };
            bindingContexts[bindingPath] = bindingContext;
        });
    }
    function get(bindingPath: string | string[]): EntityState {
        if (Array.isArray(bindingPath)) {
            bindingPath = '/' + bindingPath.join('/');
        } else {
            bindingPath = '/' + bindingPath.split('/').filter((item: any) => item).join('/');
        }
        return bindingContexts[bindingPath];
    }
    return {
        setup,
        get
    };
}
