/* eslint-disable no-use-before-define */
import { ResolvedEntity, EntitySchema, UseEntityResolver } from "../types";
import { useFormSchema } from "./use-form-schema";

export function useEntityResolver(schema: Record<string, any>): UseEntityResolver {
    const { getSchemaEntity } = useFormSchema(schema);
    function resolveEntityByDataSource(dataSource: string): ResolvedEntity | null {
        const entity = getSchemaEntity();
        const entityInfo = resolveEntity(entity, dataSource);
        return entityInfo;
    }
    function resolveEntities(entitySchema: EntitySchema): ResolvedEntity[] {
        const result: ResolvedEntity[] = [];
        function traverse(entity: EntitySchema, path: string[]) {
            const currentPath = [...path, entity.label];
            const primary = entitySchema?.type?.primary || null;
            if (primary) {
                result.push({
                    bindingPaths: currentPath,
                    primaryKey: entity.type.primary,
                    label: entity.label
                });
            }
            entity.type.entities.forEach(childEntity => {
                traverse(childEntity, currentPath);
            });
        }
        const primary = entitySchema?.type?.primary || null;
        if (!primary) {
            return result;
        }
        result.push({
            bindingPaths: [],
            primaryKey: entitySchema.type.primary,
            label: entitySchema.label
        });
        const childEntities = entitySchema?.type?.entities || [];
        childEntities.forEach((entity) => {
            traverse(entity, []);
        });
        return result;
    }
    function resolveEntity(entity: Record<string, any>, entityLabel: string, isRoot: boolean = true): ResolvedEntity | null {
        const currentPath = isRoot ? [] : [entity.label];
        if (entity.label === entityLabel) {
            return {
                bindingPaths: currentPath,
                primaryKey: entity.type.primary,
                label: entity.label
            };
        }
        for (const childEntity of entity.type.entities) {
            const result = resolveEntity(childEntity, entityLabel, false);
            if (result) {
                return {
                    bindingPaths: [...currentPath, ...result.bindingPaths],
                    primaryKey: result.primaryKey,
                    label: result.label
                };
            }
        }
        return null;
    }
    return {
        resolveEntityByDataSource,
        resolveEntity,
        resolveEntities
    };
}
