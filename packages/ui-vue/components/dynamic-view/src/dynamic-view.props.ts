import { ExtractPropTypes, PropType } from 'vue';

export const dynamicViewProps = {
    /**
     * schema
     */
    schema: { type: Object, default: null },
    /**
     * 组件值
     */
    modelValue: { type: Object as PropType<Record<string, any>>, default: null },
    /**
     * 回调
     */
    callback: { type: Function as PropType<(type: string, ...args: unknown[]) => any>, default: () => { } }
};
export type DynamicViewProps = ExtractPropTypes<typeof dynamicViewProps>;
