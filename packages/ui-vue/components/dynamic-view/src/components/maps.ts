import FAccordion from '@farris/ui-vue/components/accordion';
import FAvatar from '@farris/ui-vue/components/avatar';
import FButtonEdit from '@farris/ui-vue/components/button-edit';
import FButtonGroup from '@farris/ui-vue/components/button-group';
import FCalendar from '@farris/ui-vue/components/calendar';
import FCapsule from '@farris/ui-vue/components/capsule';
import FCheckBox from '@farris/ui-vue/components/checkbox';
import FComboList from '@farris/ui-vue/components/combo-list';
import FComboTree from '@farris/ui-vue/components/combo-tree';
import FComponent from '@farris/ui-vue/components/component';
import FColorPicker from '@farris/ui-vue/components/color-picker';
import FContentContainer from '@farris/ui-vue/components/content-container';
import FDatePicker from '@farris/ui-vue/components/date-picker';
import FDataGrid from '@farris/ui-vue/components/data-grid';
import FDropdown from '@farris/ui-vue/components/dropdown';
import FDynamicForm from '@farris/ui-vue/components/dynamic-form';
import FEventsEditor from '@farris/ui-vue/components/events-editor';
import FFilterBar from '@farris/ui-vue/components/filter-bar';
import FFieldSelector from '@farris/ui-vue/components/field-selector';
import FBindingSelector from '@farris/ui-vue/components/binding-selector';
import FImageCropper from '@farris/ui-vue/components/image-cropper';
import FInputGroup from '@farris/ui-vue/components/input-group';
import FLayout from '@farris/ui-vue/components/layout';
import FListNav from '@farris/ui-vue/components/list-nav';
import FListView from '@farris/ui-vue/components/list-view';
import FLookup from '@farris/ui-vue/components/lookup';
import FMappingEditor from '@farris/ui-vue/components/mapping-editor';
import FNav from '@farris/ui-vue/components/nav';
import FNumberRange from '@farris/ui-vue/components/number-range';
import FNumberSpinner from '@farris/ui-vue/components/number-spinner';
import FOrder from '@farris/ui-vue/components/order';
import FPageHeader from '@farris/ui-vue/components/page-header';
import FPageFooter from '@farris/ui-vue/components/page-footer';
import FPagination from '@farris/ui-vue/components/pagination';
import FProgress from '@farris/ui-vue/components/progress';
import FQuerySolution from '@farris/ui-vue/components/query-solution';
import FRadioGroup from '@farris/ui-vue/components/radio-group';
import FRate from '@farris/ui-vue/components/rate';
import FResponseToolbar from '@farris/ui-vue/components/response-toolbar';
import FResponseLayout from '@farris/ui-vue/components/response-layout';
import FResponseLayoutEditorSetting from '@farris/ui-vue/components/response-layout-editor';
import FSearchBox from '@farris/ui-vue/components/search-box';
import FSection from '@farris/ui-vue/components/section';
import FSmokeDetector from '@farris/ui-vue/components/smoke-detector';
import FSplitter from '@farris/ui-vue/components/splitter';
import FStep from '@farris/ui-vue/components/step';
import FSwitch from '@farris/ui-vue/components/switch';
import FTabs from '@farris/ui-vue/components/tabs';
import FTags from '@farris/ui-vue/components/tags';
import FText from '@farris/ui-vue/components/text';
import FTimePicker from '@farris/ui-vue/components/time-picker';
import FTransfer from '@farris/ui-vue/components/transfer';
import FTreeview from '@farris/ui-vue/components/tree-view';
import FUploader from '@farris/ui-vue/components/uploader';
import FVerifyDetail from '@farris/ui-vue/components/verify-detail';
import FVideo from '@farris/ui-vue/components/video';
import FTextArea from '@farris/ui-vue/components/textarea';
import FSchemaSelector from '@farris/ui-vue/components/schema-selector';
import FTreeGrid from '@farris/ui-vue/components/tree-grid';
import FEventParameter from '@farris/ui-vue/components/event-parameter';
// import FExternalContainer from '@farris/ui-vue/components/external-container';

const componentMap: Record<string, any> = {};
const componentPropsConverter: Record<string, any> = {};
const componentPropertyConfigConverter: Record<string, any> = {};
const resolverMap: Record<string, any> = {};

let hasLoaded = false;

// jumphere
function loadRegister() {
    if (!hasLoaded) {
        hasLoaded = true;
        FAvatar.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FAccordion.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FButtonEdit.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FButtonGroup.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FCalendar.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FCapsule.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FCheckBox.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FComboList.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FComboTree.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FComponent.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FContentContainer.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FColorPicker.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FDatePicker.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FDataGrid.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FDropdown.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FDynamicForm.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FEventsEditor.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        // FExternalContainer.register(componentMap, componentPropsConverter);
        FFilterBar.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FFieldSelector.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FImageCropper.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FInputGroup.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FLayout.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FListView.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FListNav.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FLookup.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FMappingEditor.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FNav.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FNumberRange.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FNumberSpinner.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FOrder.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FPageHeader.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FPageFooter.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FPagination.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FProgress.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FQuerySolution.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FRadioGroup.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FRate.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FResponseLayout.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FResponseLayoutEditorSetting.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FResponseToolbar.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FSchemaSelector.register(componentMap, componentPropsConverter, componentPropertyConfigConverter);
        FSearchBox.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FSection.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FSmokeDetector.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FSplitter.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FStep.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FSwitch.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FTabs.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FTags.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FText.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FTimePicker.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FTransfer.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FTreeview.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FVerifyDetail.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FUploader.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FVideo.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FTextArea.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FTreeGrid.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FBindingSelector.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
        FEventParameter.register(componentMap, componentPropsConverter, componentPropertyConfigConverter, resolverMap);
    }
}

export { componentMap, componentPropsConverter, loadRegister, resolverMap };
