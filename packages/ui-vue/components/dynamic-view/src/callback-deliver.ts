import { Caller } from "../../dynamic-resolver";

export function createCallbackDeliver(callback: (type: string, args: any[]) => any): Caller {
    function call(methodName: string, payloads: any[]): any {
        return callback(methodName, payloads);
    }
    return {
        call
    };
}
