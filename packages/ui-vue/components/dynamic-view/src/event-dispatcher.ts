
import { SetupContext } from "vue";
import { EventDispatcher } from "../../dynamic-resolver";

export function createEventDispatcher(context: SetupContext, schema: Record<string, any>): EventDispatcher {
    function dispatch(token: string, eventName: string, type: string, payloads: any[]) {
        context.emit('event', { token, name: eventName, type, payloads, schema });
    }
    return {
        dispatch
    };
}
