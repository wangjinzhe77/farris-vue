import type { App } from 'vue';
import FDynamicView from './src/dynamic-view.component';

export * from './src/dynamic-view.props';

export * from './src/components/maps';

export { FDynamicView };

export default {
    install(app: App): void {
        app.component(FDynamicView.name as string, FDynamicView);
    }
};
