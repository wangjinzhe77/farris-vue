
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * defination
 */
import { computed, defineComponent, onMounted, ref, SetupContext, watch } from 'vue';
import { InputGroupProps, inputGroupProps } from './input-group.props';
import { TextBoxProps, useClear, useTextBox } from '@farris/ui-vue/components/common';
import { useAppendedButton } from './composition/use-appended-button';
import { usePassword } from './composition/use-password';
import getNumberRender from './components/number.component';
import getTextarea from './components/textarea.component';
import getEditorRender from './components/text-edit.component';
import getExtendRender from './components/extend.component';
import getAppendedButtonRender from './components/appended-button.component';

export default defineComponent({
    name: 'FInputGroup',
    props: inputGroupProps,
    emits: [
        'clear',
        'change',
        'blur',
        'click',
        'clickHandle',
        'focus',
        'input',
        'keydown',
        'keyup',
        'iconMouseEnter',
        'iconMouseLeave',
        'update:modelValue',
        'update:value',
        'updateExtendInfo',
        'valueChange'
    ] as (string[] & ThisType<void>) | undefined,
    setup(props: InputGroupProps, context: SetupContext) {
        const modelValue = ref(props.modelValue);
        const displayText = ref(props.modelValue);
        const useTextBoxComposition = useTextBox(props as TextBoxProps, context, modelValue, displayText);
        const { inputGroupClass, inputType, inputGroupStyle } = useTextBoxComposition;
        const useAppendedButtonComposition = useAppendedButton(props, context);
        const { shouldShowAppendedButton } = useAppendedButtonComposition;
        const useClearComposition = useClear(props as TextBoxProps, context, useTextBoxComposition);
        const { onMouseEnter, onMouseLeave } = useClearComposition;
        const usePasswordComposition = usePassword(props, context, inputType, useAppendedButtonComposition);
        const renderNumber = getNumberRender(props, context);
        const { renderTextarea, renderCount, renderClear } = getTextarea(props, context,
            useClearComposition, useTextBoxComposition);
        const renderEditor = getEditorRender(props, context, usePasswordComposition, useTextBoxComposition);
        const renderAppendedButton = getAppendedButtonRender(
            props,
            context,
            useAppendedButtonComposition,
            useClearComposition,
            usePasswordComposition
        );

        const renderExtend = getExtendRender(props, context);

        const shouldRenderNumber = computed(() => props.type === 'number');
        const shouldRenderTextarea = computed(() => props.type === 'textarea');
        watch(
            () => [props.value],
            ([newValue]) => {
                modelValue.value = newValue;
                displayText.value = newValue;
            }
        );
        watch(
            () => [props.modelValue],
            ([newValue]) => {
                modelValue.value = newValue;
                displayText.value = newValue;
            }
        );
        onMounted(() => {
            if (props.value) {
                modelValue.value = props.value;
                displayText.value = props.value;;
            }
        });

        const renderInputAndPassword = () => {
            return (
                <>
                    <div id={props.id} class={inputGroupClass.value} style={inputGroupStyle.value}
                        onMouseenter={onMouseEnter} onMouseleave={onMouseLeave} >
                        {shouldRenderNumber.value ? renderNumber() :
                            shouldRenderTextarea.value ? renderTextarea() :
                                <>
                                    {renderExtend()}
                                    {renderEditor()}
                                </>
                        }
                        {!shouldRenderTextarea.value &&
                            shouldShowAppendedButton.value && renderAppendedButton()}
                        {shouldRenderTextarea.value && renderClear()}
                        {shouldRenderTextarea.value && renderCount()}
                    </div>
                </>
            );
        };

        return () => renderInputAndPassword();
    }
});
