
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import inputGroupSchema from './schema/input-group.schema.json';

type TextAlignType = 'left' | 'right' | 'center' | 'start' | 'end' | 'justify';
export const inputGroupProps = {
    id: { Type: String, default: '' },
    /** 是否自动完成 */
    autocomplete: { Type: String, default: 'off' },
    /** 自定义CLASS */
    customClass: { Type: String, default: '' },
    /** 禁用 */
    disabled: { Type: Boolean, default: false },
    /** 允许编辑 */
    editable: { Type: Boolean, default: true },
    /** 启用清除按钮 */
    enableClear: { Type: Boolean, default: true },
    /** 启用提示文本 */
    enableTitle: { Type: Boolean, default: true },
    /** 启用密码 */
    enableViewPassword: { Type: Boolean, default: true },
    /** 始终显示占位符文本 */
    forcePlaceholder: { Type: Boolean, default: false },
    /** 扩展按钮 */
    groupText: { Type: String, default: '' },
    /** 密码模式 */
    type: { Type: String, default: 'text' },
    /** 最大值 */
    max: { type: [Number, String] },
    /** 最小值 */
    min: { type: [Number, String] },
    /** 最大长度 */
    maxLength: { Type: Number || undefined, default: undefined },
    /** 最小长度 */
    minLength: { Type: Number || undefined, default: undefined },
    /** 组件值 */
    modelValue: { Type: String || Boolean, default: '' },
    /** 隐藏边线 */
    showBorder: { Type: Boolean, default: true },
    /** 步长 */
    step: { Type: Number, default: 1 },
    /** 启用提示信息 */
    placeholder: { Type: String, default: '' },
    precision: { Type: Number, default: 0 },
    /** 只读 */
    readonly: { Type: Boolean, default: false },
    /** 当组件禁用或只读时显示后边的按钮 */
    showButtonWhenDisabled: { Type: Boolean, default: false },
    /** tab索引 */
    tabIndex: { Type: Number || undefined, default: undefined },
    /** 文本在输入框中的对齐方式 */
    textAlign: { Type: String, default: 'left' },
    /** 是否启用前置扩展信息；在输入框前面 显示 ① 图标鼠标滑过后显示 */
    useExtendInfo: { Type: Boolean, default: false },
    /** 前置扩展信息 */
    extendInfo: { Type: String, default: '' },
    /** 输入值 */
    value: { Type: String, default: '' },
    /** 是否撑开高度 */
    autoHeight: { type: Boolean, default: false },
    /** 自动聚焦 */
    autofocus: { type: Boolean, default: false },
    /** 文本区域可见的行数 */
    rows: { type: Number, default: 2 },
    /** 展示输入文本数量 */
    showCount: { type: Boolean, default: false },
    /**
     * 作为内嵌编辑器被创建后默认获得焦点
     */
    focusOnCreated: { type: Boolean, default: false },
    /**
     * 作为内嵌编辑器被创建后默认选中文本
     */
    selectOnCreated: { type: Boolean, default: false },
    /**
     * modelValue 更新时机， 默认 blur； 可选值：blur | input
     * - blur: 离开焦点时更新
     * - input: 输入时更新
     */
    updateOn: { type: String, default: 'blur' }
} as Record<string, any>;

export type InputGroupProps = ExtractPropTypes<typeof inputGroupProps>;

export const propsResolver = createPropsResolver<InputGroupProps>(inputGroupProps, inputGroupSchema, schemaMapper, schemaResolver);
