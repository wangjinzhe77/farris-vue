import { SetupContext, computed, onMounted, ref, watch } from 'vue';
import { InputGroupProps } from '../input-group.props';
import { useNumber, useFormat, useSpinner, useTextBox } from '@farris/ui-vue/components/number-spinner';
import getNumberTextBoxRender from '../../../number-spinner/src/components/text-box.component';
import getSpinnerRender from '../../../number-spinner/src/components/spinner.component';

export default function (
    props: InputGroupProps,
    context: SetupContext
) {
    const useNumberComposition = useNumber(props, context);
    const useFormatComposition = useFormat(props, context, useNumberComposition);
    const useSpinnerComposition = useSpinner(props, context, useFormatComposition, useNumberComposition);
    const useTextBoxComposition = useTextBox(props, context, useFormatComposition, useNumberComposition, useSpinnerComposition);
    const renderSpinner = getSpinnerRender(props, context, useSpinnerComposition);
    const { displayValue, modelValue, getRealValue } = useNumberComposition;
    const renderNumberTextBox = getNumberTextBoxRender(props, context, useTextBoxComposition);
    const { format } = useFormatComposition;
    const { isFocus } = useTextBoxComposition;
    const shouldShowSpinner = computed(() => !props.disabled && !props.readonly);

    onMounted(() => {
        const value = getRealValue(props.modelValue);
        displayValue.value = format(value);
    });

    watch(
        () => [props.value],
        ([newValue]) => {
            const value = getRealValue(newValue);
            modelValue.value = value;
            displayValue.value = format(value);
        }
    );

    watch(
        () => [props.modelValue],
        ([newModelValue]) => {
            if (newModelValue !== modelValue.value) {
                modelValue.value = newModelValue;
                !isFocus.value && (displayValue.value = format(getRealValue(newModelValue)));
            }
        }
    );

    watch(
        () => [props.precision, props.useThousands, props.prefix, props.suffix, props.showZero],
        () => {
            displayValue.value = format(modelValue.value);
        }
    );

    return () => (
        <div class="input-group  flex-row f-cmp-number-spinner">
            {renderNumberTextBox()}
            {shouldShowSpinner.value && renderSpinner()}
        </div>
    );
}
