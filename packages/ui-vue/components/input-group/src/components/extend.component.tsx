import { SetupContext, computed, reactive } from 'vue';
import { InputGroupProps } from '../input-group.props';

export default function (
    props: InputGroupProps,
    context: SetupContext
) {
    const showExtend = computed(() => props.useExtendInfo && !!props.extendInfo);
    const tooltipOption = reactive({
        content: props.extendInfo
    });

    function onMouseOverInExtentInfo() {
        context.emit('updateExtendInfo');
    }

    return () => showExtend.value && (
        <span class="input-group-before-tips" onMouseenter={onMouseOverInExtentInfo} v-tooltip={tooltipOption}>
            <i class="f-icon f-icon-info-circle"></i><b class="tips-arrow"></b>
        </span>
    );
}
