import { SetupContext, computed } from 'vue';
import { InputGroupProps } from '../input-group.props';
import { UseAppendedButton, UseClear, UsePassword } from '../composition/types';

export default function (
    props: InputGroupProps,
    context: SetupContext,
    useAppendedButtonComposition: UseAppendedButton,
    useClearComposition: UseClear,
    usePasswordComposition: UsePassword
) {
    const { appendedButtonClass, appendedContent } = useAppendedButtonComposition;
    /** 当组件禁用或只读时显示后边的按钮 */
    const canEmitClickEvent = computed(() => props.showButtonWhenDisabled && (!props.editable || !props.readonly) && !props.disabled);
    const shouldShowStaticAppendButton = computed(() => appendedContent.value && !context.slots.groupTextTemplate);
    const shouldShowTemplateAppendButton = computed(() => !!context.slots.groupTextTemplate);

    const { clearButtonClass, clearButtonStyle, onClearValue, shouldShowClearButton } = useClearComposition;

    function renderClearButton() {
        return (
            <span id="clearIcon" class={clearButtonClass.value} style={clearButtonStyle.value} onClick={(e: MouseEvent) => onClearValue(e)}>
                <i class="f-icon modal_close"></i>
            </span>
        );
    }

    function onIconMouseEnter(e: MouseEvent) {
        context.emit('iconMouseEnter', e);
    }
    function onIconMouseLeave(e: MouseEvent) {
        context.emit('iconMouseLeave', e);
    }

    function onClickAppendedButton(event: MouseEvent) {
        if (canEmitClickEvent.value) {
            context.emit('clickHandle', { originalEvent: event });
        }
        event.stopPropagation();
    }

    const onClickHandle = props.type === 'password' ? usePasswordComposition.onClickAppendedButton : onClickAppendedButton;

    function renderStaticAppendedButton() {
        return (
            <span
                class="input-group-text"
                onMouseenter={(e: any) => onIconMouseEnter(e)}
                onMouseleave={(e: any) => onIconMouseLeave(e)}
                innerHTML={appendedContent.value}
                onClick={(e: any) => onClickHandle(e)}></span>
        );
    }

    /**
     * 自定义模板扩展区域，使用插槽'groupTextTemplate'
     */
    function renderTemplateAppendedButton() {
        return context.slots.groupTextTemplate && context.slots.groupTextTemplate();
    }

    function getAppendedButtonRender() {
        return shouldShowStaticAppendButton.value ? renderStaticAppendedButton :
            shouldShowTemplateAppendButton.value ? renderTemplateAppendedButton : '';
    }

    const renderAppendedButton = getAppendedButtonRender();

    return () =>
        <div class={appendedButtonClass.value}>
            {shouldShowClearButton.value && renderClearButton()}
            {renderAppendedButton && renderAppendedButton()}
        </div>;
}
