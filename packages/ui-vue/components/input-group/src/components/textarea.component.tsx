import { SetupContext, computed, ref } from 'vue';
import { UseClear, UseTextBox } from '@farris/ui-vue/components/common';
import { InputGroupProps } from '../input-group.props';

export default function (
    props: InputGroupProps,
    context: SetupContext,
    useClearComposition: UseClear,
    useTextBoxComposition: UseTextBox
) {
    const textAreaRef = ref();
    const { inputGroupClass, onBlur, onFocus } = useTextBoxComposition;
    const { clearButtonClass, clearButtonStyle } = useClearComposition;
    const total = computed(() => props.maxLength ? props.maxLength : (props.modelValue ? props.modelValue.length : 0));
    const onInput = (e: any) => {
        e.stopPropagation();
        context.emit('update:modelValue', e.target?.value);
        context.emit('valueChange', e.target?.value);
    };
    const textareaClass = computed(() => {
        return {
            'form-control': true,
            'h-100': props.autoHeight
        };
    });

    const shouldShowTextareaClearButton = computed(() =>
        props.enableClear && !props.readonly && !props.disabled);

    const realPlaceholder = computed(() => {
        return props.disabled || props.readonly ? '' : props.placeholder;
    });

    const textareaClearIconStyle = computed(() => {
        return {
            ...clearButtonStyle.value,
            'position': 'absolute',
            'right': '5px',
            'top': '50%',
            'transform': 'translateY(-50%)',
            'background-color': 'rgb(199, 207, 221)',
            'border-radius': '100%',
            'width': '16px',
            'height': '16px'
        };
    });

    const textareaStyle = computed(() => {
        const origin = { resize: 'none', 'padding-right': '15px' };
        if (props.rows > 2) {
            return { height: 'auto', ...origin };
        }
        return origin;
    });

    const onClear = (e: MouseEvent) => {
        e.stopPropagation();
        textAreaRef.value.value = '';
        context.emit('update:modelValue', '');
        context.emit('valueChange', '');
    };

    function renderTextarea() {
        return <textarea
            class={textareaClass.value}
            id={props.id}
            v-model={props.modelValue}
            ref={textAreaRef}
            minlength={props.minLength}
            maxlength={props.maxLength ? props.maxLength : null}
            tabindex={props.tabIndex}
            disabled={props.disabled}
            readonly={props.readonly}
            autocomplete={props.autocomplete}
            placeholder={realPlaceholder.value}
            autofocus={props.autofocus}
            rows={props.rows}
            onInput={onInput}
            onFocus={onFocus}
            onBlur={onBlur}
            style={textareaStyle.value}
        >
        </textarea>;
    }

    function renderCount() {
        return props.showCount && <span style="position: absolute;bottom: 0;right: 5px;z-index:999;font-size:12px">
            {`${props.modelValue ? props.modelValue.length : 0}/${total.value}`}
        </span>;
    }

    function renderClear() {
        return shouldShowTextareaClearButton.value && <span id="clearIcon"
            class={clearButtonClass.value}
            style={textareaClearIconStyle.value}
            onClick={(e: MouseEvent) => onClear(e)}>
            <i class="f-icon modal_close" style="font-size:8px;color:#fff"></i>
        </span>;
    }

    return { renderTextarea, renderCount, renderClear };
}
