import { SetupContext, onMounted, ref } from 'vue';
import { InputGroupProps } from '../input-group.props';
import { UsePassword } from '../composition/types';
import { UseTextBox } from '@farris/ui-vue/components/common';

export default function (
    props: InputGroupProps,
    context: SetupContext,
    usePasswordComposition: UsePassword,
    useTextBoxComposition: UseTextBox
) {
    const inputElementRef = ref();
    const autocomplete = ref(props.autocomplete);
    const enableTitle = ref(props.enableTitle);
    const { isPassword } = usePasswordComposition;
    const minLength = ref(props.minLength);
    const maxLength = ref(props.maxLength);
    const tabIndex = ref(props.tabIndex);

    const {
        disabled,
        displayText,
        inputType,
        onBlur,
        onFocus,
        onInput,
        onClick,
        onKeydown,
        onKeyup,
        onMousedown,
        onTextBoxValueChange,
        readonly,
        placeholder,
        textBoxClass,
        textBoxTitle
    } = useTextBoxComposition;

    onMounted(() => {
        if (props.selectOnCreated) {
            // (inputElementRef.value as HTMLInputElement)?.select();
        }
        if (props.focusOnCreated) {
            // (inputElementRef.value as HTMLInputElement)?.focus();
        }
    });

    return () => (
        <input
            ref={inputElementRef}
            name="input-group-value"
            autocomplete={autocomplete.value}
            class={textBoxClass.value}
            disabled={disabled?.value}
            maxlength={maxLength.value}
            minlength={minLength.value}
            placeholder={placeholder.value}
            readonly={readonly?.value}
            tabindex={tabIndex.value}
            title={enableTitle.value && !isPassword.value ? textBoxTitle.value : ''}
            type={inputType.value}
            value={displayText?.value}
            onBlur={onBlur}
            onClick={onClick}
            onChange={onTextBoxValueChange}
            onFocus={onFocus}
            onInput={onInput}
            onKeydown={onKeydown}
            onKeyup={onKeyup}
            onMousedown={onMousedown}
        />
    );
}
