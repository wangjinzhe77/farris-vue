import { InputBaseProperty } from "../../../property-panel/src/composition/entity/input-base-property";

export class InputGroupProperty extends InputBaseProperty {

    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }
 
}
