 
import { UseDesignerRules,ComponentSchema, DesignerItemContext, DesignerComponentInstance } from "@farris/ui-vue/components/designer-canvas";
import { InputGroupProperty } from "../property-config/input-group.property-config";

export function useInputGroupDesignerRules(designItemContext: DesignerItemContext, designerHostService): UseDesignerRules {

    const schema = designItemContext.schema as ComponentSchema;

    // 构造属性配置方法
    function getPropsConfig(componentId: string, componentInstance: DesignerComponentInstance) {
        const inputGroupProps = new InputGroupProperty(componentId, designerHostService);
        return inputGroupProps.getPropertyConfig(schema, componentInstance);
    }

    return { getPropsConfig } as UseDesignerRules;

}
