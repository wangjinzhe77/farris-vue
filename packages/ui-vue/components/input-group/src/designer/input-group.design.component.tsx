 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * defination
 */
import { defineComponent, inject, ref, SetupContext, onMounted } from 'vue';
import { InputGroupProps, inputGroupProps } from '../input-group.props';
import { useAppendedButton } from '../composition/use-appended-button';
import { usePassword } from '../composition/use-password';
import { TextBoxProps, useClear, useTextBoxDesign } from '@farris/ui-vue/components/common';
import getAppendedButtonRender from '../components/appended-button.component';
import { DesignerItemContext, useDesignerComponent } from '@farris/ui-vue/components/designer-canvas';
import { useInputGroupDesignerRules } from './use-input-group-rules';
// import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';

export default defineComponent({
    name: 'FInputGroupDesign',
    props: inputGroupProps,
    emits: [
        'updateExtendInfo',
        'clear',
        'valueChange',
        'clickHandle',
        'blurHandle',
        'focusHandle',
        'enterHandle',
        'iconMouseEnter',
        'iconMouseLeave',
        'keyupHandle',
        'keydownHandle',
        'inputClick'
    ] as (string[] & ThisType<void>) | undefined,
    setup(props: InputGroupProps, context: SetupContext) {
        const modelValue = ref(props.modelValue);
        const displayText = ref(props.modelValue);
        const useTextBoxComposition = useTextBoxDesign(props as TextBoxProps, context, modelValue, displayText);
        const { inputGroupClass, inputType, inputGroupStyle } = useTextBoxComposition;
        const useAppendedButtonComposition = useAppendedButton(props, context);
        const { shouldShowAppendedButton } = useAppendedButtonComposition;
        const useClearComposition = useClear(props as TextBoxProps, context, useTextBoxComposition);
        const { onMouseEnter, onMouseLeave } = useClearComposition;
        const usePasswordComposition = usePassword(props, context, inputType, useAppendedButtonComposition);

        const inputGroupRef = ref<any>();
        const designerHostService = inject('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useInputGroupDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(inputGroupRef, designItemContext, designerRulesComposition);

        onMounted(() => {
            inputGroupRef.value.componentInstance = componentInstance;
        });
        context.expose(componentInstance.value);
        
        const renderAppendedButton = getAppendedButtonRender(
            props,
            context,
            useAppendedButtonComposition,
            useClearComposition,
            usePasswordComposition
        );
        const inputElementRef = ref();

        return () => {
            return (
                <div id="inputGroup" ref={inputGroupRef} class={inputGroupClass.value} style={inputGroupStyle.value}
                    onMouseenter={onMouseEnter} onMouseleave={onMouseLeave}>
                    <input ref={inputElementRef} class="form-control" readonly
                        placeholder={props.placeholder}
                    />
                    {shouldShowAppendedButton.value && renderAppendedButton()}
                </div>
            );
        };
    }
});
