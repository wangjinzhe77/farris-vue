import { SetupContext, computed, ref } from "vue";
import { InputGroupProps } from "../input-group.props";
import { UseAppendedButton } from "./types";

export function useAppendedButton(
    props: InputGroupProps,
    context: SetupContext
): UseAppendedButton {
    const appendedContent = ref(props.groupText);

    const forcedToShowAppendedButton = computed(() => props.showButtonWhenDisabled && (props.readonly || props.disabled));

    const shouldShowAppendedButton = computed(() => props.enableClear || !!props.groupText);

    const appendedButtonClass = computed(() => {
        const classObject = {
            'input-group-append': true,
            'append-force-show': forcedToShowAppendedButton.value
        } as Record<string, boolean>;
        return classObject;
    });

    return { appendedButtonClass, appendedContent, shouldShowAppendedButton };
}
