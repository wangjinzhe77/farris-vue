import { ComputedRef, Ref } from "vue";
import { BigNumber } from 'bignumber.js';

export interface UseAppendedButton {

    appendedButtonClass: ComputedRef<Record<string, boolean>>;

    appendedContent: Ref<string>;

    shouldShowAppendedButton: ComputedRef<boolean>;

}

export interface UseClear {

    clearButtonClass: ComputedRef<Record<string, boolean>>;

    clearButtonStyle: ComputedRef<Record<string, any>>;

    hasShownClearButton: Ref<boolean>;

    onClearValue: ($event: MouseEvent) => void;

    onMouseEnter: ($event: MouseEvent) => void;

    onMouseLeave: ($event: MouseEvent) => void;

    shouldShowClearButton: ComputedRef<boolean>;
}

export interface UsePassword {

    isPassword: ComputedRef<boolean>;

    onClickAppendedButton: () => void;

}

export interface UseTextBox {

    changeTextBoxValue: (newValue: any, shouldEmitChangeEvent?: boolean) => void;

    disabled: Ref<boolean>;

    displayText: Ref<string>;

    editable: Ref<boolean>;

    inputGroupEditorClass: ComputedRef<Record<string, boolean>>;

    inputType: Ref<string>;

    isEmpty: ComputedRef<boolean>;

    modelValue: Ref<string>;

    onBlur: (event: FocusEvent) => void;

    onEnter: ($event: KeyboardEvent) => void;

    onInput: ($event: Event) => void;

    onInputClick: ($event: MouseEvent) => void;

    onInputFocus: ($event: FocusEvent) => void;

    onMousedown: ($event: MouseEvent) => void;

    onTextBoxValueChange: ($event: Event) => void;

    readonly: ComputedRef<boolean>;

    textBoxPlaceholder: ComputedRef<string>;
}
