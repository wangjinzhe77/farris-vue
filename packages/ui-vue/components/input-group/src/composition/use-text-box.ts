import { SetupContext, computed, ref, watch } from "vue";
import { InputGroupProps } from "../input-group.props";
import { UseTextBox } from "./types";

export function useTextBox(
    props: InputGroupProps,
    context: SetupContext
): UseTextBox {
    const inputType = ref('text');
    const modelValue = ref(props.modelValue);
    const displayText = ref(props.modelValue);
    /** 文本在输入框中的对齐方式 */
    const textAlign = ref(props.textAlign);
    const showBorder = ref(props.showBorder);

    const textBoxPlaceholder = computed(() => ((props.disabled || props.readonly) && !props.forcePlaceholder ? '' : props.placeholder));

    const canFocus = computed(() => props.editable || !props.readonly);

    const readonly = computed(() => props.readonly || !props.editable);

    const isEmpty = computed(() => modelValue.value === '' || modelValue.value === null || modelValue.value === undefined);

    /** 禁用 */
    const disabled = ref(props.disabled);
    /** 允许编辑 */
    const editable = ref(props.editable);

    const focusCls = 'f-state-focus';

    const inputGroupEditorClass = computed(() => {
        const classObject = {
            'form-control': true,
            'f-utils-fill': true,
            'text-left': textAlign.value === 'left',
            'text-center': textAlign.value === 'center',
            'text-right': textAlign.value === 'right'
        } as Record<string, boolean>;
        return classObject;
    });

    function onBlur(event: FocusEvent) {
        document.documentElement.classList.remove(focusCls);
        context.emit('blurHandle', event);
        event.stopPropagation();
        return false;
    }

    function onEnter($event: KeyboardEvent) {
        context.emit('enterHandle', { originalEvent: $event });
    }

    function onMousedown($event: MouseEvent) {
        const target = $event.target as HTMLElement;
        if (target.tagName !== 'INPUT') {
            $event.preventDefault();
        }
        $event.stopPropagation();
    }

    function changeTextBoxValue(newValue: any, shouldEmitChangeEvent = true) {
        if (modelValue.value !== newValue) {
            modelValue.value = newValue;
            if (shouldEmitChangeEvent) {
                context.emit('change', newValue);
            }
            context.emit('update:modelValue', newValue);
            context.emit('update:value', newValue);
        }
    }

    function onInput($event: Event) {
        context.emit('input', ($event.target as HTMLInputElement).value);
        const newValue = ($event.target as HTMLInputElement).value;
        if (modelValue.value !== newValue) {
            changeTextBoxValue(newValue, false);
        }
    }

    function onInputFocus($event: FocusEvent) {
        if (!props.disabled) {
            if (showBorder.value) {
                document.documentElement.classList.add(focusCls);
            }
            if (canFocus.value) {
                context.emit('focusHandle', $event);
            }
        }
    }

    function onInputClick($event: MouseEvent) {
        context.emit('inputClick', $event);
    }

    function onTextBoxValueChange($event: Event) {
        const newValue = ($event.target as HTMLInputElement).value;
        $event.stopPropagation();
        changeTextBoxValue(newValue);
    }

    watch(
        () => props.modelValue,
        (value: string) => {
            modelValue.value = value;
        }
    );

    return {
        changeTextBoxValue,
        disabled,
        displayText,
        editable,
        inputGroupEditorClass,
        inputType,
        isEmpty,
        readonly,
        modelValue,
        onBlur,
        onEnter,
        onInput,
        onInputClick,
        onInputFocus,
        onMousedown,
        onTextBoxValueChange,
        textBoxPlaceholder
    };
}
