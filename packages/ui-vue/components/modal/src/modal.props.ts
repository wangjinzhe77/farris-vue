/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { ModalButton } from './composition/type';

export type DragHandleType = HTMLElement | string;

export const modalProps = {
    /**
     * 自定义类
     */
    class: { type: String, default: '' },
    /**
     * 模态框标题
     */
    title: { type: String, default: '' },
    /**
     * 模态框宽度
     */
    width: { type: Number, default: 500 },
    /**
     * 模态框高度
     */
    height: { type: Number, default: 320 },
    /**
     * 自定义按钮列表
      */
    buttons: {
        type: Array<ModalButton>,
        default: []
    },
    /**
     * 是否展示模态框
     */
    modelValue: { type: Boolean, default: false },
    /**
     * 是否展示头部
     */
    showHeader: { type: Boolean, default: true },
    /**
     * 是否展示默认按钮
     */
    showButtons: { type: Boolean, default: true },
    /**
     * 是否启用自适应样式
     */
    fitContent: { type: Boolean, default: true },
    /**
     * 是否展示右上角按钮
     */
    showCloseButton: { type: Boolean, default: true },
    showMaxButton: { type: Boolean, default: false },
    minHeight: {type: Number},
    maxHeight: {type: Number},
    minWidth: {type: Number},
    maxWidth: {type: Number},
    containment: {type: Object as PropType<HTMLElement>, default: null},
    resizeable: { type: Boolean, default: false },
    draggable: { type: Boolean, default: false},
    dragHandle: { type: Object as PropType<DragHandleType>, default: null},
    closedCallback: { type: Function, default: null},
    resizeHandle: { type: Function, default: null},
    render: { type: Function, default: null},
    acceptCallback: { type: Function, default: null},
    rejectCallback: { type: Function, default: null},
    enableEsc:  { type: Boolean, default: true },
    enableEnter:  { type: Boolean, default: false },
    dialogType: { type: String, default: '' },
    src: { type: String, default: '' }
};

export type ModalProps = Partial<ExtractPropTypes<typeof modalProps>>;
