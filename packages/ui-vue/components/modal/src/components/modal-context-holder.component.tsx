import { defineComponent, SetupContext, shallowRef, VNode } from "vue";

export interface ContextHolder {
    addModal: (modal: () => VNode) => () => void;
}
export default defineComponent({
    name: 'FModalContextHolder',
    setup(props: any, context: SetupContext) {
        const modals = shallowRef<(() => VNode)[]>([]);
        const addModal = (modal: () => VNode) => {
            modals.value.push(modal);
            modals.value = modals.value.slice();
            return () => {
                // 删除当前模态框
                modals.value = modals.value.filter(currentModal => currentModal !== modal);
            };
        };
        context.expose({ addModal });
        return () => {
            return modals.value.map(modal => modal());
        };
    }
});
