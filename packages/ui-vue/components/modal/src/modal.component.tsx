/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { computed, defineComponent, ref, SetupContext, Teleport, watch, Transition, onMounted, onUnmounted, provide, nextTick } from 'vue';
import { ModalButton, ModalOptions } from './composition/type';
import { ModalProps, modalProps } from './modal.props';
import { useResizeable } from './composition/use-resizeable';
import { useDraggable } from './composition/use-draggable';
import './modal.scss';
import { useEnter, useEsc } from './composition/use-shortcut';

export default defineComponent({
    name: 'FModal',
    props: modalProps,
    emits: ['update:modelValue', 'accept', 'cancel', 'closed', 'resize', 'esc', 'enter'] as (string[] & ThisType<void>) | undefined,
    setup(props: ModalProps, context: SetupContext) {
        const width = ref(props.width || 300);
        const height = ref(props.height || 200);
        const modelValue = ref(props.modelValue);
        const modalId = ref('');
        const customClass = ref(props.class);
        const fitContent = ref(props.fitContent);
        const showHeader = ref(props.showHeader);
        const headerIconClass = ref('');
        const enableClose = ref(props.showCloseButton);
        const enableMaximize = ref(props.showMaxButton);
        const enableMinimize = ref(false);
        const dialogType = ref(props.dialogType);
        const iframeSrc = ref(props.src);
        const buttonAlignment = ref('');
        const showButtons = ref(props.showButtons);
        const title = ref(props.title);
        const resizeable = ref(props.resizeable);
        const containment = ref<HTMLElement | null>(props.containment || null);
        const modalContainerRef = ref();


        function close($event: MouseEvent, accept?: boolean) {
            modelValue.value = false;
            context.emit('update:modelValue', false);
            if (accept != null) {
                context.emit(accept ? 'accept' : 'cancel');
            }

            context.emit('closed', $event);
        }
        const defaultButtons: ModalButton[] = [
            {
                name: 'cancel',
                text: '取消',
                class: 'btn btn-secondary',
                handle: ($event: MouseEvent) => {
                    close($event, false);
                }
            },
            {
                name: 'accept',
                text: '确定',
                class: 'btn btn-primary',
                handle: ($event: MouseEvent) => {
                    close($event, true);
                }
            }
        ];
        const buttons = ref(props.buttons && props.buttons.length ? props.buttons : defaultButtons);

        const showHeaderIcon = computed(() => !!headerIconClass.value);
        const showFooter = computed(() => !!showButtons.value && !!buttons.value);

        const modalHeaderRef = ref<any>();
        const modalElementRef = ref<any>();
        const maximized = ref(false);

        const { renderResizeBar, maximize, restore, boundingElement,
            resizedEventParam, allowDrag, unWindowResizeHandle } = useResizeable(props, context);
        const { registerDraggle } = useDraggable(props, context, allowDrag);


        function removeModalOpenStyle() {
            const modalTotal = document.querySelectorAll('.farris-modal').length;
            if (!modalTotal || modalTotal - 1 <= 0) {
                document.body.classList.remove('modal-open');
            }

            if (modalContainerRef.value) {
                modalContainerRef.value.classList.remove('show');
            }
        }

        // 监听modal 标题变化
        watch(() => props.title, (newValue, oldValue) => {
            if (newValue !== oldValue) {
                title.value = newValue;
            }
        });

        // 监听打开关闭状态变化
        watch(() => props.modelValue, (newValue, oldValue) => {
            if (newValue !== oldValue) {
                modelValue.value = newValue;

                if (!newValue) {
                    removeModalOpenStyle();
                }
            }
        });
        // 监听是否展示标题变化
        watch(() => props.showHeader, (newValue, oldValue) => {
            if (newValue !== oldValue) {
                showHeader.value = newValue;
            }
        });
        // 监听是否展示按钮变化
        // todo: 可以抽出公共方法
        watch(() => props.showButtons, (newValue, oldValue) => {
            if (newValue !== oldValue) {
                showButtons.value = newValue;
            }
        });

        watch(() => resizedEventParam.value, (newSize, oldSize) => {
            const newSizeValue = newSize || {};
            const oldSizeValue = oldSize || {};

            if (JSON.stringify(newSizeValue) !== JSON.stringify(oldSizeValue)) {
                context.emit('resize', { newSize, oldSize });
            }
        });

        const showModal = computed(() => {
            if (modelValue.value) {
                document.body.classList.add('modal-open');
            }

            return modelValue.value;
        });

        const modalContainerClass = computed(() => {
            const classObject = {
                modal: true,
                'farris-modal': true,
                'fade': true,
            } as Record<string, boolean>;
            classObject['f-modal-fitContent'] = !!fitContent.value;
            classObject.show = !!showModal.value;
            return classObject;
        });

        const modalDialogClass = computed(() => {
            const classObject = { 'modal-dialog': true };
            const customClassArray = customClass.value?.split(' ');
            customClassArray?.reduce((target: any, className: string) => {
                target[className] = true;
                return target;
            }, classObject);
            return classObject;
        });

        const modalDialogStyle = computed(() => {
            const styleObject = {
                position: 'absolute',
                top: `${(window.innerHeight - height.value) / 2}px`,
                left: `${(window.innerWidth - width.value) / 2}px`,
                width: `${width.value}px`,
                height: fitContent.value ? 'auto' : `${height.value}px`
            };
            return styleObject;
        });

        const modalContentClass = computed(() => {
            const classObject = {
                'modal-content': true,
                'modal-content-has-header': showHeader.value
            };
            return classObject;
        });

        const modalHeaderStyle = computed(() => {
            const styleObject = { display: showHeader.value ? '' : 'none' };
            styleObject['pointer-events'] = allowDrag.value ? 'auto': 'none';
            return styleObject;
        });

        const headerMaxButtonClass = computed(() => {
            const classObject = { 'f-icon': true, modal_maximize: true, modalrevert: maximized.value };
            return classObject;
        });

        const modalBodyClass = computed(() => {
            const classObject = { 'modal-body': true, 'f-utils-flex-column': dialogType.value === 'iframe' };
            return classObject;
        });

        function buildFooterStyles(): Record<string, any> {
            return {};
        }

        const modalFooterStyle = computed(() => {
            const styleObject = { textAlgin: buttonAlignment.value };
            const footerSytles = buildFooterStyles();
            return Object.assign(styleObject, footerSytles);
        });

        function maxDialog($event: MouseEvent) {
            $event.stopPropagation();
            if (maximized.value) {
                maximized.value = false;
                restore();
                return;
            }
            maximize();
            maximized.value = true;
        }

        async function customButtonClick(button: ModalButton, $event: MouseEvent) {
            if (button.handle) {
                const result = await button.handle($event, button);
                if (result) {
                    context.emit('closed', $event);
                }
            }
        }

        function updateModalOptions(options: ModalOptions) {
            if (options.width) {
                width.value = options.width;
            }
            if (options.height) {
                height.value = options.height;
            }
            if (options.buttons) {
                buttons.value = options.buttons;
            }
            if (options.title) {
                title.value = options.title;
            }
        }

        let escEventHandler: any = null;
        let enterEventHandler: any = null;

        onMounted(() => {
            if (modalElementRef.value && !containment.value) {
                containment.value = (modalElementRef.value as HTMLElement).parentElement;
                boundingElement.value = containment.value;
                registerDraggle(modalHeaderRef.value, modalElementRef.value, boundingElement.value);
            }
            if (showModal.value) {
                document.body.classList.add('modal-open');
            }

            escEventHandler = useEsc(props, context);
            enterEventHandler = useEnter(props, context);

        });

        onUnmounted(() => {
            if (unWindowResizeHandle) {
                unWindowResizeHandle();
            }

            if (escEventHandler) {
                escEventHandler.remove();
            }

            if (enterEventHandler) {
                enterEventHandler.remove();
            }
        });

        context.expose({
            modalElementRef,
            updateModalOptions,
            close,
            maxDialog
        });

        function renderFloatingCloseButton() {
            return <ul>
                {enableMinimize.value && (
                    <li class="f-btn-icon f-bare">
                        <span class="f-icon modal_minimize"></span>
                    </li>
                )}
                {enableMaximize.value && (
                    <li onClick={maxDialog} class="f-btn-icon f-bare" style="pointer-events: auto;">
                        <span class={headerMaxButtonClass.value}></span>
                    </li>
                )}
                {enableClose.value && (
                    <li class="f-btn-icon f-bare"
                        onClick={(payload: MouseEvent) => close(payload, false)}  style="pointer-events: auto;">
                        <span class="f-icon modal_close"></span>
                    </li>
                )}
            </ul>;
        }

        function renderFooterButtons() {
            return <div class="modal-footer" style={modalFooterStyle.value}>
                {buttons.value && buttons.value.map((button: ModalButton) => {
                    return (
                        <button
                            name={button.name}
                            type="button"
                            class={button.class + (button.iconClass ? ' btn-icontext' : '')}
                            onClick={($event) => { customButtonClick(button, $event); }}>
                            {!!button.iconClass && <i class={button.iconClass}></i>}
                            {button.text}
                        </button>
                    );
                })}
            </div>;
        }

        function renderModalHeader() {
            return  <div ref={modalHeaderRef} class="modal-header" style={modalHeaderStyle.value} >
                <div class="modal-title">
                    {showHeaderIcon.value && <span class={headerIconClass.value} style="margin-right: 8px"></span>}
                    <span class="modal-title-label">{title.value}</span>
                </div>
                <div class="actions">
                    {renderFloatingCloseButton()}
                </div>
            </div>;
        }

        function onClickModalBackdrop($event) {
            $event.stopPropagation();
        }

        return () => {
            return (
                <Teleport to="body">
                    {showModal.value &&
                    <Transition name="fade" appear>
                        <div class={modalContainerClass.value} style="display: block"
                            ref={modalContainerRef} onClick={onClickModalBackdrop}>
                            <div id={modalId.value} class={modalDialogClass.value} style={modalDialogStyle.value} ref={modalElementRef}>
                                <div class={modalContentClass.value}>
                                    { showHeader.value && renderModalHeader() }

                                    <div class={modalBodyClass.value}>
                                        {context.slots.default?.()}
                                        {dialogType.value === 'iframe' && (
                                            <iframe
                                                title={modalId.value}
                                                class="f-utils-fill"
                                                width="100%"
                                                frameborder="0"
                                                src={iframeSrc.value}></iframe>
                                        )}
                                    </div>
                                    {showFooter.value && renderFooterButtons()}
                                </div>
                                {!fitContent.value && resizeable.value && modalElementRef.value && !maximized.value && renderResizeBar(modalElementRef.value)}
                            </div>
                        </div>
                    </Transition>}
                </Teleport>
            );
        };
    }
});
