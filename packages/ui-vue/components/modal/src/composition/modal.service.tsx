import { ref, h, render, VNode, cloneVNode, shallowRef, nextTick, App, AppContext, createApp, onUnmounted, onMounted, computed } from "vue";
import { ModalFunctions, ModalOptions } from "./type";
import FModal from '../modal.component';
// import FarrisPlugin from '../../../plugin';

function getContentRender(props: ModalOptions) {
    if (props.content && props.content.render) {
        return props.content.render;
    }
    if (props.render && typeof props.render === 'function') {
        return props.render;
    }
}

function createModalInstance(options: ModalOptions) {
    const container = document.createElement('div');
    container.style.display = 'contents';

    const app: App = createApp({
        setup(props, context) {

            onUnmounted(() => {
                document.body.removeChild(container);
            });

            const modalRef = ref<any>();

            const customClass = ref(options.class || '');
            const showButtons = ref(!!options.showButtons);
            const showHeader = ref(!!options.showHeader);
            const showCloseButton = ref(options.showCloseButton == null ? true : options.showCloseButton);
            const showModal = ref(true);
            const modalTitle = ref(options.title || '');
            const acceptCallback = options.acceptCallback || (() => { });
            const rejectCallback = options.rejectCallback || (() => { });

            const modalClosedCallback = options.closedCallback || (($event: Event) => { });
            const modalResizedCallback = options.resizeHandle || (($event: Event) => { });

            const contentRender = getContentRender(options);

            const onClosed = ($event: Event) => {
                showModal.value = false;
                app.unmount();
                modalClosedCallback($event);
            };

            onMounted(() => {
                // console.log(modalRef);
            });

            context.expose({
                modalRef
            });

            return () => (
                <FModal ref={modalRef}
                    class={customClass.value}
                    v-model={showModal.value}
                    title={modalTitle.value}
                    width={options.width}
                    height={options.height}
                    buttons={options.buttons}
                    show-header={showHeader.value}
                    show-buttons={showButtons.value}
                    show-close-button={showCloseButton.value}
                    show-max-button={false}
                    onAccept={acceptCallback}
                    onCancel={rejectCallback}
                    fitContent={options.fitContent == null ? true : options.fitContent}
                    onClosed={onClosed}
                    onResize={modalResizedCallback}
                >
                    {contentRender && contentRender(app)}
                </FModal>
            );
        }
    });
    document.body.appendChild(container);
    // if (FarrisPlugin && !!FarrisPlugin.install) {
    //     app.use(FarrisPlugin);
    // }

    app.mount(container);

    return app;
}

export default class ModalService {
    private appContext: AppContext | null = null;

    private modalRef = ref();

    private activeModalIndex = ref(0);

    private modalRefs: Record<number, any> = {};

    private isUseEscCloseModal = ref(false);

    private activeModalInstance = computed(() => {
        return this.modalRefs[this.activeModalIndex.value];
    });

    private app: App;

    constructor(currentApp: App) {
        this.app = currentApp;
        this.appContext = currentApp ? currentApp._context : null;
    }

    getCurrentModal() {
        return this.activeModalInstance.value;
    }

    private adaptToWindow(width: number, height: number) {
        // 可视区域尺寸
        const { width: winWidth, height: winHeight } = {
            width: window.innerWidth,
            height: window.innerHeight
        };

        if (winWidth < width) {
            width = winWidth;
        }

        if (winHeight < height) {
            height = winHeight;
        }

        return  {
            width,
            height
        };
    }

    static show(options: ModalOptions) {
        const modalOptions = Object.assign({
            title: '',
            showButtons: true,
            showHeader: true
        }, options);
        const app: App = createModalInstance(modalOptions);
        return app;
    }

    open(options: ModalOptions): ModalFunctions {
        // 创建一个空白文档作为模态框容器，空白文档不属于DOM树，比创建一般节点性能较好，且仅渲染子元素
        const container = document.createDocumentFragment();

        if (options.showMaxButton && options.fitContent) {
            options.showMaxButton = false;
        }

        const modalOptions = shallowRef(Object.assign({
            title: '',
            showButtons: true,
            showHeader: true,
        }, options));

        const showModal = ref(true);
        const acceptCallback = modalOptions.value.acceptCallback || (() => { });
        const rejectCallback = modalOptions.value.rejectCallback || (() => { });

        const modalClosedCallback = modalOptions.value.closedCallback || (($event?: Event, from?: string) => { });
        const modalResizedCallback = modalOptions.value.resizeHandle || (($event: Event) => { });
        let modalInstance: VNode | null;

        const contentRender = getContentRender(modalOptions.value);

        const onClosed = ($event?: Event) => {
            showModal.value = false;

            const isCloseIconClick = ($event?.target as any)?.classList.contains('modal_close');

            modalClosedCallback($event, this.isUseEscCloseModal.value ? 'esc' : isCloseIconClick ? 'icon' : 'button');

        };

        const destroy = ($event?: Event) => {
            onClosed($event);
            if (modalInstance) {
                nextTick(() => {
                    if (this.modalRefs[this.activeModalIndex.value]) {
                        delete this.modalRefs[this.activeModalIndex.value];
                    }
                    render(null, container as any);
                    modalInstance = null;
                    this.modalRef.value = null;

                    if (this.modalRefs) {
                        const modals = Object.keys(this.modalRefs).map((key: string) => Number(key));
                        if (modals.length > 0) {
                            this.activeModalIndex.value = Math.max(...modals);
                        } else {
                            this.activeModalIndex.value = 0;
                        }
                    }

                    this.isUseEscCloseModal.value = false;
                });
            }

        };

        const onEsc = (payload: any) => {
            this.isUseEscCloseModal.value = true;
            this.activeModalInstance && this.activeModalInstance.value?.close(payload?.event);
        };

        const { width: modalWidth, height: modalHeight } = modalOptions.value;
        const adaptedSize  = this.adaptToWindow(modalWidth || 500, modalHeight || 320);
        Object.assign(modalOptions.value, adaptedSize);

        const ModalWrapper = () => (
            <FModal ref={this.modalRef}
                v-model={showModal.value}
                {...modalOptions.value}
                onAccept={acceptCallback}
                onCancel={rejectCallback}
                onClosed={destroy}
                onResize={modalResizedCallback}
                onEsc={onEsc}
            >
                {contentRender && contentRender(this.app)}
            </FModal>
        );

        const renderModal = (props: Partial<ModalOptions>): VNode => {
            const vnode = h(ModalWrapper, props);
            vnode.appContext = this.appContext;
            // if (FarrisPlugin && !!FarrisPlugin.install) {
            //     vnode.appContext?.app.use(FarrisPlugin);
            // }
            render(vnode, container as any);
            return vnode;
        };
        modalInstance = renderModal(
            {
                ...modalOptions.value,
                // 'onUpdate:modelValue': onUpdateModelValue,
            }
        );

        this.activeModalIndex.value++;
        this.modalRefs[this.activeModalIndex.value] = this.modalRef.value;

        const update = (updateConfig: ModalOptions) => {
            modalOptions.value = {
                ...modalOptions.value,
                ...updateConfig
            };
            if (modalInstance) {
                render(cloneVNode(modalInstance, { ...modalOptions }), container as any);
            }
        };

        return {
            update,
            destroy,
            modalRef: this.activeModalInstance
        };
    }
}
