import { App, VNode } from "vue";
import { JSX } from "vue/jsx-runtime";
import { IResizeEvent } from "./resizeable/resize-event";

/** 自定义按钮结构 */
export interface ModalButton {
    name?: string;
    class: string;
    focusedByDefault?: boolean;
    disable?: boolean;
    iconClass?: string;
    handle: ($event: MouseEvent, context: any) => any;
    text: string;
}

export interface ModalOptions {
    class?: string;
    title?: string;
    width?: number;
    height?: number;
    minWidth?: number;
    minHeight?: number;
    showButtons?: boolean;
    showHeader?: boolean;
    showFloatingClose?: boolean;
    content?: { render(): JSX.Element };
    render?: (app: App) => JSX.Element;
    fitContent?: boolean;
    buttons?: ModalButton[];
    draggable?: boolean;
    resizeable?: boolean;
    showMaxButton?: boolean;
    showCloseButton?: boolean;
    acceptCallback?: () => void;
    rejectCallback?: () => void;
    closedCallback?: ($event?: Event, from?: 'esc' | 'icon' | 'button') => void;
    resizeHandle?: (event: IResizeEvent) => void;
    enableEsc?: boolean;
    dialogType?: string;
    src?: string;
}

export interface ModalFunctions {
    update: (options: ModalOptions) => void;
    destroy: () => void;
    modalRef: any;
}

export interface UseModal {
    FModalContextHolder: () => VNode;

}
