export interface IResizeEvent {
    size: {
        width?: number;
        height?: number;
    };
    position: {
        x?: number;
        y?: number;
    };
    transform?: string;
}
