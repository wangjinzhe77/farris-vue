import { Ref, ref, watch } from "vue";
import { Position } from "./resizeable/position";

export function useDraggable(props: any, context: any, canMove: Ref<boolean>) {

    const dragTarget = ref<HTMLElement>();
    const allowDrag = ref(props.draggable);
    const lockAxis = ref(props.lockAxis);

    const dragHandle = ref();
    const boundingElement = ref();
    const moving = ref(false);

    const origPos = ref<Position | undefined>(new Position(0, 0));
    const oldTranslate = ref<Position>(new Position(0, 0));
    const movingPos= ref<Position>(new Position(0, 0));
    const currentPos= ref<Position>(new Position(0, 0));

    watch(() => canMove.value, (move) => {
        dragHandle.value.style.cursor = move ? 'move' : 'default';
    });

    function checkHandleTarget(target: EventTarget, element: Element) {
        // Checks if the target is the element clicked, then checks each child element of element as well
        // Ignores button clicks

        // Ignore elements of type button
        if (element.tagName === 'BUTTON') {
            return false;
        }

        // If the target was found, return true (handle was found)
        if (element === target) {
            return true;
        }

        // Recursively iterate this elements children
         
        for (const child in element.children) {
            if (Object.prototype.hasOwnProperty.call(element.children, child)) {
                if (checkHandleTarget(target, element.children[child])) {
                    return true;
                }
            }
        }

        // Handle was not found in this lineage
        // Note: return false is ignore unless it is the parent element
        return false;
    }

    function transform() {
        let translateX = movingPos.value.x + oldTranslate.value.x;
        let translateY = movingPos.value.y + oldTranslate.value.y;

        if (lockAxis.value === 'x') {
            translateX = origPos.value?.x || 0;
            movingPos.value.x = 0;
        } else if (lockAxis.value === 'y') {
            translateY = origPos.value?.y || 0;
            movingPos.value.y = 0;
        }

        const value = `translate3d(${Math.round(translateX)}px, ${Math.round(translateY)}px, 0px)`;
        if (dragTarget.value) {
            dragTarget.value.style.transform = value;
        }

        // save current position
        currentPos.value.x = translateX;
        currentPos.value.y = translateY;
    }

    function checkBounds(){
        if(!boundingElement.value || !dragTarget.value){ return null; }
        const boundary = boundingElement.value.getBoundingClientRect();
        const targetRect = dragTarget.value.getBoundingClientRect();
        const result = {
            'top':  boundary.top < targetRect.top,
            'right': boundary.right > targetRect.right,
            'bottom':  boundary.bottom > targetRect.bottom,
            'left': boundary.left < targetRect.left
        };

        if (!result.top) {
            movingPos.value.y -= targetRect.top - boundary.top;
        }

        if (!result.bottom) {
            movingPos.value.y -= targetRect.bottom - boundary.bottom;
        }

        if (!result.right) {
            movingPos.value.x -= targetRect.right - boundary.right;
        }

        if (!result.left) {
            movingPos.value.x -= targetRect.left - boundary.left;
        }

        transform();
        return result;
    }

    function moveTo(p?: Position) {
        if (p) {
            if (origPos.value) {
                p.subtract(origPos.value);
            }

            movingPos.value.set(p);
            transform();
            checkBounds();
        }
    }

    function onMouseMove(event: MouseEvent) {
        if (moving.value && allowDrag.value) {
            event.stopPropagation();
            event.preventDefault();

            // Add a transparent helper div:
            // helperBlock.add();
            moveTo(Position.fromEvent(event, dragHandle.value));
        }
    }

    function stopMove() {
        if (moving.value) {

            moving.value = false;

            oldTranslate.value.add(movingPos.value);
            movingPos.value.reset();

            dragTarget.value?.classList.remove('ng-dragging');

            document.removeEventListener('mousemove', onMouseMove);
            document.removeEventListener('mouseup', stopMove);
        }
    }

    function startMove() {
        if (!moving.value && dragHandle.value) {
            moving.value = true;
            dragHandle.value.classList.add('ng-dragging');

            document.addEventListener('mousemove', onMouseMove);
            document.addEventListener('mouseup', stopMove);
        }
    }

    function resetTranslate() {
        if (dragTarget.value) {
            const pos = Position.getTransformInfo(dragTarget.value);
            oldTranslate.value.set(pos);
            return;
        }

        oldTranslate.value.reset();
    }

    function onMouseDown(event: MouseEvent) {
        if (!canMove.value) {
            return;
        }
        // 1. skip right click;
        if (event instanceof MouseEvent && event.button === 2) {
            return;
        }
        // 2. if handle is set, the element can only be moved by handle
        const target = event.target || event.srcElement;
        if (dragHandle.value !== undefined && target && !checkHandleTarget(target, dragHandle.value)) {
            return;
        }

        // 3. if allow drag is set to false, ignore the mousedown
        if (allowDrag.value === false) {
            return;
        }

        // 隐藏其他浮动层
        document.body.click();

        event.stopPropagation();
        event.preventDefault();

        origPos.value = Position.fromEvent(event, dragTarget.value);
        resetTranslate();
        startMove();
    }

    function registerDraggle(handle: HTMLElement, hostElement: HTMLElement, boundingHost: HTMLElement | null) {
        if (allowDrag.value && hostElement) {
            if (handle) {
                dragHandle.value = handle;
            } else if (props.dragHandle) {
                if (props.dragHandle instanceof HTMLElement) {
                    dragHandle.value = props.dragHandle;
                } else if (typeof props.dragHandle === 'string') {
                    const element = hostElement.querySelector(props.dragHandle);
                    if(element) {
                        dragHandle.value = element as HTMLElement;
                    }
                }
            }

            dragTarget.value = hostElement;
            boundingElement.value = boundingHost;
            if (dragHandle.value) {
                dragHandle.value.classList.add('ng-draggable');
                dragHandle.value.addEventListener('mousedown', onMouseDown);
            } else {
                allowDrag.value = false;
            }
        }
    }

    return {
        registerDraggle, resetTranslate
    };
}
