import { ref } from "vue";
import { Position } from "./resizeable/position";
import { Size } from "./resizeable/size";
import { IResizeEvent } from "./resizeable/resize-event";

export function useResizeable(props: any, context: any) {

    const resizedTarget = ref<HTMLElement>();
    const boundingElement = ref<HTMLElement | null>();

    const origMousePos = ref<Position | undefined>();
    const origSize = ref<Size | null>();
    const origPos = ref<Position | null>();
    const currSize = ref<Size | null>();
    const currPos = ref<Position | null>();
    const direction = ref<{ 'n': boolean; 's': boolean; 'w': boolean; 'e': boolean } | null>();
    const resizeHandlerElement = ref<any>();
    const modalBounding = ref<any>();
    const resizedEventParam = ref<IResizeEvent | null>();

    const lastSize = ref<IResizeEvent | null>();

    const allowDrag = ref(props.draggable);

    const isMaximized = ref(false);

    function initBounding() {
        const el = boundingElement.value || document.body;
        const computed = window.getComputedStyle(el);
        if(!computed){
            return;
        }

        if (!resizedTarget.value) {
            return;
        }

        const transforms = Position.getTransformInfo(resizedTarget.value);


        const bounding: any = {};

        if (currPos.value) {
            bounding.deltaL = resizedTarget.value.offsetLeft - currPos.value.x;
            bounding.deltaT = resizedTarget.value.offsetTop - currPos.value.y;
        }

        const p = computed.getPropertyValue('position');
        bounding.width = el.clientWidth;
        bounding.height = el.clientHeight;
        bounding.pr = parseInt(computed.getPropertyValue('padding-right'), 10);
        bounding.pb = parseInt(computed.getPropertyValue('padding-bottom'), 10);
        bounding.position = computed.getPropertyValue('position');

        if (p === 'static') {
            el.style.position = 'relative';
        }

        bounding.translateX = transforms.x;
        bounding.translateY = transforms.y;

        modalBounding.value = bounding;
    }

    function startResize(event: MouseEvent) {

        if (resizedTarget.value) {
            origSize.value = Size.getCurrent(resizedTarget.value);
            origPos.value = Position.getCurrent(resizedTarget.value);
            currSize.value = origSize.value ? Size.copy(origSize.value) : null;
            currPos.value = origPos.value ? Position.copy(origPos.value) : null;

            initBounding();

            const directionType = (event.target as HTMLElement).getAttribute('type') || '';
            direction.value = {
                n: !!directionType.match(/n/),
                s: !!directionType.match(/s/),
                w: !!directionType.match(/w/),
                e: !!directionType.match(/e/)
            };

        }
    }

    function doResize() {
        if (resizedTarget.value) {
            const container = resizedTarget.value as HTMLElement;
            if (direction.value) {
                if ((direction.value.n || direction.value.s) && currSize.value?.height) {
                    container.style.height = currSize.value.height + 'px';
                }
                if ((direction.value.w || direction.value.e) && currSize.value?.width) {
                    container.style.width = currSize.value.width + 'px';
                }

                if (currPos.value) {
                    if (currPos.value?.x) {
                        container.style.left = currPos.value.x + 'px';
                    }
                    if (currPos.value?.y) {
                        container.style.top = currPos.value.y + 'px';
                    }
                }
            }
        }
    }

    function checkSize() {

        const minHeight = !props.minHeight ? 1 : props.minHeight;
        const minWidth = !props.minWidth ? 1 : props.minWidth;

        if (currSize.value && currPos.value &&  direction.value && origSize.value) {
            if (currSize.value.height < minHeight) {
                currSize.value.height = minHeight;

                if (direction.value.n && origPos.value) {
                    currPos.value.y = origPos.value.y + (origSize.value.height - minHeight);
                }
            }

            if (currSize.value.width < minWidth) {
                currSize.value.width = minWidth;

                if (direction.value.w && origPos.value) {
                    currPos.value.x = origPos.value.x + (origSize.value.width - minWidth);
                }
            }

            if (props.maxHeight && currSize.value.height > props.maxHeight) {
                currSize.value.height = props.maxHeight;

                if (origPos.value && direction.value.n) {
                    currPos.value.y = origPos.value.y + (origSize.value.height - props.maxHeight);
                }
            }

            if (props.maxWidth && currSize.value.width > props.maxWidth) {
                currSize.value.width =  props.maxWidth;

                if (direction.value.w && origPos.value) {
                    currPos.value.x = origPos.value.x + (origSize.value.width - props.maxWidth);
                }
            }
        }

    }

    function checkBounds(){
        if (boundingElement.value) {
            const bounding = modalBounding.value;

            if (currPos.value && currSize.value  && direction.value && origSize.value) {
                const maxWidth = bounding.width - bounding.pr - bounding.deltaL - bounding.translateX - currPos.value.x;
                const maxHeight = bounding.height - bounding.pb - bounding.deltaT - bounding.translateY - currPos.value.y;

                if (direction.value.n && (currPos.value.y + bounding.translateY < 0) && origPos.value) {
                    currPos.value.y = -bounding.translateY;
                    currSize.value.height = origSize.value.height + origPos.value.y + bounding.translateY;
                }

                if (direction.value.w && (currPos.value.x + bounding.translateX) < 0 && origPos.value) {
                    currPos.value.x = -bounding.translateX;
                    currSize.value.width = origSize.value.width + origPos.value.x + bounding.translateX;
                }

                if (currSize.value.width > maxWidth) {
                    currSize.value.width = maxWidth;
                }

                if (currSize.value.height > maxHeight) {
                    currSize.value.height = maxHeight;
                }
            }
        }
    }

    function resizeTo(p: Position) {

        if (!origMousePos.value || !origSize.value || !origPos.value || !direction.value) {
            return;
        }

        p.subtract(origMousePos.value);

        const tmpX = p.x;
        const tmpY = p.y;

        if (direction.value.n) {
            // n, ne, nw
            currPos.value!.y = origPos.value.y + tmpY;
            currSize.value!.height = origSize.value.height - tmpY;
        } else if (direction.value.s) {
            // s, se, sw
            currSize.value!.height = origSize.value.height + tmpY;
        }

        if (direction.value.e) {
            // e, ne, se
            currSize.value!.width = origSize.value.width + tmpX;
        } else if (direction.value.w) {
            // w, nw, sw
            currSize.value!.width = origSize.value.width - tmpX;
            currPos.value!.x = origPos.value.x + tmpX;
        }

        checkBounds();
        checkSize();
        doResize();
    }

    function onMouseMove(event: MouseEvent) {
        if (!resizeHandlerElement.value) {
            return;
        }

        const pos = Position.fromEvent(event);
        if (pos) {
            resizeTo(pos);
        }
    }

    function getResizedEventParam(): IResizeEvent | null {
        if (resizedTarget.value) {
            const { width, height, x, y } = resizedTarget.value.getBoundingClientRect();
            const transforms = Position.getTransformInfo(resizedTarget.value);

            return {
                size: { width, height },
                position: {x: x - transforms.x, y: y - transforms.y }
            };
        }
        return null;
    }

    function onMouseLeave(event: MouseEvent) {
        if (resizedTarget.value) {
            const resizedParam = getResizedEventParam();
            resizedEventParam.value = resizedParam;
        }

        origMousePos.value = undefined;
        origSize.value = null;
        origPos.value = null;
        currSize.value = null;
        currPos.value = null;
        direction.value = null;
        resizeHandlerElement.value = null;

        document.removeEventListener('mousemove', onMouseMove);
        document.removeEventListener('mouseup', onMouseLeave);
    }

    function registerMouseEvent() {
        document.addEventListener('mousemove', onMouseMove);
        document.addEventListener('mouseup', onMouseLeave);
    }

    function onMouseDown(event: MouseEvent) {
        // skip right click;
        if (event instanceof MouseEvent && event.button === 2) {
            return;
        }

        if (!allowDrag.value) {
            return;
        }
        // 隐藏其他浮动层
        document.body.click();
        // prevent default events
        event.stopPropagation();
        event.preventDefault();

        origMousePos.value = Position.fromEvent(event);
        resizeHandlerElement.value = event.target;
        startResize(event);
        registerMouseEvent();
    }

    function renderResizeBar(hostElement: any) {
        resizedTarget.value = hostElement;
        return <>
            <div class="fv-resizable-handle fv-resizable-n" type="n" onMousedown={(e) => onMouseDown(e)}></div>
            <div class="fv-resizable-handle fv-resizable-e" type="e" onMousedown={(e) => onMouseDown(e)}></div>
            <div class="fv-resizable-handle fv-resizable-s" type="s" onMousedown={(e) => onMouseDown(e)}></div>
            <div class="fv-resizable-handle fv-resizable-w" type="w" onMousedown={(e) => onMouseDown(e)}></div>
            <div class="fv-resizable-handle fv-resizable-ne" type="ne" onMousedown={(e) => onMouseDown(e)}></div>
            <div class="fv-resizable-handle fv-resizable-se fv-resizable-diagonal" type="se" onMousedown={(e) => onMouseDown(e)}></div>
            <div class="fv-resizable-handle fv-resizable-sw" type="sw" onMousedown={(e) => onMouseDown(e)}></div>
            <div class="fv-resizable-handle fv-resizable-nw" type="nw" onMousedown={(e) => onMouseDown(e)}></div>
        </>;
    }

    function maximize(updateLastSize = true) {
        // 隐藏其他浮动层
        document.body.click();

        const container = boundingElement.value || document.body;
        const maxSize = Size.getCurrent(container);
        const target = resizedTarget.value as HTMLElement;

        if (updateLastSize && target) {
            lastSize.value = getResizedEventParam();
            lastSize.value!.transform = target.style.transform;
        }

        if (maxSize && target) {
            currSize.value = maxSize;

            target.style.height = (currSize.value.height - 14) + 'px';
            target.style.width = (currSize.value.width - 14) + 'px';
            target.style.left = '7px';
            target.style.top = '7px';
            target.style.transform = '';

            resizedEventParam.value = {
                size: currSize.value,
                position: {x: 0, y: 0}
            };

            allowDrag.value = false;
            isMaximized.value = true;
        }
    }

    function restore() {
        // 隐藏其他浮动层
        document.body.click();

        if (lastSize.value) {
            const size = { width: lastSize.value.size.width || 0, height: lastSize.value.size.height || 0 };
            const position = { x: (window.innerWidth - size.width) / 2, y: (window.innerHeight - size.height) / 2};

            currSize.value?.set(size);
            currPos.value?.set(position);

            const target = resizedTarget.value as HTMLElement;
            target.style.height = size.height + 'px';
            target.style.width = size.width + 'px';
            target.style.left = `${position.x}px`;
            target.style.top = `${position.y}px`;
            target.style.transform = '';
            resizedEventParam.value = {
                size,
                position
            };

            allowDrag.value = props.draggable;

            isMaximized.value = false;
        }
    }

    function moveToCenter() {
        if (resizedTarget.value) {
            const modalSize = Size.getCurrent(resizedTarget.value);
            if (modalSize) {
                const { width, height } = modalSize;
                resizedTarget.value.style.left = `${(window.innerWidth - width) / 2}px`;
                resizedTarget.value.style.top = `${(window.innerHeight - height) / 2}px`;
                resizedTarget.value.style.transform = '';

            }
        }
    }

    function onWindowResize() {
        const windowResizeHandle = () => {
            if (isMaximized.value) {
                maximize(false);
            } else {
                moveToCenter();
            }
            // 隐藏其他浮动层
            document.body.click();
        };

        window.addEventListener('resize',  windowResizeHandle);
        return () => {
            window.removeEventListener('resize',  windowResizeHandle);
        };
    }

    const unWindowResizeHandle = onWindowResize();

    return {
        renderResizeBar, boundingElement, resizedEventParam, maximize, restore, allowDrag, isMaximized, unWindowResizeHandle
    };
}
