import { ref } from "vue";


function registrationShortcutKeyEvent(keyCode: string, callBack: (...args) => void) {
    if (keyCode) {
        const registerKeyDown =(event: KeyboardEvent)  =>{
            if (event.key.toLowerCase() === keyCode.toLowerCase()) {
                callBack({ event, key: keyCode });
            }
        };

        document.addEventListener('keydown', registerKeyDown);

        return () => {
            document.removeEventListener('keydown', registerKeyDown);
        };
    }

}

export function useEsc(props: any, context: any) {
    const enableEsc = ref(props.enableEsc);

    let removeEventListener: any = null;

    if (enableEsc.value) {
        removeEventListener = registrationShortcutKeyEvent('Escape', ($event: any) => {
            context.emit('esc', { event: $event.event, type: 'esc' });
        });

        return {
            remove: removeEventListener
        };
    }

    return null;
}

export function useEnter(props: any, context: any) {
    const enableEnter = ref(props.enableEnter);

    let removeEventListener: any = null;

    if (enableEnter.value) {
        removeEventListener = registrationShortcutKeyEvent('Enter', ($event: any) => {
            context.emit('enter', { event: $event.event, type: 'enter' });
        });

        return {
            remove: removeEventListener
        };
    }

    return null;

}
