/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App, Plugin } from 'vue';
import FModal from './src/modal.component';
import FModalService from './src/composition/modal.service';

export * from './src/modal.props';

export const F_MODAL_SERVICE_TOKEN = Symbol('FModalService');

FModal.install = (app: App) => {
    app.component(FModal.name as string, FModal);
    const modalInstance = new FModalService(app);
    app.provide(F_MODAL_SERVICE_TOKEN, modalInstance);
    app.provide('FModalService', modalInstance);
};

export { FModal, FModalService };
export default FModal as typeof FModal & Plugin;
