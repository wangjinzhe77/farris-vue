import { Ref, SetupContext, computed, ref } from "vue";
import { ListViewProps, MultiSelectMode } from "../list-view.props";
import { UseDraggable, UseHover, UseItem } from "./types";
import { UseSelection, VisualData } from '../../../data-view';

export function useItem(
    props: ListViewProps,
    context: SetupContext,
    visibleDatas: Ref<VisualData[]>,
    useDraggableComposition: UseDraggable,
    useHoverComposition: UseHover,
    useSelectionComposition: UseSelection,
): UseItem {
    const identifyField = ref(props.idField);
    const disableField = ref(props.disableField);
    const draggable = ref(props.draggable);
    const customListViewItemClass = ref(props.itemClass);
    // const hoverIndex = ref(-1);
    // const activeIndex = ref(-1);
    // const focusedItemId = ref('');
    const enableMultiSelect = ref(props.selection.multiSelect ?? false);
    const multiSelectMode: Ref<MultiSelectMode> = ref(props.selection.multiSelectMode as MultiSelectMode);
    // const selectedItems: Ref<any[]> = ref([]);
    const { isDragging } = useDraggableComposition;
    const { activeIndex, focusedItemId, hoverIndex } = useHoverComposition;
    const { clearSelection, getSelectedItems, toggleSelectItem } = useSelectionComposition;
    const selectedItems: Ref<any[]> = ref(getSelectedItems());

    function listViewItemClass(item: VisualData, index: number) {
        const classObject = {
            'f-list-view-group-item': true,
            'f-list-view-draggable-item': draggable.value,
            'f-un-click': !item.checked,
            'f-un-select': !!item.raw[disableField.value],
            'f-listview-active': item.raw.__fv_index__ === activeIndex.value,
            'f-listview-hover': !isDragging.value && index === hoverIndex.value,
            'moving': !!item.moving
        } as Record<string, boolean>;
        const customClassArray = customListViewItemClass.value.split(' ');
        customClassArray.reduce((result, className) => {
            result[className] = true;
            return result;
        }, classObject);
        return classObject;
    };

    function getKey(item: VisualData, index: number) {
        return item.raw[identifyField.value] || '';
    }

    const shouldFocusedItemOnCheck = computed(() => {
        return !enableMultiSelect.value;
    });

    function onCheckItem(item: VisualData, index: number, oldCheckedValue: boolean) {
        // 此时checkbox双向绑定值已改变，需要还原值重新变更
        item.checked = oldCheckedValue;
        if (item.raw[disableField.value]) {
            return;
        }
        if (shouldFocusedItemOnCheck.value) {
            focusedItemId.value = item.raw[identifyField.value];
        }
        toggleSelectItem(item);
    }

    const shouldClearSelectionOnClick = computed(() => {
        return enableMultiSelect.value && multiSelectMode.value === 'OnCheckClearByClick';
    });

    const shouldSelectItemOnClick = computed(() => {
        return !enableMultiSelect.value ||
            (
                enableMultiSelect.value &&
                (multiSelectMode.value === 'OnCheckAndClick' || multiSelectMode.value === 'OnClick')
            );
    });

    function onClickItem($event: MouseEvent, item: VisualData, index: number) {
        if (item.raw[disableField.value]) {
            $event.preventDefault();
            $event.stopPropagation();
            return;
        }
        focusedItemId.value = item.raw[identifyField.value];
        activeIndex.value = index;
        if (shouldClearSelectionOnClick.value) {
            clearSelection();
            // clearSelection(visibleDatas.value);
        }
        if (shouldSelectItemOnClick.value) {
            toggleSelectItem(item);
            // 修复selectedItems不更新的问题
            selectedItems.value = getSelectedItems();
        }
        context.emit('clickItem', { data: selectedItems.value, index });
        context.emit('activeChange', selectedItems.value);
    }

    return {
        getKey,
        listViewItemClass,
        onCheckItem,
        onClickItem
    };
}
