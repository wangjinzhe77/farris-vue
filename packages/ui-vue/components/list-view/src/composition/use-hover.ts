import { ref } from "vue";
import { UseHover } from "./types";

export function useHover(): UseHover {
    const activeItemId = ref('');
    const activeIndex = ref(-1);
    const focusedItemId = ref('');
    const hoverIndex = ref(-1);
    const suspending = ref(false);

    function onMouseenterItem($event: MouseEvent, item: any, index: number) {
        hoverIndex.value = index;
    }

    function onMouseoverItem($event: MouseEvent, item: any, index: number) {
        if (suspending.value) {
            return;
        }
        hoverIndex.value = index;
    }

    function onMouseoutItem($event: MouseEvent, item: any, index: number) {
        hoverIndex.value = -1;
    }

    function suspendHover() {
        suspending.value = true;
    }

    function resumeHover() {
        suspending.value = false;

    }

    function clearActiveItem() {
        activeIndex.value = -1;
        activeItemId.value = '';
    }

    return {
        activeIndex,
        focusedItemId,
        hoverIndex,
        clearActiveItem,
        onMouseenterItem,
        onMouseoverItem,
        onMouseoutItem,
        resumeHover,
        suspendHover
    };
}
