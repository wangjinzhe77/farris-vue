 
 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
import { SetupContext, computed, defineComponent, ref, watch } from 'vue';
import { ListViewProps, listViewProps } from './list-view.props';

export default defineComponent({
    name: 'FListViewTable',
    props: listViewProps,
    emits: ['outputValue', 'currentEvent', 'selectionChanged'] as string[] | undefined,
    setup(props: ListViewProps, context: SetupContext) {
        const mockdata = [
            { name: '任芳', code: '1.PO20198989001', date: '2024-02-19', desc: '导游1', amount: 63.85, avatar: './assets/avatar1.png' },
            { name: '戴秀兰', code: '2.PO20198989002', date: '2024-03-17', desc: '导游2', amount: 60.13, avatar: './assets/avatar2.png' },
            { name: '尹磊', code: '3.PO20198989003', date: '2024-04-09', desc: '导游3', amount: 36.54, avatar: './assets/avatar3.png' },
            { name: '赵明', code: '4.PO20198989004', date: '2024-03-27', desc: '导游4', amount: 52.76, avatar: './assets/avatar4.png' },
            { name: '吕洋', code: '5.PO20198989005', date: '2024-11-17', desc: '导游5', amount: 84.13, avatar: './assets/avatar5.png' },];
        const data = computed(() => {
            const returnValue = props.data.length > 0 ? props.data : mockdata;
            return returnValue;
        });

        return () => {
            return <>
                {data.value?.map((dataItem: any) => {
                    return (
                        <div class="f-template-listview-row">
                            <div class="list-view-item-title">
                                <div class="item-title-img">
                                    <img src={dataItem.avatar} alt="" style="width: 44px;" />
                                </div>
                                <div class="item-title">
                                    <p class="item-title-heading">{dataItem.code}</p>
                                    <p class="item-title-text">{dataItem.desc}</p>
                                </div>
                            </div>
                            <div class="list-view-item-content">
                                <div class="content-message">
                                    <div class="ower">
                                        <p>创建人</p>
                                        <p class="con">{dataItem.name}</p>
                                    </div>
                                    <div class="date">
                                        <p>创建时间</p>
                                        <p class="con">{dataItem.date}</p>
                                    </div>
                                </div>
                            </div>
                        </div>);
                })}
            </>;
        };
    }
});
