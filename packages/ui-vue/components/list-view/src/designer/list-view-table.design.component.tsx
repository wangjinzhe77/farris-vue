 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
 
import { SetupContext, computed, defineComponent, inject, onMounted, ref, watch } from 'vue';
import { ListViewTableProps, listViewTableProps } from '../list-view-table.props';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';

export default defineComponent({
    name: 'FListViewTableDesign',
    props: listViewTableProps,
    emits: ['outputValue', 'currentEvent', 'selectionChanged'] as string[] | undefined,
    setup(props: ListViewTableProps, context: SetupContext) {
        const mockdata = [
            { name: '任芳', code: '1.山东大学', date: '2024-02-19', desc: '山东济南', amount: 63.85, avatar: './assets/avatar1.png' },
            { name: '戴秀兰', code: '2.山东建筑大学', date: '2024-03-17', desc: '山东济南', amount: 60.13, avatar: './assets/avatar2.png' },
            { name: '尹磊', code: '3.山东财经大学', date: '2024-04-09', desc: '山东济南', amount: 36.54, avatar: './assets/avatar3.png' },
            { name: '赵明', code: '4.北京大学', date: '2024-03-27', desc: '北京市', amount: 52.76, avatar: './assets/avatar4.png' },
            { name: '吕洋', code: '5.南京大学', date: '2024-11-17', desc: '江苏南京', amount: 84.13, avatar: './assets/avatar5.png' },
            { name: '吕娥', code: '6.清华大学', date: '2024-03-17', desc: '北京市', amount: 52.76, avatar: './assets/avatar6.png' },
            { name: '赵强', code: '7.浙江大学', date: '2024-08-01', desc: '浙江杭州', amount: 84.13, avatar: './assets/avatar7.png' },];
        const data = computed(() => {
            const returnValue = props.data.length > 0 ? props.data : mockdata;
            return returnValue;
        });
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const componentInstance = useDesignerComponent(elementRef, designItemContext);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        return () => {
            return <div ref={elementRef}>
                {data.value?.map((dataItem: any) => {
                    return (
                        <div class="f-template-listview-row">
                            <div class="list-view-item-title">
                                <div class="item-title-img">
                                    <img src={dataItem.avatar} alt="" style="width: 44px;" />
                                </div>
                                <div class="item-title">
                                    <p class="item-title-heading">{dataItem.code}</p>
                                    <p class="item-title-text">{dataItem.desc}</p>
                                </div>
                            </div>
                            <div class="list-view-item-content">
                                <div class="content-message">
                                    <div class="ower">
                                        <p>负责人</p>
                                        <p class="con">{dataItem.name}</p>
                                    </div>
                                    <div class="date">
                                        <p>预约时间</p>
                                        <p class="con">{dataItem.date}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="list-view-item-operate">
                                {/* <a href="javascript:void(0)" class="operate-btn">付款</a>
                                <a href="javascript:void(0)" class="operate-btn">收货</a> */}
                            </div>
                        </div>);
                })}
            </div>;
        };
    }
});
