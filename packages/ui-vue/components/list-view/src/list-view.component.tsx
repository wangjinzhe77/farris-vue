
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, ref, SetupContext } from 'vue';
import { ListViewProps, listViewProps } from './list-view.props';
import { useSearch } from './composition/use-search';
import {
    DataViewOptions, UseCellEditor, VisualData, useDataView, useEdit, useFilter,
    useGroupData, useHierarchy, useIdentify, useRow, useSelection, useVisualData,
    useVisualDataBound, useVisualDataCell, useVisualDataRow, useVisualGroupRow, useVisualSummaryRow
} from '@farris/ui-vue/components/data-view';
import getListArea from './components/data/data-area.component';
import getContentHeader from './components/header/content-header.component';
import getSearchHeader from './components/header/search-header.component';
import { useHover } from './composition/use-hover';

export default defineComponent({
    name: 'FListView',
    props: listViewProps,
    emits: ['afterSearch', 'checkValuesChange', 'clickItem',
        'selectionChange', 'removeItem', 'change', 'activeChange'] as (string[] & ThisType<void>) | undefined,
    setup(props: ListViewProps, context: SetupContext) {
        const listViewContentRef = ref<any>();
        const enablePagination = ref(true);

        const mouseInContent = ref(false);
        const visibleDatas = ref<VisualData[]>([]);
        const preloadCount = 0;
        const columns = ref(props.columns);

        const useFilterComposition = useFilter();
        const useIdentifyComposition = useIdentify(props as DataViewOptions);
        const useHierarchyCompostion = useHierarchy(props as DataViewOptions);
        const useGroupDataComposition = useGroupData(props as DataViewOptions, useIdentifyComposition);
        const dataView = useDataView(props as DataViewOptions, new Map(), useFilterComposition, useHierarchyCompostion, useIdentifyComposition);
        const useSelectionComposition = useSelection(props as DataViewOptions, dataView, useIdentifyComposition, visibleDatas, context);
        const useSearchComposition = useSearch(props, listViewContentRef);

        const visibleCapacity = computed(() => {
            return dataView.dataView.value.length;
        });
        const useRowComposition = useRow(props as DataViewOptions, context as SetupContext, useSelectionComposition, useIdentifyComposition);
        const useEditComposition = useEdit(props as DataViewOptions, context as SetupContext, useIdentifyComposition, useRowComposition);

        const useVisualDataBoundComposition = useVisualDataBound(props as DataViewOptions);

        const useVisualDataCellComposition = useVisualDataCell(
            props as DataViewOptions, {} as UseCellEditor, useVisualDataBoundComposition
        );

        const useVisualDataRowComposition = useVisualDataRow(
            props as DataViewOptions,
            useEditComposition,
            useHierarchyCompostion,
            useIdentifyComposition,
            useVisualDataBoundComposition,
            useVisualDataCellComposition
        );

        const useVisualGroupRowComposition = useVisualGroupRow(props as DataViewOptions, useIdentifyComposition, useVisualDataCellComposition, useVisualDataRowComposition);

        const useVisualSummaryRowComposition = useVisualSummaryRow(props as DataViewOptions, useIdentifyComposition, useVisualDataCellComposition, useVisualDataRowComposition);

        const useVisualDataComposition = useVisualData(
            props as DataViewOptions,
            columns,
            dataView,
            visibleCapacity,
            preloadCount,
            useVisualDataRowComposition,
            useVisualGroupRowComposition,
            useVisualSummaryRowComposition
        );
        const { getVisualData } = useVisualDataComposition;
        visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);

        const listViewClass = computed(() => {
            const classObject = {
                'f-list-view': true,
                'f-list-view-multiple': props.multiSelect
            } as Record<string, boolean>;
            if (props.size !== 'default') {
                classObject[`${props.size}-item`] = true;
            }
            return classObject;
        });

        const shouldShowFooter = computed(() => {
            return !!context.slots.footer || enablePagination.value;
        });

        function getHeaderFactory() {
            if (props.header === 'SearchBar') {
                return getSearchHeader;
            }
            if (props.header === 'ContentHeader') {
                return getContentHeader;
            }
            return getSearchHeader;
        }

        const headerFactroy = getHeaderFactory();

        const { renderHeader } = headerFactroy(props, context, useSearchComposition);

        const useHoverComposition = useHover();

        /** 渲染树状区域 */
        const { renderListArea } = getListArea(props, context, visibleDatas, dataView, useGroupDataComposition, useHoverComposition, useSelectionComposition, useVisualDataComposition);

        function search(searchingText: string) {
            useSearchComposition.search(searchingText);
        }

        function updateDataSource(newData: Record<string, any>[]) {
            if (newData) {
                dataView.load(newData);
                visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
            }
        }
        function clearSelection() {
            useSelectionComposition.clearSelection();
            // 清除选中行
            useHoverComposition.clearActiveItem();
        }
        context.expose({ search, updateDataSource, clearSelection });

        function onClick(payload: MouseEvent) {
            if (props.multiSelect) {
                payload.preventDefault();
                payload.stopPropagation();
            }
        }

        return () => {
            return (
                <div class={listViewClass.value} onClick={onClick}>
                    {renderHeader()}
                    <div ref={listViewContentRef} class="f-list-view-content" onMouseover={() => { mouseInContent.value = true; }} onMouseleave={() => { mouseInContent.value = false; }}>
                        {renderListArea()}
                    </div>
                    {shouldShowFooter.value && <div class="f-list-view-footer">{context.slots.footer && context.slots.footer()}</div>}
                </div>
            );
        };
    }
});
