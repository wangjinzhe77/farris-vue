import { SetupContext, computed, ref, watch, withModifiers } from "vue";
import { ListViewProps } from "../../list-view.props";
import { UseSearch } from "../../composition/types";

export default function (
    props: ListViewProps,
    context: SetupContext,
    useSearchComposition: UseSearch
) {
    const headerClass = ref<string>(props.headerClass);
    const placeholder = ref<string>(props.placeholder);
    const searchValue = ref('');
    const shouldShowSearchIcon = computed(() => !searchValue.value);
    const shouldShowClearIcon = computed(() => !!searchValue.value);

    function clearSearchValue($event: MouseEvent) {
        searchValue.value = '';
    }

    watch(searchValue, (searchingText: string) => {
        if(props.enableHighlightSearch){
            useSearchComposition.search(searchingText);
        }
        context.emit('afterSearch',searchingText);
    });

    const searchFormGroupClass = computed(() => {
        const classObject = {
            'form-group': true,
            'farris-form-group': true
        } as Record<string, boolean>;
        if (headerClass.value) {
            const headerClassArray = headerClass.value.split(' ');
            headerClassArray.reduce<Record<string, boolean>>((result: Record<string, boolean>, classString: string) => {
                result[classString] = true;
                return result;
            }, classObject);
        }

        return classObject;
    });

    function onClick(payload: MouseEvent) { }

    function renderHeader() {
        return (
            <div class="f-list-view-header" onClick={withModifiers(() => onClick, ['prevent', 'stop'])}>
                <div class={searchFormGroupClass.value}>
                    <div class="farris-input-wrap">
                        <div class="f-cmp-inputgroup">
                            <div class="input-group f-state-editable">
                                <input class="form-control f-utils-fill text-left" v-model={searchValue.value}
                                    name="input-group-value" type="text"
                                    placeholder={placeholder.value} autocomplete="off" ></input>
                                <div class="input-group-append" >
                                    {
                                        shouldShowClearIcon.value &&
                                        <span class="input-group-text input-group-clear"
                                            onClick={(payload: MouseEvent) => clearSearchValue(payload)}>
                                            <i class="f-icon f-icon-close-circle"></i>
                                        </span>
                                    }
                                    {
                                        shouldShowSearchIcon.value && <span class="input-group-text">
                                            <span class="f-icon f-icon-search"></span>
                                        </span>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    return { renderHeader };

}
