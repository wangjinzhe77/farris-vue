import { Ref, SetupContext, computed, ref } from "vue";
import { UseDraggable, UseHover, UseItem, UseRemove } from "../../composition/types";
import { UseGroupData, UseSelection, UseVisualData, VisualData } from '@farris/ui-vue/components/data-view';
import { ListViewProps } from "../../list-view.props";

import './group-item.css';

export default function (
    props: ListViewProps,
    context: SetupContext,
    visibleDatas: Ref<VisualData[]>,
    useDraggableComposition: UseDraggable,
    useGroupDataComposition: UseGroupData,
    useHoverComposition: UseHover,
    useItemCompostion: UseItem,
    useSelectionComposition: UseSelection,
    useRemoveComposition: UseRemove,
    useVisualDataComposition: UseVisualData
) {
    const groupFields = ref(props.group?.groupFields || []);
    const { collpaseGroupIconClass } = useGroupDataComposition;
    const { toggleGroupRow } = useVisualDataComposition;

    function onToggleGroupRow($event: MouseEvent, dataItem: VisualData) {
        dataItem.collapse = !dataItem.collapse;
        visibleDatas.value = toggleGroupRow(dataItem.collapse ? 'collapse' : 'expand', dataItem, visibleDatas.value);
    }

    function renderItem(groupRow: VisualData, index: number, clickItem: any) {
        return groupRow.layer > -1 && (
            <div class="f-navlookup-recentHeader" onClick={(payload: MouseEvent) => onToggleGroupRow(payload, groupRow)}>
                <div class="fv-grid-group-row-icon" >
                    <span class={collpaseGroupIconClass(groupRow)}></span>
                </div>
                <div class="f-navlookup-recommandLabel">{groupRow.raw[groupFields.value[groupRow.layer]]}</div>
            </div>
        );
    }

    return { renderItem };
}
