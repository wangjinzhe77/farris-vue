import { Ref, SetupContext, ref } from "vue";
import { UseDraggable, UseHover, UseItem, UseRemove } from "../../composition/types";
import { ListViewProps } from "../../list-view.props";
import { UseGroupData, UseSelection, UseVisualData, VisualData } from '@farris/ui-vue/components/data-view';
import { FCheckbox } from '@farris/ui-vue/components/checkbox';

export default function (
    props: ListViewProps,
    context: SetupContext,
    visibleDatas: Ref<VisualData[]>,
    useDraggableComposition: UseDraggable,
    useGroupDataComposition: UseGroupData,
    useHoverComposition: UseHover,
    useItemCompostion: UseItem,
    useSelectionComposition: UseSelection,
    useRemoveComposition: UseRemove,
    useVisualDataComposition: UseVisualData
) {
    const enableMultiSelect = ref(props.multiSelect);
    const disableField = ref(props.disableField);
    const { onMouseenterItem, onMouseoverItem, onMouseoutItem } = useHoverComposition;
    const { getKey, listViewItemClass, onCheckItem, onClickItem } = useItemCompostion;

    function renderListViewItemContent(item: VisualData, index: number, selectedItem: any) {
        if (context.slots.content) {
            return <>{context.slots.content && context.slots.content({ item: item.raw, index, selectedItem })}</>;
        }
        return <div style="margin: 10px 0;">{item.raw.name}</div>;
    }

    function renderItem(item: VisualData, index: number, clickItem: any) {
        return (
            <li
                class={listViewItemClass(item, index)}
                id={getKey(item, index)}
                key={getKey(item, index)}
                onClick={(payload: MouseEvent) => onClickItem(payload, item, index)}
                onMouseenter={(payload: MouseEvent) => onMouseenterItem(payload, item, index)}
                onMouseover={(payload: MouseEvent) => onMouseoverItem(payload, item, index)}
                onMouseout={(payload: MouseEvent) => onMouseoutItem(payload, item, index)}
            >
                {enableMultiSelect.value && (
                    <div class="f-list-select" onClick={(payload: MouseEvent) => payload.stopPropagation()}>
                        <FCheckbox id={'list-' + getKey(item, index)} customClass="listview-checkbox"
                            disabled={item[disableField.value] || !item.checked} v-model:checked={item.checked}
                            onChange={($event: any) => onCheckItem(item, index,!$event.checked)}></FCheckbox>
                    </div>
                )}
                <div class="f-list-content">
                    {renderListViewItemContent(item, index, clickItem)}
                </div>
            </li>
        );
    }

    return { renderItem };
}
