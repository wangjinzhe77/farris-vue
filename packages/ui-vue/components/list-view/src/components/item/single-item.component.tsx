 
import { Ref, SetupContext, computed, ref } from "vue";
import { UseDraggable, UseHover, UseItem, UseRemove } from "../../composition/types";
import { UseGroupData, UseSelection, UseVisualData, VisualData } from '@farris/ui-vue/components/data-view';
import { ListViewProps } from "../../list-view.props";
import { FCheckbox } from '@farris/ui-vue/components/checkbox';

export default function (
    props: ListViewProps,
    context: SetupContext,
    visibleDatas: Ref<VisualData[]>,
    useDraggableComposition: UseDraggable,
    useGroupDataComposition: UseGroupData,
    useHoverComposition: UseHover,
    useItemCompostion: UseItem,
    useSelectionComposition: UseSelection,
    useRemoveComposition: UseRemove,
    useVisualDataComposition: UseVisualData
) {
    const listViewSize = ref(props.size);
    const textField = ref(props.textField);
    const titleField = ref(props.titleField);
    const disableField = ref(props.disableField);
    // show  checkbox
    const enableMultiSelect = computed(() => 
        props.selection?.multiSelect &&
        props.selection?.showCheckbox
    );
    const { onMouseenterItem, onMouseoverItem, onMouseoutItem } = useHoverComposition;
    const { getKey, listViewItemClass, onCheckItem, onClickItem } = useItemCompostion;

    const contentStyle = computed(() => {
        if(!props.itemClass){
            const styleObject = {
                'margin': listViewSize.value === 'small' ? '0.25rem 0' : '8px 0'
            } as Record<string, any>;
            return styleObject;
        }
        return {};        
    });

    function renderItem(item: VisualData, index: number, clickItem: any) {
        item.checked = useSelectionComposition.findIndexInSelectedItems(item) > -1;
        return (
            <li
                class={listViewItemClass(item, index)}
                id={getKey(item, index)}
                key={getKey(item, index)}
                onClick={(payload: MouseEvent) => onClickItem(payload, item, index)}
                onMouseenter={(payload: MouseEvent) => onMouseenterItem(payload, item, index)}
                onMouseover={(payload: MouseEvent) => onMouseoverItem(payload, item, index)}
                onMouseout={(payload: MouseEvent) => onMouseoutItem(payload, item, index)}
            >
                {enableMultiSelect.value && (
                    <div class="f-list-select" onClick={(payload: MouseEvent) => payload.stopPropagation()}>
                        <FCheckbox id={'list-' + getKey(item, index)} customClass="listview-checkbox"
                            disabled={item[disableField.value]} 
                            v-model:checked={item.checked}
                            onChange={(payload: any) =>{onCheckItem(item, index, !payload.checked);}}></FCheckbox>
                    </div>
                )}
                <div class="f-list-content">
                    <div style={contentStyle.value} title={item.raw[titleField.value] || item.raw[textField.value]}>{item.raw[textField.value]}</div>
                </div>
            </li>
        );
    }

    return { renderItem };
}
