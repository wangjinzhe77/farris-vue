 
import { computed, Ref, ref, SetupContext } from 'vue';
import { ListViewProps } from '../../list-view.props';
import { UseDataView, UseGroupData, UseSelection, UseVisualData, VisualData } from '../../../../data-view';
import getSingleItem from '../item/single-item.component';
import getContentItem from '../item/content-item.component';
import getDraggableItem from '../item/draggable-item.component';
import getGroupItem from '../item/group-item.component';
import { useHover } from '../../composition/use-hover';
import { useDraggable } from '../../composition/use-draggable';
import { useRemove } from '../../composition/use-remove';
import { useItem } from '../../composition/use-item';
import { UseHover } from '../../composition/types';

export default function (
    props: ListViewProps,
    context: SetupContext,
    visibleDatas: Ref<VisualData[]>,
    dataViewComposition: UseDataView,
    useGroupDataComposition: UseGroupData,
    useHoverComposition: UseHover,
    useSelectionComposition: UseSelection,
    useVisualDataComposition: UseVisualData
) {
    const listViewType = ref(props.view);
    const cardView = ref(props.view === 'CardView');
    const clickItem = ref<any>({});
    const emptyMessage = ref('暂无数据');

    // const useHoverComposition = useHover();

    const useDraggableComposition = useDraggable(props, context, dataViewComposition, useHoverComposition);

    const useRemoveComposition = useRemove(props, context, dataViewComposition);

    const useItemCompostion = useItem(props, context, visibleDatas, useDraggableComposition, useHoverComposition, useSelectionComposition);

    const listViewGroupClass = computed(() => {
        const classObject = {
            'f-list-view-group': true,
            'f-list-view-group-multiselect':props.multiSelect, 
            'd-flex': cardView.value,
            'flex-wrap': cardView.value
        } as Record<string, boolean>;
        return classObject;
    });

    const shouldShowListViewGroupItem = computed(() => !!visibleDatas.value && visibleDatas.value.length > 0);
    const showEmpty = computed(() => visibleDatas.value.length === 0);

    const shouldShowEmptyTemplate = computed(() => {
        return showEmpty.value && !context.slots.empty;
    });

    function getRenderFactory() {
        if (listViewType.value === 'SingleView') {
            return getSingleItem;
        }
        if (listViewType.value === 'DraggableView') {
            return getDraggableItem;
        }
        if ((listViewType.value === 'ContentView' || listViewType.value === 'CardView') && context.slots.content) {
            return getContentItem;
        }
        return getSingleItem;
    }

    const renderFactory = getRenderFactory();

    const { renderItem } = renderFactory(props, context, visibleDatas, useDraggableComposition, useGroupDataComposition, useHoverComposition,
        useItemCompostion, useSelectionComposition, useRemoveComposition, useVisualDataComposition);

    const { renderItem: renderGroupItem } = getGroupItem(props, context, visibleDatas, useDraggableComposition, useGroupDataComposition, useHoverComposition,
        useItemCompostion, useSelectionComposition, useRemoveComposition, useVisualDataComposition);

    const visualDataItemRenders = [renderItem, renderGroupItem];

    function renderListData() {
        return visibleDatas.value
            .filter((visualData: VisualData) => visualData.visible !== false)
            .map((visualDataRow: VisualData, index: number) => {
                return visualDataItemRenders[visualDataRow.type](visualDataRow, index, clickItem);
            });
    }

    function renderEmptyPlaceholder() {
        return (
            <div class="f-list-view-emptydata">
                <p class="f-empty-title">{context.slots.empty? context.slots.empty(): emptyMessage.value}</p>
            </div>
        );
    }

    function renderListArea() {
        return (
            <ul class={listViewGroupClass.value} style="list-style: none;">
                {shouldShowListViewGroupItem.value && renderListData()}
                {showEmpty.value && renderEmptyPlaceholder()}
                {/* {context.slots.empty && context.slots.empty()} */}
            </ul>
        );
    }

    return { renderListArea };
}
