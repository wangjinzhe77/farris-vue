
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver/src/props-resolver';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import listViewSchema from './schema/list-view.schema.json';
import propertyConfig from './property-config/list-view.property-config.json';
import { DataColumn, SelectionOptions } from '../../data-view';

export type MultiSelectMode = 'OnCheck' | 'OnClick' | 'OnCheckAndClick' | 'OnCheckClearByClick';

export type ListViewType = 'SingleView' | 'ContentView' | 'DraggableView' | 'CardView';

export type ListViewSize = 'small' | 'default' | 'large';

export type ListViewHeaderType = 'SearchBar' | 'FilterBar' | 'ContentHeader';

export const listViewProps = {
    /** 列集合 */
    columns: {
        type: Array<DataColumn>, default: [
            { field: 'name', title: '', dataType: 'string' }
        ]
    },
    data: { type: Array<any>, default: [] },
    draggable: { type: Boolean, default: false },
    multiSelect: { Type: Boolean, default: false },
    multiSelectMode: { Type: String as PropType<MultiSelectMode>, default: 'OnCheck' },
    idField: { Type: String, default: 'id' },
    valueField: { Type: String, default: 'id' },
    textField: { Type: String, default: 'name' },
    titleField: { Type: String, default: 'name' },
    view: { Type: String as PropType<ListViewType>, default: 'ContentView' },
    size: { Type: String as PropType<ListViewSize>, default: 'default' },
    placeholder: { type: String, default: '' },
    header: { Type: String as PropType<ListViewHeaderType>, default: 'ContentHeader' },
    headerClass: { Type: String, default: '' },
    itemClass: { Type: String, default: '' },
    selectionValues: { type: Array<string>, default: [] },
    /** 分组配置 */
    group: { type: Object },
    /** 选择配置 */
    selection: {
        type: Object as PropType<SelectionOptions>, default: {
            enableSelectRow: true,
            multiSelect: false,
            multiSelectMode: 'DependOnCheck',
            showCheckbox: false,
            showSelectAll: false,
            showSelection: true
        }
    },
    keepOrder: { type: Boolean, default: false },
    disableField: { type: String, default: 'disabled' },
    // 搜索启用高亮
    enableHighlightSearch: { type: Boolean, default: true },
    /** 虚拟化渲染数据 */
    virtualized: { type: Boolean, default: true }
} as Record<string, any>;

export type ListViewProps = ExtractPropTypes<typeof listViewProps>;
export const propsResolver = createPropsResolver<ListViewProps>(listViewProps, listViewSchema, schemaMapper, schemaResolver, propertyConfig);
