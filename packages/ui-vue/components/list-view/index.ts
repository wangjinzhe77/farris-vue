 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App, Plugin } from 'vue';
import FListView from './src/list-view.component';
import FListViewDesign from './src/designer/list-view.design.component';
import { propsResolver } from './src/list-view.props';
import FListViewTable from './src/list-view-table.component';
import FListViewTableDesign from './src/designer/list-view-table.design.component';
import { propsResolver2 } from './src/list-view-table.props';

export * from './src/list-view.props';

FListView.install = (app: App) => {
    app.component(FListView.name as string, FListView);
    app.component(FListViewTable.name as string, FListViewTable);
};
FListView.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['list-view'] = FListView;
    propsResolverMap['list-view'] = propsResolver;
    componentMap['list-view-table'] = FListViewTable;
    propsResolverMap['list-view-table'] = propsResolver2;
};
FListView.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['list-view'] = FListViewDesign;
    propsResolverMap['list-view'] = propsResolver;
    componentMap['list-view-table'] = FListViewTableDesign;
    propsResolverMap['list-view-table'] = propsResolver2;
};

export { FListView };
export default FListView as typeof FListView & Plugin;
