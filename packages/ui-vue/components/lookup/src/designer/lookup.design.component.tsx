/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, inject, onMounted, Ref, ref, SetupContext } from 'vue';
import FButtonEditDesign from '../../../button-edit/src/designer/button-edit.design.component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { lookupProps, LookupProps } from '../lookup.props';
import { useLookupDesignerRules } from './use-lookup-rules';

export default defineComponent({
    name: 'FLookupDesign',
    props: lookupProps,
    emits: [],
    setup(props: LookupProps, context) {
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerHostService = inject('designer-host-service');
        const designerRulesComposition = useLookupDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        return () => {
            return (
                <div class="f-lookup-design" ref={elementRef}>
                    <FButtonEditDesign
                        ref={elementRef}
                        readonly={true}
                        editable={false}
                        placeholder={props.placeholder}
                    ></FButtonEditDesign>
                </div>
            );
        };
    }
});
