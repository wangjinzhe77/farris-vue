 
import { UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema, DesignerComponentInstance, DesignerItemContext } from "../../../designer-canvas/src/types";
import { LookupPropertyConfig } from "../property-config/lookup.property-config";

export function useLookupDesignerRules(designItemContext: DesignerItemContext, designerHostService): UseDesignerRules {
    // 构造属性配置方法
    function getPropsConfig(componentId: string, componentInstance: DesignerComponentInstance) {
        const schema = designItemContext.schema as ComponentSchema;
        const inputGroupProps = new LookupPropertyConfig(componentId, designerHostService);
        return inputGroupProps.getPropertyConfig(schema, componentInstance);
    }

    return { getPropsConfig } as UseDesignerRules;

}
