import { watch } from "vue";
import { pageInfoState } from "./use-state";

export function usePageInfo(props: any){

    watch(() => props.pagination, (newValue) => {
        pageInfoState.default = newValue;
    });

    watch(() => props.navigation?.pagination, (newValue) => {
        pageInfoState.navigation = newValue;
    });

    function updatePageInfo(newPageInfo: Record<string, any>,  isNavigation = false) {
        if (newPageInfo) {
            if (!isNavigation) {
                pageInfoState.default = { ...pageInfoState.default, ...newPageInfo };
            } else {
                pageInfoState.navigation = { ...pageInfoState.navigation, ...newPageInfo };
            }
        }
    }

    return {
        updatePageInfo
    };
}
