import { computed, reactive, ref, SetupContext, watch } from "vue";
import LookupSearchBar from "../components/search-bar/search-bar.component";
import { LookupProps } from "../lookup.props";
import { useSearchFields } from "./use-search-fields";
import { searchState } from "./use-state";

export function useSearchbar(props: LookupProps, isNavigation: boolean) {
    const {searchFieldItems, navSearchFieldItems} = useSearchFields(props);

    const searchFieldList: any = ref([]);
    const navSearchFieldList: any = ref([]);

    function initSearchFieldList(searchFields, isNavigation = false) {

        let newSearchFields = searchFields;

        if (props.showAllSearchColumns) {
            newSearchFields = [{label: '所有列', value: '*'}].concat(searchFields);
        }

        if (isNavigation) {
            navSearchFieldList.value = newSearchFields;
        } else {
            searchFieldList.value = newSearchFields;
        }
    }

    initSearchFieldList(searchFieldItems.value);
    initSearchFieldList(navSearchFieldItems.value, true);

    watch(() => searchFieldItems.value, (newSearchFields) => {
        initSearchFieldList(newSearchFields);
    });

    watch(() => navSearchFieldItems.value, (newSearchFields) => {
        initSearchFieldList(newSearchFields, true);
    });

    function onSearch($event: { field: string; value: string }) {
        const searchData = {...$event};
        if (isNavigation) {
            searchState.navigation = searchData;
            return;
        }
        searchState.default = searchData;
    }

    const searchFields = computed(() => {
        return  !isNavigation ? searchFieldList.value || []: navSearchFieldList.value || [];
    });

    function renderSearchBar() {
        if (props.enableSearchBar && searchFields.value.length > 0) {
            return <div class="p-2">
                <LookupSearchBar fields={searchFields.value}
                    showAllColumns={ props.showSearchAllColumns }
                    isNavigation={ isNavigation }
                    onSearch={ onSearch }
                ></LookupSearchBar>
            </div>;
        }
    }

    return {
        renderSearchBar
    };
}
