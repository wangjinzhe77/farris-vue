import { inject, ref, watch } from "vue";
import {FTreeGrid} from '@farris/ui-vue/components/tree-grid';
import {FDataGrid} from "@farris/ui-vue/components/data-grid";
import {LookupProps } from "../lookup.props";
import { useCheckProps } from "./use-check-props";
import { useSearchbar } from "./use-search-bar";
import { usePageInfo } from "./use-pageinfo";
import { LOOKUP_HTTP_COMPOSITION, UseHttpComposition } from "./use-http";
import { navigationState, pageInfoState, searchState } from "./use-state";

import { treeGridRowOption } from "./use-treegrid-row-options";

export function useNavigation(props: LookupProps, context) {
    const useHttpComposition: UseHttpComposition = inject(LOOKUP_HTTP_COMPOSITION) as UseHttpComposition;

    const { renderSearchBar } = useSearchbar(props, true);
    const { checkPaination, navIsList, navIsTreeList } = useCheckProps(props);
    const { updatePageInfo } = usePageInfo(props);
    const { loadData } = useHttpComposition;
    const selectedItems = ref<any>([]);

    if (navIsList()) {
        const paginationOptions = checkPaination(props.navigation?.pagination);
        updatePageInfo(paginationOptions, true);
    }

    const navigationPaginationOptions = ref(pageInfoState.navigation);

    const selectionOptions = {
        enableSelectRow: true,
        multiSelect: false,
        showCheckbox: false
    };

    const columnOptions = {
        fitColumns: true,
        fitMode: 'percentage',
        resizeColumn: true
    };

    const leftTreegridRef = ref();
    const leftDatagridRef = ref();
    const idField = ref('id');

    watch(() => pageInfoState.navigation, (newPageInfo) => {
        navigationPaginationOptions.value = newPageInfo;
        leftDatagridRef.value?.updatePagination(newPageInfo);
    });

    function dataGridLoadData(items: any, total: number, pageInfo?: Record<string, any> | undefined) {
        items = items || [];
        if (navIsList()) {
            const value: any = {total};
            if (pageInfo) {
                const {pageIndex: index, pageSize: size, enablePager: enable, pageList: sizeLimits } = pageInfo;
                Object.assign(value, {index, size, enable, sizeLimits});
            }
            updatePageInfo(value, true);
            leftDatagridRef.value?.updateDataSource(items);
        } else if (navIsTreeList()) {
            leftTreegridRef.value?.updateDataSource(items);
        }
    }

    function setDatagridColumns(columns: any[]) {
        if (navIsList()) {
            leftDatagridRef.value?.updateColumns(columns);
        } else if (navIsTreeList()) {
            leftTreegridRef.value?.updateColumns(columns);
        }
    }

    watch([() => navigationState?.columns,() => navigationState?.items,
        () => navigationState?.pageInfo, () => navigationState?.total], ([newColumns, newItems, newPageInfo, newTotal]) => {
        newColumns && setDatagridColumns(newColumns);
        dataGridLoadData(newItems, newTotal || 0, newPageInfo);
    });

    watch(() => navigationState?.idField, (newIdField) => {
        idField.value = newIdField || 'id';
    });

    function onTreegridSelectionChange(items: any[]) {
        selectedItems.value = items;
    }

    function onDatagridSelectionChange(items: any[]) {
        selectedItems.value = items;
    }

    function httpRequest() {
        loadData({ search: searchState.navigation, action: 'navsearch'}, (e) => {
            dataGridLoadData(e.items, e.total || 0, e.pageInfo);
        });
    }

    watch(() => searchState.navigation, (newSearchInfo) => {
        if (props.uri) {
            updatePageInfo({index: 1}, true);
            httpRequest();
            return;
        }
        context.emit('search', newSearchInfo);
    });

    function onPageInfoChanged({pageSize, pageIndex}) {
        let index = pageIndex;
        if (navigationPaginationOptions.value?.size !== pageSize) {
            index = 1;
        }
        const params = {size: pageSize, index};
        updatePageInfo(params, true);
        if (props.uri) {
            httpRequest();
            return;
        }

        context.emit("pageSizeChanged", params);
    }

    function renderLeftTreeGrid() {
        return <FTreeGrid
            ref={leftTreegridRef}
            fit={true}
            idField={idField.value}
            rowNumber={ {enable: false} }
            columnOption={columnOptions}
            selection={selectionOptions}
            virtualized={false}
            showBorder={ true }
            rowOption={treeGridRowOption}
            onSelectionChange={onTreegridSelectionChange}></FTreeGrid>;
    }

    function renderLeftDataGrid() {
        return <FDataGrid ref={leftDatagridRef}
            pagination={navigationPaginationOptions.value}
            summary={{enable: false}}
            columnOption={columnOptions}
            idField={idField.value}
            rowNumber={{enable:false}}
            showBorder={true}
            fit={true}
            virtualized={true}
            selection={selectionOptions}
            onSelectionChange={onDatagridSelectionChange}
            onChanged={onPageInfoChanged}
        ></FDataGrid>;
    }

    function renderComponent() {
        if (navIsTreeList()) {
            return renderLeftTreeGrid();
        }

        if (navIsList()) {
            return renderLeftDataGrid();
        }
    }

    function renderNavigation() {
        return <div class="d-flex h-100 w-100 flex-column">
            {renderSearchBar()}
            <div class="f-utils-fill px-2" style="position: relative;">
                {renderComponent()}
            </div>
        </div>;
    }

    function getNavigationIdField() {
        return idField.value;
    }

    function getRelationFilter(navRows: any[]) {
        if (navigationState.relations && navigationState.relations.length) {
            const relations = navigationState.relations.map((relationItem => {
                const { groupField, helpField } = relationItem;
                const relationValue: any = { fieldName: helpField, fieldValue: '' };
                const vals = navRows.map(n => {
                    return groupField.split('.').reduce((o, c) => {
                        return o[c];
                    }, n);
                });
                relationValue.fieldValue = vals.join(',');

                return relationValue;
            }));

            return relations;
        }

        return '';
    }

    return {
        renderNavigation, selectedItems, getNavigationIdField, getRelationFilter
    };
}
