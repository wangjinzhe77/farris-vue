import { ref, watch, SetupContext, Ref, computed, inject } from "vue";
import { FDataGrid } from "../../../data-grid";
import { useCheckProps } from "./use-check-props";
import { LOOKUP_HTTP_COMPOSITION, UseHttpComposition } from "./use-http";
import { usePageInfo } from "./use-pageinfo";
import { LookupHttpResult } from "./types";
import { lookupState, pageInfoState, searchState, selectionState } from "./use-state";
import { useFavorite } from "./use-favorite";

export function useDatagrid(props: any, context: SetupContext, navigationInfo) {

    const useHttpComposition = inject(LOOKUP_HTTP_COMPOSITION) as UseHttpComposition;

    const { checkPaination, checkMultiSelect, checkColumnOptions } = useCheckProps(props);
    const { updatePageInfo } = usePageInfo(props);

    const paginationOptions = checkPaination(props.pagination);
    const selectionOptions = checkMultiSelect();
    const columnOptions = checkColumnOptions();

    const datagridRef = ref();
    const selectionValues = ref<any>([]);
    const selectedItems = ref<any>([]);

    const { navSelectedItems, getNavigationIdField, getRelationFilter } = navigationInfo;

    updatePageInfo(paginationOptions);

    const currentPaginationOptions = ref(pageInfoState.default);

    const { loadData, idValues } = useHttpComposition;
    const { favorite } = useFavorite(props);

    watch(() => pageInfoState.default, (newPageInfo) => {
        currentPaginationOptions.value = newPageInfo;
        datagridRef.value?.updatePagination(newPageInfo);
    });

    function selectItems() {
        const itemIds = selectedItems.value.map(item => item[props.idField]);
        if (itemIds && itemIds.length) {
            datagridRef.value?.selectItemByIds(itemIds);
        }
    }

    function dataGridLoadData(items: any, total: number, pageInfo: Record<string, any> | undefined) {
        const value: any = {total};
        if (pageInfo) {
            const {pageIndex: index, pageSize: size, enablePager: enable, pageList: sizeLimits } = pageInfo;
            Object.assign(value, {index, size, enable, sizeLimits});
        }
        updatePageInfo(value);
        datagridRef.value?.updateDataSource(items || []);
        selectItems();
    }

    function setDatagridColumns(columns: any[]) {
        if (props.enableFavorite) {
            columns.push(favorite);
        }
        datagridRef.value?.updateColumns(columns);
    }

    const relation = computed(() => {
        if(navSelectedItems.value && navSelectedItems.value.length) {
            return {relationFilter: getRelationFilter(navSelectedItems.value)};
        }
        return null;
    });

    watch([() => lookupState?.columns,() => lookupState?.items, () => lookupState?.pageInfo, () => lookupState?.total,
        () => lookupState?.selectedData
    ], ([newColumns, newItems, newPageInfo, newTotal, selectedData]) => {
        newColumns && setDatagridColumns(newColumns);
        if (selectedData && selectedData.length) {
            selectedItems.value = selectedData;
        }
        dataGridLoadData(newItems, newTotal || 0, newPageInfo);
    });

    watch(() => props.idValue, (newValue: string) => {
        selectionValues.value = newValue.split(props.separator);
    });

    watch(() => selectedItems.value, (newItems) => {
        selectionState.value = newItems;
    });

    function onSelectionChange(items: any[]) {
        selectedItems.value = items;
    }

    function httpRequest() {
        const queryParams= { search: searchState.default, action: 'list'};
        if (relation.value) {
            Object.assign(queryParams, relation.value);
        }
        loadData(queryParams, (result: LookupHttpResult) => {
            dataGridLoadData(result.items, result.total || 0, result.pageInfo);
        });
    }

    watch(() => navSelectedItems.value, (navSelectedRows) => {
        if (props.uri) {
            updatePageInfo({index: 1});
            httpRequest();
            return;
        }
        const navSelectIds = navSelectedRows.map(item => item[getNavigationIdField()]);
        context.emit("navSelectionsChanged", { items: navSelectedRows, ids: navSelectIds });
    });

    watch(() => searchState.default, (newSearchInfo) => {
        if (props.uri) {
            updatePageInfo({index: 1});
            httpRequest();
            return;
        }
        context.emit('search', newSearchInfo);
    });

    function onPageInfoChanged({pageSize, pageIndex}) {
        let index = pageIndex;
        if (currentPaginationOptions.value?.size !== pageSize) {
            index = 1;
        }
        const params = {size: pageSize, index, isNavigation: false};
        updatePageInfo(params);
        if (props.uri) {
            httpRequest();
            return;
        }

        context.emit("pageSizeChanged", params);
    }

    function renderDataGrid() {
        return <FDataGrid ref={datagridRef}
            pagination={currentPaginationOptions.value}
            summary={{ enable: false }}
            columnOption={columnOptions}
            idField={props.idField}
            rowNumber={{ enable: false }}
            showBorder={true}
            virtualized={true}
            fit={true}
            selection={selectionOptions}
            selectionValues={selectionValues.value}
            onSelectionChange={onSelectionChange}
            onChanged={onPageInfoChanged}
        >
        </FDataGrid>;
    };

    return {
        datagridRef,
        selectedItems,
        selectionValues,
        renderDataGrid
    };
}
