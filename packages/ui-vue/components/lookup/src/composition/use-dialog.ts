import { inject, Ref, ref, SetupContext, watch } from "vue";
import { LookupProps } from "../lookup.props";
import { F_NOTIFY_SERVICE_TOKEN, FNotifyService } from "../../../../components/notify";
import { queryState, lookupState, navigationState, pageInfoState, searchState, selectionState } from "./use-state";
import { useCheckProps } from "./use-check-props";

/** 窗口关闭类型 */
enum CloseType {
    cancel = 'cancel',
    accept = 'accept',
    esc = 'esc'
};

export function useDialog(
    props: LookupProps, context: SetupContext, selectedItems: Ref<any[]>, {
        dictPicked, modelValue, buttonEditInstance, userDataService, idValue }: any) {
    const notifyService: FNotifyService | null = inject(F_NOTIFY_SERVICE_TOKEN, null);
    const loadingService: any | null = inject('FLoadingService');

    const dialogOptions = ref(props.dialog);

    const dialogSize = ref();

    const dialogCloseType = ref(CloseType.accept);

    const customData = ref();

    const { isDoubleList } = useCheckProps(props);

    async function updateModelValue() {
        const displayText = selectedItems.value.map((item: any) => {
            return item[props.textField];
        }).join(props.separator);

        const idValues = selectedItems.value.map((item: any) => {
            return item[props.idField];
        }).join(props.separator);

        modelValue.value = displayText;
        idValue.value = idValues;

        context.emit('update:modelValue', displayText);
        context.emit('update:idValue', idValues);
        const mappingInfo = { items: selectedItems.value, mappingFields: props.mappingFields };
        context.emit('update:dataMapping', mappingInfo);

        if (props.context) {
            const { editor } = props.context;
            if (editor) {
                Object.assign(editor, mappingInfo);
            }
        }

        await dictPicked({ items: selectedItems.value });

        return true;
    }

    function cancelDialog() {
        dialogCloseType.value = CloseType.cancel;
        return true;
    }

    const clearObject = (obj: Record<string, any>) => {
        Object.keys(obj).forEach(key => {
            delete obj[key];
        });
    };

    const cleanObjects = (objects: Array<any>) => {
        objects.forEach(obj => {
            clearObject(obj);
        });
    };

    function destroyed() {
        loadingService?.clearAll();
        const targets = [lookupState, navigationState, pageInfoState, searchState, queryState];
        cleanObjects(targets);
        selectionState.value = [];
        customData.value = null;
    }

    async function submitDialog() {
        let shouldUpdateValue = true;
        let message = '';

        if (selectedItems.value.length === 0) {
            notifyService?.warning({ message: '请选择数据！', position: 'top-center' });
            return false;
        }

        if (props.beforeSelectData && typeof props.beforeSelectData === 'function') {
            const shouldSubmit = await props.beforeSelectData({ items: selectedItems.value });
            if (typeof shouldSubmit === 'boolean') {
                shouldUpdateValue = shouldSubmit;
            }

            if (typeof shouldSubmit === 'object') {
                shouldUpdateValue = shouldSubmit.canSelect;
                message = shouldSubmit.message || '';
            }
        }

        if (shouldUpdateValue) {
            updateModelValue();
            dialogCloseType.value = CloseType.accept;
            return true;
        }

        if (message) {
            notifyService?.warning({ message, position: 'top-center' });
        }
        return false;
    }
    async function beforeOpenDialog($event?: any) {

        let shouldContinue = true;
        if (props.dictPicking && typeof props.dictPicking === 'function') {
            const result = await props.dictPicking({ context: { customData } });
            if (typeof result === 'boolean' && !result) {
                shouldContinue = false;
            }

            if (typeof result === 'object') {
                if (!result.showDialog) {
                    shouldContinue = false;

                    if (result.message) {
                        notifyService?.error({ message: result.message, position: 'top-center' });
                    }
                } else if (result.data) {
                    customData.value = result.data;
                }
            }
        }

        return shouldContinue;
    }

    function getDefaultDialogSize() {
        const height = dialogOptions.value?.height || 620;
        const width = dialogOptions.value?.width || 590;
        const minWidth = dialogOptions.value?.minWidth || 300;
        const minHeight = dialogOptions.value?.minHeight || 200;

        const defaultSize = {
            height, width, minWidth, minHeight
        };

        if (isDoubleList()) {
            defaultSize.width = dialogOptions.value?.width || 960;
        }

        return defaultSize;
    }

    const { width, height, minHeight, minWidth } = getDefaultDialogSize();

    const modalOptions = {
        title: dialogOptions.value?.title,
        fitContent: false,
        showHeader: false,
        dragHandle: '.lookup-modal-header',
        height,
        width,
        minWidth,
        minHeight,
        showMaxButton: dialogOptions.value?.showMaxButton == null ? true : dialogOptions.value?.showMaxButton,
        showCloseButton: dialogOptions.value?.showCloseButton == null ? true : dialogOptions.value?.showCloseButton,
        resizeable: true,
        draggable: true,
        closedCallback: ($event: Event, closeFrom: string) => {
            const emitParams: any = { type: dialogCloseType.value, button: $event?.target };

            if (closeFrom === 'esc' || closeFrom === 'icon') {
                emitParams.type = CloseType.esc;
            }

            if (emitParams.type === CloseType.accept) {
                emitParams.items = selectedItems.value;
            }

            if (emitParams.type === CloseType.cancel || emitParams.type === CloseType.esc) {
                // 还原
                modelValue.value = props.modelValue;
            }

            // 保存用户数据
            if (userDataService && props.enableUserData) {
                userDataService.saveUserData().then(() => {
                    destroyed();
                    context.emit('dialogClosed', emitParams);
                });
                return;
            }

            destroyed();
            context.emit('dialogClosed', emitParams);
        },
        buttons: [
            {
                name: 'cancel',
                text: '取消',
                class: 'btn btn-secondary',
                handle: ($event: MouseEvent) => {
                    return cancelDialog();
                }
            },
            {
                name: 'accept',
                text: '确定',
                class: 'btn btn-primary',
                handle: ($event: MouseEvent) => {
                    return submitDialog();
                }
            }
        ],
        resizeHandle: ($event: any) => {
            dialogSize.value = $event;
            const { width, height } = $event.newSize.size;
            userDataService.setDialogSize({ width: Math.round(width), height: Math.round(height) });
        },
        enableEnter: true,
        onEnter: ($event: any) => {
            $event.event.preventDefault();
            const isInSearchInput = $event.event.target.closest('.fv-lookup-search-bar');

            if (isInSearchInput) {
                return;
            }

            submitDialog().then((canClose: boolean) => {
                if (canClose) {
                    const modalInstance = buttonEditInstance.value?.getModal();
                    if (modalInstance) {
                        modalInstance.close($event.event, 'enter');
                    }
                }
            });
        }
    };

    return {
        modalOptions,
        beforeOpenDialog,
        dialogSize,
        customData,
        updateModelValue
    };
}
