import { LookupProps } from "../lookup.props";
import { LookupDisplayType, LookupPagination } from "./types";

export function useCheckProps(props: LookupProps) {
    function checkPaination(paginationOptions?: Partial<LookupPagination>) {
        const defaultPaginationOptions = {
            enable: true,
            sizeLimits: [10, 20, 30, 50, 100],
            size: 20,
            index: 1,
            total: 0,
            mode: 'server'
        };

        const mergedOptions =  Object.assign({}, defaultPaginationOptions,  paginationOptions);
        return mergedOptions;
    }

    function checkMultiSelect() {
        const selectionOption = {
            enableSelectRow: true,
            multiSelect: props.multiSelect,
            showCheckbox: props.multiSelect,
            multiSelectMode: 'OnCheckAndClick'
        };

        return selectionOption;
    }

    function checkColumnOptions() {
        return {
            fitColumns: props.fitColumns,
            fitMode: 'percentage',
            resizeColumn: true
        };
    }

    function isDoubleList() {
        const isValid = typeof props.displayType === 'string' && props.displayType in LookupDisplayType;
        if (!isValid) {
            return props.displayType.toLowerCase().indexOf('nav') === 0;
        }

        return false;
    }

    function isTreeList() {
        return props.displayType.toLowerCase() === LookupDisplayType.Tree.toLowerCase() ||
        props.displayType.toLowerCase() === LookupDisplayType.ListTree.toLowerCase();
    }

    function navIsTreeList() {
        return props.displayType.toLowerCase() === LookupDisplayType.TreeList.toLowerCase();
    }

    function navIsList() {
        const currentDisplayType = props.displayType.toLowerCase();
        return currentDisplayType === LookupDisplayType.ListList.toLowerCase() ||
            currentDisplayType === LookupDisplayType.ListTree.toLowerCase();
    }

    function getNavigationSize() {
        const width = 300;
        const minWidth = 200;
        if (props.navigation) {
            return {
                width: props.navigation.width || 320,
                minWidth: props.navigation.minWidth || 200
            };
        }
        return {
            width,
            minWidth
        };
    }

    return { checkPaination, checkMultiSelect, checkColumnOptions, isDoubleList, getNavigationSize, isTreeList, navIsTreeList, navIsList };
}
