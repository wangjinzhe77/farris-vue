import { inject, reactive, ref } from "vue";

import { encrypt } from '@farris/ui-vue/components/common';
import { F_LOOKUP_HTTP_SERVICE_TOKEN, LookupHttpResult, LookupHttpService, LookupRequestParams, LookupTabs } from "./types";

export interface LookupUserData {
    pageSize?: number;
    tabIndex?: string;
    cascadeStatus?: boolean;
    favorites?: Array<string>;
    size?: Record<string, any>;
}

export interface UserDataState {
    key: string;
    data: LookupUserData;
    favoriteItems?: any[];
}

export const LOOKUP_USER_DATA_SERVICE = 'F_LOOKUP_USER_DATA_SERVICE';

export interface UserDataService {
    userDataState: any;
    saveUserData: () => Promise<any>;
    getFavoriteData: () => Promise<any>;
    getUserData: () => Promise<any>;
    setDialogSize: (size: { width: number; height: number }) => void;
}

export function useUserData(props: any, context: any): UserDataService {
    const loadingService: any | null = inject('FLoadingService', null);

    const httpService: LookupHttpService | null = inject(F_LOOKUP_HTTP_SERVICE_TOKEN, null);

    const controlId = ref(props.id);
    function builUserDataKey() {
        return encrypt(controlId.value);
    }

    const userDataState = reactive<UserDataState>({ key: builUserDataKey(), data: {} });

    function getUserData() {
        if (props.enableFavorite && httpService) {
            return httpService.getSettings(userDataState.key).then(res => {
                if (res && res.textValue) {
                    userDataState.data = JSON.parse(res.textValue);
                } else {
                    userDataState.data = {};
                }

                if (!props.enableUserData) {
                    userDataState.data.tabIndex = LookupTabs.dataList;
                }

                return res;
            });
        }

        return Promise.resolve();
    }

    function setDialogSize({ width, height }) {
        userDataState.data.size = userDataState.data?.size || {};
        userDataState.data.size = { width, height };
    }

    function saveUserData() {
        if (props.enableFavorite && httpService) {
            const { key, data } = userDataState;
            const configData = {
                configkey1: key,
                configkey2: '',
                configkey3: '',
                textvalue: JSON.stringify(data)
            };

            return httpService?.updateSettings(key, configData);
        }
        return Promise.resolve();
    }

    function buildQueryParams(event: any) {
        const searchValue = { category: 'fav', favoriteIds: event.favoriteIds };
        const params = {
            searchValue: JSON.stringify(searchValue),
            pageIndex: 1,
            pageSize: 500
        };

        return params;
    }

    function getFavoriteData() {
        const params = buildQueryParams({ favoriteIds: userDataState.data?.favorites }) as Partial<LookupRequestParams>;
        loadingService?.show();
        if (httpService) {
            return httpService.getData(props.uri, params).then((res: Partial<LookupHttpResult>) => {
                loadingService?.clearAll();
                userDataState.favoriteItems = res && res.items || [];
                return res;
            });
        }
        return Promise.resolve();
    }

    return {
        userDataState,
        setDialogSize,
        saveUserData,
        getFavoriteData,
        getUserData
    };
}
