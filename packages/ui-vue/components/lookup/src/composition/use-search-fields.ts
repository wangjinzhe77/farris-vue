import { ref, watch } from "vue";
import { SearchField } from "./types";
import { lookupState, navigationState } from "./use-state";

export function useSearchFields(props: any) {
    const searchFieldItems = ref(props.searchFields || []);
    const navSearchFieldItems = ref(props.navigation?.searchFields || []);

    watch(() => lookupState?.searchFields, (newSearchFields) => {
        searchFieldItems.value = newSearchFields;
    });

    watch(() => navigationState?.searchFields, (newSearchFields) => {
        navSearchFieldItems.value = newSearchFields;
    });

    return {
        searchFieldItems,
        navSearchFieldItems
    };
}
