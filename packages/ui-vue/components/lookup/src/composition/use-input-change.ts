import { isEmpty } from "lodash-es";
import { computed, Ref, ref, watch } from "vue";
import { UseHttpComposition } from "./use-http";
import { BeforeOpenDialogFunction, LookupHttpResult } from "./types";
import { lookupState, queryState } from './use-state';

export type LookupInputChangeOptions = {
    updateModelValue: () => void;
    selectedItems: Ref<any[]>;
    openDialog: () => void;
    beforeOpenDialog: BeforeOpenDialogFunction;
    modelValue: Ref<string>;
    useHttpComposition: UseHttpComposition;
};

export function useInputChange(props: any, context: any, options: LookupInputChangeOptions) {
    const changeOnBlur = computed(() => props.textChangeType === 'blur' || props.textChangeType === 'any');
    const changeOnEnter  = computed(() => props.textChangeType === 'enter' || props.textChangeType === 'any');

    const {beforeOpenDialog, updateModelValue, selectedItems, openDialog, modelValue, useHttpComposition} = options;
    const {updateSearchFieldTitle} = useHttpComposition;

    function isTextChange(text: string) {
        if (isEmpty(text)) {
            return false;
        }

        const isChange = props.modelValue !== text;
        if (isChange) {
            queryState.value = text;
        } else {
            queryState.value = '';
        }
        return isChange;
    }

    async function queryDataBySearchKeys(searchText: string) {
        const searchParams = {
            search: { field: '*', value: searchText, type: 'equal' },
            action: 'search'
        };

        if (beforeOpenDialog) {
            const shouldContinue = await beforeOpenDialog(searchParams);
            if (shouldContinue === false) {
                return;
            }

            document.body.classList.add("lookup-modal-open");
            useHttpComposition.loadData(searchParams, (data: LookupHttpResult) => {
                const onlyOne = data.items && data.items.length === 1 && (!data.items[0].children || !data.items[0].children.length);
                if (onlyOne) {
                    selectedItems.value = data.items || [];
                    updateModelValue();
                    document.body.classList.remove('lookup-modal-open');
                    return;
                }

                if (data.searchFields && data.columns) {
                    const searchFields = updateSearchFieldTitle(data.searchFields, data.columns);
                    lookupState.searchFields = searchFields;
                }

                openDialog();
            });
        }
    }

    function onTextChange(event: Event, isBlur: boolean) {
        let searchText = (event.target as HTMLInputElement)?.value;
        if (searchText !== '') {
            searchText = searchText.trim();
        }
        if (isTextChange(searchText)) {
            if (props.uri) {
                queryDataBySearchKeys(searchText);
            } else {
                context.emit('textChanged', {value: searchText, type: isBlur ? 'blur': 'enter'});
            }
        }
    }

    function onInputBlur(event: MouseEvent) {
        if (changeOnBlur.value) {
            onTextChange(event, true);
        }
    }

    function onEnterKeyDown(event: KeyboardEvent) {
        if (event.key === 'Enter' && changeOnEnter.value) {
            onTextChange(event, false);
        }
    }

    return {
        changeOnBlur,
        changeOnEnter,
        onInputBlur,
        onEnterKeyDown
    };
}
