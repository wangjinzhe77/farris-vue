import { inject, ref, SetupContext, watch } from "vue";
import { LookupProps } from "../lookup.props";
import { FTreeGrid } from '@farris/ui-vue/components/tree-grid';
import { useCheckProps } from "./use-check-props";
import { lookupState, searchState, selectionState } from "./use-state";
import { LookupHttpResult } from "./types";
import { LOOKUP_HTTP_COMPOSITION, useHttp, UseHttpComposition } from "./use-http";
import { treeGridRowOption } from "./use-treegrid-row-options";


export function useTreegrid(props: LookupProps, context: SetupContext) {
    const useHttpComposition = inject(LOOKUP_HTTP_COMPOSITION) as UseHttpComposition;
    const { checkMultiSelect, checkColumnOptions } = useCheckProps(props);

    const selectionOptions = checkMultiSelect();
    const columnOptions = checkColumnOptions();

    const treegridRef = ref();
    const selectionTreeValues = ref([]);
    const selectedTreeNodes = ref<any>([]);

    const { loadData, idValues } = useHttpComposition;

    function dataGridLoadData(items: any) {
        treegridRef.value?.updateDataSource(items || []);
        if (idValues.value) {
            treegridRef.value?.selectItemByIds(idValues.value.split(props.separator));
        }
    }

    function setDatagridColumns(columns: any[]) {
        treegridRef.value?.updateColumns(columns);
    }

    watch([() => lookupState?.columns, () => lookupState?.items,
        () => lookupState?.pageInfo, () => lookupState?.total,
        () => lookupState?.selectedData], ([newColumns, newItems, newPageInfo, newTotal, selectedData]) => {

        newColumns && setDatagridColumns(newColumns);
        dataGridLoadData(newItems);

        if (selectedData && selectedData.length) {
            selectedTreeNodes.value = selectedData;
        }
    });

    watch(() => selectedTreeNodes.value, (newItems) => {
        // context.emit("update:modelValue", newItems);
        selectionState.value = newItems;
    });

    function onSelectionChange(items: any[]) {
        selectedTreeNodes.value = items;
    }

    function httpRequest() {
        loadData({ search: searchState.default, action: 'list' }, (result: LookupHttpResult) => {
            dataGridLoadData(result.items);
        });
    }

    watch(() => searchState.default, (newSearchInfo) => {
        if (props.uri) {
            httpRequest();
            return;
        }
        context.emit('search', newSearchInfo);
    });


    const hierarchy = { cascadeOption: { autoCheckChildren: false, autoCheckParent: false } };

    function renderTreeGrid() {

        return <FTreeGrid
            ref={treegridRef}
            fit={true}
            data={props.data}
            idField={props.idField}
            columns={props.columns}
            rowNumber={{ enable: false }}
            columnOption={columnOptions}
            selection={selectionOptions}
            showBorder={true}
            virtualized={false}
            displayField={'code'}
            rowOption={treeGridRowOption}
            hierarchy={hierarchy}
            onSelectionChange={onSelectionChange}></FTreeGrid>;
    };

    return {
        treegridRef,
        selectedTreeNodes,
        selectionTreeValues,
        renderTreeGrid
    };
}
