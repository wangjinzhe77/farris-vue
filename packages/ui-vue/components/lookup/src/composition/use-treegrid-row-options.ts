import { RowOptions, VisualData } from "@farris/ui-vue/components/data-view";

export const treeGridRowOption: Partial<RowOptions> = {
    customRowStatus: (visualData: VisualData) => {
        if (visualData.collapse === undefined) {
            visualData.collapse = !visualData.raw.expanded;
        }
        return visualData;
    }
};
