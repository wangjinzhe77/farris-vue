import { reactive, ref } from "vue";
import { LookupHttpResult, PagerInfoSate, QueryState, SearchState } from "./types";


export const pageInfoState = reactive<Partial<PagerInfoSate>>({});
export const lookupState = reactive<LookupHttpResult>({});
export const navigationState = reactive<LookupHttpResult>({});
export const queryState = reactive<QueryState>({});
export const searchState = reactive<SearchState>({});
export const selectionState = ref<any[]>([]);

