export interface SearchParams {
    field: string;
    value: string;
    isNavigation: boolean;
}

export enum LookupDisplayType {
    List = 'LIST',
    Tree = 'TREELIST',
    ListList = 'NAVLIST',
    TreeList = 'NAVTREELIST',
    ListTree = 'NAVLISTTREE'
}

export enum LoadTreeDataType {
    default = 'default',
    all ='all',
    async = 'async'
}


export interface PagerChangeParams{
    pageIndex?: number;
    pageSize?: number;
    isNavigation: boolean;
}

export type LookupIdValueType = string | number | null;

export type ShouldContinueResult = {showDialog: boolean; message?: string; data?: any} | boolean;
export type CanSelectResult = {canSelect: boolean; message?: string} | boolean;

export type BeforeOpenDialogFunction = (params?: any) => Promise<ShouldContinueResult>;
export type BeforeSubmitFunction = (params: any) => CanSelectResult;
export type CallBackFunction = (items: any[], options: Record<string, any>) => Promise<any>;

export const LOOKUP_ACTIVE_TAB = '__lookup_active_tab__';
export const LOOKUP_USER_DATA_ID = '__lookup_user_data_id__';

export enum LookupTabs {
    dataList = 'datalist',
    favorite = 'favorite'
}

export interface LookupPagination {
    enable?: boolean;
    sizeLimits?: number[];
    size?: number;
    index?: number;
    showLimits?: boolean;
    total?: number;
    showGoto?: boolean;
    mode?: 'server'| 'client';
}

export interface SearchField {
    value: string;
    label: string;
    isDefault?: boolean;
}

export interface NavigationOptions {
    data: any[];
    columns: any[];
    searchFields?: SearchField[];
    idField: string;
    pagination?: LookupPagination;
    width?: number;
    minWidth?: number;
}

export type TextChangedType = 'blur' | 'enter' | 'any';

export const F_LOOKUP_HTTP_SERVICE_TOKEN = Symbol('f_lookup-http-service_token');

export enum Compare {
    Equal = 0,
    NotEqual = 1,
    Greater = 2,
    GreaterOrEqual = 3,
    Less = 4,
    LessOrEqual = 5,
    Like = 6,
    LikeStartWith = 7,
    LikeEndWith = 8,
    NotLike = 9,
    NotLikeStartWith = 10,
    NotLikeEndWith = 11,
    Is = 12,
    IsNot = 13,
    In = 14,
    NotIn = 15
}
export enum FilterRelation {
    Empty = 0,
    And = 1,
    Or = 2
}

export enum ExpressValueType {
    Value = 0,
    Expression = 1,
    FrontExpress = "frontExpress"
}

export interface FilterCondition {
    filterField: string;
    value: string;
    lbracket?: string;
    rbracket?: string;
    relation?: FilterRelation;
    compare?: Compare;
    expresstype?: ExpressValueType;
}

export type LOAD_DATA_TYPE = 'all' | 'fav' | 'list' | 'nav' | 'search' | 'children' |
'textchange' | 'sort' | 'navsearch' | 'selected' | 'allChildren' | 'navAllChildren';

export interface SearchParam {
    searchField?: string;
    searchValue?: string;
    searchType?: string;
    category?: LOAD_DATA_TYPE;
    favoriteIds?: string[];
    sortName?: string;
    sortOrder?: string;
    parentLayer?: number;
    parentPath?: string;
    layerNum?: number;
}

export interface LookupRequestParams {
    pageIndex?: number;
    pageSize?: number;
    searchConditions?: Array<FilterCondition>;
    navSearchConditions?: Array<FilterCondition>;

    /** 查询参数 searchParam
     * category: 查询类型,
     *           'all'|'list'|'nav' | 'search' | 'children' | 'textchange' | 'sort' | 'navsearch'
     *          |'fav'|'addFavorite'|'removeFavorite'|'updateFavorite';
     * searchField?: 查询字段
     * searchValue?: 关键字
     * searchType: 'equal' | 'like'  查询方式
     */
    searchValue?: string;
    /** 自定义查询数据 */
    customData?: any;
    /**
     * 树结构加载方式
     * default: 内置取数; all: 加载所有; async：分层加载
     */
    loadTreeDataType?: LoadTreeDataType;
    /** 构造完整树 */
    enableFullTree?: boolean;

    /** 导航帮助对应关系条件 */
    relationFilter?: Array<{fieldName: string; fieldValue: any}>;
    /** 已选择数据ID */
    selectedInfo?: { selectedIds: string[]; selected: boolean };
    /** 树表帮助，获取所有子级数据时的所有选中的ID列表 */
    parentsIds?: any[];
    /** 左树右列表帮助，左树为分层分级码加载时，点击节点的分级码 */
    navPathCode?: any;
    /** 将树结构数据转换为普通列表 */
    treeToList?: boolean;
    navTreeToList?: boolean;
    /** 树结构分页取数 */
    treePagination?: boolean;
}

export interface DisplayInfo{
    valueField?: string;
    textField?: string;
    idField?: string;
}

export type ONLY_SELECT_LEAF = 'default' | 'yes' | 'no';

export interface TreeInfo {
    /** 数据加载方式： all, async */
    loadDataType?: 'all' | 'async';
    /** 分层方式： pathcode, parentId */
    layerType?: string;
    /** 数据中带有分级信息的字段 */
    dataField?: string;
    /** 父ID字段 */
    parentField?: string;
    /** 分级码字段 */
    pathField?: string;
    /** 级数字段 */
    layerField?: string;
    /** 是否明细字段 */
    isDetailField?: string;
    /**
     * 只允许选择叶子节点
     * - default: 应用服务器端设置，如服务器未设置，功能同  `no`
     * - `yes`: 服务器端设置失效。仅允许选择叶子节点。
     * - `no`:  服务器端设置失效。所有节点均可选择。
     */
    onlySelectLeaf?: boolean | ONLY_SELECT_LEAF;
    /** 树结构数据是否已初始化完成 */
    treeDataIsInit?: boolean;
}

export interface LookupHttpResult {
    /** 列信息 */
    columns?: any[];
    displayInfo?: DisplayInfo;
    displayType?: string;
    /** 数据列表 */
    items?: any[];
    navigation?: LookupHttpResult;
    /** 树分级信息 */
    treeInfo?: TreeInfo;
    /** 分页信息 */
    pageInfo?: { pageIndex: number; pageSize: number; pageList?: number[]; enablePager: boolean };
    /** 查询字段 */
    searchFields?: { label: string; value: string }[];
    /** 已选择数据列表 */
    selectedData?: any;
    /** 总记录数 */
    total?: number;
    title?: string;
    idField?: string;
    relations?: Array<{groupField: string; helpField: string}>;
}

export interface LookupHttpService {
    getData(uri: string, params: Partial<LookupRequestParams>): Promise<LookupHttpResult>;
    getSettings(id: string): Promise<any>;
    updateSettings(id: string, settings: any): Promise<any>;
}

export interface SearchInfo {
    field?: string;
    value?: any;
    type?: 'like' | 'equal' | '';
}

// eslint-disable-next-line @typescript-eslint/no-empty-object-type
export interface QueryState extends SearchInfo {
}

export interface SearchState {
    navigation?: SearchInfo;
    default?: SearchInfo;
}

export interface PagerInfoSate {
    navigation?: LookupPagination;
    default: LookupPagination;
}
