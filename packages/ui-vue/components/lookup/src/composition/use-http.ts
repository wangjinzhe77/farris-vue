import { inject, Ref, ref, toRaw } from "vue";
import {
    Compare, FilterRelation, LOAD_DATA_TYPE, F_LOOKUP_HTTP_SERVICE_TOKEN,
    LookupHttpResult, LookupHttpService, LookupRequestParams, SearchParam
} from "./types";
import { FLoadingService } from "@farris/ui-vue/components/loading";
import { flatten } from "lodash-es";
import { useSearchFields } from "./use-search-fields";
import { pageInfoState } from "./use-state";
import { customData } from "./use-customdata";

export const LOOKUP_HTTP_COMPOSITION = 'F_LOOKUP_HTTP_COMPOSITION_TOKEN';

export interface UseHttpComposition {
    idValues: Ref<any>;
    loadData: (event: any, callback: (data: LookupHttpResult) => void) => void;
    getData: (params: any) => Promise<any>;
    updateSearchFieldTitle: (searchFields: any[], columns: any[]) => any[];
}

export function useHttp(props: any, context: any): UseHttpComposition {
    const loadingService: any | null = inject<FLoadingService>('FLoadingService');
    const httpService = inject<LookupHttpService>(F_LOOKUP_HTTP_SERVICE_TOKEN);

    const { searchFieldItems } = useSearchFields(props);
    const uri = ref(props.uri);
    const idValues = ref(props.idValue);
    function buildConditionsWhenSearchAnyFields(searchFields: any[], searchValues: string, isNavSearch = false): any[] {
        // 处理空值或只包含空格的情况
        if (searchValues == null || !searchValues.trim()) {
            return [];
        }

        const searchKeys = Array.from(new Set(searchValues.trim().split(' ').filter(n => n.trim())));

        // 如果没有搜索字段或搜索关键字，则直接返回空数组
        if (!searchFields || !searchKeys.length) {
            return [];
        }

        const conditions = (searchFields || []).map((n: { label: string; value: string }, i) => {
            const fieldConditions = searchKeys.map((s, j) => {
                return {
                    filterField: n.value,
                    value: s,
                    lbracket: '',
                    rbracket: '',
                    relation: FilterRelation.And,
                    compare: Compare.Like,
                };
            });
            if (fieldConditions.length) {
                if (fieldConditions.length > 1) {
                    fieldConditions[0].lbracket = '(';
                    fieldConditions[fieldConditions.length - 1].rbracket = ')';
                }

                fieldConditions[fieldConditions.length - 1].relation = FilterRelation.Or;
            }
            return fieldConditions;
        }).filter(n => n.length);

        let tempConditions: any[] = [];
        if (conditions && conditions.length) {
            tempConditions = flatten(conditions);
            if (tempConditions.length > 1) {
                tempConditions[0].lbracket += '(';
                tempConditions[tempConditions.length - 1].rbracket += ')';
            }
            tempConditions[tempConditions.length - 1].relation = 0;
        }

        return tempConditions;
    }

    function buildSearchParam(event: any, action: LOAD_DATA_TYPE) {
        const searchParam: SearchParam = {};
        searchParam.category = action;
        let navSearchConditions: any = null;
        let searchConditions: any = null;

        if (event?.search) {

            const { field, value, isNavigation, type } = event.search;
            const searchType = type || 'like';
            if (value) {
                searchParam.searchValue = value.trim();
            }

            searchParam.searchField = field ? field.trim() : '*';
            searchParam.searchType = searchType;

            if (isNavigation) {
                searchParam.category = 'navsearch';
            }

            if (!props.enableMultiFieldSearch && searchParam.searchField === '*') {
                const conditions = buildConditionsWhenSearchAnyFields(searchFieldItems.value, value, isNavigation);
                if (conditions && conditions.length) {
                    if (isNavigation) {
                        navSearchConditions = conditions;
                    } else {
                        searchConditions = conditions;
                    }
                }

                if (value === '' && searchParam.category === 'search' &&
                    (!searchConditions && !navSearchConditions)) {
                    searchParam.category = 'all';
                }
            }
        }

        return {
            searchParam,
            navSearchConditions,
            searchConditions,
        };
    }

    function getPageInfo(action: LOAD_DATA_TYPE) {
        if (action === 'fav') {
            return {
                pageIndex: 1,
                pageSize: 500
            };
        }

        let prop = 'default';
        if (action === 'navsearch') {
            prop = 'navigation';
        }
        return {
            pageIndex: pageInfoState[prop]?.index || 1,
            pageSize: pageInfoState[prop]?.size || 20
        };
    }

    function buildQueryParams(event: any) {
        const queryParams: LookupRequestParams = {};
        const action: LOAD_DATA_TYPE = event?.action || 'all';

        const { searchParam, navSearchConditions, searchConditions } = buildSearchParam(event, action);
        if (searchConditions) {
            queryParams.searchConditions = searchConditions;
        }
        if (navSearchConditions) {
            queryParams.navSearchConditions = navSearchConditions;
        }

        queryParams.searchValue = JSON.stringify(searchParam);

        const { pageIndex, pageSize } = getPageInfo(action);
        queryParams.pageIndex = pageIndex;
        queryParams.pageSize = pageSize;

        queryParams.treeToList = props.treeToList;
        queryParams.navTreeToList = props.navTreeToList;

        queryParams.loadTreeDataType = props.loadTreeDataType;

        if (event?.relationFilter) {
            queryParams.relationFilter = [...event.relationFilter];
        }

        if (event.selectedInfo) {
            queryParams.selectedInfo = event.selectedInfo;
        }

        queryParams.customData = toRaw(customData.value);

        return queryParams;
    }

    function enableColumnResizable(columns: any[]) {
        return columns?.map(n => {
            n.resizable = true;
            return n;
        });
    }

    function updateSearchFieldTitle(fields: any[], columns: any[]) {
        return fields.map(field => {
            const column = columns?.find(col => col.field.toLowerCase() === field.value.toLowerCase());
            if (column) {
                field.label = column.title;
            }
            return field;
        });
    }

    function initColumnsInfo(res: LookupHttpResult) {
        if (res.columns) {
            res.columns = enableColumnResizable(res.columns);
            if (res.searchFields) {
                res.searchFields = updateSearchFieldTitle(res.searchFields, res.columns);
            }
        }

        if (res.navigation && res.navigation.columns) {
            res.navigation.columns = enableColumnResizable(res.navigation.columns);
            if (res.navigation.searchFields) {
                res.navigation.searchFields = updateSearchFieldTitle(res.navigation.searchFields, res.navigation.columns);
            }
        }
    }

    function getData(params: LookupRequestParams): Promise<LookupHttpResult> {
        // console.log('LOOKUP request Params:', params);
        if (uri.value && httpService) {
            return httpService.getData(uri.value, params).then(res => {
                initColumnsInfo(res);
                return res;
            });
        }

        return new Promise((resolve, reject) => {
            resolve({
                columns: [...props.columns],
                items: [...props.data],
                searchFields: [...props.searchFields],
                navigation: { ...props.navigation }
            });
        });
    }

    function loadData(event: any, callback: (e: LookupHttpResult) => void) {
        const params = buildQueryParams(event) as Partial<LookupRequestParams>;
        loadingService?.show();
        getData(params).then((req: Partial<LookupHttpResult>) => {
            loadingService?.clearAll();
            callback(req);
        });
    }

    return {
        idValues,
        getData,
        loadData,
        updateSearchFieldTitle
    };
}
