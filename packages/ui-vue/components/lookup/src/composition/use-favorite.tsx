import { computed, inject, ref, watch } from "vue";
import { LOOKUP_USER_DATA_SERVICE, UserDataService } from "./use-user-data";
import { F_NOTIFY_SERVICE_TOKEN, FNotifyService } from "../../../notify";
import { lookupState } from "./use-state";
import { cloneDeep } from "lodash-es";

export function useFavorite(props: any) {
    const notifyService = inject(F_NOTIFY_SERVICE_TOKEN) as FNotifyService;
    const userDataService = inject(LOOKUP_USER_DATA_SERVICE) as UserDataService;
    const { userDataState } = userDataService;

    const columns = ref();

    userDataState.data = userDataState.data || {};
    const favoriteFieldName = '_favorite_';

    const removeFavoriteItem = ref(false);

    function updateFavorites({id, data}, actiion: 'add' | 'remove' = 'add') {
        if (id == null && id === '') {
            return;
        }
        const itemId = id;

        userDataState.data.favorites = userDataState.data?.favorites || [];
        userDataState.favoriteItems = userDataState.favoriteItems || [];

        if (actiion === 'add') {
            removeFavoriteItem.value = false;
            userDataState.data.favorites = Array.from(new Set([...userDataState.data?.favorites || [], itemId]));
            userDataState.favoriteItems = [...userDataState.favoriteItems, data];
        } else {
            removeFavoriteItem.value = true;
            userDataState.data.favorites = userDataState.data.favorites.filter(id => id !== itemId);
            userDataState.favoriteItems = userDataState.favoriteItems.filter(item => item[props.idField] !== itemId);
        }

        userDataService?.saveUserData().then((res) => {
            const message = actiion === 'add' ? '已添加到收藏' : '已从收藏中移除';
            notifyService?.success({ message, position: 'top-center' });
        });

    }

    function getRowId(row) {
        return row ? row.raw[props.idField] : '';
    }

    const isFavorite = computed(() => {
        return (row: any) => {
            const itemId = getRowId(row);
            return userDataState.data?.favorites?.includes(itemId);
        };
    });

    function onFavoriteClick($evnet: MouseEvent, row: any, cell: any) {
        $evnet.stopPropagation();
        const itemId = getRowId(row);
        updateFavorites({id: itemId, data: row.raw}, !isFavorite.value(row) ? 'add' : 'remove');
    }

    const favoriteColumn = {
        field: favoriteFieldName, title: '', width: '50px', fixed: 'right', align: 'center',
        formatter: (cell, row) => {
            return <button type="button" onClick={($event) => { onFavoriteClick($event, row, cell);}}
                class={[ 'btn btn-link f-icon mx-2 f-icon-star-outline', { 'f-icon-star': isFavorite.value(row) }]}
                style="color: #ffbd8c;" title={ isFavorite.value(row) ? '取消收藏': '收藏' }></button>;
        }
    };

    watch(() => lookupState?.columns, (newColumns) => {
        columns.value = cloneDeep([...newColumns ||[]]);
    });

    return {
        favorite: favoriteColumn,
        columns,
        removeFavoriteItem
    };
}
