export function useLookupCallBack(props: any) {

    const {mapFields, separator} =  props;
    const eventParams = { mapFields, separator  };

    const dictPicked = async (items) => {
        if (props.dictPicked && typeof props.dictPicked === 'function') {
            await props.dictPicked(items, eventParams);
        }
    };

    return { dictPicked };

}
