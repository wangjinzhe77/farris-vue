/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, inject, onMounted, onUnmounted, reactive, ref, watch } from 'vue';
import { FButtonEdit } from '../../button-edit';
import LookupContainer from './components/lookup-container.component';
import LookupFavorite from './components/favorite/lookup-favorite.component';
import ModalContainer from './components/modal-container.component';
import { lookupProps, LookupProps } from './lookup.props';
import { useDialog } from './composition/use-dialog';
import { PagerChangeParams, SearchParams } from './composition/types';
import { F_MODAL_SERVICE_TOKEN, FModalService } from '../../modal';
import { customData } from "./composition/use-customdata";
import { useLookupCallBack } from './composition/use-lookup-callback';
import { useInputChange } from './composition/use-input-change';
import { selectionState } from './composition/use-state';
import { useHttp } from './composition/use-http';
import { useUserData } from './composition/use-user-data';

export default defineComponent({
    name: 'FLookup',
    props: lookupProps,
    emits: ['update:modelValue', 'update:idValue','search', 'navSelectionsChanged', 'pageIndexChanged', 'pageSizeChanged',
        'resize', 'dialogClosed', 'textChanged', 'clear', 'update:dataMapping'],
    setup(props: LookupProps, context) {
        const buttonIcon = '<i class="f-icon f-icon-lookup"></i>';
        const elementRef = ref();
        const modelValue = ref(props.modelValue);
        const modalTitle = ref();
        customData.value = ref(props.customData);

        const useHttpComposition = useHttp(props, context);
        const userDataService = useUserData(props, context);

        function openDialog() {
            elementRef.value && !elementRef.value.getModal() && elementRef.value.openDialog();
        }

        const {dictPicked} = useLookupCallBack(props);
        const { modalOptions, beforeOpenDialog, dialogSize, customData: customQueryData, updateModelValue } =
            useDialog(props, context as any, selectionState, {
                dictPicked, modelValue, 
                buttonEditInstance: elementRef, userDataService,
                idValue: useHttpComposition.idValues
            });

        const { onInputBlur, onEnterKeyDown } = useInputChange(props, context,
            { beforeOpenDialog, updateModelValue, selectedItems: selectionState, openDialog, modelValue, useHttpComposition });

        const modalConfigs = reactive(modalOptions);

        const modalService: FModalService|null = inject(F_MODAL_SERVICE_TOKEN, null);

        watch(() => customQueryData.value, (newData) => {
            customData.value = newData;
        });

        watch(() => props.modelValue, (newValue) =>{
            modelValue.value = newValue;
        });

        function updateIdValue(value: any) {
            useHttpComposition.idValues.value = value;
            context.emit('update:idValue', value);
        }

        onMounted(() => {
        });

        onUnmounted(() => {
            if (props.context) {
                const { editor } = props.context;
                if (editor) {
                    Object.assign(editor, { items: [], mappingFields: {} });
                }
            }
        });
        
        watch(() => dialogSize.value, (newSizeValue) => {
            context.emit('resize', newSizeValue);
        });

        async function onClear() {
            selectionState.value = [];
            context.emit('update:modelValue', '');
            context.emit('update:idValue', '');

            const clearParams = {items: null, mappingFields: props.mappingFields};
            context.emit('clear', clearParams);
            if (props.context) {
                const { editor } = props.context;
                if (editor) {
                    Object.assign(editor, clearParams);
                }
            }
        }

        function onSearch($event: SearchParams) {
            context.emit('search', $event);
        }

        function onNavSelectionsChanged($event) {
            context.emit('navSelectionsChanged', $event);
        }

        function onPageIndexChanged(params: Partial<PagerChangeParams>) {
            context.emit('pageIndexChanged', params);
        }

        function onPageSizeChanged(params: Partial<PagerChangeParams>) {
            context.emit('pageSizeChanged', params);
        }

        async function updateDialogOptions(options: any) {
            const modalInstance = modalService?.getCurrentModal();
            if (modalInstance) {
                modalInstance.updateModalOptions(options);
            }

            if (options.title) {
                modalTitle.value = options.title;
            }
        }

        function onCloseModal($event: MouseEvent) {
            const modalInstance = modalService?.getCurrentModal();
            if (modalInstance) {
                modalInstance.close($event);
            }
        }

        function onMaximizeModal($event) {
            const modalInstance = modalService?.getCurrentModal();
            if (modalInstance) {
                modalInstance.maxDialog($event);
            }
        }

        context.expose({
            openDialog,
            updateIdValue,
            mappingFields: props.mappingFields,
            idField: props.idField
        });

        return () => {
            return (
                <FButtonEdit
                    ref={elementRef}
                    id={props.id}
                    v-model={modelValue.value}
                    disable={props.disable}
                    readonly={props.readonly}
                    editable={props.editable}
                    inputType={"text"}
                    enableClear={props.enableClear}
                    buttonContent={buttonIcon}
                    buttonBehavior={"Modal"}
                    modalOptions={modalConfigs}
                    onClear={onClear}
                    beforeOpen={beforeOpenDialog}
                    onBlur={onInputBlur}
                    onKeydown={onEnterKeyDown}
                    placeholder={props.placeholder}
                >
                    <ModalContainer title={ modalTitle.value || '请选择'} enableFavorite={props.enableFavorite}
                        onCloseModal={onCloseModal} onMaximize={ ($event) => onMaximizeModal($event)}
                        useHttpComposition= {useHttpComposition} userDataService={userDataService}>
                        {{
                            default: () => (
                                <LookupContainer
                                    {...props}
                                    onSearch={onSearch}
                                    onNavSelectionsChanged={onNavSelectionsChanged}
                                    onPageIndexChanged={onPageIndexChanged}
                                    onPageSizeChanged={onPageSizeChanged}
                                    onChangeDialogOptions={updateDialogOptions}
                                ></LookupContainer>
                            ),
                            fav: () => (
                                <LookupFavorite idField={props.idField} displayType={props.displayType}></LookupFavorite>
                            )
                        }}
                    </ModalContainer>
                </FButtonEdit>
            );
        };
    }
});
