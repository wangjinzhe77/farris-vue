import { ExtractPropTypes, PropType } from 'vue';
import { UserDataService } from '../composition/use-user-data';
import { UseHttpComposition } from '../composition/use-http';

export const lookupModalContainerProps = {
    title: { type: String },
    height: { type: Number, default: 50 },
    draggable: { type: Boolean, default: true },
    enableFavorite: { type: Boolean, default: false },
    useHttpComposition: { type: Object as PropType<UseHttpComposition>, default: {} },
    userDataService: { type: Object as PropType<UserDataService>, default: {} }
};
export type LookupModalContainerProps = ExtractPropTypes<typeof lookupModalContainerProps>;
