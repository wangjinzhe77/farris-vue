import { computed, defineComponent, inject, KeepAlive, onMounted, provide, ref, Transition } from "vue";

import { lookupModalContainerProps, LookupModalContainerProps } from "./modal-container.props";
import { LOOKUP_ACTIVE_TAB, LookupTabs } from "../composition/types";
import { LOOKUP_USER_DATA_SERVICE, useUserData } from "../composition/use-user-data";
import { LOOKUP_HTTP_COMPOSITION } from "../composition/use-http";

export default defineComponent({
    name: 'FLookupModalContainer',
    props: lookupModalContainerProps,
    emits: ['closeModal', 'maximize'],
    setup(props: LookupModalContainerProps, context) {

        const activeTab = ref(LookupTabs.dataList);
        provide(LOOKUP_ACTIVE_TAB, activeTab);
        provide(LOOKUP_HTTP_COMPOSITION, props.useHttpComposition);
        provide(LOOKUP_USER_DATA_SERVICE, props.userDataService);

        const { userDataState, getUserData } = props.userDataService;

        function onMaximizeModal($event) {
            context.emit('maximize', $event);
        }

        function onCloseModal($event) {
            context.emit('closeModal', $event);
        }

        function onTabClick(tabName: string) {
            activeTab.value = tabName as LookupTabs;
            userDataState.data.tabIndex = tabName;
        }

        const activeTabStyle = computed(() => {
            return {
                transform: `translate3d(${activeTab.value === LookupTabs.favorite ? 84 : 0}px, 0px, 0px)`
            };
        });

        const showTabContentStyle = computed(() => {
            return (tabName) => {
                return {display: activeTab.value === tabName ? 'block' : 'none!important'};
            };
        });

        onMounted(() => {
            getUserData().then((res: any) => {
                if (userDataState.data.favorites && userDataState.data.favorites.length) {
                    activeTab.value = userDataState.data?.tabIndex || LookupTabs.dataList;
                }
            });
        });

        return () => <div class="d-flex flex-column h-100 lookup-modal">
            <div class="d-flex justify-content-between align-items-center px-3 lookup-modal-header">
                <div class="lookup-modal-header-title" title={props.title}>{props.title}</div>
                {props.enableFavorite &&  <div class="d-flex justify-content-center w-100">
                    <div class="lookup-modal-header-tabs-container d-flex" onMousedown={(event) => event.stopPropagation()}>
                        <div class="active-tab" style={activeTabStyle.value}></div>
                        <div class={['tab-item', 'd-flex', {'active': activeTab.value === LookupTabs.dataList}]}
                            onClick={() => onTabClick(LookupTabs.dataList)}>数据列表</div>
                        <div class={['tab-item', 'd-flex', {'active': activeTab.value === LookupTabs.favorite}]}
                            onClick={() => onTabClick(LookupTabs.favorite)}>收藏夹</div>
                    </div>
                </div>}

                <div class="d-flex flex-row h-100 justify-content-between align-items-center lookup-modal-header-buttons"
                    onMousedown={(event) => event.stopPropagation()}>
                    <span class="f-icon modal_maximize" onClick={onMaximizeModal} style="cursor: pointer"></span>
                    <span class="f-icon modal_close" onClick={onCloseModal} style="cursor: pointer"></span>
                </div>
            </div>
            <div class="f-utils-fill">
                {/* <Transition mode={'out-in'} name={'fade'}>
                    <div class="h-100 d-flex flex-column mx-2 f-utils-fill">
                        {activeTab.value === LookupTabs.dataList && context.slots.default && context.slots.default()}
                        {activeTab.value === LookupTabs.favorite && context.slots.fav && context.slots.fav()}
                    </div>
                </Transition> */}
                <div class="h-100 d-flex flex-column mx-2 f-utils-fill" style={showTabContentStyle.value(LookupTabs.dataList)}>
                    {context.slots.default && context.slots.default()}
                </div>
                <div class="h-100 d-flex flex-column mx-2 f-utils-fill" style={showTabContentStyle.value(LookupTabs.favorite)}>
                    {context.slots.fav && context.slots.fav()}
                </div>
            </div>
        </div>;
    }
});
