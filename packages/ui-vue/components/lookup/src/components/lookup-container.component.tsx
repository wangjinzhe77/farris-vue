import { defineComponent, ref, inject, watch, provide, onMounted, reactive, onUnmounted, Ref } from "vue";
import { FLayout, FLayoutPane } from "../../../../components/layout";

import { LookupProps, lookupProps } from "../lookup.props";

import { useDatagrid } from "../composition/use-datagrid";
import { useTreegrid } from "../composition/use-treegrid";
import { useSearchbar } from "../composition/use-search-bar";

import { useCheckProps } from "../composition/use-check-props";
import { useNavigation } from "../composition/use-navigation";
import { LOOKUP_HTTP_COMPOSITION, UseHttpComposition } from "../composition/use-http";
import { LOOKUP_ACTIVE_TAB, LookupHttpResult, LookupTabs } from "../composition/types";
import { lookupState, navigationState, queryState } from "../composition/use-state";

export default defineComponent({
    name: "LookupContainer",
    props: lookupProps,
    emits: ['update:modelValue', 'search', 'navSelectionsChanged',  'pageIndexChanged', 'pageSizeChanged',
        'changeDialogOptions', 'clearSearch'
    ],
    setup(props: LookupProps, context) {
        const currentTab = inject<Ref<string>>(LOOKUP_ACTIVE_TAB, ref(LookupTabs.dataList));
        const useHttpComposition = inject(LOOKUP_HTTP_COMPOSITION) as UseHttpComposition;

        const { renderSearchBar } = useSearchbar(props, false);
        const { isDoubleList, getNavigationSize, isTreeList } = useCheckProps(props);
        const { renderNavigation, selectedItems:navSelectedItems, getNavigationIdField, getRelationFilter } = useNavigation(props, context);

        function getRenderDataComponent() {
            if (isTreeList()) {
                const { renderTreeGrid } = useTreegrid(props, context as any);
                return renderTreeGrid;
            }

            const { renderDataGrid } = useDatagrid(props, context as any, { navSelectedItems, getNavigationIdField, getRelationFilter });
            return renderDataGrid;
        }

        const renderDataComponent = getRenderDataComponent();

        const leftPanelWidth = getNavigationSize();

        function getIdValues() {
            try {
                const idvalues = useHttpComposition.idValues.value;

                if (idvalues !== '' && idvalues != null) {
                    return idvalues;
                }

                return '';

            } catch (error) {
                console.error('Error in getIdValues:', error);
                return '';
            }
        }

        function initData() {
            const params: Record<string, any> = {};
            if (props.enableToSelect) {
                let currentIdValues = getIdValues() || [];
                if (!Array.isArray(currentIdValues)) {
                    currentIdValues = (''+ currentIdValues).split(props.separator);
                }

                params.selectedInfo = { selectedIds: currentIdValues, selected: true };

                if (queryState.value != null) {
                    params.search = {'field': '*', value: queryState.value, type: 'like'};
                    params.action = 'search';
                }
            }

            useHttpComposition?.loadData(params, (result: LookupHttpResult) => {
                if (result.navigation) {
                    Object.keys(result.navigation).forEach(key => {
                        navigationState[key] = result.navigation?.[key];
                    });

                    delete result.navigation;
                }

                Object.keys(result).forEach(key => {
                    lookupState[key] = result[key];
                });

                context.emit('changeDialogOptions', {title: result.title});
            });
        }

        watch(() => currentTab.value, (newValue, oldValue) => {
            // console.log(newValue, oldValue);
        });

        onMounted(() => {
            initData();
            document.body.classList.add('lookup-modal-open');
        });

        onUnmounted(() => {
            document.body.classList.remove('lookup-modal-open');
        });

        return () => {
            return (
                <FLayout>
                    <FLayoutPane position={'left'}
                        width={leftPanelWidth?.width} minWidth={leftPanelWidth?.minWidth} visible={isDoubleList()}>
                        {renderNavigation()}
                    </FLayoutPane>
                    <FLayoutPane position={'center'} >
                        <div class="d-flex h-100 w-100 flex-column">
                            {renderSearchBar()}
                            <div class="f-utils-fill px-2" style="position: relative;">
                                {renderDataComponent()}
                            </div>
                        </div>
                    </FLayoutPane>
                </FLayout>
            );
        };
    }
});
