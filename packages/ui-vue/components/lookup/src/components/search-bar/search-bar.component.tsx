import { computed, defineComponent, inject, onMounted, Ref, ref, watch } from "vue";
import { FButtonEdit } from "@farris/ui-vue/components/button-edit";
import { FComboList } from "../../../../combo-list";
import { searchBarProps, SearchBarProps } from "./search-bar.props";
import { SearchField } from "../../composition/types";
import { queryState } from "../../composition/use-state";

export default defineComponent({

    name: "FLookupSearchBar",
    props: searchBarProps,
    emits: ["search"],
    setup(props: SearchBarProps, context) {
        const searchFields = ref(props.fields || []);
        const searchField = ref('');
        const searchValue = ref(queryState.value);
        const isNavigation = ref(props.isNavigation);
        const searchFieldRef = ref<any>();

        const eventParams = computed(() => {
            return {
                field: searchField.value,
                value: searchValue.value
            };
        });

        function setDefaultSearchField() {
            if (!searchFields.value.length) {
                return;
            }

            let searchFieldValue = '';

            if (props.showAllColumns) {
                searchFieldValue = '*';
            }

            searchFieldValue = searchFields.value[0].value;

            const defaultSearchField = searchFields.value.find((field: SearchField) => {
                return !!field.isDefault;
            });
            if (defaultSearchField) {
                searchFieldValue = defaultSearchField.value;
            }

            searchField.value = searchFieldValue;
        }

        watch(() => props.fields, (newFields) => {
            searchFields.value = newFields;
            setDefaultSearchField();
        });

        function onSearch() {
            context.emit('search', eventParams.value);
        }

        function onClearSearchValue() {
            searchValue.value = '';
            onSearch();
        }

        function onEnterHandler($event: KeyboardEvent) {
            if ($event.key === 'Enter') {
                onSearch();
            }
        }

        function hidePopup() {
            searchFieldRef.value.hidePopup();
        }

        function onInputClick() {
            hidePopup();
            document.body.click();
        }

        onMounted(() => {
            setDefaultSearchField();
            if (isNavigation.value) {
                searchValue.value = '';
            }
        });

        return () => {
            return <div class="fv-lookup-search-bar d-flex flex-row">
                <div style="width: 160px;">
                    <FComboList
                        ref={searchFieldRef}
                        data={searchFields.value}
                        textField={'label'}
                        valueField={'value'}
                        idField={'value'}
                        v-model={searchField.value}
                        enableClear={false}
                        onClick={onInputClick}
                    ></FComboList>
                </div>
                <div class="f-utils-fill ml-2">
                    <FButtonEdit buttonContent={'<i class="f-icon f-icon-search"></i>'}
                        v-model={searchValue.value}
                        onClickButton={onSearch}
                        enableClear={true}
                        onClear={onClearSearchValue}
                        onKeyup={onEnterHandler}
                        onClick={onInputClick}
                    ></FButtonEdit>
                </div>
            </div>;
        };
    }
});
