import { ExtractPropTypes } from "vue";
import { SearchField } from "../../composition/types";

export const searchBarProps = {
    fields: { type: Array<SearchField>, default: () => [] },
    showAllColumns: { type: Boolean, default: true },
    isNavigation: { type: Boolean, default: false },
};

export type SearchBarProps = ExtractPropTypes<typeof searchBarProps>;
