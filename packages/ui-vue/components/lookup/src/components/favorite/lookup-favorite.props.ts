import { ExtractPropTypes } from 'vue';

export const lookupFavoriteComponentProps = {
    multiSelect: { type: Boolean, default: false },
    idField: { type: String },
    displayType: {type: String, default: 'list'}
};
export type LookupFavoriteComponentProps = ExtractPropTypes<typeof lookupFavoriteComponentProps>;
