import { defineComponent, inject, onMounted, ref, Ref, watch } from "vue";
import { LOOKUP_USER_DATA_SERVICE, UserDataService } from "../../composition/use-user-data";
import { FDataGrid } from "../../../../data-grid";
import { FTreeGrid } from '../../../../tree-grid';
import { lookupFavoriteComponentProps } from "./lookup-favorite.props";
import { LOOKUP_ACTIVE_TAB, LookupHttpResult, LookupTabs } from "../../composition/types";
import { useFavorite } from "../../composition/use-favorite";
import { useCheckProps } from "../../composition/use-check-props";

export default defineComponent({
    name: 'FLookupFavorite',
    props: lookupFavoriteComponentProps,
    setup(props, context) {
        const {userDataState, getFavoriteData}: any = inject<UserDataService>(LOOKUP_USER_DATA_SERVICE);
        const currentTab = inject<Ref<string>>(LOOKUP_ACTIVE_TAB, ref(LookupTabs.dataList));
        const { columns, favorite, removeFavoriteItem } = useFavorite(props);
        const { isTreeList } = useCheckProps(props);

        const favoriteDataGridRef = ref();
        const favoriteTreeGridRef = ref();

        watch(() => columns.value, (newColumns) => {
            const gridRef = isTreeList() ? favoriteTreeGridRef : favoriteDataGridRef;
            gridRef.value.updateColumns(newColumns);
            // gridRef.value.updateDataSource(userDataState.favoriteItems || []);
        });

        function hasFavorites() {
            return userDataState.data && userDataState.data.favorites && userDataState.data.favorites.length;
        }

        function loadFavoriteData(items: any, columns: any) {
            const gridRef = isTreeList() ? favoriteTreeGridRef : favoriteDataGridRef;
            if (columns && columns.length) {
                columns =columns.map(n => {
                    n.resizable = true;
                    return n;
                });
                gridRef.value.updateColumns([...columns, favorite]);
            }
            items && gridRef.value.updateDataSource(items || []);
        }

        function queryFavoriteData() {
            getFavoriteData().then((res: LookupHttpResult) => {
                if (res) {
                    loadFavoriteData(res.items || [], res.columns);
                } else {
                    loadFavoriteData([], null);
                }
            });
        }


        watch(() => currentTab.value, (newValue, oldValue) => {
            if (newValue === LookupTabs.favorite) {
                if (hasFavorites()) {
                    queryFavoriteData();
                } else {
                    loadFavoriteData([], null);
                }
                // if ((!userDataState.favoriteItems || !userDataState.favoriteItems.length) || isTreeList()) {
                //     getFavoriteData().then((res: LookupHttpResult) => {
                //         loadFavoriteData(res.items || []);
                //     });
                // }
                //  else {
                //     loadFavoriteData(userDataState.favoriteItems || []);
                // }
            }
        });

        watch(() => removeFavoriteItem.value, (newValue, oldValue) => {
            if (currentTab.value === LookupTabs.favorite && newValue) {
                if (!isTreeList()) {
                    loadFavoriteData(userDataState.favoriteItems || [], null);
                } else {
                    queryFavoriteData();
                }
                removeFavoriteItem.value = false;
            }
        });

        const columnOptions= {
            fitColumns: true,
            fitMode: 'percentage',
            resizeColumn: true
        };

        const selectionOption = {
            enableSelectRow: true,
            multiSelect: props.multiSelect,
            showCheckbox: props.multiSelect,
            multiSelectMode: 'DependOnCheck'
        };

        function onSelectionChange() {}

        onMounted(() => {

        });

        function renderTreeGrid() {
            return <FTreeGrid
                ref={favoriteTreeGridRef}
                fit={true}
                idField={props.idField}
                rowNumber={{ enable: false }}
                columnOption={columnOptions}
                selection={selectionOption}
                showBorder={true}
                virtualized={true}
                onSelectionChange={onSelectionChange}></FTreeGrid>;
        };

        function renderDataGrid() {
            return <FDataGrid
                ref={favoriteDataGridRef}
                pagination={{ enable: false}}
                summary={{ enable: false }}
                columnOption={columnOptions}
                idField={props.idField}
                rowNumber={{ enable: false }}
                showBorder={true}
                virtualized={true}
                fit={true}
                selection={selectionOption}
                onSelectionChange={onSelectionChange}>
            </FDataGrid>;
        }

        return () => {
            return isTreeList() ? renderTreeGrid() : renderDataGrid();
        };
    }
});
