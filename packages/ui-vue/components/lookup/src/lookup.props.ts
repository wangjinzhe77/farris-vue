import { ExtractPropTypes, PropType } from "vue";
import { createPropsResolver } from "../../dynamic-resolver";

// import lookupDefaultConfig from './property-config/lookup.property-config.json';
import { schemaMapper } from './schema/schema-mapper';
import lookupSchema from './schema/lookup.schema.json';
import { schemaResolver } from './schema/schema-resolver';
import { BeforeOpenDialogFunction, BeforeSubmitFunction, CallBackFunction, LoadTreeDataType,
    LookupDisplayType, LookupIdValueType, LookupPagination,
    NavigationOptions, SearchField, TextChangedType } from "./composition/types";

export const lookupProps = {
    id: {type: String, require: true},
    uri: { type: String, default: '' },
    readonly: {type: Boolean,default: false},
    disabled: {type: Boolean,default: false},
    editable: {type: Boolean,default: false},
    enableFavorite: {type: Boolean, default: false},
    mappingFields: { type: Object, default: null },
    fitColumns: { type: Boolean, default: true },
    columns: { type: Array, default: [] },
    idField: { type: String, default: 'id' },
    textField: {type: String, defalut: 'name'},
    data: { type: Array, default: [] },
    displayType: { type: String as PropType<LookupDisplayType>, default: LookupDisplayType.List },
    dialog: {
        type: Object,
        default: {
            title: '请选择'
        }
    },
    multiSelect: {type: Boolean, default: false},
    separator: {type: String, default: ','},
    pagination: {type: Object as PropType<LookupPagination>, default: {
        enable: false,
        showLimits: true,
        sizeLimits: [10, 20, 30,50, 100],
        size: 20,
        index: 1,
        total: 0,
        mode: 'server',
        showGoto: false
    }},
    enableClear: {type: Boolean, default: true},
    inputType: {type: String, default: 'text'},
    idValue: {type: String as PropType<LookupIdValueType>, default: ''},
    dictPicking: {type: Function as PropType<BeforeOpenDialogFunction>, default: null},
    dictPicked: {type: Function as PropType<CallBackFunction>, default: null},
    beforeSelectData: {type: Function as PropType<BeforeSubmitFunction>, default: null},
    enableSearchBar: { type: Boolean, default: true },
    searchFields: { type: Array as PropType<SearchField[]>, default: [] },
    showAllSearchColumns: { type: Boolean, default: true },
    navigation: { type: Object as PropType<NavigationOptions>, default: null},
    textChangeType: { type: String as PropType<TextChangedType>, default: 'any' },
    enableMultiFieldSearch: { type: Boolean, default: false },
    treeToList: { type: Boolean, default: false },
    navTreeToList: { type: Boolean, default: false },
    loadTreeDataType:{ type: String as PropType<LoadTreeDataType>, default: LoadTreeDataType.all },
    enableToSelect: { type: Boolean, default: true },
    customData: { type: Object, default: null },
    modelValue: { type: String, default: '' },
    enableUserData: { type: Boolean, default: false },
    onReady: { type: Function, default: null },
    context: { type: Object, default: {} },
    placeholder: { type: String, default: '请选择' }
} as Record<string, any>;

export type LookupProps = ExtractPropTypes<typeof lookupProps>;

export const propsResolver = createPropsResolver<LookupProps>(lookupProps, lookupSchema,schemaMapper, schemaResolver);
