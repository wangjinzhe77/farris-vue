import { MapperFunction, resolveAppearance } from '../../../dynamic-resolver';

function converMappingFieldsToObject(mappingFields: any) {
    if (typeof mappingFields == 'string' && mappingFields.startsWith('{') && mappingFields.endsWith('}')) {
        mappingFields = mappingFields.replace(/'/g, '"');
        return {mappingFields: JSON.parse(mappingFields)};
    }

    return {mappingFields};
}

export const schemaMapper = new Map<string, string | MapperFunction>([
    ['appearance', resolveAppearance],
    ['mappingFields', (key: string, value: any, resolvedSchema) => {
        if (value) {
            return converMappingFieldsToObject(value);
        }

        const {mappingFields} = resolvedSchema;
        return converMappingFieldsToObject(mappingFields);
    }],
    ['uri', (key: string, value: any, resolvedSchema) => {
        if (value) {
            return {uri: value};
        }
        return {uri: resolvedSchema?.dataSource?.uri};
    }]
]);
