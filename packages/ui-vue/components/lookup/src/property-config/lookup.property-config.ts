import { InputBaseProperty } from "../../../property-panel/src/composition/entity/input-base-property";
import { FieldSelectorRepositoryToken } from "../../../field-selector";
import {
    lookupDataSourceConverter, lookupDisplayTypeConverter,
    lookupIdFieldConverter, lookupDefaultConverter,
    lookupVisibleConverter,
    lookupTextFieldConverter
} from "./converters/lookup-property.converter";
import { DesignerComponentInstance } from "../../../designer-canvas/src/types";

export const LookupSchemaRepositoryToken = Symbol('schema_repository_token');

export class LookupPropertyConfig extends InputBaseProperty {

    private comboListEditor = {
        type: 'combo-list',
        enableClear: false,
        editable: false
    };

    getPropertyConfig(propertyData: any, componentInstance: DesignerComponentInstance) {
        const basic = this.getBasicProperties(propertyData, componentInstance);
        const appearance = this.getAppearanceConfig();
        const behavior = this.getBehaviorConfig(propertyData);
        const lookup = this.getLookupConfig(propertyData.editor);

        return {
            type: 'object',
            categories: {
                basic,
                appearance,
                // behavior,
                lookup
            }
        };
    }

    private getBehaviorConfig(propertyData) {
        return {
            description: "Basic Infomation",
            title: "行为",
            properties: {
                editable: {
                    description: "",
                    title: "允许编辑",
                    type: "boolean",
                    $converter: lookupDefaultConverter
                },
                readonly: {
                    description: "",
                    title: "只读",
                    type: "boolean",
                    $converter: lookupDefaultConverter,
                    editor: {
                        enableClear: true,
                        editable: true
                    }
                },
                required: {
                    visible: false,
                    description: "",
                    title: "必填",
                    type: "boolean",
                    $converter: lookupDefaultConverter
                },
                visible: {
                    description: "",
                    title: "可见",
                    type: "boolean",
                    visible:false,
                    $converter: lookupVisibleConverter
                },
                placeholder: {
                    title: "提示文本",
                    type: "string",
                    $converter: lookupDefaultConverter
                },
                "tabindex": {
                    visible: false,
                    description: "",
                    title: "tab索引",
                    type: "number"
                }
            }
        };
    }

    private isRelatedField() {
        const fullPath = this.designViewModelField?.path || '';

        if (fullPath && fullPath.indexOf('.') > -1) {
            const field = fullPath.split('.')[0];
            const fields = this.designViewModelUtils.getAllFields2TreeByVMId(this.viewModelId);
            if (fields && fields.length) {
                const fieldInfo = fields.map(node => node.data).find(data => data.code === field);
                return fieldInfo?.type?.$type === 'EntityType';
            }
        }
        return false;
    }

    private getLookupConfig(propertyData: any) {
        return {
            description: "Basic Infomation",
            title: "编辑器",
            properties: {
                editable: {
                    description: "",
                    title: "允许编辑",
                    type: "boolean",
                    $converter: lookupDefaultConverter,
                    editor: this.comboListEditor
                },
                readonly: {
                    description: "",
                    title: "只读",
                    type: "boolean",
                    $converter: lookupDefaultConverter,
                    editor: {...this.comboListEditor, editable: true}
                },
                dataSource: {
                    description: "数据源",
                    title: "数据源",
                    type: "string",
                    refreshPanelAfterChanged: true,
                    editor: {
                        type: "schema-selector",
                        title: "选择数据源",
                        editorParams: {
                            propertyData,
                            formBasicInfo: this.formSchemaUtils.getFormMetadataBasicInfo()
                        },
                        viewOptions: [
                            {
                                id: 'recommend', title: '推荐', type: 'List',
                                dataSource: 'Recommand',
                                enableGroup: true,
                                groupField: 'category',
                                groupFormatter: (value, data) => {
                                    return `${value === 'local' ? '本地元数据' : '最近使用'}`;
                                }
                            },
                            { id: 'total', title: '全部', type: 'List', dataSource: 'Total' }
                        ],
                        repositoryToken: LookupSchemaRepositoryToken,
                        onSubmitModal: (dataSourceSchema: any) => {
                            if (dataSourceSchema) {
                                const formInfo = this.formSchemaUtils.getFormMetadataBasicInfo();
                                // 获取数据源详细配置信息
                                return this.metadataService.getPickMetadata(formInfo.relativePath, dataSourceSchema[0].data)
                                    .then((res: any) => {
                                        const metadata = JSON.parse(res?.metadata.content);
                                        return metadata;
                                    });
                            }
                        }
                    },
                    $converter: lookupDataSourceConverter
                },
                displayType: {
                    description: "类型： 树列表、列表、双列表、左树右列表",
                    title: "展示类型",
                    type: "string",
                    $converter: lookupDisplayTypeConverter,
                    editor: {
                        type: "combo-list",
                        editable: false,
                        disabled: true,
                        data: [
                            { text: '列表', value: 'LIST' },
                            { text: '树列表', value: 'TREELIST' },
                            { text: '双列表', value: 'NAVLIST' },
                            { text: '左树右列表', value: 'NAVTREELIST' }
                        ],
                        textField: 'text',
                        idField: 'value',
                        valueField: 'value'
                    }
                },
                idField: {
                    description: "数据源标识字段",
                    title: "标识字段",
                    type: "string",
                    editor: {
                        type: "field-selector",
                        textField: 'bindingPath',
                        idField: 'bindingPath',
                        editorParams: {
                            propertyData,
                            formBasicInfo: this.formSchemaUtils.getFormMetadataBasicInfo()
                        },
                        columns: [
                            { field: 'name', title: '名称' },
                            { field: 'code', title: '编号' },
                            { field: 'bindingPath', title: '绑定字段' },
                        ],
                        repositoryToken: FieldSelectorRepositoryToken
                    },
                    $converter: lookupIdFieldConverter
                },
                textField: {
                    description: "显示文本字段",
                    title: "文本字段",
                    type: "string",
                    $converter: lookupTextFieldConverter,
                    editor: {
                        type: "field-selector",
                        textField: 'bindingPath',
                        idField: 'bindingPath',
                        editorParams: {
                            propertyData,
                            formBasicInfo: this.formSchemaUtils.getFormMetadataBasicInfo()
                        },
                        columns: [
                            { field: 'name', title: '名称' },
                            { field: 'code', title: '编号' },
                            { field: 'bindingPath', title: '绑定字段' },
                        ],
                        repositoryToken: FieldSelectorRepositoryToken
                    }
                },
                mappingFields: {
                    description: "字段映射",
                    title: "字段映射",
                    type: "string",
                    $converter: lookupDefaultConverter,
                    editor: {
                        type: "mapping-editor",
                        modalWidth: 800,
                        modalHeight: 600,
                        editable: false,
                        editorParams: {
                            propertyData,
                            formBasicInfo: this.formSchemaUtils.getFormMetadataBasicInfo()
                        },
                        fromData: {
                            editable: false,
                            formatter: (cell, data) => {
                                return `${data.raw['name']} [${data.raw['bindingPath']}]`;
                            },
                            idField: 'id',
                            textField: 'bindingPath',
                            valueField: 'bindingPath',
                            repositoryToken: FieldSelectorRepositoryToken
                        },
                        toData: {
                            idField: 'id',
                            textField: 'bindingPath',
                            valueField: 'bindingPath',
                            dataSource: this.designViewModelUtils.getAllFields2TreeByVMId(this.viewModelId),
                            formatter: (cell, data) => {
                                return `${data.raw['name']} [${data.raw['bindingPath']}]`;
                            }
                        }
                    }
                },
                multiSelect: {
                    description: "启用多选",
                    $converter: lookupDefaultConverter,
                    title: "启用多选",
                    type: "boolean",
                    editor: this.comboListEditor,
                    refreshPanelAfterChanged: true,
                    visible: !this.isRelatedField()
                },
                separator: {
                    description: "多选分隔符",
                    $converter: lookupDefaultConverter,
                    title: "多选分隔符",
                    type: "string",
                    editor: {
                        type: "combo-list",
                        editable: false,
                        disabled: false,
                        data: [
                            { text: ',', value: ',' },
                            { text: '|', value: '|' },
                            { text: '#', value: '#' },
                        ],
                        textField: 'text',
                        idField: 'value',
                        valueField: 'value'
                    },
                    visible: !!propertyData.multiSelect
                }
                // ,enableFavorite: {
                //     description: "启用收藏夹",
                //     $converter: lookupDefaultConverter,
                //     title: "启用收藏夹",
                //     type: "boolean",
                // }
            }
        };
    }

}
