export const lookupDefaultConverter = {
    convertFrom: (schema: Record<string, any>, propertyKey: string) => {
        if (propertyKey === 'multiSelect') {
            return !!schema.editor[propertyKey];
        }

        if (propertyKey === 'separator') {
            return schema.editor[propertyKey] || ',';
        }

        return schema.editor[propertyKey];
    },
    convertTo: (schema: Record<string, any>, propertyKey: string, propertyValue: any) => {
        schema.editor[propertyKey] = propertyValue;
    }
};

export const lookupVisibleConverter = {
    convertFrom: (schema: Record<string, any>, propertyKey: string) => {
        return schema[propertyKey] == null ? true : schema[propertyKey];
    },
    convertTo: (schema: Record<string, any>, propertyKey: string, propertyValue: any) => {
        schema[propertyKey] = propertyValue;
    }
};

function generateSourceUri(ctrlId: string) {
    let code = 'form_group_' + Date.now();
    if (ctrlId) {
        code = ctrlId.replaceAll('-', '_').replaceAll('.', '_');
    }
    return 'lookup.' + code;
}

export const lookupDataSourceConverter = {
    convertFrom: (schema: Record<string, any>, propertyKey: string) => {
        return schema.editor[propertyKey]?.displayName;
    },
    convertTo: (schema: Record<string, any>, propertyKey: string, propertyValue: any[]) => {
        if (propertyValue && propertyValue.length > 0) {
            const datasource = propertyValue[0];
            const {name, id, metadataContent} = datasource;

            if (!schema.editor.dataSource) {
                schema.editor.dataSource = {};
            }

            schema.editor.dataSource.displayName = name;
            schema.editor['helpId'] = id;

            const { displayType, idField, textField } = metadataContent;
            schema.editor.displayType = displayType;
            schema.editor.dataSource.idField = idField;
            schema.editor.textField = textField;
            schema.editor.dataSource.type = "ViewObject";

            // if (lookupDataSourceConverter['entityCode']) {
            //     const code = lookupDataSourceConverter['entityCode'];
            //     const field =  schema.editor.dataSource.bindingField;
            //     schema.editor.dataSource.uri = `${code}.${field}`;
            // }
            if (!schema.editor.dataSource.uri) {
                schema.editor.dataSource.uri = generateSourceUri(schema.id);
            }
        }
    }
};

export const lookupIdFieldConverter = {
    convertFrom: (schema: Record<string, any>, propertyKey: string) => {
        return schema.editor.dataSource?.idField;
    },
    convertTo: (schema: Record<string, any>, propertyKey: string, propertyValue: any) => {
        if (propertyValue && propertyValue.length > 0) {
            const fieldInfo = propertyValue[0];
            schema.editor.dataSource.idField = fieldInfo?.bindingPath;
        }
    }
};

export const lookupTextFieldConverter = {
    convertFrom: (schema: Record<string, any>, propertyKey: string) => {
        return schema.editor?.textField;
    },
    convertTo: (schema: Record<string, any>, propertyKey: string, propertyValue: any) => {
        if (propertyValue && propertyValue.length > 0) {
            const fieldInfo = propertyValue[0];
            schema.editor.textField = fieldInfo?.bindingPath;
        }
    }
};

export const lookupDisplayTypeConverter = {
    convertFrom: (schema: Record<string, any>, propertyKey: string) => {
        return schema.editor.displayType? schema.editor.displayType.toUpperCase() : 'LIST';
    },
    convertTo: (schema: Record<string, any>, propertyKey: string, propertyValue: any) => {
        schema.editor.displayType = propertyValue;
    }
};
