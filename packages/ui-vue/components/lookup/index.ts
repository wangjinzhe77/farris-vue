
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App, Plugin } from 'vue';
import FLookupDesign from './src/designer/lookup.design.component';
import FLookup from './src/lookup.component';
import { propsResolver } from './src/lookup.props';
import { LookupSchemaRepositoryToken } from './src/property-config/lookup.property-config';
import './lookup-style.scss';

export * from './src/lookup.props';
export * from './src/composition/types';

FLookup.install = (app: App) => {
    app.component(FLookup.name as string, FLookup);
};
FLookup.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap.lookup = FLookup;
    propsResolverMap.lookup = propsResolver;
};

FLookup.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.lookup = FLookupDesign;
    propsResolverMap.lookup = propsResolver;
};

export { FLookup, LookupSchemaRepositoryToken };
export default FLookup as typeof FLookup & Plugin;
