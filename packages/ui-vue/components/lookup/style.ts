import "@farris/ui-vue/components/dependent-base/style";
import "@farris/ui-vue/components/dependent-icon/style";
import "@farris/ui-vue/components/button-edit/style";
import "@farris/ui-vue/components/data-grid/style";
import "@farris/ui-vue/components/tree-grid/style";
import "@farris/ui-vue/components/modal/style";
import "@farris/ui-vue/components/pagination/style";
import "@farris/ui-vue/components/loading/style";
