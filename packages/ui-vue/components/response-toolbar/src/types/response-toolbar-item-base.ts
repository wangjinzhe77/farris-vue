import { ComputedRef, computed } from "vue";

export class ResponseToolbarItemBase {
    /** 工具栏项标识 */
    id = '';

    /** 工具栏项自定义样式 */
    class = 'btn-secondary';

    /** 图标 */
    icon = '';

    /** 所属分组 */
    groupId = '';

    /** 收藏顺序 */
    order = -1;

    /** 是否作为下拉菜单的顶层按钮 */
    asDropDownTop = false;

    /** 文本 */
    text = '';

    /** 是否可用 */
    get enable(): ComputedRef<boolean> {
        if (Object.keys(this.options).indexOf('disabled') > -1) {
            return !this.options.disabled as any;
        }
        return computed(() => true);
    };

    /** 是否可见 */
    visible = true;

    responsed = false;

    /** 是否启用提示消息 */
    tipsEnable = false;

    /** 提示消息内容 */
    tipsText = '';

    /** 记录宽度 */
    width = 0;

    onClick: ($event: MouseEvent, itemId: string) => void = () => { };

    options: Record<string, any>;

    [index: string]: any;

    constructor(item: Record<string, any>) {
        this.options = item;
        const propertyKes = [
            'id',
            'class',
            'icon',
            'groupId',
            'order',
            'asDropDownTop',
            'text',
            'isDP',
            'visible',
            'responsed',
            'width',
            'tipsEnable',
            'tipsText',
            'onClick'
        ];
        Object.keys(item)
            .filter((itemKey: string) => propertyKes.indexOf(itemKey) > -1)
            .forEach((itemKey: string) => {
                this[itemKey] = item[itemKey];
            });

        if (item.appearance?.class) {
            this.class = item.appearance?.class;
        }
    }

    /** 设置宽度 */
    setWidth(value: string) {
        this.width = parseInt(value, 10);
    }

    /** 获取宽度 */
    getWidth(): any {
        if (this.visible) {
            return this.width;
        }
        return false;
    }
}
