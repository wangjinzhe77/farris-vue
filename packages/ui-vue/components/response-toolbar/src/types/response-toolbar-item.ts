import { ResponseToolbarItemBase } from './response-toolbar-item-base';

export class ResponseToolbarItem extends ResponseToolbarItemBase {

    constructor(item: any) {
        super(item);
    }
}
