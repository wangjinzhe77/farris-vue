import { getCurrentInstance } from 'vue';
import { ResponseToolbarDropDownItem } from '../../types/response-toolbar-dropdwon-item';
import { UseIcon } from '../../composition/types';

export default function (context, useIconComposition: UseIcon) {
    function dropdownMenuClass(item: ResponseToolbarDropDownItem) {
        const classObject = {
            'dropdown-menu': true
        } as Record<string, boolean>;
        if (item.class) {
            const classNames = item.menuClass.split(' ');
            if (classNames && classNames.length) {
                classNames.reduce((result: Record<string, boolean>, className: string) => {
                    result[className] = true;
                    return result;
                }, classObject);
            }
        }
        return classObject;
    }

    function dropdownSubMenuItemClass(item: ResponseToolbarDropDownItem) {
        const classObject = {
            disabled: !item.enable,
            'dropdown-submenu': true,
            'f-rt-dropdown': true
        } as Record<string, boolean>;
        const classNames = item.dropdownClass.split(' ');
        if (classNames && classNames.length) {
            classNames.reduce((result: Record<string, boolean>, className: string) => {
                result[className] = true;
                return result;
            }, classObject);
        }
        return classObject;
    }

    function dropdownMenuItemClass(item: ResponseToolbarDropDownItem) {
        const classObject = {
            disabled: !item.enable,
            'dropdown-item': true,
            'f-rt-btn': true,
        } as Record<string, boolean>;
        const classNames = item.class.split(' ');
        if (classNames && classNames.length) {
            classNames.reduce((result: Record<string, boolean>, className: string) => {
                result[className] = true;
                return result;
            }, classObject);
        }
        return classObject;
    }

    // eslint-disable-next-line prefer-const
    let renderDropdownMenu: (item: ResponseToolbarDropDownItem, parent?: ResponseToolbarDropDownItem) => any;
    const expandedDropDownItems = new Map<string, ResponseToolbarDropDownItem>();

    function toggleDropdownSubMenu($event: MouseEvent, item: ResponseToolbarDropDownItem, parent?: ResponseToolbarDropDownItem) {
        if (($event.target as any)?.id === item.id) {
            $event.stopPropagation();
        }
        const parentKey = parent ? parent.id : '__top_item__';
        if (item.children && item.children.length) {
            item.expanded = !item.expanded;
            item.expanded ? expandedDropDownItems.set(parentKey, item) : expandedDropDownItems.delete(parentKey);
        }
        if (expandedDropDownItems.has(parentKey) && expandedDropDownItems.get(parentKey) !== item) {
            const dropDwonItemToCollapse = expandedDropDownItems.get(parentKey);
            if (dropDwonItemToCollapse) {
                dropDwonItemToCollapse.expanded = false;
            }
        }
    }

    function onItemClick(event: MouseEvent, menuItem: ResponseToolbarDropDownItem) {
        document.body.click(); // 隐藏展开的下拉菜单
        if(!menuItem.enable) {
            return;
        }
        if (typeof menuItem.onClick === 'function') {
            menuItem.onClick(event, menuItem.id);
        }
        context.emit('click', event, menuItem.id);
    }

    function renderDropdownMenuItem(item: ResponseToolbarDropDownItem) {
        return (
            item.children
                // .filter((item) => item.visible)
                .map((menuItem) => {
                    if (menuItem.children && menuItem.children.length) {
                        return (
                            <li
                                class={dropdownSubMenuItemClass(menuItem as ResponseToolbarDropDownItem)}
                                id={menuItem.id}
                                onClick={(payload: MouseEvent) =>
                                    menuItem.enable && toggleDropdownSubMenu(payload, menuItem as ResponseToolbarDropDownItem, item)
                                }>
                                <span
                                    id={menuItem.id}
                                    class={dropdownMenuItemClass(menuItem as ResponseToolbarDropDownItem)}
                                    onMouseover={(payload: MouseEvent) =>
                                        menuItem.enable && toggleDropdownSubMenu(payload, menuItem as ResponseToolbarDropDownItem, item)
                                    }>
                                    {menuItem.text}
                                    <i
                                        class="f-icon f-icon-arrow-chevron-right"
                                        style="display: inline-block;float: right;line-height: 1.25rem;"></i>
                                </span>
                                {renderDropdownMenu(menuItem as ResponseToolbarDropDownItem, menuItem as ResponseToolbarDropDownItem)}
                            </li>
                        );
                    }
                    return (
                        <li
                            class={dropdownMenuItemClass(menuItem as ResponseToolbarDropDownItem)}
                            id={menuItem.id}
                            onClick={(payload: MouseEvent) => onItemClick(payload, menuItem as ResponseToolbarDropDownItem)}
                            onMouseover={(payload: MouseEvent) =>
                                menuItem.enable && toggleDropdownSubMenu(payload, menuItem as ResponseToolbarDropDownItem, item)
                            }>
                            {useIconComposition.shouldShowIcon(menuItem) && <i class={useIconComposition.iconClass(menuItem)}></i>}
                            {menuItem.text}
                        </li>
                    );
                })
        );
    }

    function dropdownStyle(item: ResponseToolbarDropDownItem, buttonElement: Element|null, parent?: ResponseToolbarDropDownItem,) {
        const styleObject: Record<string, any> = {
            display: item.expanded ? 'block' : 'none',
            position: 'fixed',
            maxWidth: '300px',
            width: 'auto',
            minWidth: '120px'
        };
        const screenWidth = document.getElementsByTagName('body')[0].getClientRects()[0].width;
        const currentDropdownElement = buttonElement;
        const currentDropdownElementClientRect = currentDropdownElement?.getClientRects();
        if (currentDropdownElement && currentDropdownElementClientRect && currentDropdownElementClientRect.length) {
            const {top, width, left, right, height} = currentDropdownElementClientRect[0];
            const dropdownPanelTop = Math.ceil(height + top);

            styleObject.top = `${dropdownPanelTop}px`;
            styleObject.left = `${left}px`;

            const menuPanelID = item.id + '_menu';
            const menuPanelDom: HTMLElement | null = buttonElement? buttonElement.querySelector('#' +menuPanelID) : null;
            if (menuPanelDom) {
                if (styleObject.display === 'block') {
                    menuPanelDom.style.display = 'block';
                }
                const menuPanelDomRect = menuPanelDom.getBoundingClientRect();

                if (parent) {
                    styleObject.top = `${top - 6}px`;

                    const childMenuItemLeft = Math.ceil(width + left);
                    styleObject.left = `${childMenuItemLeft}px`;
                }

                if (screenWidth -left - width < menuPanelDomRect.width) {
                    styleObject.left = `${(parent ? left : right) - menuPanelDomRect.width}px`;
                }
            }
        }

        return styleObject;
    }

    renderDropdownMenu = function (item: ResponseToolbarDropDownItem, parent?: ResponseToolbarDropDownItem) {
        const instance = getCurrentInstance();
        const itemId = parent ? parent.id : item.id;
        const buttonElement = instance?.exposed?.elementRef.value?.querySelector('#' + itemId);
        return (
            <ul class={dropdownMenuClass(item)} style={dropdownStyle(item, buttonElement, parent)} id={item.id + '_menu'}>
                {renderDropdownMenuItem(item)}
            </ul>
        );
    };

    function clearAllDropDownMenu() {
        expandedDropDownItems.forEach((dropdownItem: ResponseToolbarDropDownItem) => {
            dropdownItem.expanded = false;
        });
        expandedDropDownItems.clear();
    }

    return { renderDropdownMenu, clearAllDropDownMenu };
}
