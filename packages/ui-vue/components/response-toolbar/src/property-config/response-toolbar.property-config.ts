import { BaseControlProperty } from "../../../property-panel/src/composition/entity/base-property";

export class ResponseToolbarProperty extends BaseControlProperty {

    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }

    getPropertyConfig(propertyData: any) {
        // 基本信息
        this.propertyConfig.categories['basic'] = this.getBasicPropConfig(propertyData);
        // 样式
        this.getAppearancePropConfig(propertyData);
        // 行为
        // this.getBehaviorProperties(propertyData);
        return this.propertyConfig;
    }
    getAppearancePropConfig(propertyData): any {
        this.propertyConfig.categories['appearance'] = {
            title: "外观",
            description: "Appearance",
            properties: {
                class: {
                    title: "class样式",
                    type: "string",
                    description: "组件的CSS样式",
                    $converter: "/converter/appearance.converter"
                }
            }
        };
    }

    private getBehaviorProperties(propertyData: any) {
        this.propertyConfig.categories['behavior'] = {
            "title": "行为",
            "description": "",
            "properties": {
                "alignment": {
                    "title": "对齐方式",
                    "description": "在工具栏区域内按钮组的位置",
                    "type": "enum",
                    "editor": {
                        "type": "combo-list",
                        "textField": "name",
                        "valueField": "value",
                        "data": [                            
                            {
                                "value": "left",
                                "name": "左对齐"
                            },
                            {
                                "value": "center",
                                "name": "居中"
                            },
                            {
                                "value": "right",
                                "name": "右对齐"
                            }
                        ]
                    }
                }
            }
        };
    }
};
