import { BaseControlProperty } from "../../../property-panel/src/composition/entity/base-property";

export class ResponseToolbarItemProperty extends BaseControlProperty {

    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }

    getPropertyConfig(propertyData: any) {
        // 基本信息
        this.propertyConfig.categories['basic'] = this.getBasicPropConfig(propertyData);
        // 外观
        this.getAppearanceProperties(propertyData);
        // 行为
        this.getBehaviorConfig();
        // 事件
        this.getEventPropConfig(propertyData);
        return this.propertyConfig;
    }

    getBehaviorConfig() {
        this.propertyConfig.categories['behavior'] = {
            title: "行为",
            description: "Behavior",
            properties: {
                disabled: {
                    title: "禁用",
                    type: "boolean",
                    description: "按钮禁用状态",
                    refreshPanelAfterChanged: true,
                    editor: {
                        enableClear: true,
                        editable: true
                    }
                }
            }
        };
    }
    private getAppearanceProperties(propertyData: any) {
        this.propertyConfig.categories['appearance'] = {
            "title": "外观",
            "description": "Appearance",
            "properties": {
                "class": {
                    "title": "class样式",
                    "type": "string",
                    "description": "",
                    "$converter": "/converter/appearance.converter"
                },
                "text": {
                    "title": "文本",
                    "type": "string",
                    "description": "",
                    "refreshPanelAfterChanged": true
                },
                "icon": {
                    "title": "图标",
                    "type": "string",
                    "description": ""
                }
            }
        };
    }

    private getEventPropConfig(propertyData: any) {
        const events = [
            {
                "label": "onClick",
                "name": "点击事件"
            }
        ];
        const self = this;
        const initialData = self.eventsEditorUtils['formProperties'](propertyData, self.viewModelId, events);
        const properties = {};
        properties[self.viewModelId] = {
            type: 'events-editor',
            editor: {
                initialData
            }
        };
        this.propertyConfig.categories['eventsEditor'] = {
            title: '事件',
            hideTitle: true,
            properties,
            // 这个属性，标记当属性变更得时候触发重新更新属性
            refreshPanelAfterChanged: true,
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject: any, data: any) {
                const parameters = changeObject.propertyValue;
                delete propertyData[self.viewModelId];
                if (parameters) {
                    parameters.setPropertyRelates = this.setPropertyRelates; // 添加自定义方法后，调用此回调方法，用于处理联动属性
                    self.eventsEditorUtils.saveRelatedParameters(propertyData, self.viewModelId, parameters['events'], parameters);
                }
            }
        };
    }
};
