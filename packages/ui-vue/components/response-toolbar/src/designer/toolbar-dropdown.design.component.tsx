 
import { Ref, inject, ref } from 'vue';
import { UseIcon } from '../composition/types';
import { ResponseToolbarProps } from '../response-toolbar.props';
import { ResponseToolbarDropDownItem } from '../types/response-toolbar-dropdwon-item';
import getDropdownMenu from './toolbar-dropdown-menu.design.component';
import { ComponentSchema, DesignerComponentInstance, DesignerItemContext } from '../../../designer-canvas/src/types';
import { getSchemaByType } from '../../../dynamic-resolver/src/schema-resolver';
import { DraggingResolveContext } from '../../../designer-canvas/src/composition/types';

export default function (props: ResponseToolbarProps, useIconComposition: UseIcon, componentInstance: Ref<DesignerComponentInstance>) {
    const alignment = ref(props.alignment);
    const { renderDropdownMenu, clearAllDropDownMenu } = getDropdownMenu(useIconComposition, componentInstance);
    const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
    const updateToolbarItemsHandler = inject('toolbar-item-handler', () => { }) as () => void;

    function updateToolbarItems() {
        if (updateToolbarItemsHandler) {
            updateToolbarItemsHandler();
        }
    };
    function dropdownClass(item: ResponseToolbarDropDownItem) {
        const classObject = {
            'btn-group': true,
            'f-rt-dropdown': true,
            'f-btn-ml': alignment.value === 'right',
            'f-btn-mr': alignment.value === 'left',
        } as Record<string, boolean>;
        const classNames = item.dropdownClass.split(' ');
        if (classNames && classNames.length) {
            classNames.reduce((result: Record<string, boolean>, className: string) => {
                result[className] = true;
                return result;
            }, classObject);
        }
        return classObject;
    }

    function dropdownButtonClass(item: ResponseToolbarDropDownItem) {
        const classObject = {
            btn: true,
            disabled: !item.enable,
            'position-relative': true,
            'farris-component': true,
            'f-rt-btn': true,
            'btn-icontext': !!(item.icon && item.icon.trim())
        } as Record<string, boolean>;
        if (item.class) {
            const classNames = item.class.split(' ');
            if (classNames && classNames.length) {
                classNames.reduce((result: Record<string, boolean>, className: string) => {
                    result[className] = true;
                    return result;
                }, classObject);
            }
        }
        return classObject;
    }
    function preventEvent(payload: MouseEvent) {
        if (!payload) {
            return;
        }
        payload.stopPropagation();
        payload.preventDefault();
    }
    function toggleDropdownMenu(payload: MouseEvent, item: ResponseToolbarDropDownItem) {
        preventEvent(payload);
        const currentItemStatus = item.expanded;
        document.body.click();
        item.expanded = !currentItemStatus;
    }
    const createComponentSchema = function (resolveContext: DraggingResolveContext) {
        const { componentType } = resolveContext;
        const componentSchema = getSchemaByType(componentType, resolveContext) as ComponentSchema;
        const typePrefix = componentType.toLowerCase().replace('-', '_');
        if (componentSchema && !componentSchema.id && componentSchema.type === componentType) {
            componentSchema.id = `${typePrefix}_${Math.random().toString().slice(2, 6)}`;
        }
        return componentSchema;
    };
    const createResponseToolbarItemSchema = function () {
        const resolveContext = {
            componentType: 'response-toolbar-item',
            parentComponentInstance: componentInstance.value,
            targetPosition: -1
        } as DraggingResolveContext;
        return createComponentSchema(resolveContext);
    };
    function getToolbarItemById(toolbarItemId: string): { index: number; toolbarItem: ComponentSchema } {
        const buttons = designItemContext.schema.buttons || designItemContext.schema.contents;
        const index = buttons?.findIndex((schema: ComponentSchema) => schema.id === toolbarItemId);
        const toolbarItem = index === -1 ? null : buttons[index];
        return { index, toolbarItem };
    }
    function onCopyButtonClick(payload: MouseEvent, toolbarItemId: string) {
        const { index, toolbarItem } = getToolbarItemById(toolbarItemId);
        if (index === -1) {
            return;
        }
        const toolbarItemTitle = toolbarItem.text || '按钮';
        const toolbarItemTemplateSchema = createResponseToolbarItemSchema();
        const toolbarItemSchema = Object.assign({}, toolbarItemTemplateSchema, { text: toolbarItemTitle });
        designItemContext.schema.buttons?.push(toolbarItemSchema);
        updateToolbarItems();
    }
    function onAppendChildButtonClick(payload: MouseEvent, toolbarItemId: string) {
        const { index, toolbarItem } = getToolbarItemById(toolbarItemId);
        if (index === -1) {
            return;
        }
        const toolbarItemTitle = toolbarItem.text || '按钮';
        const toolbarItemTemplateSchema = createResponseToolbarItemSchema();
        const toolbarItemSchema = Object.assign({}, toolbarItemTemplateSchema, { text: toolbarItemTitle });
        const items = designItemContext.schema.buttons[index].children || [];
        items.push(toolbarItemSchema);
        designItemContext.schema.buttons[index].children = items;
        updateToolbarItems();
    }
    function onRemoveButtonClick(payload: MouseEvent, toolbarItemId: string) {
        const buttons = designItemContext.schema.buttons || designItemContext.schema.contents;
        const index = buttons?.findIndex((schema: ComponentSchema) => schema.id === toolbarItemId);
        if (index === -1) {
            return;
        }
        buttons.splice(index, 1);
        updateToolbarItems();
    }
    function renderIconPanel(toolbarItemId: string) {
        return (
            <div class="component-btn-group">
                <div>
                    <div role="button" class="btn component-settings-button" title="删除" ref="removeButton" style="position:static;" onClick={(payload) => onRemoveButtonClick(payload, toolbarItemId)}>
                        <i class="f-icon f-icon-yxs_delete"></i>
                    </div>
                    <div role="button" class="btn component-settings-button" title="复制" ref="copyButton" style="position:static;" onClick={(payload) => onCopyButtonClick(payload, toolbarItemId)}>
                        <i class="f-icon f-icon-yxs_copy"></i>
                    </div>
                    <div role="button" class="btn component-settings-button" title="新增子级" ref="appendChildButton" style="width:85px!important;padding:0 5px;position:static;" onClick={(payload) => onAppendChildButtonClick(payload, toolbarItemId)}>
                        <i class="f-icon f-icon-plus-circle text-white mr-1"></i>
                        <span style="font-size:13px;margin:auto">新增子级</span>
                    </div>
                </div>
            </div>
        );
    }
    function deactiveElements() {
        Array.from(document.getElementsByClassName('dgComponentFocused') as HTMLCollectionOf<HTMLElement>).forEach(
            (element: HTMLElement) => element.classList.remove('dgComponentFocused')
        );
        const currentSelectedElements = document.getElementsByClassName('dgComponentSelected') as HTMLCollectionOf<HTMLElement>;
        Array.from(currentSelectedElements).forEach((element: HTMLElement) => element.classList.remove('dgComponentSelected'));
    }
    function activeEventElement(payload: MouseEvent) {
        const currentTarget = payload.currentTarget as HTMLElement;
        // currentTarget.parentElement.par
        currentTarget.classList.add('dgComponentFocused', 'dgComponentSelected');
    }
    function emitSelectionChange(toolbarItemId: string) {
        const buttons = designItemContext.schema.buttons || designItemContext.schema.contents;
        const index = buttons?.findIndex((schema: ComponentSchema) => schema.id === toolbarItemId);
        if (index === -1) {
            return;
        }
        const toolbarItem = buttons[index];
        designItemContext?.setupContext?.emit('selectionChange', 'response-toolbar-item', toolbarItem);
    }
    function onToolbarItemClick(payload: MouseEvent, item: ResponseToolbarDropDownItem) {
        preventEvent(payload);
        deactiveElements();
        activeEventElement(payload);
        preventEvent(payload);
        toggleDropdownMenu(payload, item);
        emitSelectionChange(item.id);
    }
    function renderToolbarDropdown(item: ResponseToolbarDropDownItem) {
        return (
            <div id={item.id} class={dropdownClass(item)}>
                <div
                    class={dropdownButtonClass(item)}
                    style="display: flex;padding-right: 0.1rem;"
                    onClick={(payload: MouseEvent) => item.enable && onToolbarItemClick(payload, item)}>
                    {renderIconPanel(item.id)}
                    {useIconComposition.shouldShowIcon(item) && <i class={useIconComposition.iconClass(item)}></i>}
                    <span>{item.text}</span>
                    <i
                        class="f-icon f-icon-arrow-chevron-down"
                        style="display: inline-block;float: right;line-height: 1.25rem;margin-left: .25rem;margin-right: .25rem;"></i>
                </div>
                {renderDropdownMenu(item)}
            </div>
        );
    }

    function clearAllDropDown() {
        clearAllDropDownMenu();
    }

    return { renderToolbarDropdown, clearAllDropDown };
}
