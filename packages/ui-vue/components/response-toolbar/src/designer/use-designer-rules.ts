import { getSchemaByType } from "../../../dynamic-resolver";
import { DesignerHostService, DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema, DesignerItemContext } from "../../../designer-canvas/src/types";
import { ResponseToolbarProperty } from "../property-config/response-toolbar.property-config";

export function useDesignerRules(designItemContext: DesignerItemContext, designerHostService?: DesignerHostService): UseDesignerRules {
    const schema = designItemContext.schema as ComponentSchema;

    /**
      * 判断是否可以接收拖拽新增的子级控件
      */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {
        return false;
    }

    function onResolveNewComponentSchema(resolveContext: DraggingResolveContext, componentSchema: ComponentSchema): ComponentSchema {
        componentSchema.text = resolveContext.label;
        return componentSchema;
    }

    function checkCanMoveComponent() {
        return false;
    }
    function checkCanDeleteComponent() {
        return false;
    }

    function checkCanAddComponent() {
        return true;
    }

    function hideNestedPaddingInDesginerView() {
        return true;
    }

    // 构造属性配置方法
    function getPropsConfig(componentId: string) {
        const responseToolbarProp = new ResponseToolbarProperty(componentId, designerHostService);

        return responseToolbarProp.getPropertyConfig(schema);
    }
    /**
     * 新增单个按钮
     */
    function addToolbarItem(payload: MouseEvent) {
        if (payload) {
            payload.stopPropagation();
            payload.preventDefault();
        }
        const { schema } = designItemContext;
        if (!schema.buttons) {
            schema.buttons = [];
        }
        const toolbarItemSchema = getSchemaByType('response-toolbar-item') as ComponentSchema;
        toolbarItemSchema.id = `toolbar_item_${Math.random().toString().slice(2, 6)}`;
        toolbarItemSchema.appearance = { class: 'btn btn-secondary' };

        schema.buttons.push(toolbarItemSchema);
    }
    /**
     * 获取自定义操作按钮
     */
    function getCustomButtons() {
        return [
            {
                id: 'appendItem',
                title: '新增按钮',
                icon: 'f-icon f-icon-plus-circle text-white',
                onClick: (e) => addToolbarItem(e)
            }
        ];
    }
    return {
        canAccepts,
        checkCanAddComponent,
        checkCanDeleteComponent,
        checkCanMoveComponent,
        hideNestedPaddingInDesginerView,
        onResolveNewComponentSchema,
        getPropsConfig,
        getCustomButtons
    };
}
