 
import { Ref, getCurrentInstance, inject } from 'vue';
import { ResponseToolbarDropDownItem } from '../types/response-toolbar-dropdwon-item';
import { UseIcon } from '../composition/types';
import { ComponentSchema, DesignerComponentInstance, DesignerItemContext } from '../../../../components/designer-canvas/src/types';
import { ResponseToolbarItemBase } from '../types/response-toolbar-item-base';
import { getSchemaByType } from '../../../dynamic-resolver/src/schema-resolver';
import { DraggingResolveContext } from '../../../designer-canvas/src/composition/types';

export default function (useIconComposition: UseIcon, componentInstance: Ref<DesignerComponentInstance>) {
    const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
    const updateToolbarItemsHandler = inject('toolbar-item-handler', () => { }) as () => void;

    function updateToolbarItems() {
        if (updateToolbarItemsHandler) {
            updateToolbarItemsHandler();
        }
    };

    function dropdownMenuClass(item: ResponseToolbarDropDownItem) {
        const classObject = {
            'dropdown-menu': true
        } as Record<string, boolean>;
        if (item.class) {
            const classNames = item.menuClass.split(' ');
            if (classNames && classNames.length) {
                classNames.reduce((result: Record<string, boolean>, className: string) => {
                    result[className] = true;
                    return result;
                }, classObject);
            }
        }
        return classObject;
    }

    function dropdownSubMenuItemClass(item: ResponseToolbarDropDownItem) {
        const classObject = {
            disabled: !item.enable,
            'dropdown-submenu': true,
            'f-rt-dropdown': true
        } as Record<string, boolean>;
        const classNames = item.dropdownClass.split(' ');
        if (classNames && classNames.length) {
            classNames.reduce((result: Record<string, boolean>, className: string) => {
                result[className] = true;
                return result;
            }, classObject);
        }
        return classObject;
    }

    function dropdownMenuItemClass(item: ResponseToolbarDropDownItem) {
        const classObject = {
            disabled: !item.enable,
            'dropdown-item': true,
            'f-rt-btn': true,
            'position-relative': true,
            'farris-component': true
        } as Record<string, boolean>;
        const classNames = item.class.split(' ');
        if (classNames && classNames.length) {
            classNames.reduce((result: Record<string, boolean>, className: string) => {
                result[className] = true;
                return result;
            }, classObject);
        }
        return classObject;
    }

    // eslint-disable-next-line prefer-const
    let renderDropdownMenu: (item: ResponseToolbarDropDownItem, parent?: ResponseToolbarDropDownItem) => any;
    const expandedDropDownItems = new Map<string, ResponseToolbarDropDownItem>();

    function toggleDropdownSubMenu($event: MouseEvent, item: ResponseToolbarDropDownItem, parent?: ResponseToolbarDropDownItem) {
        if (($event.target as any)?.id === item.id) {
            $event.stopPropagation();
        }
        const parentKey = parent ? parent.id : '__top_item__';
        if (item.children && item.children.length) {
            item.expanded = !item.expanded;
            item.expanded ? expandedDropDownItems.set(parentKey, item) : expandedDropDownItems.delete(parentKey);
        }
        if (expandedDropDownItems.has(parentKey) && expandedDropDownItems.get(parentKey) !== item) {
            const dropDwonItemToCollapse = expandedDropDownItems.get(parentKey);
            if (dropDwonItemToCollapse) {
                dropDwonItemToCollapse.expanded = false;
            }
        }
    }
    function preventEvent(payload: MouseEvent) {
        if (!payload) {
            return;
        }
        payload.stopPropagation();
        payload.preventDefault();
    }
    function deactiveElements() {
        Array.from(document.getElementsByClassName('dgComponentFocused') as HTMLCollectionOf<HTMLElement>).forEach(
            (element: HTMLElement) => element.classList.remove('dgComponentFocused')
        );
        const currentSelectedElements = document.getElementsByClassName('dgComponentSelected') as HTMLCollectionOf<HTMLElement>;
        Array.from(currentSelectedElements).forEach((element: HTMLElement) => element.classList.remove('dgComponentSelected'));
    }
    function activeEventElement(payload: MouseEvent) {
        const currentTarget = payload.currentTarget as HTMLElement;
        currentTarget.classList.add('dgComponentFocused', 'dgComponentSelected');
    }
    function getMenuItemById(itemId: string) {
        const { buttons = [] } = designItemContext.schema;
        let items: ComponentSchema[] = [];
        buttons.forEach((button: ComponentSchema) => {
            items = items.concat(button.children || []);
        });
        const index = items.findIndex((schema: ComponentSchema) => schema.id === itemId);
        const menuItem = index === -1 ? null : items[index];
        return { index, item: menuItem };
    }
    function getToolbarItemById(itemId: string) {
        const { buttons = [] } = designItemContext.schema;
        const index = buttons.findIndex((button: ComponentSchema) => button.id === itemId);
        const toolbarItem = index === -1 ? null : buttons[index];
        return { index, item: toolbarItem };
    }
    function emitSelectionChange(toolbarItemId: string) {
        const { buttons = [] } = designItemContext.schema;
        const { index, item } = getMenuItemById(toolbarItemId);
        designItemContext?.setupContext?.emit('selectionChange', item?.type, item);
    }
    const createComponentSchema = function (resolveContext: DraggingResolveContext) {
        const { componentType } = resolveContext;
        const componentSchema = getSchemaByType(componentType, resolveContext) as ComponentSchema;
        const typePrefix = componentType.toLowerCase().replace('-', '_');
        if (componentSchema && !componentSchema.id && componentSchema.type === componentType) {
            componentSchema.id = `${typePrefix}_${Math.random().toString().slice(2, 6)}`;
        }
        return componentSchema;
    };
    const createResponseToolbarItemSchema = function () {
        const resolveContext = {
            componentType: 'response-toolbar-item',
            parentComponentInstance: componentInstance.value,
            targetPosition: -1
        } as DraggingResolveContext;
        return createComponentSchema(resolveContext);
    };
    function onItemClick(payload: MouseEvent, menuItem: ResponseToolbarDropDownItem) {
        // document.body.click(); // 隐藏展开的下拉菜单
        // menuItem.enable && menuItem.onClick(event, menuItem.id);
        preventEvent(payload);
        deactiveElements();
        activeEventElement(payload);
        emitSelectionChange(menuItem.id);
    }
    function onRemoveButtonClick(payload: MouseEvent, item: ResponseToolbarItemBase, parent: ResponseToolbarDropDownItem) {
        preventEvent(payload);
        const menuItemId = item.id;
        const parentId = parent && parent.id;
        const { buttons = [] } = designItemContext.schema;
        if (!parentId) {
            return;
        }
        const { index: parentIndex } = getToolbarItemById(parentId);
        if (parentIndex === -1) {
            return;
        }
        const { index: menuItemIndex } = getMenuItemById(menuItemId);
        if (menuItemIndex === -1) {
            return;
        }
        buttons[parentIndex].children.splice(menuItemIndex, 1);
        updateToolbarItems();
    }
    function onCopyButtonClick(payload: MouseEvent, item: ResponseToolbarItemBase, parent: ResponseToolbarDropDownItem) {
        preventEvent(payload);
        const parentId = parent && parent.id;
        const { buttons = [] } = designItemContext.schema;
        if (!parentId) {
            return;
        }
        const { index: parentIndex } = getToolbarItemById(parentId);
        if (parentIndex === -1) {
            return;
        }
        const menuItem = createResponseToolbarItemSchema();
        const schema = Object.assign({}, menuItem, { text: item.text });
        buttons[parentIndex].children.push(schema);
        updateToolbarItems();
    }
    function onAppendSiblingButtonClick(payload: MouseEvent, item: ResponseToolbarItemBase, parent: ResponseToolbarDropDownItem) {
        preventEvent(payload);
        const parentId = parent && parent.id;
        const { buttons = [] } = designItemContext.schema;
        if (!parentId) {
            return;
        }
        const { index: parentIndex } = getToolbarItemById(parentId);
        if (parentIndex === -1) {
            return;
        }
        const menuItem = createResponseToolbarItemSchema();
        const schema = Object.assign({}, menuItem, { text: '按钮' });
        buttons[parentIndex].children.push(schema);
        updateToolbarItems();
    }
    function renderIconPanel(item: ResponseToolbarItemBase, parent: ResponseToolbarDropDownItem) {
        return (
            <div class="component-btn-group">
                <div>
                    <div role="button" class="btn component-settings-button" title="删除" ref="removeButton" style="position:static;" onClick={(payload) => onRemoveButtonClick(payload, item, parent)}>
                        <i class="f-icon f-icon-yxs_delete"></i>
                    </div>
                    <div role="button" class="btn component-settings-button" title="复制" ref="copyButton" style="position:static;" onClick={(payload) => onCopyButtonClick(payload, item, parent)}>
                        <i class="f-icon f-icon-yxs_copy"></i>
                    </div>
                    <div role="button" class="btn component-settings-button" title="新增同级" ref="appendSame" style="width:85px!important;padding:0 5px;position:static;" onClick={(payload) => onAppendSiblingButtonClick(payload, item, parent)}>
                        <i class="f-icon f-icon-plus-circle text-white mr-1"></i>
                        <span style="font-size:13px;margin:auto">新增同级</span>
                    </div>
                </div>

            </div>
        );
    }
    function renderDropdownMenuItem(item: ResponseToolbarDropDownItem) {
        return (
            item.children
                // .filter((item) => item.visible)
                .map((menuItem) => {
                    if (menuItem.children && menuItem.children.length) {
                        return (
                            <li
                                class={dropdownSubMenuItemClass(menuItem as ResponseToolbarDropDownItem)}
                                id={menuItem.id}
                                onClick={(payload: MouseEvent) =>
                                    menuItem.enable && toggleDropdownSubMenu(payload, menuItem as ResponseToolbarDropDownItem, item)
                                }>
                                <span
                                    id={menuItem.id}
                                    class={dropdownMenuItemClass(menuItem as ResponseToolbarDropDownItem)}
                                    onMouseover={(payload: MouseEvent) =>
                                        menuItem.enable && toggleDropdownSubMenu(payload, menuItem as ResponseToolbarDropDownItem, item)
                                    }>
                                    {menuItem.text}
                                    <i
                                        class="f-icon f-icon-arrow-chevron-right"
                                        style="display: inline-block;float: right;line-height: 1.25rem;"></i>
                                </span>
                                {renderDropdownMenu(menuItem as ResponseToolbarDropDownItem, menuItem as ResponseToolbarDropDownItem)}
                            </li>
                        );
                    }
                    return (
                        <li
                            class={dropdownMenuItemClass(menuItem as ResponseToolbarDropDownItem)}
                            id={menuItem.id}
                            onClick={(payload: MouseEvent) => onItemClick(payload, menuItem as ResponseToolbarDropDownItem)}
                        >
                            {renderIconPanel(menuItem, item)}
                            {useIconComposition.shouldShowIcon(menuItem) && <i class={useIconComposition.iconClass(menuItem)}></i>}
                            {menuItem.text}
                        </li>
                    );
                })
        );
    }

    function dropdownStyle(item: ResponseToolbarDropDownItem, buttonElement: Element | null, parent?: ResponseToolbarDropDownItem,) {
        const styleObject: Record<string, any> = {
            display: item.expanded ? 'block' : 'none',
            position: 'fixed',
            maxWidth: '300px',
            width: 'auto',
            minWidth: '120px'
        };
        const screenWidth = document.getElementsByTagName('body')[0].getClientRects()[0].width;
        const currentDropdownElement = buttonElement;
        const currentDropdownElementClientRect = currentDropdownElement?.getClientRects();
        if (currentDropdownElement && currentDropdownElementClientRect && currentDropdownElementClientRect.length) {
            const { top, width, left, right, height } = currentDropdownElementClientRect[0];
            const dropdownPanelTop = Math.ceil(height + top);

            styleObject.top = `${dropdownPanelTop}px`;
            styleObject.left = `${left}px`;

            const menuPanelID = item.id + '_menu';
            const menuPanelDom: HTMLElement | null = buttonElement ? buttonElement.querySelector('#' + menuPanelID) : null;
            if (menuPanelDom) {
                if (styleObject.display === 'block') {
                    menuPanelDom.style.display = 'block';
                }
                const menuPanelDomRect = menuPanelDom.getBoundingClientRect();

                if (parent) {
                    styleObject.top = `${top - 6}px`;

                    const childMenuItemLeft = Math.ceil(width + left);
                    styleObject.left = `${childMenuItemLeft}px`;
                }

                if (screenWidth - left - width < menuPanelDomRect.width) {
                    styleObject.left = `${(parent ? left : right) - menuPanelDomRect.width}px`;
                }
            }
        }

        return styleObject;
    }

    renderDropdownMenu = function (item: ResponseToolbarDropDownItem, parent?: ResponseToolbarDropDownItem) {
        const instance = getCurrentInstance();
        const itemId = parent ? parent.id : item.id;
        const buttonElement = instance?.exposed?.elementRef.value?.querySelector('#' + itemId);
        return (
            <ul class={dropdownMenuClass(item)} style={dropdownStyle(item, buttonElement, parent)} id={item.id + '_menu'}>
                {renderDropdownMenuItem(item)}
            </ul>
        );
    };

    function clearAllDropDownMenu() {
        expandedDropDownItems.forEach((dropdownItem: ResponseToolbarDropDownItem) => {
            dropdownItem.expanded = false;
        });
        expandedDropDownItems.clear();
    }

    return { renderDropdownMenu, clearAllDropDownMenu };
}
