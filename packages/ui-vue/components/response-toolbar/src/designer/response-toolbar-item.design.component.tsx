
import { computed, defineComponent, ref, SetupContext, onMounted, onUnmounted, watch, inject } from 'vue';
import { ResponseToolbarItemDesignProps, responseToolbarItemDesignProps } from '../response-toolbar.props';
import { ResponseToolbarDropDownItem } from '../types/response-toolbar-dropdwon-item';
import { useIcon } from '../composition/use-icon';
import { useToolbarItem } from '../composition/use-toolbar-item';
import { ComponentSchema, DesignerItemContext } from '../../../designer-canvas/src/types';
import { getSchemaByType } from '../../../dynamic-resolver/src/schema-resolver';
import { useDesignerItemRules } from './use-designer-item-rules';
import { useDesignerInnerComponent } from '@farris/ui-vue/components/designer-canvas';
import { getCustomClass } from '@farris/ui-vue/components/common';
import { DraggingResolveContext } from '../../../designer-canvas/src/composition/types';

const { buildResponseToolbarItems } = useToolbarItem();
export default defineComponent({
    name: 'FResponseToolbarItemDesign',
    props: responseToolbarItemDesignProps,
    emits: ['Click'],
    setup(props: ResponseToolbarItemDesignProps, context) {
        const resizedContainer = ref<any>();
        const resizedContent = ref<any>();
        const alignment = ref(props.alignment);
        const defaultDropdownOptions = { id: '__more_buttons__', text: 'More' };
        const defaultDropdown = ref<any>(new ResponseToolbarDropDownItem(defaultDropdownOptions));
        const useIconComposition = useIcon();

        const shouldShowDefaultDropdown = computed(() => defaultDropdown.value.children.length > 0);
        const elementRef = ref();
        const designerHostService = inject('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerItemRules(designItemContext, designerHostService);
        const componentInstance = useDesignerInnerComponent(elementRef, designItemContext, designerRulesComposition);
        const updateToolbarItemsHandler = inject('toolbar-item-handler', () => { }) as () => void;

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);
        function updateToolbarItems() {
            if (updateToolbarItemsHandler) {
                updateToolbarItemsHandler();
            }
        };


        function iconClass() {
            const classObject = {
                'f-icon': true
            } as Record<string, boolean>;
            const iconClass = props.icon ? props.icon.trim() : '';
            return getCustomClass(classObject, iconClass);
        }

        function shouldShowIcon() {
            return !!(props.icon && props.icon.trim());
        }

        function buttonClass() {
            const classObject = {
                btn: true,
                'f-rt-btn': true,
                'f-btn-ml': alignment.value === 'right' || alignment.value === 'center',
                'f-btn-mr': alignment.value === 'left',
                'btn-icontext': !!(props.icon && props.icon.trim()),
                'disabled': props.disabled === true,
                'no-drag': true
            } as Record<string, boolean>;
            return getCustomClass(classObject, props.class);
        }
        function preventEvent(payload: MouseEvent) {
            if (!payload) {
                return;
            }
            payload.stopPropagation();
            payload.preventDefault();
        }
        function deactiveElements() {
            Array.from(document.getElementsByClassName('dgComponentFocused') as HTMLCollectionOf<HTMLElement>).forEach(
                (element: HTMLElement) => element.classList.remove('dgComponentFocused')
            );
            const currentSelectedElements = document.getElementsByClassName('dgComponentSelected') as HTMLCollectionOf<HTMLElement>;
            Array.from(currentSelectedElements).forEach((element: HTMLElement) => element.classList.remove('dgComponentSelected'));
        }
        function activeEventElement(payload: MouseEvent) {
            const currentTarget = payload.currentTarget as HTMLElement;
            currentTarget.classList.add('dgComponentFocused', 'dgComponentSelected');
        }
        function emitSelectionChange(toolbarItemId: string) {
            const buttons = designItemContext.schema.buttons || designItemContext.schema.contents;
            const index = buttons?.findIndex((schema: ComponentSchema) => schema.id === toolbarItemId);
            if (index === -1) {
                return;
            }
            const toolbarItem = buttons[index];
            designItemContext?.setupContext?.emit('selectionChange', 'response-toolbar-item', toolbarItem, props.componentId, componentInstance.value);
        }
        const createComponentSchema = function (resolveContext: DraggingResolveContext) {
            const { componentType } = resolveContext;
            const componentSchema = getSchemaByType(componentType, resolveContext) as ComponentSchema;
            const typePrefix = componentType.toLowerCase().replace('-', '_');
            if (componentSchema && !componentSchema.id && componentSchema.type === componentType) {
                componentSchema.id = `${typePrefix}_${Math.random().toString().slice(2, 6)}`;
            }
            return componentSchema;
        };
        const createResponseToolbarItemSchema = function () {
            const resolveContext = {
                componentType: 'response-toolbar-item',
                parentComponentInstance: componentInstance.value,
                targetPosition: -1
            } as DraggingResolveContext;
            return createComponentSchema(resolveContext);
        };
        function onResponseToolbarItemClick(payload: MouseEvent, toolbarItemId: string) {
            deactiveElements();
            activeEventElement(payload);
            preventEvent(payload);
            emitSelectionChange(toolbarItemId);
        }
        function onRemoveButtonClick(payload: MouseEvent, toolbarItemId: string) {
            const buttons = designItemContext.schema.buttons || designItemContext.schema.contents;
            const index = buttons?.findIndex((schema: ComponentSchema) => schema.id === toolbarItemId);
            if (index === -1) {
                return;
            }
            buttons.splice(index, 1);
            updateToolbarItems();
        }
        function getToolbarItemById(toolbarItemId: string): { index: number; toolbarItem: ComponentSchema } {
            const buttons = designItemContext.schema.buttons || designItemContext.schema.contents;
            const index = buttons?.findIndex((schema: ComponentSchema) => schema.id === toolbarItemId);
            const toolbarItem = index === -1 ? null : buttons[index];
            return { index, toolbarItem };
        }
        function onCopyButtonClick(payload: MouseEvent, toolbarItemId: string) {
            const { index, toolbarItem } = getToolbarItemById(toolbarItemId);
            if (index === -1) {
                return;
            }
            const toolbarItemTitle = toolbarItem.text || '按钮';
            const toolbarItemTemplateSchema = createResponseToolbarItemSchema();
            const toolbarItemSchema = Object.assign({}, toolbarItemTemplateSchema, { text: toolbarItemTitle });
            designItemContext.schema.buttons?.push(toolbarItemSchema);
            updateToolbarItems();
        }
        function onAppendChildButtonClick(payload: MouseEvent, toolbarItemId: string) {
            const { index, toolbarItem } = getToolbarItemById(toolbarItemId);
            if (index === -1) {
                return;
            }
            const toolbarItemTitle = toolbarItem.text || '按钮';
            const toolbarItemTemplateSchema = createResponseToolbarItemSchema();
            const toolbarItemSchema = Object.assign({}, toolbarItemTemplateSchema, { text: toolbarItemTitle });
            const items = designItemContext.schema.buttons[index].items || [];
            items.push(toolbarItemSchema);
            designItemContext.schema.buttons[index].children = items;
            updateToolbarItems();
        }
        function renderIconPanel(toolbarItemId: string) {
            return (
                <div class="component-btn-group">
                    <div>
                        <div role="button" class="btn component-settings-button" title="删除" ref="removeButton" style="position:static;" onClick={(payload) => onRemoveButtonClick(payload, toolbarItemId)}>
                            <i class="f-icon f-icon-yxs_delete"></i>
                        </div>
                        <div role="button" class="btn component-settings-button" title="复制" ref="copyButton" style="position:static;" onClick={(payload) => onCopyButtonClick(payload, toolbarItemId)}>
                            <i class="f-icon f-icon-yxs_copy"></i>
                        </div>
                        <div role="button" class="btn component-settings-button" title="新增子级" ref="appendChildButton" style="width:85px!important;padding:0 5px;position:static;" onClick={(payload) => onAppendChildButtonClick(payload, toolbarItemId)}>
                            <i class="f-icon f-icon-plus-circle text-white mr-1"></i>
                            <span style="font-size:13px;margin:auto">新增子级</span>
                        </div>
                    </div>
                </div>
            );
        }
        return () => {
            return <div
                ref={elementRef}
                data-dragref={`${designItemContext.schema.id}-container`}
                class="farris-component position-relative" style="display:inline-block!important;">
                {/* {renderIconPanel(props.id)} */}
                <button type="button" class={buttonClass()} id={props.id}>
                    {shouldShowIcon() && <i class={iconClass()}></i>}
                    {props.text}
                </button>
            </div>;
        };
    }
});
