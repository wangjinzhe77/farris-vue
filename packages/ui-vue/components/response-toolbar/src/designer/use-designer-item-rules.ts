import { nextTick } from "vue";
import { DesignerHTMLElement, DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { DesignerItemContext } from "../../../designer-canvas/src/types";
import { TabToolbarItemProperty } from "../../../tabs/src/property-config/tab-toolbar-item.property-config";
import { ResponseToolbarItemProperty } from "../property-config/response-toolbar-item.property-config";

export function useDesignerItemRules(designerItemContext: DesignerItemContext, designerHostService): UseDesignerRules {
    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {
        return false;
    }

    function checkCanMoveComponent() {
        return false;
    }
    function checkCanDeleteComponent() {
        return true;
    }
    function checkCanAddComponent() {
        return false;
    }
    /**
     * 构造属性配置方法
     */
    function getPropsConfig(componentId: string) {
        let responseToolbarItemProp;
        switch (designerItemContext.schema.type) {
            case 'tab-toolbar-item':
                responseToolbarItemProp = new TabToolbarItemProperty(componentId, designerHostService);
                break;
            default:
                responseToolbarItemProp = new ResponseToolbarItemProperty(componentId, designerHostService);
        }
        return responseToolbarItemProp.getPropertyConfig(designerItemContext.schema);
    }
    /**
     * 修改按钮文本属性后，重新计算操作按钮位置
     */
    function updatePositionOfButtonGroup() {
        if (designerItemContext.designerItemElementRef.value) {
            nextTick(() => {
                designerItemContext.designerItemElementRef.value.click();
            });
        }
    }
    function onPropertyChanged(event: any) {
        if (!event) {
            return;
        }
        const { changeObject } = event;
        if (changeObject && changeObject.propertyID === 'text') {
            updatePositionOfButtonGroup();
        }

    }
    return {
        canAccepts,
        checkCanMoveComponent,
        checkCanDeleteComponent,
        checkCanAddComponent,
        getPropsConfig,
        onPropertyChanged
    };
}
