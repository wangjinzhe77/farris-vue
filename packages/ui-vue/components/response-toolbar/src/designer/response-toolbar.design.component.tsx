import { computed, defineComponent, ref, onMounted, onUnmounted, watch, inject } from 'vue';
import { useDesignerComponent } from '@farris/ui-vue/components/designer-canvas';
import { ResponseToolbarDesignProps, responseToolbarDesignProps } from '../response-toolbar.props';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerRules } from './use-designer-rules';
import FDesignerInnerItem from '../../../designer-canvas/src/components/designer-inner-item.component';
import FResponseToolbarDesignItemComponent from './response-toolbar-item.design.component';
import { getCustomClass } from '@farris/ui-vue/components/common';
import { DesignerHostService } from '../../../designer-canvas/src/composition/types';
import { responseToolbarItemDragula } from '../composition/toolbar-item-dragula';

export default defineComponent({
    name: 'FResponseToolbarDesign',
    props: responseToolbarDesignProps,
    emits: ['Click'],
    setup(props: ResponseToolbarDesignProps, context) {
        const toolbarItems = ref<any[]>(props.items);
        let toolbarItemRef = new Array(toolbarItems.value.length).fill(ref());
        const resizedContainer = ref<any>();
        const resizedContent = ref<any>();
        const alignment = ref(props.alignment);
        const elementRef = ref();
        const designerHostService = inject<DesignerHostService>('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);
        const updateToolbarItemsHandler = inject('toolbar-item-handler', () => { }) as () => void;
        const { initDragula, dragulaInstance } = responseToolbarItemDragula(designItemContext);

        function updateToolbarItems() {
            if (updateToolbarItemsHandler) {
                updateToolbarItemsHandler();
            }
        };

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;

            initDragula(resizedContent.value, resizedContainer.value, updateToolbarItems);
        });

        onUnmounted(() => {
            if (dragulaInstance && dragulaInstance.destroy) {
                dragulaInstance.destroy();
            }
        });

        context.expose(componentInstance.value);

        const responseToolbarClass = computed(() => {
            const classObject = {
                'f-toolbar': true,
                'f-response-toolbar': true,
                'position-relative': true,
                'px-2': true
            } as Record<string, boolean>;
            return getCustomClass(classObject, props.customClass);
        });

        const resizedContainerClass = computed(() => {
            const classObject = {
                'd-flex': true,
                'flex-nowrap': true,
                'justify-content-end': alignment.value === 'right',
                'justify-content-start': alignment.value === 'left',
                'justify-content-center': alignment.value === 'center',
                'no-drag': true
            } as Record<string, boolean>;
            return classObject;
        });

        watch(
            () => props.items,
            () => {
                toolbarItems.value = props.items;
            },
            {
                deep: true
            }
        );

        watch(toolbarItems, (newValue) => {
            toolbarItemRef = new Array(toolbarItems.value.length).fill(ref());
        });

        function onSelectionChange(schemaType: string, schemaValue: any, componentId: string, componentInstance: any) {
            designItemContext && designItemContext.setupContext &&
                designItemContext.setupContext.emit('selectionChange', schemaType, schemaValue, componentId, componentInstance);
        }

        // 删除子组件后  暂时不选中其他子组件
        function onRemoveComponent() {
            const item = resizedContent.value.querySelector('.dgComponentFocused.dgComponentSelected');
            item?.classList.remove('dgComponentSelected');
            item?.classList.remove('dgComponentFocused');

            updateToolbarItems();
        }

        return () => {
            return (
                <div
                    ref={elementRef}
                    data-dragref={`${designItemContext.schema.id}-container`}
                    class={responseToolbarClass.value}>
                    <div ref={resizedContainer} class={resizedContainerClass.value}>
                        <div ref={resizedContent} class="d-inline-block f-response-content">
                            {toolbarItems.value
                                .map((item, index: number) => {
                                    return <FDesignerInnerItem
                                        ref={toolbarItemRef[index]}
                                        key={item.id}
                                        class="p-0 display-inline-block"
                                        v-model={item}
                                        canMove={true}
                                        childType="response-toolbar-item"
                                        childLabel="按钮"
                                        contentKey="buttons"
                                        componentId={props.componentId}
                                        id={item.id}
                                        style="padding:0 !important;display:inline-block"
                                        onSelectionChange={onSelectionChange}
                                        onRemoveComponent={onRemoveComponent}
                                    >
                                        <FResponseToolbarDesignItemComponent
                                            id={item.id}
                                            disabled={item.disabled}
                                            text={item.text}
                                            icon={item.icon}
                                            class={item.appearance?.class || 'btn-secondary'}
                                            alignment={props.alignment || 'right'}
                                            componentId={props.componentId}
                                        >
                                        </FResponseToolbarDesignItemComponent>
                                    </FDesignerInnerItem>;
                                })}
                        </div>
                    </div>
                </div>
            );
        };
    }
});
