import { DesignerHTMLElement } from '../../../designer-canvas/src/composition/types';
import dragula from '@farris/designer-dragula';
import { DesignerItemContext } from '../../../designer-canvas';
import { nextTick } from 'vue';

export function responseToolbarItemDragula(designItemContext: DesignerItemContext) {

    let dragulaInstance: any;

    function onDrop(element: DesignerHTMLElement, target: DesignerHTMLElement, sibling: DesignerHTMLElement, updateToolbarItems: any) {
        const toolbarSchema = designItemContext.schema;
        if (!toolbarSchema?.buttons) {
            return;
        }
        let targetIndex;

        if (sibling) {
            const targetButtonElementId = sibling.getAttribute('id');
            targetIndex = Array.from(target.children).findIndex(e => e.getAttribute('id') === targetButtonElementId);
        } else {
            targetIndex = Array.from(target.children).length;
        }
        const sourceButtonElementId = element.getAttribute('id');
        const sourceIndex = Array.from(target.children).findIndex(e => e.getAttribute('id') === sourceButtonElementId);
        const sourceButtonSchema = toolbarSchema.buttons[sourceIndex];

        if (sourceIndex !== targetIndex && sourceIndex > -1 && targetIndex > -1) {
            if (sourceIndex < targetIndex) {
                targetIndex = targetIndex - 1;
            }

            // 更新工具栏控件schema结构
            toolbarSchema.buttons.splice(sourceIndex, 1);
            toolbarSchema.buttons.splice(targetIndex, 0, sourceButtonSchema);

            // 触发更新内嵌类工具栏的父控件schema结构(例如tabPage、Section)
            updateToolbarItems();

            // 重新计算操作按钮位置
            nextTick(() => {
                const selectedElement = target.querySelector('.dgComponentSelected') as DesignerHTMLElement;
                if (selectedElement?.className.includes('dgComponentSelected')) {
                    selectedElement.classList.remove('dgComponentSelected');
                    selectedElement.classList.remove('dgComponentFocused');
                    selectedElement.click();
                }
            });
        }
    }

    /**
     * 初始化按钮拖拽
     */
    function initDragula(resizedContent: DesignerHTMLElement, resizedContainer: DesignerHTMLElement, updateToolbarItems: any) {
        if (dragulaInstance) {
            dragulaInstance.destroy();
        }
        if (!dragula || !resizedContent || !resizedContainer) {
            return;
        }

        dragulaInstance = dragula([resizedContent], {
            mirrorContainer: resizedContainer,
            direction: 'horizontal',
            getMirrorText(element: DesignerHTMLElement): string {
                return element.innerText;
            }
        })
            .on('drop', (element: DesignerHTMLElement, target: DesignerHTMLElement, source: DesignerHTMLElement, sibling: DesignerHTMLElement
            ) => onDrop(element, target, sibling, updateToolbarItems));

    }

    return {
        dragulaInstance,
        initDragula
    };
}
