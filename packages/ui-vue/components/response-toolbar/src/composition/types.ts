import { Ref } from 'vue';
import { ResponseToolbarDropDownItem } from '../types/response-toolbar-dropdwon-item';
import { ResponseToolbarItem } from '../types/response-toolbar-item';

export interface UseIcon {
    iconClass: (item: ResponseToolbarItem | ResponseToolbarDropDownItem) => Record<string, boolean>;

    shouldShowIcon: (item: ResponseToolbarItem | ResponseToolbarDropDownItem) => boolean;
}

export interface UseToolbarItem {
    buildResponseToolbarItems: (items: any[]) => (ResponseToolbarItem | ResponseToolbarDropDownItem)[];
}
