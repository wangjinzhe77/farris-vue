import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver, createResponseToolbarEventHandlerResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import responseToolbarSchema from './schema/response-toolbar.schema.json';
import responseToolbarItemSchema from './schema/response-toolbar-item.schema.json';
// import responseToolbarItemPropertyConfig from './property-config/response-toolbar-item.property-config.json';
import { ResponseToolbarItem } from './types/response-toolbar-item';

export type ButtonAlignment = 'right' | 'left' | 'center';

export const responseToolbarProps = {
    /**  组件自定义样式 */
    customClass: { type: String, default: '' },

    alignment: { Type: String as PropType<ButtonAlignment>, default: 'right' },
    items: {
        Type: Array<ResponseToolbarItem>, default: []
    }
} as Record<string, any>;

// schemaMap[responseToolbarItemSchema.title] = responseToolbarItemSchema;
// propertyConfigSchemaMap[responseToolbarItemPropertyConfig.title] = responseToolbarItemPropertyConfig;

export type ResponseToolbarProps = ExtractPropTypes<typeof responseToolbarProps>;
export const responseToolbarDesignProps=Object.assign({},responseToolbarProps,{
    componentId: { type: String, default: '' }
});
export type ResponseToolbarDesignProps = ExtractPropTypes<typeof responseToolbarDesignProps>;

export const propsResolver = createPropsResolver<ResponseToolbarProps>(
    responseToolbarProps, responseToolbarSchema, schemaMapper, schemaResolver
);

export const responseToolbarItemDesignProps = {
    id: {type: String, default: ''},
    items: {type: Object as PropType<ResponseToolbarItem>, default: {}},
    class:{type: String, default:''},
    text: {type: String, default:''},
    // disabled: {type: Boolean, default: false},
    icon: {type: String, default:''},
    componentId: { type: String, default: '' },
    alignment: { Type: String as PropType<ButtonAlignment>, default: 'right' }
} as Record<string, any>;

export type ResponseToolbarItemDesignProps = ExtractPropTypes<typeof responseToolbarItemDesignProps>;
export const itemPropsResolver = createPropsResolver<ResponseToolbarItemDesignProps>(
    responseToolbarItemDesignProps, responseToolbarItemSchema, undefined, schemaResolver
);

export const eventHandlerResolver = createResponseToolbarEventHandlerResolver();
