import { computed, defineComponent, ref, onMounted, onUnmounted, watch } from 'vue';
import { ResponseToolbarProps, responseToolbarProps } from './response-toolbar.props';
import { ResponseToolbarDropDownItem } from './types/response-toolbar-dropdwon-item';
import { ResponseToolbarItem } from './types/response-toolbar-item';
import { useIcon } from './composition/use-icon';
import { useToolbarItem } from './composition/use-toolbar-item';
import getDropdown from './components/dropdown/toolbar-dropdown.component';
import { getCustomClass } from '@farris/ui-vue/components/common';

const { buildResponseToolbarItems } = useToolbarItem();
export default defineComponent({
    name: 'FResponseToolbar',
    props: responseToolbarProps,
    emits: ['click'],
    setup(props: ResponseToolbarProps, context) {
        const toolbarItems = ref<any[]>(buildResponseToolbarItems(props.items));
        const responseToolBarContainer = ref<any>();
        const resizedContainer = ref<any>();
        const resizedContent = ref<any>();
        const alignment = ref(props.alignment);
        const defaultDropdownOptions = { id: '__more_buttons__', text: '更多' };
        const defaultDropdown = ref<any>(new ResponseToolbarDropDownItem(defaultDropdownOptions));
        const useIconComposition = useIcon();

        const shouldShowDefaultDropdown = computed(() => defaultDropdown.value.children.length > 0);

        const responseToolbarClass = computed(() => {
            const classObject = {
                'f-toolbar': true,
                'f-response-toolbar': true,
                'position-relative': true,
            } as Record<string, boolean>;
            return getCustomClass(classObject, props.customClass);
        });

        const resizedContainerClass = computed(() => {
            const classObject = {
                'w-100': true,
                'd-flex': true,
                'flex-nowrap': true,
                'justify-content-end': alignment.value === 'right',
                'justify-content-start': alignment.value === 'left',
                'justify-content-center': alignment.value === 'center'
            } as Record<string, boolean>;
            return classObject;
        });

        const { renderToolbarDropdown, clearAllDropDown } = getDropdown(props, context, useIconComposition);

        function collapseAllDropdownMenu() {
            toolbarItems.value
                .filter((item) => item.children && item.children.length > 0)
                .forEach((item) => {
                    item.expanded = false;
                });
            defaultDropdown.value.expanded = false;
            clearAllDropDown();
        }

        function buttonClass(item: ResponseToolbarItem | ResponseToolbarDropDownItem, index) {
            const classObject = {
                btn: true,
                'f-rt-btn': true,
                'f-btn-ml': alignment.value === 'right' || index > 0 && alignment.value === 'center',
                'f-btn-mr': alignment.value === 'left',
                'btn-icontext': !!(item.icon && item.icon.trim())
            } as Record<string, boolean>;
            const itemClass = item?.appearance?.class || item.class;
            if (itemClass) {
                const classNames = itemClass.split(' ');
                if (classNames && classNames.length) {
                    classNames.reduce((result: Record<string, boolean>, className: string) => {
                        result[className] = true;
                        return result;
                    }, classObject);
                }
            }
            return classObject;
        }

        function onButtonItemClick($event: MouseEvent, item: ResponseToolbarItem) {
            document.body.click(); // 隐藏展开的下拉菜单
            if (typeof item.onClick === 'function') {
                item.onClick($event, item.id);
            }
            context.emit('click', $event, item.id);
        }

        function renderToolbarButton(item: ResponseToolbarItem, index: number) {
            return (
                <button
                    type="button"
                    class={buttonClass(item, index)}
                    id={item.id}
                    disabled={!item.enable}
                    onClick={(payload: MouseEvent) => onButtonItemClick(payload, item)}>
                    {useIconComposition.shouldShowIcon(item) && <i class={useIconComposition.iconClass(item)}></i>}
                    {item.text}
                </button>
            );
        }
        const itemsToHide = new Map<string, boolean>();
        const hiddenItems: { id: string; width: number; }[] = [];

        function toResizeToolbarItems(containerWidth: number) {
            const contentElement = resizedContent.value as HTMLElement;
            let availableSpace = containerWidth;
            const allElements = Array.from(contentElement.children);
            const moreElement = allElements[allElements.length - 1].id === '__more_buttons__' ? allElements[allElements.length - 1] : null;
            if (moreElement) {
                const marginLeftOfMoreElement = (moreElement as any).computedStyleMap().get('margin-left') as any;
                const marginRightOfMoreElement = (moreElement as any).computedStyleMap().get('margin-right') as any;
                const moreElementWidth =
                    (marginLeftOfMoreElement ? marginLeftOfMoreElement.value : 0) +
                    (moreElement as HTMLElement).getBoundingClientRect().width +
                    (marginRightOfMoreElement ? marginRightOfMoreElement.value : 0);
                availableSpace -= moreElementWidth;
            }
            const toolbarItemElements = allElements.filter((element: Element) => element.id !== '__more_buttons__');

            for (const toolbarItem of toolbarItemElements) {
                const marginLeft = (toolbarItem as any).computedStyleMap().get('margin-left') as any;
                const marginRight = (toolbarItem as any).computedStyleMap().get('margin-right') as any;
                const itemWidth =
                    (marginLeft ? marginLeft.value : 0) +
                    (toolbarItem as HTMLElement).getBoundingClientRect().width +
                    (marginRight ? marginRight.value : 0);

                if (availableSpace < itemWidth) {
                    itemsToHide.set((toolbarItem as HTMLElement).id, true);
                    hiddenItems.push({ id: toolbarItem.id, width: itemWidth });
                } else {
                    availableSpace -= itemWidth;
                }
            }
            if (hiddenItems.length) {
                for (let index = hiddenItems.length - 1; index >= 0; index--) {
                    const latestHiddenItemWidth = hiddenItems[index].width;
                    if (availableSpace >= latestHiddenItemWidth) {
                        availableSpace -= latestHiddenItemWidth;
                        itemsToHide.delete(hiddenItems[index].id);
                        hiddenItems.pop();
                    } else {
                        break;
                    }
                }
            }
            const currentDropdownOptions = Object.assign({}, defaultDropdown.value);
            currentDropdownOptions.children = [];
            const dropdownItem = new ResponseToolbarDropDownItem(currentDropdownOptions);
            const availableItems = toolbarItems.value.reduce((items: any, toolbarItem: any) => {
                if (itemsToHide.has(toolbarItem.id)) {
                    dropdownItem.children.push(toolbarItem);
                }
                toolbarItem.visible = !itemsToHide.has(toolbarItem.id);
                items.push(toolbarItem);
                return items;
            }, []);
            defaultDropdown.value = dropdownItem;
            toolbarItems.value = availableItems;
        }
        function resetToolbarItemsForWidthChanged(containerWidth: number = -1) {
            if (containerWidth < 0) {
                containerWidth = resizedContainer.value?.getBoundingClientRect().width || -1;
            }
            const contentElement = resizedContent.value;
            const contentWidth = contentElement.getBoundingClientRect().width;
            if (containerWidth >= 0 && containerWidth < contentWidth || hiddenItems.length) {
                toResizeToolbarItems(containerWidth);
                collapseAllDropdownMenu();
            }
        }
        const observer = new ResizeObserver((entries: ResizeObserverEntry[]) => {
            if (entries.length) {
                const responseContainerEntry = entries[0];
                const containerWidth = responseContainerEntry.contentRect.width;
                resetToolbarItemsForWidthChanged(containerWidth);
            }
        });

        onMounted(() => {
            const element = resizedContainer.value;
            observer.observe(element);
            document.body.addEventListener('click', collapseAllDropdownMenu);
            document.body.addEventListener('wheel', collapseAllDropdownMenu);
        });

        onUnmounted(() => {
            observer.disconnect();
            document.body.removeEventListener('click', collapseAllDropdownMenu);
            document.body.removeEventListener('wheel', collapseAllDropdownMenu);
        });

        watch(
            () => props.items,
            () => {
                toolbarItems.value = buildResponseToolbarItems(props.items);
                // 解决工具栏项数据变更，之前的下拉等数据消失问题
                resetToolbarItemsForWidthChanged();
            },
            {
                deep: true
            }
        );

        context.expose({
            elementRef: responseToolBarContainer
        });

        return () => {
            return (
                <div class={responseToolbarClass.value} ref={responseToolBarContainer}>
                    <div ref={resizedContainer} class={resizedContainerClass.value}>
                        <div ref={resizedContent} class="d-inline-block f-response-content" style="white-space: nowrap;">
                            {toolbarItems.value
                                .filter((item) => item.visible)
                                .map((item, index) => {
                                    return item.children && item.children.length > 0
                                        ? renderToolbarDropdown(item as ResponseToolbarDropDownItem)
                                        : renderToolbarButton(item as ResponseToolbarItem, index);
                                })}
                            {shouldShowDefaultDropdown.value && renderToolbarDropdown(defaultDropdown.value)}
                        </div>
                    </div>
                </div>
            );
        };
    }
});
