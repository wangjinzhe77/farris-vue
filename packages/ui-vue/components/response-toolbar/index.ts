import type { App } from 'vue';
import { ResponseToolbarDropDownItem } from './src/types/response-toolbar-dropdwon-item';
import { ResponseToolbarGroup } from './src/types/response-toolbar-group';
import { ResponseToolbarItem } from './src/types/response-toolbar-item';
import { eventHandlerResolver, itemPropsResolver, propsResolver } from './src/response-toolbar.props';
import FResponseToolbar from './src/response-toolbar.component';
import FResponseToolbarDesign from './src/designer/response-toolbar.design.component';
import FResponseToolbarItemDesign from './src/designer/response-toolbar-item.design.component';
import { withInstall } from '@farris/ui-vue/components/common';

const responseToolbarResolver = propsResolver;

FResponseToolbar.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap['response-toolbar'] = FResponseToolbar;
    propsResolverMap['response-toolbar'] = propsResolver;
    resolverMap['response-toolbar'] = { eventHandlerResolver };
};
FResponseToolbar.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['response-toolbar'] = FResponseToolbarDesign;
    propsResolverMap['response-toolbar'] = propsResolver;
    componentMap['response-toolbar-item'] = FResponseToolbarItemDesign;
    propsResolverMap['response-toolbar-item'] = itemPropsResolver;
};
export { FResponseToolbar, ResponseToolbarDropDownItem, ResponseToolbarGroup, ResponseToolbarItem, responseToolbarResolver };
export default withInstall(FResponseToolbar);
