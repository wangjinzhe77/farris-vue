import { Ref, ref, watch } from 'vue';
import {
    DataViewOptions, HeaderCell, HeaderCellStatus, UseColumn,
    UseDataView, UseFilter, UseVirtualScroll
} from '../../composition/types';
import { Condition, FieldConfig } from '../../../condition/src/types';
import { FFilterBar } from '@farris/ui-vue/components/filter-bar';

export default function (
    props: DataViewOptions,
    useColumnComposition: UseColumn,
    useDataViewComposition: UseDataView,
    useFilterComposition: UseFilter,
    useVirtualScrollComposition: UseVirtualScroll
) {
    const { columnContext } = useColumnComposition;
    const { conditions, clearCondition, removeCondition } = useFilterComposition;
    const filterFields: Ref<FieldConfig[]> = ref([]);

    function updateFilters() {
        const filterValues: FieldConfig[] = [];
        Array.from(conditions.value).forEach((codition: Condition, index: number) => {
            const filterField = {
                id: codition.id,
                code: codition.fieldCode,
                labelCode: codition.fieldCode,
                name: codition.fieldName,
                editor: { type: codition.value?.editorType }
            } as FieldConfig;
            filterValues.push(filterField);
        });
        filterFields.value = filterValues;
    }

    watch(conditions, () => updateFilters());

    function removeFilterOnColumn(headerCell: HeaderCell | undefined) {
        if (headerCell && headerCell.column) {
            headerCell.column.filter = null;
            let cellStatus = headerCell.status;
            cellStatus =
                (cellStatus & HeaderCellStatus.filtered) === HeaderCellStatus.filtered
                    ? cellStatus ^ HeaderCellStatus.filtered
                    : cellStatus;
            headerCell.status = cellStatus;
        }
    }

    function onRemoveFilter(removedFilterFieldCode: string) {
        removeCondition(`field_filter_${removedFilterFieldCode}`);
        const targetHeaderCell = columnContext.value.primaryHeaderColumns.find((headerCell: HeaderCell) => {
            return headerCell.field === removedFilterFieldCode;
        });
        removeFilterOnColumn(targetHeaderCell);
        useDataViewComposition.refresh();
        useVirtualScrollComposition.reCalculateVisualDataRows();
    }

    function onResetFilter() {
        clearCondition();
        columnContext.value.primaryHeaderColumns
            .forEach((headerCell: HeaderCell) => removeFilterOnColumn(headerCell));
        useDataViewComposition.refresh();
        useVirtualScrollComposition.reCalculateVisualDataRows();
    }

    function renderFilterPanel() {
        return (
            <div class="fv-grid-filter-panel">
                <FFilterBar
                    data={conditions.value}
                    fields={filterFields.value}
                    mode="display-only"
                    show-reset={true}
                    onRemove={onRemoveFilter}
                    onReset={onResetFilter}></FFilterBar>
            </div>
        );
    }

    return { renderFilterPanel };
}
