import { ref } from "vue";
import { HeaderCell } from "../../composition/types";

export default function (headerCell: HeaderCell) {
    const filterBoxPlaceholder = ref('请输入关键词');
    const searchIconContent = ref('<i class="f-icon f-icon-search"></i>');
    const data = [
        { name: '发票类型', checked: false },
        { name: '发票代码', checked: false },
        { name: '开票日期', checked: false },
        { name: '票价(燃油附加费)', checked: false },
        { name: '税收分类编号', checked: false },
        { name: '不含税单价', checked: false },
        { name: '往来单位', checked: false },
        { name: '商品服务描述', checked: false },
        { name: '项目负责人', checked: false },
        { name: '所属单位', checked: false }
    ];
    return (
        <f-list-view data={data} multi-select={true} view="SingleView" size="Small"> </f-list-view>
    );
}
