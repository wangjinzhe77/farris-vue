import { Checkbox } from "@farris/ui-vue/components/checkbox";
import { HeaderCell } from "../../composition/types";
import { ref } from "vue";

const booleanData: any[] = [
    { name: '是', value: true },
    { name: '否', value: false }
];
export default function (headerCell: HeaderCell) {
    const filterValue = ref(headerCell.column?.filter || false);

    headerCell.filter = headerCell.filter || function (dataItem: any) {
        const filterValues = String(headerCell.column?.filter || '').split(',');
        return filterValues.includes(String(dataItem[headerCell.field]));
    };

    const separator = ',';
    const selection = {
        multiSelect: true,
        multiSelectMode: 'OnCheckAndClick',
        showCheckbox: true,
    };
    /**
     * 值到数组值的转换
     */
    function toSelectionValues(value: any): boolean[] {
        return value?.split(separator)
            .filter((selectionValue: string) => selectionValue !== '')
            .map((selectionValue: string) => selectionValue.toLowerCase() === 'true') || [];
    }

    /**
     * 值到字符串值的转换
     */
    function toSelectionString(selectionResult: any[]) {
        const selectionValues = selectionResult.map((selectionItem: any) => selectionItem.value)
            .join(separator);
        return selectionValues;
    }

    function onConfirmFilterItem(selectionResult: any[]) {
        if (headerCell.column) {
            headerCell.column.filter = toSelectionString(selectionResult);
        }
    }

    return <f-list-view data={booleanData} multi-select={true}
        id-field="value" value-field="value" view="SingleView" size="Small" selection={selection}
        selection-values={toSelectionValues(headerCell.column?.filter || '')}
        onSelectionChange={(result: any) => onConfirmFilterItem(result)}> </f-list-view>;
}
