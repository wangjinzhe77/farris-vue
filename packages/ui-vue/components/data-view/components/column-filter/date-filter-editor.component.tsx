import { ref } from "vue";
import { HeaderCell } from "../../composition/types";

const tags = ref([
    {
        name: '七天', selectable: true
    },
    {
        name: '一个月', selectable: true
    },
    {
        name: '三个月', selectable: true
    },
    {
        name: '半年', selectable: true
    }
]);
export default function (headerCell: HeaderCell) {
    const status = ref(false);
    const filterValue = ref(headerCell.column?.filter || '');

    headerCell.filter = headerCell.filter || function (dataItem: any) {
        const filterDateValue = new Date(new Date(headerCell.column?.filter).toLocaleDateString()).valueOf();
        return new Date(new Date(dataItem[headerCell.field]).toLocaleDateString()).valueOf() === filterDateValue;
    };

    function onFilterValueChange(newValue: string) {
        if (headerCell.column) {
            headerCell.column.filter = newValue;
        }
    }

    return <div style="display:flex;flex-direction:column;">
        <div style="display:flex;margin-bottom:6px;">
            <label style="margin-right:8px">按区间筛选</label>
            <f-switch v-model={status.value} size="small"></f-switch>
        </div>
        <f-date-picker v-model={filterValue.value} onDatePicked={onFilterValueChange}></f-date-picker>
        <f-tags style="margin-top:10px" data={tags.value} selectable={true} tag-style='capsule'></f-tags>
    </div>;
}
