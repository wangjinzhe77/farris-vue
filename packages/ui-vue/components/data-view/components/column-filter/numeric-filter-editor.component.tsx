import { ref } from "vue";
import { HeaderCell } from "../../composition/types";

export default function (headerCell: HeaderCell) {
    const filterValue = ref(headerCell.column?.filter || null);

    headerCell.filter = headerCell.filter || function (dataItem: any) {
        const currentfilterValue = Number.parseFloat(String(filterValue.value));
        const matchingValue = Number.parseFloat(dataItem[headerCell.field]);
        return isNaN(currentfilterValue) ? isNaN(matchingValue) : currentfilterValue === matchingValue;
    };

    function onValueChange(newValue: any) {
        if (headerCell.column) {
            headerCell.column.filter = newValue;
        }
        filterValue.value = newValue;
    }

    return <div>
        <f-number-spinner v-model={filterValue.value} can-null={true} precision={2} onValueChange={onValueChange}></f-number-spinner>
    </div>;
}
