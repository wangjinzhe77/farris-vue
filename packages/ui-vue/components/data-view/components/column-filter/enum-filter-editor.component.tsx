import { Ref } from "vue";
import { HeaderCell, UseDataView, UseFilterHistory, UseVirtualScroll } from "../../composition/types";

export default function (
    headerCell: HeaderCell,
    gridContentRef: Ref<any>,
    rightFixedGridContentRef: Ref<any>,
    useDataViewComposition: UseDataView,
    useFilterHistoryComposition: UseFilterHistory,
    useVirtualScrollComposition: UseVirtualScroll
) {
    headerCell.filter = headerCell.filter || function (dataItem: any) {
        const filterValues = String(headerCell.column?.filter || '').split(',');
        return filterValues.includes(String(dataItem[headerCell.field]));
    };

    const separator = ',';
    const selection = {
        multiSelect: true,
        multiSelectMode: 'OnCheckAndClick',
        showCheckbox: true,
    };
    const originalValues = useDataViewComposition.rawView.value.reduce((values: any[], dataItem: any, index: number) => {
        values.push(dataItem[headerCell.field]);
        return values;
    }, []);
    const recommendedDataValues = [...new Set(originalValues)];
    const recommendedData = recommendedDataValues.map((value: any, index: number) => {
        return { id: index, name: value, value };
    });

    /**
     * 值到数组值的转换
     */
    function toSelectionValues(value: any): string[] {
        return value?.split(separator) || [];
    }

    /**
     * 值到字符串值的转换
     */
    function toSelectionString(selectionResult: any[]) {
        const selectionValues = selectionResult.map((selectionItem: any) => selectionItem.value)
            .join(separator);
        return selectionValues;
    }

    function onConfirmFilterItem(selectionResult: any[]) {
        if (headerCell.column) {
            headerCell.column.filter = toSelectionString(selectionResult);
        }
    }

    return (
        <f-list-view data={recommendedData} multi-select={true}
            id-field="value" value-field="value" view="SingleView" header="Search" size="Small" selection={selection}
            selection-values={toSelectionValues(headerCell.column?.filter || '')}
            onSelectionChange={(result: any) => onConfirmFilterItem(result)}> </f-list-view>
    );
}
