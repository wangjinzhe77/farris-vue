import { ref, Ref } from "vue";
import { HeaderCell, UseDataView, UseFilterHistory, UseVirtualScroll } from "../../composition/types";
import { FSearchBox } from "@farris/ui-vue/components/search-box";
import { FTags, Tag } from "@farris/ui-vue/components/tags";

export default function (
    headerCell: HeaderCell,
    gridContentRef: Ref<any>,
    rightFixedGridContentRef: Ref<any>,
    useDataViewComposition: UseDataView,
    useFilterHistoryComposition: UseFilterHistory,
    useVirtualScrollComposition: UseVirtualScroll
) {
    const filterValue = ref(headerCell.column?.filter || '');
    headerCell.filterHistory = headerCell.filterHistory || useFilterHistoryComposition.getFilterHistory(headerCell);
    headerCell.filter = headerCell.filter || function (dataItem: any) {
        return ((dataItem[headerCell.field] || '') as string).startsWith(filterValue.value);
    };

    function onFilterValueChange(newValue: string) {
        if (headerCell.column) {
            headerCell.column.filter = newValue;
        }
    }

    function onRemoveHistoryTag(payload: { tags: Tag[], remove: Tag }, index: number) {
        useFilterHistoryComposition.removeFilterHistory(headerCell, payload.remove.name);
        headerCell.filterHistory = useFilterHistoryComposition.getFilterHistory(headerCell);
    }

    function onSelectHistoryTag(currentTag: Tag) {
        filterValue.value = currentTag.name;
        onFilterValueChange(filterValue.value);
    }

    function onUpdateHistoryTags(tags: Tag[]) {
        headerCell.filterHistory = tags;
    }

    const originalValues = useDataViewComposition.rawView.value.reduce((values: any[], dataItem: any, index: number) => {
        values.push(dataItem[headerCell.field]);
        return values;
    }, []);
    const recommendedDataValues = [...new Set(originalValues)];
    const recommendedData = recommendedDataValues.map((value: any, index: number) => {
        return { id: index, name: value };
    });

    return <div style="display:flex;flex-direction:column;">
        <FSearchBox v-model={filterValue.value}
            popup-host={gridContentRef.value} popup-right-boundary={rightFixedGridContentRef.value}
            popup-offset-x={useVirtualScrollComposition.offsetX}
            recommended-data={recommendedData}
            onChange={(data: any) => onFilterValueChange(data)}
        ></FSearchBox>
        <FTags style="margin-top:10px" tag-type="default" data={headerCell.filterHistory}
            show-close={true} selectable={true} onRemove={onRemoveHistoryTag} onSelectionChange={onSelectHistoryTag}
            onChange={onUpdateHistoryTags}></FTags>
    </div>;
}
