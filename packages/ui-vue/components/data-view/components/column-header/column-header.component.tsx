/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, ref, Ref, SetupContext, watch } from 'vue';
import {
    HeaderCell,
    HeaderCellStatus,
    UseColumn,
    UseDataView,
    UseDragColumn,
    UseColumnFilter,
    UseFilterHistory,
    UseFitColumn,
    UseGroupColumn,
    UseSidebar,
    UseSort,
    UseVirtualScroll,
    UseFilter,
    DataViewOptions,
    UseSelection,
    VisualData
} from '../../composition/types';
import getHeaderCellHander from './column-header-handler.component';
import getGridSettingsIconRender from '../column-setting/column-setting-icon.component';
import { useResize } from '../../composition/column/use-resize';
import { FCheckbox } from '@farris/ui-vue/components/checkbox';

export default function (
    props: DataViewOptions,
    context: SetupContext,
    gridContentRef: Ref<any>,
    leftFixedGridContentRef: Ref<any>,
    rightFixedGridContentRef: Ref<any>,
    useColumnComposition: UseColumn,
    useDataViewComposition: UseDataView,
    useDragColumnComposition: UseDragColumn,
    useColumnFilterComposition: UseColumnFilter,
    useFilterComposition: UseFilter,
    useFilterHistoryComposition: UseFilterHistory,
    useFitColumnComposition: UseFitColumn,
    useGroupColumnComposition: UseGroupColumn,
    useSelectionComposition: UseSelection,
    useSidebarComposition: UseSidebar,
    useSortComposition: UseSort,
    useVirtualScrollComposition: UseVirtualScroll,
    viewPortWidth: Ref<number>,
    visibleDatas: Ref<VisualData[]>
) {
    const { showSelectAll, selectAll, unSelectAll, selectedValues } = useSelectionComposition;
    const { showRowNumer, showSidebarCheckBox, sidebarTitle, sidebarCornerCellStyle } = useSidebarComposition;
    const { columnContext, hasLeftFixedColumn, hasRightFixedColumn } = useColumnComposition;
    const { onClickColumnResizeBar, resizeHandleStyle, resizeOverlayStyle } = useResize(
        props,
        columnContext,
        useFitColumnComposition,
        useVirtualScrollComposition
    );
    const { gridHeaderColumnsStyle, leftFixedGridHeaderColumnsStyle, rightFixedGridHeaderColumnsStyle } = useVirtualScrollComposition;
    const { dragstart, dragenter, dragover, dragend } = useDragColumnComposition;
    const shouldShowSideBarCorner = computed(() => showSelectAll.value || showSidebarCheckBox.value || showRowNumer.value);

    const defaultColumnHeight = columnContext.value.defaultColumnWidth;
    const gridHeaderCellHeight = 32;
    const headerCellPositionMap = new Map<number, number>([[0, 0]]);
    const shouldShowHeader = computed(()=>props.showHeader);

    function headerCellPosition(headerCell: HeaderCell, columnIndex: number): Record<string, any> {
        const headerCellPosition = headerCellPositionMap.get(columnIndex) || 0;
        const styleObject = {
            height: `${headerCell.depth * gridHeaderCellHeight}px`,
            left: `${headerCell.left}px`,
            top: `${(headerCell.layer - 1) * gridHeaderCellHeight}px`,
            width: `${headerCell.actualWidth}px`
        } as Record<string, any>;
        if (columnContext.value.headerDepth > 1) {
            styleObject['line-height'] = `${headerCell.depth * gridHeaderCellHeight}px`;
        }
        headerCellPositionMap.set(columnIndex + 1, headerCellPosition + (headerCell.actualWidth || defaultColumnHeight));
        return styleObject;
    }

    const headerRowClass = computed(() => {
        const classObject = {
            'fv-grid-header': true,
            'fv-grid-header-group-columns': columnContext.value.headerDepth > 1
        } as Record<string, boolean>;
        return classObject;
    });

    const headerRowStyle = computed(() => {
        const styleObject = {
            height: `${columnContext.value.headerDepth * gridHeaderCellHeight}px`
        } as Record<string, any>;
        return styleObject;
    });

    const shouldShowHeaderOperation = function (headerCell: HeaderCell) {
        return (
            (headerCell.status & HeaderCellStatus.sortable) === HeaderCellStatus.sortable ||
            (headerCell.status & HeaderCellStatus.filterable) === HeaderCellStatus.filterable
        );
    };

    const { renderHeaderCellHandler } = getHeaderCellHander(
        props,
        gridContentRef,
        leftFixedGridContentRef,
        rightFixedGridContentRef,
        useColumnComposition,
        useDataViewComposition,
        useColumnFilterComposition,
        useFilterComposition,
        useFilterHistoryComposition,
        useSortComposition,
        useVirtualScrollComposition
    );

    const shouldShowGridSettings = function (headerCell: HeaderCell) {
        return props.showSetting && !!headerCell.showSetting;
    };

    const { renderGridSettingsIcon } = getGridSettingsIconRender(
        props,
        gridContentRef,
        viewPortWidth,
        useColumnComposition,
        useDataViewComposition,
        useFilterComposition,
        useFitColumnComposition,
        useSortComposition,
        useVirtualScrollComposition
    );
    function getHeaderClass(headerCell: HeaderCell) {
        const classObject = { "fv-grid-header-cell": true };
        classObject["text-" + (headerCell.column?.halign || 'left')]=true;
        return classObject;
    }
    function renderGridHeaderCell(headerCell: HeaderCell, headerCells: HeaderCell[], columnIndex: number) {
        return (
            <div
                class={getHeaderClass(headerCell)}
                style={headerCellPosition(headerCell, columnIndex)}
                draggable="true"
                onDragstart={(payload: DragEvent) => dragstart(payload, headerCell, columnIndex)}
                onDragenter={(payload: DragEvent) => dragenter(payload, columnIndex)}
                onDragend={(payload: DragEvent) => dragend(payload, headerCell)}
                onDragover={(payload: DragEvent) => dragover(payload, columnIndex)}>
                {
                    context.slots.headerCellTemplate ?
                    context.slots.headerCellTemplate({ headerCell, headerCells, columnIndex }):
                    <span class="fv-column-title">{headerCell.title}</span>
                }
                {shouldShowHeaderOperation(headerCell) && renderHeaderCellHandler(headerCell, columnIndex, headerCells)}
                {shouldShowGridSettings(headerCell) && renderGridSettingsIcon()}
                {headerCell.resizable && (
                    <span
                        class="fv-column-resize-bar"
                        onMousedown={(payload: MouseEvent) => onClickColumnResizeBar(payload, headerCell.field)}></span>
                )}
            </div>
        );
    }

    function renderGridHeaderColumns(headerCells: HeaderCell[]): any[] {
        return headerCells.map((headerCell: HeaderCell, columnIndex: number) => {
            const headerCellsNodes: any[] = [];
            headerCellsNodes.push(renderGridHeaderCell(headerCell, headerCells, columnIndex));
            if (headerCell.children && headerCell.children.length) {
                const subHeaderCellNodes = renderGridHeaderColumns(headerCell.children);
                headerCellsNodes.push(...subHeaderCellNodes);
            }
            return headerCellsNodes;
        });
    }

    function renderGridHeaderLeftFixed() {
        return (
            <div class="fv-grid-header-left-fixed">
                <div class="fv-grid-header-columns" style={leftFixedGridHeaderColumnsStyle.value}>
                    {renderGridHeaderColumns(columnContext.value.leftHeaderColumns)}
                </div>
            </div>
        );
    }

    function renderGridHeaderRigthFixed() {
        return (
            <div class="fv-grid-header-right-fixed">
                <div class="fv-grid-header-columns" style={rightFixedGridHeaderColumnsStyle.value}>
                    {renderGridHeaderColumns(columnContext.value.rightHeaderColumns)}
                </div>
            </div>
        );
    }

    const hasSelectedAll = ref<boolean>(visibleDatas.value.length === selectedValues.value.length);

    function onClickSelectAll(payload: MouseEvent) {
        hasSelectedAll.value ? selectAll() : unSelectAll();
    }

    watch(hasSelectedAll, (newValue, oldValue) => {
        if (newValue !== oldValue) {
            if (newValue) {
                selectAll();
            } else {
                unSelectAll();
            }
        }
    });

    const hasSelectedItem = computed(() => {
        return selectedValues.value.length > 0 && visibleDatas.value.length !== selectedValues.value.length;
    });

    function renderGridSideBarCorner() {
        return (
            <div class="fv-grid-header-corner d-flex" style={sidebarCornerCellStyle.value}>
                {showSidebarCheckBox.value && showSelectAll.value && !props.hierarchy && (
                    <div class="d-inline-flex align-items-center">
                        <FCheckbox id='checkbox_for_select_all' v-model:checked={hasSelectedAll.value}
                            // indeterminate={visualData.indeterminate}
                        ></FCheckbox>
                    </div>
                )}
                {showSidebarCheckBox.value && !showSelectAll.value && !props.hierarchy && (
                    <div class="d-inline-flex align-items-center" style={{ opacity: '0' }}>
                        <FCheckbox
                        ></FCheckbox>
                    </div>
                )}
                {showRowNumer.value &&
                    <div class="d-inline-flex align-items-center">
                        <div class={{
                            'text-center': true,
                            'text-truncate': props.rowNumber.showEllipsis,
                            'w-100': props.rowNumber.showEllipsis
                        }}>{sidebarTitle.value}</div>
                    </div>}
            </div>
        );
    }

    function renderGridHeader() {
        return (
            <div class={headerRowClass.value} style={headerRowStyle.value}>
                {shouldShowSideBarCorner.value && renderGridSideBarCorner()}
                {hasLeftFixedColumn.value && renderGridHeaderLeftFixed()}
                <div class="fv-grid-header-primary">
                    <div class="fv-grid-header-columns" style={gridHeaderColumnsStyle.value}>
                        {renderGridHeaderColumns(columnContext.value.primaryHeaderColumns)}
                    </div>
                </div>
                {hasRightFixedColumn.value && renderGridHeaderRigthFixed()}
            </div>
        );
    }

    function renderGridColumnResizeOverlay() {
        return (
            <>
                <div class="fv-datagrid-resize-overlay" style={resizeOverlayStyle.value}></div>
                <div class="fv-datagrid-resize-proxy" style={resizeHandleStyle.value}></div>
            </>
        );
    }

    return { renderGridHeader, renderGridColumnResizeOverlay, shouldShowHeader };
}
