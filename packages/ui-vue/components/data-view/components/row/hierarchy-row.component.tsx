import { Ref, SetupContext, ref } from "vue";
import { CellMode, ColumnContext, DataColumn, DataViewOptions, UseDataView, UseEdit, UseHierarchy, UseRow, UseSelectHierarchyItem, UseSelection, UseVirtualScroll, UseVisualData, UseVisualDataBound, VisualData, VisualDataCell } from "../../composition/types";
import { useCellPosition } from "../../composition/appearance/use-cell-position";
import { useHierarchyStyle } from "../../composition/appearance/use-hierarchy-style";
import { useTreeNodeIcon } from "../../composition/hierarchy/use-tree-node-icon";
import { useToggleHierarchyItem } from "../../composition/hierarchy/use-toggle-hierarchy-item";
import { FCheckbox } from '@farris/ui-vue/components/checkbox';

export default function (
    props: DataViewOptions,
    context: SetupContext,
    columnContext: Ref<ColumnContext>,
    visibleDatas: Ref<VisualData[]>,
    useDataViewComposition: UseDataView,
    useEditComposition: UseEdit,
    useHierarchyComposition: UseHierarchy,
    useRowComposition: UseRow,
    useSelectionCompostion: UseSelection,
    useSelectHierarchyItemComposition: UseSelectHierarchyItem,
    useVirtualScrollComposition: UseVirtualScroll,
    useVisualDataComposition: UseVisualData,
    useVisualDataBoundComposition: UseVisualDataBound
) {
    const { onClickCell } = useEditComposition;
    const { gridRowClass, onClickRow, onMouseoverRow, onMouseoutRow } = useRowComposition;
    const { enableMultiSelect, select, unSelect, selectItem } = useSelectionCompostion;
    /** 单元格高度 */
    const customCellHeight = ref(props.cellHeight);
    /** 树节点图标 */
    const treeNodeIconsData = ref(props.treeNodeIconsData || {});

    const { hasChildrenField, shouldShowCheckBox, shouldShowIcon } = useHierarchyComposition;
    const { toggleTreeNode } = useToggleHierarchyItem(props as DataViewOptions, context, visibleDatas, useDataViewComposition, useHierarchyComposition, useVirtualScrollComposition, useVisualDataComposition, useVisualDataBoundComposition);

    const { toggleSelectHierarchyItem } = useSelectHierarchyItemComposition;

    const { cellKey, cellPosition, rowKey, rowPosition } = useCellPosition(props as DataViewOptions, columnContext);;

    const { hierarchyCellContentStyle } = useHierarchyStyle(props as DataViewOptions, visibleDatas, useHierarchyComposition);

    const { treeNodeIconsClass } = useTreeNodeIcon(props as DataViewOptions, treeNodeIconsData, hasChildrenField);

    function treeNodeIconClass(visualTreeNode: VisualData, visualTreeNodeCell: VisualDataCell) {
        const hasChildren = visualTreeNode.raw[hasChildrenField.value];
        const classObject = {
            'fv-tree-node-toggle': hasChildren && visualTreeNode.collapse,
            'fv-tree-node-toggle-minus': hasChildren && !visualTreeNode.collapse
        };
        return classObject;
    }

    // function renderTreeNodeCellLine(visualData: VisualData, cell: VisualDataCell) {
    //     const isTopLevelNode = (visualData.raw.__fv_parent_index__ === undefined || visualData.raw.__fv_parent_index__ === -1);
    //     const halfOfCollapseIconWidth = 8;
    //     const collapseIconOffset = isTopLevelNode ? 0 : halfOfCollapseIconWidth;
    //     const leafNodeOffset = 0;
    //     const verticalLineWidth = `${visualData.layer * 10 + visualData.layer * collapseIconOffset + leafNodeOffset}px`;
    //     return (
    //         <>
    //             <div style={{ width: verticalLineWidth }}></div>
    //             <div style="width:10px"></div>
    //         </>
    //     );
    // }

    function onClickToggleIcon(payload: MouseEvent, visualData: VisualData) {
        payload.stopPropagation();
        toggleTreeNode(visualData);
    }

    function renderTreeNodeCellToggleIcon(visualData: VisualData, cell: VisualDataCell) {
        return <div class={treeNodeIconClass(visualData, cell)} onClick={(payload: MouseEvent) => onClickToggleIcon(payload, visualData)}></div>;
    }

    function renderTreeNodeCellIcon(visualData: VisualData, cell: VisualDataCell) {
        return shouldShowIcon.value && <div id="treeNodeIcons" class={treeNodeIconsClass(visualData, cell)}></div>;
    }

    function onSelectableCheckboxChange(visualData: VisualData) {
        if(!enableMultiSelect.value){
            selectItem(visualData);
        }else {
            visualData.checked ? select(visualData) : unSelect(visualData);
            toggleSelectHierarchyItem(visualData);
        }
    }

    function renderSelectableCheckbox(visualData: VisualData, cell: VisualDataCell) {
        return shouldShowCheckBox.value && <FCheckbox
            id={'checkbox_for_' + cellKey(visualData, cell.index)} indeterminate={visualData.indeterminate}
            disabled={visualData.disabled}
            v-model:checked={visualData.checked} onChange={() => onSelectableCheckboxChange(visualData)}
        ></FCheckbox>;
    }

    function renderCellContentData(visualData: VisualData, cell: VisualDataCell) {
        return <span title={cell.data}>{cell.mode === CellMode.editing ? cell.getEditor(cell) : 
            (cell.formatter ? cell.formatter(cell, visualData) : (cell.data != null ? cell.data.toString() : cell.data))}</span>;
    }

    function renderHierarchyCellContent(visualData: VisualData, cell: VisualDataCell, cellPositionMap: Record<string, { left: number; width?: number }>) {
        return (
            <div ref={cell.setRef} key={cellKey(visualData, cell.index)} class="fv-grid-hierarchy-cell" style={cellPosition(cell, cellPositionMap)}>
                <div style={hierarchyCellContentStyle(visualData)}>
                    {/* {renderTreeNodeCellLine(visualData, cell)} */}
                    {renderTreeNodeCellToggleIcon(visualData, cell)}
                    {renderTreeNodeCellIcon(visualData, cell)}
                    {renderSelectableCheckbox(visualData, cell)}
                    {renderCellContentData(visualData, cell)}
                </div>
            </div>
        );
    }

    function renderCellContent(visualData: VisualData, cell: VisualDataCell, cellPositionMap: Record<string, { left: number; width?: number }>) {
        return (
            <div ref={cell.setRef} key={cellKey(visualData, cell.index)} class="fv-grid-cell" style={cellPosition(cell, cellPositionMap)}
                onClick={(payload: MouseEvent) => onClickCell(payload, cell, visualData, cell.column as DataColumn)}>
                {
                    context.slots.cellTemplate ?
                        context.slots.cellTemplate({ cell, row: visualData }) :
                        (
                            cell.mode === CellMode.editing ? 
                            cell.getEditor(cell) :
                            (
                                cell.formatter ? 
                                    cell.formatter(cell, visualData) : 
                                    (cell.data != null ? cell.data.toString() : cell.data)
                            )
                        )
                }
            </div>
        );
    }

    function isHierarchyCell(cell: VisualDataCell, area: 'left' | 'primary' | 'right') {
        return (area === 'left' && columnContext.value.leftColumns.length > 0 && columnContext.value.leftColumns[0].field === cell.field) ||
            (area === 'primary' && columnContext.value.primaryColumns.length > 0 && columnContext.value.primaryColumns[0].field === cell.field);
    }

    /** 渲染树节点 */
    function renderDataRow(
        visualData: VisualData, 
        cellPositionMap: Record<string, { left: number; width?: number }>, 
        area: 'left' | 'primary' | 'right' = 'primary'
    ) {
        return (
            <div
                ref={visualData.setRef}
                key={rowKey(visualData)}
                class={gridRowClass(visualData)}
                style={rowPosition(visualData)}
                onMouseover={(payload: MouseEvent) => onMouseoverRow(payload, visualData)}
                onMouseout={(payload: MouseEvent) => onMouseoutRow(payload, visualData)}
                onClick={(payload: MouseEvent) => onClickRow(payload, visualData)}>
                {Object.values(visualData.data)
                    .filter((cell: VisualDataCell) => cellPositionMap[cell.field] && cell.rowSpan === 1)
                    .map((cell: VisualDataCell) => {
                        return isHierarchyCell(cell, area) ?
                            renderHierarchyCellContent(visualData, cell, cellPositionMap) :
                            renderCellContent(visualData, cell, cellPositionMap);
                    })}
            </div>
        );
    }

    return { renderDataRow };
}
