/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DataColumn, DataColumnCommand, VisualData, VisualDataCell, VisualDataStatus } from '../../composition/types';

export default function () {

    function shouldShowCurrentCommandButton(command: DataColumnCommand,visualDataRow: VisualData) {
        switch (command.command) {
            case 'edit':
            case 'remove':
                return visualDataRow.status === VisualDataStatus.initial;
            case 'accept':
            case 'cancel':
                return visualDataRow.status === VisualDataStatus.editing;
            default:
                return !command.hidden;
        }
    }

    function excuteCommand(command: DataColumnCommand, payload: MouseEvent, visualDataRow: VisualData) {
        command.onClick(payload, visualDataRow.dataIndex, visualDataRow);
        switch (command.command) {
            case 'edit':
                visualDataRow.status === VisualDataStatus.editing;
                break;
            case 'accept':
            case 'cancel':
                visualDataRow.status === VisualDataStatus.initial;
                break;
        }
    }

    function renderCommandColumn(column: DataColumn, visualDataRow: VisualData) {
        return <div>
            {
                column.commands && column.commands.map((command: DataColumnCommand) => {
                    return shouldShowCurrentCommandButton(command,visualDataRow) && <f-button class="ml-2"
                        type={command.type}
                        size={command.size || 'small'}
                        onClick={(payload: MouseEvent) => excuteCommand(command, payload, visualDataRow)}
                    > {command.text} </f-button>;
                })
            }
        </div>;
    }

    return { renderCommandColumn };
}
