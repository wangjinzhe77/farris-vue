import { ExtractPropTypes, PropType } from "vue";
import { VisualDataCell } from "../../composition/types";

export const columnEditorProps = {
    cell: { type: Object as PropType<VisualDataCell> }
};

export type ColumnEditorProps = ExtractPropTypes<typeof columnEditorProps>;
