/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, onMounted, ref, SetupContext, watch } from 'vue';
import { DataViewOptions, UseDataView, UsePagination, UseVirtualScroll } from '../../composition/types';
import FPagination from '@farris/ui-vue/components/pagination';

export default function (
    props: DataViewOptions,
    context: SetupContext,
    dataView: UseDataView,
    virtualScroll: UseVirtualScroll,
    usePaginationComposition: UsePagination
) {
    const { pageIndex, totalItems } = dataView;
    const paginationRef = ref();
    const { pageSize, pageList, showGotoPage, showPageIndex, showPageList, mode, disabled, shouldRenderPagination } = usePaginationComposition;
    // 分页组件总数取决于客户端分页时dataview数量或者服务端分页时total值
    // const totalDataSize = ref(total.value: data.value.length);

    const disabledPaginationStyle = computed(() => {
        return {
            position: 'absolute',
            top: '0px',
            right: '0px',
            zIndex: '1',
            width: `${paginationRef.value?.$el?.offsetWidth || 0}px`,
            height: `${paginationRef.value?.$el?.offsetHeight || 0}px`
        };
    });

    // watch(total,(newTotal:number,oldTotal:number) => {
    //     if(newTotal !== oldTotal && mode.value === 'server') {
    //         totalDataSize.value = newTotal;
    //     }
    // });
    // watch(data,(newValue) => {
    //         totalDataSize.value = newValue.length;
    // });

    function onPageIndexChanged(pageInfo: { pageIndex: number; pageSize: number }) {
        const { pageIndex, pageSize } = pageInfo;
        if (mode.value !== 'server') {
            dataView.navigatePageTo(pageIndex);
            virtualScroll.resetScroll();
        }
        context.emit('pageIndexChanged', { pageIndex, pageSize });
    }

    function onPageSizeChanged(pageInfo: { pageIndex: number; pageSize: number }) {
        const { pageIndex, pageSize } = pageInfo;
        if (shouldRenderPagination.value && mode.value !== 'server') {
            dataView.changePageSizeTo(pageSize);
            virtualScroll.resetScroll();
        }
        context.emit('pageSizeChanged', { pageIndex, pageSize });
    }

    function onChange(pageInfo: { pageIndex: number; pageSize: number }) {
        const { pageIndex, pageSize } = pageInfo;
        if (shouldRenderPagination.value && mode.value !== 'server') {
            // 服务端分页不执行重定向数据的逻辑
            dataView.navigatePageTo(pageIndex);
            virtualScroll.resetScroll();
        }
        context.emit('changed', { pageIndex, pageSize });
    }

    onMounted(() => {
        // 初始化第一页数据
        if (shouldRenderPagination.value) {
            if (mode.value !== 'server') {
                onPageIndexChanged({ pageIndex: pageIndex.value, pageSize: pageSize.value });
            }
        }
    });

    function renderDataGridPagination() {
        return (
            <div class="fv-datagrid-pagination position-relative">
                {disabled.value && <div style={disabledPaginationStyle.value}></div>}
                <FPagination
                    ref={paginationRef}
                    mode="default"
                    v-model:currentPage={pageIndex.value}
                    v-model:pageSize={pageSize.value}
                    showGoButton={showGotoPage.value}
                    showPageList={showPageList.value}
                    showPageNumbers={showPageIndex.value}
                    totalItems={totalItems.value}
                    pageList={pageList.value}
                    onPageIndexChanged={onPageIndexChanged}
                    onPageSizeChanged={onPageSizeChanged}
                    onChanged={onChange}></FPagination>
            </div>
        );
    }

    return { renderDataGridPagination };
}
