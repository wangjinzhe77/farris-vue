import { ref } from "vue";
import { DataViewOptions, UseDataView, UseDragColumn, UseGroupData, UseVirtualScroll } from "../../composition/types";

export default function (
    props: DataViewOptions,
    useDataViewComposition: UseDataView,
    useDragColumnComposition: UseDragColumn,
    useGroupDataComposition: UseGroupData,
    useVirtualScrollComposition: UseVirtualScroll
) {
    const { dropOnGroupPanel, groupColumnItems } = useDragColumnComposition;
    const { groupFields, shouldGroupingData, showGroupPanel } = useGroupDataComposition;
    const tagsRef = ref<any>();

    function onUpdateGroup(payload: { name: string; value: string }[]) {
        groupFields.value = payload.map((groupcolumnItem: { name: string; value: string }) => groupcolumnItem.value);
        useDataViewComposition.updateDataView();
        useVirtualScrollComposition.reCalculateVisualDataRows();
    }

    function onDragover(payload: DragEvent) {
        payload.preventDefault();
    }

    function renderGroupPanel() {
        return shouldGroupingData.value && showGroupPanel.value &&
            <div class="fv-grid-group-panel" onDrop={dropOnGroupPanel} onDragover={onDragover}>
                {groupColumnItems.value && <f-tags ref={tagsRef} style="margin:0.375rem" tag-type="default" data={groupColumnItems.value}
                    tag-style="capsule" show-close={true} onChange={onUpdateGroup}
                ></f-tags>}
            </div>;
    }

    return { renderGroupPanel };
}
