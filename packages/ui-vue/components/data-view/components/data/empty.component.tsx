import { SetupContext } from "vue";

export default function (context: SetupContext) {
    function renderEmpty() {
        return <div class="f-datagrid-norecords">
            <div class="f-datagrid-norecords-content"
                style="margin: 0;width: 100%;position: absolute; top: 50%;transform:translateY(-50%)"> {context.slots.empty && context.slots.empty() || '暂无数据'}
            </div>
        </div>;
    }
    return { renderEmpty };
}
