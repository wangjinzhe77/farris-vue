import { useNumberFormat, resolveField } from '@farris/ui-vue/components/common';
import { DataColumn, VisualData } from '../../composition/types';

export default function () {
    const { formatTo } = useNumberFormat();
    // format number
    function renderNumber(value: any, column: DataColumn) {
        const { formatter } = column;
        const numberOptions = {
            precision: formatter.precision,
            prefix: formatter.prefix || '',
            suffix: formatter.suffix || '',
            decimalSeparator: formatter.decimal || '.',
            groupSeparator: formatter.thousand || '',
        };
        return formatTo(value, numberOptions);
    }

    function renderNumberColumn(column: DataColumn, visualDataRow: VisualData) {
        const value = resolveField(visualDataRow.raw, column.field);
        return renderNumber(value, column);
    }
    return { renderNumberColumn };
}
