import {
    ColumnFormatterDataType, DataColumn, DataColumnCommand,
    VisualData, VisualDataCell, VisualDataStatus
} from '../../composition/types';
import getEnumColumn from './enum.component';
import getBooleanColumn from './boolean.component';
import getDateColumn from './date.component';
import getNumberColumn from './number.component';
import { resolveField } from '@farris/ui-vue/components/common';

export default function () {

    const { renderBooleanColumn } = getBooleanColumn();
    const { renderEnumColumn } = getEnumColumn();
    const { renderNumberColumn } = getNumberColumn();
    const { renderDateColumn } = getDateColumn();

    // format none
    function renderNone(value: any) {
        return value;
    }

    // format custom
    function renderCustom(value: any, column: DataColumn) {
        const { formatter } = column;
        return formatter.customFormat ?
            new Function(`return ${formatter.customFormat}`)() :
            renderNone(value);
    }

    function renderFormatColumn(dataType: string, column: DataColumn, visualDataRow: VisualData) {

        const value = resolveField(visualDataRow.raw, [column.field]);
        const { formatter } = column;
        // no formatter
        if (formatter.type === 'none') {
            return renderNone(value);
        }
        // custom formatter
        if (formatter.type === 'custom') {
            return renderCustom(value, column);
        }
        // formatter
        if (formatter.type === 'enum') {
            return renderEnumColumn(column, visualDataRow);
        } else if (formatter.type === 'number') {
            return renderNumberColumn(column, visualDataRow);
        } else if (formatter.type === 'boolean') {
            return renderBooleanColumn(column, visualDataRow);
        } else if (formatter.type === 'date') {
            return renderDateColumn(column, visualDataRow);
        }
        //  default  no formmater
        return renderNone(value);
    }
    return { renderFormatColumn };
}
