import { resolveField } from '@farris/ui-vue/components/common';
import { ColumnFormatterDataType, DataColumn, VisualData } from '../../composition/types';

export default function () {
    // format image
    function renderImage(value: any, column: DataColumn) {
        if (value) {
            return <span innerHTML={column.formatter.trueText}></span>;
        }
        return <span innerHTML={column.formatter.falseText}></span>;
    }

    // format text
    function renderText(value: any, column: DataColumn) {
        if (value) {
            return column.formatter?.trueText;
        }
        return column.formatter?.falseText;
    }

    function renderBooleanColumn(column: DataColumn, visualDataRow: VisualData) {
        const { formatter } = column;
        const value = resolveField(visualDataRow.raw, column.field);
        if (formatter.type === ColumnFormatterDataType.TEXT) {
            return renderText(value, column);
        } else if (formatter.type === ColumnFormatterDataType.IMAGE) {
            return renderImage(value, column);
        }
    }
    return { renderBooleanColumn };
}
