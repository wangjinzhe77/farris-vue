import { resolveField, useDateFormat } from '@farris/ui-vue/components/common';
import { DataColumn, VisualData } from '../../composition/types';

export default function () {
    const { formatTo } = useDateFormat();
    // format date
    function renderDate(value: any, column: DataColumn) {
        return formatTo(value, column.formatter.dateFormat);
    }

    function renderDateColumn(column: DataColumn, visualDataRow: VisualData) {
        const value = resolveField(visualDataRow.raw, column.field);
        return renderDate(value, column);
    }
    return { renderDateColumn };
}
