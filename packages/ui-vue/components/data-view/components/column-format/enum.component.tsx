import { resolveField } from '@farris/ui-vue/components/common';
import { DataColumn, VisualData } from '../../composition/types';

export default function () {

    function renderEnumColumn(column: DataColumn, visualDataRow: VisualData) {
        const { formatter } = column;
        const value = resolveField(visualDataRow.raw, column.field);
        if (value.includes(',')) {
            const valueList = value.split(',');
            const dataMap = formatter.data.reduce((total: any, next: { value: number, name: number }) => {
                total[next.value] = next;
                return total;
            }, {});
            const nameList = valueList.map((value: string) => dataMap[value].name);
            return nameList.join(',');
        }
        const selectedDataItem = formatter.data.find((item: { value: number, name: number }) => item.value === value);
        return selectedDataItem?.name || '';
    }

    return { renderEnumColumn };
}
