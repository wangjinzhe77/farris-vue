/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref } from 'vue';
import {
    DataViewOptions, UseCellContent, UseRow, UseSelection,
    UseSidebar, UseVirtualScroll, VisualData, VisualDataCell
} from '../../composition/types';

export default function (
    props: DataViewOptions,
    useRowComposition: UseRow,
    useSelectionComposition: UseSelection,
    useSidebarComposition: UseSidebar,
    useVirtualScrollComposition: UseVirtualScroll
) {
    const { isSelectingHirarchyItem, multiSelectOnClickRow, multiSelectOnlyOnCheck,
        selectItem, toggleSelectItem, toggleSelectItemWithoutRow } = useSelectionComposition;
    const { onMouseoverRow, sidebarRowClass } = useRowComposition;
    const { showRowNumer, showSidebarCheckBox, sidebarCellPosition, rowKey, cellKey } = useSidebarComposition;
    const { gridSideStyle } = useVirtualScrollComposition;


    function onClickCheckLabel(payload: MouseEvent) {
        payload.stopPropagation();
    }

    function renderSideBarCheckbox(visualData: VisualData) {
        return (
            <div class="d-inline-flex ">
                <div class="custom-control
            custom-checkbox f-checkradio-single" style="margin:0;padding-left:0.5rem">
                    <input id={cellKey(visualData)} title={cellKey(visualData)} type="checkbox"
                        indeterminate={visualData.indeterminate} disabled={visualData.disabled}
                        v-model={visualData.checked} class="custom-control-input" />
                    <label class="custom-control-label" for={cellKey(visualData)} onClick={onClickCheckLabel}></label>
                </div>
            </div>

        );
    }

    function renderSideBarRowNumber(dataItem: VisualData) {
        return (
            <div
                class="fv-grid-sidebar-row-number justify-content-center"
                style="padding:0"
                onMouseover={(payload: MouseEvent) => onMouseoverRow(payload, dataItem)}
            >
                <div title={dataItem.dataIndex} class={{
                    'text-center': true,
                    'text-truncate': props.rowNumber.showEllipsis,
                    'w-100': props.rowNumber.showEllipsis
                }}>
                    {dataItem.dataIndex}

                </div>
            </div>
        );
    }

    function onClickSideBarCell(payload: MouseEvent, visibleDatas: Ref<VisualData[]>, dataItem: VisualData) {
        isSelectingHirarchyItem.value ? (
            multiSelectOnClickRow.value ? toggleSelectItem(dataItem) : selectItem(dataItem)
        ) : (
            multiSelectOnClickRow.value ?
                toggleSelectItem(dataItem) :
                multiSelectOnlyOnCheck.value ? toggleSelectItemWithoutRow(dataItem) :
                    selectItem(dataItem)
        );
    }

    function renderGridSideBar(visibleDatas: Ref<VisualData[]>) {
        return (
            (showSidebarCheckBox.value || showRowNumer.value) &&
            visibleDatas.value.map((dataItem: VisualData) => {
                return <div key={rowKey(dataItem)} class={sidebarRowClass(dataItem)} style={sidebarCellPosition(dataItem)}
                    onClick={(payload: MouseEvent) => onClickSideBarCell(payload, visibleDatas, dataItem)}>
                    {showSidebarCheckBox.value && !props.hierarchy &&
                        renderSideBarCheckbox(dataItem)}
                    {showRowNumer.value && renderSideBarRowNumber(dataItem)}
                </div>;
            })
        );
    }

    function renderDataGridSidebar(visibleDatas: Ref<VisualData[]>) {
        return (
            <div class="fv-grid-content-side">
                <div class="fv-grid-side" style={gridSideStyle.value}>
                    {renderGridSideBar(visibleDatas)}
                </div>
            </div>
        );
    }

    return { renderDataGridSidebar };
}
