import FTransfer from '@farris/ui-vue/components/transfer';
import FTabs, { FTabPage } from '@farris/ui-vue/components/tabs';
// import FTabPage from '../../../tabs/src/components/tab-page.component';
import { FOrder, OrderedItem, SortType } from '@farris/ui-vue/components/order';
import FConditionList, { Condition, FieldConfig } from '@farris/ui-vue/components/condition';
import { App, Ref, computed, inject, nextTick, ref } from 'vue';
import {
    DataColumn, DataViewOptions, UseColumn, UseDataView,
    UseFilter, UseFitColumn, UseSort, UseVirtualScroll
} from '../../composition/types';
import { ConditionValue } from '../../../condition/src/composition/condition-value/types';
import { EditorConfig } from '@farris/ui-vue/components/dynamic-form';

export default function (
    props: DataViewOptions,
    gridContentRef: Ref<any>,
    viewPortWidth: Ref<number>,
    useColumnComposition: UseColumn,
    useDataViewComposition: UseDataView,
    useFilterComposition: UseFilter,
    useFitColumnComposition: UseFitColumn,
    useSorterComposition: UseSort,
    useVirtualScrollComposition: UseVirtualScroll,
    modalService: any
) {
    const identifyField = 'id';
    const conditionListRef = ref<any>();
    let modalInstance: any;
    const {
        applySortableColumns,
        collectionFilterableColumns,
        collectionSortableColumns,
        columnContext,
        updateColumnSettingIcon: resetSettingIconPosition
    } = useColumnComposition;
    const { conditions } = useFilterComposition;
    const { calculateColumnsSize: resetVisibleColumnPosition } = useFitColumnComposition;
    const { fitHorizontalScroll: resetHorizontalScrollPosition } = useVirtualScrollComposition;
    const orderedResult = ref<OrderedItem[]>([]);
    const shouldApplyOrderedResult = computed(() => !!orderedResult.value.length);
    const dataSoruce = computed(() => {
        return columnContext.value.primaryColumns.map((dataGridColumn: DataColumn) => ({
            id: dataGridColumn.field,
            name: dataGridColumn.title
        }));
    });

    const visibleColumns = computed(() => {
        return columnContext.value.primaryColumns
            .filter((dataGridColumn: DataColumn) => dataGridColumn.visible)
            .map((visibleColumn: DataColumn) => ({
                id: visibleColumn.field,
                name: visibleColumn.title
            }));
    });

    const sortedColumns = computed<OrderedItem[]>(() => {
        return collectionSortableColumns().map((sortedColumn: DataColumn) => ({
            id: sortedColumn.field,
            name: sortedColumn.title,
            order: sortedColumn.sort as SortType
        }));
    });

    const filterableColumns = computed<FieldConfig[]>(() => {
        return collectionFilterableColumns().map<FieldConfig>((filterableColumn: DataColumn) => ({
            id: filterableColumn.field,
            code: filterableColumn.field,
            controlType: '',
            labelCode: filterableColumn.field,
            name: filterableColumn.title,
            editor: (filterableColumn.editor as EditorConfig), // || ({ type: useFilterComposition.getFilterEditorType(filterableColumn) } as EditorConfig),
            value: {} as ConditionValue,
            visible: true
        } as FieldConfig));
    });

    function onCloseSettingPanel() {
        modalInstance?.destroy();
    }

    function resetVisibleColumns() {
        const dataGridColumnMap = new Map<string, DataColumn>();
        columnContext.value.primaryColumns.reduce((columnMap: Map<string, DataColumn>, column: DataColumn) => {
            column.visible = false;
            columnMap.set(column.field, column);
            return columnMap;
        }, dataGridColumnMap);
        return dataGridColumnMap;
    }

    function applyVisibleStatus(visibleStatus: { id: string; name: string }[], dataGridColumnMap: Map<string, DataColumn>) {
        const latestOrderedVisibleColumns = visibleStatus.map(({ id: field }) => {
            const column = dataGridColumnMap.get(field) as DataColumn;
            column.visible = true;
            dataGridColumnMap.delete(field);
            return column;
        });
        return latestOrderedVisibleColumns;
    }

    function onVisibleColumnsChange(visibleStatus: { id: string; name: string }[]) {
        const dataGridColumnMap = resetVisibleColumns();
        const latestOrderedVisibleColumns = applyVisibleStatus(visibleStatus, dataGridColumnMap);
        columnContext.value.primaryColumns = [...latestOrderedVisibleColumns, ...Array.from(dataGridColumnMap.values())];
        resetSettingIconPosition();
        resetVisibleColumnPosition();
        nextTick(() => {
            resetHorizontalScrollPosition();
        });
    }

    function onSortedColumnsChange(ordereditems: OrderedItem[]) {
        orderedResult.value = ordereditems;
    }

    function renderSettingsPanel(app: App) {
        return (
            <div class="fv-grid-settings">
                <FTabs tab-type="pills" justify-content="center">
                    {{
                        headerPrefix: () => (
                            <div class="modal-title">
                                <span class="modal-title-label">列配置</span>
                            </div>
                        ),
                        default: () => [
                            <FTabPage id="display-columns" title="显示列" class="container">
                                <FTransfer
                                    style="height: 480px"
                                    identify-field={identifyField}
                                    data-source={dataSoruce.value}
                                    selections={visibleColumns.value}
                                    onChange={onVisibleColumnsChange}></FTransfer>
                            </FTabPage>,
                            <FTabPage id="column-order" title="列排序" class="container">
                                <FOrder
                                    style="height: 480px"
                                    data-source={dataSoruce.value}
                                    items={sortedColumns.value}
                                    onChange={onSortedColumnsChange}></FOrder>
                            </FTabPage>,
                            <FTabPage id="column-filter" title="筛选" class="container">
                                <FConditionList
                                    ref={conditionListRef}
                                    style="height: 480px"
                                    fields={filterableColumns.value}
                                    conditions={conditions.value}></FConditionList>
                            </FTabPage>
                        ],
                        headerSuffix: () => (
                            <div class="f-btn-icon f-bare" onClick={(payload: MouseEvent) => onCloseSettingPanel()}>
                                <span class="f-icon modal_close"></span>
                            </div>
                        )
                    }}
                </FTabs>
            </div>
        );
    }

    function resetSortableColumns() {
        const sortableColumnMap = new Map<string, DataColumn>();
        collectionSortableColumns().reduce((columnMap: Map<string, DataColumn>, sortableColumn: DataColumn) => {
            sortableColumn.sort = 'none';
            sortableColumn.sortOrder = 0;
            columnMap.set(sortableColumn.field, sortableColumn);
            return columnMap;
        }, sortableColumnMap);
        return sortableColumnMap;
    }

    function applySortResult(sortableColumnMap: Map<string, DataColumn>) {
        const sortableColumns = orderedResult.value
            .filter((item: OrderedItem) => item.id && item.order)
            .map((orderedItem: OrderedItem, index: number) => {
                const sortOrder = index + 1;
                const sortableColumn = sortableColumnMap.get(orderedItem.id) as DataColumn;
                if (sortableColumn) {
                    sortableColumn.sort = orderedItem.order;
                    sortableColumn.sortOrder = sortOrder;
                }
                return sortableColumn;
            });
        return sortableColumns;
    }

    function applyOrderResult() {
        if (shouldApplyOrderedResult.value) {
            const sortableColumnMap = resetSortableColumns();
            const sortableColumns = applySortResult(sortableColumnMap);
            applySortableColumns(sortableColumns, useDataViewComposition, useSorterComposition);
            useVirtualScrollComposition.reCalculateVisualDataRows();
            resetVisibleColumnPosition();
        }
    }

    function applyFilterResult() {
        if (conditionListRef.value) {
            conditions.value = conditionListRef.value.getConditions() as Condition[];
            useDataViewComposition.refresh();
            useVirtualScrollComposition.reCalculateVisualDataRows();
        }
    }

    function acceptCallback() {
        applyOrderResult();
        applyFilterResult();
    }

    function rejectCallback() { }

    function openSettingPanel() {
        const title = '';
        const width = 800;
        const showButtons = true;
        const showHeader = false;
        modalInstance = modalService.open({
            title, width, showButtons, showHeader,
            render: renderSettingsPanel, acceptCallback, rejectCallback
        });
    }

    return { acceptCallback, rejectCallback, renderSettingsPanel, openSettingPanel };
}
