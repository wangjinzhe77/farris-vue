/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DataViewOptions, UseColumn, UseDataView, UseFilter, UseFitColumn, UseSort, UseVirtualScroll } from '../../composition/types';
import { F_MODAL_SERVICE_TOKEN, FModalService } from '@farris/ui-vue/components/modal';
import getDataGridSettingsRender from './column-setting.component';
import { inject, Ref } from 'vue';

export default function (
    props: DataViewOptions,
    gridContentRef: Ref<any>,
    viewPortWidth: Ref<number>,
    useColumnComposition: UseColumn,
    useDataViewComposition: UseDataView,
    useFilterComposition: UseFilter,
    useFitColumnComposition: UseFitColumn,
    useSorterComposition: UseSort,
    useVirtualScrollComposition: UseVirtualScroll
) {
    const modalService = inject(F_MODAL_SERVICE_TOKEN) as any;
    function onClickColumnHandler(payload: MouseEvent) {
        const {
            openSettingPanel
        } = getDataGridSettingsRender(
            props,
            gridContentRef,
            viewPortWidth,
            useColumnComposition,
            useDataViewComposition,
            useFilterComposition,
            useFitColumnComposition,
            useSorterComposition,
            useVirtualScrollComposition,
            modalService
        );
        openSettingPanel();
    }

    function renderGridSettingsIcon() {
        return (
            <span class="fv-grid-settings-icon" onClick={onClickColumnHandler}>
                <i class="f-icon f-icon-home-setup"></i>
            </span>
        );
    }

    return { renderGridSettingsIcon };
}
