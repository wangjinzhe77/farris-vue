export default function () {

    function renderDisableMask() {
        return <div class="fv-grid-disable"></div>;

    }
    return { renderDisableMask };
}
