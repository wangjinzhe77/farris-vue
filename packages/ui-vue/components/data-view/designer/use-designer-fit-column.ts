/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref, computed, ref, watch } from 'vue';
import { ColumnContext, DataColumn, DataViewOptions, UseFitColumn, UseGroupColumn } from '../composition/types';

export function useDesignerFitColumn(
    props: DataViewOptions,
    context: Ref<ColumnContext>,
    gridContentRef: Ref<any>,
    viewPortWidth: Ref<number>,
    useGroupColumnComposition: UseGroupColumn
): UseFitColumn {
    const { getGridHeaderCells } = useGroupColumnComposition;
    const defaultColumnWidth = 120;
    const defaultCheckboxWidth = 50;
    // const showRowNumer = ref(props.rowNumber?.enable || false);
    // const showRowCheckbox = ref(props.selection?.multiSelect || false);
    // const sidebarColumnWidth = ref(
    //     0 + (showRowNumer.value ? props.rowNumber?.width || 0 : 0) + (showRowCheckbox.value ? defaultCheckboxWidth : 0)
    // );

    const showRowNumer = computed(() => props.rowNumber?.enable || false);
    const showRowCheckbox = computed(() => props.selection?.multiSelect || props.selection?.showCheckbox || false);
    const sidebarColumnWidth = computed(() =>
        0 + (showRowNumer.value ? props.rowNumber?.width || 32 : 0) + (showRowCheckbox.value ? defaultCheckboxWidth : 0)
    );

    // const fitColumns = ref(props.columnOption?.fitColumns || false);
    const fitColumns = computed(() => props.columnOption?.fitColumns);
    // const fitMode = ref(fitColumns.value && props.columnOption?.fitMode || 'average');
    const fitMode = computed(() => fitColumns.value && props.columnOption?.fitMode || 'none');

    function calculateColumnWidthByPercentage(column: DataColumn, context: ColumnContext, viewPortWidth: number) {
        const widthPerscent = parseInt(column.width as string, 10) / 100;
        const actualWidth = viewPortWidth * widthPerscent;
        return actualWidth;
    }

    function calculateColumnWidthNormally(column: DataColumn, context: ColumnContext, viewPortWidth: number) {
        if (typeof column.width === 'string') {
            return calculateColumnWidthByPercentage(column, context, viewPortWidth);
        }
        return column.width || defaultColumnWidth;
    }

    function calculateColumnsSizeByPercentage(context: Ref<ColumnContext>, viewPortWidth: number) {
        const columnsWithNumber: DataColumn[] = [];
        const columnsWithPercentage: DataColumn[] = [];
        let totalWidthWithNumber = 0;
        context.value.primaryColumns
            .filter((column: DataColumn) => column.visible)
            .forEach((visibleColumn: DataColumn) => {
                if (typeof visibleColumn.width === 'string') {
                    columnsWithPercentage.push(visibleColumn);
                } else {
                    visibleColumn.width = visibleColumn.width || defaultColumnWidth;
                    totalWidthWithNumber += visibleColumn.width;
                    columnsWithNumber.push(visibleColumn);
                }
            });
        const viewPortWidthNumber = viewPortWidth * (columnsWithNumber.length /
            context.value.primaryColumns.filter((column: DataColumn) => column.visible).length);
        columnsWithNumber.forEach((column: DataColumn) => {
            const actualWidth = viewPortWidthNumber * ((column.width as number) / totalWidthWithNumber);
            column.actualWidth = actualWidth;
            context.value.primaryColumnsWidth += column.actualWidth;
        });
        const restOfViewPortWidth = viewPortWidth - viewPortWidthNumber;
        columnsWithPercentage.forEach((column: DataColumn) => {
            const actualWidth = calculateColumnWidthByPercentage(column, context.value, restOfViewPortWidth);
            column.actualWidth = actualWidth;
            context.value.primaryColumnsWidth += column.actualWidth;
        });
    }

    function calculateColumnsSizeByExpand(context: Ref<ColumnContext>, viewPortWidth: number) {
        const columnsWithNumber: DataColumn[] = [];
        const columnsWithPercentage: DataColumn[] = [];
        const preCalculateWidthMap = new WeakMap<DataColumn, number>();
        let totalWidthWithPercentage = 0;
        let totalWidthWithNumber = 0;
        context.value.primaryColumns
            .filter((column: DataColumn) => column.visible)
            .forEach((visibleColumn: DataColumn) => {
                if (typeof visibleColumn.width === 'string') {
                    const preCalculateWidth = calculateColumnWidthByPercentage(visibleColumn, context.value, viewPortWidth);
                    preCalculateWidthMap.set(visibleColumn, preCalculateWidth);
                    totalWidthWithPercentage += preCalculateWidth;
                    columnsWithPercentage.push(visibleColumn);
                } else {
                    visibleColumn.width = visibleColumn.width || defaultColumnWidth;
                    totalWidthWithNumber += visibleColumn.width;
                    columnsWithNumber.push(visibleColumn);
                }
            });
        const hasSpaceToExpand = viewPortWidth - totalWidthWithPercentage > totalWidthWithNumber;
        if (hasSpaceToExpand) {
            const restOfViewPortWidth = viewPortWidth - totalWidthWithPercentage;
            columnsWithPercentage.forEach((column: DataColumn) => {
                column.actualWidth = preCalculateWidthMap.get(column) || defaultColumnWidth;
                context.value.primaryColumnsWidth += column.actualWidth;
            });
            columnsWithNumber.forEach((column: DataColumn) => {
                column.actualWidth = restOfViewPortWidth * ((column.width as number) / totalWidthWithNumber);
                context.value.primaryColumnsWidth += column.actualWidth;
            });
        } else {
            calculateColumnsSizeByPercentage(context, viewPortWidth);
        }
    }

    function calculateColumnsSizeByNormal(context: Ref<ColumnContext>, viewPortWidth: number) {
        context.value.primaryColumns
            .filter((column: DataColumn) => column.visible)
            .forEach((visibleColumn: DataColumn) => {
                visibleColumn.actualWidth = calculateColumnWidthNormally(visibleColumn, context.value, viewPortWidth);
                context.value.primaryColumnsWidth += visibleColumn.actualWidth;
            });
    }

    const columnWidthCalculators = {
        expand: calculateColumnsSizeByExpand,
        none: calculateColumnsSizeByNormal,
        percentage: calculateColumnsSizeByPercentage
    } as any;

    function calculateColumnHeaders(context: Ref<ColumnContext>) {
        context.value.leftHeaderColumns = Array.from(getGridHeaderCells(context.value.leftColumns
            .filter((column: DataColumn) => column.visible)).values());
        context.value.primaryHeaderColumns = Array.from(getGridHeaderCells(context.value.primaryColumns
            .filter((column: DataColumn) => column.visible)).values());
        context.value.rightHeaderColumns = Array.from(getGridHeaderCells(context.value.rightColumns
            .filter((column: DataColumn) => column.visible)).values());
    }

    function calculateColumnsWidth(context: Ref<ColumnContext>) {
        context.value.leftColumnsWidth = 0;
        context.value.primaryColumnsWidth = 0;
        context.value.rightColumnsWidth = 0;
        context.value.leftColumns.filter((column: DataColumn) => column.visible).forEach((column: DataColumn) => {
            context.value.leftColumnsWidth += column.actualWidth || 0;
        });
        context.value.primaryColumns.filter((column: DataColumn) => column.visible).forEach((column: DataColumn) => {
            context.value.primaryColumnsWidth += column.actualWidth || 0;
        });
        context.value.rightColumns.filter((column: DataColumn) => column.visible).forEach((column: DataColumn) => {
            context.value.rightColumnsWidth += column.actualWidth || 0;
        });
        context.value = Object.assign({}, context.value);
    }

    function tryToArrangeRightColumnsWithPrimary(
        context: Ref<ColumnContext>,
        dataContentWidth: number,
        viewPortWidthWithoutFixedRight: number
    ): boolean {
        let columnsWidth = 0;
        const originalPrimaryColumns = context.value.primaryColumns;
        const originalRightColumns = context.value.rightColumns;

        context.value.rightColumns.filter((column: DataColumn) => column.visible).forEach((column: DataColumn) => {
            column.actualWidth = calculateColumnWidthNormally(column, context.value, dataContentWidth);
            columnsWidth += column.actualWidth;
        });

        const leftViewPortWidth = viewPortWidthWithoutFixedRight - columnsWidth;

        const calculator = columnWidthCalculators[fitMode.value];
        if (calculator) {
            calculator(context, leftViewPortWidth);
        }

        if (context.value.primaryColumnsWidth + columnsWidth <= viewPortWidthWithoutFixedRight) {
            context.value.primaryColumns = [...originalPrimaryColumns, ...originalRightColumns];
            context.value.primaryColumnsWidth += columnsWidth;
            context.value.rightColumns = [];
            context.value.rightColumnsWidth = 0;
            return true;
        }
        const columnsMap = new Map<string, Array<DataColumn>>();
        columnsMap.set('primary', []);
        columnsMap.set('right', []);
        context.value.primaryColumns.reduce((columnsMap: Map<string, Array<DataColumn>>, currentColumn: DataColumn) => {
            if (currentColumn.fixed === 'right') {
                columnsMap.get('right')?.push(currentColumn);
            } else {
                columnsMap.get('primary')?.push(currentColumn);
            }
            return columnsMap;
        }, columnsMap);
        if (columnsMap.get('right')?.length) {
            context.value.primaryColumns = [...(columnsMap.get('primary') || [])];
            context.value.rightColumns = [...(columnsMap.get('right') || [])];
        }
        context.value.primaryColumnsWidth = 0;
        context.value.rightColumnsWidth = 0;
        return false;
    }

    function calculateCellPositionInRow(columns: DataColumn[]) {
        const positionMap = {} as Record<string, { left: number; width?: number }>;
        columns.reduce(
            (latestPosition: Record<string, { left: number; width?: number }>, currentColumn: DataColumn, columnIndex: number) => {
                const nextColumn = columnIndex < columns.length - 1 ? columns[columnIndex + 1] : null;
                if (columnIndex === 0) {
                    latestPosition[currentColumn.field] = { left: 0 };
                }
                if (currentColumn.actualWidth !== undefined) {
                    latestPosition[currentColumn.field].width = currentColumn.actualWidth;
                }
                if (nextColumn) {
                    latestPosition[nextColumn.field] = {
                        left: latestPosition[currentColumn.field].left + (currentColumn.actualWidth || 0)
                    };
                }
                return latestPosition;
            },
            positionMap
        );
        return positionMap;
    }

    function getColumnsSize() {
        return {
            left: calculateCellPositionInRow(context.value.leftColumns.filter((column: DataColumn) => column.visible)),
            primary: calculateCellPositionInRow(context.value.primaryColumns.filter((column: DataColumn) => column.visible)),
            right: calculateCellPositionInRow(context.value.rightColumns.filter((column: DataColumn) => column.visible))
        };
    }

    function calculateColumnsSize() {
        if (gridContentRef.value) {
            context.value.leftColumnsWidth = 0;
            context.value.primaryColumnsWidth = 0;
            context.value.rightColumnsWidth = 0;

            const dataContentWidth = gridContentRef.value.clientWidth - sidebarColumnWidth.value;
            context.value.leftColumns.filter((column: DataColumn) => column.visible).forEach((column: DataColumn) => {
                column.actualWidth = calculateColumnWidthNormally(column, context.value, dataContentWidth);
                context.value.leftColumnsWidth += column.actualWidth;
            });

            const viewPortWidthWithoutFixedRight = dataContentWidth - context.value.leftColumnsWidth;

            if (tryToArrangeRightColumnsWithPrimary(context, dataContentWidth, viewPortWidthWithoutFixedRight)) {
                viewPortWidth.value = viewPortWidthWithoutFixedRight;
            } else {
                context.value.rightColumns.filter((column: DataColumn) => column.visible).forEach((column: DataColumn) => {
                    column.actualWidth = calculateColumnWidthNormally(column, context.value, dataContentWidth);
                    context.value.rightColumnsWidth += column.actualWidth;
                });

                const calculatedViewPortWidth = dataContentWidth - context.value.leftColumnsWidth - context.value.rightColumnsWidth;

                const calculator = columnWidthCalculators[fitMode.value];
                if (calculator) {
                    calculator(context, calculatedViewPortWidth);
                }

                viewPortWidth.value = calculatedViewPortWidth;
            }

            calculateColumnHeaders(context);
            context.value = Object.assign({}, context.value);
        }
    }

    watch(() => props.columnOption?.fitColumns, (newValue, oldValue) => {
        if (newValue !== oldValue) {
            if (newValue) {
                calculateColumnsSize();
            }
        }
    });

    watch(fitMode, (newValue, oldValue) => {
        if (newValue !== oldValue) {
            if (fitColumns.value) {
                calculateColumnsSize();
            }
        }
    });

    return { calculateColumnHeaders, calculateColumnsSize, calculateColumnsWidth };
}
