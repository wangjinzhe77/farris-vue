/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { cloneDeep } from 'lodash-es';
import {
    DataViewFilter, DataViewOptions, DataViewSorter,
    UseDataView, UseFilter, UseHierarchy, UseIdentify, UsePagination, VisualData
} from '../types';
import { computed, ref, watch } from 'vue';
import { useFilter } from '../filter/use-filter';
import { useGroupData } from './use-group-data';
import { useHierarchyData } from './use-hierarchy-data';
import { FNotifyService } from '../../../notify';
import { useTreeData } from '../..';

export function useDataView(
    props: DataViewOptions,
    columnMaps: Map<string, any>,
    filterComposition: UseFilter,
    useHierarchyCompostion: UseHierarchy,
    useIdentifyCompostion: UseIdentify
): UseDataView {
    const collapseMap = new Map<string, boolean>();
    const filterMap = new Map<string, DataViewFilter>();
    const groupFilterMap = new Map<string, DataViewFilter>();
    const foldFilterMap = new Map<string, DataViewFilter>();
    const sorterMap = new Map<string, DataViewSorter>();
    const filters = ref<DataViewFilter[]>([]);
    const sorters = ref<DataViewSorter[]>([]);
    const groupComposition = useGroupData(props, useIdentifyCompostion);
    const { generateGroupData, groupFields, shouldGroupingData } = groupComposition;
    const originalData = ref(props.data);
    const hierarchyDataComposition = useHierarchyData(props, originalData, useHierarchyCompostion, useIdentifyCompostion);
    const { generateHierarchyData, hasRealChildren, insertSibling, insertChild,
        shouldStratifyData, trimmedOriginalData } = hierarchyDataComposition;
    const { idField, reviseIdentifyField } = useIdentifyCompostion;
    const getNewDataItem = ref(props.newDataItem);
    const totalData = ref(props.data || []);
    const rawView = ref(props.data);
    const { flatTreeData } = useTreeData(props);
    function flattenTreeData(data: any) {
        return flatTreeData(data) as any;
    }
    if (props.data && props.data[0]?.children) {
        const flattenedData = flattenTreeData(props.data);
        originalData.value = flattenedData;
        totalData.value = flattenedData;
        rawView.value = flattenedData;
    }
    const summaryOptions = ref(props.summary);
    const groupSummaryFields = computed(() => {
        const options = summaryOptions.value;
        return options?.groupFields || [];
    });

    // const summaries = groupSummaryFields.value.reduce((sumaries: Map<string, number>, summaryField: string) => {
    //     sumaries.set(summaryField, 0);
    //     return sumaries;
    // }, new Map<string, number>());

    let summaries = new Map<string, number>();

    const totalItems = ref(totalData.value.length);

    const pagination = ref(props.pagination);
    function getPageSize() {
        return (pagination.value && pagination.value.enable && pagination.value.size > 0) ? pagination.value.size : totalData.value.length;
    }
    const pageIndex = ref(1);
    const shouldUpdateTotalItemByLocalData = computed<boolean>(() => {
        return (pagination.value && pagination.value.enable && pagination.value.mode === 'client');
    });
    const notifyService: any = new FNotifyService();

    function resetDataView() {
        totalData.value = originalData.value;
        if (shouldUpdateTotalItemByLocalData.value) {
            totalItems.value = totalData.value.length;
        }
        if (shouldGroupingData.value) {
            totalData.value = generateGroupData(groupFields.value, originalData.value, columnMaps);
        }
        summaries = groupSummaryFields.value.reduce((sumaries: Map<string, number>, summaryField: string) => {
            sumaries.set(summaryField, 0);
            return sumaries;
        }, summaries);
        const rowIndexInInitialPage = 0;
        const pageSize = getPageSize();
        // 计算完整数据范围当前页首行索引, 未启用分页时pageIndex的值缺省为1, 计算结果为0.
        const startRowIndexInWholeDataScope = (pageIndex.value - 1) * pageSize;
        // 计算本地数据范围内当前页首行索引
        // 判断完整数据范围内当前页首行索引是否超出本地数据范围
        const rowIndexExceedLocalScope = startRowIndexInWholeDataScope > totalData.value.length - 1;
        // 以下情况当前页首行索引超出本地数据范围, startRowIndex为初始页首行索引
        // 1. 服务端分页非首页数据
        // 以下情况下当前页首行索引在本地数据范围内, startRowIndex同startRowIndexInWholeDataScope
        // 1. 未启用分页
        // 2. 客户端分页
        // 3. 服务端分页首页数据
        const startRowIndex = rowIndexExceedLocalScope ? rowIndexInInitialPage : startRowIndexInWholeDataScope;
        // 计算当前数据视图中末页末行索引
        // 以下情况下startRowIndex + pageSize.value小于等于数据视图数据总数, 末页末行索引的值为startRowIndex + pageSize.value
        // 1. 未启用分页
        // 2. 服务器端分页首页
        // 以下情况下startRowIndex + pageSize.value大于于数据视图数据总数, 末页末行索引的值为totalData.value.length
        const endRowIndex = Math.min(startRowIndex + pageSize, totalData.value.length);
        let rowIndex = 0;
        const dataViewItems: any[] = [];
        if (totalData.value.length) {
            reviseIdentifyField(totalData.value);
            const shouldGenerateInnerId = idField.value !== props.idField;
            for (let index = startRowIndex; index < endRowIndex; index++) {
                const dataItem = totalData.value[index] as any;
                if (dataItem.__fv_data_grid_group_row__) {
                    rowIndex = 0;
                    dataItem.__fv_data_index__ = '';
                } else if (dataItem.__fv_data_grid_group_summary__) {
                    dataItem.__fv_data_index__ = '';
                } else {
                    rowIndex++;
                    dataItem.__fv_data_index__ = rowIndex + (pageIndex.value - 1) * pageSize;
                }
                dataItem.__fv_index__ = index;
                dataItem.__fv_visible_index__ = index;
                if (props.rowOption && props.rowOption.disabledField) {
                    dataItem.__fv_disabled__ = dataItem[props.rowOption?.disabledField];
                }
                if (shouldGenerateInnerId) {
                    dataItem[idField.value] = index;
                }
                groupSummaryFields.value.forEach((summaryField: string) => {
                    const summaryFieldValue = summaries.get(summaryField) || 0;
                    summaries.set(summaryField, summaryFieldValue + dataItem[summaryField]);
                });
                dataViewItems.push(dataItem);
            }
        }
        if (shouldStratifyData.value) {
            generateHierarchyData(dataViewItems);
        }
        rawView.value = [...dataViewItems];
        return dataViewItems;
    }
    // 加载所有数据
    const dataView = ref(resetDataView());

    const visibleDataItems = computed(() => {
        return dataView.value.filter((dataItem: any) => dataItem.__fv_visible__ !== false);
    });

    function reOrderVisibleIndex() {
        visibleDataItems.value.forEach((dataItem: any, index: number) => {
            dataItem.__fv_visible_index__ = index;
            return dataItem;
        });
    }

    function applyFilterAndSorter(filters: DataViewFilter[], sorters: DataViewSorter[], reset = false) {
        const source = reset ? resetDataView() : [...rawView.value];
        const filteredData = source.filter((dataItem: any) => filterComposition.apply(dataItem));
        const groupedData = filters && filters.length ? filteredData
            .filter((dataItem: any) => {
                return filters.reduce((result: boolean, filter: DataViewFilter) => {
                    return result && filter.filter(dataItem);
                }, true);
            }) : filteredData;
        const sorteredData = sorters && sorters.length ?
            groupedData.sort((preItem: any, postItem: any) => {
                const multiSorter = [...sorters];
                const firstSorter = multiSorter.shift() as DataViewSorter;
                let sortResult = firstSorter.compare(preItem[firstSorter.field], postItem[firstSorter.field]);
                while (sortResult === 0 && multiSorter.length !== 0) {
                    const sorter = multiSorter.shift();
                    if (sorter) {
                        sortResult = sorter.compare(preItem[sorter.field], postItem[sorter.field]);
                    }
                }
                return sortResult;
            }) : groupedData;
        dataView.value = sorteredData
            .map((dataItem: any, index: number) => {
                if (dataItem.__fv_data_grid_group_row__) {
                    const groupedRowId = `group_of_${dataItem.__fv_data_grid_group_field__}_${dataItem.__fv_data_grid_group_value__}`;
                    dataItem.__fv_data_grid_group_collapse__ = !!collapseMap.get(groupedRowId);
                }
                dataItem.__fv_index__ = index;
                dataItem.__fv_visible_index__ = index;
                return dataItem;
            });
        return dataView.value;
    }

    function setSorters(latestSorters: DataViewSorter[]) {
        sorterMap.clear();
        sorters.value = latestSorters;
        sorters.value.reduce((result: Map<string, DataViewSorter>, sorter: DataViewSorter) => {
            result.set(sorter.field, sorter);
            return result;
        }, sorterMap);
        return applyFilterAndSorter([], sorters.value);
    }

    function addSorter(sortKey: string, sorter: DataViewSorter) {
        sorterMap.set(sortKey, sorter);
        sorters.value = Array.from(sorterMap.values());
        return applyFilterAndSorter([], sorters.value);
    }

    function removeSorter(sortKey: string) {
        sorterMap.delete(sortKey);
        sorters.value = Array.from(sorterMap.values());
        return applyFilterAndSorter([], sorters.value);
    }

    function addFilter(filterKey: string, filter: DataViewFilter) {
        filterMap.set(filterKey, filter);
        filters.value = Array.from(filterMap.values());
        return applyFilterAndSorter([], Array.from(sorterMap.values()));
    }

    function removeFilter(filterKey: string) {
        filterMap.delete(filterKey);
        filters.value = Array.from(filterMap.values());
        return applyFilterAndSorter([], Array.from(sorterMap.values()), true);
    }

    function removeAllFilter() {
        filterMap.clear();
        filters.value = [];
        return applyFilterAndSorter([], Array.from(sorterMap.values()), true);
    }

    function collapse(collapseField: string, collapseValue: any) {
        const groupedRowId = `group_of_${collapseField}_${collapseValue}`;
        collapseMap.set(groupedRowId, true);
        const collapseFieldFilter = (dataItem: any) => dataItem[collapseField] !== collapseValue;
        groupFilterMap.set(`collapse_${collapseField}_${collapseValue}`, {
            field: collapseField,
            fieldName: collapseField,
            fieldType: 'string',
            filterValue: collapseValue,
            filter: collapseFieldFilter
        });
        return applyFilterAndSorter(Array.from(groupFilterMap.values()), Array.from(sorterMap.values()));
    }

    function expand(expandField: string, expandValue: any) {
        const groupedRowId = `group_of_${expandField}_${expandValue}`;
        collapseMap.set(groupedRowId, false);
        groupFilterMap.delete(`collapse_${expandField}_${expandValue}`);
        return applyFilterAndSorter(Array.from(groupFilterMap.values()), Array.from(sorterMap.values()), true);
    }

    function fold(visualData: VisualData) {
        const idOfDataItemToBeFolded = visualData.raw[idField.value];
        const foldFilter = (dataItem: any) => !(dataItem.__fv_parents__ as Map<number, boolean>).has(idOfDataItemToBeFolded);
        foldFilterMap.set(`fold_${idOfDataItemToBeFolded}`, {
            field: 'id',
            fieldName: 'id',
            fieldType: 'string',
            filterValue: String(idOfDataItemToBeFolded),
            filter: foldFilter
        });
        applyFilterAndSorter(Array.from(foldFilterMap.values()), Array.from(sorterMap.values()));
        generateHierarchyData(dataView.value);
    }

    function removeFoldFilterOf(dataItem: any) {
        const idOfDataItemToBeFolded = dataItem[idField.value];
        foldFilterMap.delete(`fold_${idOfDataItemToBeFolded}`);
    }

    function unFold(visualData: VisualData) {
        removeFoldFilterOf(visualData.raw);
        applyFilterAndSorter(Array.from(foldFilterMap.values()), Array.from(sorterMap.values()), true);
        generateHierarchyData(dataView.value);
    }

    function updateDataView() {
        const latestData = resetDataView();
        dataView.value = latestData;
        if (props.hierarchy) {
            applyFilterAndSorter(Array.from(foldFilterMap.values()), Array.from(sorterMap.values()));
            generateHierarchyData(dataView.value);
        }
    }

    function load(newData: Record<string, any>[]) {
        // jumphere, it need to remove the dependency of children property at first row, it's awful.
        const shouldFlattenData = newData && newData.length && newData[0].children;
        const loadingData = shouldFlattenData ? flattenTreeData(newData) : newData;
        originalData.value = loadingData;
        updateDataView();
    }

    function getRange(filters: DataViewFilter[], start: number, end: number) {
        const filteredData =
            filters && filters.length
                ? dataView.value.filter((dataItem: any, index: number) => {
                    return filters.reduce((result: boolean, filter: DataViewFilter) => {
                        return result && filter.filter(dataItem);
                    }, true);
                })
                : dataView.value;
        return filteredData.slice(start, end);
    }

    function navigatePageTo(targetPageIndex: number) {
        const pageSize = getPageSize();
        const intendingDataScope = targetPageIndex * pageSize;
        const shouldNavigating = 0 < intendingDataScope && intendingDataScope <= totalItems.value;
        if (shouldNavigating) {
            pageIndex.value = targetPageIndex;
            updateDataView();
        }
    }
    /** 新增树节点 */
    function addNewDataItem(newDataItem: any) {
        originalData.value.push(cloneDeep(newDataItem));
        dataView.value = resetDataView();
    }

    function insertNewDataItem(targetIndex = 0) {
        const canInsertNewItem = targetIndex > -1;
        if (!canInsertNewItem) {
            return false;
        }
        const newDataItem = getNewDataItem.value();
        const toInsertHierarchyData = props.hierarchy;
        toInsertHierarchyData ? insertSibling(targetIndex, newDataItem, originalData) :
            originalData.value.splice(targetIndex, 0, newDataItem);
        updateDataView();
    }

    function insertNewChildDataItem(targetIndex = 0) {
        const canInsertHierarchyData = targetIndex > -1 && props.hierarchy;
        if (!canInsertHierarchyData) {
            return false;
        }
        const newDataItem = getNewDataItem.value();
        const targetDataItem = originalData.value[targetIndex];
        removeFoldFilterOf(targetDataItem);
        insertChild(targetIndex, newDataItem, originalData);
        updateDataView();
    }

    function removeDataItem(dataIndex: number) {
        const targetIndex = dataIndex ? dataIndex - 1 : 0;
        originalData.value.splice(targetIndex, 1);
        updateDataView();
    }

    function removeDataItemById(dataItemId: string) {
        const indexInOriginalData = originalData.value.findIndex((dataItem: any) => dataItem[idField.value] === dataItemId);
        if (indexInOriginalData !== -1) {
            originalData.value.splice(indexInOriginalData, 1);
            updateDataView();
        }
    }

    /** 删除树节点 */
    function removeHierarchyDataItem(targetIndex: number) {
        const dataItemToRemove = targetIndex > -1 ? originalData.value[targetIndex] : null;
        const canRemoveCurrentNode = dataItemToRemove && (dataItemToRemove.deletable !== false);
        if (!canRemoveCurrentNode) {
            return false;
        }
        originalData.value = trimmedOriginalData(dataItemToRemove, originalData.value);
        updateDataView();
    }

    function recalculatePageIndexByPageSize(currentPageIndex: number, originalPageSize: number, newPageSize: number) {
        const currentVisibleRecordsCount = currentPageIndex * originalPageSize;
        const latestPageIndex = Math.ceil(currentVisibleRecordsCount / newPageSize);
        return latestPageIndex;
    }

    function refresh() {
        const collepseFilter = groupFilterMap.size > 0 ? Array.from(groupFilterMap.values()) : [];
        return applyFilterAndSorter(collepseFilter, Array.from(sorterMap.values()));
    }

    function changePageSizeTo(newPageSize: number) {
        if (pagination.value) {
            pagination.value.size = newPageSize;
            updateDataView();
        }
    }

    /** 编辑树节点 */
    function editDataItem(editIndex: string | number, newName: string) {
        const modifyItem = originalData.value[Number(editIndex) - 1];
        if (modifyItem.editable !== false) {
            modifyItem.name = newName;
            updateDataView();
        } else {
            notifyService.show({
                showCloseButton: false,
                timeout: 3000,
                animate: 'fadeIn',
                toasts: [{ type: 'string', title: '提示', msg: '当前节点禁止编辑' }],
            });
        }
    }

    function getSelectionItems(selectionValues: string[]): any[] {
        const selectionItems = selectionValues.map((selectionValue: string) => {
            const selectionItemIndex = rawView.value.findIndex((rawViewItem: any) => rawViewItem[idField.value] === selectionValue);
            if (selectionItemIndex > -1) {
                return rawView.value[selectionItemIndex];
            }
            return null;
        });
        return selectionItems.filter((item: any) => item != null);
    }
    return {
        addFilter,
        addNewDataItem,
        addSorter,
        changePageSizeTo,
        collapse,
        dataView,
        editDataItem,
        expand,
        filters,
        fold,
        getRange,
        getSelectionItems,
        hasRealChildren,
        insertNewChildDataItem,
        insertNewDataItem,
        load,
        navigatePageTo,
        pageIndex,
        rawView,
        refresh,
        removeAllFilter,
        removeDataItem,
        removeDataItemById,
        removeFilter,
        removeHierarchyDataItem,
        removeSorter,
        reOrderVisibleIndex,
        setSorters,
        sorters,
        summaries,
        totalItems,
        unFold,
        updateDataView,
        visibleDataItems
    };
}
