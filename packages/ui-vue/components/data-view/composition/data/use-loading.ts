import { computed, inject, Ref, ref, watch } from 'vue';
import { FLoadingService } from '../../../loading';
import { DataGridProps, dataGridProps } from '../../../data-grid/src/data-grid.props';

export function useLoading(props: DataGridProps, gridRef: Ref<any>) {
    const LoadingService: FLoadingService | any = inject<FLoadingService>('FLoadingService');
    const showLoading = computed(() => typeof props.loading === 'object' ? props.loading.show : props.loading);
    const loadingMessage = computed(() => typeof props.loading === 'object' ? props.loading.message : '正在加载...');
    let loadingInstance;
    function renderLoading() {
        const config: any = {
            message: loadingMessage.value,
            target: gridRef.value, type: 0,
            targetPosition: 'relative',
            delay: 0
        };
        loadingInstance = LoadingService.show(config);
        return loadingInstance;

    }

    // data-grid loading feature
    watch(showLoading, (newValue, oldValue) => {
        if (newValue !== oldValue) {
            if (newValue) {
                renderLoading();
            } else {
                loadingInstance?.value?.close();
            }
        }
    });

    return { showLoading, loadingMessage, renderLoading };
};
