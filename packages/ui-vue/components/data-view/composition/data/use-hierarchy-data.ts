 
import { Ref, ref } from "vue";
import { DataViewOptions, HierarchyGraphNode, UseHierarchy, UseHierarchyData, UseIdentify } from "../types";

export function useHierarchyData(
    props: DataViewOptions,
    originalData: Ref<any[]>,
    useHierarchyComposition: UseHierarchy,
    useIdentifyCompostion: UseIdentify
): UseHierarchyData {

    const shouldStratifyData = ref(!!props.hierarchy);
    const { idField } = useIdentifyCompostion;
    const { collapseField, hasChildrenField, parentIdField } = useHierarchyComposition;
    const hierarchyGraph = new Map<string, HierarchyGraphNode>();

    function generateParentIdAndChildrenMap(rawData: any[], rawDataItemMap: Map<string, any>): Map<string, any[]> {
        const parentIdAndChildrenMap = new Map<string, any[]>();
        rawData.reduce((childrenMap: Map<string, any[]>, rawDataItem: any) => {
            const parentIdOfCurrent = rawDataItem[parentIdField.value] !== undefined ? rawDataItem[parentIdField.value] : '';
            if (parentIdOfCurrent && rawDataItemMap.has(parentIdOfCurrent)) {
                const parentDataItem = rawDataItemMap.get(parentIdOfCurrent);
                rawDataItem.__fv_parent_index__ = parentDataItem.__fv_index__;
                parentDataItem.__fv_hasChildren__ = true;
            }
            const siblingsOfCurrent = childrenMap.has(parentIdOfCurrent) ? childrenMap.get(parentIdOfCurrent) as any[] : [];
            siblingsOfCurrent.push(rawDataItem);
            childrenMap.set(parentIdOfCurrent, siblingsOfCurrent);
            return childrenMap;
        }, parentIdAndChildrenMap);
        return parentIdAndChildrenMap;
    }

    function generateDataItemMap(rawData: any[]): Map<string, any> {
        const dataItemMap = new Map<string, any[]>();
        rawData.reduce((dataMap: Map<string, any>, rawDataItem: any) => {
            const dataItemId = rawDataItem[idField.value];
            dataMap.set(dataItemId, rawDataItem);
            return dataMap;
        }, dataItemMap);
        return dataItemMap;
    }

    function traversingAndRecordAncestor(hierarchyData: any[]) {
        hierarchyData.forEach((currentDataItem: any) => {
            const currentDataId = currentDataItem[idField.value];
            const childrenIndex = currentDataItem.__fv_children_index__;
            childrenIndex.map((childIndex: number) => hierarchyData[childIndex])
                .forEach((childDataItem: any) => {
                    childDataItem.__fv_parents__ = new Map<string, boolean>([
                        ...currentDataItem.__fv_parents__.entries(),
                        [currentDataId, true]
                    ]);
                });
        });
    }

    function reorderAndRemoveDuplicateChild(hierarchyData: any[]) {
        hierarchyData.forEach((currrent: any) => {
            const uniqueSet = new Set<number>(currrent.__fv_children_index__);
            currrent.__fv_children_index__ = [...uniqueSet].sort((preItemId, postItemId) => preItemId - postItemId);
        });
    }

    function traversingAndRecordDescendant(hierarchyData: any[], rawDataItemMap: Map<string, any>) {
        hierarchyData.forEach((currentDataItem: any) => {
            const parents = currentDataItem.__fv_parents__ as Map<string, boolean>;
            Array.from(parents.keys()).map((dataId: string) => rawDataItemMap.get(dataId))
                .forEach((ancestor: any) => {
                    ancestor.__fv_children_index__.push(currentDataItem.__fv_index__);
                });
        });
        reorderAndRemoveDuplicateChild(hierarchyData);
    }

    function calculateDescendantsLineLength(hierarchyData: any[]) {
        hierarchyData.forEach((currentDataItem: any) => {
            currentDataItem.__fv_child_with_lines__ = [];
            currentDataItem.__fv_child_length__ = currentDataItem.__fv_descendant_index__.length;
            currentDataItem.__fv_descendant_index__.map((descendantIndex: number) => hierarchyData[descendantIndex])
                .reduce((currentItem: any, descendantItem: any) => {
                    currentItem.__fv_child_length__ += descendantItem.__fv_children_index__.length;
                    currentItem.__fv_child_with_lines__.push(...descendantItem.__fv_children_index__, descendantItem.__fv_index__);
                    return currentItem;
                }, currentDataItem);
        });
    }

    function newHierarchyGroupNode(id: string, index: number): HierarchyGraphNode {
        const parentId = '';
        const parentIndex = -1;
        const parents = new Map<string, HierarchyGraphNode>();
        const children = new Map<string, HierarchyGraphNode>();
        const descendant = new Map<string, HierarchyGraphNode>();
        return { id, index, parentId, parentIndex, parents, children, descendant };
    }

    function traversingAndRecordHierarchyRelation(hierarchyGraph: Map<string, HierarchyGraphNode>, parentIdAndChildrenMap: Map<string, any[]>) {
        Array.from(parentIdAndChildrenMap.keys()).forEach((parentId: string) => {
            const childrenDataItems = parentIdAndChildrenMap.get(parentId) as any[];
            childrenDataItems.reduce((treeNodeGraph: Map<string, HierarchyGraphNode>, childDataItem: any) => {
                const childDataItemId = childDataItem[idField.value];
                const childTreeNode = hierarchyGraph.get(childDataItem[idField.value]) as HierarchyGraphNode;
                const parentTreeNode = hierarchyGraph.get(parentId) as HierarchyGraphNode;
                if (parentTreeNode) {
                    parentTreeNode.children.set(childDataItemId, childTreeNode);
                    parentTreeNode.children.set(childDataItemId, childTreeNode);
                    childTreeNode.parentId = parentId;
                    childTreeNode.parentIndex = parentTreeNode.index;
                    childTreeNode.parents.set(parentId, parentTreeNode);
                }
                return treeNodeGraph;
            }, hierarchyGraph);
        });
    }

    function recoredNodeToAncestor(currentNode: HierarchyGraphNode) {
        const childNodes = currentNode.children;
        childNodes.forEach((childNode: HierarchyGraphNode) => {
            childNode.parents = new Map<string, HierarchyGraphNode>([
                ...currentNode.parents.entries(),
                [currentNode.id, currentNode]
            ]);
            recoredNodeToAncestor(childNode);
        });
    }

    function traversingAndRecordAncestorNode(hierarchyData: any[], hierarchyGraph: Map<string, HierarchyGraphNode>) {
        hierarchyData.filter((dataItem: any) => dataItem[parentIdField.value] === undefined || dataItem[parentIdField.value] === '')
            .forEach((rootDataItem: any) => {
                const currentDataId = rootDataItem[idField.value];
                const currentNode = hierarchyGraph.get(currentDataId) as HierarchyGraphNode;
                recoredNodeToAncestor(currentNode);
            });
    }

    function traversingAndRecordDescendantNode(hierarchyGraph: Map<string, HierarchyGraphNode>) {
        Array.from(hierarchyGraph.values()).forEach((currentNode: HierarchyGraphNode) => {
            Array.from(currentNode.parents.values()).forEach((parentNode: HierarchyGraphNode) => {
                parentNode.descendant.set(currentNode.id, currentNode);
            });
        });
    }

    function generateHierarchyGraph(rawData: any): Map<string, HierarchyGraphNode> {
        hierarchyGraph.clear();
        // 初始化树节点图对象
        rawData.forEach((rawDataItem: any, index: number) => {
            const currentDataId = rawDataItem[idField.value];
            hierarchyGraph.set(currentDataId, newHierarchyGroupNode(currentDataId, index));
        });
        const rawDataItemMap = generateDataItemMap(rawData);
        const parentIdAndChildrenMap = generateParentIdAndChildrenMap(rawData, rawDataItemMap);
        // 建立节点层级
        traversingAndRecordHierarchyRelation(hierarchyGraph, parentIdAndChildrenMap);
        // 建立祖先节点路径
        traversingAndRecordAncestorNode(rawData, hierarchyGraph);
        // 建立后代节点路径
        traversingAndRecordDescendantNode(hierarchyGraph);
        // console.log(hierarchyGraph);
        return hierarchyGraph;
    };

    function hasRealChildren(rawDataItem: any) {
        const currentDataId = rawDataItem[idField.value];
        const treeNodeInGraph = hierarchyGraph.get(currentDataId) as HierarchyGraphNode;
        return treeNodeInGraph && treeNodeInGraph.children.size > 0;
    }

    function hasLazyChildren(rawDataItem: any) {
        const markedAsHasChildren = rawDataItem[hasChildrenField.value];
        return markedAsHasChildren && !hasRealChildren(rawDataItem);
    }

    function generateHierarchyData(rawData: any[]): any[] {
        generateHierarchyGraph(originalData.value);
        const rawDataItemMap = generateDataItemMap(rawData);
        const parentIdAndChildrenMap = generateParentIdAndChildrenMap(rawData, rawDataItemMap);
        rawData.forEach((rawDataItem: any, index: number) => {
            const currentDataId = rawDataItem[idField.value];
            const childrenIndex = parentIdAndChildrenMap.has(currentDataId) ?
                parentIdAndChildrenMap.get(currentDataId)?.map((childItem: any) => childItem.__fv_index__) as number[] : [];
            const treeDataNode = newHierarchyGroupNode(currentDataId, index);
            rawDataItem.__fv_graph_node__ = treeDataNode;
            rawDataItem.__fv_children_index__ = childrenIndex;
            rawDataItem.__fv_descendant_index__ = [...childrenIndex];
            rawDataItem.__fv_parents__ = new Map<string, boolean>;
            if (hasLazyChildren(rawDataItem)) {
                rawDataItem[collapseField.value] = true;
            }
        });
        traversingAndRecordAncestor(rawData);
        traversingAndRecordDescendant(rawData, rawDataItemMap);
        if (props.showLines) {
            calculateDescendantsLineLength(rawData);
        };
        return rawData;
    }

    function insertSibling(targetIndex: number, newDataItem: any, originalData: Ref<any[]>) {
        const currentDataItem = originalData.value[targetIndex];
        const currentDataItemId = currentDataItem[idField.value];
        const currentTreeNode = hierarchyGraph.get(currentDataItemId) as HierarchyGraphNode;
        if (currentTreeNode) {
            const parentNode = hierarchyGraph.get(currentTreeNode.parentId) as HierarchyGraphNode;
            let indexToInsert = originalData.value.length - 1;
            if (parentNode) {
                const currentNodeIndexInSlibings = Array.from(parentNode.children.keys()).indexOf(currentTreeNode.id);
                const isLastNode = currentNodeIndexInSlibings === parentNode.children.size - 1;
                if (isLastNode) {
                    const lastDescendantNode = Array.from(parentNode.descendant.values())[parentNode.descendant.size - 1];
                    const lastDescendantIndexInOriginalData = originalData.value.findIndex((item: any) => item[idField.value] === lastDescendantNode.id);
                    indexToInsert = lastDescendantIndexInOriginalData + 1;
                } else {
                    const slibingNode = Array.from(parentNode.children.values())[currentNodeIndexInSlibings + 1];
                    const slibingIndexInOriginalData = originalData.value.findIndex((item: any) => item[idField.value] === slibingNode.id);
                    indexToInsert = slibingIndexInOriginalData;
                }
                newDataItem[hasChildrenField.value] = false;
                // newDataItem[layerField.value] = currentDataItem[layerField.value];
                newDataItem[parentIdField.value] = parentNode.id;
                originalData.value.splice(indexToInsert, 0, newDataItem);
            }
        }
    }

    function insertChild(targetIndex: number, newDataItem: any, originalData: Ref<any[]>) {
        const currentDataItem = originalData.value[targetIndex];
        const currentDataItemId = currentDataItem[idField.value];
        const currentTreeNode = hierarchyGraph.get(currentDataItemId) as HierarchyGraphNode;
        if (currentTreeNode) {
            const currentIndexInOriginalData = originalData.value.findIndex((item: any) => item[idField.value] === currentDataItemId);
            const indexToInsert = currentIndexInOriginalData + 1;
            currentDataItem[hasChildrenField.value] = true;
            currentDataItem[collapseField.value] = false;
            newDataItem[hasChildrenField.value] = false;
            // newDataItem[layerField.value] = currentDataItem[layerField.value] + 1;
            newDataItem[parentIdField.value] = currentDataItemId;
            originalData.value.splice(indexToInsert, 0, newDataItem);
        }
    }

    /** 删除树节点后，将剩余节点排序，重新赋id及parentId */
    function trimmedOriginalData(itemToTrim: any, originalData: any[]) {
        const dataItemIdToTrim = itemToTrim[idField.value];
        const currentTreeNode = hierarchyGraph.get(dataItemIdToTrim) as HierarchyGraphNode;
        const trimmedChilds = Array.from(currentTreeNode.descendant.keys());
        trimmedChilds.unshift(dataItemIdToTrim);
        const parentTreeNode = hierarchyGraph.get(currentTreeNode.parentId) as HierarchyGraphNode;
        if (parentTreeNode) {
            parentTreeNode.children.delete(dataItemIdToTrim);
            if (parentTreeNode.children.size === 0) {
                const parentDataItem = originalData.find((item: any) => item[idField.value] === currentTreeNode.parentId);
                parentDataItem[hasChildrenField.value] = false;
                parentDataItem[collapseField.value] = false;
            }
        }
        // 利用parent的循环，将所有多余值删除
        const trimmedOriginalData = originalData.filter((dataItem: any) => trimmedChilds.indexOf(dataItem.id) === -1);
        return trimmedOriginalData;
    }

    return { generateHierarchyData, hasRealChildren, insertSibling, insertChild, shouldStratifyData, trimmedOriginalData };
}
