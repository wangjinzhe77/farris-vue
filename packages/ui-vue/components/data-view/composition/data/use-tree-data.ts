import { DataViewOptions } from "../types";

export function useTreeData(props: DataViewOptions) {
    const parentField = 'parent';
    const layerField = 'layer';
    const hasChildrenField = 'hasChildren';
    const flatTreeData = (treeData?: Array<any>, layer = 1, flattenedData: any[] = [], parentId = null) => {
        treeData = treeData || [];
        treeData.reduce((result: any[], node: any) => {
            node.id = node.id || node.data[props.idField];
            node[parentField] = parentId;
            node[layerField] = layer;
            node[hasChildrenField] = false;
            // 兼容内部treeNode 合并node和node.data属性
            Object.keys(node.data).forEach((key: string) => {
                const nodeKeys = Object.keys(node);
                if (!nodeKeys.includes(key)) {
                    node[key] = node.data[key];
                }
            });
            flattenedData.push(node);
            if (node.children && node.children.length) {
                node[hasChildrenField] = true;
                flatTreeData(node.children, layer + 1, flattenedData, node.id);
            }
            return result;
        }, flattenedData);
        return flattenedData;
    };

    return { flatTreeData };
}
