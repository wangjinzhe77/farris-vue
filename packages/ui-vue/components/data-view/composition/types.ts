/* eslint-disable no-use-before-define */
import { ComputedRef, ExtractPropTypes, PropType, Ref, VNode } from "vue";
import { Condition } from "../../condition";
import { EditorConfig, EditorType } from '../../dynamic-form';
import { ButtonSize } from "../../button";

export interface CollpasableItem {
    collapse: boolean;
}

export type FilterRelation = 0 | 1;

export type FilterFunction = (dataItem: any) => boolean;

export type SortType = 'asc' | 'desc' | 'none';

export interface DataColumnCommand {
    type: ButtonSize;
    text: string;
    command?: string;
    icon?: string;
    hidden?: boolean;
    size: string;
    onClick: (e: MouseEvent, dataIndex: number, visualDataRow: VisualData) => void;
}

export interface DataColumn {
    // 字段
    field: string;
    // 字段类型
    dataType: string;
    // 标题
    title: string;
    /** 记录原始定义宽度 */
    actualWidth?: number;
    /** 文本对齐方式 */
    align?: 'left' | 'center' | 'right';
    /** 允许分组，默认为 true */
    allowGrouping?: boolean;
    /** 列合并 */
    colspan?: number;
    /** 操作列命令 */
    commands?: DataColumnCommand[];
    /** 列编辑器 */
    editor?: EditorConfig;
    /** 允许筛选列数据 */
    filterable?: boolean;
    /** 筛选条件 */
    filter?: any;
    /** 是否固定 */
    fixed?: 'left' | 'right';
    /** 标题对齐方式 */
    halign?: 'left' | 'center' | 'right';
    /** 唯一标识 */
    id?: string;
    /** 列索引 */
    index?: number;
    /** 是否多语字段 */
    isMultilingualField?: boolean;
    /** 横向坐标 */
    left?: number;
    /** 列合并原始值  */
    origianlColSpan?: number;
    /** 上级分组标识 */
    parentId?: any;
    /** 只读 */
    readonly?: boolean | any;
    /** 允许调整列宽 */
    resizable?: boolean;
    /** 行合并 */
    rowspan?: number;
    /** 超出是否展示省略号 */
    showEllipsis?: boolean;
    /** 显示列配置按钮 */
    showSetting?: boolean;
    /** 鼠标移动至单元格后，显示悬浮消息 */
    showTips?: boolean;
    /** 排序方式 */
    sort?: SortType;
    /** 允许排序列数据 */
    sortable?: boolean;
    /** 排序函数 */
    sorter?: (preValue: any, postValue: any, sortType: SortType) => number;
    /** 排序列顺序 */
    sortOrder?: number;
    /** 单元格提示模式：
     *  allways： 鼠标滑过即显示
     *  auto: 单元格宽度不够时才会显示
     */
    tipMode?: 'allways' | 'auto';
    /** 垂直对齐方式 */
    valign?: 'top' | 'middle' | 'bottom';
    /** 是否显示 */
    visible?: boolean | any;
    /** 列宽 */
    width?: number | string;
    /**  */
    formatter?: any;
    format?: (cell: VisualDataCell, visualDataRow: VisualData) => VNode | string;
    binding?: any;
}

export interface DataFilter {
    relation: FilterRelation;
    filterFunctions: FilterFunction[];
    children: DataFilter[];
}

export interface DataViewFilter {
    field: string;
    fieldName: string;
    fieldType: string;
    filterValue: string;
    filter: (dataItem: any) => boolean;
}

export interface DataViewSorter {
    field: string;
    compare: (preItem: any, postItem: any) => number;
}

export enum CellMode {
    readonly,
    editable,
    editing
}

export enum VisualDataStatus {
    initial,
    editing
}

export enum VisualDataType {
    data,
    group,
    summary
}

export interface VisualDataCell {
    /** 实际高度 */
    actualHeight?: number;
    /** 水平方向 */
    align: string | undefined;
    /** 单元格高度 */
    cellHeight?: number;
    /** 列合并数 */
    colSpan: number;
    /** 所属列 */
    column?: DataColumn;
    /** 单元格数据 */
    data: any;
    /** 编辑中数据 */
    editingData: any;
    /** 获取编辑器函数 */
    getEditor: (cell: VisualDataCell) => any;
    /** 单元格字段 */
    field: string;
    /** 单元格格式化函数 */
    // format?: (cell: VisualDataCell, visualDataRow: any) => any;
    formatter?: (cell: VisualDataCell, visualDataRow: any) => any;
    /** 单元格索引 */
    index: number;
    /** 单元格编辑模式 */
    mode: CellMode;
    /** 引用 */
    ref?: any;
    /** 行合并数 */
    rowSpan: number;
    /** 设置引用函数 */
    setRef: (vnode: any) => void;
    /** 是否展示省略号, 默认展示 */
    showEllipsis?: boolean;
    /** 所属合并单元格 */
    spannedBy?: VisualDataCell;
    /** 所合并的单元格 */
    spanned?: VisualDataCell[];
    /** 父级节点 */
    parent: any;
    /** 垂直方向 */
    valign: string | undefined;
    /** 更新函数 */
    update: (value: any) => void;
    /** 接收变更数据 */
    accept: () => void;
    /** 撤销变更数据 */
    cancel: () => void;
    /** 单元格更新数据源 */
    updateData: (newDataItem: any) => void;
}

export interface VisualData extends CollpasableItem {
    /** 是否折叠 */
    collapse: boolean;
    /** 数据信息 */
    data: Record<string, VisualDataCell>;
    /** 详细信息 */
    details?: VisualDataCell[];
    /** 禁用 */
    disabled: boolean;
    /** 分组字段 */
    groupField?: string;
    /** 分组字段数据 */
    groupValue?: any;
    /** 层级 */
    layer: number;
    /** 显示顺序索引 */
    index: number;
    /** 数据索引 */
    dataIndex: number;
    /** 节点在数据容器内的Y坐标 */
    top: number;
    /** 节点类型 */
    type: VisualDataType;
    /** 引用 */
    ref?: any;
    /** 行高 */
    height?: number;
    /** 父节点索引 */
    parentIndex?: number;
    /** 前一个节点 */
    pre?: any;
    /** 刷新键 */
    refreshKey?: string;
    /** 设置引用函数 */
    setRef: (vnode: any) => void;
    /** 节点所处状态 */
    status: VisualDataStatus;
    /** 是否展示该节点 */
    visible: boolean;
    /** 是否被选中 */
    checked: boolean;
    /** 中间选中状态 */
    indeterminate: boolean;
    /** 原始数据 */
    raw: any;
    paddingBottom?: string;
    moving?: boolean;
    // 更新某一行的单列
    updateCell: (newDataItem: any, field: string) => any;
    // 更新某一行的多列
    updateCells: (newDataItem: any, fields: string[]) => any;
}

export const rowNumberOptions = {
    /** 显示行号 */
    enable: { type: Boolean, default: false },
    /** 行号列表头标题 */
    heading: { type: String, default: '序号' },
    /** 行号宽度，默认为 36px */
    width: { type: Number, default: 36 }
};

export interface RowNumberOptions {
    /** 显示行号 */
    enable: boolean;
    /** 行号列表头标题 */
    heading: string;
    /** 行号宽度，默认为 36px */
    width: number;
    /** 省略号 */
    showEllipsis: boolean;
};

export interface RowOptions {
    /** 自定义行样式 */
    customRowStyle: (dataItem: Record<string, any>) => Record<string, boolean>;
    /** 自定义单元格样式 */
    customCellStyle: (cell: VisualDataCell) => Record<string, boolean>;
    /** 自定义行状态 */
    customRowStatus: (visualDataRow: VisualData) => VisualData;
    /** 自定义行内容 */
    customRowContent: (visualDataRow: VisualData) => any[];
    /** 禁止行选中表达式 */
    disable: (dataItem: Record<string, any>) => boolean;
    // 禁用属性名
    disabledField: string;
    /** 默认行高度为 29px */
    height: number;
    /** 鼠标滑过行效果 */
    showHovering: boolean;
    /** 禁止数据折行 */
    wrapContent: boolean;
}

export type FocuseSelectionMode = 'current' | 'all';

export type HierarchySelectionRange = 'All' | 'OnlyChildren' | 'OnlyParent';

/**
 * 联动模式
 * 缺省状态下, 无联动设置, autoCheckChildren的值为false, autoCheckParent的值为false.
 * selectionRange - 选择结果范围的值为'All'.
 * 1.自动勾选子节点: 将autoCheckChildren属性设置为true后启用此特性, 此时勾选树节点后, 自动勾选该节点下的所有子节点.
 *   组件根据选择结果范围selectionRange的值, 更新已选记录结果, 控制树节点选择状态.
 *   1a.选择结果范围 - selectionRange的值为'All'时
 *      当前节点, 及其所有子节点, 既显示勾选状态, 也被添加入已选数据列表;
 *      勾选当前节点所有兄弟节点后, 保留当前已选结果, 根据autoCheckParent属性的值决定是否联动选择其父节点;
 *      勾选状态的所有节点均可以被反选, 反选子节点后不影响父节点勾选状态.
 *   1b.选择结果范围 - selectionRange的值为'OnlyChildren'时
 *      当前节点, 及其所有子节点, 显示勾选状态;
 *      如果当前节点包含子节点, 仅将不包括当前节点在内的所有子节点, 添加入已选数据列表;
 *      如果当前节点无子节点, 将当前节点添加入已选数据列表;
 *      勾选当前节点所有兄弟节点后, 保留所有子节点和无子节点的已选数据;
 *      根据autoCheckParent属性的值决定是否联动其父节点勾选状态, 但是不添加入已选数据列表;
 *      勾选状态的所有节点均可以被反选, 反选子节点后将其父节点置于中间状态.
 *   1c.选择结果范围 - selectionRange的值为'OnlyParent'时
 *      当前节点, 及其所有子节点, 显示勾选状态;
 *      如果当前节点包含子节点, 仅将当前节点, 不包括其子节点, 添加入已选数据列表;
 *      如果当前节点无子节点, 将当前节点添加入已选数据列表;
 *      勾选当前节点所有兄弟节点后, 根据autoCheckParent属性的值更新选择结果;
 *      勾选状态的所有节点的子节点不可以被反选.
 * 2.自动勾选父节点: 将autoCheckParent属性设置为true后启用此特性, 此时勾选树节点后, 自动将其父节点置于中间状态, 勾选所有兄弟节点后, 自动将其父节点置于勾选状态.
 *   组件根据选择结果范围selectionRange的值, 更新已选记录结果, 控制树节点选择状态.
 *   2a.选择结果范围 - selectionRange的值为'All'时
 *      将联动勾选的父节点添加入已选数据列表;
 *      勾选状态的所有节点均可以被反选, 反选子节点后不影响父节点勾选状态.
 *   2b.选择结果范围 - selectionRange的值为'OnlyChildren'时
 *      联动其父节点勾选状态, 但是不添加入已选数据列表;
 *      勾选状态的所有节点的子节点都可以被反选, 当反选父节点时, 清除其下所有子节点选择记录.
 *   2c.选择结果范围 - selectionRange的值为'OnlyParent'时
 *      将联动勾选的父节点添加入已选数据列表, 在已选记录中清除当前节点及其兄弟节点;
 *      勾选状态的所有节点的子节点不可以被反选.
 */
export interface CascadeOptions {
    /** 自动勾选子节点 */
    autoCheckChildren: boolean;
    /** 自动勾选父节点 */
    autoCheckParent: boolean;
    /** 选择结果范围 */
    selectionRange: HierarchySelectionRange;
}

export interface HierarchyOptions {
    cascadeOption: CascadeOptions;
    /** 收折状态字段 */
    collapseField: string;
    /** 拥有子节点标识 */
    hasChildrenField: string;
    /** 数据节点图标字段 */
    iconField: string;
    /** 级数字段 */
    // layerField: string;
    /** 父节点标识字段 */
    parentIdField: string;
};

/**
 * 多选模式
 * 启用多选数据特性后，可以与是否显示复选框配合，定制多种多选交互模式。
 * 1.仅使用复选框: 同时启用多选特性和显示复选框特性后，多选模式缺省为'DependOnCheck'，默认使用此模式。
 *   此时用户勾选复选框添加已选数据;
 *   焦点行高亮显示，其他已选数据不显示高亮背景;
 *   点击行时，仅切换焦点行，不改变已选状态。
 * 2.同时使用复选框和点击行: 同时启用多选特性和显示复选框特性，并将多选模式设置为'OnCheckAndClick'后，使用此模式。
 *   此时用户勾选复选框添加已选数据;
 *   高亮显示包括焦点行在内的所有已选数据;
 *   点击行时，被点击行获得焦点，同时添加入已选数据。
 * 3.仅点击行: 仅启用多选特性时，多选模式缺省为'DependOnCheck'，因为未显示复选框，默认使用此模式。
 *   此时用户按Shift+点击添加已选数据;
 *   高亮显示包括焦点行在内的所有已选数据;
 *   不按Shift键，仅点击行时，被点击行获得焦点并添加入已选数据，同时清空其他已选数据。
 */
export type MultiSelectMode = 'DependOnCheck' | 'OnCheckAndClick';

export interface SelectionOptions {
    /** 允许选中行 */
    enableSelectRow: boolean;
    /** 启用多选 */
    multiSelect: boolean;
    /** 多选模式 */
    multiSelectMode: MultiSelectMode;
    /** 每行前边显示 checkbox */
    showCheckbox: boolean;
    /** 显示全选checkbox */
    showSelectAll: boolean;
    /** 显示已选数据 */
    showSelection: boolean;
};

export interface CommandOptions {
    /** 启用操作列 */
    enable: boolean;
    /** 操作列命令 */
    commands: ColumnCommand[];
}

export interface ColumnCommand {
    type: string;
    text: string;
    command?: string;
    icon?: string;
    hidden?: boolean;
    onClick: (e: MouseEvent, dataIndex: number, visualDataRow: VisualData) => void;
}

export type InteractiveMode = 'server' | 'client';

export const paginationOptions = {
    /** 启用分页 */
    enable: { type: Boolean, default: false },
    /** 当前页码 */
    index: { type: Number, default: 1 },
    /** 分页交互模式 */
    mode: { type: String as PropType<InteractiveMode>, default: 'server' },
    /** 显示页码输入框 */
    showGoto: { type: Boolean, default: false },
    /** 显示页码 */
    showIndex: { type: Boolean, default: true },
    /** 显示每页记录数 */
    showLimits: { type: Boolean, default: false },
    /** 显示分页汇总信息 */
    showPageInfo: { type: Boolean, default: true },
    /** 默认每页记录数 */
    size: { type: Number, default: 20 },
    /** 可选择的没有记录数据 */
    sizeLimits: { type: Array<number>, default: [10, 20, 30, 50, 100] },
    /** 总记录数 */
    total: { type: Number, default: 0 },
    /** 禁用分页 */
    disabled: { type: Boolean, default: false }
};

export type PaginatonOptions = ExtractPropTypes<typeof paginationOptions>;

export interface DataViewOptions {
    /** 允许在最后一个单元回车新增一行 */
    appendOnEnterAtLastCell: boolean;
    /** 自动勾选子节点 */
    autoCheckChildren: boolean;
    /** 编辑单元格前事件 */
    beforeEditCell: boolean | ((context: { row: VisualData, cell: VisualDataCell, rawData: any, column: DataColumn }) => Promise<boolean> | boolean);
    /** 结束编辑前校验 */
    beforeEndEditCell: boolean | (() => Promise<boolean> | boolean);
    /** 单元格高度 */
    cellHeight: number;
    /** 变更策略 */
    changePolicy: string;
    /** 列集合 */
    columns: DataColumn[];
    /** 列配置 */
    columnOption: any;
    /** 操作列 */
    commandOption: CommandOptions;
    /** 被绑定数据 */
    data: any[];
    /** 用于显示信息的字段 */
    displayField: string;
    /** 禁用组件 */
    disabled: boolean;
    /** 允许编辑 */
    editable: boolean;
    /** 编辑配置 */
    editOption: any;
    fit: boolean;
    focusOnEditingCell: boolean;
    group: any;
    /** 高度 */
    height: number;
    /** 层级关系配置对象 */
    hierarchy: HierarchyOptions;
    iconField: string;
    /** 被绑定数据的标识字段 */
    idField: string;
    /** 连接线颜色 */
    lineColor: string;
    mergeCascadeValues: boolean;
    mergeCell: boolean;
    minHeight: number;

    minWidth: number;
    /** 新建数据 */
    newDataItem: () => any;
    /** 分页配置 */
    pagination: PaginatonOptions;
    /** 行号配置 */
    rowNumber: RowNumberOptions;
    /** 行配置 */
    rowOption: RowOptions;
    /** 选择配置 */
    selection: SelectionOptions;
    /** 已选数据标识 */
    selectionValues: string[];
    selectOnEditingCell: boolean;
    /** 显示列标题 */
    showHeader: boolean;
    /** 显示横向行分割线 */
    showHorizontalLines: boolean;
    /** 是否显示连接线 */
    showLines: boolean;
    /** 显示设置按钮 */
    showSetting: boolean;
    /** 显示树节点图标 */
    showTreeNodeIcons: boolean;
    /** 显示纵向列分割线 */
    showVerticallLines: boolean;
    summary: any;
    treeNodeIconsData: any;
    width: number;
    /** 虚拟化渲染数据 */
    virtualized: boolean;
}

export interface UseDataView {
    addFilter: (filterKey: string, filter: DataViewFilter) => any[];

    addNewDataItem: (newDataItem: any) => any;

    addSorter: (sortKey: string, sorter: DataViewSorter) => any[];

    changePageSizeTo: (newPageSize: number) => void;

    collapse: (collapseField: string, collapseValue: any) => any[];

    dataView: Ref<any[]>;

    editDataItem: (editIndex: string | number, newName: string) => void;

    expand: (expandField: string, expandValue: any) => any[];

    filters: Ref<DataViewFilter[]>;

    fold: (visualData: VisualData) => void;

    getRange: (filters: DataViewFilter[], start: number, end: number) => any[];

    getSelectionItems: (selectionValues: string[]) => any[];

    hasRealChildren: (rawDataItem: any) => boolean;

    insertNewChildDataItem: (targetIndex?: number) => void;

    insertNewDataItem: (targetIndex?: number) => void;

    load: (newData: Record<string, any>[]) => void;

    navigatePageTo: (pageIndex: number) => void;

    pageIndex: Ref<number>;

    rawView: Ref<any[]>;

    refresh: () => void;

    removeAllFilter: () => any[];

    removeDataItem: (dataIndex: number) => void;

    removeDataItemById: (dataItemId: string) => void;

    removeFilter: (filterKey: string) => any[];

    removeHierarchyDataItem: (deleteIndex: number) => any;

    removeSorter: (sortKey: string) => any[];

    reOrderVisibleIndex: () => void;

    setSorters: (sorters: DataViewSorter[]) => void;

    sorters: Ref<DataViewSorter[]>;

    summaries: Map<string, number>;

    totalItems: Ref<number>;

    unFold: (visualData: VisualData) => void;

    updateDataView: () => void;

    visibleDataItems: ComputedRef<any[]>;
}

export interface UseGroupData {
    collpaseGroupIconClass: (groupRow: CollpasableItem) => Record<string, boolean>;

    generateGroupData: (groupFields: string[], rawData: any[], columnMaps: Map<string, any>) => any[];

    groupFields: Ref<string[]>;

    shouldGroupingData: ComputedRef<boolean>;

    showGroupPanel: ComputedRef<boolean>;
}

export interface UseFilter {

    apply: (dataItem: any) => boolean;

    clearCondition: () => void;

    conditions: Ref<Condition[]>;

    removeCondition: (id: string) => void;

    addColumnFilter: (headerCell: HeaderCell) => void;

    getFilterEditorType: (column: DataColumn) => EditorType;

    removeColumnFilter: (headerCell: HeaderCell) => void;

}

export interface UseVisualDataBound {

    updateRowPosition: (visualData: VisualData, dataItem: any) => void;

    updateVisualInfomation: (vnode: any, targetCell: VisualDataCell, dataItem: any) => void;
}

export interface UseVisualDataCell {


    createCellByColumn: (column: DataColumn, index: number, dataItem: any, parent: VisualData, preVisualData: VisualData) => VisualDataCell;


    createCellByField: (targetField: string, mode: CellMode, index: number, dataItem: any, parent: VisualData, colSpan?: number) => VisualDataCell;
}

export interface UseEdit {
    getEditor: (cell: VisualDataCell, column: DataColumn, visualDataRow: VisualData) => any;

    onClickCell: (payload: MouseEvent, cell: VisualDataCell, row: VisualData, column: DataColumn) => any;

    onEditingRow: (visualDataRow: VisualData) => any;

    acceptEditingRow: (visualDataRow: VisualData) => any;

    cancelEditingRow: (visualDataRow: VisualData) => any;

    getEditingSnapshot: (dataIdentify: string) => VisualData | null;

    endEditCell: () => any;
}

export interface UseCellEditor {
    getEditor: (cell: VisualDataCell, column: DataColumn, visualDataRow: VisualData) => any;
}


export type VisualDataRender = (dataItem: any, preDataItem: any, preRow: VisualData, rowIndex: number, top: number, columns: DataColumn[], visibleDatas?: VisualData[]) => VisualData;

export interface UseVisualDataRow {

    createEmptyRow: (type: VisualDataType, index: number, dataItem: any, pre: any, preRow: VisualData, top: number) => VisualData;


    createNewRowFromDataItem: (columns: DataColumn[], dataItem: any, preDataItem: any, preRow: VisualData, rowIndex: number, top: number) => VisualData;


    renderDataRow: VisualDataRender;
}

export interface UseGroupRow {
    renderGroupRow: VisualDataRender;
}

export interface UseSummaryRow {
    renderSummaryRow: VisualDataRender;
}

export interface UseVisualData {
    getVisualData: (start: number, end: number, pre?: VisualData, forceToRefresh?: boolean) => VisualData[];

    maxVisibleRowIndex: Ref<number>;

    minVisibleRowIndex: Ref<number>;

    toggleGroupRow: (status: 'collapse' | 'expand', groupRow: VisualData, visibleDatas: VisualData[]) => VisualData[];
}

export interface UseVirtualScroll {
    dataGridWidth: Ref<number>;

    fitHorizontalScroll: () => void;

    gridDataStyle: ComputedRef<Record<string, any>>;

    gridHeaderColumnsStyle: ComputedRef<Record<string, any>>;

    gridMergedDataStyle: ComputedRef<Record<string, any>>;

    gridSideStyle: ComputedRef<Record<string, any>>;

    horizontalScrollThumbStyle: ComputedRef<Record<string, any>>;

    leftFixedGridDataStyle: ComputedRef<Record<string, any>>;

    leftFixedGridHeaderColumnsStyle: ComputedRef<Record<string, any>>;

    leftFixedGridMergedDataStyle: ComputedRef<Record<string, any>>;

    offsetX: Ref<number>;

    offsetY: Ref<number>;

    onMouseDownScrollThumb: ($event: MouseEvent, gridContentRef: Ref<any>, thumbType: 'vertical' | 'horizontal') => void;

    onWheel: (payload: WheelEvent) => void;

    reCalculateVisualDataRows: (forceToRefresh?: boolean) => void;

    resetScroll: () => void;

    rightFixedGridDataStyle: ComputedRef<Record<string, any>>;

    rightFixedGridHeaderColumnsStyle: ComputedRef<Record<string, any>>;

    rightFixedGridMergedDataStyle: ComputedRef<Record<string, any>>;

    shouldShowHorizontalScrollbar: ComputedRef<boolean>;

    shouldShowVirticalScrollbar: ComputedRef<boolean>;

    updateVirticalScroll: () => void;

    updateVisibleRowsOnLatestVisibleScope: () => void;

    verticalScrollThumbStyle: ComputedRef<Record<string, any>>;

    viewPortHeight: Ref<number>;

    viewPortWidth: Ref<number>;
}

export interface ColumnContext {
    defaultColumnWidth: number;
    headerDepth: number;
    leftColumns: DataColumn[];
    leftColumnsMap: Map<string, DataColumn>;
    leftColumnsWidth: number;
    leftHeaderColumns: HeaderCell[];
    primaryColumns: DataColumn[];
    primaryColumnsMap: Map<string, DataColumn>;
    primaryColumnsWidth: number;
    primaryHeaderColumns: HeaderCell[];
    rightColumns: DataColumn[];
    rightColumnsMap: Map<string, DataColumn>;
    rightHeaderColumns: HeaderCell[];
    rightColumnsWidth: number;
    summaryColumns: DataColumn[];
}

export interface UseFitColumn {
    calculateColumnHeaders: (context: Ref<ColumnContext>) => void;

    calculateColumnsSize: () => void;

    calculateColumnsWidth: (context: Ref<ColumnContext>) => void;

}

export interface UseGroupColumn {
    getGridHeaderCells: (columns: DataColumn[]) => Map<string, HeaderCell>;
}

export enum HeaderCellStatus {
    none = 0,
    filterable = 1,
    sortable = 2,
    filtered = 4,
    sorted = 8,
    ascending = 16,
    descending = 32
}

export interface HeaderCell {
    actualWidth: number;
    children: HeaderCell[];
    depth: number;
    layer: number;
    left: number;
    field: string;
    parent: HeaderCell | null;
    resizable: boolean;
    title: string;
    status: HeaderCellStatus;
    popoverRef: Ref<any>;
    showPopover: boolean;
    column: DataColumn | null;
    filter?: any;
    filterHistory?: any[];
    sortType: SortType;
    showSetting?: boolean;
}

export interface ColumnGroupItem {
    field: string;
    title?: string;
    group?: (ColumnGroupItem | string)[];
}

export interface UseSort {

    getSorterByColumn: (column: DataColumn) => any;

    updateSorter: (headerCell: HeaderCell, dataView: UseDataView) => void;
}

export interface UseColumn {
    applyColumnSorter: (dataView: UseDataView, sorterComposition: UseSort) => void;

    applySortableColumns: (sortableColumns: DataColumn[], dataView: UseDataView, sorterComposition: UseSort) => void;

    collectionFilterableColumns: () => DataColumn[];

    collectionSortableColumns: () => DataColumn[];

    columnContext: Ref<ColumnContext>;

    hasLeftFixedColumn: ComputedRef<boolean>;

    hasRightFixedColumn: ComputedRef<boolean>;

    updateColumnRenderContext: (columns: DataColumn[]) => void;

    updateColumnSettingIcon: () => void;
}

export interface UseDragColumn {
    dragstart: (e: DragEvent, item: any, index: number) => void;

    dragenter: (e: DragEvent, index: number) => void;

    dragover: (e: DragEvent, index: number) => void;

    dragend: (e: DragEvent, item: any) => void;

    dropOnGroupPanel: (e: DragEvent) => void;

    isDragging: Ref<boolean>;

    groupColumnItems: ComputedRef<{ name: string; value: string }[]>;
}

export interface UseFilterHistory {
    getFilterHistory: (headerCell: HeaderCell) => any[];

    setFilterHistory: (headerCell: HeaderCell, filterHistory: any[]) => void;

    removeFilterHistory: (headerCell: HeaderCell, filterString: string) => void;

    updateFilterHistory: (headerCell: HeaderCell, filterString: string) => void;
}

export interface UseColumnFilter {

    getFilterEditor: (headerCell: HeaderCell) => any;

}

export interface UseResize {
    onClickColumnResizeBar: ($event: MouseEvent, columnField: string) => any;

    resizeHandleStyle: Ref<Record<string, any>>;

    resizeOverlayStyle: Ref<Record<string, any>>;
}

export interface UseSidebar {

    showRowNumer: Ref<boolean>;

    showSidebarCheckBox: Ref<boolean>;

    sidebarTitle: ComputedRef<string>;

    sidebarWidth: ComputedRef<number>;

    sidebarCellPosition: (dataItem: VisualData) => Record<string, any>;

    sidebarCornerCellStyle: ComputedRef<Record<string, any>>;

    cellKey: (dataItem: VisualData) => string;

    rowKey: (dataItem: VisualData) => string;
}

export interface UseRow {
    gridCellClass(cell: VisualDataCell): Record<string, any>;

    gridRowClass(dataItem: VisualData): Record<string, any>;

    sidebarRowClass(dataItem: VisualData): Record<string, any>;

    onClickRow: ($event: MouseEvent, dataItem: VisualData) => any;

    onMouseoverRow: ($event: MouseEvent, dataItem: VisualData) => any;

    onMouseoutRow: ($event: MouseEvent, dataItem: VisualData) => any;

    clickRowItem: (dataItem: VisualData) => void;
}

export interface UseSelection {
    currentSelectedDataId: Ref<string>;

    getSelectionRow: () => VisualData;

    // setSelectionRow: (visibleDatas: Ref<VisualData[]>, selectedDataId: string) => void;

    clearSelection(): void;

    findIndexInSelectedItems(item: any): number;

    // selectedItems: Ref<any[]>;

    getSelectedItems: () => any[];

    selectedValues: Ref<string[]>;

    toggleSelectItem(visualData: VisualData): void;

    toggleSelectItemWithoutRow(visualData: VisualData): void;

    selectItem(visualData: VisualData): void;

    selectItemById(dataItemId: string): void;

    selectItemByIds(dataItemIds: string[]): void;

    showCheckBox: Ref<boolean>;

    showSelectAll: Ref<boolean>;

    enableSelectRow: Ref<boolean>;

    enableMultiSelect: Ref<boolean>;

    multiSelectMode: Ref<MultiSelectMode>;

    select: (visualDataToBeSelected: VisualData) => void;

    selectAll: () => void;

    unSelect: (visualDataToBeSelected: VisualData) => void;

    unSelectAll: () => void;

    indeterminate: (visualDataToBeSelected: VisualData) => void;

    isSelectingHirarchyItem: ComputedRef<boolean>;

    multiSelectOnClickRow: ComputedRef<boolean>;

    multiSelectOnClickRowWithShift: ComputedRef<boolean>;

    multiSelectOnlyOnCheck: ComputedRef<boolean>;
}

export interface UseSelectHierarchyItem {

    getNextSelectableHierarchyItemId(visualDataIndex: number): string | null;

    toggleSelectHierarchyItem(visualData: VisualData): void;
}

export interface UseCellPosition {
    calculateCellPositionInRow: (columns: DataColumn[]) => Record<string, { left: number; width?: number }>;

    cellKey: (dataItem: VisualData, columnIndex: number) => string;

    cellPosition: (cell: VisualDataCell, cellPositionMap: Record<string, { left: number; width?: number }>) => Record<string, any>;

    rowKey: (dataItem: VisualData) => string;

    rowPosition: (dataItem: VisualData) => Record<string, any>;

    rowSpanCellPosition: (
        visualDataItem: VisualData,
        cell: VisualDataCell,
        cellPositionMap: Record<string, { left: number; width?: number }>
    ) => Record<string, any>;

    groupCellPosition: (
        groupCell: VisualDataCell,
        cellPositionMap: Record<string, { left: number; width?: number }>,
        layer: number
    ) => Record<string, any>;

    summaryCellPosition: (
        groupCell: VisualDataCell,
        cellPositionMap: Record<string, { left: number; width?: number }>,
        layer: number
    ) => Record<string, any>;
}

export interface UseCellContent {
    cellContentClass: (cell: VisualDataCell) => Record<string, boolean>;
    cellContentPosition: (cell: VisualDataCell) => Record<string, any>;
    cellContentClassWithEllipsis: (cell: VisualDataCell) => Record<string, any>;
}

export interface DictTreeItem {
    id: number;
    name: string;
    parents: Map<number, boolean>;
    childs: number[];
    childsLength: number;
    neighborParentId: number;
    neighborChilds: number[];
    childWithLines: number[];
}

export interface UseDictTree {
    generateDictTree: (visibleDatasValue: VisualData[]) => DictTreeItem[];
}

export interface UseHierarchyData {

    generateHierarchyData: (rawData: any[]) => any[];

    hasRealChildren: (rawDataItem: any) => boolean;

    insertSibling: (targetIndex: number, newDataItem: any, originalData: Ref<any[]>) => void;

    insertChild: (targetIndex: number, newDataItem: any, originalData: Ref<any[]>) => void;

    shouldStratifyData: Ref<boolean>;

    trimmedOriginalData: (itemToTrim: any, originalData: any) => any[];

}

export interface UseHierarchy {

    collapseField: ComputedRef<string>;

    hasChildrenField: ComputedRef<string>;

    // layerField: Ref<string>;

    parentIdField: ComputedRef<string>;

    shouldShowCheckBox: ComputedRef<boolean>;

    shouldShowIcon: ComputedRef<boolean>;

}

export interface UseToggleHierarchyItem {
    toggleTreeNode(visualTreeNode: VisualData): void;
}

export interface HierarchyGraphNode {
    /** 树节点标识 */
    id: string;
    /** 树节点索引 */
    index: number;
    /** 当前父节点标识 */
    parentId: string;
    /** 当前父节点索引 */
    parentIndex: number;
    /** 祖先节点集合 */
    parents: Map<string, HierarchyGraphNode>;
    /** 子节点集合 */
    children: Map<string, HierarchyGraphNode>;
    /** 后代节点集合 */
    descendant: Map<string, HierarchyGraphNode>;
}

export interface UseDataViewContainerStyle {

    containerStyleObject: ComputedRef<Record<string, any>>;

}

export interface UseCommandColumn {
    applyCommands: (columns: Ref<DataColumn[]>) => void;
}

export interface UseCascade {

    autoCheckChildren: Ref<boolean>;

    autoCheckParent: Ref<boolean>;

    interactAfterCheckHierarchyItem: (selectionRecords: any[], dataItem: any) => any[];
}

export interface UseIdentify {

    idField: Ref<string>;

    reviseIdentifyField: (rawData: any[]) => void;
}

export interface UseNavigation {
    navigateOnKeyUp: (payload: KeyboardEvent, currentCell: VisualDataCell) => void;
}

export interface UsePagination {
    disabled: Ref<boolean>;
    pageSize: Ref<number>;
    pageList: Ref<number[]>;
    shouldRenderPagination: Ref<boolean>;
    showGotoPage: Ref<boolean>;
    showPageIndex: Ref<boolean>;
    showPageList: Ref<boolean>;
    mode: Ref<string>;
    updatePagination: (pageInfo: Partial<PaginatonOptions>) => void;
}

export const enum ColumnFormatterDataType {
    NONE = 'none',
    NUMBER = 'number',
    DATE = 'date',
    TEXT = 'boolean',
    IMAGE = 'boolean2',
    ENUM = 'enum',
    CUSTOM = 'custom'
};

export interface UseData {
    rawData: Ref<Record<string, any>[]>;
    updateData: (data: Record<string, any>[]) => void;
}

export interface HierarchyItem {
    data: Record<string, any>;
    [propName: string]: any;
    children?: HierarchyItem[];
    expanded?: boolean;
    selectable?: boolean;
}
