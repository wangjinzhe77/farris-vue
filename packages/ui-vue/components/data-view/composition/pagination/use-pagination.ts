import { computed, ref, watch } from 'vue';
import { PaginatonOptions, UseData, UseDataView, UsePagination } from '../types';
import { DataGridProps } from '../../../data-grid';

export function isUndefined(value: any): value is undefined {
    return typeof value === 'undefined';
}
export function usePagination(
    props: DataGridProps,
    dataView: UseDataView
): UsePagination {
    const { pageIndex, totalItems} = dataView;
    // 启用分页开关
    const shouldRenderPagination = ref(props.pagination?.enable);
    // 禁用分页
    const disabled = ref(props.pagination?.disabled || false);
    // 每页数量
    const pageSize = ref(props.pagination?.size);
    // 每页数量列表
    const pageList = ref(props.pagination?.sizeLimits);
    // 分页模式
    const mode = ref(props.pagination?.mode);
    // 是否展示页码
    const showPageIndex = ref(props.pagination?.showIndex);
    // 是否展示每页数量
    const showPageList = ref(props.pagination?.showLimits);

    const showGotoPage = ref(props.pagination?.showGoto || false);

    watch(() => props.pagination?.disabled, (newValue: boolean, oldValue: boolean) => {
        if (newValue !== oldValue) {
            disabled.value = newValue;
        }
    });

    watch(() => props.pagination?.sizeLimits, (newValue, oldValue) => {
        pageList.value = newValue;
    });

    watch(() => props.pagination?.showIndex, (newValue, oldValue) => {
        if (newValue !== oldValue) {
            showPageIndex.value = newValue;
        }
    });

    watch(() => props.pagination?.showLimits, (newValue, oldValue) => {
        if (newValue !== oldValue) {
            showPageList.value = newValue;
        }
    });

    watch(() => props.pagination?.total, (newValue, oldValue) => {
        if (newValue !== oldValue) {
            totalItems.value = newValue;
        }
    });

    watch(() => props.pagination?.enable, (newValue, oldValue) => {
        if (newValue !== oldValue) {
            shouldRenderPagination.value = newValue;
        }
    });

    watch(() => props.pagination?.showGoto, (newValue, oldValue) => {
        if (newValue !== oldValue) {
            showGotoPage.value = newValue;
        }
    });

    const updatePagination = (pageInfo: Partial<PaginatonOptions>) => {
        if (!pageInfo) {
            return;
        }
        const { enable, total: pageInfoTotal, size, index, sizeLimits, showGoto, showIndex, showLimits, mode: pageMode, disabled: pageInfoDisabeld } = pageInfo;
        if (!isUndefined(enable)) {
            shouldRenderPagination.value = enable;
        }
        if (!isUndefined(index)) {
            pageIndex.value = index;
        }
        if (!isUndefined(size)) {
            pageSize.value = size;
        }
        if (!isUndefined(sizeLimits)) {
            pageList.value = sizeLimits;
        }
        if (!isUndefined(showGoto)) {
            showGotoPage.value = showGoto;
        }
        if (!isUndefined(showIndex)) {
            showPageIndex.value = showIndex;
        }
        if (!isUndefined(showLimits)) {
            showPageList.value = showLimits;
        }
        if (!isUndefined(pageMode)) {
            mode.value = pageMode;
            // pageInfoMode.value = pageMode;
        }
        if (!isUndefined(pageInfoTotal)) {
            totalItems.value = pageInfoTotal;
        }
        if (!isUndefined(pageInfoDisabeld)) {
            disabled.value = pageInfoDisabeld;
        }
    };
    return {
        disabled,
        pageSize,
        pageList,
        shouldRenderPagination,
        showGotoPage,
        showPageIndex,
        showPageList,
        mode,
        updatePagination
    };
}
