/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, ref, watch } from 'vue';
import { DataViewOptions, RowNumberOptions, UseSelection, UseSidebar, VisualData } from './types';

export function useSidebar(props: DataViewOptions, useSelectionCompostion: UseSelection): UseSidebar {
    const timeStamp = String(Date.now());
    // const defaultCheckboxWidth = 24;
    const defaultCheckboxWidth = 50;
    const { showCheckBox, showSelectAll } = useSelectionCompostion;
    const showRowNumer = ref(props.rowNumber?.enable ?? false);
    const showSidebarCheckBox = computed(() => {
        const showNodeIconCheckBox = props.hierarchy && showCheckBox.value &&
            props.selection.enableSelectRow && props.selection.multiSelect;
        return showNodeIconCheckBox ? false : showCheckBox.value;
    });

    const rowNumberWidth = ref(showRowNumer.value ? props.rowNumber?.width ?? 32 : 0);
    const checkboxWidth = ref(showSidebarCheckBox.value ? defaultCheckboxWidth : 0);

    const sidebarTitle = computed(() => showRowNumer.value ? props.rowNumber?.heading ?? '序号' : '');
    const sidebarWidth = computed(() => {
        return ((showSidebarCheckBox.value && !props.hierarchy) ? Number(checkboxWidth.value) : 0)
            + (showRowNumer.value ? Number(rowNumberWidth.value) : 0);
    });

    watch(() => props.rowNumber, (rowNumberOptions: RowNumberOptions) => {
        if (rowNumberOptions && rowNumberOptions.enable !== undefined) {
            showRowNumer.value = rowNumberOptions.enable;
        }
        if (rowNumberOptions && rowNumberOptions.width !== undefined) {
            rowNumberWidth.value = rowNumberOptions.width;
        }
    });

    function sidebarCellPosition(dataItem: VisualData): Record<string, any> {
        const styleObject = {
            top: `${dataItem.top}px`,
            width: `${sidebarWidth.value}px`,
            height: `${dataItem.height || ''}px`
        } as Record<string, any>;
        return styleObject;
    }

    const sidebarCornerCellStyle = computed(() => {
        const styleObject = {
            width: `${sidebarWidth.value}px`
        } as Record<string, any>;
        return styleObject;
    });

    function cellKey(dataItem: VisualData) {
        return `${timeStamp}_${dataItem.type}_r_${dataItem.index}_${dataItem.refreshKey || ''}`;
    }

    function rowKey(dataItem: VisualData) {
        return `${timeStamp}_${dataItem.type}_r_${dataItem.index}_${dataItem.refreshKey || ''}`;
    }

    return { showRowNumer, showSidebarCheckBox, sidebarCellPosition, sidebarCornerCellStyle, sidebarTitle, sidebarWidth, rowKey, cellKey };
}
