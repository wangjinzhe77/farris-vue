import { Ref, SetupContext } from 'vue';
import { DataColumn, DataViewOptions, UseColumn, UseEdit, UseNavigation, VisualData, VisualDataCell } from './types';

export function useNavigation(
    props: DataViewOptions,
    context: SetupContext,
    dataGridComponentInstance,
    useColumnComposition: UseColumn,
    useEditComposition: UseEdit,
    visibleDatas: Ref<VisualData[]>
): UseNavigation {
    const { addNewDataItemAtLast } = dataGridComponentInstance;
    const { columnContext } = useColumnComposition;
    const { onClickCell } = useEditComposition;

    function navigateToNextCell(currentCell: VisualDataCell, payload: MouseEvent) {
        const currentColumnIndex = columnContext.value.primaryColumns.findIndex((column: DataColumn) => column.field === currentCell.field);
        const currentVisualDataRow = currentCell.parent as VisualData;
        const currentVisualDataRowIndex = currentVisualDataRow.index;
        const lastColumnIndex = columnContext.value.primaryColumns.length - 1;
        const lastVisualDataRowIndex = visibleDatas.value.length - 1;
        const atLastCell = currentColumnIndex === lastColumnIndex;
        const atLastRow = currentVisualDataRowIndex === lastVisualDataRowIndex;

        if (currentColumnIndex > -1 && !atLastCell) {
            const nextColumn = columnContext.value.primaryColumns[currentColumnIndex + 1];
            const nextCell = currentVisualDataRow.data[nextColumn.field];
            onClickCell(payload, nextCell, currentVisualDataRow, nextColumn);
        }
        if (atLastCell && !atLastRow) {
            const nextVisualDataRow = visibleDatas.value[currentVisualDataRowIndex + 1];
            const nextColumn = columnContext.value.primaryColumns[0];
            const nextCell = nextVisualDataRow.data[columnContext.value.primaryColumns[0].field];
            onClickCell(payload, nextCell, currentVisualDataRow, nextColumn);
        }
        if (atLastCell && atLastRow) {
            if (props.appendOnEnterAtLastCell) {
                addNewDataItemAtLast();
                const nextVisualDataRow = visibleDatas.value[currentVisualDataRowIndex + 1];
                const nextColumn = columnContext.value.primaryColumns[0];
                const nextCell = nextVisualDataRow.data[columnContext.value.primaryColumns[0].field];
                onClickCell(payload, nextCell, nextVisualDataRow, nextColumn);
            }
            context.emit('enterUpInLastCell');
        }
    }

    function navigateToPreviousCell(currentCell: VisualDataCell, payload: MouseEvent) {
        const currentColumnIndex = columnContext.value.primaryColumns.findIndex((column: DataColumn) => column.field === currentCell.field);
        const currentVisualDataRow = currentCell.parent as VisualData;
        const lastColumnIndex = columnContext.value.primaryColumns.length - 1;
        const notAtFirstCell = currentColumnIndex > 0 && currentColumnIndex <= lastColumnIndex;
        if (notAtFirstCell) {
            const nextColumn = columnContext.value.primaryColumns[currentColumnIndex - 1];
            const nextCell = currentVisualDataRow.data[nextColumn.field];
            onClickCell(payload as any, nextCell, currentVisualDataRow, nextColumn);
        }
    }

    function navigatoToNextRow(currentCell: VisualDataCell, payload: MouseEvent) {
        const currentColumnField = currentCell.field;
        const currentVisualDataRow = currentCell.parent as VisualData;
        const currentVisualDataRowIndex = currentVisualDataRow.index;
        const lastVisualDataRowIndex = visibleDatas.value.length - 1;
        const notAtLastRow = currentVisualDataRowIndex > -1 && currentVisualDataRowIndex < lastVisualDataRowIndex;
        if (notAtLastRow) {
            const nextVisualDataRow = visibleDatas.value[currentVisualDataRowIndex + 1];
            const nextCell = nextVisualDataRow.data[currentColumnField];
            onClickCell(payload as any, nextCell, nextVisualDataRow, nextCell.column as DataColumn);
        }
    }

    function navigateToPreviousRow(currentCell: VisualDataCell, payload: MouseEvent) {
        const currentColumnField = currentCell.field;
        const currentVisualDataRow = currentCell.parent as VisualData;
        const currentVisualDataRowIndex = currentVisualDataRow.index;
        const lastVisualDataRowIndex = visibleDatas.value.length - 1;
        const notAtFirstRow = currentVisualDataRowIndex > 0 && currentVisualDataRowIndex <= lastVisualDataRowIndex;
        if (notAtFirstRow) {
            const preVisualDataRow = visibleDatas.value[currentVisualDataRowIndex - 1];
            const preCell = preVisualDataRow.data[currentColumnField];
            onClickCell(payload as any, preCell, preVisualDataRow, preCell.column as DataColumn);
        }
    }

    function navigateOnKeyUp(payload: KeyboardEvent, currentCell: VisualDataCell) {
        const { key, shiftKey } = payload;

        const shouldNavigateToNextCell = key === 'Enter' && !shiftKey;
        if (shouldNavigateToNextCell) {
            navigateToNextCell(currentCell, payload as any);
            return;
        }

        const shouldNavigateToPreviousCell = shiftKey && key === 'Enter';
        if (shouldNavigateToPreviousCell) {
            navigateToPreviousCell(currentCell, payload as any);
            return;
        }

        const shouldNavigateToNextRow = key === 'ArrowDown';
        if (shouldNavigateToNextRow) {
            navigatoToNextRow(currentCell, payload as any);
            return;
        }

        const shouldNavigateToPreviousRow = key === 'ArrowUp';
        if (shouldNavigateToPreviousRow) {
            navigateToPreviousRow(currentCell, payload as any);
        }
    }

    return { navigateOnKeyUp };

}
