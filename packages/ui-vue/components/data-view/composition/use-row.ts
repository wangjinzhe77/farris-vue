/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref, ref, SetupContext } from 'vue';
import { DataColumn, DataViewOptions, UseIdentify, UseRow, UseSelection, VisualData, VisualDataCell, VisualDataType } from './types';

export function useRow(
    props: DataViewOptions,
    context: SetupContext,
    selectionCompostion: UseSelection,
    identifyComposition: UseIdentify
): UseRow {
    const { idField } = identifyComposition;
    const { currentSelectedDataId, multiSelectOnClickRow, multiSelectOnClickRowWithShift,
        selectItem, selectedValues, toggleSelectItem, enableSelectRow
    } = selectionCompostion;
    const clickTimes = ref(0);

    const hoverIndex = ref(-1);

    function shouldShowSelectedStatus(dataItem: VisualData) {
        return enableSelectRow.value && (!multiSelectOnClickRowWithShift.value ?
            dataItem.raw[idField.value] === currentSelectedDataId.value :
            selectedValues.value.includes(dataItem.raw[idField.value]));
    }

    function gridRowClass(dataItem: VisualData) {
        const classObject = {
            'fv-grid-row': dataItem.type === VisualDataType.data,
            'fv-grid-group-row': dataItem.type === VisualDataType.group,
            'fv-grid-summary-row': dataItem.type === VisualDataType.summary,
            'fv-grid-row-hover': dataItem.index === hoverIndex.value,
            'fv-grid-row-selected': shouldShowSelectedStatus(dataItem),
            'fv-grid-row-odd': dataItem.dataIndex % 2 > 0,
            'fv-grid-row-even': dataItem.dataIndex % 2 === 0,
            'fv-grid-disabled': dataItem.disabled
        } as Record<string, boolean>;
        if (props.rowOption?.customRowStyle) {
            const customClassObject = props.rowOption.customRowStyle(dataItem.raw) || {};
            return Object.assign({}, classObject, customClassObject);
        }
        return classObject;
    }

    // 单元格样式
    function gridCellClass(cell: VisualDataCell) {
        const cellClassObject = {
            'fv-grid-cell': true,
        } as Record<string, boolean>;
        if (props.rowOption?.customCellStyle) {
            const customClassObject = props.rowOption.customCellStyle(cell) || {};
            return Object.assign({}, cellClassObject, customClassObject);
        }
        return cellClassObject;
    }

    function raiseClickRowEvent(dataItem: VisualData) {
        clickTimes.value += 1;
        setTimeout(() => {
            if (clickTimes.value === 1) {
                context.emit('clickRow', dataItem.index, dataItem.raw);
                // console.log(`on click row: ${dataItem.index}`);
            }
            clickTimes.value = 0;
        }, 250);
        if (clickTimes.value > 1) {
            context.emit('doubleClickRow', dataItem.index, dataItem.raw);
            // console.log(`on double click row: ${dataItem.index}`);
            clickTimes.value = 0;
        }
    }

    function onClickRow($event: MouseEvent, dataItem: VisualData) {
        if (dataItem.disabled) {
            return;
        }
        const shouldToggleSelectRow = (multiSelectOnClickRowWithShift.value && $event.shiftKey) || multiSelectOnClickRow.value;
        shouldToggleSelectRow ? toggleSelectItem(dataItem) : selectItem(dataItem);
        raiseClickRowEvent(dataItem);
        // $event.stopPropagation();
    }

    // 点击某行数据操作
    function clickRowItem(dataItem: VisualData) {
        if (dataItem.disabled) {
            return;
        }
        currentSelectedDataId.value = dataItem.raw[idField.value];
        raiseClickRowEvent(dataItem);
    }

    function onMouseoverRow($event: MouseEvent, dataItem: VisualData) {
        // 此处有性能问题  暂时注释掉
        // hoverIndex.value = dataItem.index;
    }

    function onMouseoutRow($event: MouseEvent, dataItem: VisualData) {
        // 此处有性能问题  暂时注释掉
        // hoverIndex.value = -1;
    }

    function sidebarRowClass(dataItem: VisualData) {
        const classObject = {
            'fv-grid-sidebar-row': true,
            'fv-grid-sidebar-row-hover': dataItem.index === hoverIndex.value,
            'fv-grid-sidebar-row-selected': shouldShowSelectedStatus(dataItem),
            'fv-grid-sidebar-row-odd': dataItem.dataIndex % 2 > 0,
            'fv-grid-sidebar-row-even': dataItem.dataIndex % 2 === 0,
            'd-flex': true,
            'align-items-center': true
        } as Record<string, boolean>;
        if (props.rowOption?.customRowStyle) {
            const customClassObject = props.rowOption.customRowStyle(dataItem.raw) || {};
            return Object.assign({}, classObject, customClassObject);
        }
        return classObject;
    }

    return {
        clickRowItem,
        gridCellClass,
        gridRowClass,
        onClickRow,
        onMouseoverRow,
        sidebarRowClass,
        onMouseoutRow
    };
}
