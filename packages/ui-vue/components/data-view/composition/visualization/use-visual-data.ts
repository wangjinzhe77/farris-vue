
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref, ref } from 'vue';
import {
    DataColumn, DataViewOptions, UseVisualDataRow, UseDataView,
    UseGroupRow, UseSummaryRow, UseVisualData, VisualData,
} from '../types';

export function useVisualData(
    props: DataViewOptions,
    columns: Ref<DataColumn[]>,
    dataViewComposition: UseDataView,
    visibleCapacity: Ref<number>,
    preloadCount: number,
    useDataRowComposition: UseVisualDataRow,
    useGroupRowComposition?: UseGroupRow,
    useSummaryRowComposition?: UseSummaryRow
): UseVisualData {
    const maxVisibleRowIndex = ref(visibleCapacity.value - 1 + preloadCount);
    const minVisibleRowIndex = ref(0);
    const { renderDataRow } = useDataRowComposition;
    const renderGroupRow = useGroupRowComposition ? useGroupRowComposition.renderGroupRow : renderDataRow;
    const renderSummaryRow = useSummaryRowComposition ? useSummaryRowComposition.renderSummaryRow : renderDataRow;

    function getVisualDataRender(dataViewItem: any) {
        let visualDataRender = renderDataRow;
        if (dataViewItem.__fv_data_grid_group_row__) {
            visualDataRender = renderGroupRow;
        }
        if (dataViewItem.__fv_data_grid_group_summary__) {
            visualDataRender = renderSummaryRow;
        }
        return visualDataRender;
    }

    function getVisualData(start: number, end: number, pre?: any, forceToRefresh?: boolean): VisualData[] {
        const { visibleDataItems } = dataViewComposition;
        const visualDatas: VisualData[] = [];
        const itemsToVisualize = visibleDataItems.value;
        // 数据视图中有数据
        if (itemsToVisualize.length > 0) {
            // 是否强制刷新
            const refreshKey = forceToRefresh ? Date.now().toString() : '';
            for (let rowIndex = start, visualIndex = 0; rowIndex <= end; rowIndex++, visualIndex++) {
                // 当前数据项；
                const dataViewItem = itemsToVisualize[rowIndex];
                // 获取前一个数据项，如果没有则使用传入的前一个数据项；
                const preDataItem = itemsToVisualize[rowIndex - 1] || pre;
                // 获取前一个合并的可视化行
                const preRow = visualDatas[visualIndex - 1];
                // 计算目标位置，考虑前一个数据项的位置和高度
                const targetPosition = preDataItem ? (preDataItem.__fv_data_position__ || 0) + (preDataItem.__fv_data_height__ || 0) : 0;
                const renderCurrentRow = getVisualDataRender(dataViewItem);
                // 渲染当前行的可视化节点
                const currentRow = renderCurrentRow(dataViewItem, preDataItem, preRow, rowIndex, targetPosition, columns.value, visualDatas);
                // 设置刷新键
                currentRow.refreshKey = refreshKey;
                // 将可视化节点添加到数组中
                visualDatas.push(currentRow);
            }
        }
        minVisibleRowIndex.value = visualDatas.length > 0 ? visualDatas[0].index : 0;
        maxVisibleRowIndex.value = visualDatas.length > 0 ? visualDatas[visualDatas.length - 1].index : 0;
        return visualDatas;
    }

    function toggleGroupRow(status: 'collapse' | 'expand', groupRow: VisualData, visibleDatas: VisualData[]): VisualData[] {
        const groupField = groupRow.groupField || '';
        const { groupValue } = groupRow;
        dataViewComposition[status](groupField, groupValue);
        const { dataView } = dataViewComposition;
        const start = visibleDatas[0].index;
        const end = Math.min(start + visibleCapacity.value + preloadCount + 1, dataView.value.length - 1);
        const visualDatas: VisualData[] = getVisualData(start, end);
        return visualDatas;
    }

    return { getVisualData, maxVisibleRowIndex, minVisibleRowIndex, toggleGroupRow };
}
