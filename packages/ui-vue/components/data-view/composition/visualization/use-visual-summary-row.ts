import { ref } from "vue";
import {
    CellMode, DataColumn, DataViewOptions, UseIdentify, UseSummaryRow, UseVisualDataCell,
    UseVisualDataRow, VisualData, VisualDataType
} from "../types";

export function useVisualSummaryRow(
    props: DataViewOptions,
    useIdentifyComposition: UseIdentify,
    useVisualDataCell: UseVisualDataCell,
    useVisualDataRowComposition: UseVisualDataRow
): UseSummaryRow {
    const { idField } = useIdentifyComposition;
    const { createCellByField } = useVisualDataCell;
    const { createEmptyRow } = useVisualDataRowComposition;

    function renderSummaryRow(
        dataItem: any, preDataItem: any, preRow: VisualData,
        rowIndex: number, top: number, columns: DataColumn[],
        visibleDatas?: VisualData[]
    ): VisualData {
        const groupField = dataItem.__fv_data_grid_group_field__;
        const currentRow: VisualData = createEmptyRow(VisualDataType.summary, rowIndex, dataItem, preDataItem, preRow, top);
        currentRow.data[idField.value] = createCellByField(idField.value, CellMode.readonly, -1, dataItem, currentRow);
        currentRow.data[groupField] = createCellByField(groupField, CellMode.readonly, 1, dataItem, currentRow, columns.length);
        return currentRow;
    }

    return { renderSummaryRow };
}
