/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ColumnContext, DataViewOptions, UseDataView, UseVirtualScroll, UseVisualData, VisualData } from '../types';
import { throttle } from 'lodash-es';
import { computed, ref, Ref, watch } from 'vue';

export function useVirtualScroll(
    props: DataViewOptions,
    dataViewComposition: UseDataView,
    visibleDatas: Ref<VisualData[]>,
    columnContext: Ref<ColumnContext>,
    visualDataComposition: UseVisualData,
    visibleCapacity: Ref<number>,
    preloadCount: number,
    sidebarWidth: Ref<number>
): UseVirtualScroll {
    const { dataView, visibleDataItems } = dataViewComposition;
    const { getVisualData, maxVisibleRowIndex, minVisibleRowIndex } = visualDataComposition;
    const gridViewWidth = computed(() => columnContext.value.primaryColumnsWidth);
    const leftColumnsWidth = computed(() => columnContext.value.leftColumnsWidth);
    const rightColumnsWidth = computed(() => columnContext.value.rightColumnsWidth);
    const rowHeight = props.rowOption?.height || 28;
    const offsetY = ref(0);
    const offsetX = ref(0);
    let minViewPortRowIndex = 0;
    let maxViewPortRowIndex = visibleCapacity.value - 1;
    const dataGridWidth = ref(0);
    const viewPortHeight = ref(0);
    const viewPortWidth = ref(0);
    const usingVirtualScroll = ref(props.virtualized);
    const minThumbHeight = 24;
    const zoomFactorOfThumbScope = 1.5;

    let dragHandler: any;

    function getLatestGridViewHeight(): number {
        const visibleDataViewItems = visibleDataItems.value;
        if (visibleDataViewItems.length <= 0) {
            return 0;
        }
        const latestVisibleDataViewItem =
            maxVisibleRowIndex.value < visibleDataViewItems.length
                ? visibleDataViewItems[maxVisibleRowIndex.value]
                : visibleDataViewItems[visibleDataViewItems.length - 1];
        if (latestVisibleDataViewItem.__fv_data_position__ > visibleDataViewItems.length * rowHeight) {
            return visibleDataViewItems.length * rowHeight;
        }
        const gridViewHeight: number =
            (latestVisibleDataViewItem.__fv_data_position__ || 0) +
            (latestVisibleDataViewItem.__fv_data_height__ || 0) +
            Math.max(visibleDataViewItems.length - 1 - maxVisibleRowIndex.value, 0) * rowHeight;
        return gridViewHeight;
    }

    const scrollThumbHeight = computed(() => {
        const latestGridViewHeight = getLatestGridViewHeight();
        const visibleDataViewItems = visibleDataItems.value;
        let thumbScopeInScrollbar = visibleDataViewItems.length > visibleCapacity.value + preloadCount
            ? (visibleCapacity.value + preloadCount) / visibleDataViewItems.length
            : viewPortHeight.value / Math.max(latestGridViewHeight, viewPortHeight.value);
        let thumbHeight = Math.floor(thumbScopeInScrollbar * viewPortHeight.value);
        while (viewPortHeight.value > 0 && thumbHeight < minThumbHeight) {
            thumbScopeInScrollbar *= zoomFactorOfThumbScope;
            thumbHeight = Math.floor(thumbScopeInScrollbar * viewPortHeight.value);
        }
        return thumbHeight;
    });

    const scrollThumbWidth = computed(() => {
        return Math.floor((viewPortWidth.value / Math.max(gridViewWidth.value, viewPortWidth.value)) * dataGridWidth.value);
    });

    const topOfPostion = 0;
    const leftOfPostion = 0;
    const rightOfPosition = computed(() => {
        return dataGridWidth.value - (leftColumnsWidth.value + gridViewWidth.value + rightColumnsWidth.value + sidebarWidth.value);
    });

    function getVisibleRowIndexByPosition(topRowPosition: number): number {
        const visibleRowIndexPreTopRow = Math.floor(topRowPosition / rowHeight);
        const topVisibleRowIndex = visibleRowIndexPreTopRow + 1;
        const visibleDataViewItems = visibleDataItems.value;
        if (visibleDataViewItems.length > topVisibleRowIndex) {
            const matchedDataViewItem = visibleDataViewItems[topVisibleRowIndex];
            return matchedDataViewItem.__fv_visible_index__ - 1;
        }
        return 0;
    }

    function checkVisualizeDataScope(start: number, end: number, lastVisibleRowIndexInDataView: number) {
        let actualStart = start;
        let actualEnd = end;
        const visualizingDataRange = end - start + 1;
        if (visualizingDataRange < preloadCount + visibleCapacity.value + preloadCount) {
            const shortageOfData = preloadCount + visibleCapacity.value + preloadCount - visualizingDataRange;
            const cloudAppendBackward = end + shortageOfData <= lastVisibleRowIndexInDataView;
            const coludAppendForward = 0 + shortageOfData <= start;
            if (cloudAppendBackward) {
                actualEnd += shortageOfData;
                maxViewPortRowIndex = actualEnd;
            } else if (coludAppendForward) {
                actualStart -= shortageOfData;
                minViewPortRowIndex = actualStart;
            } else {
                const countCloudAppendBackward = lastVisibleRowIndexInDataView - end;
                const countCloudAppendForward = shortageOfData - countCloudAppendBackward;
                actualEnd = Math.min(end + countCloudAppendBackward, lastVisibleRowIndexInDataView);
                actualStart = Math.max(start - countCloudAppendForward, 0);
            }
        }
        return { actualStart, actualEnd };
    }

    function reCalculateVisualDataRows(forceToRefresh?: boolean) {
        const itemsToVisualize = visibleDataItems.value;
        if (!itemsToVisualize.length) {
            visibleDatas.value = [];
            return;
        }
        const start = props.pagination?.enable ? 0 : Math.max(minViewPortRowIndex - preloadCount, 0);
        const lastVisibleRowIndexInDataView = itemsToVisualize[itemsToVisualize.length - 1].__fv_visible_index__;
        const end = props.pagination?.enable
            ? itemsToVisualize.length - 1
            : Math.min(maxViewPortRowIndex + preloadCount, lastVisibleRowIndexInDataView);
        const { actualStart, actualEnd } = checkVisualizeDataScope(start, end, lastVisibleRowIndexInDataView);
        const preDataItem = itemsToVisualize[actualStart - 1];
        const visualData = getVisualData(actualStart, actualEnd, preDataItem, forceToRefresh);
        if (visualData.length) {
            visibleDatas.value = [...visualData];
        }
    }

    const shouldShowVirticalScrollbar = computed(() => {
        const latestGridViewHeight = getLatestGridViewHeight();
        return latestGridViewHeight > viewPortHeight.value;
    });

    const shouldShowHorizontalScrollbar = computed(() => {
        return Math.floor(gridViewWidth.value) > viewPortWidth.value;
    });

    function checkOffsetYForLatestGridViewHeight(originalOffsetYValue: number, latestGridViewHeight: number) {
        let offsetYValue = originalOffsetYValue;
        const onTopOfScrollbar = offsetYValue > topOfPostion;
        const shouldShowScrollBar = shouldShowVirticalScrollbar.value;
        if (shouldShowScrollBar && offsetYValue < viewPortHeight.value - latestGridViewHeight) {
            offsetYValue = viewPortHeight.value - latestGridViewHeight;
        }
        if (!shouldShowScrollBar || onTopOfScrollbar) {
            offsetYValue = topOfPostion;
        }
        if (offsetY.value !== offsetYValue) {
            offsetY.value = offsetYValue;
        }
    }

    function updateVisibleRowsOnLatestVisibleScope() {
        if (usingVirtualScroll.value) {
            minViewPortRowIndex = getVisibleRowIndexByPosition(Math.abs(offsetY.value));
            maxViewPortRowIndex = minViewPortRowIndex + visibleCapacity.value - 1;
            if (minViewPortRowIndex < minVisibleRowIndex.value || maxViewPortRowIndex > maxVisibleRowIndex.value - preloadCount / 2) {
                reCalculateVisualDataRows();
            }
        }
    }

    function updateOffsetY(deltaY: number) {
        let offsetYValue = offsetY.value + deltaY;
        if (offsetYValue > topOfPostion) {
            offsetYValue = topOfPostion;
        }
        const latestGridViewHeight = getLatestGridViewHeight();
        checkOffsetYForLatestGridViewHeight(offsetYValue, latestGridViewHeight);
        updateVisibleRowsOnLatestVisibleScope();
    }

    function updateOffsetX(deltaX: number) {
        if (!shouldShowHorizontalScrollbar.value) {
            offsetX.value = 0;
            return;
        }

        let offsetXValue = offsetX.value + deltaX;
        if (offsetXValue > leftOfPostion) {
            offsetXValue = leftOfPostion;
        }
        if (offsetXValue < rightOfPosition.value) {
            offsetXValue = rightOfPosition.value;
        }
        if (offsetX.value !== offsetXValue) {
            offsetX.value = offsetXValue;
        }
    }

    function fitHorizontalScroll() {
        if (Math.abs(offsetX.value) + viewPortWidth.value > gridViewWidth.value) {
            offsetX.value = Math.min(viewPortWidth.value - gridViewWidth.value, 0);
        }
    }

    function resetScroll() {
        offsetY.value = 0;
        minViewPortRowIndex = 0;
        maxViewPortRowIndex = visibleCapacity.value - 1;
        reCalculateVisualDataRows();
    }

    function updateVirticalScroll() {
        const latestGridViewHeight = getLatestGridViewHeight();
        checkOffsetYForLatestGridViewHeight(offsetY.value, latestGridViewHeight);
    }

    watch([viewPortWidth, gridViewWidth], () => {
        updateOffsetX(0);
    });

    function getDeltaFromEvent(e) {
        let { deltaX } = e.deltaX;
        let deltaY = -1 * e.deltaY;

        if (typeof deltaX === 'undefined') {
            // OS X Safari
            deltaX = -1 * e.wheelDeltaX / 6;
        }

        if (typeof deltaY === 'undefined') {
            deltaY = e.wheelDeltaY / 6;
        }

        if (e.deltaMode && e.deltaMode === 1) {
            // Firefox in deltaMode 1: Line scrolling
            deltaX *= 10;
            deltaY *= 10;
        }

        // if (deltaX !== deltaX && deltaY !== deltaY /* NaN checks */) {
        //     // IE in some mouse drivers
        //     deltaX = 0;
        //     deltaY = e.wheelDelta;
        // }

        if (e.shiftKey) {
            // reverse axis with shift key
            return { deltaY: -deltaY, deltaX: -deltaX };
        }
        return { deltaX, deltaY };
    }

    function onWheel(payload: WheelEvent) {
        if (props.disabled) {
            return;
        }
        if (!shouldShowVirticalScrollbar.value) {
            return;
        }
        payload.preventDefault();
        payload.stopPropagation();

        const { deltaX, deltaY } = getDeltaFromEvent(payload);

        // const deltaY = ((payload as any).wheelDeltaY || payload.deltaY) / 10;
        // const deltaX = ((payload as any).wheelDeltaX || payload.deltaX) / 10;

        updateOffsetY(deltaY);
        updateOffsetX(deltaX);
    }

    const leftFixedGridDataStyle = computed(() => {
        const styleObject = {
            height: `${dataView.value.length * rowHeight}px`,
            width: `${leftColumnsWidth.value}px`,
            transform: `translateY(${offsetY.value}px)`
        } as Record<string, any>;
        return styleObject;
    });

    const leftFixedGridMergedDataStyle = computed(() => {
        const styleObject = {
            transform: `translateY(${offsetY.value}px)`
        } as Record<string, any>;
        return styleObject;
    });

    const gridDataStyle = computed(() => {
        const styleObject = {
            height: `${dataView.value.length * rowHeight}px`,
            width: `${gridViewWidth.value}px`,
            transform: `translate(${offsetX.value}px, ${offsetY.value}px)`
        } as Record<string, any>;
        return styleObject;
    });

    const gridMergedDataStyle = computed(() => {
        const styleObject = {
            transform: `translate(${offsetX.value}px, ${offsetY.value}px)`
        } as Record<string, any>;
        return styleObject;
    });

    const rightFixedGridDataStyle = computed(() => {
        const styleObject = {
            height: `${dataView.value.length * rowHeight}px`,
            width: `${rightColumnsWidth.value}px`,
            transform: `translateY(${offsetY.value}px)`
        } as Record<string, any>;
        return styleObject;
    });

    const rightFixedGridMergedDataStyle = computed(() => {
        const styleObject = {
            transform: `translateY(${offsetY.value}px)`
        } as Record<string, any>;
        return styleObject;
    });

    const gridSideStyle = computed(() => {
        const styleObject = {
            height: `${dataView.value.length * rowHeight}px`,
            width: `${sidebarWidth.value}px`,
            transform: `translateY(${offsetY.value}px)`
        } as Record<string, any>;
        return styleObject;
    });

    const leftFixedGridHeaderColumnsStyle = computed(() => {
        const styleObject = {
            width: `${leftColumnsWidth.value}px`
        } as Record<string, any>;
        return styleObject;
    });

    const gridHeaderColumnsStyle = computed(() => {
        const styleObject = {
            transform: `translateX(${offsetX.value}px)`
        } as Record<string, any>;
        return styleObject;
    });

    const rightFixedGridHeaderColumnsStyle = computed(() => {
        const styleObject = {
            width: `${rightColumnsWidth.value}px`
        } as Record<string, any>;
        return styleObject;
    });

    const thumbPositionY = computed(() => {
        const latestGridViewHeight = getLatestGridViewHeight();
        checkOffsetYForLatestGridViewHeight(offsetY.value, latestGridViewHeight);
        return (offsetY.value / (viewPortHeight.value - latestGridViewHeight)) * (viewPortHeight.value - scrollThumbHeight.value);
    });

    const verticalScrollThumbStyle = computed(() => {
        const styleObject = {
            height: `${scrollThumbHeight.value}px`,
            top: `${thumbPositionY.value}px`
        } as Record<string, any>;
        return styleObject;
    });

    const thumbPositionX = computed(() => {
        return (offsetX.value / rightOfPosition.value) * (dataGridWidth.value - scrollThumbWidth.value);
    });

    const horizontalScrollThumbStyle = computed(() => {
        const styleObject = {
            width: `${scrollThumbWidth.value}px`,
            left: `${thumbPositionX.value}px`
        } as Record<string, any>;
        return styleObject;
    });

    const onDraggingScrollThumb = ref(false);
    const onDraggingStartY = ref(-1);
    const onDraggingStartX = ref(-1);
    const scrollThumbContainer = ref();
    let gridContentElement: any;

    function onDragHorizontalScroll($event: MouseEvent) {
        if (onDraggingScrollThumb.value && onDraggingStartX.value !== -1) {
            const mouseOffset = onDraggingStartX.value - $event.pageX;
            const mouseMoveRange = dataGridWidth.value - scrollThumbWidth.value;
            if (Math.abs(mouseOffset) <= mouseMoveRange) {
                const deltaOffsetX =
                    (mouseOffset / (viewPortWidth.value - scrollThumbWidth.value)) * (gridViewWidth.value - viewPortWidth.value);
                updateOffsetX(deltaOffsetX);
                onDraggingStartX.value = $event.pageX;
            }
        }
    }

    function onDragVerticalScroll($event: MouseEvent) {
        if (onDraggingScrollThumb.value && onDraggingStartY.value !== -1) {
            const mouseOffset = onDraggingStartY.value - $event.pageY;
            const latestGridViewHeight = getLatestGridViewHeight();
            const mouseMoveRange = latestGridViewHeight - scrollThumbHeight.value;
            if (Math.abs(mouseOffset) <= mouseMoveRange) {
                const deltaOffsetY =
                    (mouseOffset / (viewPortHeight.value - scrollThumbHeight.value)) * (latestGridViewHeight - viewPortHeight.value);

                updateOffsetY(deltaOffsetY);
                onDraggingStartY.value = $event.pageY;
            }
        }
    }

    function releaseDragScroll($event: MouseEvent) {
        onDraggingScrollThumb.value = false;
        onDraggingStartY.value = -1;
        onDraggingStartX.value = -1;
        document.removeEventListener('mouseup', releaseDragScroll);
        document.removeEventListener('mousemove', onDragHorizontalScroll);
        if (dragHandler) {
            document.removeEventListener('mousemove', dragHandler);
        }
        // document.removeEventListener('mousemove', onDragVerticalScroll);
        // if (gridContentElement) {
        //     gridContentElement.removeEventListener('mousemove', onDragHorizontalScroll);
        //     gridContentElement.removeEventListener('mousemove', onDragVerticalScroll);
        // }
        document.body.style.userSelect = '';
        if (scrollThumbContainer.value) {
            scrollThumbContainer.value.style.opacity = null;
        }
    }

    function onMouseDownScrollThumb($event: MouseEvent, gridContentRef: Ref<any>, thumbType: 'vertical' | 'horizontal') {
        onDraggingScrollThumb.value = true;

        const scrollThumbParent = ($event.target as Element)?.parentElement;
        if (scrollThumbParent) {
            scrollThumbParent.style.opacity = '1';
            scrollThumbContainer.value = scrollThumbParent;
        }

        const draggingHandler = thumbType === 'horizontal' ? onDragHorizontalScroll : onDragVerticalScroll;

        if (thumbType === 'vertical') {
            onDraggingStartY.value = $event.pageY;
        }
        if (thumbType === 'horizontal') {
            onDraggingStartX.value = $event.pageX;
        }
        if (gridContentRef.value) {
            if (!dragHandler) {
                dragHandler = throttle(draggingHandler, 100);
            }
            // gridContentRef.value.addEventListener('mousemove', draggingHandler);
            gridContentElement = gridContentRef.value;
            document.addEventListener('mousemove', dragHandler);
            document.addEventListener('mouseup', releaseDragScroll);
            document.body.style.userSelect = 'none';
        }
    }

    return {
        dataGridWidth,
        fitHorizontalScroll,
        gridDataStyle,
        gridHeaderColumnsStyle,
        gridMergedDataStyle,
        gridSideStyle,
        horizontalScrollThumbStyle,
        leftFixedGridDataStyle,
        leftFixedGridHeaderColumnsStyle,
        leftFixedGridMergedDataStyle,
        offsetX,
        offsetY,
        onMouseDownScrollThumb,
        onWheel,
        reCalculateVisualDataRows,
        resetScroll,
        rightFixedGridDataStyle,
        rightFixedGridHeaderColumnsStyle,
        rightFixedGridMergedDataStyle,
        shouldShowHorizontalScrollbar,
        shouldShowVirticalScrollbar,
        updateVirticalScroll,
        updateVisibleRowsOnLatestVisibleScope,
        verticalScrollThumbStyle,
        viewPortWidth,
        viewPortHeight
    };
}
