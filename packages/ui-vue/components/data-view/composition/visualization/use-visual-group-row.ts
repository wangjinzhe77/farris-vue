 
import { computed, ref } from "vue";
import { CellMode, DataColumn, DataViewOptions, UseGroupRow, UseIdentify, UseVisualDataCell, UseVisualDataRow, VisualData, VisualDataCell, VisualDataType } from "../types";
// import { DataGridColumn, DataGridProps } from "../data-grid.props";
// import { UseDataRow, UseGroupRow, UseVisualDataCell, VisualData, VisualDataCell, VisualDataType } from "./types";

export function useVisualGroupRow(
    props: DataViewOptions,
    useIdentifyComposition: UseIdentify,
    useVisualDataCell: UseVisualDataCell,
    useVisualDataRowComposition: UseVisualDataRow
): UseGroupRow {
    const summaryOptions = ref(props.summary);
    const { idField } = useIdentifyComposition;
    const { createCellByField } = useVisualDataCell;
    const { createEmptyRow } = useVisualDataRowComposition;

    const groupSummaryFields = computed(() => {
        const options = summaryOptions.value;
        return options?.groupFields || [];
    });

    const shouldShowColumnSummary = computed(() => {
        const options = summaryOptions.value;
        return options && options.enable && options.groupFields && options.groupFields.length > 0;
    });

    function renderSummeryInGroupRow(currentRow: VisualData, groupField: string, dataItem: any, columns: DataColumn[]) {
        if (shouldShowColumnSummary.value) {
            const groupFieldCell = currentRow.data[groupField];
            let groupCellColumnSpanCount = groupFieldCell.colSpan;
            const groupSummaryMap = groupSummaryFields.value.reduce((mapResult: Map<string, boolean>, groupField: string) => {
                mapResult.set(groupField, true);
                return mapResult;
            }, new Map<string, boolean>());
            columns.reduce((groupRow: VisualData, column: DataColumn, columnIndex: number) => {
                if (groupSummaryMap.has(column.field)) {
                    const currentCell: VisualDataCell = createCellByField(column.field, CellMode.readonly, columnIndex, dataItem, currentRow);
                    currentRow.data[column.field] = currentCell;
                    if (groupCellColumnSpanCount - 1 > columnIndex) {
                        groupCellColumnSpanCount = columnIndex;
                    }
                }
                return groupRow;
            }, currentRow);
            groupFieldCell.colSpan = groupCellColumnSpanCount;
        }
    }

    function renderGroupRow(
        dataItem: any, preDataItem: any, preRow: VisualData,
        rowIndex: number, top: number, columns: DataColumn[],
        visibleDatas?: VisualData[]
    ): VisualData {
        const groupField = dataItem.__fv_data_grid_group_field__;
        const currentRow: VisualData = createEmptyRow(VisualDataType.group, rowIndex, dataItem, preDataItem, preRow, top);
        currentRow.data[idField.value] = createCellByField(idField.value, CellMode.readonly, -1, dataItem, currentRow, 0);
        currentRow.data[groupField] = createCellByField(groupField, CellMode.readonly, 1, dataItem, currentRow, columns.length);
        renderSummeryInGroupRow(currentRow, groupField, dataItem, columns);
        return currentRow;
    }

    return { renderGroupRow };
}
