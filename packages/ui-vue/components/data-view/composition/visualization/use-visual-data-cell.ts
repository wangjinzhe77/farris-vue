import { ref } from "vue";
import {
    CellMode, DataColumn, DataViewOptions, UseCellEditor,
    UseVisualDataBound, UseVisualDataCell, VisualData,
    VisualDataCell
} from "../types";

import getCommandColumn from '../../components/editors/commands.component';
import getFormatColumn from '../../components/column-format/column-format.component';
import { resolveField, setFieldValue } from "@farris/ui-vue/components/common";

export function useVisualDataCell(
    props: DataViewOptions,
    useCellEditorComposition: UseCellEditor,
    useVisualDataBoundComposition: UseVisualDataBound
): UseVisualDataCell {
    const mergingCell = ref(props.mergeCell);
    const rowOption = ref(props.rowOption);
    const autoRowHeight = rowOption.value?.wrapContent || false;
    const { updateVisualInfomation } = useVisualDataBoundComposition;
    const { renderCommandColumn } = getCommandColumn();
    const { renderFormatColumn } = getFormatColumn();
    function createCellByField(targetField: string, mode: CellMode, index: number, dataItem: any, parent: VisualData, colSpan = 1) {
        const targetCell: VisualDataCell = {
            colSpan,
            rowSpan: 1,
            index,
            field: targetField,
            data: resolveField(dataItem, targetField),
            editingData: resolveField(dataItem, targetField),
            parent,
            mode,
            align: 'left',
            valign: 'start',
            showEllipsis: true,
            getEditor: (cell: any) => cell.data,
            setRef: (vnode: any) => updateVisualInfomation(vnode, targetCell, dataItem),
            update: () => { },
            accept: () => { },
            cancel: () => { },
            updateData: () => { }
        };
        return targetCell;
    }

    function wrapCellEditor(targetCell: VisualDataCell, column: DataColumn, dataItem: any) {
        targetCell.getEditor = (cell: VisualDataCell) => useCellEditorComposition.getEditor(cell, column, cell.parent);
        targetCell.setRef = (vnode: any) => updateVisualInfomation(vnode, targetCell, dataItem);
        targetCell.update = (value: any) => { value !== undefined && (targetCell.editingData = value); };
        targetCell.accept = () => {
            // dataItem[column.field] = targetCell.editingData;
            setFieldValue(targetCell.editingData, dataItem, column.field);
            targetCell.data = resolveField(dataItem, column.field);
        };
        targetCell.cancel = () => { targetCell.editingData = targetCell.data; };
        targetCell.updateData = (newDataItem: any) => {
            // dataItem[column.field] = newDataItem[column.field];
            setFieldValue(resolveField(newDataItem, column.field), dataItem, column.field);
            targetCell.data = resolveField(dataItem, column.field);
            targetCell.editingData = resolveField(dataItem, column.field);
        };
        return targetCell;
    }

    function tryToWrapCellCommand(targetCell: VisualDataCell, column: DataColumn) {
        if (column.dataType === 'commands') {
            targetCell.formatter = (cell: VisualDataCell, visualDataRow: VisualData) => {
                // 此处应该渲染操作列组件而不是编辑组件
                return renderCommandColumn(column, visualDataRow);
            };
        } else {
            if (column.formatter) {
                targetCell.formatter = (cell: VisualDataCell, visualDataRow: VisualData) => {
                    return typeof column.formatter === 'function' ?
                        column.formatter(cell, visualDataRow) :
                        renderFormatColumn(column.dataType, column, visualDataRow);
                };
            }
        }
        // if (column.formatter && column.dataType !== 'commands') {
        //     targetCell.formatter = (cell: VisualDataCell, visualDataRow: VisualData) => {
        //         // 解决数据中有时类型是对象的情况
        //         if (typeof column.formatter == 'function') {
        //             return column.formatter(cell, visualDataRow);
        //         }
        //     };
        // }
    }

    function merginSameValueInColumn(currentCell: VisualDataCell, preDataItem: VisualData, columnField: string) {
        const preRowMerginSpan = preDataItem && preDataItem.data[columnField];
        const hasBeenSpanned = preRowMerginSpan && preRowMerginSpan.data === currentCell.data;
        if (hasBeenSpanned) {
            const spannedBy: VisualDataCell = (preRowMerginSpan && preRowMerginSpan.spannedBy) || preRowMerginSpan;
            spannedBy.rowSpan++;
            spannedBy.spanned = spannedBy.spanned || [];
            spannedBy.spanned.push(currentCell);
            currentCell.colSpan = 0;
            currentCell.rowSpan = 0;
            currentCell.spannedBy = spannedBy;
        }
    }

    function tryToMergeCellValue(targetCell: VisualDataCell, preMergingRow: VisualData, column: DataColumn) {
        if (mergingCell.value) {
            merginSameValueInColumn(targetCell, preMergingRow, column.field);
        }
    }
    // 补充cell属性
    function improveCellByColumn(targetCell: VisualDataCell, column: DataColumn): void {
        if (column.showEllipsis !== undefined) {
            targetCell.showEllipsis = column.showEllipsis;
        }
        targetCell.column = column;
        targetCell.align = column.align || 'left';
        targetCell.valign = column.valign || 'middle';
    }

    function createCellByColumn(
        column: DataColumn, index: number, dataItem: any,
        parent: VisualData, preVisualData: VisualData
    ): VisualDataCell {
        const cellMode = column.readonly ? CellMode.readonly : CellMode.editable;
        const targetCell: VisualDataCell = createCellByField(column.field, cellMode, index, dataItem, parent);
        improveCellByColumn(targetCell, column);
        wrapCellEditor(targetCell, column, dataItem);
        tryToWrapCellCommand(targetCell, column);
        tryToMergeCellValue(targetCell, preVisualData, column);
        return targetCell;
    }

    return { createCellByColumn, createCellByField };
}
