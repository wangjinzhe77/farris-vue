import { Ref, ref } from "vue";
import {
    DataColumn,
    DataViewOptions, UseVisualDataRow, UseEdit, UseVisualDataBound, UseVisualDataCell,
    VisualData, VisualDataStatus, VisualDataType,
    UseHierarchy,
    UseIdentify
} from "../types";
import { resolveField, setFieldValue } from "@farris/ui-vue/components/common";

export function useVisualDataRow(
    props: DataViewOptions,
    useEditComposition: UseEdit,
    useHierarchyComposition: UseHierarchy,
    useIdentifyComposition: UseIdentify,
    useVisualDataBoundComposition: UseVisualDataBound,
    useVisualDataCell: UseVisualDataCell
): UseVisualDataRow {
    const rowOption = ref(props.rowOption);
    /** 每行的默认高度 */
    const rowHeight = rowOption.value?.height || 28;
    /** 是否启用自动行高度计算 */
    const autoRowHeight = rowOption.value?.wrapContent || false;
    const { idField } = useIdentifyComposition;
    const { collapseField } = useHierarchyComposition;
    const { updateRowPosition } = useVisualDataBoundComposition;
    const { createCellByColumn } = useVisualDataCell;

    const updateCellData = (newDataItem: any, raw: any, row: VisualData, field: string) => {
        setFieldValue(resolveField(newDataItem, field), raw, field);
        row.data[field].data = resolveField(raw, field);
        row.data[field].editingData = resolveField(raw, field);
    };
    function createEmptyRow(type: VisualDataType, index: number, dataItem: any, pre: any,
        preRow: VisualData | undefined, top: number, visibleDatas?: VisualData[]) {
        const {
            __fv_data_grid_group_field__: groupField,
            __fv_data_grid_group_value__: groupValue,
            __fv_data_index__: dataIndex
        } = dataItem;

        const layer = dataItem.__fv_data_grid_group_layer__ || (dataItem.__fv_parents__ ? dataItem.__fv_parents__?.size : 0) || 0;
        const parentIndex = typeof dataItem.parent === 'number' ? Number(dataItem.parent) : -1;
        // 行是否折叠
        const collapse = dataItem.__fv_data_grid_group_collapse__ || dataItem[collapseField.value];
        const visible = dataItem.__fv_visible__ === undefined ? true : dataItem.__fv_visible__;
        const checked = dataItem.__fv_checked__;
        const indeterminate = dataItem.__fv_indeterminate__;
        // 行是否禁用
        const disabled = dataItem.__fv_disabled__ || dataItem[props.rowOption?.disabledField || 'disabled'];
        const parent = visibleDatas?.find((visibleData: VisualData) => visibleData.raw[idField.value] === dataItem.parent);
        const isParentCollapse = parent && parent.collapse;
        // const isChildOfPreRow = pre && pre[idField.value] === dataItem.parent;
        const isSiblingOfPreRow = pre && pre.parent === dataItem.parent;
        const shouldBeHidden = (isParentCollapse || (parent && !parent.visible));
        // ||    (isSiblingOfPreRow && preRow && !preRow.visible);
        // const shouldBeHidden = (isChildOfPreRow && preRow && (preRow.collapse || !preRow.visible)) ||
        //     (isSiblingOfPreRow && preRow && !preRow.visible);

        const currentRow: VisualData = {
            collapse,
            data: {},
            dataIndex,
            groupField,
            groupValue,
            layer,
            index,
            parentIndex,
            pre,
            top,
            type,
            raw: dataItem,
            checked,
            disabled,
            indeterminate,
            setRef: (vnode: any) => { currentRow.ref = vnode; },
            visible: visible && !shouldBeHidden,
            status: VisualDataStatus.initial,
            updateCell: (newDataItem: any, field: string) => {
                updateCellData(newDataItem, dataItem, currentRow, field);
            },
            updateCells: (newDataItem: any, fields: string[]) => {
                fields.forEach(field => {
                    updateCellData(newDataItem, dataItem, currentRow, field);
                });
            }
        };
        if (!pre) {
            dataItem.__fv_data_position__ = 0;
        }
        if (!autoRowHeight) {
            currentRow.height = rowHeight;
            dataItem.__fv_data_height__ = currentRow.height;
            updateRowPosition(currentRow, dataItem);
        }
        if (rowOption.value?.customRowStatus) {
            rowOption.value.customRowStatus(currentRow);
            dataItem.__fv_collapse__ = currentRow.collapse;
            dataItem.__fv_disabled__ = currentRow.disabled;
            dataItem.__fv_visible__ = currentRow.visible;
        }
        return currentRow;
    }


    function createNewRowFromDataItem(columns: DataColumn[], dataItem: any, preDataItem: any, preRow: VisualData, rowIndex: number, top: number, visibleDatas?: VisualData[]) {
        const currentRow = createEmptyRow(VisualDataType.data, rowIndex, dataItem, preDataItem, preRow, top, visibleDatas);
        columns.forEach((column: DataColumn, columnIndex: number) => {
            currentRow.data[column.field] = createCellByColumn(column, rowIndex, dataItem, currentRow, preRow);
        });
        return currentRow;
    }

    function restoreFromSnapshot(editingSnapshot: VisualData, index: number, dataIndex: number, top: number, pre: any) {
        const currentRow = Object.assign(editingSnapshot, { index, dataIndex, top, pre });
        return currentRow;
    }

    function renderDataRow(
        dataItem: any,
        preDataItem: any,
        preRow: VisualData,
        rowIndex: number,
        top: number,
        columns: DataColumn[],
        visibleDatas?: VisualData[]
    ): VisualData {
        const dataIndex = dataItem.__fv_data_index__;
        const dataIdentify = dataItem[idField.value];
        const editingSnapshot = useEditComposition.getEditingSnapshot(dataIdentify) as VisualData;
        const shouldCreateNewRow = editingSnapshot === null;
        const currentRow = shouldCreateNewRow ? createNewRowFromDataItem(columns, dataItem, preDataItem,
            preRow, rowIndex, top, visibleDatas) :
            restoreFromSnapshot(editingSnapshot, rowIndex, dataIndex, top, preDataItem);
        return currentRow;
    }

    return { createEmptyRow, createNewRowFromDataItem, renderDataRow };
}
