import { DataViewOptions, UseVisualDataBound, VisualData, VisualDataCell } from "../types";

export function useVisualDataBound(
    props: DataViewOptions,
): UseVisualDataBound {
    /** 每行的默认高度 */
    const rowHeight = props.rowOption?.height || 28;

    function updateRowPosition(visualData: VisualData, dataItem: any) {
        const preDataItem = visualData.pre;
        if (preDataItem) {
            const preDataItemPosition = preDataItem ? (
                preDataItem.__fv_data_position__ !== undefined ? preDataItem.__fv_data_position__ : preDataItem.__fv_index__ * rowHeight
            ) : 0;
            const preDataItemHeight = preDataItem ? (
                preDataItem.__fv_data_height__ !== undefined ? preDataItem.__fv_data_height__ : rowHeight
            ) : 0;
            const topPosition = !visualData.visible ? preDataItem.__fv_data_position__ : preDataItemPosition + preDataItemHeight;
            visualData.top = topPosition;
            dataItem.__fv_data_position__ = topPosition;
        } else {
            visualData.top = 0;
            dataItem.__fv_data_position__ = 0;
        }
    }

    function updateVisualInfomation(vnode: any, targetCell: VisualDataCell, dataItem: any) {
        if (vnode && !targetCell.ref) {
            targetCell.ref = vnode;
            const visualData = targetCell.parent as VisualData;
            if (targetCell.cellHeight !== vnode.offsetHeight) {
                targetCell.cellHeight = vnode.offsetHeight;
            }
            if (targetCell.cellHeight && targetCell.cellHeight > (visualData.height || 0)) {
                visualData.height = targetCell.cellHeight;
                dataItem.__fv_data_height__ = visualData.height;
            }
            updateRowPosition(visualData, dataItem);
        }
    }

    return { updateRowPosition, updateVisualInfomation };
}
