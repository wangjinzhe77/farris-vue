/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { reactive, ref, SetupContext, watch } from 'vue';
import { EditorConfig, FDynamicFormInput } from '@farris/ui-vue/components/dynamic-form';
import { resolveField } from '@farris/ui-vue/components/common';
import {
    CellMode, DataColumn, DataViewOptions, UseEdit,
    UseIdentify, UseRow, VisualData, VisualDataCell, VisualDataStatus
} from './types';
import { isUndefined } from 'lodash-es';

export function useEdit(props: DataViewOptions, context: SetupContext, useIdentifyComposition: UseIdentify, useRowComposition: UseRow): UseEdit {
    const { idField } = useIdentifyComposition;
    const editable = ref(props.editable);
    const editOptions = ref(props.editOption);
    const wrapContent = ref(props.rowOption?.wrapContent || false);
    const { onClickRow } = useRowComposition;
    watch(
        () => props.editable,
        () => {
            editable.value = props.editable;
        }
    );

    watch(() => props.editOption?.editMode, (newValue) => {
        editOptions.value.editMode = newValue;
    });

    const typeToEditorName = new Map<string, string>([
        ['boolean', 'check-box'],
        ['datetime', 'date-picker'],
        ['enum', 'combo-list'],
        ['number', 'number-spinner'],
        ['string', 'input-group'],
        ['text', 'text'],
        ['commands', 'commands-editor']
    ]);

    let editingCell: VisualDataCell | null;
    let editingRow: VisualData | undefined;
    let oldCellValue: any | null;
    let editingColumn: Partial<DataColumn>;

    function getEditingSnapshot(dataIdentify: string): VisualData | null {
        if (editingRow) {
            const editingDataIdentify = editingRow.raw[idField.value];
            return editingDataIdentify === dataIdentify ? editingRow : null;
        }
        return null;
    }

    const controlData = reactive<Record<string, any>>({});

    function beginEditCell(cell: VisualDataCell, row: VisualData, column: Partial<DataColumn> = {}) {
        cell.mode = CellMode.editing;
        editingCell = cell;
        editingRow = row;
        oldCellValue = resolveField(row?.raw, cell.field);
        editingColumn = column;
    }

    function endEditCell(currentEditingCell: VisualDataCell) {
        currentEditingCell.accept();
        currentEditingCell.mode = CellMode.editable;
        editingCell = null;
        context.emit('endEditCell',
            {
                cell: currentEditingCell,
                row: editingRow,
                newValue: resolveField(editingRow?.raw, currentEditingCell.field),
                oldValue: oldCellValue,
                column: editingColumn,
                editor: controlData
            });
        // Promise.resolve()
        //     .then(() => {
        //         if (isUndefined(props.beforeEndEditCell)) {
        //             return true;
        //         }
        //         if (typeof props.beforeEndEditCell === 'function') {
        //             return props.beforeEndEditCell();
        //         }
        //         return props.beforeEndEditCell;
        //     })
        //     .then(value => {
        //         if (value) {
        //             currentEditingCell.accept();
        //             currentEditingCell.mode = CellMode.editable;
        //             editingCell = null;
        //             context.emit('endEditCell',
        //                 {
        //                     cell: currentEditingCell,
        //                     row: editingRow,
        //                     newValue: resolveField(editingRow?.raw, currentEditingCell.field),
        //                     oldValue: oldCellValue,
        //                     column: editingColumn,
        //                     editor: controlData
        //                 });
        //         }
        //     });
    }

    function onClickOutOfCell(payload: MouseEvent) {
        const clickInLookupModal = document.body.classList.contains('lookup-modal-open');
        if (clickInLookupModal) {
            return;
        }

        const clickingCellElement = (payload.target as any).closest('.fv-grid-cell');
        const clickingEditingCell = editingCell && editingCell.ref && editingCell.ref === clickingCellElement;
        if (clickingEditingCell) {
            return;
        }

        if (editingCell) {
            endEditCell(editingCell);
            if (editingRow) {
                editingRow = undefined;
            }
        }
        document.body.removeEventListener('click', onClickOutOfCell);
    }

    function onClickCell(payload: MouseEvent, cell: VisualDataCell, row: VisualData, column: Partial<DataColumn> = {}) {
        // 阻止冒泡  阻止触发onClickOutOfCell
        payload.stopPropagation();
        // 点击行选中行
        onClickRow(payload, row);
        if (editable.value && editOptions.value.editMode === 'cell' && cell.mode === CellMode.editable) {
            if (editingCell) {
                endEditCell(editingCell);
            }
            Promise.resolve()
                .then(() => {
                    if (isUndefined(props.beforeEditCell)) {
                        // 默认进入编辑
                        return true;
                    }
                    if (typeof props.beforeEditCell === 'function') {
                        return props.beforeEditCell({ row, cell, rawData: row.raw, column: column as DataColumn });
                    }
                    return props.beforeEditCell;
                })
                .then(value => {
                    if (value) {
                        beginEditCell(cell, row, column);
                        document.body.removeEventListener('click', onClickOutOfCell);
                        document.body.addEventListener('click', onClickOutOfCell);
                    }
                });
        }
    }

    function beginEditRow(visualDataRow: VisualData) {
        Object.values(visualDataRow.data)
            .filter((cell: VisualDataCell) => cell.mode === CellMode.editable && cell.field !== '__commands__')
            .forEach((editableCell: VisualDataCell) => {
                editableCell.mode = CellMode.editing;
            });
        visualDataRow.status = VisualDataStatus.editing;
        editingRow = visualDataRow;
    }

    function endEditRow(visualDataRow: VisualData, accept: boolean) {
        Object.values(visualDataRow.data)
            .filter((cell: VisualDataCell) => cell.mode === CellMode.editing)
            .forEach((editableCell: VisualDataCell) => {
                if (accept) {
                    editableCell.accept();
                } else {
                    editableCell.cancel();
                }
                editableCell.mode = CellMode.editable;
            });
        visualDataRow.status = VisualDataStatus.initial;
        if (editingRow === visualDataRow) {
            editingRow = undefined;
        }
    }

    function onEditingRow(visualDataRow: VisualData) {
        if (editable.value && editOptions.value.editMode === 'row' && editingRow !== visualDataRow) {
            if (editingRow) {
                endEditRow(editingRow, false);
            }
            beginEditRow(visualDataRow);
        }
    }

    function acceptEditingRow(visualDataRow: VisualData) {
        endEditRow(visualDataRow, true);
        visualDataRow.status = VisualDataStatus.initial;
    }

    function cancelEditingRow(visualDataRow: VisualData) {
        endEditRow(visualDataRow, false);
        visualDataRow.status = VisualDataStatus.initial;
    }

    function getEditor(cell: VisualDataCell, column: DataColumn, visualDataRow: VisualData) {
        const fieldType = column.dataType;
        const editor = column.editor || { type: typeToEditorName.get(fieldType) || 'input-group' } as EditorConfig;
        if (!wrapContent.value && editor.type === 'text') {
            editor.type = 'input-group';
        }

        if (editor) {
            editor.context = { rowData: visualDataRow.raw, column, cell, editor: controlData };
            return <FDynamicFormInput focusOnCreated={props.focusOnEditingCell} selectOnCreated={props.selectOnEditingCell}
                editor={editor} v-model={cell.editingData} id={column.id} ></FDynamicFormInput>;
        }
        return cell.data;
    }

    function onEndEditCell() {
        if (editingCell) {
            endEditCell(editingCell);
        }
    }

    return { onClickCell, onEditingRow, acceptEditingRow, cancelEditingRow, getEditingSnapshot, getEditor, endEditCell: onEndEditCell };
}
