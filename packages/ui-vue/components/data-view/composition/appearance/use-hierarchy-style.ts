 
import { Ref, ref } from "vue";
import { ColumnContext, DataViewOptions, DictTreeItem, UseHierarchy, VisualData } from "../types";

export function useHierarchyStyle(
    props: DataViewOptions,
    visibleDatas: Ref<VisualData[]>,
    useHierarchyComposition: UseHierarchy
) {
    const { hasChildrenField } = useHierarchyComposition;
    /** 连接线颜色 */
    const lineColor = ref('#9399a0');
    /** 单元格高度 */
    const cellHeight = ref(28);

    /** 计算连接线的长度 */
    function getPaddingBottom() {
        visibleDatas.value.forEach((visualData: VisualData) => {
            const isInFirstLevel = visualData.raw.__fv_parent_index__ === -1;
            const isLeafNode = visualData.raw.__fv_descendant_index__.length === 0;
            const siblingsOfCurrent = visualData.raw.__fv_parent_index__ > -1 ? visibleDatas.value[visualData.raw.__fv_parent_index__].raw.__fv_descendant_index__ : [];
            const isLastSlibingNode = !isInFirstLevel && siblingsOfCurrent[siblingsOfCurrent.length - 1] === visualData.raw.__fv_index__;
            const atTheEndPoint = isInFirstLevel || isLeafNode || isLastSlibingNode;
            const lineLength = atTheEndPoint ? 0 : (visualData.raw.__fv_child_length__) * cellHeight.value;
            visibleDatas.value[visualData.raw.__fv_index__].paddingBottom = `${lineLength}px`;
        });
    }

    /** 处理连接线显示一半的节点 */
    function getHalfBorderNodes() {
        const halfBorderNodes: number[] = [].concat(
            ...visibleDatas.value.filter((visualData: VisualData) => visualData.raw.__fv_descendant_index__.length !== 0)
                .map((visualData: VisualData) => [...visualData.raw.__fv_descendant_index__.slice(-1)])
                .flat()
        );
        const returnHalfBorderNodes = halfBorderNodes.sort((a, b) => Number(a) - Number(b));
        return returnHalfBorderNodes;
    }

    /** 计算连接线显示的长度  */
    function getPaddingBottomLength(returnIndex: number) {
        const backgroundStyleArray = new Array(visibleDatas.value.length).fill(0);
        if (props.showLines) {
            const halfBorderNodes = getHalfBorderNodes();
            visibleDatas.value.forEach((visualData: VisualData, index: number) => {
                const isHalfBorder = halfBorderNodes.includes(index);
                const hasChildren = visualData.raw[hasChildrenField.value];
                backgroundStyleArray[index] = `repeating-linear-gradient(90deg, ${lineColor.value} 0 1px, transparent 0px 2px) ${hasChildren ? '-10px' : '0px'} ${(cellHeight.value / 2)}px/20px 1px no-repeat,repeating-linear-gradient(${lineColor.value} 0 1px, transparent 0px 2px) 0px 0px/1px ${isHalfBorder ? '50%' : '100%'} no-repeat`;
            });
            backgroundStyleArray[0] = '';
        }
        return backgroundStyleArray[returnIndex];
    }

    /** 处理连接线返回值 */
    function handlePaddingBottomReturnValue(visualTreeNodeIndex: number) {
        if (props.showLines && visibleDatas.value) {
            getPaddingBottom();
            return visibleDatas.value[visualTreeNodeIndex].paddingBottom;
        }
    }

    /** 处理折叠后的连接线显示逻辑 */
    function handleCollapsedPaddingBottom(index: number, visibleDatasValue: VisualData[], dictTree: DictTreeItem[]) {
        // /** 当前节点的所有父节点 */
        const curNodeParents = dictTree[index].parents.set(index, true);
        // 当前节点的所有父节点
        curNodeParents.forEach((parent: any) => {
            dictTree[parent].childsLength = dictTree[parent].childWithLines.filter((index: number) => visibleDatasValue[index].visible).length;
        });
        return visibleDatasValue;
    }

    function hierarchyCellContentStyle(visualTreeNode: VisualData): Record<string, any> {
        const isTopLevelNode = (visualTreeNode.raw.__fv_parent_index__ === undefined || visualTreeNode.raw.__fv_parent_index__ === -1);
        const halfOfCollapseIconWidth = 8;
        const collapseIconOffset = isTopLevelNode ? 0 : halfOfCollapseIconWidth;
        const leafNodeOffset = 0;
        const hasChildren = visualTreeNode.raw[hasChildrenField.value];
        const styleObject = visualTreeNode.visible
            ? ({
                left: `${visualTreeNode.layer * 10 + visualTreeNode.layer * collapseIconOffset + leafNodeOffset}px`,
                // paddingBottom: handlePaddingBottomReturnValue(visualTreeNode.index),
                background: getPaddingBottomLength(visualTreeNode.index),
                display: 'flex',
                width: '100%',
                position: 'relative',
                paddingLeft: '0.75rem'
            } as Record<string, any>)
            : {
                display: 'none'
            };
        if (!hasChildren) {
            styleObject.paddingLeft = '2rem';
        }
        return styleObject;
    }

    return { hierarchyCellContentStyle, handleCollapsedPaddingBottom };
}
