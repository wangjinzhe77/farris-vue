import { Ref } from "vue";
import { ColumnContext, DataColumn, UseCellContent, VisualDataCell } from "../types";

export function useCellContentStyle(context: Ref<ColumnContext>): UseCellContent {
    // const columnsMap = context.value;
    function cellContentPosition(cell: VisualDataCell): Record<string, any> {
        // 水平位置 靠左 居中 靠右
        // 垂直位置 置顶  居中  置底
        const styleObject = {
        } as Record<string, any>;
        return styleObject;
    }

    function cellContentClass(cell: VisualDataCell) {
        const { valign, align } = cell;
        return {
            'd-flex': true,
            'h-100': true,
            'w-100': true,
            'justify-content-start': align !== 'center' && align !== 'right',
            'justify-content-center': align === 'center',
            'justify-content-right': align === 'right',
            'align-items-start': valign !== 'middle' && valign !== 'bottom',
            'align-items-center': valign === 'middle',
            'align-items-end': valign === 'bottom'
        } as Record<string, boolean>;
    }

    function cellContentClassWithEllipsis(cell: VisualDataCell) {
        const { align, showEllipsis } = cell;
        return {
            'w-100': true,
            'text-left': align !== 'center' && align !== 'right',
            'text-center': align === 'center',
            'text-right': align === 'right',
            'text-truncate': showEllipsis,
        } as Record<string, boolean>;
    }
    return { cellContentPosition, cellContentClass, cellContentClassWithEllipsis };
}
