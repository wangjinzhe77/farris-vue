/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, ref, watch } from 'vue';
import { DataViewOptions, UseDataViewContainerStyle } from '../types';

export function useDataViewContainerStyle(props: DataViewOptions): UseDataViewContainerStyle {
    const minHeight = ref(props.minHeight);
    const minWidth = ref(props.minWidth);
    const height = ref(props.height);
    const width = ref(props.width);

    const actualHeight = computed(() => {
        return height.value <= 0 ? 0 : Math.max(height.value, minHeight.value);
    });

    const actualWidth = computed(() => {
        return width.value <= 0 ? 0 : Math.max(width.value, minWidth.value);
    });

    watch([
        () => props.height,
        () => props.width,
        () => props.minHeight,
        () => props.minWidth
    ], ([newHeight, newWidth, newMinHeight, newMinWidth]) => {
        minHeight.value = Number(newMinHeight);
        minWidth.value = Number(newMinWidth);
        height.value = Number(newHeight);
        width.value = Number(newWidth);
    });

    const containerStyleObject = computed(() => {
        const styleObject = {
            height: actualHeight.value > 0 ? `${actualHeight.value}px` : (props.fit ? '100%' : ''),
            width: actualWidth.value > 0 ? `${actualWidth.value}px` : (props.fit ? '100%' : ''),
            flex: '1 1 0'
        } as Record<string, any>;
        return styleObject;
    });

    return { containerStyleObject };
}
