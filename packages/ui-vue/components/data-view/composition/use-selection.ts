/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref, SetupContext, computed, ref, watch } from 'vue';
import { DataViewOptions, MultiSelectMode, SelectionOptions, UseDataView, UseIdentify, UseSelection, VisualData } from './types';

export function useSelection(
    props: DataViewOptions,
    dataView: UseDataView,
    identifyComposition: UseIdentify,
    visibleDatas: Ref<VisualData[]>,
    context: SetupContext
): UseSelection {
    const { idField } = identifyComposition;
    const currentSelectedDataId = ref('');
    const enableSelectRow = computed(() => props.selection.enableSelectRow);
    const enableMultiSelect = ref(props.selection.multiSelect ?? false);
    const multiSelectMode: Ref<MultiSelectMode> = ref(props.selection.multiSelectMode as MultiSelectMode);
    const showCheckBox = ref(props.selection.showCheckbox);
    const showSelectAll = ref(props.selection.showSelectAll);
    const selectedValues = ref(props.selectionValues);

    watch(() => props.selection.showSelectAll, (newShowSelectAllValue, oldShowSelectAllValue) => {
        if (newShowSelectAllValue !== oldShowSelectAllValue) {
            showSelectAll.value = newShowSelectAllValue;
        }
    });
    function getSelectedItems() {
        return dataView.getSelectionItems(selectedValues.value);
    }

    function getSelectedVisualDataItems(): VisualData[] {
        const visibleDataItemMap = visibleDatas.value.reduce((result: Map<string, VisualData>, visibleData: VisualData) => {
            result.set(visibleData.raw[idField.value], visibleData);
            return result;
        }, new Map<string, VisualData>);
        const selectedVisualDataItems = selectedValues.value
            .map((selectedDataId: string) => visibleDataItemMap.get(selectedDataId) as VisualData)
            .filter((selectedVisibleData: VisualData) => !!selectedVisibleData);
        return selectedVisualDataItems;
    }

    const inMultiSelect = computed(() => enableSelectRow.value && enableMultiSelect.value);

    const isSelectingHirarchyItem = computed(() => !!props.hierarchy);

    const multiSelectOnlyOnCheck = computed(() => {
        const dependsAndHasCheckBox = showCheckBox.value && multiSelectMode.value === 'DependOnCheck';
        return inMultiSelect.value && dependsAndHasCheckBox;
    });

    const multiSelectOnClickRowWithShift = computed(() => {
        return inMultiSelect.value && !showCheckBox.value;
    });

    const multiSelectOnClickRow = computed(() => {
        return inMultiSelect.value && showCheckBox.value && multiSelectMode.value === 'OnCheckAndClick';
    });

    const shouldToggleSelectOnClickRow = computed(() => multiSelectOnClickRow.value);

    function emitSelectionChanged() {
        const selectedItems = dataView.getSelectionItems(selectedValues.value);
        context.emit('selectionChange', selectedItems);
    }

    function resetSelection() {
        visibleDatas.value.forEach((visualDataToBeReset: VisualData) => {
            visualDataToBeReset.checked = false;
            visualDataToBeReset.indeterminate = false;
            visualDataToBeReset.raw.__fv_checked__ = false;
            visualDataToBeReset.raw.__fv_indeterminate__ = false;

        });
        const selectedVisualDataItems = getSelectedVisualDataItems();
        selectedVisualDataItems.forEach((selectedData: VisualData) => {
            selectedData.checked = true;
            selectedData.raw.__fv_checked__ = true;
        });
        // 导致下拉面板不能展开，暂注释
        // emitSelectionChanged();
    }

    watch(() => visibleDatas.value, () => {
        resetSelection();
    });

    watch(() => props.selectionValues, (newValues: any[]) => {
        selectedValues.value = newValues;
        resetSelection();
    }, { immediate: true });

    watch(() => props.selection, (newSelection: SelectionOptions) => {
        // enableSelectRow.value = !!newSelection.enableSelectRow;
        enableMultiSelect.value = !!newSelection.multiSelect;
        multiSelectMode.value = newSelection.multiSelectMode || 'DependOnCheck';
        showCheckBox.value = !!newSelection.showCheckbox;
    });

    const shouldClearSelection = computed(() => {
        return !enableMultiSelect.value;
    });

    function getKey(item: any) {
        return item.raw[idField.value];
    }

    function findIndexInSelectedItems(item: any): number {
        const selectItemIndex = selectedValues.value.findIndex((selectedDataId: string) => {
            return selectedDataId === getKey(item);
        });
        return selectItemIndex;
    }

    function clearSelection() {
        const selectedVisualDataItems = getSelectedVisualDataItems();
        selectedVisualDataItems.forEach((selectedData: VisualData) => { selectedData.checked = false; });
        const selectedItems = dataView.getSelectionItems(selectedValues.value);
        selectedItems.forEach((selectionItem: any) => { selectionItem.__fv_checked__ = false; });
        selectedValues.value = [];
        currentSelectedDataId.value = '';
    }

    function selectWithoutRow(visualDataToBeSelected: VisualData) {
        const dataItem = visualDataToBeSelected.raw;
        dataItem.__fv_checked__ = true;
        dataItem.__fv_indeterminate__ = false;
        const uniqueValueSet = new Set(selectedValues.value);
        uniqueValueSet.add(dataItem[idField.value]);
        selectedValues.value = Array.from(uniqueValueSet.values());
        visualDataToBeSelected.checked = true;
        visualDataToBeSelected.indeterminate = false;
    }

    function unSelectWithoutRow(visualDataToBeUnSelected: VisualData) {
        const dataItem = visualDataToBeUnSelected.raw;
        dataItem.__fv_checked__ = false;
        dataItem.__fv_indeterminate__ = false;
        selectedValues.value = selectedValues.value
            .filter((seletedDataId: string) => seletedDataId !== dataItem[idField.value]);
        visualDataToBeUnSelected.checked = false;
        visualDataToBeUnSelected.indeterminate = false;
    }

    /** 勾选指定节点 */
    function select(visualDataToBeSelected: VisualData) {
        selectWithoutRow(visualDataToBeSelected);
        currentSelectedDataId.value = visualDataToBeSelected.raw[idField.value];
    }

    /** 取消勾选指定节点 */
    function unSelect(visualDataToBeUnSelected: VisualData) {
        unSelectWithoutRow(visualDataToBeUnSelected);
        currentSelectedDataId.value = '';
    }

    /** 灰选指定节点, 用于选择树形结构数据 */
    function indeterminate(visualDataToBeIndeterminate: VisualData) {
        const dataItem = visualDataToBeIndeterminate.raw;
        dataItem.__fv_checked__ = false;
        dataItem.__fv_indeterminate__ = true;
        selectedValues.value = selectedValues.value
            .filter((seletedDataId: string) => seletedDataId !== dataItem[idField.value]);
        visualDataToBeIndeterminate.checked = false;
        visualDataToBeIndeterminate.indeterminate = true;
    }

    /** 仅勾选  不选中当前行 */
    function toggleSelectItemWithoutRow(visualData: VisualData) {
        if (shouldClearSelection.value) {
            clearSelection();
        }
        visualData.checked ? unSelectWithoutRow(visualData) : selectWithoutRow(visualData);
        emitSelectionChanged();
    }

    function toggleSelectItem(visualData: VisualData) {
        if (shouldClearSelection.value) {
            clearSelection();
        }
        visualData.checked ? unSelect(visualData) : select(visualData);
        emitSelectionChanged();
    }

    function selectItem(visualData: VisualData) {
        const selectingNewItem = visualData.raw[idField.value] !== currentSelectedDataId.value;
        if (selectingNewItem) {
            // 不取消其他已经勾选的行数据
            clearSelection();
            select(visualData);
            emitSelectionChanged();
        }
    }

    function selectItemByClickRow(visualData: VisualData) {
        const selectingNewItem = visualData.raw[idField.value] !== currentSelectedDataId.value;
        if (selectingNewItem) {
            // 不取消其他已经勾选的行数据
            // clearSelection();
            select(visualData);
            emitSelectionChanged();
        }
    }

    function selectItemById(dataItemId: string) {
        const visibleItemToBeSelected = visibleDatas.value.find((visibleData: VisualData) => {
            return visibleData.raw[idField.value] === dataItemId;
        }) as VisualData;
        if (visibleItemToBeSelected) {
            selectItem(visibleItemToBeSelected);
        }
    }

    function selectItemByIds(dataItemIds: string[]) {
        const visibleItemToBeSelected = visibleDatas.value.filter((visibleData: VisualData) => {
            return dataItemIds.includes(visibleData.raw[idField.value]);
        }) as VisualData[];
        if (visibleItemToBeSelected && visibleItemToBeSelected.length) {
            clearSelection();
            visibleItemToBeSelected.forEach((visibleData: VisualData) => {
                select(visibleData);
            });
            emitSelectionChanged();
        }
    }


    /** 勾选所有节点 */
    function selectAll() {
        // currentSelectedDataId.value = '';
        visibleDatas.value.forEach((visualDataToBeSelected: VisualData) => select(visualDataToBeSelected));
        // 选中所有节点时，不需要指定当前选中节点
        emitSelectionChanged();
    }

    /** 取消勾选所有节点 */
    function unSelectAll() {
        selectedValues.value.splice(0, selectedValues.value.length);
        // currentSelectedDataId.value = '';
        visibleDatas.value.forEach((visualDataToBeUnSelected: VisualData) => {
            const dataItem = visualDataToBeUnSelected.raw;
            dataItem.__fv_checked__ = false;
            dataItem.__fv_indeterminate__ = false;
            visualDataToBeUnSelected.checked = false;
            visualDataToBeUnSelected.indeterminate = false;
        });
        emitSelectionChanged();
    }

    function getSelectionRow(): VisualData {
        return visibleDatas.value.find((dataItem: VisualData) => {
            return dataItem.raw[idField.value] === currentSelectedDataId.value;
        }) as VisualData;
    }

    return {
        clearSelection,
        currentSelectedDataId,
        enableMultiSelect,
        enableSelectRow,
        findIndexInSelectedItems,
        getSelectedItems,
        getSelectionRow,
        indeterminate,
        isSelectingHirarchyItem,
        multiSelectMode,
        multiSelectOnClickRow,
        multiSelectOnClickRowWithShift,
        multiSelectOnlyOnCheck,
        select,
        selectAll,
        selectedValues,
        selectItem,
        selectItemById,
        selectItemByIds,
        showCheckBox,
        showSelectAll,
        toggleSelectItem,
        toggleSelectItemWithoutRow,
        unSelect,
        unSelectAll
    };
}
