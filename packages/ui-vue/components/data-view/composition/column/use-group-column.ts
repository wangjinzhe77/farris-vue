/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref, ref } from 'vue';
import { ColumnContext, ColumnGroupItem, DataColumn, DataViewOptions, HeaderCell, HeaderCellStatus } from '../types';

interface GroupColumnContext {
    groupedCells: Map<string, HeaderCell>;
    fieldToRootMap: Map<string, HeaderCell>;
}

export function useGroupColumn(props: DataViewOptions, columnRenderContext: Ref<ColumnContext>) {
    const columnGroups: Ref<ColumnGroupItem[] | undefined> = ref(props.columnOption?.groups);

    function buildGroupCell(
        columnGroupItem: ColumnGroupItem,
        originalHeaderCells: Map<string, HeaderCell>,
        parentCell: HeaderCell | null,
        rootCell: HeaderCell | null,
        context: GroupColumnContext
    ): HeaderCell {
        const groupCell: HeaderCell = {
            actualWidth: 0,
            children: [],
            depth: 1,
            field: columnGroupItem.field,
            layer: 1,
            left: 0,
            parent: parentCell,
            resizable: true,
            title: columnGroupItem.title || '',
            status: HeaderCellStatus.none,
            popoverRef: ref<any>(),
            showPopover: false,
            column: null,
            sortType: 'none'
        };
        const actualRootCell = rootCell || groupCell;
        if (columnGroupItem.group) {
            let maxDepth = 1;
            columnGroupItem.group.reduce((parentGroupCell: HeaderCell, item: string | ColumnGroupItem) => {
                if (typeof item === 'string') {
                    const columnCell = originalHeaderCells.get(item);
                    if (columnCell) {
                        context.fieldToRootMap.set(item, actualRootCell);
                        parentGroupCell.actualWidth += columnCell.actualWidth;
                        parentGroupCell.children.push(columnCell);
                    }
                } else {
                    const subGroupCell = buildGroupCell(item, originalHeaderCells, parentGroupCell, actualRootCell, context);
                    maxDepth = Math.max(maxDepth, subGroupCell.depth);
                    parentGroupCell.actualWidth += subGroupCell.actualWidth;
                    parentGroupCell.children.push(subGroupCell);
                }
                return parentGroupCell;
            }, groupCell);
            groupCell.depth += maxDepth;
        }
        columnRenderContext.value.headerDepth = Math.max(columnRenderContext.value.headerDepth, groupCell.depth);
        return groupCell;
    }

    function buildGroupContext(originalHeaderCells: Map<string, HeaderCell>): GroupColumnContext {
        const context: GroupColumnContext = {
            groupedCells: new Map<string, HeaderCell>(),
            fieldToRootMap: new Map<string, HeaderCell>()
        };
        if (columnGroups.value) {
            columnGroups.value.reduce((groupContext: GroupColumnContext, item: ColumnGroupItem) => {
                const groupCell = buildGroupCell(item, originalHeaderCells, null, null, context);
                groupContext.groupedCells.set(item.field, groupCell);
                return groupContext;
            }, context);
        }
        return context;
    }

    function getGroupedCellLeft(mergedCell: HeaderCell[]) {
        if (!mergedCell || mergedCell.length === 0) {
            return 0;
        }
        const firstCell = mergedCell[0];
        const isFirstCellHasChildren = firstCell.children && firstCell.children.length > 0;
        if (isFirstCellHasChildren) {
            firstCell.left = getGroupedCellLeft(firstCell.children);
        }
        return firstCell.left;
    }

    function resetGroupedCellsLayer(mergedCell: HeaderCell[], parent?: HeaderCell) {
        const maxGroupDepth = columnRenderContext.value.headerDepth;
        mergedCell.forEach((headerCell: HeaderCell) => {
            headerCell.layer = parent ? parent.layer + 1 : 1;
            const isDetail = headerCell.children && headerCell.children.length === 0;
            headerCell.depth = isDetail ? maxGroupDepth - (headerCell.layer - 1) : 1;
            headerCell.left = isDetail ? headerCell.left : getGroupedCellLeft(headerCell.children);
            if (headerCell.children) {
                resetGroupedCellsLayer(headerCell.children, headerCell);
            }
        });
    }

    function mergeGroupedCells(
        originalHeaderCells: Map<string, HeaderCell>,
        groupContext: GroupColumnContext
    ): Map<string, HeaderCell> {
        const mergedCells = new Map<string, HeaderCell>();
        originalHeaderCells.forEach((gridHeaderCell: HeaderCell) => {
            const rootGroupCellToField = groupContext.fieldToRootMap.get(gridHeaderCell.field);
            const hasBeenGrouped = rootGroupCellToField != null;
            if (hasBeenGrouped && !mergedCells.has(rootGroupCellToField.field)) {
                mergedCells.set(rootGroupCellToField.field, rootGroupCellToField);
            }
            if (!hasBeenGrouped) {
                mergedCells.set(gridHeaderCell.field, gridHeaderCell);
            }
        });
        resetGroupedCellsLayer(Array.from(mergedCells.values()));
        return mergedCells;
    }

    function getGridHeaderCells(columns: DataColumn[]): Map<string, HeaderCell> {
        let headerCells = new Map<string, HeaderCell>();

        let cellPosition = 0;
        columns.reduce((previousHeaderCells: Map<string, HeaderCell>, dataGridColumn: DataColumn) => {
            let headerStatus = HeaderCellStatus.none;
            // headerStatus = dataGridColumn.sortable ? headerStatus | HeaderCellStatus.sortable : headerStatus;
            headerStatus = dataGridColumn.filterable ? headerStatus | HeaderCellStatus.filterable : headerStatus;
            headerStatus = dataGridColumn.sort && dataGridColumn.sort !== 'none' ?
                headerStatus | HeaderCellStatus.sorted | (
                    dataGridColumn.sort === 'asc' ? HeaderCellStatus.ascending : HeaderCellStatus.descending
                )
                : headerStatus;
            const mapKey = dataGridColumn.field;
            previousHeaderCells.set(mapKey, {
                actualWidth: dataGridColumn.actualWidth || 0,
                children: [],
                depth: 1,
                layer: 1,
                left: cellPosition,
                field: mapKey,
                parent: null,
                resizable: dataGridColumn.resizable===undefined?true:dataGridColumn.resizable,
                title: dataGridColumn.title,
                status: headerStatus,
                popoverRef: ref<any>(),
                showPopover: false,
                column: dataGridColumn,
                filterValue: null,
                sortType: dataGridColumn.sort || 'none',
                showSetting: dataGridColumn.showSetting
            } as HeaderCell);
            cellPosition += dataGridColumn.actualWidth || 0;
            return previousHeaderCells;
        }, headerCells);

        if (columnGroups.value) {
            const groupContext = buildGroupContext(headerCells);
            headerCells = mergeGroupedCells(headerCells, groupContext);
        }

        return headerCells;
    }

    return { getGridHeaderCells };
}

