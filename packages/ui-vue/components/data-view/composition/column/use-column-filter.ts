import { Ref } from "vue";
import {
    DataFilter, HeaderCell, FilterFunction, FilterRelation, UseDataView,
    UseColumnFilter, UseFilterHistory, UseVirtualScroll
} from "../types";
import booleanFitler from '../../components/column-filter/boolean-filter-editor.component';
import dateFilter from '../../components/column-filter/date-filter-editor.component';
import numericFilter from '../../components/column-filter/numeric-filter-editor.component';
import textFiler from '../../components/column-filter/text-filter-editor.component';
import enumFilter from '../../components/column-filter/enum-filter-editor.component';
import listFiler from '../../components/column-filter/list-filter-editor.component';

export function useColumnFilter(
    gridContentRef: Ref<any>,
    rightFixedGridContentRef: Ref<any>,
    dataView: UseDataView,
    useFilterHistoryComposition: UseFilterHistory,
    useVirtualScrollComposition: UseVirtualScroll
): UseColumnFilter {

    const typeToEditorName = new Map<string, string>([
        ['boolean', 'boolean-filter'],
        ['date', 'date-filter'],
        ['datetime', 'date-filter'],
        ['number', 'numeric-filter'],
        ['string', 'text-filter'],
        ['text', 'text-filter'],
        ['enum', 'enum-filter'],
        ['reference', 'list-filter']
    ]);

    const editorMap = new Map<string, (headerCell: HeaderCell, gridContentRef: Ref<any>,
        rightFixedGridContentRef: Ref<any>,
        dataView: UseDataView,
        useFilterHistoryComposition: UseFilterHistory,
        useVirtualScrollComposition: UseVirtualScroll) => any>([
            ['boolean-filter', booleanFitler],
            ['date-filter', dateFilter],
            ['enum-filter', enumFilter],
            ['list-filter', listFiler],
            ['numeric-filter', numericFilter],
            ['text-filter', textFiler]
        ]);

    function getFilterEditor(headerCell: HeaderCell) {
        const fieldType = headerCell.column?.dataType || 'string';
        const editorName = typeToEditorName.get(fieldType) || 'text-editor';

        const editor = editorMap.get(editorName);
        if (editor) {
            return editor(headerCell, gridContentRef, rightFixedGridContentRef,
                dataView, useFilterHistoryComposition, useVirtualScrollComposition);
        }
    }

    return { getFilterEditor };
}
