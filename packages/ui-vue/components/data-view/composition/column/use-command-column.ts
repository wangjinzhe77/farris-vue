/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref, ref } from 'vue';
import { DataColumn, DataViewOptions, UseCommandColumn } from '../types';

export function useCommandColumn(props: DataViewOptions): UseCommandColumn {
    const defaultColumnWidth = 120;
    const enableCommands = ref(props.commandOption.enable || false);
    const commands = ref(props.commandOption.commands || []);

    function applyCommands(columns: Ref<DataColumn[]>) {
        if (enableCommands.value) {
            const hasCommandColumn = columns.value.findIndex((column: DataColumn) => column.dataType === 'commands') > -1;
            if (!hasCommandColumn) {
                const commandColumn = {
                    field: '__commands__',
                    title: '操作',
                    width: defaultColumnWidth,
                    fixed: 'right',
                    dataType: 'commands',
                    commands: commands.value,
                    visible: true
                } as DataColumn;
                columns.value.push(commandColumn as DataColumn);
            }
        }
    }

    return { applyCommands };
}
