import { computed, ref, watch } from "vue";
import { DataViewOptions, UseHierarchy } from "../types";

export function useHierarchy(props: DataViewOptions): UseHierarchy {
    const showCheckBox = ref(props.selection.showCheckbox || false);
    const collapseField = computed(() => props.hierarchy?.collapseField || '__fv_collapse__');
    const hasChildrenField = computed(() => props.hierarchy?.hasChildrenField || '__fv_hasChildren__');
    // const layerField = ref(props.hierarchy.layerField || 'layer');
    const parentIdField = computed(() => props.hierarchy?.parentIdField || 'parent');

    const shouldShowCheckBox = computed(() => {
        return props.hierarchy && props.selection.showCheckbox;
        // &&props.selection.enableSelectRow && props.selection.multiSelect;
    });

    const shouldShowIcon = computed(() => {
        return (props.treeNodeIconsData.fold || props.iconField) && props.showTreeNodeIcons;
    });

    return { collapseField, hasChildrenField, parentIdField, shouldShowCheckBox, shouldShowIcon };
}
