import { ComputedRef, Ref, ref } from 'vue';
import { DataViewOptions, VisualData, VisualDataCell } from '../types';

/** 用于处理树节点图标相关功能 */
export function useTreeNodeIcon(props: DataViewOptions, treeNodeIconsData: Ref<any>, hasChildrenField: ComputedRef<string>): any {
    const treeNodeIconField = ref(props.iconField);

    /** 显示图标 */
    function treeNodeIconsClass(visualTreeNode: VisualData, visualTreeNodeCell: VisualDataCell) {

        if (treeNodeIconField.value) {
            return visualTreeNode.raw[props.iconField];
        }
        let styleObject = '';
        const isLeafNode = visualTreeNode.raw[hasChildrenField.value] !== true;
        // 叶子节点
        if (isLeafNode) {
            styleObject = treeNodeIconsData.value.leafnodes;
        } else {
            styleObject = visualTreeNode.collapse ? treeNodeIconsData.value.fold : treeNodeIconsData.value.unfold;
        }
        return styleObject;
    };
    // 返回包含获取可视化树节点函数的对象
    return { treeNodeIconsClass };
}
