import { Ref, SetupContext } from "vue";
import { DataViewOptions, UseDataView, UseHierarchy, UseToggleHierarchyItem, UseVirtualScroll, UseVisualData, UseVisualDataBound, VisualData } from "../types";

export function useToggleHierarchyItem(
    props: DataViewOptions,
    context: SetupContext,
    visibleDatas: Ref<VisualData[]>,
    useDataViewComposition: UseDataView,
    useHierarchyComposition: UseHierarchy,
    useVirtualScrollComposition: UseVirtualScroll,
    useVisualDataComposition: UseVisualData,
    useVisualDataBoundComposition: UseVisualDataBound
): UseToggleHierarchyItem {
    const { dataView, fold, hasRealChildren, unFold, reOrderVisibleIndex } = useDataViewComposition;
    const { collapseField, hasChildrenField } = useHierarchyComposition;
    const { reCalculateVisualDataRows, updateVirticalScroll } = useVirtualScrollComposition;
    const { getVisualData, minVisibleRowIndex } = useVisualDataComposition;
    const { updateRowPosition } = useVisualDataBoundComposition;

    /** 处理子节点部分折叠的情况,跳过折叠的子节点
     * @param i 点击的节点的index
     */
    function partiallyCollapseStatus(currentDataViewItemIndex: number, dataViewItems: any[]) {
        const currentDataItem = dataViewItems[currentDataViewItemIndex];
        const currentLayer = currentDataItem.__fv_parents__ ? currentDataItem.__fv_parents__?.size : 0;
        let i = currentDataViewItemIndex + 1;
        while (i < dataViewItems.length) {
            const dataItem = dataViewItems[i];
            const dataItemLayer = dataItem.__fv_parents__ ? dataItem.__fv_parents__?.size : 0;
            if (dataItemLayer > currentLayer) {
                i++;
            } else {
                break;
            }
        }
        return i - 1;
    }

    /** 处理选择的节点的子节点
     * @param id 选中节点的index，即id
     * @param parentIdOfCurNode 选中节点的父级id
     */
    function toggleTreeNodeCollapseStatus(id: number, parentIdOfCurNode: number) {
        const currentDataItem = dataView.value[id];
        // 选中节点的折叠状态改变
        currentDataItem[collapseField.value] = !currentDataItem[collapseField.value];
        currentDataItem[hasChildrenField.value] = hasRealChildren(currentDataItem);
        /** 选中节点的折叠状态 */
        const currentNodeCollapseStatus = currentDataItem[collapseField.value];
        // 选中节点的折叠状态改变
        /** 开关，判断是否开始切换为非子节点，若不是，则为true */
        let switchForTreeNodes = false;
        // 从当前节点索引开始往后比较
        let i = id;
        while (i < dataView.value.length) {
            /** 每个节点 */
            const dataViewItem = dataView.value[i];
            /** 每个节点的父级Id */
            // const visibleDatasItemParentId = Number(visibleDatasItem.raw.__fv_parent_index__ || -1);
            const visibleDatasItemParentId = typeof dataViewItem.__fv_parent_index__ === 'number' ?
                Number(dataViewItem.__fv_parent_index__) : -1;
            // 判断是否为选中节点的子节点
            if (visibleDatasItemParentId > parentIdOfCurNode) {
                // 选中节点的子节点，及子节点的子节点等，都需要修改显示状态
                dataViewItem.__fv_visible__ = !currentNodeCollapseStatus;
                if (dataViewItem[collapseField.value]) {
                    i = partiallyCollapseStatus(i, dataView.value);
                }
                switchForTreeNodes = true;
            } else if (switchForTreeNodes) {
                i++;
                break;
            }
            i++;
        }
        reOrderVisibleIndex();
        // let indexToUpdatePosition = id;
        // while (indexToUpdatePosition < visibleDatas.value.length) {
        //     const visibleDatasItem = visibleDatas.value[indexToUpdatePosition] as VisualData;
        //     updateRowPosition(visibleDatasItem, visibleDatasItem.raw);
        //     indexToUpdatePosition++;
        // }
    }

    /**  处理树节点单元格点击事件 */
    function toggleTreeNode(visualTreeNode: VisualData) {
        const hasChildren = visualTreeNode.raw[hasChildrenField.value];
        if (hasChildren) {
            // visualTreeNode.collapse ? unFold(visualTreeNode) : fold(visualTreeNode);
            // visibleDatas.value = getVisualData(0, dataView.value.length - 1);
            const parentIndex = typeof visualTreeNode.raw.__fv_parent_index__ === 'number' ?
                Number(visualTreeNode.raw.__fv_parent_index__) : -1;
            const currentDataViewItemIndex = Number(visualTreeNode.raw.__fv_index__);
            toggleTreeNodeCollapseStatus(currentDataViewItemIndex, parentIndex);
            reCalculateVisualDataRows();
            updateVirticalScroll();
        }
        context.emit('expandNode', { row: visualTreeNode });
    }

    return { toggleTreeNode };
}
