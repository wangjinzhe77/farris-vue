

import { Ref, SetupContext, ref } from 'vue';
import { DataViewOptions, UseSelection, UseSelectHierarchyItem, VisualData, VisualDataCell, UseIdentify, UseDataView } from '../types';
import { cloneDeep } from 'lodash-es';
import { useCascade } from './use-cascade';

/** 用于处理树自动勾选子节点和优先返回父节点相关功能 */
export function useSelectHierarchyItem(
    props: DataViewOptions,
    visibleDatasValue: Ref<VisualData[]>,
    useIdentifyComposition: UseIdentify,
    useSelectionCompostion: UseSelection,
    context: SetupContext,
): UseSelectHierarchyItem {
    const { select, unSelect, indeterminate, getSelectedItems, selectedValues, currentSelectedDataId } = useSelectionCompostion;
    const { autoCheckChildren, autoCheckParent, interactAfterCheckHierarchyItem } = useCascade(props, visibleDatasValue, useIdentifyComposition, context);
    const { idField } = useIdentifyComposition;
    /** 选中值 */
    let outputValue: any;
    /** 记录选中值（选中父节点时，不自动选中子节点）*/
    const recordCheckedValues: any = [];
    /** 找出已经checked的节点 */
    function fetchCheckedNodes(): number[] {
        /** 已经checked的节点 */
        return visibleDatasValue.value.filter((visualData: VisualData) => !!visualData.checked)
            .map((visualData: VisualData) => visualData.raw.__fv_index__ as number);
    }
    /** isBInA:判断B中的每个元素是否都在A中 */
    function isBInA(A: any, B: any) {
        for (let i = 0; i < B.length; i++) {
            if (!A.includes(B[i])) {
                return false;
            }
        }
        return true;
    }
    /** A-B:求差 */
    function getDifference(A: any, B: any) {
        const C = A.filter((element: any) => {
            return B.indexOf(element) === -1;
        });
        return C;
    }

    /** 优先返回父节点 */
    function trimmedParentValue(checkedGroup: any, outputValue: any) {
        let trimmedOutputValue: any = [];
        const subset: any = [];
        outputValue.forEach((item: any) => {
            if (!isBInA(subset, [item.__fv_index__])) {
                const isSubset = isBInA(checkedGroup, visibleDatasValue.value[item.__fv_index__].raw.__fv_children_index__);
                if (isSubset) {
                    trimmedOutputValue.push(item.__fv_index__);
                    subset.push(...visibleDatasValue.value[item.__fv_index__].raw.__fv_children_index__);
                } else {
                    trimmedOutputValue.push(item.__fv_index__);
                }
            }
        });
        // 排序
        const uniqueSet = new Set(trimmedOutputValue);
        const outputArray = [...uniqueSet];
        trimmedOutputValue = outputArray.sort((a, b) => Number(a) - Number(b));
        return trimmedOutputValue;
    }
    /** 优先返回父节点值（cascade===true） */
    function returnParentValue(trimmedOutputValue: number[]) {
        outputValue = [];
        trimmedOutputValue.forEach((checkedIndex: any) => {
            outputValue.push(cloneDeep(visibleDatasValue.value[checkedIndex].raw));
        });
        return outputValue;
    }
    /** 返回父子选中值（cascade===false */
    function returnBothValue() {
        const checkedGroup = fetchCheckedNodes();
        outputValue = [];
        checkedGroup.forEach((checkedIndex: number) => {
            outputValue.push(cloneDeep(visibleDatasValue.value[checkedIndex].raw));
        });
        if (outputValue.length !== 0) {
            // 值包含父子节点值
            context.emit('outputValue', outputValue);
            if (props.mergeCascadeValues) {
                // cascade为true，代表值优先返回父节点的值
                const trimmedOutputValue = trimmedParentValue(checkedGroup, outputValue);
                outputValue = returnParentValue(trimmedOutputValue);
            }
        }

    }
    /** 根据子节点状态，自动重置当前节点状态 */
    function resetCurrentNodeBasedOnChildsStatus(visualData: VisualData) {
        const checkedNodesIndex = fetchCheckedNodes();
        const hasCommonValue = visualData.raw.__fv_children_index__.some((child: number) => checkedNodesIndex.includes(child));
        if (hasCommonValue) {
            // 处理点击的节点，设置当前的checked的值
            const checkedValue = !!visualData.checked;
            if (checkedValue !== true) {
                indeterminate(visualData);
            }
        }
    }

    /** 记录选择的值 */
    function recordCheckedValue(visualTreeNodeCell: VisualDataCell) {
        // 如果b在a中，则删除b,否则加入b
        const index = recordCheckedValues.indexOf(visualTreeNodeCell.index);
        if (index !== -1) {
            recordCheckedValues.splice(index, 1);
        } else {
            recordCheckedValues.push(visualTreeNodeCell.index);
        }
        return recordCheckedValues;
    }
    /** 处理树节点可勾选状态下的单元格点击事件 */
    // function onClickTreeNodeSelectableCell(visualTreeNode: VisualData) {
    //     // handleCurrentTreeNode(!!visualTreeNode.checked, visualTreeNode);
    //     // 根据autoCheckChildren判断是否修改子节点；autoCheckChildren 不通过父勾选子
    //     if (props.autoCheckChildren) {
    //         handleTreeNodeChildsIcon(visualTreeNode);
    //         handleTreeNodeParentsIcon(visualTreeNode);
    //     } else {
    //         // const recordCheckedValues = recordCheckedValue(visualTreeNodeCell);
    //         handleTreeNodeParentsIconManually(visualTreeNode);
    //     }
    //     // returnBothValue();
    // }

    /** 不通过父勾选子,父节点处理方法 */
    function toInfluenceParentsCheckStatusManually(visualData: VisualData) {
        let parentIndex = visualData.raw.__fv_parent_index__;
        let checkedGroup = fetchCheckedNodes();
        // 检索已经勾选的项是否为其子节点，若为子节点，则重新置为中间状态
        resetCurrentNodeBasedOnChildsStatus(visualData);
        while (parentIndex !== undefined) {
            checkedGroup = fetchCheckedNodes();
            const parentVisualData = visibleDatasValue.value[parentIndex];
            // 计算当前子节点与已选节点的差值
            const childsLeft = getDifference(parentVisualData.raw.__fv_children_index__, checkedGroup);
            // 若完全不同，则为空状态
            if (childsLeft.length === parentVisualData.raw.__fv_children_index__.length && childsLeft.length !== 0) {
                if (parentVisualData.checked !== true) {
                    unSelect(parentVisualData);
                }
            }
            // 有重合但不完全相同，则为中间状态
            else if (childsLeft.length < parentVisualData.raw.__fv_children_index__.length) {
                if (parentVisualData.checked !== true) {
                    indeterminate(parentVisualData);
                }
            }
            parentIndex = parentVisualData.raw.__fv_parent_index__;
        }
    }

    /** 修改父节点选中图标，如indeterminate图标代表部分选中情况 */
    // function toInfluenceParentsCheckStatus(visualData: VisualData) {
    //     let parentIndex = visualData.raw.__fv_parent_index__;
    //     while (parentIndex !== undefined) {
    //         const checkedGroup = fetchCheckedNodes();
    //         const parentVisualData = visibleDatasValue.value[parentIndex];
    //         // 计算当前子节点与已选节点的差值
    //         const childsLeft = getDifference(parentVisualData.raw.__fv_children_index__, checkedGroup);
    //         // 若完全不同，则为空状态
    //         if (childsLeft.length === parentVisualData.raw.__fv_children_index__.length && childsLeft.length !== 0) {
    //             unSelect(parentVisualData);
    //         }
    //         // 若完全相同，则为勾选状态
    //         else if (childsLeft.length === 0) {
    //             select(parentVisualData);
    //         }
    //         // 有重合但不完全相同，则为中间状态
    //         else if (childsLeft.length < parentVisualData.raw.__fv_children_index__.length) {
    //             indeterminate(parentVisualData);
    //         }
    //         parentIndex = parentVisualData.raw.__fv_parent_index__;
    //     }
    // }
    function selectDataItem(dataItem: any) {
        dataItem.__fv_checked__ = true;
        dataItem.__fv_indeterminate__ = false;
        const uniqueValueSet = new Set(selectedValues.value);
        uniqueValueSet.add(dataItem[idField.value]);
        selectedValues.value = Array.from(uniqueValueSet.values());
        currentSelectedDataId.value = dataItem[idField.value];
    }
    /** 取消勾选指定节点 */
    function unSelectDataItem(dataItem: any) {
        dataItem.__fv_checked__ = false;
        dataItem.__fv_indeterminate__ = false;
        selectedValues.value = selectedValues.value
            .filter((seletedDataId: string) => seletedDataId !== dataItem[idField.value]);
        currentSelectedDataId.value = '';
    }

    /** 灰选指定节点, 用于选择树形结构数据 */
    function indeterminateDataItem(dataItem: any) {
        dataItem.__fv_checked__ = false;
        dataItem.__fv_indeterminate__ = true;
        selectedValues.value = selectedValues.value
            .filter((seletedDataId: string) => seletedDataId !== dataItem[idField.value]);
    }

    function toInfluenceParentsCheckStatus(visualData: VisualData) {
        let parentIndex = visualData.raw.__fv_parent_index__;
        while (parentIndex !== undefined) {
            const checkedGroup = fetchCheckedNodes();
            // 如果父节点不存在怎么办，更改dataView的原始值，先存起来
            const parentVisualData = visibleDatasValue.value[parentIndex];
            // 计算当前子节点与已选节点的差值
            const childsLeft = getDifference(parentVisualData.raw.__fv_children_index__, checkedGroup);
            // 若完全不同，则为空状态
            if (childsLeft.length === parentVisualData.raw.__fv_children_index__.length && childsLeft.length !== 0) {
                unSelect(parentVisualData);
            }
            // 若完全相同，则为勾选状态
            else if (childsLeft.length === 0) {
                autoCheckParent.value && select(parentVisualData);
            }
            // 有重合但不完全相同，则为中间状态
            else if (childsLeft.length < parentVisualData.raw.__fv_children_index__.length) {
                // if (parentVisualData.checked !== true) {
                //     indeterminate(parentVisualData);
                // }
                indeterminate(parentVisualData);
            }
            parentIndex = parentVisualData.raw.__fv_parent_index__;

        }
    }

    /** 处理子节点选中状态 */
    function toSynchronizeChildrenCheckStatus(visualData: VisualData) {
        const checkStatus = !!visualData.checked;
        if (autoCheckChildren.value) {
            const children = visualData.raw.__fv_children_index__.map((index: number) => {
                return visibleDatasValue.value[index];
            });
            children.forEach((childTreeNode: any) => {
                checkStatus ? select(childTreeNode) : unSelect(childTreeNode);
            });
        }
    }

    function toggleSelectHierarchyItem(visualData: VisualData) {
        // 1.根据是否联动选择子节点属性, 同步子节点选中状态.
        toSynchronizeChildrenCheckStatus(visualData);
        // 2.根据已选结果影响父节点选中状态, 自动选择父节点为true时, 联动选择父节点; 自动选择父节点为false时, 仅联动勾选状态不选择数据, 但可取消选择.

        toInfluenceParentsCheckStatus(visualData);

        // 3.在处理完勾选状态后，根据选择数据范围, 更新选择数据结果.
        const currentDataItem = visualData.raw;
        let selectedItems = getSelectedItems();
        selectedItems = interactAfterCheckHierarchyItem(selectedItems, currentDataItem);
        // 4.检索已经勾选的项是否为其子节点, 若为子节点, 则重新置为中间状态.
        resetCurrentNodeBasedOnChildsStatus(visualData);
        // 5.输出已选数据
        context.emit('selectionChange', selectedItems);
    }

    function getNextSelectableSiblingItem(currentVisualDataItem: VisualData, visualDataIndex: number) {
        let nextSiblingIndex = -1;
        const hasParentNode = currentVisualDataItem.raw.__fv_parent_index__ > -1;
        if (hasParentNode) {
            const parentNode = visibleDatasValue.value[currentVisualDataItem.raw.__fv_parent_index__];
            const siblingsIndex = parentNode.raw.__fv_descendant_index__ as number[];
            const hasSiblings = siblingsIndex.length > 1;
            const indexInSiblings = siblingsIndex.findIndex((siblingIndex: number) => siblingIndex === visualDataIndex);
            const isLastInSiblings = indexInSiblings === siblingsIndex.length - 1;
            nextSiblingIndex = hasSiblings ? (isLastInSiblings ? siblingsIndex[indexInSiblings - 1] : siblingsIndex[indexInSiblings + 1]) : -1;
        }
        const hasNextVisibleSiblingNode = nextSiblingIndex > -1;
        if (hasNextVisibleSiblingNode) {
            return visibleDatasValue.value[nextSiblingIndex];
        }
        return null;
    }

    function getNextSelectableItemFromParent(currentVisualDataItem: VisualData) {
        const hasParentNode = currentVisualDataItem.raw.__fv_parent_index__ > -1;
        if (hasParentNode) {
            const parentNode = visibleDatasValue.value[currentVisualDataItem.raw.__fv_parent_index__];
            return parentNode;
        }
        return null;
    }

    function getNextSelectableHierarchyItemId(visualDataIndex: number) {
        const currentVisualDataItem = visibleDatasValue.value[visualDataIndex];
        if (!currentVisualDataItem) {
            return null;
        }
        const nextSiblingVisibleItem = getNextSelectableSiblingItem(currentVisualDataItem, visualDataIndex);
        const hasSiblings = nextSiblingVisibleItem !== null;
        const nextVisibleItemToSelect = hasSiblings ? nextSiblingVisibleItem :
            getNextSelectableItemFromParent(currentVisualDataItem);
        return nextVisibleItemToSelect ? nextVisibleItemToSelect.raw[idField.value] : null;
    }

    return { getNextSelectableHierarchyItemId, toggleSelectHierarchyItem };
}
