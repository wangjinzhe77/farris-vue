import { Ref, SetupContext, computed, ref, watch } from "vue";
import { CascadeOptions, DataViewOptions, UseCascade, UseDataView, UseIdentify, UseSelection, VisualData } from "../types";

export function useCascade(
    props: DataViewOptions,
    visibleDatasValue: Ref<VisualData[]>,
    identifyComposition: UseIdentify,
    context: SetupContext
): UseCascade {
    const { idField } = identifyComposition;
    // 自动勾选当前节点的子节点
    const autoCheckChildren = ref(props.hierarchy?.cascadeOption?.autoCheckChildren || false);
    // 自动勾选当前节点的父节点
    const autoCheckParent = ref(props.hierarchy?.cascadeOption?.autoCheckParent || false);
    // 添加已选数据模式, 包括: All, OnlyChildren, OnlyParent.
    const selectionRange = ref(props.hierarchy?.cascadeOption?.selectionRange || 'All');

    watch(() => props.hierarchy?.cascadeOption, (newCascadeOption: CascadeOptions) => {
        autoCheckChildren.value = newCascadeOption ? !!newCascadeOption.autoCheckChildren : false;
        autoCheckParent.value = newCascadeOption ? !!newCascadeOption.autoCheckParent : false;
        selectionRange.value = newCascadeOption ? newCascadeOption.selectionRange : 'All';
    });

    /** isBInA:判断B中的每个元素是否都在A中 */
    function isBInA(A: any[], B: any[]) {
        for (let i = 0; i < B.length; i++) {
            if (!A.includes(B[i])) {
                return false;
            }
        }
        return true;
    }

    /** A-B:求差 */
    function getDifference(A: any[], B: any[]) {
        const C = A.filter((element: any) => {
            return B.indexOf(element) === -1;
        });
        return C;
    }

    const onlySelectParent = computed(() => selectionRange.value === 'OnlyParent');

    const onlySelectChildren = computed(() => selectionRange.value === 'OnlyChildren');

    function toHandleParents(selectedDataIds: string[], parentIndex: number) {
        let trimmedSelectedDataIds = selectedDataIds;
        const parentData = visibleDatasValue.value[parentIndex];
        const parentId = parentData[idField.value];
        const siblingsOfCurrent = Array.from<number>(parentData.raw.__fv_descendant_index__)
            .map((index: number) => visibleDatasValue.value[index]);
        const siblingDataIds = siblingsOfCurrent.map((sibling: VisualData) => sibling.raw[idField.value]);
        const checkedAllSiblings = isBInA(selectedDataIds, siblingDataIds);
        const checkedParent = isBInA(selectedDataIds, [parentId]);

        const shouldTrimChildren = checkedParent && checkedAllSiblings && onlySelectParent.value;
        if (shouldTrimChildren) {
            trimmedSelectedDataIds = getDifference(selectedDataIds, siblingDataIds);
            siblingsOfCurrent.forEach((trimmedSelectedData: VisualData) => { trimmedSelectedData.disabled = true; });
        }

        const shouldTrimParent = checkedParent && checkedAllSiblings && onlySelectChildren.value;
        if (shouldTrimParent) {
            trimmedSelectedDataIds = getDifference(selectedDataIds, [parentId]);
            siblingsOfCurrent.forEach((trimmedSelectedData: VisualData) => { trimmedSelectedData.disabled = true; });
        }

        return trimmedSelectedDataIds;
    }

    function toHandleChildren(selectedDataIds: string[], dataItem: any) {
        let trimmedSelectedDataIds = selectedDataIds;
        const currentDataId = dataItem[idField.value];
        const childrenOfCurrent = Array.from<number>(dataItem.__fv_children_index__)
            .map((index: number) =>
                visibleDatasValue.value[index]);
        const childrenDataIds = childrenOfCurrent.map((sibling: VisualData) => sibling.raw[idField.value]);
        const checkedAllChildren = isBInA(selectedDataIds, childrenDataIds);
        const checkedCurrent = isBInA(selectedDataIds, [dataItem[idField.value]]);

        const shouldTrimChildren = checkedCurrent && checkedAllChildren && onlySelectParent.value;
        if (shouldTrimChildren) {
            trimmedSelectedDataIds = getDifference(selectedDataIds, childrenDataIds);
            childrenOfCurrent.forEach((trimmedSelectedData: VisualData) => { trimmedSelectedData.disabled = true; });
        }

        const shouldTrimCurrent = checkedCurrent && checkedAllChildren && onlySelectChildren.value;
        if (shouldTrimCurrent) {
            trimmedSelectedDataIds = getDifference(selectedDataIds, [currentDataId]);
            childrenOfCurrent.forEach((trimmedSelectedData: VisualData) => { trimmedSelectedData.disabled = true; });
        }

        return trimmedSelectedDataIds;
    }

    function interactAfterCheckHierarchyItem(selectionRecords: any[], dataItem: any) {

        let selectedDataIds = selectionRecords.map((item: any) => item[idField.value]);
        const hasParent = dataItem.__fv_parent_index__ > 0;
        if (hasParent) {
            selectedDataIds = toHandleParents(selectedDataIds, dataItem.__fv_parent_index__);
        }
        const hasChildren = dataItem.__fv_children_index__.length > 0;
        if (hasChildren) {
            selectedDataIds = toHandleChildren(selectedDataIds, dataItem);
        }
        const selectedDataItems = selectionRecords
            .filter((selectedDataItem: any) => selectedDataIds.includes(selectedDataItem[idField.value]));
        return selectedDataItems;
    }

    return { autoCheckChildren, autoCheckParent, interactAfterCheckHierarchyItem };
}
