import { ref } from "vue";
import { DataColumn, DataFilter, FilterFunction, FilterRelation, HeaderCell, UseFilter } from "../types";
import {
    Condition, ConditionGroup, ConditionValue, useCondition, CompareType,
    CheckBoxValue, ComboLookupValue, ComboListValue, DatePickerValue, DateRangeValue, DateTimePickerValue,
    InputGroupValue, LookupValue, MonthPickerValue, MonthRangeValue, NumberRangeValue, NumberSpinnerValue,
    RadioGroupValue, YearPickerValue, TextValue,
    ValueType
} from "../../../condition";
import { EditorType } from '@farris/ui-vue/components/dynamic-form';


type CompareFunction = (dataItem: Record<string, any>, field: string, value: any) => boolean;

const compareOperators = ['equal', 'notEqual', 'greaterThan', 'greaterThanOrEqual', 'lessThan', 'lessThanOrEqual',
    'contain', 'startWidth', 'endWidth', 'in', 'notIn'];

const textCompare: Record<string, CompareFunction> = {
    'equal': (dataItem: Record<string, any>, field: string, textValue: string) => {
        return dataItem && dataItem[field] === textValue;
    },
    'notEqual': (dataItem: Record<string, any>, field: string, textValue: string) => {
        return dataItem && dataItem[field] !== textValue;
    },
    'contain': (dataItem: Record<string, any>, field: string, textValue: string) => {
        if (dataItem) {
            const sourceString = dataItem[field] as string || '';
            return sourceString.indexOf(textValue) > -1;
        }
        return false;
    },
    'startWidth': (dataItem: Record<string, any>, field: string, textValue: string) => {
        if (dataItem) {
            const sourceString = dataItem[field] as string || '';
            return sourceString.startsWith(textValue);
        }
        return false;
    },
    'endWidth': (dataItem: Record<string, any>, field: string, textValue: string) => {
        if (dataItem) {
            const sourceString = dataItem[field] as string || '';
            return sourceString.endsWith(textValue);
        }
        return false;
    }
};

const booleanCompare: Record<string, CompareFunction> = {
    'equal': (dataItem: Record<string, any>, field: string, booleanValue: boolean) => {
        return dataItem && dataItem[field] === booleanValue;
    },
    'in': (dataItem: Record<string, any>, field: string, booleanValue: boolean[]) => {
        if (dataItem) {
            const sourceValue = dataItem[field] as boolean;
            return booleanValue.indexOf(sourceValue) > -1;
        }
        return false;
    },
    'notEqual': (dataItem: Record<string, any>, field: string, booleanValue: boolean) => {
        return dataItem && dataItem[field] !== booleanValue;
    }
};

const dateTimeCompare: Record<string, CompareFunction> = {
    'equal': (dataItem: Record<string, any>, field: string, dateTimeValue: string | number | Date) => {
        if (dataItem) {
            const targetDateTimeValue = new Date(new Date(dateTimeValue).toLocaleDateString()).valueOf();
            return new Date(new Date(dataItem[field]).toLocaleDateString()).valueOf() === targetDateTimeValue;
        }
        return false;
    },
    'notEqual': (dataItem: Record<string, any>, field: string, dateTimeValue: string | number | Date) => {
        if (dataItem) {
            const targetDateTimeValue = new Date(new Date(dateTimeValue).toLocaleDateString()).valueOf();
            return new Date(new Date(dataItem[field]).toLocaleDateString()).valueOf() !== targetDateTimeValue;
        }
        return false;
    },
    'greaterThan': (dataItem: Record<string, any>, field: string, dateTimeValue: string | number | Date) => {
        if (dataItem) {
            const targetDateTimeValue = new Date(new Date(dateTimeValue).toLocaleDateString()).valueOf();
            return new Date(new Date(dataItem[field]).toLocaleDateString()).valueOf() > targetDateTimeValue;
        }
        return false;
    },
    'greaterThanOrEqual': (dataItem: Record<string, any>, field: string, dateTimeValue: string | number | Date) => {
        if (dataItem) {
            const targetDateTimeValue = new Date(new Date(dateTimeValue).toLocaleDateString()).valueOf();
            return new Date(new Date(dataItem[field]).toLocaleDateString()).valueOf() >= targetDateTimeValue;
        }
        return false;
    },
    'lessThan': (dataItem: Record<string, any>, field: string, dateTimeValue: string | number | Date) => {
        if (dataItem) {
            const targetDateTimeValue = new Date(new Date(dateTimeValue).toLocaleDateString()).valueOf();
            return new Date(new Date(dataItem[field]).toLocaleDateString()).valueOf() < targetDateTimeValue;
        }
        return false;
    },
    'lessThanOrEqual': (dataItem: Record<string, any>, field: string, dateTimeValue: string | number | Date) => {
        if (dataItem) {
            const targetDateTimeValue = new Date(new Date(dateTimeValue).toLocaleDateString()).valueOf();
            return new Date(new Date(dataItem[field]).toLocaleDateString()).valueOf() <= targetDateTimeValue;
        }
        return false;
    }
};

const enumCompare: Record<string, CompareFunction> = {
    'equal': (dataItem: Record<string, any>, field: string, enumValues: string) => {
        if (dataItem) {
            const enumArray = String(enumValues).split(',');
            return enumArray.includes(String(dataItem[field]));
        }
        return false;
    },
    'in': (dataItem: Record<string, any>, field: string, enumValues: string[]) => {
        if (dataItem) {
            const sourceValue = dataItem[field] as string;
            return enumValues.indexOf(sourceValue) > -1;
        }
        return false;
    },
    'notEqual': (dataItem: Record<string, any>, field: string, enumValues: string) => {
        if (dataItem) {
            const enumArray = String(enumValues).split(',');
            return enumArray.findIndex((enumValue: string) => enumValue === String(dataItem[field])) === -1;
        }
        return false;
    },
};

const numericCompare: Record<string, CompareFunction> = {
    'equal': (dataItem: Record<string, any>, field: string, numericValue: number) => {
        if (dataItem) {
            const filterValue = Number.parseFloat(String(numericValue));
            const matchingValue = Number.parseFloat(String(dataItem[field]));
            return isNaN(filterValue) ? isNaN(matchingValue) : filterValue === matchingValue;
        }
        return false;
    },
    'notEqual': (dataItem: Record<string, any>, field: string, numericValue: number) => {
        if (dataItem) {
            const filterValue = Number.parseFloat(String(numericValue));
            const matchingValue = Number.parseFloat(String(dataItem[field]));
            return isNaN(filterValue) ? true : filterValue !== matchingValue;
        }
        return false;
    },
    'greaterThan': (dataItem: Record<string, any>, field: string, numericValue: number) => {
        if (dataItem) {
            const filterValue = Number.parseFloat(String(numericValue));
            const matchingValue = Number.parseFloat(String(dataItem[field]));
            return isNaN(filterValue) ? isNaN(matchingValue) : matchingValue > filterValue;
        }
        return false;
    },
    'greaterThanOrEqual': (dataItem: Record<string, any>, field: string, numericValue: number) => {
        if (dataItem) {
            const filterValue = Number.parseFloat(String(numericValue));
            const matchingValue = Number.parseFloat(String(dataItem[field]));
            return isNaN(filterValue) ? isNaN(matchingValue) : matchingValue >= filterValue;
        }
        return false;
    },
    'lessThan': (dataItem: Record<string, any>, field: string, numericValue: number) => {
        if (dataItem) {
            const filterValue = Number.parseFloat(String(numericValue));
            const matchingValue = Number.parseFloat(String(dataItem[field]));
            return isNaN(filterValue) ? isNaN(matchingValue) : matchingValue < filterValue;
        }
        return false;
    },
    'lessThanOrEqual': (dataItem: Record<string, any>, field: string, numericValue: number) => {
        if (dataItem) {
            const filterValue = Number.parseFloat(String(numericValue));
            const matchingValue = Number.parseFloat(String(dataItem[field]));
            return isNaN(filterValue) ? isNaN(matchingValue) : matchingValue <= filterValue;
        }
        return false;
    }
};

const editorTypeToCompares = new Map<EditorType, Record<string, CompareFunction>>([
    ['check-box', booleanCompare],
    ['combo-list', enumCompare],
    ['combo-lookup', textCompare],
    ['date-picker', dateTimeCompare],
    ['date-range', dateTimeCompare],
    ['datetime-picker', dateTimeCompare],
    ['datetime-range', dateTimeCompare],
    ['month-picker', dateTimeCompare],
    ['month-range', dateTimeCompare],
    ['year-picker', dateTimeCompare],
    ['year-range', dateTimeCompare],
    ['input-group', textCompare],
    ['lookup', textCompare],
    ['number-range', numericCompare],
    ['number-spinner', numericCompare],
    ['radio-group', enumCompare],
    ['text', textCompare]
]);

const valueTypeToCompares = new Map<string, Record<string, CompareFunction>>([
    ['boolean', booleanCompare],
    ['datetime', dateTimeCompare],
    ['enum', enumCompare],
    ['number', numericCompare],
    ['text', textCompare]
]);

export function useFilter(): UseFilter {

    const conditions = ref<Condition[]>([]);
    const columnFilterConditionMap = new Map<string, Condition>();
    const { loadConditionGroup } = useCondition();

    function createConditionValue(editorType: EditorType, fieldValue: any): ConditionValue {
        switch (editorType) {
            case 'check-box':
                return new CheckBoxValue({ value: fieldValue } as any);
            case 'combo-list':
                return new ComboListValue({ value: fieldValue } as any);
            case 'combo-lookup':
                return new ComboLookupValue({ value: fieldValue } as any);
            case 'input-group':
                return new TextValue({ value: fieldValue });
            case 'date-picker':
                return new DatePickerValue({ value: fieldValue } as any);
            case 'date-range':
                return new DateRangeValue({ value: fieldValue } as any);
            case 'datetime-picker':
                return new DateTimePickerValue({ value: fieldValue } as any);
            case 'datetime-range':
                return new DateRangeValue({ value: fieldValue } as any);
            case 'lookup':
                return new LookupValue({ value: fieldValue } as any);
            case 'month-picker':
                return new MonthPickerValue({ value: fieldValue } as any);
            case 'month-range':
                return new MonthRangeValue({ value: fieldValue } as any);
            case 'number-range':
                return new NumberRangeValue({ value: fieldValue } as any);
            case 'number-spinner':
                return new NumberSpinnerValue({ value: fieldValue } as any);
            case 'radio-group':
                return new RadioGroupValue({ value: fieldValue } as any);
            default:
                return new TextValue({ value: fieldValue } as any);
        }
    }

    const typeToConditionEditorName = new Map<string, string>([
        ['boolean', 'check-box'],
        ['date', 'date-picker'],
        ['datetime', 'datetime-picker'],
        ['number', 'number-spinner'],
        ['string', 'text'],
        ['text', 'text'],
        ['enum', 'combo-list'],
        ['reference', 'lookup']
    ]);

    const typeToCompareType = new Map<string, CompareType>([
        ['boolean', CompareType.In],
        ['date', CompareType.Equal],
        ['datetime', CompareType.Equal],
        ['number', CompareType.Equal],
        ['string', CompareType.Equal],
        ['text', CompareType.Equal],
        ['enum', CompareType.In],
        ['reference', CompareType.Equal]
    ]);

    function getFilterEditorType(column: DataColumn) {
        const fieldType = column.dataType || 'string';
        return (column.editor?.type || typeToConditionEditorName.get(fieldType) || 'text') as EditorType;
    }

    function addColumnFilter(headerCell: HeaderCell) {
        const id = `field_filter_${headerCell.field}`;
        const conditionId = Date.now();
        const fieldCode = headerCell.field;
        const fieldName = headerCell.column?.title || '';
        const fieldType = headerCell.column?.dataType || 'string';
        const compareType = typeToCompareType.get(fieldType) || CompareType.Equal;
        const editorType = (headerCell.column?.editor?.type || typeToConditionEditorName.get(fieldType) || 'text') as EditorType;
        const value = createConditionValue(editorType, headerCell.column?.filter as any);
        const valueType = (editorType === 'lookup' || editorType === 'combo-lookup') ?
            ValueType.SmartHelp : (editorType === 'combo-list' ? ValueType.Enum : ValueType.Value);
        columnFilterConditionMap.set(id, { id, fieldCode, fieldName, compareType, valueType, value, conditionId });
        conditions.value = Array.from(columnFilterConditionMap.values());
    }

    function removeColumnFilter(headerCell: HeaderCell) {
        const id = `field_filter_${headerCell.field}`;
        columnFilterConditionMap.delete(id);
        conditions.value = Array.from(columnFilterConditionMap.values());
    }

    function removeCondition(id: string) {
        columnFilterConditionMap.delete(id);
        conditions.value = Array.from(columnFilterConditionMap.values());
    }

    function clearCondition() {
        columnFilterConditionMap.clear();
        conditions.value = [];
    }

    function getCompareFunction(condition: Condition): CompareFunction {
        const { valueType } = condition.value as ConditionValue;
        const compareType = condition.compareType || '0';
        const compareName = compareOperators[Number(compareType)];
        const compares = valueTypeToCompares.get(valueType) as Record<string, CompareFunction>;
        const compareFunction = compares[compareName];
        return compareFunction;
    }

    function converterConditionGroupToDataFilter(conditionGroup: ConditionGroup): DataFilter {
        const relation = (conditionGroup.relation === 2 ? 0 : 1) as FilterRelation;
        const filterFunctions: FilterFunction[] = conditionGroup.items.map((condition: Condition) => {
            const compareFunction = getCompareFunction(condition);
            const filterFunction = (dataItem: any) => {
                return compareFunction(dataItem, condition.fieldCode, (condition.value as ConditionValue).getValue());
            };
            return filterFunction;
        });
        const children = (conditionGroup.children && conditionGroup.children.length) ?
            conditionGroup.children.map((conditionGroup: ConditionGroup) => converterConditionGroupToDataFilter(conditionGroup)) : [];
        return { relation, filterFunctions, children };
    }

    function createDataFilter(): DataFilter {
        const conditionGroup = loadConditionGroup(conditions.value);
        return converterConditionGroupToDataFilter(conditionGroup);
    }

    function applyFilter(dataItem: any, dataFilter: DataFilter) {
        const filterResult: boolean[] = dataFilter.filterFunctions.map((filterFunction: FilterFunction) => filterFunction(dataItem));
        const filterGroupResult: boolean[] = dataFilter.children.map((subFilter: DataFilter) => applyFilter(dataItem, subFilter));
        if (dataFilter.relation === 1) {
            return filterResult.every((result: boolean) => result) && filterGroupResult.every((result: boolean) => result);
        }
        return filterResult.includes(true) || filterGroupResult.includes(true);
    }

    function apply(dataItem: any) {
        const dataFilter = createDataFilter();
        return applyFilter(dataItem, dataFilter);
    }

    return { addColumnFilter, apply, clearCondition, conditions, getFilterEditorType, removeColumnFilter, removeCondition };
}
