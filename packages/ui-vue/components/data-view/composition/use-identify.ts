import { ref } from "vue";
import { DataViewOptions, UseIdentify } from "./types";

export function useIdentify(props: DataViewOptions): UseIdentify {

    const idField = ref(props.idField);
    // 初始化行数据标识
    function reviseIdentifyField(rawData: any[]) {
        if (rawData && rawData.length) {
            const rawDataItem = rawData[0];
            const hasIdFieldInRawData = Object.keys(rawDataItem).includes(idField.value);
            if (!hasIdFieldInRawData) {
                idField.value = '__fv_indetify__';
            }
        }
    }

    return { idField, reviseIdentifyField };
}
