import { InputBaseProperty } from "../../../property-panel/src/composition/entity/input-base-property";

export class NumberSpinnerProperty extends InputBaseProperty {

    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }

    getEditorProperties(propertyData: any) {
        let maxPrecision;
        if (this.designViewModelField) {
            maxPrecision = this.designViewModelField.type.precision;
        }

        return this.getComponentConfig(propertyData, { "type": "number-spinner" }, {
            precision: {
                description: "",
                title: "精度",
                type: "number",
                editor: {
                    readonly: maxPrecision === 0,
                    min: 0,
                    max: maxPrecision
                }
            },
            step: {
                description: "",
                title: "步长",
                type: "number",
                editor: {
                    min: 0
                }
            },
            max: {
                description: "",
                title: "最大值",
                type: "number",
                editor: {
                    nullable:true
                }
            },
            min: {
                description: "",
                title: "最小值",
                type: "number",
                editor: {
                    nullable:true
                }
            },
            textAlign: {
                description: "",
                title: "对齐方式",
                type: "enum",
                editor: {
                    type: "combo-list",
                    textField: "name",
                    valueField: "value",
                    data: [
                        {
                            value: "left",
                            name: "左对齐"
                        },
                        {
                            value: "center",
                            name: "居中"
                        },
                        {
                            value: "right",
                            name: "右对齐"
                        }
                    ]
                }
            }
        });
    }
};

