import { SetupContext, computed, nextTick, onMounted, ref } from 'vue';
import { NumberSpinnerProps } from '../number-spinner.props';
import { UseFormat, UseNumber, UseTextBox } from '../composition/types';

export default function (
    props: NumberSpinnerProps,
    context: SetupContext,
    useTextBoxComposition: UseTextBox
) {
    const inputElementRef = ref();
    const { onBlurTextBox, onFocusTextBox, onInput, onKeyDown, textBoxValue } = useTextBoxComposition;
    const placeholder = computed(() => (props.disabled || props.readonly || !props.editable ? '' : props.placeholder));

    const numberTextBoxClass = computed(() => ({
        'form-control': true,
        'f-utils-fill': true
    }));

    const numberTextBoxStyle = computed(() => {
        const styleObject = {
            'text-align': props.textAlign
        } as Record<string, any>;
        return styleObject;
    });

    function onChange($event: Event) {
        $event.stopPropagation();
    }

    function setInputElementStatus() {
        if (props.selectOnCreated) {
            // (inputElementRef.value as HTMLInputElement)?.select();
        }
        if (props.focusOnCreated) {
            // (inputElementRef.value as HTMLInputElement)?.focus();
        }
    }

    async function onFocus($event: Event) {
        onFocusTextBox($event);
        await nextTick;
        setInputElementStatus();
    }

    onMounted(async () => {
        await nextTick;
        setInputElementStatus();

    });

    return () => {
        return (
            <input
                ref={inputElementRef}
                class={numberTextBoxClass.value}
                style={numberTextBoxStyle.value}
                type="text"
                value={textBoxValue.value}
                disabled={props.disabled}
                readonly={props.readonly || !props.editable}
                placeholder={placeholder.value}
                onBlur={onBlurTextBox}
                onChange={onChange}
                onFocus={onFocus}
                onInput={onInput}
                onKeydown={onKeyDown}
            />
        );
    };
}
