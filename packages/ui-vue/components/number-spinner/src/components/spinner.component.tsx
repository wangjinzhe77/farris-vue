import { SetupContext, computed } from 'vue';
import { NumberSpinnerProps } from '../number-spinner.props';
import { UseSpinner } from '../composition/types';

export default function (props: NumberSpinnerProps, conext: SetupContext, useSpinnerComposition: UseSpinner) {
    const { canDownward, canUpward, onClickDownButton, onClickUpButton } = useSpinnerComposition;

    const spinnerClass = computed(() => {
        const classObject = {
            'input-group-append': true,
            'btn-group': true,
            'btn-group-number': true
        } as Record<string, boolean>;
        return classObject;
    });

    const spinnerButtonClass = computed(() => {
        const classObject = {
            btn: true,
            'btn-secondary': true,
            'btn-number-flag': true
        } as Record<string, boolean>;
        return classObject;
    });

    const upButtonStyle = computed(() => {
        const styleObject = {
            cursor: canUpward() ? 'pointer' : 'not-allowed',
            'margin-left': 0
        } as Record<string, any>;
        return styleObject;
    });

    const downButtonStyle = computed(() => {
        const styleObject = {
            cursor: canDownward() ? 'pointer' : 'not-allowed',
            'margin-left': 0
        } as Record<string, any>;
        return styleObject;
    });

    return () => {
        return (
            <div class={spinnerClass.value}>
                <button
                    title="upButton"
                    class={spinnerButtonClass.value}
                    style={upButtonStyle.value}
                    onClick={onClickUpButton}
                    disabled={!canUpward()}>
                    <span class="f-icon f-icon-arrow-chevron-up number-arrow-chevron"></span>
                </button>
                <button
                    title="downButton"
                    class={spinnerButtonClass.value}
                    style={downButtonStyle.value}
                    onClick={onClickDownButton}
                    disabled={!canDownward()}>
                    <span class="f-icon f-icon-arrow-chevron-down number-arrow-chevron"></span>
                </button>
            </div>
        );
    };
}
