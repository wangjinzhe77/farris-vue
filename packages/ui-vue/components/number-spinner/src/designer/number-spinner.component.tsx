/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, onMounted, ref, inject } from 'vue';
import type { SetupContext } from 'vue';
import { numberSpinnerDesignProps, NumberSpinnerDesignProps } from '../number-spinner.props';
import { useSpinner } from '../composition/use-spinner';
import { useFormat } from '../composition/use-format';
import { useNumber } from '../composition/use-number';
import getSpinnerRender from '../components/spinner.component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { useNumberSpinnerDesignerRules } from '../composition/use-design-rules';

export default defineComponent({
    name: 'FNumberSpinnerDesign',
    props: numberSpinnerDesignProps,
    emits: ['update:modelValue', 'valueChange', 'change', 'blur', 'focus', 'click', 'input'] as (string[] & ThisType<void>) | undefined,
    setup(props: NumberSpinnerDesignProps, context: SetupContext) {
        const useNumberComposition = useNumber(props, context);
        const useFormatComposition = useFormat(props, context, useNumberComposition);
        const useSpinnerComposition = useSpinner(props, context, useFormatComposition, useNumberComposition);
        const renderSpinner = getSpinnerRender(props, context, useSpinnerComposition);

        const elementRef = ref();
        const designerHostService = inject('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useNumberSpinnerDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);
        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        const inputElementRef = ref();
        return () => (
            <div ref={elementRef} class="input-group flex-row f-cmp-number-spinner">
                <input
                    ref={inputElementRef}
                    class="form-control"
                    readonly
                    placeholder={props.placeholder}
                />
                {renderSpinner()}
            </div>
        );
    }
});
