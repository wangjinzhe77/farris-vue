/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, onMounted, watch } from 'vue';
import type { SetupContext } from 'vue';
import { numberSpinnerProps, NumberSpinnerProps } from './number-spinner.props';
import { useSpinner } from './composition/use-spinner';
import { useFormat } from './composition/use-format';
import { useNumber } from './composition/use-number';
import { useTextBox } from './composition/use-text-box';
import getSpinnerRender from './components/spinner.component';
import getNumberTextBoxRender from './components/text-box.component';

export default defineComponent({
    name: 'FNumberSpinner',
    props: numberSpinnerProps,
    emits: ['update:modelValue', 'valueChange', 'change', 'blur', 'focus', 'click', 'input'] as (string[] & ThisType<void>) | undefined,
    setup(props: NumberSpinnerProps, context: SetupContext) {
        const useNumberComposition = useNumber(props, context);
        const useFormatComposition = useFormat(props, context, useNumberComposition);
        const useSpinnerComposition = useSpinner(props, context, useFormatComposition, useNumberComposition);
        const useTextBoxComposition = useTextBox(props, context, useFormatComposition, useNumberComposition, useSpinnerComposition);
        const renderSpinner = getSpinnerRender(props, context, useSpinnerComposition);
        const { displayValue, modelValue, getRealValue } = useNumberComposition;
        const renderNumberTextBox = getNumberTextBoxRender(props, context, useTextBoxComposition);
        const { format } = useFormatComposition;
        const { isFocus } = useTextBoxComposition;
        const shouldShowSpinner = computed(() => !props.disabled && !props.readonly && props.showButton);
        const inputGroupClass = computed(() => {
            const classObject = {
                'input-group': true,
                'flex-row':true,
                'f-cmp-number-spinner':true,
                'f-state-disabled': props.disable,
                'f-state-readonly': props.readonly && !props.disable,
                'f-state-focus': isFocus.value
            };

            return classObject;
        });
        onMounted(() => {
            const value = getRealValue(props.modelValue, false);
            displayValue.value = format(value, false);
        });

        watch(
            () => [props.value],
            ([newValue]) => {
                const value = getRealValue(newValue, false);
                modelValue.value = value;
                displayValue.value = format(value, false);
            }
        );

        watch(
            () => [props.modelValue],
            ([newModelValue]) => {
                if (newModelValue !== modelValue.value) {
                    modelValue.value = newModelValue;
                    !isFocus.value && (displayValue.value = format(getRealValue(newModelValue, false), false));
                }
            }
        );

        watch(
            () => [props.precision, props.useThousands, props.prefix, props.suffix, props.showZero],
            () => {
                displayValue.value = format(modelValue.value, false);
            }
        );

        return () => (
            <div class={inputGroupClass.value}>
                {renderNumberTextBox()}
                {shouldShowSpinner.value && renderSpinner()}
            </div>
        );
    }
});
