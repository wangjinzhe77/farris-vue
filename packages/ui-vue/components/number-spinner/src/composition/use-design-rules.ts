 
 
import { ComponentSchema, DesignerComponentInstance, DesignerItemContext } from "../../../designer-canvas/src/types";
import { UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { NumberSpinnerProperty } from "../property-config/number-spinner.property-config";
export function useNumberSpinnerDesignerRules(designItemContext: DesignerItemContext, designerHostService): UseDesignerRules {

    const schema = designItemContext.schema as ComponentSchema;

    // 构造属性配置方法
    function getPropsConfig(componentId: string, componentInstance: DesignerComponentInstance) {
        const inputGroupProps = new NumberSpinnerProperty(componentId, designerHostService);
        return inputGroupProps.getPropertyConfig(schema, componentInstance);
    }

    return { getPropsConfig } as UseDesignerRules;

}
