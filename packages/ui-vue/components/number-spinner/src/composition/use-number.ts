import { SetupContext, computed, ref } from "vue";
import { NumberSpinnerProps } from "../number-spinner.props";
import BigNumber from "bignumber.js";
import { UseNumber } from "./types";

export function useNumber(props: NumberSpinnerProps, context: SetupContext): UseNumber {
    const displayValue = ref('');
    const modelValue = ref(props.modelValue);
    const precision = computed(() => Number(props.precision) || 0);

    /**
     * 基于精度参数修改toFixed方法
     * @param value
     * @returns
     */
    function toFixed(value: BigNumber | number) {
        return value.toFixed(precision.value);
    }

    /**
     * 判断输入框中的值是否为空
     * @param val 输入值
     * @returns 返回是否时空值的判断结果
     */
    function isEmpty(value: string | number | undefined): boolean {
        return isNaN(value as number) || value === null || value === undefined || value === '';
    }

    /**
     * 最值校验
     * @param bn
     * @returns
     */
    function getValidNumberObject(numberObject: BigNumber): BigNumber {
        const maxValue = !isEmpty(props.max) ? new BigNumber(String(props.max), 10) : null;
        const minValue = !isEmpty(props.min) ? new BigNumber(String(props.min), 10) : null;
        const validNumberObject = (maxValue && numberObject.gt(maxValue)) ? maxValue :
            ((minValue && numberObject.lt(minValue)) ? minValue : numberObject);
        return validNumberObject;
    }

    /**
     * 获取实际数值
     * @param val
     * @param needValid 外部传进来的值直接进行展示，不做大小值校验
     * @returns
     */
    function getRealValue(value: any, needValid: boolean = true) {
        if (props.parser) {
            if (!isNaN(Number(value))) {
                return value;
            }
            return props.parser(value);
        }
        let numberObject = new BigNumber(value, 10);
        if(needValid) {
            numberObject = getValidNumberObject(numberObject);
        }
        if (numberObject.isNaN()) {
            if (props.nullable) {
                return null;
            }
            const minBigNum = new BigNumber('' + props.min, 10);
            const maxBigNum = new BigNumber('' + props.max, 10);
            if (!minBigNum.isNaN()) {
                numberObject = minBigNum;
            } else if (!maxBigNum.isNaN()) {
                numberObject = maxBigNum;
            } else {
                return 0;
            }
        }
        const fixedNumberString = toFixed(numberObject);
        return fixedNumberString;
    }

    /**
     * 输入框真实数值修改，并通知回调
     * @param realVal
     */
    function onNumberValueChanged(likeNumberValue: number | string) {
        let numberValue:number | null = Number(likeNumberValue);
        if(props.nullable && likeNumberValue === null) {
            numberValue = null;
        }
        // modelValue.value = numberValue;
        context.emit('update:modelValue', numberValue);
        context.emit('valueChange', numberValue);
        context.emit('change', numberValue);
    }

    return { displayValue, getRealValue, modelValue, isEmpty, onNumberValueChanged, precision, getValidNumberObject };
}
