import { defineComponent, inject, provide, ref, Suspense } from 'vue';
import { schemaSelectorProps, SchemaSelectorProps, ViewOption } from './schema-selector.props';
import { SchemaItem, UseSchemaRepository } from './composition/types';
import { FTabs, FTabPage } from '../../tabs';
import { FSearchBox } from '../../search-box';
import FCardView from './components/card-view.component';
import FIconView from './components/icon-view.component';
import FNavList from './components/nav-list-view.component';
import FSingleListView from './components/list-view.component';

import './schema-selector.css';
import { useEmptySchemaRepository } from './composition/use-empty-schema-repository';
import { FLoadingService } from '../../loading';
import { FNotifyService } from '../../notify';

export default defineComponent({
    name: 'FSchemaSelector',
    props: schemaSelectorProps,
    emits: ['selectionChange', 'cancel', 'submit'],
    setup(props: SchemaSelectorProps, context) {
        const viewType = ref(props.viewType);
        const viewOptions = ref<ViewOption[]>(props.viewOptions);
        const viewsMap = new Map([
            ['Card', FCardView], ['Icon', FIconView], ['List', FSingleListView]
        ]);
        const schemaRepositoryToken: symbol = props.injectSymbolToken;

        const searchValue = ref('');
        provide('F_SCHEMA_SELECTOR_SEARCH_VALUE', searchValue);
        provide('FLoadingService', FLoadingService);

        const useSchemaRepository = inject<UseSchemaRepository>(schemaRepositoryToken, useEmptySchemaRepository());
        const getViewDataFunctionMap = new Map([
            ['Recent', useSchemaRepository.getRecentlyData],
            ['Recommand', useSchemaRepository.getRecommandData],
            ['Total', useSchemaRepository.getSchemaData]
        ]);
        const navListRef = ref();
        /** 标识按钮是否被点击，防止连续多次点击触发多次事件 */
        const submitButtonClicked = ref(false);

        function onSchemaSelect(schema: SchemaItem) {
            context.emit('selectionChange', schema);
        }

        function renderTabsContent() {
            return viewOptions.value.map((viewOption: ViewOption) => {
                if (viewsMap.has(viewOption.type)) {
                    const SelectorDataView = viewsMap.get(viewOption.type) as any;
                    const options = {
                        enableGroup: viewOption.enableGroup || false,
                        groupField: viewOption.groupField || 'category',
                        groupFormatter: viewOption.groupFormatter || null,
                        getData: getViewDataFunctionMap.get(viewOption.dataSource),
                        editorParams: props.editorParams,
                        pagination: viewOption.pagination || false
                    };
                    return <FTabPage id={viewOption.id} title={viewOption.title} class="container">
                        <SelectorDataView {...options} onSelectionChange={($event) => onSchemaSelect($event)}></SelectorDataView>
                    </FTabPage>;
                }
            });
        }

        function onSearch(value: string) {
            searchValue.value = value;
        }

        function onClearSearchValue() {
            searchValue.value = '';
        }

        function renderTabsHeaderSuffix() {
            return <FSearchBox showDropdown={false} updateOn={'change'} customClass="f-schema-selector-search" placeholder="请输入名称/编号搜索"
                onChange={onSearch} onClear={onClearSearchValue}></FSearchBox>;
        }

        function renderTabsView() {
            return <FTabs>{{ default: renderTabsContent, headerSuffix: renderTabsHeaderSuffix }}</FTabs>;
        }

        function renderNavView() {
            const getData = useSchemaRepository.getSchemaData;
            const getCategory = useSchemaRepository.getNavigationData;
            return <FNavList ref={navListRef} getData={getData} getCategory={getCategory} editorParams={props.editorParams} ></FNavList>;
        }

        function validateNavListData(): boolean {
            if (!navListRef?.value?.selectedController) {
                const notifyService: any = new FNotifyService();
                notifyService.globalConfig = { position: 'top-center' };
                notifyService.warning('请选择控制器');
                return false;
            }
            return true;
        }
        function onCancelClicked() {
            context.emit('cancel');
        }

        function onSubmitClicked() {
            if (submitButtonClicked.value) {
                return;
            }
            submitButtonClicked.value = true;
            if (viewType.value === 'NavList') {
                if (validateNavListData()) {
                    context.emit('submit', navListRef.value.selectedController);
                } else {
                    submitButtonClicked.value = false;
                }
                return;
            }
            context.emit('submit');
        }
        const selectorContentMap = new Map<string, () => any>([['Tabs', renderTabsView], ['NavList', renderNavView]]);
        const renderSelectorContent = selectorContentMap.get(viewType.value);

        return () => {
            return (
                <div class="f-schema-selector h-100 d-flex flex-column">
                    {renderSelectorContent && renderSelectorContent()}
                    {
                        props.showFooter ?
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" onClick={onCancelClicked}>取消</button>
                                <button type="button" class="btn btn-primary" onClick={onSubmitClicked}>确定</button>
                            </div> : ''
                    }
                </div>
            );
        };
    }
});
