 
import { defineComponent } from "vue";
import { FListView } from '../../../list-view';
import { SchemaViewProps, schemaViewProps } from "./schema-view.props";
import { GetSchemaDataFunction } from "../composition/types";

export default defineComponent({
    name: 'FSchemaSelectIconView',
    props: schemaViewProps,
    emits: ['selectionChange'],
    setup(props: SchemaViewProps, context) {
        const getData = props.getData as GetSchemaDataFunction;
        const group = {
            enable: props.enableGroup,
            groupFields: [props.groupField || 'category']
        };

        const data = getData('', { pageIndex: 1, pageSize: 1 }, props.editorParams);

        function renderListViewContent({ item, index, selectedItem }) {
            return (
                <div class="f-navUdt-item">
                    <img title={item.code} src={`/img/${item.code}.svg`} style="padding:12px 41px 9px 42px;" />
                    <div class="f-navUdt-name">{item.name}</div>
                    <div class="f-navUdt-code">{item.code}</div>
                </div>
            );
        }

        function selectionChangeEventHandler($event) {
            context.emit('selectionChange', $event);
        }


        return () => {
            return (<FListView data={data} group={group} view="CardView" style="height:420px;"  onSelectionChange={(event) => selectionChangeEventHandler(event)}>
                {{ content: renderListViewContent }}
            </FListView>
            );
        };
    }
});
