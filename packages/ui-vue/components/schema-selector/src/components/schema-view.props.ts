import { ExtractPropTypes, PropType } from "vue";
import { GetSchemaCategoryFunction, GetSchemaDataFunction, SchemaRepositoryPagination } from "../composition/types";

export const schemaViewProps = {
    /** 启用分组 */
    enableGroup: { type: Boolean, default: false },
    /** 分组字段 */
    groupField: { type: String, default: 'category' },
    groupFormatter: { type: Function, default: null },
    /** 获取数据方法 */
    getData: {
        Type: Function as PropType<GetSchemaDataFunction>,
        default: (searchingText: string, pagination: SchemaRepositoryPagination, customParams?: Record<string, any>) => []
    },
    /** 获取导航分类 */
    getCategory:{
        Type: Function as PropType<GetSchemaCategoryFunction>,
        default: (searchingText: string, pagination: SchemaRepositoryPagination, customParams?: Record<string, any>) => []
    },
    pagination: { type: Boolean, default: false },
    editorParams : {type: Object}
} as Record<string, any>;

export type SchemaViewProps = ExtractPropTypes<typeof schemaViewProps>;
