
import { defineComponent, inject, onMounted, Ref, ref, watch } from "vue";
import { SchemaViewProps, schemaViewProps } from "./schema-view.props";
import { FListView } from '../../../list-view';
import { FSearchBox } from "../../../search-box";
import { GetSchemaCategoryFunction, GetSchemaDataFunction } from "../composition/types";
import { cloneDeep } from 'lodash-es';

export default defineComponent({
    name: 'FSchemaNavListView',
    props: schemaViewProps,
    emits: ['selectionChange'],
    setup(props: SchemaViewProps, context) {
        const getData = props.getData as GetSchemaDataFunction;
        const getCategory = props.getCategory as GetSchemaCategoryFunction;
        /** 分类列表 */
        const navData = getCategory('', { pageIndex: 1, pageSize: 1 }, props.editorParams);
        /** 当前选中的分类 */
        const selectedCategory = ref();
        /** 搜索项 */
        const searchValue = ref('');
        /** 全部的控制器列表 */
        const dataList: any = ref([]);
        /** 当前展示的控制器列表 */
        const searchResult: any = ref([]);
        /** 右侧控制器列表实例 */
        const controllerListViewRef = ref();
        /** 右侧选中的控制器 */
        const selectedController = ref();

        function renderListViewContent({ item }) {
            return (
                <div class="f-navlookup-item">
                    <div class="f-navlookup-itemIcon">
                        <div class="f-icon f-icon-department"
                            style="color:#2BA85E;font-size:13px; width: 12px;height: 12px;transform: rotate(270deg);">
                        </div>
                    </div>
                    <div class="f-navlookup-name">{item.name} ({item.code})</div>
                    <div class="f-navlookup-source">{item.nameSpace}</div>
                </div>
            );
        }

        function renderCategoryitem({ item }) {
            return <div class="f-navSelector-leftCategory">{item.name}</div>;
        }

        function selectionChangeEventHandler($event) {
            context.emit('selectionChange', $event);

            selectedController.value = $event[0];
        }

        function updateListViewDataScource(items: any[]) {
            controllerListViewRef.value.clearSelection();
            controllerListViewRef.value.updateDataSource(items);
        }
        function filterListViewData() {
            let resultItems = cloneDeep(dataList.value);
            if (searchValue.value) {
                resultItems = dataList.value.filter((item: any) => {
                    return `${item.name} ${item.code}`.toLowerCase().indexOf(searchValue.value.toLowerCase()) > -1;
                });
            }
            if (selectedCategory.value?.id && selectedCategory.value.id !== 'all') {
                const selectedControllerList = selectedCategory.value.contains || [];
                resultItems = resultItems.filter((item: any) => selectedControllerList.includes(item.code));
            }


            searchResult.value = resultItems;
            updateListViewDataScource(searchResult.value);
        }
        async function loadData() {
            const loadingService: any | null = inject('FLoadingService', null);
            const loading = loadingService?.show();
            const data = await getData('', { pageIndex: 1, pageSize: 1 }, props.editorParams);
            dataList.value = data;
            filterListViewData();
            loading.value?.close();
        }
        onMounted(() => {
            loadData();
        });
        function onChangeCategory($event) {
            if (selectedCategory.value?.id !== $event[0].id) {
                selectedCategory.value = $event[0];
                filterListViewData();
                selectedController.value = null;
            }
        }


        function onSearch(value: string) {
            searchValue.value = value;
            filterListViewData();
        }

        function onClearSearchValue() {
            searchValue.value = '';
            filterListViewData();
        }

        context.expose({ selectedController });

        return () => {
            return <div class="f-navSelector">
                <div class="f-navSelector-left">
                    <FListView data={navData} onSelectionChange={onChangeCategory}>
                        {{ content: renderCategoryitem }}
                    </FListView>
                </div>
                <div class="f-navSelector-right">
                    <div class="f-navSelector-header">
                        <div class="f-navSelector-recommand">
                            <div class="f-navSelector-recommand-text">控制器列表</div>
                        </div>
                        <div class="f-navSelector-remainder">
                            <FSearchBox showDropdown={false} customClass="f-schema-selector-search" updateOn={'change'} placeholder="请输入名称/编号搜索" onChange={onSearch} onClear={onClearSearchValue}></FSearchBox>
                        </div>
                    </div>
                    <div class="f-navSelector-content">
                        <FListView ref={controllerListViewRef} data={searchResult.value} view="ContentView" style="height:400px;" onSelectionChange={(event) => selectionChangeEventHandler(event)}>
                            {{ content: renderListViewContent }}
                        </FListView>
                    </div>
                </div>
            </div>;
        };
    }
});
