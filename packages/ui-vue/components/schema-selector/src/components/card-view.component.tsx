 
import { defineComponent, ref } from "vue";
import { FListView } from '../../../list-view';
import { FPagination } from '../../../pagination';
import { SchemaViewProps, schemaViewProps } from "./schema-view.props";
import { GetSchemaDataFunction, SchemaRepositoryPagination } from "../composition/types";

export default defineComponent({
    name: 'FSchemaSelectCardView',
    props: schemaViewProps,
    emits: ['selectionChange'],
    setup(props: SchemaViewProps, context) {
        const getData = props.getData as GetSchemaDataFunction;
        const group = {
            enable: props.enableGroup,
            groupFields: [props.groupField || 'category']
        };
        const data = getData('', { pageIndex: 1, pageSize: 1 }, props.editorParams);

        function renderListViewContent({ item, index, selectedItem }) {
            return (
                <div class="f-navForm-item">
                    <div class="f-navForm-leftIcon">
                        <div class="f-navForm-itemIcon">
                            <div class="f-icon f-icon-engineering" style="color:white;font-size: 24px;margin: 8px;">
                            </div>
                        </div>
                    </div>
                    <div class="f-navForm-rightText">
                        <div class="f-navForm-name">{item.name}({item.code})</div>
                        <div class="f-navForm-source">{item.nameSpace}</div>
                    </div>
                </div>
            );
        }

        function renderListViewFooter() {
            if (props.pagination) {
                return <FPagination
                    style="float:right;"
                    mode="default"
                    currentPage={1}
                    pageSize={20}
                    totalItems={50}
                    showPageList={false}
                    showGoButton={false}>
                </FPagination>;
            }
        }

        function selectionChangeEventHandler($event) {
            context.emit('selectionChange', $event);
        }

        return () => {
            return (<FListView data={data} group={group} view="CardView" style="height:420px;"  onSelectionChange={(event) => selectionChangeEventHandler(event)}>
                {{
                    content: renderListViewContent,
                    footer: renderListViewFooter
                }}
            </FListView>
            );
        };
    }
});
