 
import { defineComponent, inject, onMounted, Ref, ref, watch } from "vue";
import { FListView } from '../../../list-view';
import { FPagination } from '../../../pagination';
import { SchemaViewProps, schemaViewProps } from "./schema-view.props";
import { GetSchemaDataFunction } from "../composition/types";

export default defineComponent({
    name: 'FSchemaSelectListView',
    props: schemaViewProps,
    emits: ['selectionChange'],
    setup(props: SchemaViewProps, context) {

        const listViewRef = ref();

        const getData = props.getData as GetSchemaDataFunction;
        const group = {
            enable: props.enableGroup,
            groupFields: [props.groupField || 'category'],
            formatter: props.groupFormatter
        };
        const searchResult: any = ref([]);
        const dataList: any = ref([]);
        const searchValue = inject<Ref>('F_SCHEMA_SELECTOR_SEARCH_VALUE');

        const loadingService: any | null = inject('FLoadingService', null);

        function updateListViewDataScource(items: any[]) {
            listViewRef.value.updateDataSource(items);
        }

        watch(() => searchValue?.value, (newValue, oldValue) => {
            let resultItems = dataList.value;
            if (newValue && newValue !== oldValue) {
                resultItems = dataList.value.filter((item: any) => {
                    return `${item.name} ${item.code}`.toLowerCase().indexOf(newValue.toLowerCase()) > -1;
                });
            }
            searchResult.value = resultItems;
            updateListViewDataScource(searchResult.value);
        });

        async function loadData() {
            const loading = loadingService?.show();
            const data = await getData('', { pageIndex: 1, pageSize: 1 }, props.editorParams);
            dataList.value = data;
            searchResult.value = data;
            updateListViewDataScource(searchResult.value);
            loading.value?.close();
        }

        onMounted(() => {
            loadData();
        });

        function renderListViewContent({ item, index, selectedItem }) {
            return (
                <div class="f-navlookup-item">
                    <div class="f-navlookup-itemIcon">
                        <div class="f-icon f-icon-window"
                            style="color:#2BA85E;font-size:13px; width: 12px;height: 12px;padding: 0px 0px 2px 0px;">
                        </div>
                    </div>
                    <div class="f-navlookup-name">{item.name} ({item.code})</div>
                    <div class="f-navlookup-source">{item.nameSpace}</div>
                </div>
            );
        }

        function renderListViewFooter() {
            if (props.pagination) {
                return <FPagination
                    style="float:right;"
                    mode="default"
                    currentPage={1}
                    pageSize={20}
                    totalItems={50}
                    showPageList={false}
                    showGoButton={false}>
                </FPagination>;
            }
        }

        function selectionChangeEventHandler($event) {
            context.emit('selectionChange', $event);
        }

        return () => {
            return (<FListView data={searchResult.value} group={group} view="ContentView" style="height:420px;" ref={listViewRef}
                onSelectionChange={(event) => selectionChangeEventHandler(event)}>
                {{
                    content: renderListViewContent,
                    footer: renderListViewFooter
                }}
            </FListView>
            );
        };
    }
});
