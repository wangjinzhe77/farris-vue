import { ExtractPropTypes, PropType } from "vue";

export interface ViewOption {
    /** 视图唯一标识 */
    id: string;
    /** 视图标题 */
    title: string;
    /** 视图类型 */
    type: 'Card' | 'Icon' | 'List';
    /** 数据源类型 */
    dataSource: 'Recent' | 'Recommand' | 'Total';
    /** 启用分组 */
    enableGroup?: boolean;
    /** 分组字段 */
    groupField?: string;
    pagination?: boolean;
    groupFormatter?: (value: any, groupData: any) => string;
}

export type SelectSchemaViewType = 'Tabs' | 'NavList';

const emptySchemaRepositorySymbol = Symbol('empty schema repository inject token');

// provide(emptySchemaRepositorySymbol, useEmptySchemaRepository());

export const schemaSelectorProps = {
    injectSymbolToken: { type: Symbol },
    /** 待选数据视图集合 */
    viewOptions: { type: Array<ViewOption>, default: [] },
    /** 数据视图类型 */
    viewType: { type: String as PropType<SelectSchemaViewType>, default: 'Tabs' },
    editorParams: { type: Object, default: null },
    showFooter: { type: Boolean, default: false }
} as Record<string, any>;

export type SchemaSelectorProps = ExtractPropTypes<typeof schemaSelectorProps>;
