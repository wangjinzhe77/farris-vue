export interface SchemaCategory {
    code: string;
    name: string;
    active?: boolean;
    contains: string[];
}

export interface SchemaItem {
    id: string;
    code: string;
    name: string;
    nameSpace: string;
    fileName?: string;
    type?: string;
    content?: any;
    category: string;
    [key: string]: any;
}

export interface SchemaRepositoryPagination {
    pageSize: number;
    pageIndex: number;
}

type SchemaDataType =  Promise<SchemaItem[]> | SchemaItem[];

export type GetSchemaDataFunction = (searchingText: string, pagination: SchemaRepositoryPagination,
    customParams?: Record<string, any>) => SchemaDataType;

export type GetSchemaCategoryFunction = (searchingText: string, pagination: SchemaRepositoryPagination,
    customParams?: Record<string, any>) => SchemaCategory[];

export interface UseSchemaRepository {

    getNavigationData: GetSchemaCategoryFunction;

    getRecentlyData: GetSchemaDataFunction;

    getRecommandData: GetSchemaDataFunction;

    getSchemaData: GetSchemaDataFunction;

}
