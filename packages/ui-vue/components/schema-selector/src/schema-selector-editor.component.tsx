import { defineComponent, inject, onMounted, ref, watch } from "vue";
import FButtonEdit from '../../button-edit/src/button-edit.component';
import FSchemaSelector from './schema-selector.component';
import { SchemaSelectorEditorProps, schemaSelectorEditorProps } from "./schema-selector-editor.props";
import { SchemaItem } from "./composition/types";

export default defineComponent({
    name: "FSchemaSelectorEditor",
    props: schemaSelectorEditorProps,
    emits: ['selectionChange', 'update:modelValue'],
    setup(props: SchemaSelectorEditorProps, context) {

        const messageService: any = inject('FMessageBoxService');
        const buttonIcon = '<i class="f-icon f-icon-lookup"></i>';
        const viewType = ref(props.viewType);
        const selected = ref();
        const displayText = ref(props.modelValue);
        const viewOptions = ref(props.viewOptions);
        const modalTitle = ref(props.title);

        function setDisplayText() {
            let displayTextValue = '';
            if (selected.value && Array.isArray(selected.value)) {
                displayTextValue = selected.value.map((item) => item[props.textField]).join(',');
            }
            displayText.value = displayTextValue;
        }

        watch(props.title, (newValue) => {
            modalTitle.value = newValue;
        });

        watch(() => props.modelValue, (newValue) => {
            if (typeof newValue === 'string') {
                displayText.value = newValue;
            } else {
                setDisplayText();
            }
        });

        function onSchemaSelect(schema: SchemaItem) {
            selected.value = schema;
        }

        function renderSchemaSelector() {
            if (viewType.value === 'controller') {
                return <FSchemaSelector injectSymbolToken={props.repositoryToken} view-type="NavList" onS></FSchemaSelector>;
            }

            return <FSchemaSelector injectSymbolToken={props.repositoryToken}
                viewOptions={viewOptions.value}
                editorParams={props.editorParams}
                onSelectionChange={($event) => onSchemaSelect($event)}></FSchemaSelector>;
        }

        onMounted(() => {
        });

        const modalOptions = {
            title: modalTitle.value || '选择',
            fitContent: false,
            height: props.modalHeight || 600,
            width: props.modalWidth || 800,
            buttons: [
                {
                    name: 'cancel',
                    text: '取消',
                    class: 'btn btn-secondary',
                    handle: ($event: MouseEvent) => {
                        return true;
                    }
                },
                {
                    name: 'accept',
                    text: '确定',
                    class: 'btn btn-primary',
                    handle: async ($event: MouseEvent) => {
                        if (selected.value && selected.value.length) {
                            setDisplayText();

                            if (props.onSubmitModal && typeof props.onSubmitModal === 'function') {
                                const metadataInfo = await props.onSubmitModal(selected.value);
                                if (metadataInfo && props.onSchemaSelected && typeof props.onSchemaSelected === 'function') {
                                    selected.value[0]['metadataContent'] = metadataInfo;
                                    props.onSchemaSelected(selected.value);
                                }
                            }

                            return true;
                        }

                        messageService?.info('请选择数据源');
                        return false;
                    }
                }
            ],
            resizeable: false,
            draggable: true
        };

        function onBeforeOpen() {
            if (props.beforeOpenDialog) {
                props.beforeOpenDialog({});
            }
        }

        return () => (<FButtonEdit
            v-model={displayText.value}
            editable={false}
            disabled={props.disabled}
            readonly={props.readonly}
            inputType={"text"}
            enableClear={false}
            buttonContent={buttonIcon}
            buttonBehavior={"Modal"}
            modalOptions={modalOptions}
            beforeOpen={onBeforeOpen}>
            {/* <div class="h-100 d-flex flex-column"> */}
            {renderSchemaSelector()}
            {/* </div> */}
        </FButtonEdit>);
    }
});
