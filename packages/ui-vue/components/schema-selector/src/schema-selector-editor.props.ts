import { ExtractPropTypes, PropType } from "vue";
import { createPropsResolver } from "../../dynamic-resolver";
import { ViewOption } from "./schema-selector.props";
import schemaSelectorEditorSchema from './schema/schema-editor.schema.json';

export const schemaSelectorEditorProps = {
    title: {type: String, default: '选择数据源'},
    textField: { type: String, default: 'name' },
    viewOptions: { type: Array<ViewOption>, default: [] },
    beforeOpenDialog: {type: Function as PropType<() => void>, default: null},
    repositoryToken: { type: Symbol},
    onSchemaSelected: { type: Function, default: null },
    editorParams: { type: Object, default: null },
    onSubmitModal: { type: Function, default: null },
    disabled: { type: Boolean, default: false },
    readonly: { type: Boolean, default: false },
    modelValue: { type: Object, default: null }
} as Record<string, any>;

export type SchemaSelectorEditorProps = ExtractPropTypes<typeof schemaSelectorEditorProps>;

export const schemaSelectorEditorPropsResolver =
    createPropsResolver<SchemaSelectorEditorProps>(schemaSelectorEditorProps, schemaSelectorEditorSchema);
