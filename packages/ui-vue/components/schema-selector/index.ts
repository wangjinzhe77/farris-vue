 
import type { App } from 'vue';
import FSchemaSelector from './src/schema-selector.component';
import FSchemaSelectorEditor from './src/schema-selector-editor.component';
import { schemaSelectorEditorPropsResolver } from './src/schema-selector-editor.props';

export * from './src/schema-selector.props';
export * from './src/composition/types';

export { FSchemaSelector, FSchemaSelectorEditor };

export default {
    install(app: App): void {
        app.component(FSchemaSelector.name as string, FSchemaSelector);
        app.component(FSchemaSelectorEditor.name as string, FSchemaSelectorEditor);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
        componentMap['schema-selector'] = FSchemaSelectorEditor;
        propsResolverMap['schema-selector'] = schemaSelectorEditorPropsResolver;
    }
};
