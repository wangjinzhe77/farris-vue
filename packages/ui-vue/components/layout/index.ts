 
import type { App, Plugin } from 'vue';
import FLayout from './src/layout.component';
import FLayoutPane from './src/components/layout-pane.component';
import FLayoutDesign from './src/designer/layout.design.component';
import FLayoutPaneDesign from './src/designer/layout-pane.design.component';
import { layoutPropsResolver } from './src/layout.props';
import { layoutPanePropsResolver } from './src/components/layout-pane.props';

export * from './src/layout.props';

FLayout.install = (app: App) => {
    app.component(FLayout.name as string, FLayout);
    app.component(FLayoutPane.name as string, FLayoutPane);
};
FLayout.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap.layout = FLayout;
    propsResolverMap.layout = layoutPropsResolver;
    componentMap['layout-pane'] = FLayoutPane;
    propsResolverMap['layout-pane'] = layoutPanePropsResolver;
};
FLayout.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.layout = FLayoutDesign;
    propsResolverMap.layout = layoutPropsResolver;
    componentMap['layout-pane'] = FLayoutPaneDesign;
    propsResolverMap['layout-pane'] = layoutPanePropsResolver;
};

export { FLayout, FLayoutPane };
export default FLayout as typeof FLayout & Plugin;
