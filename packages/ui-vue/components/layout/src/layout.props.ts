 
import { ExtractPropTypes } from "vue";
import { createPropsResolver } from "../../dynamic-resolver";
import { schemaMapper } from "./schema/schema-mapper";
import { schemaResolver } from "./schema/schema-resolver";
import layoutSchema from './schema/layout.schema.json';
import propertyConfig from './property-config/layout.property-config.json';

export const layoutProps = {
    customStyle:{ type: String, defaut: '' },
    customClass: { type: String, defaut: '' },
} as Record<string, any>;

export type LayoutProps = ExtractPropTypes<typeof layoutProps>;

export const layoutPropsResolver = createPropsResolver<LayoutProps>(layoutProps, layoutSchema, schemaMapper, schemaResolver, propertyConfig);
