import { DesignerHostService, DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { useDragulaCommonRule } from "../../../designer-canvas/src/composition/rule/use-dragula-common-rule";
import { ComponentSchema, DesignerItemContext } from "../../../designer-canvas/src/types";

export function useDesignerRules(designItemContext: DesignerItemContext, designerHostService?: DesignerHostService): UseDesignerRules {
    const schema = designItemContext.schema as ComponentSchema;
    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {

        const basalRule = useDragulaCommonRule().basalDragulaRuleForContainer(draggingContext);
        if (!basalRule) {
            return false;
        }

        return true;
    }

 
    function getDesignerClass() {
        const classArray = ['f-layout-pane'];
        if (schema.position === 'left' || schema.position === 'right') {
            classArray.push('f-page-content-nav');
        }
        if (schema.position === 'center') {
            classArray.push('f-page-content-main');

        }
        return classArray.join(' ');
    };

    return { canAccepts, getDesignerClass };

}
