import { defineComponent, inject, onMounted, provide, ref } from 'vue';
import { LayoutProps, layoutProps } from '../layout.props';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerRules } from './layout-use-designer-rules';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { useResizeHandle } from '../composition/use-resize-handle';
import { LayoutContext } from '../composition/types';

export default defineComponent({
    name: 'FLayoutDesign',
    props: layoutProps,
    emits: [],
    setup(props: LayoutProps, context) {
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRules(designItemContext.schema, designItemContext.parent?.schema);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);
        componentInstance.value.canNested = false;
        const useResizeHandleComposition = useResizeHandle(elementRef);
        const { horizontalResizeHandleStyle, verticalResizeHandleStyle, resizeOverlayStyle } = useResizeHandleComposition;
        provide<LayoutContext>('layout', { useResizeHandleComposition });

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        return () => {
            return (
                <div class="f-layout f-page-content" ref={elementRef}>
                    {context.slots.default && context.slots.default()}
                    <div class="f-layout-resize-overlay" style={resizeOverlayStyle.value}></div>
                    <div class="f-layout-horizontal-resize-proxy" style={horizontalResizeHandleStyle.value}></div>
                    <div class="f-layout-vertical-resize-proxy" style={verticalResizeHandleStyle.value}></div>
                </div>
            );
        };
    }
});
