import { computed, defineComponent, inject, onMounted, ref } from 'vue';
import { LayoutPanePosition, LayoutPaneProps, layoutPaneProps } from '../components/layout-pane.props';
import { LayoutContext } from '../composition/types';
import { useResizePane } from '../composition/use-resize-pane';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { useDesignerRules } from './layout-pane-use-designer-rules';
import { DesignerHostService } from '../../../designer-canvas/src/composition/types';

export default defineComponent({
    name: 'FLayoutPaneDesign',
    props: layoutPaneProps,
    emits: [],
    setup(props: LayoutPaneProps, context) {
        const layoutPaneElementRef = ref();
        const elementRef = ref();
        const designerHostService = inject<DesignerHostService>('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);
        // componentInstance.value.styles = 'padding:0;box-shadow:none;';
        componentInstance.value.canNested = false;
        componentInstance.value.canMove = false;
        componentInstance.value.canDelete = false;

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);
        const actualHeight = ref(Math.max(props.minHeight, props.height));
        const actualWidth = ref(Math.max(props.minWidth, props.width));
        const minHeight = ref(props.minHeight);
        const minWidth = ref(props.minWidth);
        const position = ref(props.position);
        const layoutContext = inject<LayoutContext>('layout');
        const { useResizeHandleComposition } = layoutContext as LayoutContext;
        const useResizePaneComposition = useResizePane(actualWidth, actualHeight, minWidth, minHeight, useResizeHandleComposition);
        const { onClickHorizontalResizeBar, onClickVerticalResizeBar } =
            useResizePaneComposition;

        const resizeBarClass = computed(() => {
            const classObejct = {
                'f-layout-resize-bar': true,
                'f-layout-resize-bar-e': position.value === 'left',
                'f-layout-resize-bar-n': position.value === 'bottom',
                'f-layout-resize-bar-s': position.value === 'top',
                'f-layout-resize-bar-w': position.value === 'right'
            } as Record<string, boolean>;
            return classObejct;
        });

        function onClickResizeBar(payload: MouseEvent, position: LayoutPanePosition) {
            if (position === 'left' || position === 'right') {
                onClickHorizontalResizeBar(payload, position, layoutPaneElementRef.value as HTMLElement);
            }
            if (position === 'top' || position === 'bottom') {
                onClickVerticalResizeBar(payload, position, layoutPaneElementRef.value as HTMLElement);
            }
        }

        const layoutPaneClass = computed(() => {
            const classObejct = {
                'f-layout-pane': true,
                'f-page-content-nav': position.value === 'left' || position.value === 'right',
                'f-page-content-main': position.value === 'center'
            } as Record<string, boolean>;
            props.customClass && String(props.customClass).split(' ')
                .reduce<Record<string, boolean>>((result: Record<string, boolean>, className: string) => {
                    result[className] = true;
                    return result;
                }, classObejct);
            return classObejct;
        });

        const layoutPaneStyle = computed(() => {
            const styleObject = {
                'flex': '1'
            } as Record<string, any>;
            if ((actualWidth.value && position.value === 'left') || position.value === 'right') {
                styleObject.width = `${actualWidth.value}px`;
            }
            if ((actualHeight.value && position.value === 'bottom') || position.value === 'top') {
                styleObject.height = `${actualHeight.value}px`;
            }
            return styleObject;
        });

        return () => {
            return (
                <div ref={layoutPaneElementRef} class={layoutPaneClass.value} style={layoutPaneStyle.value}>
                    <div ref={elementRef} class="drag-container" data-dragref={`${designItemContext.schema.id}-container`}>
                        {context.slots.default && context.slots.default()}
                    </div>
                    <span class={resizeBarClass.value}
                        onMousedown={(payload: MouseEvent) => onClickResizeBar(payload, position.value)}></span>
                </div>
            );
        };
    }
});
