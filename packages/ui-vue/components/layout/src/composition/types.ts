import { Ref } from "vue";

export interface UseResizePane {

    onClickHorizontalResizeBar: ($event: MouseEvent, position: string, element: HTMLElement) => any;

    onClickVerticalResizeBar: ($event: MouseEvent, position: string, element: HTMLElement) => any;
}

export interface UseResizeHandle {
    horizontalResizeBarPosition: Ref<number>;

    horizontalResizeHandleOffset: Ref<number>;

    showHorizontalResizeHandle: Ref<boolean>;

    showVerticalResizeHandle: Ref<boolean>;

    verticalResizeBarPosition: Ref<number>;

    verticalResizeHandleOffset: Ref<number>;

    horizontalResizeHandleStyle: Ref<Record<string, any>>;

    verticalResizeHandleStyle: Ref<Record<string, any>>;

    resizeOverlayStyle: Ref<Record<string, any>>;

    draggingHorizontalResizeHandle: ($event: MouseEvent, maxWidth: number, minWidth: number, position: string) => void;

    draggingVerticalResizeHandle: ($event: MouseEvent, maxHeight: number, minHeight: number, position: string) => void;

    getPanelMaxHeight: (position: string, minWidth: number) => number|undefined;
    getPanelMaxWidth: (position: string, minHeight: number) => number|undefined;

}

export interface LayoutContext {
    useResizeHandleComposition: UseResizeHandle;
}
