import { computed, Ref, ref } from 'vue';
import { UseResizeHandle } from './types';

export function useResizeHandle(
    hostElementRef: Ref<HTMLElement | undefined>
): UseResizeHandle {
    const horizontalResizeBarPosition = ref(-1);
    const verticalResizeBarPosition = ref(-1);
    const horizontalResizeHandleOffset = ref(0);
    const verticalResizeHandleOffset = ref(0);
    const showHorizontalResizeHandle = ref(false);
    const showVerticalResizeHandle = ref(false);

    const horizontalResizeHandleStyle = computed(() => {
        const styleObject = {
            display: showHorizontalResizeHandle.value ? 'block' : 'none',
            left: `${horizontalResizeHandleOffset.value}px`,
            cursor: 'e-resize'
        } as Record<string, any>;
        return styleObject;
    });

    const verticalResizeHandleStyle = computed(() => {
        const styleObject = {
            display: showVerticalResizeHandle.value ? 'block' : 'none',
            top: `${verticalResizeHandleOffset.value}px`,
            cursor: 'n-resize'
        } as Record<string, any>;
        return styleObject;
    });

    const resizeOverlayStyle = computed(() => {
        const styleObject = {
            display: (showVerticalResizeHandle.value || showHorizontalResizeHandle.value) ? 'block' : 'none'
        } as Record<string, any>;
        return styleObject;
    });

    function draggingHorizontalResizeHandle($event: MouseEvent, maxWidth: number, minWidth: number, position: string) {
        const hostElement = hostElementRef.value;
        if (hostElement) {
            const { left: splitterOffsetLeft, right, width: layoutWidth } = (hostElement.getBoundingClientRect() as DOMRect);
            let offsetLeft = $event.clientX - splitterOffsetLeft;

            if (position === 'right') {
                offsetLeft = right - $event.clientX;
            }

            if (offsetLeft > maxWidth) {
                offsetLeft = maxWidth;
            }
            if (offsetLeft< minWidth) {
                offsetLeft = minWidth;
            }

            if (position === 'right') {
                offsetLeft = layoutWidth - offsetLeft;
            }

            horizontalResizeHandleOffset.value = offsetLeft;
        }
    }

    function draggingVerticalResizeHandle($event: MouseEvent, maxHeight: number, minHeight: number, position: string) {
        const hostElement = hostElementRef.value;
        if (hostElement) {
            const { top: splitterOffsetTop, bottom, height: layoutHeight } = (hostElement.getBoundingClientRect() as DOMRect);
            let offsetTop = $event.clientY - splitterOffsetTop;

            if (position === 'bottom') {
                offsetTop = bottom - $event.clientY;
            }

            if (offsetTop > maxHeight) {
                offsetTop = maxHeight;
            }
            if (offsetTop< minHeight) {
                offsetTop = minHeight;
            }

            if (position === 'bottom') {
                offsetTop = layoutHeight - offsetTop;
            }

            verticalResizeHandleOffset.value = offsetTop;
        }
    }

    function getLayoutContainerSize() {
        const layoutElement = hostElementRef.value;
        if (layoutElement) {
            const {width, height} =  layoutElement.getBoundingClientRect();
            return { width, height };
        }

        return null;
    }

    function getLayoutPanels() {
        const layoutElement = hostElementRef.value;
        if (layoutElement) {
            return Array.from(layoutElement.querySelectorAll('[data-position]')).reduce((result: Record<string, any>, element) => {
                const key = (element as HTMLElement).getAttribute('data-position') as string;
                result = Object.assign(result, { [key]: element });
                return result;
            }, {});
        }

        return null;
    }

    function getPanelMaxWidth(positon: string, minWidth: number) {
        const layoutSize = getLayoutContainerSize();
        const layoutPanels = getLayoutPanels();
        if (layoutSize && layoutPanels) {
            const rightPanel = layoutPanels?.right;
            const leftPanel = layoutPanels?.left;

            if (positon === 'left' && leftPanel) {
                if (rightPanel) {
                    return layoutSize.width - rightPanel.clientWidth - minWidth;
                }
                return layoutSize.width - minWidth;
            }

            if (positon === 'right' && rightPanel) {
                if (leftPanel) {
                    return layoutSize.width - leftPanel.clientWidth - minWidth;
                }
                return  layoutSize.width -  minWidth;
            }
        }
    }

    function getPanelMaxHeight(positon: string, minHeight: number) {
        const layoutSize = getLayoutContainerSize();
        const layoutPanels = getLayoutPanels();
        if (layoutSize && layoutPanels) {
            const bottomPanel = layoutPanels?.bottom;
            const topPanel = layoutPanels?.top;

            if (positon === 'top' && topPanel) {
                if (bottomPanel) {
                    return layoutSize.height - bottomPanel.clientHeight - minHeight;
                }
                return layoutSize.height - minHeight;
            }

            if (positon === 'bottom' && bottomPanel) {
                if (topPanel) {
                    return layoutSize.height - topPanel.clientHeight - minHeight;
                }
                return  layoutSize.height -  minHeight;
            }
        }
    }

    return {
        horizontalResizeHandleStyle,
        verticalResizeHandleStyle,
        resizeOverlayStyle,
        showHorizontalResizeHandle,
        showVerticalResizeHandle,
        horizontalResizeBarPosition,
        verticalResizeBarPosition,
        verticalResizeHandleOffset,
        horizontalResizeHandleOffset,
        draggingHorizontalResizeHandle,
        draggingVerticalResizeHandle,
        getPanelMaxHeight,
        getPanelMaxWidth
    };
}
