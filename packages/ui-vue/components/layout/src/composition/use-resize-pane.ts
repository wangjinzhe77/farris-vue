import { Ref } from 'vue';
import { UseResizePane, UseResizeHandle } from './types';

export function useResizePane(
    actualWidth: Ref<number>,
    actualHeight: Ref<number>,
    minWidth: Ref<number>,
    minHeight: Ref<number>,
    useResizeHandleComposition: UseResizeHandle
): UseResizePane {
    const {
        horizontalResizeBarPosition,
        horizontalResizeHandleOffset,
        showHorizontalResizeHandle,
        showVerticalResizeHandle,
        verticalResizeBarPosition,
        verticalResizeHandleOffset,
        draggingHorizontalResizeHandle,
        draggingVerticalResizeHandle,
        getPanelMaxHeight,
        getPanelMaxWidth
    } = useResizeHandleComposition;

    let layoutPanel = '';
    let layoutPaneElement: any;
    let layoutElement: any;

    let mouseMoveHandler: (event: MouseEvent) => void;

    function releaseMouseMove($event: MouseEvent) {

        if ((layoutPanel === 'left' || layoutPanel === 'right') && layoutElement) {
            const { left: splitterOffsetLeft } = ((layoutElement as HTMLElement).getBoundingClientRect() as DOMRect);
            const { width: splitterPaneWidth } = ((layoutPaneElement as HTMLElement).getBoundingClientRect() as DOMRect);
            const mouseReleasePosition = $event.clientX - splitterOffsetLeft;
            let newWidth = layoutPanel === 'left' ?
                (splitterPaneWidth || 0) + (mouseReleasePosition - horizontalResizeBarPosition.value) :
                (splitterPaneWidth || 0) - (mouseReleasePosition - horizontalResizeBarPosition.value);

            newWidth = minWidth.value > 0 ? Math.max(minWidth.value, newWidth) : newWidth;

            const maxWidth = getPanelMaxWidth(layoutPanel, minWidth.value);
            if (maxWidth != null) {
                newWidth = maxWidth > newWidth ? newWidth: maxWidth;
            }
            actualWidth.value = newWidth;
        }
        if ((layoutPanel === 'top' || layoutPanel === 'bottom') && layoutElement) {
            const { top: splitterOffsetTop } = ((layoutElement as HTMLElement).getBoundingClientRect() as DOMRect);
            const { height: splitterPaneHeight } = ((layoutPaneElement as HTMLElement).getBoundingClientRect() as DOMRect);
            const mouseReleasePosition = $event.clientY - splitterOffsetTop;
            let newHeight = layoutPanel === 'top' ?
                (splitterPaneHeight || 0) + (mouseReleasePosition - verticalResizeBarPosition.value) :
                (splitterPaneHeight || 0) - (mouseReleasePosition - verticalResizeBarPosition.value);
            newHeight = minHeight.value > 0 ? Math.max(minHeight.value, newHeight) : newHeight;

            const maxHeight = getPanelMaxHeight(layoutPanel, minHeight.value);
            if (maxHeight != null) {
                newHeight = maxHeight > newHeight ? newHeight: maxHeight;
            }
            actualHeight.value = newHeight;
        }
        horizontalResizeHandleOffset.value = 0;
        verticalResizeHandleOffset.value = 0;
        horizontalResizeBarPosition.value = -1;
        verticalResizeBarPosition.value = -1;
        showHorizontalResizeHandle.value = false;
        showVerticalResizeHandle.value = false;
        document.removeEventListener('mousemove', mouseMoveHandler);
        document.removeEventListener('mouseup', releaseMouseMove);
        document.body.style.userSelect = '';
        layoutPanel = '';
        layoutPaneElement = null;
        layoutElement = null;
    }

    function onClickHorizontalResizeBar($event: MouseEvent, splitterPaneName: string, element: HTMLElement) {
        layoutPanel = splitterPaneName;
        layoutPaneElement = element;
        showHorizontalResizeHandle.value = true;
        const clickElementPath = $event.composedPath();
        layoutElement = clickElementPath.find((element: any) => (element.className || '').split(' ')[0] === 'f-layout');
        if (layoutElement) {
            const { left: splitterOffsetLeft } = ((layoutElement as HTMLElement).getBoundingClientRect() as DOMRect);
            const offsetLeft = $event.clientX - splitterOffsetLeft;
            horizontalResizeHandleOffset.value = offsetLeft;
            horizontalResizeBarPosition.value = offsetLeft;

            const maxWidth = getPanelMaxWidth(layoutPanel, minWidth.value) || 0;
            mouseMoveHandler = (event: MouseEvent) => draggingHorizontalResizeHandle(event, maxWidth, minWidth.value, layoutPanel);
            document.addEventListener('mousemove',mouseMoveHandler);
            document.addEventListener('mouseup', releaseMouseMove);
            document.body.style.userSelect = 'none';
        }
    }

    function onClickVerticalResizeBar($event: MouseEvent, splitterPaneName: string, element: HTMLElement) {
        layoutPanel = splitterPaneName;
        layoutPaneElement = element;
        showVerticalResizeHandle.value = true;
        const clickElementPath = $event.composedPath();
        layoutElement = clickElementPath.find((element: any) => (element.className || '').split(' ')[0] === 'f-layout');
        if (layoutElement) {
            const { top: splitterOffsetTop } = ((layoutElement as HTMLElement).getBoundingClientRect() as DOMRect);
            verticalResizeHandleOffset.value = $event.clientY - splitterOffsetTop;
            verticalResizeBarPosition.value = $event.clientY - splitterOffsetTop;

            const maxHeight = getPanelMaxHeight(layoutPanel, minHeight.value) || 0;
            mouseMoveHandler = (event: MouseEvent) => draggingVerticalResizeHandle(event, maxHeight, minHeight.value, layoutPanel);
            document.addEventListener('mousemove', mouseMoveHandler);
            document.addEventListener('mouseup', releaseMouseMove);
            document.body.style.userSelect = 'none';
        }
    }

    return { onClickHorizontalResizeBar, onClickVerticalResizeBar };
}
