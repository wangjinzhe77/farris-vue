 
import { ExtractPropTypes, PropType } from "vue";
import { createPropsResolver } from "../../../dynamic-resolver";
import { schemaMapper } from "../schema/schema-mapper";
import { schemaResolver } from "../schema/schema-resolver";
import layoutPaneSchema from '../schema/layout-pane.schema.json';
import propertyConfig from '../property-config/layout-pane.property-config.json';

export type LayoutPanePosition = 'left' | 'center' | 'right' | 'top' | 'bottom';

export const layoutPaneProps = {
    customClass: { type: String, defaut: '' },
    customStyle:{ type: String, defaut: '' },
    /** 记录原始定义宽度 */
    width: { type: Number, default: -1 },
    /** 记录原始定义高度 */
    height: { type: Number, default: -1 },
    /** 面板位置 */
    position: { type: String as PropType<LayoutPanePosition>, default: 'left' },
    /** 是否显示 */
    visible: { type: Boolean, default: true },
    /** True to allow the pane can be resized. */
    resizable: { type: Boolean, default: true },
    /** True to allow the pane can be collapsed. */
    collapsable: { type: Boolean, default: false },
    /** 面板最小宽度 */
    minWidth: { type: Number, default: 100 },
    /** 面板最小高度 */
    minHeight: { type: Number, default: 100 }
} as Record<string, any>;

export type LayoutPaneProps = ExtractPropTypes<typeof layoutPaneProps>;

export const layoutPanePropsResolver = createPropsResolver<LayoutPaneProps>(layoutPaneProps, layoutPaneSchema, schemaMapper, schemaResolver, propertyConfig);
