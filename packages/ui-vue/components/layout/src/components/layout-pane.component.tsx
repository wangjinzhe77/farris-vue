import { computed, defineComponent, inject, ref, watch } from 'vue';
import { LayoutPanePosition, LayoutPaneProps, layoutPaneProps } from './layout-pane.props';
import { LayoutContext } from '../composition/types';
import { useResizePane } from '../composition/use-resize-pane';

export default defineComponent({
    name: 'FLayoutPane',
    props: layoutPaneProps,
    emits: [],
    setup(props: LayoutPaneProps, context) {

        const minHeight = ref(props.minHeight <= 0? 100: props.minHeight);
        const minWidth = ref(props.minWidth <= 0? 100: props.minWidth);

        const width = ref(props.width <=0 ? 100: props.width);
        const height = ref(props.height<= 100? 100: props.height);

        const actualHeight = ref(Math.max(minHeight.value, height.value));
        const actualWidth = ref(Math.max(minWidth.value, width.value));

        const elementRef = ref<HTMLElement>();
        const position = ref(props.position);
        const resizable = ref(props.resizable);

        watch(() => props.resizable, (canResize) => {
            resizable.value = canResize;
        });

        const layoutContext = inject<LayoutContext>('layout');
        const { useResizeHandleComposition } = layoutContext as LayoutContext;
        const useResizePaneComposition = useResizePane(actualWidth, actualHeight, minWidth, minHeight, useResizeHandleComposition);
        const { onClickHorizontalResizeBar, onClickVerticalResizeBar } =
            useResizePaneComposition;

        const resizeBarClass = computed(() => {
            const classObejct = {
                'f-layout-resize-bar': true,
                'f-layout-resize-bar-e': position.value === 'left',
                'f-layout-resize-bar-n': position.value === 'bottom',
                'f-layout-resize-bar-s': position.value === 'top',
                'f-layout-resize-bar-w': position.value === 'right'
            } as Record<string, boolean>;
            return classObejct;
        });

        const showResizeBar = computed(() => {
            return position.value !== 'center'&& resizable.value;
        });

        function onClickResizeBar(payload: MouseEvent, position: LayoutPanePosition) {
            if (position === 'left' || position === 'right') {
                onClickHorizontalResizeBar(payload, position, elementRef.value as HTMLElement);
            }
            if (position === 'top' || position === 'bottom') {
                onClickVerticalResizeBar(payload, position, elementRef.value as HTMLElement);
            }
        }

        const layoutPaneClass = computed(() => {
            const classObejct = {
                'f-layout-pane': true,
                'f-page-content-nav': position.value === 'left' || position.value === 'right',
                'f-page-content-main': position.value === 'center'
            } as Record<string, boolean>;
            props.customClass && String(props.customClass).split(' ')
                .reduce<Record<string, boolean>>((result: Record<string, boolean>, className: string) => {
                    result[className] = true;
                    return result;
                }, classObejct);
            return classObejct;
        });

        const layoutPaneStyle = computed(() => {
            const styleObject = {} as Record<string, any>;
            if ((actualWidth.value && position.value === 'left') || position.value === 'right') {
                styleObject.width = `${actualWidth.value}px`;
            }
            if ((actualHeight.value && position.value === 'bottom') || position.value === 'top') {
                styleObject.height = `${actualHeight.value}px`;
            }

            if (!props.visible) {
                styleObject.display = 'none';
            }

            return styleObject;
        });

        return () => {
            return (
                <div ref={elementRef} class={layoutPaneClass.value} style={layoutPaneStyle.value} data-position={position.value}>
                    {context.slots.default && context.slots.default()}
                    {
                        showResizeBar.value &&
                        <span class={resizeBarClass.value}
                            onMousedown={(payload: MouseEvent) => onClickResizeBar(payload, position.value)}></span>
                    }
                </div>
            );
        };
    }
});
