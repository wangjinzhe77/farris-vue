import { computed, defineComponent, provide, ref } from 'vue';
import { LayoutProps, layoutProps } from './layout.props';
import { LayoutContext } from './composition/types';
import { useResizeHandle } from './composition/use-resize-handle';
import { getCustomClass, getCustomStyle } from '../../common';

export default defineComponent({
    name: 'FLayout',
    props: layoutProps,
    emits: [],
    setup(props: LayoutProps, context) {
        const elementRef = ref();
        const useResizeHandleComposition = useResizeHandle(elementRef);
        const { horizontalResizeHandleStyle, verticalResizeHandleStyle, resizeOverlayStyle } = useResizeHandleComposition;
        provide<LayoutContext>('layout', { useResizeHandleComposition });

        const layoutClass = computed(() => {
            const classObject = {
                'f-layout': true,
            } as Record<string, any>;
            return getCustomClass(classObject,props?.customClass);
        });
        const layoutStyle = computed(() => {
            const styleObject = {} as Record<string, any>;
            return getCustomStyle(styleObject,props?.customStyle);
        });

        return () => {
            return (
                <div class={layoutClass.value} style={layoutStyle.value} ref={elementRef}>
                    {context.slots.default && context.slots.default()}
                    <div class="f-layout-resize-overlay" style={resizeOverlayStyle.value}></div>
                    <div class="f-layout-horizontal-resize-proxy" style={horizontalResizeHandleStyle.value}></div>
                    <div class="f-layout-vertical-resize-proxy" style={verticalResizeHandleStyle.value}></div>
                </div>
            );
        };
    }
});
