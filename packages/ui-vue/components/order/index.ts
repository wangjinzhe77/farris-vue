 
import { App } from 'vue';
import FOrder from './src/order.component';
import FOrderDesign from './src/designer/order.design.component';
import { propsResolver } from './src/order.props';

export * from './src/order.props';

export { FOrder };

export default {
    install(app: App): void {
        app.component(FOrder.name as string, FOrder);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>): void {
        componentMap.order = FOrder;
        propsResolverMap.order = propsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
        componentMap.order = FOrderDesign;
        propsResolverMap.order = propsResolver;
    }
};
