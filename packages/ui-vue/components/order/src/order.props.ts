 
import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver/src/props-resolver';
import { schemaMapper } from './schema/schema-mapper';
import orderSchema from './schema/order.schema.json';
import { schemaResolver } from './schema/schema-resolver';
import propertyConfig from './property-config/order.property-config.json';

export type SortType = 'asc' | 'desc';

export interface OrderedItem {
    id: string;
    name: string;
    order: SortType;
}

export const orderProps = {
    dataSource: {
        type: Array<object>, default: [{ id: '1', name: '发票类型' },
            { id: '2', name: '发票代码' },
            { id: '3', name: '开票日期' },
            { id: '4', name: '票价(燃油附加费)' },
            { id: '5', name: '税收分类编号' },]
    },
    items: {
        type: Array<OrderedItem>, default: [{ id: '1', name: '发票类型', order: 'asc' },
            { id: '2', name: '发票代码', order: 'desc' },
            { id: '3', name: '开票日期', order: 'asc' }]
    }
} as Record<string, any>;

export type OrderProps = ExtractPropTypes<typeof orderProps>;

export const propsResolver = createPropsResolver<OrderProps>(orderProps,orderSchema, schemaMapper, schemaResolver, propertyConfig);
