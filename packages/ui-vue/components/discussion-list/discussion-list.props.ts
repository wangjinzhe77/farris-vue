/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';

export const discussionListProps = {
    pagerOnServer: { Type: Boolean, default: true },
    /** 是否支持分页 */
    supportPaging: { Type: Boolean, default: true },
    /** 当前页码 */
    pageIndex: { Type: Number, default: 1 },
    /** 总条数 */
    total: { Type: Number, default: 0 },
    /** 每页显示个数 */
    pageSize: { Type: Number, default: 10 },
    /** 评论数据 */
    discussionData: { Type: Object, default: [] },
    personnelsDisplayKey: { Type: String, default: 'userName' },
};

export type DiscussionListProps = ExtractPropTypes<typeof discussionListProps>;
