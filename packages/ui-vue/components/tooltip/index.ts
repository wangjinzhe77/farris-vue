/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App, Plugin } from 'vue';
import FTooltip from './src/tooltip.component';
import FTooltipDirective from './src/tooltip.directive';

export * from './src/tooltip.props';

export { FTooltip, FTooltipDirective };

FTooltip.install = (app: App) => {
    app.component(FTooltip.name as string, FTooltip);
    app.directive('tooltip', FTooltipDirective);
};
export default FTooltip as typeof FTooltip & Plugin;
