import { computed, nextTick, ref } from "vue";
import { TooltipProps } from "../tooltip.props";

export function useTooltip(
    props: TooltipProps
) {
    const tooltipContainerRef = ref();
    const showTooltip = ref(false);
    const tooltipDisplayStyle = computed(() => {
        const styleObject = {
            display: showTooltip.value ? 'block' : 'none'
        };
        return styleObject;
    });

    const tooltipCustomClass = computed(() => {
        const customClassList = props.customClass?.split(' ');
        return customClassList?.reduce((total: Record<string,boolean>,next: string) => {
            total[next] = true;
            return total;
        },{});
    });
    return { showTooltip, tooltipDisplayStyle, tooltipCustomClass, tooltipContainerRef };
};
