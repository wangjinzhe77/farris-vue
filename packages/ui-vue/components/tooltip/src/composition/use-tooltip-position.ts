/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, ref, SetupContext } from 'vue';
import { TooltipProps } from '../tooltip.props';
import { RectPosition, TooltipPosition } from './types';
import { useAdjustPlacement } from './use-adjust-placement';
import { useAdjustPosition } from './use-adjust-position';
import { useCalculatePosition } from './use-calculate-position';
import { useRelative } from './use-relative';

export function useTooltipPosition(
    props: TooltipProps,
    context: SetupContext,
    hostRect: DOMRect,
    tooltipRect: DOMRect,
    tooltipContentRect: DOMRect,
    arrowRect: DOMRect
) {
    const placementAndAlignment = ref(props.placement);

    const { getRelativeElementBound } = useRelative(props, context);

    const { calculate } = useCalculatePosition(props, context);

    const { adjustPlacement } = useAdjustPlacement(props, context);

    const { adjustPosition } = useAdjustPosition(props, context);

    const tooltipPlacement = computed<string>(() => {
        return placementAndAlignment.value.split('-')[0];
    });

    const tooltipPosition = computed<TooltipPosition>(() => {
        const relativeRect = getRelativeElementBound();
        placementAndAlignment.value = adjustPlacement(placementAndAlignment.value, relativeRect, hostRect, tooltipRect, arrowRect);
        const originalPosition = calculate(placementAndAlignment.value, hostRect, tooltipRect, tooltipContentRect, arrowRect);
        const position = adjustPosition(
            placementAndAlignment.value,
            originalPosition,
            relativeRect,
            hostRect,
            tooltipRect,
            tooltipContentRect,
            arrowRect
        );
        return position;
    });

    // const arrowPosition = computed<RectPosition>(() => {
    //     const { top, left, right } = calculateArrowPosition(
    //         placementAndAlignment.value,
    //         hostBound,
    //         tooltipPosition.value,
    //         tooltipContentBound,
    //         arrowBound
    //     );
    //     return { top, left, right };
    // });

    return { tooltipPlacement, tooltipPosition };
}
