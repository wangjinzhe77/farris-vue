
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, Fragment, nextTick, onMounted, ref, SetupContext, Teleport } from 'vue';
import { TooltipProps, tooltipProps } from './tooltip.props';

import { useTooltipPosition } from './composition/use-tooltip-position';
import { useTooltip } from './composition/use-tooltip';

import FButton from '@farris/ui-vue/components/button';

export default defineComponent({
    name: 'FTooltip',
    props: tooltipProps,
    emits: ['click'] as (string[] & ThisType<void>) | undefined,
    setup(props: TooltipProps, context: SetupContext) {
        const fragmentRef = ref();
        const arrowRef = ref<any>();
        const tooltipRef = ref<any>();
        const tooltipInnerRef = ref<any>();
        const tooltipTextRef = ref<any>();
        const placement = ref(props.placement.split('-')[0]);
        const useTooltipComposition = useTooltip(props);
        const { showTooltip, tooltipDisplayStyle, tooltipCustomClass, tooltipContainerRef } = useTooltipComposition;
        const tooltipClass = computed(() => {
            let classObject = {
                tooltip: true,
                show: true
            } as any;
            const tooltipClassName = `bs-tooltip-${placement.value}`;
            classObject[tooltipClassName] = true;
            if (context.slots.default) {
                classObject = { ...classObject, ...tooltipCustomClass.value };
            }
            return classObject;
        });

        const shouldShowTooltipText = computed(() => props.content && !props.contentTemplate);

        const tooltipText = computed(() => props.content);

        const tooltipLeftPosition = ref('0px');

        const tooltipTopPosition = ref('0px');

        const tooltipStyle = computed(() => {
            const styleObject = {
                left: tooltipLeftPosition.value,
                top: tooltipTopPosition.value
            };
            if (context.slots.default) {
                // styleObject = { ...styleObject, ...tooltipDisplayStyle.value };
            }
            return styleObject;
        });

        const arrowLeftPosition = ref('');

        const arrowTopPosition = ref('');

        const arrowStyle = computed(() => {
            const styleObject = {
                left: arrowLeftPosition.value,
                top: arrowTopPosition.value
            };
            return styleObject;
        });

        const tooltipInnerClass = computed(() => {
            return {
                'tooltip-inner': true,
                'tooltip-inner-lg': tooltipInnerRef.value && tooltipInnerRef.value && (tooltipInnerRef.value.scrollHeight > tooltipInnerRef.value.clientHeight)
            };
        });
        const tooltipInnerStyle = computed(() => {
            const styleObject = {};
            if (props.width) {
                Object.assign(styleObject, {
                    'width': props.width,
                    'max-width': 'none'
                });
            }
            return styleObject;
        });

        const setPosition = (element: any) => {
            if (arrowRef.value && tooltipRef.value && tooltipInnerRef.value) {
                const { tooltipPlacement, tooltipPosition } = useTooltipPosition(
                    props,
                    context,
                    element.getBoundingClientRect(),
                    tooltipRef.value.getBoundingClientRect(),
                    tooltipInnerRef.value.getBoundingClientRect(),
                    arrowRef.value.getBoundingClientRect()
                );
                tooltipLeftPosition.value = `${tooltipPosition.value.tooltip.left + document.documentElement.scrollLeft}px`;
                tooltipTopPosition.value = `${tooltipPosition.value.tooltip.top + document.documentElement.scrollTop}px`;
                arrowLeftPosition.value = `${tooltipPosition.value.arrow.left}px`;
                arrowTopPosition.value = `${tooltipPosition.value.arrow.top}px`;
                placement.value = tooltipPlacement.value;
            }
        };

        const triggerByHover = (element: any) => {
            element.addEventListener('mouseenter', ($event: MouseEvent) => {
                $event.stopPropagation();
                showTooltip.value = true;
                nextTick(() => {
                    setPosition(element);
                });
            });

            element.addEventListener('mouseleave', ($event: MouseEvent) => {
                $event.stopPropagation();
                if ($event.target === tooltipTextRef.value) {
                    return;
                }
                showTooltip.value = false;
            });
        };

        function triggerByClick(element: any) {
            element.addEventListener('click', ($event: MouseEvent) => {
                $event.stopPropagation();
                showTooltip.value = true;
                nextTick(() => {
                    setPosition(element);
                });
            });
            document.body.addEventListener('click', ($event: MouseEvent) => {
                $event.stopPropagation();
                if ($event.target === tooltipTextRef.value) {
                    return;
                }
                showTooltip.value = false;
            });
        }

        onMounted(() => {
            // 获取宿主元素
            const element = fragmentRef.value?.nextElementSibling;
            if (element) {
                // 查找element是组件的情况
                if (props.trigger === 'click') {
                    triggerByClick(element);
                } else {
                    triggerByHover(element);
                }
            }
            if (!shouldShowTooltipText.value && props.contentTemplate && tooltipTextRef.value) {
                tooltipTextRef.value.appendChild(props.contentTemplate);
            }

            if (props.reference) {
                setPosition(props.reference);
            }
        });

        function onClick($event: MouseEvent) {
            context.emit('click', $event);
        }

        const getTooltipRender = () => {
            return <div
                ref={tooltipRef}
                class={tooltipClass.value}
                style={tooltipStyle.value}
                onClick={onClick}
            >
                <div ref={arrowRef} class="arrow" style={arrowStyle.value}></div>
                <div ref={tooltipInnerRef} class={tooltipInnerClass.value} style={tooltipInnerStyle.value}>
                    <div class="tooltip-tmpl" >
                        {shouldShowTooltipText.value && <div class="tooltip-text" v-html={tooltipText.value}></div>}
                        {!shouldShowTooltipText.value && <div class="tooltip-text" ref={tooltipTextRef}></div>}
                    </div>
                </div>
            </div>;
        };
        const getTooltipWrapperRender = () => {
            return <div
                ref={tooltipRef}
                class={tooltipClass.value}
                style={tooltipStyle.value}
                onClick={onClick}
            >
                <div ref={arrowRef} class="arrow" style={arrowStyle.value}></div>
                <div ref={tooltipInnerRef} class={tooltipInnerClass.value} style={tooltipInnerStyle.value}>
                    <div class="tooltip-tmpl" >
                        <div class="tooltip-text" ref={tooltipTextRef}>
                            {
                                context.slots.contentTemplate ?
                                    context.slots.contentTemplate() :
                                    props.content
                            }
                        </div>
                    </div>
                </div>
            </div>;
        };

        return () => {
            return !context.slots.default ? getTooltipRender() :
                <Fragment ref={fragmentRef}>
                    {context.slots.default()}
                    {/* 默认放在body中 */}
                    <Teleport to="body">
                        {showTooltip.value && getTooltipWrapperRender()}
                    </Teleport>
                </Fragment>;
        };
    }
});
