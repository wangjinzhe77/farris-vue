/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';

export type TooltipPlacement =
    | 'top'
    | 'top-left'
    | 'top-right'
    | 'bottom'
    | 'bottom-left'
    | 'bottom-right'
    | 'right'
    | 'right-top'
    | 'right-bottom'
    | 'left'
    | 'left-top'
    | 'left-bottom';

export type TooltipTriggerMode =
    | 'hover'
    | 'click';

export const tooltipProps = {
    /** 提示内容 */
    content: { type: String, default: '' },

    /** 模板提示内容 */
    contentTemplate: { type: Object },

    /** 提示框宽度，例如'200px' */
    width: { type: String },

    /** 提示框自定义样式，应用与提示框最外层 */
    customClass: { type: String },

    /** 提示方向 */
    placement: { type: String as PropType<TooltipPlacement>, default: 'top' },

    /** 触发提示的时机 */
    trigger: { type: String as PropType<TooltipTriggerMode>, default: 'hover' },

    // 此变量在指令中不对外
    reference: { type: Object, require: true },
    horizontalRelative: { type: String, default: '' },
    verticalRelative: { type: String, default: '' }
};
export type TooltipProps = ExtractPropTypes<typeof tooltipProps>;
