/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { App, createApp, onUnmounted, reactive } from 'vue';
import { TooltipProps } from './tooltip.props';
import Tooltip from './tooltip.component';

function initInstance(props: TooltipProps, content?: string): App {
    const container = document.createElement('div');
    if (props.customClass) {
        container.className = props.customClass;
    }

    const app: App = createApp({
        setup() {
            onUnmounted(() => {
                document.body.removeChild(container);
            });
            return () => <Tooltip {...props} onClick={app.unmount}></Tooltip>;
        }
    });
    document.body.appendChild(container);
    app.mount(container);
    return app;
}

function showTooltip(options: TooltipProps): App {
    const props: TooltipProps = reactive({
        ...options
    });
    return initInstance(props);
}

/**
 * hover触发提示框
 */
function triggerByHover(element: any, binding: Record<string, any>) {
    let app: App | null;
    element.addEventListener('mouseenter', ($event: MouseEvent) => {
        $event.stopPropagation();
        if (!app) {
            const tooltipObject = element.dataset.tooltip && JSON.parse(element.dataset.tooltip) || {};
            const tooltipProps: any = Object.assign(
                {
                    reference: element,
                    contentTemplate: binding.value.contentTemplate
                },
                tooltipObject
            );
            app = showTooltip(tooltipProps);
        }
    });
    element.addEventListener('mouseleave', ($event: MouseEvent) => {
        $event.stopPropagation();
        if (app) {
            app.unmount();
            app = null;
        }
    });
}

function outsideClick($event: MouseEvent, app: App | null) {
    $event.stopPropagation();
    if (app) {
        app.unmount();
        app = null;
    }
}
/**
 * click点击触发提示框
 */
function triggerByClick(element: any, binding: Record<string, any>) {
    let app: App | null;
    element.addEventListener('click', ($event: MouseEvent) => {
        $event.stopPropagation();
        if (!app) {
            const tooltipObject = element.dataset.tooltip && JSON.parse(element.dataset.tooltip) || {};
            const tooltipProps: any = Object.assign(
                {
                    trigger: 'hover',
                    reference: element
                },
                tooltipObject
            );
            app = showTooltip(tooltipProps);
        }
    });
    document.body.addEventListener('click', ($event: MouseEvent) => {
        outsideClick($event, app);
        app = null;
    });
}

function storePropertyToElement(element: any, binding: Record<string, any>) {
    if (!binding.value) {
        element.dataset.tooltip = '';
    } else {
        const { contentTemplate, ...propsWithoutContentTemplate } = binding.value;
        element.dataset.tooltip = JSON.stringify(propsWithoutContentTemplate);
    }
}

const tooltipDirective = {
    mounted: (element: any, binding: Record<string, any>, vnode: any) => {

        storePropertyToElement(element, binding);

        switch (binding.value.trigger) {
            case 'click': {
                triggerByClick(element, binding);
                break;
            }
            default: {
                triggerByHover(element, binding);
            }
        }

    },
    unMounted: (element: any, binding: Record<string, any>, vnode: any) => {
        if (binding.value.trigger === 'click') {
            document.body.removeEventListener('click', () => outsideClick);
        }
    },
    beforeUpdate: (element: any, binding: Record<string, any>) => {
        storePropertyToElement(element, binding);
    }
};
export default tooltipDirective;
