import FDesignerOutline from './src/designer-outline.component';

export * from './src/designer-outline.props';
export * from './src/types';

export { FDesignerOutline };
