/**
 * 组件类型
 */
export enum ComponentType {
    /**
     * 表单
     */
    Frame = 'frame',

    /**
     * 列表/树表类
     */
    dataGrid = 'data-grid',

    /**
     * 列表视图
     */
    listView = 'list-view',

    /**
     * 卡片类（待优化，目前类型中带有控件列布局信息）
     */
    form = 'form',

    /**
     *  附件
     */
    attachmentPanel = 'attachment-panel',

    /**
     * 子表弹出编辑后创建的模态框组件---运行态是动态创建的
     */
    modalFrame = 'modal-frame',

    /** 表格类 */
    table = 'table',

    /** 预约日历 */
    appointmentCalendar = 'appointment-calendar'
}

/**
 * 控件树节点
 */
export interface ControlTreeNode {
    id?: string;
    data: {
        /** 节点标识 */
        id: string;
        /** 节点名称  */
        name?: string;
        /** 隶属组件节点标识 */
        componentId?: string;
    };
    /**
     * 在节点中保存的表单原始DOM对象(注意：在收折起来的控件树上rawElement并非原始DOM对象，所以针对这个属性的修改不会映射到DOM中。)
     */
    rawElement?: any;
    /**
     * 子节点集合
     */
    children?: ControlTreeNode[];
    /**
     * 节点类型，包括：组件、视图模型、控件集合、控件四种类型。
     */
    type?: string;
    /** 是否展开 */
    expanded?: boolean;
    /** 叶子节点图标 */
    icon?: any;
    /** 节点展开图标 */
    expandedIcon?: any;
    /** 节点折叠图标 */
    collapsedIcon?: any;
    /** 树节点在其父节点下的索引号 */
    index?: number;
    /** 父节点标识 */
    parentNodeId?: string;
    /** 父节点 */
    parent?: ControlTreeNode;
    controlIcon?: string;
    hideContextMenuIcon?: boolean;
    dependentParentControl?: string;
    tips?: string;
    rawParentNodeId?: string;
}

/**
 * 控件树右键菜单配置
 */
export declare class ControlTreeContextMenuConfig {
    command?: string;

    name: string;

    controlType?: string;

    isUnique?: boolean;

    subMenus?: any;

    divider?: boolean;

    [propName: string]: any;
};
