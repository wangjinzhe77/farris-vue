
/* eslint-disable no-use-before-define */
import { SetupContext, defineComponent, ref, inject, onMounted } from 'vue';
import { FTreeView } from '@farris/ui-vue/components/tree-view';
import { designerOutlineProps, DesignerOutlineProps } from './designer-outline.props';
import { useOutlineNode } from './composition/use-outline-node';
import { useDataView } from './composition/use-data-view';

import './designer-outline.css';

export default defineComponent({
    name: 'FDesignerOutline',
    props: designerOutlineProps,
    emits: ['outputValue', 'currentEvent', 'selectionChanged'] as string[] | undefined,
    setup(props: DesignerOutlineProps, context: SetupContext) {

        const treeRef = ref();

        /** 树表是否支持可选状态 */
        const selectable = ref(props.selectable);
        /** 选中父节点会自动选中子节点 */
        const autoCheckChildren = ref(props.autoCheckChildren);
        /** 返回值展示父节点和子节点 */
        const cascade = ref(props.cascade);
        /** 是否显示图标集 */
        const showTreeNodeIcons = ref(props.showTreeNodeIcons);
        /** 是否显示连接线 */
        const showLines = ref(props.showLines);
        /** 新增值 */
        const newDataItem = ref(props.newDataItem);
        /** 连接线颜色 */
        const lineColor = ref(props.lineColor);
        /** 单元格高度 */
        const cellHeight = ref(props.cellHeight);

        const designerHostService: any = inject('designer-host-service');
        const useOutlineNodeComposition = useOutlineNode(designerHostService, context);
        const { onChanged, currentSelectedNodeId } = useOutlineNodeComposition;

        const { getData } = useDataView(props, useOutlineNodeComposition);

        /** 树表绑定数据 */
        let treeViewData: any[] = getData();

        /** 刷新控件树 */
        function refreshControlTree() {
            treeViewData = getData();
            if (treeRef.value && treeRef.value.updateDataSource) {
                treeRef.value.updateDataSource(treeViewData);
                if (currentSelectedNodeId && treeRef.value.clearSelection) {
                    treeRef.value.clearSelection();
                    selectControlTreeNode({ id: currentSelectedNodeId.value });

                }
            }
        }
        /** 触发选中控件树的指定节点 */
        function selectControlTreeNode(schemaValue: any) {
            if (!schemaValue && treeRef.value && treeRef.value.clearSelection) {
                treeRef.value.clearSelection();
            }
            if (schemaValue && treeRef.value && treeRef.value.selectItem) {
                const selectTreeNodeIndex = treeViewData.findIndex(data => data.originalId === schemaValue.id);
                if (selectTreeNodeIndex > -1) {
                    treeRef.value.selectItem(selectTreeNodeIndex);
                    const selectedItem = treeViewData[selectTreeNodeIndex];
                    if (currentSelectedNodeId) {
                        currentSelectedNodeId.value = selectedItem.originalId;
                    }
                } else if (currentSelectedNodeId) {
                    // 解决在控件树上不存在的节点，不能被选中的问题
                    currentSelectedNodeId.value = '';
                }
            }
        }

        context.expose({
            selectControlTreeNode,
            refreshControlTree
        });

        onMounted(() => {
            // 默认选中表单节点
            if (treeViewData.length && treeRef.value) {
                selectControlTreeNode({ id: treeViewData[0].originalId });
            }
        });

        return () => {
            return <div class="designer-control-tree">
                <FTreeView
                    ref={treeRef}
                    data={treeViewData}
                    selectable={selectable.value}
                    autoCheckChildren={autoCheckChildren.value}
                    cascade={cascade.value}
                    showTreeNodeIcons={showTreeNodeIcons.value}
                    showLines={showLines.value}
                    newDataItem={newDataItem.value}
                    lineColor={lineColor.value}
                    cellHeight={cellHeight.value}
                    iconField="controlIcon"
                    onSelectionChange={onChanged}></FTreeView>
            </div>;
        };
    }
});
