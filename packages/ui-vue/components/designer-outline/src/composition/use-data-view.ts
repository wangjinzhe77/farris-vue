/* eslint-disable no-use-before-define */
import { ref } from 'vue';
import { DesignerOutlineProps } from '../designer-outline.props';
import { cloneDeep } from 'lodash-es';
import { UseDataView, UseOutlineNode } from './types';

/**
 * 组装控件树的数据集
 */
export function useDataView(
    props: DesignerOutlineProps,
    useOutlineNodeComposition: UseOutlineNode
): UseDataView {

    const domJsonComponents = props.data.module ? ref(props.data.module.components[0]) : ref(props.data);
    const { getIcon, getTitle } = useOutlineNodeComposition;

    /** 处理子组件 */
    function handleChildComponent(componentsItem: any, findComponents: any, number: number, parentComponent: any) {
        const childComponentItem = props.data.module?.components.find((component: any) => component.id === componentsItem.component);
        if (childComponentItem) {
            handleInputData([childComponentItem], findComponents, number, parentComponent);
        }
    }

    /** 处理数据 */
    function handleInputData(components: any, findComponents: any, number: number, parentComponent: any) {
        components.forEach((componentsItem: any) => {

            if (componentsItem.type === 'component-ref') {
                handleChildComponent(componentsItem, findComponents, number, parentComponent);
                return;
            }
            const findComponentsItem = {
                originalId: componentsItem.id,
                layer: number,
                originalParent: parentComponent?.id,
                name: getTitle(componentsItem, parentComponent),
                type: componentsItem.type,
                controlIcon: getIcon(componentsItem),
                rawSchema: componentsItem
            };
            findComponents.push(cloneDeep(findComponentsItem));

            if (componentsItem.contents || componentsItem.buttons) {
                handleInputData(componentsItem.contents || componentsItem.buttons, findComponents, number + 1, componentsItem);
            }
        });
        return findComponents;
    }

    /** 按照treeview输入值排序 */
    function getSortedData(flatData: any) {
        const sortedData: any = [];
        const rootComponent = flatData.find((flatDataItem: any) => flatDataItem.layer === 0);
        flatData = flatData.filter((flatDataItem: any) => flatDataItem.layer !== 0);
        sortedData.push(rootComponent);
        let neededId = rootComponent.originalId;
        let back = 0;
        // 死循环时跳出
        let lock = 1000;
        while (flatData.length !== 0 || lock === 0) {
            const result = flatData.find((flatDataItem: any) => flatDataItem.originalParent === neededId);
            if (result) {
                sortedData.push(result);
                neededId = result.originalId;
                flatData = flatData.filter((flatDataItem: any) => flatDataItem.originalId !== neededId);
                back = 0;
            } else {
                // 若非同层级，则找其父级的子
                back -= 1;
                neededId = sortedData.slice(back)[0].originalId;
            }
            lock -= 1;
        }
        return sortedData;
    }

    function formTreeViewFrame(originData: any) {
        originData.forEach((originDataItem: any, index: number) => {
            originDataItem.id = (index + 1).toString();
        });
        originData.forEach((originDataItem: any) => {
            originDataItem.parent = originData.find((item: any) => item.originalId === originDataItem.originalParent)?.id || '';
        });
        originData.forEach((originDataItem: any) => {
            originDataItem.hasChildren = originData.findIndex((item: any) => item.parent === originDataItem.id) > 0;
        });
        return originData;
    }

    /**
     * 追加表单顶层节点
     * @param inputData
     * @returns
     */
    function appendFormPageNode(inputData: any[]) {
        // 表单顶层节点
        const frameNode = {
            originalId: props.data.module.id,
            layer: 0,
            originalParent: null,
            name: '页面',
            type: 'module',
            controlIcon: 'fd-i-Family fd_pc-module',
            rawSchema: props.data.module

        };
        inputData[0].originalParent = frameNode.originalId;
        inputData.unshift(frameNode);
        return frameNode;
    }
    /** 获取treeview所需数据 */
    function getData() {
        let outputDataWithTreeViewFrame;
        if (domJsonComponents.value) {
            const components: any[] = [];

            components.push(domJsonComponents.value);
            const findComponents: any = [];
            const number = 1;
            const inputData = handleInputData(components, findComponents, number, null);
            appendFormPageNode(inputData);
            const outputData = getSortedData(inputData);
            outputDataWithTreeViewFrame = formTreeViewFrame(outputData);
        }
        return outputDataWithTreeViewFrame;
    }

    return { getData };
}
