import { Ref } from "vue";

export interface UseDataView {
    getData: () => any;
}

export interface UseOutlineNode {
    getIcon: (componentsItem: any) => string;

    getTitle: (componentsItem: any, parentComponentsItem: any) => any;

    onChanged: (selections: any[]) => any;

    currentSelectedNodeId?: Ref<string>;
}
