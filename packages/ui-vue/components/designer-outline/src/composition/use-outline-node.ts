
import { DgControl } from '../../../designer-canvas/src/composition/dg-control';
import { ref, SetupContext } from 'vue';
import { ComponentType } from '../types';
import { UseOutlineNode } from './types';
import { DesignerHostService } from '../../../designer-canvas/src/composition/types';

/**
 * 组装控件树节点的展示文本、图标
 */
export function useOutlineNode(designerHostService: DesignerHostService, context: SetupContext): UseOutlineNode {

    /** 当前选中的树节点id */
    const currentSelectedNodeId = ref('');
    /**
     * 获取树节点图标
     */
    function getIcon(componentsItem: any) {
        let classString = 'controlIcon mr-2 fd-i-Family ';

        let controlIcon = '';
        switch (componentsItem?.type) {
            // 输入类控件，根据editor来区分
            case 'form-group': {
                controlIcon = componentsItem.editor && componentsItem.editor.type ? `fd_pc-${componentsItem.editor.type}` : '';
                break;
            }
            default: {
                classString += `fd_pc-${componentsItem.type}`;
            }
        }

        classString += controlIcon;
        return classString;
    }

    /**
     * 获取分栏面板的分栏项的展示名称。判断条件为布局的方向和分栏顺序。
     * @param componentsItem 分栏项
     * @param parentComponentsItem 分栏面板
     */
    function getSplitterPaneTitle(componentsItem: any, parentComponentsItem: any) {
        let text = componentsItem.id;
        const splitterPanes = parentComponentsItem?.contents;

        // 只识别两个分栏项的场景
        if (parentComponentsItem?.type === 'splitter' && splitterPanes && splitterPanes.length === 2) {

            // 是否水平布局
            const isHorizontalDirection = parentComponentsItem.direction !== 'column';
            // 第一个区域
            if (componentsItem.id === splitterPanes[0]?.id) {
                text = isHorizontalDirection ? '左侧区域' : '上方区域';
            } else {
                // 第二个区域
                text = isHorizontalDirection ? '右侧区域' : '下方区域';
            }
        }
        return text;
    }

    /**
     * 获取容器类控件的展示名称
     */
    function getContentContainerTitle(componentsItem: any) {
        if (componentsItem.isLikeCardContainer) {
            return '区块';
        }
        const elementClass = componentsItem.appearance && componentsItem.appearance.class || '';
        const elementClassList = elementClass.split(' ');
        if (componentsItem.id === 'page-header' && elementClass && elementClass === 'f-page-header') {
            return '页头';
        }
        if (elementClassList.includes('f-page-header-base')) {
            return '页头容器';
        }
        if (elementClassList.includes('f-page-header-extend')) {
            return '页头扩展容器';
        }
        if (elementClassList.includes('f-title')) {
            return '标题容器';
        }
        if (elementClassList.includes('f-page')) {
            return '根容器';
        }
        if (elementClassList.includes('f-page-main')) {
            return '内容区域';
        }
        if (elementClassList.includes('f-scrollspy-content') && componentsItem.isScrollspyContainer) {
            return '滚动监听容器';
        }
        if (elementClassList.includes('f-grid-is-sub')) {
            return '表格容器';
        }

        if (elementClassList.includes('f-filter-container')) {
            return '筛选条容器';
        }
        return '容器';
    }

    function getComponentShowName(componentsItem: any) {
        const { componentType } = componentsItem;

        switch (componentType) {
            case ComponentType.Frame: {
                return '根组件';
            }
            case ComponentType.dataGrid:
            case ComponentType.table: {
                const treeGrid = designerHostService?.formSchemaUtils.selectNode(componentsItem, (item) => item.type === (DgControl['tree-grid'] && DgControl['tree-grid'].type));
                if (treeGrid) {
                    return '树表格组件';
                }
                return '表格组件';
            }
            case ComponentType.attachmentPanel: {
                return '附件组件';
            }
            case ComponentType.listView: {
                return '列表视图组件';
            }
            case ComponentType.modalFrame: {
                return '弹窗页面';
            }
            case ComponentType.appointmentCalendar: {
                return '预约日历组件';
            }
            default: {
                if (componentType.startsWith('form')) {
                    return '卡片组件';
                }
            }
        }

        return '组件';
    }
    function getTitle(componentsItem: any, parentComponentsItem: any) {
        const text = componentsItem.name || componentsItem.text || componentsItem.label || componentsItem.title || componentsItem.mainTitle;

        if (text && typeof (text) === 'string') {
            return text.trim();
        }

        switch (componentsItem.type) {
            case 'content-container': {
                return getContentContainerTitle(componentsItem);
            }
            case 'splitter-pane': {
                return getSplitterPaneTitle(componentsItem, parentComponentsItem);
            }
            case 'component':
                return getComponentShowName(componentsItem);
            default: {
                return DgControl[componentsItem.type] && DgControl[componentsItem.type].name || componentsItem.id;
            }
        }

    }

    function onChanged(selections: any[]) {
        const newSelection = selections.length ? selections[0] : null;
        if (!newSelection) {
            return;
        }
        currentSelectedNodeId.value = newSelection.originalId;
        const designId = newSelection ? `${newSelection.originalId}-design-item` : '';
        const newSelectedElement = document.getElementById(designId) as HTMLElement;

        if (newSelectedElement) {
            newSelectedElement.click();
        }
        context.emit('selectionChanged', newSelection.rawSchema);
    }

    return { getIcon, getTitle, onChanged, currentSelectedNodeId };
}
