import { BaseControlProperty } from "../../../property-panel/src/composition/entity/base-property";

export class ContentContainerProperty extends BaseControlProperty {
    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }
    public getPropertyConfig(propertyData: any) {
        // 基本信息
        this.propertyConfig.categories['basic'] = this.getBasicPropConfig(propertyData);
        // 外观
        this.propertyConfig.categories['appearance'] = this.getAppearanceConfig(propertyData);

        return this.propertyConfig;
    }
}
