import { SetupContext, defineComponent } from 'vue';
import { ContentContainerPropsType, contentContainerProps } from './content-container.props';

export default defineComponent({
    name: 'FContentContainer',
    props: contentContainerProps,
    emits: [],
    setup(props: ContentContainerPropsType, context) {
        return () => {
            return <div class={props.customClass} style={props.customStyle}>{context.slots.default && context.slots.default()}</div>;
        };
    }
});
