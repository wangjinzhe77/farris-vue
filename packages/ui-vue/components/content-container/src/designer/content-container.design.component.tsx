import { SetupContext, computed, defineComponent, inject, onMounted, ref } from 'vue';
import { ContentContainerPropsType, contentContainerProps } from '../content-container.props';
import { useDesignerRulesForContentContainer } from './use-designer-rules';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { getCustomClass } from '../../../common';
import { DesignerHostService } from '../../../designer-canvas/src/composition/types';
export default defineComponent({
    name: 'FContentContainerDesign',
    props: contentContainerProps,
    emits: [],
    setup(props: ContentContainerPropsType, context) {
        const elementRef = ref();
        const designerHostService = inject<DesignerHostService>('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRulesForContentContainer(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

        const containerClass = computed(() => {
            const classObject = {
                'drag-container': true
            } as Record<string, any>;
            return getCustomClass(classObject, props?.customClass);
        });
        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        return () => {
            return (
                <div ref={elementRef} class={containerClass.value} style={props.customStyle} data-dragref={`${designItemContext.schema.id}-container`}>
                    {context.slots.default && context.slots.default()}
                </div>
            );
        };
    }
});
