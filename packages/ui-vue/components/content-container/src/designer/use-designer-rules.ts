import { DesignerHostService, DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { useDragulaCommonRule } from "../../../designer-canvas/src/composition/rule/use-dragula-common-rule";
import { ComponentSchema, DesignerItemContext } from "../../../designer-canvas/src/types";
import { UseTemplateDragAndDropRules } from "../../../designer-canvas/src/composition/rule/use-template-rule";
import { DgControl } from "../../../designer-canvas";
import { ContentContainerProperty } from "../property-config/content-container.property-config";

export function useDesignerRulesForContentContainer(designItemContext: DesignerItemContext, designerHostService?: DesignerHostService): UseDesignerRules {
    const schema = designItemContext.schema as ComponentSchema;

    const dragAndDropRules = new UseTemplateDragAndDropRules();
    /**
     * 判断当前移动的控件是否为筛选方案或者筛选方案外层的分组面板
     */
    function checkIfDraggingQuerySolution(draggingContext: DraggingResolveContext) {
        const { componentType, sourceType, sourceElement } = draggingContext;
        if (sourceType === 'control' && componentType === DgControl['query-solution'].type) {
            return true;
        }
        if (sourceType === 'move' && componentType === DgControl['section'].type && sourceElement?.className?.includes('f-section-scheme')) {
            return true;
        }
        return false;
    }
    /**
     * 只有不可编辑的表格，才支持启用筛选方案
     */
    function getUnEditableDataGrid() {
        const formSchemaUtils = designerHostService?.formSchemaUtils;
        const matchedDataGridComponentRef = formSchemaUtils.selectNode(schema, (item) => {
            if (item.type === 'component-ref') {
                const childComponent = formSchemaUtils.getComponentById(item.component);
                if (childComponent?.componentType === 'data-grid') {
                    const dataGrid = formSchemaUtils.selectNode(childComponent, childItem => childItem.type === 'data-grid' && !childItem.fieldEditable);
                    if (dataGrid) {
                        return true;
                    }
                }
            }
        });
        return matchedDataGridComponentRef;
    }

    /**
     * 判断当前容器是否可接收筛选方案
     */
    function checkCanAcceptQuerySolution(draggingContext: DraggingResolveContext) {
        const containerClass = schema.appearance?.class;
        const { sourceType } = draggingContext;
        const isPageLayer = containerClass && containerClass.includes('f-page-is-managelist');
        if (!isPageLayer) {
            return false;
        }
        if (sourceType === 'control') {
            const solutionExisted = schema.contents && schema.contents.find(content => content.type === DgControl['section'].type && content.appearance?.class?.includes('f-section-scheme'));
            return !solutionExisted && getUnEditableDataGrid();
        }
        if (sourceType === 'move') {
            return true;
        }
        return false;
    }
    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {

        if (checkIfDraggingQuerySolution(draggingContext)) {
            return checkCanAcceptQuerySolution(draggingContext);
        }

        const basalRule = useDragulaCommonRule().basalDragulaRuleForContainer(draggingContext, designerHostService);
        if (!basalRule) {
            return false;
        }
        const { canAccept } = dragAndDropRules.getTemplateRule(designItemContext, designerHostService);

        return canAccept;
    }

    function getStyles() {
        const component = schema;
        if (component.componentType) {
            return 'display:inherit;flex-direction:inherit;margin-bottom:10px';
        }
        return '';
    }

    function checkCanMoveComponent() {
        const { canMove } = dragAndDropRules.getTemplateRule(designItemContext, designerHostService);
        return canMove;
    }
    function checkCanDeleteComponent() {
        const { canDelete } = dragAndDropRules.getTemplateRule(designItemContext, designerHostService);
        return canDelete;
    }

    function hideNestedPaddingInDesginerView() {
        const { canMove, canDelete } = dragAndDropRules.getTemplateRule(designItemContext, designerHostService);
        return !canMove && !canDelete;
    }
    /**
     * 获取属性配置
     */
    function getPropsConfig(componentId: string) {
        const componentProp = new ContentContainerProperty(componentId, designerHostService);
        const { schema } = designItemContext;
        return componentProp.getPropertyConfig(schema);
    }
    return {
        canAccepts,
        getStyles,
        checkCanMoveComponent,
        checkCanDeleteComponent,
        hideNestedPaddingInDesginerView,
        getPropsConfig
    };
}
