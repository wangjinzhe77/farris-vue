 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App, Plugin } from 'vue';
import DatePickerCalendar from './src/components/calendar/calendar.component';
import DatePickerCalendarNavBar from './src/components/calendar-navbar/calendar-navbar.component';
import DatePickerContainer from './src/components/date-picker-container/date-picker-container.component';
import DatePickerMonthView from './src/components/month/month.component';
import DatePickerYearView from './src/components/year/year.component';
import DatePicker from './src/date-picker.component';
import FDatePickerDesign from './src/designer/date-picker.design.component';
import FDateViewDesign from './src/designer/date-view.design.component';
import { datePickerPropsResolver } from './src/date-picker.props';
import { dateViewPropsResolver } from './src/components/date-picker-container/date-picker-container.props';

export * from './src/components/calendar/calendar.props';
export * from './src/date-picker.props';

export * from './src/types/calendar';
export * from './src/types/common';
export * from './src/types/date-model';
export * from './src/types/month';
export * from './src/types/year';

DatePicker.install = (app: App) =>  {
    app.component(DatePickerCalendar.name as string, DatePickerCalendar)
        .component(DatePickerCalendarNavBar.name as string, DatePickerCalendarNavBar)
        .component(DatePickerContainer.name as string, DatePickerContainer)
        .component(DatePickerMonthView.name as string, DatePickerMonthView)
        .component(DatePickerYearView.name as string, DatePickerYearView)
        .component(DatePicker.name as string, DatePicker);
};
DatePicker.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap['date-picker'] = DatePicker;
    componentMap['date-view'] = DatePickerContainer;
    propsResolverMap['date-picker'] = datePickerPropsResolver;
    propsResolverMap['date-view'] = dateViewPropsResolver;
};
DatePicker.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['date-picker'] = FDatePickerDesign;
    componentMap['date-view'] = FDateViewDesign;
    propsResolverMap['date-picker'] = datePickerPropsResolver;
    propsResolverMap['date-view'] = dateViewPropsResolver;
};

export { DatePicker, DatePickerCalendar };
export default DatePicker as typeof DatePicker & Plugin;
