/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, onMounted, ref, SetupContext, watch } from 'vue';
import { DatePickerProps, datePickerProps } from './date-picker.props';
import FButtonEdit from '@farris/ui-vue/components/button-edit';
import FDatePickerContainer from './components/date-picker-container/date-picker-container.component';
import FDateRange from './components/date-range/date-range.component';
import { useDateFormat } from '@farris/ui-vue/components/common';
import { DateObject } from './types/common';
import './date-picker.css';

export default defineComponent({
    name: 'FDatePicker',
    props: datePickerProps,
    emits: ['update:modelValue', 'datePicked'] as (string[] & ThisType<void>) | undefined,
    setup(props: DatePickerProps, context: SetupContext) {
        const groupIcon = '<span class="f-icon f-icon-date"></span>';
        const modelValue = ref(props.modelValue);
        const buttonEdit = ref<any>(null);
        /** 最晚年限 */
        const maxYear = ref(props.maxYear);
        /** 最早年限 */
        const minYear = ref(props.minYear);
        /** 是否允许日期范围 */
        const enablePeriod = ref(props.enablePeriod);
        const realValue = ref(modelValue.value);
        /** 是否高亮周六 */
        const highlightSaturday = ref(props.highlightSaturday);
        /** 是否高亮周日 */
        const highlightSunday = ref(props.highlightSunday);
        /** 每月缩写 */
        const nameOfMonths = ref(props.nameOfMonths);
        /** 高亮日期 */
        const highlightDates = ref(props.highlightDates);
        /** 显示第几周 */
        const showWeekNumber = ref(props.showWeekNumber);
        /** 显示方式 */
        const selectMode = ref(props.selectMode);
        /** 是否显示时间 */
        const displayTime = ref(props.displayTime);
        /** 禁用周末 */
        const disableWeekends = ref(props.disableWeekends);
        /** 禁用特定日 */
        const disableWeekdays = ref(props.disableWeekdays);
        /** 自...禁用 */
        const disableSince = ref([props.disableSince]);
        /** 禁用至该范围 */
        const disableUntil = ref([props.disableUntil]);

        const disable = ref(props.disabled);
        const readonly = ref(props.readonly);
        const enableClear = ref(props.enableClear);
        const editable = ref(props.editable);

        const { formatTo } = useDateFormat();

        function onClickButton() { }

        const displayDate = computed(() => {
            return formatTo(realValue.value, props.displayFormat);
        });

        function setModelValue(dateValue: string, emit = true) {
            const formattedValue = formatTo(dateValue, props.valueFormat);
            modelValue.value = formattedValue;
            realValue.value = formattedValue;
            if (emit) {
                context.emit('update:modelValue', formattedValue);
            }
        }

        function closeCalendarPanel() {
            if (buttonEdit.value) {
                buttonEdit.value.hidePopup();
            }
        }

        function onDatePicked(dateValue: DateObject) {
            let dateValueString = `${dateValue.year}-${dateValue.month}-${dateValue.day}`;
            if (props.showTime) {
                dateValueString += ` ${dateValue.hour}:${dateValue.minute}:${dateValue.second}`;
            }
            setModelValue(dateValueString);
            context.emit('datePicked', realValue.value);

            closeCalendarPanel();
        }

        function onClearDate() {
            setModelValue('');
            context.emit('datePicked', realValue.value);
        }

        watch(() => props.modelValue, (newValue, oldValue) => {
            if (newValue !== oldValue) {
                setModelValue(newValue);
            }
        });

        watch(
            [() => props.disabled, () => props.editable, () => props.enableClear, () => props.readonly],
            ([newDisabled, newEditable, newEnableClear,  newReadonly]) => {
                disable.value = newDisabled;
                editable.value = newEditable;
                enableClear.value = newEnableClear;
                readonly.value = newReadonly;
            }
        );

        onMounted(() => {
            if (modelValue.value) {
                setModelValue(modelValue.value);
            }
        });

        const onConfirm = (selectedDate: DateObject) => {
            onDatePicked(selectedDate);
        };

        return () => {
            return enablePeriod.value ? <FDateRange
                {...props}
            >

            </FDateRange> :
                <FButtonEdit
                    ref={buttonEdit}
                    modelValue={displayDate.value}
                    buttonContent={groupIcon}
                    placeholder={props.placeholder}
                    disable={disable.value}
                    readonly={readonly.value}
                    editable={editable.value}
                    enableClear={enableClear.value}
                    popupOnClick={true}
                    onClear={onClearDate}
                    onClickButton={onClickButton}
                    focusOnCreated={props.focusOnCreated}
                    selectOnCreated={props.selectOnCreated}
                    keepWidthWithReference={false}
                    placement={'auto'}>

                    {{
                        default: () =>
                            <FDatePickerContainer
                                mode="Popup"
                                value={realValue.value}
                                valueFormat={props.valueFormat}
                                minYear={minYear.value}
                                maxYear={maxYear.value}
                                enablePeriod={enablePeriod.value}
                                highlightSunday={highlightSunday.value}
                                highlightSaturday={highlightSaturday.value}
                                nameOfMonths={nameOfMonths.value}
                                highlightDates={highlightDates.value}
                                showWeekNumber={showWeekNumber.value}
                                selectMode={selectMode.value}
                                displayTime={displayTime.value}
                                disableWeekends={disableWeekends.value}
                                disableWeekdays={disableWeekdays.value}
                                // disableSince={disableSince.value}
                                disableUntil={disableUntil.value}
                                showTime={props.showTime}
                                onDatePicked={(dateValue: DateObject) => onDatePicked(dateValue)}
                                onConfirm={onConfirm}
                            >

                            </FDatePickerContainer>
                    }}
                </FButtonEdit>;
        };
    }
});
