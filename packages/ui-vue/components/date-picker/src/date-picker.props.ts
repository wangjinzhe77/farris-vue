 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import { DateObject, Period } from './types/common';
import { defaultNameOfMonths } from './types/month';
import { schemaResolver } from './schema/schema-resolver';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import datePickerSchema from './schema/date-picker.schema.json';

export const datePickerProps = {
    /** 允许编辑 */
    editable: { type: Boolean, default: false },
    /** 禁用 */
    disabled: { type: Boolean, default: false },
    /** 同disabled */
    readonly: { type: Boolean, default: false },
    /** 禁用日期 */
    disableDates: { Type: Array<DateObject>, default: [] },
    /** 禁用范围 */
    disablePeriod: { Type: Array<Period>, default: [] },
    /** 自...禁用 */
    disableSince: { Type: Object, default: { year: 0, month: 0, day: 0 } },
    /** 禁用特定日 */
    disableWeekdays: { Type: Array<string>, default: [] },
    /** 禁用周末 */
    disableWeekends: { Type: Boolean, default: false },
    /** 禁用至该范围 */
    disableUntil: { Type: Object, default: { year: 0, month: 0, day: 0 } },
    /** 是否显示时间 */
    displayTime: { Type: Boolean, default: false },
    /** 高亮日期 */
    highlightDates: { Type: Array<DateObject>, default: [] },
    /** 是否高亮周六 */
    highlightSaturday: { Type: Boolean, default: false },
    /** 是否高亮周日 */
    highlightSunday: { Type: Boolean, default: false },
    /** 最晚年限 */
    maxYear: { Type: Number, default: 2500 },
    /** 最早年限 */
    minYear: { Type: Number, default: 1 },
    /** 组件值 */
    modelValue: { type: String, default: '' },
    /** 每月缩写 */
    nameOfMonths: { Type: Object, default: defaultNameOfMonths },
    /** 定界符 */
    // periodDelimiter: { Type: String, default: '' },
    /** 显示方式 */
    selectMode: { Type: String, default: 'day' },
    /** 存储格式 */
    valueFormat: { Type: String, default: 'yyyy-MM-dd' },
    /** 显示格式 */
    displayFormat: { Type: String, default: 'yyyy-MM-dd' },
    /** 是否允许日期范围 */
    enablePeriod: { type: Boolean, default: false },
    /**
     * 作为内嵌编辑器被创建后默认获得焦点
     */
    focusOnCreated: { type: Boolean, default: false },
    /**
     * 背景文字
     */
    placeholder: { type: String, default: '请选择日期' },
    /**
     * 作为内嵌编辑器被创建后默认选中文本
     */
    selectOnCreated: { type: Boolean, default: false },
    /**
     * 是否展示时分秒
     */
    showTime: { type: Boolean, default: false },
    /** 显示第二个日期输入框 */
    showEndDate: { type: Boolean, default: true },
    /** 显示中间图标 */
    showMiddleIcon: { type: Boolean, default: true },
    /** 开始日期 */
    startDateValue: { type: String },
    /** 结束日期 */
    endDateValue: { type: String },
    /** 展示日期范围面板 */
    showPeriod: { type: Boolean, default: true },
    /** 启用删除 */
    enableClear: { type: Boolean, default: true },
} as Record<string, any>;

export type DatePickerProps = ExtractPropTypes<typeof datePickerProps>;

export const datePickerDesignProps = Object.assign({}, datePickerProps, {
    readonly: {}
});

export type DatePickerDesignProps = ExtractPropTypes<typeof datePickerDesignProps>;

export const datePickerPropsResolver = createPropsResolver<DatePickerProps>(datePickerProps, datePickerSchema, schemaMapper, schemaResolver);
