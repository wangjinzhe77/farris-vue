/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DateObject } from "./common";

export interface MonthViewItem {
    date: DateObject;
    disable: boolean;
    displayText: string;
    isCurrent: boolean;
    month: number;
    range?: boolean;
    selected?: boolean;
}

export interface ActiveMonth {
    month: number;
    year: number;
    displayTextOfMonth: string;
    displayTextOfYear: string;
}

export interface NameOfMonths {
    [month: number]: string;
}

// export const defaultNameOfMonths = {
//     1: 'Jan', 2: 'Feb', 3: 'Mar', 4: 'Apr', 5: 'May', 6: 'Jun', 7: 'Jul', 8: 'Aug', 9: 'Sep', 10: 'Oct', 11: 'Nov', 12: 'Dec'
// };

export const defaultNameOfMonths = {
    1: '一月', 2: '二月', 3: '三月', 4: '四月', 5: '五月', 6: '六月', 7: '七月', 8: '八月', 9: '九月', 10: '十月', 11: '十一月', 12: '十二月'
};
