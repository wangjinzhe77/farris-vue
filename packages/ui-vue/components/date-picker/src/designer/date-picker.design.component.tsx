/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, inject, onMounted, ref, SetupContext } from 'vue';
import { DatePickerDesignProps, datePickerDesignProps } from '../date-picker.props';
import FButtonEditDesign from '../../../button-edit/src/designer/button-edit.design.component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDatePickerDesignerRules } from './use-rules';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';

export default defineComponent({
    name: 'FDatePickerDesign',
    props: datePickerDesignProps,
    emits: ['update:modelValue', 'datePicked'] as (string[] & ThisType<void>) | undefined,
    setup(props: DatePickerDesignProps, context: SetupContext) {
        const groupIcon = '<span class="f-icon f-icon-date sss"></span>';
        const modelValue = ref(props.modelValue);
        const elementRef = ref();
        const designerHostService = inject('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDatePickerDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        return () => {
            return (
                <FButtonEditDesign
                    ref={elementRef}
                    v-model={modelValue.value}
                    buttonContent={groupIcon}
                    readonly={true}
                    editable={false}
                    forcePlaceholder={true}
                    placeholder={props.placeholder}
                    enableClear>
                </FButtonEditDesign>
            );
        };
    }
});
