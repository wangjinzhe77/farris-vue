/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import { CalendarWeekItem } from '../../types/calendar';
import { weekDays } from '../../types/common';

export const datePickerCalendarProps = {
    dates: { Type: Array<CalendarWeekItem>, default: [] },
    daysInWeek: { Type: Array<string>, default: weekDays },
    enableKeyboadNavigate: { Type: Boolean, default: true },
    enableMarkCurrent: { Type: Boolean, default: true },
    enablePeriod: { Type: Boolean, default: false },
    firstDayOfTheWeek: { Type: Boolean, default: 'Sun.' },
    selected: { Type: Object, default: null },
    selectedPeriod: { Type: Object, default: null },
    selectedWeek: { Type: Object, default: null },
    selectMode: { Type: String, default: 'day' },
    showWeekNumber: { Type: Boolean, default: false },
    weekTitle: { Type: String, default: 'Week' }
};

export type DatePickerCalendarProps = ExtractPropTypes<typeof datePickerCalendarProps>;
