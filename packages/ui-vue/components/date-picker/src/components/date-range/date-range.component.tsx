import { computed, defineComponent, onMounted, ref, Ref } from "vue";
import { FInputGroup } from "../../../../input-group";
import { DateRangeProps, dateRangeProps } from "./date-range.props";
import FPopover from '../../../../popover/src/popover.component';
import FDatePickerContainer from '../date-picker-container/date-picker-container.component';
import { DateObject } from "../../types/common";
import { useDateFormat } from "../../../../common";

export default defineComponent({
    name: 'FDateRange',
    props: dateRangeProps,
    emits: ['update:modelValue', 'confirm'] as (string[] & ThisType<void>) | undefined,
    setup(props: DateRangeProps, context) {
        const modelValue = ref(props.modelValue);
        /** 最晚年限 */
        const maxYear = ref(props.maxYear);
        /** 最早年限 */
        const minYear = ref(props.minYear);
        /** 是否允许日期范围 */
        // const enablePeriod = ref(props.enablePeriod);
        /** 是否高亮周六 */
        const highlightSaturday = ref(props.highlightSaturday);
        /** 是否高亮周日 */
        const highlightSunday = ref(props.highlightSunday);
        /** 每月缩写 */
        const nameOfMonths = ref(props.nameOfMonths);
        /** 高亮日期 */
        const highlightDates = ref(props.highlightDates);
        /** 显示第几周 */
        const showWeekNumber = ref(props.showWeekNumber);
        /** 显示方式 */
        const selectMode = ref(props.selectMode);
        /** 是否显示时间 */
        // const displayTime = ref(props.displayTime);
        /** 禁用周末 */
        const disableWeekends = ref(props.disableWeekends);
        /** 禁用特定日 */
        const disableWeekdays = ref(props.disableWeekdays);
        /** 自...禁用 */
        const disableSince = ref([props.disableSince]);
        /** 禁用至该范围 */
        const disableUntil = ref([props.disableUntil]);
        /** 展示日期范围 */
        const shouldPopupContent = ref(false);
        //
        const popoverRef = ref();
        const dateRef = ref();

        const { formatTo } = useDateFormat();

        const startDisplayDate = computed(() => formatTo(modelValue.value.split('~')?.[0], props.displayFormat));
        const endDisplayDate = computed(() => formatTo(modelValue.value.split('~')?.[1], props.displayFormat));

        const buttonEditClass = computed(() => {
            const classObject = {
                'f-button-edit': true,
                'f-cmp-inputgroup': true,
                'f-button-edit-nowrap': true
            } as Record<string, boolean>;
            return classObject;
        });

        const inputGroupClass = computed(() => {
            const classObject = {
                'f-cmp-inputgroup': true,
                'input-group': true,
                // 'f-state-disable': disabled.value,
                'f-state-editable': true,
                // 'f-state-readonly': readonly.value,
                // 'f-state-focus': hasFocused.value
            };
            return classObject;
        });

        const showDateRangePanel = () => {
            popoverRef.value?.show(dateRef.value);
        };

        const onClickButton = () => {
            shouldPopupContent.value = !shouldPopupContent.value;
            setTimeout(() => {
                if (shouldPopupContent.value) {
                    showDateRangePanel();
                }
            });
        };

        const hidePopup = () => {
            shouldPopupContent.value = false;
        };

        function closeCalendarPanel() {
            document.body.click();
        }

        const onConfirm = (dateValue: any) => {
            const { startDate, endDate } = dateValue;
            const startDateValueString = `${startDate.year}-${startDate.month}-${startDate.day}`;
            const endDateValueString = `${endDate.year}-${endDate.month}-${endDate.day}`;
            const formatStartDate = formatTo(startDateValueString, props.valueFormat);
            const formatEndDate = formatTo(endDateValueString, props.valueFormat);
            const resultDate = formatStartDate + '~' + formatEndDate;
            modelValue.value = resultDate;
            context.emit('update:modeValue', resultDate);
            context.emit('confirm', resultDate);
            closeCalendarPanel();
        };

        const onDatePicked = (dateValue: any) => {

        };

        return () => {
            return <>
                <div ref={dateRef} class={buttonEditClass.value} id={props.id}>
                    <div class={inputGroupClass.value}>
                        <FInputGroup
                            v-model={startDisplayDate.value}
                            showBorder={false}
                            onFocus={onClickButton}
                        ></FInputGroup>
                        <span class="f-icon f-icon-orientation-arrow sub-input-spliter"></span>
                        <FInputGroup
                            v-model={endDisplayDate.value}
                            showBorder={false}
                            onFocus={onClickButton}
                        ></FInputGroup>
                        <div class="input-group-append">
                            <span class="input-group-text input-group-append-button"
                                v-html={'<span class="f-icon f-icon-date"></span>'}
                                onClick={onClickButton}
                            // onMouseenter={onMouseEnterButton} onMouseleave={onMouseLeaveButton}
                            ></span>
                        </div>
                    </div>
                </div>
                {shouldPopupContent.value && <FPopover
                    ref={popoverRef}
                    id={`${props.id}-popover`}
                    visible={true}
                    placement="bottom-left"
                    host={props.popupHost} keep-width-with-reference={true} fitContent={true}
                    right-boundary={props.popupRightBoundary} minWidth={500}
                    offsetX={props.popupOffsetX}
                    onHidden={hidePopup}>
                    {context.slots.default?.()}
                    <FDatePickerContainer
                        mode="Popup"
                        value={modelValue.value}
                        valueFormat={props.valueFormat}
                        minYear={minYear.value}
                        maxYear={maxYear.value}
                        enablePeriod={true}
                        highlightSunday={highlightSunday.value}
                        highlightSaturday={highlightSaturday.value}
                        nameOfMonths={nameOfMonths.value}
                        highlightDates={highlightDates.value}
                        showWeekNumber={showWeekNumber.value}
                        selectMode={selectMode.value}
                        // displayTime={displayTime.value}
                        disableWeekends={disableWeekends.value}
                        disableWeekdays={disableWeekdays.value}
                        // disableSince={disableSince.value}
                        disableUntil={disableUntil.value}
                        onDatePicked={(dateValue: DateObject) => onDatePicked(dateValue)}
                        onConfirm={(dateValue: string) => onConfirm(dateValue)}
                    >
                    </FDatePickerContainer>
                </FPopover>}
            </>;
        };
    }
});
