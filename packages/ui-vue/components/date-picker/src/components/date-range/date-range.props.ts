import { ExtractPropTypes } from 'vue';
import { datePickerProps } from '../../..';

export const dateRangeProps = {
    ...datePickerProps,
    /** 显示第二个日期输入框 */
    showEndDate: { type: Boolean, default: true },
    /** 显示中间图标 */
    showMiddleIcon: { type: Boolean, default: true },
    /** 开始日期 */
    startDateValue: { type: String },
    /** 结束日期 */
    endDateValue: { type: String },
    /** 绑定值 */
    modelValue: { type: String, default: '' },
    /** 展示日期范围面板 */
    showPeriod: { type: Boolean, default: true },
    /** 启用删除 */
    enableClear: {type: Boolean, default: true},
} as Record<string, any>;

export type DateRangeProps = ExtractPropTypes<typeof dateRangeProps>;
