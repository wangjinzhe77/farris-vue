 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { CalendarWeekItem } from '../../types/calendar';
import { DateObject, Period, SelectMode, weekDays } from '../../types/common';
import { defaultNameOfMonths, MonthViewItem } from '../../types/month';
import { YearViewItem } from '../../types/year';
import { createPropsResolver } from '../../../../dynamic-resolver';
import { schemaMapper } from '../../schema/schema-mapper';
import { schemaResolver } from '../../schema/schema-resolver';
import dateViewSchema from '../../schema/date-view.schema.json';
import propertyConfig from '../../property-config/date-view.property-config.json';

export type DisplayMode = 'Embedded' | 'Popup';

export const datePickerContainerProps = {
    /**  */
    top: { type: Number, default: 0 },
    /**  */
    left: { type: Number, default: 0 },
    /** 位置 */
    position: { type: String, default: 'bottom' },
    /** 是否允许日期范围 */
    enablePeriod: { type: Boolean, default: false },
    dateFormat: { type: String, default: 'yyyy-MM-dd' },
    valueFormat: { type: String, default: 'yyyy-MM-dd' },
    dates: { type: Array<CalendarWeekItem>, default: [] },
    daysInWeek: { type: Array<string>, default: weekDays },
    /** 禁用日期 */
    disableDates: { Type: Array<DateObject>, default: [] },
    /** 禁用范围 */
    disablePeriod: { Type: Array<Period>, default: [] },
    /** 自...禁用 */
    disableSince: { Type: Object, default: { year: 0, month: 0, day: 0 } },
    /** 禁用特定日 */
    disableWeekdays: { Type: Array<string>, default: [] },
    /** 禁用周末 */
    disableWeekends: { Type: Boolean, default: false },
    /** 到...禁用 */
    disableUntil: { Type: Object, default: { year: 0, month: 0, day: 0 } },
    /** 是否允许键盘定位 */
    enableKeyboadNavigate: { type: Boolean, default: true },
    /** 是否启用标记当前 */
    enableMarkCurrent: { type: Boolean, default: true },
    /** 每周第一天 */
    firstDayOfTheWeek: { type: String, default: 'Sun.' },
    /** 高亮日期 */
    highlightDates: { Type: Array<DateObject>, default: [] },
    /** 是否高亮周六 */
    highlightSaturday: { Type: Boolean, default: false },
    /** 是否高亮周日 */
    highlightSunday: { Type: Boolean, default: false },
    /** 最晚年限 */
    maxYear: { Type: Number, default: 2500 },
    /** 最早年限 */
    minYear: { Type: Number, default: 1 },
    /** 模式；Embedded；popup； */
    mode: { Type: String as PropType<DisplayMode>, default: 'Embedded' },
    /** 月份 */
    months: { type: Array<Array<MonthViewItem>>, default: [[]] },
    /** 月份名称 */
    nameOfMonths: { Type: Object, default: defaultNameOfMonths },
    /** 二级日期 */
    secondaryDates: { type: Array<CalendarWeekItem>, default: [] },
    /** 二级月份 */
    secondaryMonths: { type: Array<Array<MonthViewItem>>, default: [[]] },
    /** 选择的时间*/
    selectedDate: { type: Object, default: null },
    /** 日期范围组件结束日期 */
    selectedSecondDate: { type: Object, default: null },
    /** 选择的月份 */
    selectedMonth: { type: Object, default: null },
    /** 选择期限 */
    selectedPeriod: { type: Object, default: null },
    /** 选择周 */
    selectedWeek: { type: Object, default: null },
    /** 选择方式 */
    selectMode: { type: String as PropType<SelectMode>, default: 'day' },
    /** 显示第几周 */
    showWeekNumber: { type: Boolean, default: false },
    /** 每周标题 */
    weekTitle: { type: String, default: 'Week' },
    /** 年份 */
    years: { Type: Array<Array<YearViewItem>>, default: [[]] },
    /** 日期时间值 */
    value: { type: String, default: null },
    /** 是否展示时分秒 */
    showTime: {type: Boolean, default: false}
} as Record<string, any>;

export type DatePickerContainerProps = ExtractPropTypes<typeof datePickerContainerProps>;

export const dateViewPropsResolver = createPropsResolver<DatePickerContainerProps>(datePickerContainerProps, dateViewSchema, schemaMapper, schemaResolver, propertyConfig);
