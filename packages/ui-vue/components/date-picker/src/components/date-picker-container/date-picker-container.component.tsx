/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, onMounted, ref, SetupContext, watch } from "vue";
import { datePickerContainerProps, DatePickerContainerProps, DisplayMode } from "./date-picker-container.props";
import FCalenderNavBar from '../calendar-navbar/calendar-navbar.component';
import FTimePickerTimeView from '../../../../time-picker/src/components/time.component';
import CalenderView from '../calendar/calendar.component';
import MonthView from '../month/month.component';
import YearView from '../year/year.component';
import { useDisableDate } from "../../composition/use-disable-date";
import { UseDisableDate } from "../../composition/types";
import useCalendar from "../../composition/use-calendar";
import { DateObject } from "../../types/common";
import { useDate } from "../../composition/use-date";
import { ActiveMonth } from "../../types/month";
import { useMonth } from "../../composition/use-month";
import { useYear } from "../../composition/use-year";
import { FButton } from "../../../../button";
import { TimeValueText } from "../../../../time-picker/src/composition/types";
import { convertToDate, convertToString } from "../../../../time-picker/src/composition/utils";
import { useDateFormat } from '../../../../common/date/use-date-format';
import { CalenderDayItem } from "../../types/calendar";

export default defineComponent({
    name: 'FDateView',
    props: datePickerContainerProps,
    emits: ['datePicked', 'confirm'] as (string[] & ThisType<void>) | undefined,
    setup(props: DatePickerContainerProps, context: SetupContext) {

        const timePicker = ref();
        const timeValue = ref('');
        /** 模式 */
        const displayMode = ref<DisplayMode>(props.mode);
        const top = ref<number>(props.top);
        const left = ref<number>(props.left);
        /** 位置 */
        const position = ref<string>(props.position);
        /** 是否允许日期范围 */
        const enablePeriod = ref(props.enablePeriod);
        /** 选择方式 */
        const selectMode = ref(props.selectMode);
        /** 显示日期格式 */
        const dateFormat = ref(props.dateFormat);
        /** 月份名称 */
        const nameOfMonths = ref<any>(props.nameOfMonths);
        /** 存储格式 */
        const valueFormat = ref(props.valueFormat);

        const secondaryYears = ref<any[]>([]);
        const selectingMonth = ref(false);
        const selectingSecondaryMonth = ref('');
        const selectingYear = ref(false);
        const selectingSecondaryYear = ref('');
        const selectTime = ref();
        const disablePrePage = ref(false);
        const disablePreRecord = ref(false);
        const disableNextRecord = ref(false);
        const disableNextPage = ref(false);
        const disableSecondaryPrePage = ref(false);
        const disableSecondaryPreRecord = ref(false);
        const disableSecondaryNextRecord = ref(false);
        const disableSecondaryNextPage = ref(false);
        /** 二级日期 */
        const secondaryDates = ref(props.secondaryDates);
        /** 每日 */
        const daysInWeek = ref(props.daysInWeek);
        /** 是否允许键盘定位 */
        const enableKeyboadNavigate = ref(props.enableKeyboadNavigate);
        /** 是否启用标记当前 */
        const enableMarkCurrent = ref(props.enableMarkCurrent);
        /** 每周第一天 */
        const firstDayOfTheWeek = ref(props.firstDayOfTheWeek);
        /** 显示第几周 */
        const showWeekNumber = ref(props.showWeekNumber);
        /** 选择的日期*/
        const selectedDate = ref(props.selectedDate);
        /** 选择的结束日期 */
        const selectedSecondDate = ref(props.selectedSecondDate);
        /** 当前存储的日期值 */
        const dateValue = ref(props.value);
        /** 选择周 */
        const selectedWeek = ref(props.selectedWeek);
        /** 选择期限 */
        const selectedPeriod = ref(props.selectedPeriod);
        /** 每周标题 */
        const weekTitle = ref(props.weekTitle);
        /** 二级月份 */
        const secondaryMonths = ref(props.secondaryMonths);
        /** 选择的月份 */
        const selectedMonth = ref<DateObject>(props.selectedMonth);
        /** 最早年限 */
        const minYear = ref(props.minYear);
        /** 最晚年限 */
        const maxYear = ref(props.maxYear);
        /** 自...禁用 */
        const disableSince = ref(props.disableSince);
        /** 到...禁用 */
        const disableUntil = ref(props.disableUntil);
        /** 禁用日期 */
        const disableDates = ref(props.disableDates);
        /** 禁用范围 */
        const disablePeriod = ref(props.disablePeriod);
        /** 禁用特定日 */
        const disableWeekdays = ref(props.disableWeekdays);
        /** 禁用周末 */
        const disableWeekends = ref(props.disableWeekends);

        const refDisableDate: UseDisableDate = useDisableDate(
            minYear.value,
            maxYear.value,
            disableSince.value,
            disableUntil.value,
            disableDates.value,
            disablePeriod.value,
            disableWeekends.value,
            disableWeekdays.value
        );

        const { formatTo } = useDateFormat();

        const { generateCalendar } = useCalendar(refDisableDate);
        const { getToday, getDateObject } = useDate();
        const { generateMonths, getNextMonth, getPreviousMonth } = useMonth();
        const { generateYears } = useYear();

        const today = getToday();
        const selectedDateObj = computed(() => {
            if (dateValue.value) {
                return getDateObject(dateValue.value.split(' ')[0], valueFormat.value.split(' ')[0]);
            }
            return today;
        });

        selectedDate.value = selectedDateObj.value;

        const activeMonth = ref<ActiveMonth>({
            year: selectedDateObj.value.year || 1,
            month: selectedDateObj.value.month || 1,
            displayTextOfMonth: nameOfMonths.value[selectedDateObj.value.month || '1'],
            displayTextOfYear: `${selectedDateObj.value.year}`
        });
        const initSecondaryYear = today.month === 12 ? (today.year || 1) + 1 : today.year;
        const initSecondaryMonth = (today.month || 1) < 12 ? (today.month || 1) + 1 : 1;
        const secondaryActiveMonth = ref({
            year: initSecondaryYear,
            month: initSecondaryMonth,
            displayTextOfMonth: nameOfMonths.value[today.month || '1'],
            displayTextOfYear: `${initSecondaryYear}`
        });

        const dates = computed(() => {
            const weekItems = generateCalendar(
                activeMonth.value.month,
                activeMonth.value.year,
                firstDayOfTheWeek.value,
                [],
                { marked: true, color: '' },
                props.highlightDates,
                props.highlightSaturday,
                props.highlightSunday,
                showWeekNumber.value);
            return weekItems;
        });

        const secondaryRealDates = computed(() => {
            return generateCalendar(
                secondaryActiveMonth.value.month,
                secondaryActiveMonth.value.year as number,
                firstDayOfTheWeek.value,
                [],
                { marked: true, color: '' },
                props.highlightDates,
                props.highlightSaturday,
                props.highlightSunday,
                showWeekNumber.value
            );
        });

        const months = computed(() => {
            const monthViewItems = generateMonths(
                nameOfMonths.value,
                { year: activeMonth.value.year, month: activeMonth.value.month },
                disableSince.value,
                disableUntil.value
            );
            return monthViewItems;
        });

        const years = computed(() => {
            const yearViewItems = generateYears(
                activeMonth.value.year,
                { year: activeMonth.value.year, month: activeMonth.value.month },
                minYear.value,
                maxYear.value,
                disableSince.value,
                disableUntil.value
            );
            return yearViewItems;
        });

        const containerClass = computed(() => {
            const classObject = {
                'f-datepicker-container': true
            } as Record<string, boolean>;
            const className = `container-position-${position.value}`;
            classObject[className] = true;
            return classObject;
        });

        watch(() => props.enablePeriod, (newValue, oldValue) => {
            if (newValue !== oldValue) {
                enablePeriod.value = newValue;
            }
        });

        function getWidth() { return enablePeriod.value ? '' : props.showTime ? '487px' : '287px'; };

        const containerStyle = computed(() => {
            const styleObject = {
                'top': `${top.value}px`,
                'left': `${left.value}px`,
                'width': getWidth(),
                // 'position': displayMode.value === 'Embedded' ? 'relative' : 'absolute',
                'position': 'relative',
                'z-index': displayMode.value === 'Embedded' ? 0 : 9999,
                'margin-top': '0px'
            } as Record<string, any>;
            return styleObject;
        });

        function onCloseSelector($event: KeyboardEvent) { }

        function onClickContainer($event: MouseEvent) {
            $event.stopPropagation();
        }

        function navigateToPreviousPage($event: any, inPeriod: boolean) {
            const previousYear = activeMonth.value.year - (selectingYear.value ? 10 : 1);
            const previous = {
                year: previousYear,
                month: activeMonth.value.month,
                displayTextOfMonth: nameOfMonths.value[activeMonth.value.month || '1'],
                displayTextOfYear: `${previousYear}`
            };
            activeMonth.value = previous;
        }

        function navigateToPreviousMonth($event: any, inPeriod: boolean) {
            const previousMonthDate = getPreviousMonth(activeMonth.value.month, activeMonth.value.year);
            const previous = {
                year: previousMonthDate.year || 1,
                month: previousMonthDate.month || 1,
                displayTextOfMonth: nameOfMonths.value[previousMonthDate.month || '1'],
                displayTextOfYear: `${previousMonthDate.year}`
            };
            activeMonth.value = previous;
        }

        function navigateToNextMonth($event: any, inPeriod: boolean) {
            const nextMonthDate = getNextMonth(activeMonth.value.month, activeMonth.value.year);
            const next = {
                year: nextMonthDate.year || 1,
                month: nextMonthDate.month || 1,
                displayTextOfMonth: nameOfMonths.value[nextMonthDate.month || '1'],
                displayTextOfYear: `${nextMonthDate.year}`
            };
            activeMonth.value = next;
        }

        function navigateToNextPage($event: any, inPeriod: boolean) {
            let nextYear = activeMonth.value.year + 1;
            if (selectingYear.value) {
                nextYear = years.value[3][0].year + 2;
            }
            const next = {
                year: nextYear,
                month: activeMonth.value.month,
                displayTextOfMonth: nameOfMonths.value[activeMonth.value.month || '1'],
                displayTextOfYear: `${nextYear}`
            };
            activeMonth.value = next;
        }

        function navigateToMonthView(inPeriod: boolean) {
            selectingMonth.value = !selectingMonth.value;
            selectingYear.value = false;
        }

        function navigateToYearView(inPeriod: boolean) {
            selectingMonth.value = false;
            selectingYear.value = !selectingYear.value;
        }

        const primaryCalendarNavBarProps = computed(() => {
            return {
                'active-month': activeMonth.value,
                'date-format': dateFormat.value,
                'disable-pre-page': disablePrePage.value,
                'disable-pre-record': disablePreRecord.value,
                'disable-next-record': disableNextRecord.value,
                'disable-next-page': disableNextPage.value,
                'years': years.value,
                'selecting-month': selectingMonth.value,
                'selecting-year': selectingYear.value,
                'select-mode': selectMode.value
            };
        });

        const secondaryCalendarNavBarProps = computed(() => {
            return {
                'active-month': secondaryActiveMonth.value,
                'date-format': dateFormat.value,
                'disable-pre-page': disableSecondaryPrePage.value,
                'disable-pre-record': disableSecondaryPreRecord.value,
                'disable-next-record': disableSecondaryNextRecord.value,
                'disable-next-page': disableSecondaryNextPage.value,
                'years': secondaryYears.value,
                'selecting-month': selectingSecondaryMonth.value,
                'selecting-year': selectingSecondaryYear.value,
                'select-mode': selectMode.value
            };
        });

        function renderNavBar(inPeriod: boolean, calendarNavBarProps: any) {
            return (
                <FCalenderNavBar {...calendarNavBarProps}
                    onPrePage={($event: any) => navigateToPreviousPage($event, inPeriod)}
                    onPreRecord={($event: any) => navigateToPreviousMonth($event, inPeriod)}
                    onNextRecord={($event: any) => navigateToNextMonth($event, inPeriod)}
                    onNextPage={($event: any) => navigateToNextPage($event, inPeriod)}
                    onClickMonth={($event: any) => navigateToMonthView(inPeriod)}
                    onClickYear={($event: any) => navigateToYearView(inPeriod)}
                ></FCalenderNavBar>
            );
        }

        const shouldShowCalendarView = computed(() => {
            return (!selectingMonth.value && !selectingYear.value && !selectTime.value) ||
                (enablePeriod.value && selectMode.value !== 'week' && !selectingSecondaryMonth.value
                    && !selectingSecondaryYear.value && !selectTime.value);
        });

        const shouldShowMonthView = computed(() => {
            return selectingMonth.value && !selectingYear.value;
        });

        const shouldShowYearView = computed(() => {
            return selectingYear.value && !selectingMonth.value;
        });

        const primaryCalendarProps = computed(() => {
            return {
                dates: dates.value,
                daysInWeek: daysInWeek.value,
                enableKeyboadNavigate: enableKeyboadNavigate.value,
                enableMarkCurrent: enableMarkCurrent.value,
                enablePeriod: enablePeriod.value,
                firstDayOfTheWeek: firstDayOfTheWeek.value,
                selected: selectedDate.value,
                selectedPeriod: selectedPeriod.value,
                selectedWeek: selectedWeek.value,
                selectMode: selectMode.value,
                showWeekNumber: showWeekNumber.value,
                weekTitle: weekTitle.value
            };
        });

        const secondaryCalendarProps = computed(() => {
            return {
                dates: secondaryRealDates.value,
                daysInWeek: daysInWeek.value,
                enableKeyboadNavigate: enableKeyboadNavigate.value,
                enableMarkCurrent: enableMarkCurrent.value,
                enablePeriod: enablePeriod.value,
                firstDayOfTheWeek: firstDayOfTheWeek.value,
                selected: selectedSecondDate.value,
                selectedPeriod: selectedPeriod.value,
                selectMode: selectMode.value,
                showWeekNumber: showWeekNumber.value,
                weekTitle: weekTitle.value
            };
        });

        function onClickDay($event: { event: Event, dayItem: CalenderDayItem }, isPeriod: boolean, type: string) {
            const { event, dayItem } = $event;
            const currentDate = dayItem.date;

            if (dayItem.tag === 1) {
                navigateToPreviousMonth(event, isPeriod);
            } else if (dayItem.tag === 3) {
                navigateToNextMonth(event, isPeriod);
            }

            if (!props.showTime) {
                if (type === 'start') {
                    selectedDate.value = currentDate;
                } else {
                    selectedSecondDate.value = currentDate;
                }
                context.emit('datePicked', currentDate);
            } else {
                const { year, month, day } = currentDate;
                selectedDate.value.year = year;
                selectedDate.value.month = month;
                selectedDate.value.day = day;
                // // 赋值时间
                // const dateAndTimeFormatList = valueFormat.value.split(' ');
                // const timeFormat = dateAndTimeFormatList[1] || 'HH:mm:ss';
                // const convertedDate = convertToDate(timeValue.value, timeFormat);
                // if (selectedDate.value) {
                //     selectedDate.value.hour = convertedDate?.getHours() || 0;
                //     selectedDate.value.minute = convertedDate?.getMinutes() || 0;
                //     selectedDate.value.second = convertedDate?.getSeconds() || 0;
                // }
            }
        }

        function onClickWeek($event: any) { }

        function onKeyDownCalendar($event: any) { }

        function onMouseEnterCalendar($event: any, isPeriod: boolean) { }

        function onMouseLeaveCalendar($event: any, isPeriod: boolean) { }

        function renderCalender(inPeriod: boolean, calendarProps: any, type = 'start') {
            return shouldShowCalendarView.value &&
                <CalenderView {...calendarProps}
                    onClick={(payload: any) => onClickDay(payload, inPeriod, type)}
                    onClickWeek={(payload: any) => onClickWeek(payload)}
                    onKeyDown={(payload: any) => onKeyDownCalendar(payload)}
                    onMouseEnter={(payload: any) => onMouseEnterCalendar(payload, inPeriod)}
                    onMouseLeave={(payload: any) => onMouseLeaveCalendar(payload, inPeriod)}
                ></CalenderView>;
        };

        const monthProps = computed(() => {
            return {
                months: months.value,
                enableMarkCurrent: enableMarkCurrent.value,
                enableKeyboadNavigate: enableKeyboadNavigate.value,
                enablePeriod: enablePeriod.value,
                selected: selectedMonth.value,
                selectedPeriod: selectedPeriod.value
            };
        });

        const secondMonthProps = computed(() => {
            return {
                months: secondaryMonths.value,
                enableMarkCurrent: enableMarkCurrent.value,
                enableKeyboadNavigate: enableKeyboadNavigate.value,
                enablePeriod: enablePeriod.value,
                selected: selectedMonth.value,
                selectedPeriod: selectedPeriod.value
            };
        });

        function onClickMonth(currentMonth: DateObject, isPeriod: boolean) {
            selectingMonth.value = false;
            selectingYear.value = false;
            selectedMonth.value = currentMonth;
            activeMonth.value = {
                month: currentMonth.month || 1,
                displayTextOfMonth: nameOfMonths.value[activeMonth.value.month || '1'],
                year: activeMonth.value.year || 1,
                displayTextOfYear: `${activeMonth.value.displayTextOfYear}`
            };
        }

        function onKeyDownMonthView($event: any) { }

        function onMouseEnterMonthView($event: any, isPeriod: boolean) { }

        function onMouseLeaveMonthView($event: any, isPeriod: boolean) { }

        function renderMonth(inPeriod: boolean, monthProps: any) {
            return <MonthView {...monthProps}
                onClick={(payload: any) => onClickMonth(payload, inPeriod)}
                onKeyDownMonthView={(payload: any) => onKeyDownMonthView(payload)}
                onMouseEnterMonthView={(payload: any) => onMouseEnterMonthView(payload, inPeriod)}
                onMouseLeaveMonthView={(payload: any) => onMouseLeaveMonthView(payload, inPeriod)}
            ></MonthView >;
        }

        const yearProps = computed(() => {
            return {
                years: years.value,
                enableKeyboadNavigate: enableKeyboadNavigate.value,
                enableMarkCurrent: enableMarkCurrent.value,
                enablePeriod: enablePeriod.value,
                selected: selectedDate.value,
                selectedPeriod: selectedPeriod.value
            };
        });

        const secondYearProps = computed(() => {
            return {
                years: secondaryYears.value,
                enableKeyboadNavigate: enableKeyboadNavigate.value,
                enableMarkCurrent: enableMarkCurrent.value,
                enablePeriod: enablePeriod.value,
                selected: selectedDate.value,
                selectedPeriod: selectedPeriod.value
            };
        });

        function onClickYear(currentYear: DateObject, isPeriod: boolean) {
            selectingMonth.value = true;
            selectingYear.value = false;
            activeMonth.value = {
                month: activeMonth.value.month,
                displayTextOfMonth: activeMonth.value.displayTextOfMonth,
                year: currentYear.year || 1,
                displayTextOfYear: `${currentYear.year}`
            };
        }

        function onKeyDownYearView($event: any) { }

        function onMouseEnterYearView($event: any, isPeriod: boolean) { }

        function onMouseLeaveYearView($event: any, isPeriod: boolean) { }

        function onConfirm() {
            if (props.showTime) {
                context.emit('confirm', selectedDate.value);
            } else {
                const resultDate = { startDate: {}, endDate: {} };
                if (selectedDate.value) {
                    resultDate.startDate = selectedDate.value;
                }
                if (selectedSecondDate.value) {
                    resultDate.endDate = selectedSecondDate.value;
                }
                context.emit('confirm', resultDate);
            }

        }

        function navigateToToday($event: MouseEvent) {
            activeMonth.value = {
                year: today.year || 1,
                month: today.month || 1,
                displayTextOfMonth: nameOfMonths.value[today.month || '1'],
                displayTextOfYear: `${today.year}`
            };
            selectedDate.value = today;
            context.emit('datePicked', today);
        }

        function navigateToCurrentMonth($event: MouseEvent) {
            activeMonth.value = {
                year: today.year || 1,
                month: today.month || 1,
                displayTextOfMonth: nameOfMonths.value[today.month || '1'],
                displayTextOfYear: `${today.year}`
            };
            selectedDate.value = today;
            selectedMonth.value = { year: today.year, month: today.month };
        }

        function navigateToCurrentYear($event: MouseEvent) {
            activeMonth.value = {
                year: today.year || 1,
                month: today.month || 1,
                displayTextOfMonth: nameOfMonths.value[today.month || '1'],
                displayTextOfYear: `${today.year}`
            };
            selectedDate.value = today;
        }

        const shouldShowSecondCalendar = computed(() => {
            return enablePeriod.value && selectMode.value !== 'week';
        });

        const showConfirmButton = computed(() => (shouldShowCalendarView.value && shouldShowSecondCalendar.value) || props.showTime);

        function renderYear(inPeriod: boolean, yearProps: any) {
            return <YearView {...yearProps}
                onClick={(payload: any) => onClickYear(payload, inPeriod)}
                onKeyDownYearView={(payload: any) => onKeyDownYearView(payload)}
                onClickPreRecord={(payload: any) => navigateToPreviousMonth(payload, inPeriod)}
                onClickNextRecord={(payload: any) => navigateToNextMonth(payload, inPeriod)}
                onMouseEnterYearView={(payload: any) => onMouseEnterYearView(payload, inPeriod)}
                onMouseLeaveYearView={(payload: any) => onMouseLeaveYearView(payload, inPeriod)}
            ></YearView >;
        }

        function formatDate() {
            const dateAndTimeFormatList = valueFormat.value.split(' ');
            const timeFormat = dateAndTimeFormatList[1] || 'HH:mm:ss';
            if (timeValue.value) {
                const convertedDate = convertToDate(timeValue.value, timeFormat);
                timeValue.value = convertToString(convertedDate, timeFormat);
                if (selectedDate.value) {
                    selectedDate.value.hour = convertedDate?.getHours() || 0;
                    selectedDate.value.minute = convertedDate?.getMinutes() || 0;
                    selectedDate.value.second = convertedDate?.getSeconds() || 0;
                }
            } else {
                const { hour, minute, second } = selectedDate.value;
                timeValue.value = `${hour || 0}:${minute || 0}:${second || 0}`;
                const convertedDate = convertToDate(timeValue.value, timeFormat);
                timeValue.value = convertToString(convertedDate, timeFormat);
            }
        }

        onMounted(() => {
            if (props.showTime) {
                const dateAndTimeFormatList = valueFormat.value.split(' ');
                const timeFormat = dateAndTimeFormatList[1] || 'HH:mm:ss';
                timeValue.value = formatTo(dateValue.value, timeFormat);
                formatDate();
            }
        });

        const onValueChangeHandler = (textValue: TimeValueText) => {
            timeValue.value = textValue.text;
            const dateAndTimeFormatList = valueFormat.value.split(' ');
            const timeFormat = dateAndTimeFormatList[1] || 'HH:mm:ss';
            const convertedDate = convertToDate(timeValue.value, timeFormat);
            if (selectedDate.value) {
                selectedDate.value.hour = convertedDate?.getHours() || 0;
                selectedDate.value.minute = convertedDate?.getMinutes() || 0;
                selectedDate.value.second = convertedDate?.getSeconds() || 0;
            }
        };

        return () => {
            return (
                <div class={containerClass.value} style={containerStyle.value} tabindex="0"
                    onKeyup={(payload: KeyboardEvent) => onCloseSelector(payload)}
                    onClick={(payload: MouseEvent) => onClickContainer(payload)}
                >
                    <div class="f-datepicker-content" style="width:287px">
                        {renderNavBar(false, primaryCalendarNavBarProps.value)}
                        {renderCalender(false, primaryCalendarProps.value)}
                        {selectingMonth.value && renderMonth(false, monthProps.value)}
                        {selectingYear.value && renderYear(false, yearProps.value)}
                        <div class="f-datepicker-footer">
                            <div class="f-datepicker-redirect">
                                {shouldShowCalendarView.value && <button class="btn btn-link" onClick={navigateToToday}>今天</button>}
                                {shouldShowMonthView.value && <button class="btn btn-link" onClick={navigateToCurrentMonth}>本月</button>}
                                {shouldShowYearView.value && <button class="btn btn-link" onClick={navigateToCurrentYear}>今年</button>}
                            </div>
                        </div>
                    </div>
                    {props.showTime && <div
                        class="f-datepicker-content position-relative datepicker-content-has-timer"
                        style="width:200px">
                        <div style="height: 41px;
                                border-bottom: 1px solid #E4E7EF;
                                text-align: center;
                                line-height: 40px;
                                font-size: 16px;
                                font-weight: 500;">{timeValue.value}</div>
                        <FTimePickerTimeView style="top:41px;"
                            ref={timePicker}
                            modelValue={timeValue.value}
                            onValueChange={onValueChangeHandler}
                        ></FTimePickerTimeView>
                    </div>}
                    {
                        shouldShowSecondCalendar.value &&
                        <div class="f-datepicker-content">
                            {renderNavBar(true, secondaryCalendarNavBarProps.value)}
                            {renderCalender(true, secondaryCalendarProps.value, 'end')}
                            {selectingSecondaryMonth.value && renderMonth(true, secondMonthProps.value)}
                            {selectingSecondaryYear.value && renderYear(true, secondYearProps.value)}
                        </div>
                    }
                    {
                        showConfirmButton.value && <div class="f-datepicker-commitBtn-wrapper" style="justify-content: end;">
                            <FButton onClick={onConfirm}>
                                确定
                            </FButton>
                        </div>
                    }
                </div>
            );
        };
    }
});
