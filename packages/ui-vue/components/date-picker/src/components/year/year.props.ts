/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import { YearViewItem } from '../../types/year';

export const yearProps = {
    years: { Type: Array<Array<YearViewItem>>, default: [[]] },
    enableKeyboadNavigate: { Type: Boolean, default: true },
    enableMarkCurrent: { Type: Boolean, default: true },
    enablePeriod: { Type: Boolean, default: false },
    selected: { Type: Object, default: null },
    selectedPeriod: { Type: Object, default: null }
};

export type YearPropsType = ExtractPropTypes<typeof yearProps>;
