import { InputBaseProperty } from "../../../property-panel/src/composition/entity/input-base-property";
import { DATE_FORMATS } from "./date-format";

export class DatePickerProperty extends InputBaseProperty {

    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }
    getEditorProperties(propertyData: any) {
        const displayFormatOptions = this.getDateFormatOptions(propertyData?.editor);
        const valueFormatOptions = this.getValueFormatOptions();
        return this.getComponentConfig(propertyData, { type: "date-picker" }, {
            showTime: {
                description: "",
                title: "启用时间选择",
                type: "boolean",
                readonly: propertyData?.editor?.fieldType === 'Date',
                refreshPanelAfterChanged: true
            },
            displayFormat: {
                description: "",
                title: "展示格式",
                type: "enum",
                editor: {
                    data: displayFormatOptions
                }
            },
            valueFormat: {
                description: "",
                title: "存储格式",
                type: "enum",
                editor: {
                    data: valueFormatOptions
                },
                visible: propertyData?.editor?.fieldType === 'String'
            },
            maxYear: {
                description: "",
                title: "最大年限",
                type: "number",
                editor: {
                    nullable: true,
                    min: propertyData?.editor?.minYear ? propertyData?.editor?.minYear : 0,
                    useThousands:false
                },
                refreshPanelAfterChanged: true
            },
            minYear: {
                description: "",
                title: "最小年限",
                type: "number",
                editor: {
                    nullable: true,
                    min: 0,
                    max: propertyData?.editor?.maxYear,
                    useThousands:false
                },
                refreshPanelAfterChanged: true
            }
        }, this.setEditorPropertyRelates);
    }

    private setEditorPropertyRelates(changeObject: any, propertyData: any) {
        if (!changeObject || !propertyData.editor) {
            return;
        }
        switch (changeObject.propertyID) {
            case 'showTime': {
                const editorData = propertyData.editor;
                if (changeObject.propertyValue) {
                    editorData.displayFormat = 'yyyy-MM-dd HH:mm:ss';
                    editorData.valueFormat = 'yyyy-MM-dd HH:mm:ss';
                } else {
                    editorData.displayFormat = 'yyyy-MM-dd';
                    editorData.valueFormat = 'yyyy-MM-dd';
                }
            }
        }
    }
    private getDateFormatOptions(editorData: any) {
        let formats: any[] = [];
        const { timeFormats, yMdFormats, yMFormats, mdFormats, yFormats } = DATE_FORMATS();

        if (editorData.showTime) {
            formats = formats.concat(timeFormats);
        }
        formats = formats.concat(yMdFormats);
        formats = formats.concat(yMFormats);
        formats = formats.concat(yFormats);
        formats = formats.concat(mdFormats);

        return formats;
    }

    private getValueFormatOptions() {
        return [
            { id: 'yyyy-MM-dd', name: 'yyyy-MM-dd' },
            { id: 'yyyy/MM/dd', name: 'yyyy/MM/dd' },
            { id: 'MM/dd/yyyy', name: 'MM/dd/yyyy' },
            { id: 'yyyy-MM-dd HH:mm:ss', name: 'yyyy-MM-dd HH:mm:ss' },
            { id: 'yyyy/MM/dd HH:mm:ss', name: 'yyyy/MM/dd HH:mm:ss' },
            { id: 'yyyyMM', name: 'yyyyMM' },
            { id: 'yyyy', name: 'yyyy' }
        ];
    }
}
