export function DATE_FORMATS() {

    let languageCode = 'zh-CHS';
    if (window && window.top && window.top.localStorage && window.top.localStorage.languageCode) {
        ({ languageCode } = window.top.localStorage);
    }

    const timeFormats = [
        { id: 'yyyy-MM-dd HH:mm:ss', name: 'yyyy-MM-dd HH:mm:ss' },
        { id: 'yyyy/MM/dd HH:mm:ss', name: 'yyyy/MM/dd HH:mm:ss' },
        { id: 'yyyyMMddHHmmss', name: 'yyyyMMddHHmmss' },
        { id: 'yyyy-MM-dd HH:mm', name: 'yyyy-MM-dd HH:mm' },
        { id: 'yyyy/MM/dd HH:mm', name: 'yyyy/MM/dd HH:mm' },
        { id: 'yyyyMMddHHmm', name: 'yyyyMMddHHmm' }
    ];
    const yMdFormats = [
        { id: 'yyyy-MM-dd', name: 'yyyy-MM-dd' },
        { id: 'yyyy/MM/dd', name: 'yyyy/MM/dd' },
        { id: 'yyyyMMdd', name: 'yyyyMMdd' },
        { id: 'MM/dd/yyyy', name: 'MM/dd/yyyy' }
    ];
    const yMFormats = [
        { id: 'yyyy-MM', name: 'yyyy-MM' },
        { id: 'yyyy/MM', name: 'yyyy/MM' },
        { id: 'yyyyMM', name: 'yyyyMM' }
    ];

    const mdFormats = [
        { id: 'MM/dd', name: 'MM/dd' },
        { id: 'MMdd', name: 'MMdd' },
        { id: 'MM-dd', name: 'MM-dd' }
    ];

    const yFormats = [
        { id: 'yyyy', name: 'yyyy' }
    ];

    // 区分语言
    if (languageCode === 'zh-CHS') {
        timeFormats.push(
            { id: 'yyyy年MM月dd日 HH时mm分ss秒', name: 'yyyy年MM月dd日 HH时mm分ss秒' },
            { id: 'yyyy年MM月dd日 HH时mm分', name: 'yyyy年MM月dd日 HH时mm分' }
        );
        yMdFormats.push(
            { id: 'yyyy年MM月dd日', name: 'yyyy年MM月dd日' }
        );
        yMFormats.push(
            { id: 'yyyy年MM月', name: 'yyyy年MM月' }
        );
        mdFormats.push(
            { id: 'MM月dd日', name: 'MM月dd日' }
        );
        yFormats.push(
            { id: 'yyyy年', name: 'yyyy年' }
        );
    }
    if (languageCode === 'zh-CHT') {
        timeFormats.push(
            { id: 'yyyy年MM月dd日 HH時mm分ss秒', name: 'yyyy年MM月dd日 HH時mm分ss秒' },
            { id: 'yyyy年MM月dd日 HH時mm分', name: 'yyyy年MM月dd日 HH時mm分' }
        );
        yMdFormats.push(
            { id: 'yyyy年MM月dd日', name: 'yyyy年MM月dd日' }
        );
        yMFormats.push(
            { id: 'yyyy年MM月', name: 'yyyy年MM月' }
        );
        mdFormats.push(
            { id: 'MM月dd日', name: 'MM月dd日' }
        );
        yFormats.push(
            { id: 'yyyy年', name: 'yyyy年' }
        );
    }


    return { timeFormats, yMFormats, yMdFormats, mdFormats, yFormats };

}
