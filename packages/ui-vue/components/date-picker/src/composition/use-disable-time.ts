/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DateObject } from '../types/common';
import { useCompare } from './use-compare';

export function useDisableTime() {

    const { equal } = useCompare();

    // 禁用时间对应的小时
    // 选中日期等于左区间日期时，禁用左端点的左部分[0，minHour）
    // 选中日期等于右区间日期时，禁用右端点的右部分(maxHour,23]
    function disabledHours(min: DateObject, max: DateObject, currentDate: DateObject) {
        let minHour = min.hour || 0;
        let maxHour = max.hour || 0;
        const res: number[] = [];
        if (equal(
            { year: currentDate.year, month: currentDate.month, day: currentDate.day },
            { year: min.year, month: min.month, day: min.day }
        )) {
            while (minHour-- >= 0) {
                res.push(minHour);
            }
        } else if (equal(
            { year: currentDate.year, month: currentDate.month, day: currentDate.day },
            { year: max.year, month: max.month, day: max.day }
        )) {
            while (maxHour++ < 24) {
                res.push(maxHour);
            }
        }
        return res;
    }
    // 禁用时间对应的分钟
    // 选中日期等于左区间日期时，禁用左端点的左部分[0，minMinute）
    // 选中日期等于右区间日期时，禁用右端点的右部分（maxMinute，59]
    function disabledMinutes(min: DateObject, max: DateObject, _hour: number, currentDate: DateObject) {
        const maxHour = max.hour;
        let maxMinute = max.minute || 0;
        const minHour = min.hour;
        let minMinute = min.minute || 0;
        const res: number[] = [];
        if (equal(
            { year: currentDate.year, month: currentDate.month, day: currentDate.day },
            { year: min.year, month: min.month, day: min.day }
        )) {
            if (_hour === minHour) {
                while (minMinute-- >= 0) {
                    res.push(minMinute);
                }
            }
        } else if (equal(
            { year: currentDate.year, month: currentDate.month, day: currentDate.day },
            { year: max.year, month: max.month, day: max.day }
        )) {
            if (_hour === maxHour) {
                while (maxMinute++ < 60) {
                    res.push(maxMinute);
                }
            }
        }
        return res;

    }
    // 禁用时间对应的秒
    // 选中日期等于左区间日期时，禁用左端点的左部分[0，minSecond）
    // 选中日期等于右区间日期时，禁用右端点的右部分（maxSecond，59]
    function disabledSeconds(min: DateObject, max: DateObject, _hour: number, _minute: number, currentDate: DateObject) {
        const maxHour = max.hour;
        const maxMinute = max.minute || 0;
        let maxSecond = max.second || 0;
        const minHour = min.hour || 0;
        const minMinute = min.minute || 0;
        let minSecond = min.second || 0;
        const res: number[] = [];
        if (equal(
            { year: currentDate.year, month: currentDate.month, day: currentDate.day },
            { year: min.year, month: min.month, day: min.day }
        )) {
            if (_hour === minHour && _minute === minMinute) {
                while (minSecond-- >= 0) {
                    res.push(minSecond);
                }
            }

        } else if (equal(
            { year: currentDate.year, month: currentDate.month, day: currentDate.day },
            { year: max.year, month: max.month, day: max.day }
        )) {
            if (_hour === maxHour && _minute === maxMinute) {
                while (maxSecond++ < 60) {
                    res.push(maxSecond);
                }
            }
        }
        return res;
    }
}
