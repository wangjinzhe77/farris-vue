/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';

export const loadingProps = {
    /** 是否展示文案 */
    showMessage: { type: Boolean, default: true },
    /** 展示信息 */
    message: { type: String, default: '加载中，请稍后...' },
    /** 默认展示状态,方便通过按钮点击控制loading */
    isActive: { type: Boolean, default: false },
    /** 图标大小 */
    width: { type: Number, default: 30 },
    /** 图标样式 */
    type: { type: Number, default: 0 },
    /** 指定父元素 */
    target: { type: Object as PropType<Element>, default: null },
    // 父元素比如body可能有默认的position是absolute，所以默认属性不设值
    targetPosition: { type: String, default: '' },
    /** 延迟300毫秒,解决异步加载出现一闪而逝的问题*/
    delay: { type: Number, default: 300 }
};
export type LoadingProps = Partial<ExtractPropTypes<typeof loadingProps>>;
