/* eslint-disable no-use-before-define */
import { Ref, createApp, onMounted, onUnmounted, ref } from 'vue';
import FLoading from './loading.component';
import { LoadingProps } from './loading.props';

let currentLoadingInstanceID = -1;
const loadingInstances: { [key: number]: Ref<any> } = {};

function initInstance(props?: any): Ref<InstanceType<typeof FLoading>> {
    const container = document.createElement('div');
    /** 加载范围 */
    const parentContainer = props.target || document.body;
    const canChangePosition = !!props.targetPosition;
    let positionOldValue = 'unset';
    if (canChangePosition) {
        const { position } = window.getComputedStyle(parentContainer);
        positionOldValue = position;
        parentContainer.style.position = props.targetPosition;
    }
    const newInstanceId = createInstanceId();
    currentLoadingInstanceID = newInstanceId;
    const appInstance = createApp({
        setup() {
            const loadingInstance = ref();
            onUnmounted(() => {
                if (canChangePosition) {
                    parentContainer.style.position = positionOldValue;
                }
                container.remove();
            });
            onMounted(() => {
                loadingInstance.value && loadingInstance.value.setLoadingId(newInstanceId);
            });
            function closedHandler(event) {
                appInstance.unmount();
                // 删除对应的存储对象
                event.callback && event.callback();
                delete loadingInstances[event.loadingId];

            }
            loadingInstances[newInstanceId] = loadingInstance;
            return () => (
                <FLoading ref={loadingInstance} {...props} onClosed={closedHandler}></FLoading>
            );
        }
    });

    // loading覆盖全部区域或部分区域
    parentContainer.style = parentContainer.style ? parentContainer.style : {};

    parentContainer.appendChild(container);
    appInstance.mount(container);
    return loadingInstances[newInstanceId];
}

/**
 * 获取Loading的InstanceId
 * @returns
 */
function getMaxLoadingID() {
    const ids = Object.keys(loadingInstances).map(k => parseInt(k, 10));
    if (ids.length) {
        return Math.max(...ids);
    }
    return 0;
}

/**
 * 创建Loading的InstanceId
 * @returns
 */
function createInstanceId() {
    return getMaxLoadingID() + 1;
}

export default class LoadingService {
    static show(config?: LoadingProps): Ref<InstanceType<typeof FLoading>> {
        const newConfig = { isActive: true, target: document.body, targetPosition: '', ...config };
        return initInstance(newConfig);
    }

    static close(loadingId: number, callback = () => { }): void {
        const id = loadingId || currentLoadingInstanceID;
        const loadingInstance = loadingInstances[id];
        if (loadingInstance) {
            loadingInstance.value.close(callback);
        }
    }

    static clearAll() {
        const keys = Object.keys(loadingInstances);
        if (keys.length) {
            keys.forEach(id => {
                const loadingInstance = loadingInstances[id];
                if (loadingInstance) {
                    loadingInstance.value.close();
                }
            });
        }
    }
}
