 
/* eslint-disable no-use-before-define */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext, ref, onMounted, watch, computed } from 'vue';
import { LoadingProps, loadingProps } from './loading.props';
import { LOADING_STYLES } from './composition/types';

export default defineComponent({
    name: 'FLoading',
    props: loadingProps,
    emits: ['closed', 'hidden'] as (string[] & ThisType<void>) | undefined,
    setup(props: LoadingProps, context: SetupContext) {
        /** 加载样式 */
        const loadingStyleDom = LOADING_STYLES;
        /** 是否展示加载信息 */
        const showMessage = ref(props.showMessage);
        /** 是否默认激活 */
        const isActive = ref(props.isActive);
        /** 加载信息文字 */
        const message = ref(props.message);
        /** 图标大小 */
        const width = ref(props.width);
        /** 图标样式 */
        const type = ref(props.type);
        /** loading背景 */
        const loadingBackdrop = ref<HTMLDivElement | null>(null);
        /** loading容器 */
        const loadingContainer = ref<HTMLDivElement | null>(null);

        // watch(()=>props.message,(newMessage)=>{
        //     message.value = newMessage;
        // });
        // watch(()=>props.type,(newType)=>{
        //     type.value = newType;
        // });

        let loadingId = "";

        let timerId;
        /** loading的3种样式 */
        const selectedType = computed(() => {
            const result = type.value ? type.value in [0, 1, 2] ? type.value : 0 : 0;
            return result;
        });

        onMounted(() => {
            setLoadingState();
        });
        const loadingStyle = computed(() => {
            // 是否是沾满全屏
            return 'transform:translate(-50%,-50%);' + (props.target === document.body ? 'position:fixed' : '');
        });
        /** 加载状态 */
        function setLoadingState() {
            if (isActive.value && !timerId) {
                timerId = setTimeout(() => {
                    timerId = null;
                    setPosition();
                }, props.delay);
            }
            if (!isActive.value) {
                if (timerId) {
                    clearTimeout(timerId);
                    timerId = null;
                }
            }
        }
        function setLoadingId(id) {
            loadingId = id;
        }
        function getLoadingId() {
            return loadingId;
        }
        /**
         * 关闭当前loading
         * 不通过隐藏方式
         *
        */
        function close(callback = () => { }) {
            isActive.value = false;
            context.emit('closed', { loadingId, callback });
        }

        /** loading位置 */
        function setPosition() {
            const zindex = getFloatingLayerIndex();
            if (loadingBackdrop.value && loadingBackdrop.value.style) {
                loadingBackdrop.value.style.zIndex = (zindex + 1).toString();
            }
            if (loadingContainer.value && loadingContainer.value.style) {
                loadingContainer.value.style.zIndex = (zindex + 1).toString();
            }
        }

        /**
         *  获取页面中body下所有元素的zIndex, 并返回下个浮层的新zindex
         *  此处获取最高层级的Index后续需要更改 TODO
         */
        function getFloatingLayerIndex(upperLayers = 1) {
            const selectors = [
                'body>.f-datagrid-settings-simple-host',
                'body>div',
                'body>farris-dialog>.farris-modal.show',
                'body>.farris-modal.show',
                'body>farris-filter-panel>.f-filter-panel-wrapper',
                'body .f-sidebar-show>.f-sidebar-main',
                'body>.popover.show',
                'body>filter-row-panel>.f-datagrid-filter-panel',
                'body>.f-section-maximize'
            ];

            const overlays = Array.from(document.body.querySelectorAll(selectors.join(','))).filter(n => n).map(n => {
                const { display, zIndex } = window.getComputedStyle(n);
                if (display === 'none') {
                    return 0;
                }
                return parseInt(zIndex, 10);
            }).filter(n => n);
            let maxZindex = Math.max(...overlays);
            if (maxZindex < 1040) {
                maxZindex = 1040;
            }
            return maxZindex + upperLayers;
        }
        context.expose({ close, setLoadingId, getLoadingId });
        watch(
            () => props.isActive, (newValue) => {
                isActive.value = newValue;
                setLoadingState();
                if (!newValue) {
                    close();
                }
            }
        );
        return () => {
            return (
                <>{isActive.value && <div ref={loadingBackdrop} class='farris-loading-backdrop loading-wait'></div>}
                    {
                        isActive.value && <div ref={loadingContainer} class="farris-loading" style={loadingStyle.value}>
                            <div class="ng-busy-default-wrapper">
                                <div class="ng-busy-default-sign">
                                    <div style={[
                                        { display: 'inline-block' },
                                        { margin: '4px' },
                                        { width: width.value + 'px' },
                                        { height: width.value + 'px' }
                                    ]}
                                        innerHTML={loadingStyleDom[selectedType.value]}>
                                    </div>
                                    {showMessage.value && (
                                        <div class="ng-busy-default-text" style="margin-left:0;" innerHTML={message.value}></div>
                                    )}
                                </div>
                            </div>
                        </div>
                    }</>
            );
        };
    }
});
