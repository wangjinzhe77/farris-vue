/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, ref, watch } from 'vue';
import { ListContainerProps, listContainerProps } from './list-container.props';
import FListView from '@farris/ui-vue/components/list-view';

export default defineComponent({
    name: 'FComboListContainer',
    props: listContainerProps,
    emits: ['selectionChange'],
    setup(props: ListContainerProps, context) {
        const listViewRef = ref();
        const dataSource = ref(props.dataSource);
        const selections = ref<any[]>([]);
        const separator = ref(props.separator);
        const width = ref(props.width);
        const maxHeight = ref(props.maxHeight);
        const selectionValues = ref<string[]>(String(props.selectedValues).split(separator.value));
        const showCheckbox = computed(() => props.multiSelect);
        // listview selection
        const selection = computed(() => {
            return {
                enableSelectRow: true,
                multiSelect: props.multiSelect,
                multiSelectMode: 'OnCheckAndClick',
                showCheckbox: showCheckbox.value,
                showSelectAll: false,
                showSelection: true
            };
        });
        watch(props.dataSource, () => {
            dataSource.value = props.dataSource;
        });

        const listViewHeader = computed(() => {
            return props.enableSearch ? 'SearchBar' : 'ContentHeader';
        });

        const listContainerStyle = computed(() => {
            const styleObject = {} as Record<string, any>;
            if (width.value !== undefined) {
                styleObject.width = `${width.value}px`;
            }
            // 当height设置了值之后，优先识别
            if (maxHeight.value !== undefined && maxHeight.value > 0) {
                styleObject.maxHeight = `${maxHeight.value}px`;
            }
            return styleObject;
        });

        function onSelectionChange(seletedItems: any[]) {
            selections.value = seletedItems.map((item: any) => Object.assign({}, item));
            selectionValues.value = seletedItems.map((item: any) => item[props.idField]);
            context.emit('selectionChange', selections.value);
        }

        function onAfterSearch(text: string) {
            // 如果启用高亮查询，不启用筛选
            if (props.enableHighlightSearch) {
                return;
            }
            let dataResult: any[] = [];
            const { searchOption } = props;
            if (typeof searchOption === 'function') {
                dataResult = dataSource.value.filter(dataItem => searchOption(text, dataItem));
            } else {
                dataResult = dataSource.value.filter(dataItem => dataItem[props.valueField].indexOf(text) > -1 ||
                    dataItem[props.textField].indexOf(text) > -1);
            }

            listViewRef.value.updateDataSource(dataResult);
        }

        watch(
            [() => props.selectedValues],
            ([newSelectedValues]) => {
                selectionValues.value = newSelectedValues.split(separator.value);
            });

        return () => {
            return (
                <div class="f-combo-list-container"
                    style={listContainerStyle.value}>
                    <FListView
                        ref={listViewRef}
                        size="small"
                        itemClass="f-combo-list-item"
                        header={listViewHeader.value}
                        headerClass="f-combo-list-search-box"
                        data={dataSource.value}
                        idField={props.idField}
                        textField={props.textField}
                        titleField={props.titleField}
                        multiSelect={props.multiSelect}
                        selection={selection.value}
                        enableHighlightSearch={props.enableHighlightSearch}
                        selectionValues={selectionValues.value}
                        onSelectionChange={onSelectionChange}
                        onAfterSearch={onAfterSearch}
                    >
                    </FListView>
                </div>
            );
        };
    }
});
