import { ExtractPropTypes, PropType } from "vue";
import { Option } from '../combo-list.props';

export const listContainerProps = {

    dataSource: { type: Array<Option>, default: [] },

    enableSearch: { type: Boolean, default: false },

    idField: { type: String, default: 'id' },

    multiSelect: { type: Boolean, default: false },

    selectedValues: { type: String, default: '' },

    separator: { type: String, default: ',' },

    textField: { type: String, default: 'name' },

    titleField: { type: String, default: 'name' },

    width: { type: Number },

    maxHeight: { type: Number },

    valueField: { type: String, default: 'id' },
    /** 值变化事件 */
    onSelectionChange: { type: Function, default: () => { } },
    searchOption: {
        type: [Boolean, Function] as PropType<boolean | ((searchText: string, option: any) => boolean)>,
        default: false
    },
    // 搜索启用高亮
    enableHighlightSearch: { type: Boolean, default: true },
};
export type ListContainerProps = ExtractPropTypes<typeof listContainerProps>;
