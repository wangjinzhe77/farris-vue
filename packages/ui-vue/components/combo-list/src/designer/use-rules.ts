 
import { UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema, DesignerComponentInstance, DesignerItemContext } from "../../../designer-canvas/src/types";
import { ComboListProperty } from "../property-config/combo-list.property-config";

export function useComboListDesignerRules(designItemContext: DesignerItemContext, designerHostService): UseDesignerRules {

    const schema = designItemContext.schema as ComponentSchema;

    // 构造属性配置方法
    function getPropsConfig(componentId: string, componentInstance: DesignerComponentInstance) {
        const inputGroupProps = new ComboListProperty(componentId, designerHostService);
        return inputGroupProps.getPropertyConfig(schema, componentInstance);
    }

    return {
        getPropsConfig
    } as UseDesignerRules;

}
