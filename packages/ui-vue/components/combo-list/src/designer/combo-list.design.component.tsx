/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, inject, onMounted, ref, SetupContext } from 'vue';
import { comboListDesignProps, ComboListDesignProps } from '../combo-list.props';
import FButtonEditDesign from '../../../button-edit/src/designer/button-edit.design.component';
import { useComboListDesignerRules } from './use-rules';
import { DesignerItemContext, useDesignerComponent } from '@farris/ui-vue/components/designer-canvas';

export default defineComponent({
    name: 'FComboListDesign',
    props: comboListDesignProps,
    emits: ['clear', 'update:modelValue', 'change'] as (string[] & ThisType<void>) | undefined,
    setup(props: ComboListDesignProps, context: SetupContext) {
        const elementRef = ref();
        const designerHostService = inject('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useComboListDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        return () => {
            return (
                <FButtonEditDesign
                    ref={elementRef}
                    buttonContent={props.dropDownIcon}
                    readonly={true}
                    editable={false}
                    forcePlaceholder={true}
                    placeholder={props.placeholder}
                    enableClear></FButtonEditDesign>
            );
        };
    }
});
