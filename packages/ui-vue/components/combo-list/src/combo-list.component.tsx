/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, Ref, ref, SetupContext, watch } from 'vue';
import { comboListProps, ComboListProps, Option } from './combo-list.props';
import ComboListContainer from './components/list-container.component';
import FButtonEdit from '@farris/ui-vue/components/button-edit';
import { useDataSource } from './composition/use-data-source';

export default defineComponent({
    name: 'FComboList',
    props: comboListProps,
    emits: ['clear', 'update:modelValue', 'change', 'input'] as (string[] & ThisType<void>) | undefined,
    setup(props: ComboListProps, context: SetupContext) {
        const comboEditorRef: Ref<any> = ref();
        const disable = ref(props.disabled);
        const enableClear = ref(props.enableClear);
        const enableSearch = ref(props.enableSearch);
        const readonly = ref(props.readonly);
        const { dataSource, displayText, editable, modelValue, getSelectedItemsByDisplayText } = useDataSource(props);

        const isMultiSelect = computed(() => props.multiSelect);

        const comboEditorWidth = computed(() => {
            return comboEditorRef.value ? (comboEditorRef.value.elementRef as HTMLElement).getBoundingClientRect().width : 0;
        });

        function tryHidePopupOnSelect() {
            const shouldHidePopupOnSelect = !isMultiSelect.value;
            if (shouldHidePopupOnSelect && comboEditorRef.value) {
                comboEditorRef.value.hidePopup();
            }
        }

        function onSelectionChange(selectedItems: Option[]) {
            displayText.value = selectedItems.map((item: Option) => item[props.textField]).join(props.separator);
            let value = '';
            if (selectedItems.length === 1) {
                value = selectedItems[0][props.valueField];
            } else {
                value = selectedItems.map((item: Option) => item[props.valueField]).join(props.separator);
            }
            if (modelValue.value !== value) {
                // 数据变化后  触发双向绑定和change事件
                modelValue.value = value;
                context.emit('update:modelValue', modelValue.value);
                context.emit('change', selectedItems, modelValue.value);
            }
            tryHidePopupOnSelect();
        }
        /**
         * 当点击输入框时触发
         */
        function onClickInput() {
            if (!readonly.value) {
                comboEditorRef.value.togglePopup();
            }
        }

        function onClear($event: Event) {
            modelValue.value = '';
            context.emit('update:modelValue', '');
            context.emit('clear');
        }

        function onDisplayTextChange(displayText: string) {
            const selectedItems = getSelectedItemsByDisplayText(displayText);
            onSelectionChange(selectedItems);
        }

        function getDisplayText() {
            return displayText.value;
        }
        /**
         * 当可编辑时editable为true时，才会被调用
         * @param eventData 
         */
        function onInput(eventData: string) {
            context.emit('input', eventData);
        }

        function hidePopup() {
            comboEditorRef.value.hidePopup();
        }

        context.expose({
            getDisplayText,
            hidePopup
        });

        watch(
            [() => props.disabled, () => props.editable, () => props.enableClear, () => props.enableSearch, () => props.readonly],
            ([newDisabled, newEditable, newEnableClear, newEnableSearch, newReadonly]) => {
                disable.value = newDisabled;
                editable.value = newEditable;
                enableClear.value = newEnableClear;
                enableSearch.value = newEnableSearch;
                readonly.value = newReadonly;
            }
        );

        return () => {
            return (
                <FButtonEdit
                    ref={comboEditorRef}
                    id={props.id}
                    disable={disable.value}
                    readonly={readonly.value}
                    forcePlaceholder={props.forcePlaceholder}
                    editable={editable.value}
                    buttonContent={props.dropDownIcon}
                    placeholder={props.placeholder}
                    enableClear={enableClear.value}
                    maxLength={props.maxLength}
                    tabIndex={props.tabIndex}
                    enableTitle={props.enableTitle}
                    multiSelect={props.multiSelect}
                    inputType={props.multiSelect ? props.viewType : 'text'}
                    v-model={displayText.value}
                    focusOnCreated={props.focusOnCreated}
                    selectOnCreated={props.selectOnCreated}
                    onClear={onClear}
                    onClick={onClickInput}
                    onChange={onDisplayTextChange}
                    onInput={onInput}
                    beforeOpen={props.beforeOpen}
                    placement={'auto'}
                >
                    <ComboListContainer
                        idField={props.idField}
                        valueField={props.valueField}
                        textField={props.textField}
                        titleField={props.titleField}
                        dataSource={dataSource.value}
                        selectedValues={modelValue.value}
                        multiSelect={props.multiSelect}
                        enableSearch={enableSearch.value}
                        maxHeight={props.maxHeight}
                        enableHighlightSearch={props.enableHighlightSearch}
                        width={props.fitEditor ? comboEditorWidth.value : undefined}
                        onSelectionChange={onSelectionChange}></ComboListContainer>
                </FButtonEdit>
            );
        };
    }
});
