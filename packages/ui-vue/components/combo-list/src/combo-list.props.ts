
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import comboListSchema from './schema/combo-list.schema.json';
import { schemaResolver } from './schema/schema-resolver';

export interface Option {
    disabled?: boolean;
    [prop: string]: any;
}
/**
 * remote info
 */
export interface Remote {
    url: string;
    method?: 'GET' | 'POST' | 'PUT';
    headers?: any;
    body?: any;
}

/**
 * 数据展现方式
 */
export enum ViewType {
    Text = 'text',
    Tag = 'tag'
}
/**
 * 展示位置
 */
export const enum Placement {
    /**
     * 在控件的上方展示
     */
    top = 'top',
    /**
     * 在控件底部展示
     */
    bottom = 'bottom',
    /**
     * 根据控件的位置自动确认展示位置
     */
    auto = 'auto'
}
export type BeforeOpenFunction = (params: any) => boolean | Promise<boolean>;
/**
 * 下拉列表属性
 */
export const comboListProps = {
    /**
     * 组件标识
     */
    id: { type: String },
    /**
     * 下拉数据源
     */
    data: { type: Array<Option>, default: [] },
    /**
     * 可选，展示文本
     * 默认为空字符串 -----内部需要根据value重新生成展示文本，此属性不生效
     * displayText: { type: String, default: '' },
    */
    /**
     * 可选，是否禁用
     * 默认为`false`
     */
    disabled: { default: false, type: Boolean },
    /**
     * 可选，下拉图标
     * 默认为'<span class="f-icon f-icon-arrow-60-down"></span>'
     */
    dropDownIcon: { type: String, default: '<span class="f-icon f-icon-arrow-60-down"></span>' },
    /**
     * 可选，是否可编辑
     * 默认`false`
     */
    editable: { default: false, type: Boolean },
    /**
     * 可选，是否启用清空
     * 默认启用
     */
    enableClear: { default: true, type: Boolean },
    /**
     * 可选，启用搜索
     * 默认为`false`
     */
    enableSearch: { type: Boolean, default: false },
    /**
     * 可选，鼠标悬停时是否显示控件值
     * 默认显示
     */
    enableTitle: { default: true, type: Boolean },

    fitEditor: { default: false, type: Boolean },
    /**
     * 可选，强制显示占位符
     * 默认`false`
     */
    forcePlaceholder: { default: false, type: Boolean },
    /**
     * 可选，清空值时隐藏面板
     * 默认`true`
     */
    hidePanelOnClear: { default: true, type: Boolean },
    /**
     * 可选，数据源id字段
     * 默认为`id`
     */
    idField: { default: 'id', type: String },
    /**
     * 可选，字段映射
     */
    mapFields: { type: Object },
    /**
     * 可选，最大高度
     * 默认`350`
     */
    maxHeight: { default: 350, type: Number },
    /**
     * 最大输入长度
     */
    maxLength: { type: Number },
    /**
     * 可选，是否支持多选
     * 默认`false`
     */
    multiSelect: { type: Boolean, default: false },
    /**
     * 绑定值
     */
    modelValue: {},
    /**
     * 占位符
     */
    placeholder: { type: String, default: '请选择' },
    /**
     * 可选，下拉面板展示位置
     * 默认为`auto`
     */
    placement: { type: String as PropType<Placement>, default: Placement.auto },
    /**
     * 可选，是否只读
     * 默认为`false`
     */
    readonly: { default: false, type: Boolean },
    /**
     * 远端数据源信息
     */
    remote: { default: null, type: Object as PropType<Remote> },
    /**
     * 可选，是否支持远端过滤
     * 默认`false`
     */
    remoteSearch: { default: false, type: Boolean },
    /**
     * 可选，分隔符
     * 默认`,`
     */
    separator: { default: ',', type: String },
    /**
     * tabIndex
     */
    tabIndex: { type: Number, default: -1 },
    /**
     * 可选，数据源显示字段
     * 默认为`name`
     */
    textField: { default: 'name', type: String },
    /**
     * 可选，数据源的title
     * 默认为`name`
     */
    titleField: { default: 'name', type: String },
    /**
     * 可选，数据源值字段
     * 默认为`id`
     */
    valueField: { default: 'id', type: String },
    /**
     * 可选，启用多选下，下拉列表值在输入框中的展示方式
     * 支持text | tag
     * 因为ButtonEdit内部处理类型只有在批量的情况下才会有展示类型区分
     */
    viewType: { default: 'tag', type: String as PropType<ViewType> },
    /**
     * 值变化事件
     */
    change: { type: Function, default: () => { } },
    /**
     * 作为内嵌编辑器被创建后默认获得焦点
     */
    focusOnCreated: { type: Boolean, default: false },
    /**
     * 作为内嵌编辑器被创建后默认选中文本
     */
    selectOnCreated: { type: Boolean, default: false },
    /**
     * 此属性废弃
     */
    autoHeight: { type: Boolean, default: true },
    /**
     * 打开前
     */
    beforeOpen: { type: Function as PropType<BeforeOpenFunction>, default: null },
    searchOption: {
        type: [Boolean, Function] as PropType<boolean | ((searchText: string, option: any) => boolean)>,
        default: false
    },
    // 搜索启用高亮
    enableHighlightSearch: { type: Boolean, default: true }
} as Record<string, any>;

export type ComboListProps = ExtractPropTypes<typeof comboListProps>;

export const comboListDesignProps = Object.assign({}, comboListProps, {
    readonly: {}
});

export type ComboListDesignProps = ExtractPropTypes<typeof comboListDesignProps>;

export const propsResolver = createPropsResolver<ComboListProps>(comboListProps, comboListSchema, schemaMapper, schemaResolver);
