import { InputBaseProperty } from "../../../property-panel/src/composition/entity/input-base-property";
export class ComboListProperty extends InputBaseProperty {
    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }
    getEditorProperties(propertyData) {
        const self=this;
        const editorProperties =  self.getComponentConfig(propertyData, { type: "combo-list" }, {
            editable: {
                description: "",
                title: "允许编辑",
                type: "boolean"
            },
            enableClear: {
                description: "",
                title: "启用清空",
                type: "boolean"
            },
            textField: {
                visible:false,
                description: "",
                title: "数据源显示字段",
                type: "string"
            },            
            valueField: {
                visible:false,
                description: "",
                title: "数据源值字段",
                type: "string"
            },
            idField: {
                visible:false,
                description: "",
                title: "数据源标识字段",
                type: "string"
            },
            data: {
                description: "",
                title: "数据",
                type: "array",                
                $converter:"/converter/enum-data.converter",
                ...self.getItemCollectionEditor(propertyData,propertyData.editor.valueField,propertyData.editor.textField),
                // 这个属性，标记当属性变更得时候触发重新更新属性
                refreshPanelAfterChanged: true,
            }
        });
        editorProperties['setPropertyRelates'] = function (changeObject) {
            if (!changeObject) {
                return;
            }
            switch (changeObject.propertyID) {
                case 'data': {
                    if (changeObject.propertyValue.parameters) {
                        propertyData.editor.valueField = changeObject.propertyValue.parameters.valueField;
                        propertyData.editor.textField = changeObject.propertyValue.parameters.nameField;
                    } 
                    // 如果可以编辑数据，要同步更新格式化
                    if(!self.checkEnumDataReadonly(propertyData)&&propertyData.formatter){
                        propertyData.formatter.data=[...changeObject.propertyValue.value]
                    }
                    break;
                }
            }
        };
        return editorProperties;
    }
}
