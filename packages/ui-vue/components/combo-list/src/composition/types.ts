import { Ref } from "vue";
import { Option } from '../combo-list.props';

export interface UseDataSource {

    dataSource: Ref<Option[]>;

    displayText: Ref<string>;

    editable: Ref<boolean>;

    modelValue: Ref<string>;

    getItemsByDisplayText: (text: string) => Option[];

    getItemsByValue: (value: string) => Option[];

    getSelectedItemsByDisplayText: (text: string) => Option[];

}
