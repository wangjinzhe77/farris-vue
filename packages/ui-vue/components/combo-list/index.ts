 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import FComboList from './src/combo-list.component';
import FComboListDesign from './src/designer/combo-list.design.component';
import { propsResolver } from './src/combo-list.props';
import { withInstall } from '@farris/ui-vue/components/common';

export * from './src/combo-list.props';

FComboList.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap['combo-list'] = FComboList;
    propsResolverMap['combo-list'] = propsResolver;
};
FComboList.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['combo-list'] = FComboListDesign;
    propsResolverMap['combo-list'] = propsResolver;
};
export { FComboList };
export default withInstall(FComboList);
