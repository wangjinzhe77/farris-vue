 
import FCapsule from './src/capsule.component';
import { propsResolver } from './src/capsule.props';
import CapsuleDesign from './src/designer/capsule.design.component';
import { withInstall } from '@farris/ui-vue/components/common';

export * from './src/capsule.props';

FCapsule.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.capsule = FCapsule;
    propsResolverMap.capsule = propsResolver;
};
FCapsule.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.capsule = CapsuleDesign;
    propsResolverMap.capsule = propsResolver;
};

export { FCapsule };
export default withInstall(FCapsule);
