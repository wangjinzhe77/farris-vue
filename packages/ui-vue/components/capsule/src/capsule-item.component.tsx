import { SetupContext, computed, defineComponent, onMounted, ref, watch } from "vue";
import { CapsuleItemProps, capsuleItemProps } from "./capsule.props";

export default defineComponent({
    name: 'FCapsuleItem',
    props: capsuleItemProps,
    emits: ['mounted', 'active'],
    setup(props: CapsuleItemProps, context) {
        const capsuleItemRef = ref<any>();

        const isActive = ref(props.isActive);

        watch(() => props.isActive, (newValue: boolean) => {
            isActive.value = newValue;
        });

        const capsuleItemClass = computed(() => {
            const classObject = {
                'f-capsule-item': true,
                'f-capsule-active-item': isActive.value,
                'f-capsule-disabled': props.disabled
            } as Record<string, boolean>;
            return classObject;
        });

        onMounted(() => {
            context.emit('mounted', capsuleItemRef, props.value);
        });

        function onClickCapsuleItem($event: MouseEvent) {
            if (props.disabled) {
                return;
            }
            context.emit('active', $event, { name: props.name, value: props.value });
        }

        return () => {
            return (
                <div ref={capsuleItemRef} class={capsuleItemClass.value}
                    onClick={(paylaod: MouseEvent) => onClickCapsuleItem(paylaod)}>
                    {props.icon && <i class={props.icon}></i>}
                    {props.name}
                </div>
            );
        };
    }
});
