import { ComputedRef, ExtractPropTypes, PropType } from 'vue';
import capsuleSchema from "./schema/capsule.schema.json";
import { createPropsResolver } from '../../dynamic-resolver/src/props-resolver';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import propertyConfig from './property-config/capsule.property-config.json';

export type CapsuleColerType = 'primary' | 'secondary';

export interface CapsuleItem {
    [key: string]: any;
    /**
     * 枚举值
     */
    value: any;
    /**
     * 枚举展示文本
     */
    name: string;
    /**
     * 是否展示
     */
    show?: boolean;
     /**
     * 是否禁用
     */
    disabled?: boolean;
}

export const capsuleProps = {
    items: {
        Type: Array<CapsuleItem>, default: [{ name: '升序', value: 'asc', icon: 'f-icon f-icon-col-ascendingorder' },
            { name: '降序', value: 'desc', icon: 'f-icon f-icon-col-descendingorder' }]
    },
    /**
     * 组件值
     */
    modelValue: { type: String, default: '' },

    type: { type: String as PropType<CapsuleColerType>, default: 'primary' }
} as Record<string, any>;

export const capsuleItemProps = {
    name: { type: String, default: '' },
    value: { type: String, default: '' },
    isActive: { type: Boolean, default: false },
    icon: { type: String, default: '' },
    index: { type: Number, default: 0 },
    show: { type: Boolean, default: true },
    disabled: { type: Boolean, default: false }
};

export type CapsuleProps = ExtractPropTypes<typeof capsuleProps>;

export type CapsuleItemProps = ExtractPropTypes<typeof capsuleItemProps>;

export const propsResolver = createPropsResolver<CapsuleProps>(capsuleProps, capsuleSchema, schemaMapper, schemaResolver, propertyConfig);
