 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import FButtonEdit from './src/button-edit.component';
import { propsResolver } from './src/button-edit.props';
import FButtonEditDesign from './src/designer/button-edit.design.component';
import { withInstall } from '@farris/ui-vue/components/common';

export * from './src/button-edit.props';

FButtonEdit.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap['button-edit'] = FButtonEdit;
    propsResolverMap['button-edit'] = propsResolver;
};
FButtonEdit.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['button-edit'] = FButtonEditDesign;
    propsResolverMap['button-edit'] = propsResolver;
};

export { FButtonEdit };
export default withInstall(FButtonEdit);

// export default {
//     install(app: App): void {
//         app.component(FButtonEdit.name as string, FButtonEdit);
//     },
//     register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
//         componentMap['button-edit'] = FButtonEdit;
//         propsResolverMap['button-edit'] = propsResolver;
//     },
//     registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
//         componentMap['button-edit'] = FButtonEditDesign;
//         propsResolverMap['button-edit'] = propsResolver;
//     }
// };
