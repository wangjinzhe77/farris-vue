 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { mount } from '@vue/test-utils';
import { nextTick, ref } from 'vue';
import { FButtonEdit } from '..';

describe('f-button-edit', () => {
    const mocks = {};

    beforeAll(() => { });

    describe('properties', () => {
        test('it should has default button content', () => {
            const component = mount(() => <FButtonEdit id="test-button"></FButtonEdit>);
            expect(component.find('#test-button').find('#test-button-input-group').find('div')
                .find('.input-group-append-button').element.innerHTML)
                .toBe('<i class="f-icon f-icon-lookup"></i>');
        });
        test('it should has auto complete', () => {
            const component = mount(() => <FButtonEdit id="test-button" autoComplete={true}></FButtonEdit>);
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('input').attributes().autocomplete).toBe('on');
        });
        test('it should not has auto complete', () => {
            const component = mount(() => <FButtonEdit id="test-button"></FButtonEdit>);
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('input').attributes().autocomplete).toBe('off');
        });
        test('it should be disabled', () => {
            const component = mount(() => <FButtonEdit id="test-button" disable={true}></FButtonEdit>);
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('input').attributes()).toHaveProperty('disabled');
        });
        test('it should net be disabled', () => {
            const component = mount(() => <FButtonEdit id="test-button"></FButtonEdit>);
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('input').attributes().disabled).toBeUndefined();
        });
        test('it should be editable', () => {
            const component = mount(() => <FButtonEdit id="test-button"></FButtonEdit>);
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('input').attributes().readonly).toBeUndefined();
        });
        test('it should not be editable', () => {
            const component = mount(() => <FButtonEdit id="test-button" editable={false}></FButtonEdit>);
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('input').attributes()).toHaveProperty('readonly');
        });
        test('it should show clear button', () => {
            const component = mount(() => <FButtonEdit id="test-button" enableClear={true}></FButtonEdit>);
            expect(component.find('#test-button').find('#test-button-input-group').find('div')
                .find('.input-group-clear').exists()).toBeTruthy();
        });
        test('it should not show clear button', () => {
            const component = mount(() => <FButtonEdit id="test-button" enableClear={false}></FButtonEdit>);
            expect(component.find('#test-button').find('#test-button-input-group').find('div')
                .find('.input-group-clear').exists()).toBeFalsy();
        });
        test('it should be readonly', () => {
            const component = mount(() => <FButtonEdit id="test-button" readonly={true}></FButtonEdit>);
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('input').attributes()).toHaveProperty('readonly');
        });
        test('it should not be readonly', () => {
            const component = mount(() => <FButtonEdit id="test-button" readonly={false}></FButtonEdit>);
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('input').attributes().readonly).toBeUndefined();
        });
        test('it should show append button even be disabled', () => {
            const component = mount(() => <FButtonEdit id="test-button" disable={true} showButtonWhenDisabled={true}></FButtonEdit>);
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('#test-button-button-group').classes().includes('append-force-show')).toBeTruthy();
        });
        test('it should has title', () => {
            const displayText = "it should has title";
            const component = mount(() => <FButtonEdit id="test-button" enableTitle={true} v-model={displayText}></FButtonEdit>);
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('input').attributes().title).toBe(displayText);
        });
        test('it should has type', async () => {
            const textEditorComponent = mount(() => <FButtonEdit id="test-button"></FButtonEdit>);
            expect(textEditorComponent.find('#test-button').find('#test-button-input-group')
                .find('input').attributes().type).toBe('text');
            const inputType = ref('text');
            const tagComponent = mount(() => <FButtonEdit id="test-button" multiSelect={true} inputType={inputType.value}></FButtonEdit>);
            expect(tagComponent.find('#test-button').find('#test-button-input-group')
                .find('#test-button-tag-editor').exists()).toBeFalsy();
            inputType.value = 'tag';
            await nextTick();
            expect(tagComponent.find('#test-button').find('#test-button-input-group')
                .find('#test-button-tag-editor').exists()).toBeTruthy();
        });
        test('it should has placeholder', async () => {
            const placeholder = "it should has title";
            const disable = ref(true);
            const readonly = ref(false);
            const forcePlaceholder = ref(false);
            const component = mount(() => <FButtonEdit id="test-button" disable={disable.value} readonly={readonly.value}
                forcePlaceholder={forcePlaceholder.value} placeholder={placeholder}></FButtonEdit>);
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('input').attributes().placeholder).toBe('');

            forcePlaceholder.value = true;
            await nextTick();
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('input').attributes().placeholder).toBe(placeholder);

            forcePlaceholder.value = false;
            disable.value = false;
            readonly.value = true;
            await nextTick();
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('input').attributes().placeholder).toBe('');

            forcePlaceholder.value = true;
            await nextTick();
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('input').attributes().placeholder).toBe(placeholder);

            forcePlaceholder.value = false;
            disable.value = true;
            readonly.value = true;
            await nextTick();
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('input').attributes().placeholder).toBe('');

            forcePlaceholder.value = true;
            await nextTick();
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('input').attributes().placeholder).toBe(placeholder);

            disable.value = false;
            readonly.value = false;
            await nextTick();
            expect(component.find('#test-button').find('#test-button-input-group')
                .find('input').attributes().placeholder).toBe(placeholder);
        });
        test('it should has min length', () => { });
        test('it should has max length', () => { });
        test('it should has tab index', () => { });
    });

    describe('render', () => {
        test('it should has custom class', () => {
            const component = mount({
                setup() {
                    return () => {
                        return <FButtonEdit id="test-button" customClass="test-button-custom-class1 test-button-custom-class2"></FButtonEdit>;
                    };
                }
            });
            expect(component.find('#test-button').classes().includes('test-button-custom-class1')).toBeTruthy();
            expect(component.find('#test-button').classes().includes('test-button-custom-class2')).toBeTruthy();
        });
        test('it should show tags', () => {
            const inputType = ref('tag');
            const displayText = ref('it,should,show,tags');
            const tagComponent = mount(() => <FButtonEdit id="test-button" v-model={displayText.value}
                multiSelect={true} inputType={inputType.value}></FButtonEdit>);
            expect(tagComponent.find('#test-button').find('#test-button-input-group')
                .find('#test-button-tag-editor').exists()).toBeTruthy();
        });
        test('it should update tags', () => {
            const inputType = ref('tag');
            const displayText = ref('it,should,show,tags');
            const tagComponent = mount(() => <FButtonEdit id="test-button" v-model={displayText.value}
                multiSelect={true} inputType={inputType.value}></FButtonEdit>);
            tagComponent.find('#test-button').find('#test-button-input-group')
                .find('#test-button-tag-editor').find('.tag-delete').find('.f-icon-close').trigger('click');
        });
    });

    describe('methods', () => { });

    describe('events', () => {
        test('it should emit event named clear when click clear button', async () => {
            const handleClick = jest.fn();
            const wrapper = mount({
                setup() {
                    return () => {
                        return <FButtonEdit enableClear onClear={handleClick}></FButtonEdit>;
                    };
                }
            });
            await wrapper.find('.input-group-clear').trigger('click');
            expect(handleClick).toBeCalled();
        });

        test('it should emit event named change when changed text box value', async () => {
            const handleClick = jest.fn();
            const num = ref('0');
            const wrapper = mount({
                setup() {
                    return () => {
                        return <FButtonEdit v-model={num.value} onChange={handleClick}></FButtonEdit>;
                    };
                }
            });
            // await wrapper.find('div').find('div').find('input').setValue('test');
            // expect(handleClick).toBeCalled();
        });

        test('it should emit event named click whend click text box', async () => {
            const handleClick = jest.fn();
            const component = mount(() => <FButtonEdit onClick={handleClick}></FButtonEdit>);
            await component.find('div').find('div').find('input').trigger('click');
            expect(handleClick).toBeCalled();
        });

        test('it should emit event named clickButton when click append button', async () => {
            const handleClick = jest.fn();
            const component = mount(() => <FButtonEdit onClickButton={handleClick}></FButtonEdit>);
            await component.find('.input-group-append-button').trigger('click');
            expect(handleClick).toBeCalled();
        });

        test('it should not emit event named clickButton when click append button and button edit is disabled', async () => {
            const handleClick = jest.fn();
            const component = mount(() => <FButtonEdit disable={true} onClickButton={handleClick}></FButtonEdit>);
            await component.find('.input-group-append-button').trigger('click');
            expect(handleClick).toBeCalledTimes(0);
        });

        test('it should emit event named clickButton when click append button event button edit is disabled', async () => {
            const handleClick = jest.fn();
            const component = mount(() => <FButtonEdit disable={true} onClickButton={handleClick}
                showButtonWhenDisabled={true}></FButtonEdit>);
            await component.find('.input-group-append-button').trigger('click');
            expect(handleClick).toBeCalled();
        });

        test('it should emit event named blur when text box lost focus', async () => {
            const handleClick = jest.fn();
            const wrapper = mount({
                setup() {
                    return () => {
                        return <FButtonEdit onBlur={handleClick}></FButtonEdit>;
                    };
                }
            });
            await wrapper.find('div').find('div').find('input').trigger('blur');
            expect(handleClick).toBeCalled();
        });

        test('it should emit event named focus when text box get focus', async () => {
            const handleClick = jest.fn();
            const wrapper = mount({
                setup() {
                    return () => {
                        return <FButtonEdit onFocus={handleClick}></FButtonEdit>;
                    };
                }
            });
            await wrapper.find('div').find('div').find('input').trigger('focus');
            expect(handleClick).toBeCalled();
        });

        test('it should emit event named mouseEnterIcon when mouse move in append button', async () => {
            const handleClick = jest.fn();
            const wrapper = mount({
                setup() {
                    return () => {
                        return <FButtonEdit onMouseEnterIcon={handleClick}></FButtonEdit>;
                    };
                }
            });
            await wrapper.find('.input-group-append-button').trigger('mouseenter');
            expect(handleClick).toBeCalled();
        });

        test('it should emit event named mouseLeaveIcon when mouse leave append button', async () => {
            const handleClick = jest.fn();
            const wrapper = mount({
                setup() {
                    return () => {
                        return <FButtonEdit onMouseLeaveIcon={handleClick}></FButtonEdit>;
                    };
                }
            });
            await wrapper.find('.input-group-append-button').trigger('mouseleave');
            expect(handleClick).toBeCalled();
        });

        test('it should emit event named keyup when input text in text box', async () => {
            const handleClick = jest.fn();
            const wrapper = mount({
                setup() {
                    return () => {
                        return <FButtonEdit onKeyup={handleClick}></FButtonEdit>;
                    };
                }
            });
            await wrapper.find('div').find('div').find('input').trigger('keyup');
            expect(handleClick).toBeCalled();
        });

        test('it should emit event named keydown when input text in text box', async () => {
            const handleClick = jest.fn();
            const wrapper = mount({
                setup() {
                    return () => {
                        return <FButtonEdit onKeydown={handleClick}></FButtonEdit>;
                    };
                }
            });
            await wrapper.find('div').find('div').find('input').trigger('keydown');
            expect(handleClick).toBeCalled();
        });

        test('it should emit event named input when input text in text box', async () => {
            const handleClick = jest.fn();
            const wrapper = mount({
                setup() {
                    return () => {
                        return <FButtonEdit onInput={handleClick}></FButtonEdit>;
                    };
                }
            });
            await wrapper.find('div').find('div').find('input').trigger('input');
            expect(handleClick).toBeCalled();
        });
    });

    describe('behaviors', () => {
        test('it should hightlight text box when mouse in', async () => { });
        test('it should show clear button when mouse in text box', async () => {
            const displayText = ref('');
            const component = mount(() => <FButtonEdit id="test-button" v-model={displayText.value} enableClear={true}></FButtonEdit>);
            const buttonEditInputGroup = component.find('#test-button').find('#test-button-input-group');
            buttonEditInputGroup.trigger('mouseenter');
            expect(component.find('.input-group-clear').element.computedStyleMap().get('display')).toBe('none');

            displayText.value = 'show clear when mouse in';
            const component2 = mount(() => <FButtonEdit id="test-button" v-model={displayText.value} enableClear={true}></FButtonEdit>);
            const buttonEditInputGroup2 = component2.find('#test-button').find('#test-button-input-group');
            buttonEditInputGroup2.trigger('mouseenter');
            await nextTick();
            expect(component2.find('.input-group-clear').element.computedStyleMap().get('display')).toBe('');
            buttonEditInputGroup2.trigger('mouseleave');
            await nextTick();
            expect(component2.find('.input-group-clear').element.computedStyleMap().get('display')).toBe('none');

            const component3 = mount(() => <FButtonEdit id="test-button" v-model={displayText.value} enableClear={true} readonly={true}></FButtonEdit>);
            const buttonEditInputGroup3 = component3.find('#test-button').find('#test-button-input-group');
            buttonEditInputGroup3.trigger('mouseenter');
            await nextTick();
            expect(component3.find('.input-group-clear').exists()).toBeFalsy();
            buttonEditInputGroup3.trigger('mouseleave');
            await nextTick();
            expect(component3.find('.input-group-clear').exists()).toBeFalsy();
        });
        test('it should show clear button when fouse text box and it not empty', async () => {
            const displayText = ref('');
            const component = mount(() => <FButtonEdit id="test-button" v-model={displayText.value} enableClear={true}></FButtonEdit>);
            expect(component.find('.input-group-clear').element.computedStyleMap().get('display')).toBe('none');
            const textBox = component.find('#test-button').find('#test-button-input-group').find('input');
            textBox.setValue('show clear button');
            textBox.trigger('focus');
            await nextTick();
            expect(component.find('.input-group-clear').element.computedStyleMap().get('display')).toBe('');
            textBox.trigger('blur');
            textBox.setValue('hide clear button');
            await nextTick();
            expect(component.find('.input-group-clear').element.computedStyleMap().get('display')).toBe('none');
        });
        test('it should hide clear button when text box is empty', () => {

        });
        test('it should show clear button when text any word from empty', () => { });
        test('it should popup content by default', async () => {
            const displayText = "it should has title";
            const component = mount(() => <FButtonEdit id="test-button">
                <div id="test-button-popup-content">it should popup content by default</div>
            </FButtonEdit>);
            component.find('#test-button').find('#test-button-input-group').find('div')
                .find('.input-group-append-button').trigger('click');

            await nextTick();
            // expect(component.find('#test-button').element.nextElementSibling?.id).toBe('test-button-popover');
        });
        test('it should popup content on input', async () => {
            const displayText = "it should has title";
            const component = mount(() => <FButtonEdit id="test-button" popupOnInput={true}>
                <div id="test-button-popup-content">it should popup content by default</div>
            </FButtonEdit>);
            const textBox = component.find('#test-button').find('#test-button-input-group').find('input');
            textBox.setValue(displayText);
            textBox.trigger('input');
            await nextTick();
        });
        test('it should popup content on focus', async () => {
            const displayText = "it should has title";
            const component = mount(() => <FButtonEdit id="test-button" popupOnFocus={true}>
                <div id="test-button-popup-content">it should popup content by default</div>
            </FButtonEdit>);
            const textBox = component.find('#test-button').find('#test-button-input-group').find('input');
            textBox.setValue(displayText);
            textBox.trigger('focus');
            await nextTick();
        });
        test('it should show content by overlay', async () => {
            const displayText = "it should has title";
            const component = mount(() => <FButtonEdit id="test-button" buttonBehavior="Overlay" >
                <div id="test-button-popup-content">it should popup content by default</div>
            </FButtonEdit>);
            component.find('#test-button').find('#test-button-input-group').find('div')
                .find('.input-group-append-button').trigger('click');
            await nextTick();
            // expect(component.find('#test-button').element.nextElementSibling?.id).toBe('test-button-popover');
        });
        test('it should popup on input content by overlay', async () => {
            const displayText = "it should has title";
            const component = mount(() => <FButtonEdit id="test-button" popupOnInput={true} >
                <div id="test-button-popup-content">it should popup content by default</div>
            </FButtonEdit>);
            component.find('#test-button').find('#test-button-input-group').find('div')
                .find('.input-group-append-button').trigger('click');
            await nextTick();
        });
        test('it should update value by commit', () => {
            const displayText = "it should has title";
            const componentRef = ref();
            const component = mount(() => <FButtonEdit ref={componentRef} id="test-button" popupOnInput={true} >
                <div id="test-button-popup-content">it should popup content by default</div>
            </FButtonEdit>);
            componentRef.value.commitValue();
        });
        test('it should hide popover by enter key up', async () => {
            const displayText = "it should has title";
            const componentRef = ref();
            const component = mount(() => <FButtonEdit ref={componentRef} id="test-button" popupOnInput={true} >
                <div id="test-button-popup-content">it should popup content by default</div>
            </FButtonEdit>);
            const textBox = component.find('#test-button').find('#test-button-input-group').find('input');
            textBox.setValue(displayText);
            textBox.trigger('input');
            await nextTick();
            // textBox.trigger('keyup', { key: 'Enter' });
            await nextTick();
        });
    });
});
