import { Ref, SetupContext, computed, nextTick, ref } from "vue";
import { ButtonEditProps } from "../button-edit.props";
import { UsePopup } from "./types";
import { useOpen } from "./use-open";

export function usePopup(
    props: ButtonEditProps,
    context: SetupContext,
    buttonEditRef: Ref<any>,
    modelValue: Ref<string>
): UsePopup {
    const popoverRef = ref<any>();
    const shouldPopupContent = ref(false);
    const { judgeCanOpen } = useOpen(props, modelValue);

    // function hidePopverOnClickBodyHandler($event: MouseEvent) {
    //     const closestPopoverElement = ($event.target as any)?.closest('.popover-body');
    //     const popoverInstance = popoverRef.value;
    //     if (closestPopoverElement && popoverInstance && closestPopoverElement === popoverInstance.popoverRef) {
    //         return;
    //     }
        
    //     if (popoverInstance) {
    //         shouldPopupContent.value = false;
    //         popoverInstance.hide();
    //         document.body.removeEventListener('click', hidePopverOnClickBodyHandler);
    //     }
    // };

    function tryShowPopover() {
        const popoverInstance = popoverRef.value;
        if (popoverInstance) {
            popoverInstance.show(buttonEditRef.value);
            // document.body.addEventListener('click', hidePopverOnClickBodyHandler);
        }
    }
    /**
     * 此方法的调用的场景，有内部校验之后调用，有外部没有校验之前调用
     * @param forceOpen 
     * @returns 
     * @param forceOpen
     * @returns
     */
    async function togglePopup(forceOpen = false) {
        const hasContent = !!context.slots.default;
        if (hasContent) {
            if (!forceOpen && !shouldPopupContent.value) {
                // 当前关闭状态 =》打开状态；
                const canChange = await judgeCanOpen();
                if (!canChange) {
                    return;
                }
            }
            shouldPopupContent.value = !shouldPopupContent.value;
            await nextTick();
            tryShowPopover();
        }
    }

    async function popup(forceOpen = false) {
        const hasContent = !!context.slots.default;
        if (hasContent) {
            if (!forceOpen) {
                const canChange = await judgeCanOpen();
                if (!canChange) {
                    return;
                }
            }
            shouldPopupContent.value = true;
            await nextTick();
            tryShowPopover();
        }
    }

    function hidePopup() {
        shouldPopupContent.value = false;
    }

    return { hidePopup, popup, shouldPopupContent, togglePopup, popoverRef };
}
