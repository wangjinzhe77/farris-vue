/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { UseButton, UsePopup } from './types';
import { computed, inject, nextTick, ref, Ref, SetupContext } from 'vue';
import { ButtonEditProps } from '../button-edit.props';
import OverylayService from '../../../overlay/src/overlay.service';
import { F_MODAL_SERVICE_TOKEN, FModalService } from '../../../modal';
import { useOpen } from "./use-open";

export function useButton(
    props: ButtonEditProps,
    context: SetupContext,
    buttonEditRef: Ref<any>,
    usePopupComposition: UsePopup,
    modelValue: Ref<string>
): UseButton {
    const buttonBehavior = ref(props.buttonBehavior);
    const shouldPopupOnInput = ref(props.popupOnInput);
    const buttonGroupClass = computed(() => ({
        'input-group-append': true,
        'append-force-show': props.showButtonWhenDisabled && (props.readonly || props.disable)
    }));
    const { judgeCanOpen } = useOpen(props, modelValue);
    const canClickAppendButton = computed(() => props.showButtonWhenDisabled || ((!props.editable || !props.readonly) && !props.disable));

    const modalService: FModalService | null = inject(F_MODAL_SERVICE_TOKEN, null);

    const modalInstance = ref();

    async function onClickButton($event: Event) {
        // $event.stopPropagation();
        if (!await judgeCanOpen()) {
            return;
        }

        if (canClickAppendButton.value) {
            const hasContent = !!context.slots.default;
            if (buttonBehavior.value === 'Modal') {
                const modalOption = props.modalOptions;
                const dialog = modalService?.open({
                    ...modalOption,
                    render: () => {
                        return context.slots.default && context.slots.default();
                    }
                });

                modalInstance.value = dialog?.modalRef;
            }
            if (buttonBehavior.value === 'Overlay') {
                OverylayService.show({
                    // host: buttonEditRef.value,
                    host: document.body,
                    backgroundColor: 'rgba(0,0,0,.15)',
                    render: () => {
                        return context.slots.default && context.slots.default();
                    }
                });
            }
            if (hasContent && buttonBehavior.value === 'Popup') {
                usePopupComposition.togglePopup(true);
            }
            if (hasContent && shouldPopupOnInput.value) {
                usePopupComposition.hidePopup();
            }
            context.emit('clickButton', { origin: $event, value: props.modelValue });
        }
        // $event.stopPropagation();
    }

    function onMouseEnterButton($event: MouseEvent) {
        context.emit('mouseEnterIcon', $event);
    }

    function onMouseLeaveButton($event: MouseEvent) {
        context.emit('mouseLeaveIcon', $event);
    }

    return {
        buttonGroupClass,
        onClickButton,
        onMouseEnterButton,
        onMouseLeaveButton,
        modalRef: modalInstance
    };
}
