import { Ref} from "vue";
import { ButtonEditProps } from "../button-edit.props";
import { UseOpen } from "./types";

export function useOpen(
    props: ButtonEditProps,
    modelValue: Ref<string>
): UseOpen {
    /** 判断是否允许打开窗口或者下拉面板*/
    function judgeCanOpen():Promise<boolean>{
        const beforeOpen = props.beforeOpen || props.beforeClickButton || null;
        let flag = Promise.resolve(true);
        if (beforeOpen) {
            const canOpen = beforeOpen(modelValue.value);
            if (typeof canOpen === 'undefined') {
                return flag;
            }
            if (typeof canOpen === 'boolean') {
                flag = Promise.resolve(canOpen);
            } else {
                flag = canOpen;
            }
        }
        return flag;
    };
    return {judgeCanOpen };
}
