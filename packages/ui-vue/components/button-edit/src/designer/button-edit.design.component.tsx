/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, onMounted, inject } from 'vue';
import type { SetupContext } from 'vue';
import { buttonEditProps, ButtonEditProps } from '../button-edit.props';
import { useButton } from '../composition/use-button';
import { usePopup } from '../composition/use-popup';
import getButtonGroupRender from './button-group.design.component';

import { DesignerItemContext, useDesignerComponent } from '@farris/ui-vue/components/designer-canvas';

export default defineComponent({
    name: 'FButtonEdit',
    props: buttonEditProps,
    emits: [
        'updateExtendInfo',
        'clear',
        'change',
        'click',
        'clickButton',
        'blur',
        'focus',
        'mouseEnterIcon',
        'mouseLeaveIcon',
        'keyup',
        'keydown',
        'inputClick',
        'input',
        'update:modelValue'
    ] as (string[] & ThisType<void>) | undefined,
    setup(props: ButtonEditProps, context: SetupContext) {
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const componentInstance = useDesignerComponent(elementRef, designItemContext);
        const customClass = ref<string>(props.customClass);
        const modelValue = ref(props.modelValue);
        const popupComposition = usePopup(props, context, elementRef, modelValue);
        const useButtonComposition = useButton(props, context, elementRef, popupComposition, modelValue);
        const inputElementRef = ref();
        const inputGroupClass = computed(() => {
            const classObject = {
                'f-cmp-inputgroup': true,
                'input-group': true,
                'f-state-disabled': true,
                'f-state-editable': false,
                'f-state-readonly': true,
            };

            return classObject;
        });
        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        const buttonEditClass = computed(() => {
            const classObject = {
                'f-button-edit': true,
                'f-cmp-inputgroup': true,
                'f-button-edit-nowrap': !props.wrapText
            } as Record<string, boolean>;
            if (customClass.value) {
                customClass.value.split(' ').reduce<Record<string, boolean>>((result: Record<string, boolean>, className: string) => {
                    result[className] = true;
                    return result;
                }, classObject);
            }
            return classObject;
        });

        const { renderButtonGroup } = getButtonGroupRender(props, context, useButtonComposition);

        return () => {
            return (
                <div {...context.attrs} ref={elementRef} class={buttonEditClass.value} id={props.id}>
                    <div class={inputGroupClass.value}>
                        <input
                            ref={inputElementRef}
                            class="form-control"
                            readonly
                            placeholder={props.placeholder}
                        />
                        {renderButtonGroup()}
                    </div>
                </div>
            );
        };
    }
});
