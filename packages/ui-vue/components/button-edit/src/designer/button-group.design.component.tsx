import { ref, SetupContext } from "vue";
import { ButtonEditProps } from "../button-edit.props";
import { UseButton } from "../composition/types";

export default function (
    props: ButtonEditProps,
    context: SetupContext,
    useButtonComposition: UseButton
) {
    const { buttonGroupClass } = useButtonComposition;
    const buttonHandleElement = ref<HTMLElement>();


    return {
        renderButtonGroup: () => {
            return (
                <div id={`${props.id}-button-group`} class={buttonGroupClass.value}>
                    {
                        context.slots.buttonContent ?
                            <span class="input-group-text input-group-append-button">
                                {context.slots.buttonContent()}
                            </span> : props.buttonContent ? <span class="input-group-text input-group-append-button" v-html={props.buttonContent}
                                ref={buttonHandleElement}
                            ></span> : null
                    }
                </div>
            );
        },
        buttonHandleElement
    };
}
