import { SetupContext, ref } from "vue";
import { ButtonEditProps } from "../button-edit.props";
import { UsePopup } from "../composition/types";
import FPopover from '@farris/ui-vue/components/popover';

export default function (
    props: ButtonEditProps,
    context: SetupContext,
    popupComposition: UsePopup
) {
    const popupMinWidth = ref(props.popupMinWidth);
    const { hidePopup, popoverRef } = popupComposition;

    return () => {
        return <FPopover id={`${props.id}-popover`} ref={popoverRef} visible={true} placement={props.placement}
            host={props.popupHost} keep-width-with-reference={props.keepWidthWithReference} fitContent={true}
            right-boundary={props.popupRightBoundary} minWidth={popupMinWidth.value}
            offsetX={props.popupOffsetX}
            onHidden={hidePopup}>
            {context.slots.default?.()}
        </FPopover>;
    };
};
