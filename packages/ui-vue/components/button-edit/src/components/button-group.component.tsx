import { ref, SetupContext } from "vue";
import { ButtonEditProps } from "../button-edit.props";
import { UseButton, UseClear } from "../composition/types";

export default function (
    props: ButtonEditProps,
    context: SetupContext,
    useButtonComposition: UseButton,
    useClearComposition: UseClear
) {
    const { buttonGroupClass, onClickButton, onMouseEnterButton, onMouseLeaveButton } = useButtonComposition;
    const { enableClearButton, showClearButton, onClearValue } = useClearComposition;
    const buttonHandleElement = ref<HTMLElement>();


    return {
        renderButtonGroup: () => {
            return (
                <div id={`${props.id}-button-group`} class={buttonGroupClass.value}>
                    {enableClearButton.value && (
                        <span class="input-group-text input-group-clear" v-show={showClearButton.value} onClick={onClearValue}>
                            <i class="f-icon modal_close"></i>
                        </span>
                    )}
                    {
                        context.slots.buttonContent ? <span class="input-group-text input-group-append-button"
                            onClick={onClickButton} onMouseenter={onMouseEnterButton} onMouseleave={onMouseLeaveButton}
                        >
                            {context.slots.buttonContent()}
                        </span> : props.buttonContent ? <span class="input-group-text input-group-append-button" v-html={props.buttonContent}
                            ref={buttonHandleElement}
                            onClick={onClickButton} onMouseenter={onMouseEnterButton} onMouseleave={onMouseLeaveButton}
                        ></span> : null
                    }
                </div>
            );
        },
        buttonHandleElement
    };
}
