/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, watch, onMounted } from 'vue';
import type { SetupContext } from 'vue';
import { buttonEditProps, ButtonEditProps } from './button-edit.props';
import { useButton } from './composition/use-button';
import { useClear } from './composition/use-clear';
import { useTextBox } from './composition/use-text-box';
import { usePopup } from './composition/use-popup';
import getTextEditorRender from './components/text-edit.component';
import getTagEditorRender from './components/tag-edit.component';
import getButtonGroupRender from './components/button-group.component';
import getPopupRender from './components/popup-container.component';

export default defineComponent({
    name: 'FButtonEdit',
    props: buttonEditProps,
    emits: [
        'updateExtendInfo',
        'clear',
        'change',
        'click',
        'clickButton',
        'blur',
        'focus',
        'mouseEnterIcon',
        'mouseLeaveIcon',
        'keyup',
        'keydown',
        'inputClick',
        'input',
        'update:modelValue'
    ] as (string[] & ThisType<void>) | undefined,
    setup(props: ButtonEditProps, context: SetupContext) {
        const buttonEditRef = ref<any>();
        const customClass = ref<string>(props.customClass);
        const modelValue = ref(props.modelValue);
        const popupComposition = usePopup(props, context, buttonEditRef, modelValue);
        const { shouldPopupContent, hidePopup, togglePopup } = popupComposition;
        const useButtonComposition = useButton(props, context, buttonEditRef, popupComposition, modelValue);
        const displayText = ref('');
        const useTextBoxComposition = useTextBox(props, context, modelValue, displayText, popupComposition);
        const { hasFocusedTextBox, commitValue, inputGroupClass } = useTextBoxComposition;
        const useClearComposition = useClear(props, context, modelValue, hasFocusedTextBox, displayText, useTextBoxComposition);
        const { onMouseEnterTextBox, onMouseLeaveTextBox } = useClearComposition;

        const buttonEditClass = computed(() => {
            const classObject = {
                'f-button-edit': true,
                'f-cmp-inputgroup': true,
                'f-button-edit-nowrap': !props.wrapText
            } as Record<string, boolean>;
            if (customClass.value) {
                customClass.value.split(' ').reduce<Record<string, boolean>>((result: Record<string, boolean>, className: string) => {
                    result[className] = true;
                    return result;
                }, classObject);
            }
            return classObject;
        });

        function getEditorRender() {
            if (props.multiSelect && props.inputType === 'tag') {
                return getTagEditorRender(props, modelValue, useTextBoxComposition);
            }
            return getTextEditorRender(props, modelValue, useTextBoxComposition);
        }

        let renderEditor = getEditorRender();

        watch([() => props.multiSelect, () => props.inputType], () => {
            renderEditor = getEditorRender();
        });

        const { renderButtonGroup, buttonHandleElement } = getButtonGroupRender(props, context, useButtonComposition, useClearComposition);

        const renderPopupContent = getPopupRender(props, context, popupComposition);

        const componentInstance = {
            commitValue,
            elementRef: buttonEditRef,
            hidePopup,
            shouldPopupContent,
            togglePopup,
            openDialog: () => {
                if (buttonHandleElement.value && props.buttonBehavior === 'Modal') {
                    buttonHandleElement.value.click();
                }
            },
            getModal: () => {
                if (props.buttonBehavior === 'Modal') {
                    return useButtonComposition.modalRef.value?.value;
                }
                return null;
            }
        };

        onMounted(() => {
            buttonEditRef.value.componentInstance = componentInstance;

            window.onresize = () => {
                document.body.click();
            };
        });

        context.expose(componentInstance);

        return () => {
            return (
                <>
                    <div {...context.attrs} ref={buttonEditRef} class={buttonEditClass.value} id={props.id}>
                        <div id={`${props.id}-input-group`} class={inputGroupClass.value}
                            onMouseenter={onMouseEnterTextBox} onMouseleave={onMouseLeaveTextBox}>
                            {renderEditor()}
                            {renderButtonGroup()}
                        </div>
                    </div>
                    {shouldPopupContent.value && renderPopupContent()}
                </>
            );
        };
    }
});
