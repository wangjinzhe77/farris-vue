 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Section from './src/section.component';
import SestionDesign from './src/designer/section.design.component';
import { eventHandlerResolver, propsResolver } from './src/section.props';
import { withInstall } from '@farris/ui-vue/components/common';
import { sectionToolbarItemResolver } from './src/designer/section-toolbar-item.props';

export * from './src/section.props';

Section.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap.section = Section;
    propsResolverMap.section = propsResolver;
    resolverMap.section = { eventHandlerResolver };
};
Section.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.section = SestionDesign;
    propsResolverMap.section = propsResolver;
    propsResolverMap['section-toolbar-item'] = sectionToolbarItemResolver;
};
export { Section };
export default withInstall(Section);
