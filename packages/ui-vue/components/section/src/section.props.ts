/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver, createSectionEventHandlerResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import sectionSchema from './schema/section.schema.json';

export interface ButtonAppearance {
    class: string;
}

export interface ButtonConfig {
    id: string;
    disabled: boolean;
    title: string;
    click: any;
    appearance: ButtonAppearance;
    visible?: boolean;
}

// export interface ToolbarConfig {
//     position: string;
//     contents: ButtonConfig[];
// }
export type ButtonPosition = 'inHead' | 'inContent';

export const sectionProps = {
    /** 设置内容区域样式 */
    contentClass: { type: String, default: '' },
    /** 自定义样式 */
    customClass: { type: String, default: '' },
    customStyle: { type: String, default: '' },
    /** 设置是否启用収折功能。支持：空字符串，不启用収折 | default 默认収折方式 || custom自定义収折方式 */
    enableAccordion: { type: String, default: '' },
    /** 设置初始是否处于展开状态，支持：true 内容区域被展开|false 内容区域被収折 */
    expandStatus: { type: Boolean, default: true },

    /** 设置Section的主标题 */
    mainTitle: { type: String, default: '' },
    /** 设置是否显示头部区域 */
    showHeader: { type: Boolean, default: true },
    /** 设置Section的副标题 */
    subTitle: { type: String, default: '' },
    /** 展开文本 */
    expandLabel: { type: String, default: '展开' },
    /** 收起文本 */
    collapseLabel: { type: String, default: '收起' },

    /** 头部区域的自定义样式。配置头部自定义模板后，此属性有效。 */
    headerClass: { type: String, default: '' },

    /** 设置是否启用最大化 */
    enableMaximize: { type: Boolean, default: false },

    /** 头部扩展区域的自定义样式。配置头部扩展区域模板后，此属性有效。 */
    headerContentClass: { type: String, default: '' },

    /** 扩展区域的自定义样式。配置扩展区域模板后，此属性有效。 */
    extendClass: { type: String, default: '' },

    /** 按钮组 */
    buttons: { type: Array<any>, default: [] },
    /** 按钮组的位置，inHead:头部区域| inContent:内容区域 */
    buttonPosition: { type: String as PropType<ButtonPosition>, default: 'inHead' },

    /** 设置是否铺满剩余空间，支持：false按照内容自动高度|true 铺满剩余空间 */
    fill: { type: Boolean, default: false },

    // 暂未使用该属性
    // clickThrottleTime: { type: Number, default: 350 },
    // context: { type: Object },


    // 暂未使用该属性
    // index: { type: Number },
    // maxStatus: { type: Boolean, default: false },
    // showToolbarMoreButton: { type: Boolean, default: true },
    // 暂未使用该属性
    // toolbarPosition: { type: String, default: '' },
    // toolbarButtons: { type: Array<object>, default: [] },
    // toolbar: { type: Object as PropType<ToolbarConfig>, default: {} }
} as Record<string, any>;

export type SectionProps = ExtractPropTypes<typeof sectionProps>;

export const propsResolver = createPropsResolver<SectionProps>(sectionProps, sectionSchema, schemaMapper, schemaResolver);
export const eventHandlerResolver = createSectionEventHandlerResolver();
export const sectionDesignProps = Object.assign({}, sectionProps, { componentId: { type: String, default: '' } });
export type SectionDesignProps = ExtractPropTypes<typeof sectionDesignProps>;
