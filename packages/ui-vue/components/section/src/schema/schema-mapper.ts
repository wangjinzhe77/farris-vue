import { resolveAppearance, MapperFunction } from '../../../dynamic-resolver';
import { resolveSectionAccordion } from './accordion-resolver';
import { resolveSectionToolbar } from './toolbar-resolver';

export const schemaMapper = new Map<string, string | MapperFunction>([
    ['appearance', resolveAppearance],
    ['expanded', 'expandStatus'],
    ['enableAccordion', resolveSectionAccordion],
    ['toolbar', resolveSectionToolbar]
]);
