export function resolveSectionToolbar(key: string, toolbarObject: { buttons: any[]; position: string }) {

    return {
        buttons: toolbarObject.buttons,
        buttonPosition: toolbarObject.position
    };
}
