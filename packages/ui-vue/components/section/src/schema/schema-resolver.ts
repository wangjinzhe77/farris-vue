import { ComponentSchema, DesignerComponentInstance } from "../../../designer-canvas";
import { DgControl } from "../../../designer-canvas/src/composition/dg-control";
import { DynamicResolver } from "../../../dynamic-resolver";

/**
 * 从工具箱拖拽创建分组面板，自动创建container-section两层结构
 * @param resolver 
 * @param sectionSchema 
 * @param context 
 * @param designerHostService 
 * @returns 
 */
function wrapContainerForSection(resolver: DynamicResolver, sectionSchema: Record<string, any>, context: Record<string, any>) {
    const radomNum = Math.random().toString().slice(2, 6);
    const parentComponentInstance = context.parentComponentInstance as DesignerComponentInstance;

    Object.assign(sectionSchema, {
        id: `section-${radomNum}`,
        appearance: {
            class: 'f-section-in-mainsubcard'
        },
        mainTitle: context.mainTitle || sectionSchema.mainTitle || '标题'
    });

    const containerSchema = resolver.getSchemaByType('content-container') as ComponentSchema;
    Object.assign(containerSchema, {
        id: `container-${radomNum}`,
        appearance: {
            class: 'f-struct-wrapper'
        },
        contents: [sectionSchema]
    });


    // 判断拖拽的目标容器
    const targetComponentSchema = parentComponentInstance.schema;
    switch (targetComponentSchema && targetComponentSchema.type) {
        case DgControl['splitter-pane'].type: {
            // 左列右卡的模板：修改容器样式
            sectionSchema.appearance.class = 'f-section-in-main';
            break;
        }
    }
    // 父容器是flex布局的场景，需要将容器设置为display:block，否则容器宽度有问题
    const parentElementRef: any = parentComponentInstance.elementRef;
    const computedStyle = window.getComputedStyle(parentElementRef);
    if (computedStyle && computedStyle.display === 'flex') {
        containerSchema.appearance.class += ' d-block';
    }
    return containerSchema;
}

export function schemaResolver(resolver: DynamicResolver, schema: Record<string, any>, context: Record<string, any>): Record<string, any> {
    const parentComponentInstance = context.parentComponentInstance as DesignerComponentInstance;
    if (parentComponentInstance) {
        return wrapContainerForSection(resolver, schema, context);
    } else {
        return schema;
    }

}
