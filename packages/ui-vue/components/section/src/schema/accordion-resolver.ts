export function resolveSectionAccordion(key: string, value: any, resolvedSchema: any) {

    if (!value) {
        return { enableAccordion: '' };
    } else {
        return { enableAccordion: resolvedSchema.accordionMode };
    }
}
