export const sectionPropertyConfigs = {
    title: 'section',
    description: 'A Farris Container Component',
    type: 'object',
    categories: {
        basic: {
            title: '基本信息',
            description: 'Basic Infomation',
            properties: {
                id: {
                    title: '标识',
                    type: 'string',
                    description: 'The identifier of a component.',
                    readonly: true
                },
                type: {
                    title: '组件类型',
                    type: 'enum',
                    description: 'The type of a component.',
                    editor: {
                        type: 'combo-list',
                        textField: 'value',
                        valueField: 'key',
                        editable: false,
                        data: [
                            {
                                key: 'section',
                                value: '分组面板'
                            }
                        ]
                    }
                }
            }
        },
        appearance: {
            title: '外观',
            description: 'Appearance',
            properties: {
                class: {
                    description: '组件的CSS样式',
                    title: 'class样式'
                },
                style: {
                    description: '组件的内容样式',
                    title: 'style样式'
                },
                showHeader: {
                    title: '显示标题区域',
                    type: 'boolean',
                    description: ''
                },
                mainTitle: {
                    title: '主标题',
                    type: 'string',
                    description: '',
                    visible: {
                        showHeader: true
                    }
                },
                subTitle: {
                    title: '副标题',
                    type: 'string',
                    description: '',
                    visible: {
                        showHeader: true
                    }
                },
                fill: {
                    title: '填充内容区域',
                    type: 'boolean',
                    description: ''
                },
                enableAccordion: {
                    title: '面板收折',
                    type: 'enum',
                    description: '是否启用分组面板的收折特性',
                    editor: {
                        type: 'combo-list',
                        textField: 'value',
                        valueField: 'key',
                        editable: false,
                        data: [
                            {
                                key: '',
                                value: '不启用'
                            },
                            {
                                key: 'default',
                                value: '启用默认收折'
                            }
                        ]
                    },
                    visible: {
                        showHeader: true
                    }
                }
            },
            setPropertyRelates(changeObject, prop) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject && changeObject.propertyID) {
                    case 'mainTitle': {
                        changeObject.needRefreshControlTree = true;
                        break;
                    }
                }
            }
        },
        extends: {
            title: '扩展',
            description: '',
            properties: {
                contentClass: {
                    title: '内容区域样式',
                    type: 'string',
                    description: ''
                }
            }
        }
    }
};
