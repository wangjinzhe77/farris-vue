import { Ref } from "vue";
import { UseSectionMaxMin } from "./types";

export function useSectionMaxMin(
    sectionRef: Ref<any>,
    sectionParentElementRef: Ref<any>,
    maxStatus: Ref<boolean>): UseSectionMaxMin {

    let sectionNextSiblingElement: any;

    /**
     * 获取页面中body下所有元素的zIndex, 并返回下个浮层的新zindex
     * @param upperLayers 增长的index
     */
    function getFloatingLayerIndex(upperLayers = 1) {
        const selectors = [
            'body>.f-datagrid-settings-simple-host',
            'body>div',
            'body>farris-dialog>.farris-modal.show',
            'body>.farris-modal.show',
            'body>farris-filter-panel>.f-filter-panel-wrapper',
            'body .f-sidebar-show>.f-sidebar-main',
            'body>.popover.show',
            'body>filter-row-panel>.f-datagrid-filter-panel',
            'body>.f-section-maximize'
        ];

        const overlays = Array.from(document.body.querySelectorAll(selectors.join(','))).filter(n => n).map(n => {
            const { display, zIndex } = window.getComputedStyle(n);
            if (display === 'none') {
                return 0;
            }
            return parseInt(zIndex, 10);
        }).filter(n => n);
        let maxZindex = Math.max(...overlays);
        if (maxZindex < 1040) {
            maxZindex = 1040;
        }
        return maxZindex + upperLayers;
    }

    /**
     * 最大化
     */
    function onMaximum() {

        sectionNextSiblingElement = sectionRef.value.nextElementSibling;

        const bodyElement = document && document.querySelector("body");
        if (bodyElement) {
            bodyElement.appendChild(sectionRef.value);

            sectionRef.value.style.zIndex = getFloatingLayerIndex();;
        }
    }

    /**
     * 最小化
     */
    function onMinimize() {
        if (sectionNextSiblingElement) {
            sectionParentElementRef.value.insertBefore(
                sectionRef.value,
                sectionNextSiblingElement
            );
        } else {
            sectionParentElementRef.value.appendChild(sectionRef.value);
        }

        sectionRef.value.style.zIndex = null;
    }

    /**
     * 点击最大化/最小化图标事件
     */
    function onClickMaxMinIcon() {
        if (!sectionRef || !sectionParentElementRef.value) {
            return;
        }
        maxStatus.value = !maxStatus.value;

        maxStatus.value ? onMaximum() : onMinimize();
    }

    return { onClickMaxMinIcon };
}
