 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, onMounted, ref, SetupContext, watch } from 'vue';
import { SectionProps, sectionProps } from './section.props';
import getSectionHeaderRender from './components/header.component';
import getSectionToolbar from './components/toolbar.component';
import { getCustomClass } from '../../common';

export default defineComponent({
    name: 'FSection',
    props: sectionProps,
    emits: ['click'] as (string[] & ThisType<void>) | undefined,
    setup(props: SectionProps, context: SetupContext) {
        const sectionRef = ref<any>();
        const sectionParentElementRef = ref<any>();
        const customClass = ref<string>(props.customClass);
        const enableAccordion = computed(() => props.enableAccordion);
        const expandStatus = ref(props.expandStatus);
        const toolbarPosition = computed(() => props.buttonPosition);
        const sectionMaxStatus = ref(false);

        const renderSectionHeader = getSectionHeaderRender(props, context, expandStatus, sectionRef, sectionParentElementRef, sectionMaxStatus);
        const renderSectionToolbar = getSectionToolbar(props, context);

        const sectionClass = computed(() => {
            const classObject = {
                'f-section': true,
                'f-section-accordion': enableAccordion.value === 'default',
                'f-state-collapse': (enableAccordion.value === 'default' || enableAccordion.value === 'custom') && !expandStatus.value,
                'f-section-custom-accordion': enableAccordion.value === 'custom',
                'f-section-fill': props.fill,
                'f-section-maximize': sectionMaxStatus.value
            } as Record<string, boolean>;
            return getCustomClass(classObject, customClass.value);
        });

        const contentClass = computed(() => {
            const contentClassObject = {
                'f-section-content': true
            } as Record<string, boolean>;
            return getCustomClass(contentClassObject, props.contentClass);
        });
        const extendClass = computed(() => {
            const contentClassObject = {
                'f-section-extend': true
            } as Record<string, boolean>;
            return getCustomClass(contentClassObject, extendClass.value);
        });

        function renderSectionExtend() {
            return context.slots.extend &&
                <div class={extendClass.value}>{context.slots.extend()}</div>;
        }

        function renderSectionContent() {
            return <div class={contentClass.value}>
                {toolbarPosition.value === 'inContent' && renderSectionToolbar()}
                {context.slots.default && context.slots.default()}</div>;
        }

        watch(() => props.expandStatus, (newValue,oldValue)=>{
            if(newValue !== oldValue){
                expandStatus.value = newValue;
            }
        });

        onMounted(() => {
            if (sectionRef.value && sectionRef.value) {
                sectionParentElementRef.value = sectionRef.value.parentElement;
            }
        });

        return () => {
            return (
                <div class={sectionClass.value} ref={sectionRef} style={props.customStyle}>
                    {renderSectionHeader()}
                    {renderSectionExtend()}
                    {renderSectionContent()}
                </div>
            );
        };
    }
});
