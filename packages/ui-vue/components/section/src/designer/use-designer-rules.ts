import { DesignerHostService, DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { useDragulaCommonRule } from "../../../designer-canvas/src/composition/rule/use-dragula-common-rule";
import { ComponentSchema, DesignerItemContext } from "../../../designer-canvas/src/types";
import { sectionPropertyConfigs } from "../property-config/section.property-config";
import { UseTemplateDragAndDropRules } from "../../../designer-canvas/src/composition/rule/use-template-rule";
import { DgControl } from "../../../designer-canvas";
import { getSchemaByType } from "../../../dynamic-resolver";

export function useDesignerRules(designItemContext: DesignerItemContext, designerHostService?: DesignerHostService): UseDesignerRules {

    const dragAndDropRules = new UseTemplateDragAndDropRules();
    /**
     * 构造属性配置方法
     */
    function getPropsConfig(componentId: string) {
        return sectionPropertyConfigs;
    }
    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {
        const basalRule = useDragulaCommonRule().basalDragulaRuleForContainer(draggingContext, designerHostService);
        if (!basalRule) {
            return false;
        }
        const { canAccept } = dragAndDropRules.getTemplateRule(designItemContext, designerHostService);

        return canAccept;
    }

    function checkCanDeleteComponent(): boolean {
        const { canDelete } = dragAndDropRules.getTemplateRule(designItemContext, designerHostService);
        return canDelete;
    }

    function checkCanMoveComponent(): boolean {
        const { canMove } = dragAndDropRules.getTemplateRule(designItemContext, designerHostService);
        return canMove;
    }

    function hideNestedPaddingInDesginerView() {
        const { canMove, canDelete } = dragAndDropRules.getTemplateRule(designItemContext, designerHostService);
        return !canMove && !canDelete;
    }

    function addSectionToolbar(payload: MouseEvent) {
        if (payload) {
            payload.stopPropagation();
            payload.preventDefault();
        }
        const { schema } = designItemContext;
        if (!schema.toolbar || !schema.toolbar.buttons) {
            schema.toolbar = { buttons: [] };
        }
        const sectionToolbarItemSchema = getSchemaByType('section-toolbar-item') as ComponentSchema;
        sectionToolbarItemSchema.id = `section_toolbar_item_${Math.random().toString().slice(2, 6)}`;
        sectionToolbarItemSchema.appearance = { class: 'btn btn-secondary f-btn-ml' };

        schema.toolbar.buttons.push(sectionToolbarItemSchema);
    }
    /**
     * 获取自定义图标
     */
    function getCustomButtons() {
        const { schema } = designItemContext;
        const useFormSchema = designerHostService?.formSchemaUtils;
        if (schema.showHeader === false) {
            return;
        }
        // 当分组面板不包含卡片面板，且当前无按钮时，显示新增工具栏的图标
        const responseForm = useFormSchema.selectNode(schema, (childSchema) => childSchema.type === DgControl['response-form'].type);
        const toolbarButtons = schema.toolbar?.buttons || [];
        if (!responseForm && toolbarButtons.length === 0) {
            return [{
                id: 'appendToolbar',
                title: '新增工具栏',
                icon: 'f-icon f-icon-plus-circle text-white',
                onClick: (e) => addSectionToolbar(e)
            }];
        }

    }
    return {
        canAccepts,
        checkCanDeleteComponent,
        checkCanMoveComponent,
        hideNestedPaddingInDesginerView,
        getPropsConfig,
        getCustomButtons
    } as UseDesignerRules;
}
