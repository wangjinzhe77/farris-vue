/**
* Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
import { computed, defineComponent, inject, onMounted, ref, SetupContext } from 'vue';
import { SectionDesignProps, sectionDesignProps } from '../section.props';

import { useDesignerRules } from './use-designer-rules';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import getSectionHeaderRenderDesign from './section-header.design.component';
import renderSectionToolbarDesign from './section-toolbar.design.component';
import { getCustomClass } from '../../../common';
import { DesignerHostService } from '../../../designer-canvas/src/composition/types';

export default defineComponent({
    name: 'FSectionDesign',
    props: sectionDesignProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: SectionDesignProps, context: SetupContext) {
        const elementRef = ref();
        const sectionRef = ref();
        const sectionParentElementRef = ref<any>();
        const designerHostService = inject<DesignerHostService>('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);
        componentInstance.value.styles = 'display: inherit;';

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        const customClass = ref<string>(props.customClass);
        const enableAccordion = computed(() => props.enableAccordion);
        const expandStatus = ref(props.expandStatus);
        const toolbarPosition = computed(() => props.buttonPosition);
        const sectionMaxStatus = ref(false);

        const renderSectionHeader = getSectionHeaderRenderDesign(props, context, expandStatus, sectionRef, sectionParentElementRef, sectionMaxStatus, designItemContext);

        const sectionClass = computed(() => {
            const classObject = {
                'f-section': true,
                'f-section-accordion': enableAccordion.value === 'default',
                'f-state-collapse': (enableAccordion.value === 'default' || enableAccordion.value === 'custom') && !expandStatus.value,
                'f-section-custom-accordion': enableAccordion.value === 'custom',
                'f-section-fill': props.fill,
                'f-section-maximize': sectionMaxStatus.value,
                'f-utils-fill': true,
                'p-0': true
            } as Record<string, boolean>;
            return getCustomClass(classObject, customClass.value);
        });

        const contentClass = computed(() => {
            const contentClassObject = {
                'f-section-content': true,
                'drag-container': true
            } as Record<string, boolean>;
            return getCustomClass(contentClassObject, props.contentClass);
        });
        const contentStyle = computed(() => ({
            'visibility': expandStatus.value ? 'visible' : 'hidden'
        }));

        const extendClass = computed(() => {
            const contentClassObject = {
                'f-section-extend': true
            } as Record<string, boolean>;
            return getCustomClass(contentClassObject, extendClass.value);
        });

        function renderSectionExtend() {
            return context.slots.extend &&
                <div class={extendClass.value}>{context.slots.extend()}</div>;
        }

        function renderSectionContent() {
            return <div class={contentClass.value} ref={elementRef} data-dragref={`${designItemContext.schema.id}-container`} style={contentStyle.value}>
                {toolbarPosition.value === 'inContent' && renderSectionToolbarDesign(props, designItemContext)}
                {context.slots.default && context.slots.default()}</div>;
        }

        return () => {
            return (
                <div class={sectionClass.value} ref={sectionRef} style={props.customStyle}>
                    {renderSectionHeader()}
                    {renderSectionExtend()}
                    {renderSectionContent()}
                </div>
            );
        };
    }
});
