
import { computed, ref } from 'vue';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import FResponseToolbarDesign from '../../../response-toolbar/src/designer/response-toolbar.design.component';
import FDesignerInnerItem from '../../../designer-canvas/src/components/designer-inner-item.component';
import { SectionDesignProps } from '../section.props';

export default function (
    props: SectionDesignProps,
    designerItemContext: DesignerItemContext
) {
    const toolbarClass = computed(() => {
        return 'f-section-toolbar' + (props.buttonPosition === 'inHead' ? ' f-section-header--toolbar' : ' f-section-content--toolbar');
    });
    const items = ref(props.buttons);
    const toolbarSchema = ref(designerItemContext.schema.toolbar);
    const sectionComponentId = props.componentId;
    const sectionToolbarId = `${sectionComponentId}-toolbar`;
    const toolbarAlignment = computed(() => { return props.buttonPosition === 'inHead' ? 'right' : 'left'; });

    /**
     * 选中单个按钮事件
     */
    function onSelectionChange(schemaType: string, schemaValue: any, componentId: string, componentInstance: any) {
        designerItemContext.setupContext?.emit('selectionChange', schemaType, schemaValue, componentId, componentInstance);
    }

    return (
        <>{
            toolbarSchema.value ?
                <div class={toolbarClass.value} >
                    <div class="w-100" style="flex:1">
                        <FDesignerInnerItem v-model={toolbarSchema.value}
                            class="w-100 position-relative"
                            canAdd={true}
                            childType="section-toolbar-item"
                            childLabel="按钮"
                            contentKey="buttons"
                            id={sectionToolbarId}
                            onSelectionChange={onSelectionChange}
                            componentId={sectionComponentId}>
                            <FResponseToolbarDesign customClass='d-block' items={items.value} alignment={toolbarAlignment.value} componentId={sectionComponentId}></FResponseToolbarDesign>
                        </FDesignerInnerItem>
                    </div>
                </div> : ''
        }</>


    );
}
