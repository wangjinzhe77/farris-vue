import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { SetupContext, computed, Ref, nextTick } from 'vue';
import { useSectionMaxMin } from '../composition/use-max-min';
import { SectionDesignProps } from '../section.props';
import renderDesignSectionToolbar from './section-toolbar.design.component';
import { setPositionOfSelectedComponentBtnGroup } from '../../../designer-canvas/src/composition/designer-canvas-changed';

export default function (
    props: SectionDesignProps,
    context: SetupContext,
    expandStatus: Ref<boolean>,
    sectionRef: Ref<any>,
    sectionParentElementRef: Ref<any>,
    maxStatus: Ref<boolean>,
    designItemContext: DesignerItemContext,) {

    const toolbarPosition = computed(() => props.buttonPosition);
    const { onClickMaxMinIcon } = useSectionMaxMin(sectionRef, sectionParentElementRef, maxStatus);

    const shouldShowSubHeaderTitle = computed(() => {
        return !!props.subTitle;
    });

    const shouldShowMaximize = computed(() => {
        return props.enableMaximize;
    });
    const shouldShowAccordion = computed(() => {
        return props.enableAccordion !== '';
    });
    const headerClass = computed(() => {
        const headClassObject = {
            'f-section-header': true
        } as Record<string, boolean>;

        if (context.slots.header) {
            const customClassArray = props.headerClass.split(' ') as string[];
            customClassArray.reduce<Record<string, boolean>>((classObject, classString) => {
                classObject[classString] = true;
                return classObject;
            }, headClassObject);
        }

        return headClassObject;
    });

    function onClickCollapseExpandIcon(event: PointerEvent) {
        if (props.enableAccordion !== '') {
            event.stopPropagation();
            expandStatus.value = !expandStatus.value;

            nextTick(()=>{
                setPositionOfSelectedComponentBtnGroup(designItemContext.designerItemElementRef.value);
            });
        }
    }


    function renderMaxAccordionPaneal() {
        const collapseExpandIconClass = computed(() => {
            return {
                'btn': true,
                'f-btn-collapse-expand': true,
                'f-btn-mx': true,
                'f-state-expand': expandStatus.value
            } as Record<string, boolean>;
        });

        const maxMinIconClass = computed(() => {
            return {
                'f-icon': true,
                'f-icon-maximize': !maxStatus.value,
                'f-icon-minimize': maxStatus.value
            } as Record<string, boolean>;
        });

        return (
            <div class="f-max-accordion">
                {shouldShowMaximize.value ?
                    <span class={maxMinIconClass.value} onClick={onClickMaxMinIcon}></span> : ''
                }
                {shouldShowAccordion.value ?
                    <button class={collapseExpandIconClass.value} onClick={onClickCollapseExpandIcon}>
                        <span>{expandStatus.value ? props.collapseLabel : props.expandLabel}</span>
                    </button> : ''
                }

            </div>

        );
    }
    function renderHeaderTemplate() {
        return (
            context.slots.header ?
                <div class={headerClass.value}>
                    {context.slots.header()}
                </div>
                : ''
        );
    }
    function renderDefaultTitle() {
        return (
            <div class="f-title" onClick={onClickCollapseExpandIcon}>
                <h4 class="f-title-text">{props.mainTitle}</h4>
                {shouldShowSubHeaderTitle.value && <span>{props.subTitle}</span>}
            </div>
        );
    }


    function renderHeaderContent() {
        const headerContentClass = computed(() => {
            const classObject = {
                'f-content': true
            } as Record<string, boolean>;

            if (props.headerContentClass) {
                const customClassArray = props.headerContentClass.split(' ') as string[];
                customClassArray.reduce<Record<string, boolean>>((classObject, classString) => {
                    classObject[classString] = true;
                    return classObject;
                }, classObject);
            }
            return classObject;
        });

        return (
            context.slots.headerContent ?
                <div class={headerContentClass.value}>
                    {context.slots.headerContent()}
                </div>
                : ''
        );
    }

    function renderDefaultHeader() {
        return (
            <div class={headerClass.value}>
                {context.slots.headerTitle ? context.slots.headerTitle() : renderDefaultTitle()}
                {renderHeaderContent()}
                {toolbarPosition.value === 'inHead' && renderDesignSectionToolbar(props, designItemContext)}
                {(shouldShowMaximize.value || shouldShowAccordion.value) && renderMaxAccordionPaneal()}
            </div>
        );
    }
    return () => {
        return (
            props.showHeader && (
                context.slots.header ?
                    renderHeaderTemplate() :
                    renderDefaultHeader()

            )
        );
    };
}
