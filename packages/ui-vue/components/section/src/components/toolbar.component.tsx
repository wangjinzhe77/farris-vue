import { SetupContext, computed, ref, Ref } from 'vue';
import { SectionProps } from '../section.props';
import FResponseToolbar from '@farris/ui-vue/components/response-toolbar';

export default function (props: SectionProps, context: SetupContext) {

    const items = ref(props.buttons);
    const toolbarAlignment = computed(() => { return props.buttonPosition === 'inHead' ? 'right' : 'left'; });
    const toolbarClass = computed(() => {
        return 'f-section-toolbar' + (props.buttonPosition === 'inHead' ? ' f-section-header--toolbar' : ' f-section-content--toolbar');
    });

    const shouldShowToobar = computed(() => props.buttons && props.buttons.length > 0 && props.buttonPosition === 'inHead');


    function onClickToolbarItem(payload: any, itemId: string) {
        context.emit('click', payload, itemId);
    }
    return () => {
        return (
            shouldShowToobar.value && (
                <FResponseToolbar customClass={toolbarClass.value} items={items.value} onClick={onClickToolbarItem} alignment={toolbarAlignment.value}></FResponseToolbar>
            )
        );
    };
}
