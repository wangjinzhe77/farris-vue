/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver/src/props-resolver';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import rateSchema from './schema/rate.schema.json';
import propertyConfig from './property-config/rate.property-config.json';

const toolTipContents = ['很不满意', '不满意', '一般', '满意', '非常满意'];

export const rateProps = {
    /** 星星大小 */
    size: { type: String, default: 'large' },
    /** 启用半颗星模式 */
    enableHalf: { type: Boolean, default: false },
    /** 启用再次点击后清除 */
    enableClear: { type: Boolean, default: false },
    /** 只读 */
    disabled: { type: Boolean, default: false },
    /** 分制 */
    pointSystem: { type: Number, default: 5 },
    /** 星星亮色 */
    lightColor: { type: String, default: '' },
    /** 星星暗色（底色） */
    darkColor: { type: String, default: '' },
    /** 图案样式 */
    iconClass: { type: String, default: 'f-icon-star' },
    /** 星星个数 */
    numOfStar: { type: Number, default: 5 },
    /** 禁用单个星星的文字提示 */
    toolTipDisabled: { type: Boolean, default: true },
    /** 默认的满意度文案 */
    toolTipContents: { type: Array as PropType<string[]>, default: toolTipContents },
    /** 启用评分 */
    enableScore: { type: Boolean, default: true },
    /** 启用满意度 */
    enableSatisfaction: { type: Boolean, default: false },
    /**
     * 绑定值
     */
    value: { type: Number, default: 0 },

    modelValue: { type: Number, default: 0 }

} as Record<string, any>;

export type RateProps = ExtractPropTypes<typeof rateProps>;
export const propsResolver = createPropsResolver<RateProps>(rateProps, rateSchema, schemaMapper, schemaResolver, propertyConfig);
