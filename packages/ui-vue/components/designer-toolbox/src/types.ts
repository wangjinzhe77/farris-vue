export interface ToolboxItem {
    id: string;
    type: string;
    name: string;
    category: string;
    icon?: string;
    feature?: any;
    dependentParent?: boolean;
    hideInControlBox?: boolean;
    disable?: boolean;
    fieldType?: string;
    templateCategory?: string;
    updating?: boolean;
}

export interface ToolboxCategory {
    type: string;
    name: string;
    items: ToolboxItem[];
    hideInControlBox?: boolean;
    isHide?: boolean;
};
