import { read } from "fs";
import { ExtractPropTypes, PropType } from "vue";
import { createPropsResolver } from "../../dynamic-resolver";
import mappingEditorSchema from './schema/mapping-editor.schema.json';

export interface MappingDataSource {
    idField: string;
    dataSource:  Array<any>;
    textField: string;
    valueField: string;
    repositoryToken?: symbol;
}

export const mappingEditorProps = {
    fromData: { type: Object as PropType<MappingDataSource>, default: {}},
    toData: { type: Object as PropType<MappingDataSource>, default: {}},
    disabled: {type: Boolean, default: false},
    readonly: {type: Boolean, default: false},
    title: { type: String, default: '字段映射编辑器' },
    modalWidth: { type: Number, default: 600},
    modalHeight: { type: Number, default: 500},
    modelValue: { type: Object as PropType<Record<string, string>>, default: {}},
    editorParams: { type: Object, default: null },
    onMappingFieldsChanged: { type: Function , default: null}
} as Record<string, any>;

export type MappingEditorProps = ExtractPropTypes<typeof mappingEditorProps>;

export const propsResolver = createPropsResolver<MappingEditorProps>(mappingEditorProps, mappingEditorSchema);
