import { ref, defineComponent, computed, watch, inject } from "vue";
import { FButtonEdit } from '../../button-edit';
import { FDataGrid } from "../../data-grid";
import { FMessageBoxService } from '../../message-box';
import { mappingEditorProps, MappingEditorProps } from "./mapping-editor.props";
import { F_NOTIFY_SERVICE_TOKEN, FNotifyService } from "../../../components/notify";

export interface ComboTreeRepository {
    getData(params?: any): Promise<any[]>;
}

export default defineComponent({
    name: 'FMappingEditor',
    props: mappingEditorProps,
    emits: ['submitModal', 'update:modelValue'],
    setup(props: MappingEditorProps, context) {
        const buttonIcon = '<i class="f-icon f-icon-lookup"></i>';
        const datagridRef = ref();
        const mappingFields = ref(props.modelValue || {});
        const selectedItems: any = ref([]);

        const fromDataSource = ref(props.fromData.dataSource || []);
        const toDataSource = ref(props.toData.dataSource || []);

        const repository = inject<ComboTreeRepository>(props.fromData.repositoryToken);
        const toDataRepository = inject<ComboTreeRepository>(props.toData.repositoryToken);
        const notifyService: FNotifyService = inject(F_NOTIFY_SERVICE_TOKEN) as FNotifyService;
        
        const treeNodeStatus = (visualData: any) => {
            if (visualData.raw.$type !== 'SimpleField') {
                visualData.disabled = true;
            }
            return visualData;
        };

        const gridColumns = [
            { field: 'sourceField', title: '数据源字段', dataType: 'string', editor: {
                type: 'combo-tree',
                data: fromDataSource.value,
                idField: props.fromData.idField || 'id',
                textField: props.fromData.textField || 'name',
                valueField: props.fromData.valueField || 'id',
                formatter: props.fromData.formatter,
                editorParams: props.fromData.editorParams,
                editable: false,
                customRowStatus: treeNodeStatus
            }},
            { field: 'targetField', title: '目标字段', dataType: 'string', editor: {
                type: 'combo-tree',
                data: toDataSource.value,
                idField: props.toData.idField || 'id',
                textField: props.toData.textField || 'name',
                valueField: props.toData.valueField || 'id',
                formatter: props.toData.formatter,
                editorParams: props.toData.editorParams,
                editable: false,
                customRowStatus: treeNodeStatus
            } }
        ];

        function createId() {
            return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        }

        function getDataSource() {
            let mapFields = mappingFields.value || {};
            if (typeof mapFields === 'string') {
                mapFields = JSON.parse(mapFields);
            }

            return Object.keys(mapFields).map((key: string) => {
                return {
                    id: createId(),
                    sourceField: key,
                    targetField: mapFields[key]
                };
            });
        }

        const mappingFieldList = ref(getDataSource());

        const displayText = computed(() => {
            return `共 ${mappingFieldList.value?.length || 0} 项`;
        });

        function onAppendNew() {
            datagridRef.value.endEditCell();
            datagridRef.value.addNewDataItemAtLast();
        }

        function createNewDataItem() {
            return { sourceField: '', targetField: '', id: createId() };
        }

        function onRemove() {
            if (selectedItems.value.length) {
                datagridRef.value.endEditCell();
                const itemIndex = mappingFieldList.value.findIndex((item: any) => item.id === selectedItems.value[0].id);
                if (itemIndex> -1) {
                    selectedItems.value = [];
                    datagridRef.value.removeDataItem(itemIndex + 1);
                }
            } else {
                notifyService.warning('请选择要删除的映射字段！');
            }
        }

        function onClearAll() {
            FMessageBoxService.question('确定要清空所有映射字段吗？', '', () => {
                mappingFields.value = {};
                mappingFieldList.value = [];
                datagridRef.value.updateDataSource([]);
            }, () => { });
        }

        function convertArrayToObject() {
            const mappingFieldObject: Record<string, string> = {};
            mappingFieldList.value.forEach((item: any) => {
                if (item.sourceField && item.targetField) {
                    mappingFieldObject[item.sourceField] = item.targetField;
                }
            });
            return mappingFieldObject;
        }

        watch(() => props.modelValue, (newValue) => {
            mappingFields.value = newValue;
            mappingFieldList.value = getDataSource();
        });

        function mappingFieldValidate() {
            const emptyFields = mappingFieldList.value.filter((item: any) => !item.sourceField || !item.targetField);
            return emptyFields.length;
        }
        
        const modalOptions = {
            title: props.title,
            fitContent: false,
            height: props.modalHeight,
            width: props.modalWidth,
            minWidth: 300,
            minHeight: 200,
            showMaxButton: true,
            buttons: [
                {
                    name: 'cancel',
                    text: '取消',
                    class: 'btn btn-secondary',
                    handle: ($event: MouseEvent) => {
                        return true;
                    }
                },
                {
                    name: 'accept',
                    text: '确定',
                    class: 'btn btn-primary',
                    handle: ($event: MouseEvent) => {
                        datagridRef.value.endEditCell();

                        if (mappingFieldValidate()) {
                            notifyService.warning('请将映射字段填写完整！');
                            return false;
                        }

                        const mappingFieldObject = convertArrayToObject();
                        mappingFields.value = mappingFieldObject;
                        context.emit('submitModal', mappingFieldObject);
                        context.emit('update:modelValue', mappingFieldObject);
                        if (props.onMappingFieldsChanged && typeof props.onMappingFieldsChanged === 'function') {
                            props.onMappingFieldsChanged(JSON.stringify(mappingFieldObject));
                        }
                        return true;
                    }
                }
            ],
            resizeable: true,
            draggable: true,
            closedCallback: ($event, from) => {
                if ($event.target.name === 'cancel' || from === 'icon' || from === 'esc') {
                    mappingFields.value = props.modelValue;
                    mappingFieldList.value = getDataSource(); // 重置数据
                    selectedItems.value = [];
                }
            }
        };

        function onSelectionChange(items: any[]) {
            selectedItems.value = items;
        }

        function onBeforeOpen() {

            const helpId = props.editorParams?.propertyData?.helpId;
            if (!helpId) {
                notifyService?.warning({message: '请先配置数据源！', position: 'top-center'});
                return false;
            }


            const promises: Array<Promise<any>> = [];
            const fromDataPromise = repository? repository.getData(props.editorParams): Promise.resolve(fromDataSource.value);
            promises.push(fromDataPromise);

            const toDataPromise = toDataRepository? toDataRepository.getData(props.editorParams): Promise.resolve(toDataSource.value);
            promises.push(toDataPromise);

            return Promise.all(promises).then(([fromData, toData]) => {
                gridColumns[0].editor.data = fromData;
                gridColumns[1].editor.data = toData;
                return true;
            });
        }

        // const canRemove = computed(() => {
        //     return selectedItems?.value.length > 0;
        // });

        // const canClear = computed(() => {
        //     return mappingFieldList.value.length > 0;
        // });

        return () => {
            return (<FButtonEdit
                v-model={displayText.value}
                editable={false}
                disabled={props.disabled}
                readonly={props.readonly}
                inputType={"text"}
                enableClear={false}
                buttonContent={buttonIcon}
                buttonBehavior={"Modal"}
                modalOptions={modalOptions}
                beforeOpen={onBeforeOpen}
            >
                <div class="h-100 d-flex flex-column">
                    <div class="px-2">
                        <button class="btn btn-primary" onClick={onAppendNew}>新增</button>
                        <button class="btn btn-secondary ml-2" onClick={onRemove} >删除</button>

                        <button class="btn btn-secondary ml-3" onClick={onClearAll}>清空</button>
                    </div>
                    <div class=" f-utils-fill p-2"><FDataGrid ref={datagridRef}
                        pagination={{ enable: false }}
                        summary={{ enable: false }}
                        columnOption={{ fitColumns: true }}
                        editOption={{ editMode: 'cell' }}
                        rowNumber={false}
                        showBorder={true}
                        editable="true"
                        fit={true}
                        columns={gridColumns}
                        data={mappingFieldList.value}
                        newDataItem={createNewDataItem}
                        onSelectionChange={onSelectionChange}
                    ></FDataGrid></div></div>
            </FButtonEdit>);
        };
    }
});
