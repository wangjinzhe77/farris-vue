import { Ref, SetupContext, computed, ref } from "vue";
import BigNumber from "bignumber.js";
import { UseNumber } from "./types";
import { NumberRangeProps } from "../number-range.props";

export function useNumber(props: NumberRangeProps, context: SetupContext): UseNumber {
    const precision = computed(() => Number(props.precision) || 0);

    /**
     * 基于精度参数修改toFixed方法
     * @param value
     * @returns
     */
    function toFixed(value: BigNumber | number) {
        return value.toFixed(precision.value);
    }

    /**
     * 判断输入框中的值是否为空
     * @param val 输入值
     * @returns 返回是否时空值的判断结果
     */
    function isEmpty(value: string | number | undefined): boolean {
        return isNaN(value as number) || value === null || value === undefined || value === '';
    }

    /**
     * 最值校验
     * @param bn
     * @returns
     */
    function getValidNumberObject(numberObject: BigNumber): BigNumber {
        const maxValue = !isEmpty(props.max) ? new BigNumber(String(props.max), 10) : null;
        const minValue = !isEmpty(props.min) ? new BigNumber(String(props.min), 10) : null;
        const validNumberObject = (maxValue && numberObject.gt(maxValue)) ? maxValue :
            ((minValue && numberObject.lt(minValue)) ? minValue : numberObject);
        return validNumberObject;
    }

    /**
     * 确保开始数字比结束数字小
     * @param value 输入框的值
     * @param beginModelValue 开始数字
     * @param endModelValue 结束数字
     * @param isBeginTextBox 当前编辑的是否为开始数字
     * @returns
     */
    function getValidNumberInRange(value: any, beginModelValue: Ref<string>, endModelValue: Ref<string>, isBeginTextBox: boolean): any {
        const numberObject = new BigNumber(value, 10);
        const maxModelValue = !isEmpty(endModelValue && endModelValue.value) ? new BigNumber(String(endModelValue.value), 10) : null;
        const minModelValue = !isEmpty(beginModelValue && beginModelValue.value) ? new BigNumber(String(beginModelValue.value), 10) : null;

        let validNumberObject;
        if (isBeginTextBox) {
            validNumberObject = (maxModelValue && numberObject.gt(maxModelValue)) ? maxModelValue : numberObject;
        } else {
            validNumberObject = (minModelValue && numberObject.lt(minModelValue)) ? minModelValue : numberObject;
        }

        return validNumberObject.toString();
    }
    /**
     * 获取实际数值
     * @param val
     * @returns
     */
    function getRealValue(value: any, needValid: boolean = true) {
        if (props.parser) {
            if (!isNaN(Number(value))) {
                return value;
            }
            return props.parser(value);
        }
        let numberObject = new BigNumber(value, 10);
        if(needValid) {
            numberObject = getValidNumberObject(numberObject);
        }
        if (numberObject.isNaN()) {
            if (props.nullable) {
                return null;
            }
            const minBigNum = new BigNumber('' + props.min, 10);
            const maxBigNum = new BigNumber('' + props.max, 10);
            if (!minBigNum.isNaN()) {
                numberObject = minBigNum;
            } else if (!maxBigNum.isNaN()) {
                numberObject = maxBigNum;
            } else {
                return 0;
            }
        }
        const fixedNumberString = toFixed(numberObject);
        return fixedNumberString;
    }

    return { getRealValue, isEmpty, precision, getValidNumberObject, getValidNumberInRange };
}
