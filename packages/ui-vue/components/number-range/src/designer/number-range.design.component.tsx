/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, ref, onMounted, watch, computed, inject } from 'vue';
import type { SetupContext } from 'vue';
import { numberRangeProps, NumberRangeProps } from '../number-range.props';
import { useNumber } from '../composition/use-number';
import { useFormat } from '../composition/use-format';
import { useSpinner } from '../composition/use-spinner';
import { useTextBox } from '../composition/use-text-box';
import getNumberTextBoxRender from '../components/text-box.component';
import getSpinnerRender from '../components/spinner.component';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useNumberRangeDesignerRules } from './use-design-rules';

export default defineComponent({
    name: 'FNumberRangeDesign',
    props: numberRangeProps,
    emits: ['valueChange', 'blur', 'focus', 'click', 'input', 'beginValueChange', 'endValueChange'] as
        | (string[] & ThisType<void>)
        | undefined,
    setup(props: NumberRangeProps, context: SetupContext) {
        const beginModelValue = ref(props.beginValue);
        const beginDisplayValue = ref('');
        const endModelValue = ref(props.endValue);
        const endDisplayValue = ref('');
        const useNumberComposition = useNumber(props, context);
        const useFormatComposition = useFormat(props, context, useNumberComposition);
        const benginValueChangedCallback = (numberValue: string | number) => {
            context.emit('beginValueChange', numberValue);
        };
        const elementRef = ref();
        const designerHostService = inject('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useNumberRangeDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);
        const useBeginValueSpinnerComposition = useSpinner(
            props,
            context,
            beginDisplayValue,
            beginModelValue,
            useFormatComposition,
            useNumberComposition,
            benginValueChangedCallback,
            beginModelValue,
            endModelValue,
            true
        );
        const useBeginValueTextBoxComposition = useTextBox(
            props,
            context,
            beginDisplayValue,
            beginModelValue,
            useFormatComposition,
            useNumberComposition,
            useBeginValueSpinnerComposition,
            benginValueChangedCallback,
            beginModelValue,
            endModelValue,
            true
        );
        const renderBeginValueSpinner = getSpinnerRender(props, context, useBeginValueSpinnerComposition);
        const renderBeginValueNumberTextBox = getNumberTextBoxRender(props, context, useBeginValueTextBoxComposition, true);
        const endValueChangedCallback = (numberValue: string | number) => {
            context.emit('endValueChange', numberValue);
        };
        const useEndValueSpinnerComposition = useSpinner(
            props,
            context,
            endDisplayValue,
            endModelValue,
            useFormatComposition,
            useNumberComposition,
            endValueChangedCallback,
            beginModelValue,
            endModelValue,
            false
        );
        const useEndValueTextBoxComposition = useTextBox(
            props,
            context,
            endDisplayValue,
            endModelValue,
            useFormatComposition,
            useNumberComposition,
            useEndValueSpinnerComposition,
            endValueChangedCallback,
            beginModelValue,
            endModelValue,
            false
        );
        const renderEndValueSpinner = getSpinnerRender(props, context, useEndValueSpinnerComposition);
        const renderEndValueNumberTextBox = getNumberTextBoxRender(props, context, useEndValueTextBoxComposition, false);

        const shouldShowSpinner = computed(() => !props.disabled && !props.readonly && props.showButton);

        return () => (
            <div ref={elementRef} class="input-group  number-range f-cmp-number-spinner">
                <div class="form-control input-container">
                    <div class="sub-input-group">
                        {renderBeginValueNumberTextBox()}
                        {shouldShowSpinner.value && renderBeginValueSpinner()}
                    </div>
                    <span class="spliter">~</span>
                    <div class="sub-input-group">
                        {renderEndValueNumberTextBox()}
                        {shouldShowSpinner.value && renderEndValueSpinner()}
                    </div>
                </div>
            </div>
        );
    }
});
