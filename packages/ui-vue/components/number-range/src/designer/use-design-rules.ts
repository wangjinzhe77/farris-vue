 
import { UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema, DesignerComponentInstance, DesignerItemContext } from "../../../designer-canvas/src/types";
import { NumberRangeProperty } from "../property-config/number-range.property-config";

export function useNumberRangeDesignerRules(designItemContext: DesignerItemContext, designerHostService): UseDesignerRules {

    const schema = designItemContext.schema as ComponentSchema;

    // 构造属性配置方法
    function getPropsConfig(componentId: string, componentInstance: DesignerComponentInstance) {
        const inputGroupProps = new NumberRangeProperty(componentId, designerHostService);
        return inputGroupProps.getPropertyConfig(schema, componentInstance);
    }

    return { getPropsConfig } as UseDesignerRules;

}
