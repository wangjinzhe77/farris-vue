 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { schemaResolver } from './schema/schema-resolver';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import numberRangeSchema from './schema/number-range.schema.json';

type TextAlignType = 'left' | 'right' | 'center' | 'start' | 'end' | 'justify';
export const numberRangeProps = {
    /**
     * 组件标识
     */
    id: String,
    /**
     * 自动补全小数
     */
    autoDecimal: { type: Boolean, default: true },
    /**
     * 下限placeholder---未实现
     */
    beginPlaceHolder: { type: String, default: '请输入开始数字' },
    /**
     * 下限值
     */
    beginValue: { type: [Number, String], default: '' },
    /**
     * 启用大数支持
     */
    bigNumber: { type: Boolean, default: false },
    /**
     * 允许为空
     */
    nullable: { type: Boolean, default: false },
    /**
     * 小数点符号
     */
    decimalSeparator: { type: String, default: '.' },
    /**
     * 是否禁用
     */
    disabled: { type: Boolean, default: false },

    /**
     * 是否可编辑
     */
    editable: { type: Boolean, default: true },
    /**
     * 上限placeholder---未实现
     */
    endPlaceHolder: { type: String, default: '请输入结束数字' },
    /**
     * 上限值
     */
    endValue: { type: [Number, String], default: '' },
    /**
     * 格式化 formatter 和 parser 必须同时存在
     * formatter: (val: number) => string;
     * parser: (val: string | number) => number;
     */
    formatter: { type: Function },
    /**
     * 千分位符号
     */
    groupSeparator: { type: String, default: ',' },
    /**
     * 使用千分位时，每组显示的字符数
     */
    groupSize: { type: Number, default: 3 },
    /**
     * 最大值
     */
    max: { type: [Number, String] },
    /**
     * 最小值
     */
    min: { type: [Number, String] },

    parser: { type: Function },
    /**
     * 空白提示文本
     */
    placeholder: { type: String, default: '请输入数字' },
    /**
     * 精度
     */
    precision: { type: Number, default: 0 },
    /**
     * 前缀
     */
    prefix: { type: String, default: '' },
    /**
     * 是否只读
     */
    readonly: { type: Boolean, default: false },
    /**
     * 间隔符---未实现
     */
    separator: { type: String, default: '~' },
    /**
     * 是否显示加减按钮
     */
    showButton: { type: Boolean, default: true },
    /**
     * 显示0值
     */
    showZero: { type: Boolean, default: true },
    /**
     * up or down 步长
     */
    step: { type: Number, default: 1 },
    /**
     * 后缀
     */
    suffix: { type: String, default: '' },
    /**
     * 文本方向
     */
    textAlign: { type: String as PropType<TextAlignType>, default: 'left' },

    /**
     * 是否使用千分值
     */
    useThousands: { type: Boolean, default: true }
} as Record<string, any>;

export type NumberRangeProps = ExtractPropTypes<typeof numberRangeProps>;

export const propsResolver = createPropsResolver<NumberRangeProps>(numberRangeProps, numberRangeSchema, schemaMapper, schemaResolver);
