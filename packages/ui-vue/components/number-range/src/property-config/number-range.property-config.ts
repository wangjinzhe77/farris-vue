import { InputBaseProperty } from "../../../property-panel/src/composition/entity/input-base-property";

export class NumberRangeProperty extends InputBaseProperty {

    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }
    getEditorProperties(propertyData) {
        return this.getComponentConfig(propertyData, { "type":"number-range" }, {
            "editable": {
                "description": "",
                "title": "允许编辑",
                "type": "boolean"
            }
        });
    }
}
