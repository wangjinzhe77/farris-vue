 
import { SetupContext, computed } from 'vue';
import { UseTextBox } from '../composition/types';
import { NumberRangeProps } from '../number-range.props';

export default function (props: NumberRangeProps, context: SetupContext, useTextBoxComposition: UseTextBox, isBeginTextBox: boolean) {
    const { onBlurTextBox, onFocusTextBox, onInput, onKeyDown, textBoxValue } = useTextBoxComposition;
    const placeholder = computed(() => isBeginTextBox ? props.beginPlaceHolder : props.endPlaceHolder);

    const numberTextBoxClass = computed(() => ({
        'form-control': true,
        'sub-input': true
    }));

    const numberTextBoxStyle = computed(() => {
        const styleObject = {
            'text-align': props.textAlign
        } as Record<string, any>;
        return styleObject;
    });

    function onChange($event: Event) {
        $event.stopPropagation();
    }

    return () => {
        return (
            <input
                class={numberTextBoxClass.value}
                style={numberTextBoxStyle.value}
                type="text"
                value={textBoxValue.value}
                disabled={props.disabled}
                readonly={props.readonly || !props.editable}
                placeholder={placeholder.value}
                onBlur={onBlurTextBox}
                onChange={onChange}
                onFocus={onFocusTextBox}
                onInput={onInput}
                onKeydown={onKeyDown}
            />
        );
    };
}
