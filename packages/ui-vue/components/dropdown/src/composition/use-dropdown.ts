/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use props file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ref, onMounted, SetupContext, Ref } from 'vue';
import { DropdownProps } from '../dropdown.props';

export function useDropdown(props: DropdownProps, context: SetupContext) {

    let menu: any;
    const show = ref(props.show);
    const dropdownRef = ref();
    const dropdownMenuRef = ref();
    const clickEventRef = ref();

    const commonGutter = 10;
    onMounted(() => {
        // menu = document.querySelector('.dropdown-menu');
        menu = dropdownMenuRef.value;
    });

    /**
     * 获取页面中body下所有元素的zIndex, 并返回下个浮层的新zindex
     */
    const getFloatingLayerIndex = (upperLayers = 1) => {

        const selectors = [
            'body>.f-datagrid-settings-simple-host',
            'body>div',
            'body>farris-dialog>.farris-modal.show',
            'body>.farris-modal.show',
            'body>farris-filter-panel>.f-filter-panel-wrapper',
            'body .f-sidebar-show>.f-sidebar-main',
            'body>.popover.show',
            'body>filter-row-panel>.f-datagrid-filter-panel',
            'body>.f-section-maximize'
        ];

        const overlays = Array.from(document.body.querySelectorAll(selectors.join(','))).filter(n => n).map(n => {
            const { display, zIndex } = window.getComputedStyle(n);
            if (display === 'none') {
                return 0;
            }
            return parseInt(zIndex, 10);
        }).filter(n => n);
        let maxZindex = Math.max(...overlays);
        if (maxZindex < 1040) {
            maxZindex = 1040;
        }
        return maxZindex + upperLayers;
    };

    const calculateMenuPanelPosition = (dropdownBtnGroup: HTMLElement, childMenuPanel: HTMLElement) => {
        // dropdown 指下拉指令所在的按钮，通过getNativeElement获取，下拉指令所在父元素，比如btn-group
        const { height: dpButtonGroupHeight, left: dpButtonGroupLeft,
            top: dpButtonGroupTop, width: dpButtonGroupWidth } = dropdownBtnGroup.getBoundingClientRect();
        // 获取菜单的宽度和高度
        const { width: menuWidth, height: menuHeight, top: menuTop } = childMenuPanel.getBoundingClientRect();
        const isSubmenu = dropdownBtnGroup.className.indexOf('dropdown-submenu') > -1
            || dropdownBtnGroup.closest('.dropdown-submenu') || dropdownBtnGroup.classList.contains('dropright');
        if (!isSubmenu) {
            const { marginTop: menuMarginTop, marginBottom: menuMarginBottom } = getComputedStyle(childMenuPanel);
            let maxHeight = 0;
            const menuYAxisMargin = Math.ceil(parseFloat(menuMarginTop)) + Math.ceil(parseFloat(menuMarginBottom));
            let realTop = dpButtonGroupTop + dpButtonGroupHeight;
            let realLeft = dpButtonGroupLeft;
            if (window.innerHeight - realTop - menuYAxisMargin < menuHeight) {
                realTop = dpButtonGroupTop - menuHeight;
                if (realTop < 0) {
                    // 当前的界面容不下menu上的按钮，限制menu的高度
                    if (window.innerHeight - dpButtonGroupTop - dpButtonGroupHeight > dpButtonGroupTop) {
                        // 底部的高度>上面的高度
                        realTop = dpButtonGroupTop + dpButtonGroupHeight;
                        maxHeight = window.innerHeight - realTop - menuYAxisMargin - commonGutter;
                    } else {
                        realTop = commonGutter;
                        maxHeight = dpButtonGroupTop - realTop - menuYAxisMargin;
                    }
                }
            }

            if (window.innerWidth - dpButtonGroupLeft < menuWidth) {
                // 朝右空间超出 菜单宽度
                if (window.innerWidth - dpButtonGroupLeft < dpButtonGroupLeft + dpButtonGroupWidth) {
                    // 如果朝左空间比朝右空间大
                    realLeft = dpButtonGroupLeft - menuWidth + dpButtonGroupWidth;
                }
            }

            document.body.append(childMenuPanel);
            // this.dropdown.appendMenuEl(childMenuPanel);
            childMenuPanel.style.cssText = `position:fixed;bottom:unset;left:${realLeft}px
             !important;top:${realTop}px !important;right: unset;${maxHeight ? 'max-height:' + maxHeight + 'px;overflow-y:auto;' : ''}`;
            if (maxHeight) {
                childMenuPanel.className += ' dropdown-menu-maxheight';
            }

            childMenuPanel.style.zIndex = getFloatingLayerIndex().toString();

        } else {
            // 朝右剩余空间
            const childContainerWidth = window.innerWidth - dpButtonGroupLeft - dropdownBtnGroup.offsetWidth;
            // 朝下剩余高度
            const childContainerHeight = window.innerHeight - dpButtonGroupTop;
            // 获取position
            const { position: menuPosition } = getComputedStyle(childMenuPanel);
            if (menuPosition === 'fixed') {
                // 如果处于滚动中
                let menuRealLeft = dpButtonGroupLeft + dropdownBtnGroup.offsetWidth;
                if (childMenuPanel.offsetWidth > childContainerWidth) {
                    // 宽度不够展示，不处理滚动问题，选择一个最大空间处理
                    if (dpButtonGroupLeft > childContainerWidth) {
                        // 如果，朝左剩余空间比朝右剩余空间大
                        menuRealLeft = dpButtonGroupLeft - menuWidth;
                    }
                }
                childMenuPanel.style.left = menuRealLeft + 'px';
                childMenuPanel.style.right = 'auto';
                if (window.innerHeight - 2 * commonGutter < childMenuPanel.offsetHeight) {
                    // 菜单高度超出容器高度，出现滚动条
                    childMenuPanel.style.top = commonGutter + 'px';
                    childMenuPanel.style.bottom = commonGutter + 'px';
                    childMenuPanel.style.maxHeight = window.innerHeight - 2 * commonGutter + 'px';
                    childMenuPanel.style.overflowY = 'auto';
                    childMenuPanel.className += ' dropdown-menu-maxheight';
                } else {
                    // 不出现滚动条
                     
                    if (childContainerHeight < childMenuPanel.offsetHeight) {
                        // 朝下剩余高度小于菜单高度，从按钮位置朝下放置菜单展示不开，则不限定顶部高度
                        childMenuPanel.style.top = 'auto';
                        childMenuPanel.style.bottom = commonGutter + 'px';
                    } else {
                        // 从按钮位置朝下放置菜单
                        childMenuPanel.style.top = dpButtonGroupTop + 'px';
                        childMenuPanel.style.bottom = 'auto';
                    }
                }
            } else {
                // 没有处于滚动中，定位属于absolute
                // 依托于菜单面板，在面板当前位置的左或者右翻转
                if (childMenuPanel.offsetWidth > childContainerWidth) {
                    // 菜单宽度大于朝右剩余宽度，则反向
                    const l = -menuWidth;
                    childMenuPanel.style.left = l + 'px';
                }
                if (window.innerHeight - 2 * commonGutter < childMenuPanel.offsetHeight) {
                    // 菜单高度超出容器高度，出现滚动条
                    // top位置从按钮位置开始算0，上移为负值
                    childMenuPanel.style.top = -1 * (dpButtonGroupTop - commonGutter) + 'px';
                    childMenuPanel.style.bottom = 'auto';
                    childMenuPanel.style.maxHeight = window.innerHeight - 2 * commonGutter + 'px';
                    childMenuPanel.style.overflowY = 'auto';
                    childMenuPanel.className += ' dropdown-menu-maxheight';
                } else {
                     
                    if (childContainerHeight < childMenuPanel.offsetHeight) {
                        // 朝下剩余高度小于菜单高度，top位置从按钮位置开始算0，上移为负值
                        childMenuPanel.style.top = childContainerHeight
                            - childMenuPanel.offsetHeight - commonGutter + 'px';
                        childMenuPanel.style.bottom = 'auto';
                    }
                }
            }

        }
    };
    /**
     *  计算下拉面板位置
     * @param el
     */
    const resetPosition = () => {
        calculateMenuPanelPosition(dropdownRef.value, dropdownMenuRef.value);
    };

    /** 点击元素之外  列表项消失 */
    const closeDropMenu = () => {
        if (props.hover) {
            return;
        }
        show.value = false;
        // context.emit('visibleChange');
        // 解除事件绑定
        document.removeEventListener('click', closeDropMenu);
        menu.removeEventListener('click', closeDropMenu);
        document.removeEventListener('scroll', closeDropMenu);
        menu.removeEventListener('scroll', closeDropMenu);
    };

    /** 点击列表项显示与否 */
    const showDropMenu = (e?: Event) => {
        if (props.hover || props.disabled) {
            return;
        }
        e?.stopPropagation();
        if (!show.value) {
            // 下拉元素要展示的状态 重新设置位置
            setTimeout(() => {
                resetPosition();
            });
        }
        show.value = !show.value;
        // context.emit('visibleChange');
        document.addEventListener('click', closeDropMenu);
        document.addEventListener('scroll', closeDropMenu);
        if (!props.hideOnClick) {
            menu.addEventListener('click', (event: any) => {
                event.stopPropagation();
            });
            menu.addEventListener('scroll', (event: any) => {
                event.stopPropagation();
            });
        }
    };
    /** 弹出事件 */
    const selectItem = (model: any) => {
        context.emit('select', model);
    };

    /** hover 实现 */
    const hoverDropdown = (e?: MouseEvent) => {
        if (!props.hover) {
            return;
        }
        if (!show.value) {
            // 下拉元素要展示的状态 重新设置位置
            setTimeout(() => {
                // cacheSize(e);
            });
        }
        show.value = !show.value;
        // context.emit('visibleChange');
    };

    const leftButtonClick = () => {
        // context.emit('leftClick');
    };
    const resolveSize = (size: any) => {
        const regex = /px|em|rem|pt|%/;
        // 说明是字符串
        return regex.test(size)
            ? `${parseInt(size, 10)}${size.match(regex)[0]}`
            : `${size}px`;
    };

    return {
        show,
        dropdownRef,
        dropdownMenuRef,
        clickEventRef,
        showDropMenu,
        hoverDropdown,
        leftButtonClick,
        closeDropMenu,
        selectItem,
        resolveSize,
    };
}
