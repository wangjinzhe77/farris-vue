 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { defineComponent, SetupContext, Teleport, Transition, ref } from 'vue';
import { dropdownProps, DropdownProps } from './dropdown.props';
import { useDropdown } from './composition/use-dropdown';
import FDropdownItem from './dropdown.item.component';

export default defineComponent({
    name: 'FDropdown',
    props: dropdownProps,
    emits: ['select'] as (string[] & ThisType<void>) | undefined,
    setup(props: DropdownProps, context: SetupContext) {
        // 根节点  触发元素以及下拉面板DOM ref
        const model = ref(props.model);
        const { show,
            dropdownMenuRef,
            dropdownRef,
            clickEventRef,
            showDropMenu,
            hoverDropdown,
            leftButtonClick, closeDropMenu, selectItem, resolveSize } = useDropdown(props, context);

        function selectHandler(value: any) {
            context.emit('select', value);
        }

        return () => (
            <div ref={dropdownRef}>
                <div
                    class={[
                        'farris-dropdown',
                        'btn-group',
                        { 'dropup': props.position === 'top' },
                        { 'dropdown': props.position === 'bottom' },
                        { 'dropleft': props.position === 'left' },
                        { 'dropright': props.position === 'right' },
                    ]}
                >
                    {props.splitButton && (
                        <span
                            class={[
                                'btn',
                                { 'dropdown-item': props.nest },
                                { 'btn-lg': props.size === 'large' },
                                { 'btn-sm': props.size === 'small' },
                                { 'btn-primary': props.type === 'primary' },
                                { 'btn-success': props.type === 'success' },
                                { 'btn-warning': props.type === 'warning' },
                                { 'btn-danger': props.type === 'danger' },
                                { 'btn-info': props.type === 'info' },
                            ]}
                            style="width:100%"
                            onClick={leftButtonClick}
                        >
                            {props.title}
                        </span>
                    )}
                    <span
                        ref={clickEventRef}
                        class={[
                            'dropdown-toggle',
                            { btn: !props.nest },
                            { 'dropdown-item': props.nest },
                            { 'dropdown-toggle-split': props.splitButton },
                            { 'btn-lg': props.size === 'large' },
                            { 'btn-sm': props.size === 'small' },
                            { 'btn-primary': props.type === 'primary' },
                            { 'btn-success': props.type === 'success' },
                            { 'btn-warning': props.type === 'warning' },
                            { 'btn-danger': props.type === 'danger' },
                            { 'btn-info': props.type === 'info' },
                            { 'btn-secondary': props.type === 'secondary' },
                            { 'btn-link': props.type === 'link' },
                            { 'disabled': props.disabled },
                        ]}
                        style="width:100%"
                        onClick={showDropMenu}
                        onMouseenter={hoverDropdown}
                        onMouseleave={hoverDropdown}
                    >
                        {/* 分行线 */}
                        <span class="sr-only" v-show={props.splitButton}></span>
                        {props.splitButton ? '' : props.title}
                        {/* 图标下拉 */}
                        <span class={`f-icon ${props.iconClass.toString()}`}
                            v-show={props.iconClass}></span>
                    </span>
                    <Teleport to="body">
                        <div ref={dropdownMenuRef} class={`dropdown-menu${show.value ? ' show' : ''}`}>
                            <>
                                {model.value.map(({ label, value, disabled, active, divide }: any) => (
                                    <FDropdownItem
                                        value={value.toString()}
                                        label={label}
                                        disabled={disabled}
                                        active={active}
                                        divide={divide}
                                        onSelect={($event: any) => selectHandler($event)}
                                    ></FDropdownItem>
                                ))}
                            </>
                        </div>
                    </Teleport>
                </div>
            </div >
        );
    },
});
