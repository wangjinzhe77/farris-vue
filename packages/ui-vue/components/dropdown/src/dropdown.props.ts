 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { DropdownItem } from './types/types';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaResolver } from './schema/schema-resolver';
import { schemaMapper } from './schema/schema-mapper';
import dropdownSchema from './schema/dropdown.schema.json';
import propertyConfig from './property-config/dropdown.property-config.json';

export const dropdownProps = {

    /** 默认展开或折叠 */
    show: { type: Boolean, default: false },

    /** 下拉按钮是否禁用 */
    disabled: { type: Boolean, default: false },

    /** 下拉按钮对应文字 */
    title: { type: String, default: '下拉示例' },

    /** 下拉按钮大小 */
    size: { type: String, default: '' },

    /** 下拉按钮类型 */
    type: { type: String, default: 'primary' },

    /** 图标样式 */
    iconClass: { type: String, default: '' },

    /** 下拉框内容是否被选中 */
    active: { type: Boolean, default: false },

    /** 下拉按钮是否分开展示 */
    splitButton: { type: Boolean, default: false },

    /** 下拉框展示方向 */
    position: { type: String, default: 'bottom' },

    /** 下拉框内容 */
    model: {
        type: Array as PropType<DropdownItem[]>,

        default: [
            { label: '项目一', value: 'XM1' },
            { label: '项目二', value: 'XM2' },
            { label: '项目三', value: 'XM3' }
        ]
    },

    onSelect: { type: Function, default: () => { } }

} as Record<string, any>;

export type DropdownProps = ExtractPropTypes<typeof dropdownProps>;

export const propsResolver = createPropsResolver<DropdownProps>(dropdownProps, dropdownSchema, schemaMapper, schemaResolver, propertyConfig);
