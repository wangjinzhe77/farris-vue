 
import type { App } from 'vue';
import Dropdown from './src/dropdown.component';
import { propsResolver } from './src/dropdown.props';
import DropdownDesign from './src/designer/dropdown.design.component';

export * from './src/types/types';
export * from './src/dropdown.props';

export { Dropdown };

export default {
    install(app: App): void {
        app.component(Dropdown.name as string, Dropdown);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>): void {
        componentMap.dropdown = Dropdown;
        propsResolverMap.dropdown = propsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
        componentMap.dropdown = DropdownDesign;
        propsResolverMap.dropdown= propsResolver;
    }
};
