 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App } from 'vue';
import FVerifyDetail from './src/verify-detail.component';
import FVerifyDetailService from './src/verify-detail.service';
import { propsResolver } from './src/verify-detail.props';
import FVerifyDetailDesign from './src/designer/verify-detail.design.component';
import { ValidatorListItem, VerifyTypeItem } from './src/composition/type';

export * from './src/verify-detail.props';

export { FVerifyDetail, FVerifyDetailService };
export type { ValidatorListItem, VerifyTypeItem };

export default {
    install(app: App): void {
        app.component(FVerifyDetail.name as string, FVerifyDetail);
        app.provide('FVerifyDetailService', FVerifyDetailService);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>): void {
        componentMap['verify-detail'] = FVerifyDetail;
        propsResolverMap['verify-detail'] = propsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
        componentMap['verify-detail'] = FVerifyDetailDesign;
        propsResolverMap['verify-detail'] = propsResolver;
    }
};
