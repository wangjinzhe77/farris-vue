 
import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver/src/props-resolver';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import verifyDetailSchema from './schema/verify-detail.schema.json';
import propertyConfig from './property-config/verify-detail.property-config.json';
import { ValidatorListItem, VerifyTypeItem } from './composition/type';

export const verifyDetailProps = {

    /** 表单验证默认显示的分组信息 */
    showType: { type: String, default: '' },

    /** 是否默认显示验证信息列表 */
    showList: { type: Boolean, default: false },

    /** 表单验证列表 */
    verifyList: {
        type: Array<ValidatorListItem>,
        default: [{
            id: '111',
            title: '单据信息[销售组织]',
            msg: '字段值不能为空',
            type: 'empty'
        },
        {
            id: '222',
            title: '单据信息[销售组织]',
            msg: '字段值不能为空',
            type: 'empty'
        }]
    },

    verifyType: {
        type: Array<VerifyTypeItem>,
        default: [
            {
                id: 'vertifyType1',
                type: 'all',
                title: '全部'
            },
            {
                id: 'vertifyType3',
                type: 'empty',
                title: '值为空'
            }
        ]
    },
    customClass: { type: String, default:'' },
    maxHeight: { type: Number, default: 200 }
} as Record<string, any>;

export type VerifyDetailProps = ExtractPropTypes<typeof verifyDetailProps>;
export const propsResolver = createPropsResolver<VerifyDetailProps>(verifyDetailProps, verifyDetailSchema, schemaMapper, schemaResolver, propertyConfig);
