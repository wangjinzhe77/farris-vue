/* eslint-disable prefer-destructuring */

import { SetupContext, computed, defineComponent, onBeforeMount, ref } from 'vue';
import { verifyDetailProps, VerifyDetailProps } from './verify-detail.props';

export default defineComponent({
    name: 'FVerifyDetail',
    props: verifyDetailProps,
    emits: ['validatorClick', 'listshow'] as (string[] & ThisType<void>) | undefined,
    setup(props: VerifyDetailProps, context: SetupContext) {
        let _errorLists: any = [];
        /** 是否默认显示验证信息列表 */
        const showList = ref(props.showList);
        /** 列表部分最大高度 */
        const maxHeight = ref(props.maxHeight);
        /** 默认传入的表单验证信息数组 */
        const verifyList = ref(props.verifyList);
        /** 表单验证默认显示的分组信息 */
        const showType = ref(props.showType);
        /** 表单验证的种类 */
        const verifyType = ref(props.verifyType);
        const customClass = ref(props.customClass);

        /** 显示详情 */
        function show() {
            showList.value = !showList.value;
            context.emit('listshow', showList.value);
        }

        /** 处理列表的active状态 */
        function handleActiveList(type: any) {
            if (!type && verifyType.value && verifyType.value.length > 0) {
                type = verifyType.value[0]['type'];
            }
            verifyType.value?.forEach((item: any) => {
                item.active = item.type === type;
            });
        }

        /** 处理列表优先展示的状态 */
        function handleShowType(type: string) {
            type = type ? type : _errorLists[0]['type'];
            _errorLists.forEach((item: any) => {
                item.show = item.type === type;
            });
        }

        /** 点击分组切换 */
        function showListContent(tab: any) {
            if (tab.length <= 0) { return; }
            handleActiveList(tab.type);
            handleShowType(tab.type);
        }
        /** 点击关闭按钮 */
        function close() {
            showList.value = false;
            context.emit('listshow', false);
        }
        /** 点击表单验证列表事件 */
        function errorClick(item: any) {
            context.emit('validatorClick', item);
        }

        function renderListItemContent(item: any) {
            return (
                <li class="f-verify-list" onClick={() => errorClick(item)}>
                    <span class={['f-icon f-icon-close-outline list-icon list-error', { 'list-warning': item.type === 'warn' }]}></span>
                    <div class="list-con">
                        <p class="list-title" title={item.title}>
                            {item.title}
                        </p>
                        <p class="list-msg" title={item.msg}>
                            {item.msg}
                        </p>
                    </div>
                </li>
            );
        }
        function renderListContent() {
            return _errorLists.map((errorlistItem: any) => (
                <ul class={['f-verify-list-content', { active: errorlistItem.show }]}>
                    {errorlistItem.list && errorlistItem.list.map((item: any) => renderListItemContent(item))}
                </ul>
            ));
        }
        const validator = computed(() => {
            return verifyList.value?.length > 0;
        });

        function btnStateClass(tab: any): Record<string, boolean> {
            const classObject = {
                'btn': true,
                'verify-title-btn': true,
                'f-state-selected': tab.active,
                'disabled': tab.length <= 0
            } as Record<string, boolean>;
            return classObject;
        }

        const tablistButton = computed(() => {
            return verifyType.value?.map((tab: any) => (
                <button class={btnStateClass(tab)} onClick={() => showListContent(tab)}>
                    {tab.title}
                    <span>{tab.length}</span>
                </button>
            ));
        });

        const verifyNums = computed(() => {
            return (
                <div class="f-verify-nums" onClick={show}>
                    <span class="nums-icon f-icon f-icon-warning"></span>
                    <span class="nums-count">{verifyList.value?.length}</span>
                </div>
            );
        });

        function handleDisplayText() {
            const errorlist: any = [];
            if (verifyType.value && verifyType.value?.length) {
                verifyType.value.map((typeItem: any) => {
                    const listObj = {
                        list: typeItem.type === 'all' ? verifyList.value : [],
                        show: false,
                        type: typeItem.type
                    };
                    errorlist.push(listObj);
                });

                errorlist.map((errorlistItem: any, index: number) => {
                    const matchingItem = verifyList.value.filter((verifyListItem: any) => verifyListItem.type === errorlistItem.type);
                    errorlist[index].list = index !== 0 ? matchingItem : errorlist[index].list;
                });

                verifyType.value.map((typeItem: any, index: number) => {
                    typeItem.length = errorlist[index].list.length;
                });
            }
            _errorLists = errorlist;
        }

        onBeforeMount(() => {
            handleDisplayText();
            handleShowType(showType.value);
            handleActiveList(showType.value);
        });

        return () => {
            return (
                <div class={validator.value ? `f-verify-detail ${customClass.value}` : ''}>
                    {validator.value ? (<div class="f-verify-detail-content">
                        {verifyNums.value}
                        <div class="f-verify-form-main" hidden={!showList.value}>
                            <div class="f-verify-form-content">
                                <div class="f-verify-form-content-arrow"></div>
                                <div class="f-verify-form-content-list">
                                    <div class="f-verify-forms-title">
                                        <div class="btn-group">{tablistButton.value}</div>
                                        <div class="f-verify-close" onClick={close}>
                                            <span class="f-icon f-icon-close"></span>
                                        </div>
                                    </div>
                                    <div class="f-verify-forms-list" style={{ maxHeight: maxHeight.value + 'px' }}>
                                        {renderListContent()}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>) : null}
                </div>
            );
        };
    }
});
