/* eslint-disable @typescript-eslint/no-unsafe-function-type */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { reactive, createApp, onUnmounted } from 'vue';
import type { App } from 'vue';
import { VerifyDetailProps } from './verify-detail.props';
import VerifyDetail from './verify-detail.component';

function initInstance(props: VerifyDetailProps, unmountFuc: Function, container: HTMLElement) {
    const app: App = createApp({
        setup() {
            onUnmounted(() => {
                unmountFuc && unmountFuc();
            });
            return () => <VerifyDetail {...props} ></VerifyDetail>;
        }
    });
    app.mount(container);
}

class VerifyDetailService {
    static verifyContainer: HTMLElement | null = null;
    static show(props: VerifyDetailProps, parent: HTMLElement | null): void {
        this.clear();
        let maxHeight = 0;
        this.verifyContainer = document.createElement('div');
        if (parent) {
            maxHeight = parent.offsetHeight - 110;
            parent.appendChild(this.verifyContainer);
        } else {
            maxHeight = document.documentElement.clientHeight - 110;
            document.body.appendChild(this.verifyContainer);
        }
        props['maxHeight'] = props['maxHeight'] ? props['maxHeight'] : maxHeight;
        initInstance(props, this.clear, this.verifyContainer);
    }
    static clear(): void {
        let container = this.verifyContainer;
        if (container) {
            if (container.parentNode) {
                container.parentNode.removeChild(container);
            }
            container = null;
        }
    }
}

export default VerifyDetailService;
