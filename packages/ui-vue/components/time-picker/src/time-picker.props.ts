 
import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import timePickerSchema from './schema/time-picker.schema.json';
import { schemaResolver } from './schema/schema-resolver';
import { DisabledHoursFunction, DisabledMinutesFunction, DisabledSecondsFunction } from './composition/types';

export const timePickerProps = {
    /**
     * 组件值,这个是与指定的format格式相对应的值
     */
    modelValue: { type: String, default: '' },
    hourStep: { type: Number, default: 1 },
    minuteStep: { type: Number, default: 1 },
    secondStep: { type: Number, default: 1 },
    popupClassName: { type: String, default: '' },
    placeholder: { type: String, default: '' },
    /**
     * 默认时间控件下拉面板，展开时的值
     */
    defaultOpenValue: { type: Date, default:null },
    format: { type: String, default: 'HH:mm:ss' },
    isOpen: { type: Boolean, default: false },
    showHeader: { type: Boolean, default: false },
    use12Hours: { type: Boolean, default: false },
    disabledHours: { type: Function as PropType<DisabledHoursFunction>,default:null},
    disabledMinutes: { type: Function as PropType<DisabledMinutesFunction>,default:null},
    disabledSeconds: { type: Function as PropType<DisabledSecondsFunction>,default:null},
    /**
     * 是否隐藏禁用的元素
     */
    hideDisabledElements: { type: Boolean, default: false },
    disabled: { type: Boolean, default: false },
    readonly: { type: Boolean, default: false },
    editable: { type: Boolean, default: true }
} as Record<string, any>;

export type TimePickerProps = ExtractPropTypes<typeof timePickerProps>;

export const propsResolver = createPropsResolver<TimePickerProps>(timePickerProps, timePickerSchema, schemaMapper, schemaResolver);
