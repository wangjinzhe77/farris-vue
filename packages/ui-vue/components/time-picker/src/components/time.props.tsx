import { ExtractPropTypes, PropType } from 'vue';
import { DisabledHoursFunction, DisabledMinutesFunction, DisabledSecondsFunction } from '../composition/types';

export const timeProps = {
    modelValue: { type: String, default: '' },
    hourStep: { type: Number, default: 1 },
    minuteStep: { type: Number, default: 1 },
    secondStep: { type: Number, default: 1 },
    defaultOpenValue: { type: Date, default: null},
    disabledHours: { type: Function as PropType<DisabledHoursFunction>, default: null },
    disabledMinutes: { type: Function as PropType<DisabledMinutesFunction>, default: null  },
    disabledSeconds: { type: Function as PropType<DisabledSecondsFunction>, default: null  },
    format: { type: String, default: 'HH:mm:ss' },
    isOpen: { type: Boolean, default: false },
    use12Hours: { type: Boolean, default: false },
    showHeader: { type: Boolean, default: false },
    hideDisabledElements: { type: Boolean, default: false }
};

export type TimeProps = ExtractPropTypes<typeof timeProps>;
