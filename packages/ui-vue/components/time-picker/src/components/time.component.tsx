 
/* eslint-disable no-use-before-define */
import { SetupContext, computed, defineComponent, onMounted, ref, watch } from 'vue';
import { TimeProps, timeProps } from './time.props';
import { TimeHolder } from '../composition/time-holder';
import { convertToDate, getRealFormat, isNotNull } from '../composition/utils';
import { Hour12SelectData, TimePickerSelectData, TimePickerUnit, TimeValueText } from '../composition/types';
import { useScrollToSelected } from '../composition/use-scroll-to-selected';
import { useSelected } from '../composition/use-selected';

export default defineComponent({
    name: 'FTimePickerTimeView',
    props: timeProps,
    emits: ['valueChange', 'formatedValueChange'],
    setup(props: TimeProps, context) {
        const prefixClass = 'time-picker-panel';
        const panelWidth = ref(0);
        // 记录可用时间的范围
        const hourRange = ref<Array<TimePickerSelectData>>([]);
        const minuteRange = ref<Array<TimePickerSelectData>>([]);
        const secondRange = ref<Array<TimePickerSelectData>>([]);
        const use12HoursRange = ref<Array<Hour12SelectData>>([]);
        // 标记小时元素
        const hourElements = ref();
        const minuteElements = ref();
        const secondElements = ref();
        const hours12Elements = ref();

        const time = new TimeHolder();

        const { scrollToSelected } = useScrollToSelected(props, context as SetupContext, hourRange, minuteRange, secondRange, use12HoursRange);
        const { getSelectedClass } = useSelected(time, prefixClass);
        const hourStep = ref(props.hourStep);
        const minuteStep = ref(props.minuteStep);
        const secondStep = ref(props.secondStep);

        const defaultOpenValue = ref();
        const format = ref('');
        // 记录是否启用12小时制
        const use12Hours = ref();
        const hideDisabledElements = ref(props.hideDisabledElements);
        // 记录启用几列
        const enabledColumns = ref(0);

        watch(() => props.defaultOpenValue, (newValue) => {
            if (isNotNull(newValue)) {
                defaultOpenValue.value = newValue;
            }
        });
        watch(defaultOpenValue, (newValue) => {
            if (isNotNull(newValue)) {
                time.setDefaultOpenValue(newValue);
            }
        });
        defaultOpenValue.value = props.defaultOpenValue;

        // 从TimeHolder获取修改的值
        watch(time.changedValue, (newValue: TimeValueText) => {
            context.emit("valueChange", newValue);
        });

        watch(() => props.modelValue, (newValue) => {
            setModelValue(newValue);
        });

        watch(() => props.format, (newValue) => {
            if (isNotNull(newValue)) {
                format.value = newValue;
            } else {
                format.value = 'HH:mm:ss';
            }
        });

        watch(format, (newValue) => {
            if (isNotNull(newValue)) {
                time.setFormat(format.value);
                enabledColumns.value = 0;
                if (showHour.value) {
                    enabledColumns.value++;
                }
                if (showMinute.value) {
                    enabledColumns.value++;
                }
                if (showSecond.value) {
                    enabledColumns.value++;
                }
                if (use12Hours.value) {
                    enabledColumns.value++;
                    // buildHours();
                    build12Hours();
                }
                panelWidth.value = enabledColumns.value * 66;
            }
        });

        watch(() => props.use12Hours, (newValue) => {
            use12Hours.value = newValue;
        });
        watch(use12Hours, (newValue) => {
            format.value = getRealFormat(format.value, newValue);
            time.setUse12Hours(newValue);
        });
        // 监视禁用的小时变更
        watch(() => props.disabledHours, () => {
            buildHours();
        });

        // 监视禁用的分钟方法变更
        watch(() => props.disabledMinutes, () => {
            buildMinutes();
        });

        // 监视禁用的秒方法变更
        watch(() => props.disabledSeconds, () => {
            buildSeconds();
        });
        function setModelValue(value) {
            if (isNotNull(value)) {
                time.text = value;
                const convertedValue = convertToDate(time.text, format.value);
                time.setValue(convertedValue, use12Hours.value);
                buildTimes();
            } else {
                time.setUse12Hours(use12Hours.value);
            }
        }
        // 监视禁用的小时方法变更
        function getDisabledHoursArray() {
            let result: number[] = [];
            if (props.disabledHours) {
                result = props.disabledHours();
                if (use12Hours.value) {
                    if (time.selected12Hours === 'PM') {
                        /**
                         * [0, 1, 2, ..., 12, 13, 14, 15, ..., 23] => [12, 1, 2, 3, ..., 11]
                         */
                        result = result.filter((hour: number) => hour >= 12).map((hour: number) => (hour > 12 ? hour - 12 : hour));
                    } else {
                        /**
                         * [0, 1, 2,..., 12, 13, 14, 15, ...23] => [12, 1, 2, 3, ..., 11]
                         */
                        result = result.filter((hour: number) => hour < 12 || hour === 24).map((hour: number) => (hour === 24 || hour === 0 ? 12 : hour));
                    }
                }
            }
            return result;
        };
        /**
         * 判断某个时间类型是否被启用
         * @param type
         */
        function isTimeTypeEnable(type: TimePickerUnit = 'hour') {
            if (format.value) {
                return format.value.indexOf(type[0].toUpperCase()) > -1 || format.value.indexOf(type[0].toLowerCase()) > -1;
            }
            return false;
        }

        const timePickerTimeViewClass = computed(() => {
            const classOject = {
                'time-picker-panel': true,
                'time-picker-panel-column-3': true,
                'time-picker-panel-placement-bottomLeft': true,
                'f-area-show': true
            } as Record<string, boolean>;
            return classOject;
        });

        const timePickerComboBoxStyle = computed(() => {
            const styleObject = {
                width: use12Hours.value ? `${panelWidth.value}px` : '100%'
            } as Record<string, any>;
            return styleObject;
        });

        const showHour = computed(() => {
            return isTimeTypeEnable('hour');
        });

        const showMinute = computed(() => {
            return isTimeTypeEnable('minute');
        });

        const showSecond = computed(() => {
            return isTimeTypeEnable('second');
        });

        function makeRange(length: number, step = 1, start = 0): number[] {
            step = Math.ceil(step);
            return new Array(Math.ceil(length / step)).fill(0).map((_, i) => (i + start) * step);
        }

        function scrollElementsIntoView(): void {
            if (showHour.value && hourElements.value) {
                scrollToSelected(
                    hourElements.value,
                    isNotNull(time.viewHours) ? time.viewHours! : time.defaultViewHours,
                    0,
                    'hour', time
                );
            }
            if (showMinute.value && minuteElements.value) {
                scrollToSelected(
                    minuteElements.value,
                    isNotNull(time.minutes) ? time.minutes! : time.defaultMinutes,
                    0,
                    'minute', time
                );
            }
            if (showSecond.value && secondElements.value) {
                scrollToSelected(
                    secondElements.value,
                    isNotNull(time.seconds) ? time.seconds! : time.defaultSeconds,
                    0,
                    'second', time
                );
            }
            if (use12Hours.value && hours12Elements.value) {
                const selectedHours = isNotNull(time.selected12Hours)
                    ? time.selected12Hours
                    : time.default12Hours;
                const index = selectedHours === 'AM' ? 0 : 1;
                scrollToSelected(
                    hours12Elements.value,
                    index,
                    0,
                    '12-hour', time
                );
            }
        }
        /**
         * 构造时间数据
         */
        function buildHours(): void {
            let hourRanges = 24;
            let startIndex = 0;
            if (use12Hours.value) {
                hourRanges = 12;
                startIndex = 1;
            }
            const disabledHoursArray = getDisabledHoursArray();
            hourRange.value = makeRange(hourRanges, hourStep.value, startIndex).map((num) => {
                return {
                    index: num,
                    disabled: disabledHoursArray.length > 0 && disabledHoursArray.indexOf(num) !== -1
                };
            });
            if (use12Hours.value && hourRange.value[hourRange.value.length - 1].index === 12) {
                const hourRangeList = [...hourRange.value];
                hourRangeList.unshift(hourRangeList[hourRangeList.length - 1]);
                hourRangeList.splice(hourRangeList.length - 1, 1);
                hourRange.value = hourRangeList;
            }

            // 移除禁用的值
            hourRange.value = hourRange.value.filter((n) => {
                return !(hideDisabledElements.value && n.disabled);
            });
        }
        /**
         * 构造分钟数据
         */
        function buildMinutes(): void {
            minuteRange.value = makeRange(60, minuteStep.value).map((num) => {
                return {
                    index: num,
                    disabled: !!props.disabledMinutes && props.disabledMinutes(time.hours!).indexOf(num) !== -1
                };
            });

            // 移除禁用的值
            minuteRange.value = minuteRange.value.filter((n) => {
                return !(hideDisabledElements.value && n.disabled);
            });
        }
        /**
        * 构造秒数据
        */
        function buildSeconds(): void {
            secondRange.value = makeRange(60, secondStep.value).map((num) => {
                return {
                    index: num,
                    disabled: !!props.disabledSeconds && props.disabledSeconds(time.hours!, time.minutes!).indexOf(num) !== -1
                };
            });

            // 移除禁用的值
            secondRange.value = secondRange.value.filter((n) => {
                return !(hideDisabledElements.value && n.disabled);
            });
        }

        function build12Hours(): void {
            const isUpperForamt = format.value.includes('A');
            use12HoursRange.value = [
                {
                    index: 0,
                    value: isUpperForamt ? 'AM' : 'am'
                },
                {
                    index: 1,
                    value: isUpperForamt ? 'PM' : 'pm'
                }
            ];

            // 移除禁用的值
            use12HoursRange.value = use12HoursRange.value.filter(() => {
                return !hideDisabledElements.value;
            });
        }

        /**
         *
         * @param event
         * @param hour
         * @returns
         */
        function selectHour(event: MouseEvent, hour: TimePickerSelectData) {
            event.stopPropagation();
            time.setHours(hour.index, hour.disabled);

            if (hour.disabled) {
                return;
            }

            scrollToSelected(
                hourElements.value,
                hour.index,
                120,
                'hour',
                time
            );

            if (props.disabledMinutes !== null) {
                buildMinutes();

                const minutes = minuteRange.value.filter(n => !n.disabled);
                if (!minutes.find(n => n.index === time.minutes)) {
                    const { index, disabled } = minutes[0];
                    time.setMinutes(index, disabled);
                }

            }
            if (props.disabledSeconds !== null || props.disabledMinutes !== null) {
                buildSeconds();
            }
        }
        /**
         * 选择小时
         * @param event
         * @param hour
         */
        function select12Hour(event: MouseEvent, hour: Hour12SelectData) {
            event.stopPropagation();
            time.selected12Hours = hour.value;
            buildHours();
            buildMinutes();
            buildSeconds();
            scrollToSelected(
                hours12Elements.value,
                hour.index,
                120,
                '12-hour',
                time
            );
        }
        /**
         * 选择分
         * @param event
         * @param minute
         * @returns
         */
        function selectMinute(event: MouseEvent, minute: TimePickerSelectData) {
            event.stopPropagation();
            time.setMinutes(minute.index, minute.disabled);

            if (minute.disabled) {
                return;
            }

            scrollToSelected(
                minuteElements.value,
                minute.index,
                120,
                'minute',
                time
            );
            if (props.disabledSeconds !== null) {
                buildSeconds();
                const seconds = secondRange.value.filter(n => !n.disabled);
                if (!seconds.find(n => n.index === time.seconds)) {
                    const { index, disabled } = seconds[0];
                    time.setSeconds(index, disabled);
                }
            }
        }
        /**
         * 选择秒
         * @param event
         * @param second
         * @returns
         */
        function selectSecond(event: MouseEvent, second: TimePickerSelectData) {
            event.stopPropagation();
            time.setSeconds(second.index, second.disabled);

            if (second.disabled) {
                return;
            }

            scrollToSelected(
                secondElements.value,
                second.index,
                120,
                'second',
                time
            );
        }

        function buildTimes() {
            buildHours();
            buildMinutes();
            buildSeconds();
            build12Hours();
        }

        onMounted(() => {
            format.value = props.format;
            use12Hours.value = props.use12Hours;
            setModelValue(props.modelValue);
            buildTimes();
            setTimeout(() => { scrollElementsIntoView(); }, 10);
        });
        /**
         * 获取
         * @returns
         */
        function getHeaderHtml() {
            if (props.showHeader) {
                return (<div class={prefixClass + '-combobox time-picker-panel-header'} style={timePickerComboBoxStyle.value}>
                    {showHour.value && <div class={prefixClass + '-select'} style="height: 32px" >
                        <ul>
                            <li class={'time-header' + showMinute.value ? 'time-header-split' : ''}>时</li>
                        </ul >
                    </div >}
                    {showMinute.value && <div class={prefixClass + '-select'} style="height: 32px" >
                        <ul>
                            <li class={'time-header' + showSecond.value ? 'time-header-split' : ''}>分</li>
                        </ul >
                    </div >}
                    {showSecond.value && <div class={prefixClass + '-select'} style="height: 32px" >
                        <ul>
                            <li class="time-header">秒</li>
                        </ul>
                    </div >}
                    {props.use12Hours && <div class={prefixClass + '-select'} style="height: 32px" >
                        <ul>
                            <li class="time-header"></li>
                        </ul>
                    </div >}
                </div >);
            }
            return null;

        }

        return () => {
            return (
                <div class={timePickerTimeViewClass.value}>
                    <div class="farris-timer-picker">
                        <div class={prefixClass + '-inner'}>
                            {getHeaderHtml()}
                            <div class={prefixClass + '-combobox'} style={timePickerComboBoxStyle.value}>
                                {showHour.value && (
                                    <div class={prefixClass + '-select'} ref={hourElements}>
                                        <ul>
                                            {hourRange.value.map((hour: any) => {
                                                return (
                                                    <li
                                                        class={getSelectedClass(hour, 'hour')}
                                                        onClick={(payload: MouseEvent) => selectHour(payload, hour)}>
                                                        {`${hour.index}`}
                                                    </li>
                                                );
                                            })}
                                        </ul>
                                    </div>
                                )}
                                {showMinute.value && (
                                    <div class={prefixClass + '-select'} ref={minuteElements}>
                                        <ul>
                                            {minuteRange.value.map((minute: any) => {
                                                return (
                                                    <li
                                                        class={getSelectedClass(minute, 'minute')}
                                                        onClick={(payload: MouseEvent) => selectMinute(payload, minute)}>
                                                        {`${minute.index}`}
                                                    </li>
                                                );
                                            })}
                                        </ul>
                                    </div>
                                )}
                                {showSecond.value && (
                                    <div class={prefixClass + '-select'} ref={secondElements}>
                                        <ul>
                                            {secondRange.value.map((second: any) => {
                                                return (
                                                    <li
                                                        class={getSelectedClass(second, 'second')}
                                                        onClick={(payload: MouseEvent) => selectSecond(payload, second)}>
                                                        {`${second.index}`}
                                                    </li>
                                                );
                                            })}
                                        </ul>
                                    </div>
                                )}
                                {use12Hours.value && (
                                    <div class={prefixClass + '-select'} ref={hours12Elements}>
                                        <ul>
                                            {use12HoursRange.value.map((hour: any) => {
                                                return (
                                                    <li
                                                        class={getSelectedClass(hour, '12-hour')}
                                                        onClick={(payload: MouseEvent) => select12Hour(payload, hour)}>
                                                        {`${hour.value}`}
                                                    </li>
                                                );
                                            })}
                                        </ul>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            );
        };
    }
});
