import { InputBaseProperty } from "../../../property-panel/src/composition/entity/input-base-property";

export class TimePickerProperty extends InputBaseProperty {

    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }

    getEditorProperties(propertyData: any) {
        return this.getComponentConfig(propertyData, { type: "time-picker" }, {
            editable: {
                description: "",
                title: "允许编辑",
                type: "boolean"
            },
            format: {
                description: "",
                title: "格式",
                type: "enum",
                editor: {
                    type: "combo-list",
                    textField: "name",
                    valueField: "value",
                    data: [
                        {
                            value: "HH:mm:ss",
                            name: "HH:mm:ss"
                        }
                    ]
                }
            },
            showHeader: {
                description: "",
                title: "是否显示头部区域",
                type: "boolean"
            },
            use12Hours: {
                description: "",
                title: "是否使用12小时制",
                type: "boolean"
            }
        });
    }
};

