import { SetupContext, computed, defineComponent, ref, watch } from 'vue';
import { TimePickerProps, timePickerProps } from './time-picker.props';
import FButtonEdit from '@farris/ui-vue/components/button-edit';
import FTimePickerTimeView from './components/time.component';
import { convertToDate, convertToString, getRealFormat } from './composition/utils';
import { TimeValueText } from './composition/types';

export default defineComponent({
    name: 'FTimePicker',
    props: timePickerProps,
    emits: ['update:modelValue', 'valueChange', 'clear'] as (string[] & ThisType<void>) | undefined,
    setup(props: TimePickerProps, context: SetupContext) {
        const groupIcon = '<span class="f-icon f-icon-timepicker"></span>';
        const modelValue = ref(props.modelValue);
        const buttonEdit = ref<any>(null);
        const timePicker = ref();
        function onClickButton() { }

        const formatValue = computed(() => {
            return getRealFormat(props.format, props.use12Hours);
        });

        function onClearHandler() {
            modelValue.value = '';
            context.emit('clear');
        }
        function formatDate() {
            if (modelValue.value) {
                const convertedDate = convertToDate(modelValue.value, formatValue.value);
                modelValue.value = convertToString(convertedDate, formatValue.value);
            }
        }
        function onBlurHandler() {
            formatDate();
        }
        function onValueChangeHandler(textValue: TimeValueText) {
            modelValue.value = textValue.text;
        }

        formatDate();

        watch(() => props.modelValue, (newValue) => {
            modelValue.value = newValue;
        });

        watch(modelValue, (newValue) => {
            context.emit("valueChange", newValue);
            context.emit("update:modelValue", newValue);
        });

        return () => {
            return (
                <FButtonEdit
                    ref={buttonEdit}
                    v-model={modelValue.value}
                    disable={props.disabled}
                    readonly={props.readonly}
                    editable={props.editable}
                    buttonContent={groupIcon}
                    placeholder={props.placeholder}
                    enableClear={true}
                    onClear={onClearHandler}
                    onBlur={onBlurHandler}
                >
                    <FTimePickerTimeView
                        ref={timePicker}
                        {...props}
                        onValueChange={onValueChangeHandler}
                    ></FTimePickerTimeView>
                </FButtonEdit>
            );
        };
    }
});
