import { SetupContext, defineComponent, inject, onMounted, ref } from 'vue';
import { TimePickerProps, timePickerProps } from '../time-picker.props';
import { FButtonEdit } from '../../../button-edit';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useTimePickerDesignerRules } from './use-design-rules';

export default defineComponent({
    name: 'FTimePickerDesign',
    props: timePickerProps,
    emits: ['datePicked', 'update:modelValue'],
    setup(props: TimePickerProps, context:SetupContext) {
        const groupIcon = '<span class="f-icon f-icon-timepicker"></span>';
        const modelValue = ref(props.modelValue);
        const buttonEdit = ref<any>();
        const element = ref<any>();        
        const designerHostService = inject('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useTimePickerDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(element, designItemContext,designerRulesComposition);

        onMounted(() => {
            element.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        function onClickButton() { }

        function onDatePicked(dateValue: string) {
            modelValue.value = dateValue;
            context.emit('update:modelValue', dateValue);
            context.emit('datePicked', dateValue);
            buttonEdit.value?.commitValue(dateValue);
        }

        return () => {
            return (
                <div ref={element}>
                    <FButtonEdit
                        ref={buttonEdit}
                        v-model={modelValue.value}
                        buttonContent={groupIcon}
                        enableClear
                        onClickButton={() => onClickButton()}>
                        <f-time-picker-time-view></f-time-picker-time-view>
                    </FButtonEdit>
                </div>
            );
        };
    }
});
