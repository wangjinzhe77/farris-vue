
import { format as fnsFormat, parse as fnsParse } from 'date-fns';
// tslint:disable-next-line:no-any
export function isNotNull(value: any): boolean {
    return typeof value !== 'undefined' && value !== null;
}

export function toBoolean(value: boolean | string): boolean {
    return !!value;
}

export function convertToDate(value: string, format: string = 'hh:mm:ss'): Date|undefined {
    if (!value || !format) {
        return;
    }

    if (format.indexOf('ss') === -1 && value.split(':').length === 3) {
        format = format + ':ss';
    }
    if(format.indexOf('A')>-1){
        format=format.replace(/A/g,'a');
        return fnsParse(value.toLowerCase(), format, new Date());
    }
    return fnsParse(value, format, new Date());
}

export function convertToString(
    date: Date|undefined,
    format: string = 'hh:mm:ss',
    selected12Hours = 'AM'
) {
    if (!date || isNaN(date.getTime())) {
        return '';
    }
    if(format.indexOf('A')>-1){
        format=format.replace(/A/g,'a');
        return fnsFormat(date, format).toUpperCase();
    }
    return fnsFormat(date, format);    
}

export function addZero(val: any) {
    val = String(val);
    if (val.length === 1) {
        return '0' + val;
    }
    return val;
}

export function getRealFormat(format:string,use12Hours:boolean){
    let newFormat=format?format:'hh:mm:ss';
    if(use12Hours){
        newFormat = newFormat ? newFormat.replace(/H/g, 'h') : 'hh:mm:ss a';
        if (newFormat.indexOf('a') === -1&&newFormat.indexOf('A') === -1) {
            newFormat = newFormat + ' a';
        }
    }else{
        newFormat = newFormat ? newFormat.replace(/h/g, 'H') : 'HH:mm:ss';
    }
    return newFormat;
}
