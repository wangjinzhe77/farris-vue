export type DisabledHoursFunction= () => number[];
export type DisabledMinutesFunction= (hour: number) => number[];
export type DisabledSecondsFunction= (hour: number, minute: number) => number[];
export type TimePickerUnit = 'hour' | 'minute' | 'second' | '12-hour';
export type TimePickerSelectData={index: number; disabled: boolean};
export type Hour12SelectData={index: number; value: string};
export type TimeValueText={text: string;value: Date|undefined};
