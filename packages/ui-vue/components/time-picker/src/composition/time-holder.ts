import { Ref, SetupContext, ref } from 'vue';
import { convertToString, isNotNull } from './utils';
import { isValid } from 'date-fns';
import { TimeValueText } from './types';

export class TimeHolder {
    hourStep = 1;

    minuteStep = 1;

    secondStep = 1;

    use12Hours = false;

    changedValue: Ref<TimeValueText>=ref({text:'',value:undefined});

    format='HH:mm:ss';

    private temporaryHours: number | undefined = undefined;

    /**
     * @description
     * Same as realHours
     * @see realHours
     */
    get hours(): number | undefined {
        return this.temporaryHours;
    }

    /**
     * @description
     * Set viewHours to realHours
     */
    set hours(value: number | undefined) {
        if (value !== this.temporaryHours) {
            if (this.use12Hours) {
                if (this.selected12Hours === 'PM' && value !== 12) {
                    this.temporaryHours! = (value as number) + 12;
                } else if (this.selected12Hours === 'AM' && value === 12) {
                    this.temporaryHours = 0;
                } else {
                    this.temporaryHours = value;
                }
            } else {
                this.temporaryHours = value;
            }
            this.update();
        }
    }

    /**
     * @description
     * Value hours
     * Get realHours and its range is [0, 1, 2, ..., 22, 23]
     */
    get realHours(): number | undefined {
        return this.temporaryHours;
    }

    /**
     * @description
     * UI view hours
     * Get viewHours which is selected in `time-picker-panel` and its range is [12, 1, 2, ..., 11]
     */
    get viewHours(): number | undefined {
        return this.use12Hours && isNotNull(this.temporaryHours) ? this.calculateViewHour(this.temporaryHours!) : this.temporaryHours;
    }

    private temporaryMinutes: number | undefined = undefined;

    get minutes(): number | undefined {
        return this.temporaryMinutes;
    }

    set minutes(value: number | undefined) {
        if (value !== this.temporaryMinutes) {
            this.temporaryMinutes = value;
            this.update();
        }
    }

    private temporarySeconds: number | undefined = undefined;

    get seconds(): number | undefined {
        return this.temporarySeconds;
    }

    set seconds(value: number | undefined) {
        if (value !== this.temporarySeconds) {
            this.temporarySeconds = value;
            this.update();
        }
    }

    /**
     * @description
     * Same as defaultRealHours
     */
    get defaultHours(): number {
        return this.defaultOpenValue.getHours();
    }

    /**
     * @description
     * Get deafultViewHours when defaultOpenValue is setted
     * @see viewHours
     */
    get defaultViewHours(): number {
        const hours = this.defaultOpenValue.getHours();
        return this.use12Hours && isNotNull(hours) ? this.calculateViewHour(hours) : hours;
    }

    /**
     * @description
     * Get defaultRealHours when defaultOpenValue is setted
     * @see realHours
     */
    get defaultRealHours(): number {
        return this.defaultOpenValue.getHours();
    }

    get defaultMinutes(): number {
        return this.defaultOpenValue.getMinutes();
    }

    get defaultSeconds(): number {
        return this.defaultOpenValue.getSeconds();
    }

    get default12Hours(): string {
        return this.defaultOpenValue.getHours() >= 12 ? 'PM' : 'AM';
    }

    private temporaryText = '';

    get text(): string {
        return this.temporaryText;
    }

    set text(value: string) {
        if (value !== this.temporaryText) {
            this.temporaryText = value;
        }
    }

    private temporaryValue: Date | undefined;

    get value(): Date | undefined {
        return this.temporaryValue;
    }

    set value(value: Date | undefined) {
        if (value !== this.temporaryValue) {
            this.temporaryValue = value;
            if (isNotNull(this.temporaryValue) && isValid(this.temporaryValue)) {
                // tslint:disable-next-line:no-non-null-assertion
                this.temporaryHours = this.temporaryValue!.getHours();
                this.temporaryMinutes = this.temporaryValue!.getMinutes();
                this.temporarySeconds = this.temporaryValue!.getSeconds();
                if (this.use12Hours && isNotNull(this.temporaryHours)) {
                    this.temporarySelected12Hours = this.temporaryHours >= 12 ? 'PM' : 'AM';
                }
            } else {
                this.clearTimeValue();
            }
        }
    }

    private temporarySelected12Hours: string | undefined = undefined;

    get selected12Hours(): string | undefined {
        return this.temporarySelected12Hours;
    }

    set selected12Hours(value: string | undefined) {
        if (value!.toUpperCase() !== this.temporarySelected12Hours) {
            this.temporarySelected12Hours = value!.toUpperCase();
            this.update();
        }
    }

    private temporaryDefaultOpenValue: Date = new Date();

    get defaultOpenValue(): Date {
        this.temporaryDefaultOpenValue.setHours(this.setValueByStep(this.temporaryDefaultOpenValue.getHours(), this.hourStep));
        this.temporaryDefaultOpenValue.setMinutes(this.setValueByStep(this.temporaryDefaultOpenValue.getMinutes(), this.minuteStep));
        this.temporaryDefaultOpenValue.setSeconds(this.setValueByStep(this.temporaryDefaultOpenValue.getSeconds(), this.secondStep));
        return this.temporaryDefaultOpenValue;
    }

    set defaultOpenValue(value: Date) {
        if (this.temporaryDefaultOpenValue !== value) {
            this.temporaryDefaultOpenValue = value;
            this.update();
        }
    }

    get isEmpty(): boolean {
        return !(isNotNull(this.temporaryHours) || isNotNull(this.temporaryMinutes) || isNotNull(this.temporarySeconds));
    }

    constructor() {}

    setFormat(value: string){
        this.format=value;
    }

    setHours(value: number, disabled: boolean): this {
        if (disabled) {
            return this;
        }
        this.setDefaultValueIfNil();
        this.hours = value;
        return this;
    }

    setUse12Hours(value: boolean): this {
        this.use12Hours = value;
        return this;
    }

    setMinutes(value: number, disabled: boolean): this {
        if (disabled) {
            return this;
        }
        this.setDefaultValueIfNil();
        this.minutes = value;
        return this;
    }

    setSeconds(value: number, disabled: boolean): this {
        if (disabled) {
            return this;
        }
        this.setDefaultValueIfNil();
        this.seconds = value;
        return this;
    }

    setValue(value: Date | undefined, use12Hours?: boolean): this {
        if (isNotNull(use12Hours)) {
            this.use12Hours = use12Hours as boolean;
        }
        this.value = value;
        return this;
    }

    setValueByStep(value: number, step: number) {
        let times = Math.floor(value / step);
        const remainder = value % step;
        const halfStep = step / 2;
        times = remainder > halfStep ? times + 1 : times;
        return step * times;
    }

    setDefaultOpenValue(value: Date): this {
        this.defaultOpenValue = value;
        return this;
    }

    setDefaultValueIfNil(): void {
        if (!isNotNull(this.temporaryValue)) {
            this.temporaryValue = new Date(this.defaultOpenValue);
        }
    }

    private update(): void {
        if (this.isEmpty) {
            this.temporaryValue = undefined;
        } else {
            if (!isNotNull(this.temporaryHours)) {
                this.temporaryHours = this.defaultHours;
            } else {
                this.temporaryValue!.setHours(this.hours!);
            }

            if (!isNotNull(this.temporaryMinutes)) {
                this.temporaryMinutes = this.defaultMinutes;
            } else {
                this.temporaryValue!.setMinutes(this.minutes!);
            }

            if (!isNotNull(this.temporarySeconds)) {
                this.temporarySeconds = this.defaultSeconds;
            } else {
                this.temporaryValue!.setSeconds(this.seconds!);
            }

            if (this.use12Hours) {
                if (!isNotNull(this.temporarySelected12Hours)) {
                    this.temporarySelected12Hours = this.default12Hours;
                }
                if (this.selected12Hours === 'PM' && this.temporaryHours! < 12) {
                    this.temporaryHours! += 12;
                    this.temporaryValue!.setHours(this.temporaryHours!);
                }
                if (this.selected12Hours === 'AM' && this.temporaryHours! >= 12) {
                    this.temporaryHours! -= 12;
                    this.temporaryValue!.setHours(this.temporaryHours!);
                }
            }

            this.temporaryValue = new Date(this.temporaryValue!);
            /** 更改Text的值 */
            this.text = convertToString(
                this.temporaryValue,
                this.format,
                this.selected12Hours
            );
            this.changedValue.value={text:this.text,value:this.temporaryValue};
        }
    }

    private clearTimeValue() {
        this.temporaryHours = undefined;
        this.temporaryMinutes = undefined;
        this.temporarySeconds = undefined;
        this.temporarySelected12Hours = undefined;
        this.temporaryValue = undefined;
    }

    clear(): void {
        this.clearTimeValue();
        this.update();
    }

    private calculateViewHour(value: number): number {
        const selected12Hours = this.temporarySelected12Hours || this.default12Hours;
        if (selected12Hours === 'PM' && value > 12) {
            return value - 12;
        }
        if (selected12Hours === 'AM' && value === 0) {
            return 12;
        }
        return value;
    }
}
