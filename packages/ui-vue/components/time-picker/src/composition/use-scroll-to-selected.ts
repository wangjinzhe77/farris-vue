/* eslint-disable no-use-before-define */
 
 
import { Ref, SetupContext, ref } from "vue";
import { TimeProps } from "../components/time.props";
import { useReqAnimationFrame } from "../../../common";
import { TimePickerUnit } from "./types";
import { TimeHolder } from "./time-holder";

export function useScrollToSelected(props: TimeProps, context: SetupContext,hourRange: Ref<any>,minuteRange: Ref<any>,secondRange: Ref<any>,use12HoursRange: Ref<any>) {
    function scrollToSelected(
        instance: HTMLElement,
        index: number,
        duration = 0,
        unit: TimePickerUnit,
        time: TimeHolder
    ): void {
        const transIndex = translateIndex(index, unit,time);
        const currentOption = (instance.children[0].children[transIndex] ||
            instance.children[0].children[0]) as HTMLElement;
        scrollTo(instance, currentOption.offsetTop, duration);
    }

    function translateIndex(index: number, unit: TimePickerUnit,   time: TimeHolder): number {
        if (unit === 'hour') {
            const disabledHours = props.disabledHours && props.disabledHours();
            return calculateIndex(
                disabledHours,
                hourRange.value.map(item => item.index).indexOf(index)
            );
        } if (unit === 'minute') {
            const disabledMinutes =
                props.disabledMinutes && props.disabledMinutes(time.hours!);
            return calculateIndex(
                disabledMinutes,
                minuteRange.value.map(item => item.index).indexOf(index)
            );
        } if (unit === 'second') {
            // second
            const disabledSeconds =
                props.disabledSeconds &&
                props.disabledSeconds(time.hours!,time.minutes!);
            return calculateIndex(
                disabledSeconds,
                secondRange.value.map(item => item.index).indexOf(index)
            );
        }
        // 12-hour
        return calculateIndex(
            [],
            use12HoursRange.value.map(item => item.index).indexOf(index)
        );

    }

    function scrollTo(element: HTMLElement, to: number, duration: number): void {
        if (duration <= 0) {
            element.scrollTop = to;
            return;
        }
        const difference = to - element.scrollTop;
        const perTick = (difference / duration) * 10;

        useReqAnimationFrame(() => {
            element.scrollTop += perTick;
            if (element.scrollTop === to) {
                return;
            }
            scrollTo(element, to, duration - 10);
        });
    }

    function calculateIndex(array: number[], index: number): number {
        if (array && array.length && props.hideDisabledElements) {
            return (
                index -
                array.reduce((pre, value) => {
                    return pre + (value < index ? 1 : 0);
                }, 0)
            );
        }
        return index;

    }
    return {scrollToSelected};
}
