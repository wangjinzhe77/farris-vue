import { TimeHolder } from "./time-holder";
import { isNotNull } from "./utils";
import { TimePickerUnit } from "./types";

export function useSelected(time: TimeHolder,prefixClass: string) {
    function isSelectedHour(hour: { index: number; disabled: boolean }): boolean {
        return (
            hour.index === time.viewHours ||
            (!isNotNull(time.viewHours) &&
                hour.index === time.defaultViewHours)
        );
    }

    function  isSelectedMinute(minute: { index: number; disabled: boolean }): boolean {
        return (
            minute.index === time.minutes ||
            (!isNotNull(time.minutes) &&
                minute.index === time.defaultMinutes)
        );
    }

    function  isSelectedSecond(second: { index: number; disabled: boolean }): boolean {
        return (
            second.index === time.seconds ||
            (!isNotNull(time.seconds) &&
                second.index === time.defaultSeconds)
        );
    }

    function  isSelected12Hours(value: { index: number; value: string }): boolean {
        return (
            value.value.toUpperCase() === time.selected12Hours ||
            (!isNotNull(time.selected12Hours) &&
                value.value.toUpperCase() === time.default12Hours)
        );
    }
    function getSelectedClass(value: any, type: TimePickerUnit) {
        let className = '';
        const selected = ` ${prefixClass}-select-option-selected `;
        const disabledC = ` ${prefixClass}-select-option-disabled `;

        let selectedStae = false;
        if (type === 'hour') {
            selectedStae = isSelectedHour(value);
        } else if (type === 'minute') {
            selectedStae = isSelectedMinute(value);
        } else if (type === 'second') {
            selectedStae = isSelectedSecond(value);
        } else {
            selectedStae = isSelected12Hours(value);
        }

        if (selectedStae) {
            className += selected;
        }

        if (type === '12-hour') {
            return className;
        }

        if (value.disabled) {
            className += disabledC;
        }
        return className;
    }
    return {getSelectedClass};
}
