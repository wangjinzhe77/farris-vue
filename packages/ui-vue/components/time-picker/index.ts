 
import type { App, Plugin } from 'vue';
import FTimePicker from './src/time-picker.component';
import FTimePickerTimeView from './src/components/time.component';
import { propsResolver } from './src/time-picker.props';
import FTimePickerDesign from './src/designer/time-picker.design.component';

export * from './src/time-picker.props';
export * from './src/components/time.props';

FTimePicker.install = (app: App) => {
    app.component(FTimePicker.name as string, FTimePicker)
        .component(FTimePickerTimeView.name as string, FTimePickerTimeView);
};
FTimePicker.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap['time-picker'] = FTimePicker;
    propsResolverMap['time-picker'] = propsResolver;
};
FTimePicker.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['time-picker'] = FTimePickerDesign;
    propsResolverMap['time-picker'] = propsResolver;
};

export { FTimePicker, FTimePickerTimeView };
export default FTimePicker as typeof FTimePicker & Plugin;
