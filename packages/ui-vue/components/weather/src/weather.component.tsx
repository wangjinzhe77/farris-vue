 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, SetupContext, ref, onMounted } from 'vue';
import { useResizeObserver } from '@vueuse/core';
import { WeatherProps, weatherProps } from './weather.props';
import getWeatherHome from './components/weather-home.component';
import { useWeatherColor } from './composition/use-weather-color';
import { useWeatherCharts } from './composition/use-weather-charts';
import { useWeatherData } from './composition/use-weather-data';
import './weather.css';
import FProgress from '@farris/ui-vue/components/progress';

export default defineComponent({
    name: 'FWeather',
    components: {
        "f-progress": FProgress
    },
    props: weatherProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: WeatherProps, context: SetupContext) {
        const elementRef = ref();
        /** 天气数据 */
        const data = ref(props.data.data);

        /** 颜色主题 */
        const weatherColor = useWeatherColor(props, context);
        /** 刷新图表 */
        const weatherCharts = useWeatherCharts(props, context);
        /** 加载数据 */
        const weatherData = useWeatherData(props, context);
        /** 加载主页面 */
        const { renderWeatherHome } = getWeatherHome(props, context);

        /** 背景样式 */
        const backgroundStyle = computed(() => {
            const styleObject = {
                'background-image': 'linear-gradient(132deg, ' + weatherColor.colorTheme.value[0] + ' 0%, ' + weatherColor.colorTheme.value[1] + ' 100%)'
            } as Record<string, any>;
            return styleObject;
        });

        /** 组件宽度改变 */
        function onWeatherBoradResize() {
            const weatherBoardWidth = elementRef.value.getBoundingClientRect().width;
            // 空气质量点位置
            const airPoint = document.getElementById("airPoint");
            const maxWidth = Math.max(elementRef.value.getBoundingClientRect().width, 400);
            if (airPoint) {
                airPoint.style.marginLeft = (data.value.default.air / 500 * (maxWidth - 50) - 5) + "px";
            }
            // 24h滚动条宽度
            const content = document.getElementById("f-weather-24h-content");
            if (content) {
                const max = content.scrollWidth - weatherBoardWidth - 130;
                if (content.offsetLeft < -max) { content.style.marginLeft = "-" + Math.max(max, 32) + "px"; }
            }
            // 7d预报宽度
            weatherCharts.renderChart7d();
        }

        onMounted(() => {
            // 监听容器宽度变化
            useResizeObserver(elementRef.value, onWeatherBoradResize);
            // 加载数据
            weatherData.fetchData(0, 0);
        });

        return () => {
            return (
                <div ref={elementRef} id="background"
                    class="f-weather-background" style={backgroundStyle.value}>
                    {/* 主页面 */}
                    {renderWeatherHome()}
                </div>
            );
        };
    }
});
