import { computed, ref, SetupContext } from 'vue';
import { WeatherProps } from '../weather.props';
import getWeatherDetail from './weather-detail.component';
import getWeatherCity from './weather-city.component';
import getWeatherSunset from './weather-sunset.component';
import getWeatherInformation from './weather-information.component';

/** 渲染主页面 */
export default function (props: WeatherProps, context: SetupContext) {
    /** 是否显示详细信息 */
    const isShowDetail = ref(false);

    /** 加载详细信息 */
    const { renderWeatherDetail } = getWeatherDetail(props, context);
    /** 加载城市信息 */
    const { renderWeatherCity } = getWeatherCity(props, context);
    /** 加载日落信息 */
    const { renderWeatherSunset } = getWeatherSunset(props, context);
    /** 加载主要数值信息 */
    const { renderWeatherInformation } = getWeatherInformation(props, context);

    /** 主页样式 */
    const weatherContainerStyle = computed(() => {
        const styleObject = {
            'border-bottom-left-radius': isShowDetail.value ? '0px' : null,
            'border-bottom-right-radius': isShowDetail.value ? '0px' : null
        } as Record<string, any>;
        return styleObject;
    });

    /** 详细信息页面样式 */
    const detailContainerStyle = computed(() => {
        const styleObject = {
            'display': isShowDetail.value ? 'block' : 'none'
        } as Record<string, any>;
        return styleObject;
    });

    /** 显示（隐藏）详细信息 */
    function showDetail() {
        isShowDetail.value = !isShowDetail.value;
    }

    /** 渲染页头 */
    function renderHeader() {
        return (
            <div class="f-weather-header">
                {/* 城市 */}
                {renderWeatherCity()}
                {/* 显示隐藏详细信息 */}
                <button class="f-weather-button f-weather-button-detail" onClick={showDetail}>
                    {isShowDetail.value ? <div>隐藏详细信息</div> : <div>显示详细信息</div>}
                </button>
            </div>
        );
    }

    /** 渲染主页面 */
    function renderWeatherHome() {
        return (
            <div>
                <div class="f-weather-container" style={weatherContainerStyle.value}>
                    {/* header */}
                    {renderHeader()}
                    {/* 日出日落进度条 */}
                    {renderWeatherSunset()}
                    {/* 主要数值信息 */}
                    {renderWeatherInformation()}
                </div>
                {/* 详细信息 */}
                <div style={detailContainerStyle.value}>
                    {renderWeatherDetail()}
                </div>
            </div>
        );
    }

    return {
        renderWeatherHome,
    };
}
