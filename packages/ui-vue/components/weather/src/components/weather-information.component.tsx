 
import { computed, ref, SetupContext } from 'vue';
import { WeatherProps } from '../weather.props';

/** 渲染主要数值信息 */
export default function (props: WeatherProps, context: SetupContext) {
    /** 主题列表 */
    const themeItems = props.data.themeItems.default;
    /** 主题参数 */
    const theme = ref(props.theme);
    /** 天气数据 */
    const data = ref(props.data.data);

    /** 预警提示样式 */
    const iconStyle = computed(() => {
        const styleObject = {
            'animation-name': 'icon',
            'animation-duration': '0.8s',
            'animation-iteration-count': 'infinite'
        } as Record<string, any>;
        return styleObject;
    });

    /** 空气质量等级 */
    const airLevel = computed(() => {
        return data.value.default.air <= 50 ? "优" :
            data.value.default.air <= 100 ? "良" :
                data.value.default.air <= 150 ? "轻度污染" :
                    data.value.default.air <= 200 ? "中度污染" :
                        data.value.default.air <= 300 ? "重度污染" :
                            "严重污染";
    });

    /** 预警信息 */
    const warning = computed(() => {
        // 是否雾霾
        return data.value.default.air > 300 ? { type: "smog", color: "red", title: "雾霾红色预警" } :
            data.value.default.air > 200 ? { type: "smog", color: "orange", title: "雾霾橙色预警" } :
                data.value.default.air > 150 ? { type: "smog", color: "yellow", title: "雾霾黄色预警" } :
                    // 是否高温
                    Math.max(...data.value.default.dayItemsSet[0].map((v: { temp: number }) => { return v.temp; })) > 40 ? { type: "highTemp", color: "red", title: "高温红色预警" } :
                        Math.max(...data.value.default.dayItemsSet[0].map((v: { temp: number }) => { return v.temp; })) > 37 ? { type: "highTemp", color: "orange", title: "高温橙色预警" } :
                            data.value.default.weekItems[0].max > 35 && data.value.default.weekItems[1].max > 35 && data.value.default.weekItems[2].max > 35 ? { type: "highTemp", color: "yellow", title: "高温黄色预警" } :
                                // 是否低温
                                Math.max(...data.value.default.dayItemsSet[0].map((v: { temp: number }) => { return v.temp; })) <= 0
                                    && data.value.default.dayItemsSet[0][0].temp > 16 ? { type: "lowTemp", color: "red", title: "低温红色预警" } :
                                    Math.max(...data.value.default.dayItemsSet[0].map((v: { temp: number }) => { return v.temp; })) <= 0
                                        && data.value.default.dayItemsSet[0][0].temp > 12 ? { type: "lowTemp", color: "orange", title: "低温橙色预警" } :
                                        Math.max(...data.value.default.dayItemsSet[0].map((v: { temp: number }) => { return v.temp; })) <= 4
                                            && data.value.default.dayItemsSet[0][0].temp > 14 ? { type: "lowTemp", color: "yellow", title: "低温黄色预警" } :
                                            null;
    });

    /** 渲染预警信息 */
    function renderWarning() {
        return (
            warning.value ?
                <span class="f-weather-warning-container">
                    <span class="f-weather-warning">
                        <img src={"/components/weather/src/image/warningImage/"
                            + (warning.value.type + "-" + warning.value.color) + ".png"}
                        class="f-weather-warning-icon"
                        style={warning.value ? iconStyle.value : ""} />
                        <span class="f-weather-warning-text"> {warning.value.title}</span>
                    </span>
                </span>
                : ""
        );
    }

    /** 渲染建议信息 */
    function renderAdvice() {
        return (
            <div class="f-weather-information-text f-weather-advice-container">
                {data.value.default.adviceItems.map((adviceItem: { key: string; value: { name: string; info: string; detail: string } }) => {
                    return themeItems[theme.value].adviceItems.includes(adviceItem.key) ?
                        <span class="f-weather-advice">
                            <span>{adviceItem.value.name}：{adviceItem.value.info}</span>
                            {themeItems[theme.value].adviceItems.indexOf(adviceItem.key) !== 2 ? <span class="f-weather-advice-line"></span> : ""}
                            <span class="f-weather-advice-text">{adviceItem.value.detail}</span>
                        </span>
                        : "";
                })}
            </div>
        );
    }

    /** 渲染空气质量条 */
    function renderAirProgress() {
        return (
            <div class="f-weather-air">
                <div class="f-weather-air-title">空气质量：{data.value.default.air + " - " + airLevel.value}</div>
                <div class="f-weather-air-progress">
                    <f-progress percent={100} enableBackgroundImg={true}
                        backgroundImg="linear-gradient(90deg, #01C84C 0%, #F7FF01 26%, #FF0000 53%, #800080 78%, #A52A2A 100%)"></f-progress>
                    <img src="/components/weather/src/image/point.png" id="airPoint"
                        class="f-weather-air-point" />
                </div>
            </div>
        );
    }

    /** 渲染主要数值信息 */
    function renderWeatherInformation() {
        return (
            <div class="f-weather-information-container">
                {/* 温度天气与预警 */}
                <div class="f-weather-information">
                    <div class="f-weather-information-row1">
                        {/* 温度 */}
                        <span class="f-weather-temperature">{data.value.default.temperature + " °"}</span>
                        {/* 天气 */}
                        <span class="f-weather-weather">{data.value.default.weather}</span>
                    </div>
                    <div class="f-weather-information-row2">
                        {/* 预警 */}
                        {renderWarning()}
                    </div>
                </div>
                {/* 最高最低温 */}
                <div class="f-weather-information-text f-weather-maxmin">{data.value.default.min + "° ~ " + data.value.default.max + "°"}</div>
                {/* 建议 */}
                {renderAdvice()}
                {/* 空气质量条 */}
                {renderAirProgress()}
            </div>
        );
    }

    return {
        renderWeatherInformation,
    };
}
