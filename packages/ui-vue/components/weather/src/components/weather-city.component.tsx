 
 
import { computed, ref, SetupContext } from 'vue';
import { WeatherProps } from '../weather.props';
import { useWeatherData } from '../composition/use-weather-data';
import { useWeatherColor } from '../composition/use-weather-color';

/** 渲染城市信息 */
export default function (props: WeatherProps, context: SetupContext) {
    /** 城市列表 */
    const cityItems = props.data.cityItems.default;

    /** 是否显示城市选择 */
    const isShowSelectCity = ref(false);
    /** 下拉菜单中选中的省份 */
    const selectedProvinceId = ref(0);
    /** 当前选中的省份 */
    const currentProvinceId = ref(0);
    /** 当前选中的城市 */
    const currentCityId = ref(0);

    /** 获取数据 */
    const weatherData = useWeatherData(props, context);
    /** 颜色主题 */
    const weatherColor = useWeatherColor(props, context);

    /** 提交城市选择按钮样式 */
    const submitButtonStyle = computed(() => {
        const styleObject = {
            'background-color': weatherColor.colorTheme.value[2]
        } as Record<string, any>;
        return styleObject;
    });

    /** 显示（隐藏）城市选择 */
    function showSelectCity() {
        isShowSelectCity.value = !isShowSelectCity.value;
        selectedProvinceId.value = 0;
    }

    /** 切换省份 */
    function switchProvince() {
        const selectedProvince = document.getElementById("provinceSelect") as any;
        selectedProvinceId.value = selectedProvince.selectedIndex;
    }

    /** 提交城市选择 */
    function submitCity() {
        // 更新省市id
        const selectedProvince = document.getElementById("provinceSelect") as any;
        const selectedCity = document.getElementById("citySelect") as any;
        currentProvinceId.value = selectedProvince.selectedIndex;
        currentCityId.value = selectedCity.selectedIndex;
        // 重置24h天气预报滚动位置
        const content24 = document.getElementById("f-weather-24h-content") as any;
        content24.style.marginLeft = -32 + "px";
        // 重置数据
        weatherData.fetchData(currentProvinceId.value, currentCityId.value);
        // 关闭城市选择窗口
        isShowSelectCity.value = false;
        // 重置城市选择
        selectedProvinceId.value = 0;
    }

    /** 渲染城市选择窗口 */
    function renderSelectCity() {
        return (
            isShowSelectCity.value ?
                <div class="f-weather-city-select">
                    {/* 选择省 */}
                    <select id="provinceSelect" onChange={switchProvince}>
                        {cityItems.map((provinceItem: { province: string }) => {
                            return <option>{provinceItem.province}</option>;
                        })}
                    </select>
                    {/* 选择市 */}
                    <select id="citySelect">
                        {cityItems[selectedProvinceId.value].city.map((cityItem: string) => {
                            return <option>{cityItem}</option>;
                        })}
                    </select>
                    {/* 确定按钮 */}
                    <button class="f-weather-button f-weather-button-city"
                        style={submitButtonStyle.value}
                        onClick={submitCity}>确定</button>
                </div>
                : ""
        );
    }

    /** 渲染城市信息 */
    function renderWeatherCity() {
        return (
            <div class="f-weather-city">
                {/* 显示城市 */}
                <span class="f-weather-city-title">
                    {cityItems[currentProvinceId.value].city[currentCityId.value] + "市"}
                </span>
                {/* 下拉选择城市 */}
                <img src="/components/weather/src/image/choose.png"
                    class="f-weather-city-icon"
                    onClick={showSelectCity} />
                {renderSelectCity()}
            </div>
        );
    }

    return {
        renderWeatherCity,
    };
}
