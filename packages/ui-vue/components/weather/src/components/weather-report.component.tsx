/* eslint-disable no-case-declarations */
 
 
import { ref, SetupContext, watch } from 'vue';
import { WeatherProps } from '../weather.props';
import { useWeatherFormatNumber } from '../composition/use-weather-format-number';
import { useWeatherCharts } from '../composition/use-weather-charts';

/** 渲染主页面 */
export default function (props: WeatherProps, context: SetupContext) {
    const elementRef = ref();
    /** 天气数据 */
    const data = ref(props.data.data);
    /** 当前选中的日期 */
    const currentDayId = ref(0);

    /** 格式化数字 */
    const weatherFormatNumber = useWeatherFormatNumber(props, context);
    /** 刷新图表 */
    const weatherCharts = useWeatherCharts(props, context);

    /** 切换日期 */
    function switchDay(id: number) {
        currentDayId.value = id;
    }

    /** 显示标题 */
    function showTitle(index: number) {
        switch (index) {
            case 0:
                return "今天";
            case 1:
                return "明天";
            default:
                const day = new Date(new Date().getTime() + 24 * 60 * 60 * 1000 * index).getDay();
                const str = "周";
                switch (day) {
                    case 0:
                        return str + "日";
                    case 1:
                        return str + "一";
                    case 2:
                        return str + "二";
                    case 3:
                        return str + "三";
                    case 4:
                        return str + "四";
                    case 5:
                        return str + "五";
                    case 6:
                        return str + "六";
                }
                break;
        }
    }

    /** 显示日期 */
    function showDate(index: number) {
        const time = new Date(new Date().getTime() + 24 * 60 * 60 * 1000 * index);
        return weatherFormatNumber.addZero(time.getMonth() + 1) + "/" + weatherFormatNumber.addZero(time.getDate());
    }

    /** 24h天气预报滚动左键 */
    function scrollLeft() {
        const weatherBoardWidth = elementRef.value.getBoundingClientRect().width;
        const add = weatherBoardWidth - 120;
        const content = document.getElementById("f-weather-24h-content");
        if (content) {
            if (content.offsetLeft + add > -32) {
                content.style.marginLeft = -32 + "px";
            }
            else {
                content.style.marginLeft = content.offsetLeft + add + "px";
            }
        }
    }

    /** 24h天气预报滚动右键 */
    function scrollRight() {
        const weatherBoardWidth = elementRef.value.getBoundingClientRect().width;
        const add = weatherBoardWidth - 120;
        const content = document.getElementById("f-weather-24h-content");
        if (content) {
            const max = content.scrollWidth - weatherBoardWidth - 130;
            if (content.offsetLeft - add < -max) {
                content.style.marginLeft = "-" + max + "px";
            }
            else {
                content.style.marginLeft = content.offsetLeft - add + "px";
            }
        }
    }

    /** 显示时间 */
    function showTime(dayIndex: number, hourIndex: number) {
        if (dayIndex === 0) {
            const time = new Date();
            if (hourIndex === 0) {
                return "现在";
            }
            else {
                return weatherFormatNumber.addZero((time.getHours() + hourIndex) % 24);
            }
        } else {
            return weatherFormatNumber.addZero(hourIndex);
        }
    }

    /** 当前选择的日期改变 */
    watch(currentDayId, () => {
        const content24 = document.getElementById("f-weather-24h-content") as any;
        content24.style.marginLeft = -32 + "px";
        weatherCharts.renderChart24h(currentDayId.value);
    });

    /** 渲染24h预报 */
    function render24hReport() {
        return (
            <div class="f-weather-24h-container">
                {/* 标题 */}
                <div class="f-weather-24h-title">24小时天气预报</div>
                {/* 左右按钮 */}
                <div class="f-weather-lrbutton-container">
                    <img src="/components/weather/src/image/arrow-left.png" class="f-weather-lrbutton-l"
                        onClick={scrollLeft} />
                    <img src="/components/weather/src/image/arrow-right.png" class="f-weather-lrbutton-r"
                        onClick={scrollRight} />
                </div>
                {/* 24h预报 */}
                <div class="f-weather-24h-content-scroll">
                    <div id="f-weather-24h-content">
                        {data.value.default.dayItemsSet.map((dayItems: any[], dayIndex: number) => {
                            return ((dayIndex) === currentDayId.value) ?
                                <div>
                                    <tr id={"report" + dayIndex} class="f-weather-24h-tr">
                                        {dayItems.map((dayItem: { icon: string; temp: string; }, hourIndex: number) => {
                                            return <td class="f-weather-24h-td">
                                                <div>{showTime(dayIndex, hourIndex)}</div>
                                                <img src={"http://mat1.gtimg.com/pingjs/ext2020/weather/pc/icon/weather/day/" + dayItem.icon + ".png"}
                                                    class="f-weather-24h-img"></img>
                                                <div>{dayItem.temp + "°"}</div>
                                            </td>;
                                        })}
                                    </tr>
                                </div>
                                : "";
                        })}
                        <div id="chart24h" class="f-weather-chart24h"></div>
                    </div>
                </div>
            </div>
        );
    }

    /** 渲染7d预报 */
    function render7dReport() {
        return (
            <div class="f-weather-7d-container">
                {/* 标题 */}
                <div class="f-weather-7d-title">七日天气预报</div>
                {/* 7d天气预报 */}
                <tr class="f-weather-7d-tr">
                    {data.value.default.weekItems.map((weekItem: { icon: string; max: string; min: string; }, index: number) => {
                        return <td id={"day" + index} class="f-weather-7d-td"
                            style={"background-color: " + (index === currentDayId.value ? "rgba(0, 0, 0, 0.05)" : "none")}
                            onClick={() => switchDay(index)}>
                            <div class="f-weather-7d-td-title">{showTitle(index)}</div>
                            <div class="f-weather-7d-td-date">{showDate(index)}</div>
                            <img src={"http://mat1.gtimg.com/pingjs/ext2020/weather/pc/icon/weather/day/" + weekItem.icon + ".png"}
                                class="f-weather-7d-td-img" />
                            <div class="f-weather-7d-td-margin">{weekItem.max + "°"}</div>
                            <div>{weekItem.min + "°"}</div>
                        </td>;
                    })}
                </tr>
                <div id="chart7d" class="f-weather-chart7d"></div>
            </div>
        );
    }

    /** 渲染天气预报 */
    function renderWeatherReport() {
        return (
            <div ref={elementRef}>
                {/* 24h预报 */}
                {render24hReport()}
                {/* 7d预报 */}
                {render7dReport()}
            </div>
        );
    }

    return {
        renderWeatherReport,
    };
}
