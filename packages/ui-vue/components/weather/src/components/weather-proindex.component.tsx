import { ref, SetupContext } from 'vue';
import { WeatherProps } from '../weather.props';

/** 渲染主页面 */
export default function (props: WeatherProps, context: SetupContext) {
    /** 天气数据 */
    const data = ref(props.data.data);

    /** 渲染其它参数 */
    function renderWeatherProindex() {
        return (
            <div>
                {data.value.default.proindexItemsSet.map((proindexItems: any[]) => {
                    return <tr class="f-weather-proindex-tr">
                        {proindexItems.map((proindexItem) => {
                            return <td class="f-weather-proindex-td">
                                <div class="f-weather-proindex-content-scroll">
                                    <div class="f-weather-proindex-content">
                                        {/* 参数图标 */}
                                        <img src={"/components/weather/src/image/proindexImage/" + proindexItem.icon + ".png"}
                                            class="f-weather-proindex-icon" />
                                        {/* 参数值 */}
                                        <div class="f-weather-proindex-text">
                                            <div class="f-weather-proindex-title">{proindexItem.title}</div>
                                            <div class="f-weather-proindex-value">{proindexItem.value}</div>
                                        </div>
                                        <div class="f-weather-proindex-line"></div>
                                        <br />
                                        {/* 参数详细信息 */}
                                        <div class="f-weather-proindex-info">{proindexItem.info}</div>
                                        <br />
                                        {/* 参数解释 */}
                                        <div>{proindexItem.explain}</div>
                                    </div>
                                </div>
                            </td>;
                        })}
                    </tr>;
                })}
            </div>
        );
    }

    return {
        renderWeatherProindex,
    };
}
