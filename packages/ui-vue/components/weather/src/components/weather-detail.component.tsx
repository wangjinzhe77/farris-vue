import { computed, ref, SetupContext } from 'vue';
import { WeatherProps } from '../weather.props';
import getWeatherReport from './weather-report.component';
import getWeatherProindex from './weather-proindex.component';
import { useWeatherColor } from '../composition/use-weather-color';

/** 渲染主页面 */
export default function (props: WeatherProps, context: SetupContext) {
    /** 是否显示天气预报 */
    const isShowReport = ref(true);

    /** 颜色主题 */
    const weatherColor = useWeatherColor(props, context);
    /** 加载天气预报 */
    const { renderWeatherReport } = getWeatherReport(props, context);
    /** 加载其它参数 */
    const { renderWeatherProindex } = getWeatherProindex(props, context);

    /** 天气预报按钮样式 */
    const reportButtonStyle = computed(() => {
        const styleObject = {
            'background-color': isShowReport.value ? weatherColor.colorTheme.value[2] : "",
            'color': isShowReport.value ? 'white' : ""
        } as Record<string, any>;
        return styleObject;
    });

    /** 其它参数按钮样式 */
    const proindexButtonStyle = computed(() => {
        const styleObject = {
            'background-color': !isShowReport.value ? weatherColor.colorTheme.value[2] : "",
            'color': !isShowReport.value ? 'white' : ""
        } as Record<string, any>;
        return styleObject;
    });

    /** 天气预报页面样式 */
    const reportContainerStyle = computed(() => {
        const styleObject = {
            'display': isShowReport.value ? 'block' : 'none'
        } as Record<string, any>;
        return styleObject;
    });

    /** 其它参数页面样式 */
    const proindexContainerStyle = computed(() => {
        const styleObject = {
            'display': !isShowReport.value ? 'block' : 'none'
        } as Record<string, any>;
        return styleObject;
    });

    /** 显示天气预报 */
    function showReport() {
        isShowReport.value = true;
    }

    /** 显示其它参数 */
    function showProindex() {
        isShowReport.value = false;
    }

    /** 渲染详细信息 */
    function renderWeatherDetail() {
        return (
            <div class="f-weather-container f-weather-detail">
                {/* 切换页面*/}
                <div style="display:flex">
                    <button class="f-weather-button f-weather-button-report" style={reportButtonStyle.value} onClick={showReport}>
                        天气预报</button>
                    <button class="f-weather-button f-weather-button-proindex" style={proindexButtonStyle.value} onClick={showProindex}>
                        其它参数</button>
                </div>
                {/* 天气预报 */}
                <div style={reportContainerStyle.value}>
                    {renderWeatherReport()}
                </div>
                {/* 其他参数 */}
                <div style={proindexContainerStyle.value}>
                    {renderWeatherProindex()}
                </div>
            </div>
        );
    }

    return {
        renderWeatherDetail,
    };
}
