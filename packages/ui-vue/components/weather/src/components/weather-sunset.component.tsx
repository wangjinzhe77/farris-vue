 
 
import { ref, SetupContext } from 'vue';
import { WeatherProps } from '../weather.props';
import { useWeatherFormatNumber } from '../composition/use-weather-format-number';

/** 渲染日落信息 */
export default function (props: WeatherProps, context: SetupContext) {
    /** 天气数据 */
    const data = ref(props.data.data);

    /** 格式化数字 */
    const weatherFormatNumber = useWeatherFormatNumber(props, context);

    /** 渲染日落信息 */
    function renderWeatherSunset() {
        return (
            <div>
                <div id="sunset" class="f-weather-sunset-progress"></div>
                <div class="f-weather-sunset-container">
                    {/* 日出日落图标 */}
                    <img src="/components/weather/src/image/sunrise.png"
                        class="f-weather-sunrise-img" />
                    <img src="/components/weather/src/image/sunset.png"
                        class="f-weather-sunset-img" />
                    {/* 日出日落时间 */}
                    <div>
                        <span class="f-weather-sunrise-text">
                            {weatherFormatNumber.addZero(data.value.default.sunriseTime.getHours()) + ":"
                                + weatherFormatNumber.addZero(data.value.default.sunriseTime.getMinutes())}</span>
                        <span class="f-weather-sunset-text">
                            {weatherFormatNumber.addZero(data.value.default.sunsetTime.getHours()) + ":"
                                + weatherFormatNumber.addZero(data.value.default.sunsetTime.getMinutes())}</span>
                    </div>
                </div>
            </div>
        );
    }

    return {
        renderWeatherSunset,
    };
}
