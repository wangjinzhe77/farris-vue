/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import weatherSchema from './schema/weather.schema.json';
import propertyConfig from './property-config/weather.property-config.json';
import { data } from './demo-data';

export const weatherProps = {
    /** 默认数据 */
    data: { type: Object, default: data },
    /** 主题参数 */
    theme: { type: Number, default: 1 },

} as Record<string, any>;

export type WeatherProps = ExtractPropTypes<typeof weatherProps>;

export const propsResolver = createPropsResolver<WeatherProps>(weatherProps, weatherSchema, schemaMapper, schemaResolver, propertyConfig);
