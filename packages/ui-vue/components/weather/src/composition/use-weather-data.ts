/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ref, SetupContext } from 'vue';
import { WeatherProps } from '../weather.props';
import { useWeatherCharts } from './use-weather-charts';
import jsonp from 'jsonp';
import CryptoJS from 'crypto-js';

export function useWeatherData(props: WeatherProps, context: SetupContext): WeatherProps {
    /** 城市列表 */
    const cityItems = props.data.cityItems.default;
    /** 天气数据 */
    const weatherData = ref(props.data.data);

    /** 刷新图表 */
    const weatherCharts = useWeatherCharts(props, context);

    /** 调用api获取数据 */
    function fetchData(provinceId: number, cityId: number) {
        // 返回气温、天气、日出日落、空气质量、建议
        const urlqq = "https://wis.qq.com/weather/common?source=pc&weather_type=observe|forecast_1h|forecast_24h|index|rise|air"
            + "&province=" + cityItems[provinceId].province
            + "&city=" + cityItems[provinceId].city[cityId];
        jsonp(urlqq, (err, data) => {
            if (err) {
                console.error('Error fetching data:', err);
            } else {
                const info = data.data;
                // 当前气温
                weatherData.value.default.temperature = info.observe.degree;
                // 当前最高最低温
                weatherData.value.default.max = info.forecast_24h[1].max_degree;
                weatherData.value.default.min = info.forecast_24h[1].min_degree;
                // 当前天气现象
                weatherData.value.default.weather = info.observe.weather;
                // 当前空气质量
                weatherData.value.default.air = info.air.aqi;
                // 今日日出日落时间
                weatherData.value.default.sunriseTime = new Date();
                weatherData.value.default.sunsetTime = new Date();
                weatherData.value.default.sunriseTime.setHours((Number)(info.rise[0].sunrise.split(":")[0]));
                weatherData.value.default.sunriseTime.setMinutes((Number)(info.rise[0].sunrise.split(":")[1]));
                weatherData.value.default.sunriseTime.setSeconds(0);
                weatherData.value.default.sunriseTime.setMilliseconds(0);
                weatherData.value.default.sunsetTime.setHours((Number)(info.rise[0].sunset.split(":")[0]));
                weatherData.value.default.sunsetTime.setMinutes((Number)(info.rise[0].sunset.split(":")[1]));
                weatherData.value.default.sunriseTime.setSeconds(0);
                weatherData.value.default.sunriseTime.setMilliseconds(0);
                // 当前建议
                weatherData.value.default.adviceItems.forEach((advice: { value: any; key: string }) => {
                    advice.value = info.index[advice.key];
                });
                // 紫外线指数
                weatherData.value.default.proindexItemsSet[0][0].value = info.index.ultraviolet.info;
                // 降水量
                weatherData.value.default.proindexItemsSet[0][1].value = info.observe.precipitation + "mm";
                // 7d天气预报
                let i;
                for (i = 1; i < 8; i++) {
                    weatherData.value.default.weekItems[i - 1].icon = info.forecast_24h[i].day_weather_code;
                    weatherData.value.default.weekItems[i - 1].max = info.forecast_24h[i].max_degree;
                    weatherData.value.default.weekItems[i - 1].min = info.forecast_24h[i].min_degree;
                }
                // 24h天气预报
                // d1
                for (i = 0; i < 24; i++) {
                    weatherData.value.default.dayItemsSet[0][i].icon = info.forecast_1h[i].weather_code;
                    weatherData.value.default.dayItemsSet[0][i].temp = info.forecast_1h[i].degree;
                }
                // d2
                for (i = 0; i < 24; i++) {
                    if (info.forecast_1h[i].update_time.endsWith("000000") && i !== 0) { break; }
                }
                i--;
                for (let j = 0; j < 24; i++, j++) {
                    weatherData.value.default.dayItemsSet[1][j].icon = info.forecast_1h[i].weather_code;
                    weatherData.value.default.dayItemsSet[1][j].temp = info.forecast_1h[i].degree;
                }
            }
        });
        // 返回其它参数
        const headers = new Headers();
        headers.append('User-Agent', 'Win64');
        const str = "location=" + cityItems[provinceId].city[provinceId]
            + "&public_key=P2yFGyRckTUNG9rFt"
            + "&ts=" + new Date().getTime()
            + "&ttl=3000";
        const key = "Sh3s--_vh1BWqUb_w";
        const result = CryptoJS.HmacSHA1(str, key);
        const sig = encodeURIComponent(result.toString(CryptoJS.enc.Base64));
        const urlxz = "https://api.seniverse.com/v3/weather/now.json?" + str + "&sig=" + sig;
        fetch(urlxz, {
            method: 'GET',
            headers,
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                const info = data.results[0].now;
                // 湿度
                weatherData.value.default.proindexItemsSet[1][0].value = info.humidity + "%";
                // 体感温度
                weatherData.value.default.proindexItemsSet[1][1].value = info.feels_like + "°";
                // 气压
                weatherData.value.default.proindexItemsSet[2][0].value = info.pressure + "mpa";
                // 能见度
                weatherData.value.default.proindexItemsSet[2][1].value = Math.round((Number)(info.visibility)) + "km";
            })
            .catch(error => {
                console.error('Error fetching data:', error);
            });
        setTimeout(() => {
            weatherCharts.renderCharts(0);
        }, 500);
    }

    return {
        fetchData,
    };
}
