/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ref, SetupContext } from 'vue';
import { WeatherProps } from '../weather.props';
import { useWeatherColor } from './use-weather-color';
import * as echarts from 'echarts';

export function useWeatherCharts(props: WeatherProps, context: SetupContext): WeatherProps {
    /** 天气数据 */
    const data = ref(props.data.data);

    /** 颜色主题 */
    const weatherColor = useWeatherColor(props, context);

    /** 渲染日落进度条 */
    function renderChartSunset() {
        let chartSunset = echarts.getInstanceByDom(document.getElementById("sunset") as any);
        if (!chartSunset) {
            chartSunset = echarts.init(document.getElementById("sunset"), null, {
                height: 170,
                width: 380
            });
        }
        chartSunset.setOption({
            series: [{
                data: [{
                    value: new Date().getTime() - data.value.default.sunriseTime.getTime(),
                    itemStyle: {
                        borderRadius: 10,
                        color: {
                            type: 'linear',
                            x: 1,
                            y: 0,
                            x2: 0,
                            y2: 0,
                            colorStops: [{
                                offset: 1, color: weatherColor.colorTheme.value ? weatherColor.colorTheme.value[3] : 'white'
                            }, {
                                offset: 0, color: 'white'
                            }]
                        }
                    }
                }, {
                    value: data.value.default.sunsetTime.getTime() - new Date().getTime(),
                    itemStyle: {
                        opacity: 0,
                    }
                }],
                type: 'pie',
                center: ["50%", "100%"],
                radius: ["200%", "190%"],
                startAngle: 180,
                endAngle: 360,
                label: {
                    show: false
                },
                emphasis: {
                    scale: false
                },
                // 占位圆
                emptyCircleStyle: {
                    borderType: 'dashed',
                },
            }, {
                type: "gauge",
                center: ["50%", "100%"],
                radius: "220%",
                startAngle: 180,
                endAngle: 360,
                // x轴刻度
                axisTick: {
                    show: false,
                },
                // x轴标签
                axisLabel: {
                    show: false,
                },
                axisLine: {
                    show: false
                },
                // 切分短线份数
                splitNumber: 20,
                // 单条短线
                splitLine: {
                    length: 3,
                    lineStyle: {
                        width: 10,
                        color: 'white'
                    }
                }
            }]
        });
    }
    /** 渲染24h温度折线图 */
    function renderChart24h(dayId: number) {
        let chart24h = echarts.getInstanceByDom(document.getElementById("chart24h") as any);
        if (!chart24h) {
            chart24h = echarts.init(document.getElementById("chart24h"), null, {
                width: 1885,
                height: 75
            });
        }
        chart24h.setOption({
            xAxis: {
                type: 'category',
                show: false
            },
            yAxis: {
                type: 'value',
                show: false
            },
            series: [{
                data: data.value.default.dayItemsSet[dayId].map((v: { temp: number }) => { return v.temp; }),
                type: 'line',
                itemStyle: {
                    color: 'white'
                }
            }]
        });
    }
    /** 渲染7d温度折线图 */
    function renderChart7d() {
        const background = document.getElementById("background");
        if (background) {
            const weatherBoardWidth = background.getBoundingClientRect().width;
            const chart7dWidth = Math.max(weatherBoardWidth, 400) * 1.11;
            let chart7d = echarts.getInstanceByDom(document.getElementById("chart7d") as any);
            if (!chart7d) {
                chart7d = echarts.init(document.getElementById("chart7d"), null, {
                    width: chart7dWidth,
                    height: 90
                });
            } else {
                chart7d.resize({
                    width: chart7dWidth,
                    height: 90
                });
            }
            chart7d.setOption({
                grid: {
                    left: 0
                },
                xAxis: {
                    type: 'category',
                    show: false
                },
                yAxis: {
                    type: 'value',
                    show: false
                },
                series: [{
                    data: data.value.default.weekItems.map((v: { min: number }) => { return v.min; }),
                    type: 'line',
                    itemStyle: {
                        color: '#5C90F9'
                    }
                }, {
                    data: data.value.default.weekItems.map((v: { max: number }) => { return v.max; }),
                    type: 'line',
                    itemStyle: {
                        color: '#FC9657'
                    }
                }]
            });
        }
    }
    /** 渲染空气质量条 */
    function renderAirProgress() {
        const airPoint = document.getElementById("airPoint");
        const background = document.getElementById("background");
        if (airPoint && background) {
            airPoint.style.marginLeft = (data.value.default.air / 500 * (background.getBoundingClientRect().width - 50) - 5) + "px";
        }
    }
    /** 渲染图表 */
    function renderCharts(dayId: number) {
        renderChartSunset();
        renderChart24h(dayId);
        renderChart7d();
        renderAirProgress();
    }

    return {
        renderChart24h,
        renderChart7d,
        renderCharts,
    };
}
