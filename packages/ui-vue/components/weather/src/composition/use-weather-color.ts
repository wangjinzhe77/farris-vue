/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, ref, SetupContext } from 'vue';
import { WeatherProps } from '../weather.props';

export function useWeatherColor(props: WeatherProps, context: SetupContext): WeatherProps {
    /** 天气数据 */
    const data = ref(props.data.data);

    /** 颜色主题 */
    const colorTheme = computed(() => {
        return new Date().getTime() - data.value.default.sunriseTime.getTime() < 0
            || data.value.default.sunsetTime.getTime() - new Date().getTime() < 0 ? ["#151276", "#7939e0", "#332396", "#814ca7"] :
            new Date().getTime() - data.value.default.sunriseTime.getTime() < 3600000 ? ["#49417b", "#f1b655", "#b1785b", "#d99877"] :
                data.value.default.sunsetTime.getTime() - new Date().getTime() < 3600000 ? ["#9e2e2e", "#f59b0b", "#c15334", "#ff7361"] :
                    ["#0273D7", "#76BAFF", "#388FFF", "#75C6FE"];
    });

    return {
        colorTheme,
    };
}
