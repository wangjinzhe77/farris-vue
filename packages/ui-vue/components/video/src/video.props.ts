/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, ref } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import videoSchema from './schema/video.schema.json';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import propertyConfig from './property-config/video.property-config.json';

export const videoProps = {
    src: {
        type: String,
        required: true,
        default: ''
    },
    autoplay: {
        type: Boolean,
        default: true
    },
    controls: {
        type: Boolean,
        default: true
    },
    loop: {
        type: Boolean,
        default: false
    },
    muted: {
        type: Boolean,
        default: false
    },
    preload: {
        type: String as () => 'none' | 'metadata' | 'auto',
        default: 'none'
    },
    poster: {
        type: String,
    },
    width: {
        type: [String, Number],
        default: 300
    },
    height: {
        type: [String, Number],
        default: 168
    }
} as Record<string, any>;

export type VideoProps = ExtractPropTypes<typeof videoProps>;

export const propsResolver = createPropsResolver<VideoProps>(videoProps, videoSchema, schemaMapper, schemaResolver, propertyConfig);
