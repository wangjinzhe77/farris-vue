/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * defination
 */
import { Ref, SetupContext, ref } from "vue";
import { VideoProps } from "../video.props";
import { UseVideo } from "./types";

export function useVideo(props: VideoProps, context: SetupContext): UseVideo {
    /**
     * 退出播放
     * @param $event event
     */
    function onAbort($event: Event) {
        $event.stopPropagation();
        context.emit('abort');
    }
    /**
     * 开始播放
     * @param $event event
     */
    function onPlay($event: Event) {
        $event.stopPropagation();
        context.emit('play');
    }
    function onPause($event: Event) {
        $event.stopPropagation();
        context.emit('pause');
    }
    function onEnded($event: Event) {
        $event.stopPropagation();
        context.emit('ended');
    }
    function onDurationchange($event: Event) {
        $event.stopPropagation();
        context.emit('durationchange');
    }
    function onError($event: Event) {
        $event.stopPropagation();
        context.emit('error');
    }
    function onLoadeddata($event: Event) {
        $event.stopPropagation();
        context.emit('loadeddata');
    }
    function onLoadedmetadata($event: Event) {
        $event.stopPropagation();
        context.emit('loadedmetadata');
    }
    function onLoadstart($event: Event) {
        $event.stopPropagation();
        context.emit('loadstart');
    }
    function onPlaying($event: Event) {
        $event.stopPropagation();
        context.emit('playing');
    }
    function onProgress($event: Event) {
        $event.stopPropagation();
        context.emit('progress');
    }
    function onTimeupdate($event: Event) {
        $event.stopPropagation();
        context.emit('timeupdate');
    }
    function onVolumechange($event: Event) {
        $event.stopPropagation();
        context.emit('volumechange');
    }
    function onCanplay($event: Event) {
        $event.stopPropagation();
        context.emit('canplay');
    }

    return {
        onAbort,
        onPlay,
        onPause,
        onEnded,
        onDurationchange,
        onError,
        onLoadeddata,
        onLoadedmetadata,
        onLoadstart,
        onPlaying,
        onProgress,
        onTimeupdate,
        onVolumechange,
        onCanplay
    };
}
