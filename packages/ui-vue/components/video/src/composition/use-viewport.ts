/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * defination
 */
import { computed, Ref, ref, watch } from 'vue';
import { VideoProps } from '../video.props';
import { UseViewport } from './types';

export function useViewport(props: VideoProps, elementRef: Ref<HTMLElement>): UseViewport {
    const width = ref(props.width);
    const height = ref(props.height);
    let resizeObserver: ResizeObserver;
    watch(elementRef, (element) => {
        if (!element) {
            return;
        }
        width.value = element.clientWidth;
        resizeObserver = new ResizeObserver(entries => {
            if (!entries || entries.length < 1) {
                return;
            }
            const entry = entries[0];
            const rect = entry.contentRect;
            if (!rect) {
                return;
            }
            width.value = rect.width;
        });
        resizeObserver.observe(element);
    });
    function destory() {
        if (resizeObserver) {
            resizeObserver.disconnect();
        }
    }
    return {
        destory,
        width,
        height,
    };
}
