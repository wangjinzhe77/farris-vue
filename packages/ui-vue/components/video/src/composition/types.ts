/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * defination
 */
import { Ref } from "vue";

export interface UseViewport {
    destory: () => void;
    width: Ref<number>;
    height: Ref<number>;
}
export interface UseVideo {
    onAbort: (event: Event) => void;
    onPlay: (event: Event) => void;
    onPause: (event: Event) => void;
    onEnded: (event: Event) => void;
    onDurationchange: (event: Event) => void;
    onError: (event: Event) => void;
    onLoadeddata: (event: Event) => void;
    onLoadedmetadata: (event: Event) => void;
    onLoadstart: (event: Event) => void;
    onPlaying: (event: Event) => void;
    onProgress: (event: Event) => void;
    onTimeupdate: (event: Event) => void;
    onVolumechange: (event: Event) => void;
    onCanplay: (event: Event) => void;
}
