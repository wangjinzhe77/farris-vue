/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * defination
 */
import { computed, defineComponent, onUnmounted, ref, SetupContext } from 'vue';
import { VideoProps, videoProps } from './video.props';
import { useViewport } from './composition/use-viewport';
import { useVideo } from './composition/use-video';

export default defineComponent({
    name: 'FVideo',
    props: videoProps,
    emits: [
        'abort',
        'play',
        'pause',
        'ended',
        'durationchange',
        'canplay',
        'error',
        'loadeddata',
        'loadedmetadata',
        'loadstart',
        'playing',
        'progress',
        'timeupdate',
        'volumechange'
    ] as (string[] & ThisType<void>) | undefined,
    setup(props: VideoProps, context: SetupContext) {
        const elementRef = ref();

        const {
            onAbort,
            onPlay,
            onPause,
            onEnded,
            onDurationchange,
            onError,
            onLoadeddata,
            onLoadedmetadata,
            onLoadstart,
            onPlaying,
            onProgress,
            onTimeupdate,
            onVolumechange,
            onCanplay
        } = useVideo(props, context);

        onUnmounted(() => {

        });
        return () => {
            return (
                <video
                    ref={elementRef}
                    src={props.src}
                    autoplay={props.autoplay}
                    controls={props.controls}
                    loop={props.loop}
                    muted={props.muted}
                    width={props.width}
                    height={props.height}
                    poster={props.poster}
                    onAbort={(event: Event) => onAbort(event)}
                    onPlay={(event: Event) => onPlay(event)}
                    onPause={(event: Event) => onPause(event)}
                    onEnded={(event: Event) => onEnded(event)}
                    onDurationchange={(event: Event) => onDurationchange(event)}
                    onError={(event: Event) => onError(event)}
                    onLoadeddata={(event: Event) => onLoadeddata(event)}
                    onLoadedmetadata={(event: Event) => onLoadedmetadata(event)}
                    onLoadstart={(event: Event) => onLoadstart(event)}
                    onPlaying={(event: Event) => onPlaying(event)}
                    onProgress={(event: Event) => onProgress(event)}
                    onTimeupdate={(event: Event) => onTimeupdate(event)}
                    onVolumechange={(event: Event) => onVolumechange(event)}
                    onCanplay={(event: Event) => onCanplay(event)}
                >
                    Your browser does not support the video tag.
                </video>
            );
        };
    }
});
