/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * defination
 */
import { defineComponent, inject, onMounted, onUnmounted, ref, SetupContext } from 'vue';
import { VideoProps, videoProps } from '../video.props';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { useDesignerRules } from '../../../component/src/designer/use-designer-rules';
import { useViewport } from '../composition/use-viewport';
import { DesignerHostService } from '../../../designer-canvas/src/composition/types';

export default defineComponent({
    name: 'FVideoDesign',
    props: videoProps,
    setup(props: VideoProps, context: SetupContext) {
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerHostService = inject('designer-host-service') as DesignerHostService;
        // jumphere by sagi
        const designerRulesComposition = useDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);
        const { width, height, destory } = useViewport(props, elementRef);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        onUnmounted(() => {
            destory();
        });
        context.expose(componentInstance.value);
        return () => {
            return (
                <div ref={elementRef} class="drag-container" data-dragref={`${designItemContext.schema.id}-container`}>
                    <video
                        src={props.src}
                        autoplay={props.autoplay}
                        controls={props.controls}
                        loop={props.loop}
                        muted={props.muted}
                        width={width.value}
                        height={height.value}>
                        Your browser does not support the video tag.
                    </video>
                </div>
            );
        };
    }
});
