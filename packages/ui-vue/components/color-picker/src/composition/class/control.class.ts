import { Color, ColorString } from './color.class';
import { Rgba } from './rgba.class';
import { Hsla } from './hsla.class';
import { Hsva } from './hsva.class';
import { Ref, ref, watch } from 'vue';
import { computedInject } from '@vueuse/core';

export enum ColorType {
    hex = 'hex',
    hexa = 'hexa',
    rgba = 'rgba',
    rgb = 'rgb',
    hsla = 'hsla',
    hsl = 'hsl',
    cmyk = 'cmyk'
}

export class ColorPickerControl {
    /** 所选颜色值 */
    private modelValue: Color = null as any;

    /** 色调颜色值 */
    private hueValue: Color = null as any;

    /** 初始颜色值 */
    private initValue: Color = null as any;

    public readonly valueChanged = ref<Color>();

    public initType: ColorType = null as any;
    // 此处没有监听变量变更后操作的方法  TODO
    public readonly alphaChannelVisibilityChanges = ref();

    private colorPresets: Array<Array<Color> | Color> = [];

    /** 确定输入颜色类型 */
    static finOutInputType: any;

    constructor(val: any) {
        // new Rgba(255, 0, 0, 1)
        const firstVal = val[0];
        const color = Color.from(firstVal);
        this.setValue(color);
        this.setHueColor(color);

    }

    /** 设置值 */
    public setValueFrom(color: ColorString | Color | Rgba | Hsla | Hsva, randomId: any, allowColorNull: boolean): this {
        /** 从给定的颜色字符串、颜色对象或RGBA/HSLA对象创建新颜色对象 */
        const newColor = Color.from(color);

        // 如果初始颜色值为空，则将当前颜色设置为初始颜色值
        if (!this.initValue) {
            this.initValue = Color.from(color);
        }

        if (typeof color === 'string') {
            this.finOutInputType(color);
        }
        this.setHueColor(newColor);
        this.setValue(newColor);
        return this;
    }

    private setHueColor(color: Color) {
        if (this.hueValue && color.getHsva().hue > 0 || !this.hueValue) {
            this.hueValue = new Color().setHsva(color.getHsva().hue);
        }
    }

    public get hue() {
        return this.hueValue;
    }

    public set hue(hueColor: Color) {
        this.hueValue = hueColor;
    }

    private setValue(value: Color): this {
        this.modelValue = value;
        if(value?.toRgbaString() === this.valueChanged.value?.toRgbaString()){
            this.valueChanged.value=value;
        }
        return this;
    }

    public get value(): Color {
        return this.modelValue;
    }

    public set value(value: Color) {
        this.setValue(value);
    }

    public reset(): this {
        let color;
        if (!this.initValue) {
            color = Color.from(new Rgba(255, 0, 0, 1));
            this.hueValue = new Color().setHsva(color.getHsva().hue);
        } else {
            color = this.initValue.clone();
            this.setHueColor(color);
        }

        this.setValue(color);
        return this;
    }

    public showAlphaChannel(): this {
        this.alphaChannelVisibilityChanges.value = true;
        return this;
    }

    private finOutInputType(colorString: ColorString) {
        const str = colorString.replace(/ /g, '').toLowerCase();
        if (str[0] === '#') {
            this.initType = ColorType.hex;
            if (str.length > 7) {
                this.initType = ColorType.hexa;
            }
        }

        const OpenParenthesis = str.indexOf('(');
        const colorTypeName = str.substr(0, OpenParenthesis);
        switch (colorTypeName) {
            case ColorType.rgba:
                this.initType = ColorType.rgba;
                break;
            case ColorType.rgb:
                this.initType = ColorType.rgb;
                break;
            case ColorType.hsla:
                this.initType = ColorType.hsla;
                break;
            case ColorType.hsl:
                this.initType = ColorType.hsl;
                break;
            case ColorType.cmyk:
                this.initType = ColorType.cmyk;
                break;
        }
    }

    /** 设置预定义颜色 */
    public setColorPresets(colorPresets: Array<Array<ColorString> | ColorString>): this {
        this.colorPresets = this.setPresets(colorPresets);
        return this;
    }

    private setPresets(colorPresets: Array<Array<ColorString> | ColorString>): Array<Color> {
        const presets: any = [];

        colorPresets.map((color) => {
            if (Array.isArray(color)) {
                presets.push(this.setPresets(color));
            } else {
                presets.push(new Color(color));
            }
        });
        return presets;
    }

    public get presets() {
        return this.colorPresets;
    }

    public hasPresets(): boolean {
        return this.colorPresets.length > 0;
    }
}
