import { Color } from './color.class';
import { ColorType } from './control.class';

/** 返回当前分类（hex/rgba等）下，对应的颜色值 */
export function getValueByType(color: Color | undefined, type: ColorType): string {
    if (!color) { return ''; }
    switch (type) {
        case ColorType.hex:
            return color.toHexString();
        case ColorType.hexa:
            return color.toHexString(true);
        case ColorType.rgb:
            return color.toRgbString();
        case ColorType.rgba:
            return color.toRgbaString();
        case ColorType.hsl:
            return color.toHslString();
        case ColorType.hsla:
            return color.toHslaString();
        default:
            return color.toRgbaString();
    }
}
