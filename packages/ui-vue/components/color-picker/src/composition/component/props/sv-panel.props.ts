import { ExtractPropTypes } from 'vue';

export const svPanelProps = {
    color: { type: Object, default: '' },

    hue: { type: Object, default: '' },

    randomId: { type: String, default: '' },

    allowColorNull: { type: Boolean, default: false }
};
export type SvPanelProps = ExtractPropTypes<typeof svPanelProps>;
