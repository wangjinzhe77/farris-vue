 
 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver/src/props-resolver';
import colorPickerSchema from './schema/color-picker.schema.json';
import { schemaResolver } from './schema/schema-resolver';
import { schemaMapper } from './schema/schema-mapper';
import propertyConfig from './property-config/color-picker.property-config.json';

/**
 * 属性
 */
export const colorPickerProps = {
    /** 初始颜色 */
    color: { type: String, default: '#e1e2e3' },
    /** 禁用 */
    disabled: { type: Boolean, default: false },
    /** 预置颜色 */
    presets: { type: Array<string>, default: [] },
    /** 允许颜色为空 */
    allowColorNull: { type: Boolean, default: false },
    /** 颜色值变化事件 */
    onValueChanged: { type: Function, default: () => { } }

} as Record<string, any>;
export type ColorPickerProps = ExtractPropTypes<typeof colorPickerProps>;

export const propsResolver = createPropsResolver<ColorPickerProps>(colorPickerProps, colorPickerSchema, schemaMapper, schemaResolver, propertyConfig);
