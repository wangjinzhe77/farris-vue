import { SetupContext, computed, defineComponent, ref, watch } from "vue";
import { SearchBoxContainerProps, searchBoxContainerProps } from "./search-box-container.props";
import FListView from '@farris/ui-vue/components/list-view';
import FLoading from '@farris/ui-vue/components/loading';

export default defineComponent({
    name: 'FSearchBoxContainer',
    props: searchBoxContainerProps,
    emits: ['confirmResult', 'searchedValue'] as (string[] & ThisType<void>) | undefined,
    setup(props: SearchBoxContainerProps, context: SetupContext) {
        const data = ref(props.data);
        const searchingResultListRef = ref<any>();
        const { slots } = context;
        const searchBoxContainerClass = computed(() => {
            const classObject = {
                'search-box-container': true,
                'f-utils-overflow-xhya': true,
                'position-relative': true
            } as Record<string, boolean>;
            return classObject;
        });
        function search(searchingText: string) {
            searchingResultListRef.value?.search(searchingText);
        }

        function onConfirmSearchResult(searchResult: any[]) {
            if (searchResult.length) {
                context.emit('confirmResult', searchResult[0].name);
                context.emit('searchedValue', searchResult);
            }
        }
        function renderContent(dataItem) {
            return slots.default ? <>{slots.default(dataItem)}</> : <span>{dataItem.item[props.textField]}</span>;
        }
        const containerStyle = computed(() => {
            const styleObject = {} as Record<string, any>;
            styleObject.padding = '8px';
            // 当height设置了值之后，优先识别
            if (props.maxHeight !== undefined && props.maxHeight > 0) {
                styleObject.maxHeight = `${props.maxHeight}px`;
            }
            return styleObject;
        });
        watch(() => props.loading, (newValue) => {
            // console.log('loading状态改变' + newValue);
        });

        watch(() => props.data, (newData) => {
            if (newData !== data.value) {
                data.value = newData;
            }
            searchingResultListRef.value.updateDataSource(newData);
        });
        context.expose({ search });

        return () => {
            const innerSlots = {
                content: renderContent
            };
            return (
                <div class={searchBoxContainerClass.value} style={containerStyle.value}>
                    <FLoading ref="loadingInstance" isActive={props.loading}></FLoading>
                    <FListView ref={searchingResultListRef}
                        itemClass="dropdown-item"
                        data={data.value}
                        view="ContentView"
                        onClickItem={(result: any) => onConfirmSearchResult(result.data)}
                        idField={props.idField}
                        textField={props.textField}
                        titleField={props.textField}
                        valueField={props.idField}
                        v-slots={innerSlots}
                    >
                    </FListView>
                </div>
            );
        };
    }
});
