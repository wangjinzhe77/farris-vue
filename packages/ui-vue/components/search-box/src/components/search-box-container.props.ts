import { ExtractPropTypes } from "vue";

export const searchBoxContainerProps = {
    data: { type: Array<any>, default: [] },
    /** 确认结果事件 */
    onConfirmResult: { type: Function, default: () => { } },
    /** 搜索点击后返回值 */
    onSearchedValue: { type: Function, default: () => { } },
     /**
    * 可选，数据源id字段
    * 默认为`id`
    */
     idField: { default: 'id', type: String },
     /**
     * 可选，数据源的title
     * 默认为`name`
     */
     textField: { default: 'name', type: String },
      /**
     * 可选，最大高度
     * 默认`350`
     */
    maxHeight: { default: 350, type: Number },
    loading:{default:false,type:Boolean}
};

export type SearchBoxContainerProps = ExtractPropTypes<typeof searchBoxContainerProps>;
