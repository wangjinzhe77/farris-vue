import { computed, defineComponent, ref, watch } from "vue";
import FButtonEdit from "@farris/ui-vue/components/button-edit";
import { SearchBoxProps, searchBoxProps } from "./search-box.props";
import SearchBoxContainer from './components/search-box-container.component';

export default defineComponent({
    name: 'FSearchBox',
    props: searchBoxProps,
    emits: ['update:modelValue', 'change', 'selectedValue', 'clickButton'] as (string[] & ThisType<void>) | undefined,
    setup(props: SearchBoxProps, context) {
        const data = ref(props.data);
        const buttonEditRef = ref<any>();
        const searchBoxContainerRef = ref<any>();
        const filterBoxPlaceholder = ref(props.placeholder);
        const loadingIcon = '<i class="f-icon f-icon-clockwise f-icon-spin"></i>';
        const searchIconContent = computed(() => props.loading ? loadingIcon : props.buttonContent);
        const editable = ref(props.editable);
        const searchValue = ref(props.modelValue);

        watch(searchValue, (searchingText: string) => {
            searchBoxContainerRef.value?.search(searchingText);
            context.emit('update:modelValue', searchingText);
        });

        watch(
            () => props.modelValue,
            (value: string) => {
                searchValue.value = value;
            }
        );

        function onConfirmResult(searchResult: any) {
            buttonEditRef.value.commitValue(searchResult);
        }

        function onFilterValueChange(newValue: string) {
            context.emit('change', newValue);
        }

        function returnValue(searchResult: any) {
            context.emit('selectedValue', searchResult);
        }

        function onClickButton(data: any) {
            context.emit('clickButton', data);
        }

        function togglePopup() {
            buttonEditRef.value.togglePopup();
        }

        context.expose({ togglePopup });

        return () => {
            const innerSlots = {
                default: context.slots.default
            };
            return <FButtonEdit ref={buttonEditRef} button-content={searchIconContent.value} custom-class={props.customClass}
                updateOn={props.updateOn} placeholder={filterBoxPlaceholder.value} onChange={(data: any) => onFilterValueChange(data)}
                onClickButton={(data: any) => onClickButton(data)} disable={props.disable}
                enable-clear={true} button-behavior="Execute" v-model={searchValue.value}
                popup-host={props.popupHost} popup-right-boundary={props.popupRightBoundary}
                popup-offset-x={props.popupOffsetX} popup-on-input={props.showDropdown} popup-on-focus={props.showDropdown}
                v-slots={context.slots}>
                {
                    props.showDropdown && <SearchBoxContainer ref={searchBoxContainerRef} data={props.data}
                        onConfirmResult={onConfirmResult} onSearchedValue={(data: any) => returnValue(data)}
                        v-slots={innerSlots} loading={props.loading} maxHeight={props.maxHeight} ></SearchBoxContainer>
                }
            </FButtonEdit>;
        };
    }
});
