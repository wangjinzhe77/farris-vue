 
import { ExtractPropTypes, PropType } from "vue";
import { createPropsResolver } from "../../dynamic-resolver";
import { schemaMapper } from "./schema/schema-mapper";
import searchBoxSchema from './schema/search-box.schema.json';
import { schemaResolver } from './schema/schema-resolver';
import propertyConfig from './property-config/search-box.property-config.json';

export const searchBoxProps = {
    /**
     * 自定义图标
     */
    buttonContent: { type: String, default: '<i class="f-icon f-icon-search"></i>' },
    /** 自定义样式 */
    customClass: { type: String, default: '' },
    /** 组件初始搜索值 */
    modelValue: { type: String, default: '' },

    popupHost: { type: Object as PropType<any> },
    /** */
    popupRightBoundary: { type: Object as PropType<any> },

    popupOffsetX: { type: Object as PropType<any> },
    /** 搜索框数据 */
    data: { type: Array<any>, default: [] },

    placeholder: { type: String, default: '请输入关键词' },
    /**
    * 可选，数据源id字段
    * 默认为`id`
    */
    idField: { default: 'id', type: String },
    /**
    * 可选，数据源的title
    * 默认为`name`
    */
    textField: { default: 'name', type: String },
    /**
    * 可选，最大高度
    * 默认`350`
    */
    maxHeight: { default: 350, type: Number },
    /**
     * 是否处于加载状态
     */
    loading: { default: false, type: Boolean },
    /**
     * 是否处于只读状态
     */
    disable: { default: false, type: Boolean },

    /**
     * 是否展示搜索结果面板
     */
    showDropdown: { default: true, type: Boolean },
    /**
     * popupOnFoucs TODO
     */
    /**
     * 值变化事件触发时机
     */
    updateOn: { default: 'blur', type: String as PropType<'change'|'blur'>}
} as Record<string, any>;

export type SearchBoxProps = ExtractPropTypes<typeof searchBoxProps>;

export const propsResolver = createPropsResolver<SearchBoxProps>(searchBoxProps, searchBoxSchema, schemaMapper, schemaResolver, propertyConfig);
