import { SetupContext, defineComponent, inject, onMounted, ref, watch } from "vue";
import { SearchBoxProps, searchBoxProps } from "../search-box.props";
import SearchBoxContainer from '../components/search-box-container.component';
import { useDesignerComponent } from "../../../designer-canvas/src/composition/function/use-designer-component";
import { DesignerItemContext } from "../../../designer-canvas/src/types";

export default defineComponent({
    name: 'FSearchBoxDesign',
    props: searchBoxProps,
    emits: ['update:modelValue', 'change'],
    setup(props: SearchBoxProps, context) {
        const data = ref(props.data);
        const buttonEditRef = ref<any>();
        const searchBoxContainerRef = ref<any>();
        const filterBoxPlaceholder = ref('请输入关键词');
        const searchIconContent = ref('<i class="f-icon f-icon-search"></i>');
        const searchValue = ref(props.modelValue);
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const componentInstance = useDesignerComponent(elementRef, designItemContext);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        watch(searchValue, (searchingText: string) => {
            searchBoxContainerRef.value?.search(searchingText);
            context.emit('update:modelValue', searchingText);
        });

        watch(
            () => props.modelValue,
            (value: string) => {
                searchValue.value = value;
            }
        );

        function onConfirmResult(searchResult: string) {
            buttonEditRef.value.commitValue(searchResult);
        }

        function onFilterValueChange(newValue: string) {
            context.emit('change', newValue);
        }

        return () => {
            return <div ref={elementRef}>
                <f-button-edit ref={buttonEditRef} button-content={searchIconContent.value}
                    placeholder={filterBoxPlaceholder.value} onChange={onFilterValueChange}
                    enable-clear={true} button-behavior="Execute" v-model={searchValue.value}
                    popup-host={props.popupHost} popup-right-boundary={props.popupRightBoundary}
                    popup-offset-x={props.popupOffsetX} popup-on-input={true} popup-on-focus={true}
                >
                    <SearchBoxContainer ref={searchBoxContainerRef} data={data.value}
                        onConfirmResult={onConfirmResult}></SearchBoxContainer>
                </f-button-edit>
            </div >;
        };
    }
});
