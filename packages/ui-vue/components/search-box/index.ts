 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App, Plugin } from 'vue';
import FSearchBox from './src/search-box.component';
import { propsResolver } from './src/search-box.props';
import FSearchBoxDesign from './src/designer/search-box.design.component';

export * from './src/search-box.props';

export { FSearchBox };

// export default {
//     install(app: App): void {
//         app.component(FSearchBox.name as string, FSearchBox);
//     },
//     register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
//         componentMap['search-box'] = FSearchBox;
//         propsResolverMap['search-box'] = propsResolver;
//     },
//     registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
//         componentMap['search-box'] = FSearchBoxDesign;
//         propsResolverMap['search-box'] = propsResolver;
//     }
// };
FSearchBox.install = (app: App) => {
    app.component(FSearchBox.name as string, FSearchBox);
};
FSearchBox.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap['search-box'] = FSearchBox;
    propsResolverMap['search-box'] = propsResolver;
};
FSearchBox.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['search-box'] = FSearchBoxDesign;
    propsResolverMap['search-box'] = propsResolver;
};

export default FSearchBox as typeof FSearchBox & Plugin;
