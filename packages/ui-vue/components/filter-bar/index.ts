 
import type { App } from 'vue';
import FFilterBar from './src/filter-bar.component';
import FFilterBarDesign from './src/designer/filter-bar.design.component';
import FFilterBarConfig from '../query-solution/src/designer/query-solution-config/query-solution-config.component';
import { propsResolver } from './src/filter-bar.props';
import { configPropsResolver } from './src/designer/filter-bar-config/filter-bar.resolver';

export * from './src/types';
export * from './src/filter-bar.props';

export { FFilterBar };

export default {
    install(app: App): void {
        app.component(FFilterBar.name as string, FFilterBar);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>): void {
        componentMap['filter-bar'] = FFilterBar;
        propsResolverMap['filter-bar'] = propsResolver;
        componentMap['filter-bar-config'] = FFilterBarConfig;
        propsResolverMap['filter-bar-config'] = configPropsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
        componentMap['filter-bar'] = FFilterBarDesign;
        propsResolverMap['filter-bar'] = propsResolver;
    }
};
