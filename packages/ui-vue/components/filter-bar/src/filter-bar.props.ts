 
import { ExtractPropTypes, PropType } from "vue";
import { Condition, FieldConfig } from "./types";
import { createPropsResolver } from "../../dynamic-resolver/src/props-resolver";
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import filterBarSchema from './schema/filter-bar.schema.json';
import propertyConfig from './property-config/filter-bar.property-config.json';

export type FilterMode = 'editable' | 'display-only';

export const filterBarProps = {
    /** 被绑定数据 */
    data: {
        type: Array<Condition>, default: [{
            id: 'name',
            fieldCode: 'name',
            fieldName: '名称',
            compareType: '0',
            valueType: 0,
            value: '示例1',
            relation: '1'
        }]
    },
    /** 筛选分类 */
    fields: {
        type: Array<FieldConfig>, default: [{ id: 'name', code: 'name', labelCode: 'name', name: '名称', editor: { type: 'text' } },
        ]
    },
    /** 是否可选择 */
    mode: { type: String as PropType<FilterMode>, default: 'editable' },
    /** 清空值文字 */
    resetText: { type: String, default: '清空' },
    /** 是否显示重置 */
    showReset: { type: Boolean, default: false },
} as Record<string, any>;

export type FilterBarProps = ExtractPropTypes<typeof filterBarProps>;

export const propsResolver = createPropsResolver<FilterBarProps>(filterBarProps, filterBarSchema, schemaMapper, schemaResolver, propertyConfig);
