 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';

import { FieldConfig } from '../../../../condition/src/types';
import { createPropsResolver } from '../../../../dynamic-resolver';
import querySolutionConfigSchema from './schema/filter-bar-config.schema.json';
import propertyConfig from './property-config/filter-bar-config.property-config.json';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';

export type TransFerItem = {
    id: string,
    name: string
}
export const querySolutionConfigProps = {
    fields: { type: Array<FieldConfig>, default: [] },
    modelValue: { type: Array<FieldConfig>, default: [] }

} as Record<string, any>;

export type QuerySolutionConfigProps = ExtractPropTypes<typeof querySolutionConfigProps>;

export const configPropsResolver = createPropsResolver<QuerySolutionConfigProps>(querySolutionConfigProps, querySolutionConfigSchema, schemaMapper, schemaResolver, propertyConfig);
