import { EditorConfig } from "../../dynamic-form";
import { Condition as ConditionExternal, FieldConfig as FieldConfigExternal } from "../../condition/src/types";

export interface FilterItem {
    id: string;
    fieldCode: string;
    fieldName: string;
    editor: EditorConfig;
    displayText?: string;
    hasValue?: boolean;
};

export type Condition = Pick<ConditionExternal, 'id' | 'fieldCode' | 'fieldName' | 'compareType' | 'valueType' | 'value'>;
export type FieldConfig = FieldConfigExternal;
