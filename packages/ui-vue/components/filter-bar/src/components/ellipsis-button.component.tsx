import { SetupContext } from 'vue';
import { FilterBarProps } from '../filter-bar.props';

export default function (props: FilterBarProps, context: SetupContext) {
    function expandFilter() {}

    return () => {
        return (
            <div class="f-filter-list-ellipsis" onClick={(payload: MouseEvent) => expandFilter()}>
                ...
            </div>
        );
    };
}
