 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import listNavSchema from './schema/list-nav.schema.json';
import propertyConfig from './property-config/list-nav.property-config.json';

type positionValue = 'top' | 'left' | 'bottom' | 'right';

export const listNavProps = {
    /** 位置 */
    position: { Type: String as PropType<positionValue>, default: 'left' },
    /** listNav名称 */
    title: { Type: String, default: '' },
    /** 宽度或者高度 */
    size: { Type: Number || String, default: 218 },
    /** 是否启用收折 */
    collapsible: { Type: Boolean, default: true },
    /** 初始收折状态 */
    folded: { Type: Boolean, default: false },
    /** 控件禁用状态 */
    disabled: { Type: Boolean, default: false },
    /** 是否启用拖拽 TODO */
    draggable: { Type: Boolean, default: false }
} as Record<string, any>;

export type ListNavProps = ExtractPropTypes<typeof listNavProps>;

export const propsResolver = createPropsResolver<ListNavProps>(listNavProps, listNavSchema, schemaMapper, schemaResolver, propertyConfig);
