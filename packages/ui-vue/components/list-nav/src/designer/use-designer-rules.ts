 
 
import { DesignerHostService, DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { useDragulaCommonRule } from "../../../designer-canvas/src/composition/rule/use-dragula-common-rule";
import { DesignerItemContext } from "../../../designer-canvas/src/types";
import { UseTemplateDragAndDropRules } from "../../../designer-canvas/src/composition/rule/use-template-rule";

export function useDesignerRules(designItemContext: DesignerItemContext, designerHostService?: DesignerHostService): UseDesignerRules {
    const dragAndDropRules = new UseTemplateDragAndDropRules();
    const { canMove, canAccept, canDelete } = dragAndDropRules.getTemplateRule(designItemContext, designerHostService);

    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {

        const basalRule = useDragulaCommonRule().basalDragulaRuleForContainer(draggingContext, designerHostService);
        if (!basalRule) {
            return false;
        }
        return canAccept;
    }


    function getStyles() {
        return 'display: flex;flex-direction:column;height:100%';;
    }


    function checkCanMoveComponent() {
        return canMove;
    }
    function checkCanDeleteComponent() {
        return canDelete;
    }

    function hideNestedPaddingInDesginerView() {
        return !canMove && !canDelete;
    }

    return {
        canAccepts,
        getStyles,
        checkCanMoveComponent,
        checkCanDeleteComponent,
        hideNestedPaddingInDesginerView
    };
}
