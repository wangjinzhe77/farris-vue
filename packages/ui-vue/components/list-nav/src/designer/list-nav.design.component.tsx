 /**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, inject, onMounted, ref, SetupContext } from 'vue';
import { listNavProps, ListNavProps } from '../list-nav.props';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerRules } from './use-designer-rules';
import { DesignerHostService } from '../../../designer-canvas/src/composition/types';

export default defineComponent({
    name: 'FListNavDesign',
    props: listNavProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: ListNavProps, context: SetupContext) {
        const elementRef = ref();
        const designerHostService = inject<DesignerHostService>('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);
        componentInstance.value.canMove = false;
        componentInstance.value.canNested = false;
        /** 导航位置 */
        const navPosition = ref(props.position);
        /** 标题 */
        const title = ref(props.title);
        const hideNav = ref(false);
        const afterInitAnimatClass = ref(false);
        /**
         * 判断尺寸
         */
        const navStyleSize = computed(() => {
            const result = {};
            const propName = ['top', 'bottom'].indexOf(navPosition.value) > -1 ? 'height' : 'width';
            result[propName] = !hideNav.value ? props.size + 'px' : '0px';
            return result;
        });

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        return () => {
            return (
                <div class={`f-list-nav f-list-nav-${navPosition.value}`}>
                    <div class="f-list-nav-in" style={navStyleSize.value}>
                        <div class="f-list-nav-main">
                            {context.slots.navHeader && <div class="f-list-nav-header">{context.slots.navHeader()}</div>}
                            {!context.slots.navHeader && title.value && (
                                <div class="f-list-nav-header">
                                    <div class="f-list-nav-title">{title.value}</div>
                                </div>
                            )}
                            {context.slots.navContent && <div class="f-list-nav-content">{context.slots.navContent()}</div>}
                            {context.slots.navFooter && <div class="f-list-nav-footer">{context.slots.navFooter()}</div>}
                        </div>
                        {props.collapsible && <div class={['f-list-nav-toggle-sidebar', { 'disabled': props.disabled, 'active': hideNav.value, 'splitter-pane-collapse-animate': afterInitAnimatClass.value }]}>
                            <span class="triangle"></span>
                        </div>}
                    </div>
                </div >
            );
        };
    }
});
