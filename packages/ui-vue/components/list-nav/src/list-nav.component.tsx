 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, ref, SetupContext } from 'vue';
import { listNavProps, ListNavProps } from './list-nav.props';


export default defineComponent({
    name: 'FListNav',
    props: listNavProps,
    emits: ['collapse'] as (string[] & ThisType<void>) | undefined,
    setup(props: ListNavProps, context: SetupContext) {
        /** 导航位置 */
        const navPosition = ref(props.position);
        /** 标题 */
        const title = ref(props.title);
        /** 初始时，是否隐藏Nav */
        const hideNav = ref(props.folded);
        const afterInitAnimatClass = ref(false);
        /**
         * 切换收折状态
         * @param event 
         * @returns 
         */
        function toggleCollaspe(event) {
            event && event.stopPropagation();
            if (props.disabled) {
                return;
            }
            // 初始没有加上这个样式，是避免初始设置收起的时候，看到有収折的动画
            afterInitAnimatClass.value = true;
            hideNav.value = !hideNav.value;
            context.emit('collapse', hideNav.value);
        }
        /**
         * 判断尺寸
         */
        const navStyleSize = computed(() => {
            const result = {};
            const propName = ['top', 'bottom'].indexOf(navPosition.value) > -1?'height':'width';
            result[propName] = !hideNav.value ? props.size + 'px' : '0px';
            return result;
        });
        return () => {
            return (
                <div class={`f-list-nav f-list-nav-${navPosition.value}`}>
                    <div class="f-list-nav-in" style={navStyleSize.value}>
                        <div class="f-list-nav-main">
                            {context.slots.navHeader && <div class="f-list-nav-header">{context.slots.navHeader()}</div>}
                            {!context.slots.navHeader && title.value && (
                                <div class="f-list-nav-header">
                                    <div class="f-list-nav-title">{title.value}</div>
                                </div>
                            )}
                            {context.slots.navContent && <div class="f-list-nav-content">{context.slots.navContent()}</div>}
                            {context.slots.navFooter && <div class="f-list-nav-footer">{context.slots.navFooter()}</div>}
                        </div>
                        {props.collapsible && <div class={['f-list-nav-toggle-sidebar', { 'disabled': props.disabled, 'active': hideNav.value, 'splitter-pane-collapse-animate': afterInitAnimatClass.value }]} onClick={(event: MouseEvent) => toggleCollaspe(event)}>
                            <span class="triangle"></span>
                        </div>}
                    </div>
                </div >
            );
        };
    }
});
