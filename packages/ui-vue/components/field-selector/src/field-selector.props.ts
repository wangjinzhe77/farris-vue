import { ExtractPropTypes } from "vue";
import { DataColumn } from '../../../components/data-view';
import { createPropsResolver } from "../../dynamic-resolver";
import fieldSelectorSchema from './schema/field-selector.schema.json';

const defaultColumns = [
    { field: 'code', title: '绑定字段' }, { field: 'name', title: '名称' }, { field: 'fieldType', title: '字段类型' }
];

export interface FieldSelectorService {
    getData(params: any): Promise<any>;
}

export const FieldSelectorRepositoryToken = Symbol('Field_Selector Component Repository Service Token');

export const fieldSelectorProps = {
    disabled: { type: Boolean, default: false },
    readonly: { type: Boolean, default: false },
    modelValue: { type: [String, Array], default: null },
    data: { type: Array<Record<string, any>>, default: [] },
    idField: { type: String, default: 'id' },
    columns: { type: Array<DataColumn>, default: defaultColumns },
    title: { type: String, default: '字段选择器' },
    modalWidth: { type: Number, default: 800 },
    modalHeight: { type: Number, default: 600 },
    multiSelect: { type: Boolean, default: false },
    /**
     * 可选，分隔符
     * 默认`,`
     */
    separator: { type: String, default: ',' },
    bindingData: { type: Array<Record<string, any>>, default: [] },
    textField: { type: String, default: 'code' },
    beforeOpenDialog: { type: Function, default: null },
    editorParams: { type: Object , default: null },
    onSubmitModal: { type: Function, default: null },
    onFieldSelected: { type: Function, default: null },
    repositoryToken: { type: Symbol, default: null },


} as Record<string, any>;

export type FieldSelectorProps = ExtractPropTypes<typeof fieldSelectorProps>;

export const propsResolver = createPropsResolver<FieldSelectorProps>(fieldSelectorProps, fieldSelectorSchema);
