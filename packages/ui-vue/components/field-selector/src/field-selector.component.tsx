import { SetupContext, ref, defineComponent, computed, watch, onMounted, inject } from "vue";
import FButtonEdit from '../../button-edit/src/button-edit.component';
import FieldSelectorComponent from './components/field-selector-container.component';
import { FieldSelectorProps, fieldSelectorProps, FieldSelectorService } from "./field-selector.props";
import { F_NOTIFY_SERVICE_TOKEN, FNotifyService } from "../../notify";


export default defineComponent({
    name: 'FFieldSelector',
    props: fieldSelectorProps,
    emits: ['selected'],
    setup(props: FieldSelectorProps, context) {
        const buttonIcon = '<i class="f-icon f-icon-lookup"></i>';
        const bindingData = ref(props.bindingData);
        const dataSource = ref(props.data);
        const fieldSelectorRef = ref();
        const notifyService: FNotifyService = inject(F_NOTIFY_SERVICE_TOKEN) as FNotifyService;
        
        if (notifyService) {
            notifyService.globalConfig = { position: 'top-center' };
        }

        const repository = inject<FieldSelectorService>(props.repositoryToken);
        watch(() => props.data, (newData) => {
            dataSource.value = newData;
        });

        const displayText = ref(props.modelValue);

        function onFieldSelected(selectedFields: Array<Record<string, any>>) {
            bindingData.value = selectedFields;
        }

        function setDisplayText() {
            let displayValue = '';
            if (bindingData.value && bindingData.value.length) {
                displayValue = bindingData.value.map((item) => item[props.textField]).join(props.separator);
            }
            displayText.value = displayValue;
        }

        watch(() => props.modelValue, (newValue) => {
            if (typeof newValue === 'string') {
                displayText.value = newValue;
            } else {
                setDisplayText();
            }
        });

        function loadData(data: any[]) {
            dataSource.value = data;
        }

        function renderFieldSelector() {
            return <FieldSelectorComponent ref={fieldSelectorRef}
                data={dataSource.value}
                columns={props.columns}
                onSelected={onFieldSelected}
                modelValue={props.modelValue}
                idField={props.idField}
                bindingData={bindingData.value}></FieldSelectorComponent>;
        }

        function checkBindingData(): boolean {
            if (!bindingData.value || !bindingData.value.length) {
                notifyService.warning({ message: '请先配置数据源！' });
                return false;
            }
            return true;
        }

        const modalOptions = {
            title: props.title,
            fitContent: false,
            height: props.modalHeight,
            width: props.modalWidth,
            minWidth: 300,
            minHeight: 200,
            showMaxButton: true,
            buttons: [
                {
                    name: 'cancel',
                    text: '取消',
                    class: 'btn btn-secondary',
                    handle: ($event: MouseEvent) => {
                        return true;
                    }
                },
                {
                    name: 'accept',
                    text: '确定',
                    class: 'btn btn-primary',
                    handle: ($event: MouseEvent) => {
                        if (!checkBindingData()) {
                            return false;
                        }
                        setDisplayText();
                        // 非表单自带属性面板处使用，期望自己处理相关逻辑时，通过onSubmitModal控制
                        // result 无返回时时终止后续操作，存在返回值继续  
                        if (props.onSubmitModal && typeof props.onSubmitModal === 'function') {
                            const result = props.onSubmitModal(bindingData.value);
                            if (!result) {
                                return true;
                            }
                        }
                        if (props.onFieldSelected && typeof props.onFieldSelected == 'function') {
                            props.onFieldSelected(bindingData.value);
                        }
                        return true;
                    }
                }
            ],
            resizeable: true,
            draggable: true
        };

        function onBeforeOpen() {
            const helpId = props.editorParams?.propertyData?.helpId;
            if (!helpId) {
                notifyService?.warning({message: '请先配置数据源！'});
                return false;
            }

            if (props.beforeOpenDialog) {
                props.beforeOpenDialog({ instance: { loadData } });
                return;
            }

            if (repository) {
                repository?.getData(props.editorParams).then(data => loadData(data));
            }
        }

        return () => {
            return (<FButtonEdit
                v-model={displayText.value}
                editable={false}
                disabled={props.disabled}
                readonly={props.readonly}
                inputType={"text"}
                enableClear={false}
                buttonContent={buttonIcon}
                buttonBehavior={"Modal"}
                modalOptions={modalOptions}
                beforeOpen={onBeforeOpen}>
                <div class="h-100 d-flex flex-column">
                    {renderFieldSelector()}
                </div>
            </FButtonEdit>);
        };
    }
});
