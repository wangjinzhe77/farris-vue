import { defineComponent, ref, watch } from "vue";
import { FieldSelectorProps, fieldSelectorProps } from "../field-selector.props";
import { FTreeGrid } from '../../../tree-grid';
import { RowOptions, VisualData } from "../../../data-view";
import { FormSchemaEntityField$Type } from "../../../common/entity/entity-schema";

export default defineComponent({
    name: 'FFieldSelectorContainer',
    props: fieldSelectorProps,
    emits: ['selected', 'bindingTypeChange'],
    setup(props: FieldSelectorProps, context) {

        const dataSource = ref(props.data);
        const bindingData = ref(props.bindingData);
        const treegridRef = ref();

        watch(() => props.data, (newData) => {
            dataSource.value = newData;
            treegridRef.value.updateDataSource(newData);

            if (props.modelValue) {
                treegridRef.value.selectItemById(props.modelValue);
            }
        });

        const treegridContainerStyle = {
            'f-utils-fill': true,
            'm-2': props.bindingType?.enable,
            'mx-2': !props.bindingType?.enable,
            'border': true
        };

        function onSelectionChange(selectedItems: Array<Record<string, any>>) {
            bindingData.value = selectedItems;
            context.emit('selected', selectedItems);
        }


        const rowOption: Partial<RowOptions> = {
            customRowStatus: (visualData: VisualData) => {
                if (visualData.raw.$type !== FormSchemaEntityField$Type.SimpleField) {
                    visualData.disabled = true;
                }
                return visualData;
            }
        };

        function renderTreeGrids() {
            return <FTreeGrid
                ref={treegridRef}
                fit={true}
                data={dataSource.value}
                idField={props.idField}
                columns={props.columns}
                rowNumber={{ enable: false }}
                columnOption={{ fitColumns: true }}
                onSelectionChange={onSelectionChange}
                rowOption={rowOption}></FTreeGrid>;
        }

        return () => {
            return (<div class="h-100 d-flex flex-column">
                <div class={treegridContainerStyle} style="position:relative;border-radius:10px;">{renderTreeGrids()}</div>
            </div>);
        };
    }
});
