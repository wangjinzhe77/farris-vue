 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App } from 'vue';
import FieldSelector from './src/field-selector.component';
import FFieldSelectorContainer from './src/components/field-selector-container.component';
import { propsResolver } from './src/field-selector.props';

export * from './src/field-selector.props';

export { FieldSelector };

export default {
    install(app: App): void {
        app.component(FieldSelector.name as string, FieldSelector);
        app.component(FFieldSelectorContainer.name as string, FFieldSelectorContainer);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>): void {
        componentMap['field-selector'] = FieldSelector;
        propsResolverMap['field-selector'] = propsResolver;
    },
};
