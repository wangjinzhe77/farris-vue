 
import { ExtractPropTypes, PropType } from "vue";
// import { createPropsResolver } from "../../dynamic-resolver";
// import { schemaMapper } from "./schema/schema-mapper";
// import borderEditorSchema from './schema/border-editor.schema.json';
// import { schemaResolver } from './schema/schema-resolver';
// import propertyConfig from './property-config/border-editor.property-config.json';

export const borderEditorProps = {
    /** 边框线型列表 */
    defaultBorderStyles: {
        type: Array, default: [
            { id: 'none', name: '无' },
            { id: 'solid', name: '实线' },
            { id: 'dotted', name: '点线' },
            { id: 'dashed', name: '虚线' },
        ]
    },
    /** 边框线型 */
    styles: { type: Array, default: ['none', 'none', 'none', 'none', 'none'] },
    /** 边框宽度 */
    widths: { type: Array, default: [0, 0, 0, 0, 0] },
    /** 边框颜色 */
    colors: { type: Array, default: ['#000000', '#000000', '#000000', '#000000', '#000000'] },
    /** 边框圆角 */
    radiuses: { type: Array, default: [0, 0, 0, 0, 0] },

} as Record<string, any>;

export type BorderEditorProps = ExtractPropTypes<typeof borderEditorProps>;

// export const propsResolver = createPropsResolver<BorderEditorProps>(borderEditorProps, borderEditorSchema, schemaMapper, schemaResolver, propertyConfig);
