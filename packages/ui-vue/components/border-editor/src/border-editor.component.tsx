import { computed, defineComponent, ref, SetupContext } from 'vue';
import { BorderEditorProps, borderEditorProps } from './border-editor.props';
import './border-editor.css';
import getBorderEditorBasic from './components/border-editor-basic.component';
import getBorderEditorRadius from './components/border-editor-radius.component';

export default defineComponent({
    name: 'FBorderEditor',
    props: borderEditorProps,
    emits: ['valueChanged'] as (string[] & ThisType<void>) | undefined,
    setup(props: BorderEditorProps, context: SetupContext) {
        /** 是否显示边框编辑器 */
        const isShowBorderCustom = ref(false);

        /** 加载基本属性编辑器 */
        const { renderBorderEditorBasic } = getBorderEditorBasic(props, context);
        /** 加载圆角编辑器 */
        const { renderBorderEditorRadius } = getBorderEditorRadius(props, context);

        /** 标题箭头样式 */
        const borderIconClass = computed(() => {
            const classObject = {
                'f-icon': true,
                'mr-1': true,
                'f-border-editor-icon': true,
                'f-icon-arrow-60-right': !isShowBorderCustom.value,
                'f-icon-arrow-60-down': isShowBorderCustom.value
            } as Record<string, boolean>;
            return classObject;
        });

        /** 显示（隐藏）边框编辑器 */
        function showBorderCustom() {
            isShowBorderCustom.value = !isShowBorderCustom.value;
        }

        return () => {
            return <div>
                <div class="f-border-editor-container">
                    {/* 标题 */}
                    <div style="display: flex">
                        <div class="f-border-editor-title"
                            style="width: 80px">
                            <div class={borderIconClass.value}
                                onClick={showBorderCustom} />
                            <div>边框</div>
                        </div>
                    </div>
                    {/* 边框编辑器 */}
                    {isShowBorderCustom.value ?
                        <div>
                            {/* 设置基本属性 */}
                            {renderBorderEditorBasic()}
                            {/* 设置圆角 */}
                            {renderBorderEditorRadius()}
                        </div>
                        : ""}
                </div>
            </div>;
        };
    }
});
