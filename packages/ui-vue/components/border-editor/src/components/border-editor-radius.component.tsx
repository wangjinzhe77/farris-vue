import { computed, nextTick, ref, SetupContext } from 'vue';
import { BorderEditorProps } from '../border-editor.props';
import FNumberSpinner from '@farris/ui-vue/components/number-spinner';

export default function (props: BorderEditorProps, context: SetupContext) {
    /** 边框线型 */
    const styles = ref(props.styles);
    /** 边框宽度 */
    const widths = ref(props.widths);
    /** 边框颜色 */
    const colors = ref(props.colors);
    /** 边框圆角 */
    const radiuses = ref(props.radiuses);
    /** 当前圆角方向 */
    const currentRadiusDirection = ref(4);
    /** 是否显示圆角编辑器 */
    const isShowRadiusCustom = ref(true);

    /** 圆角方向键样式 */
    const radiusDirectionStyle = computed(() => {
        return function (index: number) {
            const styleObject = {
                background: currentRadiusDirection.value === index ?
                    'linear-gradient(rgba(0, 0, 0, .4), rgba(0, 0, 0, .2))' : 'rgba(0, 0, 0, 0)'
            } as Record<string, any>;
            return styleObject;
        };
    });
    /** 目标样式 */
    const targetStyle = computed(() => {
        const styleObject = {
            borderStyle: styles.value.slice(0, 4).join(' '),
            borderWidth: widths.value.slice(0, 4).join('px ') + 'px',
            borderColor: colors.value.slice(0, 4).join(' '),
            borderRadius: radiuses.value.slice(0, 4).join('px ') + 'px',
        } as Record<string, any>;
        return styleObject;
    });

    /** 设置圆角方向 */
    function onRadiusDirectionButtonClicked(direction: number) {
        currentRadiusDirection.value = direction;
        isShowRadiusCustom.value = false;
        nextTick(() => {
            isShowRadiusCustom.value = true;
        });
    }
    /** 设置边框圆角 */
    function onBorderRadiusChanged(item: any) {
        if (currentRadiusDirection.value < 4)
            {radiuses.value[currentRadiusDirection.value] = item;}
        else
            {radiuses.value.fill(item);}
        context.emit('valueChanged', targetStyle.value);
    }

    /** 渲染方向设置 */
    function renderDirectionSetter() {
        return (
            <div class="f-border-editor-directions">
                <div>
                    <div style={radiusDirectionStyle.value(0)}
                        class="f-border-editor-direction"
                        title="左上角"
                        onClick={() => onRadiusDirectionButtonClicked(0)}>┏</div>
                    <div style={radiusDirectionStyle.value(3)}
                        class="f-border-editor-direction"
                        title="左下角"
                        onClick={() => onRadiusDirectionButtonClicked(3)}>┗</div>
                </div>
                <div style={radiusDirectionStyle.value(4)}
                    class="f-border-editor-direction"
                    title="全部"
                    onClick={() => onRadiusDirectionButtonClicked(4)}>╋</div>
                <div>
                    <div style={radiusDirectionStyle.value(1)}
                        class="f-border-editor-direction"
                        title="右上角"
                        onClick={() => onRadiusDirectionButtonClicked(1)}>┓</div>
                    <div style={radiusDirectionStyle.value(2)}
                        class="f-border-editor-direction"
                        title="右下角"
                        onClick={() => onRadiusDirectionButtonClicked(2)}>┛</div>
                </div>
            </div>
        );
    }

    /** 渲染样式设置 */
    function renderStyleSetter() {
        return (
            <div class="f-border-editor-customs">
                <div class="f-border-editor-custom">
                    <div class="f-border-editor-title">圆角(px)</div>
                    <FNumberSpinner style="flex: 1 1 0"
                        min={0}
                        v-model={radiuses.value[currentRadiusDirection.value]}
                        onChange={onBorderRadiusChanged}
                        class="f-border-editor-input"></FNumberSpinner>
                </div>
            </div>
        );
    }

    /** 渲染圆角编辑器 */
    function renderBorderEditorRadius() {
        return (
            <div>
                {isShowRadiusCustom.value ?
                    <div class="f-border-editor-custom-container">
                        {/* 设置方向 */}
                        {renderDirectionSetter()}
                        {/* 设置样式 */}
                        {renderStyleSetter()}
                    </div>
                    : ""}
            </div>
        );
    };

    return {
        renderBorderEditorRadius,
    };
}
