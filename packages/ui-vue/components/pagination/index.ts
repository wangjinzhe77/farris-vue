 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { App } from 'vue';
import FPagination from './src/pagination.components';
import FPaginationDesign from './src/designer/pagination.design.components';
import { propsResolver } from './src/pagination.props';
import { withInstall } from '@farris/ui-vue/components/common';

export * from './src/pagination.props';

FPagination.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.pagination = FPagination;
    propsResolverMap.pagination = propsResolver;
};
FPagination.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.pagination = FPaginationDesign;
    propsResolverMap.pagination = propsResolver;
};

export { FPagination };
export default withInstall(FPagination);
