
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver } from "../../dynamic-resolver";
import { schemaMapper } from "./schema/schema-mapper";
import paginationSchema from './schema/pagination.schema.json';
import propertyConfig from './property-config/pagination.property-config.json';
import { schemaResolver } from './schema/schema-resolver';

export type PaginationMode = 'default' | 'simple';

export const paginationProps = {
    /**
     * 当前页码
     */
    currentPage: { type: Number, default: 1 },
    /**
     * 分页模式
     */
    mode: { type: String as PropType<PaginationMode>, default: 'default' },
    /**
     * 每页数量
     */
    pageSize: { type: Number, default: 20 },
    /**
     * 总数量
     */
    totalItems: { type: Number, default: 0 },
    /**
     * 是否显示跳转按钮
     */
    showGoButton: { type: Boolean, default: false },
    /**
     * 是否展示每个数量下拉
     */
    showPageList: { type: Boolean, default: true },
    /**
     * 是否展示页码
     */
    showPageNumbers: { type: Boolean, default: true },
    /**
     * 是否展示跳转到哪一页
     */
    showRedirection: { type: Boolean, default: true },
    /**
     * 自定义每页数量
     */
    pageList: { type: Array<number>, default: [20, 50, 100] }
} as Record<string, any>;

export type PaginationPropsType = ExtractPropTypes<typeof paginationProps>;

export const propsResolver = createPropsResolver<PaginationPropsType>(paginationProps, paginationSchema, schemaMapper, schemaResolver, propertyConfig);
