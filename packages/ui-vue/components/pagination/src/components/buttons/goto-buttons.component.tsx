/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ComputedRef, ref, Ref, SetupContext, watch } from 'vue';

export default function (
    currentPage: Ref<number>,
    lastPage: ComputedRef<number>,
    currentPageSize: Ref<number>,
    context: SetupContext
) {
    const gotoPrefix = ref('跳转至');
    const gotoSuffix = ref('');
    const pageNumber = ref(currentPage.value);
    watch(pageNumber, (value: number, previousValue: number) => {
        if (value >= 1 && value <= lastPage.value) {
            currentPage.value = value;
        } else {
            pageNumber.value = previousValue;
        }
    });
    // 实现pageNumber响应式变化
    watch(currentPage, (newValue: number) => {
        if (pageNumber.value !== newValue) {
            pageNumber.value = newValue;
        }
    });
    // 失去焦点  触发变更
    function onBlur($event: any) {
        pageNumber.value = ($event.target as any).valueAsNumber;
        context.emit('update:currentPage', pageNumber.value);
        context.emit('changed', { pageIndex: pageNumber.value, pageSize: currentPageSize.value });
        context.emit('pageIndexChanged', { pageIndex: pageNumber.value, pageSize: currentPageSize.value });
    }

    function goto($event: KeyboardEvent) {
        if ($event.key === 'Enter') {
            pageNumber.value = ($event.target as any).valueAsNumber;
            // 回车  触发变更
            context.emit('update:currentPage', pageNumber.value);
            context.emit('changed', { pageIndex: pageNumber.value, pageSize: currentPageSize.value });
            context.emit('pageIndexChanged', { pageIndex: pageNumber.value, pageSize: currentPageSize.value });
        }
    }

    function renderGotoButton() {
        return (
            <li class="page-goto-input d-flex flex-row" style="padding-left: 10px; white-space: nowrap;">
                <span class="pagination-message">{gotoPrefix.value}</span>
                <input title="page-index-spinner"
                    type="number"
                    class="form-control farris-gotopagenumber"
                    value={pageNumber.value}
                    min={1}
                    max={lastPage.value}
                    style="display: inline-block;margin-left:3px;"
                    onBlur={(event) => onBlur(event)}
                    onKeyup={(payload: KeyboardEvent) => goto(payload)} />
                {gotoSuffix.value}
            </li>
        );
    }

    return { renderGotoButton };
}
