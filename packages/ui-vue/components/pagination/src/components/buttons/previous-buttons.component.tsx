/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, Ref, SetupContext } from 'vue';

export default function (
    currentPage: Ref<number>,
    isFirstPage: Ref<boolean>,
    shouldShowFirstPageLink: Ref<boolean>,
    currentPageSize: Ref<number>,
    context: SetupContext
) {
    const firstPageClass = computed(() => {
        const classObject = {
            'page-item': true,
            disabled: isFirstPage.value
        } as Record<string, boolean>;
        return classObject;
    });

    function onClickToNavigateToFirstPage($event: MouseEvent) {
        currentPage.value = 1;
        context.emit('update:currentPage', currentPage.value);
        context.emit('changed', { pageIndex: currentPage.value, pageSize: currentPageSize.value });
        context.emit('pageIndexChanged', { pageIndex: currentPage.value, pageSize: currentPageSize.value });
    }

    function onClickToNavigateToPreviousPage($event: MouseEvent) {
        currentPage.value = currentPage.value > 2 ? currentPage.value - 1 : 1;
        context.emit('update:currentPage', currentPage.value);
        context.emit('changed', { pageIndex: currentPage.value, pageSize: currentPageSize.value });
        context.emit('pageIndexChanged', { pageIndex: currentPage.value, pageSize: currentPageSize.value });
    }

    function renderFirstPage() {
        return (
            <li class={firstPageClass.value}>
                {shouldShowFirstPageLink.value && (
                    <a tabindex="0" class="page-link" onClick={(payload: MouseEvent) => onClickToNavigateToFirstPage(payload)}>
                        <span class="f-icon f-page-first"></span>
                    </a>
                )}
                {isFirstPage.value && (
                    <span class="page-link">
                        <span class="f-icon f-page-first"></span>
                    </span>
                )}
            </li>
        );
    }

    function renderPreviousPage() {
        return (
            <li class={firstPageClass.value}>
                {shouldShowFirstPageLink.value && (
                    <a tabindex="0" class="page-link" onClick={(payload: MouseEvent) => onClickToNavigateToPreviousPage(payload)}>
                        <span class="f-icon f-page-pre"></span>
                    </a>
                )}
                {isFirstPage.value && (
                    <span class="page-link">
                        <span class="f-icon f-page-pre"></span>
                    </span>
                )}
            </li>
        );
    }

    return { renderFirstPage, renderPreviousPage };
}
