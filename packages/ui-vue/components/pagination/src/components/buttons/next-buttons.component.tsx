/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, ComputedRef, Ref, SetupContext } from 'vue';

export default function (
    currentPage: Ref<number>,
    isLastPage: Ref<boolean>,
    lastPage: ComputedRef<number>,
    currentPageSize: Ref<number>,
    context: SetupContext
) {
    const lastPageClass = computed(() => {
        const classObject = {
            'page-item': true,
            disabled: isLastPage.value
        } as Record<string, boolean>;
        return classObject;
    });

    function onClickToNavigateToNextPage($event: MouseEvent) {
        currentPage.value = currentPage.value < lastPage.value ? currentPage.value + 1 : lastPage.value;
        context.emit('update:currentPage', currentPage.value);
        context.emit('changed', { pageIndex: currentPage.value, pageSize: currentPageSize.value });
        context.emit('pageIndexChanged', { pageIndex: currentPage.value, pageSize: currentPageSize.value });
    }

    function onClickToNavigateToLastPage($event: MouseEvent) {
        currentPage.value = lastPage.value;
        context.emit('update:currentPage', currentPage.value);
        context.emit('changed', { pageIndex: currentPage.value, pageSize: currentPageSize.value });
        context.emit('pageIndexChanged', { pageIndex: currentPage.value, pageSize: currentPageSize.value });
    }

    function renderNextPage() {
        return (
            <li class={lastPageClass.value}>
                {!isLastPage.value && (
                    <a class="page-link" tabindex="0" onClick={(payload: MouseEvent) => onClickToNavigateToNextPage(payload)}>
                        <span class="f-icon f-page-next"></span>
                    </a>
                )}
                {isLastPage.value && (
                    <span class="page-link">
                        <span class="f-icon f-page-next"></span>
                    </span>
                )}
            </li>
        );
    }

    function renderLastPage() {
        return (
            <li class={lastPageClass.value}>
                {!isLastPage.value && (
                    <a class="page-link" tabindex="0" onClick={(payload: MouseEvent) => onClickToNavigateToLastPage(payload)}>
                        <span class="f-icon f-page-last"></span>
                    </a>
                )}
                {isLastPage.value && (
                    <span class="page-link">
                        <span class="f-icon f-page-last"></span>
                    </span>
                )}
            </li>
        );
    }

    return { renderLastPage, renderNextPage };
}
