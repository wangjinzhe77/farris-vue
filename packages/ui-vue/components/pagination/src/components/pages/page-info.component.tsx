/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, ref, Ref } from 'vue';

export default function (position: Ref<string>, totalItems: Ref<number>) {
    const prefixTotalItems = ref('共');
    const suffixTotalItems = ref('条');

    const pageInfoClass = computed(() => {
        const classObject = {
            'pagination-message': true,
            'text-truncate': true,
            'd-flex': true,
            'ml-auto': position.value === 'right',
            'flex-fill': position.value === 'right'
        } as Record<string, boolean>;
        return classObject;
    });

    function renderPageInfo() {
        return (
            <li class={pageInfoClass.value}>
                <div class="text-truncate" style="line-height: 26px">
                    <span class="pg-message-text">{prefixTotalItems.value}</span>
                    <b class="pg-message-total">{totalItems.value}</b>
                    <span class="pg-message-text">{suffixTotalItems.value}</span>
                </div>
            </li>
        );
    }

    return { renderPageInfo };
}
