/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, ref, SetupContext, watch, watchEffect } from 'vue';
import { paginationProps, PaginationPropsType } from './pagination.props';
import getGotoButton from './components/buttons/goto-buttons.component';
import getNextButtons from './components/buttons/next-buttons.component';
import getPageInfo from './components/pages/page-info.component';
import getPageList from './components/pages/page-list.component';
import getPageNumber from './components/pages/page-number.component';
import getPreviousButtons from './components/buttons/previous-buttons.component';
import { usePages } from './composition/use-pages';

export default defineComponent({
    name: 'FPagination',
    props: paginationProps,
    emits: ['changed', 'pageIndexChanged', 'pageSizeChanged',
        'update:currentPage', 'update:pageSize'] as (string[] & ThisType<void>) | undefined,
    setup(props: PaginationPropsType, context: SetupContext) {
        const responsive = ref(false);
        const viewModel = ref(props.mode);
        const position = ref('');
        const autoHide = ref(false);
        const currentPageSize = ref(props.pageSize);
        const currentPage = ref(props.currentPage);
        const totalItems = ref(props.totalItems);
        const { pages, updatePages } = usePages(props);

        const isFirstPage = computed(() => {
            return currentPage.value === 1;
        });

        const pageList = computed(() => {
            return props.pageList;
        });

        const lastPage = computed(() => {
            return Math.ceil(totalItems.value / currentPageSize.value);
        });

        const isLastPage = computed(() => {
            return currentPage.value === lastPage.value;
        });

        const shouldShowGoButton = computed(() => {
            return props.showGoButton;
        });

        const shouldShowFirstPageLink = computed(() => {
            return currentPage.value > 1;
        });

        const shouldShowNavigation = computed(() => {
            return !(autoHide.value && pages.value.length <= 1);
        });

        const shouldShowPageInfo = computed(() => {
            return true;
        });

        const shouldShowPageList = computed(() => {
            return props.showPageList;
        });

        const shouldShowPageNumbers = computed(() => {
            return props.showPageNumbers;
        });

        const shouldShowRedirectionLinks = computed(() => {
            return true;
        });

        const { renderFirstPage, renderPreviousPage } = getPreviousButtons(currentPage, isFirstPage,
            shouldShowFirstPageLink, currentPageSize, context);
        const { renderLastPage, renderNextPage } = getNextButtons(currentPage, isLastPage, lastPage, currentPageSize, context);
        const { renderPageInfo } = getPageInfo(position, totalItems);
        const { renderPageList } = getPageList(currentPage, currentPageSize, pageList, totalItems, context);
        const { renderPageNumbers } = getPageNumber(currentPage, pages, currentPageSize, context);
        const { renderGotoButton } = getGotoButton(currentPage, lastPage, currentPageSize, context);

        updatePages(currentPage.value, currentPageSize.value, totalItems.value, 7);

        watch(() => props.totalItems, (newTotal) => {
            totalItems.value = newTotal;
        });

        watch([currentPage, currentPageSize, totalItems], () => {
            updatePages(currentPage.value, currentPageSize.value, totalItems.value, 7);
        });

        watch(currentPage, (newPageIndex: number, oldPageIndex: number) => {
            if (newPageIndex !== oldPageIndex) {
                // context.emit('update:currentPage', currentPage.value);
                // context.emit('pageIndexChanged', { pageIndex: currentPage.value, pageSize: currentPageSize.value });
            }
        });

        watch(currentPageSize, (newPageSize: number, oldPageSize: number) => {
            if (newPageSize !== oldPageSize) {
                // context.emit('update:pageSize', currentPageSize.value);
                // context.emit('pageSizeChanged', { pageIndex: currentPage.value, pageSize: currentPageSize.value });
            }
        });

        watch(() => props.totalItems, (newValue) => {
            totalItems.value = newValue;
            updatePages(currentPage.value, currentPageSize.value, totalItems.value, 7);
        });
        watch(() => props.currentPage, (newCurrentPage: number, oldCurrentPage: number) => {
            if (newCurrentPage !== oldCurrentPage) {
                currentPage.value = newCurrentPage;
            }
        });

        watch(() => props.pageSize, (newPageSize: number, oldPageSize: number) => {
            if (newPageSize !== oldPageSize) {
                currentPageSize.value = newPageSize;
            }
        });
        // 初始化立即执行此副作用，设置当前每页数量，不追踪依赖，执行完销毁
        const stop = watchEffect(() => {
            const hasPageSize = pageList.value.includes(currentPageSize.value);
            currentPageSize.value = hasPageSize ? currentPageSize.value : pageList.value[0];
            if (!hasPageSize) {
                // 此处如果每页数量改变了 需要双向绑定
                context.emit('update:pageSize', currentPageSize.value);
                context.emit('changed', { pageIndex: currentPage.value, pageSize: currentPageSize.value });
                context.emit('pageSizeChanged', { pageIndex: currentPage.value, pageSize: currentPageSize.value });
            }
        });
        stop();

        const paginationClass = computed(() => {
            const classObject = {
                'd-flex': true,
                'flex-wrap': true,
                'justify-content-end': true,
                'w-100': true,
                'ngx-pagination': true,
                pagination: true,
                responsive: responsive.value,
                'pager-viewmode-default': viewModel.value === 'default',
                'pager-viewmode-simple': viewModel.value === 'simple'
            } as Record<string, boolean>;
            return classObject;
        });

        const paginationStyle = computed(() => {
            const styleObject = {
                position: 'relative',
                'justify-content': position.value === 'center' ? 'center' : 'start'
            } as Record<string, any>;
            return styleObject;
        });

        function renderDefaultPagination() {
            return (
                <>
                    {shouldShowPageInfo.value && renderPageInfo()}
                    {shouldShowPageList.value && renderPageList()}
                    {shouldShowRedirectionLinks.value && renderFirstPage()}
                    {shouldShowRedirectionLinks.value && renderPreviousPage()}
                    {shouldShowPageNumbers.value && renderPageNumbers()}
                    {shouldShowRedirectionLinks.value && renderNextPage()}
                    {shouldShowRedirectionLinks.value && renderLastPage()}
                    {shouldShowGoButton.value && renderGotoButton()}
                </>
            );
        }

        function renderSimplePagination() {
            return (
                <>
                    <li class="page-item d-flex flex-fill"></li>
                    {shouldShowRedirectionLinks.value && renderFirstPage()}
                    {shouldShowRedirectionLinks.value && renderPreviousPage()}
                    {renderGotoButton()}
                    <li class="page-item page-separator" style="margin-left: 10px">
                        <span style="font-size: 15px; font-weight: 200;"> /</span>
                    </li>
                    <li class="page-item page-total" style="margin-left: 5px">
                        <span style="font-size: 16px; font-weight: 600;"> {lastPage.value}</span>
                    </li>
                    {shouldShowRedirectionLinks.value && renderNextPage()}
                    {shouldShowRedirectionLinks.value && renderLastPage()}
                </>
            );
        }

        return () => {
            return (
                <div class="pagination-container">
                    {shouldShowNavigation.value && (
                        <ul role="navigation" class={paginationClass.value} style={paginationStyle.value}>
                            {viewModel.value === 'default' ? renderDefaultPagination() : renderSimplePagination()}
                        </ul>
                    )}
                </div>
            );
        };
    }
});
