import { Ref } from "vue";
import { Tag } from "../tags.props";

export interface TagsInnerElement {
    type: 'Tag' | 'Input' | 'AddButton';
    payload?: Tag;
}

export interface UseDraggable {

    dragstart: (e: DragEvent, item: any, index: number) => void;

    dragenter: (e: DragEvent, index: number) => void;

    dragover: (e: DragEvent, index: number) => void;

    dragend: (e: DragEvent, item: any) => void;

    isDragging: Ref<boolean>;
}
