 
import { ExtractPropTypes, PropType } from 'vue';
import { schemaResolver } from './schema/schema-resolver';
import { schemaMapper } from './schema/schema-mapper';
import propertyConfig from './property-config/tags.property-config.json';
import { createPropsResolver } from '../../dynamic-resolver/src/props-resolver';
import tagsSchema from './schema/tags.schema.json';

export interface Tag {
    name: string;
    checked?: boolean;
    closable?: boolean;
    color?: string;
    selectable?: boolean;
    size?: string;
    tagType?: string;
    value?: string;
}

export type TagStyleType = 'default' | 'capsule';

export const tagsProps = {
    activeTag: { type: String, default: '' },
    addButtonText: { type: String, default: '' },
    customClass: { type: String, default: '' },
    customStyle: { type: String, default: '' },
    data: { type: Array<any>, default: [{ name: '示例1' },] },
    enableAddButton: { type: Boolean, default: false },
    selectable: { type: Boolean, default: false },
    showAddButton: { type: Boolean, default: false },
    showClose: { type: Boolean, default: true },
    showColor: { type: Boolean, default: false },
    tagType: { type: String, default: '' },
    tagStyle: { type: String as PropType<TagStyleType>, default: '' },
    wrapText: { type: Boolean, default: false },
    draggable: { type: Boolean, default: false },
    disable: { type: Boolean,default:false },
} as Record<string, any>;

export type TagsProps = ExtractPropTypes<typeof tagsProps>;
export const propsResolver = createPropsResolver<TagsProps>(tagsProps, tagsSchema, schemaMapper, schemaResolver, propertyConfig);
