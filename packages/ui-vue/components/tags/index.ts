 
 
import { withInstall } from '@farris/ui-vue/components/common';
import FTags from './src/tags.component';
import { propsResolver } from './src/tags.props';
import FTagsDesign from './src/designer/tags.design.component';

export * from './src/tags.props';

FTags.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>): void => {
    componentMap.tags = FTags;
    propsResolverMap.tags = propsResolver;
};

FTags.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void => {
    componentMap.tags = FTagsDesign;
    propsResolverMap.tags = propsResolver;
};

export { FTags };

export default withInstall(FTags);
