/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, inject, onMounted, ref, SetupContext } from 'vue';
import { accordionItemProps, AccordionItemProps } from './accordion-item.props';
import { AccordionProps } from '../accordion.props';

export default defineComponent({
    name: 'FAccordionItem',
    props: accordionItemProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: AccordionItemProps, context: SetupContext) {

        const parentContext = inject('accordionContext') as { parentProps: AccordionProps };
        const title = ref(props.title);
        const isActive = ref(props.active);

        function selectAccordionItem() {
            if(props.disabled || !parentContext?.parentProps?.enableFold){
                return;
            }
            isActive.value = !isActive.value;
        }

        function onClick($event: Event) {
            selectAccordionItem();
        }

        const accordionItemClass = computed(() => ({
            'f-state-disabled': props.disabled,
            card: true,
            'farris-panel-item': true,
            'f-state-selected': isActive.value
        }));

        const shouldShowHeader = computed(() => {
            return true;
        });

        const headIconClass = computed(() => ({
            'f-icon': true,
            'f-accordion-collapse': !isActive.value,
            'f-accordion-expand': isActive.value
        }));

        const cardContainerStyle = computed(() => {
            const styleObject = {
                transition: 'height 0.36s ease 0s',
                height: isActive.value ? `${props.height}px` : 0
            } as Record<string, any>;
            if (!isActive.value) {
                styleObject.overflow = 'hidden';
            }
            return styleObject;
        });

        onMounted(() => {
            if(parentContext?.parentProps?.expanded){
                isActive.value = true;
            }
        });

        return () => {
            return (
                <div class={accordionItemClass.value}>
                    <div class="card-header" onClick={onClick}>
                        <div class="panel-item-title">
                            {shouldShowHeader.value && <span>{title.value}</span>}
                            {context.slots.head && context.slots.head()}
                            <span class={headIconClass.value}></span>
                        </div>
                        <div class="panel-item-tool">{context.slots.toolbar && context.slots.toolbar()}</div>
                        <div class="panel-item-clear"></div>
                    </div>
                    <div style={cardContainerStyle.value}>
                        <div class="card-body">{context.slots.default && context.slots.default()}</div>
                    </div>
                </div>
            );
        };
    }
});
