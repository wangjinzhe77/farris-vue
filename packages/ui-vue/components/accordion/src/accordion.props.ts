 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import accordionSchema from './schema/accordion.schema.json';
import { schemaResolver } from './schema/schema-resolver';
import propertyConfig from './property-config/accordion.property-config.json';

export const accordionProps = {
    customClass: { type: String, default: '' },
    height: { type: Number },
    width: { type: Number },
    enableFold: { type: Boolean, default: true },
    expanded: { type: Boolean, default: false }
} as Record<string, any>;

export type AccordionProps = ExtractPropTypes<typeof accordionProps>;

export const accordionPropsResolver = createPropsResolver<AccordionProps>(accordionProps, accordionSchema, schemaMapper, schemaResolver, propertyConfig);
