/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, ref, SetupContext } from 'vue';
import { accordionItemProps, AccordionItemProps } from '../components/accordion-item.props';

export default defineComponent({
    name: 'FAccordionItemDesign',
    props: accordionItemProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: AccordionItemProps, context: SetupContext) {
        const title = ref(props.title);
        const isActive = ref(false);
        const isDisabled = ref(false);

        function selectAccordionItem() {
            isActive.value = !isActive.value;
        }

        function onClick($event: Event) {
            selectAccordionItem();
        }

        const accordionItemClass = computed(() => ({
            'f-state-disabled': isDisabled.value,
            card: true,
            'farris-panel-item': true,
            'f-state-selected': isActive.value
        }));

        const shouldShowHeader = computed(() => {
            return true;
        });

        const shouldShowCustomHeader = computed(() => {
            return false;
        });

        const headIconClass = computed(() => ({
            'f-icon': true,
            'f-accordion-collapse': !isActive.value,
            'f-accordion-expand': isActive.value
        }));

        const cardContainerStyle = computed(() => {
            const styleObject = {
                transition: 'height 0.36s ease 0s',
                height: isActive.value ? `${props.height}px` : 0
            } as Record<string, any>;
            if (!isActive.value) {
                styleObject.overflow = 'hidden';
            }
            return styleObject;
        });

        /**
         * 校验组件是否支持移动
         */
        function checkCanMoveComponent(): boolean {
            return true;
        }

        /**
         * 校验组件是否支持选中父级
         */
        function checkCanSelectParentComponent(): boolean {
            return false;
        }

        /**
         * 校验组件是否支持删除
         */
        function checkCanDeleteComponent() {
            return true;
        }

        context.expose({
            canMove: checkCanMoveComponent(),
            canSelectParent: checkCanSelectParentComponent(),
            canDelete: checkCanDeleteComponent(),
            canNested: true
        });

        return () => {
            return (
                <div class={accordionItemClass.value}>
                    <div class="card-header" onClick={onClick}>
                        <div class="panel-item-title">
                            {shouldShowHeader.value && <span>{title.value}</span>}
                            {shouldShowCustomHeader.value && context.slots.head && context.slots.head()}
                            <span class={headIconClass.value}></span>
                        </div>
                        <div class="panel-item-tool">{context.slots.toolbar && context.slots.toolbar()}</div>
                        <div class="panel-item-clear"></div>
                    </div>
                    <div style={cardContainerStyle.value}>
                        <div class="card-body drag-container">{context.slots.default && context.slots.default()}</div>
                    </div>
                </div>
            );
        };
    }
});
