 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App, Plugin } from 'vue';
import FAccordion from './src/accordion.component';
import FAccordionItem from './src/components/accordion-item.component';
import FAccordionDesign from './src/designer/accordion.design.component';
import FAccordionItemDesign from './src/designer/accordion-item.design.component';
import { accordionPropsResolver } from './src/accordion.props';
import { accordionItemPropsResolver } from './src/components/accordion-item.props';

export * from './src/accordion.props';
export * from './src/components/accordion-item.props';

export { FAccordion, FAccordionItem };

FAccordion.install = (app: App) => {
    app.component(FAccordion.name as string, FAccordion);
    app.component(FAccordionItem.name as string, FAccordionItem);
};
FAccordion.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap.accordion = FAccordion;
    componentMap['accordion-item'] = FAccordionItem;
    propsResolverMap.accordion = accordionPropsResolver;
    propsResolverMap['accordion-item'] = accordionItemPropsResolver;
};
FAccordion.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.accordion = FAccordionDesign;
    componentMap['accordion-item'] = FAccordionItemDesign;
    propsResolverMap.accordion = accordionPropsResolver;
    propsResolverMap['accordion-item'] = accordionItemPropsResolver;
};

export default FAccordion as typeof FAccordion & Plugin;
