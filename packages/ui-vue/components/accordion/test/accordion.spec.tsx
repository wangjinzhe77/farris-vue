import { mount } from '@vue/test-utils';
import { FAccordion, FAccordionItem } from '..';
import { nextTick } from 'vue';

describe('f-accordion', () => {
    const mocks = {};

    beforeAll(() => { });

    describe('properties', () => { });

    describe('render', () => {
        test('it should work', () => {
            const wrapper = mount({
                setup(props, ctx) {
                    return () => {
                        return (
                            <FAccordion>
                                <FAccordionItem title="Accordion Panel One "></FAccordionItem>
                                <FAccordionItem title="Accordion Panel Two "></FAccordionItem>
                            </FAccordion>
                        );
                    };
                }
            });
            expect(wrapper.find('div').classes('accordion')).toBeTruthy();
            expect(wrapper.find('div').classes('farris-panel')).toBeTruthy();
        });

        test('it should has right size', () => {
            const component = mount({
                setup(props, ctx) {
                    return () => {
                        return (
                            <FAccordion width={600} height={800}>
                                <FAccordionItem title="Accordion Panel One "></FAccordionItem>
                                <FAccordionItem title="Accordion Panel Two "></FAccordionItem>
                            </FAccordion>
                        );
                    };
                }
            });
            expect(component.find('div').element.style.width).toBe('600px');
            expect(component.find('div').element.style.height).toBe('800px');
        });

        test('it should should custom header', async () => {
            const component = mount(() => {
                return (
                    <FAccordion>
                        <FAccordionItem title="Accordion Panel One ">
                            {{
                                head: () => <div class="custom-header-in-test">This is custom header</div>,
                                toolbar: () => <div class="custom-toolbar-in-test">This is custom toolbar</div>,
                                default: () => <div class="custom-content-in-test">This is accordion item</div>
                            }}
                        </FAccordionItem>
                        <FAccordionItem title="Accordion Panel Two "></FAccordionItem>
                    </FAccordion>
                );
            });
            const accordionItem = component.find('div').find('.farris-panel-item');
            expect(accordionItem.find('.panel-item-title').find('.custom-header-in-test')).toBeTruthy();
            expect(accordionItem.find('.panel-item-tool').find('.custom-toolbar-in-test')).toBeTruthy();
            expect(accordionItem.find('.card-body').find('.custom-content-in-test')).toBeTruthy();

        });
    });

    describe('methods', () => { });

    describe('events', () => { });

    describe('behavior', () => {
        test('it should be selected by click', async () => {
            const component = mount(() => {
                return (
                    <FAccordion width={600} height={800}>
                        <FAccordionItem title="Accordion Panel One "></FAccordionItem>
                        <FAccordionItem title="Accordion Panel Two "></FAccordionItem>
                    </FAccordion>
                );
            });
            const accordionItem = component.find('div').find('.farris-panel-item');
            accordionItem.find('.card-header').trigger('click');
            await nextTick();
            expect(accordionItem.classes('f-state-selected')).toBeTruthy();
        });
    });
});
