import { SetupContext, computed, defineComponent, inject, onMounted, ref } from 'vue';
import { Step, StepProps, stepProps } from '../step.props';

import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';

export default defineComponent({
    name: 'FStepDesign',
    props: stepProps,
    emits: [],
    setup(props: StepProps, context) {
        const direction = ref(props.direction);
        const fill = ref(props.fill);
        const height = ref(props.height);
        const steps = ref(props.steps);
        const activeIndex = ref(0);
        const clickable = ref(props.clickable);
        const currentIndex = ref(0);
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const componentInstance = useDesignerComponent(elementRef, designItemContext);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);
        const stepsClass = computed(() => {
            const classObject = {
                'f-progress-step-list': direction.value === 'horizontal',
                'f-progress-step-list-block': direction.value === 'vertical',
                'f-progress-step-horizontal-fill': direction.value === 'horizontal' && fill.value,
                'f-progress-step-vertical-fill': direction.value === 'vertical' && fill.value
            } as Record<string, boolean>;
            return classObject;
        });

        const stepsStyle = computed(() => {
            const styleObject = {
                height: direction.value === 'vertical' ? `${height.value}px` : ''
            } as Record<string, any>;
            return styleObject;
        });

        function stepClass(step: Step, stepIndex: number) {
            const classObject = {
                step: true,
                active: stepIndex === activeIndex.value,
                clickable: clickable.value,
                'click-disable': step.disable,
                current: stepIndex === activeIndex.value
            } as Record<string, boolean>;
            if (stepIndex < activeIndex.value) {
                classObject.finish = true;
            }
            if (step.status) {
                classObject[step.status] = true;
            }
            // eslint-disable-next-line no-prototype-builtins
            if (step.hasOwnProperty('class')) {
                step.class.split(' ').reduce((preClassObject: any, className: string) => {
                    preClassObject[className] = true;
                    return preClassObject;
                }, classObject);
            }
            return classObject;
        }

        function onClickStep(step: Step, stepIndex: number) { }

        function stepRowClass(step: Step, stepIndex: number) {
            const classObject = {
                'f-progressstep-row': true,
                'step-active': stepIndex === activeIndex.value,
                'step-current': stepIndex === activeIndex.value
            } as Record<string, boolean>;
            if (stepIndex < activeIndex.value) {
                classObject['step-finish'] = true;
            }
            if (step.status) {
                classObject['step-' + step.status] = true;
            }
            return classObject;
        }

        function stepIconClass(step: Step, showSuccess: boolean) {
            const classObject = {
                'step-icon': true,
                'step-success': showSuccess,
                'k-icon': showSuccess,
                'k-i-check': showSuccess
            } as Record<string, boolean>;
            // eslint-disable-next-line no-prototype-builtins
            if (step.hasOwnProperty('icon')) {
                step.class.split(' ').reduce((preClassObject: Record<string, boolean>, className: string) => {
                    preClassObject[className] = true;
                    return preClassObject;
                }, classObject);
            }
            return classObject;
        }

        function renderStepIcon(step: Step, stepIndex: number) {
            const result: any[] = [];
            if (step.icon) {
                result.push(<span class={stepIconClass(step, false)}></span>);
            } else if (stepIndex >= activeIndex.value || stepIndex === currentIndex.value) {
                result.push(<span class="step-icon">{stepIndex + 1}</span>);
            } else {
                result.push(<span class={stepIconClass(step, true)}></span>);
            }
            return result;
        }

        function stepTitleClass(step: Step, stepIndex: number) {
            const classObject = {
                'step-name': true,
                'step-name-success': stepIndex < activeIndex.value
            } as Record<string, boolean>;
            return classObject;
        }

        function renderStepTitle(step: Step, stepIndex: number) {
            return (
                <div class="f-progress-step-title">
                    <p class={stepTitleClass(step, stepIndex)}>{step.title}</p>
                </div>
            );
        }

        function stepLineClass(step: Step, stepIndex: number) {
            const classObject = {
                'f-progress-step-line': true,
                'f-progress-step-line-success': stepIndex === activeIndex.value
            } as Record<string, boolean>;
            return classObject;
        }

        function shouldShowStepLine(step: Step, stepIndex: number) {
            return stepIndex !== steps.value.length - 1;
        }

        const stepLineTriangleClass = computed(() => {
            const classObject = {
                triangle: true,
                '': direction.value === 'vertical'
            } as Record<string, boolean>;
            return classObject;
        });

        function renderSteps() {
            return steps.value.map((step: Step, stepIndex: number) => {
                return (
                    <li class={stepClass(step, stepIndex)} onClick={(payload: MouseEvent) => onClickStep(step, stepIndex)}>
                        <div class={stepRowClass(step, stepIndex)}>
                            <div class="f-progress-step-content">
                                {renderStepIcon(step, stepIndex)}
                                {renderStepTitle(step, stepIndex)}
                            </div>
                            {shouldShowStepLine(step, stepIndex) && (
                                <div class={stepLineClass(step, stepIndex)}>
                                    <span class={stepLineTriangleClass.value}></span>
                                </div>
                            )}
                        </div>
                    </li>
                );
            });
        }

        return () => {
            return (
                <div ref={elementRef} class="f-progress-step">
                    <ul class={stepsClass.value} style={stepsStyle.value}>
                        {renderSteps()}
                    </ul>
                </div>
            );
        };
    }
});
