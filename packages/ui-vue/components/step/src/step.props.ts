import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import stepSchema from './schema/step.schema.json';
import propertyConfig from './property-config/step.property-config.json';

export type StepDirectionType = 'horizontal' | 'vertical';
export type StepStatus = 'active' | 'finish' | '';

// export interface Step {
//     disable: boolean;
//     status: StepStatus;
//     class: string;
//     icon: string;
//     title: string;
// }
export interface Step {
    id: string;
    class: string;
    description: string;
    disable: boolean;
    icon: string;
    title: string;
    status: StepStatus;
}

export const stepProps = {
    /** 步骤条方向  */
    direction: { type: String as PropType<StepDirectionType>, default: 'horizontal' },
    /** 是否平铺 */
    fill: { type: Boolean, default: false },
    /** 竖向步骤条，fill 时需要传递的高度 */
    height: { type: Number, default: 0 },
    /** 是否支持点击 */
    clickable: { type: Boolean, default: true },
    /** 步骤条的具体参数 */
    steps: {
        type: Array<Step>, default: [{
            id: '1',
            title: '审批中（示例）',
            description: '正在审批'
        },
        {
            id: '2',
            title: '复核中（示例）',
            description: '等待复核',
        }]
    },
    /** 当前active步骤的索引 */
    activeIndex: { type: Number, default: 0 },
    /** 步骤条点击后事件 */
    onClick: { type: Function, default: () => { } }
} as Record<string, any>;

export type StepProps = ExtractPropTypes<typeof stepProps>;

export const propsResolver = createPropsResolver<StepProps>(stepProps, stepSchema, schemaMapper, schemaResolver, propertyConfig);
