import { SetupContext, computed, defineComponent, ref, watch } from 'vue';
import { Step, StepProps, stepProps } from './step.props';

export default defineComponent({
    name: 'FStep',
    props: stepProps,
    emits: ['click'],
    setup(props: StepProps, context) {
        /** 步骤条方向  */
        const direction = ref(props.direction);
        /** 是否平铺 */
        const fill = ref(props.fill);
        /** 竖向步骤条，fill 时需要传递的高度 */
        const height = ref(props.height);
        /** 步骤条的具体参数 */
        const steps = ref(props.steps);
        /** 是否支持点击 */
        const clickable = ref(props.clickable);
        const currentIndex = ref(0);
        const activeIndex = ref(props.activeIndex);

        const stepsClass = computed(() => {
            const classObject = {
                'f-progress-step-list': direction.value === 'horizontal',
                'f-progress-step-list-block': direction.value === 'vertical',
                'f-progress-step-horizontal-fill': direction.value === 'horizontal' && fill.value,
                'f-progress-step-vertical-fill': direction.value === 'vertical' && fill.value
            } as Record<string, boolean>;
            return classObject;
        });

        const stepsStyle = computed(() => {
            const styleObject = {
                height: direction.value === 'vertical' ? `${height.value}px` : ''
            } as Record<string, any>;
            return styleObject;
        });

        function stepClass(step: Step, stepIndex: number) {
            const classObject = {
                step: true,
                active: stepIndex === activeIndex.value,
                clickable: clickable.value,
                // 'click-disable': step.disable,
                current: stepIndex === activeIndex.value
            } as Record<string, boolean>;
            if (stepIndex < activeIndex.value) {
                classObject.finish = true;
            }
            // if (step.status) {
            //     classObject[step.status] = true;
            // }
             
            // if (step.hasOwnProperty('class')) {
            //     step.class.split(' ').reduce((preClassObject: any, className: string) => {
            //         preClassObject[className] = true;
            //         return preClassObject;
            //     }, classObject);
            // }
            return classObject;
        }

        /** 点击后返回当前值 */
        function onClickStep(step: Step, stepIndex: number) {
            const currentStepInfo = {
                step,
                stepIndex
            };
            context.emit('click', currentStepInfo);
        }

        function stepRowClass(step: Step, stepIndex: number) {
            const classObject = {
                'f-progressstep-row': true,
                'step-active': stepIndex === activeIndex.value,
                'step-current': stepIndex === activeIndex.value
            } as Record<string, boolean>;
            if (stepIndex < activeIndex.value) {
                classObject['step-finish'] = true;
            }
            // if (step.status) {
            //     classObject['step-' + step.status] = true;
            // }
            return classObject;
        }

        function stepIconClass(step: Step, showSuccess: boolean) {
            const classObject = {
                'step-icon': true,
                'step-success': showSuccess,
                'f-icon': showSuccess,
                'f-icon-check': showSuccess
            } as Record<string, boolean>;
             
            // if (step.hasOwnProperty('icon')) {
            //     step.class.split(' ').reduce((preClassObject: Record<string, boolean>, className: string) => {
            //         preClassObject[className] = true;
            //         return preClassObject;
            //     }, classObject);
            return classObject;
        }

        /** 步骤条图标 */
        function renderStepIcon(step: Step, stepIndex: number) {
            const result: any[] = [];
            // if (step.icon) {
            //     result.push(<span class={stepIconClass(step, false)}></span>);
            // } else
            if (stepIndex >= activeIndex.value || stepIndex === currentIndex.value) {
                result.push(<span class="step-icon">{stepIndex + 1}</span>);
            } else {
                result.push(<span class={stepIconClass(step, true)}></span>);
            }
            return result;
        }

        function stepTitleClass(step: Step, stepIndex: number) {
            const classObject = {
                'step-name': true,
                'step-name-success': stepIndex < activeIndex.value
            } as Record<string, boolean>;
            return classObject;
        }

        function renderStepTitle(step: Step, stepIndex: number) {
            return (
                <div class="f-progress-step-title">
                    <p class={stepTitleClass(step, stepIndex)}>{step.title}</p>
                </div>
            );
        }

        function stepLineClass(step: Step, stepIndex: number) {
            const classObject = {
                'f-progress-step-line': true,
                'f-progress-step-line-success': stepIndex === activeIndex.value
            } as Record<string, boolean>;
            return classObject;
        }

        /** 步骤条连接线 */
        function shouldShowStepLine(step: Step, stepIndex: number) {
            return stepIndex !== steps.value.length - 1;
        }

        const stepLineTriangleClass = computed(() => {
            const classObject = {
                triangle: true,
                '': direction.value === 'vertical'
            } as Record<string, boolean>;
            return classObject;
        });

        function renderSteps() {
            return steps.value.map((step: Step, stepIndex: number) => {
                return (
                    <li class={stepClass(step, stepIndex)} onClick={(payload: MouseEvent) => onClickStep(step, stepIndex)}>
                        <div class={stepRowClass(step, stepIndex)}>
                            <div class="f-progress-step-content">
                                {renderStepIcon(step, stepIndex)}
                                {renderStepTitle(step, stepIndex)}
                            </div>
                            {shouldShowStepLine(step, stepIndex) && (
                                <div class={stepLineClass(step, stepIndex)}>
                                    <span class={stepLineTriangleClass.value}></span>
                                </div>
                            )}
                        </div>
                    </li>
                );
            });
        }
        watch(() => props.activeIndex, () => {
            // 最少为第一步，最多为最后一步完成
            if (props.activeIndex > -1 && props.activeIndex <= steps.value.length) {
                activeIndex.value = props.activeIndex;
                renderSteps();
            }
        });

        return () => {
            return (
                <div class="f-progress-step">
                    <ul class={stepsClass.value} style={stepsStyle.value}>
                        {renderSteps()}
                    </ul>
                </div>
            );
        };
    }
});
