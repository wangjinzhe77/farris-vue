 
import type { App } from 'vue';
import Step from './src/step.component';
import { propsResolver } from './src/step.props';
import FStepDesign from './src/designer/step.design.component';

export * from './src/step.props';

export { Step };

export default {
    install(app: App): void {
        app.component(Step.name as string, Step);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>): void {
        componentMap.step = Step;
        propsResolverMap.step = propsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
        componentMap.step = FStepDesign;
        propsResolverMap.step = propsResolver;
    }
};
