import { Ref, SetupContext, computed, nextTick, ref } from "vue";
import { PopoverProps } from "../popover.props";
import { UsePopup, UsePosition } from "./types";

export function usePopup(
    props: PopoverProps,
    context: SetupContext,
    arrowRef: Ref<any>,
    popoverRef: Ref<any>,
    reference: Ref<any>,
    shouldFitWidthToReference: Ref<boolean>,
    usePositionComposition: UsePosition
): UsePopup {
    const showPopover = ref(props.visible);
    const shown = computed(() => showPopover.value);

    const { fitToReference, locateToReference } = usePositionComposition;

    // eslint-disable-next-line prefer-const
    let hidePopverOnClickBodyHandler: ($event: MouseEvent | Event) => any;

    function hide() {
        showPopover.value = false;
        document.body.addEventListener('click', hidePopverOnClickBodyHandler);
        document.body.removeEventListener('mousedown', hidePopverOnClickBodyHandler);
        document.body.removeEventListener('wheel', hidePopverOnClickBodyHandler, true);
        document.removeEventListener('scroll', hidePopverOnClickBodyHandler);
        context.emit('hidden');
    }

    hidePopverOnClickBodyHandler = ($event: MouseEvent | Event) => {
        const isInReference = reference.value.contains($event.target as Node);

        if($event.type === 'scroll' || ($event.type === 'wheel' && isInReference)) {
            hide();
            return;
        }

        const closestPopoverElement = ($event.target as any)?.closest('.popover');
        if (closestPopoverElement && popoverRef.value && closestPopoverElement === popoverRef.value) {
            $event.stopPropagation();
            return;
        }

        if (!isInReference && showPopover.value) {
            const closestPopover = reference.value.closest('.popover') as HTMLElement;
            if (closestPopover) {
                closestPopover.removeEventListener('click', hidePopverOnClickBodyHandler);
            }
            hide();
        }
    };

    async function show(referenceElement: HTMLElement) {
        if (popoverRef.value && arrowRef.value && referenceElement) {
            showPopover.value = true;
            reference.value = referenceElement;
            if (shouldFitWidthToReference.value) {
                fitToReference(referenceElement);
            }
            await nextTick();
            locateToReference(referenceElement);
            document.body.addEventListener('click', hidePopverOnClickBodyHandler);
            document.body.addEventListener('mousedown', hidePopverOnClickBodyHandler);
            document.body.addEventListener('wheel', hidePopverOnClickBodyHandler, true);
            document.addEventListener('scroll', hidePopverOnClickBodyHandler);
    
            const closestPopover = referenceElement.closest('.popover') as HTMLElement;
            if (closestPopover) {
                closestPopover.addEventListener('click', hidePopverOnClickBodyHandler);
            }
            context.emit('shown');
        }
    }

    context.expose({ hide,popoverRef, show, shown });

    return { showPopover, hidePopverOnClickBodyHandler };
}
