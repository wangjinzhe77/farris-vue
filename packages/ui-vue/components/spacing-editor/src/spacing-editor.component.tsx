import { computed, defineComponent, ref, SetupContext } from 'vue';
import { SpacingEditorProps, spacingEditorProps } from './spacing-editor.props';
import './spacing-editor.css';
import getSpacingEditorMargin from './components/spacing-editor-margin.component';
import getSpacingEditorPadding from './components/spacing-editor-padding.component';

export default defineComponent({
    name: 'FSpacingEditor',
    props: spacingEditorProps,
    emits: ['valueChanged'] as (string[] & ThisType<void>) | undefined,
    setup(props: SpacingEditorProps, context: SetupContext) {

        /** 加载外边距编辑器 */
        const { renderSpacingEditorMargin } = getSpacingEditorMargin(props, context);
        /** 加载内边距编辑器 */
        const { renderSpacingEditorPadding } = getSpacingEditorPadding(props, context);

        return () => {
            return <div>
                {/* 设置外间距 */}
                {renderSpacingEditorMargin()}
                {/* 设置内间距 */}
                {renderSpacingEditorPadding()}
            </div>;
        };
    }
});
