 
import { ExtractPropTypes, PropType } from "vue";
import { createPropsResolver } from "../../dynamic-resolver";
// import { schemaMapper } from "./schema/schema-mapper";
// import spacingEditorSchema from './schema/spacing-editor.schema.json';
// import { schemaResolver } from './schema/schema-resolver';
// import propertyConfig from './property-config/spacing-editor.property-config.json';

export const spacingEditorProps = {
    /** 预设间距列表 */
    defaultSpacings: {
        type: Array, default: [
            { id: 'default', value: '4', name: '系统默认' },
            { id: 'large', value: '20', name: '大(20px)' },
            { id: 'middle', value: '16', name: '中(16px)' },
            { id: 'small', value: '12', name: '小(12px)' },
            { id: 'exsmall', value: '8', name: '超小(8px)' },
            { id: 'none', value: '0', name: '无(0px)' },
            { id: 'custom', value: 'custom', name: '自定义' },
        ]
    },
    /** 方向列表 */
    directions: {
        type: Array, default: [
            { id: "top", title: "上" },
            { id: "right", title: "右" },
            { id: "bottom", title: "下" },
            { id: "left", title: "左" },
        ]
    },
    /** 外间距 */
    margins: { type: Array, default: [4, 4, 4, 4] },
    /** 内间距 */
    paddings: { type: Array, default: [4, 4, 4, 4] },

} as Record<string, any>;

export type SpacingEditorProps = ExtractPropTypes<typeof spacingEditorProps>;

// export const propsResolver = createPropsResolver<SpacingEditorProps>(spacingEditorProps, spacingEditorSchema, schemaMapper, schemaResolver, propertyConfig);
