import { computed, nextTick, ref, SetupContext } from 'vue';
import { SpacingEditorProps } from '../spacing-editor.props';
import FNumberSpinner from '@farris/ui-vue/components/number-spinner';
import FComboList from '@farris/ui-vue/components/combo-list';

export default function (props: SpacingEditorProps, context: SetupContext) {
    /** 预设间距列表 */
    const {defaultSpacings} = props;
    /** 方向列表 */
    const {directions} = props;
    /** 外间距 */
    const margins = ref(props.margins);
    /** 内间距 */
    const paddings = ref(props.paddings);

    /** 是否显示内间距编辑器 */
    const isShowPaddingCustom = ref(false);
    /** 内间距 */
    const initPaddingValue = ref('default');

    /** 内间距标题箭头样式 */
    const paddingIconClass = computed(() => {
        const classObject = {
            'f-icon': true,
            'mr-1': true,
            'f-spacing-editor-icon': true,
            'f-icon-arrow-60-right': !isShowPaddingCustom.value,
            'f-icon-arrow-60-down': isShowPaddingCustom.value
        } as Record<string, boolean>;
        return classObject;
    });
    /** 目标样式 */
    const targetStyle = computed(() => {
        const styleObject = {
            margin: margins.value.join('px ') + 'px',
            padding: paddings.value.join('px ') + 'px',
        } as Record<string, any>;
        return styleObject;
    });

    /** 显示（隐藏）内间距编辑器 */
    function showPaddingCustom() {
        isShowPaddingCustom.value = !isShowPaddingCustom.value;
    }
    /** 设置预设内间距 */
    function onPaddingListChanged(items: any[]) {
        if (items[0].name !== "自定义") {
            paddings.value.fill(items[0].value);
            isShowPaddingCustom.value = false;
            nextTick(() => {
                isShowPaddingCustom.value = true;
            });
            context.emit('valueChanged', targetStyle.value);
        }
        else
        {isShowPaddingCustom.value = true;}
    }
    /** 设置自定义内间距 */
    function onPaddingCustomChanged() {
        initPaddingValue.value = 'custom';
        context.emit('valueChanged', targetStyle.value);
    }

    /** 渲染自定义内间距编辑器 */
    function renderPaddingCustomSetter() {
        return (
            isShowPaddingCustom.value ?
                <div class="f-spacing-editor-custom-container">
                    {directions.map((direction: { title: string }, index: number) => {
                        return <div>
                            <div class="f-spacing-editor-custom">
                                <div class="f-spacing-editor-title">{direction.title}间距(px)</div>
                                <FNumberSpinner style="flex: 1 1 0"
                                    min={0}
                                    v-model={paddings.value[index]}
                                    onChange={onPaddingCustomChanged}></FNumberSpinner>
                            </div>
                        </div>;
                    })}
                </div>
                : ""
        );
    }

    /** 渲染内间距编辑器 */
    function renderSpacingEditorPadding() {
        return (
            <div class="f-spacing-editor-container">
                {/* 预设内间距选择器 */}
                <div style="display: flex">
                    <div class="f-spacing-editor-title">
                        <div class={paddingIconClass.value}
                            onClick={showPaddingCustom} />
                        <div>内间距</div>
                    </div>
                    <FComboList style="flex: 1 1 0"
                        data={defaultSpacings}
                        v-model={initPaddingValue.value}
                        onChange={onPaddingListChanged}></FComboList>
                </div>
                {/* 自定义内间距编辑器 */}
                {renderPaddingCustomSetter()}
            </div>
        );
    };

    return {
        renderSpacingEditorPadding,
    };
}
