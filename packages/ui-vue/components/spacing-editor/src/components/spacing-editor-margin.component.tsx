import { computed, nextTick, ref, SetupContext } from 'vue';
import { SpacingEditorProps } from '../spacing-editor.props';
import FNumberSpinner from '@farris/ui-vue/components/number-spinner';
import FComboList from '@farris/ui-vue/components/combo-list';

export default function (props: SpacingEditorProps, context: SetupContext) {
    /** 预设间距列表 */
    const {defaultSpacings} = props;
    /** 方向列表 */
    const {directions} = props;
    /** 外间距 */
    const margins = ref(props.margins);
    /** 内间距 */
    const paddings = ref(props.paddings);

    /** 是否显示自定义外间距编辑器 */
    const isShowMarginCustom = ref(false);

    /** 外间距 */
    const initMarginValue = ref('default');

    /** 外间距标题箭头样式 */
    const marginIconClass = computed(() => {
        const classObject = {
            'f-icon': true,
            'mr-1': true,
            'f-spacing-editor-icon': true,
            'f-icon-arrow-60-right': !isShowMarginCustom.value,
            'f-icon-arrow-60-down': isShowMarginCustom.value
        } as Record<string, boolean>;
        return classObject;
    });
    /** 目标样式 */
    const targetStyle = computed(() => {
        const styleObject = {
            margin: margins.value.join('px ') + 'px',
            padding: paddings.value.join('px ') + 'px',
        } as Record<string, any>;
        return styleObject;
    });

    /** 显示（隐藏）自定义外间距编辑器 */
    function showMarginCustom() {
        isShowMarginCustom.value = !isShowMarginCustom.value;
    }
    /** 设置预设外间距 */
    function onMarginListChanged(items: any[]) {
        if (items[0].name !== "自定义") {
            margins.value.fill(items[0].value);
            isShowMarginCustom.value = false;
            nextTick(() => {
                isShowMarginCustom.value = true;
            });
            context.emit('valueChanged', targetStyle.value);
        }
        else
        {isShowMarginCustom.value = true;}
    }
    /** 设置自定义外间距 */
    function onMarginCustomChanged() {
        initMarginValue.value = 'custom';
        context.emit('valueChanged', targetStyle.value);
    }

    /** 渲染自定义外间距编辑器 */
    function renderMarginCustomSetter() {
        return (
            isShowMarginCustom.value ?
                <div class="f-spacing-editor-custom-container">
                    {directions.map((direction: { title: string }, index: number) => {
                        return <div>
                            <div class="f-spacing-editor-custom">
                                <div class="f-spacing-editor-title">{direction.title}间距(px)</div>
                                <FNumberSpinner style="flex: 1 1 0"
                                    min={0}
                                    v-model={margins.value[index]}
                                    onChange={onMarginCustomChanged}></FNumberSpinner>
                            </div>
                        </div>;
                    })}
                </div>
                : ""
        );
    }

    /** 渲染外间距编辑器 */
    function renderSpacingEditorMargin() {
        return (
            <div class="f-spacing-editor-container">
                {/* 预设外间距选择器 */}
                <div style="display: flex">
                    <div class="f-spacing-editor-title">
                        <div class={marginIconClass.value}
                            onClick={showMarginCustom} />
                        <div>外间距</div>
                    </div>
                    <FComboList style="flex: 1 1 0"
                        data={defaultSpacings}
                        v-model={initMarginValue.value}
                        onChange={onMarginListChanged}></FComboList>
                </div>
                {/* 自定义外间距编辑器 */}
                {renderMarginCustomSetter()}
            </div>
        );
    };

    return {
        renderSpacingEditorMargin,
    };
}
