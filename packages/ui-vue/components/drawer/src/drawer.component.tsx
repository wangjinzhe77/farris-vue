 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { computed, defineComponent, SetupContext, Teleport, Transition, withModifiers } from "vue";
import { DrawerProps, drawerProps } from "./drawer.props";
import './drawer.css';

export default defineComponent({
    name: 'FDrawer',
    props: drawerProps,
    emits: ['afterClose', 'update:modelValue'] as (string[] & ThisType<void>) | undefined,
    setup(props: DrawerProps, context: SetupContext) {

        const wrapperClassObject = computed(() => {
            return {
                'f-drawer-wrapper': true,
                [`f-drawer-wrapper-${props.position}`]: true
            };
        });

        const width = computed(() => {
            const widthParam = props.width;
            return typeof widthParam === 'string' ? widthParam : typeof widthParam === 'number' ? `${widthParam}px` : '20%';
        });

        const height = computed(() => {
            const heightParam = props.height;
            return typeof heightParam === 'string' ? heightParam : typeof heightParam === 'number' ? `${heightParam}px` : '20%';
        });

        const transitionName = computed(() => {
            return `f-drawer-${props.position}`;
        });

        const onClose = () => {
            context.emit('update:modelValue', false);
            context.emit('afterClose');
        };

        return () => {
            return (
                <Teleport to="body">
                    <div class="f-drawer">
                        <Transition name="f-drawer-mask" appear>
                            {props.modelValue && props.showMask && <div class="f-drawer-mask" onClick={withModifiers((e) => {
                                onClose();
                            }, ['stop'])}></div>}
                        </Transition>
                        <Transition name={transitionName.value} appear>
                            {props.modelValue && <div class={wrapperClassObject.value} style={{ width: width.value, height: height.value, backgroundColor: props.backgroundColor }}>
                                <div class="f-drawer-container">
                                    <div class="f-drawer-header">
                                        {props.showClose &&
                                            <div class="f-drawer-close">
                                                <span class="f-icon f-icon-close" onClick={withModifiers((e) => {
                                                    onClose();
                                                }, ['stop'])}></span>
                                            </div>
                                        }
                                        <div class="f-drawer-title">
                                            {context.slots.title ? context.slots.title() : props.title}
                                        </div>
                                    </div>
                                    <div class="f-drawer-body">
                                        {context.slots.content?.()}
                                    </div>
                                </div>
                            </div>}
                        </Transition>
                    </div>
                </Teleport>
            );
        };
    }
});
