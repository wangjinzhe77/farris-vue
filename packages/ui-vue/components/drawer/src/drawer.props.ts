 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';

export const drawerProps = {
    /** 背景色 */
    backgroundColor: { type: String, default: '#fff' },
    /** 高度 */
    height: { type: String as PropType<number | string>, default: 300 },
    /** 打开关闭抽屉 */
    modelValue: { type: Boolean, default: false },
    /** 从哪个位置呼出 */
    position: { type: String as PropType<'left' | 'right' | 'top' | 'bottom'>, default: 'right' },
    /** 是否展示关闭按钮 */
    showClose: { type: Boolean, default: true },
    /** 是否展示遮罩层 */
    showMask: { type: Boolean, default: true },
    /** 标题 */
    title: { type: String, default: '' },
    /** 宽度 */
    width: { type: String as PropType<number | string>, default: 300 }
} as Record<string, any>;

export type DrawerProps = ExtractPropTypes<typeof drawerProps>;
