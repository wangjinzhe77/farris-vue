 
import { SetupContext, defineComponent, inject, onBeforeMount, onMounted, ref, provide, computed, watch } from 'vue';
import FUploadProgress from './components/upload-progress.component';
import FPreviewCheckbox from './components/preview-checkbox.component';
import FFileSelect from './components/file-select.component';
import { uploaderProps, UploaderProps } from './uploader.props';
import { useUploader } from './composition/use-uploader';
import { FUploadFileExtend, FUploadPreviewColumn, UploadOutput } from './composition/type';
import {
    judegeFileSizeLimit,
    judgeContentTypeLimit,
    formatUploadOpts,
    getFileTypeClassName,
    getUploadProgress,
    getRealFileSize,
} from './composition/utils';
import { UploaderService } from './service/uploader-service';

export default defineComponent({
    name: 'FUploader',
    components: {
        'f-upload-progress': FUploadProgress,
        'f-preview-checkbox': FPreviewCheckbox,
        'f-file-select': FFileSelect
    },
    props: uploaderProps,
    emits: ['filePreviewEvent', 'fileDownloadEvent', 'previewMultiSelectedEvent', 'selectedEvent', 'fileRemoveEvent', 'fileRemovedEvent',
        'fUploadDoneEvent', 'uploadDoneEvent'],
    setup(props: UploaderProps, context: SetupContext) {
        const fileStateChange = ref();
        const uploaderService = new UploaderService(fileStateChange, props.uploadServerToken);

        // 赋值
        provide('uploaderService', uploaderService);
        // 自定义信息
        const customInfo = ref(props.customInfo);
        const uploadInputOpts = formatUploadOpts(props.uploadOptions, {
            // 允许上传的文件类型
            allowedContentTypes: ['*'],
            // 默认不限制附件上传个数
            maxUploads: 0,
            // 单位KB，默认是12M
            maxFileSize: '12MB'
        });
        // 文件选择组件的实例
        const fileSelectRef = ref(null);

        /**
         * 获取模版相关参数
         */
        const {
            disabled,
            uploadVisible,
            uploadDisabled,
            downloadButtonDisable,
            previewVisible,
            previewColumns,
            uploadFiles,
            innerFileInfos,
            previewEnableMulti,
            previewMultiSelectedLength,
            previewSelectAllBtnChecked,
            uploadEnableMulti,
            previewCurrentId,
            multiFileRemoveHandler,
            previewMultiSelectChangeHandler,
            fileMultiDownloadHandler,
            selectOrCancelAllHandler,
            rowSelectedHandler,
            hasColumnHtmlFunc,
            showPreviewStateColumn,
            fileDownloadHandler, filePreviewHandler, fileRemoveHandler,
            handleStateChange } = useUploader(props, context as SetupContext, fileSelectRef);

        watch(fileStateChange, (output) => {
            handleStateChange(output);
            switch (output.type) {
                case 'removed':
                    context.emit('fileRemovedEvent', output.files[0]);
                    break;
                default:
            }
        });

        /**
         * 是否存在文件信息
         */
        const isThemeAnyFileInfos = computed(() => {
            return innerFileInfos.value.length > 0;
        });

        /**
         * 支持类型
         */
        function contentTypeLimitHtml() {
            return judgeContentTypeLimit((uploadInputOpts?.allowedContentTypes || [])) ?
                <li>
                    <span>支持类型：</span>
                    <span class="support-info--item-detail" style="margin-right:4px;">
                        {uploadInputOpts.allowedContentTypes?.join('')},
                    </span>
                </li>
                : '';
        }
        /**
         * 单个文件限制
         * @returns
         */
        function fileSizeLimitHtml() {
            return judegeFileSizeLimit(uploadInputOpts?.maxFileSize || '') ?
                <li>
                    <span>单个文件限制：</span>
                    <span class="support-info--item-detail">{uploadInputOpts.maxFileSize}, &nbsp;&nbsp;</span>
                </li>
                : '';
        }
        /**
         * 文件总数限制
         * @returns
         */
        function maxUploadsHtml() {
            return (uploadInputOpts?.maxUploads || 0) > 0 && <li>
                <span>文件总数限制：</span>
                <span class="support-info--item-detail">{uploadInputOpts.maxUploads}个</span>
            </li>;
        }
        // const hhtpServ = inject('key', {})
        // hhtpServ.getData().then()
        /**
        * 提示结构
        * @returns
        */
        function beforeUploadSupportInfoHtml() {
            if (uploadVisible.value && !uploadDisabled.value && !disabled.value) {
                return <div class="ffileupload--support-info">
                    {customInfo.value ?
                        <ul class="support-info--wrapper"><li>
                            <span class="support-info--item-detail" v-html={customInfo.value}></span>
                        </li></ul>
                        : <ul class="support-info--wrapper">{contentTypeLimitHtml()}
                            {fileSizeLimitHtml()}
                            {maxUploadsHtml()}
                        </ul>
                    }
                </div>;
            }
            return null;
        }

        /**
         * 返回上传附件的名称模版
         * @param uploadInfo
         * @returns
         */
        function getUploadFileNameHtml(uploadInfo: UploadOutput) {
            return (
                <div class="uploadAndpreview--title-container">
                    <div class="ffilepreview--item-icon">
                        <span class={['ffilepreview--filetype-icon', getFileTypeClassName(uploadInfo.file?.name || '')]}></span>
                    </div>
                    <div class="uploadAndpreview--right">
                        <a class="item-content--title" title={uploadInfo.file?.name}>
                            {uploadInfo.file?.name || '无名称'}
                        </a>
                        <f-upload-progress
                            status={uploadInfo.type === 'done' ? 'success' : 'active'}
                            percent={getUploadProgress(uploadInfo)}
                            maxWidth={300}></f-upload-progress>
                    </div>
                </div>
            );
        };

        /**
         * 返回预览文件的名称模版
         * @param previewInfo
         * @returns
         */
        function getPreviewFileNameHtml(previewInfo: FUploadFileExtend) {
            return (
                <div class="uploadAndpreview--title-container">
                    <div class="ffilepreview--item-icon">
                        <span class={['ffilepreview--filetype-icon', getFileTypeClassName(previewInfo.name)]}></span>
                    </div>
                    <div class="uploadAndpreview--right">
                        <a
                            class={['item-content--title', "previewButtonDisable.value?'no-preview':''"]}
                            title={previewInfo.name}
                            onClick={(event: MouseEvent) => filePreviewHandler(event, previewInfo)}>
                            {previewInfo.name}
                        </a>
                    </div>
                </div>
            );
        };

        /**
         * 返回文件大小的模版
         * @param state
         * @param fileInfo
         * @returns
         */
        function getFileSizeHtml(state: string, uploadInfo: UploadOutput | null, previewInfo: FUploadFileExtend | null) {
            return (
                <div class="uploadAndpreview--filesize-container">
                    {state === 'preview' && previewInfo ? <span>{getRealFileSize(previewInfo.size)}</span> : null}
                    {state === 'upload' && uploadInfo ? <span>{getUploadProgress(uploadInfo)}</span> : null}
                </div>
            );
        };
        /**
        * 返回附件上传的状态
        * @param state
        * @param fileInfo
        */
        function getFileStateHtml(state: string, fileInfo: UploadOutput | null) {
            if (fileInfo && fileInfo.type === 'error') {
                return (
                    <div class="uploadAndpreview--state-container">
                        <p class="upload-state--uploaded">
                            <i class="f-icon f-icon-message_warning text-danger"></i>
                            <span>存在异常</span>
                        </p>
                    </div>
                );
            }
            return (
                <div class="uploadAndpreview--state-container">
                    {state === 'upload' ? (
                        fileInfo && fileInfo.type !== 'done' ?
                            <p class="upload-state--uploading">
                                <i class="f-icon f-icon-clock text-warning"></i>
                                <span>正在上传...</span>
                            </p>
                            :
                            <p class="upload-state--uploaded">
                                <i class="f-icon f-icon-success text-success"></i>
                                <span>上传成功</span>
                            </p>
                    ) : state === 'preview' ?
                        <p class="upload-state--uploaded">
                            <i class="f-icon f-icon-success text-success"></i>
                            <span>已上传</span>
                        </p> : ''}
                </div>
            );
        };
        /**
       * 返回预览按钮模版
       * @param previewInfo
       * @returns
       */
        function getPreviewActionHtml(previewInfo: FUploadFileExtend) {
            return <div class="uploadAndpreview--action-container">
                {!downloadButtonDisable.value ?
                    <button class="btn preview-btn" title="下载" onClick={(event: MouseEvent) => fileDownloadHandler(event, previewInfo)}>
                        <span class="f-icon f-icon-enclosure_download"></span>
                    </button>
                    : ''}
                {!props.previewButtonDisable ?
                    <button class="btn preview-btn" title="预览" onClick={(event: MouseEvent) => filePreviewHandler(event, previewInfo)}>
                        <span class="f-icon f-icon-enclosure_browse"></span>
                    </button>
                    : ''}
                {!props.deleteButtonDisable ?
                    <button class="btn preview-btn" title="删除" onClick={(event: MouseEvent) => fileRemoveHandler(event, previewInfo, uploaderService)}>
                        <span class="f-icon f-icon-enclosure_delete" style="top: -1px"></span>
                    </button>
                    : ''}
            </div>;
        };
        /**
        * 返回创建时间
        * @param previewInfo
        */
        function getPreviewDateHtml(previewInfo: FUploadFileExtend) {
            return <div class="uploadAndpreview--date-container">{previewInfo.createTime || ''}</div>;
        };

        /**
        * 找到模版
        */
        function findColumnHtmlFunc(state, uploadInfo: UploadOutput | null, previewInfo: any, columnInfo: FUploadPreviewColumn) {
            let result;
            state = state || 'preview';
            if (columnInfo.formatter) {
                result = columnInfo.formatter(previewInfo[columnInfo.field] || '', state, state === 'preview' ? previewInfo : uploadInfo, columnInfo);
                return result;
            }
            if (state === 'preview' && previewInfo) {
                switch (columnInfo.field) {
                    case 'state':
                        result = getFileStateHtml('preview', null);
                        break;
                    case 'name':
                        result = getPreviewFileNameHtml(previewInfo);
                        break;
                    case 'action':
                        result = getPreviewActionHtml(previewInfo);
                        break;
                    case 'size':
                        result = getFileSizeHtml(state, uploadInfo, previewInfo);
                        break;
                    case 'createTime':
                        result = getPreviewDateHtml(previewInfo);
                        break;
                    default:
                        result = `<div>${previewInfo[columnInfo.field] || ''} </div>`;

                }
            }
            if (state === 'upload' && uploadInfo) {
                switch (columnInfo.field) {
                    case 'state':
                        result = getFileStateHtml('upload', uploadInfo);
                        break;
                    case 'name':
                        result = getUploadFileNameHtml(uploadInfo);
                        break;
                    case 'size':
                        result = getFileSizeHtml(state, uploadInfo, previewInfo);
                        break;
                    default:
                        result = ``;
                }
            }
            return result;
        };
        /**
         * 获取附件上传子组件模版
         * @returns
         */
        function getFileSelectHtml() {
            if (uploadVisible.value) {
                return <div class="header--left-container">
                    <f-file-select ref={fileSelectRef} disabled={uploadDisabled.value} selectText={props.selectText} uploadedCount={props.uploadedCount} uploadServerToken={props.uploadServerToken}
                        enableMulti={uploadEnableMulti.value} options={props.uploadOptions} extendConfig={props.extendConfig} onStateChange={(datas: any) => handleStateChange(datas)}></f-file-select>
                </div>;
            }
            return null;
        }
        /**
         * 获取附件个数、附件已选信息的模版
         * @returns
         */
        function getAttachCountHtml() {
            const canShowCountInfo = !uploadVisible.value && previewVisible.value && isThemeAnyFileInfos.value;
            if (canShowCountInfo) {
                return <div class="header--left-container header--countInfo">
                    <p class="m-0">
                        共<span class="count">{uploadFiles.value.length + innerFileInfos.value.length}</span>个附件
                        {previewMultiSelectedLength.value > 0 ? <>(已选<span class="count-selected">{previewMultiSelectedLength.value}</span>个)</> : ''}
                    </p>
                </div>;
            }
            return null;
        }
        /**
         * 获取头部按钮的模版
         */
        function getHeaderButtonHtml() {
            const canShowOneButton = !downloadButtonDisable.value || !props.deleteButtonDisable;
            const canShowBtnHtml = canShowOneButton && previewVisible.value && isThemeAnyFileInfos.value;
            if (canShowBtnHtml) {
                return <div class="header--right-container">
                    {!downloadButtonDisable.value ? <button class="btn btn-primary f-btn-ml" disabled={previewMultiSelectedLength.value === 0}
                        onClick={(event: MouseEvent) => fileMultiDownloadHandler(event)} >
                        下载
                    </button> : null}
                    {!props.deleteButtonDisable.value ? <button class="btn btn-secondary f-btn-ml" disabled={previewMultiSelectedLength.value === 0} onClick={(event: MouseEvent) => multiFileRemoveHandler(event)}>
                        删除
                    </button> : null}
                </div>;
            }
            return null;
        }
        /**
         * 获取列表头部模版
         */
        function getListHeaderHtml() {
            return previewColumns.value.map((column: FUploadPreviewColumn) => (
                showPreviewStateColumn(column) ?
                    <th style={{ 'width': column.width ? column.width + 'px' : 'auto' }}
                        class={[(!disabled.value && column.checkbox ? column.checkbox : false) ? 'td--hascheckbox' : '']}>
                        {
                            !disabled.value && previewEnableMulti.value && column.checkbox ? <f-preview-checkbox class="preview-checkbox" id="previewMultiBtn"
                                checked={previewSelectAllBtnChecked.value} onCheckedChange={(data: any) => selectOrCancelAllHandler(data)}>
                            </f-preview-checkbox> : null
                        }
                        {column.title}
                    </th> : null

            ));
        }
        /**
         * 获取上传列表模版
         */
        function getUploadListHtml() {
            return uploadFiles.value.map((uploadInfo: UploadOutput) => (
                <tr class="uploadAndpreview--upload-item"
                    id={'uploadAndpreview--upload-item' + (uploadInfo?.file?.id || '')}>
                    {
                        previewColumns.value.map((column: FUploadPreviewColumn) => (
                            <td class={[(!disabled.value && column.checkbox ? column.checkbox : false) ? 'td--hascheckbox' : '']}>
                                {
                                    previewEnableMulti.value && column.checkbox ? <f-preview-checkbox id={uploadInfo?.file?.id} checked={uploadInfo.checked}
                                        disabled={uploadInfo.type !== 'done'} class="preview-checkbox" onCheckedChange={(data: any) => previewMultiSelectChangeHandler(data)}>
                                    </f-preview-checkbox> : null
                                }
                                {
                                    hasColumnHtmlFunc(column, 'upload') ? findColumnHtmlFunc('upload', uploadInfo, null, column) : null
                                }</td>
                        ))
                    }
                </tr>
            ));
        }
        /**
         * 获取预览列表模版
         */
        function getPreviewListHtml() {
            return innerFileInfos.value.map((previewInfo: FUploadFileExtend) => (
                <tr class={["uploadAndpreview--preview-item", previewCurrentId.value === previewInfo.id ? "uploadAndpreview--currentfile" : ""]}
                    id={'uploadAndpreview--preview-item' + previewInfo.id} onClick={(event) => rowSelectedHandler(event, previewInfo)}>
                    {
                        previewColumns.value.map((column: FUploadPreviewColumn) => (
                            showPreviewStateColumn(column) ? <td class={[(!disabled.value && column.checkbox ? column.checkbox : false) ? 'td--hascheckbox' : '']}>
                                {
                                    !disabled.value && previewEnableMulti.value && column.checkbox ? <f-preview-checkbox id={previewInfo.id} checked={previewInfo.checked} class="preview-checkbox" onCheckedChange={(data: any) => previewMultiSelectChangeHandler(data)}>
                                    </f-preview-checkbox> : null
                                }
                                {
                                    hasColumnHtmlFunc(column, 'preview') ? findColumnHtmlFunc('preview', null, previewInfo, column) : <div class="text-truncate" v-html={findColumnHtmlFunc('preview', null, previewInfo, column)}></div>
                                }
                            </td> : null
                        ))
                    }
                </tr>
            ));
        }
        return () => (
            disabled.value ? <div class="fv-upload-and-preview">
                {!isThemeAnyFileInfos.value ? beforeUploadSupportInfoHtml() : null}
                {
                    previewVisible.value ? <div class="uploadAndpreview--content">
                        {
                            isThemeAnyFileInfos.value ? <table class="table table-bordered uploadAndpreview--table">
                                <thead>{getListHeaderHtml()}</thead>
                                <tbody>
                                    {getPreviewListHtml()}
                                </tbody>
                            </table> : <p class="uploadAndpreview--nodata">无附件信息</p>
                        }
                    </div> : null
                }
                {isThemeAnyFileInfos.value ? beforeUploadSupportInfoHtml() : null}
            </div> : <div class="fv-upload-and-preview">
                <div class="uploadAndpreview--header">
                    {getFileSelectHtml()}
                    {getAttachCountHtml()}
                    {getHeaderButtonHtml()}
                </div>
                {!isThemeAnyFileInfos.value ? beforeUploadSupportInfoHtml() : null}
                {
                    previewVisible.value || uploadVisible.value ? <div class="uploadAndpreview--content">
                        {
                            (downloadButtonDisable.value || !uploadVisible.value) && uploadFiles.value.length === 0 && !isThemeAnyFileInfos.value ? <p class="uploadAndpreview--nodata">无附件信息</p> : ''
                        }
                        {
                            uploadFiles.value.length > 0 || isThemeAnyFileInfos ? <table class="table table-bordered uploadAndpreview--table">
                                <thead>{getListHeaderHtml()}</thead>
                                <tbody>
                                    {getUploadListHtml()}
                                    {getPreviewListHtml()}
                                </tbody>
                            </table> : null
                        }
                    </div> : null
                }
                {isThemeAnyFileInfos.value ? beforeUploadSupportInfoHtml() : null}
            </div>);
    }
});
