import { defineComponent, SetupContext, watch, onMounted, onUnmounted, getCurrentInstance, ref, Ref, defineExpose, inject } from 'vue';
import { fileSelectProps, FileSelectProps } from './sub-component.props';

import { UploaderOptions, UploadInput } from '../composition/type';
import { UploaderService } from '../service/uploader-service';

export default defineComponent({
    name: 'FFileSelect',
    props: fileSelectProps,
    emits: ['change', 'stateChange'] as (string[] & ThisType<void>) | undefined,
    setup(props: FileSelectProps, context: SetupContext) {
        // 获取服务
        // const uploaderService = new UploaderService(context, props.uploadServerToken);
        const uploaderService = inject('uploaderService') as UploaderService;
        uploaderService.setUploadContext(context);
        // 当前实例
        const instance = getCurrentInstance();
        const accept = ref('*');
        // 记录附件上传Input元素
        let uploadInput: any;
        /**
         * 选择附件变更
         */
        const changeHandler = () => {
            if (uploadInput.files) {
                context.emit("change", uploadInput.files);
                uploaderService.handleFiles(uploadInput.files);
                uploadInput.value = null;
            }
        };
        /**
         * 重置服务
         */
        function reset() {
            uploadInput.value = null;
            uploaderService.reset();
        };
        /**
         * 监听状态的变化
         */
        watch(() => props.disabled, () => {
            reset();
        });
        /**
         * 监听
         */
        watch(() => props.uploadedCount, (newValue) => {
            uploaderService.setOptions({ 'uploadedCount': newValue });
        });
        /**
         * 扩展配置
         */
        watch(() => props.extendConfig, (newValue) => {
            uploaderService.setExtendServerConfig(newValue);
        });

        /**
         * 触发input弹出
         */
        function selectFile(event: MouseEvent) {
            event && event.stopPropagation();
            uploadInput.click();
        };
        /**
         * 处理上传事件
         */
        function handleUploadEvent(uploadEvents: UploadInput) {
            uploaderService.handleUploadEvent(uploadEvents);
        }
        /**
         * 注册一个回调函数，在组件挂载完成后执行。
         */
        onMounted(() => {
            uploadInput = instance?.proxy?.$refs.uploadInput as HTMLElement;
            reset();
            uploaderService.setOptions(props.options);
            uploaderService.setExtendServerConfig(props.extendConfig);
            accept.value = ((props.options as UploaderOptions)?.allowedContentTypes || ['*']).join(",");
        });

        /**
         * 注册一个回调函数，在组件实例被卸载之后调用。
         */
        onUnmounted(() => {
        });

        /**
         * 对外抛事件
         */
        context.expose({
            handleUploadEvent
        });

        return () => (
            <>
                <input
                    ref="uploadInput"
                    type="file"
                    class="ffileupload--browser"
                    disabled={props.disabled}
                    multiple={props.enableMulti}
                    accept={accept.value}
                    onChange={() => changeHandler()}
                    style="width: 10px"
                    title="uploadInput"
                />
                <div
                    class={['upload-container', props.disabled ? 'f-state-disabled' : '']}
                    onClick={(event: MouseEvent) => selectFile(event)}
                    title={props.selectText}>
                    <i class="f-icon f-icon-upload upload-icon" style="top: 2px;position: relative;"></i>{props.selectText}
                </div>
            </>
        );
    }
});
