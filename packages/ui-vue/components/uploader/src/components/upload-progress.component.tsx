 
import { defineComponent,  SetupContext, toRefs } from 'vue';
import { UploadProgressProps, uploadProgressProps } from './sub-component.props';

export default defineComponent({
    name: 'FUploadProgress',
    props: uploadProgressProps,
    setup(props: UploadProgressProps, context: SetupContext) {
        const { showInfo, status, maxWidth, percent } = toRefs(props);
        
        return () => (
            <div class={['f-progress upload-progress', 'upload-progress-status-' + status.value, 'f-progress-status-' + status.value, 'upload-progress-line', showInfo.value ? 'upload-progress-show-info' : '']}>
                <div class="upload-progress-outer" style={{ 'max-width': maxWidth.value > 0 ? maxWidth.value + 'px' : 'auto' }}>
                    <div class="upload-progress-inner">
                        <div class="upload-progress-bg f-progress-bg" style={{ 'width': percent.value + '%' }}>
                        </div>
                    </div>
                </div>
                {
                    showInfo.value ? <span class="upload-progress-text">
                        {(status.value === 'error' || status.value === 'success') ? <span class={["upload-progress-text-icon f-icon", "f-icon-" + status.value]}></span> : <>{percent.value}%</>}
                    </span> : ''
                }

            </div>
        );
    }
});
