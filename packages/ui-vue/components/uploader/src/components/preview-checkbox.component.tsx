 
import { defineComponent, ref, toRefs, watch } from 'vue';
import { PreviewCheckboxProps, previewCheckboxProps } from './sub-component.props';

export default defineComponent({
    name: 'FPreviewCheckbox',
    props: previewCheckboxProps,
    emits: ['checkedChange'],
    setup(props: PreviewCheckboxProps, context) {
        const { disabled } = toRefs(props);
        // 内部处理
        const innerChecked = ref(props.checked);
        // 处理点击
        const clickHandler = (event: MouseEvent) => {
            event.stopPropagation();
            if (!disabled.value) {
                innerChecked.value = !innerChecked.value;
                context.emit('checkedChange', { checked: innerChecked.value, id: props.id });
            }
        };

        watch(() => props.checked, (newValue) => {
            innerChecked.value = newValue;
        });

        return () => (
            <div class="preview-checkbox d-inline-flex align-middle">
                <div class="custom-control custom-checkbox f-checkradio-single  m-0" >
                    <input title="custom-control-input" class="custom-control-input" type="checkbox" disabled={disabled.value} checked={innerChecked.value} />
                    <label class="custom-control-label" onClick={(event: MouseEvent) => clickHandler(event)} onMousedown={(event: MouseEvent) => event.stopPropagation()}></label>
                </div>
            </div>
        );
    }
});
