import { ExtractPropTypes, PropType, Ref } from 'vue';
import { UploadInput, UploaderOptions } from '../composition/type';

export const previewCheckboxProps = {
    // 是否被选中
    checked: { type: Boolean, default: false },
    // 进度条状态 默认'normal'
    id: { type: String, default: '' },
    // 禁用
    disabled: { type: Boolean, default: false },
};
export type PreviewCheckboxProps = ExtractPropTypes<typeof previewCheckboxProps>;

/**
 * 进度条组件
 */
// 进度条状态 默认normal
export type UploadProgressStatusType = 'success' | 'error' | 'active' | 'normal';

export const uploadProgressProps = {
    // 是否显示进度条信息
    showInfo: { type: Boolean, default: true },
    // 进度条状态 默认'normal'
    status: { type: String as PropType<UploadProgressStatusType>, default: 'normal' },
    // 已完成的分段百分比
    percent: { type: Number, default: 0 },
    maxWidth: { type: Number, default: 0 },
};
export type UploadProgressProps = ExtractPropTypes<typeof uploadProgressProps>;

/**
 * 文件选择组件
 */
export const fileSelectProps = {
    /**
    * 按钮文本
    */
    selectText: { type: String, default: '选择文件' },
    /**
     * 禁用
     */
    disabled: { type: Boolean, default: false },
    /**
     * 启用多选
     */
    enableMulti: { type: Boolean, default: true },
    /**
     * 允许上传类型
     */
    accept: { type: String, default: "*" },
    /**
     * 配置对象
     */
    options: { type: Object as PropType<UploaderOptions>, default: {} },
    /**
     * 已上传附件个数
     */
    uploadedCount: { type: Number, default: 0 },
    /**
    * 发起服务器端请求，某个组件使用的特殊的参数
    */
    extendConfig: { type: Object, default: {} },
    /**
 * 附件服务器端服务注入的Token
 */
    uploadServerToken: { type: String, default: '' },
};
export type FileSelectProps = ExtractPropTypes<typeof fileSelectProps>;
