 
import { Ref, SetupContext, defineComponent, inject, onBeforeMount, onMounted, ref } from 'vue';
import FUploadProgress from '../components/upload-progress.component';
import FPreviewCheckbox from '../components/preview-checkbox.component';
import FFileSelect from '../components/file-select.component';
import { uploaderProps, UploaderProps } from '../uploader.props';
import { useUploader } from '../composition/use-uploader';
import { FUploadFileExtend, FUploadPreviewColumn } from '../composition/type';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { formatUploadOpts, getFileTypeClassName, getRealFileSize, judegeFileSizeLimit, judgeContentTypeLimit } from '../composition/utils';

import './uploader.scss';

export default defineComponent({
    name: 'FUploaderDesign',
    components: {
        'f-preview-checkbox': FPreviewCheckbox,
        'f-file-select': FFileSelect
    },
    props: uploaderProps,
    emits: ['filePreviewEvent', 'fileDownloadEvent', 'previewMultiSelectedEvent', 'selectedEvent', 'fileRemoveEvent',
    'fUploadDoneEvent','uploadDoneEvent'],
    setup(props: UploaderProps, context) {
        // 自定义信息
        const customInfo = ref(props.customInfo);
        const uploadInputOpts = formatUploadOpts(props.uploadOptions, {
            // 允许上传的文件类型
            allowedContentTypes: ['*'],
            // 默认不限制附件上传个数
            maxUploads: 0,
            // 单位KB，默认是12M
            maxFileSize: '12MB'
        });
        // 文件选择组件的实例
        const fileSelectRef = ref(null);
        // 内置的文件信息
        const innerFileInfos = [{ id: "attach01", name: "" }, { id: "attach02", name: "" }];

        /**
         * 获取模版相关参数
         */
        const {
            disabled,
            uploadVisible,
            uploadDisabled,
            downloadButtonDisable,
            previewVisible,
            previewColumns,
            previewEnableMulti,
            previewCurrentId,
            showPreviewStateColumn } = useUploader(props, context as SetupContext, fileSelectRef);
        /**
         * 支持类型
         */
        function contentTypeLimitHtml() {
            return judgeContentTypeLimit((uploadInputOpts?.allowedContentTypes || [])) ?
                <li>
                    <span>支持类型：</span>
                    <span class="support-info--item-detail" style="margin-right:4px;">
                        {uploadInputOpts.allowedContentTypes?.join('')},
                    </span>
                </li> : null;
        }
        /**
         * 单个文件限制
         * @returns
         */
        function fileSizeLimitHtml() {
            return judegeFileSizeLimit(uploadInputOpts?.maxFileSize || '') ?
                <li>
                    <span>单个文件限制：</span>
                    <span class="support-info--item-detail">{uploadInputOpts.maxFileSize}, &nbsp;&nbsp;</span>
                </li> : null;
        }
        /**
         * 文件总数限制
         * @returns
         */
        function maxUploadsHtml() {
            return (uploadInputOpts?.maxUploads || 0) > 0 ? <li>
                <span>文件总数限制：</span>
                <span class="support-info--item-detail">{uploadInputOpts.maxUploads}个</span>
            </li> : null;
        }
        /**
        * 提示结构
        * @returns
        */
        function beforeUploadSupportInfoHtml() {
            if (uploadVisible.value && !uploadDisabled.value && !disabled.value) {
                return <div class="ffileupload--support-info">
                    {customInfo.value ?
                        <ul class="support-info--wrapper"><li>
                            <span class="support-info--item-detail" v-html={customInfo.value}></span>
                        </li></ul>
                        : <ul class="support-info--wrapper">
                            {contentTypeLimitHtml()}
                            {fileSizeLimitHtml()}
                            {maxUploadsHtml()}
                        </ul>
                    }
                </div>;
            }
            return null;
        }
        /**
             * 返回预览文件的名称模版
             * @param previewInfo
             * @returns
             */
        function getPreviewFileNameHtml(previewInfo: FUploadFileExtend) {
            return <div class="uploadAndpreview--title-container">
                <div class="ffilepreview--item-icon">
                    <span class="ffilepreview--filetype-icon"></span>
                </div>
                <div class="uploadAndpreview--right">
                    <span class="ffilepreview--preview-block"></span>
                </div>
            </div>;
        };

        /**
         * 返回文件大小的模版
         * @param state
         * @param fileInfo
         * @returns
         */
        function getFileSizeHtml(previewInfo: FUploadFileExtend | null) {
            return (
                <div class="uploadAndpreview--filesize-container">
                    <span class="ffilepreview--preview-block"></span>
                </div>
            );
        };
        /**
            * 返回附件上传的状态
            * @param state
            * @param fileInfo
            */
        function getFileStateHtml() {
            return <div class="uploadAndpreview--state-container">
                <p class="upload-state--uploaded">
                    <i class="f-icon f-icon-success text-success"></i>
                    <span>已上传</span>
                </p>
            </div>;
        }
        /**
       * 返回预览按钮模版
       * @param previewInfo
       * @returns
       */
        function getPreviewActionHtml(previewInfo: FUploadFileExtend) {
            return <div class="uploadAndpreview--action-container">
                {!downloadButtonDisable.value ?
                    <button class="btn preview-btn" title="下载">
                        <span class="f-icon f-icon-enclosure_download"></span>
                    </button>
                    : ''}
                {!props.previewButtonDisable ?
                    <button class="btn preview-btn" title="预览" >
                        <span class="f-icon f-icon-enclosure_browse"></span>
                    </button>
                    : ''}
                {!props.deleteButtonDisable ?
                    <button class="btn preview-btn" title="删除" >
                        <span class="f-icon f-icon-enclosure_delete" style="top: -1px"></span>
                    </button>
                    : ''}
            </div>;
        };
        /**
        * 返回创建时间
        * @param previewInfo
        */
        function getPreviewDateHtml(previewInfo: FUploadFileExtend) {
            return <div class="uploadAndpreview--date-container"> <span class="ffilepreview--preview-block"></span></div>;
        };
        /**
            * 找到模版
            */
        function findColumnHtmlFunc(state, previewInfo: any, columnInfo: FUploadPreviewColumn) {
            let result = null;
            state = state || 'preview';
            if (state === 'preview' && previewInfo) {
                switch (columnInfo.field) {
                case 'state':
                    result = getFileStateHtml();
                    break;
                case 'name':
                    result = getPreviewFileNameHtml(previewInfo);
                    break;
                case 'action':
                    result = getPreviewActionHtml(previewInfo);
                    break;
                case 'size':
                    result = getFileSizeHtml(previewInfo);
                    break;
                case 'createTime':
                    result = getPreviewDateHtml(previewInfo);
                    break;
                default:
                    result = <span class="ffilepreview--preview-block"></span>;
                }
            }
            return result;
        };
        /**
         * 获取附件上传子组件模版
         * @returns
         */
        function getFileSelectHtml() {
            if (uploadVisible.value) {
                return <div class="header--left-container">
                    <f-file-select ref={fileSelectRef} disabled={uploadDisabled.value} selectText={props.selectText}></f-file-select>
                </div>;
            }
            return null;
        }
        /**
         * 获取头部按钮的模版
         */
        function getHeaderButtonHtml() {
            const canShowOneButton = !downloadButtonDisable.value || !props.deleteButtonDisable;
            const canShowBtnHtml = canShowOneButton && previewVisible.value;
            if (canShowBtnHtml) {
                return <div class="header--right-container">
                    {!downloadButtonDisable.value ? <button class="btn btn-primary f-btn-ml" disabled>下载</button> : null}
                    {!props.deleteButtonDisable.value ? <button class="btn btn-secondary f-btn-ml" disabled>删除</button> : null}
                </div>;
            }
            return null;
        }
        /**
         * 获取列表头部模版
         */
        function getListHeaderHtml() {
            return previewColumns.value.map((column: FUploadPreviewColumn) => (
                showPreviewStateColumn(column) ?
                    <th style={{ 'width': column.width ? column.width + 'px' : 'auto' }}
                        class={[(!disabled.value && column.checkbox ? column.checkbox : false) ? 'td--hascheckbox' : '']}>
                        {
                            !disabled.value && previewEnableMulti.value && column.checkbox ? <f-preview-checkbox class="preview-checkbox" id="previewMultiBtn">
                            </f-preview-checkbox> : null
                        }
                        {column.title}
                    </th> : null

            ));
        }
        /**
         * 获取预览列表模版
         */
        function getPreviewListHtml() {
            return innerFileInfos.map((previewInfo: FUploadFileExtend) => (
                <tr class={["uploadAndpreview--preview-item", previewCurrentId.value === previewInfo.id ? "uploadAndpreview--currentfile" : ""]}
                    id={'uploadAndpreview--preview-item' + previewInfo.id} >
                    {
                        previewColumns.value.map((column: FUploadPreviewColumn) => (
                            showPreviewStateColumn(column) ? <td class={[(!disabled.value && column.checkbox ? column.checkbox : false) ? 'td--hascheckbox' : '']}>
                                {
                                    !disabled.value && previewEnableMulti.value && column.checkbox ? <f-preview-checkbox class="preview-checkbox"></f-preview-checkbox> : null
                                }
                                {
                                    findColumnHtmlFunc('preview', previewInfo, column)
                                }
                            </td> : null
                        ))
                    }
                </tr>
            ));
        }

        onBeforeMount(() => {

        });

        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const componentInstance = useDesignerComponent(elementRef, designItemContext);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        return () => (
            <div class="fv-upload-and-preview" ref={elementRef}>
                {previewVisible.value || uploadVisible.value ?
                    <>
                        {disabled.value ? null :
                            <div class="uploadAndpreview--header">
                                {getFileSelectHtml()}
                                {getHeaderButtonHtml()}
                            </div>}
                        <div class="uploadAndpreview--content">
                            <table class="table table-bordered uploadAndpreview--table">
                                <thead>{getListHeaderHtml()}</thead>
                                <tbody>
                                    {getPreviewListHtml()}
                                </tbody>
                            </table>
                        </div>
                        {beforeUploadSupportInfoHtml()}
                    </>
                    : null}
            </div>);
    }
});
