import { inject } from "vue";
import { IUploadServer, NotifyServiceAPI, UploadServerAPI } from "../composition/type";
import { UploaderDefaultService } from "../service/default-service";
import { FNotifyService } from '../../../notify';

/**
 * 通过注入NotifyService获取提示服务
 */
const getNotify = (notifyServiceToken: string = ''): NotifyServiceAPI => {
    let realService;
    if (notifyServiceToken && inject(notifyServiceToken, null)) {
        realService = inject(notifyServiceToken) as NotifyServiceAPI;
    }
    if (!realService) {
        // 引入FarrisVue提示服务
        realService = new FNotifyService() as NotifyServiceAPI;
    }
    return realService;
};
/**
 * 获取附件的
 */
const getServerAPI = (uploadServerToken: string = ''): UploadServerAPI => {
    let realService;
    if (uploadServerToken && inject(uploadServerToken, null)) {
        const serviceFromUser = inject(uploadServerToken) as IUploadServer;
        if (serviceFromUser && serviceFromUser()) {
            realService = serviceFromUser();
        }
    }
    if (!realService) {
        // 引入附件默认上传服务
        realService = new UploaderDefaultService();
    }
    return realService;
};

export const GetService = {
    getNotify, getServerAPI
};
