/* eslint-disable no-prototype-builtins */
import { Ref, SetupContext, computed, onUnmounted, ref, toRefs, watch } from 'vue';
import { UploaderProps } from '../uploader.props';
import { FUploadFileExtend, FUploadPreviewColumn, UploadFile, UploadInput, UploadOutput, UploadType } from './type';
import {
    sortPreviewData,
    changeUploadFileToFileExtend,
    formateDateTo
} from './utils';
import { GetService } from './use-service';
import FFileSelect from '../components/file-select.component';
import { UploaderService } from '../service/uploader-service';

export function useUploader(props: UploaderProps, ctx: SetupContext, fileSelectRef: Ref<typeof FFileSelect | null>) {


    // 获取服务
    const notifyService = GetService.getNotify(props.notifyServiceToken);
    // 内部需要更改
    const fileInfos = ref(props.fileInfos);
    const {
        disabled,
        uploadVisible,
        uploadDisabled,
        previewVisible,
        orderField,
        previewEnableMulti,
        downloadButtonDisable,
        previewColumns,
        uploadEnableMulti,
        previewButtonDisable
    } = toRefs(props);

    // 全选状态
    const previewSelectAllBtnChecked = ref(false);
    // 记录选中数据
    const previewMultiSelected: Ref<Array<FUploadFileExtend>> = ref([]);
    // 用于模版的展示
    const previewMultiSelectedLength = computed(() => {
        return previewMultiSelected.value.length;
    });

    // 记录文件个数
    const uploadFiles: Ref<Array<UploadOutput>> = ref([]);
    // 记录当前行的ID
    const previewCurrentId = ref("");

    // 格式化数据
    function formatFileInfos(datas) {
        // 处理其他数据切换引起数据源的变更
        if (!datas.length || previewMultiSelected.value.length !== datas.length) {
            previewSelectAllBtnChecked.value = false;
        }
        const newFileInfos = datas.map((item: FUploadFileExtend) => {
            if (!item.hasOwnProperty('size')) {
                item.size = undefined;
            }
            if (!item.hasOwnProperty('createTime')) {
                item.createTime = undefined;
            }
            if (previewEnableMulti.value) {
                item.checked = !!previewMultiSelected.value.find((n) => n.id === item.id) || previewSelectAllBtnChecked.value;
            }
            return item;
        });
        const formatValue = [...newFileInfos];
        formatValue.sort(sortPreviewData(orderField.value));
        if (previewEnableMulti.value) {
            previewMultiSelected.value = [...formatValue].filter((n) => n.checked);
        }
        return formatValue;
    };

    const innerFileInfos = ref(formatFileInfos(fileInfos.value));

    watch(() => props.fileInfos, (newValue) => {
        innerFileInfos.value = formatFileInfos(newValue);
    });
    /**
     * 下载
     * @param event
     * @param fileInfo
     */
    const fileDownloadHandler = (event: Event, fileInfo: FUploadFileExtend) => {
        event.stopImmediatePropagation();
        ctx.emit('fileDownloadEvent', { fileInfos: [fileInfo], name: '' });
    };
    /**
     * 预览
     * @param event
     * @param fileInfo
     */
    const filePreviewHandler = (event: Event, fileInfo: FUploadFileExtend) => {
        event.stopImmediatePropagation();
        if (previewButtonDisable.value) {
            return;
        }
        ctx.emit('filePreviewEvent', fileInfo);
    };
    /**
     * 删除数据
     * @param event
     * @param fileInfo
     */
    const fileRemoveHandler = (ev: Event, fileInfo: FUploadFileExtend, uploaderService: UploaderService) => {
        // ev.stopImmediatePropagation();
        if (fileInfo.checked) {
            previewMultiSelected.value = previewMultiSelected.value.filter((n) => n.id !== fileInfo.id);
        }
        ctx.emit('fileRemoveEvent', fileInfo);
        uploaderService.handleUploadEvent({ type: "remove", file: fileInfo });
    };

    /**
     * 找到模版
     */
    const hasColumnHtmlFunc = (columnInfo: FUploadPreviewColumn, type = 'preview') => {
        if (columnInfo.formatter) {
            return true;
        }
        if (type === 'preview') {
            const previewDefaultFields = ['state', 'name', 'action', 'size', 'createTime'];
            return previewDefaultFields.findIndex((pfield) => pfield === columnInfo.field) > -1;
        }
        if (type === 'upload') {
            const uploadDefaultFields = ['state', 'name', 'size'];
            return uploadDefaultFields.findIndex((ufield) => ufield === columnInfo.field) > -1;
        }
        return false;
    };

    /**
     * 是否显示预览状态列
     * @param columnInfo
     * @returns
     */
    const showPreviewStateColumn = (columnInfo: FUploadPreviewColumn) => {
        if (columnInfo.field === 'state' && (uploadDisabled.value || !uploadVisible.value)) {
            return false;
        }
        return true;
    };

    /**
     * 获取名称
     * @param name
     * @returns
     */
    const getName = (name: string) => {
        if (name.lastIndexOf('.') > 0) {
            return name.substring(0, name.lastIndexOf('.'));
        }
        return '';
    };

    /**
     * 批量下载附件
     * @param event
     */
    const fileMultiDownloadHandler = (event: Event) => {
        event.stopImmediatePropagation();
        if (previewMultiSelectedLength.value > 1) {
            let rename = '';
            rename = getName(previewMultiSelected.value[0].name);
            ctx.emit('fileDownloadEvent', {
                fileInfos: previewMultiSelected.value,
                name: rename
            });
        }
    };

    /**
     * 批量删除
     * @param event
     */
    const multiFileRemoveHandler = (event: Event) => {
        event.stopImmediatePropagation();
        ctx.emit('fileRemoveEvent', previewMultiSelected.value);
    };

    /**
     * 选择或取消所有
     * @param selectInfo
     */
    const selectOrCancelAllHandler = (selectInfo: any) => {
        const allInfos = innerFileInfos.value;
        if (selectInfo.checked) {
            // 全选
            allInfos.forEach((item) => {
                item.checked = true;
            });
            previewMultiSelected.value = [...innerFileInfos.value];
            previewSelectAllBtnChecked.value = true;
        } else {
            // 取消
            previewMultiSelected.value = [];
            allInfos.forEach((item) => {
                item.checked = false;
            });
            previewSelectAllBtnChecked.value = false;
        }
        // 抛出多选
        ctx.emit('previewMultiSelectedEvent', previewMultiSelected.value);
    };

    /**
 * 选中当前行事件
 */
    const rowSelectedHandler = (event: Event | null, fileInfo: FUploadFileExtend) => {
        if (event) {
            event.stopImmediatePropagation();
            event.stopPropagation();
        }
        if (!previewCurrentId.value || fileInfo.id !== previewCurrentId.value) {
            previewCurrentId.value = fileInfo.id;
            // 抛出当前行数据
            ctx.emit('selectedEvent', fileInfo);
        }
    };

    /**
     * 多选变更
     */
    const previewMultiSelectChangeHandler = (selectInfo: any) => {
        const foundIndex = previewMultiSelected.value.findIndex((item) => item.id === selectInfo.id);
        const hasInSelected = foundIndex > -1;
        const foundFile = innerFileInfos.value.find((item) => item.id === selectInfo.id);
        if (foundFile) {
            foundFile.checked = selectInfo.checked;
        }
        if (selectInfo.checked && !hasInSelected && foundFile) {
            // 选入
            previewMultiSelected.value.push(foundFile);
        }
        if (!selectInfo.checked && hasInSelected) {
            // 移出
            previewMultiSelected.value.splice(foundIndex, 1);
        }
        // 判断是否全选
        if (innerFileInfos.value.length > 0) {
            if (previewMultiSelected.value.length === innerFileInfos.value.length) {
                // 都全选
                previewSelectAllBtnChecked.value = true;
            } else {
                // 未全选
                previewSelectAllBtnChecked.value = false;
            }
        }
        rowSelectedHandler(null, foundFile);
        // 抛出多选
        ctx.emit('previewMultiSelectedEvent', previewMultiSelected.value);
    };

    /**
     * 捕捉事件变更
     */
    const uploadEventEmit = (event: UploadInput) => {
        fileSelectRef?.value?.handleUploadEvent(event);
    };

    /**
     * 单个文件上传
     */
    const startUpload = (file: UploadFile): void => {
        uploadEventEmit({ type: "upload", file });
    };

    /**
     * 更新附件的状态
     * @param output
     * @param eventType
     */
    const updateFilesState = (output: UploadOutput, eventType: UploadType) => {
        let startFiles: UploadFile[] = [];
        if (output.files && output.files.length > 0) {
            // 此处更新界面可能有性能问题
            startFiles = output.files as UploadFile[];
        } else if (typeof output.file !== 'undefined') {
            startFiles = [output.file];
        }
        eventType = eventType || output.type;
        startFiles.map((item) => {
            const index = uploadFiles.value.findIndex((fileOutput) => typeof item !== 'undefined' && fileOutput?.file?.id === item.id);
            // 变更状态
            if (index > -1) {
                uploadFiles.value[index] = { type: eventType, file: item };
            }
        });
    };

    /**
    * 临时删除附件
    * @param output
    */
    const hideAfterLoaded = (output: UploadOutput): void => {
        if (output.files) {
            // 删除或者移除文件
            const ids = output.files.map((fileItem) => fileItem.id);
            uploadFiles.value = uploadFiles.value.filter((fileOutput: UploadOutput) => {
                const tIndex = ids.findIndex((tId) => tId === fileOutput?.file?.id);
                return !(tIndex > -1);
            });
            uploadEventEmit({
                type: "hide",
                id: ids.join(","),
            });
        }
    };

    /**
     * 处理上传信息
     * @param output
     */
    const showNotify = (output: UploadOutput) => {
        let notifyType = 'warning';
        if (output.type === 'cancelled' || output.type === 'removed') {
            notifyType = 'success';
        }
        const message = output.hasOwnProperty('message') ? output.message : '';
        notifyService.show({
            options: { type: notifyType, message },
            position: 'top-center'
        });
    };

    /**
     * allAddedToQueue 选中文件追加到队列
     * @param output
     */
    const handleStateChange = (output: UploadOutput): void => {
        if (output.type === 'addedToQueue' && typeof output.file !== 'undefined') {
            uploadFiles.value.push({ type: output.type, file: output.file });
            // 开始上传
            startUpload(output.file);
        } else if (output.type === 'start') {
            // 开始上传
            updateFilesState(output, 'start');
        } else if (output.type === 'done') {
            // 上传完成
            updateFilesState(output, 'done');

            const newAddedFiles = (output.files || []).map((fileItem) => {
                return {
                    checked: false,
                    id: fileItem.id,
                    name: fileItem.name,
                    size: fileItem.size,
                    createTime: formateDateTo(new Date(), 'yyyy-MM-dd HH:mm:ss')
                };
            });
            // 此处自动更新内容
            fileInfos.value = [...newAddedFiles, ...innerFileInfos.value];
            // 上传完成后不在当前页面上显示
            hideAfterLoaded(output);
            ctx.emit('fUploadDoneEvent', changeUploadFileToFileExtend(output.files as UploadFile[] || []));
            ctx.emit('uploadDoneEvent', changeUploadFileToFileExtend(output.files as UploadFile[] || []));
        } else if (output.type === 'cancelled' || output.type === 'removed' || output.type === 'error') {
            // 删除或者移除文件
            const filterRemovedFiles = fileInfos.value.filter((fileOutput: FUploadFileExtend) => {
                return output.files && output.files.findIndex((item) => fileOutput.id === item.id) < 0;
            });
            fileInfos.value = [...filterRemovedFiles];
            // 给出提示
            showNotify(output);
        } else if (output.type === 'rejected' && typeof output.file !== 'undefined') {
            // 上传被拒绝
            showNotify(output);
        }
    };

    /**
     * 实例被卸载
     */
    onUnmounted(() => {

    });

    return {
        disabled,
        uploadVisible,
        uploadDisabled,
        downloadButtonDisable,
        previewVisible,
        previewColumns,
        uploadFiles,
        innerFileInfos,
        previewEnableMulti,
        previewMultiSelectedLength,
        previewSelectAllBtnChecked,
        uploadEnableMulti,
        previewCurrentId,
        multiFileRemoveHandler,
        previewMultiSelectChangeHandler,
        fileMultiDownloadHandler,
        selectOrCancelAllHandler,
        hasColumnHtmlFunc,
        showPreviewStateColumn,
        rowSelectedHandler,
        fileDownloadHandler,
        filePreviewHandler,
        fileRemoveHandler,
        handleStateChange
    };
};
