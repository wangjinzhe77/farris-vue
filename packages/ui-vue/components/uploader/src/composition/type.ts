 

export interface UploaderOptions {
    concurrency?: number;
    allowedContentTypes?: string[];
    maxUploads?: number;
    maxFileSize?: number; // MB为单位
    uploadedCount?: number;
}
export enum UploadStatus {
    Queue,
    Uploading,
    Done,
    Cancelled,
    Remove,// 正在删除
    Error
}

export interface UploadProgress {
    status: UploadStatus;
    data?: {
        percentage: number;
        speed?: number;
        speedHuman?: string;
        startTime?: number | null;
        endTime?: number | null;
        eta?: number | null;
        etaHuman?: string | null;
    };
}
export interface FUploadFileExtend {
    id: string;
    name: string;
    disabled?: boolean;
    checked?: boolean;
    size?: number | undefined; // 文件大小
    createTime?: string | undefined; // 创建日期
    type?: string;
    extend?: any | null; // 记录返回数据
    extendHeaders?: { [key: string]: string } | null;
    [key: string]: any;
}

export interface UploadFile {
    id: string;
    fileIndex: number;
    lastModifiedDate: Date | string;
    name: string;
    size: number;
    type: string;
    form?: FormData;
    progress?: UploadProgress;//
    response?: any; // 记录返回数据
    responseStatus?: number;
    nativeFile?: File;
    responseHeaders?: { [key: string]: string };
}

export type UploadType = 'addedToQueue' | 'allAddedToQueue' | 'uploading' | 'done' | 'start' | 'cancelled' | 'removed' | 'allRemoved' | 'rejected' | 'error';

export interface UploadOutput {
    type: UploadType;
    file?: UploadFile;
    nativeFile?: File;
    message?: string;
    files?: UploadFile[]|FUploadFileExtend[];
    checked?: boolean;
}
export interface UploadConfig {
    url?: string;
    method?: string; // POST，GET
    id?: string;
    fieldName?: string;
    fileIndex?: number;
    file?: UploadFile|FUploadFileExtend;
    data?: {
        [key: string]: any;
    };
    headers?: { [key: string]: string } | null;
    includeWebKitFormBoundary?: boolean; // If false, only the file is send trough xhr.send (WebKitFormBoundary is omit)
    withCredentials?: boolean;
    timeout?: number; // 暂不支持
}

export interface UploadInput extends UploadConfig {
    type: 'upload' | 'uploadAll' | 'cancel' | 'cancelAll' | 'remove' | 'removeAll' | 'config' | 'hide';
    /** 分块上传时，每块大小默认为 1M */
    chunkSize?: number;
}

export abstract class UploadServerAPI {
    /**
     * 上传配置
     */
    uploadConfig: UploadConfig;

    /**
     * 删除配置
     */
    removeConfig: UploadConfig;

    constructor(uploadConfig: UploadConfig = {}, removeConfig: UploadConfig = {}) {
        // 处理公共的配置
        const commonConfig = { type: 'config', url: '', timeout: 0, headers: null, data: {} };
        this.uploadConfig = Object.assign({}, commonConfig, uploadConfig);
        this.removeConfig = Object.assign({}, commonConfig, removeConfig);
    }

    // extendConfig 用来处理传递的配置
    abstract upload(files: UploadFile[], event: UploadInput, extendConfig: any, eventEmit: (any) => void): Promise<UploadOutput>;

    // extendConfig 用来处理传递的配置
    abstract remove(files: FUploadFileExtend[], event: UploadInput, extendConfig: any, eventEmit: (any) => void): Promise<UploadOutput>;
}

/**
 * 上传预览显示列
 */
export interface FUploadPreviewColumn {
    field: string;
    width: number;
    title: string;
    checkbox?: boolean;
    formatter?:(value,state:string,info: UploadOutput | any, columnInfo: FUploadPreviewColumn)=>any;
}
/**
 * 注入附件服务器端的服务
 */
export type IUploadServer = () => UploadServerAPI;

/**
 * 提示服务
 */
export type NotifyServiceAPI = {
    show: (par: any) => any;
};

/**
 * --------------------------
 *  默认服务里使用
 * --------------------------
 */

export interface BlobFile extends Blob {
    name: string;
}
