 
/* eslint-disable no-prototype-builtins */
/* eslint-disable no-case-declarations */
import {
    FUploadFileExtend, UploadFile, UploadInput, UploadOutput,
    UploadServerAPI, UploadStatus, UploaderOptions
} from "../composition/type";
import { isContentTypeAllowed, isFileSizeAllowed, makeUploadFile } from "../composition/utils";
import { GetService } from "../composition/use-service";
import { Ref, SetupContext } from "vue";

export class UploaderService {
    // 记录在上传的附件数组
    queue: UploadFile[];

    uploadOpts: UploaderOptions = {
        // 默认不限制,0代表不限制 Number.POSITIVE_INFINITY
        allowedContentTypes: ["*"],
        maxUploads: 0,
        /** 单位M，默认是12M,0代表不限制 */
        maxFileSize: 12,
        concurrency: Number.POSITIVE_INFINITY,
        uploadedCount: 0
    };

    /**
     * 服务器端扩展
     */
    private extendServerConfig = null;

    private uploadServerSer: UploadServerAPI;
    private uploadContext: SetupContext | null = null;

    constructor(private stateChangeRecord: Ref<UploadOutput>, private serverToken: string) {
        this.queue = [];
        this.uploadServerSer = GetService.getServerAPI(this.serverToken);
    }
    setUploadContext(context: SetupContext) {
        this.uploadContext = context;
    }
    /**
     * 处理从服务器端返回的结果
     */
    private handleResultFromServer(uploadOutput: UploadOutput) {
        // 附件已删除成功
        switch (uploadOutput.type) {
            case 'removed':
                // 事件中返回的都是
                this.queue = this.queue.filter(
                    (item) => item.progress?.status !== UploadStatus.Remove
                );
                if (!uploadOutput.hasOwnProperty("message")) {
                    uploadOutput.message = "被删除";
                }
                this.stateChangeRecord.value = uploadOutput;
                break;
            case 'error':
                // 上传失败的附件移除
                this.queue = this.queue.filter((queueItem) => {
                    return (uploadOutput.files && uploadOutput.files.findIndex((item) => queueItem.id === item.id) < 0);
                });
                this.uploadContext?.emit("stateChange", uploadOutput);
                break;
            default:
                this.uploadContext?.emit("stateChange", uploadOutput);
        }

    }

    setOptions(options: UploaderOptions) {
        // 重置文件大小、类型、个数限制
        if (options) {
            this.uploadOpts = Object.assign(this.uploadOpts, options);
        }
    }

    /**
     * 处理上传的附件信息
     * @param incomingFiles
     */
    handleFiles(incomingFiles: FileList): void {
        const allowedIncomingFiles: File[] = Array.from(incomingFiles).reduce(
            (acc: File[], checkFile: File, i: number) => {
                const futureQueueLength = acc.length + this.queue.length + 1;
                const judgeResult = this.rejectedReason(
                    checkFile.name,
                    futureQueueLength,
                    checkFile.size
                );
                if (judgeResult.allowed) {
                    acc = acc.concat(checkFile);
                } else {
                    // 不符合当前文件类型或者内容超出限制，抛出事件
                    const rejectedFile: UploadFile = makeUploadFile(
                        checkFile,
                        i
                    );
                    this.uploadContext?.emit("stateChange", {
                        type: "rejected",
                        file: rejectedFile,
                        message: judgeResult.message,
                    });
                }
                return acc;
            },
            []
        );

        // 构造文件结构，并单个抛出事件
        [].map.call(allowedIncomingFiles, (file: File, i: number) => {
            const uploadFile: UploadFile = makeUploadFile(file, i);
            this.queue.push(uploadFile);
            this.uploadContext?.emit("stateChange", { type: "addedToQueue", file: uploadFile });
        });
        // 所有的文件都已经添加，抛出事件
        this.uploadContext?.emit("stateChange", { type: "allAddedToQueue" });
    }

    /**
     * 获取上传被拒绝的理由
     * @param name
     * @param queuelength
     * @param size
     * @returns
     */
    rejectedReason(name: string, queuelength: number, size: number) {
        let allowed = false;
        let message = "";
        // 已存在同名文件
        const findDuplicateIndex = this.queue.findIndex((file) => file.name === name);
        if (findDuplicateIndex > -1) {
            message = "上传失败：已存在同名文件";
        } else if (this.uploadOpts.allowedContentTypes && !isContentTypeAllowed(this.uploadOpts.allowedContentTypes, name)) {
            message = `上传失败：只允许上传${this.uploadOpts.allowedContentTypes.join(",")}类型的文档`;
        } else if (this.exceedMaxUpload(queuelength)) {
            message = `上传失败：文件总个数超出${this.uploadOpts.maxUploads}限制`;
        } else if (!isFileSizeAllowed(this.uploadOpts.maxFileSize, size)) {
            message = `上传失败：单个文件大小超出${this.uploadOpts.maxFileSize}MB的限制`;
        } else if (size === 0) {
            message = "上传失败：不允许文件为空";
        } else {
            allowed = true;
        }
        return {
            allowed,
            message,
        };
    }

    /**
     * 超出最大上传数值
     * @param queuelength
     * @returns
     */
    private exceedMaxUpload(queuelength = 0) {
        if (!this.uploadOpts || !this.uploadOpts.maxUploads) {
            return false;
        }
        const tUploadCount = this.uploadOpts.uploadedCount || 0;

        return this.uploadOpts.maxUploads > 0 && (
            this.uploadOpts.maxUploads <= tUploadCount ||
            queuelength + tUploadCount > this.uploadOpts.maxUploads);
    }

    /**
     * 从前端传来事件，进行服务器端方法类型判断
     * @param input
     */
    handleUploadEvent(event: UploadInput) {
        switch (event.type) {
            case "upload":
                const uploadFileIndex = this.queue.findIndex(
                    (file) => file === event.file
                );
                if (uploadFileIndex !== -1 && event.file) {
                    this.serverMethod({
                        files: [this.queue[uploadFileIndex]],
                        event,
                    });
                }
                break;
            case "remove":
                if (!event?.file?.id) {
                    return;
                }
                const removeIndex = this.queue.findIndex(
                    (file) => file.id === event?.file?.id
                );
                if (removeIndex !== -1 && this.queue[removeIndex] && this.queue[removeIndex].progress) {
                    // 得有个开始删除和已经删除
                    const { progress } = this.queue[removeIndex];
                    if (progress) {
                        progress.status = UploadStatus.Remove;
                    }
                    // this.handleInputEvent({
                    //     files: [this.queue[removeIndex]],
                    //     event
                    // });
                }
                this.serverMethod({ files: [event.file], event });
                break;
            case "removeAll":
                // 此处是通过status的值来判断是否已上传
                const removeQueueFiles = this.queue.filter(
                    (uploadFile) => uploadFile?.progress?.status === UploadStatus.Queue
                );
                if (removeQueueFiles.length) {
                    this.uploadContext?.emit('stateChange', {
                        type: "cancelled",
                        files: removeQueueFiles,
                        message: "删除附件成功",
                    });
                    this.queue = this.queue.filter(
                        (uploadFile) => uploadFile?.progress?.status !== UploadStatus.Queue
                    );
                }
                if (this.queue.length) {
                    event.type = "remove";
                    this.queue.forEach((item) => {
                        if (item.progress) {
                            item.progress.status = UploadStatus.Remove;
                        }
                    });
                    this.serverMethod({ files: this.queue, event });
                }
                break;
            default:
        }
    }

    /**
     * 处理上传的扩展参数
     * @param extendSer
     */
    setExtendServerConfig(extendSer: any) {
        this.extendServerConfig = extendSer;
    }

    /**
     * 处理服务器接口的方法
     * @param upload
     * @returns
     */
    serverMethod(upload: { event: UploadInput; files: UploadFile[] | FUploadFileExtend[] }) {
        let apiResult;
        switch (upload.event.type) {
            case "upload":
                apiResult = this.upload(upload.files as UploadFile[], upload.event, (eventInfo: any) => this.handleResultFromServer(eventInfo));
                break;
            case "removeAll":
            case "remove":
                apiResult = this.remove(upload.files as FUploadFileExtend[], upload.event, (eventInfo: any) => this.handleResultFromServer(eventInfo));
                break;
            default:
                apiResult = null;
        }
        if (apiResult) {
            apiResult.then((info) => {
                this.handleResultFromServer(info);
            }).catch((error) => {
                this.handleResultFromServer(error);
            });
        }
    }

    /**
     * 上传附件
     * @param files
     * @param event
     * @returns
     */
    upload(files: UploadFile[], event: UploadInput, eventEmit: (any) => void): Promise<UploadOutput> {
        // 抛出开始上传的事件
        this.uploadContext?.emit('stateChange', { type: "start", files });
        return this.uploadServerSer.upload(files, event, this.extendServerConfig, eventEmit);
    }

    /**
     * 删除附件
     * @param files
     * @param event
     * @returns
     */
    remove(files: FUploadFileExtend[], event: UploadInput, eventEmit: (any) => void): Promise<UploadOutput> {
        return this.uploadServerSer.remove(files, event, this.extendServerConfig, eventEmit);
    }

    /**
     * 重置
     */
    reset() {
        this.queue = [];
    }

    /**
     * 销毁
     */
    destroyed() {
        this.reset();
    }
}
