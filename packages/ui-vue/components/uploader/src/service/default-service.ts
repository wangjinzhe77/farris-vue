import { BlobFile, UploadFile, UploadInput, UploadOutput, UploadServerAPI, UploadStatus } from "../composition/type";
import { humanizeBytes, parseResponseHeaders, secondsToHuman } from "../composition/utils";

export class UploaderDefaultService extends UploadServerAPI {

    upload(files: UploadFile[], event: UploadInput, extendConfig: any, eventEmit: (any) => void): Promise<UploadOutput> {
        const file = files[0];
        return new Promise((resolve, reject) => {
            const url = event.url || extendConfig.url || '';

            const method = event.method || 'POST';
            const data = event.data || {};
            const headers = event.headers || {};

            const xhr = new XMLHttpRequest();
            const time: number = new Date().getTime();
            let progressStartTime: number = (file.progress?.data && file.progress.data.startTime) || time;
            let speed = 0;
            let eta: number | null = null;

            xhr.upload.addEventListener('progress', (e: ProgressEvent) => {
                if (e.lengthComputable) {
                    const percentage = Math.round((e.loaded * 100) / e.total);
                    const diff = new Date().getTime() - time;
                    speed = Math.round(e.loaded / diff * 1000);
                    progressStartTime = (file.progress?.data && file.progress.data.startTime) || new Date().getTime();
                    eta = Math.ceil((e.total - e.loaded) / speed);

                    file.progress = {
                        status: UploadStatus.Uploading,
                        data: {
                            percentage,
                            speed,
                            speedHuman: `${humanizeBytes(speed)}/s`,
                            startTime: progressStartTime,
                            endTime: null,
                            eta,
                            etaHuman: secondsToHuman(eta)
                        }
                    };

                    eventEmit({ type: 'uploading', files: [file] });
                }
            }, false);

            xhr.upload.addEventListener('error', (e: Event) => {
                reject(e);
            });

            xhr.onreadystatechange = () => {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    const speedAverage = Math.round(file.size / (new Date().getTime() - progressStartTime) * 1000);
                    file.progress = {
                        status: UploadStatus.Done,
                        data: {
                            percentage: 100,
                            speed: speedAverage,
                            speedHuman: `${humanizeBytes(speedAverage)}/s`,
                            startTime: progressStartTime,
                            endTime: new Date().getTime(),
                            eta,
                            etaHuman: secondsToHuman(eta || 0)
                        }
                    };

                    file.responseStatus = xhr.status;

                    try {
                        file.response = JSON.parse(xhr.response);
                    } catch (e) {
                        file.response = xhr.response;
                    }

                    file.responseHeaders = parseResponseHeaders(xhr.getAllResponseHeaders());

                    resolve({ type: 'done', files: [file] });
                }
            };

            xhr.open(method, url, true);
            xhr.withCredentials = !!event.withCredentials;

            try {
                const uploadFile = <BlobFile>file.nativeFile;
                Object.keys(headers).forEach(key => xhr.setRequestHeader(key, headers[key]));

                let bodyToSend: FormData | BlobFile;

                if (event.includeWebKitFormBoundary !== false) {
                    Object.keys(data).forEach(key => file.form?.append(key, data[key]));
                    file.form?.append(event.fieldName || 'file', uploadFile, uploadFile.name);
                    bodyToSend = file.form as FormData;
                } else {
                    bodyToSend = uploadFile;
                }

                // serviceEvents.emit({ type: 'start', file: file });
                xhr.send(bodyToSend);
            } catch (e) {
                reject(e);
            }

             
            return () => {
                xhr.abort();
            };
        });
    }

    // 删除附件
    remove(files: UploadFile[], event: UploadInput, extendConfig: any, eventEmit: (any) => void): Promise<UploadOutput> {
        return new Promise((resolve, reject) => {
            resolve({ type: 'removed', files });
        });
    }
}
