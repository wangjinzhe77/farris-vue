import { ExtractPropTypes, PropType } from 'vue';
import { FUploadFileExtend, FUploadPreviewColumn, UploaderOptions } from './composition/type';
import { createPropsResolver } from '../../dynamic-resolver/src/props-resolver';
import { StepProps } from '../../step/src/step.props';
import uploaderSchema from './schema/uploader.schema.json';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import propertyConfig from './property-config/uploader.property-config.json';

export const uploaderProps = {

    /** 内容区域填充 */
    contentFill: { type: Boolean, default: false },
    /** 排序字段 */
    orderField: { type: String, default: 'createTime' },
    previewColumns: {
        type: Array<FUploadPreviewColumn>, default: [
            { field: "name", width: 200, title: "文件名", checkbox: true },
            { field: "size", width: 100, title: "大小" },
            { field: "createTime", width: 100, title: "日期" },
            { field: "state", width: 100, title: "状态" },
            { field: "action", width: 100, title: "操作" },
        ]
    },
    previewVisible: { type: Boolean, default: true },
    /** 是否启用批量操作 */
    previewEnableMulti: { type: Boolean, default: true },
    /** 默认重命名 */
    previewDefaultRename: { type: String, default: '' },
    /** 附件上传部分禁用状态 */
    uploadDisabled: { type: Boolean, default: false },
    // 附件上传部分是否可见
    uploadVisible: { type: Boolean, default: true },
    // 需要重置,通过不断的赋值来改变
    // @Input() uploadNeedReset: Observable<any> = new Subject();
    /** 选择文件按钮上的文字 */
    uploadSelectText: { type: String, default: '选择文件' },
    /** 启用多选 */
    uploadEnableMulti: { type: Boolean, default: true },
    /** 已经上传文件 */
    uploadedCount: { type: Number, default: 0 },
    /** 整个控件禁用 */
    disabled: { type: Boolean, default: false },
    /** 下载按钮是否禁用 */
    downloadButtonDisable: { type: Boolean, default: false },
    /** 预览按钮是否禁用 */
    previewButtonDisable: { type: Boolean, default: false },
    /** 删除按钮是否禁用 */
    deleteButtonDisable: { type: Boolean, default: false },
    /**
     * 自定义展示信息
     */
    customInfo: { type: String, default: '' },
    /**
     * 处理传递预览的数据
     */
    fileInfos: { type: Array<FUploadFileExtend>, default: [] },
    /**
     * 上传配置
     */
    uploadOptions: { type: Object as PropType<UploaderOptions> | null, default: null },
    /**
     * 发起服务器端请求，某个组件使用的特殊的参数
     */
    extendConfig: { type: Object, default: {} },
    /**
     * 附件服务器端服务注入的Token
     */
    uploadServerToken: { type: String, default: '' },
    /**
     * 提示服务注入的Token
     */
    notifyServiceToken: { type: String, default: '' }

} as Record<string, any>;

export type UploaderProps = ExtractPropTypes<typeof uploaderProps>;
export const propsResolver = createPropsResolver<StepProps>(uploaderProps, uploaderSchema, schemaMapper, schemaResolver, propertyConfig);
