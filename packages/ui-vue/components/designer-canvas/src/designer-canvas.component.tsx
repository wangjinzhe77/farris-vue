
import { computed, defineComponent, inject, onMounted, onUnmounted, provide, ref, watch } from 'vue';
import { ComponentSchema, DesignerComponentInstance, DesignerItemContext } from './types';
import { canvasChanged, setPositionOfButtonGroupInContainer } from './composition/designer-canvas-changed';
import { designerCanvasProps, DesignerCanvasPropsType } from './composition/props/designer-canvas.props';
import { useDragula } from './composition/function/use-dragula';
import { DesignerHostService, UseDragula } from './composition/types';
import FDesignerItem from './components/designer-item.component';
import './composition/class/designer-canvas.css';
import './composition/class/control.css';
import { loadDesignerRegister } from './components/maps';
import { F_MODAL_SERVICE_TOKEN } from '../../modal';

export default defineComponent({
    name: 'FDesignerCanvas',
    props: designerCanvasProps,
    emits: ['init', 'selectionChange', 'canvasChanged'],
    setup(props: DesignerCanvasPropsType, context) {
        const schema = ref();
        const componentSchema = ref();
        const designerCanvasElementRef = ref();
        const designerCanvasContainerElementRef = ref();
        const designerItemElementRef = ref();
        const componentInstance = ref();
        const componentId = ref(props.componentId);
        let resizeObserver: ResizeObserver | null;
        let resizeObserverTimer;

        const designerHostService = {
            eventsEditorUtils: inject('eventsEditorUtils'),
            formSchemaUtils: inject('useFormSchema'),
            formMetadataConverter: inject('formMetadataConverter'),
            designViewModelUtils: inject('designViewModelUtils'),
            controlCreatorUtils: inject('controlCreatorUtils'),
            metadataService: inject('Meatdata_Http_Service_Token'),
            schemaService: inject('schemaService'),
            useFormCommand: inject('useFormCommand'),
            modalService: inject(F_MODAL_SERVICE_TOKEN),
            formStateMachineUtils: inject('useFormStateMachine')
        };
        provide<DesignerHostService>('designer-host-service', designerHostService);

        const useDragulaComposition = useDragula(designerHostService);

        loadDesignerRegister();
        provide<UseDragula>('canvas-dragula', useDragulaComposition);
        provide<DesignerItemContext>('design-item-context', {
            designerItemElementRef,
            componentInstance,
            schema: componentSchema.value,
            parent: undefined
        });

        const designerCanvasClass = computed(() => {
            const classObject = {
                'd-flex': true,
                'flex-fill': true,
                'flex-column': true
            } as Record<string, boolean>;
            return classObject;
        });

        watch(canvasChanged, () => {
            setPositionOfButtonGroupInContainer(designerCanvasElementRef.value);

            context.emit('canvasChanged');
        }, { flush: 'post' });

        /**
         *  监听画布尺寸变化，重新计算操作图标位置
         */
        function registerResizeListenner() {
            resizeObserver = new ResizeObserver(() => {
                if (resizeObserverTimer) {
                    clearTimeout(resizeObserverTimer);
                }

                resizeObserverTimer = setTimeout(() => {
                    setPositionOfButtonGroupInContainer(designerCanvasElementRef.value);
                });
            });
            resizeObserver.observe(designerCanvasElementRef.value);
        }

        function onSelectionChange(schemaType: string, schemaValue: ComponentSchema, cmpId: string, componentInst: DesignerComponentInstance) {
            context.emit('selectionChange', schemaType, schemaValue, cmpId, componentInst);
        }

        /**
         * 监听画布父容器横向滚动条的滚动，重新计算操作图标位置
         */
        function registerEditorPanelScrollEvent() {
            designerCanvasContainerElementRef.value.addEventListener('scroll', (e) => {
                setPositionOfButtonGroupInContainer(designerCanvasElementRef.value);
            });
        }
        onMounted(() => {
            if (designerCanvasElementRef.value) {
                useDragulaComposition.initializeDragula(designerCanvasElementRef.value);
            }
            schema.value = props.modelValue;
            context.emit('init', useDragulaComposition);
            registerResizeListenner();
            registerEditorPanelScrollEvent();
        });

        onUnmounted(() => {
            if (resizeObserver) {
                resizeObserver.unobserve(designerCanvasElementRef.value);
                resizeObserver.disconnect();
                resizeObserver = null;
            }
        });
        return () => {
            return (
                <div class="editorDiv flex-fill h-100" ref={designerCanvasContainerElementRef}>
                    <div class="editorPanel d-flex flex-fill flex-column PC">
                        <div ref={designerCanvasElementRef} class={designerCanvasClass.value}>
                            {schema.value && <FDesignerItem v-model={schema.value} onSelectionChange={onSelectionChange} componentId={componentId.value}></FDesignerItem>}
                        </div>
                    </div>
                </div>
            );
        };
    }
});
