
import { Ref, SetupContext, computed, defineComponent, inject, onMounted, provide, ref, watch, onBeforeUnmount } from 'vue';
import { DesignerItemPropsType, designerItemProps } from '../composition/props/designer-item.props';
import { componentMap, componentPropsConverter } from './maps';
import { UseDragula } from '../composition/types';
import { ComponentSchema, DesignerComponentInstance, DesignerItemContext } from '../types';
import FDesignerPlaceholder from './designer-placeholder.component';
import { canvasChanged, setPositionOfButtonGroup, setPositionOfButtonGroupInContainer } from '../composition/designer-canvas-changed';
import { getCustomClass } from '@farris/ui-vue/components/common';

const FDesignerItem = defineComponent({
    name: 'FDesignerItem',
    props: designerItemProps,
    emits: ['selectionChange'],
    setup(props: DesignerItemPropsType, context) {
        const id = ref(`${props.modelValue.id}-component`);
        const canMove = ref(props.canMove);
        const canDelete = ref(props.canDelete);
        const canNested = ref(false);
        const schema = ref(props.modelValue);
        const componentId = ref(props.componentId || '');
        const designComponentStyle = ref('');
        const designComponentClass = ref('');
        const designCustomClass = ref(props.customClass);
        const designerItemElementRef = ref();
        const useDragulaComposition = inject<UseDragula>('canvas-dragula');
        const componentInstance = ref() as Ref<DesignerComponentInstance>;
        const parent = inject<DesignerItemContext>('design-item-context');
        const designItemContext = { designerItemElementRef, componentInstance, schema: schema.value, parent, setupContext: context as SetupContext };
        provide<DesignerItemContext>('design-item-context', designItemContext);
        const useFormSchema = inject<any>('useFormSchema');

        const designerItemClass = computed(() => {
            const componentClass = props.modelValue.appearance ? (props.modelValue.appearance.class as string) || '' : '';
            const customButtons = componentInstance.value?.getCustomButtons && componentInstance.value.getCustomButtons();
            let classObject = {
                'farris-component': true,
                // 受position-relative影响，整个容器的高度不能被撑起
                'flex-fill': schema.value.id === 'root-component',
                'position-relative': canMove.value && canDelete.value || (customButtons?.length),
                'farris-nested': canNested.value,
                'can-move': canMove.value,
                'd-none': designerItemElementRef.value && (designerItemElementRef.value as HTMLElement).classList.contains('d-none'),
                'dgComponentSelected': designerItemElementRef.value && (designerItemElementRef.value as HTMLElement).classList.contains('dgComponentSelected'),
                'dgComponentFocused': designerItemElementRef.value && (designerItemElementRef.value as HTMLElement).classList.contains('dgComponentFocused'),
            } as Record<string, boolean>;
            classObject[`farris-component-${schema.value.type}`] = true;
            classObject = getCustomClass(classObject, componentClass);
            classObject = getCustomClass(classObject, designComponentClass.value);
            classObject = getCustomClass(classObject, designCustomClass.value);
            return classObject;
        });

        const desginerItemStyle = computed(() => {
            const styleObject = {} as Record<string, any>;
            const componentStyle = props.modelValue.appearance ? (props.modelValue.appearance.style as string) || '' : '';
            if (componentStyle) {
                componentStyle.split(';').reduce((result: Record<string, any>, styleString: string) => {
                    const [styleKey, styleValue] = styleString.split(':');
                    result[styleKey] = styleValue;
                    return result;
                }, styleObject);
            }
            if (designComponentStyle.value) {
                designComponentStyle.value.split(';').reduce((result: Record<string, any>, styleString: string) => {
                    const [styleKey, styleValue] = styleString.split(':');
                    if (styleKey) {
                        result[styleKey] = styleValue;
                    }
                    return result;
                }, styleObject);
            }
            return styleObject;
        });

        function onClickDeleteButton(payload: MouseEvent, schemaToRemove: ComponentSchema) {
            if (payload) {
                payload.preventDefault();
                payload.stopPropagation();
            }

            // 连同所属组件一起删除，使用场景如data-grid、form控件等。
            if (componentInstance.value.triggerBelongedComponentToDeleteWhenDeleted) {
                const belongedComponentInstance = componentInstance.value.getBelongedComponentInstance(componentInstance);
                if (belongedComponentInstance && belongedComponentInstance.parent) {
                    const belongedComponentInstanceParent = ref(belongedComponentInstance?.parent) as any;
                    const indexToRemove = belongedComponentInstanceParent.value.contents.findIndex(
                        (contentItem: ComponentSchema) => contentItem.id === belongedComponentInstance.schema.id
                    );
                    belongedComponentInstanceParent.value?.contents?.splice(indexToRemove, 1);

                    canvasChanged.value++;
                }
                return;
            }
            componentInstance.value.onRemoveComponent();
            let parentContext = parent;
            let locatePredicate: any = (contentItem: ComponentSchema) => contentItem.id === schemaToRemove.id;
            if (schemaToRemove.type === 'component') {
                parentContext = parent?.parent;
                locatePredicate = (contentItem: ComponentSchema) => contentItem.component === schemaToRemove.id;

            }
            if (parentContext && parentContext.schema.contents) {
                const indexToRemove = parentContext.schema.contents.findIndex(locatePredicate);
                parentContext.schema.contents.splice(indexToRemove, 1);

                canvasChanged.value++;
                context.emit('selectionChange');

            }

        }

        function renderDeleteButton(componentSchema: ComponentSchema) {
            return (
                canDelete.value && (
                    <div
                        role="button"
                        class="btn component-settings-button"
                        title="删除"
                        ref="removeComponent"
                        onClick={(payload: MouseEvent) => {
                            onClickDeleteButton(payload, componentSchema);
                        }}>
                        <i class="f-icon f-icon-yxs_delete"></i>
                    </div>
                )
            );
        }

        function renderMoveButton() {
            return (
                canMove.value && (
                    <div role="button" class="btn component-settings-button" title="移动" ref="moveComponent">
                        <i data-dragging-icon="true" class="cmp_move f-icon f-icon-yxs_move"></i>
                    </div>
                )
            );
        }

        function renderCustomButtons() {
            const customButtons = componentInstance.value?.getCustomButtons && componentInstance.value.getCustomButtons();

            return (
                customButtons &&
                !!customButtons.length &&
                customButtons.map((buttonConfig: any) => {
                    return (
                        <div
                            role="button"
                            class={`btn component-settings-button ${buttonConfig.class || ''}`}
                            title={buttonConfig.title}
                            ref={buttonConfig.id}
                            onClick={(payload: MouseEvent) => buttonConfig.onClick && buttonConfig.onClick(payload)}>
                            <i class={buttonConfig.icon}></i>
                        </div>
                    );
                })
            );
        }

        function renderIconPanel(componentSchema: ComponentSchema) {
            return (
                <div class="component-btn-group" data-noattach="true">
                    <div>
                        {renderDeleteButton(componentSchema)}
                        {renderMoveButton()}
                        {renderCustomButtons()}
                    </div>
                </div>
            );
        }

        function onSelectionChange(schemaType: string, schemaValue: ComponentSchema, componentId: string, componentInstance: DesignerComponentInstance) {
            context.emit('selectionChange', schemaType, schemaValue, componentId, componentInstance);
        }

        function renderContent(viewSchema: ComponentSchema) {
            const componentKey = viewSchema.type;
            const Component = componentMap[componentKey];
            const propsConverter = componentPropsConverter[componentKey];
            const viewProps = propsConverter ? propsConverter(viewSchema) : {};
            viewProps.customClass = props.ignore ? viewProps.customClass : '';
            viewProps.componentId = componentId.value;
            viewProps.id = viewSchema.id;
            const shouldShowPlaceholder = viewSchema.contents && viewSchema.contents.length === 0;
            const hasContent = viewSchema.contents && !!viewSchema.contents.length;
            return hasContent && Component ? (
                <Component ref={componentInstance} {...viewProps}>
                    {(viewSchema.contents as ComponentSchema[]).map((contentSchema: any) => (
                        <FDesignerItem key={contentSchema.id} v-model={contentSchema} componentId={componentId.value} onSelectionChange={onSelectionChange}></FDesignerItem>
                    ))}
                </Component>
            ) : Component ? (
                shouldShowPlaceholder ? (
                    <Component ref={componentInstance} {...viewProps}>
                        <FDesignerPlaceholder></FDesignerPlaceholder>
                    </Component>
                ) : (
                    <Component ref={componentInstance} {...viewProps}></Component>
                )
            ) : (
                <div></div>
            );
        }

        function renderChildComponentContent(viewSchema: ComponentSchema) {
            const componentKey = viewSchema.type;
            if (componentKey === 'component-ref') {
                // eslint-disable-next-line prefer-const
                let componentSchema = useFormSchema?.getFormSchema().module.components
                    .find((component: any) => component.id === viewSchema.component);
                if (componentSchema) {

                    return <FDesignerItem key={componentSchema.id} v-model={componentSchema} componentId={componentSchema.id} onSelectionChange={onSelectionChange}></FDesignerItem>;
                }
            }
        }
        watch(
            () => props.modelValue,
            (value: any) => {
                schema.value = value;
                id.value = `${value.id}-component`;
            }
        );

        /**
         * 记录滚动区域
         */
        function recordScrollContainer(element: HTMLElement) {
            if (!window['scrollContainerList']) { window['scrollContainerList'] = new Set(); }

            const id = element.getAttribute('id');
            if (id) {
                window['scrollContainerList'].add(id);
            }
        }

        function updatePositionOfBtnGroupWhenScroll(event: Event | any) {
            const targetElement = event?.target as any;
            recordScrollContainer(targetElement);
            setPositionOfButtonGroupInContainer(targetElement);
        }
        function bindingScrollEvent() {
            if (schema.value?.contents?.length && designerItemElementRef.value) {
                designerItemElementRef.value.addEventListener('scroll', updatePositionOfBtnGroupWhenScroll);
            }
        }

        onMounted(() => {
            if (designerItemElementRef.value && componentInstance.value && componentInstance.value.schema) {
                const draggableContainer = designerItemElementRef.value.querySelector(
                    `[data-dragref='${componentInstance.value.schema.id}-container']`
                );
                if (useDragulaComposition && draggableContainer) {
                    useDragulaComposition.attachComponents(draggableContainer, schema.value);
                }
                canNested.value = componentInstance.value.canNested !== undefined ? componentInstance.value.canNested : canNested.value;
                canDelete.value = componentInstance.value.canDelete !== undefined ? componentInstance.value.canDelete : canDelete.value;
                canMove.value = componentInstance.value.canMove !== undefined ? componentInstance.value.canMove : canMove.value;
                designComponentStyle.value = componentInstance.value.styles || '';
                designComponentClass.value = componentInstance.value.designerClass || '';
                if (designerItemElementRef.value) {
                    designerItemElementRef.value.componentInstance = componentInstance;
                    designerItemElementRef.value.designItemContext = designItemContext;
                }
                componentInstance.value.belongedComponentId = componentId.value;

            }
            bindingScrollEvent();

            canvasChanged.value++;
        });

        onBeforeUnmount(() => {
            if (designerItemElementRef.value) {
                designerItemElementRef.value.removeEventListener('scroll', updatePositionOfBtnGroupWhenScroll);
            }
        });

        function onClickDesignerItem(payload: MouseEvent) {
            if (payload) {
                payload.preventDefault();
                payload.stopPropagation();
            }
            let draggabledesignerItemElementRef: any = designItemContext.designerItemElementRef;
            const designerItemElement = designerItemElementRef.value as HTMLElement;
            if (designerItemElement) {
                const currentFocusedElements = document.getElementsByClassName('dgComponentFocused') as HTMLCollectionOf<HTMLElement>;
                // 重复点击
                const duplicateClick =
                    currentFocusedElements &&
                    currentFocusedElements.length === 1 &&
                    currentFocusedElements[0] === designerItemElementRef.value;
                if (!duplicateClick) {
                    Array.from(currentFocusedElements).forEach((element: HTMLElement) => element.classList.remove('dgComponentFocused'));
                    Array.from(document.getElementsByClassName('dgComponentSelected') as HTMLCollectionOf<HTMLElement>).forEach(
                        (element: HTMLElement) => element.classList.remove('dgComponentSelected')
                    );

                    designerItemElement.classList.add('dgComponentFocused');
                    context.emit('selectionChange', schema.value.type, schema.value, componentId.value, componentInstance.value);
                    if (componentInstance.value.getDraggableDesignItemElement) {
                        draggabledesignerItemElementRef = componentInstance.value.getDraggableDesignItemElement(designItemContext);
                        if (draggabledesignerItemElementRef && draggabledesignerItemElementRef.value) {
                            draggabledesignerItemElementRef.value.classList.add('dgComponentSelected');

                        }
                    }

                }
            }

            setPositionOfButtonGroup(draggabledesignerItemElementRef?.value);
        }

        return () => {
            return (
                schema.value.type === 'component-ref' ?
                    renderChildComponentContent(schema.value) :
                    <div
                        id={`${schema.value.id}-design-item`}
                        ref={designerItemElementRef}
                        class={designerItemClass.value}
                        style={desginerItemStyle.value}
                        onClick={onClickDesignerItem}>
                        {renderIconPanel(schema.value)}
                        {renderContent(schema.value)}
                    </div>
            );
        };
    }
});
export default FDesignerItem;
