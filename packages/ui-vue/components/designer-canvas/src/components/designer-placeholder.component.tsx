import { SetupContext, computed, defineComponent } from 'vue';
import { DesignerPlaceholderPropsType, designerPlaceholderProps } from '../composition/props/designer-placeholder.props';

export default defineComponent({
    name: 'FDesignerPlaceholder',
    props: designerPlaceholderProps,
    emits: [],
    setup(props: DesignerPlaceholderPropsType) {
        const designerPlaceholderClass = computed(() => {
            const classObject = {
                'drag-and-drop-alert': true,
                'no-drag': true,
                'w-100': true
            } as Record<string, boolean>;
            return classObject;
        });

        const designerPlaceholderStyle = computed(() => {
            const styleObject = {
                'height': '60px',
                'display': 'flex',
                'justify-content': 'center',
                'align-items': 'center',
                'margin': 0,
                'padding': '.75rem 1.25rem',
                'border': '1px solid transparent',
                'border-radius': '3px',
                'color': '#315585',
                'background-color': '#dfedff',
                'border-color': '#d2e6ff',
            } as Record<string, any>;
            return styleObject;
        });

        return () => (
            <div
                class={designerPlaceholderClass.value}
                style={designerPlaceholderStyle.value}
                role="alert"
                data-noattach="true"
                data-position="0">
                拖拽组件到这里
            </div>
        );
    }
});
