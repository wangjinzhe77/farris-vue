import { computed, defineComponent, ref } from 'vue';
import { DesignerItemPropsType, designerItemProps } from '../composition/props/designer-item.props';
import { canvasChanged } from '../composition/designer-canvas-changed';

const FDesignerTemplateItem = defineComponent({
    name: 'FDesignerTemplateItem',
    props: designerItemProps,
    emits: ['selectionChange'],
    setup(props: DesignerItemPropsType, context) {
        const designerItemElementRef = ref();

        const designerItemClass = computed(() => {
            const classObject = {
                'farris-component': true
            } as Record<string, boolean>;
            return classObject;
        });

        function onClickDesignerItem(payload: MouseEvent) {
            
            if (payload) {
                payload.preventDefault();
                payload.stopPropagation();
            }
            const designerItemElement = designerItemElementRef.value as HTMLElement;
            if (designerItemElement) {
                const currentSelectedElements = document.getElementsByClassName('dgComponentFocused') as HTMLCollectionOf<HTMLElement>;
                
                // 重复点击
                const duplicateClick =
                    currentSelectedElements &&
                    currentSelectedElements.length === 1 &&
                    currentSelectedElements[0] === designerItemElementRef.value;
                if (!duplicateClick) {
                    Array.from(currentSelectedElements).forEach((element: HTMLElement) => element.classList.remove('dgComponentSelected'));
                    Array.from(currentSelectedElements).forEach((element: HTMLElement) => element.classList.remove('dgComponentFocused'));

                    designerItemElement.classList.add('dgComponentFocused');
                }
            }
            canvasChanged.value++;
        }

        return () => {
            return (
                <div
                    id={`${props.id}-design-item`}
                    ref={designerItemElementRef}
                    class={designerItemClass.value}
                    onClick={onClickDesignerItem}>
                    {context.slots.default && context.slots.default()}
                </div>
            );
        };
    }
});
export default FDesignerTemplateItem;
