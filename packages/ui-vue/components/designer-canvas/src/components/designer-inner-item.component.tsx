import { Ref, SetupContext, computed, defineComponent, inject, onMounted, provide, ref, watch, onBeforeUnmount, withModifiers } from 'vue';
import { DesignerInnerItemPropsType, designerInnerItemProps } from '../composition/props/designer-inner-item.props';
import { DraggingResolveContext, UseDragula } from '../composition/types';
import { ComponentSchema, DesignerComponentInstance, DesignerItemContext } from '../types';
import { canvasChanged, setPositionOfButtonGroup, setPositionOfButtonGroupInContainer } from '../composition/designer-canvas-changed';
import { useDesignerInnerComponent } from '../composition/function/use-designer-inner-component';

const FDesignerInnerItem = defineComponent({
    name: 'FDesignerInnerItem',
    props: designerInnerItemProps,
    emits: ['selectionChange', 'addComponent', 'removeComponent'],
    setup(props: DesignerInnerItemPropsType, context) {
        const canMove = ref(props.canMove);
        const canAdd = ref(props.canAdd);
        const canDelete = ref(props.canDelete);
        const canNested = ref(false);
        const contentKey = ref(props.contentKey);
        const childType = ref(props.childType);
        const childLabel = ref(props.childLabel);
        const schema = ref(props.modelValue);
        const designComponentStyle = ref('');
        const designerItemElementRef = ref();
        const useDragulaComposition = inject<UseDragula>('canvas-dragula');
        const componentInstance = ref() as Ref<DesignerComponentInstance>;
        const parent = inject<DesignerItemContext>('design-item-context');
        const designItemContext = { designerItemElementRef, componentInstance, schema: schema.value, parent, setupContext: context as SetupContext };
        provide<DesignerItemContext>('design-item-context', designItemContext);

        const designerItemClass = computed(() => {
            const classObject = {
                'farris-component': true,
                // 受position-relative影响，整个容器的高度不能被撑起
                'flex-fill': props.id === 'root-component',
                'position-relative': canMove.value || canDelete.value,
                'farris-nested': canNested.value,
                'can-move': canMove.value,
                'd-none': designerItemElementRef.value && (designerItemElementRef.value as HTMLElement).classList.contains('d-none')
            } as Record<string, boolean>;
            return classObject;
        });

        const desginerItemStyle = computed(() => {
            const styleObject = {} as Record<string, any>;
            if (designComponentStyle.value) {
                designComponentStyle.value.split(';').reduce((result: Record<string, any>, styleString: string) => {
                    const [styleKey, styleValue] = styleString.split(':');
                    if (styleKey) {
                        result[styleKey] = styleValue;
                    }
                    return result;
                }, styleObject);
            }
            return styleObject;
        });

        function onClickDeleteButton(payload: MouseEvent, schemaToRemove: ComponentSchema) {
            if (parent && parent.schema[contentKey.value]) {
                const indexToRemove = parent.schema[contentKey.value].findIndex(
                    (contentItem: ComponentSchema) => contentItem.id === schemaToRemove.id
                );
                // 如果仍然存在子级，点击子级事件，展示属性面板
                if (indexToRemove > -1) {
                    const childrenSize = parent.schema[contentKey.value].length;
                    const nextSchema = parent.schema[contentKey.value][indexToRemove % childrenSize];
                    const designerItem = parent.designerItemElementRef.value.querySelector(
                        `#${nextSchema.id}-design-item`
                    );
                    parent.schema[contentKey.value].splice(indexToRemove, 1);

                    // 此处删除子组件schame后，会自动选中第一个元素但属性面板不更新，暂时绕过此问题，使用emit  removeComponent方式来解决
                    canvasChanged.value++;
                    context.emit('removeComponent');
                    context.emit('selectionChange');
                }
            }

        }

        function onClickAddButton(payload: MouseEvent) {
            if (componentInstance.value.addNewChildComponentSchema) {
                const resolveContext = {
                    componentType: childType.value,
                    label: childLabel.value,
                    parentComponentInstance: componentInstance.value,
                    targetPosition: -1
                } as DraggingResolveContext;
                const childComponentSchema = componentInstance.value.addNewChildComponentSchema(resolveContext);
                schema.value[contentKey.value].push(childComponentSchema);
                context.emit('addComponent');
            }
        }

        function renderAddButton() {
            return (
                canAdd.value && (
                    <div
                        role="button"
                        class="btn component-settings-button"
                        title="新增"
                        ref="removeComponent"
                        onClick={(payload: MouseEvent) => {
                            onClickAddButton(payload);
                        }}>
                        <i class="f-icon f-icon-plus-circle"></i>
                    </div>
                )
            );
        }

        function renderDeleteButton(componentSchema: ComponentSchema) {
            return (
                canDelete.value && (
                    <div
                        role="button"
                        class="btn component-settings-button"
                        title="删除"
                        ref="removeComponent"
                        onClick={withModifiers((payload: Event) =>
                            onClickDeleteButton(payload as MouseEvent, componentSchema),
                            ['stop'])}>
                        <i class="f-icon f-icon-yxs_delete"></i>
                    </div>
                )
            );
        }
        function renderMoveButton() {
            return (
                canMove.value && (
                    <div role="button" class="btn component-settings-button" title="移动" ref="moveComponent">
                        <i data-dragging-icon="true" class="cmp_move f-icon f-icon-yxs_move"></i>
                    </div>
                )
            );
        }

        function renderIconPanel(componentSchema: ComponentSchema) {
            return (
                <div class="component-btn-group" data-noattach="true">
                    <div>
                        {renderAddButton()}
                        {renderMoveButton()}
                        {renderDeleteButton(componentSchema)}
                    </div>
                </div>
            );
        }

        watch(
            () => props.modelValue,
            (value: any) => {
                schema.value = value;
                designItemContext.schema = value;
            }
        );
        /**
         * 记录滚动区域
         */
        function recordScrollContainer(element: HTMLElement) {
            if (!window['scrollContainerList']) { window['scrollContainerList'] = new Set(); }

            const id = element.getAttribute('id');
            if (id) {
                window['scrollContainerList'].add(id);
            }
        }
        function updatePositionOfBtnGroupWhenScroll(e: Event | any) {
            const targetElement = e.target as any;
            recordScrollContainer(targetElement);
            setPositionOfButtonGroupInContainer(targetElement);
        }

        function bindingScrollEvent() {
            if (schema.value?.contents?.length && designerItemElementRef.value) {
                designerItemElementRef.value.addEventListener('scroll', updatePositionOfBtnGroupWhenScroll);
            }
        }

        function createDefaultComponentInstance() {
            const designerItemElement = designerItemElementRef.value as HTMLElement;
            const innerComponentElementRef = ref(designerItemElement.children[1] as HTMLElement);
            const innerComponentInstance = useDesignerInnerComponent(innerComponentElementRef, designItemContext);
            return innerComponentInstance.value;
        }

        onMounted(() => {
            if (designerItemElementRef.value) {
                const draggableContainer = designerItemElementRef.value.querySelector(
                    `[data-dragref='${schema.value.id}-container']`
                );
                componentInstance.value = (draggableContainer && draggableContainer.componentInstance) ?
                    draggableContainer.componentInstance.value : createDefaultComponentInstance();

                if (useDragulaComposition && draggableContainer) {
                    useDragulaComposition.attachComponents(draggableContainer, schema.value);
                }
                canNested.value = componentInstance.value.canNested !== undefined ? componentInstance.value.canNested : canNested.value;
                canAdd.value = componentInstance.value.canAdd !== undefined ? componentInstance.value.canAdd : canAdd.value;
                canDelete.value = componentInstance.value.canDelete !== undefined ? componentInstance.value.canDelete : canDelete.value;
                canMove.value = componentInstance.value.canMove !== undefined ? componentInstance.value.canMove : canMove.value;
                designComponentStyle.value = componentInstance.value.styles || '';
                if (designerItemElementRef.value) {
                    designerItemElementRef.value.componentInstance = componentInstance;
                    designerItemElementRef.value.designItemContext = designItemContext;
                }
            }
            bindingScrollEvent();

            canvasChanged.value++;
        });

        onBeforeUnmount(() => {
            if (designerItemElementRef.value) {
                designerItemElementRef.value.removeEventListener('scroll', updatePositionOfBtnGroupWhenScroll);
            }
        });

        function onClickDesignerItem(payload: MouseEvent) {

            if (payload) {
                payload.preventDefault();
                payload.stopPropagation();
            }
            let draggabledesignerItemElementRef: any = designItemContext.designerItemElementRef;
            const designerItemElement = designerItemElementRef.value as HTMLElement;
            if (designerItemElement) {
                const currentFocusedElements = document.getElementsByClassName('dgComponentFocused') as HTMLCollectionOf<HTMLElement>;
                // 重复点击
                const duplicateClick =
                    currentFocusedElements &&
                    currentFocusedElements.length === 1 &&
                    currentFocusedElements[0] === designerItemElementRef.value;
                if (!duplicateClick) {
                    Array.from(currentFocusedElements).forEach((element: HTMLElement) => element.classList.remove('dgComponentFocused'));
                    Array.from(document.getElementsByClassName('dgComponentSelected') as HTMLCollectionOf<HTMLElement>).forEach(
                        (element: HTMLElement) => element.classList.remove('dgComponentSelected')
                    );

                    designerItemElement.classList.add('dgComponentFocused');
                    context.emit('selectionChange', schema.value.type, schema.value, props.componentId, componentInstance.value);
                    if (componentInstance.value.getDraggableDesignItemElement) {
                        draggabledesignerItemElementRef = componentInstance.value.getDraggableDesignItemElement(designItemContext);
                        if (draggabledesignerItemElementRef && draggabledesignerItemElementRef.value) {
                            draggabledesignerItemElementRef.value.classList.add('dgComponentSelected');

                        }
                    }
                }
            }
            setPositionOfButtonGroup(draggabledesignerItemElementRef?.value);
        }

        return () => {
            return (
                <div
                    id={`${props.id}-design-item`}
                    ref={designerItemElementRef}
                    class={designerItemClass.value}
                    style={desginerItemStyle.value}
                    onClick={onClickDesignerItem}>
                    {renderIconPanel(schema.value)}
                    {context.slots.default && context.slots.default()}
                </div>
            );
        };
    }
});
export default FDesignerInnerItem;
