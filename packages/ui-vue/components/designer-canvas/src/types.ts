/* eslint-disable no-use-before-define */
import { Ref, SetupContext } from "vue";
import { DesignerHostService, DesignerHTMLElement, DraggingResolveContext } from "./composition/types";

export interface ComponentSchema {

    /** 设计时使用 */
    key?: string;

    id: string;

    type: string;

    contents?: ComponentSchema[];

    // 其他属性
    [propName: string]: any;
}

export interface DesignerComponentInstance {

    canAdd?: boolean;

    canMove: boolean;

    canSelectParent: boolean;

    canDelete: boolean;

    canNested: boolean;

    contents?: ComponentSchema[];

    elementRef: Ref<any>;

    parent: Ref<DesignerComponentInstance> | undefined;

    schema: ComponentSchema;

    styles?: string;

    designerClass?: string;

    canAccepts: (draggingContext: DraggingResolveContext) => boolean;

    getBelongedComponentInstance: (componentInstance: Ref<DesignerComponentInstance>) => DesignerComponentInstance | null;

    /** 获取可拖拽的上层容器 */
    getDraggableDesignItemElement: (context: DesignerItemContext) => Ref<any> | null;

    getDraggingDisplayText: () => string;

    getDragScopeElement: () => HTMLElement | undefined;

    /** 移动内部控件后事件：在可视化设计器中，容器接收控件后事件 */
    onAcceptMovedChildElement: (element: DesignerHTMLElement, soureElement?: DesignerHTMLElement) => void;

    onChildElementMovedOut: (element: HTMLElement) => void;

    addNewChildComponentSchema: (resolveContext: DraggingResolveContext, designerHostService?: DesignerHostService) => ComponentSchema;

    /** 组件在拖拽时是否需要将所属的Component一起拖拽，用于form、data-grid等控件的拖拽 */
    triggerBelongedComponentToMoveWhenMoved?: Ref<boolean>;

    /** 组件在被移除时是否需要将所属的Component一起移除，用于form、data-grid等控件的拖拽 */
    triggerBelongedComponentToDeleteWhenDeleted?: Ref<boolean>;

    /** 获取属性配置 */
    getPropConfig: (...args) => any;

    /** 控件所属Component的标识*/
    belongedComponentId?: string;

    /** 控件删除后事件 */
    onRemoveComponent: () => void;

    /** 获取控件自定义操作按钮 */
    getCustomButtons?: () => DesignerComponentButton[];

    /** 控件属性变更后事件 */
    onPropertyChanged?: (event:any) => void;
}

export interface DesignerItemContext {

    designerItemElementRef: Ref<HTMLElement>;

    componentInstance: Ref<DesignerComponentInstance>;

    schema: ComponentSchema;

    parent?: DesignerItemContext;

    setupContext?: SetupContext;

}

/**
 * 控件自定义操作按钮
 */
export interface DesignerComponentButton {
    id: string;
    title: string;
    icon: string;
    class?: string;
    onClick: (payload: MouseEvent) => void;
}
