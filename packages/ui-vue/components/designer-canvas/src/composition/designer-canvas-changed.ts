import { ref } from "vue";

/** 用于响应画布发生变更 */
export const canvasChanged = ref(0);

/**
 * 判断DOM 是否在可视区域内
 * @param el 元素
 * @param containerEl 容器
 */
function isElementInViewport(el: HTMLElement, containerEl: HTMLElement) {
    const container = containerEl.getBoundingClientRect();
    const box = el.getBoundingClientRect();
    const top = box.top >= container.top;
    const bottom = box.top <= container.bottom;
    return (top && bottom);
}
/**
 * 计算选中控件的绝对位置
 */
function setPositionForSelectedElement(selectElement: HTMLElement) {
    const toolbar = selectElement.querySelector('.component-btn-group') as HTMLElement;
    if (!toolbar) {
        return;
    }
    toolbar.style.display = '';
    const toolbarRect = toolbar.getBoundingClientRect();
    const divPanel = toolbar.querySelector('div') as HTMLElement;
    if (divPanel) {
        const divPanelRect = divPanel.getBoundingClientRect();
        divPanel.style.top = toolbarRect.top + 'px';

        // 若操作按钮的最左边比画布最左边还要靠左，那么操作按钮就以控件的最左边为界
        let left = toolbarRect.left - divPanelRect.width;
        const editorDiv = document.querySelector('.editorDiv');
        if (editorDiv) {
            const editorDivRect = editorDiv.getBoundingClientRect();
            if (left < editorDivRect.left) {
                const elementRect = selectElement.getBoundingClientRect();
                ({ left } = elementRect);
            }
        }
        divPanel.style.left = left + 'px';
    }
}
/**
 * 获取选中控件所在的滚动区域
 */
function getScrollParentElementWhenClick(selectElement: HTMLElement) {
    if (!window['scrollContainerList']) {
        return;
    }
    const scrollContainerArray = Array.from(window['scrollContainerList']);
    if (!scrollContainerArray.length) {
        return;
    }
    // 1、若当前只有一个滚动区域：返回滚动区域
    if (scrollContainerArray.length === 1) {
        const scrollContainerId = scrollContainerArray[0];
        const scrollContainer = document.querySelector(`[id=${scrollContainerId}]`) as HTMLElement;

        if (scrollContainer && scrollContainer.contains(selectElement)) {
            return scrollContainer;
        }

    }
}
/**
 * 定位画布中已选控件的操作按钮的位置
 * @param selectElement 选中的控件元素
 */
export function setPositionOfButtonGroup(selectElement: HTMLElement) {
    if (!selectElement) {
        return;
    }

    const selectDomRect = selectElement.getBoundingClientRect();
    if (selectDomRect.width === 0 && selectDomRect.height === 0) {
        return;
    }
    const toolbar = selectElement.querySelector('.component-btn-group') as HTMLElement;
    if (toolbar) {
        let isInView = true;
        const scrollParentEle = getScrollParentElementWhenClick(selectElement);
        if (scrollParentEle) {
            isInView = isElementInViewport(selectElement, scrollParentEle);
        }

        if (!isInView) {
            toolbar.style.display = 'none';
            return;
        }
        // 计算位置
        setPositionForSelectedElement(selectElement);
    }
}


/**
 * 定位画布中已选控件的操作按钮的位置
 * @param containerElement 监听的父容器
 */
export function setPositionOfButtonGroupInContainer(containerElement: HTMLElement) {
    if (!containerElement) {
        return;
    }
    let selectElement: HTMLElement;
    if (containerElement.className.includes('dgComponentSelected')) {
        selectElement = containerElement;
    } else {
        selectElement = containerElement.querySelector('.dgComponentSelected') as HTMLElement;
    }
    if (!selectElement) {
        return;
    }

    setPositionOfButtonGroup(selectElement);
}

/**
 * 定位页面中选中控件的操作按钮位置。
 * 场景：控件内部点击收折或者切换显示内容后，需要重新计算页面中下方选中控件的按钮位置。
 * 例如点击section控件的收折图标后，需要重新计算section下方已选控件的操作按钮位置
 */
export function setPositionOfSelectedComponentBtnGroup(canvasElement: HTMLElement) {
    const selectElement = document.querySelector('.dgComponentSelected') as HTMLElement;
    if (!selectElement) {
        return;
    }
    const selectedElementRect = selectElement.getBoundingClientRect();
    const elementRect = canvasElement.getBoundingClientRect();

    const toolbar = selectElement.querySelector('.component-btn-group') as HTMLElement;
    if (toolbar) {
        const toolbarRect = toolbar.getBoundingClientRect();
        const isBelow = elementRect.top < selectedElementRect.top;

        // 选中控件已显示并且在基准位置的下方
        if (toolbarRect.top !== 0 && isBelow) {
            setPositionForSelectedElement(selectElement);
        }
    }
}
