import { ModalFunctions } from "../../../../modal/src/composition/type";
import { ComponentBindingSourceContext, DesignerHostService, DesignerHTMLElement, DraggingResolveContext } from "../types";
import EntityBindingSelectorComponent from '../../../../entity-binding-selector/entity-binding-selector.component';
import { FBindingSelectorContainer as BindingSelectorComponent } from "@farris/ui-vue/components/binding-selector";
import { DesignViewModelField, FormVariable } from "../../../../common/entity/entity-schema";
import { merge } from "lodash-es";
import { DesignerComponentInstance } from "../../types";
import { DgControl } from "../dg-control";
import { FNotifyService } from "@farris/ui-vue/components/notify";

export function dragResolveService(designerHostService: DesignerHostService) {
    /** 弹窗实例 */
    let modalEditorRef: ModalFunctions;
    /** 拖拽上下文 */
    let componentResolveContext: DraggingResolveContext;

    /**
    * 获取拖拽上下文信息
    */
    function getComponentResolveContext(sourceElement: DesignerHTMLElement, sourceContainer: DesignerHTMLElement, targetContainer: DesignerHTMLElement)
        : DraggingResolveContext {

        const resolveContext: DraggingResolveContext = {
            componentType: sourceElement.getAttribute('data-controltype') || '',
            componentFeature: sourceElement.getAttribute('data-feature') || '',
            componentCategory: sourceElement.getAttribute('data-category') || '',
            label: sourceElement.getAttribute('data-controlTypeName') || '',
            sourceType: sourceElement.getAttribute('data-sourceType') || 'move',
            parentComponentInstance: targetContainer.componentInstance.value,
            sourceElement,
            sourceContainer,
            targetContainer,
            bindingSourceContext: null,
            bindingTargetId: sourceElement.getAttribute('data-fieldId') || ''
        };

        // 现有控件移动位置：从控件实例上获取控件类型
        if (sourceElement.componentInstance) {
            resolveContext.componentType = sourceElement.componentInstance.value.schema?.type;
        }

        return resolveContext;
    }
    /**
     * 选择绑定实体后事件
     */
    function onSubmitEntitySelctor(resolveContext: any) {
        const { bindingContext, componentType } = resolveContext;
        componentResolveContext.bindingSourceContext = bindingContext;
        if (componentType) {
            componentResolveContext.componentType = componentType;
        }

        if (modalEditorRef?.modalRef?.value.close) {
            modalEditorRef?.modalRef?.value.close();
        }
    }
    /**
     * 取消绑定实体
     */
    function onCancelEntitySelector() {
        componentResolveContext.bindingSourceContext = undefined;
        if (modalEditorRef?.modalRef?.value.close) {
            modalEditorRef?.modalRef?.value.close();
        }
    }
    /**
     * 选择绑定实体窗口
     */
    function renderEntityComponent() {
        const { componentType, bindingTargetId } = componentResolveContext;
        let steps: string[] = [];
        // 从控件树拖拽：已确定控件类型，需要选择实体以及实体中的字段。
        if (componentType) {
            steps = ['selectEntity', 'selectFields'];
        }
        // 从实体树拖拽：已确定绑定实体，需要选择控件类型以及实体中的字段。
        if (bindingTargetId) {
            steps = ['selectComponentType', 'selectFields'];
        }
        return () => (<><EntityBindingSelectorComponent
            componentType={componentType}
            bindingEntityId={bindingTargetId}
            steps={steps}
            designerHostService={designerHostService}
            onSubmit={onSubmitEntitySelctor}
            onCancel={onCancelEntitySelector}> </EntityBindingSelectorComponent> </>);
    }
    /**
     * 弹出实体绑定窗口
     */
    function triggerBindingEntity() {
        return new Promise((resolve, reject) => {
            modalEditorRef = designerHostService.modalService.open({
                title: '选择绑定',
                width: 800,
                height: 600,
                fitContent: false,
                showButtons: false,
                render: renderEntityComponent(),
                rejectCallback: () => {
                    componentResolveContext.bindingSourceContext = undefined;
                },
                closedCallback: () => {
                    resolve(componentResolveContext);
                },
                draggable: true
            });
        });
    }
    /**
     * 取消绑定字段后事件
     */
    function onCancelFieldSelector() {
        componentResolveContext.bindingSourceContext = undefined;
        if (modalEditorRef?.modalRef?.value.close) {
            modalEditorRef?.modalRef?.value.close();
        }
    }
    /**
     * 获取控件拖拽后的分组信息，以便于后续记录到视图模型
     */
    function getFieldGroupInfo(parentComponentInstance: DesignerComponentInstance) {
        let groupId = '';
        let groupName = '';
        if (DgControl['field-set'] && parentComponentInstance.schema.type === DgControl['field-set'].type) {
            groupId = parentComponentInstance.schema.id;
            groupName = parentComponentInstance.schema.title;
        }
        return { groupId, groupName };
    }
    /**
     * 选择绑定字段后事件
     */
    function onSubmitFieldSelctor(data: { selectedData: any, bindingType: any }) {
        if (!data || !data.selectedData || !data.bindingType) {
            return;
        }
        const { selectedData, bindingType } = data;
        // 若添加到小分组内，需要向vm保存groupId和groupName
        const { groupId, groupName } = getFieldGroupInfo(componentResolveContext.parentComponentInstance);
        const bindingSourceContext: ComponentBindingSourceContext = { bindingType: 'field' };

        if (bindingType === 'Form') {
            // 绑定字段
            const entityField = selectedData as DesignViewModelField;
            bindingSourceContext.entityFieldNode = entityField;

            bindingSourceContext.designViewModelField = merge({}, entityField, { groupId, groupName });

            componentResolveContext.bindingSourceContext = bindingSourceContext;
        } else {
            // 绑定变量
            const varibleField = selectedData as FormVariable;
            bindingSourceContext.variableFieldNode = merge({}, varibleField, { groupId, groupName });
            componentResolveContext.bindingSourceContext = bindingSourceContext;
        }

        if (modalEditorRef?.modalRef?.value.close) {
            modalEditorRef?.modalRef?.value.close();
        }
    }
    /**
     * 绑定字段弹窗
     */
    function renderFieldComponent() {
        const { parentComponentInstance } = componentResolveContext;
        const viewModelId = designerHostService.formSchemaUtils.getViewModelIdByComponentId(parentComponentInstance?.belongedComponentId);
        const editorParams = {
            viewModelId,
            designerHostService,
            disableOccupiedFields: true,
            componentSchema: { editor: { type: componentResolveContext.componentType } }
        };
        const bindingSettings = { enable: false };
        return () => (<><BindingSelectorComponent editorParams={editorParams} showCustomFooter={true} bindingType={bindingSettings} onSubmit={onSubmitFieldSelctor} onCancel={onCancelFieldSelector}  > </BindingSelectorComponent> </>);
    }
    /**
     * 工具箱拖拽控件：弹出绑定字段的窗口
     */
    function getBindingSourceContextByControlBox() {
        return new Promise((resolve, reject) => {
            modalEditorRef = designerHostService.modalService.open({
                title: '选择绑定',
                width: 800,
                height: 600,
                fitContent: false,
                showButtons: false,
                render: renderFieldComponent(),
                rejectCallback: () => {
                    componentResolveContext.bindingSourceContext = undefined;
                },
                closedCallback: () => {
                    resolve(componentResolveContext);
                },
                draggable: true
            });
        });
    }
    /**
     * 收集绑定同一个实体的同类型组件中已使用的字段（form类、dataGrid类...）
     */
    function resolveOccupiedFields() {
        const { formSchemaUtils } = designerHostService;
        const targetComponentId = componentResolveContext.parentComponentInstance.belongedComponentId;
        const occupiedFieldSet = new Set<string>();
        const formSchema = formSchemaUtils.getFormSchema().module;
        const targetComponentNode = formSchema.components.find(component => component.id === targetComponentId);
        const targetViewModelNode = formSchemaUtils.getViewModelById(targetComponentNode.viewModel);


        // 当前组件的类型
        let targetComponentType = targetComponentNode.componentType;

        // 根组件和table组件内的输入控件与form内不能重复
        if (targetComponentType === 'frame' || targetComponentType === 'table') {
            targetComponentType = 'form';
        }
        formSchema.viewmodels.forEach(viewModel => {
            if (!viewModel.fields || viewModel.fields.length === 0) {
                return;
            }
            const componentNode = formSchema.components.find(component => component.viewModel === viewModel.id);;
            let { componentType } = componentNode;
            if (componentType === 'frame' || componentType === 'table') {
                componentType = 'form';
            }
            // 同类型的组件并且绑定同一个实体（form类、dataGrid类...）
            if (componentType !== targetComponentType || viewModel.bindTo !== targetViewModelNode.bindTo) {
                return;
            }

            viewModel.fields.forEach(field => {
                occupiedFieldSet.add(field.id);
            });
        });
        return occupiedFieldSet;
    }

    /**
     * 实体树拖拽字段：收集字段信息
     */
    function getBindingSourceContextBySchemaTree() {
        const { bindingTargetId, parentComponentInstance } = componentResolveContext;
        if (!bindingTargetId) {
            componentResolveContext.bindingSourceContext = undefined;
            return;
        }
        const occupiedFieldsMap = resolveOccupiedFields();
        if (occupiedFieldsMap.has(bindingTargetId)) {
            const notifyService: any = new FNotifyService();
            notifyService.globalConfig = { position: 'top-center' };
            notifyService.warning('表单已经包含此字段，请勿重复添加');
            componentResolveContext.bindingSourceContext = undefined;
            return;
        } else {
            const bindingSourceContext: ComponentBindingSourceContext = { bindingType: 'field' };
            const targetComponentNode = designerHostService.formSchemaUtils.getComponentById(parentComponentInstance.belongedComponentId);
            const resolveResult = designerHostService.schemaService.getFieldByIDAndVMID(bindingTargetId, targetComponentNode.viewModel);
            const schemaField = merge({}, resolveResult.schemaField);

            bindingSourceContext.entityFieldNode = schemaField;

            // 若添加到分组内，需要保存向vm保存groupId和groupName
            const { groupId, groupName } = getFieldGroupInfo(componentResolveContext.parentComponentInstance);
            bindingSourceContext.designViewModelField = merge({}, schemaField, { groupId, groupName });

            componentResolveContext.bindingSourceContext = bindingSourceContext;
        }
    }
    /**
     * 字段绑定
     */
    function triggerBindingField() {
        if (componentResolveContext.bindingTargetId) {
            // 包含字段标识，说明是实体树拖拽字段
            return getBindingSourceContextBySchemaTree();
        } else {
            // 工具箱拖拽控件
            return getBindingSourceContextByControlBox();
        }

    }
    /**
      * 生成控件schema结构
      */
    function resolveComponentSchema() {
        const { parentComponentInstance } = componentResolveContext;
        const componentSchema = parentComponentInstance.addNewChildComponentSchema(componentResolveContext, designerHostService);
        componentResolveContext.componentSchema = componentSchema;
    }
    /**
     * 解析拖拽元素，并根据场景展示不同的绑定窗口
     */
    async function resolveBindingSource() {
        const { componentCategory } = componentResolveContext;

        switch (componentCategory) {
            case 'input': {
                await triggerBindingField();
                break;
            }
            case 'dataCollection': {
                await triggerBindingEntity();
                break;
            }

        }
    }
    /**
     * 根据拖拽元素解析并创建控件
     */
    async function resolveComponentCreationContextByDrop(sourceElement: DesignerHTMLElement, sourceContainer: DesignerHTMLElement, targetContainer: DesignerHTMLElement): Promise<DraggingResolveContext | null> {
        componentResolveContext = getComponentResolveContext(sourceElement, sourceContainer, targetContainer);

        await resolveBindingSource();

        // 若返回 undefined 代表终止后续生成
        if (componentResolveContext.bindingSourceContext === undefined) {
            return null;
        } else {
            resolveComponentSchema();
            return componentResolveContext;
        }

    }

    return {
        getComponentResolveContext,
        resolveComponentCreationContextByDrop
    };
}
