import { SetupContext } from 'vue';
import { DesignerOutlineProps } from '../../../../designer-outline/src/designer-outline.props';

/** 创建设计器 */
export function createDesigner(props: DesignerOutlineProps, context: SetupContext) {

    function createDesignBuilder(domJson: any) {
        // 画布渲染过程中，可能会在控件上补充属性，这种变更用户不感知，所以不需要通知IDE框架
        // window.suspendChangesOnForm = true;

        const elements = document.getElementsByClassName('editorPanel');
        let rootElement;
        if (elements && elements.length) {
            rootElement = elements[0];
        }

        // if (!DesignerBuilder) {
        //     return;
        // }

        // addServiceToDevkit();
        // const designerHost = addServiceToUI();

        const childComponents = domJson.module.components.slice(1);
        const rootComponent = domJson.module.components[0];
        const designerSchema = {
            contents: [rootComponent]
        };
        // designBuilder = new DesignerBuilder(rootElement, designerSchema, childComponents, AllComponents, designerHost);
        // designBuilder.instance.ready.then(() => {
        //     onBuilderReady();

        // 加载控件工具箱
        // controlBox.getDisplayedControls();

        // 设计器渲染完毕，开始监听DOM变更
        // window.suspendChangesOnForm = false;
        // });

        // designBuilder.instance.on('change', () => {
        //     onComponentChanged();
        // });
        // designBuilder.instance.on('removeComponent', () => {
        //     updatePropertyPanel(null);
        //     onComponentChanged();
        // });
    }

    return {
    };

};
