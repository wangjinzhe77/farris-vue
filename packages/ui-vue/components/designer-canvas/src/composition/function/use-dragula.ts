import dragula from '@farris/designer-dragula';
import { DesignerHostService, DesignerHTMLElement, DraggingResolveContext, UseDragula } from '../types';
import { findIndex } from 'lodash-es';
import { ref } from 'vue';
import { canvasChanged } from '../designer-canvas-changed';
import { ComponentSchema } from '../../types';
import { dragResolveService } from './drag-resolve';

export function useDragula(designerHostService: DesignerHostService): UseDragula {

    let dragulaInstance: any;

    /**
     * 判断是否可以接收拖拽的新控件
     * @param el 拖拽的新控件元素
     * @param target  目标位置
     * @returns boolean
     */
    function checkCanAcceptDrops(
        element: DesignerHTMLElement,
        target: DesignerHTMLElement,
        sourceContainer: DesignerHTMLElement
    ): boolean {
        if (!!element.contains(target) || target.classList.contains('no-drop')) {
            return false;
        }
        const result = true;
        if (element.componentInstance && element.componentInstance.value.getDragScopeElement) {
            const dragScopEle = element.componentInstance.value.getDragScopeElement();
            if (dragScopEle) {
                if (!dragScopEle.contains(target)) {
                    return false;
                }
            }
        }
        if (target.componentInstance && target.componentInstance.value.canAccepts) {
            const dragResolveUtil = dragResolveService(designerHostService);
            const draggingContext = dragResolveUtil.getComponentResolveContext(element, sourceContainer, target);

            return target.componentInstance.value.canAccepts(draggingContext);
        }
        return result;
    }

    /**
     * 判断DOM 是否在可视区域内
     * @param el 元素
     * @param containerEl 容器
     */
    function isElementInViewport(element: HTMLElement, sourceContainer: HTMLElement) {
        const container = sourceContainer.getBoundingClientRect();
        const box = element.getBoundingClientRect();
        const top = box.top >= container.top;
        const bottom = box.top < container.bottom;
        return (top && bottom);
    }

    /**
     * 拖拽过程若中产生了页面的上下滚动，需要将已选控件的操作按钮上下移动相等的距离。
     * @param formElement 滚动父容器
     * @param scrollDirection 滚动方向
     * @param scrollHeight 滚动距离
     */
    function scrollInDragging(formElement: HTMLElement, scrollHeight: number) {
        const selectedDom = formElement.querySelector('.dgComponentSelected') as HTMLElement;
        if (!selectedDom || scrollHeight === 0) {
            return;
        }
        if (isElementInViewport(selectedDom, formElement)) {
            const toolbar = selectedDom.querySelector('.component-btn-group');
            if (toolbar) {
                const divPanel = toolbar.querySelector('div');
                if (divPanel && divPanel.style.top) {
                    const top = Number.parseFloat(divPanel.style.top);
                    divPanel.style.top = (top - scrollHeight) + 'px';
                }
            }
        }
    }

    /**
     * 将新控件json添加到新容器schema json中
     * @param target 目标容器元素
     * @param sourceControlSchema 新控件的JSON schema结构
     * @param sibling 目标位置的下一个同级元素
     */
    function addNewControlToTarget(
        target: DesignerHTMLElement,
        sourceControlSchema: ComponentSchema | null,
        sibling: DesignerHTMLElement
    ): number {
        let index = -1;
        if (!sourceControlSchema) {
            return -1;
        }
        if (target.componentInstance.value.contents) {
            if (sibling && sibling.componentInstance) {
                if (!sibling.getAttribute('data-noattach')) {
                    // 定位目标位置
                    const siblingComponentSchema = sibling.componentInstance.value.schema;
                    let locatePredicate: any = { id: siblingComponentSchema.id };
                    if (siblingComponentSchema.type === 'component') {
                        locatePredicate = { component: siblingComponentSchema.id };
                    }

                    index = findIndex(target.componentInstance.value.contents, locatePredicate);
                    index = (index === -1) ? 0 : index;
                } else {
                    index = Number(sibling.getAttribute('data-position'));
                }
                if (index !== -1) {
                    target.componentInstance.value.contents.splice(index, 0, sourceControlSchema);
                }
            } else {
                target.componentInstance.value.contents.push(sourceControlSchema);
            }
        }
        return index;
    }

    /**
     * 从控件工具箱中拖拽新建控件
     * @param element 拖拽的元素
     * @param target 目标容器元素
     * @param source 原容器元素
     * @param sibling 目标位置的下一个同级元素
     */
    function createControlFromOutside(
        element: DesignerHTMLElement,
        target: DesignerHTMLElement,
        source: DesignerHTMLElement,
        sibling: DesignerHTMLElement
    ) {
        const dragResolveUtil = dragResolveService(designerHostService);
        dragResolveUtil.resolveComponentCreationContextByDrop(element, source, target).then(componentResolveContext => {
            if (!componentResolveContext) {
                return;
            }
            const sourceControlSchema = componentResolveContext.componentSchema;
            if (sourceControlSchema) {
                addNewControlToTarget(target, sourceControlSchema, sibling);
            }
        });
        // 移除拷贝生成的源DOM
        if (target.contains(element)) {
            target.removeChild(element);
        }
    }
    /**
     * 在现有的表单中拖拽移动控件位置
     * @param element 拖拽的元素
     * @param target 目标容器元素
     * @param source 源容器元素
     * @param sibling 目标位置的下一个同级元素
     */
    function dragBetweenCurrentForm(
        element: DesignerHTMLElement,
        target: DesignerHTMLElement,
        source: DesignerHTMLElement,
        sibling: DesignerHTMLElement
    ) {
        let sourceControlSchema;
        let index = -1;
        // Form、DataGrid等控件在拖拽时，需要连同所属Component一起拖拽。
        if (element.componentInstance && element.componentInstance.value.triggerBelongedComponentToMoveWhenMoved) {
            const cmpInstance = element.componentInstance.value.getBelongedComponentInstance(element.componentInstance);
            if (cmpInstance) {
                // 将拖拽元素替换为所属Component
                element = ref(cmpInstance.elementRef).value.parentElement as DesignerHTMLElement;
                // 将源容器元素替换为所属Component的父级元素
                source = element.parentElement as DesignerHTMLElement;
            }

        }
        const elementComponentSchema = element.componentInstance && element.componentInstance.value.schema;

        let locatePredicate: any = { id: elementComponentSchema && elementComponentSchema.id };
        if (elementComponentSchema.type === 'component') {
            locatePredicate = { component: elementComponentSchema && elementComponentSchema.id };
        }

        index = findIndex(source.componentInstance.value.contents, locatePredicate);

        // 从源容器schema json中移除
        if (index !== -1 && source.componentInstance.value.contents) {
            sourceControlSchema = source.componentInstance.value.contents.splice(index, 1);
            sourceControlSchema = sourceControlSchema[0];
        }

        addNewControlToTarget(target, sourceControlSchema as ComponentSchema, sibling);

        // 源容器的控件被移除掉
        if (source.componentInstance && source.componentInstance.value.onChildElementMovedOut) {
            source.componentInstance.value.onChildElementMovedOut(element);
        }

        // 目标容器接收新控件
        if (target.componentInstance && target.componentInstance.value.onAcceptMovedChildElement) {
            target.componentInstance.value.onAcceptMovedChildElement(element, source);
        }
    }

    /**
     * 拖拽结束
     * @param element 拖拽的元素
     * @param target 目标容器元素
     * @param source 原容器元素
     * @param sibling 目标位置的下一个同级元素
     */
    function onDrop(element: DesignerHTMLElement, target: DesignerHTMLElement, source: DesignerHTMLElement, sibling: DesignerHTMLElement) {
        if (!target) {
            return;
        }
        if (element.contains(target)) {
            return;
        }
        const sourceType = element.getAttribute('data-sourceType');

        switch (sourceType) {
            case 'control': case 'field': case 'entity': {
                createControlFromOutside(element, target, source, sibling);
                break;
            }
            default: {
                if (source.componentInstance.value.contents) {
                    dragBetweenCurrentForm(element, target, source, sibling);
                } else {
                    // 移除拷贝生成的源DOM
                    if (target.contains(element)) {
                        target.removeChild(element);
                    }
                }
            }
        }
        canvasChanged.value++;

    }

    function initializeDragula(containerElement: DesignerHTMLElement) {
        if (dragulaInstance) {
            dragulaInstance.destroy();
        }

        if (!dragula) {
            return;
        }

        dragulaInstance = dragula([], {
            // 镜像容器
            mirrorContainer: containerElement,
            direction: 'mixed',
            revertOnSpill: true,
            // 判断是否可移动
            moves(element: DesignerHTMLElement, container: DesignerHTMLElement, handle: DesignerHTMLElement): boolean {
                let moves = true;

                // 包含no-drag样式的元素不允许拖动
                if (element.classList.contains('no-drag')) {
                    moves = false;
                }
                // 为防止误操作，可视化区域的控件只能通过移动图标来拖拽
                if (element.componentInstance) {
                    moves = !!handle.getAttribute('data-dragging-icon');
                }
                return moves;
            },
            // 判断是否可拷贝
            copy(element: HTMLElement): boolean {
                // 工具箱里的div需要配置drag-copy
                return element.classList.contains('drag-copy');
            },
            // 获取镜像元素的文本内容
            getMirrorText(element: DesignerHTMLElement): string {
                if (element.componentInstance && element.componentInstance.value.getDraggingDisplayText) {
                    return element.componentInstance.value.getDraggingDisplayText();
                }
                return element.innerText || '控件';
            },
            // 判断目标区域是否可接收拖拽的控件
            accepts(element: DesignerHTMLElement, target: DesignerHTMLElement, source: DesignerHTMLElement): boolean {
                const canAccept = checkCanAcceptDrops(element, target, source);
                const guMirrotElement = containerElement.lastElementChild as Element;
                if (canAccept) {
                    guMirrotElement.className = guMirrotElement.className.replace('undroppable', '');
                } else if (!guMirrotElement.className.includes('undroppable')) {
                    guMirrotElement.className += ' undroppable';
                }
                return canAccept;
            }
        }).on('over', (el: DesignerHTMLElement, container: DesignerHTMLElement) => {
            container.className += ' drag-over';
        }).on('out', (el: DesignerHTMLElement, container: DesignerHTMLElement) => {
            container.className = container.className.replace('drag-over', '').replace('  ', '');
        }).on('drop', (
            element: DesignerHTMLElement, target: DesignerHTMLElement, source: DesignerHTMLElement, sibling: DesignerHTMLElement
        ) => {
            onDrop(element, target, source, sibling);
        }).on('dragend', (element: HTMLElement, scrollHeight: number) => {
            scrollInDragging(element, scrollHeight);
        });
    }

    /**
     * 子组件JSON结构和当前组件的实例添加到DOM中并注册拖拽容器。节点class= 'builder-components...'
     * @param element dom元素
     * @param childrenComponents 容器内的子组件实例集合
     * @param childrenContents 子组件JSON schema集合
     * @param component 容器组件实例
     * @returns 容器类组件的子组件集合
     */
    function attachComponents(element: HTMLElement, component: Record<string, any>) {

        // don't attach if no element was found or component doesn't participate in drag'n'drop.
        if (!element) {
            return;
        }
        if (component.noDragDrop) {
            return element;
        }
        // 获取容器中的子组件集合节点
        const containerElement: HTMLElement = element.querySelector(`[data-dragref='${component.id}-container']`) || element;

        // 将容器添加到拖拽列表中，dragula控件会监听容器中元素的拖动事件
        if (dragulaInstance && containerElement) {
            // containerElement 为页面中的容器节点的builder-components层级
            dragulaInstance.containers.push(containerElement);
        }
    }


    function getDragulaInstance() {
        return dragulaInstance;
    }
    return { attachComponents, initializeDragula, getDragulaInstance };

}
