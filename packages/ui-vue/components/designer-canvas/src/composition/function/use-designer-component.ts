import { Ref, ref } from "vue";
import { DesignerHostService, DesignerHTMLElement, DraggingResolveContext, UseDesignerRules } from "../types";
import { ComponentSchema, DesignerComponentInstance, DesignerItemContext } from "../../types";
import { getSchemaByType } from '../../../../dynamic-resolver/src/schema-resolver';

export function useDesignerComponent(
    elementRef: Ref<HTMLElement>,
    designItemContext?: DesignerItemContext,
    designerRules?: UseDesignerRules
): Ref<DesignerComponentInstance> {

    const styles = (designerRules && designerRules.getStyles && designerRules.getStyles()) || '';
    const designerClass = (designerRules && designerRules.getDesignerClass && designerRules.getDesignerClass()) || '';
    const componentInstance = ref<DesignerComponentInstance>();
    /**
     * 校验组件是否支持移动
     */
    function checkCanMoveComponent(): boolean {
        if (designItemContext?.schema.componentType === 'frame') {
            return false;
        }
        if (designerRules && designerRules.checkCanMoveComponent) {
            return designerRules.checkCanMoveComponent();
        }
        return true;
    }

    /**
     * 校验组件是否支持选中父级
     */
    function checkCanSelectParentComponent(): boolean {
        return false;
    }

    /**
     * 校验组件是否支持删除
     */
    function checkCanDeleteComponent() {
        if (designItemContext?.schema.componentType === 'frame') {
            return false;
        }
        if (designerRules && designerRules.checkCanDeleteComponent) {
            return designerRules.checkCanDeleteComponent();
        }
        return true;
    }

    /**
     * 判断在可视化区域中是否隐藏容器间距和线条
     */
    function hideNestedPaddingInDesginerView() {
        if (designItemContext?.schema.componentType === 'frame') {
            return true;
        }
        if (designerRules && designerRules.hideNestedPaddingInDesginerView) {
            return designerRules.hideNestedPaddingInDesginerView();
        }
        return false;
    }

    /**
     * 获取组件在表单DOM中所属的Component的实例
     * @param componentInstance 组件实例
     */
    function getBelongedComponentInstance(currentComponentInstance?: Ref<DesignerComponentInstance>): DesignerComponentInstance | null {
        if (!currentComponentInstance || !currentComponentInstance.value) {
            return null;
        }
        if (currentComponentInstance.value.schema && currentComponentInstance.value.schema.type === 'component') {
            return currentComponentInstance.value;
        }
        const parent = ref(currentComponentInstance?.value.parent) as Ref<DesignerComponentInstance>;
        const grandParent = getBelongedComponentInstance(parent);
        if (grandParent) {
            return grandParent;
        }
        return null;
    }

    function getDraggableDesignItemElement(context: DesignerItemContext = designItemContext as DesignerItemContext): Ref<any> | null {
        const { componentInstance, designerItemElementRef } = context;
        if (!componentInstance || !componentInstance.value) {
            return null;
        }
        const { getCustomButtons } = componentInstance.value;
        if (componentInstance.value.canMove || (getCustomButtons && getCustomButtons()?.length)) {
            return designerItemElementRef;
        }
        return getDraggableDesignItemElement(context.parent);
    }

    /**
     * 判断是否可以接收拖拽新增的子级控件
     * @param data 新控件的类型、所属分类
     * @returns boolean
     */
    function canAccepts(draggingContext: DraggingResolveContext) {
        return !!designerRules && designerRules.canAccepts(draggingContext);
    }

    function getDraggingDisplayText() {
        return designItemContext?.schema.label || designItemContext?.schema.title || designItemContext?.schema.name;
    }

    /**
     * 控件可以拖拽到的最外层容器，用于限制控件向外层容器拖拽的范围。不写则不限制
     */
    function getDragScopeElement(): HTMLElement | undefined {
        return undefined;
    }

    /**
     * 移动控件后事件：在可视化设计器中，将现有的控件移动到容器中
     * @param element 移动的源DOM结构
     */
    function onAcceptMovedChildElement(element: DesignerHTMLElement, sourceContainer?: DesignerHTMLElement) {
        if (!element || !sourceContainer) {
            return;
        }
        if (designerRules?.onAcceptMovedChildElement) {
            designerRules.onAcceptMovedChildElement(element, sourceContainer);
        }
    }
    /**
     * 当前容器接收新创建的子控件，返回子控件schema结构
     */
    function addNewChildComponentSchema(resolveContext: DraggingResolveContext, designerHostService: DesignerHostService) {
        const { componentType } = resolveContext;
        let componentSchema = getSchemaByType(componentType, resolveContext, designerHostService) as ComponentSchema;
        if (designerRules && designerRules.onResolveNewComponentSchema) {
            componentSchema = designerRules.onResolveNewComponentSchema(resolveContext, componentSchema);
        }

        const typePrefix = componentType.toLowerCase().replace(/-/g, '_');
        if (componentSchema && !componentSchema.id && componentSchema.type === componentType) {
            componentSchema.id = `${typePrefix}_${Math.random().toString().slice(2, 6)}`;
        }
        return componentSchema;
    }

    /**
     * 移动内部控件后事件：在可视化设计器中，将现有的控件移动到容器中
     * @param element 移动的源DOM结构
     */
    function onChildElementMovedOut(element: HTMLElement) {

    }

    /** 
     * 获取控件属性配置
     */
    function getPropConfig(...args) {
        if (designerRules && designerRules.getPropsConfig) {
            return designerRules.getPropsConfig(...args);
        }
    }
    /**
     * 控件删除后事件
     */
    function onRemoveComponent() {
        // 调用当前控件的删除后事件
        if (designerRules && designerRules.onRemoveComponent) {
            designerRules.onRemoveComponent();
        }
        // 递归触发子级控件的删除后事件
        if (designItemContext?.schema.contents) {
            designItemContext.schema.contents.map(content => {
                let contentSchemaId = content.id;
                if (content.type === 'component-ref') {
                    contentSchemaId = content.component;
                }
                const contentElements: any = elementRef.value.querySelectorAll(`#${contentSchemaId}-design-item`);
                if (contentElements?.length) {
                    Array.from(contentElements).map((contentElement: any) => {
                        if (contentElement?.componentInstance?.value.onRemoveComponent) {
                            contentElement.componentInstance.value.onRemoveComponent();
                        }
                    });
                }

            });
        }
    }
    /**
     * 校验组件是否支持删除
     */
    function getCustomButtons() {
        if (designerRules && designerRules.getCustomButtons) {
            return designerRules.getCustomButtons();
        }

    }

    /**
     * 控件属性变更后事件
     */
    function onPropertyChanged(event: any) {
        if (designerRules && designerRules.onPropertyChanged) {
            return designerRules.onPropertyChanged(event);
        }
    }
    componentInstance.value = {
        canMove: checkCanMoveComponent(),
        canSelectParent: checkCanSelectParentComponent(),
        canDelete: checkCanDeleteComponent(),
        canNested: !hideNestedPaddingInDesginerView(),
        contents: designItemContext?.schema.contents,
        elementRef,
        parent: designItemContext?.parent?.componentInstance,
        schema: designItemContext?.schema,
        styles,
        designerClass,
        canAccepts,
        getBelongedComponentInstance,
        getDraggableDesignItemElement,
        getDraggingDisplayText,
        getPropConfig,
        getDragScopeElement,
        onAcceptMovedChildElement,
        onChildElementMovedOut,
        addNewChildComponentSchema,
        triggerBelongedComponentToMoveWhenMoved: !!designerRules && designerRules.triggerBelongedComponentToMoveWhenMoved || ref(false),
        triggerBelongedComponentToDeleteWhenDeleted: !!designerRules && designerRules.triggerBelongedComponentToDeleteWhenDeleted || ref(false),
        onRemoveComponent,
        getCustomButtons,
        onPropertyChanged
    } as DesignerComponentInstance;

    return componentInstance as any;

}
