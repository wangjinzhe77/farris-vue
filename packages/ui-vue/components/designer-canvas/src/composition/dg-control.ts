export const DgControl = {
    'button': { type: 'button', name: '按钮', icon: 'Button' },

    'response-toolbar': { type: 'response-toolbar', name: '工具栏', icon: 'ButtonGroup' },

    'response-toolbar-item': { type: 'response-toolbar-item', name: '按钮', icon: 'Button' },

    'content-container': { type: 'content-container', name: '容器', icon: 'ContentContainer' },

    'input-group': { type: 'input-group', name: '文本', icon: 'TextBox' },

    'textarea': { type: 'textarea', name: '多行文本', icon: 'MultiTextBox' },

    'lookup': { type: 'lookup', name: '帮助', icon: 'LookupEdit' },

    'number-spinner': { type: 'number-spinner', name: '数值', icon: 'NumericBox' },

    'date-picker': { type: 'date-picker', name: '日期', icon: 'DateBox' },

    'switch': { type: 'switch', name: '开关', icon: 'SwitchField' },

    'radio-group': { type: 'radio-group', name: '单选组', icon: 'RadioGroup' },

    'check-box': { type: 'check-box', name: '复选框', icon: 'CheckBox' },

    'check-group': { type: 'check-group', name: '复选框组', icon: 'CheckGroup' },

    'combo-list': { type: 'combo-list', name: '下拉列表', icon: 'EnumField' },

    'response-form': { type: 'response-form', name: '卡片面板', icon: 'Form' },

    'response-layout': { type: 'response-layout', name: '布局容器', icon: 'ResponseLayout3' },

    'response-layout-item': { type: 'response-layout-item', name: '布局', icon: 'ResponseLayout1' },

    'tree-grid': { type: 'tree-grid', name: '树表格', icon: 'TreeGrid' },

    'tree-grid-column': { type: 'tree-grid-column', name: '树表格列' },

    'data-grid': { type: 'data-grid', name: '表格', icon: 'DataGrid' },

    'data-grid-column': { type: 'data-grid-column', name: '表格列' },

    'module': { type: 'Module', name: '模块', icon: 'Module' },

    'component': { type: 'component', name: '组件', icon: 'Component' },

    'tabs': { type: 'tabs', name: '标签页', icon: 'Tab' },

    'tab-page': { type: 'tab-page', name: '标签页项', dependentParentControl: 'Tab' },

    'tab-toolbar-item': { type: 'tab-toolbar-item', name: '标签页工具栏按钮', icon: 'Button' },

    'time-picker': { type: 'time-picker', name: '时间选择', icon: 'TimePicker' },

    'section': { type: 'section', name: '分组面板', icon: 'Section' },

    'section-toolbar': { type: 'section-toolbar', name: '分组面板工具栏' },

    'section-toolbar-item': { type: 'section-toolbar-item', name: '分组面板按钮' },

    'splitter': { type: 'splitter', name: '分栏面板', icon: 'Splitter' },

    'splitter-pane': { type: 'splitter-pane', name: '分栏面板项', dependentParentControl: 'Splitter' },

    'component-ref': { type: 'component-ref', name: '组件引用节点' },

    'uploader': { type: 'uploader', name: '附件上传', icon: 'FileUpload' },

    'page-header': { type: 'page-header', name: '页头', icon: 'Header' },

    'page-footer': { type: 'page-footer', name: '页脚', icon: 'ModalFooter' },

    'tab-toolbar': { type: 'tab-toolbar', name: '标签页工具栏', icon: 'TabToolbar' },

    'fieldset': { type: 'fieldset', name: '分组', icon: 'fieldset' },

    'query-solution': { type: 'query-solution', name: '筛选方案', icon: 'QueryScheme'}
};
