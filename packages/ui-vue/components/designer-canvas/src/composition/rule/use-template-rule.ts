import { DesignerItemContext } from "../../types";
import { DesignerHostService } from "../types";

export interface DragDropRule {
    canAccept: boolean;
    canMove: boolean;
    canDelete: boolean;
    [index: string]: boolean;
}

/**
 * 解析模板拖拽控制规则
 */
export class UseTemplateDragAndDropRules {
    public getTemplateRule(designItemContext: DesignerItemContext, designerHostService?: DesignerHostService): DragDropRule {

        const formSchemaUtils = designerHostService?.formSchemaUtils;
        const dragTemplateRules = formSchemaUtils?.getFormTemplateRule();
        const dragDropRule = { canAccept: true, canDelete: true, canMove: true };
        if (!dragTemplateRules) {
            return dragDropRule;
        }
        const componentContext = this.getComponentContext(designItemContext);
        const { componentClassList } = componentContext;

        componentClassList.forEach(componentClass => {
            if (!componentClass || !dragTemplateRules[componentClass]) {
                return;
            }

            const { canMove: moveRule, canDelete: deleteRule, canAccept: acceptRule } = dragTemplateRules[componentClass];
            dragDropRule.canMove = dragDropRule.canMove && this.resolveRuleValue(moveRule, componentContext);
            dragDropRule.canDelete = dragDropRule.canDelete && this.resolveRuleValue(deleteRule, componentContext);
            dragDropRule.canAccept = dragDropRule.canAccept && this.resolveRuleValue(acceptRule, componentContext);
        });

        return dragDropRule;
    }

    private resolveRuleValue(ruleSchema: any, componentContext: any): boolean {

        if (typeof ruleSchema === 'boolean') {
            return ruleSchema;
        } else {
            return this.parseRuleValueSchema(ruleSchema, componentContext);
        }
    }

    private parseRuleValueSchema(ruleSchema: any, componentContext: any) {
        const invalidContext = ruleSchema.invalidContext || [];
        let isMatched = true;
        for (const ruleContext of invalidContext) {

            // 判断子级节点是否匹配
            if (ruleContext.firstLevelChild) {
                if (ruleContext.firstLevelChild.class) {
                    const { firstLevelChildClassList } = componentContext;
                    if (firstLevelChildClassList && !firstLevelChildClassList.includes(ruleContext.firstLevelChild.class)) {
                        isMatched = false;
                        continue;
                    }
                }
                if (ruleContext.firstLevelChild.type) {
                    const { firstLevelChildSchema } = componentContext;
                    if (!firstLevelChildSchema || firstLevelChildSchema.type !== ruleContext.firstLevelChild.type) {
                        isMatched = false;
                        continue;
                    }
                }

            }
            // 判断孙子节点是否匹配
            if (ruleContext.secondLevelChild) {
                if (ruleContext.secondLevelChild.class) {
                    const { secondLevelChildClassList } = componentContext;
                    if (secondLevelChildClassList && !secondLevelChildClassList.includes(ruleContext.secondLevelChild.class)) {
                        isMatched = false;
                        continue;
                    }
                }
                if (ruleContext.secondLevelChild.type) {
                    const { secondLevelChildSchema } = componentContext;
                    if (!secondLevelChildSchema || secondLevelChildSchema.type !== ruleContext.secondLevelChild.type) {
                        isMatched = false;
                        continue;
                    }
                }

            }
            // 判断父级节点是否匹配
            if (ruleContext.parent) {
                if (ruleContext.parent.class) {
                    const { parentClassList } = componentContext;
                    if (parentClassList && !parentClassList.includes(ruleContext.parent.class)) {
                        isMatched = false;
                        continue;
                    }
                }
                if (ruleContext.parent.type) {
                    const { parentSchema } = componentContext;
                    if (parentSchema && parentSchema.type !== ruleContext.parent.type) {
                        isMatched = false;
                        continue;
                    }
                }
            }
            isMatched = true;
            break;
        }
        return !isMatched;

    }
    public getComponentContext(designItemContext: DesignerItemContext) {
        const component = designItemContext.schema;

        // 控件本身
        const componentClass = component.appearance && component.appearance.class || '';
        const componentClassList = componentClass.split(' ') || [];

        // 控件子级节点
        const childContents = component.contents || [];
        const firstLevelChildSchema = childContents.length ? childContents[0] : null;
        const firstLevelChildClass = firstLevelChildSchema && firstLevelChildSchema.appearance ? firstLevelChildSchema.appearance.class : '';
        const firstLevelChildClassList = firstLevelChildClass ? firstLevelChildClass.split(' ') : [];

        // 控件孙子级节点
        const secondLevelChildSchema = firstLevelChildSchema?.contents?.length ? firstLevelChildSchema?.contents[0] : null;
        const secondLevelChildClass = secondLevelChildSchema && secondLevelChildSchema.appearance ? secondLevelChildSchema.appearance.class : '';
        const secondLevelChildClassList = secondLevelChildClass ? secondLevelChildClass.split(' ') : [];

        // 控件父级节点
        const parentSchema = component.type === 'component' ? designItemContext.parent?.parent?.schema : designItemContext.parent?.schema;
        const parentClass = parentSchema && parentSchema.appearance && parentSchema.appearance.class || '';
        const parentClassList = parentClass ? parentClass.split(' ') : [];

        return {
            componentClass,
            componentClassList,
            childContents,
            firstLevelChildSchema,
            firstLevelChildClass,
            firstLevelChildClassList,
            secondLevelChildSchema,
            secondLevelChildClass,
            secondLevelChildClassList,
            parentSchema,
            parentClass,
            parentClassList
        };
    }
}
