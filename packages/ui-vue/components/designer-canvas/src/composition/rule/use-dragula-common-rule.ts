import { DgControl } from "../dg-control";
import { DesignerHostService, DraggingResolveContext } from "../types";

export function useDragulaCommonRule() {

    /**
     * 容器类控件的基础控制规则
     */
    function basalDragulaRuleForContainer(draggingContext: DraggingResolveContext, designerHostService?: DesignerHostService): boolean {
        if (!draggingContext) {
            return false;
        }

        /** 目标容器的组件实例 */
        const targetCmpInstance = draggingContext.targetContainer?.componentInstance &&
            draggingContext.targetContainer.componentInstance.value;

        if (!targetCmpInstance) {
            return false;
        }
        const targetContainerType = targetCmpInstance.schema.type;
        const belongedComponent = designerHostService?.formSchemaUtils.getComponentById(targetCmpInstance.belongedComponentId);

        // 限制输入类控件的可接收容器
        if (draggingContext.componentCategory === 'input' || draggingContext.componentType === 'form-group') {
            if (![DgControl['response-layout-item'].type, DgControl['response-form'].type].includes(targetContainerType)) {
                return false;
            }
        }

        // 限制标签页区域、分组面板的可接收容器
        if (draggingContext.componentType === DgControl.tabs.type || draggingContext.componentType === DgControl.section.type) {
            const belongedComponentType = belongedComponent?.componentType;
            if (belongedComponentType !== 'frame') {
                return false;
            }
            if (![DgControl['content-container'].type, DgControl['splitter-pane'].type, DgControl['response-layout-item'].type].includes(targetContainerType)) {
                return false;
            }
        }
        // 限制筛选方案
        if (draggingContext.componentType === DgControl['query-solution'].type) {
            return false;
        }
        return true;
    }

    return {
        basalDragulaRuleForContainer
    };
}
