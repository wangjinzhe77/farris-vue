import { DesignFormVariable, DesignViewModelField, FormSchemaEntity, FormSchemaEntityField } from "../../../common/entity/entity-schema";
import { Ref } from "vue";
import { ComponentSchema, DesignerComponentButton, DesignerComponentInstance } from "../types";

export interface DesignerHTMLElement extends HTMLElement {
    /** 记录各子元素对应的控件schema json的集合，用于container类dom节点 */
    contents?: ComponentSchema[];

    /** 记录element对应的component实例，用于单个component节点 */
    componentInstance: Ref<DesignerComponentInstance>;

    schema: ComponentSchema;
}

export interface UseDragula {
    attachComponents: (element: HTMLElement, component: ComponentSchema) => void;

    initializeDragula: (containerElement: DesignerHTMLElement) => void;

    getDragulaInstance: () => any;
}
export interface DesignerHostService {
    eventsEditorUtils: any;
    formSchemaUtils: any;
    formMetadataConverter: any;
    designViewModelUtils: any;
    controlCreatorUtils: any;
    metadataService?: any;
    formStateMachineUtils: any;
    schemaService?: any;
    [key: string]: any;
}
/**
 * 绑定上下文
 */
export interface ComponentBindingSourceContext {
    /** 控件绑定类型：字段|实体 */
    bindingType: 'field' | 'entity';
    /** 控件绑定的实体schema字段 */
    entityFieldNode?: FormSchemaEntityField;
    /** 控件绑定的实体schema字段若是关联带出字段，此属性记录关联字段所属的根字段 */
    entityRootFieldNode?: FormSchemaEntityField;
    /** 实体schema字段对应的DesignViewModel结构 */
    designViewModelField?: DesignViewModelField;
    /** 变量字段节点 */
    variableFieldNode?: DesignFormVariable;
    /** 要绑定的实体 */
    bindingEntity?: FormSchemaEntity;
    /** 要绑定的字段集合 */
    bindingEntityFields?: FormSchemaEntityField[];
    /** 实体在视图模型中的绑定信息 */
    bindTo?: string;
    /** 生成控件的标题 */
    entityTitle?: string;
}

/**
 * 拖拽上下文
 */
export interface DraggingResolveContext {

    /** 拖拽的源元素 */
    sourceElement: DesignerHTMLElement;
    /** 拖拽的源元素父容器 */
    sourceContainer: DesignerHTMLElement;
    /** 拖拽的目标容器 */
    targetContainer: DesignerHTMLElement;

    /** 拖拽的控件类型 */
    componentType: string;
    /** 拖拽的控件名称 */
    label: string;
    /** 拖拽目标区域的组件实例 */
    parentComponentInstance: DesignerComponentInstance;
    /** 拖拽位置在目标区域的索引值 */
    targetPosition?: number;
    /** 拖拽控件的类别 */
    componentCategory?: string;
    /** 拖拽来源:控件工具箱control / 实体树字段field / 实体树实体entity / 现有控件移动位置move */
    sourceType: string;
    /** 工具箱中的控件，启用的特性 */
    componentFeature?: string;
    /** 要添加的控件Schema */
    componentSchema?: any;

    /** 绑定信息 */
    bindingSourceContext?: ComponentBindingSourceContext | null;

    /** 绑定的目标字段或实体id */
    bindingTargetId?: string;

    // [propName: string]: any;
}

export interface UseDesignerRules {
    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    canAccepts(draggingContext: DraggingResolveContext): boolean;

    /**
     * 判断当前容器是否是固定的上下文的中间层级
     */
    checkIsInFixedContextRules?(): boolean;

    getStyles?(): string;

    getDesignerClass?(): string;
    /**
     * 容器接收新创建的子控件
     */
    onResolveNewComponentSchema?: (resolveContext: DraggingResolveContext, componentSchema: ComponentSchema) => ComponentSchema;

    /**
     * 移动控件后事件：在可视化设计器中，容器接收控件后调用此事件
     */
    onAcceptMovedChildElement?: (sourceElement: DesignerHTMLElement, sourceContainer: DesignerHTMLElement) => void;

    /**
     * 判断是否支持移动组件
     */
    checkCanMoveComponent?(): boolean;

    /**
     * 判断是否支持删除组件
     */
    checkCanDeleteComponent?(): boolean;

    /**
    * 判断是否支持增加组件
    */
    checkCanAddComponent?(): boolean;

    /**
     * 判断是否隐藏组件间距和线条
     */
    hideNestedPaddingInDesginerView?(): boolean;

    /** 接收控件属性信息 */
    getPropsConfig?(schema?: any, componentInstance?: any): any;

    /**
     * 组件在拖拽时是否需要将所属的Component一起拖拽，用于form、data-grid等控件的拖拽
     */
    triggerBelongedComponentToMoveWhenMoved?: Ref<boolean>;
    /**
     * 组件在被移除时是否需要将所属的Component一起移除，用于form、data-grid等控件的拖拽
     */
    triggerBelongedComponentToDeleteWhenDeleted?: Ref<boolean>;

    /** 组件删除后事件 */
    onRemoveComponent?(): void;

    /** 获取控件自定义操作按钮 */
    getCustomButtons?: () => DesignerComponentButton[];

    /** 控件属性变更后事件 */
    onPropertyChanged?: (event: any) => void;
}
