import { ExtractPropTypes } from 'vue';

export const designerCanvasProps = {
    /**
     * 组件值
     */
    modelValue: { type: Object, default: {} },
    componentId: { type: String, default: '' },
} as Record<string, any>;

export type DesignerCanvasPropsType = ExtractPropTypes<typeof designerCanvasProps>;
