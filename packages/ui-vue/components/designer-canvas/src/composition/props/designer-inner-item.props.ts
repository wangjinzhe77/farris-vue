import { ExtractPropTypes } from "vue";

export const designerInnerItemProps = {
    id: { type: String, default: '' },
    componentId: { type: String, default: '' },
    canAdd: { type: Boolean, default: false },
    canDelete: { type: Boolean, default: false },
    canMove: { type: Boolean, default: false },
    contentKey: { type: String, default: 'contents' },
    childLabel: { type: String, default: '' },
    childType: { type: String, default: '' },
    /**
     * 组件值
     */
    modelValue: { type: Object },
} as Record<string, any>;

export type DesignerInnerItemPropsType = ExtractPropTypes<typeof designerInnerItemProps>;
