import { defineComponent } from 'vue';
import { SmokeDetectorProps, smokeDetectorProps } from './smoke-detector.props';
import './smoke-detector.css';

export default defineComponent({
    name: 'FSmokeDetector',
    props: smokeDetectorProps,
    emits: [''],
    setup(props: SmokeDetectorProps) {

        return () => {
            return (
                <div class='f-smoke-detector'>
                    <img title="smoke-detector" src='./image/smoke-detector-2.png'></img>
                </div>
            );
        };
    }
});
