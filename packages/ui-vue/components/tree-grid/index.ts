 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import FTreeGrid from './src/tree-grid.component';
import FTreeGridDesign from './src/designer/tree-grid.design.component';
import { designPropsResolver, propsResolver, selectionItemResolver, bindingResolver, updateColumnsResolver } from './src/tree-grid.props';
import { withInstall } from '@farris/ui-vue/components/common';

export * from './src/tree-grid.props';

FTreeGrid.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap['tree-grid'] = FTreeGrid;
    propsResolverMap['tree-grid'] = propsResolver;
    resolverMap['tree-grid'] = { bindingResolver, selectionItemResolver, updateColumnsResolver };
};

FTreeGrid.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['tree-grid'] = FTreeGridDesign;
    propsResolverMap['tree-grid'] = designPropsResolver;
};

export { FTreeGrid };
export default withInstall(FTreeGrid);
