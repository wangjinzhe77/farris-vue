 
import { ComponentSchema, DesignerComponentInstance } from "../../../designer-canvas/src/types";
import { DynamicResolver } from "../../../dynamic-resolver";

function wrapComponentForDataGrid(resolver: DynamicResolver, schema: ComponentSchema, context: Record<string, any>): ComponentSchema {
    const parentComponentInstance = context.parentComponentInstance as DesignerComponentInstance;
    const parentContainerType = parentComponentInstance.schema.type;
    const parentClass = String(parentComponentInstance.schema.appearance?.class || '').split(' ');
    const droppedOnCardContainer = parentContainerType === 'section' || parentContainerType === 'tab-page';
    const droppedOnNavContainer = parentClass.includes('f-page-content-nav');
    const droppedOnMainContainer = parentClass.includes('f-page-main');
    const componentSchema = resolver.getSchemaByType('component') as ComponentSchema;
    componentSchema.id = `${schema.id}-component`;
    componentSchema.componentType = 'data-grid';
    let componentClass = '';
    componentClass += droppedOnCardContainer ? 'f-struct-data-grid-in-card f-struct-is-subgrid' : '';
    componentClass += droppedOnNavContainer ? 'f-struct-data-grid-in-nav f-struct-wrapper f-utils-fill-flex-column' : '';
    componentClass += droppedOnMainContainer ? 'f-struct-data-grid f-struct-wrapper' : '';
    // let structWrapperClass = droppedOnTitledContainer ? 'f-struct-is-subgrid' : 'f-struct-data-grid f-struct-wrapper';
    // structWrapperClass += droppedOnFilledContainer ? ' f-utils-fill-flex-column' : '';
    componentSchema.appearance = {
        class: componentClass
    };
    componentSchema.contents = [schema];
    return componentSchema;
}

function wrapSectionForDataGrid(resolver: DynamicResolver, schema: ComponentSchema, context: Record<string, any>): ComponentSchema {
    const parentComponentInstance = context.parentComponentInstance as DesignerComponentInstance;
    const parentClass = String(parentComponentInstance.schema.appearance.class || '').split(' ');
    const droppedOnNavContainer = parentClass.includes('f-page-content-nav');
    const droppedOnMainContainer = parentClass.includes('f-page-main');
    const sectionSchema = resolver.getSchemaByType('section') as ComponentSchema;
    sectionSchema.id = `${schema.id}-section`;
    let sectionClass = '';
    sectionClass += droppedOnNavContainer ? 'f-section-in-nav ' : '';
    sectionClass += droppedOnMainContainer ? 'f-section-in-managelist ' : '';
    sectionClass += 'f-section-grid';
    sectionSchema.appearance = { class: sectionClass };
    sectionSchema.contents = [schema];
    return sectionSchema;
}

function wrapContentContainerForDataGrid(resolver: DynamicResolver, schema: ComponentSchema, context: Record<string, any>): ComponentSchema {
    const contentContainerSchema = resolver.getSchemaByType('content-container') as ComponentSchema;
    contentContainerSchema.id = `${schema.id}-layout`;
    contentContainerSchema.appearance = { class: 'f-grid-is-sub f-utils-flex-column' };
    contentContainerSchema.contents = [schema];
    return contentContainerSchema;
}

export function schemaResolver(resolver: DynamicResolver, schema: Record<string, any>, context: Record<string, any>): Record<string, any> {
    const parentComponentInstance = context.parentComponentInstance as DesignerComponentInstance;
    const parentContainerType = parentComponentInstance.schema.type;
    const droppedOnTitledContainer = parentContainerType === 'section' || parentContainerType === 'tab-page';
    schema.appearance = { class: 'f-component-grid' };
    const wrapContainerForDataGrid = droppedOnTitledContainer ? wrapContentContainerForDataGrid : wrapSectionForDataGrid;
    const gridWrappedWithContainer = wrapContainerForDataGrid(resolver, schema as ComponentSchema, context);
    const gridWrappedWithComponent = wrapComponentForDataGrid(resolver, gridWrappedWithContainer, context);
    return gridWrappedWithComponent;
}
