
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, onUnmounted, onMounted, ref, nextTick, watch, SetupContext } from 'vue';
import { useResizeObserver } from '@vueuse/core';
import { TreeGridProps, treeGridProps } from './tree-grid.props';
import {
    getColumnHeader, getDisableMask, getHorizontalScrollbar, getSidebar, getVerticalScrollbar,
    DataViewOptions, useDataView, useDataViewContainerStyle, useEdit, useHierarchy, useVirtualScroll,
    useVisualData, useVisualDataBound, useVisualDataCell, useVisualDataRow, useSelection,
    useGroupData, useFilter, useColumn, useGroupColumn, useFitColumn, useSort,
    useSidebar, useFilterHistory, useColumnFilter, useRow, useDragColumn, VisualData,
    useCellPosition, useCommandColumn, useSelectHierarchyItem, DataColumn,
    useIdentify, useLoading, getEmpty
} from '@farris/ui-vue/components/data-view';
import getDataArea from './components/data/data-area.component';
import './tree-grid.css';

export default defineComponent({
    name: 'FTreeGrid',
    props: treeGridProps,
    emits: ['selectionChange', 'clickRow', 'expandNode'],
    setup(props: TreeGridProps, context) {
        const preloadCount = 0;
        const rowHeight = props.rowOption?.height || 28;
        const defaultVisibleCapacity = ref(20);
        const columns = ref(props.columns);
        const useIdentifyComposition = useIdentify(props as DataViewOptions);
        const dataGridRef = ref<any>();
        const gridContentRef = ref<any>();
        const primaryGridContentRef = ref<any>();
        const leftFixedGridContentRef = ref<any>();
        const rightFixedGridContentRef = ref<any>();
        const visibleDatas = ref<VisualData[]>([]);
        const mouseInContent = ref(false);
        const wrapContent = ref(props.rowOption?.wrapContent || false);
        const useGroupDataComposition = useGroupData(props as DataViewOptions, useIdentifyComposition);
        const useFilterComposition = useFilter();
        const useHierarchyComposition = useHierarchy(props as DataViewOptions);
        const { showLoading, renderLoading } = useLoading(props, dataGridRef);
        const treeDataView = useDataView(props as DataViewOptions, new Map(), useFilterComposition, useHierarchyComposition, useIdentifyComposition);
        const useSelectionComposition = useSelection(props as DataViewOptions, treeDataView, useIdentifyComposition, visibleDatas, context as SetupContext);
        const useSelectHierarchyItemComposition = useSelectHierarchyItem(props as DataViewOptions, visibleDatas, useIdentifyComposition, useSelectionComposition, context as SetupContext);
        const isDisabled = computed(() => props.disabled);
        const visibleCapacity = computed(() => {
            return props.virtualized ? Math.min(treeDataView.dataView.value.length, defaultVisibleCapacity.value) : treeDataView.dataView.value.length;
        });

        const { containerStyleObject } = useDataViewContainerStyle(props as DataViewOptions);

        const useCommandColumnComposition = useCommandColumn(props as DataViewOptions);
        const { applyCommands } = useCommandColumnComposition;
        applyCommands(columns);

        const useColumnComposition = useColumn(props as DataViewOptions);
        const useSortComposition = useSort(props as DataViewOptions);
        const { applyColumnSorter, columnContext, updateColumnRenderContext } = useColumnComposition;
        applyColumnSorter(treeDataView, useSortComposition);

        const useGroupColumnComposition = useGroupColumn(props as DataViewOptions, columnContext);
        const useRowComposition = useRow(props as DataViewOptions, context as SetupContext, useSelectionComposition, useIdentifyComposition);
        const useEditComposition = useEdit(props as DataViewOptions, context as SetupContext, useIdentifyComposition, useRowComposition);

        const useVisualDataBoundComposition = useVisualDataBound(props as DataViewOptions);

        const useVisualDataCellComposition = useVisualDataCell(
            props as DataViewOptions, useEditComposition, useVisualDataBoundComposition
        );

        const useVisualDataRowComposition = useVisualDataRow(
            props as DataViewOptions,
            useEditComposition,
            useHierarchyComposition,
            useIdentifyComposition,
            useVisualDataBoundComposition,
            useVisualDataCellComposition,
        );

        const useVisualDataComposition = useVisualData(
            props as DataViewOptions,
            columns,
            treeDataView,
            visibleCapacity,
            preloadCount,
            useVisualDataRowComposition
        );
        const { getVisualData } = useVisualDataComposition;

        const useCellPositionComposition = useCellPosition(props as DataViewOptions, columnContext);

        const useSidebarComposition = useSidebar(props as DataViewOptions, useSelectionComposition);
        const { sidebarWidth } = useSidebarComposition;

        const useVirtualScrollComposition = useVirtualScroll(
            props as DataViewOptions,
            treeDataView,
            visibleDatas,
            columnContext,
            useVisualDataComposition,
            visibleCapacity,
            preloadCount,
            sidebarWidth
        );
        const { onWheel, dataGridWidth, viewPortHeight, viewPortWidth, resetScroll, updateVisibleRowsOnLatestVisibleScope } = useVirtualScrollComposition;

        const useFitColumnComposition = useFitColumn(props as DataViewOptions, columnContext, gridContentRef, viewPortWidth, useGroupColumnComposition);
        const { calculateColumnsSize } = useFitColumnComposition;

        const useFilterHistoryComposition = useFilterHistory();

        const useColumnFilterComposition = useColumnFilter(
            gridContentRef,
            rightFixedGridContentRef,
            treeDataView,
            useFilterHistoryComposition,
            useVirtualScrollComposition
        );

        const useDragColumnComposition = useDragColumn(
            props as DataViewOptions,
            context as SetupContext,
            useColumnComposition,
            treeDataView,
            useGroupColumnComposition,
            useGroupDataComposition,
            useVirtualScrollComposition
        );

        const gridClass = computed(() => {
            const classObject = {
                'fv-grid': true,
                'fv-grid-bordered': props.showBorder,
                'fv-grid-horizontal-bordered': props.showHorizontalLines,
                'fv-datagrid-strip': props.showStripe
            } as Record<string, boolean>;
            return classObject;
        });

        const gridContentClass = computed(() => {
            const classObject = {
                'fv-grid-content': true,
                'fv-grid-content-hover': mouseInContent.value,
                'fv-grid-wrap-content': wrapContent.value
            } as Record<string, boolean>;
            return classObject;
        });

        const { renderDataArea } = getDataArea(
            props,
            context as SetupContext,
            primaryGridContentRef,
            leftFixedGridContentRef,
            rightFixedGridContentRef,
            useCellPositionComposition,
            useColumnComposition,
            treeDataView,
            useEditComposition,
            useHierarchyComposition,
            useRowComposition,
            useSelectionComposition,
            useSelectHierarchyItemComposition,
            useVirtualScrollComposition,
            useVisualDataComposition,
            useVisualDataBoundComposition,
            visibleDatas
        );

        const { renderGridHeader, renderGridColumnResizeOverlay, shouldShowHeader } = getColumnHeader(
            props as DataViewOptions,
            context as SetupContext,
            gridContentRef,
            leftFixedGridContentRef,
            rightFixedGridContentRef,
            useColumnComposition,
            treeDataView,
            useDragColumnComposition,
            useColumnFilterComposition,
            useFilterComposition,
            useFilterHistoryComposition,
            useFitColumnComposition,
            useGroupColumnComposition,
            useSelectionComposition,
            useSidebarComposition,
            useSortComposition,
            useVirtualScrollComposition,
            viewPortWidth,
            visibleDatas
        );

        const { renderDataGridSidebar } = getSidebar(props as DataViewOptions, useRowComposition, useSelectionComposition, useSidebarComposition, useVirtualScrollComposition);

        const { renderDisableMask } = getDisableMask();

        const { renderHorizontalScrollbar } = getHorizontalScrollbar(props as DataViewOptions, gridContentRef, useVirtualScrollComposition);

        const { renderVerticalScrollbar } = getVerticalScrollbar(props as DataViewOptions, gridContentRef, useVirtualScrollComposition);

        function onGridContentResize() {
            if (gridContentRef.value && gridContentRef.value.clientHeight > 0 && gridContentRef.value?.clientWidth > 0) {
                const newVisibleCapacity = Math.ceil(gridContentRef.value.clientHeight / rowHeight);
                if (newVisibleCapacity > defaultVisibleCapacity.value) {
                    defaultVisibleCapacity.value = newVisibleCapacity;
                    updateVisibleRowsOnLatestVisibleScope();
                }
                viewPortHeight.value = primaryGridContentRef.value?.clientHeight || 0;
                dataGridWidth.value = gridContentRef.value?.clientWidth || 0;
                calculateColumnsSize();
            }
        }

        onMounted(() => {
            if (gridContentRef.value) {
                defaultVisibleCapacity.value = Math.max(Math.ceil(gridContentRef.value.clientHeight / rowHeight), defaultVisibleCapacity.value);
                visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
                useResizeObserver(gridContentRef.value, onGridContentResize);
                calculateColumnsSize();
                nextTick(() => {
                    if (gridContentRef.value) {
                        dataGridWidth.value = gridContentRef.value.clientWidth;
                    }
                    if (primaryGridContentRef.value) {
                        viewPortWidth.value = primaryGridContentRef.value.clientWidth;
                        viewPortHeight.value = primaryGridContentRef.value.clientHeight;
                    }
                });
            }
            if (showLoading.value) {
                renderLoading();
            }
        });

        onUnmounted(() => {
            // window.removeEventListener('resize', updateGridViewPortWidth);
        });

        watch(viewPortWidth, () => {
            if (gridContentRef.value) {
                calculateColumnsSize();
            }
        });

        function getSelectedRowVisualIndex() {
            const currentSelectionRow = useSelectionComposition.getSelectionRow();
            const visualIndex = currentSelectionRow ? currentSelectionRow.dataIndex - 1 : -1;
            return visualIndex;
        }

        /** 新增树节点 */
        function addNewDataItem() {
            const targetIndex = getSelectedRowVisualIndex();
            treeDataView.insertNewDataItem(targetIndex);
            visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
        }

        function addNewChildDataItem() {
            const targetIndex = getSelectedRowVisualIndex();
            treeDataView.insertNewChildDataItem(targetIndex);
            visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
        }

        /** 删除树节点 */
        function removeDataItem() {
            const targetIndex = getSelectedRowVisualIndex();
            const nextDataItemIdToBeSelected = useSelectHierarchyItemComposition.getNextSelectableHierarchyItemId(targetIndex);
            treeDataView.removeHierarchyDataItem(targetIndex);
            visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
            if (nextDataItemIdToBeSelected) {
                useSelectionComposition.selectItemById(nextDataItemIdToBeSelected);
            }
        }

        /** 编辑树节点 */
        function editDataItem(visualDataRow: VisualData) {
            useEditComposition.onEditingRow(visualDataRow);
        }

        /** 确定 */
        function acceptDataItem(visualDataRow: VisualData) {
            useEditComposition.acceptEditingRow(visualDataRow);
        }

        /** 取消 */
        function cancelDataItem(visualDataRow: VisualData) {
            useEditComposition.cancelEditingRow(visualDataRow);
        }

        function updateColumns(newColumns: DataColumn[]) {
            if (newColumns) {
                columns.value = newColumns;
                applyCommands(columns);
                updateColumnRenderContext(columns.value);
                applyColumnSorter(treeDataView, useSortComposition);
                calculateColumnsSize();
            }
        }

        function updateDataSource(newData: Record<string, any>[]) {
            if (newData) {
                treeDataView.load(newData);
                // visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
                resetScroll();
            }
        }

        function selectItemById(dataItemId: string) {
            useSelectionComposition.selectItemById(dataItemId);
        }

        function clickRowItemById(dataItemId: string) {
            const visibleItemToBeSelected = visibleDatas.value.find((visibleData: VisualData) => {
                return visibleData.raw[useIdentifyComposition.idField.value] === dataItemId;
            }) as VisualData;
            if (visibleItemToBeSelected) {
                useRowComposition.clickRowItem(visibleItemToBeSelected);
            }
        }

        function selectItemByIds(dataItemIds: string[]) {
            useSelectionComposition.selectItemByIds(dataItemIds);
        }
        function getSelectedItems() {
            return useSelectionComposition.getSelectedItems();
        }
        function clearSelection() {
            useSelectionComposition.clearSelection();
        }
        const shouldRenderEmptyContent = computed(() => !visibleDatas.value || !visibleDatas.value.length);
        // 渲染空数据提示
        const { renderEmpty } = getEmpty(context as SetupContext);
        function getSelectionRow() {
            return useSelectionComposition.getSelectionRow();
        }
        context.expose({
            addNewDataItem,
            addNewChildDataItem,
            removeDataItem,
            editDataItem,
            acceptDataItem,
            cancelDataItem,
            updateColumns,
            updateDataSource,
            selectItemById,
            selectItemByIds,
            getSelectedItems,
            clearSelection,
            getSelectionRow,
            clickRowItemById
        });

        return () => {
            return (
                <div ref={dataGridRef} class={gridClass.value} style={containerStyleObject.value} onWheel={onWheel}>
                    {gridContentRef.value && shouldShowHeader.value && renderGridHeader()}
                    <div ref={gridContentRef} class={gridContentClass.value}
                        onMouseover={() => mouseInContent.value = true}
                        onMouseleave={() => mouseInContent.value = false}>
                        {gridContentRef.value && renderDataGridSidebar(visibleDatas)}
                        {gridContentRef.value && renderDataArea()}
                        {gridContentRef.value && shouldRenderEmptyContent.value && renderEmpty()}
                        {gridContentRef.value && renderHorizontalScrollbar()}
                        {gridContentRef.value && renderVerticalScrollbar()}
                    </div>
                    {renderGridColumnResizeOverlay()}
                    {isDisabled.value && renderDisableMask()}
                </div>
            );
        };
    }
});
