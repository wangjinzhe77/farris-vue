import { PropertyChangeObject } from "../../../property-panel/src/composition/entity/property-entity";
import { BaseControlProperty } from "../../../property-panel/src/composition/entity/base-property";
import { FormSchemaEntity } from "../../../common/entity/entity-schema";

export class TreeGridProperty extends BaseControlProperty {

    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }

    getPropertyConfig(propertyData: any) {

        // 基本信息
        this.getBasicPropConfig(propertyData);

        // 外观
        this.getAppearanceProperties(propertyData);

        // 事件
        this.getEventPropConfig(propertyData);

        return this.propertyConfig;
    }

    getBasicPropConfig(propertyData: any) {
        const mainEntity = this.formSchemaUtils.getFormSchema()?.module?.entity[0]?.entities[0];
        const entityTreeData = this.assembleSchemaEntityToTree(mainEntity, 0);
        const self = this;
        const basicConfig = super.getBasicPropConfig(propertyData);
        this.propertyConfig.categories['basic'] = {
            description: 'Basic Information',
            title: '基本信息',
            properties: {
                ...basicConfig.properties,
                dataSource: {
                    description: '绑定数据源',
                    title: '绑定数据源',
                    editor: {
                        type: 'combo-tree',
                        textField: 'name',
                        valueField: 'label',
                        data: entityTreeData,
                        editable: false
                    },
                    readonly: true
                }
            },
            setPropertyRelates(changeObject: PropertyChangeObject, data: any) {
                switch (changeObject && changeObject.propertyID) {
                    case 'dataSource': {
                        const viewModelNode = self.formSchemaUtils.getViewModelById(self.viewModelId);
                        if (viewModelNode) {
                            const selectedEntity = entityTreeData.find(entityData => entityData.label === changeObject.propertyValue);
                            viewModelNode.bindTo = selectedEntity.bindTo;
                        }
                        self.designViewModelUtils.assembleDesignViewModel();
                        break;
                    }
                }
            }
        };
    }
    private getAppearanceProperties(propertyData: any) {
        const self = this;
        this.propertyConfig.categories['appearance'] = {
            title: '外观',
            properties: {
                columns: {
                    title: '列设置',
                    description: '列设置',
                    $converter: '/converter/items-count.converter',
                    editor: {
                        type: "grid-field-editor",
                        viewModelId: this.viewModelId,
                        gridData: propertyData,
                        gridType: "tree-grid-column"
                    },
                    // 这个属性，标记当属性变更得时候触发重新更新属性
                    refreshPanelAfterChanged: true,
                },
                fit: {
                    title: '适配父组件尺寸',
                    type: "boolean",
                    description: '是否适配父组件尺寸',
                    visible:false
                },
                showStripe: {
                    title: '显示条纹',
                    type: "boolean",
                    description: '是否显示条纹'
                },
                showBorder: {
                    title: '显示边框',
                    type: "boolean",
                    description: '是否显示边框'
                },
                // showTreeNodeIcons: {
                //     title: '显示图标',
                //     type: "boolean",
                //     description: '是否显示图标'
                // },
                // showSetting: {
                //     title: '显示设置按钮',
                //     type: "boolean",
                //     description: '是否显示设置按钮',
                //     visible:false
                // }
            },
            setPropertyRelates(changeObject: PropertyChangeObject, data: any) {
                switch (changeObject && changeObject.propertyID) {
                    case 'useBlankWhenDataIsEmpty':
                        propertyData.emptyDataHeight = propertyData.useBlankWhenDataIsEmpty ? 36 : 240;
                        break;
                    case 'fields':
                        propertyData.fields = changeObject.propertyValue || [];
                        break;

                }
            }
        };

        this.propertyConfig.categories['pagination'] = {
            title: '分页',
            $converter: '/converter/pagination.converter',
            properties: {
                enable: {
                    title: '启用分页',
                    type: 'boolean',
                    refreshPanelAfterChanged: true,
                    editor: {
                        readonly: true
                    }
                },
                mode: {
                    visible: propertyData.pagination?.enable || false,
                    title: '分页交互模式',
                    type: "enum",
                    editor: {
                        type: "combo-list",
                        textField: "name",
                        valueField: "value",
                        data: [{ "value": "client", "name": "客户端" }, { "value": "server", "name": "服务器端" }]
                    }
                },
                showIndex: {
                    visible: propertyData.pagination?.enable || false,
                    title: '显示页码',
                    type: 'boolean'
                },
                showLimits: {
                    visible: propertyData.pagination?.enable || false,
                    title: '显示每页记录数',
                    type: 'boolean'
                }
            }
        };
    }

    private getEventPropConfig(propertyData: any) {
        const events = [
            {
                "label": "onSelectionChange",
                "name": "行切换事件"
            },
            {
                "label": "onClickRow",
                "name": "行点击事件"
            }
        ];
        const self = this;
        const initialData = self.eventsEditorUtils['formProperties'](propertyData, self.viewModelId, events);
        const properties = {};
        properties[self.viewModelId] = {
            type: 'events-editor',
            editor: {
                initialData
            }
        };
        this.propertyConfig.categories['eventsEditor'] = {
            title: '事件',
            hideTitle: true,
            properties,
            // 这个属性，标记当属性变更得时候触发重新更新属性
            refreshPanelAfterChanged: true,
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject: any, data: any) {
                const parameters = changeObject.propertyValue;
                delete propertyData[self.viewModelId];
                if (parameters) {
                    parameters.setPropertyRelates = this.setPropertyRelates; // 添加自定义方法后，调用此回调方法，用于处理联动属性
                    self.eventsEditorUtils.saveRelatedParameters(propertyData, self.viewModelId, parameters['events'], parameters);
                }
                // 联动修改排序开关
                propertyData.remoteSort = propertyData.columnSorted ? true : false;
            }
        };
    }
    /**
     * 将schema实体表组装成树
     */
    private assembleSchemaEntityToTree(
        schemaEntity: FormSchemaEntity,
        layer: number,
        parent?: FormSchemaEntity,
        bindToPath = '',
        treeData: any[] = []
    ) {
        const bindTo = bindToPath ? `${bindToPath}/${schemaEntity.label}` : '/';
        treeData.push({
            id: schemaEntity.id,
            name: schemaEntity.name,
            label: schemaEntity.label,
            layer,
            parent: parent && parent.id,
            bindTo: bindTo.replace('//', '/')
        });

        if (schemaEntity.type.entities && schemaEntity.type.entities.length) {
            schemaEntity.type.entities.map(ele => this.assembleSchemaEntityToTree(ele, layer + 1, schemaEntity, bindTo, treeData));

        }

        return treeData;
    }
};

