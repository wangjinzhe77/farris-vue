
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref, SetupContext } from 'vue';
import { DataGridColumn, TreeGridProps } from '../../tree-grid.props';
import { DataColumn, DataViewOptions, getHierarchyRow, UseCellPosition, UseColumn, UseDataView, UseEdit, UseHierarchy, UseRow, UseSelectHierarchyItem, UseSelection, UseVirtualScroll, UseVisualData, UseVisualDataBound, VisualData } from '../../../../data-view';

export default function (
    props: TreeGridProps,
    context: SetupContext,
    primaryGridContentRef: Ref<any>,
    leftFixedGridContentRef: Ref<any>,
    rightFixedGridContentRef: Ref<any>,
    useCellPositionComposition: UseCellPosition,
    useColumnComposition: UseColumn,
    useDataViewComposition: UseDataView,
    useEditComposition: UseEdit,
    useHierarchyComposition: UseHierarchy,
    useRowComposition: UseRow,
    useSelectionCompostion: UseSelection,
    useSelectHierarchyItemComposition: UseSelectHierarchyItem,
    useVirtualScrollComposition: UseVirtualScroll,
    useVisualDataComposition: UseVisualData,
    useVisualDataBoundComposition: UseVisualDataBound,
    visibleDatas: Ref<VisualData[]>
) {
    const { calculateCellPositionInRow } = useCellPositionComposition;

    const { columnContext, hasLeftFixedColumn, hasRightFixedColumn } = useColumnComposition;

    const { gridDataStyle, leftFixedGridDataStyle, rightFixedGridDataStyle } = useVirtualScrollComposition;

    const { renderDataRow } = getHierarchyRow(props as DataViewOptions, context, columnContext, visibleDatas, useDataViewComposition, useEditComposition, useHierarchyComposition, useRowComposition, useSelectionCompostion, useSelectHierarchyItemComposition, useVirtualScrollComposition, useVisualDataComposition, useVisualDataBoundComposition);
    const visualDataRowRenders = [renderDataRow];

    function renderGridData(columns: DataColumn[], area: 'left' | 'primary' | 'right') {
        const cellPositionMap = calculateCellPositionInRow(columns);
        return visibleDatas.value
            .filter((visualData: VisualData) => visualData.visible !== false)
            .map((visibleData: VisualData) => {
                return visualDataRowRenders[visibleData.type](visibleData, cellPositionMap, area);
            });
    }

    function renderLeftFixedGrid() {
        return (
            <div ref={leftFixedGridContentRef} class="fv-grid-content-left-fixed">
                <div class="fv-grid-data" style={leftFixedGridDataStyle.value}>
                    {renderGridData(columnContext.value.leftColumns.filter((column: DataGridColumn) => column.visible), 'left')}
                </div>
            </div>
        );
    }

    function renderPrimaryFixedDataArea() {
        return (
            <div ref={primaryGridContentRef} class="fv-grid-content-primary">
                <div class="fv-grid-data" style={gridDataStyle.value}>
                    {renderGridData(columnContext.value.primaryColumns.filter((column: DataGridColumn) => column.visible), 'primary')}
                </div>
            </div>
        );
    }

    function renderRightFixedGrid() {
        return (
            <div ref={rightFixedGridContentRef} class="fv-grid-content-right-fixed">
                <div class="fv-grid-data" style={rightFixedGridDataStyle.value}>
                    {renderGridData(columnContext.value.rightColumns.filter((column: DataGridColumn) => column.visible), 'right')}
                </div>
            </div>
        );
    }

    function renderDataArea() {
        const renderResult: any[] = [];
        if (hasLeftFixedColumn.value) {
            renderResult.push(renderLeftFixedGrid());
        }
        renderResult.push(renderPrimaryFixedDataArea());
        if (hasRightFixedColumn.value) {
            renderResult.push(renderRightFixedGrid());
        }
        return renderResult;
    }

    return { renderDataArea };
}
