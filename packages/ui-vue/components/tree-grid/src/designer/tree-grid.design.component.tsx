
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, onMounted, ref, nextTick, watch, SetupContext, inject } from 'vue';
import { useResizeObserver } from '@vueuse/core';
import { TreeGridDesignProps, treeGridDesignProps } from '../tree-grid.props';
import {
    getHorizontalScrollbar, DataViewOptions, useDataView, useDataViewContainerStyle, useEdit, useHierarchy, useVirtualScroll,
    useVisualData, useVisualDataBound, useVisualDataCell, useVisualDataRow, useSelection, useColumn, useFitColumn, useSidebar, VisualData,
    useCommandColumn, useIdentify, useGroupColumn, usePagination,
    useFilter,
    useRow,
    DataColumn,
    useSort
} from '../../../data-view';
import getColumnHeader from '../../../data-grid/src/designer/column-header.design.component';
import { DesignerItemContext } from '../../../designer-canvas';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { useDesignerRulesForTreeGrid } from './use-designer-rules';
import { useDesignerFitColumn } from '../../../data-view/designer';

export default defineComponent({
    name: 'FTreeGridDesign',
    props: treeGridDesignProps,
    emits: [],
    setup(props: TreeGridDesignProps, context) {
        const elementRef = ref();
        const preloadCount = 0;
        const rowHeight = props.rowOption?.height || 28;
        const defaultVisibleCapacity = ref(20);
        const columns = ref(props.columns);
        const useIdentifyComposition = useIdentify(props as DataViewOptions);
        const gridContentRef = ref<any>();
        const primaryGridContentRef = ref<any>();
        const visibleDatas = ref<VisualData[]>([]);
        const mouseInContent = ref(false);
        const wrapContent = ref(props.rowOption?.wrapContent || false);
        const designerHostService = inject('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRulesForTreeGrid(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);
        // 为设计时的外层容器FDesignerItem增加样式
        componentInstance.value['designerClass'] = 'f-utils-fill-flex-column';
        const useFilterComposition = useFilter();
        const useHierarchyComposition = useHierarchy(props as DataViewOptions);
        const treeDataView = useDataView(props as DataViewOptions, new Map(), useFilterComposition, useHierarchyComposition, useIdentifyComposition);
        const useSelectionComposition = useSelection(props as DataViewOptions, treeDataView, useIdentifyComposition, visibleDatas, context as SetupContext);
        const visibleCapacity = computed(() => {
            return treeDataView.dataView.value.length;
        });

        const { containerStyleObject } = useDataViewContainerStyle(props as DataViewOptions);

        const useCommandColumnComposition = useCommandColumn(props as DataViewOptions);
        const { applyCommands } = useCommandColumnComposition;
        applyCommands(columns);

        const useColumnComposition = useColumn(props as DataViewOptions);
        const { applyColumnSorter, columnContext, updateColumnRenderContext } = useColumnComposition;
        const useSortComposition = useSort(props as DataViewOptions);

        const useGroupColumnComposition = useGroupColumn(props as DataViewOptions, columnContext);
        const useRowComposition = useRow(props as DataViewOptions, context as SetupContext, useSelectionComposition, useIdentifyComposition);
        const useEditComposition = useEdit(props as DataViewOptions, context as SetupContext, useIdentifyComposition, useRowComposition);

        const useVisualDataBoundComposition = useVisualDataBound(props as DataViewOptions);

        const useVisualDataCellComposition = useVisualDataCell(
            props as DataViewOptions, useEditComposition, useVisualDataBoundComposition
        );

        const useVisualDataRowComposition = useVisualDataRow(
            props as DataViewOptions,
            useEditComposition,
            useHierarchyComposition,
            useIdentifyComposition,
            useVisualDataBoundComposition,
            useVisualDataCellComposition
        );

        const useVisualDataComposition = useVisualData(
            props as DataViewOptions,
            columns,
            treeDataView,
            visibleCapacity,
            preloadCount,
            useVisualDataRowComposition
        );
        const { getVisualData } = useVisualDataComposition;

        const useSidebarComposition = useSidebar(props as DataViewOptions, useSelectionComposition);
        const { sidebarWidth } = useSidebarComposition;

        visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);

        const useVirtualScrollComposition = useVirtualScroll(
            props as DataViewOptions,
            treeDataView,
            visibleDatas,
            columnContext,
            useVisualDataComposition,
            visibleCapacity,
            preloadCount,
            sidebarWidth
        );
        const { onWheel, dataGridWidth, viewPortHeight, viewPortWidth, updateVisibleRowsOnLatestVisibleScope } = useVirtualScrollComposition;

        const useFitColumnComposition = useDesignerFitColumn(props as DataViewOptions, columnContext, gridContentRef, viewPortWidth, useGroupColumnComposition);
        const { calculateColumnsSize } = useFitColumnComposition;

        const gridClass = computed(() => {
            const classObject = {
                'drag-container': true,
                'fv-grid': true,
                'fv-grid-bordered': props.showBorder,
                'fv-datagrid-strip': props.showStripe
            } as Record<string, boolean>;
            return classObject;
        });

        const gridContentClass = computed(() => {
            const classObject = {
                'fv-grid-content': true,
                'fv-grid-content-hover': mouseInContent.value,
                'fv-grid-wrap-content': wrapContent.value
            } as Record<string, boolean>;
            return classObject;
        });
        const { renderGridHeader } = getColumnHeader(
            designItemContext as DesignerItemContext,
            props as DataViewOptions,
            useColumnComposition,
            useVirtualScrollComposition,
            'tree-grid-column'
        );

        const { renderHorizontalScrollbar } = getHorizontalScrollbar(props as DataViewOptions, gridContentRef, useVirtualScrollComposition);

        function onGridContentResize() {
            const newVisibleCapacity = Math.ceil(gridContentRef.value.clientHeight / rowHeight);
            if (newVisibleCapacity > defaultVisibleCapacity.value) {
                defaultVisibleCapacity.value = newVisibleCapacity;
                updateVisibleRowsOnLatestVisibleScope();
            }
            viewPortHeight.value = primaryGridContentRef.value?.clientHeight || 0;
            dataGridWidth.value = gridContentRef.value?.clientWidth || 0;
            calculateColumnsSize();
        }

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
            if (gridContentRef.value) {
                defaultVisibleCapacity.value = Math.max(Math.ceil(gridContentRef.value.clientHeight / rowHeight), defaultVisibleCapacity.value);
                visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
                useResizeObserver(gridContentRef.value, onGridContentResize);
                calculateColumnsSize();
                nextTick(() => {
                    if (gridContentRef.value) {
                        dataGridWidth.value = gridContentRef.value.clientWidth;
                    }
                    if (primaryGridContentRef.value) {
                        viewPortWidth.value = primaryGridContentRef.value.clientWidth;
                        viewPortHeight.value = primaryGridContentRef.value.clientHeight;
                    }
                });
            }
        });
        function updateColumns(newColumns: DataColumn[]) {
            if (newColumns) {
                columns.value = newColumns;
                applyCommands(columns);
                updateColumnRenderContext(columns.value);
                applyColumnSorter(treeDataView, useSortComposition);
                calculateColumnsSize();
            }
        }

        watch(() => props.columns, (newDatas) => {
            updateColumns(newDatas);
        });

        watch(viewPortWidth, () => {
            if (gridContentRef.value) {
                calculateColumnsSize();
            }
        });
        context.expose(componentInstance.value);

        return () => {
            return (
                <div ref={elementRef} class={gridClass.value} style={containerStyleObject.value} onWheel={onWheel} dragref={`${designItemContext.schema.id}-container`}>
                    {gridContentRef.value && renderGridHeader()}
                    <div ref={gridContentRef} class={gridContentClass.value}
                        onMouseover={() => { mouseInContent.value = true; }}
                        onMouseleave={() => { mouseInContent.value = false; }}>
                        {renderHorizontalScrollbar()}
                    </div>
                </div>
            );
        };
    }
});
