import { ref } from "vue";
import { DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema, DesignerItemContext } from "../../../designer-canvas/src/types";
import { TreeGridProperty } from "../property-config/tree-grid.property-config";

export function useDesignerRulesForTreeGrid(designItemContext: DesignerItemContext, designerHostService): UseDesignerRules {
    const schema = designItemContext.schema as ComponentSchema;
    /** 组件在拖拽时需要将所属的Component一起拖拽 */
    const triggerBelongedComponentToMoveWhenMoved = ref(true);
    /** 组件在删除时需要将所属的Component一起拖拽 */
    const triggerBelongedComponentToDeleteWhenDeleted = ref(true);

    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {
        // 接收工具箱中拖拽来的输入类控件
        if (draggingContext && draggingContext.sourceType === 'control') {
            return draggingContext.componentCategory === 'input';
        }
        return false;
    }

    /**
     *  tree-grid不支持删除，需要选中所属组件Component节点删除。
     */
    function checkCanDeleteComponent() {
        return false;
    }
    /**
     * tree-grid不支持移动，需要选中所属组件Component节点移动。
     */
    function checkCanMoveComponent() {
        return false;
    }

    function hideNestedPaddingInDesginerView() {
        return true;
    }

    /**
     * 构造属性配置方法
     */
    function getPropsConfig(componentId: string) {
        const dataGridProp = new TreeGridProperty(componentId, designerHostService);

        return dataGridProp.getPropertyConfig(schema);
    }

    return {
        canAccepts,
        checkCanDeleteComponent,
        checkCanMoveComponent,
        hideNestedPaddingInDesginerView,
        triggerBelongedComponentToMoveWhenMoved,
        triggerBelongedComponentToDeleteWhenDeleted,
        getPropsConfig
    } as UseDesignerRules;

}
