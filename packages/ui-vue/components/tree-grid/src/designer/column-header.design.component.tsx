 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, ref, Ref } from 'vue';
import {
    HeaderCell,
    HeaderCellStatus,
    UseColumn,
    UseDataView,
    UseDragColumn,
    UseColumnFilter,
    UseFilterHistory,
    UseFitColumn,
    UseGroupColumn,
    UseSidebar,
    UseSort,
    UseVirtualScroll,
    UseFilter,
    DataViewOptions,
    UseSelection,
    VisualData
} from '@farris/ui-vue/components/data-view';
import { FCheckbox } from '@farris/ui-vue/components/checkbox';
import { FDesignerItem, DesignerItemContext } from '@farris/ui-vue/components/designer-canvas';
export default function (
    designItemContext: DesignerItemContext,
    props: DataViewOptions,
    gridContentRef: Ref<any>,
    leftFixedGridContentRef: Ref<any>,
    rightFixedGridContentRef: Ref<any>,
    useColumnComposition: UseColumn,
    useDataViewComposition: UseDataView,
    useDragColumnComposition: UseDragColumn,
    useColumnFilterComposition: UseColumnFilter,
    useFilterComposition: UseFilter,
    useFilterHistoryComposition: UseFilterHistory,
    useFitColumnComposition: UseFitColumn,
    useGroupColumnComposition: UseGroupColumn,
    useSelectionComposition: UseSelection,
    useSidebarComposition: UseSidebar,
    useSortComposition: UseSort,
    useVirtualScrollComposition: UseVirtualScroll,
    viewPortWidth: Ref<number>,
    visibleDatas: Ref<VisualData[]>
) {
    const { showSelectAll, selectAll, unSelectAll, selectedValues } = useSelectionComposition;
    const { showRowNumer, showSidebarCheckBox, sidebarTitle, sidebarCornerCellStyle } = useSidebarComposition;
    const { columnContext, hasLeftFixedColumn, hasRightFixedColumn } = useColumnComposition;
    const { gridHeaderColumnsStyle, leftFixedGridHeaderColumnsStyle, rightFixedGridHeaderColumnsStyle } = useVirtualScrollComposition;
    const shouldShowSideBarCorner = computed(() => showSelectAll.value || showSidebarCheckBox.value || showRowNumer.value);

    const defaultColumnHeight = columnContext.value.defaultColumnWidth;
    const gridHeaderCellHeight = 32;
    const headerCellPositionMap = new Map<number, number>([[0, 0]]);

    function headerCellPosition(headerCell: HeaderCell, columnIndex: number): Record<string, any> {
        const headerCellPosition = headerCellPositionMap.get(columnIndex) || 0;
        const styleObject = {
            height: `${headerCell.depth * gridHeaderCellHeight}px`,
            left: `${headerCell.left}px`,
            top: `${(headerCell.layer - 1) * gridHeaderCellHeight}px`,
            width: `${headerCell.actualWidth}px`,
            padding: 0
        } as Record<string, any>;
        if (columnContext.value.headerDepth > 1) {
            styleObject['line-height'] = `${headerCell.depth * gridHeaderCellHeight}px`;
        }
        headerCellPositionMap.set(columnIndex + 1, headerCellPosition + (headerCell.actualWidth || defaultColumnHeight));
        return styleObject;
    }

    const headerRowClass = computed(() => {
        const classObject = {
            'fv-grid-header': true,
            'fv-grid-header-group-columns': columnContext.value.headerDepth > 1
        } as Record<string, boolean>;
        return classObject;
    });

    const headerRowStyle = computed(() => {
        const styleObject = {
            height: `${columnContext.value.headerDepth * gridHeaderCellHeight}px`
        } as Record<string, any>;
        return styleObject;
    });

    const shouldShowHeaderOperation = function (headerCell: HeaderCell) {
        return (
            (headerCell.status & HeaderCellStatus.sortable) === HeaderCellStatus.sortable ||
            (headerCell.status & HeaderCellStatus.filterable) === HeaderCellStatus.filterable
        );
    };

    const shouldShowGridSettings = function (headerCell: HeaderCell) {
        return props.showSetting && !!headerCell.showSetting;
    };
    function onSelectionChange(contentType, schema, componentId, componentInstance) {
        designItemContext?.setupContext?.emit('selectionChange', contentType, schema, componentId, componentInstance);
    }
    function getColumnSchema(fieldName) {
        const columnKey = Object.prototype.hasOwnProperty.call(designItemContext.schema, 'fields') ? 'fields' : 'columns';
        const columnSchema = designItemContext.schema[columnKey].find(item => item.dataField === fieldName);
        return Object.assign(columnSchema, { type: "data-grid-column" });
    }
    function renderGridHeaderCell(headerCell: HeaderCell, headerCells: HeaderCell[], columnIndex: number) {
        // const columnSchema=getColumnSchema(headerCell.field);
        const columnSchema = headerCell.column;
        return (
            <div
                class="fv-grid-header-cell"
                style={headerCellPosition(headerCell, columnIndex)}>
                <FDesignerItem v-model={columnSchema} customClass={'w-100 d-flex align-item-center justify-content-center'} canAdd={false} canDelete={false} canMove={false} key={columnSchema?.id} type="data-grid-column" componentId={props['componentId']} onSelectionChange={onSelectionChange}>
                </FDesignerItem>
            </div>
        );
    }

    function renderGridHeaderColumns(headerCells: HeaderCell[]): any[] {
        return headerCells.map((headerCell: HeaderCell, columnIndex: number) => {
            const headerCellsNodes: any[] = [];
            headerCellsNodes.push(renderGridHeaderCell(headerCell, headerCells, columnIndex));
            if (headerCell.children && headerCell.children.length) {
                const subHeaderCellNodes = renderGridHeaderColumns(headerCell.children);
                headerCellsNodes.push(...subHeaderCellNodes);
            }
            return headerCellsNodes;
        });
    }

    function renderGridHeaderLeftFixed() {
        return (
            <div class="fv-grid-header-left-fixed">
                <div class="fv-grid-header-columns" style={leftFixedGridHeaderColumnsStyle.value}>
                    {renderGridHeaderColumns(columnContext.value.leftHeaderColumns)}
                </div>
            </div>
        );
    }

    function renderGridHeaderRigthFixed() {
        return (
            <div class="fv-grid-header-right-fixed">
                <div class="fv-grid-header-columns" style={rightFixedGridHeaderColumnsStyle.value}>
                    {renderGridHeaderColumns(columnContext.value.rightHeaderColumns)}
                </div>
            </div>
        );
    }

    const hasSelectedAll = ref<boolean>(visibleDatas.value.length === selectedValues.value.length);

    function onClickSelectAll(payload: MouseEvent) {
        hasSelectedAll.value ? selectAll() : unSelectAll();
    }

    const hasSelectedItem = computed(() => {
        return selectedValues.value.length > 0 && visibleDatas.value.length !== selectedValues.value.length;
    });

    function renderGridSideBarCorner() {
        return (
            <div class="fv-grid-header-corner d-flex" style={sidebarCornerCellStyle.value}>
                {showSelectAll.value && (
                    <FCheckbox id='checkbox_for_select_all' indeterminate={hasSelectedItem.value} v-model={hasSelectedAll.value}
                        onChange={onClickSelectAll}
                        style="margin-left:8px"
                    ></FCheckbox>
                )}
                {showRowNumer.value &&
                    <div style="line-height:18px">{sidebarTitle.value}</div>}
            </div>
        );
    }

    function renderGridHeader() {
        return (
            <div class={headerRowClass.value} style={headerRowStyle.value}>
                {shouldShowSideBarCorner.value && renderGridSideBarCorner()}
                {hasLeftFixedColumn.value && renderGridHeaderLeftFixed()}
                <div class="fv-grid-header-primary">
                    <div class="fv-grid-header-columns" style={gridHeaderColumnsStyle.value}>

                        {renderGridHeaderColumns(columnContext.value.primaryHeaderColumns)}
                    </div>
                </div>
                {hasRightFixedColumn.value && renderGridHeaderRigthFixed()}
            </div>
        );
    }

    return { renderGridHeader };
}
