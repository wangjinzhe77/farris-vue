import { ref, defineComponent, onBeforeMount, onMounted, computed } from "vue";
import { FNotifyService } from "../notify";
import { entityBindingSelectorProps, EntityBindingSelectorProps } from "./entity-binding-selector.props";
import { FTreeGrid } from '../tree-grid';
import { useEntityTree } from "./composition/use-entity-tree";
import { ComponentBindingSourceContext } from "../designer-canvas/src/composition/types";
import { Step } from '../step';
import { useFieldTree } from "./composition/use-field-tree";
import { FDynamicFormGroup } from "../dynamic-form";

export default defineComponent({
    name: 'FEntityBindingSelector',
    props: entityBindingSelectorProps,
    emits: ['submit', 'cancel'],
    setup(props: EntityBindingSelectorProps, context) {
        const useEntityTreeUtil = useEntityTree(props);
        const { entityColumns, entityTreeGridRef, entityDataSource, checkAndGetSelectedEntity, resolveEntityDataSource, entityTreeRowOption, initialSelectedEntity, isMainEntityInitialSelected } = useEntityTreeUtil;

        const useFieldTreeUtil = useFieldTree(props);
        const { fieldColumns, resolveFieldDataSource, fieldDataSource, fieldTreeGridRef, fieldTreeRowOption, fieldSelectionOption, checkAndGetSelectedFields, currentComponentType } = useFieldTreeUtil;

        const notifyService: any = new FNotifyService();
        notifyService.globalConfig = { position: 'top-center' };
        const componentTypeEnums = [
            { name: '卡片面板', value: 'response-form' },
            { name: '表格', value: 'data-grid' }
        ];
        const componentType = ref();
        const componentTitle = ref('');
        const steps = ref([
            { id: 'selectComponentType', title: '选择组件类型' },
            { id: 'selectEntity', title: '选择实体' },
            { id: 'selectFields', title: '选择显示字段' }
        ]);

        const activeStepIndex = ref(0);
        const activeStep = ref('');
        const stepPanelClass = computed(() => (stepId: string) => {
            return {
                'f-utils-fill': true,
                'border': stepId !== 'selectComponentType',
                'mx-3': true,
                'my-2': true,
                'position-relative': true,
                'd-none': steps.value[activeStepIndex.value].id !== stepId
            };
        });
        function initComponentSelector() {
            if (props.bindingEntityId && initialSelectedEntity?.value) {
                componentTitle.value = initialSelectedEntity.value.name;
            }
            if (!props.componentType) {
                componentType.value = isMainEntityInitialSelected.value ? 'response-form' : 'data-grid';
            }
        }
        onBeforeMount(() => {
            if (props.steps) {
                steps.value = steps.value.filter(step => props.steps.includes(step.id));
            }
            activeStep.value = steps.value[0].id;
            resolveEntityDataSource();
            initComponentSelector();
        });

        onMounted(() => {
            if (entityTreeGridRef && initialSelectedEntity.value?.id) {
                entityTreeGridRef.value.selectItemById(initialSelectedEntity.value?.id);
            }
        });

        function onComponentTypeChanged(newValue: string) {
            if (fieldTreeGridRef?.value?.clearSelection) {
                fieldTreeGridRef.value.clearSelection();
            }
        }
        /** 选择实体 */
        function renderEntityTreeGrid() {
            return <FTreeGrid
                key={1}
                ref={entityTreeGridRef}
                fit={true}
                data={entityDataSource.value}
                idField="id"
                columns={entityColumns.value}
                rowNumber={{ enable: false }}
                row-option={entityTreeRowOption}
                columnOption={{ fitColumns: true }}
            ></FTreeGrid>;
        }
        /** 选择实体下的字段 */
        function renderFieldTreeGrid() {
            return <FTreeGrid
                key={2}
                ref={fieldTreeGridRef}
                fit={true}
                data={fieldDataSource.value}
                idField="id"
                columns={fieldColumns.value}
                rowNumber={{ enable: false }}
                columnOption={{ fitColumns: true }}
                row-option={fieldTreeRowOption}
                selection={fieldSelectionOption}
            ></FTreeGrid>;
        }
        /** 选择控件类型并填写控件标题 */
        function renderComponentSelector() {
            const componentTypeEditor = { editable: false, type: 'combo-list', data: componentTypeEnums, idField: 'value', valueField: 'value', textField: 'name' };
            const componentTitleEditor = { type: 'input-group', enableClear: false };
            return <div>
                <div class="f-utils-fill">
                    <div class="f-section py-0">
                        <div class="f-section-header">
                            <div class="f-title">
                                <h4 class="f-title-text">组件信息</h4>
                            </div>
                        </div>
                        <div class="f-section-content">
                            <div class="f-form-layout farris-form farris-form-controls-inline">
                                <FDynamicFormGroup label="组件类型" customClass="col-12" v-model={componentType.value} required="true" editor={componentTypeEditor} onChange={onComponentTypeChanged}></FDynamicFormGroup>
                                <FDynamicFormGroup label="组件标题" customClass="col-12" v-model={componentTitle.value} required="true" editor={componentTitleEditor}></FDynamicFormGroup>
                            </div>
                        </div>
                    </div>
                </div>
            </div>;
        }
        function clickPreviousStep() {
            activeStepIndex.value--;
            activeStep.value = steps.value[activeStepIndex.value].id;
        }
        function clickNextStep() {
            switch (activeStep.value) {
                case 'selectEntity': {
                    const selectedData = checkAndGetSelectedEntity();
                    if (!selectedData) {
                        return;
                    }
                    resolveFieldDataSource(selectedData);
                    fieldTreeGridRef.value.updateDataSource(fieldDataSource.value);
                    break;
                }
                case 'selectComponentType': {
                    if (!componentType.value) {
                        notifyService.warning('请先选择组件类型');
                        return;
                    }
                    if (!componentTitle.value?.trim()) {
                        notifyService.warning('请先填写组件标题');
                        return;
                    }
                    currentComponentType.value = componentType.value;
                    resolveFieldDataSource(initialSelectedEntity.value);
                    fieldTreeGridRef.value.updateDataSource(fieldDataSource.value);
                    break;
                }
            }

            activeStepIndex.value++;
            activeStep.value = steps.value[activeStepIndex.value].id;
        }
        function onCancel() {
            context.emit('cancel');
        }

        function onSubmit() {
            const selectedData = checkAndGetSelectedEntity();
            const selectedFields = checkAndGetSelectedFields();
            if (selectedData && selectedFields) {
                const bindingContext: ComponentBindingSourceContext = { bindingType: 'entity' };
                bindingContext.bindingEntity = selectedData;
                bindingContext.bindTo = useEntityTreeUtil.entityBindToMap.get(bindingContext.bindingEntity?.label || '');
                bindingContext.bindingEntityFields = selectedFields;
                bindingContext.entityTitle = componentTitle.value;
                context.emit('submit', { bindingContext, componentType: componentType.value });
            }
        }

        return () => {
            return (<div class="h-100 d-flex flex-column">
                <Step steps={steps} activeIndex={activeStepIndex} style="align-self:center"></Step>
                <div class={stepPanelClass.value('selectEntity')} style="border-radius:10px;">{renderEntityTreeGrid()}</div>
                <div class={stepPanelClass.value('selectComponentType')} style="border-radius:10px;">{renderComponentSelector()}</div>
                <div class={stepPanelClass.value('selectFields')} style="border-radius:10px;">{renderFieldTreeGrid()}</div>

                <div class="modal-footer">
                    <button class="btn btn-secondary" onClick={onCancel}>取消</button>
                    {activeStepIndex.value > 0 ? <button class="btn btn-secondary" onClick={clickPreviousStep}>上一步</button> : ''}
                    {activeStepIndex.value < 1 ? <button class="btn btn-primary" onClick={clickNextStep}>下一步</button> : ''}
                    {activeStepIndex.value > 0 ? <button class="btn btn-primary" onClick={onSubmit}>确定</button> : ''}
                </div>
            </div>);
        };
    }
});
