import { ExtractPropTypes } from "vue";

export const entityBindingSelectorProps = {
    designerHostService: { type: Object, default: {} },
    /** 预设的控件类型 */
    componentType: { type: String, default: '' },
    /** 预设绑定的实体id */
    bindingEntityId: { type: String, default: '' },
    /** 绑定步骤 */
    steps: { type: Array<string>, default: ['selectEntity', 'selectFields'] }
} as Record<string, any>;

export type EntityBindingSelectorProps = ExtractPropTypes<typeof entityBindingSelectorProps>;
