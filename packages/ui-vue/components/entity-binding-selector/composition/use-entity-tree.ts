import { FormSchemaEntity } from "../../common/entity/entity-schema";
import { cloneDeep } from "lodash-es";
import { EntityBindingSelectorProps } from "../entity-binding-selector.props";
import { ref } from "vue";
import { FNotifyService } from "../../notify";
import { RowOptions, VisualData } from "../../data-view";
import { useFieldTree } from "./use-field-tree";

export function useEntityTree(props: EntityBindingSelectorProps) {

    const { designerHostService } = props;
    const entityBindToMap: Map<string, string> = new Map();
    const entityTreeGridRef = ref();
    const entityDataSource = ref();
    const entityColumns = ref([{ field: 'name', title: '实体名称' }]);
    /** 初始选中的实体id */
    const initialSelectedEntity = ref();
    const isMainEntityInitialSelected = ref(false);
    /**
     * 判断实体中是否有可选择的字段，若没有，将实体置灰
     */
    function resolveHasSelectableFields(schemaEntity: FormSchemaEntity): boolean {
        const useFieldTreeUtil = useFieldTree(props);
        useFieldTreeUtil.resolveFieldDataSource(schemaEntity);
        if (useFieldTreeUtil.hasSelectableField.value) {
            initialSelectedEntity.value = initialSelectedEntity.value || schemaEntity;
            return true;
        }
        return false;
    }
    /**
     * 根据控件类型判断实体是否可以选择
     */
    function resolveSelectableByComponentType(schemaEntity: FormSchemaEntity): boolean {
        // 目前卡片面板只支持绑定主表
        const bindTo = entityBindToMap.get(schemaEntity.label);
        // if (props.componentType && props.componentType === 'response-form' && bindTo && bindTo !== '/') {
        //     return false;
        // }
        // 目前表格控件只支持绑定子表
        if (props.componentType && props.componentType === 'data-grid' && bindTo === '/') {
            return false;
        }
        return true;
    }
    /**
     * 判断实体是否可以选择
     */
    function resolveEntitySelectable(schemaEntity: FormSchemaEntity): boolean {
        if (props.bindingEntityId && props.bindingEntityId !== schemaEntity.id) {
            return false;
        }

        // if (!resolveSelectableByComponentType(schemaEntity)) {
        //     return false;
        // }
        if (!resolveHasSelectableFields(schemaEntity)) {
            return false;
        }
        return true;

    }
    function resolveEntity(schemaEntity: FormSchemaEntity, parentLabelPath: string = ''): any {
        if (!schemaEntity) {
            return;
        }
        const clonedSchemaEntity: FormSchemaEntity = cloneDeep(schemaEntity);

        const viewModelBindTo = parentLabelPath ? `${parentLabelPath}/${clonedSchemaEntity.label}` : `/`;
        entityBindToMap.set(clonedSchemaEntity.label, viewModelBindTo.replace('//', '/'));

        const selectable = resolveEntitySelectable(clonedSchemaEntity);
        const result: any = {
            data: Object.assign({ selectable }, clonedSchemaEntity),
            children: []
        };
        if (clonedSchemaEntity.type.entities && clonedSchemaEntity.type.entities.length) {
            const childTable = clonedSchemaEntity.type.entities.map(childEntity => resolveEntity(childEntity, viewModelBindTo));
            result.children = result.children.concat(childTable);
        }
        if (props.bindingEntityId === schemaEntity.id) {
            initialSelectedEntity.value = schemaEntity;
        }
        if (initialSelectedEntity.value?.id === schemaEntity.id && !parentLabelPath) {
            isMainEntityInitialSelected.value = true;
        }
        return result;
    }
    /**
     * 组装实体树
     */
    function resolveEntityDataSource() {
        const { formSchemaUtils } = designerHostService;
        const rootSchemaEntity = formSchemaUtils.getFormSchema().module.entity[0]?.entities[0];
        entityBindToMap.clear();

        const rootSchemaTreeNode = resolveEntity(rootSchemaEntity);
        entityDataSource.value = [rootSchemaTreeNode];
    }

    function checkAndGetSelectedEntity() {
        const selectedItems = entityTreeGridRef.value.getSelectedItems();
        if (!selectedItems.length) {
            const notifyService: any = new FNotifyService();
            notifyService.globalConfig = { position: 'top-center' };
            notifyService.warning({ message: '请先选择实体' });
            return;
        }
        return selectedItems[0].data;
    }
    /**
     * 配置实体列表的行禁用效果
     */
    const entityTreeRowOption: Partial<RowOptions> = {
        customRowStatus: (visualData: VisualData) => {
            visualData.disabled = !visualData.raw.selectable;
            return visualData;
        }
    };
    return {
        entityTreeGridRef,
        entityDataSource,
        resolveEntityDataSource,
        entityBindToMap,
        checkAndGetSelectedEntity,
        entityColumns,
        entityTreeRowOption,
        initialSelectedEntity,
        isMainEntityInitialSelected
    };
}
