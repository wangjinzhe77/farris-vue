import { FormSchemaEntity, FormSchemaEntityField } from "../../common/entity/entity-schema";
import { cloneDeep } from "lodash-es";
import { EntityBindingSelectorProps } from "../entity-binding-selector.props";
import { ref } from "vue";
import { RowOptions, VisualData } from "../../data-view";
import { FNotifyService } from "../../notify";

export function useFieldTree(props: EntityBindingSelectorProps) {

    const { designerHostService } = props;
    /** 表单中已使用的字段 */
    const occupiedFieldMap: Map<string, boolean> = new Map();
    const fieldDataSource = ref();
    const fieldTreeGridRef = ref();
    /** 树表列配置 */
    const fieldColumns = ref([
        { field: 'name', title: '字段名称' },
        { field: 'bindingField', title: '绑定字段' },
        { field: 'displayedTypeName', title: '字段类型' }
    ]);
    /** 树表选择模式 */
    const fieldSelectionOption = {
        enableSelectRow: true,
        multiSelect: true,
        multiSelectMode: 'OnCheckAndClick',
        showCheckbox: true,
        showSelectAll: true
    };
    /** 标识字段是否可以选择 */
    const hasSelectableField = ref(false);

    const currentComponentType = ref(props.componentType);
    /**
     * schema字段集合组装成树
     * @param fields schema字段集合
     */
    function resolveFields(fields: FormSchemaEntityField[]) {
        const treeData: any = [];
        hasSelectableField.value = false;
        fields.forEach(element => {
            let children = [];

            if (element.type && element.type.fields && element.type.fields.length > 0) {
                children = resolveFields(element.type.fields);
            }
            const clonedField = cloneDeep(element);
            const displayedTypeName = element.multiLanguage ? '多语言' : element.type.displayName;

            const isFieldSelectable = children.length ? false : !occupiedFieldMap.has(element.id);

            if (isFieldSelectable) {
                hasSelectableField.value = true;
            }
            treeData.push({
                data: Object.assign({ displayedTypeName, selectable: isFieldSelectable }, clonedField),
                children
            });
        });
        return treeData;
    }
    function getComponentTypeByControlType(componentType: string) {
        switch (componentType) {
            case 'data-grid': case 'tree-grid': {
                return 'data-grid';
            }
            case 'response-form': {
                return 'form';
            }
            default: {
                return componentType;
            }
        }
    }
    /**
     * 获取指定实体中已在当前表单中被占用的字段，场景：创建卡片组件时将已在其他组件中添加的字段排除掉
     */
    function getOccupiedFieldMapBySelectedEntity(bindingEntity: FormSchemaEntity) {
        occupiedFieldMap.clear();
        if (!bindingEntity) {
            return;
        }
        // 根组件和table组件内的输入控件与form内不能重复
        let targetComponentType = getComponentTypeByControlType(currentComponentType.value);
        if (['frame', 'table', 'form'].includes(targetComponentType)) {
            targetComponentType = 'form';
        }
        const { formSchemaUtils, schemaService } = designerHostService;

        formSchemaUtils.getFormSchema().module.viewmodels.forEach(viewModel => {
            if (!viewModel.fields || viewModel.fields.length === 0) {
                return;
            }
            const componentNode = formSchemaUtils.getComponentByViewModelId(viewModel.id);
            // 绑定同一个实体，并且是同类型的组件（form类、dataGrid类...）
            let sourceComponentType = componentNode.componentType;
            if (['frame', 'table', 'form'].includes(sourceComponentType)) {
                sourceComponentType = 'form';
            }
            const entityInfo = schemaService.getTableInfoByViewModelId(viewModel.id);

            if (sourceComponentType !== targetComponentType || !entityInfo || entityInfo.id !== bindingEntity.id) {
                return;
            }

            viewModel.fields.forEach(field => {
                occupiedFieldMap.set(field.id, true);
            });
        });

    }

    /**
     * 组装字段树
     */
    function resolveFieldDataSource(bindingEntity: FormSchemaEntity) {
        getOccupiedFieldMapBySelectedEntity(bindingEntity);
        const { fields } = bindingEntity.type;

        fieldDataSource.value = resolveFields(fields);
        if (fieldTreeGridRef.value) {
            fieldTreeGridRef.value.updateDataSource(fieldDataSource.value);
        }

    }

    /**
     * 配置字段列表的行禁用效果
     */
    const fieldTreeRowOption: Partial<RowOptions> = {
        customRowStatus: (visualData: VisualData) => {
            visualData.disabled = !visualData.raw.selectable;
            return visualData;
        }
    };
    /**
     * 获取已选字段
     */
    function checkAndGetSelectedFields() {
        const selectedItems = fieldTreeGridRef.value.getSelectedItems();
        if (!selectedItems.length) {
            const notifyService: any = new FNotifyService();
            notifyService.globalConfig = { position: 'top-center' };
            notifyService.warning({ message: '请先选择显示字段' });
            return;
        }
        return selectedItems.map(item => item.data);
    }
    return {
        fieldTreeGridRef,
        resolveFieldDataSource,
        occupiedFieldMap,
        fieldDataSource,
        fieldTreeRowOption,
        fieldColumns,
        fieldSelectionOption,
        checkAndGetSelectedFields,
        hasSelectableField,
        currentComponentType
    };
}
