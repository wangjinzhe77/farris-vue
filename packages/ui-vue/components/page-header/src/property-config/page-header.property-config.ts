import { BaseControlProperty } from "../../../property-panel/src/composition/entity/base-property";

export class PageHeaderProperty extends BaseControlProperty {

    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }

    getPropertyConfig(propertyData: any) {
        // 基本信息
        this.propertyConfig.categories['basic'] = this.getBasicPropConfig(propertyData);
        // 外观
        this.propertyConfig.categories['appearance'] = this.getAppearanceProperties(propertyData);

        return this.propertyConfig;
    }


    private getAppearanceProperties(propertyData: any) {
        return {
            title: '外观',
            description: 'Appearance',
            properties: {
                class: {
                    title: 'class样式',
                    type: 'string',
                    description: '组件的CSS样式'
                },
                icon: {
                    title: '图标',
                    type: 'string',
                    description: '图标'
                },
                title: {
                    title: '标题',
                    type: 'string',
                    description: '标题',
                }
            },
            setPropertyRelates(changeObject, prop) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject && changeObject.propertyID) {
                    case 'title': {
                        changeObject.needRefreshControlTree = true;
                        break;
                    }
                }
            }
        };
    }

};
