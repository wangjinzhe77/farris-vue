import { MapperFunction, resolveAppearance, resolveToolbar } from '../../../dynamic-resolver';

function resolveToolbarClass(key: string, buttonClass, headerSchema: any) {
    const toolbarObject = headerSchema.toolbar;
    return { buttonClass: toolbarObject?.appearance?.class };
}


export const schemaMapper = new Map<string, string | MapperFunction>([
    ['appearance', resolveAppearance],
    ['toolbar', resolveToolbar],
    ['buttonClass', resolveToolbarClass]
]);
