 
import { ExtractPropTypes } from "vue";
import { createPageHeaderEventHandlerResolver, createPropsResolver } from "../../dynamic-resolver";
import { schemaMapper } from "./schema/schema-mapper";
import pageHeaderSchema from './schema/page-header.schema.json';
// import pageHeaderPropertyConfig from './property-config/page-header.property-config.json';
import { schemaResolver } from './schema/schema-resolver';

export const pageHeaderProps = {
    /** 组件自定义样式 */
    customClass: { type: String, default: '' },

    /** 是否显示图标 */
    showIcon: { type: Boolean, default: true },

    /** 图标的名称 */
    icon: { type: String, default: 'f-icon-page-title-record' },

    /** 图标自定义样式，比如颜色、背景 */
    iconClass: { type: String, default: 'f-text-orna-bill' },

    /** 主标题 */
    title: { type: String, default: '' },

    /** 副标题 */
    subTitle: { type: String, default: '' },

    /** 按钮组的样式 */
    buttonClass: { type: String, default: 'col-6' },

    /** 按钮组 */
    buttons: { type: Array<any>, default: [] },

    /** 标题区域模板自定义样式 */
    titleContentClass: { type: String, default: '' },

    /** 内容区域模板自定义样式 */
    contentClass: { type: String, default: '' },

    /** 下方扩展区域模板自定义样式 */
    downContentClass: { type: String, default: '' },

    /** 是否显示翻页图标 */
    showPagination: { type: Boolean, default: false },

    /** 向前翻页是否禁用 */
    prePaginationDisabled: { type: Boolean, default: true },

    /** 向后翻页是否禁用 */
    nextPaginationDisabled: { type: Boolean, default: false }

} as Record<string, any>;
export type PageHeaderProps = ExtractPropTypes<typeof pageHeaderProps>;
export const pageHeaderDesignerProps=Object.assign({},pageHeaderProps,{
    componentId: { type: String, default: '' }
});
export type PageHeaderDesignerProps = ExtractPropTypes<typeof pageHeaderDesignerProps>;

export const propsResolver = createPropsResolver<PageHeaderProps>(pageHeaderProps, pageHeaderSchema, schemaMapper, schemaResolver);
export const eventHandlerResolver = createPageHeaderEventHandlerResolver();
