 
import { defineComponent, ref, computed, watch } from "vue";
import { PageHeaderProps, pageHeaderProps } from "./page-header.props";
import FResponseToolbar from '@farris/ui-vue/components/response-toolbar';

export default defineComponent({
    name: 'FPageHeader',
    props: pageHeaderProps,
    emits: ['click', 'prePaginationClick', 'nextPaginationClick'],
    setup(props: PageHeaderProps, context) {
        const items = ref(props.buttons);

        const onClickToolbarItem = (payload: any, itemId: string) => {
            context.emit('click', payload, itemId);
        };

        function classConverter(currentClass: object, customClass: string) {
            if (currentClass && customClass) {
                const customClassArray = customClass.split(' ');
                customClassArray.reduce((result: any, className: any) => {
                    result[className] = true;
                    return result;
                }, currentClass);
            }
        }
        const headerClass = computed(() => {
            const classObject = {
                'f-page-header': true
            } as Record<string, boolean>;
            classConverter(classObject, props.customClass);
            return classObject;
        });
        const iconClass = computed(() => {
            const classObject = {
                'f-title-icon': true
            } as Record<string, boolean>;
            classConverter(classObject, props.iconClass);
            return classObject;
        });
        const icon = computed(() => {
            const classObject = {
                'f-icon': true
            } as Record<string, boolean>;
            classConverter(classObject, props.icon);
            return classObject;
        });
        const titleContentClass = computed(() => {
            const classObject = {
                'f-title': true
            } as Record<string, boolean>;
            classConverter(classObject, props.titleContentClass);
            return classObject;
        });
        const contentClass = computed(() => {
            const classObject = {
                'f-content': true
            } as Record<string, boolean>;
            classConverter(classObject, props.contentClass);
            return classObject;
        });
        const downContentClass = computed(() => {
            const classObject = {
                'f-content': true
            } as Record<string, boolean>;

            classConverter(classObject, props.downContentClass);
            return classObject;
        });

        const prePaginationIconClass = computed(() => {
            return {
                'f-icon f-icon-arrow-w': true,
                'f-state-disabled': props.prePaginationDisabled
            } as Record<string, boolean>;
        });
        const nextPaginationIconClass = computed(() => {
            return {
                'f-icon f-icon-arrow-e': true,
                'f-state-disabled': props.nextPaginationDisabled
            } as Record<string, boolean>;
        });

        function onPrePaginationClick(payload: MouseEvent) {
            if (!props.prePaginationDisabled) {
                context.emit('prePaginationClick', payload);
            }
        }
        function onNextPaginationClick(payload: MouseEvent) {
            if (!props.nextPaginationDisabled) {
                context.emit('nextPaginationClick', payload);
            }
        }
        function renderLeftArea() {
            return context.slots.titleContent ?
                <div class={titleContentClass.value}> {context.slots.titleContent()}</div >
                :
                <div class="f-title">
                    {props.showIcon && props.icon ? <span class={iconClass.value}><i class={icon.value}></i></span> : ''}
                    <h4 class="f-title-text">{props.title}</h4>
                    {props.subTitle ? <h5 class="f-title-subtitle">{props.subTitle}</h5> : ''}

                    {props.showPagination ?
                        <div class="f-title-pagination" >
                            <span class={prePaginationIconClass.value} onClick={onPrePaginationClick}></span>
                            <span class={nextPaginationIconClass.value} onClick={onNextPaginationClick}></span>
                        </div>
                        : ''
                    }
                </div>;

        }

        function renderContentArea() {
            return context.slots.content ? < div class={contentClass.value} > {context.slots.content()}</div > : '';
        }

        function renderResponseToolbar() {
            if (props.buttons && props.buttons.length > 0) {
                return <FResponseToolbar customClass={props.buttonClass} items={items.value} onClick={onClickToolbarItem}></FResponseToolbar>;
            }
        }

        function renderDownArea() {
            return context.slots.downContent ? <div class={downContentClass.value}> {context.slots.downContent()}</div> : '';
        }

        watch(() => props.buttons, (newValue) => {
            items.value = newValue;
        }, {
            deep: true
        }
        );

        return () => {
            return (
                <div class={headerClass.value} >
                    <nav class="f-page-header-base">
                        {renderLeftArea()}
                        {renderContentArea()}
                        {renderResponseToolbar()}
                    </nav>
                    {renderDownArea()}
                </div>
            );
        };
    }
});
