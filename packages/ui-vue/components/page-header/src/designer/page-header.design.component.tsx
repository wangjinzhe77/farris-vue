import { computed, defineComponent, inject, onMounted, ref, watch } from 'vue';
import { PageHeaderDesignerProps, pageHeaderDesignerProps } from '../page-header.props';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerRules } from './use-designer-rules';
import { responseToolbarResolver } from '../../../response-toolbar';
import FResponseToolbarDesignComponent from '../../../response-toolbar/src/designer/response-toolbar.design.component';
import FDesignerInnerItem from '../../../designer-canvas/src/components/designer-inner-item.component';

export default defineComponent({
    name: 'FPageHeaderDesign',
    props: pageHeaderDesignerProps,
    emits: ['click'],
    setup(props: PageHeaderDesignerProps, context) {
        // const items = ref(props.buttons);
        const elementRef = ref();
        const designerHostService = inject('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRules(designItemContext.schema, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);
        const toolbarSchema = ref(designItemContext.schema.toolbar || { 'type': 'response-toolbar', 'buttons': [] });
        const responseToolbarPropsRef = ref(responseToolbarResolver(toolbarSchema.value));

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        watch(() => designItemContext.schema.toolbar, () => {
            toolbarSchema.value = designItemContext.schema.toolbar || { 'type': 'response-toolbar', 'buttons': [] };
            responseToolbarPropsRef.value = responseToolbarResolver(toolbarSchema.value);
        }, { deep: true });

        const items = computed(() => {
            return [...responseToolbarPropsRef.value.items];
        });

        function onSelectionChange(schemaType: string, schemaValue: any, componentId: string, componentInstance: any) {
            designItemContext && designItemContext.setupContext &&
                designItemContext.setupContext.emit('selectionChange', schemaType, schemaValue, componentId, componentInstance);
        }

        context.expose(componentInstance.value);
        function classConverter(currentClass: object, customClass: string) {
            if (currentClass && customClass) {
                const customClassArray = customClass.split(' ');
                customClassArray.reduce((result: any, className: any) => {
                    result[className] = true;
                    return result;
                }, currentClass);
            }
        }
        const iconClass = computed(() => {
            const classObject = {
                'f-title-icon': true
            } as Record<string, boolean>;
            classConverter(classObject, props.iconClass);
            return classObject;
        });
        const icon = computed(() => {
            const classObject = {
                'f-icon': true
            } as Record<string, boolean>;
            classConverter(classObject, props.icon);
            return classObject;
        });
        const toolbarDesignerClass = computed(() => {
            const classObject = {
                'text-right': true,
                'col-6': true
            } as Record<string, boolean>;
            classConverter(classObject, props.buttonClass || '');
            return classObject;
        });
        return () => {
            return (
                <div ref={elementRef}>
                    <nav class="f-page-header-base">
                        <div class="f-title">
                            {props.showIcon && props.icon ?
                                <span class={iconClass.value}>
                                    <i class={icon.value}></i>
                                </span>
                                : ''}

                            <h4 class="f-title-text">{props.title}</h4>
                        </div>
                        <FDesignerInnerItem class={toolbarDesignerClass.value} v-model={toolbarSchema.value}
                            canAdd={true}
                            childType="response-toolbar-item"
                            childLabel="按钮"
                            contentKey="buttons"
                            id={toolbarSchema.value.id}
                            componentId={props.componentId}
                            onSelectionChange={onSelectionChange}>
                            <FResponseToolbarDesignComponent customClass={'w-100'} items={items.value} componentId={props.componentId}></FResponseToolbarDesignComponent>
                        </FDesignerInnerItem>
                    </nav>
                </div>
            );
        };
    }
});
