import { DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema } from "../../../designer-canvas/src/types";
import { PageHeaderProperty } from '../property-config/page-header.property-config';

export function useDesignerRules(schema: ComponentSchema, designerHostService): UseDesignerRules {
    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {
        return false;
    }
    function checkCanDeleteComponent() {
        return false;
    }

    function checkCanMoveComponent() {
        return false;
    }
    function hideNestedPaddingInDesginerView() {
        return true;
    }

    // 构造属性配置方法
    function getPropsConfig(componentId: string) {
        const dataGridProp = new PageHeaderProperty(componentId, designerHostService);

        return dataGridProp.getPropertyConfig(schema);
    }
    return {
        canAccepts,
        hideNestedPaddingInDesginerView,
        getPropsConfig,
        checkCanDeleteComponent,
        checkCanMoveComponent
    };
}
