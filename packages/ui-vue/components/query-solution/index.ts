 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App } from 'vue';
import FQuerySolution from './src/query-solution.component';
import FQuerySolutionDesign from './src/designer/query-solution.design.component';
import FQuerySolutionConfig from './src/designer/query-solution-config/query-solution-config.component';
import FSolutionPreset from './src/designer/solution-preset-config/solution-preset.component';
import { propsResolver } from './src/query-solution.props';
import { configPropsResolver } from './src/designer/query-solution-config/query-solution-config.props';
import { solutionPresetPropsResolver } from './src/designer/solution-preset-config/solution-preset.props';

export * from './src/query-solution.props';

export { FQuerySolution };

export default {
    install(app: App): void {
        app.component(FQuerySolution.name as string, FQuerySolution);
        app.component(FQuerySolutionConfig.name as string, FQuerySolutionConfig);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>): void {
        componentMap['query-solution'] = FQuerySolution;
        componentMap['query-solution-config'] = FQuerySolutionConfig;
        componentMap['solution-preset'] = FSolutionPreset;
        propsResolverMap['query-solution'] = propsResolver;
        propsResolverMap['query-solution-config'] = configPropsResolver;
        propsResolverMap['solution-preset'] = solutionPresetPropsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
        componentMap['query-solution'] = FQuerySolutionDesign;
        propsResolverMap['query-solution'] = propsResolver;
    }
};
