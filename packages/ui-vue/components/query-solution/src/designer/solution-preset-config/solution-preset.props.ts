 
import { ExtractPropTypes, PropType } from "vue";
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import { createPropsResolver } from '@farris/ui-vue/components/dynamic-resolver';
import solutionPresetSchema from './schema/query-solution-config.schema.json';
import { ModalOptions } from "@farris/ui-vue/components/modal/src/composition/type";
import { FieldConfig } from "@farris/ui-vue/components/condition";

export const solutionPresetProps = {

    dataSource: {
        type: Array<object>, default: []
    },
    modelValue: { type: Array<FieldConfig>, default: [] },
    onFieldsChanged: { type: Function, default: null}
} as Record<string, any>;

export type SolutionPresetProps = ExtractPropTypes<typeof solutionPresetProps>;

export const solutionPresetPropsResolver = createPropsResolver<SolutionPresetProps>(solutionPresetProps, solutionPresetSchema, schemaMapper, schemaResolver);
