/**
* Copyright (c) 2020 - present, Inspur Genersoft selectItem., Ltd.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

import { defineComponent, onMounted, ref, inject, computed, watch } from 'vue';
import { FButtonEdit } from '@farris/ui-vue/components/button-edit';
import { FTransfer } from '@farris/ui-vue/components/transfer';
import { SolutionPresetProps, solutionPresetProps } from './solution-preset.props';
import { cloneDeep } from 'lodash-es';

export default defineComponent({
    name: 'FSolutionPreset',
    props: solutionPresetProps,
    emits: ['change'] as (string[] & ThisType<void>),
    setup(props: SolutionPresetProps, context) {

        const selections = ref(cloneDeep(props.modelValue));
        const dataSource = ref(cloneDeep(props.dataSource));
        const textValue = computed(() => {
            return '共' + props.modelValue.length + '项';
        });

        watch(()=> props.modelValue, newModelValue => {
            selections.value = cloneDeep(newModelValue);
        });

        watch(()=> props.dataSource, newDataSource => {
            dataSource.value = cloneDeep(newDataSource);
        });

        function onPanelChange(changeItems) {
            selections.value = cloneDeep(changeItems);
        };
        
        function acceptCallback() {
            if(props.onFieldsChanged && typeof props.onFieldsChanged === 'function') {
                props.onFieldsChanged(selections.value);
            }
        };

        function rejectCallback() {
        };

        const modalOptions = {
            fitContent: false,
            width: 900,
            height: 600,
            draggable: true,
            title: '筛选方案配置',
            acceptCallback,
            rejectCallback
        };

        
        return () => (
            <FButtonEdit
                button-behavior={'Modal'}
                modal-options={modalOptions}
                v-model={textValue.value}
                editable={false}
            >
                <div class="f-utils-absolute-all">
                    <FTransfer
                        selections={selections.value}
                        data-source={dataSource.value}
                        class="mx-2"
                        onChange={onPanelChange}
                    ></FTransfer>
                </div>
            </FButtonEdit>
        );
    }
});
