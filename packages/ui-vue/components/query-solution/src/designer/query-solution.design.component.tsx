/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, SetupContext, inject, onMounted, watch } from 'vue';
import { querySolutionProps, QuerySolutionProps } from '../query-solution.props';
import FConditionFieldsDesign from '../../../condition/src/condition-fields.design.component';

import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { useQuerysolutionDesignerRules } from './query-solution-config/composition/use-querysolution-rules';
import { cloneDeep } from 'lodash-es';

export default defineComponent({
    name: 'FQuerySolutionDesign',
    props: querySolutionProps,
    emits: ['save', 'saveAs'] as (string[] & ThisType<void>) | undefined,
    setup(props: QuerySolutionProps, context: SetupContext) {
        const fields = ref(props.fields);
        const conditions = ref([]);
        const elementRef = ref();
        // 记录props修改次数，通过修改组件key值触发重新渲染
        const modifyCount = ref(1);

        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerHostService = inject('designer-host-service');
        const designerRulesComposition = useQuerysolutionDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);


        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        const title = computed(() => props.presetQuerySolutionName || '');
        const filterText = computed(() => props.filterText || '');
        watch(() => props.presetFields, (newPresetFields) => {
            const updatedFields = cloneDeep(newPresetFields);
            updatedFields.forEach((field) => {
                field.editor.type = field.controlType;

            });
            fields.value = updatedFields;
            conditions.value = fields.value.map((field) => {
                return {
                    id: field.id,
                    fieldCode: field.labelCode,
                    fieldName: field.name,
                    value: null
                };
            });
            modifyCount.value = modifyCount.value + 1;
        }, 
        {
            immediate:true
        });


        const shouldShowClearButton = ref(true);

        const querySolutionClass = computed(() => {
            const classObject = {
                'farris-panel': true,
                'position-relative': true,
                'query-solution': true
            } as Record<string, boolean>;
            return classObject;
        });

        const querySolutionStyle = computed(() => {
            const styleObject = {
                border: 'none'
            } as Record<string, any>;
            return styleObject;
        });

        /**
         * 校验组件是否支持移动
         */
        function checkCanMoveComponent(): boolean {
            return true;
        }

        /**
         * 校验组件是否支持选中父级
         */
        function checkCanSelectParentComponent(): boolean {
            return false;
        }

        /**
         * 校验组件是否支持删除
         */
        function checkCanDeleteComponent() {
            return true;
        }

        function renderSolutionToolbar() {
            return (
                <div class="solution-action">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary ">
                            {filterText.value}
                        </button>
                    </div>
                    {shouldShowClearButton.value && (
                        <div class="icon-group ml-2">
                            <span class="icon-group-remove" title="清空">
                                <span class="f-icon f-icon-remove"></span>
                            </span>
                        </div>
                    )}
                </div>
            );
        }

        function renderHeader() {
            return (
                <div class="solution-header">
                    <div class="btn-group mr-3">
                        <div class="solution-header-title">
                            {title.value}
                            <span class="f-icon f-accordion-expand"></span>
                        </div>
                    </div>
                    {renderSolutionToolbar()}
                </div>
            );
        }

        function renderCondition() {
            const ConditionComponent = FConditionFieldsDesign;
            return <ConditionComponent fields={fields.value} conditions={conditions.value} key={modifyCount.value} isControlInline={props.isControlInline}></ConditionComponent>;
        }

        return () => (
            <div ref={elementRef} class={querySolutionClass.value} tabindex="1" style={querySolutionStyle.value}>
                {renderHeader()}
                {renderCondition()}
            </div>
        );
    }
});
