/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ControlType, PropertyItem, UsePanel } from './types';
import { ref, Ref, SetupContext, watch } from 'vue';
import { QuerySolutionConfigProps, Source } from '../query-solution-config.props';
import FPropertyPanel from '../../../../../property-panel/src/property-panel.component';
import { useProperty } from './use-property';


export function usePanel(props: QuerySolutionConfigProps, context: SetupContext, activePanel: Ref<any>, fieldsData: Map<string, PropertyItem>): UsePanel {

    // 属性面板组件实例
    const PropertyPanelInstance = ref();

    const propertyName = ref('query-solution-config');
    // focusingSchema， propertyConfig暂时无意义
    const focusingSchema = ref({});
    const propertyConfig = ref();
    // 当前选中的筛选项双向绑定的值
    const currentPanelValue = ref<any>(null);
    const { getPropertyConfig } = useProperty(props, context);

    watch(() => activePanel.value?.id, (newId, oldId) => {

        if (!newId) {
            currentPanelValue.value = null;
        } else if (newId !== oldId) {
            const currentField = fieldsData.get(newId);
            currentPanelValue.value = currentField;
        }

    });

    function getBasicSchemaProerty(panel: any) {
        return {
            id: panel.id,
            code: panel.code || panel.id,
            labelCode: panel.labelCode,
            name: panel.name,
            controlType: panel.controlType,
        };

    }

    function getQuerySolutionExtendSchemaProerty(controlType: ControlType, options?) {
        const basicSchema = {
            class: '',
            required: false
        };
        let resultSchema = {};
        switch (controlType) {
            case 'input-group':
                break;
            case 'number-range':
            case 'number-spinner':
                resultSchema = {
                    precision: 2,
                    max: null,
                    min: null
                };
                break;
            case 'combo-list':
                resultSchema = {
                    uri: '',
                    multiSelect: false,
                    data: options?.data || [],
                    valueField: 'value',
                    textField: 'name',
                    idField: 'value',
                    enumValueType: options?.enumValueType || 'string'
                };
                break;
            case 'lookup':
                resultSchema = {
                    helpId: '',
                    uri: '',
                    textField: '',
                    valueField: '',
                    idField: '',
                    mapFields: '',
                    multiSelect: false,
                    panelHeight: null,
                };
                break;
            case 'combo-lookup':
                resultSchema = {
                    helpId: '',
                    uri: '',
                    textField: '',
                    valueField: '',
                    idField: '',
                    mapFields: '',
                    multiSelect: false,
                    editable: false,
                    nosearch: false,
                    clearFields: '',
                    dialogTitle: '',
                    enableMultiFieldSearch: false
                };
                break;
            case 'date-range':
            case 'date-picker':
                resultSchema = {
                    valueFormat: 'yyyy-MM-dd',
                    displayFormat: 'yyyy-MM-dd',
                };
                break;
            case 'radio-group':
                resultSchema = {
                    direction: 'horizontal',
                    showLabel: false,
                    data: options?.data || []
                };
                break;
        };
        return { ...basicSchema, ...resultSchema };
    }

    function getFilterBarionExtendSchemaProerty(controlType: ControlType) {
        const basicSchema = {
            class: '',
            required: false,
            isExtend: false
        };
        let resultSchema = {};
        switch (controlType) {
            case 'input-group':
                break;
            case 'number-range':
            case 'number-spinner':
                resultSchema = {
                    precision: 2,
                    max: null,
                    min: null
                };
                break;
            case 'combo-list':
                resultSchema = {
                    uri: '',
                    enumValue: [],
                    multiSelect: false
                };
                break;
            case 'lookup':
                resultSchema = {
                    helpId: '',
                    uri: '',
                    textField: '',
                    valueField: '',
                    idField: '',
                    mapFields: '',
                    multiSelect: false,
                    panelHeight: null,
                };
                break;
            case 'combo-lookup':
                resultSchema = {
                    helpId: '',
                    uri: '',
                    textField: '',
                    valueField: '',
                    idField: '',
                    mapFields: '',
                    multiSelect: false,
                    editable: false,
                    nosearch: false,
                    clearFields: '',
                    dialogTitle: '',
                    enableMultiFieldSearch: false
                };
                break;
            case 'date-range':
            case 'date-picker':
                resultSchema = {
                    valueFormat: 'yyyy-MM-dd',
                    displayFormat: 'yyyy-MM-dd',
                };
                break;
            case 'radio-group':
                resultSchema = {
                    direction: 'horizontal',
                    showLabel: false,
                };
                break;
        };
        return { ...basicSchema, ...resultSchema };
    }

    function getExtendSchemaProerty(controlType: ControlType,  options?) {
        const sourceType: Source = props.source;
        if (sourceType === 'query-solution') {
            return getQuerySolutionExtendSchemaProerty(controlType, options);
        }
        return getFilterBarionExtendSchemaProerty(controlType);

    }

    watch(() => [currentPanelValue.value?.id, currentPanelValue.value?.controlType], ([newId, newControlType], [oldId, oldControlType]) => {
        if (!newId) {
            PropertyPanelInstance.value.updatePropertyConfig({}, {});
        } else if (newId !== oldId) {
            const newPropertyConfig = getPropertyConfig(currentPanelValue.value);
            PropertyPanelInstance.value.updatePropertyConfig(newPropertyConfig, currentPanelValue.value);
        } else if (newControlType !== oldControlType) {
            const currentField = fieldsData.get(newId);
            const basicSchema = getBasicSchemaProerty(currentField);
            basicSchema.controlType = newControlType;
            const newField = { ...basicSchema, ...getExtendSchemaProerty(newControlType, currentField) };
            fieldsData.set(newId, newField);
            currentPanelValue.value = fieldsData.get(newId);
            const newPropertyConfig = getPropertyConfig(currentPanelValue.value);
            PropertyPanelInstance.value.updatePropertyConfig(newPropertyConfig, currentPanelValue.value);
        }
    });

    /**
     * FPropertyPanel发生变更 增加了关于designer.Item的相关处理,此处需要后续处理
     * @returns 
     */
    function renderPanel() {
        return <FPropertyPanel propertyConfig={propertyConfig.value} ref={PropertyPanelInstance}
            propertyName={propertyName.value}
            schema={focusingSchema.value}> </FPropertyPanel>;
    }
    return {
        renderPanel,
        getExtendSchemaProerty
    };
}
