/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { PropertyItem, UseProperty } from './types';
import { SetupContext } from 'vue';
import { QuerySolutionConfigProps } from '../query-solution-config.props';
import querySolutionConfigExtend from "../property-config/query-solution-config-extend.json";
import { LookupSchemaRepositoryToken } from '../../../../../lookup/src/property-config/lookup.property-config';
import { FieldSelectorRepositoryToken } from '../../../../../field-selector/index';
import { queryEnumDefaultConverter } from '../converter/query-solution-config-property.converter';

export function useProperty(
    props: QuerySolutionConfigProps,
    context: SetupContext
): UseProperty {
    const CONTROL_TYPE_LIST = {
        'input-group': {
            key: "input-group",
            value: "文本"
        },
        "number-spinner": {
            key: "number-spinner",
            value: "数值"
        },
        "number-range": {
            key: "number-range",
            value: "数值区间"
        },
        "lookup": {
            key: "lookup",
            value: "弹出帮助"
        },
        "combo-lookup": {
            key: "combo-lookup",
            value: "下拉帮助"
        },
        "date-picker": {
            key: "date-picker",
            value: "日期"
        },
        "combo-list": {
            key: "combo-list",
            value: "下拉面板"
        },
        "radio-group": {
            key: "radio-group",
            value: "单选"
        }
    };
    function getControlTypeListByField(propertyData) {
        const fieldConfig = props.fieldsConfig.find(field => field.id === propertyData.id);
        let ControlTypeList:Array<{key, value}> = [];
        if(fieldConfig) {
            switch(fieldConfig.type.name) {
                case 'Enum': case 'Boolean':
                    ControlTypeList = [CONTROL_TYPE_LIST['radio-group'], CONTROL_TYPE_LIST['combo-list']];
                    break;
                case 'Number': case 'Integer': case 'Decimal':case 'BigNumber':
                    ControlTypeList = [CONTROL_TYPE_LIST['number-spinner'], CONTROL_TYPE_LIST['number-range']];
                    break;
                case 'String':
                    ControlTypeList = [CONTROL_TYPE_LIST['input-group'], CONTROL_TYPE_LIST['lookup'], CONTROL_TYPE_LIST['date-picker'], CONTROL_TYPE_LIST['radio-group'], CONTROL_TYPE_LIST['combo-list']];
                    break;
                case 'Date': case 'DateTime':
                    ControlTypeList = [CONTROL_TYPE_LIST['date-picker']];
                    break;
            }
            return  ControlTypeList;     
        } else {
            return [{
                key: "input-group",
                value: "文本"
            }];
        }

    }
    function getBasicConfig(propertyData) {
        return {
            description: "Basic Infomation",
            title: "基本信息",
            properties: {
                id: {
                    description: "标识",
                    title: "标识",
                    type: "string",
                    readonly: true
                },
                code: {
                    description: "编号",
                    title: "控件编号",
                    type: "string",
                    readonly: true
                },
                labelCode: {
                    description: "标签",
                    title: "标签",
                    type: "string",
                    readonly: true
                },
                name: {
                    description: "控件名称",
                    title: "控件名称",
                    type: "string"
                },
                controlType: {
                    description: "控件类型",
                    title: "控件类型",
                    type: "string",
                    editor: {
                        type: "combo-list",
                        data: getControlTypeListByField(propertyData),
                        valueField: "key",
                        textField: "value",
                        idField: "id",
                        editable: false,
                        multiSelect: false
                    }
                }
            }
        };
    }

    function generateSourceUri(ctrlId: string) {
        let code = 'form_group_' + Date.now();
        if (ctrlId) {
            code = ctrlId.replaceAll('-', '_').replaceAll('.', '_');
        }
        return 'lookup.' + code;
    }
    function getHelpControlConfig(propertyData: any) {
        const helpId = {
            description: "帮助元数据",
            title: "帮助元数据",
            type: "string",
            refreshPanelAfterChanged: true,
            editor: {
                type: "schema-selector",
                title: "选择数据源",
                editorParams: {
                    propertyData,
                    formBasicInfo: props.formSchemaUtils.getFormMetadataBasicInfo()
                },
                viewOptions: [
                    {
                        id: 'recommend', title: '推荐', type: 'List',
                        dataSource: 'Recommand',
                        enableGroup: true,
                        groupField: 'category',
                        groupFormatter: (value, data) => {
                            return `${value === 'local' ? '本地元数据' : '最近使用'}`;
                        }
                    },
                    { id: 'total', title: '全部', type: 'List', dataSource: 'Total' }
                ],
                repositoryToken: LookupSchemaRepositoryToken,
                onSubmitModal: (dataSourceSchema: any) => {
                    if (dataSourceSchema) {
                        const formInfo = props.formSchemaUtils.getFormMetadataBasicInfo();
                        // 获取数据源详细配置信息
                        return props.metadataService.getPickMetadata(formInfo.relativePath, dataSourceSchema[0].data)
                            .then((res: any) => {
                                const metadata = JSON.parse(res?.metadata.content);
                                propertyData.helpId = metadata.id;
                                propertyData.displayName = metadata.name;
                                propertyData.displayType = metadata.displayType;
                                propertyData.textField = metadata.textField;
                                propertyData.valueField = metadata.valueField;
                                propertyData.idField = metadata.idField;
                                propertyData.uri = generateSourceUri(metadata.id);
                                propertyData.dataSource = {
                                    displayName: metadata.name,
                                    idField: metadata.idField,
                                    uri: propertyData.uri
                                };
                                return null;
                            });
                    }
                }
            },
        };

        const textField = {
            description: "显示文本字段",
            title: "文本字段",
            type: "string",
            editor: {
                type: "field-selector",
                bindingType: { "enable": false },
                textField: 'bindingPath',
                editorParams: {
                    propertyData,
                    formBasicInfo: props.formSchemaUtils.getFormMetadataBasicInfo()
                },
                columns: [
                    { field: 'name', title: '名称' },
                    { field: 'code', title: '编号' },
                    { field: 'bindingPath', title: '绑定字段' },
                ],
                repositoryToken: FieldSelectorRepositoryToken,
                onSubmitModal: (dataSourceSchema: any) => {
                    propertyData.textField = dataSourceSchema[0].bindingPath;
                    return null;
                }
            }
        };
        const valueField = {
            description: "显示值字段",
            title: "值字段",
            type: "string",
            editor: {
                type: "field-selector",
                bindingType: { "enable": false },
                textField: 'bindingPath',
                editorParams: {
                    propertyData,
                    formBasicInfo: props.formSchemaUtils.getFormMetadataBasicInfo()
                },
                columns: [
                    { field: 'name', title: '名称' },
                    { field: 'code', title: '编号' },
                    { field: 'bindingPath', title: '绑定字段' },
                ],
                repositoryToken: FieldSelectorRepositoryToken,
                onSubmitModal: (dataSourceSchema: any) => {
                    propertyData.valueField = dataSourceSchema[0].bindingPath;
                    return null;
                }
            }
        };
        const mappingFields = {
            description: "字段映射",
            title: "字段映射",
            type: "string",
            // $converter: lookupDefaultConverter,
            editor: {
                type: "mapping-editor",
                modalWidth: 800,
                modalHeight: 600,
                editable: false,
                editorParams: {
                    propertyData,
                    formBasicInfo: props.formSchemaUtils.getFormMetadataBasicInfo()
                },
                fromData: {
                    editable: false,
                    formatter: (cell, data) => {
                        return `${data.raw[cell.field]} [${data.raw['bindingPath']}]`;
                    },
                    idField: 'id',
                    textField: 'bindingPath',
                    valueField: 'bindingPath',
                    repositoryToken: FieldSelectorRepositoryToken
                },
                toData: {
                    idField: 'id',
                    textField: 'bindingPath',
                    valueField: 'bindingPath',
                    dataSource: props.designViewModelUtils.getAllFields2TreeByVMId(props.viewModelId),
                    formatter: (cell, data) => {
                        return `${data.raw[cell.field]} [${data.raw['bindingPath']}]`;
                    }
                }
            }
        };
        return {helpId, textField, valueField, mappingFields};
    }

    function getEnumControlConfig(propertyData: any) {
        const enumValues = {
            description: "枚举值",
            title: "枚举值",
            type: "string",
            editor: {
                columns: [
                    { field: 'value', title: '值', dataType: 'string' },
                    { field: 'name', title: '名称', dataType: 'string' },
                    { field: 'disabled', title: '禁用', visible: false, dataType: 'boolean', editor: { type: 'switch' } },
                ],
                type: "item-collection-editor",
                valueField: 'value',
                nameField: 'name',
                requiredFields: ['value','name'],
                uniqueFields: ['value', 'name'],
            },
            $converter: queryEnumDefaultConverter
        };
        
        return {enumValues};
    }
    function getControlConfig(propertyData: any) {
        const { controlType } = propertyData;
        const baseControlConfig =  {
            description: "",
            title: "控件",
            properties: querySolutionConfigExtend[controlType]
        };
        if(controlType === "lookup" || controlType === "combo-lookup") {
            const {helpId, textField, valueField, mappingFields} = getHelpControlConfig(propertyData);
            baseControlConfig.properties.helpId = helpId;
            baseControlConfig.properties.textField = textField;
            baseControlConfig.properties.valueField = valueField;
            baseControlConfig.properties.mapFields = mappingFields;
        } else if(controlType === "radio-group" || controlType === "combo-list") {
            const {enumValues} = getEnumControlConfig(propertyData);
            baseControlConfig.properties.data = enumValues;
        } else if(controlType === "date-picker") {
            const fieldConfig = props.fieldsConfig.find(fieldConfig => fieldConfig.id === propertyData.id);
            if(fieldConfig && (fieldConfig.type.name === 'Date' || fieldConfig.type.name === 'DateTime')) {
                const { valueFormat, ...dateProperties} =  baseControlConfig.properties;
                baseControlConfig.properties = dateProperties;
            }

        }
        return baseControlConfig;
    }

    function getEventsByControlType(controlType) {
        const eventList = [
            {
                label: "valueChangedCmd",
                name: "值变化事件"
            }
        ];
        switch (controlType) {
            case "combo-lookup":
            case "lookup":
                eventList.push({
                    label: "preEventCmd",
                    name: "帮助前事件"
                },{
                    label: "preEventCmd",
                    name: "帮助前事件" 
                });
                break;
            case "combo-list":
                eventList.push({
                    label: "beforeShow",
                    name: "面板展开前事件"
                },{
                    label: "afterShow",
                    name: "面板关闭后事件" 
                });
                break;
        }
        return eventList;
    }
    
    function getEventPropConfig(propertyData: any) {
        const events = getEventsByControlType(propertyData.controlType);
        const initialData = props.eventsEditorUtils['formProperties'](propertyData, props.viewModelId, events);
        const properties = {};
        properties[props.viewModelId] = {
            type: 'events-editor',
            editor: {
                initialData
            }
        };
        const eventsEditor = {
            title: '事件',
            hideTitle: true,
            properties,
            // 这个属性，标记当属性变更得时候触发重新更新属性
            refreshPanelAfterChanged: true,
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject: any, data: any) {
                const parameters = changeObject.propertyValue;
                delete propertyData[props.viewModelId];
                if (parameters) {
                    parameters.setPropertyRelates = this.setPropertyRelates; // 添加自定义方法后，调用此回调方法，用于处理联动属性
                    props.eventsEditorUtils.saveRelatedParameters(propertyData, props.viewModelId, parameters['events'], parameters);
                }
                // 联动修改排序开关
                propertyData.remoteSort = propertyData.columnSorted ? true : false;
            }
        };
        return eventsEditor;
    }
     
    function getPropertyConfig(propertyData: PropertyItem) {
        const basic = getBasicConfig(propertyData);
        const control = getControlConfig(propertyData);
        const eventsEditor = getEventPropConfig(propertyData);


        return {
            title: "query-solution-config",
            description: "A Farris Component",
            type: 'object',
            categories: {
                basic,
                control,
                // eventsEditor
            }
        };
    }

    return {
        getPropertyConfig
    };
}

