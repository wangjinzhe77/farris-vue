/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Ref } from "vue";
import { TransFerItem } from '../query-solution-config.props';
import { JSX } from "vue/jsx-runtime";

export type ControlType = 'number-spinner' | 'number-range' | 'combo-list' | 'lookup' | 'combo-lookup' | 'date-range' | 'date-picker' 
 | 'radio-group' | 'input-group';

export interface UseTransfer {

    selectPanels: Ref<TransFerItem[]>;
    /**
     * 切换单选按钮事件
     */
    renderTransfer: () => JSX.Element | null;
    initData: () => void;
    confirm: () => void;
    cancel: () => void;
}

export interface PropertyItem {
    id: string;
    code: string;
    labelCode: string;
    name: string;
    isExtend?: string;
    controlType: ControlType;
    class?: string;
    required?: boolean;
    precision?: number;
    max?: number;
    min?: number;
    uri?: string;
    displayName?: string;
    multiSelect?: boolean;
    enumValue?: string;
    helpId?: string;
    textField?: string;
    valueField?: string;
    idField?: string;
    mapFields?: string;
    editable?: boolean;
    nosearch?: boolean;
    displayFields?: string;
    clearFields?: string;
    dialogTitle?: string;
    enableMultiFieldSearch?: boolean;
    returnForma?: string;
    format?: string;
    direction?: string;
    showLabel?: boolean;
    data?: Array<any>;
    dataSource?: {displayName:string, idField:string, uri:string};
    [prop: string]: any;
}

export interface PropertyConfig {
    description: string;
    title: string;
    type: string;
    readonly?: boolean;
    categories: any;
}

export interface UsePanel {
    renderPanel: () => JSX.Element | null;
    getExtendSchemaProerty: (controlType: ControlType, schemaField) => Partial<PropertyItem>;
}

export interface UseProperty {
    getPropertyConfig: (propertyData: PropertyItem) => PropertyConfig;
}
