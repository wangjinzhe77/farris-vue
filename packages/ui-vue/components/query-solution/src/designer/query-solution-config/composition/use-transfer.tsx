/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { PropertyItem, UseTransfer } from './types';
import { ref, Ref, SetupContext } from 'vue';
import { FieldConfig } from '../../../../../condition/src/types';
import { QuerySolutionConfigProps } from '../query-solution-config.props';
import FTransfer from '../../../../../transfer/src/transfer.component';
import { cloneDeep } from 'lodash-es';

export function useTransfer(
    props: QuerySolutionConfigProps,
    context: SetupContext,
    activePanel: Ref<PropertyItem | null>,
    fieldsData: Map<string, PropertyItem>,
    usePanelComposition: any
): UseTransfer {

    const finishInit = ref(false);
    const firstInitData = ref();
    const firstSelectPanels = ref<PropertyItem[]>([]);
    const selectPanels = ref<PropertyItem[]>([]);
    const dataSource = ref<any[]>([]);
    const { getExtendSchemaProerty } = usePanelComposition;

    function onActiveChange(transFerItem: PropertyItem[]) {
        activePanel.value = transFerItem.length ? transFerItem[0] : null;
    }

    function onPanelChange(changeItems: PropertyItem[]) {
        selectPanels.value = changeItems;
    }

    function getEditorByType(type, options?: any) {
        const editor = getExtendSchemaProerty(type, options);
        return editor;
    }

    function getSolutionField(schemaField: any) {
        if (schemaField.$type !== 'SimpleField') {
            // console.warn('字段类型错误！');
            return;
        }
        let editorType = '';
        let editor = {} as any;
        switch (schemaField.type.name) {
            case 'String': {
                editorType = 'input-group';
                editor = getEditorByType('input-group');
                break;
            }
            case 'Number': case 'Integer': case 'Decimal':
            case 'BigNumber': {
                editorType = 'number-range';
                editor = getEditorByType('number-spinner');
                break;
            }
            case 'Date': case 'DateTime': {
                editorType = 'date-picker';
                editor = getEditorByType('date-picker');
                break;
            }
            case 'Enum':{
                editorType = 'combo-list';
                const options = {
                    data: schemaField.type.enumValues,
                    enumValueType: 'string'
                };
                editor = getEditorByType('combo-list', options);
                break; 
            }
            case 'Boolean': {
                editorType = 'combo-list';
                const options = {
                    data: schemaField.type.enumValues || [{name:'true', value: true}, {name: 'false', value: false}],
                    enumValueType: 'boolean'
                };
                editor = getEditorByType('combo-list', options);
                break;
            }
            default: {
                // type = 'Text'时目前不支持，暂时按照text处理，后续修改
                editorType = 'input-group';
                editor = getEditorByType('input-group');
            }
        }

        // 不支持的字段类型
        if (!editor) {
            return;
        }
        const fieldConfig: any = {
            id: schemaField.id,
            labelCode: schemaField.path,
            code: schemaField.code,
            name: schemaField.name,
            placeHolder: '',
            controlType: editorType,
            ...editor
        };

        fieldsData.set(fieldConfig.id, cloneDeep(fieldConfig));
        return fieldConfig;
    }

    function convertToFieldConfig() {
        const fields = cloneDeep(props.fieldsConfig);
        const fieldsConfig = fields.map(field => {
            return getSolutionField(field);
        });
        return fieldsConfig;

    }

    function getDataSource() {
        return convertToFieldConfig();
    }

    function initData() {
        dataSource.value = getDataSource();
        const data: any = {};
        const existFields = cloneDeep(props.modelValue) || [];
        selectPanels.value = [];
        existFields.forEach((field: FieldConfig) => {
            const currentField = dataSource.value.find((fieldData: FieldConfig) => fieldData.id === field.id);
            if (currentField) {
                data[field.id] = {
                    ...field.editor,
                    id: currentField.id,
                    code: currentField.code,
                    labelCode: currentField.labelCode,
                    name: field.name,
                    controlType: field.controlType,

                };
                delete data[field.id].type;
                selectPanels.value.push(data[field.id]);
                fieldsData.set(field.id, cloneDeep(data[field.id]));
            }
        });
        finishInit.value = true;
        firstInitData.value = cloneDeep(data);
        firstSelectPanels.value = cloneDeep(selectPanels.value);
    }


    function confirm() {

        const selectPanelsData: FieldConfig[] = [];
        selectPanels.value.forEach((panel: PropertyItem) => {
            const currentField = fieldsData.get(panel.id);
            if (currentField) {
                const { id, code, labelCode, name, controlType, ...editor } = currentField;
                const selectPanelData = {
                    id,
                    code,
                    labelCode,
                    name,
                    controlType,
                    editor
                } as FieldConfig;
                selectPanelData.editor.type = controlType;
                selectPanelsData.push(selectPanelData);
            }
        });
        context.emit('update:modelValue', selectPanelsData);
        if(props.onFieldsChanged && typeof props.onFieldsChanged === 'function') {
            props.onFieldsChanged(selectPanelsData);
        }
        finishInit.value = false;
        activePanel.value = null;
    }

    function cancel() {
        selectPanels.value = cloneDeep(firstSelectPanels.value);
        finishInit.value = false;
        activePanel.value = null;
    }

    function renderTransfer() {
        return finishInit.value ? <FTransfer
            selections={selectPanels.value}
            data-source={dataSource.value}
            class="query-solution-config-transfer"
            onActiveChange={onActiveChange}
            onChange={onPanelChange}
        ></FTransfer> : null;
    }
    return {
        renderTransfer,
        initData,
        selectPanels,
        confirm,
        cancel
    };
}
