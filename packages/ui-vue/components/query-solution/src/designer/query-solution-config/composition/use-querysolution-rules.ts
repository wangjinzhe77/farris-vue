import { UseDesignerRules } from "../../../../../designer-canvas/src/composition/types";
import { ComponentSchema, DesignerComponentInstance, DesignerItemContext } from "../../../../../designer-canvas/src/types";
import { QuerySolutionPropertyConfig } from "../../../property-config/query-solution.property-config";;

export function useQuerysolutionDesignerRules(designItemContext: DesignerItemContext, designerHostService): UseDesignerRules {

    // 构造属性配置方法
    function getPropsConfig(componentId: string, componentInstance: DesignerComponentInstance) {
        const schema = designItemContext.schema as ComponentSchema;
        const querySolutionProps = new QuerySolutionPropertyConfig(componentId, designerHostService);
        return querySolutionProps.getPropertyConfig(schema, componentInstance);
    }

    function canAccepts() {
        return false;
    }
    function checkCanDeleteComponent() {
        return false;
    }
    function checkCanMoveComponent() {
        return false;
    }
    /**
     * 组件删除后事件
     */
    function onRemoveComponent() {
        // 1、移除根节点对应的样式
        if (designItemContext?.parent?.parent?.schema) {
            const sectionParentSchema = designItemContext?.parent?.parent.schema;
            if (sectionParentSchema.appearance?.class && sectionParentSchema.appearance?.class.includes('f-page-has-scheme')) {
                sectionParentSchema.appearance.class = sectionParentSchema.appearance?.class.replace('f-page-has-scheme', '').replace('  ', '');
            }
        }

        // 2、移除筛选方案相关的变量
        const { formSchemaUtils } = designerHostService;
        const targetComponentId = designItemContext.componentInstance.value.belongedComponentId;
        const viewModelId = formSchemaUtils.getViewModelIdByComponentId(targetComponentId);
        const viewModel = formSchemaUtils.getViewModelById(viewModelId);
        if (viewModel && viewModel.states) {
            const filterVariableIndex = viewModel.states.findIndex(state => state.code === 'originalFilterConditionList');
            if (filterVariableIndex > -1) {
                viewModel.states.splice(filterVariableIndex, 1);
            }
        }
    }

    return {
        getPropsConfig,
        canAccepts,
        checkCanDeleteComponent,
        checkCanMoveComponent,
        onRemoveComponent
    } as UseDesignerRules;

}
