/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, SetupContext, onMounted } from 'vue';
import { querySolutionConfigProps, QuerySolutionConfigProps } from './query-solution-config.props';
import { usePanel } from './composition/use-panel';
import { useTransfer } from './composition/use-transfer';
import FButtonEdit from '../../../../button-edit/src/button-edit.component';

import './query-solution-config.css';
import { PropertyItem } from './composition/types';

export default defineComponent({
    name: 'FQuerySolutionConfig',
    props: querySolutionConfigProps,
    emits: ['update:modelValue'] as (string[] & ThisType<void>) | undefined,
    setup(props: QuerySolutionConfigProps, context: SetupContext) {
        const activePanel = ref<PropertyItem | null>(null);
        const textValue = computed(() => {
            return '共' + props.modelValue.length + '项';
        });
        // fieldsData 为打开弹窗后所用的数据，包含所有字段，点击确定时针对穿梭框selectPanels进行赋值操作
        const fieldsData = new Map();
        const usePanelComposition = usePanel(props, context, activePanel, fieldsData);
        const { renderPanel } = usePanelComposition;
        const { renderTransfer, initData, confirm, cancel } = useTransfer(props, context, activePanel, fieldsData, usePanelComposition);

        function acceptCallback() {
            confirm();
        }

        function rejectCallback() {
            cancel();
        }

        const modalOptions = {
            fitContent: false,
            width: 900,
            height: 600,
            draggable: true,
            title: '筛选方案配置',
            acceptCallback,
            rejectCallback
        };

        function renderQuerySolutionConfig() {
            return <>
                {renderTransfer()}
                {renderPanel()}
            </>;
        }

        return () => (

            <FButtonEdit
                id={props.id}
                button-behavior={'Modal'}
                modal-options={modalOptions}
                v-model={textValue.value}
                onClickButton={() => initData()}
            >
                <div class="query-solution-config d-flex f-utils-absolute-all">
                    {renderQuerySolutionConfig()}
                </div>
            </FButtonEdit>
        );
    }
});
