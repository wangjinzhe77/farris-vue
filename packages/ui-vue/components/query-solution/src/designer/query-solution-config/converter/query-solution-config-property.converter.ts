export const queryEnumDefaultConverter = {
    convertFrom: (schema: Record<string, any>, propertyKey: string) => {
        return schema[propertyKey];
    },
    convertTo: (schema: Record<string, any>, propertyKey: string, propertyValue: any) => {
        schema[propertyKey] = propertyValue.value;
    }
};

