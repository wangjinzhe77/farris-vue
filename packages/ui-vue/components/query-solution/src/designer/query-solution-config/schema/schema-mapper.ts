import { MapperFunction, resolveAppearance } from '../../../../../dynamic-resolver';
// import {resolvePreset} from './preset-resolver';

export const schemaMapper = new Map<string, string | MapperFunction>([
    ['appearance', resolveAppearance],
    // ['preset', resolvePreset]
]);
