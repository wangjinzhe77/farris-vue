/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, SetupContext, onMounted } from 'vue';
import { solutionManagerProps, SolutionManagerProps } from './solution-manager.props';
import { FDataGrid } from '@farris/ui-vue/components/data-grid';

export default defineComponent({
    name: 'FSolutionManager',
    props: solutionManagerProps,
    emits: ['changeDelete'] as (string[] & ThisType<void>) | undefined,
    setup(props: SolutionManagerProps, context: SetupContext) {
        const columns = [
            { field: 'code', title: '名称', width: 80, resizable: true, dataType: 'string' },
            {
                field: 'type', title: '类型', width: 200, dataType: 'string',
                editor: {
                    type: 'combo-list',
                    data: [
                        { id: 'private', name: '用户个人方案' }
                        // { id: 'public', name: '系统公共方案' },
                        // { id: 'org', name: '组织公共方案' }
                    ]
                }
            },
        ];
        const querysolutionTypeRadios = {
            private: '用户个人方案',
            public: '系统公共方案',
            org: '组织公共方案'
        };
        const deleteSolutionIds = ref<string[]>([]);
        const showSolutions = ref([]);
        const dataGridRef = ref();

        function handleShowSolutions() {
            const data = props.solutions.filter(solution => {
                return !deleteSolutionIds.value.includes(solution.id) && !solution.isSystem;
            }).map(solution => {
                return {
                    id: solution.id,
                    code: solution.code,
                    type: querysolutionTypeRadios[solution.type],
                };
            });
            showSolutions.value = data;
            dataGridRef.value.updateDataSource(data);
        }

        onMounted(() => {
            handleShowSolutions();
        });

        function deleteSolution(rawData) {
            const solutionId = rawData.raw.id;
            if (!deleteSolutionIds.value.includes(solutionId)) {
                deleteSolutionIds.value = [...deleteSolutionIds.value, solutionId];
                context.emit('changeDelete', deleteSolutionIds.value);
                handleShowSolutions();
            }

        }
        function renderSolutionManager() {
            const commandOption = {
                enable: true,
                commands: [
                    {
                        text: '删除',
                        type: 'danger',
                        command: 'remove',
                        onClick(payload: MouseEvent, dataIndex: number, visualDataRow) {
                            deleteSolution(visualDataRow);
                        }
                    }
                ]
            };
            return <FDataGrid ref={dataGridRef} columns={columns} data={showSolutions.value} commandOption={commandOption} columnOption={{fitColumns:true}}></FDataGrid>;
        }

        return () => (
            <div class="ml-3 mr-3 w-100 h-100">
                {renderSolutionManager()}
            </div>
        );
    }
});
