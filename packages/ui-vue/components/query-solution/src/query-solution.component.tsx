/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, SetupContext, onMounted } from 'vue';
import { querySolutionProps, QuerySolutionProps } from './query-solution.props';
import { QuerySolution } from './query-solution';
import FConditionFields from '../../condition/src/condition-fields.component';
import FConditionList from '../../condition/src/condition-list.component';
import { cloneDeep } from 'lodash-es';
import { Condition } from '../../condition';
import { useSolution } from './composition/use-solution';
import { useHeader } from './composition/use-header';
import { useHttp } from './composition/use-http';

export default defineComponent({
    name: 'FQuerySolution',
    props: querySolutionProps,
    emits: ['save', 'saveAs', 'delete', 'change', 'conditionChange', 'query'] as (string[] & ThisType<void>) | undefined,
    setup(props: QuerySolutionProps, context: SetupContext) {
        const fields = ref();
        const isControlInline = ref(props.isControlInline);
        const solutions = ref<QuerySolution[]>([]);
        const currentSolution = ref<QuerySolution | null>(null);

        const useHttpComposition = useHttp(props, context);
        const useSolutionComposition = useSolution(props, context, useHttpComposition, solutions, currentSolution, fields);
        const useHeaderComposition = useHeader(props, context, useHttpComposition, useSolutionComposition, solutions, currentSolution, fields);

        const { loadAllSolution, handleQuery } = useSolutionComposition;
        const { renderHeader, loadFields, expanded } = useHeaderComposition;

        onMounted(() => { 
            loadFields();
            loadAllSolution();
        });

        const currentConditionMode = computed(() => currentSolution.value ? currentSolution.value.mode : '1');

        const conditionTypeMap: Record<string, any> = {
            '1': FConditionFields,
            '2': FConditionList
        };

        const querySolutionClass = computed(() => {
            const classObject = {
                'farris-panel': true,
                'position-relative': true,
                'query-solution': true
            } as Record<string, boolean>;
            return classObject;
        });

        const querySolutionStyle = computed(() => {
            const styleObject = {
                border: 'none'
            } as Record<string, any>;
            return styleObject;
        });

        function onConditionChange(e: any, condiion: Condition) {
            context.emit('change', e, condiion);
            handleQuery();
        }
        function onLabelCodeChange(condiion: Condition) {
            context.emit('change', condiion);
            context.emit('conditionChange', cloneDeep(currentSolution.value?.conditions));
        }
        function onCompareTypeChange(e: any, condiion: Condition) {
            context.emit('change', e, condiion);
            context.emit('conditionChange', cloneDeep(currentSolution.value?.conditions));
        }


        function renderCondition() {
            const ConditionComponent = conditionTypeMap[currentConditionMode.value] || FConditionFields;
            return currentSolution.value && <ConditionComponent
                key={currentSolution.value.id}
                fields={fields.value}
                conditions={currentSolution.value.conditions}
                isControlInline={isControlInline.value}
                onValueChange={(e: any, condiion: Condition) => onConditionChange(e, condiion)}
                onLabelCodeChange={(condiion: Condition) => onLabelCodeChange(condiion)}
                onCompareTypeChange={(e: any, condiion: Condition) => onCompareTypeChange(e, condiion)}
            ></ConditionComponent>;
        }

        return () => (
            <div class={querySolutionClass.value} tabindex="1" style={querySolutionStyle.value}>
                {renderHeader()}
                {expanded.value && renderCondition()}
            </div>
        );
    }
});
