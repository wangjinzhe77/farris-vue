/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { UseHttp, UseSolution } from './types';
import { Ref, SetupContext } from 'vue';
import { QuerySolutionProps } from '../query-solution.props';
import { QuerySolution } from '../query-solution';
import { Condition, FieldConfig } from '../../../condition';
import { cloneDeep } from 'lodash-es';
import { useCondition } from './use-condition';
import { useConditionValue } from '@farris/ui-vue/components/condition';
import { EditorType } from '@farris/ui-vue/components/dynamic-form';

export function useSolution(
    props: QuerySolutionProps, 
    context: SetupContext, 
    useHttpComposition:UseHttp, 
    solutions: Ref<QuerySolution[]>, 
    currentSolution: Ref<QuerySolution | null>,  
    fields: Ref<FieldConfig[]>
    ): UseSolution {

    const { createConditionValue } = useConditionValue();
    const { getFilterConditions } = useCondition(props, context);
    const { loadSolution } = useHttpComposition;
    function getRandomId() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
    function getGuid() {
        return (getRandomId() + getRandomId() + "-" + getRandomId() + "-" + getRandomId() + "-" + getRandomId() + "-" + getRandomId() + getRandomId() + getRandomId());
    }

    
    function fieldToCondition(field: FieldConfig): Pick<Condition, 'id' | 'fieldCode' | 'fieldName'| 'required'| 'value'> {
        const condition = {
            id: field.id,
            fieldCode: field.labelCode,
            fieldName: field.name,
            required: field.editor.required,
            value: createConditionValue(field.controlType as EditorType)
        };
        if(field.controlType === 'lookup') {
            condition.value.valueField = field.editor.valueField;
        }
        return condition;
    }
    function getDefaultSolution():QuerySolution {
        const defaultSolution = {
            id: getGuid(),
            code: 'Default',
            name: props.presetQuerySolutionName || '默认筛选条件',
            belongId: props.formId,
            mode: '1',
            type: 'preset',
            isDefault: false,
            isSystem: true,
            conditions: props.presetFields.map((field: FieldConfig) => {
                return fieldToCondition(field);
            })
        };
        return defaultSolution;
    }
    
    function handleQuery() {
        const queryItems = currentSolution.value?.conditions.filter(condition => {
           return  !condition.value?.isEmpty();
        });
        const querys = getFilterConditions(queryItems, fields.value);

        context.emit('conditionChange', querys);
    }

    function loadAllSolution() {
        loadSolution().then((res:any) => {
            if(res && res.data) {
                const filterSolutions:QuerySolution[] = res.data.map((solution) => {
                    const conditions = JSON.parse(solution.queryConditionString);
                    solution.conditions = conditions.filter((condition:Condition) => {
                        return fields.value.find((field:FieldConfig) => field.labelCode === condition.fieldCode);
                    }).map((condition:Condition) => {
                        condition.value = createConditionValue(condition.value?.editorType as EditorType, condition.value);
                        return condition;
                    });
                    return solution;
                });
                if (!filterSolutions.length) {
                    const defaultSolution:QuerySolution = getDefaultSolution();
                    filterSolutions.push(defaultSolution);
                }
                solutions.value = filterSolutions;
                if(currentSolution.value) {
                    const targetSolution = solutions.value.find(solution => solution.id === currentSolution.value?.id);
                    if(targetSolution) {
                        currentSolution.value = cloneDeep(targetSolution);
                    } else {
                        currentSolution.value = cloneDeep(solutions.value[0]);
                    }
                } else {
                    currentSolution.value = cloneDeep(solutions.value[0]);
                }
                handleQuery();
            }
        }).catch(() => {
            const defaultSolution:QuerySolution = getDefaultSolution();
            solutions.value = [defaultSolution];
            currentSolution.value = cloneDeep(defaultSolution);
        });
    }

    return {
        getDefaultSolution,
        loadAllSolution,
        getGuid,
        handleQuery,
        fieldToCondition
    };
}

