/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { QuerySolution } from '../query-solution';
import { JSX } from "vue/jsx-runtime";
import { AxiosResponse } from 'axios';
import { Ref } from 'vue';
import { Condition, FieldConfig } from '../../../condition';

export enum ValueType{
   /**
    *值类型
    */
   Value = 0,
   /**
    * 表达式
    */
   Express = 1
}

export enum RelationType{

   Empty = 0,
   /**
    * 与
    */
   // And=0,
   And = 1,
   /**
    * 或
    */
   // Or=1
   Or = 2
}

/**
 * 比较符
 */
 export enum CompareType{
   Equal = 0,
   NotEqual = 1,
   Greater = 2,
   GreaterOrEqual = 3,
   Less = 4,
   LessOrEqual = 5,
   Like = 6,
   LikeStartWith = 7,
   LikeEndWith = 8,
   NotLike = 9,
   NotLikeStartWith = 10,
   NotLikeEndWith = 11,
   Is = 12,
   IsNot = 13,
   In = 14,
   NotIn = 15
}

export type TransferItme = {
   id: string,
   name: string
}

export type QueryItem = {
   FilterField: string,
   Compare: CompareType,
   Value: any,
   Relation: RelationType,
   Expresstype: ValueType,
   Lbracket?: string,
   Rbracket?: string
}
export interface UseSolution {

   /**
    * 生成默认筛选方案
    */
   getDefaultSolution: () => QuerySolution;
   loadAllSolution: () => void;
   getGuid: () => string;
   handleQuery: () => void;
   fieldToCondition: (field: FieldConfig) => Pick<Condition, 'id' | 'fieldCode' | 'fieldName'| 'required'| 'value'>;
}

export interface UseHeader {
   expanded: Ref<boolean>;
   renderHeader: () => JSX.Element;
   loadFields: () => void;

}
export interface UseCondition {
   getFilterConditions: (conditions, fields) => QueryItem[];

}
export interface UseHttp {
   createSolution: (solution) => Promise<AxiosResponse<any, any>>;
   loadSolution: () => Promise<AxiosResponse<any, any>>;
   updateSolution: (solution) => Promise<AxiosResponse<any, any>>;
   getAuth: () => Promise<AxiosResponse<any, any>>;
   batchUpdate: (solutions) => Promise<AxiosResponse<any, any>>;

}

