/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { UseHttp } from './types';
import { SetupContext } from 'vue';

import axios from 'axios';
import { QuerySolutionProps } from '../query-solution.props';

export function useHttp(props: QuerySolutionProps, context: SetupContext): UseHttp {

    const url = '/api/runtime/sys/v1.0/querysolution';
    const sessionid = 'default';
    function createSolution(solution) {
        const headers = {
            'Content-Type': 'application/json',
            'sessionid': sessionid
        };
        const options = {
            headers: headers,
            sessionid
        };
        return axios.post(url, solution, options);
    }

    /**
         * 根据表单ID查询当前表单当前登录用户的查询方案列表
         * @param formId 表单ID
         */
    function loadSolution() {
        const options = {
            headers: {
                'Content-Type': 'application/json',
                'sessionid': sessionid
            },
            sessionid
        };
        return axios.get(`/api/runtime/sys/v1.0/querysolution/belongId/${props.formId}`, options);
    }

    function updateSolution(solution) {
        const headers = {
            'Content-Type': 'application/json',
            'sessionid': sessionid
        };
        const options = {
            headers: headers
        };

        return axios.put(url, solution, options);
    }

    function batchUpdate(solutions) {
        const headers = {
            'Content-Type': 'application/json',
            'sessionid': sessionid
        };
        const options = {
            headers: headers
        };

        const requestUrl = `${url}/batch`;
        return axios.put(requestUrl, solutions, options);
    }

    function getAuth() {
        const headers = {
            "Content-Type": "application/json",
            "sessionid": sessionid
        };
        const options = {
            headers: headers
        };
        const requestUrl = `${url}/componentType/QuerySolution`;
        return axios.get(requestUrl, options);
    }
    return {
        createSolution,
        loadSolution,
        updateSolution,
        getAuth,
        batchUpdate
    };
}

