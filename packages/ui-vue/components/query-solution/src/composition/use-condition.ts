/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { UseCondition, ValueType, RelationType, CompareType, QueryItem } from './types';
import { SetupContext } from 'vue';

import { QuerySolutionProps } from '../query-solution.props';

export function useCondition(props: QuerySolutionProps, context: SetupContext): UseCondition {

	function convertInputGroup(condition) {
		const inputGroupValue = condition.value;
		const querys: QueryItem[] = [];

		querys.push({
			'FilterField': condition.fieldCode,
			'Compare': (condition.compareType || condition.compareType === 0) ? condition.compareType : CompareType.Like,
			'Value': inputGroupValue.value,
			'Relation': (condition.relation || condition.relation === 0) ? condition.relation : RelationType.And,
			'Expresstype': ValueType.Value,
			'Lbracket': condition.Lbracket || null,
			'Rbracket': condition.Rbracket || null
		});
		return querys;
	}

	function convertNumberSpinner(condition) {
		const numberSpinnerValue = condition.value.getValue();
        return [{
            'FilterField' : condition.fieldCode,
            'Compare' : (condition.compareType || condition.compareType === 0) ? condition.compareType : CompareType.Equal,
            'Value' : numberSpinnerValue,
            'Relation' : (condition.relation || condition.relation === 0) ? condition.relation : RelationType.And,
            'Expresstype':ValueType.Value,
            'Lbracket': condition.Lbracket || null,
            'Rbracket': condition.Rbracket || null
        }];
	}

	function convertNumberRange(condition) {
		const querys: QueryItem[] = [];
        const numberRangeValue = condition.value.getValue();
        (numberRangeValue.begin != null) && querys.push({
            'FilterField' : condition.fieldCode,
            'Compare' : CompareType.GreaterOrEqual,
            'Value' : numberRangeValue.begin,
            'Relation' : RelationType.And,
            'Expresstype': ValueType.Value
        });
        (numberRangeValue.end != null) && querys.push({
            'FilterField' : condition.fieldCode,
            'Compare' : CompareType.LessOrEqual,
            'Value' : numberRangeValue.end,
            'Relation' : RelationType.And,
            'Expresstype': ValueType.Value
        });

        return querys;
	}

	function convertLookup(condition) {
		const querys: QueryItem[] = [];
        const lookupValue = condition.value.getValue();
        if(lookupValue === ''){
			return [];
		};

        lookupValue.split(',').forEach(id => {
			id && querys.push({
				'FilterField' : condition.fieldCode,
				'Compare' : (condition.compareType || condition.compareType === 0) ? condition.compareType : CompareType.Equal,
				'Value' : id,
				'Relation' : condition.compareType === CompareType.NotEqual ? RelationType.And : RelationType.Or,
				'Expresstype':ValueType.Value
			});
		});
		if(querys.length > 0){
			querys[0]['Lbracket'] = condition.Lbracket ? (condition.Lbracket + '(') :'(';
			querys[querys.length -1]['Rbracket'] = condition.Rbracket ? (condition.Rbracket + ')') : ')';
			querys[querys.length - 1]['Relation'] = (condition.relation || condition.relation === 0) ? condition.relation : RelationType.And;
			return querys;
		}else{
			return [];
		}
	}

	function convertDatePicker(condition) {
		const querys: QueryItem[] = [];
        const datePickerValue = condition.value.getValue();
		querys.push({
			'FilterField' : condition.fieldCode,
			'Compare' : (condition.compareType || condition.compareType === 0) ? condition.compareType : CompareType.Equal,
			'Value' : datePickerValue,
			'Relation' : (condition.relation || condition.relation === 0) ? condition.relation : RelationType.And,
			'Expresstype':ValueType.Value,
			'Lbracket': condition.Lbracket || null,
			'Rbracket': condition.Rbracket || null
		});
        return querys;
	}

	function convertComboList(condition, field) {
		const querys: QueryItem[] = [];
        const comboListValue = condition.value.getValue();
		if(field.editor.enumValueType === 'string') {
			comboListValue.split(',').forEach(value => {
				querys.push({
					'FilterField' : condition.fieldCode,
					'Compare' : (condition.compareType || condition.compareType === 0) ? condition.compareType : CompareType.Equal,
					'Value' : value,
					'Relation' : condition.compareType === CompareType.NotEqual ? RelationType.And : RelationType.Or,
					'Expresstype':ValueType.Value
				});
			});
			querys[0]['Lbracket'] = condition.Lbracket ? (condition.Lbracket + '(') :'(';
        	querys[querys.length -1]['Rbracket'] = condition.Rbracket ? (condition.Rbracket + ')') : ')';
        	querys[querys.length - 1]['Relation'] = (condition.relation || condition.relation === 0) ? condition.relation : RelationType.And;
		} else if(field.editor.enumValueType === 'int' || field.editor.enumValueType === 'boolean') {
			querys.push({
				'FilterField' : condition.fieldCode,
				'Compare' : (condition.compareType || condition.compareType === 0) ? condition.compareType : CompareType.Equal,
				'Value' : comboListValue,
				'Relation' : RelationType.And,
				'Expresstype':ValueType.Value
			});
		}
        return querys;
	}

	function convertRadioGroup(condition) {
		const querys: QueryItem[] = [];
        const radioGroupValue = condition.value.getValue();
		return [{
            'FilterField' : condition.fieldCode,
            'Compare' : (condition.compareType || condition.compareType === 0) ? condition.compareType : CompareType.Equal,
            'Value' :radioGroupValue,
            'Relation' : (condition.relation || condition.relation === 0) ? condition.relation : RelationType.And,
            'Expresstype':ValueType.Value,
            'Lbracket': condition.Lbracket || null,
            'Rbracket': condition.Rbracket || null
        }];
	}

	function convert(condition, field) {
		let querysResult:QueryItem[] = [];
		switch (condition.value.editorType) {
			case 'input-group':
				querysResult = convertInputGroup(condition);
				break;
			case 'number-spinner':
				querysResult = convertNumberSpinner(condition);
				break;
			case 'number-range':
				querysResult = convertNumberRange(condition);
				break;
			case 'lookup':
				querysResult = convertLookup(condition);
				break;
			case 'date-picker':
				querysResult = convertDatePicker(condition);
				break;
			case 'combo-list':
				querysResult = convertComboList(condition, field);
				break;
			case 'radio-group':
				querysResult = convertRadioGroup(condition);
				break;
			default: 
				querysResult = [];
		}

		return querysResult;
	}
	function getFilterConditions(conditions, fields): QueryItem[] {
		const filterConditionList:QueryItem[] = [];
		conditions.forEach(condition => {
			if (!condition.value.isEmpty()) {
				const conditionField = fields.find(field => field.id === condition.id);
				const convertResult = convert(condition, conditionField);
				filterConditionList.push(...convertResult);
			}
		});
		return filterConditionList;
	}

	return {
		getFilterConditions
	};
}

