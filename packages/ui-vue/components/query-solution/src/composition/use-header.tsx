/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { UseHeader, TransferItme, UseHttp, UseSolution } from './types';
import { computed, inject, ref, Ref, SetupContext } from 'vue';
import { QuerySolutionProps } from '../query-solution.props';
import { QuerySolution } from '../query-solution';
import { Condition, FieldConfig } from '../../../condition';
import { F_MODAL_SERVICE_TOKEN, FModalService } from '@farris/ui-vue/components/modal';
import { FTransfer } from '@farris/ui-vue/components/transfer';
import { FInputGroup } from '@farris/ui-vue/components/input-group';
import FRadioGroup from '@farris/ui-vue/components/radio-group';
import { FNotifyService } from "@farris/ui-vue/components/notify";
import { cloneDeep } from 'lodash-es';
import FSolutionManager from '../components/manager/solution-manager.component';


export function useHeader(
    props: QuerySolutionProps, 
    context: SetupContext, 
    useHttpComposition:UseHttp, 
    useSolutionComposition:UseSolution, 
    solutions: Ref<QuerySolution[]>,
    currentSolution: Ref<QuerySolution | null>, 
    fields: Ref<FieldConfig[]>
    ): UseHeader {

    const querysolutionTypeRadios = [
        { id: 'private', text: '用户个人方案' }
        // { id: 'public', text: '系统公共方案' },
        // { id: 'org', text: '组织公共方案' }
    ];
    const notifyService: any = new FNotifyService();
    const newSolutionName = ref('');
    const newSolutionAuth = ref('private');
    const deleteSolutionIds = ref<string[]>([]);
    const configConditions = ref<TransferItme[]>([]);
    const expanded = ref<boolean>(props.expanded);
    const { createSolution, updateSolution, getAuth, batchUpdate } = useHttpComposition;
    const { getGuid, loadAllSolution, handleQuery, fieldToCondition } = useSolutionComposition;
    const modalService: FModalService | null = inject(F_MODAL_SERVICE_TOKEN, null);
    const title = computed(() => currentSolution.value ? currentSolution.value.name : '');

    const shouldShowClearButton = computed(() => !currentSolution.value || currentSolution.value.mode !== '2');
    const shouldShowSolutionList = ref(false);
    const solutionListClass = computed(() => ({
        'dropdown-menu': true,
        show: shouldShowSolutionList.value
    }));
    const solutionListCloseModakClass = computed(() => ({
        'z-index': 99
    }));
    function onSelectionChange(solution: QuerySolution) {
        currentSolution.value = solution;
        handleQuery();
    }

    function query() {
        try {
            currentSolution.value?.conditions.map(condition => {
                if(condition.required && (!condition.value || condition.value.isEmpty())){
                    throw '查询前请填写必填项：' + condition.fieldName;
                }          
            });
            context.emit('query');
        } catch (error) {
            notifyService.warning({ message: error, position:'top-center' });
        }
       
    }
    const expandStyle = computed(() => {
        const styleObject = {
            transform: expanded.value ? 'rotate(0deg)' : 'rotate(180deg)'
        } as Record<string, any>;
        return styleObject;
    });
    function clear() {
        if (!currentSolution.value) {
            console.warn('no querysolution');
            return;
        }
        currentSolution.value.conditions.forEach((condition: Partial<Condition>) => {
            condition.value && condition.value.clear();
        });
        handleQuery();

    }
    function save() {
        if (!currentSolution.value) {
            return;
        }
        getAuth().then(authResponse => {
            const { data } = authResponse;
            const saveItem = cloneDeep(currentSolution.value) as QuerySolution;
            if (!data['success'] && (saveItem.type === 'public' || saveItem.type === 'org')) {
                notifyService.warning({ message: '您暂无权限修改公共类型方案。', position:'top-center' });
                return;
            } else {
                saveItem.queryConditionString = JSON.stringify(saveItem.conditions);
                updateSolution(saveItem).then(res => loadAllSolution());
                context.emit('save', currentSolution.value);
            }
        }); 
    }

    function renderSaveAs() {
        return <div class='m-3'>
            <div class="farris-group-wrap">
                <div class="form-group farris-form-group">
                    <div class="farris-input-wrap">
                        <FInputGroup v-model={newSolutionName.value}></FInputGroup>
                    </div>
                </div>
            </div>
            <FRadioGroup data={querysolutionTypeRadios} name='' textField='text' valueField='id' v-model={newSolutionAuth.value}></FRadioGroup>
        </div>;
    }

    function saveAsConfirm() {
        if(!currentSolution.value){
            return;
        } else if(!newSolutionName.value) {
            notifyService.warning({ message: '请填写方案名称', position:'top-center' });
            
            return;
        }
        getAuth().then(authResponse => {
            const { data } = authResponse;
            if(!data['success'] && (newSolutionAuth.value === 'public' || newSolutionAuth.value  === 'org')) {
                notifyService.warning({ message: '您暂无权限修改公共类型方案。' , position:'top-center'});
                return;
            } else {
                const saveItem = cloneDeep(currentSolution.value) as QuerySolution;
                saveItem.id = getGuid();
                saveItem.isSystem = false;
                saveItem.code = newSolutionName.value;
                saveItem.name = newSolutionName.value;
                saveItem.type = newSolutionAuth.value;
                saveItem.queryConditionString = JSON.stringify(saveItem.conditions);
                createSolution(saveItem).then(res => loadAllSolution());
                newSolutionAuth.value = 'private';
                newSolutionName.value = '';
            }
        });
    }
    function saveAsCancel() {
        newSolutionAuth.value = 'private';
        newSolutionName.value = '';
    }

    function managerConfirm() {
        const belongId = props.formId;
        const deletedIds = deleteSolutionIds.value;
        batchUpdate({belongId, deletedIds}).then((response) => {
            loadAllSolution();
        });
        deleteSolutionIds.value = [];
    }
    
    function managerCancel() {
        deleteSolutionIds.value = [];
    }
    function changeDeleteSolutionIds(solutionIds) {
        deleteSolutionIds.value = solutionIds;
    }
    function saveAs() {
        modalService?.open({
            width: 600,
            height: 600,
            title: '新增方案',
            acceptCallback: saveAsConfirm,
            rejectCallback: saveAsCancel,
            render: () => {
                return renderSaveAs();
            }
        });
        context.emit('saveAs', currentSolution.value);
    }


    function showSolutionManger() {
        modalService?.open({
            width: 600,
            height: 550,
            fitContent: false,
            title: '方案管理',
            acceptCallback: managerConfirm,
            rejectCallback: managerCancel,
            render: () => {
                return <FSolutionManager solutions={solutions.value} onChangeDelete={changeDeleteSolutionIds}/>;
            }
        });

    }
    function renderSolutionList() {
        const solutionItems = solutions.value.map((solutionItem: any) => {
            return (
                <li class="dropdown-item" onClick={() => onSelectionChange(solutionItem)}>
                    {solutionItem.name}
                </li>
            );
        });
        solutionItems.push(
            <li class="solution-header-title-btns">
                {
                    !currentSolution.value?.isSystem && <span class="dropdown-item-btn f-cursor-pointer" onClick={save}>
                        保存
                    </span>
                }
                <span class="dropdown-item-btn f-cursor-pointer ml-2 mr-2" onClick={saveAs}>
                    另存为
                </span>
                <span class="dropdown-item-btn f-cursor-pointer ml-2 mr-2" onClick={showSolutionManger}>
                    管理
                </span>
            </li>
        );
        return <ul class={solutionListClass.value}>{solutionItems}</ul>;
    }

    function toggleSolutionList(event:MouseEvent) {
        event.stopPropagation();
        shouldShowSolutionList.value = !shouldShowSolutionList.value;
    }
    function renderSummary() {
        return (
            <div>
                {/* <FFilterBar
                    data={currentSolution.value.conditions}
                    fields={fields.value}
                    mode="display-only"
                    show-reset={true}></FFilterBar> */}
            </div>
        );
    }

    function configConfirm() {
        if (currentSolution.value) {
            const currentConditions = currentSolution.value.conditions || [];
            const newConditions: Partial<Condition>[] = [];
            configConditions.value.forEach(configCondition => {
                const existCondition = currentConditions.find(condition => condition.id === configCondition.id);
                if (existCondition) {
                    newConditions.push(existCondition);
                } else {
                    const newCondition = fields.value.find(condition => condition.id === configCondition.id);
                    if (newCondition) {
                        newConditions.push(fieldToCondition(newCondition));
                    }
                }
            });
            currentSolution.value.conditions = newConditions;
        }
        configConditions.value = [];
        handleQuery();


    }
    function configCancel() {
        configConditions.value = [];

    }
    function changeConfigConditions(transferData) {
        configConditions.value = transferData;
    }

    function showConfig() {
        configConditions.value = [];
        const selections = currentSolution.value?.conditions.map((condition) => {
            return { id: condition.id || '', name: condition.fieldName || '' };
        }) || [];
        const dataSource = fields.value.map((field) => {
            return { id: field.id, name: field.name };
        }) || [];
        configConditions.value = selections;
        modalService?.open({
            width: 900,
            height: 600,
            title: '筛选条件配置',
            acceptCallback: configConfirm,
            rejectCallback: configCancel,

            render: () => {
                return <FTransfer
                    selections={selections}
                    data-source={dataSource}
                    class="query-solution-config-transfer ml-3"
                    onChange={changeConfigConditions}
                ></FTransfer>;
            }
        });
    }

    function handleExpand() {
        expanded.value = !expanded.value;
    }
    function renderSolutionToolbar() {
        return (
            <div class="solution-action">
                <div class="btn-group">
                    <button type="button" class="btn btn-primary " onClick={query}>
                        {props.filterText}
                    </button>
                </div>
                {shouldShowClearButton.value && (
                    <div class="icon-group ml-2" onClick={clear}>
                        <span class="f-icon f-icon-remove f-cursor-pointer"></span>
                    </div>
                )}
                {expandStyle.value && <div class="icon-group ml-2" onClick={showConfig}>
                    <span class="f-icon f-icon-home-setup f-cursor-pointer"></span>
                </div>}
                <div class="icon-group ml-2" onClick={handleExpand}>
                        <span class="f-icon f-icon-packup f-cursor-pointer" style={expandStyle.value}></span>
                </div>
            </div>
        );
    }

    function renderHeader() {
        return (
            <div class="solution-header">
                <div class="btn-group mr-3" onClick={toggleSolutionList}>
                    <div class="solution-header-title">
                        {title.value}
                        <span class="f-icon f-accordion-expand"></span>
                    </div>
                    {renderSolutionList()}
                    {
                        shouldShowSolutionList.value &&
                        <div class="position-fixed w-100 h-100" style={solutionListCloseModakClass.value} onClick={toggleSolutionList}></div>
                    }
                </div>
                {renderSummary()}
                {renderSolutionToolbar()}
            </div>
        );
    }

    function loadFields() {
        // 部分属性不需要进行配置且使用的值与组件默认值不一致需要在此处理
        fields.value = props.fields.map((field) => {
            const currentField = cloneDeep(field);
            if (!currentField.editor.type) {
                currentField.editor.type = field.controlType;
            }
            if(currentField.editor.type === 'number-spinner' || currentField.editor.type === 'number-range') {
                currentField.editor.showZero = true;
                currentField.editor.nullable = true;
            }
            if(currentField.editor.type === 'combo-list') {
                currentField.editor.enableClear = true;
            }
            return currentField;
        });
    }
    return {
        expanded,
        renderHeader,
        loadFields
    };
}

