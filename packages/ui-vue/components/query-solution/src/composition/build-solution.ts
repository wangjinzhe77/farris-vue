import { useGuid } from "../.../../../../common";
import { DesignerHostService, DgControl } from "../.../../../../designer-canvas";
import { getSchemaByType } from "../.../../../../dynamic-resolver";

export function querySolutionCreatorService(context: Record<string, any>, designerHostService?: DesignerHostService) {

    const formSchemaUtils = designerHostService?.formSchemaUtils;
    const targetComponentInstance = context.parentComponentInstance;
    /** 加载命令的控制器id */
    const loadCommandCmpId = '54bddc89-5f7e-4b91-9c45-80dd6606cfe9';

    /**
     * 创建查询相关命令
     * @param solutionMetadata 筛选方案元数据
     */
    function createCommandsForSolution(solutionMetadata: any) {
        const targetComponentId = targetComponentInstance.belongedComponentId;
        const viewModelId = formSchemaUtils.getViewModelIdByComponentId(targetComponentId);
        const viewModel = formSchemaUtils.getViewModelById(viewModelId);

        // 1、预制筛选方案查询命令
        let newFilterCommandId;
        if (viewModel && viewModel.commands) {
            let filterCommand = viewModel.commands.find(command => command.handlerName === 'Filter' && command.cmpId === loadCommandCmpId);
            if (!filterCommand) {
                newFilterCommandId = useGuid().guid();
                filterCommand = {
                    id: newFilterCommandId,
                    code: 'rootviewmodelFilter1',
                    name: '过滤并加载数据1',
                    params: [
                        {
                            name: 'filter',
                            shownName: '过滤条件',
                            value: '{UISTATE~/#{root-component}/originalFilterConditionList}'
                        },
                        {
                            name: 'sort',
                            shownName: '排序条件',
                            value: ''
                        }
                    ],
                    handlerName: 'Filter',
                    cmpId: loadCommandCmpId,
                    shortcut: {},
                    extensions: []
                };
                viewModel.commands.push(filterCommand);
            } else {
                if (filterCommand.params && filterCommand.params.length) {
                    const filterParam = filterCommand.params.find(p => p.name === 'filter');
                    if (filterParam) {
                        filterParam.value = '{UISTATE~/#{root-component}/originalFilterConditionList}';
                    }
                }
            }

            const filterCommandCode = filterCommand.code;
            solutionMetadata.onQuery = filterCommandCode;
        }

        // 2、预置筛选方案查询数据的构件
        const webCmds = formSchemaUtils.getFormSchema().module.webcmds;
        if (webCmds) {
            let loadDataCmd = webCmds.find(cmd => cmd.id === loadCommandCmpId);
            if (!loadDataCmd) {
                loadDataCmd = {
                    id: loadCommandCmpId,
                    path: 'igix/Web/WebCmp/bo-webcmp/metadata/webcmd/data-commands',
                    name: 'LoadCommands.webcmd',
                    refedHandlers: []
                };
                formSchemaUtils.getFormSchema().module.webcmds.push(loadDataCmd);
            }
            if (newFilterCommandId) {
                loadDataCmd.refedHandlers.push(
                    {
                        host: newFilterCommandId,
                        handler: 'Filter'
                    }
                );

            }
        }
    }

    function createQuerySolution(): any {
        // 1、组装筛选方案容器和筛选方案dom结构
        const solutionContainer = getSchemaByType(DgControl.section.type);
        const solutionMetadata = getSchemaByType(DgControl['query-solution'].type);
        if (!solutionContainer || !solutionMetadata) {
            return;
        }
        const prefix = Math.random().toString(36).slice(2, 6);
        Object.assign(solutionContainer, {
            id: 'solution-container-' + prefix,
            appearance: {
                class: 'f-section-scheme f-section-in-managelist'
            },
            showHeader: false,
            contents: [solutionMetadata]
        });

        Object.assign(solutionMetadata, {
            id: 'solution-' + prefix,
            fields: [],
            presetFields: [],
            formId: formSchemaUtils.getFormMetadataBasicInfo().code
        });


        // 2、关联DOM处理--新增样式
        const targetContainer = targetComponentInstance.schema;
        if (targetContainer) {
            if (!targetContainer.appearance) {
                targetContainer.appearance.class = 'f-page-has-scheme';
            } else if (targetContainer.appearance.class && !targetContainer.appearance.class.includes('f-page-has-scheme')) {
                targetContainer.appearance.class += ' f-page-has-scheme';
            }
        }


        // 3、预置筛选方案相关的变量
        const targetComponentId = targetComponentInstance.belongedComponentId;
        const viewModelId = formSchemaUtils.getViewModelIdByComponentId(targetComponentId);
        const viewModel = formSchemaUtils.getViewModelById(viewModelId);
        if (viewModel?.states) {
            if (!viewModel.states.find(state => state.code === 'originalFilterConditionList')) {
                viewModel.states.push(
                    {
                        id: useGuid().guid(),
                        category: 'locale',
                        code: 'originalFilterConditionList',
                        name: '筛选方案筛选条件',
                        type: 'String'
                    }
                );
            }
        }

        // 4、创建查询相关命令
        createCommandsForSolution(solutionMetadata);

        // 5、因为涉及到新增控制器，所以这里需要获取控制器信息，方便交互面板展示命令数据
        const formCommandService = designerHostService?.useFormCommand;
        if (formCommandService) {
            formCommandService.checkCommands();
        }

        return solutionContainer;
    }

    return {
        createQuerySolution
    };
}
