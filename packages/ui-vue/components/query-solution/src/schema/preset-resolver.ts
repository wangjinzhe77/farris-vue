import { CompareType } from "../../../condition/src/composition/use-compare";
import { Condition, ConditionValue, FieldConfig, ValueType } from "../../../condition";
import { QuerySolution } from "../query-solution";

export function resolvePreset(key: string, presetFields: FieldConfig[]) {
    const conditions = presetFields.map((presetField: FieldConfig, conditionId: number) => {
        const { id, labelCode: fieldCode, name: fieldName } = presetField;
        const compareType = CompareType.Equal;
        const valueType = ValueType.Value;
        const value: ConditionValue = {} as ConditionValue;
        return { id, fieldCode, fieldName, compareType, valueType, value, conditionId } as Condition;
    });
    const defaultQuerySolution = {
        id: 'default',
        code: 'default',
        name: '默认筛选方案',
        conditions,
        isSystem: true,
        isDefault: true,
        type: '',
        mode: '1'
    } as QuerySolution;
    return { "solutions": [defaultQuerySolution] };
}
