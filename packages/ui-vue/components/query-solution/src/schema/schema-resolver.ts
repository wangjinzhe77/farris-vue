import { DesignerComponentInstance, DesignerHostService } from "../../../designer-canvas";
import { DynamicResolver } from "../../../dynamic-resolver";
import { querySolutionCreatorService } from "../composition/build-solution";

export function schemaResolver(resolver: DynamicResolver, schema: Record<string, any>, context: Record<string, any>, designerHostService?: DesignerHostService): Record<string, any> {

    const parentComponentInstance = context.parentComponentInstance as DesignerComponentInstance;
    if (parentComponentInstance && designerHostService) {
        const solutionCreator = querySolutionCreatorService(context, designerHostService);
        const solutionContainer = solutionCreator.createQuerySolution();
        if (solutionContainer) {
            return solutionContainer;
        }
    }
    return schema;
}
