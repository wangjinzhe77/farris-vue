 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import { QuerySolution } from './query-solution';
import { FieldConfig } from '../../condition/src/types';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import querySolutionSchema from './schema/query-solution.schema.json';
import { schemaResolver } from './schema/schema-resolver';

export const querySolutionProps = {
    /**
     * 表单id
     */
    formId: { type:String, default: ''},
    fields: { type: Array<FieldConfig>, default: [] },
    presetFields: { type: Array<FieldConfig>, default: [] },
    solutions: { type: Array<QuerySolution>, default: [] },
    /**
     * 控间标签同行展示（标准模式下）
     */
     isControlInline: { type: Boolean, default: true },
     /**
      * 筛选按钮文案
      */
     filterText: { type: String, default: '筛选' },
     /**
      * 是否默认展开
      */
     expanded: { type: Boolean, default: true },
     /**
      * 是否开启高级模式下
      */
     openAdvancedMode: { type: Boolean, default: false },
     /**
      * 预设筛选方案名称
      */
     presetQuerySolutionName: { type: String, default: '默认筛选方案' },
} as Record<string, any>;

export type QuerySolutionProps = ExtractPropTypes<typeof querySolutionProps>;

export const propsResolver = createPropsResolver<QuerySolutionProps>(querySolutionProps, querySolutionSchema, schemaMapper, schemaResolver);
