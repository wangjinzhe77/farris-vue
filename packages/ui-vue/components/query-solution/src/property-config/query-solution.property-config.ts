import { InputBaseProperty } from "../../../property-panel/src/composition/entity/input-base-property";
import { DesignerComponentInstance } from "../../../designer-canvas/src/types";
import { PropertyItem } from "../designer/query-solution-config/composition/types";
import { solutionFieldsConverter, solutionPresetConverter } from "./converters/query-solution-property.converter";


export class QuerySolutionPropertyConfig extends InputBaseProperty {
    getPropertyConfig(propertyData: any, componentInstance: DesignerComponentInstance) {
        this.propertyConfig.categories['basic'] = this.getBasicPropConfig(propertyData);
        this.getAppearanceConfig();
        this.getBehaviorConfig(propertyData);
        this.getQuerySolutionConfig(propertyData);
        this.getEventPropConfig(propertyData);
        return this.propertyConfig;
    }

    private getBehaviorConfig(propertyData) {
        this.propertyConfig.categories['apperance'] = {
            description: "Basic Infomation",
            title: "行为",
            properties: {
                isControlInline: {
                    description: "控件标签同行展示",
                    type: "boolean",
                    title: "控件标签同行展示"
                },
                presetQuerySolutionName: {
                    description: "系统预置筛选方案名称",
                    type: "string",
                    title: "系统预置筛选方案名称"
                },
                filterText: {
                    description: "筛选按钮文本",
                    type: "string",
                    title: "筛选按钮文本"
                },
                expanded: {
                    description: "展开",
                    type: "boolean",
                    title: "展开",
                    // visible: {
                    //     anyOf: [
                    //         {
                    //             isControlInline: {
                    //                 equal: true
                    //             }
                    //         }
                    //     ]
                    // }
                }
            }
        };
    }

    private getQuerySolutionConfig(propertyData: any) {
        const allFields = this.designViewModelUtils.getAllFields2TreeByVMId(this.viewModelId);
        let fields: PropertyItem[] = [];
        allFields.forEach(field => {
            if (field.children.length > 0) {
                field.children.forEach(childrenField => {
                    fields.push(childrenField.data);
                });
            } else {
                fields.push(field.data);
            }
        });
        fields = fields.filter(field => field.$type === 'SimpleField');
        this.propertyConfig.categories['control'] = {
            description: "Basic Infomation",
            title: "编辑器",
            properties: {
                fields: {
                    description: "筛选方案字段设置",
                    title: "筛选方案字段",
                    type: "",
                    refreshPanelAfterChanged: true,
                    editor: {
                        type: "query-solution-config",
                        fieldsConfig: fields,
                        formSchemaUtils: this.formSchemaUtils,
                        metadataService: this.metadataService,
                        viewModelId: this.viewModelId,
                        designViewModelUtils: this.designViewModelUtils,
                        eventsEditorUtils: this.eventsEditorUtils

                    },
                    $converter: solutionFieldsConverter
                },
                presetFields: {
                    description: "系统预置字段",
                    title: "系统预置字段",
                    type: "",
                    editor: {
                        type: "solution-preset",
                        dataSource: propertyData.fields
                    },
                    $converter: solutionPresetConverter
                }
            }
        };
    }

    private getEventPropConfig(propertyData: any) {
        const events = [
            {
                "label": "onQuery",
                "name": "查询事件"
            }
        ];
        const self = this;
        const initialData = self.eventsEditorUtils['formProperties'](propertyData, self.viewModelId, events);
        const properties = {};
        properties[self.viewModelId] = {
            type: 'events-editor',
            editor: {
                initialData
            }
        };
        this.propertyConfig.categories['eventsEditor'] = {
            title: '事件',
            hideTitle: true,
            properties,
            // 这个属性，标记当属性变更得时候触发重新更新属性
            refreshPanelAfterChanged: true,
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject: any, data: any) {
                const parameters = changeObject.propertyValue;
                delete propertyData[self.viewModelId];
                if (parameters) {
                    parameters.setPropertyRelates = this.setPropertyRelates; // 添加自定义方法后，调用此回调方法，用于处理联动属性
                    self.eventsEditorUtils.saveRelatedParameters(propertyData, self.viewModelId, parameters['events'], parameters);
                }
                // 联动修改排序开关
                propertyData.remoteSort = propertyData.columnSorted ? true : false;
            }
        };
    }
}
