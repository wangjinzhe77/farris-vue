import { FieldConfig } from "@farris/ui-vue/components/condition";

export const solutionFieldsConverter = {
    convertFrom: (schema: Record<string, any>, propertyKey: string) => {
        return schema[propertyKey];
    },
    convertTo: (schema: Record<string, any>, propertyKey: string, propertyValue: any) => {
        const originPresetFields = schema['presetFields'];
        const presetFields:Array<FieldConfig> = [];
        originPresetFields.forEach(presetField => {
            const currentField = propertyValue.find(field => field.id === presetField.id);
            if(currentField) {
                presetFields.push(currentField);
            }
        });
        schema[propertyKey] = propertyValue;
        schema['presetFields'] = presetFields;
        
    }
};
export const solutionPresetConverter = {
    convertFrom: (schema: Record<string, any>, propertyKey: string) => {
        return schema[propertyKey].map(field => {
            return {id:field.id, name:field.name + '(' + field.code + ')'};
        });
    },
    convertTo: (schema: Record<string, any>, propertyKey: string, propertyValue: any) => {
        const presetFields:Array<FieldConfig> = [];
        const { fields } = schema;
        propertyValue.forEach(presetField => {
            const currentField = fields.find(field => field.id === presetField.id);
            if(currentField) {
                presetFields.push(currentField);
            }
        });
        schema[propertyKey] = presetFields;
    }
};
