import { computed } from "vue";
import { UseGradient, UseProgress, UseProgressType, UseStrokeWidth } from "./types";
import { ProgressProps } from "../progress.props";

export function useProgress(
    props: ProgressProps,
    useProgressType: UseProgressType,
    useGradient: UseGradient,
    useStrokeWidth: UseStrokeWidth
): UseProgress {
    const { isCircleStyle } = useProgressType;
    const { isGradient, linearGradient } = useGradient;
    const { strokeWidth } = useStrokeWidth;
    const progressClass = computed(() => {
        const classObject = {
            'f-progress': true,
            'f-progress-line': props.progressType === 'line',
            'ant-progress-small': props.size === 'small',
            'f-progress-show-info': props.showInfo,
            'f-progress-circle': isCircleStyle.value
        } as Record<string, boolean>;
        const statusKey = `f-progress-status-${props.progressStatus}`;
        classObject[statusKey] = true;
        return classObject;
    });
    const inCircleProgressBarStyle = computed(() => {
        const styleObject = {
            width: `${props.width}px`,
            height: `${props.width}px`,
            'font-size': `${props.width * 0.15 + 6}px`,
            'f-progress-circle-gradient': isGradient.value
        } as Record<string, any>;
        return styleObject;
    });
    const inCircleProgressBarClass = computed(() => {
        return {
            'f-progress-inner': true,
            'f-progress-circle-gradient': isGradient.value,
        } as Record<string, boolean>;
    });
    const gradientIdSeed = 0;
    const gradientId = computed(() => {
        return gradientIdSeed + 1;
    });
    const pathString = computed(() => {
        const radius = 50 - strokeWidth.value / 2;
        const position = props.gapPosition || (props.progressType === 'circle' ? 'top' : 'bottom');
        let beginPositionX = 0;
        let beginPositionY = -radius;
        let endPositionX = 0;
        let endPositionY = radius * -2;
        switch (position) {
        case 'left':
            beginPositionX = -radius;
            beginPositionY = 0;
            endPositionX = radius * 2;
            endPositionY = 0;
            break;
        case 'right':
            beginPositionX = radius;
            beginPositionY = 0;
            endPositionX = radius * -2;
            endPositionY = 0;
            break;
        case 'bottom':
            beginPositionY = radius;
            endPositionY = radius * 2;
            break;
        default:
        }
        return `M 50,50 m ${beginPositionX},${beginPositionY}
                a ${radius},${radius} 0 1 1 ${endPositionX},${-endPositionY}
                a ${radius},${radius} 0 1 1 ${-endPositionX},${endPositionY}`;
    });
    const trailPathStyle = computed(() => {
        const radius = 50 - strokeWidth.value / 2;
        const len = Math.PI * 2 * radius;
        const degree = props.gapDegree || (props.progressType === 'circle' ? 0 : 75);
        return {
            strokeDasharray: `${len - degree}px ${len}px`,
            strokeDashoffset: `-${degree / 2}px`,
            transition: 'stroke-dashoffset .3s ease 0s, stroke-dasharray .3s ease 0s, stroke .3s'
        };
    });
    const progressCirclePaths = computed(() => {
        const radius = 50 - strokeWidth.value / 2;
        const len = Math.PI * 2 * radius;
        const degree = props.gapDegree || (props.progressType === 'circle' ? 0 : 75);
         
        const values = (props.successPercent !== undefined && props.successPercent !== null) ? [props.successPercent, props.percent] : [props.percent];
        return values.map((value, index) => {
            const isSuccessPercent = values.length === 2 && index === 0;
            return {
                stroke: isGradient.value && !isSuccessPercent ? `url(#gradient-${gradientId.value})` : null,
                strokePathStyle: {
                    stroke: !isGradient.value ? (isSuccessPercent ? 'default' : (props.strokeColor as string)) : null,
                    transition: 'stroke-dashoffset .3s ease 0s, stroke-dasharray .3s ease 0s, stroke .3s, stroke-width .06s ease .3s',
                    strokeDasharray: `${((value || 0) / 100) * (len - degree)}px ${len}px`,
                    strokeDashoffset: `-${degree / 2}px`
                }
            };
        }).reverse();
    });
    const inlineProgressBackgroundStyle = computed(() => {
        const styleObject = {
            width: `${props.percent}%`,
            'border-radius': props.strokeLinecap === 'round' ? '100px' : '0',
            'background': !isGradient.value ? props.strokeColor : null,
            'background-image': isGradient.value ? linearGradient.value : null,
            height: `${strokeWidth.value}px`,
        } as Record<string, any>;
        return styleObject;
    });
    return {
        progressClass,
        inCircleProgressBarStyle,
        inCircleProgressBarClass,
        pathString,
        trailPathStyle,
        progressCirclePaths,
        gradientId,
        inlineProgressBackgroundStyle
    };
}
