import { computed } from "vue";
import { FProgressGradientProgress, ProgressProps } from "../progress.props";
import { UseGradient } from "./types";

export function useGradient(props: ProgressProps): UseGradient {
    function stripPercentToNumber(percent: string): number {
        return +percent.replace('%', '');
    }
    function sortGradient(gradients: FProgressGradientProgress) {
        let tempArr: Array<{ key: number; value: string }> = [];
        Object.keys(gradients).forEach(key => {
            const value = gradients[key];
            const formatKey = stripPercentToNumber(key);
            if (!isNaN(formatKey)) {
                tempArr.push({
                    key: formatKey,
                    value
                });
            }
        });
        tempArr = tempArr.sort((a, b) => a.key - b.key);
        return tempArr;
    };
    const isGradient = computed(() => {
        return (!!props.strokeColor && typeof props.strokeColor !== 'string');
    });
    const linearGradient = computed(() => {
        const { from = '#59a1ff', to = '#59a1ff', direction = 'to right', ...rest } = props.strokeColor;
        if (Object.keys(rest).length !== 0) {
            const sortedGradients = sortGradient(rest as FProgressGradientProgress)
                .map(({ key, value }) => `${value} ${key}%`)
                .join(', ');
            return `linear-gradient(${direction}, ${sortedGradients})`;
        }
        return `linear-gradient(${direction}, ${from}, ${to})`;
    });
    const circleGradient = computed(() => {
        return sortGradient(props.strokeColor).map(({ key, value }) => ({ offset: `${key}%`, color: value }));
    });
    return {
        isGradient,
        linearGradient,
        circleGradient
    };
}
