import { computed } from "vue";
import { ProgressProps } from "../progress.props";
import { UseProgressStatus } from "./types";

export function useProgressStatus(props: ProgressProps): UseProgressStatus {
    const inlineProgrssStatusClass = computed(() => {
        const classObject = {
            'f-progress-text-icon': true,
            'f-icon': true,
        } as Record<string, boolean>;
        const statusKey = `f-icon-${props.progressStatus}`;
        classObject[statusKey] = true;
        return classObject;
    });
    const circleProgrssStatusClass = computed(() => {
        const classObject = {
            'f-progress-text-icon': true,
            'f-icon': true,
        } as Record<string, boolean>;
        const iconClass = props.progressStatus === 'success' ? 'f-icon-checkmark' : 'f-icon-x';
        classObject[iconClass] = true;
        return classObject;
    });
    const shouldShowStatus = computed(() => {
        return props.progressStatus === 'error' || props.progressStatus === 'success';
    });
    return {
        inlineProgrssStatusClass,
        circleProgrssStatusClass,
        shouldShowStatus
    };
}
