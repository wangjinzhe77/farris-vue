import { computed } from "vue";
import { ProgressProps } from "../progress.props";
import { UseSuccessPercent } from "./types";

export function useSuccessPercent(props: ProgressProps): UseSuccessPercent {
    const inlineProgressSuccessBackgroundStyle = computed(() => {
        const styleObject = {
            width: `${props.successPercent}%`,
            'border-radius': props.strokeLinecap === 'round' ? '100px' : '0',
            height: `${props.strokeWidth}px`,
            'background-image': props.enableBackgroundImg ? `url(${props.backgroundImg})` : null
        } as Record<string, any>;
        return styleObject;
    });

    const shouldShowSuccessInlineProgressBackground = computed(() => {
        return props.successPercent || props.successPercent === 0;
    });
    return { inlineProgressSuccessBackgroundStyle, shouldShowSuccessInlineProgressBackground };
}
