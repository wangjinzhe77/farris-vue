import { computed } from "vue";
import { UseProgressType } from "./types";
import { ProgressProps } from "../progress.props";

export function useProgressType(props: ProgressProps): UseProgressType {
    const isCircleStyle = computed(() => {
        return props.progressType === 'circle' || props.progressType === 'dashboard';
    });
    return {isCircleStyle};
}
