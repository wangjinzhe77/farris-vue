import { ComputedRef } from "vue";
/**
 * 进度条渐变色计算属性
 */
export interface UseGradient {
    /**
     * 直线渐变配置
     */
    linearGradient: ComputedRef<string>;
    /**
     * 环形渐变配置
     */
    circleGradient: ComputedRef<{ offset: string; color: string }[]>;
    /**
     * 是否启用渐变
     */
    isGradient: ComputedRef<boolean>;
}
/**
 * 进度条状态计算属性
 */
export interface UseProgressStatus {
    /**
     * 直线进度条状态class
     */
    inlineProgrssStatusClass: ComputedRef<Record<string, boolean>>;
    /**
     * 环形进度条状态class
     */
    circleProgrssStatusClass: ComputedRef<Record<string, boolean>>;
    /**
     * 是否要显示状态
     */
    shouldShowStatus: ComputedRef<boolean>;
}

export interface UseBackground {
    inlineProgressBackgroundStyle: ComputedRef<Record<string, boolean>>;
    shouldShowSuccessInlineProgressBackground: ComputedRef<boolean>;
}

export interface UseProgressType {
    isCircleStyle: ComputedRef<boolean>;
}

export interface UseStrokeWidth {
    strokeWidth: ComputedRef<any>;
}

export interface UseFormat {
    formatPercentageText: ComputedRef<any>;
}

export interface UseProgress {
    progressClass: ComputedRef<Record<string, boolean>>;
    inCircleProgressBarStyle: ComputedRef<Record<string, any>>;
    inCircleProgressBarClass: ComputedRef<Record<string, boolean>>;
    pathString: ComputedRef<string>;
    trailPathStyle: ComputedRef<any>;
    progressCirclePaths: ComputedRef<any>;
    gradientId: ComputedRef<number>;
    inlineProgressBackgroundStyle: ComputedRef<Record<string, any>>;
}

export interface UseSuccessPercent {
    inlineProgressSuccessBackgroundStyle: ComputedRef<Record<string, any>>;
    shouldShowSuccessInlineProgressBackground: ComputedRef<any>;
}
