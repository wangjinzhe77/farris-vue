import { computed } from "vue";
import { ProgressProps } from "../progress.props";
import { UseStrokeWidth } from "./types";

export function useStrokeWidth(props: ProgressProps): UseStrokeWidth {
    const strokeWidth = computed(() => {
        return props.strokeWidth || (props.progressType === 'line' && props.size !== 'small' ? 8 : 6);
    });
    return { strokeWidth };
}
