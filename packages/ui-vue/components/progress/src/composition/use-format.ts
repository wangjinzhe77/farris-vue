import { computed } from "vue";
import { ProgressProps } from "../progress.props";
import { UseFormat } from "./types";

export function useFormat(props: ProgressProps): UseFormat{
    const formatPercentageText = computed(() => {
        return props.format(props.percent, props.successPercent);
    });
    return {formatPercentageText};
}
