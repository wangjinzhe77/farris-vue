 
import { SetupContext, defineComponent, ref, watch } from 'vue';
import { ProgressProps, progressProps } from './progress.props';

import { useGradient } from './composition/use-gradient';
import { useStrokeWidth } from './composition/use-stroke-width';
import { useFormat } from './composition/use-format';
import { useProgress } from './composition/use-progress';
import { useProgressType } from './composition/use-progress-type';
import { useProgressStatus } from './composition/use-progress-status';
import { useSuccessPercent } from './composition/use-success-percent';

export default defineComponent({
    name: 'FProgress',
    props: progressProps,
    emits: [],
    setup(props: ProgressProps) {
        const progressType = ref(props.progressType);
        const showInfo = ref(props.showInfo);
        const percent = ref(props.percent);
        const strokeLinecap = ref(props.strokeLinecap);

        const gradientComposition = useGradient(props);
        const { isGradient, circleGradient } = gradientComposition;
        const strokeWidthComposition = useStrokeWidth(props);
        const { strokeWidth } = strokeWidthComposition;
        const { formatPercentageText } = useFormat(props);
        const progressTypeComposition = useProgressType(props);
        const { isCircleStyle } = progressTypeComposition;
        const { progressClass, inCircleProgressBarStyle, inCircleProgressBarClass, pathString, trailPathStyle, progressCirclePaths, gradientId, inlineProgressBackgroundStyle } = useProgress(props, progressTypeComposition, gradientComposition, strokeWidthComposition);
        const { inlineProgrssStatusClass, circleProgrssStatusClass, shouldShowStatus } = useProgressStatus(props);
        const { inlineProgressSuccessBackgroundStyle, shouldShowSuccessInlineProgressBackground } = useSuccessPercent(props);
        watch(
            () => props.percent,
            (value: number) => {
                percent.value = value || 0;
            }
        );

        function renderProgressInfo() {
            return (
                showInfo.value && (
                    <span class="f-progress-text">
                        {shouldShowStatus.value
                            ? (<span class={isCircleStyle.value ? circleProgrssStatusClass.value : inlineProgrssStatusClass.value}></span>)
                            : formatPercentageText.value}
                    </span>)
            );
        }
        function renderProgressBarInLine() {
            return (
                <>
                    <div class="f-progress-outer">
                        <div class="f-progress-inner">
                            <div class="f-progress-bg" style={inlineProgressBackgroundStyle.value}></div>
                            {shouldShowSuccessInlineProgressBackground.value && (
                                <div class="f-progress-success-bg" style={inlineProgressSuccessBackgroundStyle.value}></div>
                            )}
                        </div>
                    </div>
                    {renderProgressInfo()}
                </>
            );
        }
        function renderProgressBarInCircle() {
            return (
                <div class={inCircleProgressBarClass.value} style={inCircleProgressBarStyle.value} >
                    <svg class="f-progress-circle" viewBox="0 0 100 100">
                        {isGradient.value && (
                            <defs>
                                <linearGradient id={'gradient-' + gradientId.value} x1="100%" y1="0%" x2="0%" y2="0%">
                                    {circleGradient.value.map((griadient: any) => {
                                        return <stop offset={griadient.offset} stop-color={griadient.color}></stop>;
                                    })}
                                </linearGradient>
                            </defs>
                        )}
                        <path
                            class="f-progress-circle-trail"
                            stroke="#efefef"
                            fill-opacity="0"
                            stroke-width={strokeWidth.value}
                            d={pathString.value}
                            style={trailPathStyle.value}></path>
                        {progressCirclePaths.value.map((circlePath: any) => {
                            return (
                                <path
                                    class="f-progress-circle-path"
                                    fill-opacity="0"
                                    d={pathString.value}
                                    stroke-linecap={strokeLinecap.value}
                                    stroke={circlePath.stroke}
                                    stroke-width={percent.value ? strokeWidth.value : 0}
                                    style={circlePath.strokePathStyle}></path>
                            );
                        })}
                    </svg>
                    {renderProgressInfo()}
                </div >
            );
        }

        return () => {
            return (
                <div class={progressClass.value}>
                    {progressType.value === 'line' && renderProgressBarInLine()}
                    {isCircleStyle.value && renderProgressBarInCircle()}
                </div>
            );
        };
    }
});
