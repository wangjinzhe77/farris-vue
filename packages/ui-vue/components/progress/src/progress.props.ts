 
 
import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import progressSchema from './schema/progress.schema.json';
import propertyConfig from './property-config/progress.property-config.json';

export type ProgressType = 'line' | 'circle' | 'dashboard';
export type ProgressSize = 'default' | 'small';
export type ProgressStatus = 'default' | 'success' | 'error' | 'active';
export type ProgressStrokeColorType = '';
export type ProgressStrokeLinecapType = 'round' | 'square';
export type GapPosition = 'top' | 'bottom' | 'left' | 'right';
export interface FProgressGradientProgress { [percent: string]: string }

export const progressProps = {
    progressType: { type: String as PropType<ProgressType>, default: 'line' },
    strokeWidth: { type: Number, default: 0 },
    size: { type: String as PropType<ProgressSize>, default: 'default' },
    showInfo: { type: Boolean, default: true },
    progressStatus: { type: String as PropType<ProgressStatus>, default: 'default' },
    successPercent: { type: Number, default: 0 },
    strokeColor: { type: String as PropType<ProgressStrokeColorType>, default: '' },
    strokeLinecap: { type: String as PropType<ProgressStrokeLinecapType>, default: 'round' },
    width: { type: Number, default: 100 },
    percent: { type: Number, default: 20 },
    /**
     * 启用成功分段背景图片
     */
    enableBackgroundImg: { type: Boolean, default: false },
    /**
     * 成功分段背景图片
     */
    backgroundImg: { type: String, default: '' },
    /**
     * 仪表盘缺口位置
     */
    gapPosition: { type: String as PropType<GapPosition> },
    /**
     * 仪表盘缺口角度，默认0°
     */
    gapDegree: { type: Number, default: 0 },
    /**
     * 格式化进度信息
     */
    format: { type: Function, default: (percent: number, success: number) => { return `${percent}%`; } }
} as Record<string, any>;

export type ProgressProps = ExtractPropTypes<typeof progressProps>;

export const propsResolver = createPropsResolver<ProgressProps>(progressProps, progressSchema, schemaMapper, schemaResolver, propertyConfig);
