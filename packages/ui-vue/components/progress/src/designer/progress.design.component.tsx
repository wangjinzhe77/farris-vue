import { SetupContext, computed, defineComponent, inject, onMounted, ref, watch } from 'vue';
import { ProgressProps, progressProps } from '../progress.props';

import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';

export default defineComponent({
    name: 'FProgressDesign',
    props: progressProps,
    emits: [],
    setup(props: ProgressProps, context) {
        const progressStatus = ref(props.progressStatus);
        const progressType = ref(props.progressType);
        const size = ref(props.size);
        const showInfo = ref(props.showInfo);
        const strokeWidth = ref(props.strokeWidth);
        const percent = ref(props.percent);
        const strokeLinecap = ref(props.strokeLinecap);
        const isGradient = ref(false);
        const strokeColor = ref(props.strokeColor);
        const lineGradient = ref(null);
        const successPercent = ref(0);
        const width = ref(props.width);
        const gradientId = ref('');
        const circleGradient = ref([]);
        const pathString = ref('');
        const progressCirclePath = ref([]);
        const enableBackgroundImg = ref(props.enableBackgroundImg);
        const backgroundImg = ref(props.backgroundImg);
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const componentInstance = useDesignerComponent(elementRef, designItemContext);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        watch(
            () => props.percent,
            (value: number) => {
                percent.value = value || 0;
            }
        );

        const strokeW = computed(() => {
            return strokeWidth.value || (progressType.value === 'line' && size.value !== 'small' ? 8 : 6);
        });

        const isCircleStyle = computed(() => {
            return progressType.value === 'circle' || progressType.value === 'dashboard';
        });

        const progressClass = computed(() => {
            const classObject = {
                'f-progress': true,
                'f-progress-line': progressType.value === 'line',
                'ant-progress-small': size.value === 'small',
                'f-progress-show-info': showInfo.value,
                '.f-progress-circle': isCircleStyle.value
            } as Record<string, boolean>;
            const statusKey = `f-progress-status-${progressStatus.value}`;
            classObject[statusKey] = true;
            return classObject;
        });

        const inlineProgressBackgroundStyle = computed(() => {
            const styleObject = {
                width: `${percent.value}%`,
                'border-radius': strokeLinecap.value === 'round' ? '100px' : '0',
                backgroud: !isGradient.value ? strokeColor.value : null,
                'background-image': isGradient.value ? lineGradient.value : null,
                height: `${strokeW.value}px`
            } as Record<string, any>;
            return styleObject;
        });

        const inlineProgressSuccessBackgroundStyle = computed(() => {
            const styleObject = {
                width: `${percent.value}%`,
                'border-radius': strokeLinecap.value === 'round' ? '100px' : '0',
                height: `${strokeW.value}px`,
                'background-image': enableBackgroundImg.value ? backgroundImg.value : null
            } as Record<string, any>;
            return styleObject;
        });

        const shouldShowSuccessInlineProgressBackground = computed(() => {
            return successPercent.value || successPercent.value === 0;
        });

        function renderProgressBarInLine() {
            return (
                <div class="f-progress-outer">
                    <div class="f-progress-inner">
                        <div class="f-progress-bg" style={inlineProgressBackgroundStyle.value}></div>
                        {shouldShowSuccessInlineProgressBackground.value && (
                            <div class="f-progress-success-bg" style={inlineProgressSuccessBackgroundStyle.value}></div>
                        )}
                    </div>
                </div>
            );
        }

        const inCircleProgressBarStyle = computed(() => {
            const styleObject = {
                width: `${width.value}px`,
                height: `${width.value}px`,
                'font-size': `${width.value * 0.15 + 6}px`,
                'f-progress-circle-gradient': isGradient.value
            } as Record<string, any>;
            return styleObject;
        });

        const trailPathStyle = computed(() => {
            const styleObject = {} as Record<string, any>;
            return styleObject;
        });

        function renderProgressBarInCircle() {
            return (
                <div class="f-progress-inner" style={inCircleProgressBarStyle.value}>
                    <svg class="f-progress-circle" viewBox="0 0 100 100">
                        {isGradient.value && (
                            <defs>
                                <linearGradient id={'gradient-' + gradientId.value} x1="100%" y1="0%" x2="0%" y2="0%">
                                    {circleGradient.value.map((griadient: any) => {
                                        return <stop offset={griadient.offset} stop-color={griadient.color}></stop>;
                                    })}
                                </linearGradient>
                            </defs>
                        )}
                        <path
                            class="f-progress-circle-trail"
                            stroke="#efefef"
                            fill-opacity="0"
                            stroke-width={strokeW.value}
                            d={pathString.value}
                            style={trailPathStyle.value}></path>
                        {progressCirclePath.value.map((circlePath: any) => {
                            return (
                                <path
                                    class="f-progress-circle-path"
                                    fill-opacity="0"
                                    d={pathString.value}
                                    stroke-linecap={strokeLinecap.value}
                                    stroke={circlePath.stroke}
                                    stroke-width={percent.value ? strokeW.value : 0}
                                    style={circlePath.strokePathStyle}></path>
                            );
                        })}
                    </svg>
                </div>
            );
        }

        return () => {
            return (
                <div ref={elementRef} class={progressClass.value}>
                    {progressType.value === 'line' && renderProgressBarInLine()}
                    {isCircleStyle.value && renderProgressBarInCircle()}
                </div>
            );
        };
    }
});
