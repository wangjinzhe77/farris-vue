 
import Progress from './src/progress.component';
import FProgressDesign from './src/designer/progress.design.component';
import { propsResolver } from './src/progress.props';
import { withInstall } from '@farris/ui-vue/components/common';

export * from './src/progress.props';
Progress.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.progress = Progress;
    propsResolverMap.progress = propsResolver;
};
Progress.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.progress = FProgressDesign;
    propsResolverMap.progress = propsResolver;
};

export { Progress };
export default withInstall(Progress);
