import { UpdateColumnsResolver } from "./types";

export function createDataViewUpdateColumnsResolver(): UpdateColumnsResolver {
    function updateColumns(component: any, schema: Record<string, any>) {
        const { columns } = schema;
        return component.updateColumns(columns);
    }
    return {
        updateColumns
    };
}
