import { cloneDeep, isPlainObject } from "lodash-es";
import { DesignerHostService } from "../../designer-canvas/src/composition/types";
import { MapperFunction, SchemaResolverFunction } from "./types";

const schemaMap = {} as Record<string, any>;
const schemaResolverMap = {} as Record<string, SchemaResolverFunction>;

function getSchemaValueByDefault(defaultSchema: Record<string, any>): Record<string, any> {
    const { properties, title, ignore: ignoreList } = defaultSchema as Record<string, any>;
    const canIgnoreProperty = ignoreList && Array.isArray(ignoreList);
    const resolvedSchema = Object.keys(properties).reduce((propsObject: Record<string, any>, propKey: string) => {
        if (!canIgnoreProperty || !ignoreList.find(item => item === propKey)) {
            propsObject[propKey] = (properties[propKey].type === 'object' && !!properties[propKey].properties) ?
                getSchemaValueByDefault(properties[propKey]) : cloneDeep(properties[propKey].default);
        }
        return propsObject;
    }, {});
    if (title && (!canIgnoreProperty || !ignoreList.find(item => item === 'id'))) {
        const typePrefix = title.toLowerCase().replace(/-/g, '_');
        resolvedSchema.id = `${typePrefix}_${Math.random().toString().slice(2, 6)}`;
    }
    return resolvedSchema;
}
/**
 * 获取控件元数据，只组装必填的字段
 */
function getRequiredSchemaValueByDefault(defaultSchema: Record<string, any>): Record<string, any> {
    const { properties, title, required: requiredProperty } = defaultSchema as Record<string, any>;
    if (requiredProperty && Array.isArray(requiredProperty)) {
        const resolvedSchema = requiredProperty.reduce((propsObject: Record<string, any>, propKey: string) => {

            propsObject[propKey] = (properties[propKey].type === 'object' && !!properties[propKey].properties) ?
                getSchemaValueByDefault(properties[propKey]) : cloneDeep(properties[propKey].default);

            return propsObject;
        }, {});
        if (title && requiredProperty.find(item => item === 'id')) {
            const typePrefix = title.toLowerCase().replace(/-/g, '_');
            resolvedSchema.id = `${typePrefix}_${Math.random().toString().slice(2, 6)}`;
        }
        return resolvedSchema;
    }
    return {
        type: title
    };

}
function getSchemaByType(componentType: string, resolveContext: Record<string, any> = {}, designerHostService?: DesignerHostService)
    : Record<string, any> | null {
    const defaultSchema = schemaMap[componentType];
    if (defaultSchema) {
        let componentSchema = getRequiredSchemaValueByDefault(defaultSchema);
        const schemaResolver = schemaResolverMap[componentType];
        componentSchema = schemaResolver ? schemaResolver({ getSchemaByType }, componentSchema, resolveContext, designerHostService)
            : componentSchema;
        return componentSchema;
    }
    return null;
}

function resolveSchema(schemaValue: Record<string, any>, defaultSchema: Record<string, any>): Record<string, any> {

    const resolvedSchema = getSchemaValueByDefault(defaultSchema);

    Object.keys(resolvedSchema).reduce((resolvedSchema: Record<string, any>, propKey: string) => {
        if (Object.prototype.hasOwnProperty.call(schemaValue, propKey)) {
            // 解决属性是对象类型，默认值被冲掉的情况
            // 增加非判断，解决针对属性时对象，但是schemaValue=null或者undefined情况
            if (resolvedSchema[propKey] && isPlainObject(resolvedSchema[propKey]) && (isPlainObject(schemaValue[propKey] || !schemaValue[propKey]))) {
                Object.assign(resolvedSchema[propKey], schemaValue[propKey] || {});
            } else {
                resolvedSchema[propKey] = schemaValue[propKey];
            }
        }

        return resolvedSchema;
    }, resolvedSchema);

    return resolvedSchema;
};

function mappingSchemaToProps(resolvedSchema: Record<string, any>, schemaMapper: Map<string, string | MapperFunction>) {
    const props = Object.keys(resolvedSchema)
        .filter((propKey: string) => resolvedSchema[propKey] != null)
        .reduce((resolvedProps: Record<string, any>, propKey: string) => {
            if (schemaMapper.has(propKey)) {
                const mapper = schemaMapper.get(propKey) as string | MapperFunction;
                if (typeof mapper === 'string') {
                    resolvedProps[mapper] = resolvedSchema[propKey];
                } else {
                    const mapperResult = (mapper as MapperFunction)(propKey, resolvedSchema[propKey], resolvedSchema);
                    Object.assign(resolvedProps, mapperResult);
                }
            } else {
                resolvedProps[propKey] = resolvedSchema[propKey];
            }
            return resolvedProps;
        }, {});
    return props;
}

function resolveSchemaToProps(
    schemaValue: Record<string, any>,
    defaultSchema: Record<string, any>,
    schemaMapper: Map<string, string | MapperFunction> = new Map()
): Record<string, any> {
    const resolvedSchema = resolveSchema(schemaValue, defaultSchema);
    const props = mappingSchemaToProps(resolvedSchema, schemaMapper);
    return props;
}

function resolveSchemaWithDefaultValue(schemaValue: Record<string, any>): Record<string, any> {
    const componentType = schemaValue.type;
    if (componentType) {
        const defaultSchema = schemaMap[componentType];
        if (!defaultSchema) {
            return schemaValue;
        }
        const resolvedSchema = resolveSchema(schemaValue, defaultSchema);
        const editorType = schemaValue.editor?.type || '';
        /* 解决schemeValue结构如下图场景，在editor下，获取不到date-picker类型的默认值的问题
        * {type:'input-group',...,editor:{type:'date-picker',...}}       
        */
        if (editorType) {
            const defaulEditorSchema = schemaMap[editorType];
            const resolvedEditorSchema = resolveSchema(schemaValue.editor, defaulEditorSchema);
            resolvedSchema.editor = resolvedEditorSchema;
        }
        return resolvedSchema;
    }
    return schemaValue;
}

export { getSchemaByType, resolveSchemaWithDefaultValue, resolveSchemaToProps, schemaMap, schemaResolverMap, mappingSchemaToProps };
