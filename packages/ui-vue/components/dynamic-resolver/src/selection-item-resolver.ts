import { SelectionItemResolver } from "./types";

export function createDataGridSelectionItemResolver(): SelectionItemResolver {
    function selectItemById(component: any, id: string) {
        return component.selectItemById(id);
    }
    return {
        selectItemById
    };
}

export function createTreeGridSelectionItemResolver(): SelectionItemResolver {
    function selectItemById(component: any, id: string) {
        return component.selectItemById(id);
    }
    return {
        selectItemById
    };
}
