 
import { EventDispatcher } from "./types";

export function createEventsResolver() {
    return (component: any, schema: Record<string, any>, dispatcher: EventDispatcher) => {
        const token = schema.id;
        const { type } = schema;
        const events = component.emits as any[];
        if (!events) {
            return {};
        }
        return events.filter((eventName: string) => eventName !== 'update:modelValue').reduce((mergedEvents: Record<string, any>, eventName: string) => {
            const eventNameProp = `on${eventName.charAt(0).toUpperCase()}${eventName.slice(1)}`;
            mergedEvents[eventNameProp] = (...payloads) => {
                dispatcher.dispatch(token, eventName, type, payloads);
            };
            return mergedEvents;
        }, {});
    };
}
