import { computed, ref } from "vue";
import { EffectFunction, PropertyConverter, SchemaService } from './types';
import { EditorConfig } from "../../dynamic-form";
import { useObjectExpression } from './object-expression';
import { ComponentSchema } from "../../designer-canvas/src/types";
import { resolveSchemaWithDefaultValue } from "./schema-resolver";
import appearanceConverter from './converter/appearance.converter';
import buttonsConverter from "./converter/buttons.converter";
import propertyEditorConverter from "./converter/property-editor.converter";
import typeConverter from "./converter/type.converter";
import changeEditorConverter from "./converter/change-editor.converter";
import fieldSelectorConverter from "./converter/field-selector.converter";
import paginationConverter from "./converter/pagination.converter";
import rowNumberConverter from "./converter/row-number.converter";
import gridSelectionConverter from "./converter/grid-selection.converter";
import { ElementPropertyConfig, PropertyEntity } from "../../property-panel/src/composition/entity/property-entity";
import itemsCountConverter from "./converter/items-count.converter";
import enumDataConverter from "./converter/enum-data.converter";
import formGroupLabelConverter from "./converter/form-group-label.converter";

const propertyConfigSchemaMap = {} as Record<string, any>;
const propertyConverterMap = new Map<string, PropertyConverter>([
    ['/converter/appearance.converter', appearanceConverter],
    ['/converter/buttons.converter', buttonsConverter],
    ['/converter/property-editor.converter', propertyEditorConverter],
    ['/converter/items-count.converter', itemsCountConverter],
    ['/converter/type.converter', typeConverter],
    ['/converter/change-editor.converter', changeEditorConverter],
    ['/converter/form-group-label.converter', formGroupLabelConverter],
    ['/converter/field-selector.converter', fieldSelectorConverter],
    ['/converter/pagination.converter', paginationConverter],
    ['/converter/row-number.converter', rowNumberConverter],
    ['/converter/grid-selection.converter', gridSelectionConverter],
    ['/converter/enum-data.converter', enumDataConverter]
]);
const propertyEffectMap = {} as Record<string, EffectFunction>;
const propertyEditorMap = new Map<string, EditorConfig>([
    ['string', { type: 'input-group', enableClear: false }],
    ['boolean', {
        "type": "combo-list",
        "textField": "name",
        "valueField": "value",
        "idField": "value",
        "enableClear": false,
        "editable": false,
        "maxHeight": 64,
        "data": [
            {
                "value": true,
                "name": "是"
            },
            {
                "value": false,
                "name": "否"
            }
        ]
    }],
    ['enum', { "type": "combo-list", "maxHeight": 128, "enableClear": false, "editable": false }],
    ['array', { "type": "button-edit" }],
    ['number', { "type": "number-spinner", "placeholder": "" }],
    ['events-editor', { "type": "events-editor", hide: true }]
]);

const useObjectExpressionComponstion = useObjectExpression();

function generateBooleanValue(pvalueSchema: Record<string, any>, propertyConfigMap: Record<string, PropertyEntity>) {
    return () => useObjectExpressionComponstion.parseValueSchema(pvalueSchema, propertyConfigMap);
}

function isVisible(propertySchemaKeys: string[], propertySchema: Record<string, any>, propertyConfigMap: Record<string, PropertyEntity>)
    : boolean | (() => boolean) {
    if (propertySchemaKeys.includes('visible') && propertySchema.visible !== undefined) {
        return typeof propertySchema.visible === 'boolean' ?
            () => Boolean(propertySchema.visible) :
            propertySchema.visible === undefined ? true : generateBooleanValue(propertySchema.visible, propertyConfigMap);
    }
    return () => true;
}

function isReadonly(propertySchemaKeys: string[], propertySchema: Record<string, any>, propertyConfigMap: Record<string, PropertyEntity>) {
    if (propertySchemaKeys.includes('readonly') && propertySchema.readonly !== undefined) {
        return typeof propertySchema.readonly === 'boolean' ?
            () => Boolean(propertySchema.readonly) :
            generateBooleanValue(propertySchema.readonly, propertyConfigMap);
    }
    return () => false;
}

function tryGetPropertyConverter(propertySchema: Record<string, any>, categoryConverter): PropertyConverter | null {
    const $converter = propertySchema['$converter'] || categoryConverter;
    if (typeof $converter === 'string') {
        if ($converter && propertyConverterMap.has($converter)) {
            return propertyConverterMap.get($converter) || null;
        }
    }
    return $converter || null;
}

/**
 * 
 * @param propertiesInCategory 
 * 举例：
 * visible: {
        description: "运行时组件是否可见",
        title: "是否可见",
        type: "boolean"   
    }
 其中type属性 这个属性用来控制编辑器是哪一种，对应关系在propertyEditorMap中定义，boolean指定了下拉
 * @param propertyConfigMap 
 * @param editingSchema 
 * @param rawSchema 
 * @param schemaService 
 * @returns 
 */
function getPropertyEntities(
    propertiesInCategory: Record<string, any>,
    propertyConfigMap: Record<string, PropertyEntity>,
    editingSchema: ComponentSchema,
    rawSchema: ComponentSchema,
    schemaService: SchemaService,
    componentId = '',
    categoryConverter: any = ''
): PropertyEntity[] {
    const propertyEntities = Object.keys(propertiesInCategory).map<PropertyEntity>((propertyKey: string) => {
        const updateCount = ref(1);
        const propertyID = propertyKey;
        const propertySchema = propertiesInCategory[propertyKey];
        const propertySchemaKeys = Object.keys(propertySchema);
        const propertyName = propertySchema.title;
        const propertyType = propertySchema.type;
        const defaultEditor = propertyEditorMap.get(propertyType) || { type: 'input-group', enableClear: false };
        const editor = propertySchema.editor ? Object.assign({}, defaultEditor, propertySchema.editor) as EditorConfig : Object.assign({}, defaultEditor);
        const visible = isVisible(propertySchemaKeys, propertySchema, propertyConfigMap);
        const readonly = isReadonly(propertySchemaKeys, propertySchema, propertyConfigMap);
        editor.readonly = editor.readonly === undefined ? readonly() : editor.readonly;
        const cascadeConfig = propertySchema.type === 'cascade' ? getPropertyEntities(propertySchema.properties, propertyConfigMap, editingSchema, rawSchema, schemaService, componentId, categoryConverter) : [];
        const hideCascadeTitle = true;
        let converter = tryGetPropertyConverter(propertySchema, categoryConverter);
        // const propertyValue = ref(converter ? converter.convertFrom(schema, propertyKey) : schema[propertyKey]);
        const propertyValue = computed({
            get() {
                if (updateCount.value) {
                    // class、style 统一处理
                    if (['class', 'style'].find(id => id === propertyID) && !converter) {
                        converter = propertyConverterMap.get('/converter/appearance.converter') || null;
                    }
                    if (converter && converter.convertFrom) {
                        return converter.convertFrom(editingSchema, propertyKey, schemaService, componentId);
                    }
                    // 获取属性时，如果没有convertForm，并且通过在Schema上获取得值是空，那就获取defaultValue属性值或者是空
                    const editingSchemaValue = editingSchema[propertyKey];
                    return typeof editingSchemaValue == 'string' && editingSchemaValue === '' ? propertySchema['defaultValue'] || '' : editingSchemaValue;
                }
                return null;
            },
            set(newValue) {
                updateCount.value += 1;
                if (converter && converter.convertTo) {
                    converter.convertTo(rawSchema, propertyKey, newValue, schemaService, componentId);
                    converter.convertTo(editingSchema, propertyKey, newValue, schemaService, componentId);
                } else {
                    rawSchema[propertyKey] = newValue;
                    editingSchema[propertyKey] = newValue;
                }
            }
        });
        const { refreshPanelAfterChanged, description, isExpand } = propertySchema;
        const propertyEntity = { propertyID, propertyName, propertyType, propertyValue, editor, visible, readonly, cascadeConfig, hideCascadeTitle, refreshPanelAfterChanged, description, isExpand };
        propertyConfigMap[propertyID] = propertyEntity;
        return propertyEntity;
    });
    return propertyEntities;
}

function getPropertyConfigByType(schemaType: string, schemaService: SchemaService, schema = {} as ComponentSchema): ElementPropertyConfig[] {
    const propertyConfigMap = {} as Record<string, PropertyEntity>;
    const propertyConfigSchema = propertyConfigSchemaMap[schemaType];
    if (propertyConfigSchema && propertyConfigSchema.categories) {
        const propertyConfigs = Object.keys(propertyConfigSchema.categories).map<ElementPropertyConfig>((categoryId: string) => {
            const propertyCategory = propertyConfigSchema.categories[categoryId];
            const categoryName = propertyCategory?.title;
            const properties = getPropertyEntities(propertyCategory.properties || {}, propertyConfigMap, {} as ComponentSchema, schema, schemaService);
            return { categoryId, categoryName, properties };
        });
        return propertyConfigs;
    }
    return [];
}

function tryToResolveReference(categoryId: string, propertyCategory: Record<string, any>, rawSchema: ComponentSchema, schemaService: SchemaService, componentId = '') {
    const refSchemaPath = propertyCategory.$ref.schema;
    const $converter = propertyCategory.$ref.converter;
    const refSchema = rawSchema[refSchemaPath];

    const schemaType = refSchema.type;
    const editingSchema = resolveSchemaWithDefaultValue(refSchema) as ComponentSchema;
    const propertyConfigMap = {} as Record<string, PropertyEntity>;
    const propertyConfigSchema = propertyConfigSchemaMap[schemaType];
    if (propertyConfigSchema && propertyConfigSchema.categories) {
        const propertyCategory = propertyConfigSchema.categories[categoryId];
        const categoryName = propertyCategory?.title;
        if ($converter) {
            Object.keys(propertyCategory.properties).forEach((propertyKey: any) => {
                propertyCategory.properties[propertyKey].$converter = $converter;
            });
        }
        const propertiesInCategory = propertyCategory?.properties || {};
        const properties = getPropertyEntities(propertiesInCategory, propertyConfigMap, editingSchema, refSchema, schemaService, componentId);
        return { categoryId, categoryName, properties };

    }
    return { categoryId, categoryName: '', properties: [] };
}

// jumphere
function getPropertyConfigBySchema(rawSchema: ComponentSchema, schemaService: SchemaService, designerItem: any, componentId: string, propertyConfig?: Record<string, any>): ElementPropertyConfig[] {
    const schemaType = rawSchema.type;
    const editingSchema = resolveSchemaWithDefaultValue(rawSchema) as ComponentSchema;
    const propertyConfigMap = {} as Record<string, PropertyEntity>;

    // 先从ConfigMap中取简单类属性，若找不到，则在控件实例中取复杂属性
    let propertyConfigSchema = propertyConfig || propertyConfigSchemaMap[schemaType];
    if (propertyConfigSchema && Object.keys(propertyConfigSchema).length === 0 && designerItem && designerItem.getPropConfig) {
        propertyConfigSchema = designerItem.getPropConfig(componentId);
    }
    if (propertyConfigSchema && propertyConfigSchema.categories) {
        const propertyConfigs = [] as Array<ElementPropertyConfig>;
        Object.keys(propertyConfigSchema.categories).map((categoryId: string) => {
            const propertyCategory = propertyConfigSchema.categories[categoryId];
            if (propertyCategory.$ref) {
                propertyConfigs.push(tryToResolveReference(categoryId, propertyCategory, rawSchema, schemaService, componentId) as ElementPropertyConfig);
                return;
            }
            const categoryName = propertyCategory?.title;
            const tabId = propertyCategory?.tabId;
            const tabName = propertyCategory?.tabName;
            const hide = propertyCategory?.hide;
            const hideTitle = propertyCategory?.hideTitle;
            const properties = getPropertyEntities(propertyCategory.properties || {}, propertyConfigMap, editingSchema, rawSchema, schemaService, componentId, propertyCategory['$converter']);
            const { setPropertyRelates } = propertyCategory;
            propertyConfigs.push({ categoryId, categoryName, tabId, tabName, hide, properties, hideTitle, setPropertyRelates });
        });
        return propertyConfigs;
    }
    return [];
}

export { getPropertyConfigBySchema, getPropertyConfigByType, propertyConfigSchemaMap, propertyConverterMap, propertyEffectMap };
