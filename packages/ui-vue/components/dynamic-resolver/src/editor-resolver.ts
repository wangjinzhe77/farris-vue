import { EditorResolver } from "./types";

export function createFormGroupEditorResolver(): EditorResolver {
    function resolve(viewSchema: Record<string, any>) {
        return viewSchema.editor;
    }
    return {
        resolve
    };
}
