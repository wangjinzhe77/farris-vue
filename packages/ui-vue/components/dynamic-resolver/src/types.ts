 
import { DesignerHostService } from "../../designer-canvas/src/composition/types";

export type MapperFunction = (key: string, value: any, resolvedSchema?: any) => Record<string, any>;

export interface SchemaService {

    closest: (componentId: string, componentType: string) => Record<string, any> | null;

    getSchemaById: (string) => Record<string, any>;

    load: (componentSchema: Record<string, any>) => void;

    select: (root: Record<string, any>, predicate: (child: Record<string, any>) => boolean) => Record<string, any>;

    getRealEditorType: (editorType: string) => string;
}

export interface PropertyConverter {

    convertTo: (schema: Record<string, any>, propertyKey: string, propertyValue: any, schemaService: SchemaService, componentId?: string) => void;

    convertFrom: (schema: Record<string, any>, propertyKey: string, schemaService: SchemaService, componentId?: string) => any;
}

export interface DynamicResolver {
    getSchemaByType: (componentType: string, resolveContext?: Record<string, any>, designerHostService?: DesignerHostService) => Record<string, any> | null;
    getPropertyConfigByType?: (componentType: string) => Record<string, any> | null;
};

export type SchemaResolverFunction = (
    dynamicResolver: DynamicResolver,
    schema: Record<string, any>,
    resolveContext: Record<string, any>,
    designerHostService?: DesignerHostService
) => Record<string, any>;

export type EffectFunction = (source: Record<string, any>) => Record<string, any>;

export interface EventDispatcher {
    dispatch(token: string, eventName: string, ...payloads);
}

export interface BindingModel {
    getValue(field: string);
    setValue(field: string, value: any);
    getList(dataSource: string);
    updateData(...payloads);
    dataMapping(...payloads);
}

export interface BindingData {
    getValue(elementId: string);
    setValue(elementId: string, field: string, value: any);
}
export interface BindingResolver {
    resolve(schema: Record<string, any>, bindingData: BindingData);
}

export interface SelectionItemResolver {
    selectItemById(component: any, id: string);
}

export interface EditorResolver {
    resolve(schema: Record<string, any>);
}

export interface UpdateColumnsResolver {
    updateColumns(component: any ,schema: Record<string, any>);
}

export interface ViewEvent {
    token: string;
    type: string;
    name: string;
    payloads: any[];
}
export interface EventHandlerResolver {
    resolve(schema: Record<string, any>, event: ViewEvent);
}
export interface Caller {
    call: (methodName: string, ...payloads: any[]) => any;
}
