import { PropertyConverter } from "../types";

export default {
    convertFrom: (schema: Record<string, any>, propertyKey: string) => {
        // return schema['binding'] ? schema['binding']['type'] + ":" + schema['binding']['path'] : '';
        return schema['binding'] ? schema['binding']['path'] : '';

    },
    convertTo: (schema: Record<string, any>, propertyKey: string, propertyValue: any) => {
        if (propertyValue && propertyValue.length > 0) {
            const bindingData = propertyValue[0];

            if (!schema.binding) {
                schema.binding = {};
            }

            schema.binding.type = 'Form';
            schema.binding.path = bindingData.bindingField;
            schema.binding.field = bindingData.id;
            schema.binding.fullPath = bindingData.path;
            schema.path = bindingData.bindingPath;
        }
    }
} as PropertyConverter;;
