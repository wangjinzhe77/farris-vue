import { ComponentSchema } from "../../../designer-canvas/src/types";
import { DgControl } from "../../../designer-canvas/src/composition/dg-control";
import { PropertyConverter, SchemaService } from "../types";

export default {
    convertFrom: (schema: ComponentSchema, propertyKey: string, schemaService: SchemaService) => {
        const foundValue=schema.editor&&schema.editor[propertyKey]?schema.editor[propertyKey]:schema[propertyKey];
        return DgControl[foundValue]?.name||foundValue;
    }
} as PropertyConverter;
