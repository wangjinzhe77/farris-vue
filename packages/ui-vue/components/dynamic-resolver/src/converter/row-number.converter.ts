import { ComponentSchema } from "../../../designer-canvas/src/types";
import { PropertyConverter, SchemaService } from "../types";
export default {
    convertTo: (schema: ComponentSchema, propertyKey: string, propertyValue: any, schemaService: SchemaService) => {
        if (!schema.rowNumber) {
            schema.rowNumber = {};
        } 
        schema.rowNumber[propertyKey] = propertyValue;
    },
    convertFrom: (schema: ComponentSchema, propertyKey: string, schemaService: SchemaService) => {
        return schema.rowNumber ? schema.rowNumber[propertyKey] : schema[propertyKey];
    }
} as PropertyConverter;
