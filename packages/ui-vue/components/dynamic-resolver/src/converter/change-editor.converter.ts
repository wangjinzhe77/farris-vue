import { ComponentSchema } from "../../../designer-canvas/src/types";
import { PropertyConverter, SchemaService } from "../types";

export default {
    convertTo: (schema: ComponentSchema, propertyKey: string, propertyValue: any, schemaService: SchemaService) => {
        // eslint-disable-next-line no-self-assign
        schema[propertyKey] = schema[propertyKey];
    },
    convertFrom: (schema: ComponentSchema, propertyKey: string, schemaService: SchemaService) => {
        if (schema.editor) {
            return schemaService.getRealEditorType(schema.editor.type);
        }
        return '';
    }
} as PropertyConverter;
