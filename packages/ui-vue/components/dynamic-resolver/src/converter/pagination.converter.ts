import { ComponentSchema } from "../../../designer-canvas/src/types";
import { PropertyConverter, SchemaService } from "../types";
/**
 * NG版，此处pagination类型是布尔
 */
export default {
    convertTo: (schema: ComponentSchema, propertyKey: string, propertyValue: any, schemaService: SchemaService) => {
        if (!schema.pagination) {
            schema.pagination = {};
        } 
        schema.pagination[propertyKey] = propertyValue;
    },
    convertFrom: (schema: ComponentSchema, propertyKey: string, schemaService: SchemaService) => {
        return schema.pagination ? schema.pagination[propertyKey] : schema[propertyKey];
    }
} as PropertyConverter;
