import { PropertyConverter } from "../types";

export default {
    convertFrom: (schema: Record<string, any>, propertyKey: string) => {
        return schema[propertyKey] || '';
    },
    convertTo: (schema: Record<string, any>, propertyKey: string, propertyValue: any) => {
        schema[propertyKey] = propertyValue;
    }
} as PropertyConverter;;
