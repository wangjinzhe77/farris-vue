import { ComponentSchema } from "../../../designer-canvas/src/types";
import { PropertyConverter, SchemaService } from "../types";

export default {
    convertFrom: (schema: ComponentSchema, propertyKey: string, schemaService: SchemaService) => {
        return (schema[propertyKey] && schema[propertyKey].length) ? `共 ${schema[propertyKey].length} 项` : '';
    }
} as PropertyConverter;
