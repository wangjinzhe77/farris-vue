import { ComponentSchema } from "../../../designer-canvas/src/types";
import { PropertyConverter, SchemaService } from "../types";

export default {
    convertTo: (schema: ComponentSchema, propertyKey: string, propertyValue: any, schemaService: SchemaService) => {
        if (schema.editor) {
            schema.editor[propertyKey] = propertyValue;
        }
    },
    convertFrom: (schema: ComponentSchema, propertyKey: string, schemaService: SchemaService) => {
        return schema.editor && Object.prototype.hasOwnProperty.call(schema.editor, propertyKey) ?
            schema.editor[propertyKey] : schema[propertyKey];
    }
} as PropertyConverter;
