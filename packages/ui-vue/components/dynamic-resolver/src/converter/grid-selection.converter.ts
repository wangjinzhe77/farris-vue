import { ComponentSchema } from "../../../designer-canvas/src/types";
import { PropertyConverter, SchemaService } from "../types";
export default {
    convertTo: (schema: ComponentSchema, propertyKey: string, propertyValue: any, schemaService: SchemaService) => {
        if (!schema.selection) {
            schema.selection = {};
        } 
        schema.selection[propertyKey] = propertyValue;
    },
    convertFrom: (schema: ComponentSchema, propertyKey: string, schemaService: SchemaService) => {
        return schema.selection ? schema.selection[propertyKey] : schema[propertyKey];
    }
} as PropertyConverter;
