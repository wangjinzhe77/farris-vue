import { ComponentSchema } from "../../../designer-canvas/src/types";
import { PropertyConverter, SchemaService } from "../types";

export default {
    convertTo: (schema: ComponentSchema, propertyKey: string, propertyValue: any, schemaService: SchemaService) => {
        if (!schema.appearance) {     
            schema.appearance={};
        }
        schema.appearance[propertyKey] = propertyValue;
    },
    convertFrom: (schema: ComponentSchema, propertyKey: string, schemaService: SchemaService) => {
        return schema.appearance ? schema.appearance[propertyKey] : schema[propertyKey];
    }
} as PropertyConverter;
