import { BindingData, BindingResolver } from "./types";

export function createFormBindingResolver(): BindingResolver {
    function resolve(schema: Record<string, any>, bindingData: BindingData) {
        const { id } = schema || {};
        const { field } = schema.binding || {};
        return {
            'modelValue': bindingData.getValue(id),
            'onUpdate:modelValue': (value: any) => {
                bindingData.setValue(id, field, value);
            }
        };
    };
    return {
        resolve
    };
}

export function createCollectionBindingResolver(): BindingResolver {
    function resolve(schema: Record<string, any>, bindingData: BindingData) {
        const { id } = schema || {};
        const { dataSource } = schema || {};
        if (dataSource === undefined) {
            return {};
        }
        return {
            'data': bindingData.getValue(id),
            'onUpdate:data': (...payload: any[]) => {
                // bindingData.updateData(...payload);
            }
        };
    };
    return {
        resolve
    };
}

export function createTreeGridBindingResolver(): BindingResolver{
    function resolve(schema: Record<string, any>, bindingData: BindingData) {
        const { id } = schema || {};
        const { dataSource } = schema || {};
        if (dataSource === undefined) {
            return {};
        }
        return {
        };
    };
    return {
        resolve
    };
}

export function createDataMappingBindingResolver(): BindingResolver {
    function resolve(schema: Record<string, any>, bindingData: BindingData) {
        const { path } = schema.binding || {};
        if (!path) {
            return {};
        }
        return {
            'onUpdate:dataMapping': (...payloads: any[]) => {
                // bindingModel.dataMapping(...payloads);
            }
        };
    };
    return {
        resolve
    };
}
