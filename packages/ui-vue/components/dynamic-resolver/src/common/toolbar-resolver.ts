export function resolveToolbar(key: string, toolbarObject: { buttons: any[],appearance:any }) {
    const result = [] as any;
    toolbarObject?.buttons.map(button => {
        const newButton = {};
        Object.keys(button).map(key => {
            if (key === 'appearance') {
                newButton['class']=button[key]?.class||'';
            }else{
                newButton[key] = button[key];
            }
        });
        result.push(newButton);
    });
    return { buttons: result };
}
