import { EventHandlerResolver, ViewEvent } from "./types";

export function createEventHandlerResolver(): EventHandlerResolver {
    function resolve(schema: Record<string, any>, event: ViewEvent) {
        const { name } = event;
        return schema[`on${name.charAt(0).toUpperCase()}${name.slice(1)}`] || schema[name];
    };
    return {
        resolve
    };
}

export function createPageHeaderEventHandlerResolver(): EventHandlerResolver {
    function resolve(schema: Record<string, any>, event: ViewEvent) {
        const buttons = schema.toolbar?.buttons as any[];
        if (!buttons || buttons.length < 1) {
            return null;
        }
        const [payload, buttonId] = event.payloads;
        const button = buttons.find((button: Record<string, any>) => button.id === buttonId);
        if (!button) {
            return null;
        }
        return button.onClick || button.click;
    };
    return {
        resolve
    };
}

export function createTabsEventHandlerResolver(): EventHandlerResolver {
    function resolve(schema: Record<string, any>, event: ViewEvent) {
        const tabPages = schema.contents;
        if (!tabPages || tabPages.length < 1) {
            return null;
        }

        const buttons = tabPages.reduce((tabsButtons: any[], tabPage: any) => {
            const tabPageButtons = tabPage.toolbar && tabPage.toolbar.buttons || [];
            tabsButtons.push(...tabPageButtons);
            return tabsButtons;
        }, []);
        if (!buttons || buttons.length < 1) {
            return;
        }
        const [payload, buttonId] = event.payloads;
        const button = buttons.find((button: Record<string, any>) => button.id === buttonId);
        if (!button) {
            return null;
        }
        return button.onClick || button.click;
    };
    return {
        resolve
    };
}
export function createResponseToolbarEventHandlerResolver(): EventHandlerResolver {
    function resolve(schema: Record<string, any>, event: ViewEvent) {
        const buttons = schema.buttons as any[];
        if (!buttons || buttons.length < 1) {
            return null;
        }
        const [payload, buttonId] = event.payloads;
        const button = buttons.find((button: Record<string, any>) => button.id === buttonId);
        if (!button) {
            return null;
        }
        return button.onClick || button.click;
    };
    return {
        resolve
    };
}
export function createSectionEventHandlerResolver(): EventHandlerResolver {
    function resolve(schema: Record<string, any>, event: ViewEvent) {
        const buttons = schema.toolbar?.buttons as any[];
        if (!buttons || buttons.length < 1) {
            return null;
        }
        const [payload, buttonId] = event.payloads;
        const button = buttons.find((button: Record<string, any>) => button.id === buttonId);
        if (!button) {
            return null;
        }
        return button.onClick || button.click;
    };
    return {
        resolve
    };
}
