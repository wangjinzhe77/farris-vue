export function createVisiblePropResolver() {
    function resolve(schema: Record<string, any>) {
        return Object.prototype.hasOwnProperty.call(schema, 'visible') ? schema.visible : null;
    }
    return {
        resolve
    };
}
