 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App } from 'vue';
import ResponseLayoutEditor from './src/response-layout-editor.component';
import ResponseLayoutEditorSettingDesign from './src/designer/response-layout-editor-setting.design.component';
import { propsResolver } from './src/composition/props/response-layout-editor-setting.props';
import ResponseLayoutEditorSetting from './src/response-layout-editor-setting.component';
import { ResponseFormLayoutContext, type UseResponseLayoutEditorSetting } from './src/type';

export * from './src/composition/props/response-layout-editor.props';

export {
    ResponseLayoutEditorSetting,
    ResponseLayoutEditor, ResponseFormLayoutContext,
    UseResponseLayoutEditorSetting
};

export default {
    install(app: App): void {
        app.component(ResponseLayoutEditor.name as string, ResponseLayoutEditor);
        app.component(ResponseLayoutEditorSetting.name as string, ResponseLayoutEditorSetting);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>): void {
        componentMap['response-layout-editor-setting'] = ResponseLayoutEditorSetting;
        propsResolverMap['response-layout-editor-setting'] = propsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
        componentMap['response-layout-editor-setting'] = ResponseLayoutEditorSettingDesign;
        propsResolverMap['response-layout-editor-setting'] = propsResolver;
    }
};
