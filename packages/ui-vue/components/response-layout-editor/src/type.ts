import { ComponentSchema } from "@farris/ui-vue/components/designer-canvas";

export type ComboValue = {
    label: string;
    id: string;
}[];

/**
 * 响应式布局参数实体。
 * 默认按照小屏sm、中等md、大屏lg、超大屏el 划分屏幕大小
 * ux约定：小屏上显示1列（每个控件占12栅格），中屏显示2列（每个控件占6栅格），大屏显示4列（每个控件占3栅格），超大屏显示6列（每个控件占2栅格），
 */
export class ResponseFormLayoutContext {

    /** 控件标题 */
    public label: string = '';

    /** 控件id */
    public id: string = '';

    /** 每个控件占用的栅格数 */
    public columnInSM = 12;

    public columnInMD = 6;

    public columnInLG = 3;

    public columnInEL = 2;

    /** 每个控件占用的列数 */
    public displayWidthInSM = 1;

    public displayWidthInMD = 1;

    public displayWidthInLG = 1;

    public displayWidthInEL = 1;

    /** 编辑器内部默认显示的屏幕大小-----可以去掉 */
    public displayColumnCountAtBreakPoint = 'md';

    /** 控件所在行，传0即可-----编辑器内部使用 */
    public tagRow = 0;

    /** 控件是否显示上方空白：传0即可-----编辑器内部使用 */
    public showTopBorder = 0;

    /** 区域，从1开始。卡片内的控件从上往下，从左往右划分区域，遇到分组fieldSet时group+1，分组结束后group+1 */
    public group: number = 1;

    /** 控件是否符合标准的class配置(设计器用的) */
    public isSupportedClass = true;

    /** 控件所在分组id(设计器用的) */
    public fieldSetId: string = '';

}
export interface ResponseFormLayoutParam {
    defaultState: {
        /** 当前控件所在的区域 */
        defaultGroupNumber: number;
        /**  控件是否符合标准的class配置。只要当前卡片中有一个控件样式不符合标准，那么所有控件的model属性都是customize */
        model: 'standard' | 'customize';
    };
    importData: ResponseFormLayoutContext[];
}
export interface CheckLayoutEditor {
    result: boolean;
    message: string;
    formNode?: any;
}

export interface UseResponseLayoutEditorSetting {
    checkCanFindFormNode:(componentId: string)=> CheckLayoutEditor;
    checkCanOpenLayoutEditor: (schema: ComponentSchema, componentId: string) => ResponseFormLayoutParam | CheckLayoutEditor;
    changeFormControlsByResponseLayoutConfig: (propertyValue: any, componentId: string) => string;
    getResonseFormLayoutConfig: (propertyData: any,configs: ResponseFormLayoutContext[],group: number,isFieldSet?:boolean) => void;
}
