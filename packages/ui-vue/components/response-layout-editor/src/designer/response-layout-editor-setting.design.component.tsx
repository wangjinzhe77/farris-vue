 
import { defineComponent, onMounted, inject, ref, SetupContext } from 'vue';
import { ResponseLayoutEditorSettingProps, responseLayoutEditorSettingProps } from '../composition/props/response-layout-editor-setting.props';
import { FModalService } from '../../../modal';
import FResponseLayoutEditor from '../response-layout-editor.component';

import '../response-layout-editor.css';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';

export default defineComponent({
    name: 'FResponseLayoutEditorSettingDesign',
    props: responseLayoutEditorSettingProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: ResponseLayoutEditorSettingProps, context: SetupContext) {
        /** 传入参数 */
        const initialState = ref(props.initialState);
        /** 页面宽度 */
        const width = ref(props.width);
        /** 页面高度 */
        const height = ref(props.height);

        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const componentInstance = useDesignerComponent(elementRef, designItemContext);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        function toggleModal() {
            const myModal = FModalService.show({
                // title: '响应式列宽编辑器',
                render: () => {
                    return <FResponseLayoutEditor
                        initialState={initialState}
                        width={width}
                        height={height}
                    ></FResponseLayoutEditor>;
                },
                width: 900,
                height: 500,
                buttons: [
                    {
                        class: 'btn btn-primary', text: '确定', iconClass: 'f-icon f-icon-check',
                        handle: () => {
                            myModal['destroy']();
                        }
                    },
                    {
                        class: 'btn btn-light', text: '取消', iconClass: 'f-icon f-icon-close',
                        handle: () => {
                            myModal['destroy']();
                        }
                    }
                ]
            });
        }

        return () => {
            return (
                <div ref={elementRef} class="f-column-width-setting-button" onClick={toggleModal}>
                    {/* 图标 */}
                    <div class="f-icon f-icon-add"></div>
                    {/* 高级设置按钮 */}
                    <div class="f-column-width-setting-button-name">
                        <span class="f-column-width-setting-button-center">高级设置</span>
                    </div>
                </div>
            );
        };
    }
});
