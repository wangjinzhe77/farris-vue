 
import { App, defineComponent, inject, ref, SetupContext, watch } from 'vue';
import { FMessageBoxService } from '../../message-box';
import { FModalService } from '../../modal';
import { ResponseLayoutEditorSettingProps, responseLayoutEditorSettingProps } from './composition/props/response-layout-editor-setting.props';
import FResponseLayoutEditor from './response-layout-editor.component';

import './response-layout-editor.css';
import { ImportData, InitialState } from './composition/types';

export default defineComponent({
    name: 'FResponseLayoutEditorSetting',
    props: responseLayoutEditorSettingProps,
    emits: ['change'] as (string[] & ThisType<void>) | undefined,
    setup(props: ResponseLayoutEditorSettingProps, context: SetupContext) {
        /** 传入参数 */
        const initialState = ref<InitialState>(props.initialState);
        /** 页面宽度 */
        const width = ref(props.width);
        /** 页面高度 */
        const height = ref(props.height);

        // watch(() => props.modelValue, (newValue: ImportData[]) => {
        //     initialState.value['importData'] = newValue;
        // });

        watch(()=>props.initialState,(newValue: InitialState) => {
            initialState.value = newValue;
        });

        function onSubmit(app: App, newValue: ImportData[]) {
            initialState.value['importData'] = newValue;
            context.emit('change', newValue);
            app && app.unmount();
        }

        function onClose(app: App) {
            app && app.unmount();
        }

        function toggleModal() {
            if (!initialState.value.defaultState || !initialState.value.importData) {
                const errorMessage = (initialState.value as any).message || '只可以在响应式表单组件中调整响应式布局配置';
                FMessageBoxService.info(errorMessage, "");
                return;
            }

            FModalService.show({
                render: (app: App) => {
                    return <FResponseLayoutEditor
                        initialState={initialState}
                        width={width.value}
                        height={height.value}
                        onClose={() => onClose(app)}
                        onSubmit={(newValue) => onSubmit(app, newValue)}>
                    </FResponseLayoutEditor>;
                },
                width: 900,
                height: 500,
                showButtons: false,
                showHeader: false,
                showFloatingClose: true
            });
        }

        return () => {
            return (
                <div class="f-column-width-setting-button" onClick={toggleModal}>
                    {/* 图标 */}
                    <i class="f-icon f-icon-add"></i>
                    {/* 高级设置按钮 */}
                    <div class="f-column-width-setting-button-name">
                        <span class="f-column-width-setting-button-center">高级设置</span>
                    </div>
                </div>
            );
        };
    }
});
