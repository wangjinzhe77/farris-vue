 
import { ComponentSchema } from "../../../designer-canvas/src/types";
import { DynamicResolver } from "../../../dynamic-resolver";

function wrapSectionForForm(resolver: DynamicResolver, schema: ComponentSchema, context: Record<string, any>, formSchemaId: string): ComponentSchema {
    const sectionSchema = resolver.getSchemaByType('section') as ComponentSchema;
    sectionSchema.id = `${formSchemaId}-section`;
    sectionSchema.appearance = { class: 'f-section-form f-section-in-main' };
    sectionSchema.mainTitle = '标题';
    sectionSchema.contents = [schema];
    return sectionSchema;
}

function wrapComponentForForm(resolver: DynamicResolver, schema: ComponentSchema, context: Record<string, any>, formSchemaId: string): ComponentSchema {
    const componentSchema = resolver.getSchemaByType('component') as ComponentSchema;
    componentSchema.id = `${formSchemaId}-component`;
    componentSchema.componentType = 'form-col-1';
    componentSchema.appearance = {
        class: 'f-struct-form f-struct-wrapper'
    };
    componentSchema.contents = [schema];
    return componentSchema;
}

export function schemaResolver(resolver: DynamicResolver, schema: Record<string, any>, context: Record<string, any>): Record<string, any> {
    schema.appearance = { class: 'f-form-layout farris-form farris-form-controls-inline' };
    const wrappedSchema = wrapSectionForForm(resolver, schema as ComponentSchema, context, (schema as ComponentSchema).id);
    const wrappedFormComponent = wrapComponentForForm(resolver, wrappedSchema, context, (schema as ComponentSchema).id);
    return wrappedFormComponent;
}
