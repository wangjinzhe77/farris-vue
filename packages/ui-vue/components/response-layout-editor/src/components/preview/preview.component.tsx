import { defineComponent, onMounted, ref, SetupContext, watch } from 'vue';
import { PreviewProps, previewProps } from './preview.props';
import '../../response-layout-editor.css';
import { cloneDeep } from 'lodash-es';
import FColumnWidthView from './column-width-view/column-width-view.component';

export default defineComponent({
    name: 'FPreview',
    props: previewProps,
    emits: ['update:twoDimensionalArray'] as (string[] & ThisType<void>) | undefined,
    setup(props: PreviewProps, context: SetupContext) {
        /** 默认屏幕分辨率对应的英文缩写 */
        const currentScreenResolution = ref(props.currentScreenResolution);
        /** 模式开关 */
        const switchInStandardMode = ref(props.switchInStandardMode);
        /** 记录当前展示的区域 */
        const currentArea = ref(props.currentArea);
        /** 接受组件列表区传入的二维数组 */
        const twoDimensionalArray = ref(props.twoDimensionalArray);
        /** 按照原数组-不同区域-不同行生成新三维数组*/
        const threeDimensionalArray = ref<any[]>([]);

        watch([
                () => props.currentArea,
                () => props.switchInStandardMode,
                () => props.currentScreenResolution,
            ],
            ([newArea, newMode, newResolution], [oldArea, oldMode, oldResolution]) => {
                if (newArea !== oldArea) {
                    currentArea.value = newArea;
                }
                if (newMode !== oldMode) {
                    switchInStandardMode.value = newMode;
                }
                if (newResolution !== oldResolution) {
                    currentScreenResolution.value = newResolution;
                }
            }
        );

        /** 针对区域，逐个生成三维数组(预览区栅格数；预览区第几行；属于第几组；) */
        function ArrayConversion(previewElement: any[], previousGroupRow: number) {
            const twoDimensionalArray: any = [];
            let middleArray: any[] = [];
            
            previewElement.forEach(item => {
                if (previousGroupRow !== item.componentRow) {
                    previousGroupRow +=1;
                    twoDimensionalArray.push(middleArray);
                    middleArray = [];
                }
                middleArray.push(item);
            });
            twoDimensionalArray.push(middleArray);
        
            return twoDimensionalArray;
        }
        
        /** 生成三维数组 */
        function arrayConversionTotal() {
            const setThreeDimensionalArray: any[] = [];
            // 显示第几行，从第一行开始
            let previousGroupRow = 1;
            threeDimensionalArray.value.forEach((element: any[]) => {
                // 记录当前区域的数组变化
                const groupDimensionRecord = ArrayConversion(element, previousGroupRow);
                // 更新不同区域对应占的行数
                previousGroupRow += groupDimensionRecord.length;
                // 记录每个区域的变化
                setThreeDimensionalArray.push(groupDimensionRecord);
            });
            return setThreeDimensionalArray;
        }

        function transferArrayDimensionFromTwoToThree() {
            // 拷贝原有二维数组
            threeDimensionalArray.value = cloneDeep(twoDimensionalArray.value);
            // 转化为三维数组
            threeDimensionalArray.value = arrayConversionTotal();
        }
        
        watch(() => props.twoDimensionalArray, () => {
            twoDimensionalArray.value = props.twoDimensionalArray;
            transferArrayDimensionFromTwoToThree();
        });

        onMounted(() => {
            transferArrayDimensionFromTwoToThree();
        });

        /** 返回栅格背景 */
        function renderGridBackground() {
            if (switchInStandardMode.value) {
                return '';
            }
            const columns = Array.from({ length: 12 }, () => (
                <div class='col-1'>
                    <div class='f-layout-editor-preview-customize-single-background'>&nbsp;</div>
                </div>
            ));
            return (<div class='f-layout-editor-preview-customize-whole-background'>{columns}</div>);
        }
        
        return () => (
            <div class='f-layout-editor-preview f-utils-fill-flex-column'>
                {/* 标题 */}
                <div class='f-layout-editor-preview-header'>
                    预览区
                </div>
                {/* 自定义模式-栅格背景 */}
                {renderGridBackground()}
                <div class='f-layout-editor-preview-view f-utils-overflow-xhya'>
                    <div class='f-layout-editor-preview-row-group'>
                        {threeDimensionalArray.value.map((previewElementInSameGroup: any[]) => (
                            previewElementInSameGroup[0][0]?.group.toString() === currentArea.value[0]?.id &&
                            previewElementInSameGroup.map((rows) => (
                                <div class='f-layout-editor-row'>
                                    {rows.map((item: any) => (
                                        <FColumnWidthView
                                            v-model:item={item}
                                            currentScreenResolution={currentScreenResolution.value}
                                            switchInStandardMode={switchInStandardMode.value}
                                        />
                                    ))}
                                </div>
                            ))
                        ))}
                    </div>
                </div>
            </div>
        );
    }
});
