import { computed, defineComponent, ref, SetupContext, watch } from 'vue';
import { ColumnWidthViewProps, columnWidthViewProps } from './column-width-view.props';
import '../../../response-layout-editor.css';

export default defineComponent({
    name: 'FColumnWidthView',
    props: columnWidthViewProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: ColumnWidthViewProps, context: SetupContext) {

        /** 单个组件的预览状态 */
        const item = ref(props.item);
        /** 默认屏幕分辨率对应的英文缩写 */
        const currentScreenResolution = ref(props.currentScreenResolution);
        /** 接受switch状态，若为自定义模式，则需要改变Col-num的num来源；*/
        // const switchInStandardMode = ref(props.switchInStandardMode);

        /** 响应式设计样式：col-8等 */
        const colClass = computed(() => {
            // 对应的列所占栅格数12，12，6，12等
            const colClassNumber = item.value[`columnIn${currentScreenResolution.value}`];
            // 设置拼接的字符串部分
            return `col-${colClassNumber}`;
        });

        watch([() => props.item, () => props.currentScreenResolution],
            ([newItem, newResolution], [oldItem, oldResolution]) => {
                if (newItem !== oldItem) {
                    item.value = newItem;
                }
                if (newResolution !== oldResolution) {
                    currentScreenResolution.value = newResolution;
                }
            });

        return () => {
            return (
                <div class={colClass.value}>
                    <div class='f-layout-editor-column-width-view' title={item.value.label}>
                        <div class='f-layout-editor-column-width-view-label' >
                            {item.value.label}
                        </div>
                    </div>
                </div>
            );
        };
    }
});
