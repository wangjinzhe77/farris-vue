 
import { ExtractPropTypes } from "vue";

export const columnWidthViewProps = {

    /** 单个组件的预览状态 */
    item: { type: Object, default: {} },
    /** 默认屏幕分辨率对应的英文缩写 */
    currentScreenResolution: { type: String, default: "" },
    /** 接受switch状态，若为自定义模式，则需要改变Col-num的num来源；*/
    switchInStandardMode: { type: Boolean, default: false },

} as Record<string, any>;

export type ColumnWidthViewProps = ExtractPropTypes<typeof columnWidthViewProps>;
