 
import { ExtractPropTypes } from "vue";

export const previewProps = {
    /** 默认屏幕分辨率对应的英文缩写 */
    currentScreenResolution: { type: String, default: "" },
    /** 模式开关 */
    switchInStandardMode: { type: Boolean, default: false },
    /** 记录当前展示的区域 */
    currentArea: { type: Object, default: [] },
    /** 接受组件列表区传入的二维数组 */
    twoDimensionalArray: { type: Array, default: [] },

} as Record<string, any>;

export type PreviewProps = ExtractPropTypes<typeof previewProps>;
