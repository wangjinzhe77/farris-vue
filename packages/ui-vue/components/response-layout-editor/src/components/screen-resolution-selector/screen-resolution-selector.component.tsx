 
import { defineComponent, onBeforeMount, ref, SetupContext, watch } from 'vue';
import { ScreenResolutionSelectorProps, screenResolutionSelectorProps } from './screen-resolution-selector.props';
import '../../response-layout-editor.css';
import FCapsule from '../../../../capsule/src/capsule.component';
import FSwitch from '../../../../switch/src/switch.component';
import { CapsuleItem } from '../../../../capsule';

export default defineComponent({
    name: 'FScreenResolutionSelector',
    props: screenResolutionSelectorProps,
    emits: ['screenNameChanged', 'currentScreenResolutionChanged', 'switchInStandardModeChanged'] as (string[] & ThisType<void>) | undefined,
    setup(props: ScreenResolutionSelectorProps, context: SetupContext) {

        /** 屏幕分辨率设置 */
        const screenResolution = ref<CapsuleItem[]>([
            { name: '小屏幕', code: 'SM', value:'SM', active: false },
            { name: '中等屏幕', code: 'MD', value:'MD', active: true },
            { name: '大屏幕', code: 'LG', value:'LG',active: false },
            { name: '极大屏幕', code: 'EL',value:'EL', active: false }
        ]);

        /** 记录默认模式及分组状态 */
        const defaultStates = ref(props.defaultStates);
        /** 用户设置的屏幕分辨率-例:MD*/
        const currentScreenResolution = ref('MD');
        /** 用户设置的屏幕分辨率缩写-例:中等屏幕 */
        const currentScreenName = ref('中等屏幕');
        /** 记录当前列宽编辑器的模式；默认为false */
        const switchInStandardMode = ref(true);

        /** 用户点击的屏幕分辨率变化 */
        function changeBreakPointTo(newCurrentScreenCode: string) {
            screenResolution.value.forEach(item => {
                item.active = (item.code === newCurrentScreenCode);
                if (item.active) {
                    currentScreenResolution.value = item.code;
                    currentScreenName.value = item.name;
                    context.emit('screenNameChanged', currentScreenName.value);
                    context.emit('currentScreenResolutionChanged', currentScreenResolution.value);
                }
            });
        }

        /** 模式切换事件 */
        function onValueChange() {
            context.emit('switchInStandardModeChanged', switchInStandardMode.value);
        }

        watch(switchInStandardMode,()=>{
        });

        onBeforeMount(() => {
            if (defaultStates.value.model === 'standard') {
                switchInStandardMode.value = true;
            }
            else {
                switchInStandardMode.value = false;
                onValueChange();
            }
        });

        function renderScreenScopeSelector(){
            return <FCapsule class="f-layout-editor-screen-resolution-selector-frame" items={screenResolution.value} v-model={currentScreenResolution.value} onChange={changeBreakPointTo}></FCapsule>;
        }

        return () => {
            return (<div class='f-layout-editor-screen-resolution-selector'>
                {/* 标头名称 */}
                <div class='f-layout-editor-screen-resolution-selector-header'>
                    <div class='f-layout-editor-screen-resolution-selector-form'>表单布局配置</div>
                </div>
                {/* 屏幕分辨率切换组件 */}
                {renderScreenScopeSelector()}
                {/* <div class='f-layout-editor-screen-resolution-selector-frame'>
                    {screenResolution.value.map((item: { active: boolean; name: string }) => {
                        return (
                            <span class={item.active ? 'f-layout-editor-namelist-focus' : 'f-layout-editor-namelist'}
                                onClick={() => changeBreakPointTo(item.name)}>
                                {item.name}
                            </span>);
                    })}
                </div> */}
                {/* 显示模式切换按钮 */}
                <div class='f-layout-editor-screen-resolution-selector-switch'>
                    <FSwitch class='f-layout-editor-show-switch'
                        v-model={switchInStandardMode.value}
                        onModelValueChanged={onValueChange}
                    ></FSwitch>
                    {switchInStandardMode.value ?
                        <div class='f-layout-editor-show-switch-label'>标准模式</div>
                        : <div class='f-layout-editor-show-switch-label'>自定义模式</div>}
                </div>
            </div>);
        };
    }
});
