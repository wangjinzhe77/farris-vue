 
import { ExtractPropTypes } from "vue";

export const screenResolutionSelectorProps = {

    /** 记录默认模式及分组状态 */
    defaultStates: { type: Object, default: { mode: '' } },
    /** 当前开关模式 */
    onSwitchInStandardModeChanged: { type: Function, default: () => { } },
    /** 用户设置的屏幕分辨率变化-例:MD*/
    onCurrentScreenResolutionChanged: { type: Function, default: () => { } },
    /** 用户设置的屏幕分辨率缩写变化-例:中等屏幕 */
    onScreenNameChanged: { type: Function, default: () => { } }
} as Record<string, any>;

export type ScreenResolutionSelectorProps = ExtractPropTypes<typeof screenResolutionSelectorProps>;
