 
import { ExtractPropTypes } from "vue";

export const fieldListProps = {
    /** 用户切换屏幕分辨率-缩写-例:MD； */
    currentScreenResolution: { type: String, default: "" },
    /** 检测用户是否切换模式 */
    switchInStandardMode: { type: Boolean, default: false },
    /** 输入的控件原始数据 */
    originalData: { type: Array, default: [] },
    /** 在用户切换模式时显示原数据，确保两个模式下的数据不互通 */
    originalDataCopy: { type: Array, default: [] },
    /** 默认状态：包括defaultGroupNumber(默认显示组)及mode(当前模式：standard或customize) */
    defaultStates: { type: Object, default: {} },
    /** twoDimensionalArray值变化事件 */
    onTwoDimensionalArrayChanged: { type: Function, default: () => { } },
    /** originalData值变化事件 */
    onOriginalDataChanged: { type: Function, default: () => { } }
} as Record<string, any>;

export type FieldListProps = ExtractPropTypes<typeof fieldListProps>;
