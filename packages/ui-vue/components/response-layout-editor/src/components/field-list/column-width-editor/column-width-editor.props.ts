 
import { ExtractPropTypes } from "vue";

export const columnWidthEditorProps = {

    /** 默认屏幕分辨率对应的英文缩写 */
    currentScreenResolution: { type: String, default: "" },
    /** 传入的单个控件的响应式列宽值 */
    field: { type: Object, default: {} },
    /** 模式开关 */
    switchInStandardMode: { type: Boolean, default: false },
    /** 列数值变化事件 */
    fieldChanged: { type: Function, default: () => { } }
} as Record<string, any>;

export type ColumnWidthEditorProps = ExtractPropTypes<typeof columnWidthEditorProps>;
