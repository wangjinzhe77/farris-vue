import { defineComponent, inject, onMounted, ref, SetupContext, watch } from 'vue';
import { ColumnWidthEditorProps, columnWidthEditorProps } from './column-width-editor.props';
import FRadioButton from '../../../../../radio-button/src/radio-button.component';
import FRate from '../../../../../rate/src/rate.component';

import '../../../response-layout-editor.css';
import { UseDraggable } from '../../../composition/types';

export default defineComponent({
    name: 'FColumnWidthEditor',
    props: columnWidthEditorProps,
    emits: ['fieldChanged'] as (string[] & ThisType<void>) | undefined,
    setup(props: ColumnWidthEditorProps, context: SetupContext) {

        /** 不同屏幕分辨率下的显示范围 */
        const defaultColumnBoundary: { [key: string]: string[] } = {
            'SM': ['1'],
            'MD': ['1', '2'],
            'LG': ['1', '2', '3', '4'],
            'EL': ['1', '2', '3', '4', '5', '6'],
        };

        /** 默认屏幕分辨率对应的英文缩写 */
        const currentScreenResolution = ref(props.currentScreenResolution);
        /** 传入的单个控件的响应式列宽值 */
        const field = ref(props.field);
        /** 模式开关 */
        const switchInStandardMode = ref(props.switchInStandardMode);
        /** 设置不同屏幕分辨率下的显示范围 */
        const boundaryForEachColumn = ref();
        /** 控件所占栅格数 */
        const occupiedGridNum = ref('');
        /** 标准模式-用户所选控件列数值/已设定传入名称 */
        const modeChange = ref();
        /** 标准模式-每个控件所占列数 */
        const mode = ref('customize');
        /** 自定义模式-栅格数值 */
        const occupiedGridNumber = ref();

        const useDraggableComposition = inject<UseDraggable>('ResponseLayoutEditorDraggableComposition') as UseDraggable;
        const { dragstart, dragenter, dragover, dragend } = useDraggableComposition;

        /** 设置当前模式下控件所占栅格数 */
        function getOccupiedGrid() {
            boundaryForEachColumn.value = defaultColumnBoundary?.[currentScreenResolution.value];
            if (switchInStandardMode.value) {
                occupiedGridNum.value = `displayWidthIn${currentScreenResolution.value}`;
                mode.value = `${field.value[occupiedGridNum.value]}`;
            } else {
                occupiedGridNum.value = `columnIn${currentScreenResolution.value}`;
                occupiedGridNumber.value = field.value[occupiedGridNum.value];
            }
        }

        /** 标准模式-用户所选控件列数值变化事件 */
        function onStandardNumChanged(value: any) {
            if (switchInStandardMode.value === true) {
                const selectedNum = Number(value?.text);
                // 用户所选控件列数值
                modeChange.value = selectedNum;
                // 用于计算12/3或6/1等对应每分辨率应占栅格
                const colNum = field.value[`columnIn${currentScreenResolution.value}`] /
                    field.value[`displayWidthIn${currentScreenResolution.value}`];
                // 计算新占据的列
                field.value[`columnIn${currentScreenResolution.value}`] = selectedNum * colNum;;
                field.value[`displayWidthIn${currentScreenResolution.value}`] = Number(selectedNum);
                // 将当前控件的变化传出
                context.emit('fieldChanged', field.value);
            }
        }

        /** 自定义模式-用户所选控件列数值变化事件 */
        function onCustomizeNumChanged(newoccupiedGridNumber: any) {
            occupiedGridNumber.value = newoccupiedGridNumber;
            // 用于计算12/3或6/1等对应每分辨率应占栅格
            const colNum = field.value[`columnIn${currentScreenResolution.value}`] /
                field.value[`displayWidthIn${currentScreenResolution.value}`];
            field.value[`columnIn${currentScreenResolution.value}`] = occupiedGridNumber.value;
            field.value[`displayWidthIn${currentScreenResolution.value}`] = occupiedGridNumber.value / colNum;
            context.emit('fieldChanged', field.value);
        }

        /** 自定义模式-返回rate组件 */
        function returnRateComponent() {
            return (<FRate
                enableHalf={true}
                size='extraLarge'
                pointSystem={12}
                numOfStar={6}
                lightColor='#5D89FE'
                darkColor='#e7e9f3'
                iconClass='f-icon-column-rectangle'
                v-model={occupiedGridNumber.value}
                enableScore={false}
                onSelectedValue={() => onCustomizeNumChanged(occupiedGridNumber.value)}>
            </FRate>);
        }

        /** 处理传入值格式 */
        function handleInputDataFormat() {
            const inputData: any[] = [];
            boundaryForEachColumn.value = defaultColumnBoundary?.[currentScreenResolution.value];
            const activeItem = field.value[`displayWidthIn${currentScreenResolution.value}`];
            boundaryForEachColumn.value.forEach((item) => {
                const itemFormat: any = {
                    'text': item,
                    'active': activeItem.toString() === item
                };
                inputData.push(itemFormat);
            });
            return inputData;
        }

        /** 标准模式-返回列编辑组件 */
        function returnColumnEditorComponent() {
            const inputData = handleInputDataFormat();
            return (
                <FRadioButton
                    enumData={inputData}
                    suffixValue='列'
                    onSelectedValueChanged={onStandardNumChanged}
                > </FRadioButton>
            );
        }

        /** 标记组件属于第几行 */
        function markComponentRow() {
            if (field?.value?.showTopBorder === 1) {
                return (
                    <div class='f-layout-editor-column-width-editor-row'>
                        <span class='f-layout-editor-column-width-editor-row-left'></span>
                        <span class='f-layout-editor-column-width-editor-row-middle'>第{field.value.componentRow}行</span>
                        <span class='f-layout-editor-column-width-editor-row-right'></span>
                    </div>);
            }
        }

        /** 生成不同模式下的调整列宽组件 */
        function renderComponentInDifferentMode() {
            if (switchInStandardMode.value) {
                /* 标准模式调整列宽组件 */
                return (
                    <div class='f-column-width-editor-switch-standard'>
                        <div class='f-layout-editor-column-width-editor-click-button'>
                            {returnColumnEditorComponent()}
                        </div>
                    </div>);
            }
            /* 自定义模式调整列宽组件 */
            return (
                <div class='f-column-width-editor-switch-customize'
                    onDragenter={(payload: DragEvent) => dragenter(payload, field.value)}
                    onDragend={(payload: DragEvent) => dragend(payload, field.value)}
                    onDragover={(payload: DragEvent) => dragover(payload)}
                >
                    <div class='f-layout-editor-column-width-editor-rectangle'>
                        {returnRateComponent()}
                        <div class='f-layout-editor-rate-show-column'>{occupiedGridNumber.value}列</div>
                        <span class='f-layout-editor-drag-handler f-icon f-icon-drag-vertical'
                            draggable="true"
                            onDragstart={(payload: DragEvent) => dragstart(payload, field.value)}
                        ></span>
                    </div>
                </div>);
        }

        /** 生成不同模式下的左侧列宽编辑区域 */
        function renderComponentInFieldList() {
            return (
                <div class='f-layout-editor-column-width-editor-component'>
                    {/* 显示label */}
                    <div class='f-layout-editor-column-width-editor-label'>{field?.value?.label}</div>
                    {/* 调整列宽组件 */}
                    {renderComponentInDifferentMode()}
                </div>
            );
        }

        watch(
            () => [props.field, props.switchInStandardMode, props.currentScreenResolution],
            ([newField, newSwitch, newResolution], [oldField, oldSwitch, oldResolution]) => {
                if (newResolution !== oldResolution) {
                    currentScreenResolution.value = newResolution;
                    boundaryForEachColumn.value = defaultColumnBoundary[currentScreenResolution.value];
                    getOccupiedGrid();
                }
                if (newSwitch !== oldSwitch) {
                    switchInStandardMode.value = newSwitch;
                }
                if (switchInStandardMode.value === false) {
                    // 切换至自定义视图时，重会自定义视图，以便于同步在标准视图中可能更新的配置。
                    getOccupiedGrid();
                }
                if (newField !== oldField) {
                    field.value = newField;
                }
            }
        );

        onMounted(() => {
            getOccupiedGrid();
        });

        return () => {
            return (<div class='f-layout-editor-column-width-editor'>
                {/* 显示第几行 */}
                {markComponentRow()}
                {/* 组件部分 */}
                {renderComponentInFieldList()}
            </div>);
        };
    }
});
