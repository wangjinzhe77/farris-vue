import { defineComponent, onBeforeMount, provide, Ref, ref, SetupContext, watch } from 'vue';
import { FieldListProps, fieldListProps } from './field-list.props';
import { cloneDeep } from 'lodash-es';
import { ComboValue } from '../../type';
import { useDraggable } from '../../composition/use-draggable';
import FComboList from '../../../../combo-list/src/combo-list.component';
import FColumnWidthEditor from './column-width-editor/column-width-editor.component';

import '../../response-layout-editor.css';

export default defineComponent({
    name: 'FFieldList',
    props: fieldListProps,
    emits: ['update:currentArea', 'twoDimensionalArrayChanged', 'originalDataChanged'] as (string[] & ThisType<void>) | undefined,
    setup(props: FieldListProps, context: SetupContext) {
        /** 用户切换屏幕分辨率-缩写-例:MD */
        const currentScreenResolution = ref(props.currentScreenResolution);
        /** 检测用户是否切换模式 */
        const switchInStandardMode = ref(props.switchInStandardMode);
        /** 输入的控件原始数据 */
        const originalData = ref(props.originalData);
        /** 在用户切换模式时显示原数据，确保两个模式下的数据不互通 */
        const originalDataCopy = ref(props.originalDataCopy);
        /** 默认状态：包括defaultGroupNumber(默认显示组)及mode(当前模式：standard或customize) */
        const defaultStates = ref(props.defaultStates);
        /** comboList中初始状态显示值 */
        const currentGroup = ref('');
        /** 存储comboList所用值：[{id:1,label:区域1};{id:2,label:区域2}；...] */
        const groupItems: Ref<Array<object>> = ref([]);
        /** 展示用户在comboList中所选的区域，例：[{1：区域1}] */
        const currentArea: Ref<Array<any>> = ref(props.currentArea);
        /** 标记属于第几行 */
        const componentRow = ref(0);
        /** 根据原数据，生成一个根据同组划分的数组 */
        const sameGroupArrayInTotal = ref();
        /** 记录当前区域共占几行 */
        const maxRow = ref(0);
        /** 用户初始点击的控件所在的区域，决定默认显示区域 */
        const defaultAreaNumber = ref('1');
        /** cdk drag区域对应的显示数组 */
        const cdkDropGroup: Ref<Array<object>> = ref([]);
        
        /**
         * 下拉框-从传入值的group汇总所有的区域值，例：区域12345
         */
        function groupCategory() {
            const middleArray: any[] = [];
            originalData.value.forEach((element: { group: any }) => {
                if (middleArray.indexOf(element.group) === -1) {
                    middleArray.push(element.group);
                }
            });
            return middleArray;
        }

        /**
         * 下拉框-默认显示区域值
         */
        function defaultArea() {
            defaultAreaNumber.value = defaultStates.value.defaultGroupNumber.toString();
            // comboList默认显示值
            currentGroup.value = `区域${defaultAreaNumber.value}`;
            // 下拉框对应的左侧操作区的所有控件
            currentArea.value = [groupItems.value[Number(defaultAreaNumber.value) - 1]];
            // 通知视图区
            context.emit('update:currentArea', currentArea.value);
        }

        /**
         * 下拉框-生成下拉框中的所有区域的列表
         */
        function groupForComboList() {
            const middleArray = groupCategory();
            for (let i = 0; i < middleArray.length; i++) {
                // 对应comboList中的id和label
                const groupItem = {
                    'id': middleArray[i]?.toString(),
                    'label': '区域' + middleArray[i]
                };
                groupItems.value.push(groupItem);
            }
            defaultArea();
        }

        /**
         * 根据group分区域-根据originalData中的group，生成根据group来分类后的新数组sameGroupArrayInTotal
         */
        function generateArrayByGroup() {
            let sameGroup: any[] = [];
            sameGroupArrayInTotal.value = [];
            let group = 1;
            // 同group的放到一个数组中
            originalData.value.forEach((element: any) => {
                if (element.group === group) {
                    sameGroup.push(element);
                }
                else {
                    group += 1;
                    sameGroupArrayInTotal.value.push(sameGroup);
                    sameGroup = [];
                    sameGroup.push(element);
                }
            });
            sameGroupArrayInTotal.value.push(sameGroup);
        }

        /**
         * 根据分组，计算每个控件所在的行,并把值赋给componentRow
         */
        function setComponentRow(components: any[], maxRow:number) {
            const gridNumber = 12;
            let currentGridNumber = gridNumber;
            componentRow.value = 1 + maxRow;
            components.forEach(component => {
                component.showTopBorder = 0;
                const gridItem = component[`columnIn${currentScreenResolution.value}`];
                // 若>0，则放在此行
                if (currentGridNumber >= gridItem) {
                    component.componentRow = componentRow.value;
                    currentGridNumber -= gridItem;
                }
                // 若小于0，则放在下一行
                else {
                    componentRow.value += 1;
                    component.componentRow = componentRow.value;
                    currentGridNumber = gridNumber - gridItem;
                    // 计算12-现有数字后的剩余空间，判断是否能放开下一个控件
                    component.showTopBorder = 1;
                }
            });
            components[0].showTopBorder = 1;
            return components;
        }

        /**
         * 将已分区域&已标记分组的二维数组，转为一维存储
         */
        function resetData(recordCurrentComponentRow: any) {
            let reset: any[] = [];
            for (const item of recordCurrentComponentRow) {
                reset = reset.concat(item);
            }
            originalData.value = cloneDeep(reset);
            maxRow.value = 0;
        }

        /**
         * 根据cdkDropGroup变化更新sameGroupArrayInTotal
         */
        function updateData() {
            sameGroupArrayInTotal.value[currentArea.value[0].id - 1] = cloneDeep(cdkDropGroup.value);
            const recordCurrentComponentRow: any[] = [];
            maxRow.value = 0;
            sameGroupArrayInTotal.value.forEach((element: any[]) => {
              // 每一个区域-计算控件所在行
              const recordInMiddle = setComponentRow(element, maxRow.value);
              recordCurrentComponentRow.push(recordInMiddle);
              // 记录当前控件共占几行；若区域1占3行，则区域2应从4开始
              maxRow.value = recordInMiddle[(recordInMiddle.length) - 1].componentRow;
            });
            resetData(recordCurrentComponentRow);
        }

        /**
         * 二维数组-已分区域的数组-计算每个区域中的控件所在行
         */
        function generateTotalComponentRow() {
            generateArrayByGroup();
            const recordCurrentComponentRow: any[][] = [];
            maxRow.value = 0;

            sameGroupArrayInTotal.value.forEach((element: any) => {
                // 每一个区域-计算控件所在行
                const recordInMiddle = setComponentRow(element,maxRow.value);
                recordCurrentComponentRow.push(recordInMiddle);
                // 记录当前控件共占几行；若区域1占3行，则区域2应从4开始；setComponentRow函数部分有体现；
                maxRow.value = recordInMiddle[(recordInMiddle.length) - 1].componentRow;
            });
            // 将已生成的二维数据传递给预览区
            context.emit('twoDimensionalArrayChanged', sameGroupArrayInTotal.value);
            resetData(recordCurrentComponentRow);
            return originalData.value;
        }

        /**
         * cdk drag-展示特定区域(currentArea)对应的可拖拽数组
         */
        function renderCdkDropGroup() {
            cdkDropGroup.value = [];
            originalData.value.forEach((element: { group: any }) => {
                if (element.group === Number(currentArea.value[0].id)) {
                    cdkDropGroup.value.push(element);
                }
            });
        }

        /**
         * 传出originalData事件
         */
        function emitOriginalData() {
            originalData.value = cloneDeep(generateTotalComponentRow());
            context.emit('originalDataChanged', originalData.value);
        }

        /**
         * field 变化事件
         */
        function onFieldChanged(field: any) {
            cdkDropGroup.value.forEach((element: any)=>{
                if(element.label === field.label){
                    Object.assign(element, field);
                }
            });
            updateData();
            emitOriginalData();
            renderCdkDropGroup();
        }

        /**
         * comboList 所选值变化事件
         */
        function onValueChange(comboValue: ComboValue) {
            for (let i = 0; i < comboValue.length; i++) {
                currentArea.value=[{
                    'id': comboValue[i].id,
                    'label': comboValue[i].label
                    }];
            }
            context.emit('update:currentArea', currentArea.value);
            renderCdkDropGroup();
            updateData();
        }


        /** 返回组件区域分组下拉框 */
        function renderFieldListCombo(){
            return(
                <div class='f-layout-editor-field-list-top'>
                <div class='f-layout-editor-field-list-header'>组件列表</div>
                <div class='f-layout-editor-choose-group'>
                    <FComboList
                        v-model={defaultAreaNumber.value}
                        placeholder='当前可编辑组'
                        enableClear={false}
                        id-field='id'
                        text-field='label'
                        data={groupItems.value}
                        editable={false}
                        onChange={onValueChange}></FComboList>
                </div>
            </div>);
        }

        /** 返回组件列宽编辑器 */
        function renderColumnWidthEditor(){
            return(
                <div class='f-layout-editor-field-list-content f-utils-overflow-xhya'>
                    {cdkDropGroup.value.map((field: any) => {
                        return ( 
                            <FColumnWidthEditor
                                key={field.label}
                                currentScreenResolution={currentScreenResolution.value}
                                v-model:field={field}
                                v-model:switchInStandardMode={switchInStandardMode.value}
                                onFieldChanged={onFieldChanged}
                            ></FColumnWidthEditor> 
                        );
                    })}
                </div>
            );
            {/* *cdkDragPreview  */}
            {/* <div class='f-layout-editor-field-list-content-cdk-preview'>
                <div class='f-layout-editor-column-width-editor-label'>{field.label}</div>
            </div>  */}
        }

        /**
         * 拖拽处理API，第二个参数为拖拽事件回调函数，用于更新界面元素顺序
         */
        const draggableComposition =  useDraggable(cdkDropGroup as any,()=>{
            updateData();
            emitOriginalData();
            renderCdkDropGroup();
        });
        /** 通过依赖注入向column-width-editor组件传递拖拽处理API */
        provide('ResponseLayoutEditorDraggableComposition',draggableComposition);

        watch(() => props.currentArea, (newValue,oldValue) => {
            if(newValue !== oldValue){
                currentArea.value = props.currentArea;
                renderCdkDropGroup();
            }
        });

        watch(currentArea, (newValue,oldValue) => {
            if(newValue !== oldValue){
                renderCdkDropGroup();
            }
        });

        watch(() => props.currentGroup, (newValue,oldValue) => {
            if(newValue !== oldValue){
                currentGroup.value = props.currentGroup;
            }
        });

        watch(() => props.currentScreenResolution, (newValue,oldValue) => {
            if(newValue !== oldValue){
                currentScreenResolution.value = props.currentScreenResolution;
                emitOriginalData();
                generateArrayByGroup();
                if (currentArea.value[0]) {
                    cdkDropGroup.value = cloneDeep(sameGroupArrayInTotal.value[Number(currentArea.value[0].id) - 1]);
                }
            }
        });

        watch(()=> props.switchInStandardMode, (newValue,oldValue) => {
            if(newValue !== oldValue){
                switchInStandardMode.value = props.switchInStandardMode;
                originalData.value = cloneDeep(originalDataCopy.value);
            }
        });

        // watch(defaultAreaNumber,()=>{
        //     console.log(cdkDropGroup.value);
        // });

        watch(defaultStates, (newValue,oldValue) => {
            if(newValue !== oldValue){
                emitOriginalData();
            }
        });

        onBeforeMount(() => {
            // 生成所有区域
            groupForComboList();
            generateTotalComponentRow();
            renderCdkDropGroup();
        });

        return () => {
            return (
                <div class='f-layout-editor-field-list f-utils-fill-flex-column'>
                    {switchInStandardMode.value ? (
                        /* 标准模式 */
                        <div class='f-layout-editor-field-list-standard f-utils-fill-flex-column'>
                            {renderFieldListCombo()}
                            {renderColumnWidthEditor()}
                        </div>
                    ) : (
                        /* 自定义模式 */
                        <div class='f-layout-editor-field-list-customize f-utils-fill-flex-column'>
                            {renderFieldListCombo()}
                            {renderColumnWidthEditor()}
                        </div>
                    )}
                </div>
            );
        };
    }
});
