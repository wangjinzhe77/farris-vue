import { App, computed, defineComponent, onBeforeMount, ref, SetupContext,watch } from 'vue';
import { cloneDeep } from 'lodash-es';
import { InitialState } from './composition/types';
import { ResponseLayoutEditorProps, responseLayoutEditorProps } from './composition/props/response-layout-editor.props';
import FScreenResolutionSelector from './components/screen-resolution-selector/screen-resolution-selector.component';
import FPreview from './components/preview/preview.component';
import FFieldList from './components/field-list/field-list.component';

import './response-layout-editor.css';

export default defineComponent({
    name: 'FResponseLayoutEditor',
    props: responseLayoutEditorProps,
    emits: ['close', 'submit'] as (string[] & ThisType<void>) | undefined,
    setup(props: ResponseLayoutEditorProps, context: SetupContext) {
        /** 传入参数 */
        const initialState = ref<InitialState>(props.initialState);
        /** 记录初始入参 */
        const originalData = ref();
        /** 接受传入的原始数据的defaultState */
        const defaultStates = ref();
        /** 默认屏幕分辨率 */
        const currentScreenName = ref('');
        /** 默认屏幕分辨率对应的英文缩写 */
        const currentScreenResolution = ref('');
        /** 模式开关默认为true */
        const switchInStandardMode = ref(true);
        /** 记录当前展示的区域 */
        const currentArea = ref([{id:'1',label:'区域1'}]);
        /** 接受组件列表区传入的二维数组 */
        const twoDimensionalArray = ref([]);
        /** 存储原始数据-两种模式数据不互通 */
        const originalDataCopy = ref();
        /** 页面宽度 */
        const width = ref(props.width);
        /** 页面高度 */
        const height = ref(props.height);

        watch(()=>props.initialState,(newValue:InitialState)=>{
            initialState.value = newValue;
        });

        watch(
            [() => props.currentArea, () => props.switchInStandardMode],
            ([newArea, newMode], [oldArea, oldMode]) => {
                if (newArea !== oldArea) {
                    currentArea.value = newArea;
                }
                if (newMode !== oldMode) {
                    switchInStandardMode.value = newMode;
                }
            }
        );

        /** 当前屏幕分辨率值变化事件 */
        function onScreenNameChanged(newCurrentScreenName: string) {
            currentScreenName.value = newCurrentScreenName;
        }

        /** 当前屏幕分辨率英文缩写值变化事件 */
        function onCurrentScreenResolutionChanged(newCurrentScreenResolution: string) {
            currentScreenResolution.value = newCurrentScreenResolution;
        }

        /** 当前显示模式改变事件 */
        function onSwitchInStandardModeChanged(newSwitchInStandardMode: boolean) {
            switchInStandardMode.value = newSwitchInStandardMode;
        }

        /** 当前显示区域值变化事件 */
        function onCurrentAreaChanged(newShowArea: any) {
            currentArea.value = [].concat(newShowArea);
        }

        /** 列宽所占列及位置变化事件 */
        function onOriginalDataChanged(newOriginalData: any) {
            originalData.value = cloneDeep(newOriginalData);
        }

        /** 二维数组值变化事件 */
        function onTwoDimensionalArrayChanged(newTwoDimensionalArray: any) {
            twoDimensionalArray.value = newTwoDimensionalArray;
        }

        /** 弹窗关闭事件 */
        function back() {
            context.emit('close');
        }

        /** 上传修改后的数据 */
        function uploadData() {
            context.emit('submit', originalData.value);
        }

        /** 返回组件列表 */
        function returnFieldList(){
            return (<FFieldList class='f-utils-fill-flex-column'
                defaultStates={defaultStates.value}
                originalDataCopy={originalDataCopy.value}
                v-model:currentScreenResolution={currentScreenResolution.value}
                v-model:originalData={originalData.value}
                v-model:switchInStandardMode={switchInStandardMode.value}
                v-model:currentArea={currentArea.value}
                onCurrentAreaChanged={onCurrentAreaChanged}
                onTwoDimensionalArrayChanged={onTwoDimensionalArrayChanged}
                onOriginalDataChanged={onOriginalDataChanged}></FFieldList>);
        }

        function returnPreview(){
            return (<FPreview class='f-utils-fill-flex-column'
                v-model:currentArea={currentArea.value}
                v-model:currentScreenResolution={currentScreenResolution.value}
                switchInStandardMode={switchInStandardMode.value}
                v-model:twoDimensionalArray={twoDimensionalArray.value}></FPreview>);
        }
        /** 返回当前模式下左侧组件列表对应的样式 */
        const currentModeLeftPartClass = computed(()=>{
            const currentModeClass = switchInStandardMode.value ? 
            'f-layout-editor-left-standard f-utils-fill-flex-column':
            'f-layout-editor-left-customize f-utils-fill-flex-column';
            return currentModeClass;
        });

        /** 返回当前模式下右侧预览区对应的样式 */
        const currentModeRightPartClass = computed(()=>{
            const currentModeClass = switchInStandardMode.value ? 
            'f-layout-editor-right-standard f-utils-fill-flex-column':
            'f-layout-editor-right-customize f-utils-fill-flex-column';
            return currentModeClass;
        });

        onBeforeMount(() => {
            // 将用户控件数据初始值赋给originalData
            originalData.value = cloneDeep(initialState.value.importData);
            originalDataCopy.value = cloneDeep(initialState.value.importData);
            // 将当前支持模式及默认显示区域赋给defaultStates
            defaultStates.value = initialState.value.defaultState;
            // 默认屏幕分辨率
            currentScreenName.value = '中等屏幕';
            currentScreenResolution.value = 'MD';
        });

        return (app: App) => {
            return (<div class='f-layout-editor f-utils-flex-column' style={'height:' + height.value + 'px;width:' + width.value + 'px;'}>
                {/* 标头区域 */}
                <div class='f-layout-editor-top'>
                    {/* 切换屏幕分辨率组件&切换模式按钮 */}
                    <FScreenResolutionSelector
                        defaultStates={defaultStates.value}
                        onScreenNameChanged={onScreenNameChanged}
                        onCurrentScreenResolutionChanged={onCurrentScreenResolutionChanged}
                        onSwitchInStandardModeChanged={onSwitchInStandardModeChanged}></FScreenResolutionSelector>
                </div>
                {/* 内容区域 */}
                <div class='f-layout-editor-content f-utils-fill'>
                    {/* 左侧操作区 */}
                    <div class='f-layout-editor-left f-utils-fill-flex-column'>
                        <div class={currentModeLeftPartClass.value}>
                            {returnFieldList()}
                        </div>
                    </div>
                    {/* 右侧视图区 */}
                    <div class='f-layout-editor-right f-utils-fill-flex-column'>
                        <div class={currentModeRightPartClass.value}>
                            {returnPreview()}
                        </div>
                    </div>
                </div>
                {/* 底部区域 */}
                <div class='f-layout-editor-bottom' >
                    <div class='f-layout-editor-button'>
                        <div class='f-layout-editor-cancel-btn' onClick={back}>
                            <span class='f-layout-editor-text-cancel'>取消</span>
                        </div>
                        <div class='f-layout-editor-sure-btn' onClick={uploadData} >
                            <span class='f-layout-editor-text-sure'>确认</span>
                        </div>
                    </div>
                </div>
            </div >
            );
        };
    }
});
