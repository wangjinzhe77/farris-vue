 
import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver } from '../../../../dynamic-resolver/src/props-resolver';
import { schemaMapper } from '../../schema/schema-mapper';
import { schemaResolver } from '../../schema/schema-resolver';
import responseLayoutEditorSettingSchema from '../../schema/response-layout-editor-setting.schema.json';
import propertyConfig from '../../property-config/response-layout-editor-setting.property-config.json';
import { ImportData, InitialState } from '../types';

export const responseLayoutEditorSettingProps = {
    /** 接受传入的原始数据的defaultState */
    initialState: { type: Object as PropType<InitialState>},
    /** 页面高度 */
    height: { type: Number, default: 620 },
    /** 页面宽度 */
    width: { type: Number, default: 700 },

    modelValue: { type: Object as PropType<ImportData[]> }
} as Record<string, any>;

export type ResponseLayoutEditorSettingProps = ExtractPropTypes<typeof responseLayoutEditorSettingProps>;
export const propsResolver = createPropsResolver<ResponseLayoutEditorSettingProps>(responseLayoutEditorSettingProps, responseLayoutEditorSettingSchema, schemaMapper, schemaResolver, propertyConfig);
