 
import { ExtractPropTypes } from 'vue';

export const responseLayoutEditorProps = {
    /** 接受传入的原始数据的defaultState */
    initialState: { type: Object, default: {} },
    /** 页面高度 */
    height: { type: Number, default: 620 },
    /** 页面宽度 */
    width: { type: Number, default: 700 }
} as Record<string, any>;

export type ResponseLayoutEditorProps = ExtractPropTypes<typeof responseLayoutEditorProps>;
