import { DgControl,ComponentSchema } from "@farris/ui-vue/components/designer-canvas";
import { CheckLayoutEditor, ResponseFormLayoutContext, ResponseFormLayoutParam, UseResponseLayoutEditorSetting } from "../../type";
export function useResponseLayoutEditorSetting(formShemaService): UseResponseLayoutEditorSetting {

    let defaultGroupNumber;
    let currentControlId;
    const originalControlMap = new Map<string, any>();
    let responseLayoutConfig: ResponseFormLayoutContext[] = [];

    /**
     * 按照控件class样式，组装列编辑器需要的参数。
     * 标准的模板样式为col-12 col-md-6 col-xl-3 col-el-2
     * @param controlClass 控件class样式
     * @param formResponseLayoutContext 响应式布局参数实体
     */
    function resovleResponseContext(controlClass: string, formResponseLayoutContext: ResponseFormLayoutContext) {

        const controlClassArray = controlClass.split(' ');
        const colClassItems = controlClassArray.filter(classItem => classItem.startsWith('col-'));

        // 没有写任何col-xx 的样式，不支持配置布局
        if (colClassItems.length === 0) {
            formResponseLayoutContext.isSupportedClass = false;
            return;
        }
        let colClass = colClassItems.find(item => /^col-([1-9]|10|11|12)$/.test(item));
        let colMDClass = colClassItems.find(item => /^col-md-([1-9]|10|11|12)$/.test(item));
        let colXLClass = colClassItems.find(item => /^col-xl-([1-9]|10|11|12)$/.test(item));
        let colELClass = colClassItems.find(item => /^col-el-([1-9]|10|11|12)$/.test(item));

        // 小屏幕：列数只支持1
        colClass = colClass || 'col-12';
        formResponseLayoutContext.columnInSM = parseInt(colClass.replace('col-', ''), 10);
        formResponseLayoutContext.displayWidthInSM = formResponseLayoutContext.columnInSM / 12;
        if (formResponseLayoutContext.displayWidthInSM !== 1) {
            formResponseLayoutContext.isSupportedClass = false;
        }

        // 中等屏幕，列数支持1,2
        colMDClass = colMDClass || 'col-md-' + formResponseLayoutContext.columnInSM;
        formResponseLayoutContext.columnInMD = parseInt(colMDClass.replace('col-md-', ''), 10);
        formResponseLayoutContext.displayWidthInMD = formResponseLayoutContext.columnInMD / 6;
        if (![1, 2].includes(formResponseLayoutContext.displayWidthInMD)) {
            formResponseLayoutContext.isSupportedClass = false;
        }

        // 大屏幕，列数支持1,2, 3, 4
        colXLClass = colXLClass || 'col-xl-' + formResponseLayoutContext.columnInMD;
        formResponseLayoutContext.columnInLG = parseInt(colXLClass.replace('col-xl-', ''), 10);
        formResponseLayoutContext.displayWidthInLG = formResponseLayoutContext.columnInLG / 3;
        if (![1, 2, 3, 4].includes(formResponseLayoutContext.displayWidthInLG)) {
            formResponseLayoutContext.isSupportedClass = false;
        }

        // 超大等屏幕，列数支持1,2, 3, 4, 5, 6
        colELClass = colELClass || 'col-el-' + formResponseLayoutContext.columnInLG;
        formResponseLayoutContext.columnInEL = parseInt(colELClass.replace('col-el-', ''), 10);
        formResponseLayoutContext.displayWidthInEL = formResponseLayoutContext.columnInEL / 2;
        if (![1, 2, 3, 4, 5, 6].includes(formResponseLayoutContext.displayWidthInEL)) {
            formResponseLayoutContext.isSupportedClass = false;
        }
    }

    function getResonseFormLayoutConfig(
        propertyData: any,
        configs: ResponseFormLayoutContext[],
        group: number,
        isFieldSet = false
    ) {
        // 前一个控件是否为分组控件，用于计算group值
        let isLastControlFieldSet = false;

        propertyData.contents.forEach(control => {
            if (control.type === 'fieldset') {
                group += 1;
                getResonseFormLayoutConfig(control, configs, group, true);
                isLastControlFieldSet = true;
                return;
            }
            if (isLastControlFieldSet) {
                group += 1;
                isLastControlFieldSet = false;
            }
            const controlClass = control.appearance && control.appearance.class;
            const formResponseLayoutContext = new ResponseFormLayoutContext();
            if (!controlClass) {
                formResponseLayoutContext.isSupportedClass = false;

            } else {
                resovleResponseContext(controlClass, formResponseLayoutContext);
            }
            formResponseLayoutContext.label = control.label || control.id;
            formResponseLayoutContext.id = control.id;
            formResponseLayoutContext.group = group;

            if (isFieldSet) {
                formResponseLayoutContext.fieldSetId = propertyData.id;
            }

            if (currentControlId === control.id) {
                defaultGroupNumber = group;
            }
            originalControlMap.set(control.id, control);
            configs.push(formResponseLayoutContext);
        });
    }
    function checkCanFindFormNode(componentId: string): CheckLayoutEditor{
        const componentNode = formShemaService.getComponentById(componentId);
        if (!componentNode || !componentNode.componentType || !componentNode.componentType.startsWith('form')) {
            return { result: false, message: '只可以在响应式表单组件中调整响应式布局配置' };
        }
        const formNode = formShemaService.selectNode(componentNode, item => item.type === DgControl['response-form'].type);
        if (!formNode || !formNode.contents || formNode.contents.length === 0) {
            return { result: false, message: 'Form区域内没有控件，请先添加控件' };
        }
        return { result: true, message: '', formNode };
    }
    /**
     * 遍历控件所属Form下所有的控件，检测是否能弹出布局配置窗口，并收集配置参数
     * @param propertyData 控件属性值
     */
    function checkCanOpenLayoutEditor(schema: ComponentSchema, componentId: string): ResponseFormLayoutParam | CheckLayoutEditor {
        componentId = componentId || schema.id;
        const { result, message, formNode } = checkCanFindFormNode(componentId);
        if (!result) {
            return { result, message };
        }
        currentControlId = schema.id;
        responseLayoutConfig = [];
        originalControlMap.clear();
        const firstGroup = formNode.contents[0].type === DgControl['fieldset'].type ? 0 : 1;
        getResonseFormLayoutConfig(formNode, responseLayoutConfig, firstGroup);

        const hasInvalidControl = responseLayoutConfig.find(config => !config.isSupportedClass);

        const responseParam: ResponseFormLayoutParam = {
            defaultState: {
                defaultGroupNumber: defaultGroupNumber || 1,
                model: hasInvalidControl ? 'customize' : 'standard'
            },
            importData: responseLayoutConfig
        };
        return responseParam;
    }
    /**
    * 按照列编辑器返回的配置修改控件class和顺序，并触发页面刷新
    * （针对带有分组的卡片，编辑器只支持控件在原分组下进行拖动，所以这里的处理相对简单，不涉及viewModel的修改）
    * @param componentId 控件所在组件ID
    */
    function changeFormControlsByResponseLayoutConfig(propertyValue: any, componentId: string): string {
        // eslint-disable-next-line no-self-assign
        componentId = componentId;
        const { result, formNode } = checkCanFindFormNode(componentId);
        if (!result) {
            return '';
        }
        const newFormContents: ComponentSchema[] = [];
        propertyValue.forEach(config => {
            // 原控件节点
            const controlNode = originalControlMap.get(config.id);
            const controlClass = controlNode.appearance && controlNode.appearance.class;

            if (controlClass) {
                const controlClassArray = controlClass.split(' ');
                const otherClassItems = controlClassArray.filter(classItem => !classItem.startsWith('col-'));

                const colClass = 'col-' + config.columnInSM;
                const colMDClass = 'col-md-' + config.columnInMD;
                const colXLClass = 'col-xl-' + config.columnInLG;
                const colELClass = 'col-el-' + config.columnInEL;

                const newClassItems = [colClass, colMDClass, colXLClass, colELClass].concat(otherClassItems);
                controlNode.appearance.class = newClassItems.join(' ');
            }

            if (config.fieldSetId) {
                // 分组节点
                const originFieldSetNode = formNode.contents.find(c => c.id === config.fieldSetId);
                const newFieldSetNode = newFormContents.find(c => c.id === config.fieldSetId);
                if (!newFieldSetNode) {
                    newFormContents.push(originFieldSetNode);
                    originFieldSetNode.contents = [controlNode];
                } else {
                    newFieldSetNode.contents?.push(controlNode);
                }
            } else {
                // 未分组节点
                newFormContents.push(controlNode);
            }
        });
        formNode.contents = newFormContents;
        return formNode.id;
    }
    return {
        checkCanFindFormNode,
        checkCanOpenLayoutEditor,
        changeFormControlsByResponseLayoutConfig,
        getResonseFormLayoutConfig
    };
}
