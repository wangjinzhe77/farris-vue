import { Ref, ref } from "vue";
import { ImportData, UseDraggable } from "./types";

export function useDraggable(fieldGroup: Ref<ImportData[]>, updateCallback: () => void): UseDraggable {
    const draggingIndex = ref(-1);
    const isDragging = ref(false);

    /**
     * 将拖拽图像设置为列宽编辑器
     * @param e 拖拽事件参数
     */
    function setDraggingImage(e: DragEvent) {
        if ((e.target as HTMLElement).offsetParent) {
            // offsetParent指向最近设置position:absolute,relative的父组件, 通过将列宽编辑器的position属性设置为relative，在此处获得列宽编辑器组件元素
            const draggingElement = (e.target as HTMLElement).offsetParent as HTMLElement;
            // 鼠标在拖拽元素中的X坐标 = 列宽编辑器宽度 - 1/2拖拽手柄宽度 - 拖拽手柄右侧外边距
            const draggingHandlerX = draggingElement.offsetWidth - (e.target as HTMLElement).offsetWidth / 2 - 4;
            // 鼠标在拖拽元素中的Y坐标 = 1/2列宽编辑器宽度
            const draggingHandlerY = draggingElement.offsetHeight / 2;
            // 设置拖拽图像及鼠标在拖拽图像中的位置
            e.dataTransfer && e.dataTransfer.setDragImage(draggingElement, draggingHandlerX, draggingHandlerY);
        }
    }

    /**
     * 记录拖拽元素在元素集合中的索引和拖拽状态等信息
     * @param targetItem 拖拽元素
     */
    function recordDraggingStatus(targetItem: any) {
        if (targetItem) {
            const targetItemIndex = fieldGroup.value.findIndex((searchElement: ImportData) => {
                return searchElement.label === (targetItem as ImportData).label;
            });
            setTimeout(() => {
                draggingIndex.value = targetItemIndex;
                isDragging.value = true;
                targetItem.moving = true;
            });
        }
    }

    function dragstart(e: DragEvent, targetItem: any) {
        e.stopPropagation();
        setDraggingImage(e);
        recordDraggingStatus(targetItem);
    }

    /**
     * 交互当前拖动元素和拖拽覆盖元素的位置
     * @param draggingEventItem 拖拽覆盖元素
     */
    function exchangeDraggingItemPosition(draggingEventItem: any) {
        // 匹配拖拽覆盖元素的索引
        const draggingEnterItemIndex = fieldGroup.value.findIndex((searchElement: ImportData) => {
            return searchElement.label === (draggingEventItem as ImportData).label;
        });
        if (draggingIndex.value !== draggingEnterItemIndex) {
            const draggingItem = fieldGroup.value[draggingIndex.value];
            const reOrderedItems = fieldGroup.value;
            // 1. 移除当前拖动元素
            reOrderedItems.splice(draggingIndex.value, 1);
            // 2. 将当前拖动元素插入拖拽覆盖元素位置，完成互换位置
            reOrderedItems.splice(draggingEnterItemIndex, 0, draggingItem);
            draggingIndex.value = draggingEnterItemIndex;
            // 3. 调用回调函数刷新页面
            if (updateCallback) {
                updateCallback();
            }
        }
    }

    function dragenter(e: DragEvent, dragEnterItem: any) {
        e.preventDefault();
        exchangeDraggingItemPosition(dragEnterItem);
    }

    function dragover(e: DragEvent) {
        e.preventDefault();
        if (e.dataTransfer) {
            e.dataTransfer.dropEffect = 'move';
        }
    }

    function dragend(e: DragEvent, targetItem: any) {
        if (targetItem) {
            targetItem.moving = false;
        }
        isDragging.value = false;
    }

    return {
        dragstart,
        dragenter,
        dragover,
        dragend,
        isDragging
    };
}
