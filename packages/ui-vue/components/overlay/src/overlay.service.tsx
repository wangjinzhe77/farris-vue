import { App, createApp, onUnmounted } from 'vue';
import FOverlay from './overlay.component';
import { JSX } from 'vue/jsx-runtime';

export interface OverlayServiceOptions {
    host: any;
    popupPosition?: { left: number; top: number };
    content?: { render(): JSX.Element };
    render?(): any;
    onClickCallback?(): void;
    backgroundColor?: string;
}

function getContentRender(props: OverlayServiceOptions) {
    if (props.content && props.content.render) {
        return props.content.render;
    }
    if (props.render && typeof props.render === 'function') {
        return props.render;
    }
}

function createOverlayInstance(options: OverlayServiceOptions): App {
    const container = document.createElement('div');
    container.style.display = 'contents';
    // eslint-disable-next-line prefer-const
    let overlayApp: App;
    const onClickCallback = options.onClickCallback || (() => { });
    const onClose = () => {
        onClickCallback();
        if (overlayApp) {
            overlayApp.unmount();
        }
    };
    overlayApp = createApp({
        setup() {
            onUnmounted(() => {
                document.body.removeChild(container);
            });
            const contentRender = getContentRender(options);
            return () => (
                <FOverlay popup-content-position={options.popupPosition}
                    host={options.host} onClick={onClose}
                    backgroundColor={options.backgroundColor}
                >
                    {contentRender && contentRender()}
                </FOverlay>
            );
        }
    });
    document.body.appendChild(container);
    overlayApp.mount(container);
    return overlayApp;
}

export default class OverylayService {

    static show(options: OverlayServiceOptions): App {
        return createOverlayInstance(options);
    }
}
