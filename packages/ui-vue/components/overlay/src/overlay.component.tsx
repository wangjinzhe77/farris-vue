/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, ref, SetupContext, Teleport } from 'vue';
import { OverlayProps, overlayProps } from './overlay.props';

export default defineComponent({
    name: 'FOverlay',
    props: overlayProps,
    emits: ['click'] as (string[] & ThisType<void>) | undefined,
    setup(props: OverlayProps, context: SetupContext) {
        const popupPosition = ref<{ left: number; top: number }>(props.popupContentPosition as { left: 0; top: 0 });
        const hostElement = ref(props.host);
       
        const accordionStyle = computed(() => ({
            backgroundColor: props.backgroundColor || '',
            pointerEvents: 'auto'
 
        }));
        function onClickOverlayContainer($event: MouseEvent) {
            context.emit('click');
            $event.preventDefault();
            $event.stopPropagation();
        }

        const position = computed(() => {
            const element = hostElement.value as HTMLElement;
            if (element) {
                const hostElementClientRect = element.getBoundingClientRect();
                const { left, top, height } = hostElementClientRect;
                return { left, top: top + height };
            }
            return popupPosition.value;
        });

        const popupContentContainerStyle = computed(() => {
            const styleObject = {
                // position: 'relative',
                // left: `${position.value.left}px`,
                // top: `${position.value.top}px`
            } as Record<string, any>;
            return styleObject;
        });

        return () => {
            return (
                <Teleport to="body">
                    <div class="overlay-container" onClick={(payload) => onClickOverlayContainer(payload)} style={accordionStyle.value}>
                        <div style={popupContentContainerStyle.value}>{context.slots.default?.()}</div>
                    </div>
                </Teleport>
            );
        };
    }
});
