 
import type { App } from 'vue';
import FEventsEditor from './src/events-editor.component';
import FEventsEditorDesign from './src/designer/events-editor.design.component';
import { propsResolver } from './src/events-editor.props';

export * from './src/types';
export * from './src/events-editor.props';

export { FEventsEditor };

export default {
    install(app: App): void {
        app.component(FEventsEditor.name as string, FEventsEditor);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>): void {
        componentMap['events-editor'] = FEventsEditor;
        propsResolverMap['events-editor'] = propsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
        componentMap['events-editor'] = FEventsEditorDesign;
        propsResolverMap['events-editor'] = propsResolver;
    }
};
