import { ExtractPropTypes, PropType } from "vue";
import { InternalCommand } from "./types";
import { createPropsResolver } from "../../dynamic-resolver/src/props-resolver";
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import eventsEditorSchema from './schema/events-editor.schema.json';
import propertyConfig from './property-config/events-editor.property-config.json';
import { initialData } from '../src/data/initial-data';

export const eventsEditorProps = {
    initialData: { type: Object as PropType<InternalCommand>, default: initialData }
} as Record<string, any>;

export type EventsEditorProps = ExtractPropTypes<typeof eventsEditorProps>;

export const propsResolver = createPropsResolver<EventsEditorProps>(
    eventsEditorProps, eventsEditorSchema, schemaMapper, schemaResolver, propertyConfig);
