import { cloneDeep } from "lodash-es";
import { UseEvents, UseInteraction, UseMethods, UseOriginalData, UseTargetComponent } from "./types";
import { Ref, SetupContext, ref } from "vue";
import { BoundEventItem, Command, InteractionItem, InternalCommand, NewControllerItem, SavedCommandChanged, SwitchOption, ComponentItem } from "../types";
import { useEventsEditorUtil } from "./use-events-editor-util";


export function useMethods(
    initialData: Ref<InternalCommand>,
    useInteractionComposition: UseInteraction,
    context: SetupContext
): UseMethods {
    const { interactions } = useInteractionComposition;
    const { setComponentLists } = useEventsEditorUtil();
    const controllers = ref<Command[]>(initialData.value.internalCommandList);
    /** 方法-导入新方法值变化事件 */
    // function onNewImportChanged(value: boolean) {
    //     newImport.value = value;
    //     initTargetComponent(initialData.value);
    // }

    /** 内置构件选择“导入新方法”后的值 */
    // function onAddInternalCommandListChanged(value: any[]) {
    //     addInternalCommandList.value = cloneDeep(value);
    // }

    /** 方法来源关闭按钮事件 */
    function onClosed(actionIndex: number) {
        interactions.value[actionIndex].showSwitch = [];
        interactions.value[actionIndex].showSwitch.push(cloneDeep({// 存储所有的展示设定
            showSection: [[false, false, false, false, false], [false, false, true, false, false], [false, true, false, true, false]],
            // 展示哪个部分
            showSwitchNumber: 2,
        }));
        // 关闭浮层
        // showPopover.value = false;
        interactions.value = cloneDeep(interactions.value);
        // 若当前事件下没有新增任何方法，则直接删除此事件，并重构事件列表；
        // if (interactions.value[actionIndex].command.length === 0) {
        //     deleteCurrentEvent(actionIndex);
        // }
    }

    /** 方法绑定-下拉框中用户选择绑定的变量 */
    function onCurrentCommandChanged(currentCommand: any[], actionIndex: number, commandItemIndex: number) {
        // eventIndex.value = actionIndex;
        // currentCommand = cloneDeep(currentCommand);
        // interactions.value[actionIndex].showSwitch[commandItemIndex].showSwitchNumber = 2;
        // // 除当前用户点击的这一项展开之外，其余全部收折
        // interactions.value[actionIndex].showSwitch[commandItemIndex].showSection[2][4] = true;
        // // 关闭浮层
        // showPopover.value = false;
        // hasAddNewFunction = false;
    }

    /** 如果最后用户绑定的方法中有新增的控制器，则传出该方法控制器相关参数 */
    function emitNewController(boundEventsList: BoundEventItem[]) {
        const newController: NewControllerItem[] = [];
        initialData.value.newController?.forEach((newControllerItem: NewControllerItem) => {
            boundEventsList.forEach(boundEventsListItem => {
                if (newControllerItem.Code === boundEventsListItem.controller.label) {
                    newController.push(cloneDeep(newControllerItem));
                }
            });
        });
        return newController;
    }

    /** 传出前更新repititionCommand的值 */
    function updateRepititionCommand(boundEventsList: BoundEventItem[]) {
        if (initialData.value.repititionCommand.length !== 0) {
            initialData.value.repititionCommand.forEach((repititionCommandItem: any) => {
                boundEventsList.forEach(boundEventsListItem => {
                    if (repititionCommandItem.command.id === boundEventsListItem.controller.id) {
                        repititionCommandItem.command.params = cloneDeep(boundEventsListItem.command?.property);
                        const targetComponent = boundEventsListItem.command?.targetComponent;
                        boundEventsListItem.command?.componentLists.forEach((componentListsItem: any) => {
                            repititionCommandItem.targetComponent = componentListsItem.componentId === targetComponent ? {
                                id: componentListsItem.componentId,
                                viewModelId: componentListsItem.viewModelId
                            } : repititionCommandItem.targetComponent;
                        });
                    }
                });
            });
            return initialData.value.repititionCommand;
        }
        return undefined;
    }
    /** 值传出事件 */
    function emitFinalState(addNewFunctionState = false, events = null) {
        initialData.value.newFuncEvents = events ? cloneDeep(events) : null;
        initialData.value.isAddControllerMethod = addNewFunctionState;
        const boundEventList: BoundEventItem[] = [];
        const internalCommandList: any[] = cloneDeep(initialData.value.internalCommandList);
        interactions.value.map((interaction: InteractionItem) => {
            const controller = cloneDeep(interaction.controller);
            const command = cloneDeep(interaction.command);
            const boundEvents = cloneDeep(interaction.event);
            const currentInterface: BoundEventItem = { controller, command, boundEvents };
            boundEventList.push(cloneDeep(currentInterface));
        });
        const newController = initialData.value.newController?.length && boundEventList.length ?
            cloneDeep(emitNewController(boundEventList)) : [];
        // 传出前更新repititionCommand的值
        // if (initialData.value.repititionCommand) {
        //     initialData.value.repititionCommand = updateRepititionCommand(boundEventList);
        // }
        // 当前界面显示的所有事件
        const savedCommandList: SavedCommandChanged = {
            /** 拼接名称时的前缀 */
            viewModelId: initialData.value.viewModelId,
            /** 视图模型绑定的事件下的方法列表 */
            savedCommand: cloneDeep(initialData.value.viewModel),
            /** 当前界面显示的绑定事件 */
            boundEventsList: cloneDeep(boundEventList),
            /** 可选择的绑定事件列表 */
            events: cloneDeep(initialData.value.events),
            /** 内置构件列表； */
            internalCommandList: cloneDeep(internalCommandList) as any[],
            /** 所有可选的组件 */
            componentLists: [],
            /** 接收formBasicService.formMetaBasicInfo.relativePath */
            relativePath: initialData.value.relativePath,
            /** 接收formBasicService.envType */
            envType: initialData.value.envType,
            /** 初始为空，用于存储用户点击「导入新方法」的控制器值 */
            newController: cloneDeep(newController),
            /** 切换到代码视图标识 */
            isAddControllerMethod: initialData.value.isAddControllerMethod,
            /** 用户选择「已有方法」 */
            repititionCommand: initialData.value.repititionCommand,
            /** 上一个绑定的方法 */
            preCommand: initialData.value.preCommand,
            /** 新增方法的对应事件 */
            newFuncEvents: initialData.value.newFuncEvents,
        };
        // 值传出事件
        context.emit('savedCommandListChanged', savedCommandList);
    }

    function showMethodList(switchOption: SwitchOption) {
        // 1. 显示方法列表
        switchOption.showSection[switchOption.showSwitchNumber][2] = true;
    }

    function hideMethodList(switchOption: SwitchOption) {
        // 1. 隐藏方法列表
        switchOption.showSection[switchOption.showSwitchNumber][2] = false;
    }

    function showMethodName(switchOption: SwitchOption) {
        // 1. 显示方法名
        switchOption.showSection[switchOption.showSwitchNumber][3] = true;
    }

    function hideMethodName(switchOption: SwitchOption) {
        // 1. 隐藏方法名
        switchOption.showSection[switchOption.showSwitchNumber][3] = false;
    }

    function toggleSelectingMethod(switchOption: SwitchOption) {
        const isSelectingMethod = switchOption.showSection[switchOption.showSwitchNumber][2];
        if (isSelectingMethod) {
            hideMethodList(switchOption);
            showMethodName(switchOption);
        } else {
            showMethodList(switchOption);
            hideMethodName(switchOption);
        }
    }
    /**
     * 获取组件列表
     * @returns 
     */
    function getComponentList(): ComponentItem[] {
        return initialData.value.componentLists;
    }
    /**
     * 处理目标组件的更新
     * @param command 
     */
    function updateTarget(command) {
        initialData.value.viewModel.forEach(viewModelItem => {
            for (let m = 0; m < viewModelItem.controllerList.length; m++) {
                if (viewModelItem.controllerList[m].label === command.label) {
                    viewModelItem.controllerList[m] = cloneDeep(command);
                }
            }
        });
    }
    function getViewModeId(): string {
        return initialData.value.viewModelId;
    }

    function getMethodsOnViewModel(): Array<any> {
        return initialData.value.getEventPath ? initialData.value.getEventPath().viewModelDisplay : [];
    }
    /**
     * 内部更新hasPath属性后，执行此方法
     * @param controllerItem 
     * @param commandItem 
     * @returns 
     */
    function updateCommandHasPath(controllerItem, commandItem) {
        return setComponentLists(controllerItem, commandItem, initialData.value);
    }
    /**
     * 添加新控制器并去重 
    */
    function addNewController(webCommand) {
        initialData.value.newController = initialData.value.newController || [];
        initialData.value.newController.push(cloneDeep(webCommand));
        const webCmds = initialData.value.newController;
        for (let i = 0; i < webCmds.length; i++) {
            for (let j = i + 1; j < webCmds.length; j++) {
                if (webCmds[i].Id === webCmds[j].Id) {
                    webCmds.splice(j, 1);
                    j--;
                }
            }
        }
    }
    /**
     * 检测新增的控制器不存在于原internals中
     * @param importData 
     */
    function checkIfNewControllerExists(importData): boolean {
        let checkExist = false;
        initialData.value.internalCommandList.forEach(internalCommandListItem => {
            if (internalCommandListItem.controllerName.label === importData.controllerName.label) {
                checkExist = true;
                return;
            }
        });
        if (!checkExist) {
            initialData.value.internalCommandList.push(importData);
        }
        return checkExist;
    }

    return {
        controllers,
        onClosed,
        emitFinalState,
        onCurrentCommandChanged,
        toggleSelectingMethod,
        getViewModeId,
        getComponentList,
        updateTarget,
        updateCommandHasPath,
        addNewController,
        checkIfNewControllerExists,
        getMethodsOnViewModel
    };
}
