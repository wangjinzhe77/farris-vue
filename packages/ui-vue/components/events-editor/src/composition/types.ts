import { Ref } from "vue";
import { BoundEventItem, Command, ComponentItem, EventItem, InteractionItem, InternalCommand, SwitchOption } from "../types";

export interface UseEventsEditorUtil {

    isContextDependent: (controllerId: string, commandHandlerName: string) => boolean;

    setComponentLists: (controllerItem: any, commandItem: any, initialData: InternalCommand) => any;

}

export interface UseOriginalData {

    getOriginalData: (initialData: InternalCommand) => void;

    initBoundEvents: (boundEvents: BoundEventItem[], unboundEvents: EventItem[], internalCommandList: Command[]) => void;

    initUnboundEvents: (unboundEvents: EventItem[], internalCommandList: Command[]) => void;

    // originalData: Ref<OriginalDataItem[]>;
}

export interface UseTargetComponent {
    initTargetComponent: (initialData: InternalCommand) => void;
}

export interface UseInteraction {
    addNewInteraction: (selectedEvent: EventItem) => void;

    collapseInteractions: () => void;

    deleteInteracton: (interactionIndex: number) => void;

    interactions: Ref<InteractionItem[]>;

    loadInteractions: () => void;

    toggleMethodInInteraction: (interactionIndex: number) => void;
}

export interface UseEvents {

    allEvents: Ref<EventItem[]>;

    boundEvents: Ref<BoundEventItem[]>;

    candidateEvents: Ref<EventItem[]>;

    excludeFromCandidates: (selectedEvent: EventItem) => void;

    loadEvents: () => void;

    resetBoundEvents: () => void;

    unBoundEvent: (eventToBeUnBound: EventItem) => void;
}

export interface UseMethods {

    controllers: Ref<Command[]>;

    // onAddInternalCommandListChanged: (value: any[]) => void;

    onClosed: (actionIndex: number) => void;

    // onExistChanged: (value: number) => void;

    onCurrentCommandChanged: (currentCommand: any[], actionIndex: number, commandItemIndex: number) => void;
    // onNewImportChanged: (value: boolean) => void;

    toggleSelectingMethod: (switchOption: SwitchOption) => void;
    emitFinalState: (addNewFunctionState, event) => void;
    getComponentList: () => ComponentItem[];
    updateTarget: (command) => void;
    updateCommandHasPath: (controllerItem, commandItem) => any;
    addNewController: (webCommand) => void;
    checkIfNewControllerExists: (importData) => boolean;
    getMethodsOnViewModel: () => Array<any>;
    getViewModeId: () => string
}
