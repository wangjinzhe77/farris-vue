import { cloneDeep } from 'lodash-es';
import { InternalCommand } from '../types';
import { UseEventsEditorUtil } from './types';

export function useEventsEditorUtil(): UseEventsEditorUtil {

    /** 检查匹配是否为需要依赖上下文的方法 */
    function isContextDependent(controllerId: string, commandHandlerName: string): boolean {
        /** 存储所有需要依赖上下文的方法 */
        const contextDependent = [
            {
                controllerId: '45be24f9-c1f7-44f7-b447-fe2ada458a61',
                controllerLabel: 'AdvancedListCardController',
                controllerName: '高级列卡控制器',
                commands: [
                    {
                        name: '增加子表数据',
                        id: '250cd2a2-9995-4c01-64aa-5029afba08ca',
                        handlerName: 'AddItem',
                    },
                    {
                        name: '删除子表数据',
                        id: 'a2c06958-29b1-0582-5f3e-c3cfcc741f8f',
                        handlerName: 'RemoveItem',
                    },
                    {
                        name: '插入数据',
                        id: '3208b00c-543d-c050-3c54-081715972dda',
                        handlerName: 'Insert',
                    },
                    {
                        name: '查询从表数据（分页）',
                        id: 'b60b7754-35cf-7eaa-0f1d-bc5e5d61c0bc',
                        handlerName: 'queryChild',
                    },
                    {
                        name: '批量删除子表',
                        id: '378fcd7d-ce91-c052-5947-d60ecdb38df9',
                        handlerName: 'batchDelete',
                    },
                    {
                        name: '移动数据',
                        id: 'fcac5d5e-2367-2b61-a82a-f8e874e8efc0',
                        handlerName: 'move',
                    },
                    {
                        name: '打开批量编辑界面',
                        id: '8d8b05d6-2d22-b0b7-7cb8-51bbb8cd8a18',
                        handlerName: 'openBatchEditDialog',
                    }
                ]
            },
            {
                controllerId: 'd7ce1ba6-49c7-4a27-805f-f78f42e72725',
                controllerLabel: 'EditableListController',
                controllerName: '可编辑列表控制器',
                commands: [
                    {
                        name: '打开批量编辑界面',
                        id: 'c3831e36-27d1-1c98-dd60-30e2d7ec04d2',
                        handlerName: 'openBatchEditDialog',
                    }
                ]
            },
            {
                controllerId: '8fe977a1-2b32-4f0f-a6b3-2657c4d03574',
                controllerLabel: 'TreeCardController',
                controllerName: '树卡控制器',
                commands: [
                    {
                        name: '新增子表数据',
                        id: '21b0c3af-3caf-b11d-2360-d9af20016501',
                        handlerName: 'AddItem',
                    },
                    {
                        name: '删除子表数据',
                        id: '113f1d77-65a4-63bf-3973-80dd15f294d1',
                        handlerName: 'RemoveItem',
                    },
                    {
                        name: '批量删除子表',
                        id: 'bf13585c-a8f3-384b-356c-bd8bf06751c4',
                        handlerName: 'batchDelete',
                    }
                ]
            },
            {
                controllerId: '8d21e69c-70b3-44f6-88b5-fd6a8d3ce11b',
                controllerLabel: 'PopController',
                controllerName: '弹出控制器',
                commands: [
                    {
                        name: '新增子表数据',
                        id: 'd6933772-8047-9bde-220b-449481883142',
                        handlerName: 'AddItem',
                    },
                    {
                        name: '删除子表数据',
                        id: '03c233eb-e39e-8e34-6809-ee4ab3a0d97c',
                        handlerName: 'RemoveItem',
                    },
                    {
                        name: '加载并新增',
                        id: '6882ab4a-9c23-8d59-6aac-de891eae20d2',
                        handlerName: 'LoadAndAdd',
                    }
                ]
            },
            {
                controllerId: '43f68561-eae4-4495-b318-d629615523f8',
                controllerLabel: 'BatchEditCommands',
                controlleName: '批量编辑控制器',
                commands: [
                    {
                        name: '打开批量编辑界面',
                        id: 'a659aaba-daa4-3c07-8b26-164b01726022',
                        handlerName: 'openBatchEditDialog',
                    },
                    {
                        name: '复制行',
                        id: 'd5f67e0a-767d-a238-5ad4-b1285476c16f',
                        handlerName: 'copyRow',
                    },
                    {
                        name: '复制行',
                        id: '229d2dd3-2fe7-f06a-b705-cfa0fc711614',
                        handlerName: 'clone',
                    }
                ]
            },
            {
                controllerId: 'c121742e-6028-48bf-817c-1dda7fb098df',
                controllerLabel: 'AdvancedListCardWithSidebarController',
                controllerName: '高级列卡控制器（侧边栏）',
                commands: [
                    {
                        name: '增加子表数据（侧边栏）',
                        id: '250cd2a2-9995-4c01-64aa-5029afba08ca',
                        handlerName: 'AddItem',
                    },
                    {
                        name: '删除子表数据（侧边栏）',
                        id: 'a2c06958-29b1-0582-5f3e-c3cfcc741f8f',
                        handlerName: 'RemoveItem',
                    }
                ]
            },
            {
                controllerId: '8172a979-2c80-4637-ace7-b13074d3f393',
                controllerLabel: 'CardController',
                controllerName: '卡片控制器',
                commands: [
                    {
                        name: '新增子表数据',
                        id: '6f987222-ebe2-0f3c-1594-a12408b22801',
                        handlerName: 'AddItem',
                    },
                    {
                        name: '删除子表数据',
                        id: '0cdeddcc-8332-f13f-be96-b5eeac84a334',
                        handlerName: 'RemoveItem',
                    },
                    {
                        name: '插入数据',
                        id: '2666b526-a1b2-f268-1629-b1b95b71c8dd',
                        handlerName: 'Insert',
                    },
                    {
                        name: '打开批量编辑界面',
                        id: '8104dd8e-bb27-a659-ff7c-a0321523f727',
                        handlerName: 'openBatchEditDialog',
                    },
                    {
                        name: '新增子节点（子表树）',
                        id: 'bbd11957-de2a-c727-11d6-64788c4b0fa8',
                        handlerName: 'addSubChild',
                    },
                    {
                        name: '删除子表树节点',
                        id: 'c8c9f652-ba52-3a9b-5eb9-dadd42c832cd',
                        handlerName: 'RemoveTreeItem',
                    },
                    {
                        name: '批量删除子表',
                        id: 'e96857fa-1617-fd92-8a05-5c684e0819a1',
                        handlerName: 'batchDelete',
                    }
                ]
            },
            {
                controllerId: '31c1022c-ab40-4e8d-bc31-85d539f1d36c',
                controllerLabel: 'FileController',
                controllerName: '文件控制器',
                commands: [
                    {
                        name: '批量删除文件数据',
                        id: 'd5bf021b-1aa4-06fe-3236-188fd218f4f7',
                        handlerName: 'removeFileRows',
                    }
                ]
            },
            {
                controllerId: '2eb7bbd1-fabd-4d0f-991d-7242f53225b1',
                controllerLabel: 'AttachmentController',
                controllerName: '附件控制器',
                commands: [
                    {
                        name: '上传并更新行',
                        id: '2a84e28f-7202-d858-1466-748a8040c1f9',
                        handlerName: 'UploadAndUpdateRow',
                    },
                    {
                        name: '通过属性名上传并更新行',
                        id: '0f98c9b8-a01b-55d4-3115-269a73f7ccff',
                        handlerName: 'uploadAndUpdateRowWithPropertyName',
                    },
                    {
                        name: '移除版本附件行',
                        id: 'b441b6a7-9e13-9f75-77a2-10b2c1f7ef26',
                        handlerName: 'RemoveAttachmentItem',
                    }
                ]
            },
        ];

        /** 检查匹配是否为需要依赖上下文的方法 */
        let result = -1;
        const num = contextDependent.findIndex(controllerItem => controllerId === controllerItem.controllerId);
        if (num !== -1) {
            result = contextDependent[num].commands.findIndex(commandItem => commandHandlerName === commandItem.handlerName);
        }
        if (result === -1) {
            return false;
        }
        return true;

    }

    function setComponentLists(controllerItem: any, commandItem: any, initialData: InternalCommand): any {
        // 依赖上下文的方法，hasPath置为true，否则置为false；
        const hasPath = isContextDependent(controllerItem.controllerName.id, commandItem.handlerName);
        // hasPath为true的，componentLists存入值，否则为空；targetComponent有值的，保留值；否则默认值设置为第一个值
        if (initialData.componentLists.length !== 0 && hasPath) {
            commandItem.hasPath = true;
            commandItem.targetComponent = commandItem.targetComponent || initialData.componentLists[0].componentId;
            commandItem.componentLists = cloneDeep(initialData.componentLists);
        }
        return commandItem;
    }

    return { isContextDependent, setComponentLists };
}
