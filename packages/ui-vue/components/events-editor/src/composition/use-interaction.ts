import { Ref, ref } from "vue";
import { cloneDeep } from "lodash-es";
import { BoundEventItem, EventItem, InteractionItem, InternalCommand, SwitchOption } from "../types";
import { UseEvents, UseInteraction } from "./types";

export function useInteraction(
    initialData: Ref<InternalCommand>,
    useEventsComposition: UseEvents,
): UseInteraction {
    const interactions = ref<InteractionItem[]>([]);
    const { boundEvents, unBoundEvent } = useEventsComposition;

    function createInteraction(boundEventItem: BoundEventItem) {
        const interaction = {
            /** 当前事件名称及label */
            event: {
                label: boundEventItem.boundEvents.label,
                name: boundEventItem.boundEvents.name,
            },
            /** 控制组件的展示及顺序:
             * 0：事件下拉框
             * 1：事件名称
             * 2：来源+方法列表
             * 3：方法名
             * 4：方法参数*/
            showSwitch: [{
                // 存储所有的展示设定
                showSection: [[false, false, false, false, false], [false, false, true, false, false], [false, true, false, true, false]],
                // 展示哪个部分
                showSwitchNumber: 2,
            }],
            /** 方法列表 */
            command: cloneDeep(boundEventItem.command),
            /** 当前的控制器 */
            controller: cloneDeep(boundEventItem.controller)
        } as InteractionItem;
        return interaction;
    }

    function loadInteractions() {
        interactions.value = boundEvents.value?.map((boundEventItem: BoundEventItem) => createInteraction(boundEventItem));
    }

    function addNewInteraction(selectedEvent: EventItem) {
        const eventToBeBinded = {
            boundEvents: selectedEvent,
            command: null,
            controller: { id: '', name: '', label: '' }
        } as BoundEventItem;
        const newInteraction = createInteraction(eventToBeBinded);
        newInteraction.showSwitch[0].showSwitchNumber = 1;
        newInteraction.showSwitch[0].showSection[2][3] = true;
        interactions.value.unshift(newInteraction);
    }

    function collapseInteractions() {
        // 1. 收折其他已展示的事件列表
        interactions.value?.forEach((interactionItem: InteractionItem) => {
            interactionItem.showSwitch.forEach((switchOption: SwitchOption) => {
                switchOption.showSection[2][3] = false;
            });
        });
    }

    /** 事件-事件收折：隐藏方法列表 */
    function toggleMethodInInteraction(interactionIndex: number) {
        // 仅显示事件名称；
        interactions.value[interactionIndex].showSwitch.forEach((switchOption: SwitchOption) => {
            switchOption.showSection[2][3] = !switchOption.showSection[2][3];
        });
    }

    function deleteInteracton(interactionIndex: number) {
        if (interactionIndex >= 0 && interactionIndex <= interactions.value.length - 1) {
            // 1. 解除待移除交换中的绑定事件
            const interactionToBeRemoved = interactions.value[interactionIndex];
            unBoundEvent(interactionToBeRemoved.event);
            // 2. 移除交互
            interactions.value.splice(interactionIndex, 1);
        }
    }

    return {
        addNewInteraction,
        collapseInteractions,
        deleteInteracton,
        toggleMethodInInteraction,
        interactions,
        loadInteractions
    };
}
