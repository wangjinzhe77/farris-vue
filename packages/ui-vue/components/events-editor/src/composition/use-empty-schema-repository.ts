import { SchemaCategory, SchemaItem, SchemaRepositoryPagination, UseSchemaRepository } from "../../../../components/schema-selector";

export function useEmptySchemaRepository(): UseSchemaRepository {

    function getNavigationData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaCategory[] {
        return [];
    }

    function getRecentlyData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return [];
    }

    function getRecommandData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return [];
    }

    function getSchemaData(searchingText: string, pagination: SchemaRepositoryPagination): SchemaItem[] {
        return [];
    }

    return { getNavigationData, getRecentlyData, getRecommandData, getSchemaData };
}
