import { cloneDeep } from "lodash-es";
import { BoundEventItem, EventItem, GetEventResult, InternalCommand } from "../types";
import { UseEvents } from "./types";
import { Ref, ref } from "vue";
import { useEventsEditorUtil } from "./use-events-editor-util";

export function useEvents(initialData: Ref<InternalCommand>): UseEvents {

    const allEvents = ref<EventItem[]>([]);

    const boundEvents = ref<BoundEventItem[]>([]);

    const candidateEvents = ref<EventItem[]>([]);
    const { isContextDependent } = useEventsEditorUtil();

    /** 目标组件：根据设定的规则，确定可选的目标组件的范围 */
    function initTargetComponent() {
        const { allComponentList } = initialData.value;
        // 若componentLists为空，不显示目标组件下拉框（hasPath=false），默认添加到自己的viewmodel下
        if (initialData.value.componentLists.length && initialData.value.viewModel) {
            initialData.value.viewModel.forEach(controllerItem => {
                controllerItem.controllerList.forEach(commandItem => {
                    if (commandItem['targetComponent']) {
                        // 判断targetComponent的vm是否和控件所在vm相同
                        const componentList = allComponentList
                            .find(componentListsItem => componentListsItem.componentId === commandItem.targetComponent);
                        const targetVmId = componentList ? componentList.viewModelId : undefined;
                        if (targetVmId === initialData.value.viewModelId) {
                            // 一段式
                            commandItem.hasPath = false;
                        }
                        else {
                            // 三段式
                            const hasPath = isContextDependent(controllerItem.controllerName.id, commandItem.handlerName);
                            commandItem.hasPath = hasPath ? true : commandItem.hasPath;
                            commandItem.componentLists = hasPath ? cloneDeep(initialData.value.componentLists) : commandItem.componentLists;
                        }
                    }
                });
            });
        }
        if (initialData.value.componentLists.length && initialData.value.boundEventsList) {
            initialData.value.boundEventsList.forEach(boundEventsListItem => {
                if (boundEventsListItem.command?.targetComponent) {
                    const componentList = allComponentList
                        .find(componentListsItem =>
                            componentListsItem.componentId === (boundEventsListItem.command?.targetComponent || '')
                        );
                    const targetVmId = componentList ? componentList.viewModelId : undefined;
                    if (targetVmId === initialData.value.viewModelId) {
                        // 一段式
                        boundEventsListItem.command['hasPath'] = false;
                    }
                    else {
                        // 三段式
                        const hasPath = isContextDependent(
                            boundEventsListItem.controller['id'],
                            boundEventsListItem.command['handlerName']
                        );
                        boundEventsListItem.command['hasPath'] = hasPath ? true : boundEventsListItem.command['hasPath'];
                        boundEventsListItem.command['componentLists'] = hasPath ?
                            cloneDeep(initialData.value.componentLists) : boundEventsListItem.command['componentLists'];
                    }
                }
            });
        }
    }

    /** 得到原始数据-1.组合「新建交互」下拉框展示值; */
    function getEventsArray() {
        let eventsArray = cloneDeep(initialData.value.events || []);
        initialData.value.boundEventsList.forEach(boundEventsListItem => {
            initialData.value.events.forEach(function (eventsItem, index) {
                // 如果events列表中有已绑定的事件，则用已空数组来替换原有的数据,仅保留没有选择参数的事件
                if (boundEventsListItem.boundEvents.label === eventsItem.label) { eventsArray.splice(index, 1, [] as any); }
            });
        });
        // eventsArrayItem.length != 0 此处判断有问题===========================TODO
        eventsArray = eventsArray.filter(function (eventsArrayItem) { return !!eventsArrayItem; });
        return eventsArray;
    }


    /** 得到原始数据-1.组合「新建交互」下拉框展示值;*/
    function getUnboundEvents(): EventItem[] {
        const boundEventLabelMap = boundEvents.value.reduce<Map<string, boolean>>(
            (labelMap: Map<string, boolean>, boundEventItem: BoundEventItem) => {
                labelMap.set(boundEventItem.boundEvents.label, true);
                return labelMap;
            }, new Map<string, boolean>);
        const unboundEvents = allEvents.value.filter((eventItem: EventItem) => !boundEventLabelMap.has(eventItem.label));
        return unboundEvents;
    }

    function resetCandidateEvents() {
        const hasBoundEvents = boundEvents.value?.length > 0;
        // 1. 筛选出已绑定事件，保留未绑定事件用于事件下拉框展示
        // 2. 若没有事件存在绑定值，则直接将所有事件放在「新建交互」下拉框
        candidateEvents.value = hasBoundEvents ? getUnboundEvents() : allEvents.value;
    }

    function loadEvents() {           
        initTargetComponent();  
        allEvents.value = initialData.value.events;
        boundEvents.value = initialData.value.boundEventsList;
        resetCandidateEvents();
    }

    function excludeFromCandidates(selectedEvent: EventItem) {
        candidateEvents.value = candidateEvents.value.filter((eventItem: EventItem) => eventItem.label !== selectedEvent.label);
    }

    function unBoundEvent(eventToBeUnBound: EventItem) {
        boundEvents.value = boundEvents.value
            .filter((boundEvent: BoundEventItem) => boundEvent.boundEvents.label !== eventToBeUnBound.label);
        resetCandidateEvents();
    }

    /** 移除已绑定的事件 */
    function resetBoundEvents() {
        const eventsFunc = initialData.value?.getEventList() as GetEventResult;
        const { events } = eventsFunc;
        if (eventsFunc) {
            initialData.value.events = cloneDeep(events);
            initialData.value.boundEventsList = eventsFunc.boundEventsList;
        }
        if (initialData.value.boundEventsList) {
            initialData.value.boundEventsList = initialData.value.boundEventsList.filter(function (boundEventsListItem) {
                return events.find(event => event.label === boundEventsListItem.boundEvents.label);
            });
        }
    }

    return {
        allEvents,
        boundEvents,
        candidateEvents,
        excludeFromCandidates,
        loadEvents,
        resetBoundEvents,
        unBoundEvent
    };
}
