export const events = [
    {
        "label": "pageChange",
        "name": "MockMock切换页码"
    },
    {
        "label": "pageSizeChanged",
        "name": "Mock分页条数变化事件"
    },
    {
        "label": "scrollYLoad",
        "name": "Mock滚动加载事件"
    },
    {
        "label": "onEditClicked",
        "name": "Mock操作列点击编辑"
    },
    {
        "label": "onDeleteClicked",
        "name": "Mock操作列点击删除"
    },
];

export const boundEventsList = [
    {
        boundEvent: {
            "label": "pageChange",
            "name": "Mock切换页码"
        },
        controller: {
            id: '70b4abd4-9f2c-4b7c-90e9-6ac6f4b74c72',
            label: 'ListController',
            name: '列表控制器'
        },
        command: {
            "id": "1a1b7c33-38f0-469f-a017-223086ee6259",
            "label": "ChangePage1",
            "name": "Mock切换页码",
            "handlerName": "ChangePage",
            "params": [
                {
                    "name": "MockloadCommandName",
                    "shownName": "Mock切换页面后回调方法",
                    "value": "Filter1",
                    "defaultValue": null
                },
                {
                    "name": "MockloadCommandFrameId",
                    "shownName": "Mock目标组件",
                    "value": "root-component",
                    "defaultValue": null
                }
            ],
            "cmpId": "70b4abd4-9f2c-4b7c-90e9-6ac6f4b74c72",
            "isNewGenerated": false,
            "isInvalid": false,
            "targetComponent": "data-grid-component"
        }
    },
    {
        boundEvent: {
            "label": "pageSizeChanged",
            "name": "Mock分页条数变化事件"
        },
        controller: {
            id: '70b4abd4-9f2c-4b7c-90e9-6ac6f4b74c72',
            label: 'ListController',
            name: '列表控制器'
        },
        command: {
            "id": "1a1b7c33-38f0-469f-a017-223086ee6259",
            "label": "ChangePage1",
            "name": "Mock切换页码",
            "handlerName": "ChangePage",
            "params": [
                {
                    "name": "loadCommandName",
                    "shownName": "Mock切换页面后回调方法",
                    "value": "Filter1",
                    "defaultValue": null
                },
                {
                    "name": "loadCommandFrameId",
                    "shownName": "Mock目标组件",
                    "value": "root-component",
                    "defaultValue": null
                }
            ],
            "cmpId": "70b4abd4-9f2c-4b7c-90e9-6ac6f4b74c72",
            "isNewGenerated": false,
            "isInvalid": false,
            "targetComponent": "data-grid-component"
        }
    },
    {
        boundEvent: {
            "label": "scrollYLoad",
            "name": "Mock滚动加载事件"
        },
        controller: {
            id: '70b4abd4-9f2c-4b7c-90e9-6ac6f4b74c72',
            label: 'ListController',
            name: '列表控制器'
        },
        command: {
            "id": "1a1b7c33-38f0-469f-a017-223086ee6259",
            "label": "ChangePage1",
            "name": "Mock切换页码",
            "handlerName": "ChangePage",
            "params": [
                {
                    "name": "loadCommandName",
                    "shownName": "Mock切换页面后回调方法",
                    "value": "Filter1",
                    "defaultValue": null
                },
                {
                    "name": "loadCommandFrameId",
                    "shownName": "Mock目标组件",
                    "value": "root-component",
                    "defaultValue": null
                }
            ],
            "cmpId": "70b4abd4-9f2c-4b7c-90e9-6ac6f4b74c72",
            "isNewGenerated": false,
            "isInvalid": false,
            "targetComponent": "data-grid-component"
        }
    },
    {
        boundEvent: {
            "label": "onEditClicked",
            "name": "Mock操作列点击编辑"
        },
        controller: {
            id: '70b4abd4-9f2c-4b7c-90e9-6ac6f4b74c72',
            label: 'ListController',
            name: '列表控制器'
        },
        command: {
            "id": "76bb66ba-72ac-43b5-a288-fb126dfb846f",
            "label": "datagridcomponentviewmodelEdit1",
            "name": "Mock编辑报销单",
            "handlerName": "Edit",
            "params": [
                {
                    "name": "url",
                    "shownName": "Mock功能菜单标识",
                    "value": "c5aede54-ebe1-48fd-bb60-b5deccb7594c",
                    "defaultValue": null
                },
                {
                    "name": "params",
                    "shownName": "Mock附加参数",
                    "value": "{\"action\":\"LoadAndEdit1\", \"id\":\"{DATA~/data-grid-component/id}\"}",
                    "defaultValue": null
                },
                {
                    "name": "idToEdit",
                    "shownName": "Mock待编辑数据的标识",
                    "value": "",
                    "defaultValue": null
                },
                {
                    "name": "enableRefresh",
                    "shownName": "Mock支持刷新列表数据",
                    "value": "",
                    "defaultValue": null
                },
                {
                    "name": "tabName",
                    "shownName": "Mock标签页标题",
                    "value": "",
                    "defaultValue": null
                },
                {
                    "name": "destructuring",
                    "shownName": "Mock是否解构参数",
                    "value": "",
                    "defaultValue": null
                }
            ],
            "cmpId": "70b4abd4-9f2c-4b7c-90e9-6ac6f4b74c72",
            "isNewGenerated": false,
            "isInvalid": false,
            "targetComponent": "data-grid-component"
        }
    },
    {
        boundEvent: {
            "label": "onDeleteClicked",
            "name": "Mock操作列点击删除"
        },
        controller: {
            id: '70b4abd4-9f2c-4b7c-90e9-6ac6f4b74c72',
            label: 'ListController',
            name: '列表控制器'
        },
        command: {
            "id": "6a6a1356-edbb-4d89-bdaf-31b72986fc98",
            "label": "datagridcomponentviewmodelRemove1",
            "name": "Mock删除报销单",
            "handlerName": "Remove",
            "params": [
                {
                    "name": "id",
                    "shownName": "Mock待删除数据的标识",
                    "value": "{DATA~/data-grid-component/id}",
                    "defaultValue": null
                },
                {
                    "name": "refreshCommandName",
                    "shownName": "Mock删除后回调方法",
                    "value": "",
                    "defaultValue": null
                },
                {
                    "name": "refreshCommandFrameId",
                    "shownName": "Mock目标组件",
                    "value": "",
                    "defaultValue": null
                },
                {
                    "name": "successMsg",
                    "shownName": "Mock删除成功提示信息",
                    "value": "",
                    "defaultValue": null
                }
            ],
            "cmpId": "70b4abd4-9f2c-4b7c-90e9-6ac6f4b74c72",
            "isNewGenerated": false,
            "isInvalid": false,
            "targetComponent": "data-grid-component"
        }
    }
];

export const initialData = {
    propertyID: 'data-grid-component-viewmodel',
    propertyType: 'events',
    /** 内置构件显示的命令列表 */
    internalCommandList: [
        {
            controllerName: {
                id: '70b4abd4-9f2c-4b7c-90e9-6ac6f4b74c72',
                label: 'ListController',
                name: '列表控制器'
            },
            controllerList: [
                {
                    "id": "93ee1cd2-cf0b-40b3-b99f-958a3d1fad1c",
                    "label": "Load",
                    "name": "Mock加载数据",
                    "handlerName": "Load"
                },
                {
                    "id": "71ae8a4c-6202-4875-9246-2e2d959da37f",
                    "label": "Search",
                    "name": "Mock查询",
                    "handlerName": "Search"
                },
                {
                    "id": "77556491-41c0-4356-8ccf-25e39817060e",
                    "label": "Add",
                    "name": "Mock添加数据",
                    "handlerName": "Add"
                },
                {
                    "id": "52fdcac3-46c8-466e-aa5d-19920ece2076",
                    "label": "View",
                    "name": "Mock查看数据",
                    "handlerName": "View"
                },
                {
                    "id": "7ade9996-0531-4401-b1bc-fb9ec8ee3f8e",
                    "label": "Edit",
                    "name": "Mock编辑数据",
                    "handlerName": "Edit"
                },
                {
                    "id": "6d5a354f-871f-43e6-82bc-7837184380d3",
                    "label": "RemoveRows",
                    "name": "Mock删除选中行",
                    "handlerName": "RemoveRows"
                },
                {
                    "id": "1a1b7c33-38f0-469f-a017-223086ee6259",
                    "label": "ChangePage",
                    "name": "Mock切换页码",
                    "handlerName": "ChangePage"
                },
                {
                    "id": "debae2dd-3387-48cf-90ba-96e74ab5a8e5",
                    "label": "Remove",
                    "name": "Mock删除指定数据",
                    "handlerName": "Remove"
                }
            ]
        }
    ],
    /** (事件设定)可绑定事件默认列表; */
    events,
    /** 已绑定的事件（拼接已有的参数值，拼接当前事件->待集成-从dom结构中取值，事件及对应的字符串） */
    boundEventsList: [],
    /** 视图模型已有命令 */
    viewModel: [],
    /** 目标组件对应的所有可选的组件 */
    componentLists: [
        { componentId: 'root-component', viewModelId: 'root-viewmodel' },
        { componentId: 'data-grid-component', viewModelId: 'data-grid-component-viewmodel' }
    ],
    /** 所有组件列表 */
    allComponentList: [
        { componentId: 'root-component', viewModelId: 'root-viewmodel' },
        { componentId: 'data-grid-component', viewModelId: 'data-grid-component-viewmodel' }
    ],
    /** 初始为空，用于存储用户点击「导入新命令」的控制器值 */
    newController: [],
    isAddControllerMethod: false,
    getEventList: () => { return { events, boundEventsList: [] }; }
};
