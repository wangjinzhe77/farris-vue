 
export interface PropertyItem {
    /** 参数名 */
    name: string;
    /** 方法参数值 */
    value: string;
    /** 方法参数名 */
    shownName: string;
    /** 方法说明 */
    description: string;
    /** 参数ID */
    id: string;
    /** 编辑器类型 */
    editorType: string;
    /** 是否有返回值 */
    isRetVal: boolean;
    /** 参数类型 */
    parameterType: string;
    origin: any;
}

/** 内置构件对应的单个控制器的单个方法 */
export interface CommandItem {
    /** 方法id */
    id: any;
    /** 方法编号 */
    label: string;
    /** 方法中文名 */
    name: string;
    /** 无前后缀的方法 */
    handlerName: string;
    /** 当前方法需要选择目标组件*/
    hasPath: boolean;
    /** 当前方法选择的目标组件的id*/
    // targetComponent示例：root-component
    targetComponent?: string;
    cmpId: string;
    componentLists?: any;
    /** 单个方法下的多种参数 */
    property?: PropertyItem[];
    shortcut?: any;
    /** 在控制器中增加命令后，再次用新命令替代 */
    isNewGenerated?: boolean;
    extensions?: [];
    isInvalid?: boolean;
    /** isRTCmd=true 运行时定制的命令，可以删改 */
    isRTCmd?: boolean;
}

export interface ControllerName {
    /** 控制器编码 */
    id: string;
    /** 控制器编号 */
    label: string;
    /** 控制器中文名 */
    name: string;
}

/** 内置构件对应的单个控制器方法列表 */
export interface Command {
    /** 控制器名称 */
    controllerName: ControllerName;
    /** 当前控制器下的方法列表 */
    controllerList: CommandItem[];
}

export interface MethodChangedEventArgs {
    method: CommandItem;
    controller: Command;
}

/** 可选择的绑定事件列表 */
export interface EventItem {
    /** 事件编号 */
    label: string;
    /** 事件中文名 */
    name: string;
}

/** 单个事件绑定变化 
 原 controllerList=》command,
 原 controllerName=》controller
  { 
    "command": { "label": "loadCard1", "name": "加载卡片数据", "id": "8788c27e-722a-4b98-9d57-98eafb526fe5", "handlerName": "loadCard", "showTargetComponent": false, "cmpId": "", "componentLists": [], "isInvalid": false, "property": [], "targetComponent": "data-grid-component" }, 
    "controller": { "label": "ListCardController", "name": "列卡控制器", "id": "7c48ef46-339c-42d4-8365-a21236c63044" }, 
    "boundEvents": { "label": "onSelectionChange", "name": "行切换事件" } 
};
*/
export interface BoundEventItem {
    boundEvents: EventItem;
    /** 当前界面显示的绑定事件-控制器名称 */
    controller: ControllerName;
    /** 当前界面显示的绑定事件-方法 */
    command: CommandItem | null;
}

/** 所有可选的组件 */
export interface ComponentItem {
    /** 组件id */
    componentId: string;
    /** 组件对应的视图模型的id */
    viewModelId: string;
}

/** 控制器参数 */
export interface NewControllerItem {
    Code?: string;
    Commands?: [];
    Description?: any;
    Extends?: any;
    Id?: string;
    Name?: string;
}

export interface GetEventResult {
    events: EventItem[];

    boundEventsList: BoundEventItem[];

    internalCommandList: Command[];
}

/** 内置构件显示的方法列表 */
export interface InternalCommand {
    /** 拼接名称时的前缀 */
    viewModelId: string;
    /** 内置构件显示的方法列表 */
    internalCommandList: Command[];
    /** 视图模型存储的方法列表 */
    viewModel: Command[];
    /** 可选择的绑定事件列表 */
    events: EventItem[];
    /** 已绑定的事件列表 */
    boundEventsList: BoundEventItem[];
    /** 所有可选的组件 */
    componentLists: ComponentItem[];
    /** 所有组件列表 */
    allComponentList: ComponentItem[];
    /** 接收this.formBasicService.formMetaBasicInfo.relativePath */
    relativePath: string;
    /** 接收this.formBasicService.envType */
    envType: string;
    /** 初始为空，用于存储用户点击「导入新方法」的控制器值 */
    newController?: NewControllerItem[];
    /** 切换到代码视图标识 */
    isAddControllerMethod: boolean;
    getEventList: () => GetEventResult;
    /** 是否为非frame类型&非form-xx类型 */
    showTargetComponent: boolean;
    /** 获取事件的路径 */
    getEventPath?: () => any;
    /** 当前存在重复使用的方法 */
    repititionCommand?: any;
    /** 该事件上一次绑定的方法 */
    preCommand?: any;
    /** 是否为内置命令 */
    isPreset?: boolean;
    /** 「新增方法」的对应事件 */
    newFuncEvents?: any;
    /** 点击「新增方法」后返回的具体方法 */
    getFuncCommand?: () => any;
}

/** 视图模型存储的交互事件变化 */
export interface SavedCommandChanged {
    /** 拼接名称时的前缀 */
    viewModelId: string;
    /** 视图模型绑定的事件下的方法列表 */
    savedCommand: Command[];
    /** 当前界面显示的绑定事件 */
    boundEventsList: BoundEventItem[];
    /** 可选择的绑定事件列表 */
    events: EventItem[];
    /** 内置构件列表 */
    internalCommandList: Command[];
    /** 所有可选的组件 */
    componentLists: ComponentItem[];
    /** 接收this.formBasicService.formMetaBasicInfo.relativePath */
    relativePath: string;
    /** 接收this.formBasicService.envType */
    envType: string;
    /** 初始为空，用于存储用户点击「导入新方法」的控制器值 */
    newController?: any[];
    isAddControllerMethod?: boolean;
    /** 当前存在重复使用的方法 */
    repititionCommand?: any;
    /** 事件上一次绑定的方法 */
    preCommand?: any;
    /** 新增方法的对应事件 */
    newFuncEvents?: any;
}

export interface SwitchOption {
    showSection: [boolean, boolean, boolean, boolean, boolean][];
    showSwitchNumber: number;
}

export interface InteractionItem {
    /** 当前事件名称及label */
    event: EventItem;

    showSwitch: SwitchOption[];

    command: CommandItem;

    controller: ControllerName;
}
