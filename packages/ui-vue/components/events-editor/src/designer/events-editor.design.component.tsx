/* eslint-disable no-use-before-define */
 
import { SetupContext, defineComponent, inject, onBeforeMount, onMounted, provide, ref } from "vue";
import { EventsEditorProps, eventsEditorProps } from "../events-editor.props";
import { EventItem, InteractionItem, InternalCommand } from "../types";
import { useEventsEditorUtil } from "../composition/use-events-editor-util";
import { useInteraction } from "../composition/use-interaction";
import { useEvents } from "../composition/use-events";

import InteractionButton from '../components/interaction-button/interaction-button.component';
import InteractionItemComponent from '../components/interaction-item/interaction-item.component';
import { UseEvents, UseInteraction, UseMethods } from "../composition/types";
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';

import '../events-editor.css';
import { useMethods } from "../composition/use-methods";

export default defineComponent({
    name: 'FEventsEditorDesign',
    props: eventsEditorProps,
    emits: ['savedCommandListChanged'] as (string[] & ThisType<void>) | undefined,
    setup(props: EventsEditorProps, context: SetupContext) {
        const initialData = ref<InternalCommand>(props.initialData as InternalCommand);
        /** 入参数据处理-生成内部使用的层级结构 */
        // const originalData = ref<OriginalDataItem[]>([]);
        const useEventsEditorUtilComposition = useEventsEditorUtil();
        const { isContextDependent } = useEventsEditorUtilComposition;
        // const useInteractionCompostion = useInteraction(initialData, useOriginalDataComposition);
        // const { interactions } = useInteractionCompostion;

        const useEventsComposition = useEvents(initialData);
        const { loadEvents, resetBoundEvents } = useEventsComposition;

        const useInteractionCompostion = useInteraction(initialData, useEventsComposition);
        const { addNewInteraction, deleteInteracton, interactions, loadInteractions } = useInteractionCompostion;

        const useMethodsComposition = useMethods(initialData, useInteractionCompostion, context);

        provide<UseEvents>('useEventsComposition', useEventsComposition);
        provide<UseInteraction>('useInteractionCompostion', useInteractionCompostion);
        provide<UseMethods>('useMethodsComposition', useMethodsComposition);

        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const componentInstance = useDesignerComponent(elementRef, designItemContext);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
             // 重置切换到代码视图的标识
             resetDataVmLabel();
              // 移除已绑定的事件
            resetBoundEvents();
            // 加载可选事件
            loadEvents();
            // 加载已有交互
            loadInteractions();
        });

        context.expose(componentInstance.value);

        /** 重置切换到代码视图的标识 */
        function resetDataVmLabel() {
            initialData.value.isAddControllerMethod = false;
        }

        // onBeforeMount(() => {           
        //     // 移除已绑定的事件
        //     resetBoundEvents();
        //     // 加载可选事件
        //     loadEvents();
        //     // 加载已有交互
        //     loadInteractions();
        // });

        function onDeleteInteraction(interactionIndex: number) {
            deleteInteracton(interactionIndex);
        }

        function renderInteraction(interaction: InteractionItem, displayOrder: number) {
            const switchElement = interaction.showSwitch[0];
            const collapsed = switchElement.showSection[switchElement.showSwitchNumber][3];
            return (
                <InteractionItemComponent
                    interaction={interaction}
                    displayOrder={displayOrder}
                    collapsed={collapsed}
                    onDelete={onDeleteInteraction}
                ></InteractionItemComponent>
            );
        }

        /** 根据入参展示所有已绑定事件的状态 */
        function renderInteractionList() {
            return interactions.value?.map((interaction: InteractionItem, displayOrder: number) => {
                return <div class="events-display-order" key={interaction.event.label}>
                    {renderInteraction(interaction, displayOrder)}
                </div>;
            });
        }

        function onClickAddActionButton(selectedEvent: EventItem) {
            addNewInteraction(selectedEvent);
        }

        return () => {
            return (
                <div ref={elementRef} class="f-page-events-editor">
                    <div class="f-page-events-editor-content">
                        <InteractionButton onClick={onClickAddActionButton}></InteractionButton>
                        {renderInteractionList()}
                    </div>
                </div>
            );
        };
    }
});
