import { ExtractPropTypes } from "vue";
import { EventItem } from "../../types";

export const eventListProps = {
    events: { type: Array<EventItem[]>, default: [] }
} as Record<string, any>;

export type EventListProps = ExtractPropTypes<typeof eventListProps>;
