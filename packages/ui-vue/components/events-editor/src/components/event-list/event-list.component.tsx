import { SetupContext, defineComponent, ref } from "vue";
import { EventListProps, eventListProps } from "./event-list.props";
import { EventItem } from "../../types";
/**
 * 点击 【新建交互事件】按钮，展开的事件列表
 */
export default defineComponent({
    name: 'FEventList',
    props: eventListProps,
    emits: ['click'] as (string[] & ThisType<void>) | undefined,
    setup(props: EventListProps, context: SetupContext) {
        const events = ref<EventItem[]>(props.events);

        function onClickEventItem(selectedEvent: EventItem) {
            context.emit('click', selectedEvent);
        }

        function renderEventItem(eventItem: EventItem) {
            return (
                <div class="f-page-events-editor-first-bound-event">
                    <div onClick={() => onClickEventItem(eventItem)} class="f-page-events-editor-first-bound-event-name">
                        {eventItem.name}</div>
                </div>
            );
        }

        return () => {
            return (
                <div class="f-page-events-editor-events">
                    {events.value.map((eventItem: EventItem) => renderEventItem(eventItem))}
                </div>
            );
        };

    },
});
