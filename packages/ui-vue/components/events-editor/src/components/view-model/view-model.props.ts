import { ExtractPropTypes } from "vue";

export const viewModelProps = {} as Record<string, any>;

export type ViewModelProps = ExtractPropTypes<typeof viewModelProps>;
