import { SetupContext, defineComponent, ref } from "vue";
import { ViewModelProps, viewModelProps } from "./view-model.props";
import InputGroup from '@farris/ui-vue/components/input-group';

export default defineComponent({
    name: 'FViewModel',
    props: viewModelProps,
    emits: [],
    setup(props: ViewModelProps, context: SetupContext) {
        const viewModelData = ref<any>([]);
        const groupIcon = ref('');
        const searchValue = ref('');

        function onChange() {

        }

        function onClear() {

        }

        function onClickHandle() {

        }

        function renderViewModelSearchBar() {
            return (
                <div class="f-vm-search">
                    <div class="f-vm-searchBar">
                        <InputGroup groupText={groupIcon.value} v-model={searchValue.value}
                            onChange={onChange} onClear={onClear} onClickHandle={onClickHandle}></InputGroup>
                    </div>
                </div>
            );
        }

        function viewModelItemClass(mapItem: any) {
            const classObject = {
                'f-vm-item': !mapItem.active,
                'f-vm-item-focus': mapItem.active
            } as Record<string, boolean>;
            return classObject;
        }

        function getViewModelCommand(mapItem: any, viewModelDataItem: any) {

        }

        function viewModelItemCommandClass(mapItem: any) {
            const classObject = {
                'f-event-func': !mapItem.command.isInvalid,
                'f-event-func-invalid': mapItem.command.isInvalid
            } as Record<string, any>;
            return classObject;

        }

        function renderViewModelMapItem(viewModelDataItem: any) {
            return viewModelDataItem.sourceComponent.map.filter((mapItem: any) => !mapItem.hide)
                .map((mapItem: any) => {
                    return (
                        <div class={viewModelItemClass(mapItem)} onClick={() => getViewModelCommand(mapItem, viewModelDataItem)}>
                            <div class="f-event-commandItem">
                                <div class="f-icon-command">
                                    <div class="f-icon f-icon-source-code" style="font-size:10px;color:#30B471;"> </div>
                                </div>
                                <div class={viewModelItemCommandClass(mapItem)}>
                                    {mapItem.command.name}({mapItem.command.label})
                                </div>
                            </div>
                            <div class="f-event-path">{viewModelDataItem.path}{mapItem.event.name}</div>
                        </div>
                    );
                });
        }

        function renderViewModels() {
            return viewModelData.value.length > 0 &&
                <div class="f-vm-viewModel">
                    {viewModelData.value.map((viewModelDataItem: any) => {
                        return renderViewModelMapItem(viewModelDataItem);
                    })};
                </div>;

        }

        return () => {
            return (
                <div class="f-vm-all">
                    {renderViewModelSearchBar()}
                    {renderViewModels()}
                </div>
            );
        };
    },
});
