import { SetupContext, computed, defineComponent, inject, ref } from "vue";
import { CommandSourceProps, commandSourceProps } from "./command-source.props";
import { Command, CommandItem, MethodChangedEventArgs, PropertyItem } from "../../types";
import { FSchemaSelector } from '@farris/ui-vue/components/schema-selector';
import { F_MODAL_SERVICE_TOKEN } from '@farris/ui-vue/components/modal';
import { UseMethods } from '../../composition/types';
import './command-source.css';

import CommandList from '../command-list/command-list.component';
import { ControllerSchemaRepositorySymbol } from "@farris/ui-vue/components/common";
import { cloneDeep } from "lodash-es";
import { FNotifyService as NotifyService } from "@farris/ui-vue/components/notify";
/**
 * 命令来源
 * 选择事件后，点击【+】展开的下拉面板，下拉面板的内容：
 * 命令分类
 *      命令列表
 * -------------
 *  引入控制器 按钮
 *  添加新方法 按钮
 *  已有方法 按钮
 * ---------------
 *   取消 按钮
 *
 */
export default defineComponent({
    name: 'FCommandSource',
    props: commandSourceProps,
    emits: [
        'addInternalCommandListChanged',
        'newImportChanged',
        'currentCommandChanged',
        'originalDataChanged',
        'click',
        'existChanged',
        'newFunctionChanged',
        'close',
        'selectMethod'
    ] as (string[] & ThisType<void>) | undefined,
    setup(props: CommandSourceProps, context: SetupContext) {
        const methodsComposition = inject<UseMethods>('useMethodsComposition') as UseMethods;
        const formCommandService = inject('useFormCommand') as any;
        const notifyService = new NotifyService();
        const { controllers, checkIfNewControllerExists, addNewController } = methodsComposition;
        const haveBoundCommand = ref(false);
        const runtimeCustom = ref(props.canAddNewMethod);

        const shouldShowAddNewMethod = computed(() => {
           // return !runtimeCustom.value;
           return false;
        });

        const shouldShowBondEvents = computed(() => {
            return haveBoundCommand.value;
        });

        const actionButtonStyle = computed(() => {
            return {
                'font-size': '15px',
                'margin': '5px',
                'color': '#226eff',
                'flex': '1 1 0'
            };
        });
        const useFormSchema = inject('useFormSchema') as any;
        const modalService = inject(F_MODAL_SERVICE_TOKEN) as any;
        let newFuncModal: any;

        /**
         * 「引入控制器」-方法绑定
         * @param controller 
         * @param code 
         * @param nameSpace 
         */
        function importNewController(controller, code, nameSpace) {
            // 存储所有用户新增的控制器,可能存在控制器更新新命令的情况，所以先添加再去重
            addNewController(controller);
            if (controller.Commands) {
                // 构造内置控制器
                const importData = formCommandService['getInternalControllerFromControllerMetadata'](controller, code, nameSpace);
                // 检测新增的控制器不存在于原internals中
                if (checkIfNewControllerExists(importData)) {
                    notifyService.info({ position: 'top-center', message: '该控制器已存在' });
                }
            }
        }
        /**
         * 选择控制器后事件
         * @param selectedController 控制器元数据
         */
        async function onSubmitController(selectedController: any) {
            formCommandService['getSupportedControllerMetadata'](selectedController).then((result: any) => {
                if (result) {
                    // 引入控制器
                    importNewController(result['controller'], result['code'], result['nameSpace']);
                }
            }).finally(() => {
                if (newFuncModal && newFuncModal.destroy) {
                    newFuncModal.destroy();
                }
            });
        }

        function onCancelController(e) {
            if (newFuncModal && newFuncModal.destroy) {
                newFuncModal.destroy();
            }
        }
        function renderControllerModal() {
            const controllerSelectorParams = {
                formBasicInfo: useFormSchema?.getFormMetadataBasicInfo()
            };
            return <FSchemaSelector
                injectSymbolToken={ControllerSchemaRepositorySymbol}
                view-type="NavList"
                editorParams={controllerSelectorParams}
                showFooter={true}
                onCancel={onCancelController}
                onSubmit={onSubmitController}></FSchemaSelector>;
        }
        function openMetadataSelector() {
            newFuncModal = modalService.open({ title: '选择控制器', width: 950, render: renderControllerModal, showButtons: false });
        }

        function generateNewFunc() {
            context.emit('newFunctionChanged');
        }

        function showBoundEvents() {

        }

        function renderBondEvents() {

        }

        function onClose() {
            context.emit('close', null);
        }

        function renderBreakline() {
            return (
                <div class="f-command-breakline">
                    ------------------------------------------------------------
                </div>
            );
        }

        function renderNewCommand() {
            return (
                <div class="f-page-internals-new-command">
                    <div class="f-page-internals-new-command-func1">
                        <div class="f-icon f-icon-panel-retraction" style={actionButtonStyle.value}></div>
                        <div class="f-function-class" onClick={() => openMetadataSelector()}>引入控制器</div>
                    </div>
                    {shouldShowAddNewMethod.value && <div class="f-page-internals-new-command-func2">
                        <div class="f-icon f-icon-new-edit" style={actionButtonStyle.value}></div>
                        <div class="f-function-class" onClick={() => generateNewFunc()}>添加新方法</div>
                    </div>}
                    {shouldShowBondEvents.value && renderBondEvents()}
                    {/* <div class="f-page-internals-new-command-func3">
                        <div class="f-icon f-icon-licensed" style={actionButtonStyle.value}></div>
                        <div class="f-function-class" onClick={() => showBoundEvents()}>已有方法</div>
                    </div> */}
                </div>
            );
        }

        function renderControllerTitle(controller: Command) {
            return (
                <div class="f-page-internals-controller">
                    <div class="f-page-internals-controller-name" title={controller.controllerName.name}>
                        {controller.controllerName.name}
                    </div>
                </div>
            );
        }

        function renderMethod(methods: CommandItem[]) {
            return methods.map((command: CommandItem) => (
                <div class="f-page-internals-command">
                    <div class="f-page-internals-command-name" title={command.label}>{command.label}</div>
                </div>
            ));
        }

        function onSelectMethodChanged(selectedMethodChangeArgs: MethodChangedEventArgs) {
            context.emit('selectMethod', selectedMethodChangeArgs);
        }

        function renderMethodList() {
            return (
                <div class="f-page-command-source-choice">
                    <CommandList controllers={controllers.value} onSelectMethod={onSelectMethodChanged}></CommandList>
                </div>
            );
        }

        return () => {
            return (
                <div class="f-page-events-editor-command-source">
                    {/* 方法来源对应的方法列表 */}
                    {renderMethodList()}
                    {/* 分割线 */}
                    {renderBreakline()}
                    {/* 可选择功能 */}
                    {renderNewCommand()}
                    {/* 分割线 */}
                    {renderBreakline()}
                    <div>
                        <div class="f-page-internals-close" onClick={onClose}>取消</div>
                    </div>
                </div>
            );
        };
    }
});
