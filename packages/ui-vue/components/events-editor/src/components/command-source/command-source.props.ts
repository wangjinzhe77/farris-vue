import { ExtractPropTypes } from "vue";
import { Command } from "../../types";

export const commandSourceProps = {
    /** 入参 */
    initialData: { Type: Object, default: {} },
    /** 原层级结构数据 */
    originalData: { Type: Object, default: {} },
    /** 判断是否为新引入的方法 */
    newImport: { Type: Boolean, default: false },
    /** 新增的内置构件列表 */
    addInternalCommandList: { Type: Array<any>, default: [] },
    /** 外层传入数据 */
    outerLayerValue: { Type: Number, default: 0 },
    /** 判断是否为：点击“导入此方法”后，绑定了新增值 */
    exit: { Type: Number, default: -1 },
    /** 接收是否显示「可选择方法」功能 */
    haveBoundcommand: { Type: Boolean, default: false }
} as Record<string, any>;

export type CommandSourceProps = ExtractPropTypes<typeof commandSourceProps>;
