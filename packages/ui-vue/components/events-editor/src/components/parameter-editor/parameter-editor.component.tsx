import { SetupContext, computed, defineComponent, reactive, ref } from "vue";
import { ParameterEditorProps, parameterEditorProps } from "./parameter-editor.props";
import ComboList from '@farris/ui-vue/components/combo-list';
import FEventParameter from "@farris/ui-vue/components/event-parameter";
import { CommandItem } from "../../types";
import './parameter-editor.css';
import { FNotifyService as NotifyService } from "@farris/ui-vue/components/notify";
/**
 * 参数编辑器，包含下列内容：
 * 命令名称 编辑按钮【点击编辑按钮，展开参数面板】
 * 参数面板：
 *   参数名称1
 *      输入框编辑器
 *   参数名称2
 *      输入框编辑器
 *   .....
 * 
 */

export default defineComponent({
    name: 'FParameterEditor',
    props: parameterEditorProps,
    emits: ['confirm', 'change', 'targetChange'] as (string[] & ThisType<void>) | undefined,
    setup(props: ParameterEditorProps, context: SetupContext) {
        const notifyService = new NotifyService();
        const command = ref<CommandItem>(props.command);
        const commandItemIndex = ref<number>(0);
        // 方法被废弃
        const showAbandonedIcon = computed(() => command.value.id === 'abandoned');
        // 方法被删除
        const showDeletedIcon = computed(() => command.value.id === 'deleted');

        const shouldShowParametersOfTheCommand = computed(() => command.value !== null);

        const shouldShowEditButton = computed(() => command.value.id !== 'abandoned'&&command.value.id !== 'deleted');

        const shouldShowParameterEditorGroup = ref(false);
        function getDefaultTargetComponentVM(): string {
            const { targetComponent } = command.value;
            let resultComponentVM = '';
            if (command.value.componentLists) {
                command.value.componentLists.forEach(componentListsItem => {
                    if (componentListsItem.componentId === targetComponent) {
                        resultComponentVM = componentListsItem.viewModelId;
                        return;
                    }
                });
            }
            return resultComponentVM;
        }

        // 找到已指定的目标组件的视图Id
        const targetComponentVM = ref(getDefaultTargetComponentVM());
        function showAbandonedMessage() {
            notifyService.info({ position: 'top-center', message: '方法已被移除，请重新绑定' });
        }
        /**
         * 渲染被废弃的图标
         * @returns 
         */
        function renderAbandonedIcon() {
            return (
                <div >
                    <div title="方法已被移除，请重新绑定" class="f-icon f-icon-flowline-warning text-danger text-center "
                        style="font-size: 13px;margin-right:10px;width: 30px;height: 20px;"
                        onClick={() => showAbandonedMessage()}></div>
                </div>
            );
        }

        function showDeletedMessage() {
            notifyService.info({ position: 'top-center', message: '方法已失效，请重新绑定' });
        }
        /**
         * 渲染删除图标
         * @returns 
         */
        function renderDeletedIcon() {
            return <div>
                <div title="'方法已被删除，请绑定其他方法'" class="f-icon f-icon-flowline-warning text-danger text-center "
                    style="font-size: 13px;margin-right:10px;width: 30px;height: 20px;"
                    onClick={() => showDeletedMessage()}></div>
            </div>;
        }

        /**
         * 点击 编辑按钮，显示/隐藏参数列表
         * @param commandItemIndex 
         */
        function hideCurrentParameter(commandItemIndex: number) {
            shouldShowParameterEditorGroup.value = !shouldShowParameterEditorGroup.value;
        }

        function renderEditButton() {
            return <div >
                <div class="f-icon f-icon-edit-button" style="font-size: 13px;margin-right:10px;"
                    onClick={() => hideCurrentParameter(commandItemIndex.value)}></div>
            </div>;
        }

        // const shouldShowParameterEditorGroup = computed(() => {
        //     return currentItem.value.showSwitch[commandItemIndex.value].showSection[2][4];
        // });

        const shouldSelectTargetComponent = computed(() => {
            return command.value.componentLists && command.value.hasPath;
        });

        function getSelectedComponent(selectedItems, value) {
            command.value.targetComponent = selectedItems[0]['componentId'];
            context.emit('targetChange', command.value);
        }

        function renderTargetComponentSelector() {
            const paramDescriptionTooltip = reactive({ content: '将此方法添加到指定的组件', placement: 'top' });
            return <div>
                <div class="f-page-parameter-editor-targetComponent">
                    <div class="f-page-parameter-editor-targetComponent-topic">挂载到目标组件
                        <span class="farris-label-tips ml-2" v-tooltip={paramDescriptionTooltip}>
                            <i class="f-icon f-icon-description-tips"></i>
                        </span>
                    </div>
                    <div class="f-page-parameter-editor-targetComponent-dropdown">
                        <ComboList v-model={targetComponentVM.value}
                            enableClear={false} idField={'viewModelId'} valueField={'viewModelId'} textField={'componentId'} data={command.value.componentLists}
                            editable={false} onChange={getSelectedComponent}>
                        </ComboList>
                    </div>
                </div>
            </div>;
        }
        /**
         * 输入框值变更改变参数
         * @param event 
         * @param propertyItem 
         */
        function onEditorChangeHandler(event, propertyItem) {
            propertyItem['value'] = event;
        }
        function onEditorBlurHandler() {
            context.emit('change', command.value);
        }

        function renderParameterEditor(propertyItem) {
            // todo: ide-parameter-editor
            const { tabData } = propertyItem.context.generalData;
            const data = reactive(propertyItem.context.data);
            const {
                assembleSchemaFieldsByComponent,
                assembleOutline,
                assembleStateVariables
            } = propertyItem.context.generalData;
            return <FEventParameter
                v-model={propertyItem.value}
                data={data}
                fieldData={assembleSchemaFieldsByComponent()}
                formData={assembleOutline()}
                varData={assembleStateVariables()}
                editorType={propertyItem?.origin?.controlSource?.type || 'Default'}
                idField={propertyItem?.origin?.controlSource?.context?.valueField?.value
                    || propertyItem?.origin?.controlSource?.context?.idField?.value || 'id'}
                textField={
                    propertyItem?.origin?.controlSource?.context?.textField?.value || 'label'
                }
                onConfirm={(event) => {
                    onEditorChangeHandler(event, propertyItem);
                    onEditorBlurHandler();
                }}
                onValueChange={(event) => {
                    onEditorChangeHandler(event, propertyItem);
                    onEditorBlurHandler();
                }}
            >
            </FEventParameter>;
            {/* <input title="ide-parameter-editor" class="form-control f-utils-fill text-left" value={propertyItem.value}
                    onChange={(event) => onEditorChangeHandler(event, propertyItem)}
                    onBlur={(event) => onEditorBlurHandler()} 
                    /> */}
        }

        function renderParameterEditors() {
            return command.value.property?.map((propertyItem: any, index: number) => {
                const paramDescriptionTooltip = reactive({ content: propertyItem.description, placement: 'top' });
                return <div key={propertyItem.name + index}>
                    {/* 参数编辑第二行-参数名、说明对应的图标  */}
                    <div class="f-page-parameter-editor-row1">
                        <div class="f-page-parameter-editor-parameter-name">
                            {propertyItem.shownName}
                            {propertyItem.description && <div class="ml-2 farris-label-tips" v-tooltip={paramDescriptionTooltip}>
                                <i class="f-icon f-icon-description-tips"></i>
                            </div>}
                        </div>
                    </div>
                    {/* 参数第三行-参数值输入框 */}
                    <div class="f-page-parameter-editor-row2">
                        {renderParameterEditor(propertyItem)}
                    </div>
                </div>;
            });
        }

        function foldIn(commandItemIndex: number) {
            shouldShowParameterEditorGroup.value = false;
            context.emit('confirm');
        }

        function renderConfirmButton() {
            return <div>
                <div class="f-page-parameter-editor-btn">
                    <div class="btn-finish" onClick={() => foldIn(commandItemIndex.value)}>完成</div>
                </div>
            </div>;
        }

        /**
         * 
         * @returns 
         */
        function renderParameterEditorGroup() {
            return <div class="f-page-parameter-editor-group">
                {shouldSelectTargetComponent.value && renderTargetComponentSelector()}
                {renderParameterEditors()}
            </div>;
        }
        /**
         * 显示方法名称和编辑按钮
         * 显示参数列表
         * 显示确定按钮
         * @returns 
         */
        function showParametersOfCommand() {
            return (
                <div>
                    {/* 参数编辑第一行-方法名称 */}
                    <div class="f-page-parameter-editor-first-row">
                        <span class="f-page-parameter-editor-command-name" title={command.value.label}>{command.value.name}</span>
                        <div class="f-page-parameter-editor-first-row-icon">
                            {showAbandonedIcon.value && renderAbandonedIcon()}
                            {showDeletedIcon.value && renderDeletedIcon()}
                            {shouldShowEditButton.value && renderEditButton()}
                        </div>
                    </div>
                    {shouldShowParameterEditorGroup.value && renderParameterEditorGroup()}
                    {shouldShowParameterEditorGroup.value && renderConfirmButton()}
                </div>
            );
        }

        return () => {
            return (
                <div class="f-page-parameter-editor">
                    {shouldShowParametersOfTheCommand.value && showParametersOfCommand()}
                </div>
            );
        };
    },
});
