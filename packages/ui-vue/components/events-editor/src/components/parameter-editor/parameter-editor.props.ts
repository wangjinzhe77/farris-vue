import { ExtractPropTypes, PropType } from "vue";
import { CommandItem } from "../../types";

export const parameterEditorProps = {
    command: { type: Object as PropType<CommandItem>, default: {} }
} as Record<string, any>;

export type ParameterEditorProps = ExtractPropTypes<typeof parameterEditorProps>;
