import { ExtractPropTypes } from "vue";

export const interactionButtonProps = {} as Record<string, any>;

export type InteractionButtonProps = ExtractPropTypes<typeof interactionButtonProps>;
