import { SetupContext, defineComponent, inject, ref } from "vue";
import { InteractionButtonProps, interactionButtonProps } from "./interaction-button.props";

import FEventList from '../event-list/event-list.component';
import { EventItem } from "../../types";
import { UseEvents, UseInteraction } from "../../composition/types";
import { FNotifyService as NotifyService } from "@farris/ui-vue/components/notify";
/**
 * 【新建交互事件】按钮
 */
export default defineComponent({
    name: 'FInteractionButton',
    props: interactionButtonProps,
    emits: ['click'] as (string[] & ThisType<void>) | undefined,
    setup(props: InteractionButtonProps, context: SetupContext) {
        const notifyService = new NotifyService();
        const eventsComposition = inject<UseEvents>('useEventsComposition') as UseEvents;
        const interactionComposition = inject<UseInteraction>('useInteractionCompostion') as UseInteraction;
        const { candidateEvents } = eventsComposition;
        const { collapseInteractions } = interactionComposition;
        const shouldShowCandidateEventsList = ref(false);

        function excludeFromCandidates(selectedEvent: EventItem) {
            candidateEvents.value = candidateEvents.value.filter((eventItem: EventItem) => eventItem.label !== selectedEvent.label);
        }
        function toggleEventList(state = false) {
            shouldShowCandidateEventsList.value = state;
        }
        function onClickEventItem(eventItem: EventItem) {
            toggleEventList();
            excludeFromCandidates(eventItem);
            context.emit('click', eventItem);
        }
        function onClickActionButton() {
            if (candidateEvents.value.length === 0) {
                notifyService.info({ position: 'top-center', message: '事件已全部绑定' });
                return;
            }
            // 1. 收折其他已展示的事件列表
            collapseInteractions();
            // 2. 显示/隐藏候选事件事件列表
            toggleEventList(!shouldShowCandidateEventsList.value);
        }

        return () => {
            return (
                <>
                    <div class="f-page-events-editor-top-button">
                        <button class="f-page-events-editor-button" onClick={onClickActionButton}>
                            <span class="f-page-events-editor-text">新建交互事件</span>
                        </button>
                    </div>
                    {shouldShowCandidateEventsList.value &&
                        <FEventList events={candidateEvents.value} onClick={onClickEventItem}></FEventList>
                    }
                </>
            );
        };

    }
});
