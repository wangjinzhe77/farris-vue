import { SetupContext, defineComponent, ref } from "vue";
import { CommandListProps, commandListProps } from "./command-list.props";

import './command-list.css';
import { Command, CommandItem, MethodChangedEventArgs } from "../../types";

export default defineComponent({
    name: 'FCommandList',
    props: commandListProps,
    emits: ['selectMethod'] as (string[] & ThisType<void>) | undefined,
    setup(props: CommandListProps, context: SetupContext) {
    /**
     * 此处原来是originalData[i].internalCommandList
     */
        const innternalCommandList = ref<Command[]>(props.controllers);

        function shouldShowControllerTitle(item: any) {
            return item.controllerList.length !== 0;
        }

        function renderControllerTitle(item: any) {
            return shouldShowControllerTitle(item) &&
                <div class="f-page-internals-controller">
                    <div class="f-page-internals-controller-name" title={item.controllerName.name}>{item.controllerName.name}
                    </div>
                </div>;
        }

        function selectCommandChanged(method: CommandItem, controller: Command) {
            context.emit('selectMethod', { method, controller } as MethodChangedEventArgs);
        }

        function renderMethods(controller: Command) {
            return controller.controllerList.map((method: CommandItem) => {
                return (
                    <div class="f-page-internals-command" onClick={() => selectCommandChanged(method, controller)}>
                        <div class="f-page-internals-command-name" title={method.label}>{method.name}</div>
                    </div>
                );
            });
        }

        function renderController(controller: Command) {
            return <div>
                {renderControllerTitle(controller)}
                {renderMethods(controller)}
            </div>;

        }

        return () => {
            return (
                <div class="f-page-internals">
                    {innternalCommandList.value.map((controller: Command) => {
                        return renderController(controller);
                    })}
                </div>
            );
        };
    },
});
