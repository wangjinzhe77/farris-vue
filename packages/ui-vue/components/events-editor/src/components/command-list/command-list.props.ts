import { ExtractPropTypes } from 'vue';
import { Command } from '../../types';

export const commandListProps = {
    controllers: { type: Array<Command>, default: [] },
} as Record<string, any>;

export type CommandListProps = ExtractPropTypes<typeof commandListProps>;
