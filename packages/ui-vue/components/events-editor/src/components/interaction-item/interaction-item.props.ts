import { ExtractPropTypes, PropType } from "vue";
import { Command, InteractionItem } from "../../types";

export const interactionItemProps = {
    collapsed: { type: Boolean, default: true },
    displayOrder: { type: Number, default: -1 },
    interaction: { type: Object as PropType<InteractionItem>, default: {} },
    controllers: { type: Array<Command>, default: [] }
} as Record<string, any>;

export type InteractionItemProps = ExtractPropTypes<typeof interactionItemProps>;
