import { SetupContext, computed, defineComponent, inject, ref } from "vue";
import { InteractionItemProps, interactionItemProps } from "./interaction-item.props";
import { Command, CommandItem, InteractionItem, MethodChangedEventArgs } from "../../types";

import FCommandSource from '../command-source/command-source.component';
import FParameterEditor from '../parameter-editor/parameter-editor.component';
import { UseMethods } from "../../composition/types";
import { cloneDeep } from "lodash-es";
import { useGuid } from "@farris/ui-vue/components/common";
/**
 * 事件交互项
 *
 * 事件名  添加 删除  展开|收起按钮=》展开命令面板
 * 
 * 展开命令面板：
 * 
 *  命令名 编辑按钮=》展开参数项
 *   参数项1
 *   参数项2
 * 
 *  命令名 编辑按钮=》展开参数项
 *   参数项1
 *   参数项2 
 * 
 */
export default defineComponent({
    name: 'FInteractionItem',
    props: interactionItemProps,
    emits: ['addAction', 'delete', 'toggle'] as (string[] & ThisType<void>) | undefined,
    setup(props: InteractionItemProps, context: SetupContext) {
        const { guid } = useGuid();
        const methodsComposition = inject<UseMethods>('useMethodsComposition') as UseMethods;
        const { toggleSelectingMethod, emitFinalState, getViewModeId, getMethodsOnViewModel, getComponentList, updateTarget,updateCommandHasPath } = methodsComposition;
        const controllers = ref<Command[]>(props.controllers);
        const interaction = ref<InteractionItem>(props.interaction);
        const interactionIndex = ref<number>(props.displayOrder);
        const shouldShowCommandSource = computed(() => {
            const switchOption = interaction.value.showSwitch[0];
            // 获取是否显示方法列表
            const toSelectMethod = switchOption.showSection[switchOption.showSwitchNumber][2];
            return toSelectMethod;
        });
        const shouldShowMethod = computed(() => {
            const hasCommand = !!interaction.value.command;
            const switchOption = interaction.value.showSwitch[0];
            // 获取是否显示方法名
            const toShowMethod = switchOption.showSection[switchOption.showSwitchNumber][3];
            return hasCommand && toShowMethod;
        });

        const toggleButtonIconClass = computed(() => {
            const classObject = {
                'f-icon': true,
                'f-icon-arrow-chevron-down': shouldShowMethod.value,
                'f-icon-arrow-chevron-left': !shouldShowMethod.value
            };
            return classObject;
        });

        function onClickDeleteButton() {
            context.emit('delete', interactionIndex.value);
        }

        function onClickToggleButton() {
            const switchOption = interaction.value.showSwitch[0];
            //  切换方法名显示状态
            switchOption.showSection[switchOption.showSwitchNumber][3] =
                !switchOption.showSection[switchOption.showSwitchNumber][3];
            context.emit('toggle', interactionIndex.value);
        }

        function onClickAddActionButton() {
            const hasBoundCommand = interaction.value.command !== null;
            if (hasBoundCommand) {
                const switchOption = interaction.value.showSwitch[0];
                toggleSelectingMethod(switchOption);
                context.emit('addAction', interactionIndex.value);
            }
        }

        function onCloseMethodList(command: CommandItem) {
            if (command === null && interaction.value.command === null) {
                context.emit('delete', interactionIndex.value);
            }
            const switchOption = interaction.value.showSwitch[0];
            toggleSelectingMethod(switchOption);
        }
        /**
         * 生成不重复的方法名
         * @param suffix 
         * @param viewModelId 
         * @param code 
         * @returns 
         */
        function generateFunctionName(suffix, viewModelId, code): { newName, suffix } {
            let newName = `${viewModelId}${code}${suffix}`;
            let repitition = -1;
            const viewMedelMethods = getMethodsOnViewModel();
            // 循环直到生成不重复的方法名
            while (repitition === -1) {
                let commandNameExist = false;
                for (let i = 0; i < viewMedelMethods.length; i++) {
                    viewMedelMethods[i].controllerList.forEach(commandItem => {
                        if (commandItem.label || commandItem.label === '') {
                            commandNameExist = commandItem.label.toLowerCase() === newName.toLowerCase() ? true : commandNameExist;
                        }
                        else {
                            commandNameExist = false;
                        }
                    });
                }

                if (commandNameExist) {
                    suffix += 1;
                    newName = `${viewModelId}${code}${suffix}`;
                }
                else {
                    repitition = 0;
                    break;
                }
            }
            return { newName, suffix };
        }

        function onSelectedMethodChanged(selectedMethodChangeArgs: MethodChangedEventArgs) {
            const { method, controller } = selectedMethodChangeArgs;
            const { label, handlerName, property, componentLists, hasPath } = method;
            const viewModelId = getViewModeId().replace(/-/g, '').replace('component', '').replace('viewmodel', '');
            const code = label.slice(0, 1).toUpperCase() + label.slice(1);
            const { newName, suffix } = generateFunctionName(1, viewModelId, code);

            const { controllerName } = controller;
            const cmpId = controllerName.id;
            const newMethod={ id: guid(), label: newName, name: `${method.name}${suffix}`, handlerName, hasPath, cmpId, property, componentLists };
            interaction.value.command = updateCommandHasPath(controller,newMethod);
            interaction.value.controller = {
                id: controllerName.id,
                label: controllerName.label,
                name: controllerName.name
            };

            const switchOption = interaction.value.showSwitch[0];
            // 1. 切换至已绑定方法状态
            switchOption.showSwitchNumber = 2;
            // 2. 隐藏方法列表
            switchOption.showSection[switchOption.showSwitchNumber][2] = false;
            // 3. 显示方法名
            switchOption.showSection[switchOption.showSwitchNumber][3] = true;
            emitFinalState(false, null);
        }
        /**
         * 添加新方法
         */
        function onNewFunctionChangedHandler() {
            interaction.value.command = {
                id: 'abandoned',
                label: '未绑定方法',
                name: '未绑定方法',
                property: [],
                hasPath: false,
                handlerName: '未绑定方法',
                cmpId: '',
                shortcut: {},
                isRTCmd: undefined,
                isNewGenerated: undefined,
                extensions: [],
                isInvalid: false
            };
            interaction.value.controller = {
                label: '',
                name: '',
                id: '',
            };
            emitFinalState(true, interaction.value.event);
            const switchOption = interaction.value.showSwitch[0];
            toggleSelectingMethod(switchOption);
        }
        function renderMethodSource() {
            return <FCommandSource
                controllers={controllers.value}
                onClose={onCloseMethodList}
                onSelectMethod={onSelectedMethodChanged}
                onNewFunctionChanged={() => onNewFunctionChangedHandler()}
            ></FCommandSource>;
        }
        /**
         * 参数编辑器点击完成触发
         */
        function onParameterEditorConfirmHandler() {
            const switchOption = interaction.value.showSwitch[0];
            switchOption.showSection[switchOption.showSwitchNumber][3] = false;
        }
        /**
         * 参数编辑器变更触发
         * @param event 
         */
        function onParameterEditorChangeHandler(event) {
            interaction.value.command = cloneDeep(event);
            emitFinalState(false, null);
        }
        /**
         * 目标组件变更触发
         * @param event 
         */
        function onParameterEditorTargetChangeHandler(changedCommand) {
            changedCommand.componentLists = getComponentList();
            updateTarget(changedCommand);
            interaction.value.command = cloneDeep(changedCommand);
            emitFinalState(false, null);
        }
        function renderMethodInfo() {
            return <FParameterEditor
                command={interaction.value.command}
                onConfirm={() => onParameterEditorConfirmHandler()}
                onChange={(event) => onParameterEditorChangeHandler(event)}
                onTargetChange={(event) => onParameterEditorTargetChangeHandler(event)}
            ></FParameterEditor>;
        }

        return () => {
            return (
                <div class="show-switch">
                    <div class="f-page-events-editor-bound-event">
                        <div class="f-page-events-editor-bound-event-name" title={interaction.value.event.label}>
                            {interaction.value.event.name}
                        </div>
                        <div style="float:right;margin:5px;">
                            <div class={toggleButtonIconClass.value} style="font-size: 10px;" onClick={onClickToggleButton}></div>
                        </div>
                        <div class="f-icon f-icon-yxs_delete" style="font-size: 13px;float: right; margin:8px;"
                            onClick={onClickDeleteButton}></div>
                        <div class="f-icon f-icon-home-add" style="float:right; font-size: 10px; margin:8px;"
                            onClick={onClickAddActionButton}></div>
                    </div>
                    {shouldShowCommandSource.value && renderMethodSource()}
                    {shouldShowMethod.value && renderMethodInfo()}
                </div>

            );
        };
    }
});
