 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext, computed, ref, watch, onUnmounted, onBeforeUnmount } from 'vue';
import { ConditionProps, conditionProps } from './condition.props';
import { Condition } from './types';
import { FDynamicFormGroup } from '@farris/ui-vue/components/dynamic-form';
import { useFieldConfig } from './composition/use-field-config';
import { useSize } from './composition/use-size';
import { ConditionValue } from './composition/condition-value/types';

export default defineComponent({
    name: 'FConditionFields',
    props: conditionProps,
    emits: ['valueChange', 'blur', 'focus', 'click', 'input'] as (string[] & ThisType<void>) | undefined,
    setup(props: ConditionProps, context: SetupContext) {
        const fieldsElement = ref<any>();
        const key = ref(props.key);
        const isControlInline = ref(props.isControlInline);
        const conditions = ref(props.conditions);
        const useFieldComposition = useFieldConfig(props, context);
        const { initialConditionValue, fieldMap, loadFieldConfigs } = useFieldComposition;
        const useSizeComposition = useSize(props, context, fieldsElement);
        const { conditionClass,  resizeObserver } = useSizeComposition;

        loadFieldConfigs(true);

        initialConditionValue(conditions.value);
        onBeforeUnmount(() => {
            resizeObserver.value?.unobserve(fieldsElement.value);
        });

        watch(
            () => props.conditions,
            () => {
                conditions.value = props.conditions;
                initialConditionValue(conditions.value);
            }
        );

        const queryConditionClass = computed(() => ({
            // row: true,
            'f-utils-flex-row-wrap': true,
            'farris-form': true,
            'farris-form-controls-inline': props.isControlInline
        }));

        const conditionItemClass = computed(() => {
            // const classArray = ['col-12', 'col-md-6', 'col-xl-3'];
            // if (conditions.value.length > 4) {
            //     classArray.push('col-el-2');
            // }
            // return classArray.join(' ');
            return 'col-12 col-md-6 col-xl-3 col-el-2 ';
        });

        function onChange(e:any, condition:Condition) {
            context.emit('valueChange', e, condition);
        }
        
        function renderFieldConditions() {
            return conditions.value.map((condition: Condition) => {
                const editor = fieldMap.get(condition.fieldCode)?.editor;
                let needEmitChange = true;
                if(condition.value?.editorType === 'lookup' && editor) {
                    editor.idValue = condition.value.idValue;
                    editor['onClear'] = () => {
                        condition.value.idValue = '';
                        condition.value.mapFields = [];
                        onChange('', condition);
                    };
                    editor['onUpdate:dataMapping'] = (mapFields) => {
                        condition.value.mapFields = mapFields.items;
                        onChange(condition.value.getValue(), condition);
                    };
                    needEmitChange = false;
                } else if(condition.value?.editorType === 'number-range' && editor) {
                    editor.beginValue = condition.value.begin;
                    editor['onBeginValueChange'] = (value) => {
                        condition.value.begin = value;
                        onChange(value, condition);
                    };
                    editor.endValue = condition.value.end;
                    editor['onEndValueChange'] = (value) => {
                        condition.value.end = value;
                        onChange(value, condition);
                    };
                    needEmitChange = false;
                };
                return <FDynamicFormGroup
                    customClass={editor?.appearance?.class || conditionItemClass.value}
                    label={condition.fieldName}
                    editor={editor}
                    required={editor?.required}
                    v-model={(condition.value as ConditionValue).value}
                    onChange={(e:any) => {needEmitChange && onChange(e, condition);}}></FDynamicFormGroup>;
            });
        }

        return () => (
            <div class={queryConditionClass.value} key={key.value} ref={fieldsElement} v-area-response>
                {renderFieldConditions()}
            </div>
        );
    }
});
