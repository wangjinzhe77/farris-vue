 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 import { defineComponent, SetupContext, computed, ref, watch, onUnmounted, onBeforeUnmount } from 'vue';
 import { ConditionProps, conditionProps } from './condition.props';
 import { Condition } from './types';
 import FFormGroupDesign from '../../dynamic-form/src/designer/form-group.design.component';
 import { useFieldConfig } from './composition/use-field-config';
 import { useSize } from './composition/use-size';
 import areaResponseDirective from '../../common/directive/area-response';
 
 export default defineComponent({
     name: 'FConditionFieldsDesign',
     directives: {
        "area-response":areaResponseDirective,
      },
     props: conditionProps,
     emits: ['valueChange', 'blur', 'focus', 'click', 'input'] as (string[] & ThisType<void>) | undefined,
     setup(props: ConditionProps, context: SetupContext) {
         const fieldsElement = ref<any>();
         const key = ref(props.key);
         const isControlInline = ref(props.isControlInline);
         const conditions = ref(props.conditions);
         const useFieldComposition = useFieldConfig(props, context);
         const { initialConditionValue, fieldMap, loadFieldConfigs } = useFieldComposition;
         const useSizeComposition = useSize(props, context, fieldsElement);
         const { conditionClass,  resizeObserver } = useSizeComposition;
 
         loadFieldConfigs(true);
         onBeforeUnmount(() => {
             resizeObserver.value?.unobserve(fieldsElement.value);
         });
 
         watch(
             () => props.conditions,
             () => {
                 conditions.value = props.conditions;
             }
         );
 
         const queryConditionClass = computed(() => ({
             // row: true,
             'f-utils-flex-row-wrap': true,
             'farris-form': true,
             'farris-form-controls-inline': props.isControlInline
         }));
 
         const conditionItemClass = computed(() => {
             return 'col-12 col-md-6 col-xl-3 col-el-2 ';
         });
         
         function renderFieldConditions() {
             return conditions.value.map((condition: Condition) => {
                const editor = fieldMap.get(condition.fieldCode)?.editor;
                editor && (editor.disabled = true);
                return <FFormGroupDesign
                customClass={editor?.appearance?.class || conditionItemClass.value}
                label={condition.fieldName}
                required={editor?.required}
                editor={editor}
                type="form-group"></FFormGroupDesign>;
             });
         }
 
         return () => (
             <div class={queryConditionClass.value} key={key.value} ref={fieldsElement} v-area-response>
                 {renderFieldConditions()}
             </div>
         );
     }
 });
