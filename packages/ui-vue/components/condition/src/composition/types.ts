import { ComputedRef, Ref } from "vue";
import { ConditionGroup, FieldConfig, Condition } from "../types";
import { EditorType } from "../../../dynamic-form";
import { ConditionValue } from "./condition-value/types";

export interface UseFieldConfig {

    convertToSingleControl(configs: FieldConfig[]): FieldConfig[];

    fields: Ref<FieldConfig[]>;

    fieldMap: Map<string, FieldConfig>;

    fieldConditions: Ref<FieldConfig[]>;

    loadFieldConfigs: (useRangeEditor: boolean) => void;

    initialConditionValue: (conditions: Condition[]) => Condition[];
}

export interface UseCondition {

    addCondition(): void;

    changeGroupRelation(targetGroup: ConditionGroup): void;

    conditionGroupMap: Map<number, ConditionGroup>;

    getConditions: (conditionGroup: ConditionGroup) => Condition[];

    group(targetsToGroup: (Condition | ConditionGroup)[]): void;

    groupParentMap: Map<number, ConditionGroup>;

    insertConditionTo(preCondtion: Condition): void;

    loadConditionGroup(conditions: Condition[]): ConditionGroup;

    refresh(): void;

    removeCondition(targetToRemove: Condition): void;

    rootGroup: Ref<ConditionGroup>;

    unGroup(groupToDisperse: ConditionGroup): void;
}

export interface UseConditionValue {

    createConditionValue: (editorType: EditorType, initialValue?: any) => ConditionValue;

}

export interface UseSelection {

    clear(): void;

    selectedItems: Ref<Set<number>>;

    toggleSelect(condition: Condition): void;

    getSelectedGroupItem(): (Condition | ConditionGroup)[];
}

export interface UseCompare {

    getCompareOperators(condition: Condition): { name: string; value: string }[];

}
export interface UseSizeValue {

    conditionClass: ComputedRef<string>,
    resizeObserver: Ref<ResizeObserver | null>
}
