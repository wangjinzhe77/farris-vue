import { EditorType } from '../../../../dynamic-form';
import { ConditionValue } from "./types";

export class TextValue implements ConditionValue {

    editorType: EditorType = 'text';

    value: string | undefined;

    valueType = 'text';

    constructor(initialData: { value: string } = { value: '' }) {
        this.value = initialData.value;
    }

    clear(): void {
        this.value = undefined;
    }

    getValue() {
        return this.value;
    }

    getDisplayText() {
        return this.getValue();
    }

    setValue(value: any): void {
        this.value = value;
    }

    isEmpty(): boolean {
        return !this.value;
    }
}
