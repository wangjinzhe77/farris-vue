import { EditorType } from "@farris/ui-vue/components/dynamic-form";
import { ConditionValue } from "./types";

export class CheckBoxValue implements ConditionValue {

    editorType: EditorType = 'check-box';

    value: boolean[];

    valueType = 'boolean';

    constructor(initialData: { value: any } = { value: [] }) {
        const originalBooleanValues = Array.isArray(initialData.value) ? initialData.value : (
            typeof initialData.value === 'string' ? initialData.value.split(',') : []
        );
        const booleanValues = originalBooleanValues.map<boolean>((originalValue: any) => JSON.parse(originalValue));
        this.value = booleanValues;
    }

    clear(): void {
        this.value = [];
    }

    setValue(value: boolean[]) {
        this.value = value;
    }

    getValue() {
        return this.value;
    }

    getDisplayText() {
        return this.value.map((booleanValue: boolean) => booleanValue ? '是' : '否').join(',');
    }

    isEmpty(): boolean {
        return this.value.length === 0;
    }
}
