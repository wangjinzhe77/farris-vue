import { EditorType } from '../../../../dynamic-form';
import { ConditionValue } from "./types";

export class MonthPickerValue implements ConditionValue {

    editorType: EditorType = 'month-picker';

    value: string | undefined;

    valueType = 'datetime';

    constructor(initialData: { value: string } = { value: '' }) {
        this.value = initialData.value;
    }

    setValue(target: { formatted: string | undefined }): void {
        this.value = target.formatted;
    }

    getValue() {
        return this.value;
    }

    getDisplayText() {
        return this.getValue();
    }

    clear(): void {
        this.value = undefined;
    }

    isEmpty(): boolean {
        return !this.value;
    }

}
