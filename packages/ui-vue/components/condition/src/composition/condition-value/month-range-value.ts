import { EditorType } from '../../../../dynamic-form';
import { ConditionValue } from "./types";

export class MonthRangeValue implements ConditionValue {

    editorType: EditorType = 'month-range';

    begin: string;

    end: string;

    valueType = 'datetime';

    clear(): void {
        this.begin = '';
        this.end = '';
    }

    constructor(initialData: { begin: string; end: string } = { begin: '', end: '' }) {
        this.begin = initialData.begin;
        this.end = initialData.end;
    }

    getValue() {
        if (!this.begin || !this.end) {
            return '';
        }
        return `${this.begin}~${this.end}`;
    }

    getDisplayText() {
        return this.getValue();
    }

    setValue(value: { dataRange: string; delimiter: string }): void {
        if (value.dataRange) {
            this.begin = value.dataRange.split(value.delimiter)[0];
            this.end = value.dataRange.split(value.delimiter)[1];
        } else {
            this.clear();
        }
    }

    isEmpty(): boolean {
        return !this.begin || !this.end;
    }
}
