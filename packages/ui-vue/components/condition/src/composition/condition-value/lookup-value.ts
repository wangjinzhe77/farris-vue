import { EditorType } from '../../../../dynamic-form';
import { ConditionValue } from "./types";

export class LookupValue implements ConditionValue {

    editorType: EditorType = 'lookup';

    idValue: string;

    mapFields: any[];

    value: string;

    valueField: string;

    valueType = 'text';

    // 帮助的值是否为手动输入的任意值，对应帮助的任意输入属性nosearch
    isInputText: boolean;

    constructor(initialData: {
        mapFields: any[];
        value: any;
        valueField: string;
        isInputText: boolean;
    } = { mapFields: [], value: '', valueField: '', isInputText: false }) {
        this.value = initialData.value;
        this.valueField = initialData.valueField;
        this.mapFields = initialData.mapFields;
        this.idValue = initialData.mapFields.map(field => field.id).join(',');
        this.isInputText = initialData.isInputText;
    }

    clear(): void {
        this.value = '';
        this.idValue = '';
        this.mapFields = [];
    }

    getValue(): string {
        return this.mapFields.map(field => field[this.valueField || 'id']).join(',');
    }

    getDisplayText() {
        return this.getValue();
    }

    setValue(value: any): void {
        throw new Error("Method not implemented.");
    }

    isEmpty(): boolean {
        return !this.mapFields.length;
    }
}
