import { EditorType } from '@farris/ui-vue/components/dynamic-form';
import { ConditionValue } from "./types";

type propsDateValue = {
    count: null | number;
    dateType: string;
    isFirstDay: boolean;
    isLastDay: boolean;
    name: string;
    period: string;
    text: string;
};

export class DatePickerValue implements ConditionValue {

    editorType: EditorType = 'date-picker';

    value: string | propsDateValue | undefined;

    valueType = 'datetime';

    constructor(initialData: { value: string | propsDateValue } = { value: '' }) {
        this.value = initialData.value;
    }

    setValue(target: { formatted: string | propsDateValue }): void {
        this.value = target.formatted;
    }

    getValue() {
        return this.value;
    }

    getDisplayText() {
        return this.getValue();
    }
    
    isEmpty(): boolean {
        return !this.value;
    }

    clear(): void {
        this.value = undefined;
    }

}
