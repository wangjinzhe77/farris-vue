import { EditorType } from '@farris/ui-vue/components/dynamic-form';
import { ConditionValue } from "./types";

function getEnumValues(data: { value: string; displayText: string }) {
    const originalEnumValues = Array.isArray(data.value) ? data.value : (
        typeof data.value === 'string' ? data.value.split(',') : []
    );
    const originalEnumTexts = Array.isArray(data.displayText) ? data.displayText : (
        typeof data.displayText === 'string' ? data.displayText.split(',') : []
    );
    if (originalEnumValues.length !== originalEnumTexts.length) {
        throw new Error('initialize combo-list value error, the enum value is not matched the enum display text');
    }
    const enumValues = originalEnumValues.map<{ value: string; name: string }>((originalEnumValue: string, index: number) => {
        const value = originalEnumValue;
        const name = originalEnumTexts[index];
        return { value, name };
    });
    return enumValues;
}

export class ComboListValue implements ConditionValue {

    editorType: EditorType = 'combo-list';

    value: any;// 旧结构{Type: '', Content: {value: string, name: string}} 新结构Array[{value: string, name: string}]

    valueType = 'enum';

    // displayText: string;// 旧结构无，新结构为选中的value值，多选是以,分割的字符串

    constructor(initialData: { value: any } = { value: '' }) {
        // this.value = getEnumValues(initialData);
        this.value = initialData.value;
        // this.displayText = initialData.displayText;
    }

    clear(): void {
        this.value = [];
        // this.displayText = '';
    }

    getValue() {
        return this.value;
        // return this.value.map((enumValue: { value: string; name: string }) => enumValue.value).join(',');
    }

    getDisplayText() {
        return this.value.map((enumValue: { value: string; name: string }) => enumValue.name).join(',');
    }

    // setValue(data: { value: string; displayText: string }) {
    //     // this.displayText = data.dispalyText;
    //     const enumValues = getEnumValues(data);
    //     this.value = enumValues;
    //     return this.displayText;
    // }

    setValue(data: { value: string; displayText: string }) {
        // this.displayText = data.displayText;
        this.value = data.value;
    }

    isEmpty(): boolean {
        return !this.value && this.value !== false;
    }
}
