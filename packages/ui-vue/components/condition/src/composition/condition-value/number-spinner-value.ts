import { EditorType } from '../../../../dynamic-form';
import { ConditionValue } from "./types";

export class NumberSpinnerValue implements ConditionValue {

    editorType: EditorType = 'number-spinner';

    value: number | null | undefined;

    valueType = 'number';

    constructor(initialData: { value: string } = { value: '' }) {
        this.value = initialData.value == null ? null : parseFloat(initialData.value);
    }

    clear(): void {
        this.value = null;
    }

    getValue() {
        return this.value;
    }

    getDisplayText() {
        return this.getValue();
    }

    setValue(value: any): void {
        // throw new Error("Method not implemented.");
        this.value = isNaN(parseFloat(value)) ? null : value;
    }

    isEmpty(): boolean {
        return this.value == null || isNaN(this.value);
    }
}
