import { EditorType } from '../../../../dynamic-form';
import { CheckBoxValue } from './checkbox-value';
import { ComboLookupValue } from './combo-lookup-value';
import { DatePickerValue } from './date-picker-value';
import { DateRangeValue } from './date-range-value';
import { DateTimePickerValue } from './datetime-picker-value';
import { ComboListValue } from './dropdown-value';
import { InputGroupValue } from './input-group-value';
import { LookupValue } from './lookup-value';
import { MonthPickerValue } from './month-picker-value';
import { MonthRangeValue } from './month-range-value';
import { NumberRangeValue } from './number-range-value';
import { NumberSpinnerValue } from './number-spinner-value';
import { RadioGroupValue } from './radio-group-value';
import { YearPickerValue } from './single-year-value';
import { TextValue } from './text-value';

export {
    CheckBoxValue, ComboLookupValue, ComboListValue, DatePickerValue, DateRangeValue, DateTimePickerValue,
    InputGroupValue, LookupValue, MonthPickerValue, MonthRangeValue, NumberRangeValue, NumberSpinnerValue,
    RadioGroupValue, YearPickerValue, TextValue
};
export interface ConditionValue {

    clear(): void;

    editorType: EditorType;

    value?: any;

    idValue?: any;

    valueField?: string;

    mapFields?: any[];

    begin?: any;

    end?: any;

    valueType: string;

    enumValueType?: string;

    getValue(): any;

    getDisplayText(): any;

    setValue(value: any): void;

    isEmpty(): boolean;
}
