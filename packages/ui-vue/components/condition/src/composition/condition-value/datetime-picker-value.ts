import { EditorType } from '../../../../dynamic-form';
import { ConditionValue } from "./types";

export class DateTimePickerValue implements ConditionValue {

    editorType: EditorType = 'datetime-picker';

    value: string | undefined;

    valueType = 'datetime';

    constructor(initialData: { value: string } = { value: '' }) {
        this.value = initialData.value;
    }

    clear(): void {
        this.value = undefined;
    }

    getValue() {
        return this.value;
    }

    getDisplayText() {
        return this.getValue();
    }

    setValue(value: any): void {
        this.value = value.formatted;
    }

    isEmpty(): boolean {
        return !this.value;
    }

}
