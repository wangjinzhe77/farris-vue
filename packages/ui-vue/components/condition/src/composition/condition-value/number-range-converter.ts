import { NumberRangeValue } from "./number-range-value";
import { ConditionValue } from "./types";

export class NumberRangeValueConvertor {

    convertToObject(conditionValue: ConditionValue): object {
        const numberRangeValue = conditionValue as NumberRangeValue;
        const { begin, end } = numberRangeValue;
        return { begin, end };
    }

    convertFromObject(valueObject: any): NumberRangeValue {
        const begin = valueObject.begin != null ? parseFloat(valueObject.begin) : null;
        const end = valueObject.end != null ? parseFloat(valueObject.end) : null;
        return new NumberRangeValue({ begin, end });
    }
}
