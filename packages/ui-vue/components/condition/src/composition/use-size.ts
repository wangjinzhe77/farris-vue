import { SetupContext, ref, Ref, watch, computed } from "vue";
import { ConditionProps } from "../condition.props";
import { FieldConfig, Condition } from "../types";
import { UseSizeValue } from "./types";


export function useSize(props: ConditionProps, context: SetupContext, fieldsElement: Ref<any>): UseSizeValue {


    const resizeObserver = ref<ResizeObserver | null>(null);
    const element = ref(fieldsElement);
    const elementWidth = ref(0);

    // eslint-disable-next-line @typescript-eslint/no-unsafe-function-type
    function throttle(fn: Function, delay: number) {
        let timer: NodeJS.Timeout | null = null;

        return function () {
            // eslint-disable-next-line prefer-rest-params
            const args = arguments;
            if (!timer) {
                fn(...args);
                timer = setTimeout(() => {
                    timer = null;                   
                }, 200);
            } else {
                clearTimeout(timer);
                timer = setTimeout(() => {
                    timer = null;
                    fn(...args);
                }, 200);
            }
        };
    }

    function addSizeListener() {
        if (!element.value) {
            return;
        }
        resizeObserver.value = new ResizeObserver(throttle((entries: Array<ResizeObserverEntry>) => {
            const modifyElement = entries[0];
            elementWidth.value = modifyElement.contentRect.width;
        }, 500));
        resizeObserver.value.observe(element.value);
    }

    function getDefaultClass(width: number) {
        const baseWidth = 250;
        let conditionClass = 'col-12';
        if (width > baseWidth * 6) {
            conditionClass = 'col-2';
        } else if (width > baseWidth * 4) {
            conditionClass = 'col-3';
        } else if (width > baseWidth * 3) {
            conditionClass = 'col-4';
        } else if (width > baseWidth * 2) {
            conditionClass = 'col-6';
        }
        return conditionClass;

    }

    const conditionClass = computed(() => {
        return getDefaultClass(elementWidth.value);
    });

    watch([fieldsElement], ([newFieldsElement]) => {

        element.value = newFieldsElement;
        addSizeListener();
    });

    return {
        conditionClass,
        resizeObserver
    };

}
