import { SetupContext, defineComponent, ref } from 'vue';
import { ConditionProps, conditionProps } from './condition.props';
import { ConditionGroup, Condition } from './types';
import { FDynamicFormGroup } from '../../dynamic-form';
import FComboList from '@farris/ui-vue/components/combo-list';

import { useFieldConfig } from './composition/use-field-config';
import { useCondition } from './composition/use-condition';
import { useSelection } from './composition/use-selection';
import { useCompare } from './composition/use-compare';

import { useConditionValue } from './composition/use-condition-value';
import { ConditionValue } from './composition/condition-value/types';

export default defineComponent({
    name: 'FConditionList',
    props: conditionProps,
    emits: ['valueChange', 'labelCodeChange', 'compareTypeChange'] as (string[] & ThisType<void>) | undefined,
    setup(props: ConditionProps, context: SetupContext) {
        const key = ref(props.key);
        const queryConditions = ref(props.conditions);
        const useFieldComposition = useFieldConfig(props, context);
        const { convertToSingleControl, fields, fieldMap, loadFieldConfigs } = useFieldComposition;
        const useConditionComposition = useCondition();
        const { addCondition, changeGroupRelation, getConditions, group, insertConditionTo,
            loadConditionGroup, refresh, removeCondition, rootGroup, unGroup } = useConditionComposition;
        const { createConditionValue } = useConditionValue();
        const useSelectionComposition = useSelection(props, context, useConditionComposition);
        const { selectedItems, toggleSelect } = useSelectionComposition;
        const { getCompareOperators } = useCompare(props, context, useFieldComposition);

        loadFieldConfigs(false);

        fields.value = convertToSingleControl(fields.value);

        loadConditionGroup(queryConditions.value);

        function onFieldChanged(condition: Condition) {
            const editor = fieldMap.get(condition.fieldCode)?.editor || { type: 'text' };
            condition.value = createConditionValue(editor.type, undefined);
            context.emit('labelCodeChange', condition);
        }

        function onCompareChanged(e: any, condition: Condition) {
            context.emit('compareTypeChange', condition);
        }

        function addItem() { }

        function reset() { }

        context.expose({ getConditions });

        function onChange(e: any, condition: Condition) {
            context.emit('valueChange', e, condition);
        }

        function renderFieldOfCondition(condition: Condition) {
            return <div class="condition-list-item-type mr-3">
                <FComboList value-field={'labelCode'} text-field={'name'} id-field={'id'}
                    data={fields.value} v-model={condition.fieldCode}
                    onChange={(e: any) => onFieldChanged(condition)}>
                </FComboList>
            </div>;
        }

        function renderCompareOfCondition(condition: Condition) {
            return <div class="condition-list-item-compare mr-3">
                <FComboList value-field={'value'} text-field={'name'} id-field={'id'}
                    data={getCompareOperators(condition)} v-model={condition.compareType}
                    onChange={(e: any) => onCompareChanged(e, condition)}>
                </FComboList>
            </div>;
        }

        function renderValueOfConditon(condition: Condition) {
            return <div class="condition-list-item-control mr-3">
                <FDynamicFormGroup editor={fieldMap.get(condition.fieldCode)?.editor}
                    showLabel={false} v-model={(condition.value as ConditionValue).value}
                    onChange={(e: any) => { onChange(e, condition); }}>
                </FDynamicFormGroup>
            </div>;
        }

        function renderOperationsOfCondition(condition: Condition) {
            return [
                <div class="condition-list-item-extend">
                    <span class="f-icon f-icon-plus-sm mr-2" onClick={() => insertConditionTo(condition)}></span>
                    <span class="f-icon f-icon-minus-sm" onClick={() => removeCondition(condition)}></span>
                </div>,
                <div class="custom-control custom-checkbox">
                    <input title="selection" class="custom-control-input" type="checkbox"
                        checked={selectedItems.value.has(condition.conditionId)} />
                    <label class="custom-control-label" onClick={() => toggleSelect(condition)}></label>
                </div>
            ];
        }

        function renderGroupItems(conditionGroup: ConditionGroup) {
            return conditionGroup.items.map((condition: Condition) => (
                <div class="condition-list-item" key={condition.conditionId}>
                    {renderFieldOfCondition(condition)}
                    {renderCompareOfCondition(condition)}
                    {renderValueOfConditon(condition)}
                    {renderOperationsOfCondition(condition)}
                </div>
            ));
        }

        function renderGroupRelation(conditionGroup: ConditionGroup) {
            return (
                <div class="condition-list-relation">
                    <span class="condition-list-relation-close f-icon-filter-cancel btn-link"
                        onClick={() => unGroup(conditionGroup)}></span>
                    <span class="condition-list-relation-text btn-link" onClick={() => changeGroupRelation(conditionGroup)}>
                        {conditionGroup.relation === 1 ? '与' : '或'}
                    </span>
                </div>
            );
        }

        function renderGroup(conditionGroup: ConditionGroup) {
            return (
                <div class="condition-list-content">
                    <div class="condition-list-content-group">
                        {renderGroupItems(conditionGroup)}
                        {conditionGroup.children && conditionGroup.children.map((subGroup: ConditionGroup) => renderGroup(subGroup))}
                    </div>
                    {renderGroupRelation(conditionGroup)}
                </div>
            );
        }

        function addGroup() {
            group(useSelectionComposition.getSelectedGroupItem());
            useSelectionComposition.clear();
        }

        function renderFooterBar() {
            return (
                <div class="condition-list-bottom">
                    <div class="add-condition-btn mb-1 mr-3" onClick={addCondition}>
                        <span class="f-icon f-icon-filter-add"></span>
                        <span class="ml-1">添加条件</span>
                    </div>
                    <div class="add-condition-btn mb-1" onClick={addGroup}>
                        <span class="f-icon f-icon-filter-grouping"></span>
                        <span class="ml-1">生成条件组</span>
                    </div>
                    <div class="condition-list-reset add-condition-btn" onClick={reset}>
                        重置
                    </div>
                </div>
            );
        }

        return () => {
            return (
                <div class="condition-list" key={key.value}>
                    <div class="condition-list-body">{renderGroup(rootGroup.value)}</div>
                    {renderFooterBar()}
                </div>
            );
        };
    }
});
