/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import { FieldConfig, Condition } from './types';

export const conditionProps = {
    conditions: { type: Array<Condition>, default: [] },
    fields: { type: Array<FieldConfig>, default: [] },
    key: { type: String, default: '' },
     /**
     * 控间标签同行展示
     */
    isControlInline: { type: Boolean, default: true},
};

export type ConditionProps = ExtractPropTypes<typeof conditionProps>;
