/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App, Plugin } from 'vue';
import FConditionFields from './src/condition-fields.component';
import FConditionList from './src/condition-list.component';

export * from './src/condition.props';
export * from './src/types';
export * from './src/composition/types';
export * from './src/composition/condition-value/types';
export * from './src/composition/use-compare';
export * from './src/composition/use-condition';
export * from './src/composition/use-condition-value';

FConditionList.install = (app: App) => {
    app.component(FConditionFields.name as string, FConditionFields)
        .component(FConditionList.name as string, FConditionList);
};

export { FConditionFields, FConditionList };
export default FConditionList as typeof FConditionList & Plugin;
