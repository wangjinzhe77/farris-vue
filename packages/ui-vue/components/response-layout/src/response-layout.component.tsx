import { defineComponent } from 'vue';
import { ResponseLayoutPropsType, responseLayoutProps } from './response-layout.props';

export default defineComponent({
    name: 'FResponseLayout',
    props: responseLayoutProps,
    emits: [],
    setup(props: ResponseLayoutPropsType, context) {
        return () => {
            return <div class={props.customClass}>{context.slots.default && context.slots.default()}</div>;
        };
    }
});
