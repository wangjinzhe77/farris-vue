import { defineComponent } from 'vue';
import { ResponseLayoutItemPropsType, responseLayoutItemProps } from './response-layout-item.props';

export default defineComponent({
    name: 'FResponseLayoutItem',
    props: responseLayoutItemProps,
    emits: [],
    setup(props: ResponseLayoutItemPropsType, context) {
        return () => {
            return <div class={props.customClass}>{context.slots.default && context.slots.default()}</div>;
        };
    }
});
