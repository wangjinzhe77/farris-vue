import { defineComponent, inject, onMounted, ref } from 'vue';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { ResponseLayoutPropsType, responseLayoutProps } from '../response-layout.props';

export default defineComponent({
    name: 'FResponseLayoutDesign',
    props: responseLayoutProps,
    emits: [],
    setup(props: ResponseLayoutPropsType, context) {
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const componentInstance = useDesignerComponent(elementRef, designItemContext);
        componentInstance.value.canNested = false;

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        return () => {
            return (
                <div ref={elementRef} class="d-flex response-layout">
                    {context.slots.default && context.slots.default()}
                </div>
            );
        };
    }
});
