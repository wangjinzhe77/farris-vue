/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, ref, withModifiers } from 'vue';
import type { SetupContext } from 'vue';
import FButton from '@farris/ui-vue/components/button';
import { buttonGroupProps, ButtonGroupProps } from './button-group.props';
import getDropdownButton from './components/button-group-dropdown.component';
import { ButtonItem } from './composition/types';

export default defineComponent({
    name: 'FButtonGroup',
    props: buttonGroupProps,
    emits: ['click', 'change', 'changeState'] as (string[] & ThisType<void>) | undefined,
    setup(props: ButtonGroupProps, context: SetupContext) {
        const buttonGroupRef = ref();
        /* 显示出来的按钮组 */
        const flatButtons: any[] = (props.data && props.data.slice(0, props.count)) || [];
        const dpButtons = (props.data && props.data.slice(props.count)) || [];

        const buttonType = (buttonItem: ButtonItem) => {
            return buttonItem.type || props.type;
        };
        const buttonDisabled = (buttonItem: ButtonItem) => {
            return buttonItem.disabled !== undefined ? buttonItem.disabled : props.disabled;
        };
        const customButtonClass = function (buttonItem: any) {
            const classObject = {
                'f-btn-ml': (buttonItem.type && buttonItem.type !== 'link' || props.type !== 'link') && props.mode === 'default',
                'btn-link': !buttonItem.type && !props.type || props.type === 'link' || buttonItem.type === 'link',
            } as Record<string, boolean>;
            const buttonType = buttonItem.type ? buttonItem.type : props.type && props.type !== 'link' ? props.type : '';
            if (buttonType) {
                classObject[`btn-${buttonType}`] = true;
            }
            return classObject;
        };
        const buttonGroupClass = function () {
            const classObject = {
                'position-relative': true,
                'btn-group': props.mode === 'group',
                'f-btn-group-links': props.type === 'link',
                // 'btn-group-lg': props.size === 'large',
                // 'btn-group-sm': props.size === 'small',
            } as Record<string, boolean>;
            return classObject;
        };

        function onClick(payload: MouseEvent, buttonItem: any) {
            context.emit('change', buttonItem.id);
            context.emit('click', buttonItem);
        }

        function renderFlatButton(buttonItem: ButtonItem) {
            // return (
            //     <button type="button" id={buttonItem.id} title={buttonItem.id}
            //         class={buttonClass(buttonItem)} onClick={(payload: MouseEvent) => onClick(payload, buttonItem)}>
            //         {buttonItem.icon && <i class={`f-icon ${buttonItem.icon}`}></i>}
            //         {!buttonItem.icon && buttonItem.text}
            //     </button>
            // );
            return <FButton
                disabled={buttonDisabled(buttonItem)}
                id={buttonItem.id}
                icon={buttonItem.icon}
                size={props.size}
                type={buttonType(buttonItem)}
                customClass={customButtonClass(buttonItem)}
                onClick={(payload: MouseEvent) => onClick(payload, buttonItem)}>
                {buttonItem.text}
            </FButton>;
        }

        const { renderDropdownButton } = getDropdownButton(props, context);

        return () => (
            <div class="f-btn-group" style="position:relative;" ref={buttonGroupRef}>
                <div class={buttonGroupClass()} style="display:inline-flex;vertical-align:middle">
                    {flatButtons.map((buttonItem) => renderFlatButton(buttonItem))}
                </div>
                {!!dpButtons.length && renderDropdownButton(dpButtons)}
            </div>
        );
    }
});
