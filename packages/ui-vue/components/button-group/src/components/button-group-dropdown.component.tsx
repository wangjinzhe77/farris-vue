import { SetupContext, ref } from 'vue';
import { ButtonGroupProps } from '../button-group.props';
import { usePopup } from '../composition/use-popup';
import getDropdownMenu from './button-group-dropdown-menu.component';

export default function (props: ButtonGroupProps, context: SetupContext) {
    const buttonGroupRef = ref();
    const dropdownButtonRef = ref();
    const usePopupComposition = usePopup(props, context, dropdownButtonRef);
    const { onMouseleaveDropdownButton, onClickDropdownButton, shouldPopupContent } = usePopupComposition;
    const { renderDropdownMenu } = getDropdownMenu(props, buttonGroupRef, usePopupComposition);

    function renderDropdownButton(dropdownButtons: any[]) {
        return (
            <div class="btn-group f-btn-group-dropdown" ref={buttonGroupRef} onMouseleave={onMouseleaveDropdownButton}>
                <button title="button" ref={dropdownButtonRef} type="button" class="f-btn-dropdown" onClick={onClickDropdownButton}>
                    <span class="f-icon f-icon-lookup"></span>
                </button>
                {shouldPopupContent.value && renderDropdownMenu(dropdownButtons)}
            </div>
        );
    }

    return { renderDropdownButton };
}
