import { Ref } from "vue";
import { ButtonGroupProps } from "../button-group.props";
import { UsePopup } from "../composition/types";
import FPopover from '@farris/ui-vue/components/popover';

export default function (props: ButtonGroupProps, buttonGroupRef: Ref<any>, usePopupComposition: UsePopup) {

    const { hidePopup, onClickDropdownMenuItem, popoverRef } = usePopupComposition;

    function renderDropdownItem(dropdownItem: any) {
        return (
            <>
                {dropdownItem.divider && <div class="dropdown-divider"></div>}
                <div
                    id={dropdownItem.id}
                    class={'dropdown-item' + (dropdownItem.disabled ? ' disabled' : '')}
                    onClick={(event: MouseEvent) => onClickDropdownMenuItem(event, dropdownItem)}>
                    {dropdownItem.icon && <i class={'f-icon dropdown-item-icon' + dropdownItem.icon}></i>}
                    <span>{dropdownItem.text}</span>
                </div>
            </>
        );
    }

    function renderDropdownMenu(dropdownButtons: any[]) {
        return (
            <FPopover id={`${props.id}-popover`} ref={popoverRef} visible={true} placement={props.placement}
                host={buttonGroupRef.value} fitContent={true} onHidden={hidePopup} minWidth={120}>
                {dropdownButtons.map((dropdownItem: any) => renderDropdownItem(dropdownItem))}
            </FPopover>
        );
    };

    return { renderDropdownMenu };
}
