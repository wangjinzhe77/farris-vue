/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, inject, onMounted, ref } from 'vue';
import type { SetupContext } from 'vue';
import { buttonGroupProps, ButtonGroupProps } from '../button-group.props';
import { DesignerItemContext, useDesignerComponent } from '@farris/ui-vue/components/designer-canvas';
import getDropdownButton from '../components/button-group-dropdown.component';

export default defineComponent({
    name: 'FButtonGroupDesign',
    props: buttonGroupProps,
    emits: ['click', 'changeState', 'change', 'clickMenuOut'] as (string[] & ThisType<void>) | undefined,
    setup(props: ButtonGroupProps, context: SetupContext) {
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const componentInstance = useDesignerComponent(elementRef, designItemContext);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        /* 显示出来的按钮组 */
        const flatButtons: any[] = (props.data && props.data.slice(0, props.count)) || [];
        const dpButtons = (props.data && props.data.slice(props.count)) || [];

        const buttonClass = function (buttonItem: any) {
            const classObject = {
                'btn': true,
                'f-btn-ml': buttonItem.type && buttonItem.type !== 'link',
                'btn-link': !!buttonItem.icon || !buttonItem.type,
                'btn-icontext': !!buttonItem.icon,
                'disabled': buttonItem.disabled
            } as Record<string, boolean>;
            if (buttonItem.type) {
                classObject[`btn-${buttonItem.type}`] = true;
            }
            return classObject;
        };

        function renderFlatButton(buttonItem: any) {
            return (
                <div>
                    <button type="button" id={buttonItem.id} title={buttonItem.id}
                        class={buttonClass(buttonItem)}>
                        {buttonItem.icon && <i class={`f-icon ${buttonItem.icon}`}></i>}
                        {!buttonItem.icon && buttonItem.text}
                    </button>
                </div>
            );
        }

        const { renderDropdownButton } = getDropdownButton(props, context);

        return () => (
            <div class="f-btn-group" style="position:relative;" ref={elementRef}>
                <div class={(props.size === 'large' ? 'btn-group-lg' : 'btn-group-sm') + ' btn-group f-btn-group-links'}>
                    {flatButtons.map((buttonItem) => renderFlatButton(buttonItem))}
                </div>
                {!!dpButtons.length && renderDropdownButton(dpButtons)}
            </div>
        );
    }
});
