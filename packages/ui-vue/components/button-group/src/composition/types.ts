import type { ButtonProps } from "@farris/ui-vue/components/button";
import type { Ref } from "vue";

/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export interface UsePopup {

    hidePopup(): void;

    shouldPopupContent: Ref<boolean>;

    togglePopup(): void;

    popoverRef: Ref<any>;

    onClickDropdownButton: (payload: MouseEvent) => void;

    onClickDropdownMenuItem: (payload: MouseEvent, menuItem: any) => void;

    onMouseleaveDropdownButton: (payload: MouseEvent) => void;
}
export type PlacementDirection =
    | 'top'
    | 'top-left'
    | 'top-right'
    | 'left'
    | 'left-top'
    | 'left-bottom'
    | 'bottom'
    | 'bottom-left'
    | 'bottom-right'
    | 'right'
    | 'right-top'
    | 'right-bottom';
// export type ButtonSize = 'large' | 'small' | 'middle';
// export type ButtonItem = {
//     id: string;
//     text: string;
//     type?: string;
//     disable?: boolean;
//     icon?: string;
// };
export type ButtonItem = ButtonProps & { text: string };
