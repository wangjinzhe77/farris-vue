/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ButtonGroupProps } from '../button-group.props';
import { SetupContext, ref, nextTick, Ref } from 'vue';
import { UsePopup } from './types';

export function usePopup(
    props: ButtonGroupProps,
    context: SetupContext,
    dropdownButtonRef: Ref<any>
): UsePopup {
    const popoverRef = ref<any>();
    const shouldPopupContent = ref(false);

    function tryShowPopover() {
        const popoverInstance = popoverRef.value;
        if (popoverInstance) {
            popoverInstance.show(dropdownButtonRef.value);
        }
    }

    async function togglePopup() {
        shouldPopupContent.value = !shouldPopupContent.value;
        await nextTick();
        tryShowPopover();
    }

    function hidePopup() {
        shouldPopupContent.value = false;
        context.emit('changeState', shouldPopupContent.value);
    }

    function onClickDropdownButton($event: Event) {
        $event.stopPropagation();
        togglePopup();
        context.emit('changeState', shouldPopupContent.value);
    }

    function onClickDropdownMenuItem($event: any, menuItem: any) {
        $event.stopPropagation();
        if (menuItem.disabled) {return;}
        shouldPopupContent.value = false;
        context.emit('change', menuItem.id);
        context.emit('click', menuItem);
    }

    function onMouseleaveDropdownButton() {
        if (shouldPopupContent.value) {
            setTimeout(() => {
                hidePopup();
            }, 1000);
        }
    }

    return {
        hidePopup,
        onClickDropdownButton,
        onClickDropdownMenuItem,
        onMouseleaveDropdownButton,
        popoverRef,
        shouldPopupContent,
        togglePopup
    };

}
