 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { ButtonType, ButtonSize } from '@farris/ui-vue/components/button';
import { createPropsResolver } from '@farris/ui-vue/components/dynamic-resolver';
import buttonGroupSchema from './schema/button-group.schema.json';
import { schemaResolver } from './schema/schema-resolver';
import { schemaMapper } from './schema/schema-mapper';
import propertyConfig from './property-config/button-group.property-config.json';
import { PlacementDirection, ButtonItem } from './composition/types';

type ButtonGroupType = 'default' | 'group';

export const buttonGroupProps = {
    /**
     * 组件标识
     */
    id: String,
    /**
     * 按钮信息
     */
    data: {
        type: Array<ButtonItem>, default: []
    },
    /**
     * 禁用
     */
    disabled: { type: Boolean, default: false },
    /**
     * 显示的按钮数量  默认为2
     */
    count: { type: Number, default: 2 },
    /**
     * 按钮组模式，默认按钮之间带间隔，另一种不带间隔
     */
    mode: { type: String as PropType<ButtonGroupType>, default: 'default' },
    /**
     * 按钮大小 small/middle/large
     */
    size: { type: String as PropType<ButtonSize>, default: 'middle' },
    /**
     * 按钮样式
     */
    type: { type: String as PropType<ButtonType>, default: 'primary' },
    /**
     * 按钮展示位置
     */
    placement: { type: String as PropType<PlacementDirection>, default: 'bottom-left' },

} as Record<string, any>;
export default buttonGroupProps;

export type ButtonGroupProps = ExtractPropTypes<typeof buttonGroupProps>;
export const propsResolver = createPropsResolver<ButtonGroupProps>(buttonGroupProps, buttonGroupSchema, schemaMapper, schemaResolver, propertyConfig);
