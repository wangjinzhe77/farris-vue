/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { type App, Plugin } from 'vue';
import FNotify from './src/notify.component';
import FToast from './src/components/toast.component';
import FNotifyService from './src/notify.service';

export * from './src/notify.props';
export * from './src/components/toast.props';

export { FNotify, FNotifyService, FToast };

export const F_NOTIFY_SERVICE_TOKEN = Symbol('NOTIFY_SERVICE_TOKEN');

FNotify.install = (app: App) => {
    app.component(FNotify.name as string, FNotify);
    app.component(FToast.name as string, FToast);
    const notifyInstance = new FNotifyService();
    app.provide(F_NOTIFY_SERVICE_TOKEN, notifyInstance);
    app.provide('FNotifyService', notifyInstance);
};

export default FNotify as typeof FNotify & Plugin;
