/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';

export type NotifyPosition = 'bottom-right' | 'bottom-left' | 'top-right' | 'top-left' | 'top-center' | 'bottom-center' | 'center-center';

export type ToastyAnimate =
    | 'bounceInRight'
    | 'bounceInLeft'
    | 'bounceInRight'
    | 'bounceInLeft'
    | 'bounceInDown'
    | 'bounceInUp'
    | 'bounceIn'
    | 'fadeIn';

export type NotifyTheme = 'default' | 'material' | 'bootstrap';

export interface NotifyButton {
    customClass?: string;
    text: string;
    disable?: boolean;
    onClick?: ($event: Event, component: any) => any;
}

export interface NotifyData {
    type: string;
    title?: string;
    message?: string;
    /** 按钮列表模板 */
    buttons?: Array<NotifyButton>;
    showClose?: boolean;
    theme?: string;
    timeout?: number;
    id?: number | string;
}

// export interface NotifyData extends NotifyOptions {}

export const notifyProps = {
    showCloseButton: { type: Boolean, default: true },
    position: { type: String as PropType<NotifyPosition>, default: 'top-center' },
    timeout: { type: Number, default: 3000 },
    theme: { type: String as PropType<NotifyTheme>, default: 'bootstrap' },
    left: { type: Number },
    right: { type: Number },
    top: { type: Number },
    bottom: { type: Number },
    id: { type: String },
    animate: { type: String as PropType<ToastyAnimate>, default: 'fadeIn' },
    options: { type: Object as PropType<NotifyData> }
};
export type NotifyProps = ExtractPropTypes<typeof notifyProps>;

export interface ShowNotifyParams {
    /** 通知内容 */
    message: string;
    /** 标题 */
    title?: string;
    /** 是否显示关闭按钮 */
    showCloseButton?: boolean;
    /** 通知位置 */
    position?: NotifyPosition;
    /** 延时关闭时间，默认 3000ms */
    timeout?: number;
}

export interface NotifyGlobalConfig {
    /** 是否显示关闭按钮 */
    showCloseButton?: boolean;
    /** 通知位置 */
    position?: NotifyPosition;
    /** 延时关闭时间，默认 3000ms */
    timeout?: number;
}
