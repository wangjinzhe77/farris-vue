/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, inject, isRef, reactive, ref, SetupContext, onMounted } from 'vue';
import { NotifyData, NotifyProps, notifyProps } from './notify.props';
import Toast from './components/toast.component';
import { extend } from 'lodash-es';
import NotifyService from './notify.service';

export default defineComponent({
    name: 'Notify',
    props: notifyProps,
    emits: ['close', 'empty'] as (string[] & ThisType<void>) | undefined,
    setup(props: NotifyProps, context: SetupContext) {
        const notifyClass = computed(() => ({
            'farris-notify': true
        }));

        const defaultNotifyDistance = {
            left: 12,
            right: 12,
            top: 20,
            bottom: 12
        };

        const notifyContainerRef = ref<HTMLElement>();

        const notifyOptions = ref(props.options);
        const showCloseButton = ref(props.showCloseButton);

        const notifyPosition = computed(() => {
            return props.position || 'bottom-right';
        });

        const timeout = computed(() => {
            return props.timeout !=null? props.timeout: 3000;
        });

        const notifyStyle = computed(() => {

            const _bottom = props.bottom ? props.bottom : defaultNotifyDistance.bottom;
            const _top = props.top ? props.top : defaultNotifyDistance.top;

            const styleObject = {
                transition: 'all 0.2s ease',
                left: notifyPosition.value.indexOf('left') > -1 ? `${props.left ? props.left : defaultNotifyDistance.left}px` : '',
                right: notifyPosition.value.indexOf('right') > -1 ? `${props.right ? props.right : defaultNotifyDistance.right}px` : '',
                top: notifyPosition.value.indexOf('top') > -1 ? `${_top}px` : '',
                bottom: notifyPosition.value.indexOf('bottom') > -1 ? `${_bottom}px` : ''
            } as any;
            if (notifyPosition.value.indexOf('center') > -1) {
                styleObject.left = '50%';
                styleObject.marginLeft = 'calc(-24rem / 2)';

                if (notifyPosition.value === 'center-center') {
                    styleObject.top = '50%';
                    styleObject.transform = 'translate(-50%, -50%)';
                }
            }

            return styleObject;
        });

        function closeToast(toast?: NotifyData) {
            context.emit('close');
        }

        if (timeout.value) {
            setTimeout(() => {
                closeToast();
            }, timeout.value);
        }

        context.expose({ closeToast, container: notifyContainerRef, notifyPosition });

        function onClose($event: Event, toast?: NotifyData) {
            closeToast(toast);
        }

        return () => {
            return (
                <div class={notifyClass.value} style={notifyStyle.value} ref={notifyContainerRef}>
                    <Toast
                        options={notifyOptions.value}
                        showCloseButton={showCloseButton.value}
                        animate={props.animate}
                        onClose={($event: any) => onClose($event, notifyOptions.value)}></Toast>
                </div>
            );
        };
    }
});
