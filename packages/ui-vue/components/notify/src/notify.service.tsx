import { createApp, inject, onMounted, onUnmounted, reactive, ref, Transition, type App } from "vue";
import { NotifyGlobalConfig, NotifyProps, ShowNotifyParams } from "./notify.props";
import Notify from './notify.component';

export default class NotifyService {

    notifyRefs: Array<any> = [];

    globalConfig: Partial<NotifyGlobalConfig> = reactive({});

    private createNotifyInstance(options: Partial<NotifyProps>): App {
        const self = this;

        const defulatConfig = {
            timeout: 3000,
            position: 'bottom-right',
            showCloseButton: true,
        };

        const notifySettings: any = Object.assign(defulatConfig, this.globalConfig, { ...options });
        const container = document.createElement('div');
        container.style.display = 'contents';

        const app: App = createApp({
            setup() {
                const notifyIns = ref();

                function onNotifyClosed() {
                    notifyIns.value.container.style.transform = 'scale(0)';
                    setTimeout(() => {
                        self.updateNotifyPositionForClose(notifySettings, notifyIns);
                        app.unmount();
                    }, 220);
                };

                if (notifySettings.position.indexOf('top') > -1) {
                    const notifyInstances = self.getNotifyInstances(notifySettings.position);
                    const lastNotify = notifyInstances[notifyInstances.length - 1];
                    if (lastNotify) {
                        const lastNotifyRect = lastNotify.value.container.getBoundingClientRect();
                        notifySettings.top = lastNotifyRect.bottom;
                    }
                }

                onUnmounted(() => {
                    document.body.removeChild(container);
                });

                onMounted(() => {
                    self.updateNotifyPositionForCreate(notifySettings, notifyIns);
                });

                return () => <Transition mode={'out-in'} name={'fade'} appear>
                    <Notify ref={notifyIns} {...notifySettings} onClose={onNotifyClosed}></Notify>
                </Transition>;
            }
        });
        app.provide('NotifyService', this);
        document.body.appendChild(container);
        app.mount(container);
        return app;
    };

    private getNotifyInstances(notifyPosition: string): Array<any> {
        return this.notifyRefs.filter(instance => {
            return instance.value.notifyPosition === notifyPosition;
        });
    }

    private updateNotifyPositionForCreate(notifyProps: NotifyProps, notifyInstance: any) {
        if (this.notifyRefs && this.notifyRefs.length) {
            const winHeight = window.innerHeight;

            if (notifyProps.position.indexOf('bottom') > -1) {
                this.getNotifyInstances(notifyProps.position).forEach((item: any) => {
                    const itemRect = item.value.container.getBoundingClientRect();
                    item.value.container.style.bottom = (itemRect.height + winHeight - itemRect.bottom) + 'px';
                });
            }

        }

        this.notifyRefs = [...this.notifyRefs, notifyInstance];
    }

    private updateNotifyPositionForClose(notifyProps: NotifyProps, notifyInstance: any) {
        const index = this.notifyRefs.indexOf(notifyInstance);

        if (notifyProps.position.indexOf('top') > -1) {
            const notifyInstances = this.getNotifyInstances(notifyProps.position);
            const notifyInstanceIndex = notifyInstances.indexOf(notifyInstance);
            notifyInstances.slice(notifyInstanceIndex + 1).forEach(item => {
                item.value.container.style.top = (item.value.container.offsetTop - item.value.container.offsetHeight) + 'px';
            });
        }

        if (index > -1) {
            this.notifyRefs.splice(index, 1);
        }

    }

    show(props: Partial<NotifyProps>): App {
        const notifyInstance = this.createNotifyInstance(props);
        return notifyInstance;
    }

    private buildNotifyProps(type: string, notifyConfig: ShowNotifyParams | string) {
        let message = '';
        let title = '';
        let timeout;
        let position;
        let showCloseButton;

        const notfiyConfigIsString = typeof notifyConfig === 'string';

        if (notfiyConfigIsString) {
            message = notifyConfig;
        } else if (notifyConfig) {
            message = notifyConfig.message || '';
            title = notifyConfig.title || '';
            position = notifyConfig.position || null;
            showCloseButton = notifyConfig.showCloseButton != null ? notifyConfig.showCloseButton : null;
            timeout = notifyConfig.timeout != null ? notifyConfig.timeout : null;
        }
        const props: Partial<NotifyProps> = {
            options: { type, message, title }
        };

        if (position != null) {
            props.position = position;
        }

        if (showCloseButton != null) {
            props.showCloseButton = showCloseButton;
        }

        if (timeout != null) {
            props.timeout = timeout;
        }

        return props;
    }

    info(opts: ShowNotifyParams | string): App {
        const props = this.buildNotifyProps('info', opts);
        return this.show(props);
    }

    success(opts: ShowNotifyParams | string): App {
        const props = this.buildNotifyProps('success', opts);
        return this.show(props);
    }

    warning(opts: ShowNotifyParams | string): App {
        const props = this.buildNotifyProps('warning', opts);
        return this.show(props);
    }

    error(opts: ShowNotifyParams | string): App {
        const props = this.buildNotifyProps('error', opts);
        return this.show(props);
    }

    close(notifyApp: App): void {
        if (notifyApp) {
            notifyApp.unmount();
        }
    }

    closeAll(): void {
        this.notifyRefs.forEach(t => {
            t?.value.closeToast();
        });
        this.notifyRefs.length = 0;
    }
}
