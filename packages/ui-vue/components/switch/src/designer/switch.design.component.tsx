 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, inject, onMounted, ref, SetupContext, toRefs } from 'vue';
import { switchProps, SwitchProps } from '../switch.props';
import { DesignerItemContext } from '@farris/ui-vue/components/designer-canvas';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { useSwitchDesignerRules } from './use-design-rules';

export default defineComponent({
    name: 'FSwitchDesign',
    props: switchProps,
    emits: ['update:modelValue'] as (string[] & ThisType<void>) | undefined,
    setup(props: SwitchProps, context: SetupContext) {
        const { size, onLabel, offLabel } = toRefs(props);
        const elementRef = ref();
        const designerHostService = inject('designer-host-service');        
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useSwitchDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext,designerRulesComposition);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        const switchContainerClass = computed(() => ({
            switch: true,
            'f-cmp-switch': true,
            checked: false,
            'switch-large': size.value === 'large',
            'switch-medium': size.value === 'medium',
            'switch-small': size.value === 'small'
        }));

        const shouldShowSwitch = computed(() => {
            return onLabel?.value || offLabel?.value;
        });

        return () => {
            return (
                <span ref={elementRef} tabindex="0" role="button" class={switchContainerClass.value}   >
                    {shouldShowSwitch.value && (
                        <span class="switch-pane">
                            <span class="switch-label-unchecked">{offLabel?.value}</span>
                        </span>
                    )}
                    <small>{context.slots.default && context.slots.default()}</small>
                </span>
            );
        };
    }
});
