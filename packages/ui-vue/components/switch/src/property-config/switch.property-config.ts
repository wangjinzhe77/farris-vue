import { InputBaseProperty } from "../../../property-panel/src/composition/entity/input-base-property";

export class SwitchProperty extends InputBaseProperty {

    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }

    getEditorProperties(propertyData: any) {
        return this.getComponentConfig(propertyData, { type:"switch" }, {
            disabled: {
                visible:false
            },
            placeholder:{
                visible:false
            },
            onLabel: {
                description: "",
                title: "打开时标签",
                type: "string"
            },
            offLabel: {
                description: "",
                title: "关闭时标签",
                type: "string"
            },
            onBackground: {
                description: "",
                title: "打开时背景色",
                type: "string"
            },
            offBackground: {
                description: "",
                title: "关闭时背景色",
                type: "string"
            },
            size:{
                description: "",
                title: "尺寸",
                type: "enum",
                editor:{
                    data: [
                        {
                            id: "small",
                            name: "小号"
                        },
                        {
                            id: "medium",
                            name: "中号"
                        },
                        {
                            id: "large",
                            name: "大号"
                        }
                    ]
                }
            }
        });
    }
};

