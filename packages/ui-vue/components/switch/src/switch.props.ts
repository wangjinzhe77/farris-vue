/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import switchSchema from './schema/switch.schema.json';
import { schemaResolver } from './schema/schema-resolver';

export type SwitchType = 'small' | 'medium' | 'large';

export const switchProps = {
    /** 组件值*/
    modelValue: { type: Boolean, default: false },
    /** 禁用 */
    disabled: { type: Boolean,default:false },
    /**
     * 同disabled 
     */
    readonly: { type: Boolean,default:false },
    onBackground: { type: String },
    offBackground: { type: String },
    onColor: { type: String },
    offColor: { type: String },
    onLabel: { type: String },
    offLabel: { type: String },
    /** 尺寸大小 */
    size: { type: String as PropType<SwitchType>, default: 'medium' },
    /** 开关值变化事件 */
    onModelValueChanged: { type: Function, default: () => { } },
} as Record<string, any>;

export type SwitchProps = ExtractPropTypes<typeof switchProps>;

export const propsResolver = createPropsResolver<SwitchProps>(switchProps, switchSchema, schemaMapper, schemaResolver);
