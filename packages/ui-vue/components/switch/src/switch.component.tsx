/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, ref, SetupContext, toRefs, watch } from 'vue';
import { switchProps, SwitchProps } from './switch.props';
import './switch.css';

export default defineComponent({
    name: 'FSwitch',
    props: switchProps,
    emits: ['update:modelValue', 'modelValueChanged'] as (string[] & ThisType<void>) | undefined,
    setup(props: SwitchProps, context: SetupContext) {
        const { disabled, size, onLabel, offLabel, onBackground, offBackground, onColor, offColor,readonly } = toRefs(props);
        const modelValue = ref(props.modelValue);

        function getSwitchColor() {
            return '';
        }

        const switchContainerClass = computed(() => ({
            switch: true,
            'f-cmp-switch': true,
            checked: modelValue.value,
            disabled: readonly.value||disabled.value,
            'switch-large': size.value === 'large',
            'switch-medium': size.value === 'medium',
            'switch-small': size.value === 'small'
        }));

        const switchContainerStyle = computed(() => {
            if (modelValue.value) {
                return {
                    outline: 'none',
                    background: onBackground.value,
                };
            }

            return {
                outline: 'none',
                backgroundColor: offBackground.value,
            };
        });

        const smallStyle = computed(() => {
            if (modelValue.value) {
                return {
                    background: onColor.value,
                };
            }

            return {
                backgroundColor: offColor.value,
            };
        });

        const shouldShowSwitch = computed(() => {
            // checkedLabel || uncheckedLabel
            return onLabel?.value || offLabel?.value;
        });

        function updateChecked() {
            if (readonly.value||disabled.value) {
                return;
            }
            modelValue.value = !modelValue.value;
            context.emit('update:modelValue', modelValue.value);
            // context.emit('modelValueChanged', modelValue.value);

        }
        watch(modelValue, (newValue, oldValue) => {
            if (newValue !== oldValue) {
                context.emit('modelValueChanged', newValue);
            }
        });

        watch(() => props.modelValue, (newValue, oldValue) => {
            if (newValue !== oldValue) {
                modelValue.value = newValue;
            }
        });

        return () => {
            return (
                <span tabindex="0" role="button" class={switchContainerClass.value} style={switchContainerStyle.value}
                    onClick={updateChecked}>
                    {shouldShowSwitch.value && (
                        <span class="switch-pane">
                            <span class="switch-label-checked">{onLabel?.value}</span>
                            <span class="switch-label-unchecked">{offLabel?.value}</span>
                        </span>
                    )}
                    <small style={smallStyle.value}>{context.slots.default && context.slots.default()}</small>
                </span>
            );
        };
    }
});
