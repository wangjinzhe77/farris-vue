 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App } from 'vue';
import FSwitch from './src/switch.component';
import { propsResolver } from './src/switch.props';
import FSwitchDesign from './src/designer/switch.design.component';
import { withInstall } from '@farris/ui-vue/components/common';

export * from './src/switch.props';

FSwitch.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap.switch = FSwitch;
    propsResolverMap.switch = propsResolver;
};
FSwitch.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.switch = FSwitchDesign;
    propsResolverMap.switch = propsResolver;
};

export { FSwitch };
export default withInstall(FSwitch);
