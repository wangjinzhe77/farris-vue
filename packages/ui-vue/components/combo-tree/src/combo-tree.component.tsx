import { Ref, SetupContext, computed, defineComponent, ref, watch } from "vue";
import FButtonEdit from '@farris/ui-vue/components/button-edit';
import { ComboTreeProps, comboTreeProps, Option } from "./combo-tree.props";
import ComboTreeContainer from './components/tree-container.component';
import { useDataSource } from "./composition/use-data-source";

export default defineComponent({
    name: 'FComboTree',
    props: comboTreeProps,
    emits: ['clear', 'update:modelValue', 'change'],
    setup(props: ComboTreeProps, context: SetupContext) {
        return () => {
            const comboEditorRef: Ref<any> = ref();
            const disable = ref(props.disabled);
            const enableClear = ref(props.enableClear);
            const enableSearch = ref(props.enableSearch);
            const readonly = ref(props.readonly);
            const { dataSource, displayText, editable, modelValue, getSelectedItemsByDisplayText } = useDataSource(props);

            const isMultiSelect = computed(() => props.multiSelect);

            const comboEditorWidth = computed(() => {
                return comboEditorRef.value ? (comboEditorRef.value.elementRef as HTMLElement).getBoundingClientRect().width : 0;
            });

            function tryHidePopupOnSelect() {
                const shouldHidePopupOnSelect = !isMultiSelect.value;
                if (shouldHidePopupOnSelect && comboEditorRef.value) {
                    comboEditorRef.value.hidePopup();
                }
            }

            function onSelectionChange(selectedItems: Option[]) {
                displayText.value = selectedItems.map((item: Option) => item[props.textField]).join(props.separator);
                modelValue.value = selectedItems.map((item: Option) => item[props.valueField]).join(props.separator);
                context.emit('update:modelValue', modelValue.value);
                context.emit('change', selectedItems, modelValue.value);
                tryHidePopupOnSelect();
            }

            function onClear($event: Event) {
                modelValue.value = '';
                context.emit('update:modelValue', '');
                context.emit('clear');
            }

            function onDisplayTextChange(displayText: string) {
                const selectedItems = getSelectedItemsByDisplayText(displayText);
                onSelectionChange(selectedItems);
            }

            watch(
                [() => props.disabled, () => props.editable, () => props.enableClear, () => props.enableSearch, () => props.readonly],
                ([newDisabled, newEditable, newEnableClear, newEnableSearch, newReadonly]) => {
                    disable.value = newDisabled;
                    editable.value = newEditable;
                    enableClear.value = newEnableClear;
                    enableSearch.value = newEnableSearch;
                    readonly.value = newReadonly;
                }
            );

            return (
                <FButtonEdit
                    ref={comboEditorRef}
                    id={props.id}
                    disable={disable.value}
                    readonly={readonly.value}
                    forcePlaceholder={props.forcePlaceholder}
                    editable={editable.value}
                    buttonContent={props.dropDownIcon}
                    placeholder={props.placeholder}
                    enableClear={enableClear.value}
                    maxLength={props.maxLength}
                    tabIndex={props.tabIndex}
                    enableTitle={props.enableTitle}
                    multiSelect={props.multiSelect}
                    inputType={props.multiSelect ? 'tag' : 'text'}
                    popupOnClick={true}
                    v-model={displayText.value}
                    onClear={onClear}
                    onChange={onDisplayTextChange}
                    focusOnCreated={props.focusOnCreated}
                    selectOnCreated={props.selectOnCreated}
                    placement={'auto'}>
                    <ComboTreeContainer
                        maxHeight={props.maxHeight}
                        idField={props.idField}
                        valueField={props.valueField}
                        textField={props.textField}
                        titleField={props.titleField}
                        data={dataSource.value}
                        selectedValues={modelValue.value}
                        onSelectionChange={onSelectionChange}
                        formatter={props.formatter}
                        editorParams={props.editorParams}
                        repositoryToken={props.repositoryToken}
                        customRowStatus={props.customRowStatus}
                    ></ComboTreeContainer>
                </FButtonEdit>
            );
        };
    }
});
