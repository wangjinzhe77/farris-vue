import { SetupContext, computed, defineComponent, inject, onMounted, ref } from "vue";
import FTreeView from '@farris/ui-vue/components/tree-view';
import { ComboTreeHttpService } from "../combo-tree.props";
import { TreeContainerProps, treeContainerProps } from "./tree-container.props";

export default defineComponent({
    name: 'FComboTreeContainer',
    props: treeContainerProps,
    emits: ['selectionChange'],
    setup(props: TreeContainerProps, context: SetupContext) {
        const dataSource = ref(props.data);
        const selections = ref<any[]>([]);
        const separator = ref(props.separator);
        const width = ref(props.width);
        const height = ref(props.height);
        const maxHeight = ref(props.maxHeight);
        const selectionValues = ref<string[]>(String(props.selectedValues).split(separator.value));

        const rowOption = {
            customRowStatus: props.customRowStatus
        };

        let repository: any = null;
        if (props.repositoryToken) {
            repository = inject<ComboTreeHttpService>(props.repositoryToken);
        }

        const treeColumn = computed(() => {
            return [{
                field: props.textField, title: '', dataType: 'string', formatter: props.formatter
            }];
        });

        const comboTreeContainerStyle = computed(() => {
            const styleObject = {} as Record<string, any>;
            if (width.value !== undefined) {
                styleObject.width = `${width.value}px`;
            }
            if (height.value !== undefined) {
                styleObject.height = `${height.value}px`;
            }

            // 当height设置了值之后，优先识别
            if (maxHeight.value !== undefined && maxHeight.value > 0) {
                styleObject.maxHeight = `${maxHeight.value}px`;
                styleObject.overflow = 'auto';
            }
            return styleObject;
        });

        function onSelectionChange(seletedItems: any[]) {
            selections.value = seletedItems.map((item: any) => Object.assign({}, item));
            selectionValues.value = seletedItems.map((item: any) => item[props.idField]);
            context.emit('selectionChange', selections.value);
        }

        onMounted(() => {
            if (repository) {
                repository.getData(props.editorParams).then(data => {
                    dataSource.value = data;
                });
            }
        });

        return () => {
            return (
                <div class="f-combo-tree-container" style={comboTreeContainerStyle.value}>
                    <FTreeView
                        data={dataSource.value}
                        idField={props.idField}
                        columns={ treeColumn.value }
                        selection-values={selectionValues.value}
                        onSelectionChange={onSelectionChange}
                        rowOption={rowOption}
                    ></FTreeView>
                </div>
            );
        };
    }
});
