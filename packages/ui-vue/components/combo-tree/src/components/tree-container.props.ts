import { ExtractPropTypes } from "vue";
import { Option } from '../combo-tree.props';

export const treeContainerProps = {

    data: { type: Array<Option>, default: [] },

    enableSearch: { type: Boolean, default: false },

    idField: { type: String, default: 'id' },

    multiSelect: { default: false, type: Boolean },

    selectedValues: { type: String, default: '' },

    separator: { type: String, default: ',' },

    textField: { type: String, default: 'name' },

    titleField: { type: String, default: 'name'},

    width: { type: Number },

    height: { type: Number },

    valueField: { type: String, default: 'id' },

    formatter: { type: Function },
    maxHeight: {type: Number, default: 350},
    repositoryToken: { type: Symbol, default: null },
    editorParams: { type: Object},
    customRowStatus: { type: Object, default: null }
};
export type TreeContainerProps = ExtractPropTypes<typeof treeContainerProps>;
