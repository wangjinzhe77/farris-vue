import { ref, watch } from "vue";
import { ComboTreeProps, Option } from "../combo-tree.props";
import { UseDataSource } from "./types";

export function useDataSource(props: ComboTreeProps): UseDataSource {
    const displayText = ref<string>('');
    const modelValue = ref(props.modelValue);
    const dataSource = ref(props.data || []);
    const editable = ref(props.editable);

    function flatTreeNodes(items, result = []) {
        items = items || [];
        return items.reduce((resultObject, current) => {
            resultObject.push(current);
            if (current.children && current.children.length) {
                flatTreeNodes(current.children, resultObject);
            }
            return resultObject;
        }, result);
    }

    function getItemsByValue(value: string): Option[] {
        const valueArray = String(value).split(props.separator).map<[string, boolean]>((valueText: string) => {
            return [valueText, true];
        });
        const valueMap = new Map<string, boolean>(valueArray);
        const items = [];
        flatTreeNodes(dataSource.value, items);
        return items.filter((item: Option) => {
            if (item.data) {
                return valueMap.has(String(item.data[props.valueField]));
            }
            return valueMap.has(String(item[props.valueField]));
        }).map((item: Option) => item.data ? item.data : item);
    }

    function updateDisplayTextByValue(value: string) {
        const matchedValue = getItemsByValue(value).map((item: Option) => item[props.textField]).join(props.separator);
        displayText.value = editable.value ? (matchedValue || value) : matchedValue;
    }

    function getItemsByDisplayText(text: string): Option[] {
        const displayTextArray = text.split(props.separator).map<[string, boolean]>((optionText: string) => {
            return [optionText, true];
        });
        const displayTextMap = new Map<string, boolean>(displayTextArray);
        // 如果是树结构  需要平铺数据
        if(dataSource.value[0].children){
            flatTreeNodes(dataSource.value, []);
        }
        return dataSource.value.filter((item: Option) => displayTextMap.has(item[props.textField]));
    }

    function buildSelectedItemByDisplayText(displayText: string) {
        const changedValue = {} as Record<string, string>;
        changedValue[props.idField] = displayText;
        changedValue[props.textField] = displayText;
        return [changedValue];
    }

    function getSelectedItemsByDisplayText(displayText: string) {
        let selectedItems = getItemsByDisplayText(displayText);
        const hasMatchedItems = selectedItems && selectedItems.length > 0;
        if (editable.value && !hasMatchedItems) {
            selectedItems = buildSelectedItemByDisplayText(displayText);
        }
        return selectedItems;
    }

    watch(() => props.data, () => {
        dataSource.value = props.data;
    });

    watch([dataSource], ([dataSourceValue]) => {
        if (props.modelValue) {
            const currentItem = dataSourceValue.find((item: any) => item[props.valueField] === props.modelValue);
            if (currentItem) {
                displayText.value = currentItem[props.textField];
            }
        }
    });

    watch(() => props.modelValue, (newValue, oldValue) => {
        if (newValue !== oldValue) {
            modelValue.value = newValue;
            updateDisplayTextByValue(newValue);
        }
    });

    updateDisplayTextByValue(props.modelValue);

    return { dataSource, displayText, editable, modelValue, getItemsByDisplayText, getItemsByValue, getSelectedItemsByDisplayText };
}
