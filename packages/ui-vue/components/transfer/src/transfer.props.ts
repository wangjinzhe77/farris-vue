 
import { ExtractPropTypes, PropType } from "vue";
import { RowOptions } from "@farris/ui-vue/components/data-view";
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import propertyConfig from './property-config/transfer.property-config.json';
import { createPropsResolver } from '../../dynamic-resolver/src/props-resolver';
import transferSchema from './schema/transfer.schema.json';

export const transferProps = {
    dataSource: {
        type: Array<object>, default: []
    },
    displayType: { type: String, default: 'List' },
    enableSearch: { type: Boolean, default: true },
    identifyField: { type: String, default: 'id' },
    textField: { type: String, default: 'name' },
    placeholder: { type: String, default: '' },
    selections: { type: Array<object>, default: [] },
    selectionValues: { type: Array<string>, default: [] },
    rowOption: { type: Object as PropType<RowOptions> }
} as Record<string, any>;

export type TransferProps = ExtractPropTypes<typeof transferProps>;

export const propsResolver = createPropsResolver<TransferProps>(transferProps, transferSchema, schemaMapper, schemaResolver, propertyConfig);
