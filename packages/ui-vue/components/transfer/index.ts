 
import { App } from 'vue';
import FTransfer from './src/transfer.component';
import FTransferDesign from './src/designer/transfer.design.component';
import { propsResolver } from './src/transfer.props';
import { withInstall } from '@farris/ui-vue/components/common';

export * from './src/transfer.props';
FTransfer.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.transfer = FTransfer;
    propsResolverMap.transfer = propsResolver;
};
FTransfer.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.transfer = FTransferDesign;
    propsResolverMap.transfer = propsResolver;
};

export { FTransfer };
export default withInstall(FTransfer);
