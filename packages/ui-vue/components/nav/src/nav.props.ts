/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import { NavItem } from './composition/types';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import navSchema from './schema/nav.schema.json';
import { schemaResolver } from './schema/schema-resolver';
import propertyConfig from './property-config/nav.property-config.json';

export const navProps = {
    /** 未读最大值 */
    maxNum: { Type: Number, default: 99 },
    /** 导航数据 */
    navData: {
        Type: Array<NavItem>, default: [{
            id: '1',
            text: '全部'
        },
        {
            id: '2',
            text: '已回复',
            disable: true
        },
        {
            id: '3',
            text: '待回复',
            num: 200
        },
        {
            id: '4',
            text: '@我',
            num: 33
        }]
    },
    /** 水平或垂直方向 */
    horizontal: { Type: Boolean, default: true },
    /** 当前激活的id */
    activeNavId: { Type: String, default: '1' },
    /** nav切换前事件 */
    navPicking: { Type: Function, default: (emptyObj?: object) => Promise<any> },
} as Record<string, any>;

export type NavProps = ExtractPropTypes<typeof navProps>;

export const propsResolver = createPropsResolver<NavProps>(navProps, navSchema, schemaMapper, schemaResolver, propertyConfig);
