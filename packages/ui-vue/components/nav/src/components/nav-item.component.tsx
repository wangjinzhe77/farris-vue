 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext } from 'vue';
import { navItemProps, NavItemProps } from './nav-item.props';

export default defineComponent({
    name: 'FNav',
    props: navItemProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: NavItemProps, context: SetupContext) {

        const navClick = () => {
        };

        return () => {
            return (
                <div class={["farris-nav-item",
                    // { 'active': props.id === activeNavId.value },
                    { 'disabled': props.disabled }]}
                onClick={() => navClick()}
                >
                    <div class="farris-nav-item-link">
                        <span class="farris-nav-item-link-text">
                            {context.slots.default?.()}

                        </span>
                    </div>
                </div>
            );
        };
    }
});
