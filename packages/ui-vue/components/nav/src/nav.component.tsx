/* eslint-disable no-use-before-define */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, ref, SetupContext, watch } from 'vue';
import { navProps, NavProps } from './nav.props';

export default defineComponent({
    name: 'FNav',
    props: navProps,
    emits: ['nav', 'update:activeNavId'] as (string[] & ThisType<void>) | undefined,
    setup(props: NavProps, context: SetupContext) {
        /** 当前激活的id */
        const _activeNavId = ref(props.activeNavId);
        /** 导航数据 */
        const _navData = ref(props.navData);
        /* 是否水平分布 */
        const _horizontal = ref(props.horizontal);
        /** 切换前函数 */
        const navPicking = ref(props.navPicking);

        // 监听激活id的变化
        watch(() => props.activeNavId, (newValue, oldValue) => {
            if (newValue !== oldValue) {
                _activeNavId.value = newValue;
            }
        });

        const navData = computed({
            get() {
                return _navData.value;
            },
            set(data: any) {
                _navData.value = data;
                initNavData();
            }
        });

        const activeNavId = computed({
            get(): string {
                return _activeNavId.value;
            },
            set(data: string) {
                _activeNavId.value = data;
                if (_activeNavId.value && navData && navData.value.length) {
                    const nav = navData.value.find((nav: any) => {
                        return nav.id === _activeNavId.value;
                    });
                    if (nav) {
                        context.emit('nav', nav);
                    }
                }
            }
        });

        const horizontal = computed({
            set(val: boolean) {
                _horizontal.value = val;
            },
            get() {
                return _horizontal.value;
            }
        });

        function initNavData() {
            if (navData.value && navData.value.length) {
                if (!_activeNavId.value) {
                    const activeNav = navData.value.find((nav: any) => {
                        return !nav.disable;
                    });
                    if (activeNav) {
                        _activeNavId.value = activeNav.id;
                    }
                }
            }
        }
        function navClick(nav: any) {
            if (!(nav.disable || nav.id === _activeNavId.value)) {
                _activeNavId.value = nav.id;
                context.emit('update:activeNavId', nav.id);
                context.emit('nav', nav);
            }
        }

        return () => {
            return (
                <div class={["farris-nav", { 'farris-nav-vertical': !horizontal.value }]}>
                    {navData.value?.map((item: any) => {
                        return (
                            <div class={["farris-nav-item",
                                { 'active': item.id === activeNavId.value },
                                { 'disabled': item.disable }]}
                                onClick={() => navClick(item)}
                            >
                                <div class="farris-nav-item-link">
                                    <span class="farris-nav-item-link-text">
                                        {typeof item.text === 'function' ? item.text() : item.text}
                                        {item.num && (
                                            <div class="farris-nav-item-tag">
                                                {item.num <= props.maxNum && (
                                                    <span class="tag-text">{item.num}</span>
                                                )}
                                                {item.num > props.maxNum && (
                                                    <span class="tag-text">{props.maxNum}+</span>
                                                )}
                                            </div>
                                        )}
                                    </span>
                                </div>
                            </div>
                        );
                    })}
                </div>
            );
        };
    }
});
