export interface NavItem {
    id: string;
    text: string;
    num?: number;
    disable?: boolean;
}
