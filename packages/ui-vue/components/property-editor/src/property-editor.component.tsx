
/* eslint-disable no-use-before-define */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext, Ref, ref, onMounted, watch } from 'vue';
import { PropertyEditorProps, propertyEditorProps } from './property-editor.props';
import { cloneDeep } from 'lodash-es';
import { option1, archiveOfPropertyValue, dropdownStatesInTotal } from './composition/data';
import { FComboList } from '../../../components/combo-list';
import { FInputGroup } from '../../../components/input-group';
import { FNumberSpinner } from '../../../components/number-spinner';
import { useInitializedValue } from './composition/use-initialized-value';
import { useUpdate } from './composition/use-update-data';

import './property-editor.css';

export default defineComponent({
    name: 'FPropertyEditor',
    props: propertyEditorProps,
    emits: ['propertyEditorValueChanged'] as (string[] & ThisType<void>) | undefined,
    setup(props: PropertyEditorProps, context: SetupContext) {
        /** 初始传入值PropertyEditorOptions*/
        // props.importedOriginalData
        const importedOriginalData: any = ref(option1);

        /** 表达式绑定值 */
        const searchText = ref('');

        /** 左侧状态切换-当前状态 （const-常量;variable-变量;custom-自定义） */
        const currentState: any = ref('const');

        /** 常量-enum类型-存储所有枚举值的数组 */
        const constEnumValueArray: any = ref([]);

        /** 变量-根据是否显示control名称，确认是否显示button */
        const showAddButton: Ref<boolean> = ref(false);

        /** 变量-点击button-存储根据命名规则生成的变量名*/
        let newVariable: any;

        /** 变量-点击button-生成代码编辑器中的field部分 */
        const fieldContent: Ref<string> = ref('');

        /** 变量-点击button-记录上一次点击值 */
        const frontValue: Ref<string> = ref('');

        /** 变量-下拉框-用户点击增加按钮后的变量数组 */
        const displayArray: any = ref([]);

        // /** 变量-下拉框-绑定值 */
        // let bindVariableValue: any;

        /** 状态机-下拉框 */
        let bindStateMachineValue: any;

        /** 状态机-用户点击下拉框后的变量数组 */
        const displayArrayState = ref([]);

        /** 状态机-文字-是/否 */
        const exists: Ref<string> = ref('是');

        /** 自定义-string类型-input-用户输入内容绑定 */
        const currentValue: any = ref('');

        /** 表达式-弹窗 */
        const showExpressionComponent: Ref<boolean> = ref(false);

        /** 表达式-group */
        const groupIcon = ref("<span class='f-icon f-icon-more-horizontal'></span>");

        /** 只读属性 */
        const readOnly: Ref<boolean> = ref(false);

        const dropdownStates: any = ref([]);
        /** 初始化数据 */
        const { numberConstValue, bindVariableValue, dataStatesBindValue, dataStatesValueArray, constEnumBindValue, setValue } = useInitializedValue(
            importedOriginalData, constEnumValueArray,
            currentState, bindStateMachineValue, exists,
            searchText, showExpressionComponent);
        /** 更新数据 */
        const { updateData, changeState, prefix, propertyEditorValueChange } = useUpdate(
            context, importedOriginalData, constEnumBindValue, constEnumValueArray,
            archiveOfPropertyValue, displayArrayState, bindStateMachineValue, exists,
            dataStatesValueArray, searchText, readOnly, currentState);

        /** 判断当前显示的变量是否已被删除，若被删除，则标红处理 */
        function ifHighlightBorder() {
            // if (currentState.value.id === 'variable' && importedOriginalData.value.propertyValue.type === 'variable') {
            //     // 更新变量下拉列表
            //     const getVariablesFunction = importedOriginalData.value.editorOptions.getVariables;
            //     if (getVariablesFunction) {
            //         displayArray.value = getVariablesFunction();
            //     }
            //     if (document.getElementsByClassName('f-page-single-property-editor-component-right-variable')) {
            //         if (document.getElementsByClassName('f-page-single-property-editor-component-right-variable')[0]) {
            //             const html = document.getElementsByClassName('f-page-single-property-editor-component-right-variable')[0];
            //             if (html.getElementsByClassName('input-group')) {
            //                 if (html.getElementsByClassName('input-group')[0]) {
            //                     const element = html.getElementsByClassName('input-group')[0];
            //                     // 查看是否该变量已被删除
            //                     const variableExist = displayArray.value.find(item => item.path === bindVariableValue);
            //                     // if (!variableExist && (bindVariableValue !== '' && bindVariableValue !== undefined)) {
            //                     //     element.style?.setProperty('borderColor', 'red');
            //                     // }
            //                     // else {
            //                     //     element.style?.setProperty('borderColor', '#D8DCE6');
            //                     // }
            //                 }
            //             }
            //         }
            //     }
            // }
        }

        /** 判断初始支持的状态 */
        function generateDefaultState() {
            dropdownStates.value = dropdownStatesInTotal.value.filter(item =>
                importedOriginalData.value.editorOptions.types.includes(item.id)
            );
        }

        /** 属性编辑器左侧初始状态 */
        function setCurrentState() {
            // 初始状态： find查询入参中存储的默认状态，与当前存储的3种状态：常量、变量、自定义进行比对；
            currentState.value = cloneDeep(dropdownStates.value.find(
                item => (item.id === importedOriginalData.value.propertyValue.type)));
        }
        /**
         *  根据入参，判断属性编辑器初始状态，并存储入参到archiveOfPropertyValue的对应状态中
         */
        function defaultStateValue() {
            setCurrentState();
            // 判断特例：variable传值为undefined,需设定好path等相关参数
            if (currentState.value.id === 'variable' && importedOriginalData.value.propertyValue.value === undefined) {
                importedOriginalData.value.propertyValue = cloneDeep(archiveOfPropertyValue.value.variableValue);
            }
            // 存储初始值：将默认值存储到archive对应的constValue、variableValue、customValue
            archiveOfPropertyValue.value[`${currentState.value.id}Value`] = cloneDeep(importedOriginalData.value.propertyValue);
            // 配置各显示框中的值
            setValue();
        }

        /**
         * 左侧区域类型转换后，同时切换右侧区域
         */
        function getSelectedState(selectedValue) {
            // 确认左侧类型
            currentState.value = cloneDeep(dropdownStates.value.find((item: any) => item.label === selectedValue[0].label));
            // 切换为表达式状态后，显示对应的弹窗组件
            if (currentState.value.id === 'expression') {
                if (archiveOfPropertyValue.value.expressionValue.value) {
                    searchText.value = archiveOfPropertyValue.value.expressionValue.value.value;
                }
                showExpressionComponent.value = true;
            }
            // 如果为自定义类型，进行subscribe
            if (currentState.value.id === 'custom') {
                // TODO 此处的频繁watch是有问题的
                customValueChange();
            }
            updateData();
            ifHighlightBorder();
        }

        /**
         * 常量-枚举-下拉框：用户切换绑定变量
         */
        function getSelectedFormState(value) {
            // 此处value是数组中的key
            constEnumBindValue.value = cloneDeep(value.value);
            importedOriginalData.value.propertyValue.value = cloneDeep(value.value);
            archiveOfPropertyValue.value.constValue.value = cloneDeep(value.value);
            updateData();
        }

        /**
         * 常量-数字变化
         */
        function numberConstValueChanged($event) {
            archiveOfPropertyValue.value.constValue.value = $event;
            updateData();
        }

        /**
         * 常量-字符串-记录input值
         */
        function getConstInputValue(currentValue) {
            archiveOfPropertyValue.value.constValue.value = currentValue;
            updateData();
        }

        /**
         * 变量-点击button-点击产生新的变量值；
         */
        function generateVariables() {
            // 根据新变量的命名规则，进行字符串拼接
            const setUpperCaseNameRight = importedOriginalData.value.propertyName;
            // 首字母大写
            const splicingNameRight = setUpperCaseNameRight[0].toUpperCase() + setUpperCaseNameRight.substr(1);
            const setUpperCaseNameMiddle = importedOriginalData.value.controlName;
            // 根据前缀是否存在决定首字母是否小写
            const newVariablePrefix = importedOriginalData.value.editorOptions.newVariablePrefix || '';
            const splicingNameMiddle = newVariablePrefix ? setUpperCaseNameMiddle[0].toUpperCase() + setUpperCaseNameMiddle.substr(1) :
                setUpperCaseNameMiddle[0].toLowerCase() + setUpperCaseNameMiddle.substr(1);
            newVariable = `${newVariablePrefix}${splicingNameMiddle}${splicingNameRight}`;
            saveVariablesChanges();
            ifHighlightBorder();
        }

        /**
         * 变量-点击button-是否生成变量、绑定、生成展示数组、更新数据
         */
        function saveVariablesChanges() {
            iterateVariableArray();
            if (currentState.value.id === 'variable') {
                bindVariableValue.value = importedOriginalData.value.propertyValue.value.path;
            }
            generateCurrentVariableArray();
            // 当重复前一个绑定值时，不需要更新数据
            if (frontValue.value !== importedOriginalData.value.propertyValue.value.path) {
                updateData();
            }
        }

        /**
         * 变量-点击按钮-遍历数组，确定是否生成新的变量
         */
        function iterateVariableArray() {
            let count = 0;
            importedOriginalData.value.editorOptions.variables?.forEach(item => {
                // 如果遍历一遍后，没有与newVariable相同的值，则表明可以生成新的值；
                if (item.path !== newVariable) {
                    count += 1;
                }
                else {
                    // 若遍历到相同的值，则记录这个值，并将这个值设定为当前绑定值
                    frontValue.value = importedOriginalData.value.propertyValue.value.path;
                    importedOriginalData.value.propertyValue.value = cloneDeep(item);
                    archiveOfPropertyValue.value.variableValue.value = cloneDeep(item);
                }
            });
            // 如果编辑后所有的path都不与newVariable重复，则生成新变量；
            generateNewVariable(count);
        }

        /**
         * 变量-点击按钮-遍历数组-生成新变量
         */
        function generateNewVariable(count: number) {
            // 通过count与原值length的对比，判断是否编辑后所有的path都不与newVariable重复
            if (count === importedOriginalData.value.editorOptions.variables?.length) {
                // 生成新变量值
                importedOriginalData.value.propertyValue.value.category = 'local';
                importedOriginalData.value.propertyValue.value.path = newVariable;
                importedOriginalData.value.propertyValue.value.field = fieldContent;
                importedOriginalData.value.propertyValue.value.fullPath = newVariable;
                importedOriginalData.value.propertyValue.value.newVariableType = importedOriginalData.value.editorOptions.newVariableType;
                propertyEditorValueChange.value.isNewVariable = true;
                // 将新的变量放入变量数组
                importedOriginalData.value.editorOptions.variables.push(cloneDeep(importedOriginalData.value.propertyValue.value));
                archiveOfPropertyValue.value.variableValue.value = cloneDeep(importedOriginalData.value.propertyValue.value);
            }
        }

        /**
         * 变量-下拉框-点击下拉框中的值-改变当前绑定的变量值
         */
        function changeSelectVariable(value) {
            if (!value || !value.selections || value.selections.length < 1) {
                return;
            }
            archiveOfPropertyValue.value.variableValue.value.category = cloneDeep(value.selections[0].category);
            archiveOfPropertyValue.value.variableValue.value.path = cloneDeep(value.selections[0].path);
            archiveOfPropertyValue.value.variableValue.value.fullPath = cloneDeep(value.selections[0].fullPath);
            archiveOfPropertyValue.value.variableValue.value.field = cloneDeep(value.selections[0].field);
            bindVariableValue.value = cloneDeep(value.value);
            updateData();
            ifHighlightBorder();
        }

        /** 变量-清空当前值 */
        function clearVariable() {
            importedOriginalData.value.propertyValue.value.category = '';
            importedOriginalData.value.propertyValue.value.path = '';
            importedOriginalData.value.propertyValue.value.field = '';
            importedOriginalData.value.propertyValue.value.fullPath = '';
            if (currentState.value.id === 'variable') {
                bindVariableValue.value = importedOriginalData.value.propertyValue.value.path;
            }
            updateData();
            ifHighlightBorder();
        }

        /**
       * 变量-生成当前的变量数组
       */
        function generateCurrentVariableArray() {
            displayArray.value = cloneDeep(importedOriginalData.value.editorOptions.variables);
        }

        /**
         * 变量-显示变量下拉框前回调
         */
        const beforeShowVariable = (instance: any) => {
            const getVariablesFunction = importedOriginalData.value.editorOptions.getVariables;
            const result: any = {
                showDialog: true
            };
            if (getVariablesFunction) {
                displayArray.value = getVariablesFunction();
                if (instance) {
                    instance.data = displayArray;
                }
                ifHighlightBorder();
            }
            return Promise.resolve(true);
        };

        /** 状态机-点击下拉框中的值-改变当前绑定的状态机的值 */
        function changeSelectStateMachine(value) {
            const middleArray = {
                id: value[0].id,
                name: value[0].exist,
                exist: value[0].name
            };
            archiveOfPropertyValue.value.stateMachineValue.value = cloneDeep(middleArray);
            exists.value = importedOriginalData.value.propertyValue.value.exist === '是' ? '是' : '否';
            bindStateMachineValue = cloneDeep(middleArray.id);
            updateData();
        }

        /** 状态机-生成当前的状态机数组 */
        function generateCurrentStatesArray() {
            displayArrayState.value = cloneDeep(importedOriginalData.value.editorOptions.stateMachine);
            exists.value = archiveOfPropertyValue.value.stateMachineValue.value.exist === '是' ? '是' : '否';
        }

        /**
       * 自定义-检测自定义值变换并将结果传出
       */
        const textChangeSubject = ref();
        function customValueChange() {
            watch(textChangeSubject, (newValue) => {
                archiveOfPropertyValue.value.customValue.value = newValue;
                updateData();
            });
        }
        function getCustomizeValue(changeValue) {
            textChangeSubject.value = changeValue;
        }

        /** 表达式-弹窗 */
        function showExpression() {
            // 修改中
        }

        /** 数据状态-用户切换绑定变量 */
        function getSelectedDataStates(value) {
            importedOriginalData.value.propertyValue.value = cloneDeep(value);
            archiveOfPropertyValue.value.dataStatesValue.value = cloneDeep(value);
            const result = value.map(element => element.valueField).join(',');
            dataStatesBindValue.value = cloneDeep(result);
            updateData();
        }

        /** 变量 */
        function returnVariableStructure() {
            return (
                <div
                    class='f-page-single-property-editor-component-right-variable' >
                    {/* 如果此组件没有对应的变量名，则展示按钮，双击可添加新变量 */}
                    <FComboList
                        class={[{ 'form-control-select-show': showAddButton.value },
                        { 'form-control-select-hide': !showAddButton.value }]}
                        v-model={bindVariableValue.value}
                        placeholder={''}
                        viewType={'text'}
                        enableClear={true}
                        valueField='path'
                        textField='fullPath'
                        data={displayArray.value}
                        beforeOpen={beforeShowVariable}
                        readonly={readOnly.value}
                        editable={false}
                        // itemTemplate={itemTemp}
                        onValueChange={(e) => changeSelectVariable(e)}
                        onClear={clearVariable}
                    >
                    </FComboList >
                    {/* comboList新增模板 */}
                    {/* < itemTemp let-item let-idx='index'>
                    <span class='f-page-single-property-editor-template'
                    title={'来源:' + displayArray.value[idx].category}>
                    {displayArray.value[idx].fullPath}</span>
                    </> */}
                    {/* 如果此组件有对应的变量名，则展示dropdown组件，可以绑定全局变量及本模块对应变量  */}
                    {showAddButton.value ? (<div class='f-page-single-property-editor-component-variable-button'>
                        <button title="variable-button" class='f-variable-button'
                            onClick={generateVariables} >
                            <span class='f-icon f-icon-add' style={'margin:unset;'}></span>
                        </button >
                    </div >) : ''}
                </div >
            );
        }
        /** 自定义 */
        function returnCustomStructure() {
            return (
                <div class='f-page-single-property-editor-component-right-customize' >
                    <input class='form-control'
                        placeholder={'输入自定义内容'}
                        v-model={archiveOfPropertyValue.value.customValue.value}
                        onChange={(e) => getCustomizeValue((e.target as HTMLInputElement).value)}
                        disabled={readOnly.value} ></input>
                </div >
            );
        }
        /** 表达式 */
        function returnExpressionStructure() {
            return (
                <div
                    class='f-page-single-property-editor-component-right-expression' >
                    <FInputGroup
                        editable={false}
                        enableClear={false}
                        readonly={readOnly.value}
                        groupText={groupIcon.value}
                        v-model={searchText.value}
                        onClickHandle={showExpression}
                    ></FInputGroup>
                </div >
            );
        }
        /** 状态机 */
        function returnStateMachineStructure() {
            return (
                <div class='f-page-single-property-editor-component-right-stateMachine'>
                    {/* 显示文字 */}
                    <div class='f-stateMachine-exist'
                        onClick={changeState}>
                        {exists.value}
                    </div >
                    <div class='f-stateMachine-combo' style='border:unset'>
                        <FComboList
                            v-model={bindStateMachineValue}
                            placeholder={''}
                            viewType={'text'}
                            enableClear={false}
                            valueField='id'
                            textField='name'
                            data={displayArrayState.value}
                            editable={false}
                            readonly={readOnly.value}
                            // itemTemplate={stateMachineTemp}
                            onChange={(e) => { changeSelectStateMachine(e); }}
                        >
                        </FComboList>
                        {/* <ng-template #stateMachineTemp let-item let-idx='index'>
                    <span class='f-page-single-property-editor-template'
                [title]='displayArrayState[idx].id || 'unknow''>{{ displayArrayState[idx].name }}</span>
            </ng - template > */}
                    </div >
                </div >
            );
        }
        /** 数据状态 */
        function returnDataStatesStructure() {
            return (
                <div class='f-page-single-property-editor-component-right-dataStates' >
                    <FComboList
                        v-model={dataStatesBindValue.value}
                        placeholder={''}
                        viewType={'text'}
                        enableClear={false}
                        multiSelect={true}
                        idField='valueField'
                        valueField='valueField'
                        textField='textField'
                        titleField="titleField"
                        data={dataStatesValueArray.value}
                        editable={false}
                        readonly={readOnly.value}
                        onChange={(e) => getSelectedDataStates(e)}
                    >
                    </FComboList >
                </div >
            );
        }
        /** 常量-自定义类型 */
        function returnStringConstStructure() {
            return (
                <input class='form-control'
                    v-model={archiveOfPropertyValue.value.constValue.value}
                    onChange={(e) => { getConstInputValue(e); }}
                    disabled={readOnly.value}
                    placeholder={'输入自定义内容'}
                ></input>
            );
        }
        /** 常量-枚举类型 */
        function returnEnumConstStructure() {
            return (
                <FComboList
                    v-model={constEnumBindValue.value}
                    placeholder={''}
                    viewType={'text'}
                    enableClear={false}
                    valueField='key'
                    textField='value'
                    data={constEnumValueArray.value}
                    editable={false}
                    readonly={readOnly.value}
                    onValueChange={(e) => getSelectedFormState(e)}
                >
                </FComboList>
            );
        }
        /** 常量-数字类型 */
        function returnNumberConstStructure() {
            return (
                <FNumberSpinner
                    nullable={true}
                    readonly={readOnly.value}
                    v-model={numberConstValue.value}
                    onValueChange={(value) => { numberConstValueChanged(value); }}
                    class='f-page-single-property-editor-component-const-number' >
                </FNumberSpinner >
            );
        }
        /** 左侧选择区域:常量、变量、自定义、表达式 */
        function returnLeftController() {
            return (
                <div class='f-page-single-property-editor-component-left-controller'>
                    <FComboList
                        v-model={currentState.value.id}
                        placeholder={''}
                        enableClear={false}
                        valueField='id'
                        textField='label'
                        data={dropdownStates.value}
                        editable={false}
                        readonly={readOnly.value}
                        onChange={(e) => getSelectedState(e)}
                    >
                    </FComboList>
                </div>
            );
        }
        /** 右侧区域组件 */
        function returnRightController() {
            return (
                <div class='f-page-single-property-editor-component-right-controller'>
                    {/* 对应常量部分 */}
                    {currentState.value.id === 'const' ? (
                        <div class='f-page-single-property-editor-component-right-const'>
                            {/* 常量-string类型 */}
                            {importedOriginalData.value.propertyType === 'string' ? returnStringConstStructure() : ''}
                            {/* 常量-枚举类型 */}
                            {importedOriginalData.value.propertyType === 'enum' ? returnEnumConstStructure() : ''}
                            {/* 常量-number类型 */}
                            {importedOriginalData.value.propertyType === 'number' ? returnNumberConstStructure() : ''}
                        </div >
                    ) : ''}
                    {/* 对应变量部分 */}
                    {currentState.value.id === 'variable' ? returnVariableStructure() : ''}
                    {/* 对应自定义部分 */}
                    {currentState.value.id === 'custom' ? returnCustomStructure() : ''}
                    {/* 对应表达式部分  */}
                    {(currentState.value.id === 'expression' && showExpressionComponent.value) ? returnExpressionStructure() : ''}
                    {/* 对应状态机部分 */}
                    {currentState.value.id === 'stateMachine' ? returnStateMachineStructure() : ''}
                    {/* 对应数据状态部分 */}
                    {currentState.value.id === 'dataStates' ? returnDataStatesStructure() : ''}
                </div >
            );
        }

        onMounted(() => {
            // 如果为只读属性
            // if (isReadonly.value[0] === true) {
            //     readOnly.value = true;
            //     showAddButton.value = false;
            // } else {
            //     readOnly.value = false;
            //     showAddButton.value = true;
            // }
            // 判断初始支持的状态
            generateDefaultState();
            defaultStateValue();
            generateCurrentVariableArray();
            generateCurrentStatesArray();

            if (importedOriginalData.value.hasPrefix && importedOriginalData.value.hasPrefix === 2) {
                prefix.value = true;
            }
            if (importedOriginalData.value.controlName !== undefined) {
                showAddButton.value = true;
            }
        });

        // watch(currentState.value, () => {
        // 如果为只读属性
        // if (changes['isReadonly']) {
        //     if (changes['isReadonly']['currentValue'][0] === true) {
        //       readOnly = true;
        //       showAddButton = false;
        //     }
        //     else {
        //       readOnly = false;
        //       showAddButton = true;
        //     }
        //   }
        //     if (importedOriginalData.value.currentValue.propertyValue && (searchText.value === null || searchText.value.length === 0)) {
        //         generateDefaultState();
        //         defaultStateValue();
        //     }
        // });

        return () => {
            return (
                <>
                    <div class='f-page-single-property-editor-component clearfix'>
                        {returnLeftController()}
                        {returnRightController()}
                    </div >
                </>
            );
        };
    }
});
