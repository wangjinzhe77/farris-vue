 
/* eslint-disable no-use-before-define */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { PropertyEditorProps, propertyEditorProps } from '../property-editor.props';
import { SetupContext, ref, Ref } from 'vue';
import { cloneDeep } from 'lodash-es';

export function useUpdate(context: SetupContext, importedOriginalData, constEnumBindValue, constEnumValueArray,
                          archiveOfPropertyValue, displayArrayState, bindStateMachineValue, exists, dataStatesValueArray,
                          searchText, readOnly, currentState) {
    /** 状态机是否有前缀viewModel.stateMachine && */
    const prefix: Ref<boolean> = ref(false);

    /** 出参属性PropertyEditorValueChanged */
    const propertyEditorValueChange: any = ref({
        propertyName: '',
        /** 属性类型：boolean, string, number, enum */
        propertyType: '',
        /** 是否生成新变量*/
        isNewVariable: false,
        /** 属性编辑器的初始值 */
        propertyValue: null,
        /** 状态机是否有前缀viewModel.stateMachine &&  */
        hasPrefix: false,
    });

    /**
    * 常量-枚举：设置枚举对应的绑定值和展示数组
    */
    function setStateOfEnum() {
        if (importedOriginalData.value.propertyValue.type === 'const') {
            importedOriginalData.value.propertyValue.value = cloneDeep(archiveOfPropertyValue.value.constValue.value);
            constEnumBindValue.value = cloneDeep(archiveOfPropertyValue.value.constValue.value);
            constEnumValueArray.value = cloneDeep(importedOriginalData.value.editorOptions.enums);
        }
    }

    /** 状态机-设置对应的绑定值和生成数组 */
    function setStateOfStateMachine() {
        if (importedOriginalData.value.propertyValue.type === 'stateMachine') {
            importedOriginalData.value.propertyValue.value = cloneDeep(archiveOfPropertyValue.value.stateMachineValue.value);
            displayArrayState.value = cloneDeep(importedOriginalData.value.editorOptions.stateMachine);
            if (importedOriginalData.value.propertyValue.value.name) {
                bindStateMachineValue = cloneDeep(importedOriginalData.value.propertyValue.value.id);
            }
            else {
                importedOriginalData.value.propertyValue.value = {
                    id: '',
                    name: '',
                    exist: ''
                };
                bindStateMachineValue = '';
                exists.value = '否';
            }
        }
    };
    /** 数据状态-设置对应的绑定值和生成数组 */
    function setValueOfDataStates() {
        if (importedOriginalData.value.propertyValue.type === 'dataStates') {
            importedOriginalData.value.propertyValue.value = cloneDeep(archiveOfPropertyValue.value.dataStatesValue.value);
            dataStatesValueArray.value = cloneDeep(importedOriginalData.value.editorOptions.dataStates);
        }
    };
    /** 表达式-将表达式绑定值置为空 */
    function setExpressionBindValue() {
        searchText.value = '';
        if (archiveOfPropertyValue.value.expressionValue) {
            archiveOfPropertyValue.value.expressionValue.value = '';
        }
    }
    /** 状态机-切换状态-是/否 */
    function changeState() {
        if (!readOnly.value) {
            exists.value = exists.value === '否' ? '是' : '否';
            importedOriginalData.value.propertyValue.value.exist = exists;
            archiveOfPropertyValue.value.stateMachineValue.value.exist = exists;
            emitOutPutInterfaceData();
        }
    }

    /** 自定义-清空符合状态机的值 */
    function clearCustomBindValue() {
        if (propertyEditorValueChange.value.propertyValue.value) {
            if (propertyEditorValueChange.value.propertyValue.value.includes('viewModel.stateMachine')) {
                archiveOfPropertyValue.value.customValue.value = '';
            }
        }
    }

    /** 将最终状态和值，赋给propertyEditorValueChange */
    function setOutputData() {
        propertyEditorValueChange.value.propertyName = cloneDeep(importedOriginalData.value.propertyName);
        propertyEditorValueChange.value.propertyType = cloneDeep(importedOriginalData.value.propertyType);
        propertyEditorValueChange.value.propertyValue = cloneDeep(importedOriginalData.value.propertyValue);
        propertyEditorValueChange.value.hasPrefix = cloneDeep(prefix);
    }
    /**
         * 出参-传出接口
         */
    function emitOutPutInterfaceData() {
        if (!readOnly.value) {
            setOutputData();
            // 判定需要出参的情况
            switch (propertyEditorValueChange.value.propertyValue.type) {
            case 'const':
                // const时的出参判断条件
                if ((propertyEditorValueChange.value.propertyType === 'enum' && propertyEditorValueChange.value.propertyValue.value !== '')
                        || (propertyEditorValueChange.value.propertyType === 'number' && propertyEditorValueChange.value.propertyValue.value != null)
                        || (propertyEditorValueChange.value.propertyType === 'string')) {
                    // 将表达式绑定值置为空
                    setExpressionBindValue();
                    context.emit('propertyEditorValueChanged', propertyEditorValueChange);
                }
                break;
            case 'variable':
                // variable时的出参判断条件
                if (propertyEditorValueChange.value.propertyValue.value.path !== '') {
                    // 将表达式绑定值置为空
                    setExpressionBindValue();
                    context.emit('propertyEditorValueChanged', propertyEditorValueChange);
                }
                break;
            case 'custom':
                // 将表达式绑定值置为空
                setExpressionBindValue();
                clearCustomBindValue();
                context.emit('propertyEditorValueChanged', propertyEditorValueChange);
                break;
            case 'expression':
                if (searchText.value) {
                    context.emit('propertyEditorValueChanged', propertyEditorValueChange);
                }
                break;
            case 'stateMachine':
                // variable时的出参判断条件
                if (propertyEditorValueChange.value.propertyValue.value.id) {
                    setExpressionBindValue();
                    context.emit('propertyEditorValueChanged', propertyEditorValueChange);
                }
                break;
            case 'dataStates':
                // 将表达式绑定值置为空
                if (propertyEditorValueChange.value.propertyValue.value !== '') {
                    setExpressionBindValue();
                    context.emit('propertyEditorValueChanged', propertyEditorValueChange);
                    break;
                }
            }
        }
    }
    /**
     * 更新数据-左侧区域切换不同模式or各区域值变更后，更新当前数据
     */
    function updateData() {
        // 将archiveOfPropertyValue中对应constValue/variableValue/customValue存储的值放入当前值
        importedOriginalData.value.propertyValue = archiveOfPropertyValue.value[`${currentState.value.id}Value`];
        // 出现枚举时，需要绑定
        setStateOfEnum();
        setStateOfStateMachine();
        setValueOfDataStates();
        emitOutPutInterfaceData();
        // 检测当前是否生成了新的变量
        propertyEditorValueChange.value.isNewVariable = false;
    }

    return {
        updateData,
        changeState,
        prefix,
        propertyEditorValueChange
    };
}
