/**
 * 枚举选项
 */
export interface EnumItem {
    /** 枚举项的值 */
    key: any;
    /** 枚举项的显示名称 */
    value: string;
}

/**
 * 变量选项
 */
export interface VariableItem {
    /** 变量类型，支持状态变量（variable），实体变量（entity） */
    category: string;
    /** 变量绑定路径 */
    path: string;
    /** 变量唯一标识 */
    field: string;
    /** 包含层级结构的变量路径 */
    fullPath: string;
}

/**
 * 属性编辑器的值。
 * 包括：常量、变量、状态机、自定义等类型
 */
export interface PropertyEditorValue {
    /** 值类型：常量（const）, 变量（variable）, 状态机（stateMachine）, 自定义（custom） */
    type: string;
    /** 属性值 */
    value: any;
}

/**
 * 属性编辑器可选值配置
 */
export interface EditorOption {
    /** 支持的编辑器类型 */
    types: string[];
    /** 枚举类型常量编辑器的枚举列表 */
    enums?: EnumItem[];
    /** 变量编辑器的变量列表 */
    variables?: VariableItem[];
    /** 新增变量的类型 */
    newVariableType?: string;
    /** 新增变量名称的前缀 */
    newVariablePrefix?: string;
    /** 获取最新变量列表函数 */
    getVariables?: any;
    /** 获取最新状态机列表函数 */
    getStates?: any;
    /** 状态机编辑器的状态列表 */
    stateMachine?: any;
    /** 数据状态列表 */
    dataStates?: any;
    expressionConfig?: any;
}

export interface ArchiveOfPropertyValue {
    /** 常量值 */
    constValue: PropertyEditorValue;
    /** 变量值 */
    variableValue: PropertyEditorValue;
    /** 状态值 */
    stateValue: PropertyEditorValue | null;
    /** 自定义属性值 */
    customValue: PropertyEditorValue;
    /** 表达式值 */
    expressionValue: PropertyEditorValue;
    /** 状态机值 */
    stateMachineValue: PropertyEditorValue;
    /** 数据状态值 */
    dataStatesValue: PropertyEditorValue;
}

export interface PropertyEditorValueChanged {
    /** 待编辑的属性名 */
    propertyName: string;
    /** 属性类型：boolean, string, number, enum */
    propertyType: string;
    /** 检测是否产生新变量 */
    isNewVariable: boolean;
    /** 属性编辑器的初始值 */
    propertyValue: PropertyEditorValue;
    /** 状态机是否有viewModel.stateMachine前缀 */
    hasPrefix?: boolean;
}

/**
 * 属性编辑器配置
 * 用来初始化属性编辑器
 */
export interface PropertyEditorOptions {
    /** 当前选中的控件对应的英文名称，例userName */
    controlName: string;
    /** 待编辑的属性名 */
    propertyName: string;
    /** 属性类型：boolean, string, number, enum */
    propertyType: string;
    /** 检测是否产生新变量 */
    isNewVariable: boolean;
    /** 属性编辑器的初始值 */
    propertyValue: PropertyEditorValue;
    /** 属性编辑器可选值配置 */
    editorOptions: EditorOption;
    /** 状态机是否有前缀viewModel.stateMachine &&  */
    hasPrefix?: number;
}
