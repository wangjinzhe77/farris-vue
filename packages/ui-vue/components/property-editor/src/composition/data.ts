import { PropertyEditorOptions, ArchiveOfPropertyValue, PropertyEditorValueChanged } from './type';
import {ref} from 'vue';

/** 所有值-内部存档&修改ArchiveOfPropertyValue */
export const archiveOfPropertyValue: any = ref({
    /** 常量值 */
    constValue: {
        /** 值类型：常量（const）, 变量（variable）, 状态机（state）, 自定义（custom） */
        type: 'const',
        /** 属性值 */
        value: '',
    },
    /** 变量值 */
    variableValue: {
        type: 'variable',
        value: {
            category: '',
            path: '',
            field: '',
            fullPath: ''
        }
    },
    /** 状态值 */
    stateValue: null,
    /** 自定义属性值 */
    customValue: {
        type: 'custom',
        value: ''
    },
    expressionValue: {
        type: 'expression',
        /** 用户点击弹窗内确认按钮后置为true */
        value: '',
    },
    stateMachineValue: {
        type: 'stateMachine',
        value: '',
    },
    dataStatesValue: {
        type: 'dataStates',
        value: '',
    }
});

/** 左侧状态数组：常量、变量、自定义 */
export const dropdownStatesInTotal = ref([
    {
        'id': 'const',
        'label': '常量'
    },
    {
        'id': 'variable',
        'label': '变量'
    },
    {
        'id': 'custom',
        'label': '自定义'
    },
    {
        'id': 'stateMachine',
        'label': '状态机'
    },
    {
        'id': 'expression',
        'label': '表达式'
    },
    {
        'id': 'dataStates',
        'label': '数据状态'
    }
]);

/** 枚举类型-默认显示const状态 */
export const option1: PropertyEditorOptions = {
    controlName: 'userName',
    propertyName: 'readonly',
    propertyType: 'enum',
    isNewVariable: false,
    propertyValue: {
        // type: 'dataStates',
        // value: undefined
        type:'const',
        value:'true'
    },
    editorOptions: {
        types: ['const', 'variable', 'custom', 'expression', 'stateMachine', 'dataStates'],
        enums: [{ key: 'true', value: '是' }, { key: 'false', value: '否' }],
        variables: [
            {
                category: 'remote',
                path: 'root-component.isUserNameReadonly',
                field: '49311371-fd9f-4019-8611-dce4dcae97fe',
                fullPath: 'isUserNameReadonly'
            },
            {
                category: 'remote',
                path: 'isSystem',
                field: '49311371-fd9f-4019-8611-dce4dcae99fe',
                fullPath: 'isSystem'
            }
        ],
        expressionConfig: {
            editor: 'ExpressionEditorComponent',
            beforeOpenModal: () => {
                return {
                    editorParams: {
                        modalTitle: '只读编辑器',
                        fieldId: 'ca30ad64-c9f6-4660-ad66-38073f6bd0b5',
                        viewModelId: 'basic-form-viewmodel',
                        expType: 'readonly'
                    },
                    value: '{\'expr\':\'DefaultFunction.Length(\\\'aaa\\\')>2\',\'sexpr\':\'\'}'
                };
            },
            exprValue: {
                type: 'expression',
                value: {
                    type: 'expression',
                    parameters: 'ca30ad64-c9f6',
                    value: '{\'expr\':\'DefaultFunction.Length(\\\'aaa\\\')>2\',\'sexpr\':\'\'}'
                }
            },
        },
        stateMachine: [
            {
                id: 'canRemove',
                name: '删除',
                exist: '非'
            },
            {
                id: 'canCancelApprove',
                name: '取消提交审批',
                exist: '是'
            },
            {
                id: 'canApprove',
                name: '提交审批',
                exist: '是'
            },
            {
                id: 'editable',
                name: '可编辑',
                exist: '是'
            },
            {
                id: 'canEdit',
                name: '编辑',
                exist: '是'
            },
            {
                id: 'canRemoveDetail',
                name: '删除明细',
                exist: '是'
            },
            {
                id: 'canAdd',
                name: '新增',
                exist: '是'
            },
            {
                id: 'canSave',
                name: '保存',
                exist: '是'
            },
            {
                id: 'canCancel',
                name: '取消',
                exist: '是'
            },
            {
                id: 'canAddDetail',
                name: '新增明细',
                exist: '是'
            }
        ],
        dataStates: [
            {
                textField: '制单',
                titleField: '制单',
                valueField: 'Billing'
            },
            {
                textField: '提交审批',
                titleField: '提交审批',
                valueField: 'SubmitApproval'
            },
            {
                textField: '审批通过',
                titleField: '审批通过',
                valueField: 'Approved'
            },
            {
                textField: '审批不通过',
                titleField: '审批不通过',
                valueField: 'ApprovalNotPassed'
            }
        ]
    },
    /** 状态机是否有前缀viewModel.stateMachine &&  */
    hasPrefix: 2
};

/** 枚举类型-默认显示variable状态 */
const option2: PropertyEditorOptions = {
    controlName: 'UserAge',
    propertyName: 'readonly',
    propertyType: 'enum',
    isNewVariable: false,
    propertyValue: {
        type: 'variable',
        value:
        {
            category: 'remote',
            path: 'root-component.isUserAgeReadonly',
            field: '49311371-fd9f-4019-8611-dce4dcae97fe',
            fullPath: 'isUserAgeReadonly'
        },
    },
    editorOptions: {
        types: ['variable'],
        enums: [{ key: 'true', value: '是' }, { key: 'false', value: '否' }],
        variables: [
            {
                category: 'remote',
                path: 'root-component.isUserAgeReadonly',
                field: '49311371-fd9f-4019-8611-dce4dcae97fe',
                fullPath: 'isUserAgeReadonly'
            },
        ]
    }
};

/** 枚举类型-默认显示custom状态 */
const option3: PropertyEditorOptions = {
    controlName: 'UserAddress',
    propertyName: 'readonly',
    propertyType: 'enum',
    isNewVariable: false,
    propertyValue: {
        type: 'custom',
        value: '!viewModel.stateMachine["editable"]'
    },
    editorOptions: {
        types: ['const', 'variable', 'custom'],
        enums: [{ key: 'true', value: '是' }, { key: 'false', value: '否' }],
        variables: [
            {
                category: 'local',
                path: 'isSystem',
                field: '49311371-fd9f-4019-8611-dce4dcae97fe',
                fullPath: 'isSystem'
            }
        ]
    }
};

/** string类型- 默认显示const状态 */
const option4: PropertyEditorOptions = {
    controlName: 'UserAddress',
    propertyName: 'readonly',
    propertyType: 'string',
    isNewVariable: false,
    propertyValue: {
        type: 'const',
        value: '!viewmodel.statemachine["editable"]'
    },
    editorOptions: {
        types: ['const', 'variable', 'custom'],
        enums: [{ key: 'true', value: '是' }, { key: 'false', value: '否' }],
        variables:
            [{
                // 标记来自表单变量或组件变量
                category: 'local',
                path: 'isSystem',
                field: '49311371-fd9f-4019-8611-dce4dcae97fe',
                fullPath: 'isSystem'
            }
            ]
    }
};

/** number类型- 默认显示custom状态 */
const option5: PropertyEditorOptions = {
    controlName: 'UserAddress',
    propertyName: 'readonly',
    propertyType: 'number',
    isNewVariable: false,
    propertyValue: {
        type: 'custom',
        value: '!viewModel.stateMachine["editable"]'
    },
    editorOptions: {
        types: ['const', 'variable', 'custom'],
        enums: [{ key: 'true', value: '是' }, { key: 'false', value: '否' }],
        variables:
            [{
                category: 'local',
                path: 'isSystem',
                field: '49311371-fd9f-4019-8611-dce4dcae97fe',
                fullPath: 'isSystem'
            }
            ],

    }
};

const archiveOfThisPropertyValue: ArchiveOfPropertyValue = {
    /** 常量值 */
    constValue: {
        /** 值类型：常量（const）, 变量（variable）, 状态机（state）, 自定义（custom） */
        type: 'const',
        /** 属性值 */
        value: 'true',
    },
    /** 变量值 */
    variableValue: {
        type: 'variable',
        value: {
            category: 'local',
            path: 'isUserNameReadonly',
            field: '49311371-fd9f-4019-8611-dce4dcae97fe',
            fullPath: 'isUserNameReadonly'
        }
    },
    /** 状态值 */
    stateValue: null,
    /** 自定义属性值 */
    customValue: {
        type: 'custom',
        value: '!viewModel.stateMachine["editable"]'
    },
    /** 表达式属性值 */
    expressionValue: {
        type: 'expression',
        value: false,
    },
    /** 状态机属性值 */
    stateMachineValue: {
        type: 'stateMachine',
        value: ''
    },
    /** 数据状态属性值 */
    dataStatesValue: {
        type: 'dataStates',
        value: ''
    }
};

const propertyValueChangedEventArgs1: PropertyEditorValueChanged = {
    /** 待编辑的属性名 */
    propertyName: 'readonly',
    /** 属性类型：enum, string, number, enum */
    propertyType: 'enum',
    isNewVariable: false,
    propertyValue: archiveOfThisPropertyValue.constValue
};

const propertyValueChangedEventArgs2: PropertyEditorValueChanged = {
    /** 待编辑的属性名 */
    propertyName: 'readonly',
    /** 属性类型：enum, string, number, enum */
    propertyType: 'enum',
    isNewVariable: false,
    propertyValue: archiveOfThisPropertyValue.variableValue
};

const propertyValueChangedEventArgs3: PropertyEditorValueChanged = {
    /** 待编辑的属性名 */
    propertyName: 'readonly',
    /** 属性类型：enum, string, number, enum */
    propertyType: 'enum',
    isNewVariable: false,
    propertyValue: archiveOfThisPropertyValue.customValue
};
