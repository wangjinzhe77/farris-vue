/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref, ref } from 'vue';
import { cloneDeep } from 'lodash-es';

export function useInitializedValue(
    importedOriginalData, constEnumValueArray,
    currentState, bindStateMachineValue, exists,
    searchText, showExpressionComponent
) {
    /** 常量-number类型-number-数字输入框 */
    const numberConstValue = ref(0);
    /** 数据状态-用户设定绑定值 */
    const dataStatesBindValue = ref('');

    /** 数据状态-存储所有值的数组 */
    const dataStatesValueArray = ref([]);

    /** 常量-enum类型-用户设定绑定值 */
    const constEnumBindValue = ref('');

    /** 变量-用户设定的绑定值 */
    const bindVariableValue = ref('');

    /**
     * 常量-枚举：初始设置枚举对应的绑定值和展示数组
     */
    function setEnumConstValue() {
        // 存储 常量-enum 的变量值数组
        if (importedOriginalData.value.propertyType === 'enum' && importedOriginalData.value.propertyValue.type === 'const') {
            // 当默认显示常量-enum类型时，使用constEnumBindValue来存储comboList中对应的数据,此处value是数组中的key
            constEnumBindValue.value = cloneDeep(importedOriginalData.value.propertyValue.value);
            constEnumValueArray.value = cloneDeep(importedOriginalData.value.editorOptions.enums);
        }
    };

    /**
     * 常量-数字
     */
    function setNumberConstValue() {
        if (importedOriginalData.value.propertyType === 'number') {
            numberConstValue.value = importedOriginalData.value.propertyValue.value;
        }
    }

    /** 状态机-设置初始绑定值 */
    function setBindStateMachineValue() {
        if (currentState.value.id === 'stateMachine') {
            bindStateMachineValue = cloneDeep(importedOriginalData.value.propertyValue.value.id);
            if (bindStateMachineValue) {
                exists.value = cloneDeep(importedOriginalData.value.propertyValue.value.exist);
            } else {
                exists.value = '否';
            }
        }
    }

    /** 数据状态-设置初始绑定值 */
    function setBindDataStatesValue() {
        if (currentState.value.id === 'dataStates') {
            // 数据状态数组
            dataStatesValueArray.value = cloneDeep(importedOriginalData.value.editorOptions.dataStates);
            // 数据状态根据propertyValue设置初始值
            if (importedOriginalData.value.propertyValue.value) {
                const string = importedOriginalData.value.propertyValue.value;
                const result = dataStatesValueArray.value
                    .filter((element: any) => string.includes(element.value))
                    .map((element: any) => element.value)
                    .join(',');
                dataStatesBindValue.value = cloneDeep(result);
            }
        }
    }
    /**
     * 变量-点击按钮-设置当前绑定的变量值
     */
    function setBindVariableValue() {
        if (currentState.value.id === 'variable') {
            bindVariableValue.value = importedOriginalData.value.propertyValue.value.path;
        }
    }
    function setBindExpressionValue() {
        // 表达式-弹窗组件
        if (currentState.value.id === 'expression') {
            showExpressionComponent.value = true;
            // 弹窗默认显示值
            const { handleExpressionValue } = importedOriginalData.value.editorOptions.expressionConfig;
            if (handleExpressionValue) {
                searchText.value = handleExpressionValue().value.value;
            } else {
                searchText.value = importedOriginalData.value.editorOptions.expressionConfig.expressionValue.value.value;
            }
        }
    }
    function setBindCustomValue() {
        // 如果为自定义类型，进行subscribe
        if (currentState.value.id === 'custom') {
            // customValueChange();
        }
    }
    /** 配置各显示框中的值 */
    function setValue() {
        // 常量-enum类型-初始显示值
        setEnumConstValue();
        // 常量-number类型-初始显示值
        setNumberConstValue();
        // 变量-下拉框-初始显示值
        setBindVariableValue();
        // 状态机-下拉框-初始显示值
        setBindStateMachineValue();
        // 数据状态-下拉框-初始显示值
        setBindDataStatesValue();
        // 表达式-帮助-初始显示值
        setBindExpressionValue();
        // 自定义-输入框-初始显示值
        setBindCustomValue();
    }

    return {
        numberConstValue,
        bindVariableValue,
        dataStatesBindValue,
        dataStatesValueArray,
        constEnumBindValue,
        setValue
    };
}
