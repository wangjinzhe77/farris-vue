 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App, Plugin } from 'vue';
import Radio from './src/components/radio.component';
import RadioGroup from './src/radio-group.component';
import { propsResolver } from './src/radio-group.props';
import FRadioGroupDesign from './src/designer/radio-group.design.component';
import FItemCollectionEditor from './src/designer/item-collection-editor.component';
import {itemCollectionEditorPropsResolver} from './src/designer/item-collection-editor.props';

export * from './src/radio-group.props';
export * from './src/components/radio.props';

RadioGroup.install = (app: App) => {
    app.component(RadioGroup.name as string, RadioGroup);
    app.component(Radio.name as string, Radio);
};
RadioGroup.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap['radio-group'] = RadioGroup;
    propsResolverMap['radio-group'] = propsResolver;
    componentMap['item-collection-editor'] = FItemCollectionEditor;
    propsResolverMap['item-collection-editor'] = itemCollectionEditorPropsResolver;
};
RadioGroup.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['radio-group'] = FRadioGroupDesign;
    propsResolverMap['radio-group'] = propsResolver;
};

export { RadioGroup,Radio };
export default RadioGroup as typeof RadioGroup & Plugin;
