
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import { itemCollectionEditorSchemaResolver } from '../schema/schema-resolver';
import { itemCollectionEditorSchemaMapper } from '../schema/schema-mapper';
import itemCollectionEditorSchema from '../schema/item-collection-editor.schema.json';
import { DataGridColumn } from '@farris/ui-vue/components/data-grid';
import { createPropsResolver } from '@farris/ui-vue/components/dynamic-resolver';

export const itemCollectionEditorProps = {
    /**
    * 必填的列
    */
    requiredFields: { type: Array<string>, default: [] },
    /**
     * 值唯一的列
     */
    uniqueFields: { type: Array<string>, default: [] },
    dynamicMappingKeys: { type: Boolean, default: false },
    /**
     * 列配置
     */
    columns: { type: Array<DataGridColumn>, default: [] },
    /**
     * 模态框标题
     */
    modalTitle: { type: String, default: '项编辑器' },

    /**
     * 数据是否可以置空
     */
    canEmpty: { type: Boolean, default: true },
    /**
     * 动态映射键值时，枚举值的key
     */
    valueField: { type: String, default: 'value' },
    /**
     * 动态映射键值时，枚举名称的key
     */
    nameField: { type: String, default: 'name' },
    /**
     * 目前假定idField和valueField一致
     */
    idField: { type: String, default: 'value' },
    /**
     * 是否进入只读状态
     */
    readonly: { type: Boolean, default: false },
    /**
     * 组件值
     */
    modelValue: { type: Array<any>, default: [] },
    /**
    * 列表只有一列时，将结果集转化为字符串数组
    */
    // isSimpleArray: { type: Boolean, default: false }


} as Record<string, any>;

export type ItemCollectionEditorProps = ExtractPropTypes<typeof itemCollectionEditorProps>;

export const itemCollectionEditorPropsResolver = createPropsResolver<ItemCollectionEditorProps>(itemCollectionEditorProps, itemCollectionEditorSchema, itemCollectionEditorSchemaMapper, itemCollectionEditorSchemaResolver);

export const itemCollectionEditorInnerProps = {
    /**
    * 必填的列
    */
    requiredFields: { type: Array<string>, default: [] },
    /**
     * 值唯一的列
     */
    uniqueFields: { type: Array<string>, default: [] },
    dynamicMappingKeys: { type: Boolean, default: false },
    /**
     * 列配置
     */
    columns: { type: Array<DataGridColumn>, default: [] },
    /**
     * 数据是否可以置空
     */
    canEmpty: { type: Boolean, default: true },
    /**
     * 动态映射键值时，枚举值的key
     */
    valueField: { type: String, default: 'value' },
    /**
     * 动态映射键值时，枚举名称的key
     */
    nameField: { type: String, default: 'value' },
    /**
     * 组件值
     */
    datas: { type: Array<any>, default: [] },
    /**
     * 是否进入只读状态
     */
    readonly: { type: Boolean, default: false }
} as Record<string, any>;

export type ItemCollectionEditorInnerProps = ExtractPropTypes<typeof itemCollectionEditorProps>;
