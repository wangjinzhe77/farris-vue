/**
* Copyright (c) 2020 - present, Inspur Genersoft selectItem., Ltd.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

import { defineComponent, onMounted, ref} from 'vue';
import { itemCollectionEditorInnerProps, ItemCollectionEditorInnerProps } from './item-collection-editor.props';
import FDataGrid from '@farris/ui-vue/components/data-grid';
import { useGuid } from '@farris/ui-vue/components/common';
import { FNotifyService } from '@farris/ui-vue/components/notify';
/**
 * 先假定 valueField和idField是同一个字段
 * ToDo 支持不同字段
 */
export default defineComponent({
    name: 'FItemCollectionEditorInner',
    props: itemCollectionEditorInnerProps,
    emits: ['change'] as (string[] & ThisType<void>),
    setup(props: ItemCollectionEditorInnerProps, context) {
        const { guid } = useGuid();
        const notifyService: any = new FNotifyService();
        const valueField = ref(props.valueField);
        const nameField = ref(props.nameField);
        // 指定单元格编辑
        const editOption = {
            editMode: 'cell'
        };
        // 列编辑属性
        const columnOption={
            fitColumns:true
        };
        // 指定对象实例，用来处理数据更新
        const gridComponentInstance = ref<any>();
        // 指定列数据
        const compactColumns = props.columns;
        let datagridDatas= props.datas;

        /**
         * 创建新数据
         * @returns 
         */
        function createNewData() {
            const data = {};
            compactColumns.forEach(column => {
                if (column.editor && (column.editor.type === 'checkbox' || column.editor.type === 'switch')) {
                    data[column.field] = false;
                } else {
                    data[column.field] = '';
                }
            });
            return data;

        }

        /**
         * 校验字段是否未空
         * @returns 
         */
        function checkMappingKeys() {
            if (props.dynamicMappingKeys) {
                const msg = '请先填写XXX';
                const enumValueFieldLabel = !valueField.value.trim() ? '枚举值字段' : !nameField.value.trim() ? '枚举名称字段' : '';
                notifyService.warning({position: 'top-center', message: msg.replace('XXX', enumValueFieldLabel) });
                return false;
            }
            return true;
        }
        /**
        * 保存前检查
        */
        function checkBeforeSave(data: any[]): boolean {
            const duplicationNotAllowed = '不允许重复。';
            // 校验动态key值
            if (!checkMappingKeys()) {
                return false;
            }
            // 1、空数组
            if (!data || data.length === 0) {
                if (!props.canEmpty) {
                    notifyService.warning({position: 'top-center', message: '请添加值' });
                    return false;
                }
                return true;
            }
            // 2、非空，则校验每个的键值是否为空；（区分枚举值为布尔型的场景；排除枚举值为整数0的场景）
            const requiredFields = props.requiredFields || [];
            const emptyNotAllowed = '不允许为空';
            for (const item of data) {
                for (const itemKey of Object.keys(item)) {
                    const column = compactColumns.find(col => col.field === itemKey);
                    if (column && column.editor && (column.editor.type === 'checkbox' || column.editor.type === 'switch')) {
                        if (requiredFields.includes(itemKey) && (item[itemKey] === null || item[itemKey] === undefined)) {
                            notifyService.warning({position: 'top-center', message: column.title + emptyNotAllowed });
                            return false;
                        }
                    } else {
                        if (requiredFields.includes(itemKey) &&
                            (item[itemKey] === undefined || item[itemKey] === '' || item[itemKey] === null)) {
                            notifyService.warning({ position: 'top-center', message: column.title + emptyNotAllowed });;
                            return false;
                        }
                    }
                }
            }
            // 3、不允许重复
            const uniqueFields = props.uniqueFields || [];
            for (const uniqueField of uniqueFields) {
                const fieldValues = data.map(item => item[uniqueField]);
                const keySet = new Set(fieldValues);
                const exclusiveKeys = Array.from(keySet);
                if (fieldValues.length !== exclusiveKeys.length) {
                    const column = compactColumns.find(col => col.field === uniqueField);
                    notifyService.warning({position: 'top-center', message: column.title + duplicationNotAllowed });
                    return false;
                }
            }
            return true;
        }
        function addItem() {
            if (props.readonly) {
                return;
            }
            if (!checkMappingKeys()) {
                return;
            }
            const newData = createNewData();
            const hId = guid();
            datagridDatas.push({
                hId,
                ...newData
            });
            gridComponentInstance.value.updateDataSource(datagridDatas);
            gridComponentInstance.value.selectItemById(hId);
        }
        /**
         * 设置表格选中项
         */        
        function setGridSelectedItem() {
            if (datagridDatas && datagridDatas.length > 0) {
                gridComponentInstance.value.selectItemById(datagridDatas[0].hId);
            }
        }
        function removeItem() {
            const otherDatas = gridComponentInstance.value.getVisibleData().filter(item => !item.checked).map(item => item.raw);
            datagridDatas = [...otherDatas];
            gridComponentInstance.value.updateDataSource(datagridDatas);
            setGridSelectedItem();
        }
        /**
         * 按照指定的字段名转换value和name
         * @param latestData 
         * @returns 
         */
        function convertDynamicMappingkeys(latestData: any[]) {
            if (!latestData || latestData.length === 0) {
                return latestData;
            }
            const dynamicDataList = [] as any;
            latestData.forEach(data => {
                const { value, name, ...other } = data;
                const dynamicData = Object.assign({}, other);
                dynamicData[valueField.value.trim()] = value;
                dynamicData[nameField.value.trim()] = name;
                dynamicDataList.push(dynamicData);
            });
            return dynamicDataList;
        }

        function clickConfirm() {
            // 获取最新数组
            gridComponentInstance.value.endEditCell();
            let latestData = [] as any;
            gridComponentInstance.value.getVisibleData().forEach((data: any) => {
                const { hId, ...others } = data.raw;
                const itemData={};
                for (const key in others) {
                    if (!key.startsWith('_')) {
                        itemData[key]= others[key];
                    }
                }
                latestData.push(itemData);
            });
            // 校验
            if (!checkBeforeSave(latestData)) {
                return false;
            }
            
            if (!props.dynamicMappingKeys) {
                context.emit('change', { value: latestData });
            } else {
                latestData = convertDynamicMappingkeys(latestData);
                context.emit('change', {
                    value: latestData,
                    parameters: {
                        valueField: valueField.value.trim(),
                        nameField: nameField.value.trim()
                    }
                });
            }
            return true;
        }

        context.expose({ clickConfirm });

        onMounted(() => {
            setGridSelectedItem();
        });

        return () => (
            <div class="f-utils-fill-flex-column">
                {!props.readonly && <div class="mb-1 py-1 ml-1">
                    <button class="btn f-rt-btn f-btn-ml btn-primary" onClick={() => addItem()}>新增</button>
                    <button class="btn f-rt-btn f-btn-ml btn-secondary" onClick={() => removeItem()}>删除</button>
                </div>}
                <div class="f-utils-fill border" style="margin:0 8px;border-radius:12px;">
                    <FDataGrid ref={gridComponentInstance}
                        showBorder={true}
                        idField={'hId'}
                        columns={compactColumns}
                        data={datagridDatas}
                        editable={!props.readonly}
                        edit-option={editOption}
                        columnOption={columnOption}
                        fit={true}
                    ></FDataGrid>
                </div>
            </div >
        );
    }
});
