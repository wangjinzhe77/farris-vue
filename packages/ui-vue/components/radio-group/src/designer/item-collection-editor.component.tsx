/**
* Copyright (c) 2020 - present, Inspur Genersoft selectItem., Ltd.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

import { defineComponent, onMounted, ref, watch } from 'vue';
import FButtonEdit from '../../../button-edit/src/button-edit.component';
import { itemCollectionEditorProps, ItemCollectionEditorProps } from './item-collection-editor.props';
import { DataColumn } from '@farris/ui-vue/components/data-view';
import { useGuid } from '@farris/ui-vue/components/common';
import { cloneDeep } from 'lodash-es';
import FItemCollectionEditorInner from './item-collection-editor-inner.component';
/**
 * 先假定 valueField和idField是同一个字段
 * ToDo 支持不同字段
 */
export default defineComponent({
    name: 'FItemCollectionEditor',
    props: itemCollectionEditorProps,
    components: {},
    emits: ['change'] as (string[] & ThisType<void>),
    setup(props: ItemCollectionEditorProps, context) {
        const { guid } = useGuid();
        const textValue = ref(`共 ${(props.modelValue||[]).length} 项`);
        const valueField = ref(props.valueField);
        const nameField = ref(props.nameField);
        // 指定对象实例，用来处理数据更新
        const innerInstance = ref<any>();
        /**
       * 格式化初始数据
       * @returns 
       */
        function formateInitData() {
            let datagridDatas = [] as any;
            if (!props.dynamicMappingKeys) {
                datagridDatas = cloneDeep(props.modelValue||[]);
                datagridDatas.forEach((data: any) => {
                    data.hId = guid();
                });
            } else {
                // 动态指定枚举值和枚举名称key
                datagridDatas = [] as any;
                valueField.value = valueField.value.trim();
                nameField.value = nameField.value.trim();
                (props.modelValue||[]).forEach(item => {
                    if (valueField.value && nameField.value) {
                        const { [valueField.value]: value, [nameField.value]: name, ...other } = item;
                        const data = other || {};
                        data.value = value;
                        data.name = name;
                        data.hId = guid();
                        datagridDatas.push(data);
                    }
                });
            }
            return datagridDatas;
        }
        /**
       * 获取列
       * @returns 
       */
        function updateColumns() {
            let compactColumns=[] as any;
            if (props.columns && props.columns.length > 0) {
                compactColumns = props.columns;
            } else {
                compactColumns = [
                    { field: props.valueField, title: '值', dataType: 'string' },
                    { field: props.nameField, title: '名称', dataType: 'string' },
                ] as DataColumn[];
            }
            return compactColumns;
            // gridComponentInstance.value?.updateColumns(compactColumns);
        }
        function onBeforeOpen() {
          //  updateColumns();
           // formateInitData();
        }
        const modalOptions = {
            fitContent: false,
            width: 900,
            height: 600,
            title: '项编辑器',
            buttons: [
                {
                    name: 'cancel',
                    text: '取消',
                    class: 'btn btn-secondary',
                    handle: ($event: MouseEvent) => {
                        return true;
                    }
                },
                {
                    name: 'accept',
                    text: '确定',
                    class: 'btn btn-primary',
                    handle: ($event: MouseEvent) => {
                        const state=innerInstance.value.clickConfirm();
                        if (!state) {
                            return false;
                        }
                        return true;
                    }
                }
            ]
        };
        function changeHandler(eventData) {
            textValue.value = `共 ${eventData.value.length} 项`;
            context.emit('change', eventData);
        }
        /**
         * 解决有时组件不重新初始化问题
         */
        watch(()=>props.modelValue,(newValue)=>{
            textValue.value = `共 ${(props.modelValue||[]).length} 项`;
        });

        onMounted(() => {

        });

        return () => (
            <FButtonEdit
                button-behavior={'Modal'}
                modal-options={modalOptions}
                v-model={textValue.value}
                editable={false}
                beforeOpen={onBeforeOpen}
            >
                <div class="f-utils-absolute-all f-utils-flex-column">
                    {props.dynamicMappingKeys && <div class="farris-form-controls-inline p-2 f-form-layout farris-form f-form-lable-auto">
                        <div class="farris-group-wrap mr-3">
                            <div class="form-group farris-form-group">
                                <label class="col-form-label">
                                    <span class="farris-label-info text-danger">*</span>
                                    <span class="farris-label-text">枚举值字段</span>
                                </label>
                                <div class="farris-input-wrap">
                                    <input type="input" class="form-control" v-model={valueField.value} readonly={props.readonly} />
                                </div>
                            </div>
                        </div>
                        <div class="farris-group-wrap">
                            <div class="form-group farris-form-group">
                                <label class="col-form-label">
                                    <span class="farris-label-info text-danger">*</span>
                                    <span class="farris-label-text">枚举名称字段</span>
                                </label>
                                <div class="farris-input-wrap">
                                    <input type="input" class="form-control" v-model={nameField.value} readonly={props.readonly} />
                                </div>
                            </div>
                        </div>
                    </div>}
                    <FItemCollectionEditorInner
                        ref={innerInstance}
                        valueField={valueField.value}
                        nameField={nameField.value}
                        requiredFields={props.requiredFields}
                        uniqueFields={props.uniqueFields}
                        dynamicMappingKeys={props.dynamicMappingKeys}
                        readonly={props.readonly}
                        canEmpty={props.canEmpty}
                        datas={formateInitData()}
                        columns={updateColumns()}
                        onChange={(eventData) => changeHandler(eventData)}
                    ></FItemCollectionEditorInner>
                </div >
            </FButtonEdit >
        );
    }
});
