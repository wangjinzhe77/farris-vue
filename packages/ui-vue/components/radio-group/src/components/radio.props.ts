import { ExtractPropTypes } from "vue";

export const radioProps = {
    value: { type: String, default: '' },
    /** 值 */
    modelValue: { type: String, default: '' },
    /** name值 */
    name: { type: String, default: '' },
    /** 标签名 */
    label: { type: String, default: '' },
    /** 标识 */
    id: { type: String, default: '' }
} as Record<string, any>;

export type RadioProps = ExtractPropTypes<typeof radioProps>;
