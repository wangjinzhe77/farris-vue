import { InjectionKey, Ref } from "vue";

export const RADIOGROUP_CONTEXT: InjectionKey<{ radios: Ref<Array<any>>;[key: string]: any }> | undefined = Symbol('radioGroupContext');
