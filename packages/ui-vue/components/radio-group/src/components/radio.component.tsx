/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, inject, onMounted } from 'vue';
import type { InjectionKey, Ref, SetupContext } from 'vue';
import { RadioProps, radioProps } from './radio.props';
import { RADIOGROUP_CONTEXT } from './symbol';
import { useCheck } from '@farris/ui-vue/components/common';
import './radio.css';

export default defineComponent({
    name: 'FRadio',
    props: radioProps,
    emits: ['update:value'] as (string[] & ThisType<void>) | undefined,
    setup(props: RadioProps, context: SetupContext) {
        const radioGroupContext = inject(RADIOGROUP_CONTEXT as InjectionKey<{ radios: Ref<Array<any>>;[key: string]: any }>, null);
        //
        const { buttonClass, checked, disabled, name, shouldRenderButton, shouldRenderNative, onClickRadio } =
            useCheck(props, context, radioGroupContext?.parentProps, radioGroupContext?.parentContext);
        onMounted(() => {
        });
        return () => {
            return <>
                {
                    shouldRenderButton.value && <div
                        class={buttonClass.value}
                        style="border-radius:0;border: 1px solid #E8EBF2;"
                        onClick={onClickRadio}
                    >
                        {context.slots.default?.()}
                    </div>
                }
                {
                    shouldRenderNative.value && <div class="custom-control custom-radio"
                        onClick={onClickRadio}
                    >
                        <input
                            type="radio"
                            class="custom-control-input"
                            name={name.value}
                            id={props.id}
                            value={props.value}
                            checked={checked.value}
                            disabled={disabled.value}
                        />
                        <div class="custom-control-label">
                            {context.slots.default?.()}
                        </div>
                    </div>
                }

            </>;
        };
    }
});
