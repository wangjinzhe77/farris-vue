/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeRadio, Radio } from './types';
import { computed, Ref, SetupContext } from 'vue';
import { RadioGroupProps } from '../radio-group.props';

export function changeRadio(props: RadioGroupProps, context: SetupContext, modelValue: Ref<string>): ChangeRadio {
    const canChangeRadioButton = computed(() => !props.disabled);
    const enumData = computed(() => props.data || props.enumData || props.options || []);

    function getValue(item: Radio): any {
        return item[props.valueField];
    }

    function getText(item: Radio): any {
        return item[props.textField];
    }

    function onClickRadio(item: Radio, $event: Event) {
        if (canChangeRadioButton.value) {
            const newValue = getValue(item);
            if (modelValue.value !== newValue) {
                modelValue.value = newValue;

                context.emit('changeValue', newValue);

                context.emit('update:modelValue', newValue);
            }
        }
        $event.stopPropagation();
    }

    return {
        enumData,
        getValue,
        getText,
        onClickRadio
    };
}
