 
import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import externalContainerSchema from './schema/external-container.schema.json';
import propertyConfig from './property-config/external-contaner.property-config.json';

export const externalContainerProps = {
    customClass: { type: String, default: '' },
    componentType: { type: String, default: '' },
    url: { type: String, default: '' },
    schema: { type: Object },
    useIsolateJs: { type: Boolean, default: false }
} as Record<string, any>;

export type ExteranlContainerPropsType = ExtractPropTypes<typeof externalContainerProps>;

export const propsResolver = createPropsResolver<ExteranlContainerPropsType>(externalContainerProps, externalContainerSchema, schemaMapper, schemaResolver, propertyConfig);
