import { App, defineComponent } from 'vue';
import { FModalService } from '../../../modal';
import { FListView } from '../../../list-view';

import './import-external-schema.css';

export default defineComponent({
    name: 'FImportExteranlSchema',
    emits: ['close', 'submit'],
    setup(props, context) {

        const data = [
            { name: '导航列表' },
            { name: '树列表' }
        ];

        function acceptCallback(app: App) {
            context.emit('submit', 'nav_list');
            app && app.unmount();
        }

        function rejectCallback(app: App) {
            app && app.unmount();
        }

        function renderSchemaList(app: App) {
            return <div>
                <FListView data={data}> </FListView>
                <div class='f-layout-editor-bottom' >
                    <div class='f-layout-editor-button'>
                        <div class='f-layout-editor-cancel-btn' onClick={() => rejectCallback(app)}>
                            <span class='f-layout-editor-text-cancel'>取消</span>
                        </div>
                        <div class='f-layout-editor-sure-btn' onClick={() => acceptCallback(app)} >
                            <span class='f-layout-editor-text-sure'>确认</span>
                        </div>
                    </div>
                </div>
            </div>;
        }

        function onClick() {
            const title = 'Settings';
            const width = 800;
            const showButtons = false;
            const showHeader = false;
            const showFloatingClose = true;
            FModalService.show({ title, width, showButtons, showFloatingClose, showHeader, render: renderSchemaList });
        }

        return () => (
            <div class="f-import-exteranl-component-button" onClick={onClick}>
                {/* 图标 */}
                <div class="f-icon f-icon-add"></div>
                {/* 高级设置按钮 */}
                <div class="f-import-exteranl-component-button-name">
                    <span class="f-import-exteranl-component-button-center">引入组件</span>
                </div>
            </div>
        );
    }
});
