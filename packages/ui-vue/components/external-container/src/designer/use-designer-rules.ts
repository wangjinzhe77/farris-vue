import { DesignerHostService, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { DesignerItemContext } from "../../../designer-canvas/src/types";

export function useDesignerRules(designItemContext: DesignerItemContext, designerHostService?: DesignerHostService): UseDesignerRules {
    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(): boolean {
        return false;
    }

    function checkCanDeleteComponent() {
        return true;
    }

    function checkCanMoveComponent() {
        return true;
    }


    function hideNestedPaddingInDesginerView() {
        return true;
    }
    function getStyles(): string {
        return ' position:relative; padding:0 !important;border:0;';
    }
    return { canAccepts, checkCanDeleteComponent, checkCanMoveComponent, hideNestedPaddingInDesginerView, getStyles };
}
