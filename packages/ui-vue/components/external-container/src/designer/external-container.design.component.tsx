import { defineComponent, inject, ref, onMounted, computed } from 'vue';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import FDesignerItem from '../../../designer-canvas/src/components/designer-item.component';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { externalContainerProps, ExteranlContainerPropsType } from '../external-container.props';
import { useDesignerRules } from './use-designer-rules';

import navListSchema from './nav_list.json';

import FImportExternalSchema from './import-external-schema.component';
import { DesignerHostService } from '../../../designer-canvas/src/composition/types';

export default defineComponent({
    name: 'FExternalContainerDesign',
    props: externalContainerProps,
    emits: [],
    setup(props: ExteranlContainerPropsType, context) {
        const elementRef = ref();
        const designerHostService = inject<DesignerHostService>('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        const externalSchema = ref();

        const shouldShowImportSchema = computed(() => !externalSchema.value);

        function renderExternalSchema() {
            return <FDesignerItem v-model={externalSchema.value}></FDesignerItem>;
        }

        function onSubmit(schemaPath: string) {
            externalSchema.value = navListSchema;
        }

        function renderImportExternalSchema() {
            return <FImportExternalSchema onSubmit={onSubmit}></FImportExternalSchema>;
        }

        return () => {
            return <div ref={elementRef} class="drag-container" data-dragref={`${designItemContext.schema.id}-container`}>
                {shouldShowImportSchema.value ? renderImportExternalSchema() : renderExternalSchema()}
            </div>;
        };
    }
});
