import { defineComponent } from 'vue';
import { ExteranlContainerPropsType, externalContainerProps } from './external-container.props';

export default defineComponent({
    name: 'FExternalContainer',
    props: externalContainerProps,
    emits: [],
    setup(props: ExteranlContainerPropsType, context) {
        return () => {
            return <div class={props.customClass}>{context.slots.default && context.slots.default()}</div>;
        };
    }
});
