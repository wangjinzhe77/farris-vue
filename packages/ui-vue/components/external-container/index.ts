 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App } from 'vue';
import FExternalContainer from './src/external-comtainer.component';
import FExternalContainerDesign from './src/designer/external-container.design.component';
import { propsResolver } from './src/external-container.props';

export * from './src/external-container.props';
export { FExternalContainer, FExternalContainerDesign };

export default {
    install(app: App): void {
        app.component(FExternalContainer.name as string, FExternalContainer);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap['external-container'] = FExternalContainer;
        propsResolverMap['external-container'] = propsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>): void {
        componentMap['external-container'] = FExternalContainerDesign;
        propsResolverMap['external-container'] = propsResolver;
    }
};
