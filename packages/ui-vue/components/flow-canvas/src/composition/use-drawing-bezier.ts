import { ref } from "vue";
import { UseBezierCurve, UseDrawingBezier } from "./types";

export function useDrawingBezier(
    useBezierCurveComposition: UseBezierCurve
): UseDrawingBezier {

    const ancherPosition = ref();
    const startX = ref(0);
    const startY = ref(0);
    const startPoint = ref({ x: 0, y: 0 });
    const startAncherElement = ref<HTMLElement>();
    const { drawing } = useBezierCurveComposition;

    function convertToNumber(pxStr: string) {
        return Number(pxStr.replace('px', ''));
    }

    function isAncherPoint(element: HTMLElement) {
        const classNames = element && element.className && element.className.split ? element.className.split(' ') : [];
        return classNames.includes('f-flow-ancher');
    }

    function getAncherPointPosition(ancherElement: HTMLElement | null) {
        const classNames = ancherElement ? ancherElement.className.split(' ') : [];
        const circleAncherClass = classNames.find((className: string) => className.startsWith('circle-')) || '';
        switch (circleAncherClass) {
            case 'circle-left':
                return 'west';
            case 'circle-right':
                return 'east';
            case 'circle-top':
                return 'north';
            case 'circle-bottom':
                return 'south';
            default:
                return 'center';
        }
    }

    function resetStartPosition(ancherX: number, ancherY: number, ancherElement: HTMLElement) {
        const startAncherRect = ancherElement.getBoundingClientRect() as DOMRect;
        startX.value = startAncherRect.left;
        startY.value = startAncherRect.top;
        startPoint.value = { x: ancherX, y: ancherY };
        ancherPosition.value = getAncherPointPosition(ancherElement);
    }

    function updateDrawingLine(mouseMovingArgs: MouseEvent) {
        const leftOffset = mouseMovingArgs.clientX - startX.value;
        const topOffset = mouseMovingArgs.clientY - startY.value;
        const newLeft = startPoint.value.x + leftOffset;
        const newTop = startPoint.value.y + topOffset;
        const targetElement = mouseMovingArgs.target as HTMLElement;
        const endPoinPosition = targetElement && typeof targetElement.className === 'string' &&
            isAncherPoint(targetElement) ? getAncherPointPosition(targetElement) : 'center';
        drawing(
            `${startAncherElement.value?.id}_curve_to`,
            { x: startPoint.value.x, y: startPoint.value.y },
            { x: newLeft, y: newTop },
            ancherPosition.value, endPoinPosition
        );
    }

    function eraseDrawingLine(curveId: string) {
        const drawingLine = document.getElementById(curveId);
        drawingLine && drawingLine.remove();
    }

    function bindingDrawingEvents() {
        document.addEventListener('mousemove', updateDrawingLine);
    }

    function releaseDrawingEvents() {
        document.removeEventListener('mousemove', updateDrawingLine);
    }

    function finishToDraw(event: MouseEvent) {
        startAncherElement.value = undefined;
        releaseDrawingEvents();
    }

    function drawFrom(ancherElementId: string, payload: MouseEvent) {
        startAncherElement.value = document.getElementById(ancherElementId) as HTMLElement;
        const flowNodeElement = startAncherElement.value.offsetParent as HTMLElement;
        const startAncherRect = startAncherElement.value.getBoundingClientRect() as DOMRect;
        const flowNodeRect = flowNodeElement.getBoundingClientRect() as DOMRect;
        const flowNodeX = convertToNumber(flowNodeElement.style.left) + (startAncherRect.left - flowNodeRect.left);
        const flowNodeY = convertToNumber(flowNodeElement.style.top) + (startAncherRect.top - flowNodeRect.top);
        const startX = flowNodeX + startAncherRect.width / 2;
        const startY = flowNodeY + startAncherRect.height / 2;
        resetStartPosition(startX, startY, startAncherElement.value);
        bindingDrawingEvents();
    }

    return { drawFrom, eraseDrawingLine, finishToDraw, getAncherPointPosition, isAncherPoint };
}
