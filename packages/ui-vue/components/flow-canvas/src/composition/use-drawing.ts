import { ref } from "vue";
import { UseCurve } from "./types";

export function useDrawing(
    useCurveComposition: UseCurve
) {

    const startX = ref(0);
    const startY = ref(0);
    const startPoint = ref({ x: 0, y: 0 });
    const startAncherElement = ref<HTMLElement>();
    const { createCurveBetween, drawingCurveBetween } = useCurveComposition;

    function convertToNumber(pxStr: string) {
        return Number(pxStr.replace('px', ''));
    }

    function isAncherPoint(element: HTMLElement) {
        const classNames = element ? element.className.split(' ') : [];
        return classNames.includes('f-flow-ancher');
    }

    function getAncherPointPosition(ancherElement: HTMLElement | null) {
        if (ancherElement !== null) {
            const flowNodeElement = ancherElement.parentNode as any;
            const flowNodeX = convertToNumber(flowNodeElement.style.left);
            const flowNodeY = convertToNumber(flowNodeElement.style.top);
            return {
                x: flowNodeX + ancherElement.offsetLeft + ancherElement.offsetWidth / 2,
                y: flowNodeY + ancherElement.offsetTop + ancherElement.offsetHeight / 2
            };
        }
        return { x: 0, y: 0 };
    };

    function resetStartPosition(ancherX: number, ancherY: number, ancherElement: HTMLElement) {
        startX.value = ancherX;
        startY.value = ancherY;
        startPoint.value = getAncherPointPosition(ancherElement);
    }

    function updateDrawingLine(mouseMovingArgs: MouseEvent) {
        const leftOffset = mouseMovingArgs.clientX - startX.value;
        const topOffset = mouseMovingArgs.clientY - startY.value;
        const newLeft = startPoint.value.x + leftOffset;
        const newTop = startPoint.value.y + topOffset;
        drawingCurveBetween(startPoint.value.x, startPoint.value.y, newLeft, newTop);
    }

    function eraseDrawingLine() {
        const drawingLine = document.getElementById("tempSvgContainer");
        drawingLine && drawingLine.remove();
    }

    function confirmToDrawLine(startAncherElement: HTMLElement, endAncherElement: HTMLElement) {
        if (isAncherPoint(startAncherElement) && isAncherPoint(endAncherElement)) {
            createCurveBetween(startAncherElement.id, endAncherElement.id);
        }
    }

    function finishToDraw(event: MouseEvent) {
        // eslint-disable-next-line no-use-before-define
        releaseDrawingEvents();
        eraseDrawingLine();
        confirmToDrawLine(startAncherElement.value as HTMLElement, event.target as HTMLElement);
    }

    function bindingDrawingEvents() {
        document.addEventListener('mousemove', updateDrawingLine);
        document.addEventListener('mouseup', finishToDraw);
    }

    function releaseDrawingEvents() {
        document.removeEventListener('mousemove', updateDrawingLine);
        document.removeEventListener('mouseup', finishToDraw);
    }

    function drawFrom(ancherElementId: string, payload: MouseEvent) {
        startAncherElement.value = document.getElementById(ancherElementId) as HTMLElement;
        resetStartPosition(payload.clientX, payload.clientY, startAncherElement.value);
        bindingDrawingEvents();
    }

    return { drawFrom };
}
