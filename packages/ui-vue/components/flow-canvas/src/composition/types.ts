export interface CurvePoint {
    x: number;
    y: number;
}

export type CurveOrientation = 'default' | 'portrait' | 'landscape';

export interface CurvePointOffset {
    aroundDirection: string;
    left: number;
    right: number;
    top: number;
    bottom: number;
    x: number;
    y: number;
}

export interface UseDrawing {
    drawFrom: (ancherElementId: string, payload: MouseEvent) => void;
}

export interface UseCurve {
    createCurveBetween: (starPointId: string, endPointId: string) => void;

    drawingCurveBetween: (startX: number, startY: number, endX: number, endY: number) => void;
}

export interface UseBezierCurve {
    connect: (startPointId: string, endPointId: string, startPointPosition: string, endPoinPosition: string) => void;

    drawing: (curveId: string, startPoint: CurvePoint, endPoint: CurvePoint, startPointPosition: string, endPoinPosition: string) => void;
}

export interface UseDrawingBezier {
    drawFrom: (ancherElementId: string, payload: MouseEvent) => void;

    eraseDrawingLine: (curveId: string) => void;

    finishToDraw: (event: MouseEvent) => void;

    getAncherPointPosition: (ancherElement: HTMLElement | null) => string;

    isAncherPoint: (element: HTMLElement) => boolean;
}

export type ConnectionType = 'Curve' | 'Line';

export interface Connection {
    from: string;
    to: string;
    type: ConnectionType;
}

export interface UseConnections {

    addConnection: (fromNodeId: string, startAncherId: string, toNodeId: string, endAncherId: string) => void;

    getConnectionsOfNode: (nodeId: string) => Connection[];

}
