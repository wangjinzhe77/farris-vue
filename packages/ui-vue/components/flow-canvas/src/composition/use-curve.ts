export function useCurve() {

    function convertToNumber(pxStr: string) {
        return Number(pxStr.replace('px', ''));
    }

    function createCurvePath() {
        const curvePath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        curvePath.setAttribute("fill", "none");
        curvePath.setAttribute("stroke", "#4d53e8");
        curvePath.setAttribute("stroke-width", "2");
        curvePath.setAttribute("class", "");
        return curvePath;
    }

    function createArrowPath() {
        const arrowPath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        arrowPath.setAttribute("fill", "none");
        arrowPath.setAttribute("stroke", "#4d53e8");
        arrowPath.setAttribute("stroke-width", "2");
        arrowPath.setAttribute("stroke-linecap", "round");
        return arrowPath;
    }

    function bindCurveEvents(curveId: string, curveElement: HTMLElement, curvePath: SVGPathElement, arrowPath: SVGPathElement) {
        const deleteLine = (payload: KeyboardEvent) => {
            if (payload.key === 'Delete') {
                const splitIndex = curveId.indexOf('^');
                document.removeEventListener('keydown', deleteLine);
                curveElement.remove();
            }
        };

        curvePath.addEventListener('mouseenter', () => {
            curvePath.setAttribute("stroke", "#37d0ff");
            arrowPath.setAttribute("stroke", "#37d0ff");
            curvePath.setAttribute("stroke-width", "3");
            document.addEventListener('keydown', deleteLine);
        });

        curvePath.addEventListener('mouseleave', () => {
            curvePath.setAttribute("stroke", "#4d53e8");
            curvePath.setAttribute("stroke-width", "2");
            arrowPath.setAttribute("stroke", "#4d53e8");
            document.removeEventListener('keydown', deleteLine);
        });
    }

    function getAncherPointPosition(ancherElement: HTMLElement | null) {
        if (ancherElement !== null) {
            const flowNodeElement = ancherElement.parentNode as any;
            const flowNodeX = convertToNumber(flowNodeElement.style.left);
            const flowNodeY = convertToNumber(flowNodeElement.style.top);
            return {
                x: flowNodeX + ancherElement.offsetLeft + ancherElement.offsetWidth / 2,
                y: flowNodeY + ancherElement.offsetTop + ancherElement.offsetHeight / 2
            };
        }
        return { x: 0, y: 0 };
    };

    const widthOffset = 50;
    const controlPointOffset = 4 * widthOffset;

    function createCurveId(starPointId: string, endPointId: string) {
        return starPointId + "^" + endPointId;
    }

    function createCurveElement(curveId: string) {
        let curveElement = document.getElementById(curveId) as HTMLElement;
        if (curveElement == null) {
            const curveSVG = document.createElementNS("http://www.w3.org/2000/svg", "svg");
            const curvePath = createCurvePath();
            curveSVG.appendChild(curvePath);
            const arrowPath = createArrowPath();
            curveSVG.appendChild(arrowPath);

            curveElement = document.createElement("div");
            curveElement.id = curveId;
            curveElement.style.position = "absolute";
            curveElement.appendChild(curveSVG);
            bindCurveEvents(curveId, curveElement, curvePath, arrowPath);

            const svgContainer = document.getElementById("svg-container");
            if (svgContainer) {
                svgContainer.appendChild(curveElement);
            }
            return curveElement;
        }
        return curveElement;
    }

    function getPointRange(startPointId: string, endPointId: string) {
        const startAncherElement = document.getElementById(startPointId);
        const endAcherElement = document.getElementById(endPointId);
        const startPoint = getAncherPointPosition(startAncherElement);
        const endPoint = getAncherPointPosition(endAcherElement);
        const startX = startPoint.x + 2;
        const startY = startPoint.y;
        const endX = endPoint.x - 2;
        const endY = endPoint.y;
        return { startX, startY, endX, endY };
    }

    function updateCurvePath(
        curveElement: HTMLElement, width: number, height: number, startX: number, startY: number, endX: number, endY: number
    ) {
        const curveSVGElement = curveElement.childNodes[0] as Element;
        curveSVGElement.setAttribute("width", String(width));
        curveSVGElement.setAttribute("height", String(height));
        curveElement.appendChild(curveSVGElement);

        const curvePath = curveSVGElement.childNodes[0] as Element;
        const xMiddle = (startX + endX) / 2;
        const startPointX = endX > startX ? startX : width - controlPointOffset;
        const startPointY = startY;
        const firstControlPointX = endX > startX ? xMiddle : width;
        const firstControlPointY = endX > startX ? startY : widthOffset / 2;
        const secondControlPointX = endX > startX ? xMiddle : 0;
        const secondControlPointY = endX > startX ? endY : height - widthOffset / 2;
        const endPointX = endX > startX ? endX : controlPointOffset;
        const endPointY = endY;

        const startPosition = `${startPointX} ${startPointY}`;
        const firstControlPointPosition = `${firstControlPointX} ${firstControlPointY}`;
        const secondControlPointPosition = `${secondControlPointX} ${secondControlPointY}`;
        const endPosition = `${endPointX} ${endPointY}`;
        const curvePathData = "M ".concat(startPosition)
            .concat(" C ").concat(firstControlPointPosition).concat(',' + secondControlPointPosition + ',')
            .concat(endPosition);

        curvePath.setAttribute("d", curvePathData);
        curvePath.setAttribute("fill", "none");
        curvePath.setAttribute("stroke", "#4d53e8");
        curvePath.setAttribute("stroke-width", "2");
        curvePath.setAttribute("class", "");

        const path2 = curveSVGElement.childNodes[1] as Element;
        const path2D = "M".concat(" " + (endPointX - 6) + "," + (endPointY - 6))
            .concat(" L " + endPointX + "," + endPointY)
            .concat(" L " + (endPointX - 6) + "," + (endPointY + 6));

        path2.setAttribute("d", path2D);
        path2.setAttribute("fill", "none");
        path2.setAttribute("stroke", "#4d53e8");
        path2.setAttribute("stroke-width", "2");
        path2.setAttribute("stroke-linecap", "round");
    }

    function reCalculateCurvePosition(startX: number, startY: number, endX: number, endY: number) {
        const widthOffset = endX > startX ? 0 : controlPointOffset * 2;
        const width = Math.abs(endX - startX) + widthOffset;
        const height = Math.abs(endY - startY) + 22;

        const leftOffset = endX > startX ? 0 : 0 - controlPointOffset;
        const left = Math.min(startX, endX) + leftOffset;
        const top = Math.min(startY, endY) - 7;

        const pointWidth = Math.abs(endX - startX);
        const pointHeight = Math.abs(endY - startY);
        const offsetStartX = endX > startX ? 0 : controlPointOffset + pointWidth;
        const offsetStartY = endY > startY ? 0 : pointHeight;
        const offsetEndY = endY > startY ? pointHeight : 0;
        const offsetEndX = endX > startX ? pointWidth : controlPointOffset;

        startX = offsetStartX + 2;
        startY = offsetStartY + 7;
        endX = offsetEndX - 3;
        endY = offsetEndY + 7;

        return { width, height, left, top, startX, startY, endX, endY };
    }

    function setCurveBoundray(curveElement: HTMLElement, left: number, top: number, width: number, height: number) {
        curveElement.style.position = "absolute";
        curveElement.style.alignItems = "center";
        curveElement.style.left = `${left}px`;
        curveElement.style.top = `${top}px`;
        curveElement.style.width = width + "px";
        curveElement.style.height = height + "px";
    }

    function drawCurve(curveElement: HTMLElement, x1: number, y1: number, x2: number, y2: number) {
        const { width, height, left, top, startX, startY, endX, endY } = reCalculateCurvePosition(x1, y1, x2, y2);
        setCurveBoundray(curveElement, left, top, width, height);
        updateCurvePath(curveElement, width, height, startX, startY, endX, endY);
    }

    function createCurveBetween(startPointId: string, endPointId: string) {
        const curveId = createCurveId(startPointId, endPointId);
        const curveElement = createCurveElement(curveId);
        const { startX, startY, endX, endY } = getPointRange(startPointId, endPointId);
        drawCurve(curveElement, startX, startY, endX, endY);
    }

    function drawingCurveBetween(startX: number, startY: number, endX: number, endY: number) {
        const curveElement = createCurveElement('tempSvgContainer');
        drawCurve(curveElement, startX, startY, endX, endY);
    }

    return { createCurveBetween, drawingCurveBetween };

}
