/* eslint-disable complexity */
 
import { CurveOrientation, CurvePoint, CurvePointOffset, UseBezierCurve } from "./types";

export function useBezierCurve(): UseBezierCurve {

    const widthOffset = 50;
    const controlPointOffset = 2 * widthOffset;
    const lineWidth = 2;
    const defaultPointOffset: CurvePointOffset = { aroundDirection: 'none', left: lineWidth, right: lineWidth, top: lineWidth, bottom: lineWidth, x: 0, y: 0 };

    function createCurvePath() {
        const curvePath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        curvePath.setAttribute("fill", "none");
        curvePath.setAttribute("stroke", "#4d53e8");
        curvePath.setAttribute("stroke-width", "2");
        curvePath.setAttribute("class", "");
        return curvePath;
    }

    function createArrowPath() {
        const arrowPath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        arrowPath.setAttribute("fill", "none");
        arrowPath.setAttribute("stroke", "#4d53e8");
        arrowPath.setAttribute("stroke-width", "2");
        arrowPath.setAttribute("stroke-linecap", "round");
        return arrowPath;
    }

    function createControlPointPath() {
        const arrowPath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        arrowPath.setAttribute("fill", "none");
        arrowPath.setAttribute("stroke", "#4d53e8");
        arrowPath.setAttribute("stroke-width", "2");
        arrowPath.setAttribute("stroke-linecap", "round");
        return arrowPath;
    }

    function bindCurveEvents(curveId: string, curveElement: HTMLElement, curvePath: SVGPathElement, arrowPath: SVGPathElement) {
        const deleteLine = (payload: KeyboardEvent) => {
            if (payload.key === 'Delete') {
                const splitIndex = curveId.indexOf('^');
                document.removeEventListener('keydown', deleteLine);
                curveElement.remove();
            }
        };

        curvePath.addEventListener('mouseenter', () => {
            curvePath.setAttribute("stroke", "#37d0ff");
            arrowPath.setAttribute("stroke", "#37d0ff");
            curvePath.setAttribute("stroke-width", "3");
            document.addEventListener('keydown', deleteLine);
        });

        curvePath.addEventListener('mouseleave', () => {
            curvePath.setAttribute("stroke", "#4d53e8");
            curvePath.setAttribute("stroke-width", "2");
            arrowPath.setAttribute("stroke", "#4d53e8");
            document.removeEventListener('keydown', deleteLine);
        });
    }

    function createCurveElement(curveId: string) {
        let curveElement = document.getElementById(curveId) as HTMLElement;
        if (curveElement == null) {
            const curveSVG = document.createElementNS("http://www.w3.org/2000/svg", "svg");
            const curvePath = createCurvePath();
            curveSVG.appendChild(curvePath);
            const arrowPath = createArrowPath();
            curveSVG.appendChild(arrowPath);
            const firstControlPointPath = createControlPointPath();
            curveSVG.appendChild(firstControlPointPath);
            const secondControlPointPath = createControlPointPath();
            curveSVG.appendChild(secondControlPointPath);
            // curveSVG.style = "background-color: aliceblue;";

            curveElement = document.createElement("div");
            curveElement.id = curveId;
            curveElement.style.position = "absolute";
            curveElement.appendChild(curveSVG);
            bindCurveEvents(curveId, curveElement, curvePath, arrowPath);

            const svgContainer = document.getElementById("svg-container");
            if (svgContainer) {
                svgContainer.appendChild(curveElement);
            }
            return curveElement;
        }
        return curveElement;
    }

    function getFirstPointOffset(startDirection: string, pathDirection: string, connectDirection: string, width: number, height: number) {
        const firstPointOffset = { x: 0, y: 0 } as CurvePointOffset;
        if (startDirection === 'west') {
            if (pathDirection === 'north_west') {
                if (connectDirection === 'north') {
                    firstPointOffset.x = 0;
                    firstPointOffset.y = 0 - height / 2;
                }
                if (connectDirection === 'west') {
                    firstPointOffset.x = 0 - width / 2;
                    firstPointOffset.y = 0;
                }
            }
            if (pathDirection === 'north_east') {
                if (connectDirection === 'north') {
                    firstPointOffset.x = 0 - controlPointOffset;
                    firstPointOffset.y = 0 - height / 2;
                }
                if (connectDirection === 'east') {
                    firstPointOffset.x = 0 - controlPointOffset;
                    firstPointOffset.y = 0 - height / 2;
                }
            }
            if (pathDirection === 'south_east') {
                if (connectDirection === 'south') {
                    firstPointOffset.x = 0 - controlPointOffset;
                    firstPointOffset.y = 0 + height / 2;
                }
                if (connectDirection === 'east') {
                    firstPointOffset.x = 0 - controlPointOffset;
                    firstPointOffset.y = 0 + height / 2;
                }
            }
            if (pathDirection === 'south_west') {
                if (connectDirection === 'south') {
                    firstPointOffset.x = 0;
                    firstPointOffset.y = 0 + height / 2;
                }
                if (connectDirection === 'west') {
                    firstPointOffset.x = 0 - width / 2;
                    firstPointOffset.y = 0;
                }
            }
        }
        if (startDirection === 'east') {
            if (pathDirection === 'north_west') {
                if (connectDirection === 'north') {
                    firstPointOffset.x = 0 - controlPointOffset;
                    firstPointOffset.y = 0 - height / 2;
                }
                if (connectDirection === 'west') {
                    firstPointOffset.x = 0 + controlPointOffset;
                    firstPointOffset.y = 0 - height / 2;
                }
            }
            if (pathDirection === 'north_east') {
                if (connectDirection === 'north') {
                    firstPointOffset.x = 0;
                    firstPointOffset.y = 0 - height / 2;
                }
                if (connectDirection === 'east') {
                    firstPointOffset.x = 0 + width / 2;
                    firstPointOffset.y = 0;
                }
            }
            if (pathDirection === 'south_east') {
                if (connectDirection === 'south') {
                    firstPointOffset.x = 0;
                    firstPointOffset.y = 0 + height / 2;
                }
                if (connectDirection === 'east') {
                    firstPointOffset.x = 0 + width / 2;
                    firstPointOffset.y = 0;
                }
            }
            if (pathDirection === 'south_west') {
                if (connectDirection === 'south') {
                    firstPointOffset.x = 0 + controlPointOffset;
                    firstPointOffset.y = 0 + height / 2;
                }
                if (connectDirection === 'west') {
                    firstPointOffset.x = 0 + controlPointOffset;
                    firstPointOffset.y = 0 + height / 2;
                }
            }
        }
        if (startDirection === 'north') {
            if (pathDirection === 'north_west') {
                if (connectDirection === 'north') {
                    firstPointOffset.x = 0;
                    firstPointOffset.y = 0 - height / 2;
                }
                if (connectDirection === 'west') {
                    firstPointOffset.x = 0 - width / 2;
                    firstPointOffset.y = 0;
                }
            }
            if (pathDirection === 'north_east') {
                if (connectDirection === 'north') {
                    firstPointOffset.x = 0;
                    firstPointOffset.y = 0 - height / 2;
                }
                if (connectDirection === 'east') {
                    firstPointOffset.x = 0 + width / 2;
                    firstPointOffset.y = 0;
                }
            }
            if (pathDirection === 'south_east') {
                if (connectDirection === 'south') {
                    firstPointOffset.x = 0 + width / 2;
                    firstPointOffset.y = 0 - controlPointOffset;
                }
                if (connectDirection === 'east') {
                    firstPointOffset.x = 0 + width / 2;
                    firstPointOffset.y = 0 - controlPointOffset;
                }
            }
            if (pathDirection === 'south_west') {
                if (connectDirection === 'south') {
                    firstPointOffset.x = 0 - width / 2;
                    firstPointOffset.y = 0 - controlPointOffset;
                }
                if (connectDirection === 'west') {
                    firstPointOffset.x = 0 - width / 2;
                    firstPointOffset.y = 0 - controlPointOffset;
                }
            }
        }
        if (startDirection === 'south') {
            if (pathDirection === 'north_west') {
                if (connectDirection === 'north') {
                    firstPointOffset.x = 0 - width / 2;
                    firstPointOffset.y = 0 + controlPointOffset;
                }
                if (connectDirection === 'west') {
                    firstPointOffset.x = 0 - width / 2;
                    firstPointOffset.y = 0 + controlPointOffset;
                }
            }
            if (pathDirection === 'north_east') {
                if (connectDirection === 'north') {
                    firstPointOffset.x = 0 + width / 2;
                    firstPointOffset.y = 0 + controlPointOffset;
                }
                if (connectDirection === 'east') {
                    firstPointOffset.x = 0 + width / 2;
                    firstPointOffset.y = 0 + controlPointOffset;
                }
            }
            if (pathDirection === 'south_east') {
                if (connectDirection === 'south') {
                    firstPointOffset.x = 0;
                    firstPointOffset.y = 0 + height / 2;
                }
                if (connectDirection === 'east') {
                    firstPointOffset.x = 0 + width / 2;
                    firstPointOffset.y = 0;
                }
            }
            if (pathDirection === 'south_west') {
                if (connectDirection === 'south') {
                    firstPointOffset.x = 0;
                    firstPointOffset.y = height / 2;
                }
                if (connectDirection === 'west') {
                    firstPointOffset.x = 0 - width / 2;
                    firstPointOffset.y = 0;
                }
            }
        }
        return firstPointOffset;
    }

    function getSecondPointOffset(connectDirection: string, slotDirection: string, width: number, height: number) {
        const secondPointOffset = { x: 0, y: 0 } as CurvePointOffset;
        if (connectDirection === 'south') {
            if (slotDirection === 'west') {
                secondPointOffset.x = 0 - controlPointOffset;
                secondPointOffset.y = 0 - height / 2;
            }
            if (slotDirection === 'east') {
                secondPointOffset.x = 0 + controlPointOffset;
                secondPointOffset.y = 0 - controlPointOffset;
            }
            if (slotDirection === 'south') {
                secondPointOffset.x = 0 - width / 2;
                secondPointOffset.y = controlPointOffset;
            }
            if (slotDirection === 'north' || slotDirection === 'center') {
                secondPointOffset.x = 0;
                secondPointOffset.y = 0 - height / 2;
            }
        }
        if (connectDirection === 'north') {
            if (slotDirection === 'west') {
                secondPointOffset.x = 0 - controlPointOffset;
                secondPointOffset.y = 0 + controlPointOffset;
            }
            if (slotDirection === 'east') {
                secondPointOffset.x = 0 + controlPointOffset;
                secondPointOffset.y = 0 + controlPointOffset;
            }
            if (slotDirection === 'north') {
                secondPointOffset.x = 0 - width / 2;
                secondPointOffset.y = 0 - controlPointOffset;
            }
            if (slotDirection === 'south' || slotDirection === 'center') {
                secondPointOffset.x = 0;
                secondPointOffset.y = height / 2;
            }
        }
        if (connectDirection === 'east') {
            if (slotDirection === 'north') {
                secondPointOffset.y = 0 - controlPointOffset;
                secondPointOffset.x = 0 - controlPointOffset;
            }
            if (slotDirection === 'south') {
                secondPointOffset.y = controlPointOffset;
                secondPointOffset.x = 0 - controlPointOffset;
            }
            if (slotDirection === 'east') {
                secondPointOffset.x = 0 + controlPointOffset;
                secondPointOffset.y = 0 + controlPointOffset;
            }
            if (slotDirection === 'west' || slotDirection === 'center') {
                secondPointOffset.x = 0 - width / 2;
                secondPointOffset.y = 0;
            }
        }
        if (connectDirection === 'west') {
            if (slotDirection === 'north') {
                secondPointOffset.y = 0 - controlPointOffset;
                secondPointOffset.x = 0 + controlPointOffset;
            }
            if (slotDirection === 'south') {
                secondPointOffset.y = 0 + controlPointOffset;
                secondPointOffset.x = 0 + controlPointOffset;
            }
            if (slotDirection === 'west') {
                secondPointOffset.x = 0 - controlPointOffset;
                secondPointOffset.y = height / 2;
            }
            if (slotDirection === 'east' || slotDirection === 'center') {
                secondPointOffset.x = width / 2;
                secondPointOffset.y = 0;
            }
        }
        return secondPointOffset;
    }

    function getControlPointAtNorthEast(
        width: number, height: number, shouldReserve: boolean, startPointOffset: CurvePointOffset,
        startPointPosition: string, pathDirection: string, endPoinPosition: string
    ): [CurvePoint, CurvePoint, CurvePoint] {
        const orientation = (endPoinPosition === 'south' || endPoinPosition === 'east') ? 'portrait' : 'landscape';
        const startDirection = startPointPosition;
        const connectDirection = orientation === 'landscape' ? 'east' : 'north';
        const slotDirection = endPoinPosition === 'center' ? (orientation === 'landscape' ? 'west' : 'south') : endPoinPosition;

        const firstControlPointOffset = getFirstPointOffset(startDirection, pathDirection, connectDirection, width, height);
        const { x: x1, y: y1 } = firstControlPointOffset;

        const secondControlPointOffset = getSecondPointOffset(connectDirection, slotDirection, width, height);
        const { x, y } = secondControlPointOffset;

        if (y < 0) {
            height += Math.abs(y);
        }

        const defaultXOfFirstControlPoint = new Map<CurveOrientation, number>([['default', 0 + x1], ['portrait', 0 + x1], ['landscape', 0 + x1]]);

        const defaultYOfFirstControlPoint = new Map<CurveOrientation, number>([['default', height + y1], ['portrait', height + y1], ['landscape', height + y1]]);

        if (x1 < 0) {
            width += Math.abs(x1);
        }

        if (x > 0) {
            width += x;
        }

        const defaultXOfSecondControlPoint = new Map<CurveOrientation, number>([['default', width + x], ['portrait', width + x], ['landscape', width + x]]);

        const defaultYOfSecondControlPoint = new Map<CurveOrientation, number>([['default', 0 + y], ['portrait', 0 + y], ['landscape', 0 + y]]);

        const shouldPassAroundLeft = startPointOffset.left > defaultPointOffset.left;
        const shouldPassAroundBottom = startPointOffset.bottom > defaultPointOffset.bottom;

        const firstControlPointX = shouldPassAroundLeft ? 0 : (shouldPassAroundBottom ? width / 2 : defaultXOfFirstControlPoint.get(orientation) as number);
        const firstControlPOintY = shouldPassAroundBottom ? height + startPointOffset.bottom : (shouldPassAroundLeft ? height / 2 : defaultYOfFirstControlPoint.get(orientation) as number);

        const secondControlPointX = defaultXOfSecondControlPoint.get(orientation) as number;
        const secondControlPointY = defaultYOfSecondControlPoint.get(orientation) as number;

        return [{ x: firstControlPointX, y: firstControlPOintY }, { x: secondControlPointX, y: secondControlPointY }, { x, y }];
    }

    function getControlPointAtNorthWest(
        width: number, height: number, shouldReserve: boolean, startPointOffset: CurvePointOffset,
        startPointPosition: string, pathDirection: string, endPoinPosition: string
    ): [CurvePoint, CurvePoint, CurvePoint] {
        const orientation = (endPoinPosition === 'south' || endPoinPosition === 'west') ? 'portrait' : 'landscape';
        const startDirection = startPointPosition;
        const connectDirection = orientation === 'landscape' ? 'west' : 'north';
        const slotDirection = endPoinPosition;

        const firstControlPointOffset = getFirstPointOffset(startDirection, pathDirection, connectDirection, width, height);
        const { x: x1, y: y1 } = firstControlPointOffset;

        const secondControlPointOffset = getSecondPointOffset(connectDirection, slotDirection, width, height);
        const { x, y } = secondControlPointOffset;

        if (y < 0) {
            height += Math.abs(y);
        }

        if (x1 > 0) {
            width += x1;
        }

        if (x < 0) {
            width += Math.abs(x);
        }

        const defaultXOfFirstControlPoint = new Map<CurveOrientation, number>([['default', width + x1], ['portrait', width + x1], ['landscape', width + x1]]);

        const defaultYOfFirstControlPoint = new Map<CurveOrientation, number>([['default', height + y1], ['portrait', height + y1], ['landscape', height + y1]]);

        const defaultXOfSecondControlPoint = new Map<CurveOrientation, number>([['default', 0 + x], ['portrait', 0 + x], ['landscape', 0 + x]]);

        const defaultYOfSecondControlPoint = new Map<CurveOrientation, number>([['default', 0 + y], ['portrait', 0 + y], ['landscape', 0 + y]]);

        const shouldPassAroundRight = startPointOffset.right > defaultPointOffset.right;
        const shouldPassAroundBottom = startPointOffset.bottom > defaultPointOffset.bottom;

        const firstControlPointX = shouldPassAroundRight ? width + startPointOffset.right : (shouldPassAroundBottom ? width / 2 : defaultXOfFirstControlPoint.get(orientation) as number);
        const firstControlPOintY = shouldPassAroundBottom ? height + startPointOffset.bottom : (shouldPassAroundRight ? height / 2 : defaultYOfFirstControlPoint.get(orientation) as number);

        const secondControlPointX = defaultXOfSecondControlPoint.get(orientation) as number;
        const secondControlPointY = defaultYOfSecondControlPoint.get(orientation) as number;

        return [{ x: firstControlPointX, y: firstControlPOintY }, { x: secondControlPointX, y: secondControlPointY }, { x, y }];
    }

    function getControlPointAtSouthEast(
        width: number, height: number, shouldReserve: boolean, startPointOffset: CurvePointOffset,
        startPointPosition: string, pathDirection: string, endPoinPosition: string
    ): [CurvePoint, CurvePoint, CurvePoint] {
        const orientation = (endPoinPosition === 'north' || endPoinPosition === 'east') ? 'portrait' : 'landscape';
        const startDirection = startPointPosition;
        const connectDirection = orientation === 'landscape' ? 'east' : 'south';
        const slotDirection = endPoinPosition;

        const firstControlPointOffset = getFirstPointOffset(startDirection, pathDirection, connectDirection, width, height);
        const { x: x1, y: y1 } = firstControlPointOffset;

        const secondControlPointOffset = getSecondPointOffset(connectDirection, slotDirection, width, height);
        const { x, y } = secondControlPointOffset;

        const defaultXOfFirstControlPointInOrientation = new Map<CurveOrientation, number>([['default', 0 + x1], ['portrait', 0 + x1], ['landscape', 0 + x1]]);

        const defaultYOfFirstControlPointInOrientation = new Map<CurveOrientation, number>([['default', 0 + y1], ['portrait', 0 + y1], ['landscape', 0 + y1]]);

        if (x1 < 0) {
            width += Math.abs(x1);
        }

        if (y1 < 0) {
            height += Math.abs(y1);
        }

        if (x > 0) {
            width += x;
        }

        if (y > 0) {
            height += y;
        }

        const defaultXOfSecondControlPoint = new Map<CurveOrientation, number>([['default', width + x], ['portrait', width + x], ['landscape', width + x]]);

        const defaultYOfSecondControlPoint = new Map<CurveOrientation, number>([['default', height + y], ['portrait', height + y], ['landscape', height + y]]);

        const shouldPassAroundLeft = startPointOffset.left > defaultPointOffset.left;
        const shouldPassAroundTop = startPointOffset.top > defaultPointOffset.top;

        const firstControlPointX = shouldPassAroundLeft ? 0 : (shouldPassAroundTop ? width / 2 : defaultXOfFirstControlPointInOrientation.get(orientation) as number);
        const firstControlPOintY = shouldPassAroundTop ? 0 : (shouldPassAroundLeft ? height / 2 : defaultYOfFirstControlPointInOrientation.get(orientation) as number);

        const secondControlPointX = defaultXOfSecondControlPoint.get(orientation) as number;
        const secondControlPointY = defaultYOfSecondControlPoint.get(orientation) as number;

        return [{ x: firstControlPointX, y: firstControlPOintY }, { x: secondControlPointX, y: secondControlPointY }, { x, y }];
    }

    function getControlPointAtSouthWest(
        width: number, height: number, shouldReserve: boolean, startPointOffset: CurvePointOffset,
        startPointPosition: string, pathDirection: string, endPoinPosition: string
    ): [CurvePoint, CurvePoint, CurvePoint] {
        const orientation = (endPoinPosition === 'north' || endPoinPosition === 'west') ? 'portrait' : 'landscape';
        const startDirection = startPointPosition;
        const connectDirection = orientation === 'landscape' ? 'west' : 'south';
        const slotDirection = endPoinPosition;

        const firstControlPointOffset = getFirstPointOffset(startDirection, pathDirection, connectDirection, width, height);
        const { x: x1, y: y1 } = firstControlPointOffset;

        const secondControlPointOffset = getSecondPointOffset(connectDirection, slotDirection, width, height);
        const { x, y } = secondControlPointOffset;

        if (x < 0) {
            width += Math.abs(x);
        }

        const defaultXOfFirstControlPoint = new Map<CurveOrientation, number>([['default', width + x1], ['portrait', width + x1], ['landscape', width + x1]]);

        const defaultYOfFirstControlPoint = new Map<CurveOrientation, number>([['default', 0 + y1], ['portrait', 0 + y1], ['landscape', 0 + y1]]);

        if (y1 < 0) {
            height += Math.abs(y1);
        }
        if (y > 0) {
            height += y;
        }

        const defaultXOfSecondControlPoint = new Map<CurveOrientation, number>([['default', 0 + x], ['portrait', 0 + x], ['landscape', 0 + x]]);

        const defaultYOfSecondControlPoint = new Map<CurveOrientation, number>([['default', height + y], ['portrait', height + y], ['landscape', height + y]]);

        const shouldPassAroundRight = startPointOffset.right > defaultPointOffset.right;
        const shouldPassAroundTop = startPointOffset.top > defaultPointOffset.top;

        const firstControlPointX = shouldPassAroundRight ? width + startPointOffset.right : (shouldPassAroundTop ? width / 2 : defaultXOfFirstControlPoint.get(orientation) as number);
        const firstControlPOintY = shouldPassAroundTop ? 0 : (shouldPassAroundRight ? height / 2 : defaultYOfFirstControlPoint.get(orientation) as number);

        const secondControlPointX = defaultXOfSecondControlPoint.get(orientation) as number;
        const secondControlPointY = defaultYOfSecondControlPoint.get(orientation) as number;

        return [{ x: firstControlPointX, y: firstControlPOintY }, { x: secondControlPointX, y: secondControlPointY }, { x, y }];
    }

    const controlPointCalculatorMap = new Map([
        ['north_east', getControlPointAtNorthEast],
        ['north_west', getControlPointAtNorthWest],
        ['south_east', getControlPointAtSouthEast],
        ['south_west', getControlPointAtSouthWest]
    ]);

    const reverseQuadrantsMap = new Map([
        ['east', ['north_west', 'south_west']],
        ['west', ['north_east', 'south_east']],
        ['north', ['south_west', 'south_east']],
        ['south', ['north_west', 'north_east']],
        ['center', []]
    ]);

    function getEastStartPointOffset(quadrant: string, defaultPointOffset: CurvePointOffset) {
        if (quadrant === 'north_west') {
            return {
                aroundDirection: 'north_west',
                left: defaultPointOffset.left,
                right: controlPointOffset,
                top: defaultPointOffset.top,
                bottom: defaultPointOffset.top
            } as CurvePointOffset;
        }
        if (quadrant === 'south_west') {
            return {
                aroundDirection: 'south_west',
                left: defaultPointOffset.left,
                right: controlPointOffset,
                top: defaultPointOffset.top,
                bottom: defaultPointOffset.bottom
            } as CurvePointOffset;
        }
        return defaultPointOffset;
    }

    function getWestStartPointOffset(quadrant: string, defaultPointOffset: CurvePointOffset) {
        if (quadrant === 'north_east') {
            return {
                left: controlPointOffset,
                right: defaultPointOffset.right,
                top: defaultPointOffset.top,
                bottom: defaultPointOffset.top
            } as CurvePointOffset;
        }
        if (quadrant === 'south_east') {
            return {
                left: controlPointOffset,
                right: defaultPointOffset.right,
                top: defaultPointOffset.top,
                bottom: defaultPointOffset.bottom
            } as CurvePointOffset;
        }
        return defaultPointOffset;
    }

    function getNorthStartPointOffset(quadrant: string, defaultPointOffset: CurvePointOffset) {
        if (quadrant === 'south_west') {
            return {
                left: defaultPointOffset.left,
                right: defaultPointOffset.right,
                top: controlPointOffset,
                bottom: defaultPointOffset.top
            } as CurvePointOffset;
        }
        if (quadrant === 'south_east') {
            return {
                left: defaultPointOffset.left,
                right: defaultPointOffset.right,
                top: controlPointOffset,
                bottom: defaultPointOffset.bottom
            } as CurvePointOffset;
        }
        return defaultPointOffset;
    }

    function getSouthStartPointOffset(quadrant: string, defaultPointOffset: CurvePointOffset) {
        if (quadrant === 'north_west') {
            return {
                left: defaultPointOffset.left,
                right: defaultPointOffset.right,
                top: defaultPointOffset.top,
                bottom: controlPointOffset
            } as CurvePointOffset;
        }
        if (quadrant === 'north_east') {
            return {
                left: defaultPointOffset.left,
                right: defaultPointOffset.right,
                top: defaultPointOffset.top,
                bottom: controlPointOffset
            } as CurvePointOffset;
        }
        return defaultPointOffset;
    }

    function getCenterStartPointOffset(quadrant: string, defaultPointOffset: CurvePointOffset) {
        return defaultPointOffset;
    }

    const getStartPointOffsetMap = new Map([
        ['east', getEastStartPointOffset],
        ['west', getWestStartPointOffset],
        ['north', getNorthStartPointOffset],
        ['south', getSouthStartPointOffset],
        ['center', getCenterStartPointOffset]
    ]);

    function calculateBezierCurveBoundray(startPoint: CurvePoint, endPoint: CurvePoint, startPointPosition: string, endPoinPosition: string) {
        const horizontalDistance = endPoint.x - startPoint.x;
        const verticalDistance = endPoint.y - startPoint.y;
        const horizontalDirection = horizontalDistance >= 0 ? 'east' : 'west';
        const verticalDirection = verticalDistance >= 0 ? 'south' : 'north';
        const quadrant = `${verticalDirection}_${horizontalDirection}`;
        const reversedQuadrants = reverseQuadrantsMap.get(startPointPosition) || [];

        const shouldReversStartPointPosition = reversedQuadrants.includes(quadrant);

        const getStartPointOffset = getStartPointOffsetMap.get(startPointPosition) as (quadrant: string, defaultPointOffset: CurvePointOffset) => CurvePointOffset;
        const startPointOffset = shouldReversStartPointPosition ? getStartPointOffset(quadrant, defaultPointOffset) : defaultPointOffset;

        const widthBetweenTwoPoint = Math.abs(horizontalDistance);
        const heightBetweenTwoPoint = Math.abs(verticalDistance);

        const getControlPoint = controlPointCalculatorMap.get(`${verticalDirection}_${horizontalDirection}`) as (
            width: number, height: number, shouldReserve: boolean, startPointOffset: CurvePointOffset,
            startPointPosition: string, pathDirection: string, endPoinPosition: string
        ) => [CurvePoint, CurvePoint, CurvePoint];
        const [firstControlPoint, secondControlPoint, secondControlPointOffset] = getControlPoint(
            widthBetweenTwoPoint, heightBetweenTwoPoint, shouldReversStartPointPosition, startPointOffset, startPointPosition, quadrant, endPoinPosition
        );

        const secondControlPointOffsetWidth = horizontalDirection === 'west' ? (secondControlPointOffset.x < 0 ? Math.abs(secondControlPointOffset.x) : 0) : (secondControlPointOffset.x > 0 ? secondControlPointOffset.x : 0);
        const width = (startPointOffset.left + startPointOffset.right) + widthBetweenTwoPoint + secondControlPointOffsetWidth;
        const secondControlPointOffsetHeight = verticalDirection === 'north' ? (secondControlPointOffset.y < 0 ? Math.abs(secondControlPointOffset.y) : 0) : (secondControlPointOffset.y > 0 ? secondControlPointOffset.y : 0);
        const height = (startPointOffset.top + startPointOffset.bottom) + heightBetweenTwoPoint + secondControlPointOffsetHeight;

        const left = horizontalDirection === 'east' ? startPoint.x - startPointOffset.left : startPoint.x + startPointOffset.right - width;
        const top = verticalDirection === 'south' ? startPoint.y - startPointOffset.top : startPoint.y + startPointOffset.bottom - height;

        const startXInBoundray = horizontalDirection === 'east' ? startPointOffset.left : width - startPointOffset.right;
        const startYInBoundray = verticalDirection === 'north' ? height - startPointOffset.bottom : startPointOffset.top;

        const endXInBoundray = horizontalDirection === 'west' ? (secondControlPointOffset.x < 0 ? Math.abs(secondControlPointOffset.x) : 0) : (secondControlPointOffset.x < 0 ? width : width - secondControlPointOffset.x);
        const endYInBoundray = verticalDirection === 'north' ? (secondControlPointOffset.y < 0 ? Math.abs(secondControlPointOffset.y) : 0) : (secondControlPointOffset.y < 0 ? height : height - secondControlPointOffset.y);

        const startPoinInBoundray = { x: startXInBoundray, y: startYInBoundray };
        const endPoinInBoundray = { x: endXInBoundray, y: endYInBoundray };

        return { width, height, left, top, startPoinInBoundray, firstControlPoint, secondControlPoint, endPoinInBoundray };
    }

    function setCurveBoundray(curveElement: HTMLElement, left: number, top: number, width: number, height: number) {
        curveElement.style.position = "absolute";
        curveElement.style.alignItems = "center";
        curveElement.style.left = `${left}px`;
        curveElement.style.top = `${top}px`;
        curveElement.style.width = width + "px";
        curveElement.style.height = height + "px";
    }

    function updateCurvePath(
        curveElement: HTMLElement, width: number, height: number,
        startPoinInBoundray: CurvePoint, firstControlPoint: CurvePoint, secondControlPoint: CurvePoint, endPoinInBoundray: CurvePoint
    ) {
        const curveSVGElement = curveElement.childNodes[0] as Element;
        curveSVGElement.setAttribute("width", String(width));
        curveSVGElement.setAttribute("height", String(height));
        curveElement.appendChild(curveSVGElement);

        const curvePath = curveSVGElement.childNodes[0] as Element;

        const startPosition = `${startPoinInBoundray.x} ${startPoinInBoundray.y}`;
        const firstControlPointPosition = `${firstControlPoint.x} ${firstControlPoint.y}`;
        const secondControlPointPosition = `${secondControlPoint.x} ${secondControlPoint.y}`;
        const endPosition = `${endPoinInBoundray.x} ${endPoinInBoundray.y}`;
        const curvePathData = "M ".concat(startPosition)
            .concat(" C ").concat(firstControlPointPosition).concat(',' + secondControlPointPosition + ',')
            .concat(endPosition);

        curvePath.setAttribute("d", curvePathData);
        curvePath.setAttribute("fill", "none");
        curvePath.setAttribute("stroke", "#4d53e8");
        curvePath.setAttribute("stroke-width", "2");
        curvePath.setAttribute("class", "");
    }

    function drawBezier(curveElement: HTMLElement, startPoint: CurvePoint, endPoint: CurvePoint, startPointPosition: string, endPoinPosition: string) {
        const {
            width, height, left, top,
            startPoinInBoundray, firstControlPoint, secondControlPoint, endPoinInBoundray
        } = calculateBezierCurveBoundray(startPoint, endPoint, startPointPosition, endPoinPosition);
        setCurveBoundray(curveElement, left, top, width, height);
        updateCurvePath(curveElement, width, height, startPoinInBoundray, firstControlPoint, secondControlPoint, endPoinInBoundray);
    }

    function drawing(curveId: string, startPoint: CurvePoint, endPoint: CurvePoint, startPointPosition: string, endPoinPosition: string) {
        const curveElement = createCurveElement(curveId);
        drawBezier(curveElement, startPoint, endPoint, startPointPosition, endPoinPosition);
    }

    function convertToNumber(px: string) {
        return Number(px.replace('px', ''));
    }

    function getAncherPointPosition(ancherElement: HTMLElement | null) {
        if (ancherElement !== null) {
            const flowNodeElement = ancherElement.parentNode as any;
            const flowNodeX = convertToNumber(flowNodeElement.style.left);
            const flowNodeY = convertToNumber(flowNodeElement.style.top);
            return {
                x: flowNodeX + ancherElement.offsetLeft + ancherElement.offsetWidth / 2,
                y: flowNodeY + ancherElement.offsetTop + ancherElement.offsetHeight / 2
            };
        }
        return { x: 0, y: 0 };
    };

    function getPointRange(startPointId: string, endPointId: string) {
        const startAncherElement = document.getElementById(startPointId);
        const endAcherElement = document.getElementById(endPointId);
        const startPoint1 = getAncherPointPosition(startAncherElement);
        const endPoint1 = getAncherPointPosition(endAcherElement);
        const startX = startPoint1.x + 2;
        const startY = startPoint1.y;
        const endX = endPoint1.x - 2;
        const endY = endPoint1.y;
        const startPoint = { x: startX, y: startY };
        const endPoint = { x: endX, y: endY };
        return { startPoint, endPoint };
    }

    function connect(startPointId: string, endPointId: string, startPointPosition: string, endPoinPosition: string) {
        const { startPoint, endPoint } = getPointRange(startPointId, endPointId);
        drawing(`${startPointId}_${endPointId}`, startPoint, endPoint, startPointPosition, endPoinPosition);
    }

    return { connect, drawing };

}
