import { start } from "repl";
import { Connection, UseConnections } from "./types";

export function useConnections(connections: string[][]): UseConnections {
    const connectionMap = new Map<string, string[]>();

    const nodeConnectionMap = new Map<string, Connection[]>();

    function addConnectionToNode(nodeId: string, connection: Connection) {
        const connectionsOfTheNode = nodeConnectionMap.get(nodeId) || [];
        const isAlreadyConnected = connectionsOfTheNode.filter(
            (value: Connection) => connection.from === value.from && connection.to === value.to
        ).length > 0;
        if (!isAlreadyConnected) {
            connectionsOfTheNode.push(connection);
            nodeConnectionMap.set(nodeId, connectionsOfTheNode);
        }
    }

    function addConnection(fromNodeId: string, startAncherId: string, toNodeId: string, endAncherId: string) {
        const targetNodeSet = new Set(connectionMap.get(startAncherId) || []);
        targetNodeSet.add(endAncherId);
        connectionMap.set(startAncherId, [...targetNodeSet]);

        const connection = { from: startAncherId, to: endAncherId, type: 'Curve' } as Connection;
        addConnectionToNode(fromNodeId, connection);
        addConnectionToNode(toNodeId, connection);
    }

    function removeConnection(startAncherId: string, endAcherId: string) {
        if (connectionMap.has(startAncherId)) {
            const targetNodeSet = new Set(connectionMap.get(startAncherId) || []);
            targetNodeSet.delete(endAcherId);
            if (targetNodeSet.size === 0) {
                connectionMap.delete(startAncherId);
            } else {
                connectionMap.set(startAncherId, [...targetNodeSet]);
            }
        }
    }

    function getConnectionsOfNode(nodeId: string): Connection[] {
        return nodeConnectionMap.get(nodeId) || [];
    }

    return { addConnection, getConnectionsOfNode };

}
