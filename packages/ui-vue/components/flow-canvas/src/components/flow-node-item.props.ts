import { ExtractPropTypes } from "vue";

export const flowNodeItemProps = {
    id: { type: String, default: '' },
    type: { type: String, default: '' },
    /**
     * 组件值
     */
    modelValue: { type: Object },
    x: { type: Number, default: 0 },
    y: { type: Number, default: 0 },
    connection: { type: Array, default: [] }
} as Record<string, any>;

export type FlowNodeItemProps = ExtractPropTypes<typeof flowNodeItemProps>;
