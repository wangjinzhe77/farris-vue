import { computed, defineComponent, inject, ref, watch } from "vue";
import { FlowNodeItemProps, flowNodeItemProps } from "./flow-node-item.props";
import { Connection, UseBezierCurve, UseConnections, UseDrawingBezier } from "../composition/types";
import { FDesignerCanvas } from '../../../designer-canvas';

export default defineComponent({
    name: 'FFlowNodeItem',
    props: flowNodeItemProps,
    emits: [],
    setup(props: FlowNodeItemProps, context) {
        const id = ref(props.id);
        const schema = ref(props.modelValue);
        const startAncherElement = ref<HTMLElement>();
        const useDrawingBezierComposition = inject<UseDrawingBezier>('use-drawing-bezier-composition') as UseDrawingBezier;
        const { drawFrom, eraseDrawingLine, finishToDraw, getAncherPointPosition, isAncherPoint } = useDrawingBezierComposition;
        const useBezierCurveComposition = inject<UseBezierCurve>('use-bezier-curve-composition') as UseBezierCurve;
        const { connect } = useBezierCurveComposition;
        const useConnectionsComposition = inject<UseConnections>('use-connections-composition') as UseConnections;
        const { addConnection, getConnectionsOfNode } = useConnectionsComposition;

        const connectionMap = new Map<string, string[]>(props.connection);

        const flowNodeItemStyle = computed(() => {
            const styleObject = {
                'left': `${props.x}px`,
                'top': `${props.y}px`
            } as Record<string, any>;
            return styleObject;
        });

        function connectAncherElement(startAncherElement: HTMLElement, endAncherElement: HTMLElement) {
            if (isAncherPoint(startAncherElement) && isAncherPoint(endAncherElement)) {
                const starAncherPosition = getAncherPointPosition(startAncherElement);
                const endAncherPosition = getAncherPointPosition(endAncherElement);
                connect(startAncherElement.id, endAncherElement.id, starAncherPosition, endAncherPosition);
                // const targetNodeSet = new Set(connectionMap.get(startAncherElement.id) || []);
                // targetNodeSet.add(endAncherElement.id);
                // connectionMap.set(startAncherElement.id, [...targetNodeSet]);
                const startAncherId = startAncherElement.id;
                const endAcherId = endAncherElement.id;
                const fromNodeId = startAncherId.split(/(-left-point|-right-point|-top-point|-bottom-point)/, 1)[0];
                const toNodeId = endAcherId.split(/(-left-point|-right-point|-top-point|-bottom-point)/, 1)[0];
                addConnection(fromNodeId, startAncherId, toNodeId, endAcherId);
            }
        }

        function onMouseup(event: MouseEvent) {
            document.removeEventListener('mouseup', onMouseup);
            finishToDraw(event);
            eraseDrawingLine(`${startAncherElement.value?.id}_curve_to`);
            connectAncherElement(startAncherElement.value as HTMLElement, event.target as HTMLElement);
        }

        function onMousedownToDrawingBezier(nodeId: string, payload: MouseEvent) {
            startAncherElement.value = document.getElementById(nodeId) as HTMLElement;
            drawFrom(nodeId, payload);
            document.addEventListener('mouseup', onMouseup);
            payload.stopPropagation();
        }

        watch([() => props.x, () => props.y], () => {
            // Array.from(connectionMap.entries()).forEach(([startAncherId, endAnchers]) => {
            //     const startAncherElement = document.getElementById(startAncherId) as HTMLElement;
            //     endAnchers.forEach((endAncherId: string) => {
            //         const endAncherElement = document.getElementById(endAncherId) as HTMLElement;
            //         if (startAncherElement && endAncherElement) {
            //             connectAncherElement(startAncherElement, endAncherElement);
            //         }
            //     });
            // });
            const connectionsOfTheNode = getConnectionsOfNode(id.value);
            if (connectionsOfTheNode.length) {
                connectionsOfTheNode.forEach((connection: Connection) => {
                    const startAncherElement = document.getElementById(connection.from);
                    const endAncherElement = document.getElementById(connection.to);
                    if (startAncherElement && endAncherElement) {
                        connectAncherElement(startAncherElement, endAncherElement);
                    }
                });
            }
        });

        const flowNodeContentClass = computed(() => {
            const classObject = {
                'node-content': true,
            } as Record<string, boolean>;
            return classObject;
        });

        return () => {
            return <div id={id.value} class="br-node" style={flowNodeItemStyle.value}>
                <div id={`${id.value}-left-point`} class="f-flow-ancher circle-left"
                    onMousedown={(payload: MouseEvent) => onMousedownToDrawingBezier(`${id.value}-left-point`, payload)}></div>
                <div id={`${id.value}-top-point`} class="f-flow-ancher circle-top"
                    onMousedown={(payload: MouseEvent) => onMousedownToDrawingBezier(`${id.value}-top-point`, payload)}></div>
                <div id={`${id.value}-content`} class={flowNodeContentClass.value} style="min-width:200px;min-height:200px;">
                    {schema.value && <FDesignerCanvas v-model={schema.value} ></FDesignerCanvas>}
                </div>
                <div id={`${id.value}-right-point`} class="f-flow-ancher circle-right"
                    onMousedown={(payload: MouseEvent) => onMousedownToDrawingBezier(`${id.value}-right-point`, payload)}></div>
                <div id={`${id.value}-bottom-point`} class="f-flow-ancher circle-bottom"
                    onMousedown={(payload: MouseEvent) => onMousedownToDrawingBezier(`${id.value}-bottom-point`, payload)}></div>
            </div>;
        };
    }
});
