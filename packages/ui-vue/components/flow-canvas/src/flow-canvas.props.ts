import { ExtractPropTypes } from "vue";

export const flowCanvasProps = {
    /**
     * 组件值
     */
    modelValue: { type: Object, default: {} },
} as Record<string, any>;

export type FlowCanvasProps = ExtractPropTypes<typeof flowCanvasProps>;
