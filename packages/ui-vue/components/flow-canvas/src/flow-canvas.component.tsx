import { computed, defineComponent, onMounted, provide, ref } from "vue";
import { FlowCanvasProps, flowCanvasProps } from "./flow-canvas.props";

import './flow-canvas.css';
import { useCurve } from "./composition/use-curve";
import { useDrawing } from "./composition/use-drawing";
import { useBezierCurve } from "./composition/use-bezier-curve";
import { useDrawingBezier } from "./composition/use-drawing-bezier";

import FFlowNodeItem from './components/flow-node-item.component';
import { useConnections } from "./composition/use-connections";

export default defineComponent({
    name: 'FFlowCanvas',
    props: flowCanvasProps,
    emits: [],
    setup(props: FlowCanvasProps, context) {
        const schema = ref();
        const useBezierCurveComposition = useBezierCurve();
        const useDrawingBezierComposition = useDrawingBezier(useBezierCurveComposition);
        const useConnectionsComposition = useConnections([]);

        provide('use-bezier-curve-composition', useBezierCurveComposition);
        provide('use-connections-composition', useConnectionsComposition);
        provide('use-drawing-bezier-composition', useDrawingBezierComposition);

        const svgContainerStyle = computed(() => {
            const styleObject = {
                'transition': 'transform 0.2s',
                'background-color': '#F2F3F5',
                'height': '100%',
                'width': '100%',
                'position': 'relative'
            } as Record<string, any>;
            return styleObject;
        });

        onMounted(() => {
            schema.value = props.modelValue;
        });

        const movingNode = ref();
        const movingNodeOriginalX = ref();
        const movingNodeOriginalY = ref();
        const startX = ref();
        const startY = ref();

        function movingFlowNodeItem(mouseMovingArgs: MouseEvent) {
            const leftOffset = mouseMovingArgs.clientX - startX.value;
            const topOffset = mouseMovingArgs.clientY - startY.value;
            if (movingNode.value && movingNode.value.position) {
                movingNode.value.position.x = movingNodeOriginalX.value + leftOffset;
                movingNode.value.position.y = movingNodeOriginalY.value + topOffset;
            }
        }

        function finishToMoveFlowNodeItem() {
            document.removeEventListener('mousemove', movingFlowNodeItem);
            document.removeEventListener('mouseup', finishToMoveFlowNodeItem);
            movingNode.value = undefined;
            startX.value = undefined;
            startY.value = undefined;
        }

        function onMousedown(flowNodeItem: Record<string, any>, payload: MouseEvent) {
            movingNode.value = flowNodeItem;
            movingNodeOriginalX.value = flowNodeItem.position.x;
            movingNodeOriginalY.value = flowNodeItem.position.y;
            startX.value = payload.clientX;
            startY.value = payload.clientY;
            document.addEventListener('mousemove', movingFlowNodeItem);
            document.addEventListener('mouseup', finishToMoveFlowNodeItem);
        }

        return () => {
            return (
                <div class="f-flow-canvas editorDiv flex-fill h-100">
                    <div id="svg-container" class="f-struct-wrapper flex-fill" style={svgContainerStyle.value}>
                        {
                            schema.value && schema.value.contents.map((flowNodeItem: Record<string, any>) =>
                                <FFlowNodeItem v-model={flowNodeItem.content[0]}
                                    id={flowNodeItem.id} x={flowNodeItem.position.x} y={flowNodeItem.position.y}
                                    onMousedown={(payload: MouseEvent) => onMousedown(flowNodeItem, payload)}></FFlowNodeItem>
                            )
                        }
                    </div>
                </div>
            );
        };
    }
});
