/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver, createTabsEventHandlerResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import tabsSchema from './schema/tabs.schema.json';


export type TabType = 'fill' | 'pills' | 'default' | 'one-page';
export type TabPosition = 'left' | 'right' | 'top' | 'bottom';
export type TabHeaderJustifyMode = 'left' | 'center' | 'right';

export const tabsProps = {
    /** 标签页头部区域的展示类型 */
    tabType: { type: String as PropType<TabType>, default: 'default' },
    autoTitleWidth: { type: Boolean, default: false },
    titleLength: { type: Number, default: 7 },
    position: { type: String as PropType<TabPosition>, default: 'top' },
    showDropdown: { type: Boolean, default: true },
    scrollStep: { type: Number, default: 10 },
    autoResize: { type: Boolean, default: false },
    selectedTab: { type: String, default: '' },
    width: { type: Number },
    height: { type: Number },
    searchBoxVisible: { type: Boolean, default: true },
    titleWidth: { type: Number, default: 0 },
    customClass: { type: String, default: '' },
    /** 同上selectedTab属性 */
    activeId: { type: String },
    /** 标签页内容区域是否填充 */
    fill: { type: Boolean, default: false },
    justifyContent: { type: String as PropType<TabHeaderJustifyMode>, default: 'left' }
} as Record<string, any>;

export type TabsProps = ExtractPropTypes<typeof tabsProps>;
// !!!!!!!!!!!!!!
export const tabsPropsResolver = createPropsResolver<TabsProps>(tabsProps, tabsSchema, schemaMapper, schemaResolver);
export const tabsDesignProps = Object.assign({},tabsProps,{
    componentId: { type: String, default: '' }
});
export type TabsDesignProps = ExtractPropTypes<typeof tabsDesignProps>;
export const tabsDesignPropsResolver = createPropsResolver<TabsDesignProps>(tabsDesignProps, tabsSchema, schemaMapper, schemaResolver);
export const eventHandlerResolver = createTabsEventHandlerResolver();
