import { DesignerHTMLElement } from '../../../designer-canvas/src/composition/types';
import dragula from '@farris/designer-dragula';
import { DesignerItemContext } from '../../../designer-canvas';
import { nextTick, Ref } from 'vue';
import { canvasChanged } from '../../../designer-canvas/src/composition/designer-canvas-changed';
import { TabPageContext } from './types';

export function tabHeaderItemDragula(designItemContext: DesignerItemContext, tabPages: Ref<TabPageContext[]>) {

    let dragulaInstance: any;

    function onDrop(element: DesignerHTMLElement, target: DesignerHTMLElement, sibling: DesignerHTMLElement) {
        const tabSchema = designItemContext.schema;
        if (!tabSchema?.contents) {
            return;
        }
        let targetIndex;

        if (sibling) {
            const targetPageElementId = sibling.getAttribute('id');
            targetIndex = Array.from(target.children).findIndex(e => e.getAttribute('id') === targetPageElementId);
        } else {
            targetIndex = Array.from(target.children).length;
        }
        const sourcePageElementId = element.getAttribute('id');
        const sourceIndex = Array.from(target.children).findIndex(e => e.getAttribute('id') === sourcePageElementId);
        const sourcePageSchema = tabSchema.contents[sourceIndex];

        if (sourceIndex !== targetIndex && sourceIndex > -1 && targetIndex > -1) {
            if (sourceIndex < targetIndex) {
                targetIndex = targetIndex - 1;
            }
            // 更新工具栏控件schema结构
            tabSchema.contents.splice(sourceIndex, 1);
            tabSchema.contents.splice(targetIndex, 0, sourcePageSchema);

            // 更新页面tabPage展示顺序
            if (tabPages.value) {
                const currentTabPage = tabPages.value[sourceIndex];
                tabPages.value.splice(sourceIndex, 1);
                tabPages.value.splice(targetIndex, 0, currentTabPage);
            }

            nextTick(() => {
                // 重新计算操作按钮位置
                if (element.className.includes('dgComponentSelected')) {
                    element.classList.remove('dgComponentSelected');
                    element.classList.remove('dgComponentFocused');
                    element.click();
                }
                // 触发更新控件树
                canvasChanged.value++;
            });
        }
    }

    /**
     * 初始化头部页签拖拽
     */
    function initHeaderDragula(navElement: DesignerHTMLElement, navParentContainer: DesignerHTMLElement) {
        if (dragulaInstance) {
            dragulaInstance.destroy();
        }
        if (!dragula || !navElement || !navParentContainer) {
            return;
        }

        dragulaInstance = dragula([navElement], {
            mirrorContainer: navParentContainer,
            direction: 'horizontal',
            moves(element: DesignerHTMLElement): boolean {
                // 防止拖拽加号图标（新增页签的图标）
                return !element.classList.contains('no-drag');
            },
            getMirrorText(element: DesignerHTMLElement): string {
                return element.innerText;
            },
            accepts(el: HTMLElement, target: DesignerHTMLElement, source: HTMLElement, sibling: DesignerHTMLElement): boolean {
                return sibling && sibling.className.includes('nav-item');

            }
        })
            .on('drop', (element: DesignerHTMLElement, target: DesignerHTMLElement, source: DesignerHTMLElement, sibling: DesignerHTMLElement
            ) => onDrop(element, target, sibling));

    }

    return {
        dragulaInstance,
        initHeaderDragula
    };
}
