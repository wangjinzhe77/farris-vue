/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref, SetupContext, ShallowRef, nextTick, ref } from 'vue';
import { TabsProps } from '../tabs.props';
import { TabPageContext, UseDesignTabs } from '../composition/types';
import { ComponentSchema, DesignerComponentInstance, DesignerItemContext } from '../../../designer-canvas/src/types';
import { canvasChanged } from '../../../designer-canvas/src/composition/designer-canvas-changed';
import { getSchemaByType } from '../../../dynamic-resolver/src/schema-resolver';

export function useDesignTabs(
    props: TabsProps,
    context: SetupContext,
    tabNavigationElementRef: ShallowRef<any>,
    designerItemContext: DesignerItemContext
): UseDesignTabs {
    // 存储tab的props数组
    const tabPages = ref<TabPageContext[]>([]);
    // 激活状态的tabId
    const activeId = ref(props.activeId || '');
    // 显示下拉面板
    const hideDropDown = ref(true);
    const toolbarSchema = ref();
    const toolbarItems = ref<any>([]);

    function setActiveId(tabPages: Ref<TabPageContext[]>) {
        const index = tabPages.value.findIndex((tabPage: TabPageContext) =>
            tabPage.props.show !== false && !activeId.value);
        if (!activeId.value && index !== -1) {
            activeId.value = tabPages.value[index].props.id;
        }
    }

    function addClass(element: HTMLElement, className: string) {
        if (!element.classList.contains(className)) {
            element.classList.add(className);
        }
    }
    function removeClass(element: HTMLElement, className: string) {
        element.classList.remove(className);
    }
    function applyTitleTextCustomClass(element: HTMLElement) {
        const { parentElement } = element;
        if (!parentElement) {
            return;
        }
        if (element.scrollWidth > parentElement.offsetWidth) {
            addClass(element, 'farris-title-text-custom');
        } else {
            removeClass(element, 'farris-title-text-custom');
        }
    }
    function changeTitleStyle(tabNavigationElementRef: ShallowRef<any>) {
        if (props.autoTitleWidth) {
            return;
        }
        const textElement = tabNavigationElementRef.value?.querySelectorAll('.st-tab-text');
        if (!textElement) {
            return;
        }
        textElement.forEach((element: HTMLElement) => applyTitleTextCustomClass(element));
    }
    const stopBubble = (event: MouseEvent) => {
        event.preventDefault();
        event.stopPropagation();
    };
    function setAcitveTab(tabId: string) {
        tabPages.value = tabPages.value.filter((tabPage: TabPageContext) => tabPage.props.id !== tabId);
        if (activeId.value === tabId) {
            activeId.value = '';
            setActiveId(tabPages);
        }
    }
    function removeTab(event: MouseEvent, targetTabId: string, closeDropdown = false) {
        const removeIndex = tabPages.value.findIndex((tabPage: TabPageContext) => tabPage.props.id === targetTabId);
        setAcitveTab(targetTabId);
        stopBubble(event);
        nextTick(() => {
            changeTitleStyle(tabNavigationElementRef);
            if (closeDropdown) {
                hideDropDown.value = true;
            }
            // const index = tabPages.value.findIndex((tabPage: TabPageContext) => tabPage.props.id === activeId.value);
            context.emit('tabRemove', {
                removeIndex,
                removeId: targetTabId,
                activeId: activeId.value
            });
        });
    }
    /**
     * 切换标签页按钮时，更新页面的渲染内容
     */
    function changeToolbarItems() {
        const activePage = tabPages.value.find((tabPageContext: TabPageContext) => tabPageContext.props.id === activeId.value);
        toolbarItems.value = [];
        if (activePage && activePage.props.toolbar) {
            // 解决兼容问题
            const buttonProperty = Object.prototype.hasOwnProperty.call(activePage.props.toolbar, 'contents') ? 'contents' : 'buttons';
            if (activePage.props.toolbar[buttonProperty] && activePage.props.toolbar[buttonProperty].length) {
                toolbarItems.value = [...activePage.props.toolbar[buttonProperty]];
            }
        }

        const tabsSchema = designerItemContext.schema.contents;
        const currentTabSchema = tabsSchema?.find((schema: ComponentSchema) => schema.id === activeId.value);
        toolbarSchema.value = currentTabSchema?.toolbar;
    }
    function selectTabByTabId(targetTabId: string) {
        const prevId = activeId.value;
        activeId.value = targetTabId;
        changeToolbarItems();
        context.emit('tabChange', {
            prevId,
            nextId: activeId.value
        });
    }

    function selectTab(targetTabId: string) {
        selectTabByTabId(targetTabId);
    };

    /**
     * 增加新页签
     * @param target 标签页上下文对象
     */
    function addTab(target: TabPageContext) {
        const index = tabPages.value.findIndex((tabPage: TabPageContext) => tabPage.props.id === target.props.id);
        if (index === -1) {
            tabPages.value.push(target);
        }
    };

    /**
     * 更新页签标题
     * @param target 标签页上下文对象
     */
    function updateTab(target: TabPageContext) {
        const index = tabPages.value.findIndex((tabPage: TabPageContext) => tabPage.props.id === target.props.id);
        if (index === -1) {
            return;
        }
        tabPages.value.forEach((tabPage: TabPageContext) => {
            if (tabPage.props.id === target.props.id) {
                tabPage = target;
            }
        });
        nextTick(() => {
            changeTitleStyle(tabNavigationElementRef);
        });
    };

    function preventEvent(payload: MouseEvent) {
        if (!payload) {
            return;
        }
        payload.stopPropagation();
        payload.preventDefault();
    }
    /**
     * 触发tabPage内部控件的删除后事件
     * @param tabId 
     */
    function removeComponentInTabPage(tabId: string) {
        const tabContentElement = designerItemContext.designerItemElementRef.value.querySelector(`.farris-tabs-content`);
        const tabPageElement: any = tabContentElement?.querySelector(`#${tabId}-design-item`);
        if (tabPageElement?.componentInstance?.value.onRemoveComponent) {
            tabPageElement.componentInstance.value.onRemoveComponent();
        }
    }
    /**
     * 设计时，删除标签页事件
     */
    function removeDesignTab(payload: MouseEvent, tabId: string) {
        preventEvent(payload);

        removeComponentInTabPage(tabId);
        const tabs = designerItemContext.schema.contents;
        const index = tabs?.findIndex((schema: ComponentSchema) => schema.id === tabId);
        if (index === -1 || index === undefined) {
            return;
        }
        tabs?.splice(index, 1);
        setAcitveTab(tabId);
        changeToolbarItems();

        // 触发刷新属性面板
        designerItemContext?.setupContext?.emit('selectionChange');
        // 触发更新控件树
        canvasChanged.value++;
    }

    function deactiveElements() {
        Array.from(document.getElementsByClassName('dgComponentFocused') as HTMLCollectionOf<HTMLElement>).forEach(
            (element: HTMLElement) => element.classList.remove('dgComponentFocused')
        );
        const currentSelectedElements = document.getElementsByClassName('dgComponentSelected') as HTMLCollectionOf<HTMLElement>;
        Array.from(currentSelectedElements).forEach((element: HTMLElement) => element.classList.remove('dgComponentSelected'));
    }
    function activeEventElement(payload: MouseEvent) {
        const currentTarget = payload.currentTarget as HTMLElement;
        currentTarget.classList.add('dgComponentFocused', 'dgComponentSelected');
    }
    function emitSelectionChange(tabId: string, componentInstance: DesignerComponentInstance) {
        const { contents = [] } = designerItemContext.schema;
        const index = contents?.findIndex((schema: ComponentSchema) => schema.id === tabId);
        const tabPage = contents[index];
        designerItemContext?.setupContext?.emit('selectionChange', tabPage.type, tabPage, props.componentId, componentInstance);
    }

    /**
     * 判断页签是否在可视化区域内
     * @param navEle 单个标签页nav div
     * @param navTabsEle  整个标签页nav div的父级
     */
    function isNavElementInViewport(navEle: HTMLElement, navTabsEle: HTMLElement) {

        const container = navTabsEle.getBoundingClientRect();
        const box = navEle.getBoundingClientRect();

        const isRightInView = (box.left + box.width) <= (container.left + container.width);

        const isLeftInView = (box.left + box.width) >= container.left;

        return isRightInView && isLeftInView;
    }
    /**
     * 计算工具栏位置
     * @param tabNavEle 工具栏父级
     */
    function setPositionOfBtnGroupInHeader(tabNavEle: HTMLElement) {
        const toolbar = tabNavEle.querySelector('.component-btn-group') as HTMLElement;
        if (toolbar) {
            toolbar.style.display = '';
            const toolbarRect = toolbar.getBoundingClientRect();
            const divElement = toolbar.querySelector('div');
            if (divElement) {
                const divPanelRect = divElement.getBoundingClientRect();
                divElement.style.left = (toolbarRect.left - divPanelRect.width) + 'px';
                divElement.style.top = toolbarRect.top + 'px';
            }
        }
    }
    /**
     * 横向滚动滚动条时计算工具栏位置
     * @param element 监听滚动的元素
     */
    function listenTabNavElementScroll() {
        if (tabNavigationElementRef && tabNavigationElementRef.value) {
            tabNavigationElementRef.value.addEventListener('scroll', (e) => {
                const selectElement = (e.target as any).querySelector('.dgComponentSelected');
                if (selectElement) {
                    const toolbar = selectElement.querySelector('.component-btn-group');
                    if (isNavElementInViewport(selectElement, e.target)) {
                        setPositionOfBtnGroupInHeader(selectElement);
                    } else {
                        if (toolbar) {
                            toolbar.style.display = 'none';
                        }
                    }
                }
            });
        }

    }
    /**
     * 选中单个页签事件
     */
    function selectDesignTab(payload: MouseEvent, tabId: string, componentInstance: DesignerComponentInstance) {
        deactiveElements();
        preventEvent(payload);
        activeEventElement(payload);
        emitSelectionChange(tabId, componentInstance);

        // 计算操作按钮位置
        const currentTarget = payload.currentTarget as HTMLElement;
        setPositionOfBtnGroupInHeader(currentTarget);
    }
    /**
     * 设置横向滚动条位置
     */
    function setHorizontalScrollbarPosition() {
        if (tabNavigationElementRef && tabNavigationElementRef.value) {
            const targetNavEle = tabNavigationElementRef.value.querySelector('.f-state-active');
            if (targetNavEle) {
                // 设置横向滚动条位置
                const targetNavEleRect = targetNavEle.getBoundingClientRect();
                const containerEleRect = targetNavEle.parentElement.getBoundingClientRect();
                tabNavigationElementRef.value.scrollLeft = targetNavEleRect.left - containerEleRect.left;

                // 计算工具栏位置
                setPositionOfBtnGroupInHeader(targetNavEle);
            }
        }

    }
    /**
     * 标签页新增工具栏
     */
    function addTabPageToolbar(payload: MouseEvent, tabId: string) {
        preventEvent(payload);
        const tabs = designerItemContext.schema.contents;
        const tabPage = tabs?.find((schema: ComponentSchema) => schema.id === tabId);
        if (!tabPage) {
            return;
        }
        if (!tabPage.toolbar || !tabPage.toolbar.buttons) {
            tabPage.toolbar = { buttons: [] };
        }

        const tabToolbarItemSchema = getSchemaByType('tab-toolbar-item') as ComponentSchema;
        tabToolbarItemSchema.id = `tab_toolbar_item_${Math.random().toString().slice(2, 6)}`;
        tabToolbarItemSchema.appearance = { class: 'btn btn-secondary f-btn-ml' };

        tabPage.toolbar.buttons.push(tabToolbarItemSchema);

        toolbarItems.value = [...tabPage.toolbar.buttons];
        toolbarSchema.value = tabPage.toolbar;

        nextTick(() => {
            setHorizontalScrollbarPosition();
        });

    }

    return {
        activeId,
        addTab,
        changeTitleStyle,
        removeTab,
        selectTab,
        selectTabByTabId,
        tabPages,
        updateTab,
        toolbarItems,
        removeDesignTab,
        selectDesignTab,
        toolbarSchema,
        changeToolbarItems,
        addTabPageToolbar,
        listenTabNavElementScroll,
        setHorizontalScrollbarPosition
    };
}
