import { Ref, ShallowRef, computed } from "vue";
import { TabsProps } from "../tabs.props";
import { TabPageContext, UseDesignTabs, UseNav, UseOnePage } from "../composition/types";

import renderTabHeaderItem from './tab-header-item.design.component';
import { ComponentSchema, DesignerComponentInstance } from "../../../designer-canvas/src/types";
import { getSchemaByType } from "../../../dynamic-resolver/src/schema-resolver";
import { UseDesignerRules } from "../../../designer-canvas/src/composition/types";

export default function (
    props: TabsProps,
    tabNavigationElementRef: ShallowRef<any>,
    useNavComposition: UseNav,
    useOnePageComposition: UseOnePage,
    useTabsComposition: UseDesignTabs,
    componentInstance: Ref<DesignerComponentInstance>,
    designerRulesComposition: UseDesignerRules
) {
    const { shouldShowNavigationButtons } = useNavComposition;
    // 存储tab的props数组
    const { tabPages } = useTabsComposition;

    const tabHeaderClass = computed(() => {
        const classObject = {
            'spacer': true,
            'f-utils-fill': true,
            'spacer-sides-dropdown': shouldShowNavigationButtons.value
        } as Record<string, boolean>;
        return classObject;
    });

    const tabHeaderStyle = computed(() => {
        const styleObject = {
            'width': '100%',
            'justify-content': props.justifyContent
        } as Record<string, any>;
        return styleObject;
    });

    const tabNavigatorClass = computed(() => {
        const classObject = {
            'nav': true,
            'farris-nav-tabs': true,
            'flex-nowrap': true,
            'nav-fill': props.tabType === 'fill',
            'nav-pills': props.tabType === 'pills',
            'flex-row': props.position === 'top' || props.position === 'bottom',
            'flex-column': props.position === 'left' || props.position === 'right'
        } as Record<string, boolean>;
        return classObject;
    });
    const tabNavigatorStyle = computed(() => {
        const styleObject = {
            'overflow': 'auto'
        } as Record<string, any>;
        return styleObject;
    });

    const onAddItemClick = function (event: any) {
        const tabPageSchema = getSchemaByType('tab-page') as ComponentSchema;
        tabPageSchema.id = `tab_page_${Math.random().toString().slice(2, 6)}`;
        tabPageSchema.title = `标题`;
        componentInstance.value.schema.contents?.push(tabPageSchema);

        const { selectTabByTabId } = useTabsComposition;
        selectTabByTabId(tabPageSchema.id);
    };

    return () => {
        return (
            <div class={tabHeaderClass.value} style={tabHeaderStyle.value}>
                <ul class={tabNavigatorClass.value} style={tabNavigatorStyle.value} ref={tabNavigationElementRef}>
                    {
                        tabPages.value.map((tabPage: TabPageContext) =>
                            renderTabHeaderItem(props, tabPage.props, tabPage, useOnePageComposition, useTabsComposition, componentInstance))
                    }
                    <li class="nav-item no-drag">
                        <a class="nav-link tabs-text-truncate" title="添加页签" onClick={(event) => onAddItemClick(event)}>
                            <i class="f-icon f-icon-plus" style="font-size: 20px;margin: 0 auto;line-height: 24px;"></i>
                        </a>
                    </li>
                </ul>
            </div>
        );
    };
}
