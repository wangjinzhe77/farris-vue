/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, inject, onMounted, SetupContext, watch, ref, onUnmounted, computed } from 'vue';
import { TabPageContext, TabsContext } from '../composition/types';
import { TabPageProps, tabPageProps } from '../components/tab-page.props';
import FSection from '../../../section/src/section.component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { useDesignerRules } from './tab-page-use-designer-rules';
import { JSX } from 'vue/jsx-runtime';
import { DesignerHostService } from '../../../designer-canvas/src/composition/types';

export default defineComponent({
    name: 'FTabPageDesign',
    props: tabPageProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: TabPageProps, context: SetupContext) {
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerHostService = inject<DesignerHostService>('designer-host-service');
        const designerRulesComposition = useDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);
        componentInstance.value.canNested = false;
        componentInstance.value.canDelete = false;
        componentInstance.value.canMove = false;

        context.expose(componentInstance.value);

        const tabsContext = inject<TabsContext>('tabs') as TabsContext;
        const showContent = ref(true);
        const tabPageContext: TabPageContext = {
            slots: context.slots,
            props
        };
        const tabType = ref(tabsContext?.tabType.value || 'default');

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
            const index = tabsContext.tabPages.value.findIndex((tabPage: TabPageContext) => tabPage.props.id === props.id);
            if (!index || index === -1) {
                tabsContext?.addTab(tabPageContext);
            } else if (index > -1) {
                showContent.value = false;
                console.warn(`已经存在id为${props.id}的页签啦`);
            }
        });

        onUnmounted(() => { });

        const isActivePage = computed(() => props?.id === tabsContext?.activeId.value);

        const tabPageStyle = computed(() => {
            const styleObject = {
                display: isActivePage.value ? '' : 'none'
            } as Record<string, any>;
            return styleObject;
        });

        watch(
            () => props,
            (value) => {
                tabsContext?.updateTab({
                    props: value,
                    slots: context.slots
                });
            },
            {
                immediate: true,
                deep: true
            }
        );

        function sectionRender() {
            const content = context.slots.default?.() as any;
            return (
                <FSection main-title={props.title} class="farris-tab-page">
                    {content}
                </FSection>
            );
        }

        const tabPageClass = computed(() => {
            const classObject = {
                'farris-tab-page': true,
                'drag-container': true,
                'farris-tab-page-active': isActivePage.value
            } as Record<string, boolean>;
            return classObject;
        });

        function defaultRender() {
            const content = context.slots.default?.() as any;
            return (
                <div
                    ref={elementRef}
                    class={tabPageClass.value}
                    data-dragref={`${designItemContext.schema.id}-container`}
                    style={tabPageStyle.value}>
                    {content}
                </div>
            );
        }

        const pageContentRenderMap = new Map<string, () => JSX.Element>([
            ['default', defaultRender],
            ['one-page', sectionRender]
        ]);

        const pageContentRender = pageContentRenderMap.get(tabType.value) || defaultRender;

        return () => {
            // const content = context.slots.default?.() as any;
            return showContent.value ? pageContentRender() : null;
        };
    }
});
