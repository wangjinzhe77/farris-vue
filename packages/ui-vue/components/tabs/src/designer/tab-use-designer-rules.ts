import { nextTick } from "vue";
import { UseTemplateDragAndDropRules } from "../../../designer-canvas/src/composition/rule/use-template-rule";
import { DesignerHostService, DesignerHTMLElement, DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema, DesignerItemContext } from "../../../designer-canvas/src/types";
import { TabPageProperty } from "../property-config/tab-page.property-config";
import { TabsProperty } from "../property-config/tabs.property-config";

export function useDesignerRules(designItemContext: DesignerItemContext, designerHostService?: DesignerHostService): UseDesignerRules {
    const schema = designItemContext.schema as ComponentSchema;

    const dragAndDropRules = new UseTemplateDragAndDropRules();
    const { canMove, canAccept, canDelete } = dragAndDropRules.getTemplateRule(designItemContext, designerHostService);


    /**
      * 判断是否可以接收拖拽新增的子级控件
      */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {
        return canAccept;
    }

    function checkCanDeleteComponent(): boolean {
        return canDelete;
    }

    function checkCanMoveComponent(): boolean {
        return canMove;
    }

    function hideNestedPaddingInDesginerView() {
        return !canMove && !canDelete;
    }

    // 构造属性配置方法
    function getPropsConfig(componentId: string) {
        // 在画布中点击tabPage 和tabs 都会走此方法，这里根据画布中选中的控件来返回各自的属性配置
        const focusedElementId = document.querySelector('.dgComponentFocused')?.id;
        const containerProperty = focusedElementId === `${schema.id}-design-item` ? new TabsProperty(componentId, designerHostService) : new TabPageProperty(componentId, designerHostService);
        return containerProperty.getPropertyConfig(schema);
    }

    /**
     * 修改页签标题属性后，重新计算操作按钮位置
     */
    function updatePositionOfButtonGroup() {
        if (designItemContext.designerItemElementRef.value) {
            nextTick(() => {
                const selectedElement = designItemContext.designerItemElementRef.value.querySelector('.dgComponentSelected') as DesignerHTMLElement;
                if (selectedElement?.className.includes('dgComponentSelected')) {
                    selectedElement.click();
                }
            });
        }
    }
    function onPropertyChanged(event: any) {
        if (!event) {
            return;
        }
        const { changeObject } = event;
        if (changeObject && changeObject.propertyID === 'title') {
            updatePositionOfButtonGroup();
        }
    }

    return {
        canAccepts,
        checkCanDeleteComponent,
        checkCanMoveComponent,
        getPropsConfig,
        hideNestedPaddingInDesginerView,
        onPropertyChanged
    } as UseDesignerRules;

}
