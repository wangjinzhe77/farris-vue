
import { computed, Ref, ref } from "vue";
import { TabPageContext, UseOnePage, UseDesignTabs } from "../composition/types";
import { TabPageProps } from "../components/tab-page.props";
import { TabsProps } from "../tabs.props";
import { FMessageBoxService } from "../../../message-box";
import { DesignerComponentInstance } from "../../../designer-canvas/src/types";

export default function (
    tabsProps: TabsProps,
    props: TabPageProps,
    tabPage: TabPageContext,
    useOnePageComposition: UseOnePage,
    useTabsComposition: UseDesignTabs,
    componentInstance: Ref<DesignerComponentInstance>
) {
    const { activeId, selectTab, removeDesignTab, selectDesignTab, addTabPageToolbar } = useTabsComposition;
    const tabTextClass = computed(() => {
        const classObject = {
            'st-tab-text': true,
            'farris-title-auto': tabsProps.autoTitleWidth
        } as Record<string, boolean>;
        return classObject;
    });

    const tabHeaderItemClass = computed(() => {
        const classObject = {
            'farris-component': true,
            'nav-item': true,
            'f-state-active': props.id === activeId.value,
            'f-state-disabled': props.disabled
        } as Record<string, boolean>;
        classObject[`${props.id}-design-item`] = true;
        return classObject;
    });

    const tabHeaderItemStyle = computed(() => {
        const styleObject = {
            width: `${props.tabWidth}px`
        } as Record<string, any>;
        return styleObject;
    });

    const tabHeaderItemLinkClass = computed(() => {
        const classObject = {
            'nav-link': true,
            'tabs-text-truncate': true,
            'active': props.id === activeId.value,
        } as Record<string, boolean>;
        return classObject;
    });

    function tabTitleRender(tabPageProps: TabPageProps) {
        return <span class={tabTextClass.value} title={tabPageProps.title}>
            {tabPageProps.title}
        </span>;
    }

    function getTabTitleRender(tabPage: TabPageContext) {
        return tabPage.slots.title ? tabPage.slots.title : tabTitleRender;
    }

    const renderTabTitle = getTabTitleRender(tabPage);

    const shouldShowCloseButton = ref(props.removeable);

    function renderCloseButton() {
        return <span class="st-drop-close" style={{ opacity: .6 }}>
            <i class="f-icon f-icon-close"></i>
        </span>;
    }

    function onClick($event: MouseEvent) {
        selectTab(props.id);
        if (tabsProps.tabType === 'one-page') {
            useOnePageComposition.scrollToByPaggId(props.id);
        }
    }
    function onTabHeaderClick(payload: MouseEvent) {
        selectDesignTab(payload, props.id, componentInstance.value);
    }
    function onDeleteButtonClick(payload: MouseEvent) {
        removeDesignTab(payload, props.id);
    }
    function onAddButtonClick(payload: MouseEvent) {
        addTabPageToolbar(payload, props.id);
    }
    function renderDeleteButton() {
        return (
            <div
                role="button"
                class="btn component-settings-button"
                title="删除"
                ref="removeComponent"
                onClick={(payload: MouseEvent) => onDeleteButtonClick(payload)}>
                <i class="f-icon f-icon-yxs_delete"></i>
            </div>
        );
    }
    function renderAddButton() {
        return (
            <div
                role="button"
                class="btn component-settings-button"
                title="新增按钮"
                ref="addComponent"
                onClick={(payload: MouseEvent) => onAddButtonClick(payload)}>
                <i class="f-icon f-icon-plus-circle"></i>
            </div>
        );
    }
    function renderIconPanel() {
        return (
            <div class="component-btn-group" data-noattach="true">
                <div>
                    {renderDeleteButton()}
                    {!props.toolbar || !props.toolbar.buttons || !props.toolbar.buttons.length ? renderAddButton() : ''}
                </div>
            </div>
        );
    }
    return (
        <li onClick={(payload) => onTabHeaderClick(payload)} class={tabHeaderItemClass.value} style={tabHeaderItemStyle.value} id={`${props.id}-design-item`}>
            {renderIconPanel()}
            <a class={tabHeaderItemLinkClass.value} onClick={onClick}>
                {renderTabTitle(props)}
                {shouldShowCloseButton.value && renderCloseButton()}
            </a>
        </li>
    );
}
