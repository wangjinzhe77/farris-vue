 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// 暂未使用
import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver } from '../../../dynamic-resolver';
import { schemaMapper } from '../schema/schema-mapper';
import { tabToolbarItemSchemaResolver } from '../schema/schema-resolver';
import tabToolbarItemSchema from '../schema/tab-toolbar-item.schema.json';

export const tabToolbarItemProps = {
    text: { type: String , default: '' },
    icon: { type: String, default: '' },
} as Record<string, any>;

export type TabToolbarItemProps = ExtractPropTypes<typeof tabToolbarItemProps>;
export const tabToolbarItemResolver = createPropsResolver<TabToolbarItemProps>(tabToolbarItemProps, tabToolbarItemSchema, schemaMapper, tabToolbarItemSchemaResolver);
