import { Ref, watch } from 'vue';
import { DesignerComponentInstance, DesignerItemContext } from '../../../designer-canvas/src/types';
import FResponseToolbarDesign from '../../../response-toolbar/src/designer/response-toolbar.design.component';
import { UseDesignTabs } from '../composition/types';
import { UseDesignerRules } from '../../../designer-canvas/src/composition/types';
import FDesignerInnerItem from '../../../designer-canvas/src/components/designer-inner-item.component';

export default function (
    useTabsComposition: UseDesignTabs,
    componentInstance: Ref<DesignerComponentInstance>,
    designerRulesComposition: UseDesignerRules,
    designerItemContext: DesignerItemContext,
    tabComponentId: string
) {
    const { toolbarItems, toolbarSchema, changeToolbarItems } = useTabsComposition;
    /**
     * 选中单个按钮事件
     */
    function onSelectionChange(schemaType: string, schemaValue: any, componentId: string, componentInstance: any) {
        designerItemContext.setupContext?.emit('selectionChange', schemaType, schemaValue, componentId, componentInstance);
    }
    /**
     * 工具栏新增按钮后事件
     */
    function onAddComponent() {
        changeToolbarItems();
    }
    return (
        <div class="farris-tabs-toolbar" >
            <div class="farris-tabs-inline-flex w-100" style="flex:1">
                <FDesignerInnerItem v-model={toolbarSchema.value}
                    class="w-100 position-relative"
                    canAdd={true}
                    childType="tab-toolbar-item"
                    childLabel="按钮"
                    contentKey="buttons"
                    id={toolbarSchema.value.id}
                    onSelectionChange={onSelectionChange}
                    componentId={tabComponentId}
                    onAddComponent={onAddComponent}>
                    <FResponseToolbarDesign items={toolbarItems.value} componentId={tabComponentId}></FResponseToolbarDesign>
                </FDesignerInnerItem>
            </div>
        </div>
    );
}
