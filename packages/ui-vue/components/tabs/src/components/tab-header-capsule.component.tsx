import { ShallowRef, computed, onMounted, ref, watch } from "vue";
import { TabsProps } from "../tabs.props";
import { TabPageContext, UseNav, UseOnePage, UseTabs } from "../composition/types";
import FCapsule, { CapsuleItem } from '@farris/ui-vue/components/capsule';

export default function (
    props: TabsProps,
    tabNavigationElementRef: ShallowRef<any>,
    useNavComposition: UseNav,
    useOnePageComposition: UseOnePage,
    useTabsComposition: UseTabs
) {
    const capsuleInstance = ref<any>();
    const { shouldShowNavigationButtons } = useNavComposition;
    // 存储tab的props数组
    const { activeId, tabPages, selectTab } = useTabsComposition;

    const capsuleActiveId = ref(activeId.value);

    const tabHeaderItems = computed<CapsuleItem[]>(() => {
        return tabPages.value.map((tabPage: TabPageContext) => {
            return { name: tabPage.props.title, value: tabPage.props.id, show: tabPage.props.show, disabled: tabPage.props.disabled };
        });
    });

    const tabHeaderClass = computed(() => {
        const classObject = {
            'spacer': true,
            'f-utils-fill': true,
            'spacer-sides-dropdown': shouldShowNavigationButtons.value
        } as Record<string, boolean>;
        return classObject;
    });

    const tabHeaderStyle = computed(() => {
        const styleObject = {
            'width': '100%',
            'display': 'flex',
            'justify-content': props.justifyContent
        } as Record<string, any>;
        return styleObject;
    });

    onMounted(() => {
        if (capsuleInstance.value) {
            tabNavigationElementRef.value = capsuleInstance.value.$el;
        }
    });

    function onChange(tabId:string) {
        selectTab(tabId);
    }

    watch(
        () => activeId.value,
        (newActiveId) => {
           if(newActiveId !== capsuleActiveId.value) {
            capsuleActiveId.value = newActiveId;
           }
        },
        {
            immediate: true
        }
    );

    return () => {
        return (
            <div class={tabHeaderClass.value} style={tabHeaderStyle.value}>
                <FCapsule items={tabHeaderItems.value} v-model={capsuleActiveId.value}
                    onChange={onChange} ref={capsuleInstance}></FCapsule>
            </div>
        );
    };
}
