import { BaseControlProperty } from "../../../property-panel/src/composition/entity/base-property";
export class TabPageProperty extends BaseControlProperty {
    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }
    getPropertyConfig(propertyData: any) {
        // 基本信息
        this.getBasicPropConfig(propertyData);
        this.getBehaviorProperties(propertyData);
        return this.propertyConfig;
    }
    getBehaviorProperties(propertyData: any) {
        this.propertyConfig.categories['behavior'] = {
            description: '',
            title: '行为',
            properties: {
                removeable: {
                    description: '是否可移除',
                    type: 'boolean',
                    title: '是否可移除'
                },
                disabled: {
                    description: '是否禁用',
                    type: 'boolean',
                    title: '是否禁用'
                },
                show: {
                    description: '是否可见',
                    type: 'boolean',
                    title: '是否可见'
                }
            }
        };
    }
    getBasicPropConfig(propertyData: any) {
        this.propertyConfig.categories['basic'] = {
            description: 'Basic Infomation',
            title: '基本信息',
            properties: {
                id: {
                    description: '标签页项的标识',
                    title: '标识',
                    type: 'string',
                    readonly: true
                },
                title: {
                    description: '标签页项的标题',
                    title: '标题',
                    type: 'string'
                }
            },
            setPropertyRelates(changeObject, prop) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject && changeObject.propertyID) {
                    case 'title': {
                        changeObject.needRefreshControlTree = true;
                        break;
                    }
                }
            }
        };
    }
};
