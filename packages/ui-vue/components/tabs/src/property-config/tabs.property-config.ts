import { BaseControlProperty } from "../../../property-panel/src/composition/entity/base-property";
import { DgControl } from "../../../designer-canvas/src/composition/dg-control";

export class TabsProperty extends BaseControlProperty {
    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }
    getPropertyConfig(propertyData: any) {
        // 基本信息
        this.propertyConfig.categories['basic'] = this.getBasicPropConfig(propertyData);
        // 外观
        this.propertyConfig.categories['appearance'] = this.getAppearanceProperties(propertyData);
        // 事件
        // this.getEventPropConfig(propertyData);
        return this.propertyConfig;
    }
    private getAppearanceProperties(propertyData: any) {
        return {
            "title": "外观",
            "description": "Appearance",
            "properties": {
                "class": {
                    "title": "class样式",
                    "type": "string",
                    "description": ""
                },
                "titleWidth": {
                    "description": "标题区域宽度占页面比",
                    "type": "number",
                    "title": "标题区域宽度（%）",
                    "min": 0,
                    "decimals": 0,
                    "max": 100
                },
                "autoTitleWidth": {
                    "description": "开启，则显示全部字符；关闭，最多显示7个字符",
                    "type": "boolean",
                    "title": "标题自适应宽度",
                    "defaultValue": false,
                    "visible":false
                },
                "fill": {
                    "description": "flex布局下，填充满剩余部分",
                    "type": "boolean",
                    "title": "填充"
                }
            }
        };
    }
}
