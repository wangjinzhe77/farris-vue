import { MapperFunction, resolveAppearance } from '../../../dynamic-resolver';

function resolveTabPageToolbar(key: string, value: any, resolvedSchema: any) {

    if (value && value.contents && value.contents.length) {
        value.contents.forEach((toolbarItem: any) => {
            if (toolbarItem.title) {
                toolbarItem.text = toolbarItem.title;
            }
        });
        value.buttons = value.contents;
        delete value.contents;
    }

    return { toolbar: value };
}

export const schemaMapper = new Map<string, string | MapperFunction>([
    ['appearance', resolveAppearance],
    ['toolbar', resolveTabPageToolbar]
]);
