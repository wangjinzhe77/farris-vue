import { DgControl } from "../../../designer-canvas/src/composition/dg-control";
import { ComponentSchema, DesignerComponentInstance } from "../../../designer-canvas/src/types";
import { DynamicResolver } from "../../../dynamic-resolver";

/**
 * 从工具箱拖拽创建标签页，自动创建container-section-tabs三层结构
 */
function wrapContainerSectionForTabs(resolver: DynamicResolver, context: Record<string, any>): Record<string, any> {
    const radomNum = Math.random().toString().slice(2, 6);
    const parentComponentInstance = context.parentComponentInstance as DesignerComponentInstance;

    const tabPageSchema = resolver.getSchemaByType('tab-page') as ComponentSchema;
    tabPageSchema.id = `tab-page-${radomNum}`;
    tabPageSchema.title = `标题`;

    const tabsSchema = resolver.getSchemaByType('tabs') as ComponentSchema;
    Object.assign(tabsSchema, {
        id: `tabs-${radomNum}`,
        appearance: {
            class: 'f-component-tabs f-tabs-has-grid'
        },
        contents: [tabPageSchema],
        activeId: tabPageSchema.id
    });

    const sectionSchema = resolver.getSchemaByType('section') as ComponentSchema;
    Object.assign(sectionSchema, {
        id: `section-${radomNum}`,
        appearance: {
            class: 'f-section-tabs f-section-in-mainsubcard'
        },
        fill: false,
        showHeader: false,
        contents: [tabsSchema]
    });

    const containerSchema = resolver.getSchemaByType('content-container') as ComponentSchema;
    Object.assign(containerSchema, {
        id: `container-${radomNum}`,
        appearance: {
            class: 'f-struct-wrapper'
        },
        contents: [sectionSchema]
    });


    // 判断拖拽的目标容器
    const targetComponentSchema = parentComponentInstance.schema;
    switch (targetComponentSchema && targetComponentSchema.type) {
        case DgControl['splitter-pane'].type: {
            // 左列右卡的模板：修改容器样式
            sectionSchema.appearance.class = 'f-section-tabs f-section-in-main';
            tabsSchema.appearance.class = 'f-component-tabs';

            break;
        }
    }

    // 父容器是flex布局的场景，需要将容器设置为display:block，否则容器宽度有问题
    const parentElementRef: any = parentComponentInstance.elementRef;
    const computedStyle = window.getComputedStyle(parentElementRef);
    if (computedStyle && computedStyle.display === 'flex') {
        containerSchema.appearance.class += ' d-block';
    }

    return containerSchema;
}
export function schemaResolver(resolver: DynamicResolver, schema: Record<string, any>, context: Record<string, any>): Record<string, any> {
    const parentComponentInstance = context.parentComponentInstance as DesignerComponentInstance;
    if (parentComponentInstance) {
        return wrapContainerSectionForTabs(resolver, context);
    } else {
        const tabPageSchema = resolver.getSchemaByType('tab-page') as ComponentSchema;
        schema.contents = [tabPageSchema];
        return schema;
    }

}

export function tabToolbarItemSchemaResolver(resolver: DynamicResolver, schema: Record<string, any>, context: Record<string, any>)
    : Record<string, any> {
    return schema;
}
