 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App, Plugin } from 'vue';
import FTabs from './src/tabs.component';
import FTabPage from './src/components/tab-page.component';
import FTabsDesign from './src/designer/tabs.design.component';
import FTabPageDesign from './src/designer/tab-page.design.component';
import { tabsPropsResolver, tabsDesignPropsResolver, eventHandlerResolver } from './src/tabs.props';
import { tabPagePropsResolver } from './src/components/tab-page.props';
import { tabToolbarItemResolver } from './src/designer/tab-toolbar-item.props';

export * from './src/tabs.props';
export * from './src/components/tab-page.props';

export { FTabs, FTabPage };

FTabs.install = (app: App) => {
    app.component(FTabs.name as string, FTabs);
    app.component(FTabPage.name as string, FTabPage);
};
FTabs.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap.tabs = FTabs;
    componentMap['tab-page'] = FTabPage;
    propsResolverMap.tabs = tabsPropsResolver;
    propsResolverMap['tab-page'] = tabPagePropsResolver;
    resolverMap.tabs = { eventHandlerResolver };
};
FTabs.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.tabs = FTabsDesign;
    componentMap['tab-page'] = FTabPageDesign;
    propsResolverMap.tabs = tabsDesignPropsResolver;
    propsResolverMap['tab-page'] = tabPagePropsResolver;
    propsResolverMap['tab-toolbar-item'] = tabToolbarItemResolver;
};

export default FTabs as typeof FTabs & Plugin;
