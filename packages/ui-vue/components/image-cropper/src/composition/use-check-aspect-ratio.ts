/** 用于处理树自动勾选子节点和优先返回父节点相关功能 */
export function useCheckAspectRatio(cropper: any, maxSize: any, aspectRatio: any, moveStart: any): any {
    function handleTop(overflowX: number, overflowY: number) {
        cropper.value.x2 = cropper.value.x1 + (cropper.value.y2 - cropper.value.y1) * aspectRatio.value;
        overflowX = Math.max(cropper.value.x2 - maxSize.value.width, 0);
        overflowY = Math.max(0 - cropper.value.y1, 0);
        if (overflowX > 0 || overflowY > 0) {
            cropper.value.x2 -= overflowY * aspectRatio.value > overflowX ? overflowY * aspectRatio.value : overflowX;
            cropper.value.y1 += overflowY * aspectRatio.value > overflowX ? overflowY : overflowX / aspectRatio.value;
        }
    }
    function handleBottom(overflowX: number, overflowY: number) {
        cropper.value.x2 = cropper.value.x1 + (cropper.value.y2 - cropper.value.y1) * aspectRatio.value;
        overflowX = Math.max(cropper.value.x2 - maxSize.value.width, 0);
        overflowY = Math.max(cropper.value.y2 - maxSize.value.height, 0);
        if (overflowX > 0 || overflowY > 0) {
            cropper.value.x2 -= overflowY * aspectRatio.value > overflowX ? overflowY * aspectRatio.value : overflowX;
            cropper.value.y2 -= overflowY * aspectRatio.value > overflowX ? overflowY : overflowX / aspectRatio.value;
        }
    }
    function handleTopleft(overflowX: number, overflowY: number) {
        cropper.value.y1 = cropper.value.y2 - (cropper.value.x2 - cropper.value.x1) / aspectRatio.value;
        overflowX = Math.max(0 - cropper.value.x1, 0);
        overflowY = Math.max(0 - cropper.value.y1, 0);
        if (overflowX > 0 || overflowY > 0) {
            cropper.value.x1 += overflowY * aspectRatio.value > overflowX ? overflowY * aspectRatio.value : overflowX;
            cropper.value.y1 += overflowY * aspectRatio.value > overflowX ? overflowY : overflowX / aspectRatio.value;
        }
    }
    function handleTopright(overflowX: number, overflowY: number) {
        cropper.value.y1 = cropper.value.y2 - (cropper.value.x2 - cropper.value.x1) / aspectRatio.value;
        overflowX = Math.max(cropper.value.x2 - maxSize.value.width, 0);
        overflowY = Math.max(0 - cropper.value.y1, 0);
        if (overflowX > 0 || overflowY > 0) {
            cropper.value.x2 -= overflowY * aspectRatio.value > overflowX ? overflowY * aspectRatio.value : overflowX;
            cropper.value.y1 += overflowY * aspectRatio.value > overflowX ? overflowY : overflowX / aspectRatio.value;
        }
    }
    function handleRightAndBottomRight(overflowX: number, overflowY: number) {
        cropper.value.y2 = cropper.value.y1 + (cropper.value.x2 - cropper.value.x1) / aspectRatio.value;
        overflowX = Math.max(cropper.value.x2 - maxSize.value.width, 0);
        overflowY = Math.max(cropper.value.y2 - maxSize.value.height, 0);
        if (overflowX > 0 || overflowY > 0) {
            cropper.value.x2 -= overflowY * aspectRatio.value > overflowX ? overflowY * aspectRatio.value : overflowX;
            cropper.value.y2 -= overflowY * aspectRatio.value > overflowX ? overflowY : overflowX / aspectRatio.value;
        }
    }
    function handleLeftAndBottomleft(overflowX: number, overflowY: number) {
        cropper.value.y2 = cropper.value.y1 + (cropper.value.x2 - cropper.value.x1) / aspectRatio.value;
        overflowX = Math.max(0 - cropper.value.x1, 0);
        overflowY = Math.max(cropper.value.y2 - maxSize.value.height, 0);
        if (overflowX > 0 || overflowY > 0) {
            cropper.value.x1 += overflowY * aspectRatio.value > overflowX ? overflowY * aspectRatio.value : overflowX;
            cropper.value.y2 -= overflowY * aspectRatio.value > overflowX ? overflowY : overflowX / aspectRatio.value;
        }
    }
    function handleCenter() {
        cropper.value.x2 = cropper.value.x1 + (cropper.value.y2 - cropper.value.y1) * aspectRatio.value;
        cropper.value.y2 = cropper.value.y1 + (cropper.value.x2 - cropper.value.x1) / aspectRatio.value;
        const overflowX1 = Math.max(0 - cropper.value.x1, 0);
        const overflowX2 = Math.max(cropper.value.x2 - maxSize.value.width, 0);
        const overflowY1 = Math.max(cropper.value.y2 - maxSize.value.height, 0);
        const overflowY2 = Math.max(0 - cropper.value.y1, 0);
        if (overflowX1 > 0 || overflowX2 > 0 || overflowY1 > 0 || overflowY2 > 0) {
            cropper.value.x1 += overflowY1 * aspectRatio.value > overflowX1 ? overflowY1 * aspectRatio.value : overflowX1;
            cropper.value.x2 -= overflowY2 * aspectRatio.value > overflowX2 ? overflowY2 * aspectRatio.value : overflowX2;
            cropper.value.y1 += overflowY2 * aspectRatio.value > overflowX2 ? overflowY2 : overflowX2 / aspectRatio.value;
            cropper.value.y2 -= overflowY1 * aspectRatio.value > overflowX1 ? overflowY1 : overflowX1 / aspectRatio.value;
        }
    }
    /** 检查图片比率 */
    function checkAspectRatio(): void {
        const overflowX = 0;
        const overflowY = 0;
        if (maxSize.value.width && maxSize.value.height) {
            switch (moveStart.position) {
            case 'top':
                handleTop(overflowX, overflowY);
                break;
            case 'bottom':
                handleBottom(overflowX, overflowY);
                break;
            case 'topleft':
                handleTopleft(overflowX, overflowY);
                break;
            case 'topright':
                handleTopright(overflowX, overflowY);
                break;
            case 'right':
            case 'bottomright':
                handleRightAndBottomRight(overflowX, overflowY);
                break;
            case 'left':
            case 'bottomleft':
                handleLeftAndBottomleft(overflowX, overflowY);
                break;
            case 'center':
                handleCenter();
                break;
            }
        }
    }
    return { checkAspectRatio };
}
