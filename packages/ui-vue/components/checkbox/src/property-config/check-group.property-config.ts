import { InputBaseProperty } from "../../../property-panel/src/composition/entity/input-base-property";

export class CheckGroupProperty extends InputBaseProperty {

    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }
    getEditorProperties(propertyData) {
        const self = this;
        const editorProperties = self.getComponentConfig(propertyData, { "type": "check-group" }, {
            placeholder: {
                visible: false
            },
            disabled: {
                visible: false
            },
            direction: {
                description: "",
                title: "排列方向",
                type: "enum",
                editor: {
                    type: "combo-list",
                    textField: "value",
                    valueField: "key",
                    data: [{ "key": "horizontal", "value": "横向" }, { "key": "vertical", "value": "纵向" }]
                }
            },
            textField: {
                description: "",
                title: "文本字段",
                type: "string",
                visible: false
            },
            valueField: {
                description: "",
                title: "值字段",
                type: "string",
                visible: false
            },
            data: {
                description: "",
                title: "数据",
                type: "array",
                $converter: "/converter/enum-data.converter",
                ...self.getItemCollectionEditor(propertyData, propertyData.editor.valueField, propertyData.editor.textField),
                // 这个属性，标记当属性变更得时候触发重新更新属性
                refreshPanelAfterChanged: true,
            }
        });
        editorProperties['setPropertyRelates'] = function (changeObject) {
            if (!changeObject) {
                return;
            }
            switch (changeObject.propertyID) {
                case 'data': {
                    if (changeObject.propertyValue.parameters) {
                        propertyData.editor.valueField = changeObject.propertyValue.parameters.valueField;
                        propertyData.editor.textField = changeObject.propertyValue.parameters.nameField;
                    }
                    // 此类型默认可以编辑数据，要同步更新格式化
                    if(propertyData.formatter){
                        propertyData.formatter.data=[...changeObject.propertyValue.value]
                    }
                    break;
                }
            }
        };
        return editorProperties;
    }
}
