import { InputBaseProperty } from "../../../property-panel/src/composition/entity/input-base-property";

export class CheckBoxProperty extends InputBaseProperty {

    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }
    getEditorProperties(propertyData) {
        return this.getComponentConfig(propertyData, { "type": "check-box" },{
            placeholder: {
                visible:false
            },
            disabled: {
                visible:false
            }
            // name: {
            //     description: "控件名称，提交时的唯一标识，可以为空但不能与其他控件重复",
            //     title: "控件名称",
            //     type: "string"
            // }
        });
    }
}
