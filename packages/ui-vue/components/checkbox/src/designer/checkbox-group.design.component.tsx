/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, computed, ref, watch, inject, onMounted } from 'vue';
import type { SetupContext } from 'vue';
import { CheckboxGroupProps, checkboxGroupProps } from '../checkbox-group.props';
import { useCheckboxGroup } from '../composition/use-checkbox-group';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useCheckGroupDesignerRules } from './use-rules';

export default defineComponent({
    name: 'FCheckboxGroupDesign',
    props: checkboxGroupProps,
    emits: ['changeValue', 'update:modelValue'] as (string[] & ThisType<void>) | undefined,
    setup(props: CheckboxGroupProps, context: SetupContext) {
        const modelValue = ref(props.modelValue);
        const tabIndex = ref(props.tabIndex);
        const name = ref(props.name);
        const horizontalClass = computed(() => ({
            'farris-checkradio-hor':  props.direction === 'horizontal'
        }));
        const { enumData, onClickCheckbox, getValue, getText, checked } = useCheckboxGroup(props, context, modelValue);
        const elementRef = ref();
        const designerHostService = inject('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useCheckGroupDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });
        /**
         * 解决在设计时，数据为空数组，界面不显示内容的问题
         */
        const realEnumData = computed(() => {
            if (!enumData.value || enumData.value.length === 0) {
                const result = [] as any;
                [
                    { value: 'example1', name: '示例一' },
                    { value: 'example2', name: '示例二' }
                ].map(item => {
                    const tempData = {};
                    tempData[props.valueField] = item['value'];
                    tempData[props.textField] = item['name'];
                    result.push(tempData);
                });
                return result;
            }
            return enumData.value;
        });

        context.expose(componentInstance.value);

        watch(
            () => props.modelValue,
            (value: any) => {
                modelValue.value = value;
                context.emit('changeValue', modelValue.value);
            }
        );

        return () => {
            return (
                <div ref={elementRef} class={['farris-input-wrap', horizontalClass.value]}>
                    {realEnumData.value.map((item, index) => {
                        const id = 'checkbox_' + name.value + index;
                        return (
                            <div class="custom-control custom-checkbox">
                                <input
                                    type="checkbox"
                                    class="custom-control-input"
                                    name={name.value}
                                    id={id}
                                    value={getValue(item)}
                                    checked={checked(item)}
                                    disabled={props.readonly || props.disabled}
                                    tabindex={tabIndex.value}
                                    onClick={(event: MouseEvent) => onClickCheckbox(item, event)}
                                />
                                <label class="custom-control-label" for={id} title={getText(item)}>
                                    {getText(item)}
                                </label>
                            </div>
                        );
                    })}
                </div>
            );
        };
    }
});
