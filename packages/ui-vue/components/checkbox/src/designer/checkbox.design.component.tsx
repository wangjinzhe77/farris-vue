/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, inject, onMounted, ref, SetupContext, watch } from 'vue';
import { CheckBoxProps, checkBoxProps } from '../components/checkbox.props';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { useCheckBoxDesignerRules } from './use-rules';

export default defineComponent({
    name: 'FCheckBoxDesign',
    props: checkBoxProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: CheckBoxProps, context: SetupContext) {
        const elementRef = ref();
        const id = ref(props.id);
        const indeterminate = ref(props.indeterminate);
        const designerHostService = inject('designer-host-service');  
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useCheckBoxDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext,designerRulesComposition);

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        watch(() => props.indeterminate, (newValue: boolean) => {
            indeterminate.value = newValue;
        });

        context.expose(componentInstance.value);
        return () => {
            return (
                <div
                    ref={elementRef}
                    dragref={`${designItemContext.schema.id}-container`}
                    class="drag-container custom-control custom-checkbox"
                >
                    <input
                        type="checkbox"
                        class="custom-control-input"
                        id={id}
                        value={props.value}
                        checked={false}
                        readonly={true}
                    />
                    <div class="custom-control-label"></div>
                </div>
            );
        };
    }
});
