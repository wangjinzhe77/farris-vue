 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import checkboxSchema from '../schema/checkbox.schema.json';
import { createPropsResolver } from '@farris/ui-vue/components/dynamic-resolver';
import { schemaMapper } from '../schema/schema-mapper';
import { schemaResolver } from '../schema/schema-resolver';

export const checkBoxProps = {
    /** 组件唯一标识 */
    id: { Type: String, default: '' },
    /** 自定义样式 */
    customClass: { Type: String, default: '' },
    /** 禁用状态 */
    disabled: { Type: Boolean, default: false },
    /**
     * 功能同disabled
     */
    readonly: { type: Boolean, default: false },
    /** 中间状态 */
    indeterminate: { Type: Boolean, default: false },
    /** 选择状态 */
    modelValue: { Type: Boolean, default: false },
    /**
     * 作为内嵌编辑器被创建后默认获得焦点
     */
    focusOnCreated: { type: Boolean, default: false },
    /** 选中的值 value与modelValue重合 TODO*/
    value: { type: String, default: '' },
    /** name值 */
    name: { type: String, default: '' },
    /** 标识是否被选中 */
    checked: { type: Boolean, default: false }
} as Record<string, any>;

export type CheckBoxProps = ExtractPropTypes<typeof checkBoxProps>;

export const checkBoxPropsResolver = createPropsResolver<CheckBoxProps>(checkBoxProps, checkboxSchema, schemaMapper, schemaResolver);
