/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, inject, onMounted, ref, SetupContext } from 'vue';
import { CHECKBOX_CONTEXT, useCheck } from '@farris/ui-vue/components/common';
import FCheckboxButton from './checkbox-button.component';
import { CheckBoxProps, checkBoxProps } from './checkbox.props';

export default defineComponent({
    name: 'FCheckbox',
    props: checkBoxProps,
    emits: ['change', 'changeValue', 'update:modelValue', 'update:checked'] as (string[] & ThisType<void>) | undefined,
    setup(props: CheckBoxProps, context: SetupContext) {
        const checkBoxContext = inject(CHECKBOX_CONTEXT, null);
        const { checked, disabled, indeterminate, name, onClickCheckBox, shouldRenderButton, shouldRenderNative }
            = useCheck(props, context, checkBoxContext?.parentProps, checkBoxContext?.parentContext);
        const id = ref(props.id);

        onMounted(() => {
            // if (props.focusOnCreated) {
            //     (inputElementRef.value as HTMLInputElement)?.focus();
            // }
        });

        return () => {
            return (
                <>
                    {
                        shouldRenderButton.value &&
                        <FCheckboxButton
                            {
                            ...props
                            }
                            onClick={onClickCheckBox}
                        >
                            {context.slots.default?.()}
                        </FCheckboxButton>
                        // <div
                        //     class={buttonClass.value}
                        //     style="border-radius:0;border: 1px solid #E8EBF2;"
                        //     onClick={onClickCheckBox}
                        // >
                        //     {context.slots.default?.()}
                        // </div>
                    }
                    {
                        shouldRenderNative.value && <div class="custom-control custom-checkbox" onClick={onClickCheckBox}>
                            <input id={id} title="checkbox-input" type="checkbox" class="custom-control-input"
                                checked={checked.value} disabled={disabled.value} indeterminate={indeterminate.value} name={name.value} value={props.value} />
                            <div class="custom-control-label">
                                {context.slots.default?.()}
                            </div>
                        </div>
                    }
                </>
            );
        };
    }
});
