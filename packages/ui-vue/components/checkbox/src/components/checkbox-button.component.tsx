/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, inject, SetupContext } from 'vue';
import { CHECKBOX_CONTEXT, useCheck } from '@farris/ui-vue/components/common';
import { CheckBoxProps, checkBoxProps } from './checkbox.props';
import './checkbox.css';

export default defineComponent({
    name: 'FCheckboxButton',
    props: checkBoxProps,
    emits: ['change', 'changeValue', 'update:modelValue', 'update:checked', 'click'] as (string[] & ThisType<void>) | undefined,
    setup(props: CheckBoxProps, context: SetupContext) {
        const checkBoxContext = inject(CHECKBOX_CONTEXT, null);
        const { buttonClass } =
            useCheck(props, context, checkBoxContext?.parentProps, checkBoxContext?.parentContext);
        const onClick = (e: MouseEvent) => {
            e.stopPropagation();
            context.emit('click',e);
        };
        return () => {
            return (
                <div
                    class={buttonClass.value}
                    style="border-radius:0;border: 1px solid #E8EBF2;"
                    onClick={onClick}
                >
                    {context.slots.default?.()}
                </div>
            );
        };
    }
});
