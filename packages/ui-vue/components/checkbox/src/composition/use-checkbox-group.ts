/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeCheckbox, Checkbox } from './types';
import { computed, Ref, SetupContext, ref, watch } from 'vue';
import { CheckboxGroupProps } from '../checkbox-group.props';

export function useCheckboxGroup(
    props: CheckboxGroupProps,
    context: SetupContext,
    modelValue: Ref<string | Array<string> | undefined>
): ChangeCheckbox {
    const canChangeRadioButton = computed(() => !(props.readonly && props.disabled));
    const enumData: Ref<Checkbox[]> = ref(props.data || props.enumData || props.options);
    function getValue(item: Checkbox): any {
        return item[props.valueField];
    }

    function getText(item: Checkbox): any {
        return item[props.textField];
    }

    /**
     * 值到数组值的转换
     */
    function transformToArr(value: any): string[] {
        if (!value) {
            return [];
        }
        if (!props.isStringValue) {
            return value;
        }

        return value.split(props.separator);
    }

    /**
     * 值到字符串值的转换
     */
    function transformToStr(value: Array<string>) {
        const allEnumValues = enumData.value.map((enumItem) => getValue(enumItem));

        const result = allEnumValues.filter((target: any) => value.some((item) => item === String(target)));

        if (!props.isStringValue) {
            return result;
        }
        return result.join(props.separator);
    }
    /**
     * 校验复选框是否为选中状态
     */
    function checked(item: Checkbox) {
        const val = String(getValue(item));
        const checkedValue = transformToArr(modelValue.value);

        // 多值
        return checkedValue.some((item) => item === val);
    }

    /**
     * 点击复选框事件
     */
    function onClickCheckbox(item: Checkbox, $event: Event) {
        if (canChangeRadioButton.value) {
            let arrValue = transformToArr(modelValue.value) || [];

            const val = String(getValue(item));
            if (!arrValue || !arrValue.length) {
                arrValue.push(val);
            } else if (arrValue.some((item) => item === val)) {
                arrValue = arrValue.filter((n) => n !== val);
            } else {
                arrValue.push(val);
            }

            // 更新value值
            modelValue.value = transformToStr(arrValue);

            context.emit('changeValue', modelValue.value);

            // 不可少，用于更新组件绑定值
            context.emit('update:modelValue', modelValue.value);
        }

        $event.stopPropagation();
    }

    watch(() => props.data, () => {
        enumData.value = props.data;
    });
    
    watch(() => props.enumData, () => {
        enumData.value = props.enumData;
    });
    return {
        enumData,
        getValue,
        getText,
        checked,
        onClickCheckbox
    };
}
