 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App, Plugin } from 'vue';
import FCheckbox from './src/components/checkbox.component';
import { checkBoxPropsResolver } from './src/components/checkbox.props';
import FCheckboxGroup from './src/checkbox-group.component';
import { checkBoxGroupPropsResolver } from './src/checkbox-group.props';
import FCheckboxGroupDesigner from './src/designer/checkbox-group.design.component';
import FCheckboxDesigner from './src/designer/checkbox.design.component';

export * from './src/checkbox-group.props';
export * from './src/components/checkbox.props';
export * from './src/composition/types';

FCheckboxGroup.install = (app: App) => {
    app.component(FCheckboxGroup.name as string, FCheckboxGroup);
    app.component(FCheckbox.name as string, FCheckbox);
};

FCheckboxGroup.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap['check-box'] = FCheckbox;
    propsResolverMap['check-box'] = checkBoxPropsResolver;
    componentMap['check-group'] = FCheckboxGroup;
    propsResolverMap['check-group'] = checkBoxGroupPropsResolver;
};
FCheckboxGroup.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['check-box'] = FCheckboxDesigner;
    propsResolverMap['check-box'] = checkBoxPropsResolver;
    componentMap['check-group'] = FCheckboxGroupDesigner;
    propsResolverMap['check-group'] = checkBoxGroupPropsResolver;
};

export { FCheckboxGroup, FCheckbox };
export default FCheckboxGroup as typeof FCheckboxGroup & Plugin;
