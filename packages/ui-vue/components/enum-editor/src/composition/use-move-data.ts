import { SelectedEnumItem } from "./types";

export function useMoveData() {

    function moveTop(data: Array<Record<string, any>>, currentItem?: SelectedEnumItem) {
        if (currentItem) {
            const {index} = currentItem;
            if (index > 0) {
                data.unshift(currentItem.item);
                data.splice(index + 1, 1);
                currentItem.index = 0;
            }
        }
    }

    function moveUp(data: Array<Record<string, any>>, currentItem?: SelectedEnumItem) {
        if (currentItem) {
            const {index} = currentItem;
            if (index > 0) {
                const removedItemes = data.splice(index, 1);
                data.splice(index - 1, 0, ...removedItemes);
                currentItem.index = index - 1;
            }
        }
    }

    function moveDown(data: Array<Record<string, any>>, currentItem?: SelectedEnumItem) {
        if (currentItem) {
            const {index} = currentItem;

            if (index < data.length -1) {
                const removedItemes = data.splice(index, 1);
                data.splice(index + 1, 0, ...removedItemes);

                currentItem.index = index + 1;
            }
        }
    }

    function moveBottom(data: Array<Record<string, any>>, currentItem?: SelectedEnumItem) {
        if (currentItem) {
            const {index} = currentItem;
            if (index < data.length -1) {
                data.push(currentItem.item);
                data.splice(currentItem.index, 1);
                currentItem.index = data.length - 1;
            }
        }
    }

    return {
        moveTop, moveUp, moveDown, moveBottom
    };

}
