import { ExtractPropTypes } from "vue";

export const enumEditorTableProps = {
    items: { type: Array<Record<string, any>>, default: []},
    valueField: { type: String, default: 'value'},
    textField: { type: String, default: 'name'}
};

export type EnumEditorTableProps = ExtractPropTypes<typeof enumEditorTableProps>;
