import { SetupContext, computed, defineComponent, reactive, ref, watch } from "vue";
import InputGroupComponent from '@farris/ui-vue/components/input-group';
import { EnumEditorTableProps, enumEditorTableProps } from "./enum-editor-table.props";
import { SelectedEnumItem } from "../composition/types";

export default defineComponent({
    name: 'FEnumEditorTable',
    props: enumEditorTableProps,
    emits: ['selected'],
    setup(props: EnumEditorTableProps, context: SetupContext) {

        const enumData = ref(props.items || []);
        const enumTableElement =ref();
        const selectedRowInfo = ref<SelectedEnumItem>();

        function updateSelectedRowInfo(itemIndex: number) {
            if (!selectedRowInfo.value || !enumData.value || enumData.value.length === 0) {
                selectedRowInfo.value = undefined;
                return;
            }

            const selectedRowIndex = selectedRowInfo.value.index;
            let nextSelectIndex = selectedRowIndex;

            if (selectedRowIndex === itemIndex) {
                // 尝试选择下一项；如果没有，则选择上一项；如果都没有，则取消选择
                nextSelectIndex = Math.min(selectedRowIndex + 1, enumData.value.length - 1);
                if (nextSelectIndex < 0) {
                    selectedRowInfo.value = undefined;
                } else {
                    nextSelectIndex = nextSelectIndex < itemIndex? nextSelectIndex: itemIndex;
                    selectedRowInfo.value = { item: enumData.value[nextSelectIndex], index: nextSelectIndex };
                }
            } else {
                // 选择相邻项
                nextSelectIndex = selectedRowIndex > itemIndex ? selectedRowIndex - 1 : selectedRowIndex;
                if (nextSelectIndex >= 0 && nextSelectIndex < enumData.value.length) {
                    selectedRowInfo.value = { item: enumData.value[nextSelectIndex], index: nextSelectIndex };
                }
            }
        }

        function removeEnumItem(event: MouseEvent, itemIndex: number) {
            event.stopPropagation();

            try {
                enumData.value.splice(itemIndex, 1); // 移除指定项

                if (selectedRowInfo.value) {
                    updateSelectedRowInfo(itemIndex); // 更新选中项信息
                    context.emit('selected', selectedRowInfo.value); // 通知更新
                }
            } catch (error) {
                console.error("An error occurred during the enumeration data update:", error);
            }

        }

        function onClickEnumItem(event: MouseEvent, clickedInfo: SelectedEnumItem) {
            selectedRowInfo.value = clickedInfo;
            context.emit('selected', clickedInfo);
        }

        function getActiveElement(rowIndex: number) {
            return enumTableElement.value?.querySelector('[data-index="'+ rowIndex +'"]');
        }

        function updateSelectedInfo(newData: Array<Record<string, any>>) {
            if (!newData || !newData.length) {
                selectedRowInfo.value = undefined;
                return;
            }

            const selectedItem = selectedRowInfo.value?.item;
            if (selectedItem && typeof props.valueField === "string" && props.valueField in selectedItem) {
                const valueFieldSet = new Set(newData.map(item => item[props.valueField]));
                const selectedItemIsInDataSource = valueFieldSet.has(selectedItem[props.valueField]);
                if (!selectedItemIsInDataSource) {
                    selectedRowInfo.value = undefined;
                }
            }
        }

        watch(() => props.items, (newData) => {
            enumData.value = newData;
            updateSelectedInfo(newData);
        });

        watch(() => selectedRowInfo.value?.index, (newRowIndex) => {
            if (newRowIndex === undefined) {
                return;
            }
            const currentDom = getActiveElement(newRowIndex);
            if (currentDom) {
                currentDom.scrollIntoView({block: 'center', inline: 'nearest'});
            }
        });

        function renderEnumDataRow(items: Array<Record<string, any>>) {
            return items.map((enumItem: Record<string, any>, itemIndex) => {
                return <tr onClick={(event: MouseEvent) => onClickEnumItem(event, {item: enumItem, index: itemIndex})}
                    key={enumItem.id} data-index={itemIndex}
                    class={selectedRowInfo.value?.index === itemIndex ? 'selected': ''}>
                    <td>
                        <InputGroupComponent v-model={enumItem[props.valueField]}></InputGroupComponent>
                    </td>
                    <td>
                        <InputGroupComponent v-model={enumItem[props.textField]}></InputGroupComponent>
                    </td>
                    <td style="width: 42px">
                        <button type="button" class="btn btn-link" onClick={(payload: MouseEvent) => removeEnumItem(payload, itemIndex)}>
                            <span class="f-icon f-icon-close"></span>
                        </button>
                    </td>
                </tr>;
            });
        }

        return () => {
            return (<><div class="shading-border-top" style="position: absolute;top: 0;width: 100%;z-index: 2;"></div>
                <div class="table-body f-utils-fill win11Scroll show nobtn ml-3" style="overflow-y: scroll;">
                    <table class="table table-bordered" ref={enumTableElement}>
                        <thead style="position: sticky; top: 0; z-index: 1;">
                            <tr>
                                <th><b>编码</b></th>
                                <th><b>名称</b></th>
                                <th style="width: 42px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            { renderEnumDataRow(enumData.value) }
                        </tbody>
                    </table>
                </div></>);
        };
    }
});
