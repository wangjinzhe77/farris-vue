import { SetupContext, defineComponent, ref } from "vue";
import { EnumEditorProps, enumEditorProps } from "../enum-editor.props";
import EnumeditorTableComponent from './enum-editor-table.component';

import EnumEditorButtons from './enum-editor-buttons.component';
import { useMoveData } from "../composition/use-move-data";
import { SelectedEnumItem } from "../composition/types";
import { useGuid } from "@farris/ui-vue/components/common";

export default defineComponent({
    name: 'FEnumEditorContainer',
    props: enumEditorProps,
    emits: ['dataChanged'],
    setup(props: EnumEditorProps, context: SetupContext) {

        const { moveTop, moveDown, moveBottom, moveUp } = useMoveData();
        const { guid } = useGuid();

        const enumData = ref(props.items || []);
        const currentItem = ref<SelectedEnumItem>();
        const enumTableRef = ref();

        function onAddNew() {
            enumData.value.push({
                id: guid(),
                [props.valueField]: '',
                [props.textField]: ''
            });
            context.emit('dataChanged', enumData.value);
        }

        function onClear() {
            enumData.value = [];
            currentItem.value = undefined;
            context.emit('dataChanged', []);
        }

        function onMove(event: MouseEvent, type: string) {
            switch (type) {
                case 'top':
                    moveTop(enumData.value, currentItem.value);
                    break;
                case 'up':
                    moveUp(enumData.value, currentItem.value);
                    break;
                case 'down':
                    moveDown(enumData.value, currentItem.value);
                    break;
                case 'bottom':
                    moveBottom(enumData.value, currentItem.value);
                    break;
                default:
                    break;
            }

            context.emit('dataChanged', enumData.value);
        }

        function onSelectedItem(selectedItem: SelectedEnumItem) {
            currentItem.value = selectedItem;
        }

        return () => {
            return (<div class="h-100 d-flex flex-column"><EnumeditorTableComponent
                ref={enumTableRef}
                valueField={props.valueField}
                textField={props.textField}
                items={enumData.value}
                onSelected={onSelectedItem}
            ></EnumeditorTableComponent>
                <EnumEditorButtons
                    activeData={currentItem.value}
                    itemsTotal={enumData.value?.length}
                    onAdd={onAddNew}
                    onClear={onClear}
                    onMove={(event: MouseEvent, type: string) => onMove(event, type)}
                ></EnumEditorButtons></div>);
        };
    }
});
