import { ExtractPropTypes, PropType } from "vue";
import { SelectedEnumItem } from "../composition/types";

export const enumEditorButtonsProps = {
    activeData: {type: Object as PropType<SelectedEnumItem>, default: null},
    itemsTotal: { type: Number, default: 0 }
};

export type EnumEditorButtonsProps = ExtractPropTypes<typeof enumEditorButtonsProps>;
