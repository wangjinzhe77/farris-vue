import { SetupContext, computed, defineComponent, ref, watch } from "vue";
import { EnumEditorButtonsProps, enumEditorButtonsProps } from "./enum-editor-buttons.props";

export default defineComponent({
    name: 'EnumEditorButtons',
    props: enumEditorButtonsProps,
    emits: ['add','clear', 'move'],
    setup(props: EnumEditorButtonsProps, context: SetupContext) {

        const activeData = ref(props.activeData);
        const itemsTotal = ref(props.itemsTotal);

        const currentIndex = computed(() => {
            return activeData.value?.index ?? -1;
        });

        const canMoveUp = computed(() => {
            return itemsTotal.value && currentIndex.value > 0;
        });

        const canMoveDown = computed(() => {
            return itemsTotal.value && currentIndex.value > -1 && currentIndex.value < itemsTotal.value - 1;
        });

        function addNew() {
            context.emit('add');
        }

        function clear() {
            context.emit('clear');
        }

        function onMoveItem(event: MouseEvent, type: string) {
            context.emit('move', event, type);
        }

        watch(() => props.activeData, (newActiveData) => {
            activeData.value = newActiveData;
        });
        watch(() => props.itemsTotal, (newTotal) => {
            itemsTotal.value = newTotal;
        });

        return () => {
            return (<div class="footer-container mx-3 d-flex align-items-center" style="height: 36px;">
                <button type="button" onClick={addNew} class="btn btn-sm btn-primary mr-2">
                    <span class="f-icon f-icon-add"></span>新增</button>
                <button type="button" onClick={clear} class="btn btn-sm btn-secondary mr-3" disabled={!itemsTotal.value}>
                    <span class="f-icon f-icon-delete"></span>清空</button>
                <button type="button" disabled={!canMoveUp.value} onClick={(event: MouseEvent) => onMoveItem(event, 'top')}
                    class="btn btn-sm btn-secondary mr-1"><span class="f-icon f-icon-roofing"></span>
                    置顶</button>
                <button type="button" disabled={!canMoveUp.value} onClick={(event: MouseEvent) => onMoveItem(event, 'up')}
                    class="btn btn-sm btn-secondary mr-1"><span class="f-icon f-icon-arrow-chevron-up"></span>
                    上移</button>
                <button type="button" disabled={!canMoveDown.value} onClick={(event: MouseEvent) => onMoveItem(event, 'down')}
                    class="btn btn-sm btn-secondary mr-1"><span class="f-icon f-icon-arrow-chevron-down"></span>
                    下移</button>
                <button type="button" disabled={!canMoveDown.value} onClick={(event: MouseEvent) => onMoveItem(event, 'bottom')}
                    class="btn btn-sm btn-secondary"><span class="f-icon f-icon-page-last" style="transform: rotate(90deg);"></span>
                    置底</button>
            </div>);
        };
    }
});
