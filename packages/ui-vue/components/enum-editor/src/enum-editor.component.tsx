import { cloneDeep } from "lodash-es";
import { SetupContext, ref, defineComponent, computed, watch } from "vue";

import FButtonEdit from '@farris/ui-vue/components/button-edit';

import { EnumEditorProps, enumEditorProps } from "./enum-editor.props";
import EnumEditorContainerComponent from './components/enum-editor-container.component';

export default defineComponent({
    name: 'FEnumEditor',
    props: enumEditorProps,
    emits: ['submitModal'],
    setup(props: EnumEditorProps, context: SetupContext){

        const buttonIcon = '<i class="f-icon f-icon-lookup"></i>';

        const valueField = ref(props.valueField);
        const textField = ref(props.textField);
        const initialEnumData = ref(props.items || []);

        const enumData = ref();

        const displayText = computed(() => {
            return `共 ${ initialEnumData.value?.length || 0 } 项`;
        });

        watch(() => props.items, (newData) => {
            initialEnumData.value = newData;
        });

        watch(() => props.valueField, (newValueField) => {
            valueField.value = newValueField;
        });

        watch(() => props.textField, (newTextField) => {
            textField.value = newTextField;
        });

        function onDataSourceChanged(newDataSource: Array<Record<string, any>>) {
            enumData.value = newDataSource;
        }

        function renderEnumEditor() {
            // const canOpen = props.beforeOpen();

            // if (canOpen) {
            // }

            enumData.value = cloneDeep(initialEnumData.value);
            return <EnumEditorContainerComponent
                valueField={valueField.value}
                textField={textField.value}
                items={enumData.value}
                onDataChanged={onDataSourceChanged}></EnumEditorContainerComponent>;
        }

        const modalOptions = {
            title: '枚举数据编辑器',
            fitContent: false,
            height: 500,
            width: 600,
            acceptCallback: () => {
                initialEnumData.value = enumData.value;
                const enumDataValue = enumData.value.map((enumItem: Record<string, string>) => {
                    return {
                        [valueField.value]: enumItem[valueField.value],
                        [textField.value]: enumItem[textField.value]
                    };
                });
                context.emit('submitModal', enumDataValue);
            },
            rejectCallback: () => {
                // enumData.value = initialEnumData.value;
            }
        };

        return () => {
            return (<FButtonEdit
                v-model={displayText.value}
                editable={false}
                disabled={props.disabled}
                readonly={props.readonly}
                inputType={"text"}
                enableClear={false}
                buttonContent={buttonIcon}
                buttonBehavior={"Modal"}
                modalOptions={modalOptions}
            >
                <div class="h-100 d-flex flex-column">
                    {renderEnumEditor()}
                </div>
            </FButtonEdit>);
        };
    }
});
