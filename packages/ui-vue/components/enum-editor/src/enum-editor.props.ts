import { ExtractPropTypes, readonly } from "vue";

export const enumEditorProps = {
    items: { type: Array<Record<string, any>>, default: [] },
    valueField: { type: String, default: 'id'},
    textField: { type: String, default: 'name'},
    beforeOpen: { type: Function, default: () => true},
    disabled: {type: Boolean, default: false},
    readonly: {type: Boolean, default: false}
};

export type EnumEditorProps = ExtractPropTypes<typeof enumEditorProps>;
