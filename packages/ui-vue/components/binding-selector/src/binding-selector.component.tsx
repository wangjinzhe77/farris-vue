import { ref, defineComponent } from "vue";
import FButtonEdit from '@farris/ui-vue/components/button-edit';
import { FNotifyService } from "@farris/ui-vue/components/notify";
import BindingSelectorComponent from './components/binding-selector-container.component';
import { BindingSelectorProps, bindingSelectorProps } from "./binding-selector.props";
import { useFieldSelection } from "./composition/use-field-selection";


export default defineComponent({
    name: 'FBindingSelector',
    props: bindingSelectorProps,
    emits: ['selected', 'bindingTypeChanged'],
    setup(props: BindingSelectorProps, context) {
        const buttonIcon = '<i class="f-icon f-icon-lookup"></i>';
        /** 绑定类型数据源 */
        const bindingData = ref(props.bindingData);
        /** 树表绑定数据源 */
        const dataSource = ref(props.data);
        const bindingSelectorRef = ref();
        /** 绑定类型 */
        const bindingType = ref(props.modelValue?.type || 'Form');
        /** 文本框展示内容 */
        const displayText = ref(props.modelValue?.path);

        const { updateViewModel, updateComponentSchema } = useFieldSelection(props);

        function onFieldSelected(selectedFields: Array<Record<string, any>>) {
            bindingData.value = selectedFields;
        }

        function onBindingTypeChanged(newBindingType: string) {
            bindingType.value = newBindingType;
        }

        function setDisplayText(setDisplayText: any) {
            displayText.value = setDisplayText.path;
        }

        function renderBindingSelector() {
            return <BindingSelectorComponent ref={bindingSelectorRef}
                data={dataSource.value}
                onSelected={onFieldSelected}
                modelValue={props.modelValue}
                idField={props.idField}
                bindingData={bindingData.value}
                onBindingTypeChange={onBindingTypeChanged}
                editorParams={props.editorParams}
                bindingType={props.bindingType}
                showCustomFooter={false}
            ></BindingSelectorComponent>;
        }

        function checkBindingData(): boolean {
            if (!bindingData.value || !bindingData.value.length) {
                const notifyService: any = new FNotifyService();
                notifyService.globalConfig = { position: 'top-center' };
                notifyService.warning({ message: '请先选择数据' });
                return false;
            }
            return true;
        }

        const modalOptions = {
            title: props.title,
            fitContent: false,
            height: props.modalHeight,
            width: props.modalWidth,
            buttons: [
                {
                    name: 'cancel',
                    text: '取消',
                    class: 'btn btn-secondary',
                    handle: ($event: MouseEvent) => {
                        bindingData.value = [];
                        return true;
                    }
                },
                {
                    name: 'accept',
                    text: '确定',
                    class: 'btn btn-primary',
                    handle: ($event: MouseEvent) => {
                        if (!checkBindingData()) {
                            return false;
                        }
                        updateViewModel(bindingData.value[0], bindingType.value);
                        const newBindingValue = updateComponentSchema(bindingData.value[0], bindingType.value);
                        setDisplayText(newBindingValue);

                        // 触发变更
                        if (props.onFieldSelected && typeof props.onFieldSelected == 'function') {
                            props.onFieldSelected(newBindingValue);
                        }
                        return true;
                    }
                }
            ],
            resizeable: false,
            draggable: true
        };

        return () => {
            return (<FButtonEdit
                v-model={displayText.value}
                editable={false}
                disabled={props.disabled}
                readonly={props.readonly}
                inputType={"text"}
                enableClear={false}
                buttonContent={buttonIcon}
                buttonBehavior={"Modal"}
                modalOptions={modalOptions}>
                <div class="h-100 d-flex flex-column">
                    {renderBindingSelector()}
                </div>
            </FButtonEdit>);
        };
    }
});
