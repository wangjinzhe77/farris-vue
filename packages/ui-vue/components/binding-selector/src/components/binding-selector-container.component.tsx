import { defineComponent, ref, onMounted, onBeforeMount } from "vue";
import FTreeGrid from '@farris/ui-vue/components/tree-grid';
import FRadioGroup from '@farris/ui-vue/components/radio-group';
import { RowOptions, VisualData } from "@farris/ui-vue/components/data-view";
import { FormSchemaEntityField$Type } from "@farris/ui-vue/components/common";
import { BindingSelectorProps, bindingSelectorProps, bindingTypes, fieldColumns, variableColumns } from "../binding-selector.props";
import { useFieldSelection } from "../composition/use-field-selection";
import { FNotifyService } from "@farris/ui-vue/components/notify";

export default defineComponent({
    name: 'FBindingSelectorContainer',
    props: bindingSelectorProps,
    emits: ['selected', 'bindingTypeChange', 'cancel', 'submit'],
    setup(props: BindingSelectorProps, context) {

        const dataSource = ref(props.data);
        const bindingType = ref(props.modelValue?.type || 'Form');
        const bindingData = ref(props.bindingData);
        const treegridRef = ref();
        const occupiedFieldset = ref();
        const availableFieldTypes = ref();
        const { getTreeDataSource, initTreeData, resolveOccupiedFields, resolveFieldTypesByEditorType } = useFieldSelection(props);
        const columns = ref<any>([]);
        onBeforeMount(() => {
            initTreeData();
            dataSource.value = getTreeDataSource(bindingType.value);

            if (props.editorParams?.disableOccupiedFields) {
                occupiedFieldset.value = resolveOccupiedFields(props.editorParams?.componentSchema?.binding);
            }
            availableFieldTypes.value = resolveFieldTypesByEditorType(props.editorParams?.componentSchema?.editor?.type);
            columns.value = bindingType.value === 'Form' ? fieldColumns : variableColumns;
        });

        onMounted(() => {
            const bindingFieldId = props.editorParams?.componentSchema?.binding?.field;
            if (bindingFieldId) {
                treegridRef.value.selectItemById(bindingFieldId);
            }
        });

        const treegridContainerStyle = {
            'f-utils-fill': true,
            'm-2': props.bindingType?.enable,
            'mx-2': !props.bindingType?.enable,
            'border': true
        };

        function onBindingTypeChanged(newBindingType: string) {
            columns.value = newBindingType === 'Form' ? fieldColumns : variableColumns;
            treegridRef.value.updateColumns(columns.value);

            dataSource.value = getTreeDataSource(newBindingType);
            treegridRef.value.updateDataSource(dataSource.value);

            treegridRef.value.clearSelection();
            bindingData.value = [];
            context.emit('selected', []);
            context.emit('bindingTypeChange', newBindingType);
        }

        function onSelectionChange(selectedItems: Array<Record<string, any>>) {
            bindingData.value = selectedItems;
            context.emit('selected', selectedItems);
        }

        function renderBindingType() {
            return <div class="px-3">
                <FRadioGroup
                    name={'bindingType'}
                    textField={props.bindingType.textField}
                    valueField={props.bindingType.valueField}
                    enumData={bindingTypes}
                    v-model={bindingType.value}
                    onChangeValue={onBindingTypeChanged}></FRadioGroup>
            </div>;
        }

        // 配置行禁用
        const rowOption: Partial<RowOptions> = {
            customRowStatus: (visualData: VisualData) => {
                // 复杂类型字段，不可选
                if (visualData.raw.$type && visualData.raw.$type !== FormSchemaEntityField$Type.SimpleField) {
                    visualData.disabled = true;
                    return visualData;
                }
                // 表单中已占用的字段，不可选
                if (occupiedFieldset.value && occupiedFieldset.value.has(visualData.raw.id)) {
                    visualData.disabled = true;
                    return visualData;
                }
                // 与当前控件类型不匹配的字段，不可选
                const fieldTypeNode = bindingType.value === 'Form' ? visualData.raw.type?.name : visualData.raw.type;
                if (fieldTypeNode && availableFieldTypes.value && !availableFieldTypes.value.includes(fieldTypeNode)) {
                    visualData.disabled = true;
                    return visualData;
                }
                visualData.disabled = false;
                return visualData;
            }
        };


        function renderTreeGrids() {
            return <FTreeGrid
                ref={treegridRef}
                fit={true}
                data={dataSource.value}
                idField={props.idField}
                columns={columns.value}
                rowNumber={{ enable: false }}
                columnOption={{ fitColumns: true }}
                onSelectionChange={onSelectionChange}
                row-option={rowOption}></FTreeGrid>;
        }
        function onCancel() {
            context.emit('cancel');
        }

        function checkBindingData(): boolean {
            if (!bindingData.value || !bindingData.value.length) {
                const notifyService: any = new FNotifyService();
                notifyService.globalConfig = { position: 'top-center' };
                notifyService.warning({ message: '请先选择数据' });
                return false;
            }
            return true;
        }
        function onSubmit() {
            if (!checkBindingData()) {
                return;
            }
            context.emit('submit', { selectedData: bindingData.value[0], bindingType: bindingType.value });
        }

        return () => {
            return (<div class="h-100 d-flex flex-column" >
                {props.bindingType.enable && renderBindingType()}
                <div class={treegridContainerStyle} style="position:relative;border-radius:10px;"> {renderTreeGrids()}</div>
                {
                    props.showCustomFooter ?
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" onClick={onCancel}>取消</button>
                            <button type="button" class="btn btn-primary" onClick={onSubmit}>确定</button>
                        </div> : ''
                }
            </div>);
        };
    }
});
