import { DesignerHostService } from "@farris/ui-vue/components/designer-canvas";
import { createPropsResolver } from "@farris/ui-vue/components/dynamic-resolver";
import { ExtractPropTypes, PropType } from "vue";
import bindingSelectorSchema from './schema/binding-selector.schema.json';

export const fieldColumns = [
    { field: 'name', title: '名称' },
    { field: 'bindingField', title: '绑定字段' },
    { field: 'fieldType', title: '字段类型' }
];
export const variableColumns = [
    { field: 'name', title: '名称' },
    { field: 'code', title: '编号' },
    { field: 'displayTypeName', title: '变量类型' }
];
export const bindingTypes = [
    { value: 'Form', text: '绑定字段' },
    { value: 'LocaleVariable', text: '绑定组件变量' },
    { value: 'RemoteVariable', text: '绑定表单变量' }
];
export interface BindingSelectorService {
    getData(params: any): Promise<any>;
}

export interface BindingTypeOption {
    enable: boolean;
    data: Array<{ [key: string]: string }>;
    value: string;
    textField: string;
    valueField: string;
}
export interface BindingSelectorEditorParams {
    /** 已选控件schema */
    componentSchema: any;
    /** 已选字段是否需要同步至表单viemodel.fields节点 */
    needSyncToViewModel: { type: boolean, default: false },
    /** 当前控件所属的视图模型id */
    viewModelId: { type: string, default: '' },
    /** 设计器服务类 */
    designerHostService: { type: DesignerHostService },
    /** 表单中已使用的字段是否需要设置为禁用 */
    disableOccupiedFields: { type: boolean, default: false },
}
export const bindingSelectorProps = {
    disabled: { type: Boolean, default: false },
    readonly: { type: Boolean, default: false },
    modelValue: { type: Object, default: null },
    bindingType: {
        type: Object as PropType<BindingTypeOption>, default: {
            enable: true,
            value: 'Form',
            textField: 'text',
            valueField: 'value'
        }
    },

    data: { type: Array<Record<string, any>>, default: [] },
    idField: { type: String, default: 'id' },
    title: { type: String, default: '字段选择器' },
    modalWidth: { type: Number, default: 800 },
    modalHeight: { type: Number, default: 600 },
    multiSelect: { type: Boolean, default: false },
    /**
     * 可选，分隔符
     * 默认`,`
     */
    separator: { type: String, default: ',' },
    bindingData: { type: Array<Record<string, any>>, default: [] },
    textField: { type: String, default: 'code' },
    beforeOpenDialog: { type: Function, default: null },
    editorParams: { type: Object as PropType<BindingSelectorEditorParams>, default: null },
    onFieldSelected: { type: Function, default: null },
    /** 是否显示自定义的底部按钮区域 */
    showCustomFooter: { type: Boolean, default: false }

} as Record<string, any>;

export type BindingSelectorProps = ExtractPropTypes<typeof bindingSelectorProps>;

export const propsResolver = createPropsResolver<BindingSelectorProps>(bindingSelectorProps, bindingSelectorSchema);
