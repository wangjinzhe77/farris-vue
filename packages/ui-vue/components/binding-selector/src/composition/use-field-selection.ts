import { SchemaDOMMapping } from "@farris/ui-vue/components/property-panel";
import { merge } from "lodash-es";
import { BindingSelectorProps } from "../binding-selector.props";

export function useFieldSelection(props: BindingSelectorProps) {

    const { designViewModelUtils } = props.editorParams.designerHostService;
    const { formSchemaUtils } = props.editorParams.designerHostService;
    const { viewModelId } = props.editorParams;
    let fieldTreeData = [];
    let localeVariableTreeData = [];
    let formVariableTreeData = [];

    function getTreeDataSource(bindingType: string) {
        switch (bindingType) {
            case 'LocaleVariable': {
                return localeVariableTreeData;
            }
            case 'RemoteVariable': {
                return formVariableTreeData;
            }
            default: {
                return fieldTreeData;
            }
        }

    }
    function loadFieldTreeData() {
        fieldTreeData = designViewModelUtils.getAllFields2TreeByVMId(viewModelId);
    }
    function loadLocaleVariableTreeData() {

        const viewModel = formSchemaUtils.getViewModelById(viewModelId);
        if (viewModel.parent) {
            const rootLocalVars = formSchemaUtils.getLocaleVariablesByViewModelId(viewModel.parent);
            if (rootLocalVars && rootLocalVars.length) {
                rootLocalVars[0].data.name = '根组件';
                localeVariableTreeData = localeVariableTreeData.concat(rootLocalVars);
            }
        }
        const localVars = formSchemaUtils.getLocaleVariablesByViewModelId(viewModelId);
        localeVariableTreeData = localeVariableTreeData.concat(localVars);

    }
    function loadFormVariableTreeData() {
        formVariableTreeData = formSchemaUtils.getRemoteVariables();
    }
    /**
     * 同步视图模型字段
     */
    function updataViewModelField(bindingData: Record<string, any>) {
        const currentSelectedField = merge({}, bindingData.rawData, { groupId: null, groupName: null });
        const originalBinding = props.editorParams.componentSchema?.binding;
        const dgViewModel = designViewModelUtils.getDgViewModel(viewModelId);

        // 删除原始绑定信息
        if (originalBinding?.field) {
            const originalBindingField = dgViewModel.fields.find(f => f.id === originalBinding.field);
            dgViewModel.removeField([originalBinding.field]);
            if (originalBindingField) {
                const { groupId, groupName, editor } = originalBindingField;
                merge(currentSelectedField, { editor, groupId, groupName });
            }
        }
        // 向视图模型中添加新字段
        dgViewModel.addField(currentSelectedField);
        if (props.editorParams.componentSchema?.editor?.type) {
            dgViewModel.changeField(currentSelectedField.id, { editor: props.editorParams.componentSchema.editor.type });
        }
    }

    function updataViewModelVariable(bindingData: Record<string, any>) {
        const originalBinding = props.editorParams.componentSchema?.binding;
        const dgViewModel = designViewModelUtils.getDgViewModel(viewModelId);

        const currentSelectedVar = {
            type: 'Variable',
            id: bindingData.id,
            fieldName: bindingData.code,
            groupId: '',
            groupName: ''
        };
        // 删除原始绑定信息
        if (originalBinding?.field) {
            const viewModel = formSchemaUtils.getViewModelById(viewModelId);
            const originalBindingField = viewModel?.fields.find(f => f.id === originalBinding?.field);
            if (originalBindingField) {
                currentSelectedVar.groupName = originalBindingField.groupName;
                currentSelectedVar.groupId = originalBindingField.groupId;
            }
            dgViewModel.removeField([originalBinding?.field]);
        }
        // 向视图模型中添加新字段
        formSchemaUtils.addViewModelField(viewModelId, currentSelectedVar);
    }

    /**
     * 同步视图模型
     */
    function updateViewModel(bindingData: Record<string, any>, bindingType: string) {
        if (!props.editorParams.needSyncToViewModel || !props.editorParams.viewModelId || !props.editorParams.designerHostService) {
            return;
        }

        const originalBinding = props.editorParams.componentSchema.binding;
        if (originalBinding && originalBinding.field === bindingData.id) {
            return;
        }
        if (bindingType === 'Form') {
            updataViewModelField(bindingData);
        } else {
            updataViewModelVariable(bindingData);
        }
    }

    /**
     * 更新控件schema数据
     */
    function updateComponentSchema(bindingData: Record<string, any>, bindingType: string) {
        if (!props.editorParams.componentSchema) {
            return;
        }

        if (!props.editorParams.componentSchema.binding) {
            props.editorParams.componentSchema.binding = {};
        }
        switch (bindingType) {
            case 'Form': {
                props.editorParams.componentSchema.binding.path = bindingData.bindingField;
                props.editorParams.componentSchema.binding.field = bindingData.id;
                props.editorParams.componentSchema.binding.fullPath = bindingData.path;
                break;
            }
            case 'LocaleVariable': {
                props.editorParams.componentSchema.binding.path = bindingData.viewModelId === viewModelId ? bindingData.code : 'root-component.' + bindingData.code;
                props.editorParams.componentSchema.binding.field = bindingData.id;
                props.editorParams.componentSchema.binding.fullPath = bindingData.code;
                break;
            }
            case 'RemoteVariable': {
                props.editorParams.componentSchema.binding.path = 'root-component.' + bindingData.code;
                props.editorParams.componentSchema.binding.field = bindingData.id;
                props.editorParams.componentSchema.binding.fullPath = bindingData.code;
                break;
            }
        }

        props.editorParams.componentSchema.binding.type = bindingType;
        props.editorParams.componentSchema.path = bindingData.bindingPath;
        return props.editorParams.componentSchema.binding;
    }

    /**
     * 收集绑定同一个实体的同类型组件中已使用的字段（form类、dataGrid类...）
     * @param currentBinding 
     */
    function resolveOccupiedFields(currentBinding: any) {
        const currentBindingId = currentBinding?.field;
        const occupiedFieldSet = new Set<string>();
        const formSchema = formSchemaUtils.getFormSchema().module;
        const viewModelNode = formSchemaUtils.getViewModelById(viewModelId);
        const componentNode = formSchema.components.find(component => component.viewModel === viewModelId);


        // 当前组件的类型
        let targetComponentType = componentNode.componentType;

        // 根组件和table组件内的输入控件与form内不能重复
        if (targetComponentType === 'frame' || targetComponentType === 'table') {
            targetComponentType = 'form';
        }
        formSchema.viewmodels.forEach(viewModel => {
            if (!viewModel.fields || viewModel.fields.length === 0) {
                return;
            }
            const componentNode = formSchema.components.find(component => component.viewModel === viewModel.id);;
            let { componentType } = componentNode;
            if (componentType === 'frame' || componentType === 'table') {
                componentType = 'form';
            }
            // 同类型的组件并且绑定同一个实体（form类、dataGrid类...）
            if (componentType !== targetComponentType || viewModel.bindTo !== viewModelNode.bindTo) {
                return;
            }

            viewModel.fields.forEach(field => {
                if (field.id !== currentBindingId) {
                    occupiedFieldSet.add(field.id);
                }
            });
        });
        return occupiedFieldSet;
    }

    /** *
     * 获取当前控件类型下可选的字段类型
     */
    function resolveFieldTypesByEditorType(editorType: string): string[] {
        if (!editorType) {
            return [];
        }

        const controlFieldMapping = {};
        const { fieldControlTypeMapping } = SchemaDOMMapping;

        for (const fieldType in fieldControlTypeMapping) {
            const controlTypes = fieldControlTypeMapping[fieldType];
            if (!controlTypes || !controlTypes.length) {
                continue;
            }
            controlTypes.forEach(item => {
                controlFieldMapping[item.key] = controlFieldMapping[item.key] || [];
                controlFieldMapping[item.key].push(fieldType);
            });
        }

        // 追加多语控件
        // controlFieldMapping.LanguageTextBox = ['multiLanguage'];

        return controlFieldMapping[editorType];
    }

    /**
     * 获取树表格数据
     */
    function initTreeData() {
        loadFieldTreeData();
        loadLocaleVariableTreeData();
        loadFormVariableTreeData();
    }

    return {
        initTreeData,
        getTreeDataSource,
        updateViewModel,
        updateComponentSchema,
        resolveOccupiedFields,
        resolveFieldTypesByEditorType
    };
}
