/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App, Plugin } from 'vue';
import BindingSelector from './src/binding-selector.component';
import FBindingSelectorContainer from './src/components/binding-selector-container.component';
import { propsResolver } from './src/binding-selector.props';

export * from './src/binding-selector.props';

BindingSelector.install = (app: App) => {
    app.component(BindingSelector.name as string, BindingSelector);
    app.component(FBindingSelectorContainer.name as string, FBindingSelectorContainer);
};
BindingSelector.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap['binding-selector'] = BindingSelector;
    propsResolverMap['binding-selector'] = propsResolver;
};
export { BindingSelector, FBindingSelectorContainer };

export default BindingSelector as typeof BindingSelector & Plugin;
