import { FormSchemaEntity } from "../../../common/entity/entity-schema";
import { DesignerComponentInstance } from "../../../designer-canvas";


export interface ComponentBuildInfo {
    componentId: string;
    /** 组件名称 */
    componentName: string;
    /** 组件类型 */
    componentType?: string;
    /** 卡片类组件中每行放置的控件列数 */
    formColumns?: number;
    /** 拖拽的父容器id */
    parentContainerId?: string;
    /** 拖拽的父容器类型*/
    parentContainerType?: string;
    /** 拖拽的父容器实例*/
    parentComponentInstance?: DesignerComponentInstance;
    /** 表格类组件中表格是否可编辑 */
    editable?: boolean;
    /** 显示在卡片或表格中的字段 */
    selectedFields?: any[];
    /** 组件绑定 */
    bindTo: string;
    /** 表格类组件的数据源 */
    dataSource?: string;
    /** 绑定的实体 */
    selectedEntity?: FormSchemaEntity;
}
