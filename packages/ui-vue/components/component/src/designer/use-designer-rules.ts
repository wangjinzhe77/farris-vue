import { DesignerHostService, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { DesignerItemContext } from "../../../designer-canvas/src/types";
import { UseTemplateDragAndDropRules } from "../../../designer-canvas/src/composition/rule/use-template-rule";
import { ComponentProperty } from "../property-config/component.property-config";

export function useDesignerRules(designItemContext: DesignerItemContext, designerHostService?: DesignerHostService): UseDesignerRules {
    const dragAndDropRules = new UseTemplateDragAndDropRules();
    const { canMove, canDelete } = dragAndDropRules.getTemplateRule(designItemContext, designerHostService);

    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(): boolean {
        return false;
    }

    function checkCanDeleteComponent() {
        return canDelete;
    }

    function checkCanMoveComponent() {
        return canMove;
    }


    function hideNestedPaddingInDesginerView() {
        return false;
    }
    function getStyles(): string {
        return ' display: inherit;flex-direction: inherit;margin-bottom:10px';
    }
    /**
     * 获取属性配置
     */
    function getPropsConfig(componentId: string) {
        const componentProp = new ComponentProperty(componentId, designerHostService);
        const { schema } = designItemContext;
        return componentProp.getPropertyConfig(schema);
    }

    /**
     * 删除DataGrid/Form类组件时，将父级Section或者TabPage上的相关按钮一起删除
     */
    function removeAddDeleteBtnOnParentContainer() {
        const schema = designItemContext?.schema;
        const formSchemaUtils = designerHostService?.formSchemaUtils;
        const viewModelId = formSchemaUtils.getViewModelIdByComponentId(schema.id);

        if (schema.componentType !== 'data-grid' && schema.componentType !== 'form') {
            return;
        }
        if (!designItemContext.componentInstance?.value.parent) {
            return;
        }
        const parentComponentSchema = designItemContext.componentInstance?.value.parent['schema'];
        if (parentComponentSchema?.toolbar?.buttons && parentComponentSchema.toolbar.buttons.length) {
            parentComponentSchema.toolbar.buttons.forEach(button => {
                const clickEvent = button.onClick;
                // 判断三段式结构
                const clickEventPath = clickEvent && clickEvent.split('.');
                if (!clickEventPath || clickEventPath.length < 3) {
                    return;
                }
                const targetViewModelId = clickEventPath[clickEventPath.length - 2];

                // 按钮绑定的命令若是在当前viewModel下，则将按钮标记为待删除
                if (targetViewModelId === viewModelId) {
                    button.needRemove = true;
                }
            });
            parentComponentSchema.toolbar.buttons = parentComponentSchema.toolbar.buttons.filter(button => !button.needRemove);

            // 为解决标签页画布无法更新的问题，手动触发update方法
            const parentComponentInstance = designItemContext.componentInstance?.value.parent;
            if (parentComponentInstance['parent'] && parentComponentInstance['parent']['updateToolbarItems']) {
                parentComponentInstance['parent']['updateToolbarItems']();
            }
        }

    }
    /**
     * 组件删除后事件：移除viewmodel和component
     */
    function removeViewModelComponent() {
        const designViewModelUtils = designerHostService?.designViewModelUtils;
        const formSchemaUtils = designerHostService?.formSchemaUtils;
        const schema = designItemContext?.schema;

        if (designViewModelUtils && formSchemaUtils) {
            const viewModelId = formSchemaUtils.getViewModelIdByComponentId(schema.id);
            designViewModelUtils.deleteViewModelById(viewModelId);

            formSchemaUtils.deleteComponent(schema.id);
        }
    }
    /**
     * 组件删除后事件：移除表达式、界面规则、受控规则等全局配置
     */
    function removeGlobalConfigs() {
        const designViewModelUtils = designerHostService?.designViewModelUtils;
        const formSchemaUtils = designerHostService?.formSchemaUtils;
        const schema = designItemContext?.schema;
        const viewModelId = formSchemaUtils.getViewModelIdByComponentId(schema.id);

        designViewModelUtils.getDgViewModel(viewModelId).fields.forEach(field => {

            // 若绑定字段配置了表达式，需要删除表达式
            if (formSchemaUtils.getExpressions() && formSchemaUtils.getExpressions().length) {
                const expFieldIndex = formSchemaUtils.getExpressions().findIndex(e => e.fieldId === field.id);
                if (expFieldIndex > -1) {
                    formSchemaUtils.getExpressions().splice(expFieldIndex, 1);
                }
            }
        });
    }
    /**
     * 组件删除后事件
     */
    function onRemoveComponent() {
        removeAddDeleteBtnOnParentContainer();
        removeGlobalConfigs();
        removeViewModelComponent();
    }
    return { canAccepts, checkCanDeleteComponent, checkCanMoveComponent, hideNestedPaddingInDesginerView, getStyles, getPropsConfig, onRemoveComponent };
}
