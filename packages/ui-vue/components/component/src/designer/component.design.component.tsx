import { DesignerHostService } from '../../../designer-canvas/src/composition/types';
import { SetupContext, defineComponent, inject, ref, onMounted, computed } from 'vue';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { ComponentPropsType, componentProps } from '../component.props';
import { useDesignerRules } from './use-designer-rules';
import { getCustomClass } from '@farris/ui-vue/components/common';

export default defineComponent({
    name: 'FComponetDesign',
    props: componentProps,
    emits: ['init', 'afterViewInit'],
    setup(props: ComponentPropsType, context) {
        const elementRef = ref();
        const designerHostService = inject<DesignerHostService>('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

        const componentClass = computed(() => {
            const classObject = {
                'drag-container': true
            } as Record<string, any>;
            return getCustomClass(classObject, props?.customClass);
        });
        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;

            if (designItemContext.parent?.schema?.type === 'component-ref') {
                componentInstance.value.parent = designItemContext.parent?.parent?.componentInstance;
            }
        });

        context.expose(componentInstance.value);

        return () => {
            return (
                <div ref={elementRef} class={componentClass.value} style={props.customStyle} data-dragref={`${designItemContext.schema.id}-container`}>
                    {context.slots.default && context.slots.default()}
                </div>
            );
        };
    }
});
