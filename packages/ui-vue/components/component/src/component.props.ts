 
import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import componentSchema from './schema/component.schema.json';
import { schemaResolver } from './schema/schema-resolver';

export const componentProps = {
    id: { type: String },
    customClass: { type: String, default: '' },
    customStyle: { type: String, default: '' },
    componentType: { type: String, default: '' },
    formColumns: { type: Number, default: 4 }
} as Record<string, any>;

export type ComponentPropsType = ExtractPropTypes<typeof componentProps>;

export const propsResolver = createPropsResolver<ComponentPropsType>(componentProps, componentSchema, schemaMapper, schemaResolver);

export const componentDesignProps = Object.assign({}, componentProps, { componentId: { type: String, default: '' } });
export type ComponentDesignPropsType = ExtractPropTypes<typeof componentDesignProps>;


