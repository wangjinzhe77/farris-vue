import { SetupContext, defineComponent, onBeforeMount, onMounted } from 'vue';
import { ComponentPropsType, componentProps } from './component.props';

export default defineComponent({
    name: 'FComponent',
    props: componentProps,
    emits: ['init', 'afterViewInit'],
    setup(props: ComponentPropsType, context) {
        onBeforeMount(() => {
            context.emit('init', props.id);
        });
        onMounted(() => {
            context.emit('afterViewInit', props.id);
        });
        return () => {
            return <div class={props.customClass}>{context.slots.default && context.slots.default()}</div>;
        };
    }
});
