import { PropertyChangeObject } from "../../../property-panel/src/composition/entity/property-entity";
import { BaseControlProperty } from "../../../property-panel/src/composition/entity/base-property";

import { DesignerComponentInstance } from "@farris/ui-vue/components/designer-canvas";

export class ComponentProperty extends BaseControlProperty {
    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }
    public getPropertyConfig(propertyData: any) {
        // 基本信息
        this.propertyConfig.categories['basic'] = this.getBasicPropConfig(propertyData);
        // 外观
        this.propertyConfig.categories['appearance'] = this.getAppearanceConfig(propertyData);
         // 事件
        this.getEventPropConfig(propertyData);
        return this.propertyConfig;
    }
    private getEventPropConfig(propertyData: any) {
        const events = [
            {
                label: 'onInit',
                name: '初始化事件'
            },
            {
                label: 'onAfterViewInit',
                name: '视图初始化后事件'
            },
        ];
        const self = this;
        const initialData = self.eventsEditorUtils['formProperties'](propertyData, self.viewModelId, events);
        const properties = {};
        properties[self.viewModelId] = {
            type: 'events-editor',
            editor: {
                initialData
            }
        };
        this.propertyConfig.categories['eventsEditor'] = {
            title: '事件',
            hideTitle: true,
            properties,
            // 这个属性，标记当属性变更得时候触发重新更新属性
            refreshPanelAfterChanged: true,
            tabId: 'commands',
            tabName: '交互',
            setPropertyRelates(changeObject: any, data: any) {
                const parameters = changeObject.propertyValue;
                delete propertyData[self.viewModelId];
                if (parameters) {
                    parameters.setPropertyRelates = this.setPropertyRelates; // 添加自定义方法后，调用此回调方法，用于处理联动属性
                    self.eventsEditorUtils.saveRelatedParameters(propertyData, self.viewModelId, parameters['events'], parameters);
                }
            }
        };
    }
}
