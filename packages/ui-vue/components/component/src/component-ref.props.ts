 
import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import componentRefSchema from './schema/component-ref.schema.json';
import { schemaResolver } from './schema/schema-resolver';

export const componentProps = {
    id: { type: String },
    component: { type: String, default: '' },
} as Record<string, any>;

export type ComponentPropsType = ExtractPropTypes<typeof componentProps>;

export const componentPropsResolver = createPropsResolver<ComponentPropsType>(componentProps, componentRefSchema, schemaMapper, schemaResolver);
