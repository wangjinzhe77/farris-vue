
import { ExtractPropTypes, PropType } from 'vue';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import { createPropsResolver } from '../../dynamic-resolver/src/props-resolver';
import { DataColumn, HierarchyOptions, RowOptions, SelectionOptions } from '../../data-view';
import propertyConfig from './property-config/tree-view.property-config.json';
import treeViewSchema from './schema/tree-view.schema.json';

export const treeViewProps = {
    /** 列集合 */
    columns: {
        type: Array<DataColumn>, default: [
            { field: 'name', title: '', dataType: 'string' }
        ]
    },
    displayField: { type: String, default: 'name', require: true },
    /** 是否显示连接线 */
    showLines: { type: Boolean, default: false },/** 树表数据 */
    /** 被绑定数据 */
    data: { type: Object, default: [] },
    /** 禁用组件 */
    disable: { type: Boolean, default: false },
    disabledField: { type: String, default: 'disabled' },
    /** 适配父组件尺寸 */
    fit: { type: Boolean, default: false },
    /** 高度 */
    height: { type: Number, default: -1 },
    /** DataGrid组件唯一标识 */
    id: { type: String, default: '' },
    /** 被绑定数据的标识字段 */
    idField: { type: String, default: 'id', require: true },
    /** 最小高度 */
    minHeight: { type: Number, default: 300 },
    /** 最小宽度 */
    minWidth: { type: Number, default: 400 },
    /** 分页配置 */
    pagination: {
        type: Object, default: {
            enable: false,
            size: -1
        }
    },
    /** 选择配置 */
    selection: {
        type: Object as PropType<SelectionOptions>, default: {
            enableSelectRow: true,
            multiSelect: false,
            multiSelectMode: 'DependOnCheck',
            showCheckbox: false,
            showSelectAll: false,
            showSelection: true
        }
    },
    /** 已选数据标识 */
    selectionValues: { type: Array<string>, default: [] },
    /** 是否显示图标集 */
    showTreeNodeIcons: { type: Boolean, default: false },
    /** 树节点图标数据 */
    treeNodeIconsData: { type: [Object, String], default: {} },

    /** 绑定数据中的图标属性key值 */
    iconField: { type: String, default: '' },

    /** 宽度 */
    width: { type: Number, default: -1 },
    /** 新增值 */
    newDataItem: {
        type: [Function as PropType<(...args: unknown[]) => any>, Object], default: () => { }
    },
    /** 连接线颜色 */
    lineColor: { type: String, default: '#9399a0' },
    /** 单元格高度 */
    cellHeight: { type: Number, default: 28 },
    onOutputValue: { type: Function, default: () => { } },
    onCurrentEvent: { type: Function, default: () => { } },
    hierarchy: {
        type: Object as PropType<HierarchyOptions>, default: {
            cascadeOption: {
                autoCheckChildren: false,
                autoCheckParent: false,
                selectionRange: 'All'
            },
            parentIdField: 'parent'
        }
    },
    /** 行配置 */
    rowOption: { type: Object as PropType<RowOptions> },
    /** 虚拟化渲染数据 */
    virtualized: { type: Boolean, default: false }
} as Record<string, any>;

export type TreeViewProps = ExtractPropTypes<typeof treeViewProps>;

export const propsResolver = createPropsResolver<TreeViewProps>(treeViewProps, treeViewSchema, schemaMapper, schemaResolver, propertyConfig);
