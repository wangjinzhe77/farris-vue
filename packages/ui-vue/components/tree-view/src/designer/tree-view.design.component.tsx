

import { Ref, SetupContext, computed, defineComponent, inject, ref } from 'vue';
import { TreeViewProps, treeViewProps } from '../tree-view.props';
import {
    DataViewOptions, UseCellEditor, VisualData, useCellPosition, useColumn, useDataView, useDataViewContainerStyle, useEdit, useFilter, useHierarchy, useIdentify,
    usePagination, useRow, useSelectHierarchyItem, useSelection, useVirtualScroll, useVisualData, useVisualDataBound, useVisualDataCell, useVisualDataRow
} from '../../../data-view';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import getTreeArea from '../components/data/tree-area.component';

export default defineComponent({
    name: 'FTreeViewDesign',
    props: treeViewProps,
    emits: ['outputValue', 'currentEvent', 'selectionChange'] as (string[]) | undefined,
    setup(props: TreeViewProps, context: SetupContext) {
        const elementRef = ref();
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const componentInstance = useDesignerComponent(elementRef, designItemContext);
        const primaryGridContentRef = ref<any>();
        const idField = ref(props.idField);
        const treeContentRef = ref<any>();
        const mouseInContent = ref(false);
        const visibleDatas = ref<VisualData[]>([]);
        const useFilterComposition = useFilter();
        const useHierarchyCompostion = useHierarchy(props as DataViewOptions);
        const useIdentifyComposition = useIdentify(props as DataViewOptions);
        /** 处理树状数据 */
        const treeDataView = useDataView(props as DataViewOptions, new Map(), useFilterComposition, useHierarchyCompostion, useIdentifyComposition);
        const useSelectionComposition = useSelection(props as DataViewOptions, treeDataView, useIdentifyComposition, visibleDatas, context);
        const useSelectHierarchyItemComposition = useSelectHierarchyItem(props as DataViewOptions, visibleDatas, useIdentifyComposition, useSelectionComposition, context);

        const treeClass = computed(() => {
            const classObject = {
                'fv-tree': true
            } as Record<string, boolean>;
            return classObject;
        });

        const treeContentClass = computed(() => {
            const classObject = {
                'fv-tree-content': true
            } as Record<string, boolean>;
            return classObject;
        });
        const { containerStyleObject } = useDataViewContainerStyle(props as DataViewOptions);

        /** 处理鼠标滚轮事件 */
        function onWheel(e: MouseEvent) { }

        /** 渲染水平滚动条 */
        function renderHorizontalScrollbar() {
            return [];
        }

        /** 渲染垂直滚动条 */
        function renderVerticalScrollbar() {
            return [];
        }
        const columns = ref(props.columns);
        const useColumnComposition = useColumn(props as DataViewOptions);
        const { columnContext } = useColumnComposition;

        const defaultVisibleCapacity = 20;
        const preloadCount = 0;

        const visibleCapacity = computed(() => {
            return treeDataView.dataView.value.length;
        });

        const useRowComposition = useRow(props as DataViewOptions, context, useSelectionComposition, useIdentifyComposition);

        const useEditComposition = useEdit(props as DataViewOptions, context as SetupContext, useIdentifyComposition, useRowComposition);

        const useVisualDataBoundComposition = useVisualDataBound(props as DataViewOptions);

        const useVisualDataCellComposition = useVisualDataCell(
            props as DataViewOptions, {} as UseCellEditor, useVisualDataBoundComposition
        );

        const useVisualDataRowComposition = useVisualDataRow(
            props as DataViewOptions,
            useEditComposition,
            useHierarchyCompostion,
            useIdentifyComposition,
            useVisualDataBoundComposition,
            useVisualDataCellComposition
        );

        const useVisualDataComposition = useVisualData(
            props as DataViewOptions,
            columns,
            treeDataView,
            visibleCapacity,
            preloadCount,
            useVisualDataRowComposition
        );
        const { getVisualData } = useVisualDataComposition;

        const useCellPositionComposition = useCellPosition(props as DataViewOptions, columnContext);

        visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);


        const useVirtualScrollComposition = useVirtualScroll(
            props as DataViewOptions,
            treeDataView,
            visibleDatas,
            columnContext,
            useVisualDataComposition,
            visibleCapacity,
            preloadCount,
            ref(0)
        );

        /** 渲染树状区域 */
        const { renderTreeArea } = getTreeArea(props, context, visibleDatas, useCellPositionComposition, useColumnComposition, 
            treeDataView, useEditComposition, useHierarchyCompostion, useRowComposition, useSelectionComposition, useSelectHierarchyItemComposition, 
            useVirtualScrollComposition, useVisualDataComposition, useVisualDataBoundComposition,primaryGridContentRef);

        function getSelectedRowVisualIndex() {
            const currentSelectionRow = useSelectionComposition.getSelectionRow();
            const visualIndex = currentSelectionRow ? currentSelectionRow.dataIndex - 1 : -1;
            return visualIndex;
        }

        /** 新增树节点 */
        function addNewDataItem() {
            const targetIndex = getSelectedRowVisualIndex();
            treeDataView.insertNewDataItem(targetIndex);
            visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
        }

        function addNewChildDataItem() {
            const targetIndex = getSelectedRowVisualIndex();
            treeDataView.insertNewChildDataItem(targetIndex);
            visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
        }

        /** 删除树节点 */
        function removeDataItem() {
            const targetIndex = getSelectedRowVisualIndex();
            const nextDataItemIdToBeSelected = useSelectHierarchyItemComposition.getNextSelectableHierarchyItemId(targetIndex);
            treeDataView.removeHierarchyDataItem(targetIndex);
            visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
            if (nextDataItemIdToBeSelected) {
                useSelectionComposition.selectItemById(nextDataItemIdToBeSelected);
            }
        }

        /** 编辑树节点 */
        function editDataItem(editIndex: string | number, newName: string) {
            treeDataView.editDataItem(editIndex, newName);
            visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
        }

        /** 确定 */
        function acceptDataItem() {
            // treeDataView.acceptEditingRow(visualDataRow);
        }

        /** 取消 */
        function cancelDataItem() {
            // treeDataView.cancelEditingRow(visualDataRow);
        }

        context.expose(componentInstance.value);

        return () => {
            return (
                <div class={treeClass.value} style={containerStyleObject.value} onWheel={onWheel}>
                    <div ref={treeContentRef} class={treeContentClass.value} onMouseover={() => { mouseInContent.value = true; }} onMouseleave={() => { mouseInContent.value = false; }}>
                        {renderTreeArea()}
                        {renderHorizontalScrollbar()}
                        {renderVerticalScrollbar()}
                    </div>
                </div>
            );
        };
    }
});
