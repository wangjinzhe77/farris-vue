

import { SetupContext, computed, defineComponent, nextTick, onMounted, ref, watch } from 'vue';
import { TreeViewProps, treeViewProps } from './tree-view.props';
import {
    DataViewOptions, UseCellEditor, VisualData, getHorizontalScrollbar, getVerticalScrollbar, useCellPosition, useColumn,
    useDataView, useDataViewContainerStyle, useEdit, useFilter, useHierarchy, useIdentify, useRow, useSelection, useVirtualScroll, useVisualData, useVisualDataBound,
    useVisualDataCell, useVisualDataRow
} from '../../data-view';
import getTreeArea from './components/data/tree-area.component';
import getContentHeader from './components/header/content-header.component';
import { useSelectHierarchyItem } from '../../data-view/composition/hierarchy/use-select-hierarchy-item';
import { useResizeObserver } from '@vueuse/core';
import './tree-view.css';
export default defineComponent({
    name: 'FTreeView',
    props: treeViewProps,
    emits: ['outputValue', 'currentEvent', 'selectionChange','expandNode'] as (string[]) | undefined,
    setup(props: TreeViewProps, context: SetupContext) {
        const rowHeight = props.rowOption?.height || 28;
        const treeContentRef = ref<any>();
        const mouseInContent = ref(false);
        const primaryGridContentRef = ref<any>();
        const defaultVisibleCapacity = ref(20);
        const visibleDatas = ref<VisualData[]>([]);
        const useFilterComposition = useFilter();
        const useIdentifyComposition = useIdentify(props as DataViewOptions);
        const useHierarchyCompostion = useHierarchy(props as DataViewOptions);
        /** 处理树状数据 */
        const treeDataView = useDataView(props as DataViewOptions, new Map(), useFilterComposition, useHierarchyCompostion, useIdentifyComposition);
        const useSelectionComposition = useSelection(props as DataViewOptions, treeDataView, useIdentifyComposition, visibleDatas, context);
        const useSelectHierarchyItemComposition = useSelectHierarchyItem(props as DataViewOptions, visibleDatas, useIdentifyComposition, useSelectionComposition, context);

        const treeClass = computed(() => {
            const classObject = {
                // 'fv-tree': true
                'fv-grid': true,
                'fv-tree-view': true
            } as Record<string, boolean>;
            return classObject;
        });

        const treeContentClass = computed(() => {
            const classObject = {
                // 'fv-tree-content': true
                'fv-grid-content': true,
                'fv-grid-content-hover': mouseInContent.value,
            } as Record<string, boolean>;
            return classObject;
        });
        const { containerStyleObject } = useDataViewContainerStyle(props as DataViewOptions);

        /** 处理鼠标滚轮事件 */
        // function onWheel(e: MouseEvent) { }

        // /** 渲染水平滚动条 */
        // function renderHorizontalScrollbar() {
        //     return [];
        // }

        // /** 渲染垂直滚动条 */
        // function renderVerticalScrollbar() {
        //     return [];
        // }
        const columns = ref(props.columns);
        const useColumnComposition = useColumn(props as DataViewOptions);
        const { columnContext } = useColumnComposition;

        const preloadCount = 0;
        // const defaultVisibleCapacity = 100;
        const visibleCapacity = computed(() => {
            // return treeDataView.dataView.value.length;
            return props.virtualized ? Math.min(treeDataView.dataView.value.length, defaultVisibleCapacity.value) : treeDataView.dataView.value.length;
        });

        const useRowComposition = useRow(props as DataViewOptions, context, useSelectionComposition, useIdentifyComposition);

        const useEditComposition = useEdit(props as DataViewOptions, context as SetupContext, useIdentifyComposition, useRowComposition);

        const useVisualDataBoundComposition = useVisualDataBound(props as DataViewOptions);

        const useVisualDataCellComposition = useVisualDataCell(
            props as DataViewOptions, {} as UseCellEditor, useVisualDataBoundComposition
        );

        const useVisualDataRowComposition = useVisualDataRow(
            props as DataViewOptions,
            useEditComposition,
            useHierarchyCompostion,
            useIdentifyComposition,
            useVisualDataBoundComposition,
            useVisualDataCellComposition
        );

        const useVisualDataComposition = useVisualData(
            props as DataViewOptions,
            columns,
            treeDataView,
            visibleCapacity,
            preloadCount,
            useVisualDataRowComposition
        );
        const { getVisualData } = useVisualDataComposition;

        const useCellPositionComposition = useCellPosition(props as DataViewOptions, columnContext);

        // visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);

        const useVirtualScrollComposition = useVirtualScroll(
            props as DataViewOptions,
            treeDataView,
            visibleDatas,
            columnContext,
            useVisualDataComposition,
            visibleCapacity,
            preloadCount,
            ref(0)
        );
        const { onWheel, dataGridWidth, viewPortHeight, viewPortWidth, resetScroll, updateVisibleRowsOnLatestVisibleScope } = useVirtualScrollComposition;
        const { renderVerticalScrollbar } = getVerticalScrollbar(props as DataViewOptions, treeContentRef, useVirtualScrollComposition);
        const { renderHorizontalScrollbar } = getHorizontalScrollbar(props as DataViewOptions, treeContentRef, useVirtualScrollComposition);


        function getHeaderFactory() {
            if (props.header === 'ContentHeader') {
                return getContentHeader;
            }
            return getContentHeader;
        }

        const headerFactroy = getHeaderFactory();

        const { renderHeader } = headerFactroy(props, context);

        /** 渲染树状区域 */
        const { renderTreeArea } = getTreeArea(
            props, context, visibleDatas,
            useCellPositionComposition, useColumnComposition, treeDataView,
            useEditComposition, useHierarchyCompostion, useRowComposition,
            useSelectionComposition, useSelectHierarchyItemComposition,
            useVirtualScrollComposition, useVisualDataComposition, useVisualDataBoundComposition,
            primaryGridContentRef
        );

        function getSelectedRowVisualIndex() {
            const currentSelectionRow = useSelectionComposition.getSelectionRow();
            const visualIndex = currentSelectionRow ? currentSelectionRow.dataIndex - 1 : -1;
            return visualIndex;
        }
        function onGridContentResize() {
            const newVisibleCapacity = Math.ceil(treeContentRef.value.clientHeight / rowHeight);
            if (newVisibleCapacity > defaultVisibleCapacity.value) {
                defaultVisibleCapacity.value = newVisibleCapacity;
                updateVisibleRowsOnLatestVisibleScope();
            }
            viewPortHeight.value = primaryGridContentRef.value?.clientHeight || 0;
            dataGridWidth.value = treeContentRef.value?.clientWidth || 0;
        }
        onMounted(() => {
            if (treeContentRef.value) {
                defaultVisibleCapacity.value = Math.max(Math.ceil(treeContentRef.value.clientHeight / rowHeight),
                    defaultVisibleCapacity.value);
                visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
                useResizeObserver(treeContentRef.value, onGridContentResize);
                // calculateColumnsSize();
                nextTick(() => {
                    if (treeContentRef.value) {
                        dataGridWidth.value = treeContentRef.value.clientWidth;
                    }
                    if (primaryGridContentRef.value) {
                        viewPortWidth.value = primaryGridContentRef.value.clientWidth;
                        viewPortHeight.value = primaryGridContentRef.value.clientHeight;
                    }
                });
            }
        });

        /** 新增树节点 */
        function addNewDataItem() {
            const targetIndex = getSelectedRowVisualIndex();
            treeDataView.insertNewDataItem(targetIndex);
            visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
        }

        function addNewChildDataItem() {
            const targetIndex = getSelectedRowVisualIndex();
            treeDataView.insertNewChildDataItem(targetIndex);
            visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
        }

        /** 删除树节点 */
        function removeDataItem() {
            const targetIndex = getSelectedRowVisualIndex();
            const nextDataItemIdToBeSelected = useSelectHierarchyItemComposition.getNextSelectableHierarchyItemId(targetIndex);
            treeDataView.removeHierarchyDataItem(targetIndex);
            visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
            if (nextDataItemIdToBeSelected) {
                useSelectionComposition.selectItemById(nextDataItemIdToBeSelected);
            }
        }

        /** 编辑树节点 */
        function editDataItem(editIndex: string | number, newName: string) {
            treeDataView.editDataItem(editIndex, newName);
            visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
        }

        /** 确定 */
        function acceptDataItem() {
            // treeDataView.acceptEditingRow(visualDataRow);
        }

        /** 取消 */
        function cancelDataItem() {
            // treeDataView.cancelEditingRow(visualDataRow);
        }

        /** 选中指定行 */
        function selectItem(targetIndex: number) {
            if (targetIndex > -1) {
                const itemToSelect = visibleDatas.value[targetIndex];
                if (itemToSelect) {
                    useSelectionComposition.selectItem(itemToSelect);
                }
            }
        }
        function updateDataSource(newData: Record<string, any>[]) {
            if (newData) {
                const currentSelectionRow = useSelectionComposition.getSelectionRow();

                // 刷新树结构并重绘
                treeDataView.load(newData);
                // visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
                resetScroll();
                // 重新定位选中行
                if (currentSelectionRow && currentSelectionRow.raw && currentSelectionRow.raw.originalId) {
                    const selectTreeNodeIndex = newData.findIndex((data: any) => data.originalId === currentSelectionRow.raw.originalId);
                    if (selectTreeNodeIndex > -1) {
                        selectItem(selectTreeNodeIndex);
                    }
                }

            }
        }
        function clearSelection() {
            useSelectionComposition.clearSelection();
        }
        function selectItemById(dataItemId: string) {
            useSelectionComposition.selectItemById(dataItemId);
        }
        context.expose({
            addNewDataItem,
            addNewChildDataItem,
            removeDataItem,
            editDataItem,
            acceptDataItem,
            cancelDataItem,
            selectItem,
            updateDataSource,
            clearSelection,
            selectItemById
        });

        // watch(
        //     () => [props.data],
        //     ([newValue]) => {
        //         const currentSelectionRow = useSelectionComposition.getSelectionRow();

        //         // 刷新树结构并重绘
        //         treeDataView.load(newValue);
        //         visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);

        //         // 重新定位选中行
        //         if (currentSelectionRow && currentSelectionRow.raw && currentSelectionRow.raw.originalId) {
        //             const selectTreeNodeIndex = newValue.findIndex((data: any) => data.originalId === currentSelectionRow.raw.originalId);
        //             if (selectTreeNodeIndex > -1) {
        //                 selectItem(selectTreeNodeIndex);
        //             }
        //         }
        //     }
        // );
        return () => {
            return (
                <div class={treeClass.value} style={containerStyleObject.value} onWheel={onWheel}>
                    {renderHeader()}
                    <div ref={treeContentRef} class={treeContentClass.value} onMouseover={() => { mouseInContent.value = true; }} onMouseleave={() => { mouseInContent.value = false; }}>
                        {renderTreeArea()}
                        {treeContentRef.value && renderHorizontalScrollbar()}
                        {treeContentRef.value && renderVerticalScrollbar()}
                    </div>
                </div>
            );
        };
    }
});
