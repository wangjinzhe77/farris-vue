import { SetupContext } from "vue";
import { TreeViewProps } from "../../tree-view.props";

export default function (props: TreeViewProps, context: SetupContext) {

    function renderHeader() {
        return (
            context.slots.header && <div class="f-tree-view-header">{context.slots.header()}</div>
        );
    }

    return { renderHeader };

}
