import { Ref, SetupContext } from 'vue';
import { TreeViewProps } from '../../tree-view.props';
import { DataViewOptions, UseCellPosition, UseColumn, UseDataView, UseEdit, UseHierarchy, UseRow, UseSelectHierarchyItem, UseSelection, UseVirtualScroll, UseVisualData, UseVisualDataBound, VisualData, getHierarchyRow } from '../../../../data-view';

/** 渲染树状区域 */
export default function (
    props: TreeViewProps,
    context: SetupContext,
    visibleDatas: Ref<VisualData[]>,
    useCellPositionComposition: UseCellPosition,
    useColumnComposition: UseColumn,
    useDataViewComposition: UseDataView,
    useEditComposition: UseEdit,
    useHierarchyComposition: UseHierarchy,
    useRowComposition: UseRow,
    useSelectionComposition: UseSelection,
    useSelectHierarchyItemComposition: UseSelectHierarchyItem,
    useVirtualScrollComposition: UseVirtualScroll,
    useVisualDataComposition: UseVisualData,
    useVisualDataBoundComposition: UseVisualDataBound,
    primaryGridContentRef: Ref<any>
) {
    const { calculateCellPositionInRow } = useCellPositionComposition;
    const { columnContext } = useColumnComposition;
    const { gridDataStyle } = useVirtualScrollComposition;

    const { renderDataRow } = getHierarchyRow(props as DataViewOptions, context, columnContext, visibleDatas, useDataViewComposition, useEditComposition, useHierarchyComposition, useRowComposition, useSelectionComposition, useSelectHierarchyItemComposition, useVirtualScrollComposition, useVisualDataComposition, useVisualDataBoundComposition);

    /** 渲染树状视图的数据区域 */
    function renderTreeData() {
        const cellPositionMap = calculateCellPositionInRow(columnContext.value.primaryColumns);
        return visibleDatas.value
            .filter((visualData: VisualData) => visualData.visible !== false)
            .map((visualDataRow: VisualData) => {
                return renderDataRow(visualDataRow, cellPositionMap, 'primary');
            });
    }

    /** 渲染树状视图的整个区域，包括数据区域 */
    function renderTreeArea() {
        return (
            // <div class="fv-tree-data" style={gridDataStyle.value}>
            <div ref={primaryGridContentRef} class="fv-grid-content-primary">
                <div class="fv-grid-data" style={gridDataStyle.value}>
                    {renderTreeData()}
                </div>
            </div>
            // </div>
        );
    }

    return { renderTreeArea };
}
