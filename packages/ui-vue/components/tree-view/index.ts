 
import FTreeView from './src/tree-view.component';
import FTreeViewDesign from './src/designer/tree-view.design.component';

import { propsResolver } from './src/tree-view.props';
import { withInstall } from '@farris/ui-vue/components/common';

export * from './src/tree-view.props';

FTreeView.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['tree-view'] = FTreeView;
    propsResolverMap['tree-view'] = propsResolver;
};
FTreeView.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['tree-view'] = FTreeViewDesign;
    propsResolverMap['tree-view'] = propsResolver;
};

export { FTreeView };
export default withInstall(FTreeView);
