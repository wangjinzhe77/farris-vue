import { SetupContext, computed, defineComponent, inject, ref } from 'vue';
import { SplitterPanePropsType, splitterPaneProps } from './splitter-pane.props';
import { SplitterContext } from '../composition/types';
import { useResizePane } from '../composition/use-resize-pane';
import { getCustomClass, getCustomStyle } from '../../../common';

export default defineComponent({
    name: 'FSplitterPane',
    props: splitterPaneProps,
    emits: [],
    setup(props: SplitterPanePropsType, context: SetupContext) {
        const splitterPaneElementRef = ref<HTMLElement>();
        const splitterContext = inject<SplitterContext>('splitter');
        const { useResizeHandleComposition } = splitterContext as SplitterContext;
        const useResizePaneComposition = useResizePane(props, useResizeHandleComposition);
        const { actualHeight, actualWidth, onClickSplitterHorizontalResizeBar, onClickSplitterVerticalResizeBar } =
            useResizePaneComposition;
        const position = ref(props.position);

        const splitterPaneClass = computed(() => {
            const classObject = {
                'f-splitter-pane': true,
                'f-splitter-pane-main': position.value === 'center',
                'has-resize-bar': props.resizable
            } as Record<string, any>;
            classObject['f-splitter-pane-' + position.value] = true;
            return getCustomClass(classObject, props?.customClass);
        });

        const resizeHandle = computed(() => {
            if (props.resizeHandle) {
                return props.resizeHandle.split(',').filter(handleItem => ['e', 'n', 's', 'w'].find(item => item === handleItem));
            }
            return [];
        });

        const splitterPaneStyle = computed(() => {
            const styleObject = {} as Record<string, any>;
            if ((actualWidth.value && position.value === 'left') || position.value === 'right' || resizeHandle.value.find(item => item === 'e') || resizeHandle.value.find(item => item === 'w')) {
                styleObject.width = `${actualWidth.value}px`;
            }
            if ((actualHeight.value && position.value === 'bottom') || position.value === 'top' || resizeHandle.value.find(item => item === 's') || resizeHandle.value.find(item => item === 'n')) {
                styleObject.height = `${actualHeight.value}px`;
            }
            return getCustomStyle(styleObject, props?.customStyle);
        });


        const splitterResizeBarClass = computed(() => {
            const classObejct = {
                'f-splitter-resize-bar': true,
                'f-splitter-resize-bar-e': position.value === 'left',
                'f-splitter-resize-bar-n': position.value === 'bottom',
                'f-splitter-resize-bar-s': position.value === 'top',
                'f-splitter-resize-bar-w': position.value === 'right'
            } as Record<string, boolean>;
            return classObejct;
        });

        function transferPosition(direction: string) {
            const transfer = {
                e: 'left',
                w: 'right',
                s: 'top',
                n: 'bottom'
            };
            return transfer[direction] || direction;
        }

        function onClickResizeBar(payload: MouseEvent, direction: string) {
            direction = transferPosition(direction);
            if (['left', 'right'].find(item => item === direction)) {
                onClickSplitterHorizontalResizeBar(payload, direction, splitterPaneElementRef.value as HTMLElement);
            }
            if (['top', 'bottom'].find(item => item === direction)) {
                onClickSplitterVerticalResizeBar(payload, direction, splitterPaneElementRef.value as HTMLElement);
            }
        }

        function renderResizeBar() {
            if (resizeHandle.value.length > 0) {
                return resizeHandle.value.map(handle => {
                    return <span
                        class={'f-splitter-resize-bar f-splitter-resize-bar-' + handle}
                        onMousedown={(payload: MouseEvent) => onClickResizeBar(payload, handle)}></span>;
                });
            }
            return <span
                class={splitterResizeBarClass.value}
                onMousedown={(payload: MouseEvent) => onClickResizeBar(payload, position.value)}></span>;
        }

        return () => {
            return (
                <div ref={splitterPaneElementRef} class={splitterPaneClass.value} style={splitterPaneStyle.value}>
                    {context.slots.default && context.slots.default()}
                    {props.resizable && renderResizeBar()}
                </div>
            );
        };
    }
});
