import { ExtractPropTypes, PropType } from "vue";
import { createPropsResolver } from "../../../dynamic-resolver";
import { splitterPaneSchemaMapper } from '../schema/splitter-pane-schema-mapper';
import splitterPaneSchema from '../schema/splitter-pane.schema.json';
import splitterPanePropertyConfig from '../property-config/splitter-pane.property-config.json';

export type SplitterPanePosition = 'left' | 'center' | 'right' | 'top' | 'bottom';
export const splitterPaneProps = {
    customClass: { type: String, defaut: '' },
    customStyle: { type: String, default: '' },
    width: { type: Number },
    /** 记录原始定义高度 */
    height: { type: Number },
    /** 面板位置 */
    position: { type: String as PropType<SplitterPanePosition>, default: 'left' },
    /** 是否显示 */
    visible: { type: Boolean, default: true },
    /** True to allow the pane can be resized. */
    resizable: { type: Boolean, default: false },
    /** 指定拖拽的方向，可以指定多个方向，e向东,n向北,s向南,w向西 
     * 因为位置在右侧，不一定就是w方向可拖拽的
    */
    resizeHandle:{type:String,default:''},
    /** True to allow the pane can be collapsed. */
    collapsable: { type: Boolean, default: false },
    /** 面板最小宽度 */
    minWidth: { type: Number, default: 0},
    /** 面板最小高度 */
    minHeight: { type: Number, default: 0 }
} as Record<string, any>;

export type SplitterPanePropsType = ExtractPropTypes<typeof splitterPaneProps>;

export const splitterPanePropsResolver = createPropsResolver<SplitterPanePropsType>(
    splitterPaneProps, splitterPaneSchema, splitterPaneSchemaMapper, undefined, splitterPanePropertyConfig);
