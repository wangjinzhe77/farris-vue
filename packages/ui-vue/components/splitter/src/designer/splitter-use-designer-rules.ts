import { DesignerHostService, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { DesignerItemContext } from "../../../designer-canvas/src/types";
import { UseTemplateDragAndDropRules } from "../../../designer-canvas/src/composition/rule/use-template-rule";

export function useDesignerRules(designItemContext: DesignerItemContext, designerHostService?: DesignerHostService): UseDesignerRules {
    const dragAndDropRules = new UseTemplateDragAndDropRules();
    const { canMove, canDelete } = dragAndDropRules.getTemplateRule(designItemContext, designerHostService);

    function canAccepts(): boolean {
        return false;
    }
    function checkCanDeleteComponent() {
        return canDelete;
    }

    function checkCanMoveComponent() {
        return canMove;
    }

    function hideNestedPaddingInDesginerView() {
        return true;
    }

    return { canAccepts, checkCanDeleteComponent, checkCanMoveComponent, hideNestedPaddingInDesginerView };

}
