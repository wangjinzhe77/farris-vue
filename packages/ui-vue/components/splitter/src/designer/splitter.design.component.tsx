import { SetupContext, computed, defineComponent, inject, onMounted, provide, ref } from 'vue';
import { SplitterPropsType, splitterProps } from '../splitter.props';
import '../splitter.css';
import { useResizeHandle } from '../composition/use-resize-handle';
import { SplitterContext } from '../composition/types';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { useDesignerRules } from './splitter-use-designer-rules';
import { getCustomClass, getCustomStyle } from '../../../common';
import { DesignerHostService } from '../../../designer-canvas/src/composition/types';

export default defineComponent({
    name: 'FSplitterDesign',
    props: splitterProps,
    emits: [],
    setup(props: SplitterPropsType, context) {
        const elementRef = ref();
        const designerHostService = inject<DesignerHostService>('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);
        componentInstance.value.canNested = false;
        const useResizeHandleComposition = useResizeHandle(elementRef);
        const { horizontalResizeHandleStyle, verticalResizeHandleStyle, resizeOverlayStyle } = useResizeHandleComposition;
        provide<SplitterContext>('splitter', { useResizeHandleComposition });

        const splitterClass = computed(() => {
            const classObject = {
                'f-splitter': true,
            } as Record<string, any>;
            return getCustomClass(classObject, props?.customClass);
        });
        const splitterStyle = computed(() => {
            const styleObject = {
                'flex-direction': props.direction === 'row' ? 'row' : 'column'
            } as Record<string, any>;
            return getCustomStyle(styleObject, props?.customStyle);
        });

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        return () => {
            return (
                <div class={splitterClass.value} ref={elementRef} style={splitterStyle.value}>
                    {context.slots.default && context.slots.default()}
                    <div class="f-splitter-resize-overlay" style={resizeOverlayStyle.value}></div>
                    <div class="f-splitter-horizontal-resize-proxy" style={horizontalResizeHandleStyle.value}></div>
                    <div class="f-splitter-vertical-resize-proxy" style={verticalResizeHandleStyle.value}></div>
                </div>
            );
        };
    }
});
