import { computed, defineComponent, inject, onMounted, ref } from 'vue';
import { SplitterPanePropsType, splitterPaneProps } from '../components/splitter-pane.props';
import { SplitterContext } from '../composition/types';
import { useResizePane } from '../composition/use-resize-pane';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { useDesignerRules } from './splitter-pane-use-designer-rules';
import { getCustomClass, getCustomStyle } from '../../../common';
import { DesignerHostService } from '../../../designer-canvas/src/composition/types';

export default defineComponent({
    name: 'FSplitterPaneDesign',
    props: splitterPaneProps,
    emits: [],
    setup(props: SplitterPanePropsType, context) {
        const elementRef = ref();
        const designerHostService = inject<DesignerHostService>('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);
        componentInstance.value.styles = 'padding:0;box-shadow:none;';
        componentInstance.value.canNested = false;
        componentInstance.value.canMove = false;
        componentInstance.value.canDelete = false;

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);

        const splitterContext = inject<SplitterContext>('splitter');
        const { useResizeHandleComposition } = splitterContext as SplitterContext;
        const useResizePaneComposition = useResizePane(props, useResizeHandleComposition);
        const { actualHeight, actualWidth } = useResizePaneComposition;
        const position = ref(props.position);

        const splitterPaneClass = computed(() => {
            const classObject = {
                'f-splitter-pane': true,
                'f-splitter-pane-main': position.value === 'center',
                'has-resize-bar': props.resizable,
                'f-component-splitter-pane': true,
                'drag-container': true
            } as Record<string, any>;
            classObject['f-splitter-pane-' + position.value] = true;
            return getCustomClass(classObject, props?.customClass);
        });

        const resizeHandle = computed(() => {
            if (props.resizeHandle) {
                return props.resizeHandle.split(',').filter(handleItem => ['e', 'n', 's', 'w'].find(item => item === handleItem));
            }
            return [];
        });

        const splitterPaneStyle = computed(() => {
            const styleObject = {} as Record<string, any>;
            if ((actualWidth.value && position.value === 'left') || position.value === 'right' || resizeHandle.value.find(item => item === 'e') || resizeHandle.value.find(item => item === 'w')) {
                styleObject.width = `${actualWidth.value}px`;
            }
            if ((actualHeight.value && position.value === 'bottom') || position.value === 'top' || resizeHandle.value.find(item => item === 's') || resizeHandle.value.find(item => item === 'n')) {
                styleObject.height = `${actualHeight.value}px`;
            }
            return getCustomStyle(styleObject, props?.customStyle);
        });

        const splitterResizeBarClass = computed(() => {
            const classObejct = {
                'no-drag': true,
                'f-splitter-resize-bar': true,
                'f-splitter-resize-bar-e': position.value === 'left',
                'f-splitter-resize-bar-n': position.value === 'bottom',
                'f-splitter-resize-bar-s': position.value === 'top',
                'f-splitter-resize-bar-w': position.value === 'right'
            } as Record<string, boolean>;
            return classObejct;
        });

        function renderResizeBar() {
            if (resizeHandle.value.length > 0) {
                return resizeHandle.value.map(handle => {
                    return <span
                        class={'f-splitter-resize-bar f-splitter-resize-bar-' + handle}></span>;
                });
            }
            return <span
                class={splitterResizeBarClass.value}></span>;
        }

        return () => {
            return (
                <div
                    ref={elementRef}
                    data-dragref={`${designItemContext.schema.id}-container`}
                    class={splitterPaneClass.value}
                    style={splitterPaneStyle.value}>
                    {context.slots.default && context.slots.default()}
                    {props.resizable && renderResizeBar()}
                </div>
            );
        };
    }
});
