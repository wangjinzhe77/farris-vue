import { Ref } from "vue";

export interface UseResizePane {
    actualHeight: Ref<number>;

    actualWidth: Ref<number>;

    onClickSplitterHorizontalResizeBar: ($event: MouseEvent, position: string, element: HTMLElement) => any;

    onClickSplitterVerticalResizeBar: ($event: MouseEvent, position: string, element: HTMLElement) => any;
}

export interface UseResizeHandle {
    horizontalResizeBarPosition: Ref<number>;

    horizontalResizeHandleOffset: Ref<number>;

    showHorizontalResizeHandle: Ref<boolean>;

    showVerticalResizeHandle: Ref<boolean>;

    verticalResizeBarPosition: Ref<number>;

    verticalResizeHandleOffset: Ref<number>;

    horizontalResizeHandleStyle: Ref<Record<string, any>>;

    verticalResizeHandleStyle: Ref<Record<string, any>>;

    resizeOverlayStyle: Ref<Record<string, any>>;

    draggingHorizontalResizeHandle: ($event: MouseEvent) => void;

    draggingVerticalResizeHandle: ($event: MouseEvent) => void;

}

export interface SplitterContext {
    useResizeHandleComposition: UseResizeHandle;
}
