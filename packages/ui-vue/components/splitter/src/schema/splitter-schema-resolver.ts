 
import { ComponentSchema, DesignerComponentInstance } from "../../../designer-canvas/src/types";
import { DynamicResolver } from "../../../dynamic-resolver";

function wrapContentContainerForDataGrid(resolver: DynamicResolver, schema: ComponentSchema, context: Record<string, any>): ComponentSchema {
    const contentContainerSchema = resolver.getSchemaByType('content-container') as ComponentSchema;
    contentContainerSchema.id = `${schema.id}-layout`;
    contentContainerSchema.appearance = { class: 'f-grid-is-sub f-utils-flex-column' };
    contentContainerSchema.contents = [schema];
    return contentContainerSchema;
}

function addLeftPane(resolver: DynamicResolver, schema: ComponentSchema, context: Record<string, any>): ComponentSchema {
    const leftPaneSchema = resolver.getSchemaByType('splitter-pane') as ComponentSchema;
    leftPaneSchema.appearance = {
        class: "f-col-w6 f-page-content-nav"
    };
    leftPaneSchema.position = 'left';
    (schema.contents as ComponentSchema[]).unshift(leftPaneSchema);
    return schema;

}

function addMainPane(resolver: DynamicResolver, schema: ComponentSchema, context: Record<string, any>): ComponentSchema {
    const mainPaneSchema = resolver.getSchemaByType('splitter-pane') as ComponentSchema;
    mainPaneSchema.appearance = {
        class: "f-page-content-main"
    };
    mainPaneSchema.position = 'main';
    (schema.contents as ComponentSchema[]).push(mainPaneSchema);
    return schema;
}

export function schemaResolver(resolver: DynamicResolver, schema: Record<string, any>, context: Record<string, any>): Record<string, any> {
    const parentComponentInstance = context.parentComponentInstance as DesignerComponentInstance;
    const parentClass = String(parentComponentInstance.schema.appearance.class || '').split(' ');
    const droppedOnMainContainer = parentClass.includes('f-page-main');
    if (droppedOnMainContainer) {
        schema.appearance = { class: 'f-page-content' };
    }
    addLeftPane(resolver, schema as ComponentSchema, context);
    addMainPane(resolver, schema as ComponentSchema, context);
    return schema;
}
