import { SetupContext, computed, defineComponent, provide, ref } from 'vue';
import { SplitterPropsType, splitterProps } from './splitter.props';
import { useResizeHandle } from './composition/use-resize-handle';
import { SplitterContext } from './composition/types';
import { getCustomClass, getCustomStyle } from '../../common';

export default defineComponent({
    name: 'FSplitter',
    props: splitterProps,
    emits: [],
    setup(props: SplitterPropsType, context) {
        const splitterElementRef = ref<HTMLElement>();
        const useResizeHandleComposition = useResizeHandle(splitterElementRef);
        const { horizontalResizeHandleStyle, verticalResizeHandleStyle, resizeOverlayStyle } = useResizeHandleComposition;
        provide<SplitterContext>('splitter', { useResizeHandleComposition });

        const splitterClass = computed(() => {
            const classObject = {
                'f-splitter': true,
            } as Record<string, any>;
           return getCustomClass(classObject,props?.customClass);
        });
        const splitterStyle = computed(() => {
            const styleObject = {
                'flex-direction': props.direction === 'row' ? 'row' : 'column'
            } as Record<string, any>;
            return getCustomStyle(styleObject,props?.customStyle);
        });

        return () => {
            return (
                <div class={splitterClass.value} ref={splitterElementRef} style={splitterStyle.value}>
                    {context.slots.default && context.slots.default()}
                    <div class="f-splitter-resize-overlay" style={resizeOverlayStyle.value}></div>
                    <div class="f-splitter-horizontal-resize-proxy" style={horizontalResizeHandleStyle.value}></div>
                    <div class="f-splitter-vertical-resize-proxy" style={verticalResizeHandleStyle.value}></div>
                </div>
            );
        };
    }
});
