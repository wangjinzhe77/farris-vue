 
import { ExtractPropTypes, PropType } from "vue";
import { createPropsResolver } from "../../dynamic-resolver";
import { splitterSchemaMapper } from "./schema/splitter-schema-mapper";
import { schemaResolver } from './schema/splitter-schema-resolver';
import splitterSchema from './schema/splitter.schema.json';
import splitterPropertyConfig from './property-config/splitter.property-config.json';

export type SpliteDirection = 'column' | 'row';

export const splitterProps = {
    customStyle: { type: String, default: '' },
    customClass: { type: String, default: '' },
    direction: { Type: String as PropType<SpliteDirection>, default: 'row' }
} as Record<string, any>;

export type SplitterPropsType = ExtractPropTypes<typeof splitterProps>;

export const splitterPropsResolver = createPropsResolver<SplitterPropsType>(splitterProps, splitterSchema, splitterSchemaMapper, schemaResolver, splitterPropertyConfig);
