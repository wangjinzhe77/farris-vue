 
import type { App, Plugin } from 'vue';
import FSplitter from './src/splitter.component';
import FSplitterPane from './src/components/splitter-pane.component';
import FSplitterDesign from './src/designer/splitter.design.component';
import FSplitterPaneDesign from './src/designer/splitter-pane.design.component';
import { splitterPropsResolver } from './src/splitter.props';
import { splitterPanePropsResolver } from './src/components/splitter-pane.props';

export * from './src/splitter.props';

FSplitter.install = (app: App) => {
    app.component(FSplitter.name as string, FSplitter);
    app.component(FSplitterPane.name as string, FSplitterPane);
};
FSplitter.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap.splitter = FSplitter;
    propsResolverMap.splitter = splitterPropsResolver;
    componentMap['splitter-pane'] = FSplitterPane;
    propsResolverMap['splitter-pane'] = splitterPanePropsResolver;
};
FSplitter.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap.splitter = FSplitterDesign;
    propsResolverMap.splitter = splitterPropsResolver;
    componentMap['splitter-pane'] = FSplitterPaneDesign;
    propsResolverMap['splitter-pane'] = splitterPanePropsResolver;
};

export { FSplitter, FSplitterPane };
export default FSplitter as typeof FSplitter & Plugin;
