import { FARRIS_LOCALES } from './locales';

// export const FARRIS_LOCAL_CUSTOM_DATA = new InjectionToken('自定义语言数据， 格式：{ "languageCode": { "name": { "key": "value" } } }');

export class LocaleService {
    static langData: any;
    static localeId: string;  // Add this property

    constructor(public localeId: string, private localeData: any) {

        if (!localeId) {
            localeId = 'zh-CHS';
        }

        /** 合并语言资源 */
        LocaleService.setLocaleData(localeData, localeId);
    }

    /** 获取语言资源
     *  path : 资源路径，如:
     *  getResources('zh-CHS') // 返回所有中文语言资源
     *  getResources('zh-CHS.lookup') // 返回所有中文下帮助的资源
     */
    static getResources(path = '') {
        if (path) {
            return LocaleService.getValue(path, FARRIS_LOCALES);
        }
        return FARRIS_LOCALES;
    }

    static getComponentOptions(ctrlName: any) {
        return LocaleService.langData[ctrlName];
    }

    /** 合并现有的多语资源 */
    static setLocaleData(localeData = null, localeId: any) {
        if (localeData) {
            // 解析得到提供的语言编码 
            const langCodes = Object.keys(localeData);
            langCodes.forEach((code: string) => {
                const resName = Object.keys(localeData[code]);
                resName.forEach(k => {
                    LocaleService.appendLanguageResource(k, localeData[code][k], code);
                });
            });
        }

        LocaleService.langData = (FARRIS_LOCALES as Record<string, Record<string, any>>)[localeId];
        if (!LocaleService.langData) {
            LocaleService.langData = FARRIS_LOCALES['zh-CHS'];
        }
    }

    static appendLanguageResource(key: string, data: any, lang: string = LocaleService.localeId) {
        if (!lang) {
            LocaleService.langData[key] = LocaleService.langData[key] || {};
            LocaleService.langData[key] = Object.assign(LocaleService.langData[key], data || {});
        } else {
            (FARRIS_LOCALES as Record<string, Record<string, any>>)[lang][key] = data || {};
        }
    }
    static getLocaleValue(propertyName: string) {
        const val = LocaleService.getValue(propertyName, LocaleService.langData);
        if (val) {
            return val;
        } else {
            return '';
        }
    }
    /**
    * 获取对象中指定字段的值。 field: 可以为带有层级结构的路径，如： user.firstName | name 等
    * data: 获取字段的数据源，一般为JSON对象
    * safe: 为true, 将html字符进行转码输出，默认为 false
    */
    static getValue(field: string, data: any, safe = false) {
        if (!data) {
            return '';
        }
        let resultVal = '';
        if (field.indexOf('.') === -1 && data.hasOwnProperty(field)) {
            resultVal = data[field];
        } else {
            resultVal = field.split('.').reduce((obj, key) => {
                if (obj) {
                    return obj[key];
                } else {
                    return null;
                }
            }, data);
        }

        if (safe) {
            return LocaleService.formatterValue(resultVal);
        } else {
            return resultVal;
        }
    }
    static formatterValue(val: any) {
        if (val === null || val === undefined || val === '') {
            return '';
        }

        if (typeof val === 'string') {
            return LocaleService.escapeHtml(val);
        }

        return val;
    }

    static escapeHtml(str: any) {
        if (str === null || str === undefined) {
            return '';
        }
        return str
            .replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/\"/g, '&quot;')
            .replace(/\'/g, '&#39;')
            .replace(/\//g, '&#x2F;');
    }
}
