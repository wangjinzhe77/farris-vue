
import { EN_US } from './en-us/index';
import { ZH_CN } from './zh-cn/index';
import { ZH_CHT } from './zh-CHT/index';

export const FARRIS_LOCALES = {
    'zh-CHS': ZH_CN,
    en: EN_US,
    'zh-CHT': ZH_CHT
};
