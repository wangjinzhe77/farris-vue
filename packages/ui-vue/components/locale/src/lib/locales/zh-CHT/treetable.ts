export const TREETABLE_LOCALE_ZHCHT = {
    emptyMessage: '暫無數據',
    pagination: {
        previousLabel: '上一頁',
        nextLabel: '下一頁',
        message: '每頁 {0} 條記錄，共 {1} 條記錄。'
    }
};
