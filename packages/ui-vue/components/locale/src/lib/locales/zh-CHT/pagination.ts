export const PAGINATION_LOCALE_ZHCHT = {
    message: '共 <b>{1}</b> 條 ',
    totalinfo: {
        firstText: '共',
        lastText: '條'
    },
    pagelist: {
        firstText: '每頁',
        lastText: '條'
    },
    previous: '上一頁',
    next: '下一頁',
    goto: {
        prefix: '跳至',
        suffix: '頁'
    }
};
