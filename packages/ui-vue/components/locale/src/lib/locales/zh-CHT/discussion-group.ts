export const DISCUSSION_GROUP_LOCALE_ZHCHT = {
    submit: '提交',
    cancel: '取消',
    colleague: '同事',
    all: '所有人員可見',
    related: '僅相關人員可見',
    confirm: '確定',
    reply: '回複',
    emptyMessage: '暫無數據',
    placeholder: '請輸入姓名搜索',
    notEmpty: '提交內容不能為空',
    selectEmployee: '選擇員工',
    next: '下級',
    emptySelected: '清空已選',
    emptyRight: '請在左側選擇人員',
    allOrg: '全部組織',
    selected: '已選',
    section: '部門',
    people: '人員',
    viewMore: '查看更多',
    per: '人',
    pcs: '個',
    emptyList: '聯係人為空',
    advancedQuery: '高級查詢'
};
