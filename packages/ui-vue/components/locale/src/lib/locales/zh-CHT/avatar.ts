export const AVATAR_LOCALE_ZHCHT = {
    imgtitle: '點擊修改',
    typeError: '上傳圖片類型不正確',
    sizeError: '上傳圖片不能大於',
    uploadError: '圖片上傳失敗，請重試!',
    loadError: '加載錯誤'
};
