export const MULTI_SELECT_LOCALE_ZHCHT = {
    leftTitle: '未選擇',
    rightTitle: '已選擇',
    noDataMoveMessage: '請選擇要移動的數據。',
    shiftRight: '右移',
    shiftLeft: '左移',
    allShiftRight: '全部右移',
    allShiftLeft: '全部左移',
    top: '置頂',
    bottom: '置底',
    shiftUp: '上移',
    shiftDown: '下移',
    emptyData: '暫無數據',
    filterPlaceholder: '輸入篩選項名稱搜索'
};
