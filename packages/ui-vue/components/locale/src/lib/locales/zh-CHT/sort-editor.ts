export const SORT_EDITOR_LOCALE_ZHCHT = {
    // 取消
    cancel: '取消',
    // 確定
    ok: '確定',
    // 添加子句
    add: '添加',
    clear: '清空',
    // 置頂
    moveTop: '置頂',
    // 上移
    moveUp: '上移',
    // 下移
    moveDown: '下移',
    // 置底
    moveBottom: ' 置底',

    // 字段
    field: '字段',
    // 排序
    order: '排序',
    asc: '升序',
    desc: '降序',
    title: '排序設置'
};
