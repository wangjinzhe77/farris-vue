export const FILTER_EDITOR_LOCALE_ZHCHT = {
    // 取消
    cancelButton: '取消',
    // 確定
    okButton: '確定',
    // 添加子句
    addWhere: '添加子句',
    clear: '清空',
    // 置頂
    moveTop: '置頂',
    // 上移
    moveUp: '上移',
    // 下移
    moveDown: '下移',
    // 置底
    moveBottom: '置底',
    // 左括號
    leftBrackets: '左括號',
    // 字段
    field: '字段',
    // 操作符
    operator: '操作符',
    // 值
    value: '值',
    // 值類型
    valueType: '值類型',
    expressType: {
        value: '值',
        express: '錶達式',
        frontExpress: '表單表達式'
    },
    // 右括號
    rightBrackets: '右括號',
    // 關係
    relation: '關係',
    relationValue: {
        and: '並且',
        or: '或者'
    },
    designTab: '設計器',
    jsonTab: '源代碼',
    sqlTab: 'Sql預覽',
    title: '條件編輯器',
    message: '確認要清空當前所有數據嗎？',
    validate: {
        bracket: '左右括號不匹配，請檢查',
        relation: '條件關系不完整，請檢查',
        field: '條件字段未設置，請檢查'
    }
};

export const ENUM_EDITOR_LOCALE_ZHCHT = {
    // 取消
    cancelButton: '取消',
    // 確定
    okButton: '確定',
    // 添加子句
    addWhere: '添加',
    clear: '清空',
    // 置頂
    moveTop: '置頂',
    // 上移
    moveUp: '上移',
    // 下移
    moveDown: '下移',
    // 置底
    moveBottom: '置底',

    title: '枚舉數據編輯器',
    message: '確認要清空當前所有數據嗎？',
    value: '值',
    name: '名稱'
};

