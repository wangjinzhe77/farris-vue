export const LOOKUP_LOCALE_ZHCHT = {
    placeholder: '請選擇',
    favorites: '收藏夾',
    selected: '已選數據',
    okText: '確定',
    cancelText: '取消',
    allColumns: '所有列',
    datalist: '數據列錶',
    mustWriteSomething: '請輸入關鍵字後查詢。',
    mustChoosAdatarow: '請選擇一條記錄！',
    tipText: '您要找的是不是這些？',
    cascade: {
        enable: '同步選擇',
        disable: '僅選擇自身',
        up: '包含上級',
        down: '包含下級'
    },
    favoriteInfo: {
        addFav: '收藏成功。',
        cancelFav: '取消收藏成功。 '
    },
    getAllChilds: '獲取所有子級數據',
    contextMenu: {
        expandall: '全部展開',
        collapseall: '全部收起',
        expandByLayer: '按層級展開',
        expand1: '展開 1 級',
        expand2: '展開 2 級',
        expand3: '展開 3 級',
        expand4: '展開 4 級',
        expand5: '展開 5 級',
        expand6: '展開 6 級',
        expand7: '展開 7 級',
        expand8: '展開 8 級',
        expand9: '展開 9 級'
    },
    quick: {
        notfind: '未找到搜索內容',
        more: '顯示更多'
    },
    configError: '幫助顯示列未配置，請檢查是否已正確配置幫助數據源!',
    selectedInfo: {
        total: '已選 <span class="selected-counter mx-1">{0}</span> 條',
        clear: '取消已選',
        remove: '刪除已選({0})',
        confirm: '您確認要取消所有已選中的記錄嗎?'
    }
};
