export const QUERY_CONDITION_LOCALE_ZHCHS = {
    configDialog: {
        unSelectedOptions: '未选择项',
        selectedOptions: '已选择项',
        confirm: '确定',
        cancel: '取消',
        placeholder: '请输入搜索关键字',
        moveUp: '上移',
        moveAllUp: '全部上移',
        moveDown: '下移',
        moveAllDown: '全部下移',
        moveRight: '右移',
        moveAllRight: '全部右移',
        moveLeft: '左移',
        moveAllLeft: '全部左移',
        pleaseSelect: '请选择字段',
        noOptionMove: '没有可移动字段',
        selectOptionUp: '请选择上移字段',
        cannotMoveUp: '无法上移',
        selectOptionTop: '请选择置顶字段',
        optionIsTop: '字段已置顶',
        selectOptionDown: '请选择下移字段',
        cannotMoveDown: '无法下移',
        selectOptionBottom: '请选择置底字段',
        optionIsBottom: '字段已置底'
    },
    container: {
        query: '筛选',
        saveAs: '另存为',
        save: '保存',
        config: '配置'
    }
};
