export const FILTER_EDITOR_LOCALE_ZHCHS = {
    // 取消
    cancelButton: '取消',
    // 确定
    okButton: '确定',
    // 添加子句
    addWhere: '添加子句',
    clear: '清空',
    // 置顶
    moveTop: '置顶',
    // 上移
    moveUp: '上移',
    // 下移
    moveDown: '下移',
    // 置底
    moveBottom: '置底',
    // 左括号
    leftBrackets: '左括号',
    // 字段
    field: '字段',
    // 操作符
    operator: '操作符',
    // 值
    value: '值',
    // 值类型
    valueType: '值类型',
    expressType: {
        value: '值',
        express: '表达式',
        frontExpress: '表单表达式'
    },
    // 右括号
    rightBrackets: '右括号',
    // 关系
    relation: '关系',
    relationValue: {
        and: '并且',
        or: '或者'
    },
    designTab: '设计器',
    jsonTab: '源代码',
    sqlTab: 'Sql预览',
    title: '条件编辑器',
    message: '确认要清空当前所有数据吗？',
    validate: {
        bracket: '左右括号不匹配，请检查',
        relation: '条件关系不完整，请检查',
        field: '条件字段未设置，请检查'
    }
};

export const ENUM_EDITOR_LOCALE_ZHCHS = {
    // 取消
    cancelButton: '取消',
    // 确定
    okButton: '确定',
    // 添加子句
    addWhere: '添加',
    clear: '清空',
    // 置顶
    moveTop: '置顶',
    // 上移
    moveUp: '上移',
    // 下移
    moveDown: '下移',
    // 置底
    moveBottom: '置底',

    title: '枚举数据编辑器',
    message: '确认要清空当前所有数据吗？',
    value: '值',
    name: '名称'
};

