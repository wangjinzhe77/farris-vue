export const LANGUAGE_LABEL_LOCALE_ZHCHS = {
    en: '英语',
    "zh-cn": '简体中文',
    "zh-CHS": '简体中文',
    "zh-CHT": '繁体中文',
    ok: '确定',
    cancel: '取消'
};
