import { LISTVIEW_LOCALE_ZHCHS } from './list-view';
import { LOOKUP_LOCALE_ZHCHS } from './lookup';
import { DATAGRID_LOCALE_ZHCHS } from './datagrid';
import { SECTION_LOCALE_ZHCHS } from './section';
import { LOADING_LOCALE_ZHCHS } from './loading';
import { FILTER_EDITOR_LOCALE_ZHCHS, ENUM_EDITOR_LOCALE_ZHCHS } from './filter-editor';
import { MESSAGER_LOCALE_ZHCHS } from './messager';
import { NOTIFY_LOCALE_ZHCHS } from './notify';
import { PAGINATION_LOCALE_ZHCHS } from './pagination';
import { SORT_EDITOR_LOCALE_ZHCHS } from './sort-editor';
import { TEXT_LOCALE_ZHCHS } from './text';
import { SIDEBAR_LOCALE_ZHCHS } from './sidebar';
import { TABS_LOCALE_ZHCHS } from './tabs';
import { RESPONSE_TOOLBAR_LOCALE_ZHCHS } from './response-toolbar';
import { MULTI_SELECT_LOCALE_ZHCHS } from './multi-select';
import { QUERY_CONDITION_LOCALE_ZHCHS } from './query-condition';
import { QUERY_SOLUTION_LOCALE_ZHCHS } from './query-solution';
import { COLLAPSE_DIRECTIVE_LOCALE_ZHCHS } from './collapse';
import { TREETABLE_LOCALE_ZHCHS } from './treetable';
import { AVATAR_LOCALE_ZHCHS } from './avatar';
import { LIST_FILTER_LOCALE_ZHCHS } from './list-filter';
import { PROGRESS_STEP_LOCALE_ZHCHS } from './progress-step';
import { LANGUAGE_LABEL_LOCALE_ZHCHS } from './language-label';
import { VERIFY_DETAIL_ZHCHS } from './verify-detail';
import { BATCH_EDIT_DIALOG_LOCALE_ZHCHS } from './batch-edit-dialog';
import { PAGE_WALKER_ZHCHS } from './page-walker';
import { FOOTER_LOCALE_ZHCHS } from './footer';
import { DISCUSSION_GROUP_LOCALE_ZHCHS } from './discussion-group';
import { TAG_LOCALE_ZHCHS } from './tag';
import { NUMERIC_LOCALE_ZHCHS } from './numeric';
import { FILTER_PANEL_LOCALE_ZHCHS } from './filter-panel';
import { SCROLLSPY_LOCALE_ZHCHS } from './scrollspy';
import { LOOKUP_CONFIG_LOCALE_ZHCHS } from './lookup-config';
import { COMBO_LOCALE } from './combo';

export const ZH_CN = {
    locale: 'ZH_CN',
    combo: COMBO_LOCALE,
    combolist: {},
    datagrid: DATAGRID_LOCALE_ZHCHS,
    filterEditor: FILTER_EDITOR_LOCALE_ZHCHS,
    enumEditor: ENUM_EDITOR_LOCALE_ZHCHS,
    lookup: LOOKUP_LOCALE_ZHCHS,
    loading: LOADING_LOCALE_ZHCHS,
    modal: {},
    messager: MESSAGER_LOCALE_ZHCHS,
    notify: NOTIFY_LOCALE_ZHCHS,
    dialog: {},
    datatable: {},
    colorPicker: {},
    numberSpinner: NUMERIC_LOCALE_ZHCHS,
    inputGroup: {},
    sortEditor: SORT_EDITOR_LOCALE_ZHCHS,
    treetable: TREETABLE_LOCALE_ZHCHS,
    multiSelect: MULTI_SELECT_LOCALE_ZHCHS,
    tabs: TABS_LOCALE_ZHCHS,
    timePicker: {},
    wizard: {},
    tree: {},
    tooltip: {},
    listview: LISTVIEW_LOCALE_ZHCHS,
    text: TEXT_LOCALE_ZHCHS,
    switch: {},
    sidebar: SIDEBAR_LOCALE_ZHCHS,
    section: SECTION_LOCALE_ZHCHS,
    pagination: PAGINATION_LOCALE_ZHCHS,
    responseToolbar: RESPONSE_TOOLBAR_LOCALE_ZHCHS,
    queryCondition: QUERY_CONDITION_LOCALE_ZHCHS,
    querySolution: QUERY_SOLUTION_LOCALE_ZHCHS,
    collapseDirective: COLLAPSE_DIRECTIVE_LOCALE_ZHCHS,
    avatar: AVATAR_LOCALE_ZHCHS,
    listFilter: LIST_FILTER_LOCALE_ZHCHS,
    progressStep: PROGRESS_STEP_LOCALE_ZHCHS,
    languageLabel: LANGUAGE_LABEL_LOCALE_ZHCHS,
    verifyDetail: VERIFY_DETAIL_ZHCHS,
    batchEditDialog: BATCH_EDIT_DIALOG_LOCALE_ZHCHS,
    pageWalker: PAGE_WALKER_ZHCHS,
    footer: FOOTER_LOCALE_ZHCHS,
    discussionGroup: DISCUSSION_GROUP_LOCALE_ZHCHS,
    tag: TAG_LOCALE_ZHCHS,
    filterPanel: FILTER_PANEL_LOCALE_ZHCHS,
    scrollspy: SCROLLSPY_LOCALE_ZHCHS,
    lookupConfig: LOOKUP_CONFIG_LOCALE_ZHCHS
};
