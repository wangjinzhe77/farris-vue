export const NUMERIC_LOCALE_ZHCHS = {
    placeholder: '请输入数字',
    range: {
        begin: '请输入开始数字',
        end: '请输入结束数字'
    }
};
