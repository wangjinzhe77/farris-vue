export const MESSAGER_LOCALE_ZHCHS = {
    yes: '是',
    no: '否',
    ok: '确定',
    cancel: '取消',
    title: '系统提示',
    errorTitle: '错误提示',
    prompt: {
        fontSize: {
            name:  '字体大小',
            small: '小',
            middle: '中',
            big: '大',
            large: '特大',
            huge: '超大'
        },
        tips: {
            surplus: '还可以输入 {0} 个字符',
            length: '已输入 {0} 个字符'
        }
    },
    exception: {
        expand: '展开',
        collapse: '收起',
        happend: '发生时间',
        detail: '详细信息',
        copy: '复制详细信息',
        copySuccess: '复制成功',
        copyFailed: '复制失败',
        roger: '知道了'
    }
};
