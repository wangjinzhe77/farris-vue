export const NUMERIC_LOCALE = {
    placeholder: 'Please enter the number',
    range: {
        begin: 'Please enter the begin number',
        end: 'Please enter the end number'
    }
};
