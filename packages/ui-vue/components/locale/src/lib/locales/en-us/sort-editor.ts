export const SORT_EDITOR_LOCALE = {
    // 取消
    cancel: 'cancel',
    // 确定
    ok: 'ok',
    // 添加子句
    add: 'Add',
    clear: 'Clear',
    // 置顶
    moveTop: 'Top',
    // 上移
    moveUp: 'Up',
    // 下移
    moveDown: 'Down',
    // 置底
    moveBottom: 'Bottom',

    // 字段
    field: 'Field Name',
    // 操作符
    order: 'Order',

    asc: 'ASC',
    desc: 'DESC',
    title: 'Sort Editor'
};
