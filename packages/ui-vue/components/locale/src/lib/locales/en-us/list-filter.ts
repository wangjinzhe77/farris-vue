export const LIST_FILTER_LOCALE = {
    filter: 'Filter',
    confirm: 'OK',
    cancel: 'Cancel',
    reset: 'Reset'
};
