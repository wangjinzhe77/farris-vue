export const AVATAR_LOCALE = {
    imgtitle: 'Amend',
    typeError: 'Type error',
    sizeError: 'Can not be larger than',
    uploadError: 'Upload Fail!',
    loadError: 'Load error.'
};