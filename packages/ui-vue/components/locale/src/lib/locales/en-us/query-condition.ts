export const QUERY_CONDITION_LOCALE = {
    configDialog: {
        unSelectedOptions: 'Unselected Options',
        selectedOptions: 'Selected Options',
        confirm: 'Confirm',
        cancel: 'Cancel',
        placeholder: 'Please input search keywords',
        moveUp: 'Move Up',
        moveAllUp: 'Move All Up',
        moveDown: 'Move Down',
        moveAllDown: 'Move All Down',
        moveRight: 'Move Right',
        moveAllRight: 'Move All Right',
        moveLeft: 'Move Left',
        moveAllLeft: 'Move All Left',
        pleaseSelect: 'Please select options',
        noOptionMove: 'No moveable options left',
        selectOptionUp: 'Please select options to move up',
        cannotMoveUp: 'Can\'t move up',
        selectOptionTop: 'Please select options to the top',
        optionIsTop: 'The option is at top',
        selectOptionDown: 'Please select options to move down',
        cannotMoveDown: 'Can\'t move down',
        selectOptionBottom: 'Please select options to the bottom',
        optionIsBottom: 'The option is at bottom'
    },
    container: {
        query: 'Filter',
        saveAs: 'Save as',
        save: 'Save',
        config: 'Configurations'
    }
};
