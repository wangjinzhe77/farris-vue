export const FILTER_PANEL_LOCALE = {
    filter: 'Filter',
    confirm: 'OK',
    cancel: 'Cancel',
    reset: 'Reset',
    advancedFilter: 'advancedFilter',
    expand: 'Expand',
    fold: 'Fold',
    last1Month: 'LastOneMonth',
    last3Month: 'LastThreeMonth',
    last6Month: 'LastSixMonth',
    pleaseInput: 'Please input ',
    searchHistory: 'Search History',
    searchResult: 'Search Result',
    intervalFilter: 'Filter by interval',
    beginPlaceHolder: 'Minimum value',
    endPlaceHolder: 'Maximum value',
    dateBeginPlaceHolder: 'Start date',
    dateEndPlaceHolder: 'End date',
    empty: 'Empty',
    clear: 'Clear',
    today:'Today',
    yesterday: 'Yesterday',
    checkall: 'Check all'
};
