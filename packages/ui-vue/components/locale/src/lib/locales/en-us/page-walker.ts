export const PAGE_WALKER_LOCALE = {
    next: 'Next',
    prev: 'Prev',
    skip: 'Skip',
    startNow:'Start now'
};
