export const LOOKUP_CONFIG_LOCALE = {
    placeholder: 'Select the form metadata',
    code: 'Code',
    name: 'Name',
    select: 'Select the help metadata',
    filter: 'Configuration conditions',
    helpidEmpty: 'HelpID cannot be empty',
    selectTitle: 'Helps with metadata selection',
    lookupTitle: 'Help configuration',
    sure: 'Confirm',
    cancel: 'Cancel',
    successSave: 'Save successfully',
    helpIdError:'Please select the help metadata',
    fileNamePlaceholder:'Select the help text field',
    selectFileNameTitle:'Text field selector',
    bindingPath:'Binding field',
    fieldError:'The bound field does not exist!',
    textFieldLable:'Select the textField',
    loadTypeTitle: 'Select the loadType',
    loadTypeList: {
        all:'All load',
        layer:'Layer load',
        default: 'Default load'
    },
    powerTitle:'Permission setting',
    powerObjLabel:'Permission objects',
    powerFieldLabel:'Permission field',
    powerOperateLabel:'Permission operate',
    linkfieldLabel:'Help associated field',
    powerDataTitle:'Permission object selection',
    powerFieldTitle:'Permission field selection',
    powerOperateTitle:'Operation selection',
    businessLable:'Business object ',
    powerLable:'Permission objects',
    powerError:'Select the permission objects',
    operateError:'Select the Operation',
    linkfieldError:'Select permission field'
};
