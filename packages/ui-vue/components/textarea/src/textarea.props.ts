/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import textareaSchema from './schema/textarea.schema.json';
import { schemaResolver } from './schema/schema-resolver';

export const textAreaProps = {
    /** 是否撑开高度 */
    autoHeight: { type: Boolean, default: false },
    /** 自动聚焦 */
    autofocus: { type: Boolean, default: false },
    /** 自动完成 */
    autocomplete: { type: Boolean, default: false },
    /** 禁用 */
    disabled: { type: Boolean, default: false },
    /** 启用清除按钮 */
    enableClear: { type: Boolean, default: false},
    /** 组件标识 */
    id: { type: String, default: '' },
    /** 最小字符长度 */
    minLength: { type: Number, default: 0 },
    /** 最大字符长度 */
    maxLength: { type: Number },
    /** 组件值 */
    modelValue: { type: String, default: '' },
    /** 背景文字 */
    placeholder: { type: String, default: '' },
    /** 只读 */
    readonly: { type: Boolean, default: false },
    /** 文本区域可见的行数 */
    rows: { type: Number, default: 2},
    /** 展示输入文本数量 */
    showCount: { type: Boolean, default: false },
    /** 展示边线 */
    showBorder: { Type: Boolean, default: true },
    /** 键盘按键索引 */
    tabIndex: { type: Number, default: -1 }
} as Record<string, any>;

export type TextAreaProps = ExtractPropTypes<typeof textAreaProps>;

export const textAreaDesignProps = Object.assign({}, textAreaProps, {
    readonly: {}
});

export type TextAreaDesignProps = ExtractPropTypes<typeof textAreaDesignProps>;

export const propsResolver = createPropsResolver<TextAreaProps>(textAreaProps, textareaSchema, schemaMapper, schemaResolver);

