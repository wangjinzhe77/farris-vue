/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, onMounted, ref, SetupContext } from 'vue';
import { TextAreaProps, textAreaProps } from './textarea.props';
import { TextBoxProps, useClear, useTextBox } from '@farris/ui-vue/components/common';

export default defineComponent({
    name: 'FTextarea',
    props: textAreaProps,
    emits: ['update:modelValue', 'valueChange'] as (string[] & ThisType<void>) | undefined,
    setup(props: TextAreaProps, context: SetupContext) {
        const textAreaRef = ref();
        const modelValue = ref(props.modelValue);
        const displayText = computed(() => props.modelValue);
        const useTextBoxComposition = useTextBox(props as TextBoxProps, context, modelValue, displayText);
        const { inputGroupClass, onBlur, onFocus } = useTextBoxComposition;
        const useClearComposition = useClear(props as TextBoxProps, context, useTextBoxComposition);
        const { onMouseEnter, onMouseLeave, clearButtonClass, shouldShowClearButton, clearButtonStyle } = useClearComposition;
        const total = computed(() => props.maxLength ? props.maxLength : (props.modelValue ? props.modelValue.length : 0));
        const onInput = (e: any) => {
            e.stopPropagation();
            context.emit('update:modelValue', e.target?.value);
            context.emit('valueChange', e.target?.value);
        };

        const onClear = (e: MouseEvent) => {
            e.stopPropagation();
            textAreaRef.value.value = '';
            context.emit('update:modelValue', '');
            context.emit('valueChange', '');
        };

        const textareaClass = computed(() => {
            return {
                'form-control': true,
                'h-100': props.autoHeight
            };
        });

        const textareaStyle = computed(() => {
            if (props.rows > 2) {
                return { height: 'auto' };
            }
            return {};
        });
        const realPlaceholder = computed(() => {
            return props.disabled || props.readonly ? '' :props.placeholder;
        });
        context.expose({ elementRef: textAreaRef });

        onMounted(() => {
        });

        return () => {
            return (
                <div class={inputGroupClass.value} onMouseenter={onMouseEnter} onMouseleave={onMouseLeave} style="position:relative">
                    <textarea
                        class={textareaClass.value}
                        id={props.id}
                        v-model={props.modelValue}
                        ref={textAreaRef}
                        minlength={props.minLength}
                        maxlength={props.maxLength ? props.maxLength : null}
                        tabindex={props.tabIndex}
                        disabled={props.disabled}
                        readonly={props.readonly}
                        autocomplete={props.autocomplete}
                        placeholder={realPlaceholder.value}
                        autofocus={props.autofocus}
                        rows={props.rows}
                        onInput={onInput}
                        onFocus={onFocus}
                        onBlur={onBlur}
                        style={textareaStyle.value}
                    >
                    </textarea>
                    {
                        shouldShowClearButton.value && <span id="clearIcon"
                            class={clearButtonClass.value}
                            style={clearButtonStyle.value}
                            onClick={(e: MouseEvent) => onClear(e)}>
                            <i class="f-icon modal_close"></i>
                        </span>
                    }
                    {props.showCount && <span style="position: absolute;bottom: 5px;right: 10px;z-index:999">
                        {`${props.modelValue ? props.modelValue.length : 0}/${total.value}`}
                    </span>}
                </div>

            );
        };

    }
});

