
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, inject, onMounted, ref, SetupContext, toRefs } from 'vue';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { textAreaDesignProps, TextAreaDesignProps } from '../textarea.props';
import { useTextareaDesignerRules } from './use-design-rules';

export default defineComponent({
    name: 'FTextareaDesign',
    props: textAreaDesignProps,
    emits: [''] as (string[] & ThisType<void>) | undefined,
    setup(props: TextAreaDesignProps, context: SetupContext) {
        const elementRef = ref();
        const designerHostService = inject('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useTextareaDesignerRules(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

        const total = computed(() => props.maxLength ? props.maxLength : props.modelValue.length);

        const containerStyle = computed(() => {
            return {
                position: 'relative'
            };
        });

        const textareaStyle = computed(() => {
            return {
                'box-shadow': '0 0 0 1px #dcdfe6 inset',
                border: 'none',
                padding: '5px 10px',
                width: '100%'
            };
        });

        const spanStyle = {
            position: 'absolute',
            bottom: '5px',
            right: '10px'
        };

        onMounted(() => {
            elementRef.value.componentInstance = componentInstance;
        });
        context.expose(componentInstance.value);

        return () => {
            return (
                <div style={containerStyle.value} ref={elementRef}>
                    <textarea
                        id={props.id}
                        tabindex={props.tabIndex}
                        disabled={false}
                        readonly={true}
                        style={textareaStyle.value}
                        placeholder={props.placeholder}
                        rows={props.rows}
                    >
                    </textarea>
                    {props.showCount && <span style={spanStyle}>
                        {`${props.modelValue.length}/${total.value}`}
                    </span>}
                </div>

            );
        };

    }
});
