 
 
import { ComponentSchema, DesignerComponentInstance, DesignerItemContext } from "../../../designer-canvas/src/types";
import { UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { TextareaProperty } from "../property-config/textarea.property-config";
export function useTextareaDesignerRules(designItemContext: DesignerItemContext, designerHostService): UseDesignerRules {
    const schema = designItemContext.schema as ComponentSchema;

    // 构造属性配置方法
    function getPropsConfig(componentId: string, componentInstance: DesignerComponentInstance) {
        const inputGroupProps = new TextareaProperty(componentId, designerHostService);
        return inputGroupProps.getPropertyConfig(schema, componentInstance);
    }

    return { getPropsConfig } as UseDesignerRules;

}
