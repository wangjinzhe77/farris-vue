import { InputBaseProperty } from "../../../property-panel/src/composition/entity/input-base-property";

export class TextareaProperty extends InputBaseProperty {

    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }
    getEditorProperties(propertyData: any) {
        return this.getComponentConfig(propertyData, { type: "textarea" }, {
            rows: {
                description: "",
                title: "文本区域可见的行数",
                type: "number",
                editor: {
                    min: 0,
                    nullable:true
                }
            },
            showCount: {
                description: "",
                title: "展示输入文本数量",
                type: "boolean"
            }
        });
    }
};

