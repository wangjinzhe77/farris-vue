 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import FTextarea from './src/textarea.component';
import FTextareaDesign from './src/designer/textarea.design.component';
import { propsResolver } from './src/textarea.props';
import { withInstall } from '@farris/ui-vue/components/common';

export * from './src/textarea.props';

FTextarea.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap['textarea'] = FTextarea;
    propsResolverMap['textarea'] = propsResolver;
};
FTextarea.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['textarea'] = FTextareaDesign;
    propsResolverMap['textarea'] = propsResolver;
};

export { FTextarea };
export default withInstall(FTextarea);
