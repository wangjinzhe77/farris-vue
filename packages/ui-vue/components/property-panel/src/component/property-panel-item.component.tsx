/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext, ref, computed, watch, reactive, Teleport } from 'vue';
import { propertyPanelItemProps, PropertyPanelItemProps } from '../composition/props/property-panel-item.props';
import FDynamicFormGroup from '../../../dynamic-form/src/component/dynamic-form-group/dynamic-form-group.component';
import '../composition/class/property-panel-item.css';
import { PropertyChangeObject, PropertyEntity } from '../composition/entity/property-entity';

export default defineComponent({
    name: 'FPropertyPanelItem',
    props: propertyPanelItemProps,
    emits: ['propertyChange', 'triggerRefreshPanel'] as (string[] & ThisType<void>) | undefined,
    setup(props: PropertyPanelItemProps, context: SetupContext) {
        const categoryId = ref(props.category?.categoryId);
        const propertyId = ref(props.elementConfig.propertyID);
        const propertyName = ref(props.elementConfig.propertyName);
        const editor = ref(props.elementConfig.editor);
        const propertyValue = ref(props.elementConfig.propertyValue);
        const propertyItemClass = computed(() => {
            const visibleProperty = (props.elementConfig as PropertyEntity).visible;
            const visiblePropertyValue = typeof visibleProperty == 'boolean' ?
                visibleProperty : visibleProperty === undefined ? true : visibleProperty();
            return {
                'farris-group-wrap': true,
                'property-item': true,
                'd-none': !visiblePropertyValue
            };
        });
        watch(() => props.elementConfig?.propertyValue, (value) => {
            propertyValue.value = value;
        });
        // 修复表格切换列编辑器时，标签名丢失问题
        watch(() => props.elementConfig?.propertyName, (value) => {
            propertyName.value = value;
        });
        function onPropertyChange(newValue: any) {
            (props.elementConfig as PropertyEntity).propertyValue = newValue;
            const changeObject: PropertyChangeObject = {
                propertyID: (props.elementConfig as PropertyEntity).propertyID,
                propertyValue: newValue
            };
            context.emit('propertyChange', changeObject);

            // 属性变更后需要触发刷新面板
            if (props.elementConfig.refreshPanelAfterChanged) {
                context.emit('triggerRefreshPanel');
            }
        }
        const descriptionElement = props.elementConfig.description ? `<div style="color: gray; "> 描述 ：${props.elementConfig.description}</div>` : '';
        const labelTooltip = reactive({
            content: `
            <div>
                <div style="color: black; ">${props.elementConfig.propertyName}</div>
                <div style="color: gray; ">ID：${props.elementConfig.propertyID}</div>
                ${descriptionElement}
            </div>`,
            placement: 'left'
        });
        return () => {
            return (
                <div class={propertyItemClass.value}>
                    {props.elementConfig.propertyName && <label class={`col-form-label ${categoryId.value}-${propertyId.value}`} v-tooltip={labelTooltip}>
                        <span class="farris-label-text">{propertyName.value}</span>
                    </label>}
                    <FDynamicFormGroup
                        id={propertyId.value}
                        showLabel={false}
                        editor={props.elementConfig.editor}
                        modelValue={propertyValue.value}
                        onChange={onPropertyChange}
                        editorParams={props.elementConfig.editorParams}
                    ></FDynamicFormGroup>
                </div >
            );
        };
    }
});
