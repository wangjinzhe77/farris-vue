
import { DgControl } from "../../../../designer-canvas/src/composition/dg-control";

export class SchemaDOMMapping {

    /**
     * <字段类型,可配置的控件类型列表>的映射
     */
    static fieldControlTypeMapping = {
        String: [
            { key: DgControl['input-group'].type, value: DgControl['input-group'].name },
            { key: DgControl['lookup'].type, value: DgControl['lookup'].name },
            { key: DgControl['date-picker'].type, value: DgControl['date-picker'].name },
            { key: DgControl['check-group'].type, value: DgControl['check-group'].name },
            { key: DgControl['radio-group'].type, value: DgControl['radio-group'].name },
            { key: DgControl['combo-list'].type, value: DgControl['combo-list'].name },
            { key: DgControl['textarea'].type, value: DgControl['textarea'].name },
        ],
        Text: [
            { key: DgControl['textarea'].type, value: DgControl['textarea'].name },
            { key: DgControl['lookup'].type, value: DgControl['lookup'].name }
        ],
        Decimal: [
            { key: DgControl['number-spinner'].type, value: DgControl['number-spinner'].name }
        ],
        Integer: [
            { key: DgControl['number-spinner'].type, value: DgControl['number-spinner'].name }
        ],
        Number: [
            { key: DgControl['number-spinner'].type, value: DgControl['number-spinner'].name }
        ],
        BigNumber: [
            { key: DgControl['number-spinner'].type, value: DgControl['number-spinner'].name }
        ],
        Date: [
            { key: DgControl['date-picker'].type, value: DgControl['date-picker'].name }
        ],
        DateTime: [
            { key: DgControl['date-picker'].type, value: DgControl['date-picker'].name }
        ],
        Boolean: [
            { key: DgControl['switch'].type, value: DgControl['switch'].name },
            { key: DgControl['check-box'].type, value: DgControl['check-box'].name },
        ],
        Enum: [
            { key: DgControl['combo-list'].type, value: DgControl['combo-list'].name },
            { key: DgControl['radio-group'].type, value: DgControl['radio-group'].name },
        ],
        Object: [
            { key: DgControl['lookup'].type, value: DgControl['lookup'].name },
            { key: DgControl['combo-list'].type, value: DgControl['combo-list'].name },
            { key: DgControl['radio-group'].type, value: DgControl['radio-group'].name },
        ]
    };
    /**
     * 根据绑定字段类型获取可用的输入类控件
     */
    static getEditorTypesByMDataType(fieldType: string) {
        const foundTypes = SchemaDOMMapping.fieldControlTypeMapping[fieldType];
        return foundTypes ? foundTypes : [{ key: "", value: "" }];

    }
    /**
     * 获取所有输入类控件
     */
    static getAllInputTypes() {
        const allControlTypes: Array<{ key: string, value: string }> = [];
        for (const fieldType in SchemaDOMMapping.fieldControlTypeMapping) {
            SchemaDOMMapping.fieldControlTypeMapping[fieldType].forEach(control => {
                if (!allControlTypes.find(controlKeyValue => controlKeyValue.key === control.key && controlKeyValue.value === control.value)) {
                    allControlTypes.push({ key: control.key, value: control.value });
                }
            });
        }

        return allControlTypes;
    }

    /**
     * 提供schema字段基础属性和DOM控件属性的映射
     * @param control 控件元数据
     */
    static mappingDomPropAndSchemaProp(control) {
        const controlType = control.editor?.type;
        if (!controlType) {
            return [];
        }
        // 1、 基础字段
        const array: any[] = [];
        // 必填属性
        array.push({ domField: 'editor.required', schemaField: 'require' });

        // 只读属性
        array.push({ domField: 'editor.readonly', schemaField: 'readonly' });


        // 字段名称的映射根据控件类型区分
        switch (controlType) {
            case DgControl['data-grid-column'].type: case DgControl['tree-grid-column'].type:
                { array.push({ domField: 'title', schemaField: 'name' }); break; }
            default: { array.push({ domField: 'label', schemaField: 'name' }); }
        }

        // 2、类型字段
        // 文本类型和数字类型映射长度
        if (controlType === DgControl['input-group'].type || controlType === DgControl['textarea'].type
            || controlType === DgControl['number-spinner'].type) {
            array.push({ domField: 'editor.maxLength', schemaField: 'type.length' });
        }
        // 数字类型映射精度
        if (controlType === DgControl['number-spinner'].type) {
            array.push({ domField: 'editor.precision', schemaField: 'type.precision' });
        }

        // 枚举类型：下拉控件、表格列控件
        if (controlType === DgControl['combo-list'].type || controlType === DgControl['radio-group'].type) {
            array.push({ domField: 'editor.data', schemaField: 'type.enumValues' });
        }

        // 3、编辑器字段
        // if (controlType !== DgControl['data-grid-column'].type && controlType !== DgControl['tree-grid-column'].type) {
        //     array.push({ domField: 'type', schemaField: 'editor.$type' });
        // }

        // 日期类型
        if (controlType === DgControl['date-picker'].type) {
            array.push({ domField: 'editor.displayFormat', schemaField: 'editor.format' });
            array.push({ domField: 'editor.fieldType', schemaField: 'type.name' });
        }
        // 数字类型、日期类型映射最大、最小值
        if (controlType === DgControl['number-spinner'].type) {
            array.push({ domField: 'editor.max', schemaField: 'editor.maxValue' });
            array.push({ domField: 'editor.min', schemaField: 'editor.minValue' });
        }
        // 帮助类型
        if (controlType === DgControl.lookup.type) {
            array.push({ domField: 'editor.dataSource', schemaField: 'editor.dataSource' });
            array.push({ domField: 'editor.valueField', schemaField: 'editor.valueField' });
            array.push({ domField: 'editor.textField', schemaField: 'editor.textField' });
            array.push({ domField: 'editor.displayType', schemaField: 'editor.displayType' });
            array.push({ domField: 'editor.mapFields', schemaField: 'editor.mapFields' });
            array.push({ domField: 'editor.helpId', schemaField: 'editor.helpId' });

        }
        // 列表多语
        // if (controlType === DgControl['data-grid-column'].type) {
        //     array.push({ domField: 'multiLanguage', schemaField: 'multiLanguage' });
        // }
        // 列编辑器
        // if ((controlType === DgControl['data-grid-column'].type || controlType === DgControl['tree-grid-column'].type) && control.editor) {
        //     const editorMaps = this.mappingDomPropAndSchemaProp(control.editor);
        //     editorMaps.map(m => m.domField = 'editor.' + m.domField);
        //     array.push(...editorMaps);
        // }

        // 字段label相关的属性
        array.push({ domField: 'path', schemaField: 'bindingPath' });
        array.push({ domField: 'binding.path', schemaField: 'bindingField' });
        array.push({ domField: 'binding.fullPath', schemaField: 'path' });
        if (controlType === DgControl['data-grid-column'].type || controlType === DgControl['tree-grid-column'].type) {
            array.push({ domField: 'field', schemaField: 'bindingPath' });
        }
        return array;
    }
}
