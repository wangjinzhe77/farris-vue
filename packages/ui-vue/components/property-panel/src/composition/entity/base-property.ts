import { DesignerComponentInstance } from "@farris/ui-vue/components/designer-canvas";
import { DgControl } from "../../../../designer-canvas/src/composition/dg-control";
import { cloneDeep } from "lodash-es";

/**
 * 控件属性基类
 */
export class BaseControlProperty {
    public componentId: string;

    public viewModelId: string;

    public eventsEditorUtils: any;

    public formSchemaUtils: any;
    public formMetadataConverter: any;
    public designViewModelUtils: any;
    public designViewModelField: any;
    public controlCreatorUtils: any;
    public designerHostService: any;

    schemaService: any = null;

    metadataService: any = null;

    protected propertyConfig = {
        type: 'object',
        categories: {}
    };

    constructor(componentId: string, designerHostService: any) {
        this.componentId = componentId;
        this.designerHostService = designerHostService;
        this.eventsEditorUtils = designerHostService['eventsEditorUtils'];
        this.formSchemaUtils = designerHostService['formSchemaUtils'];
        this.formMetadataConverter = designerHostService['formMetadataConverter'];
        this.viewModelId = this.formSchemaUtils?.getViewModelIdByComponentId(componentId) || '';
        this.designViewModelUtils = designerHostService['designViewModelUtils'];
        this.controlCreatorUtils = designerHostService['controlCreatorUtils'];
        this.metadataService = designerHostService['metadataService'];
        this.schemaService =  designerHostService['schemaService'];
    }

    getTableInfo() {
        return this.schemaService?.getTableInfoByViewModelId(this.viewModelId);
    }

    setDesignViewModelField(propertyData: any) {
        const bindingFieldId = propertyData.binding && propertyData.binding.type === 'Form' && propertyData.binding.field;
        // 视图模型中[字段更新时机]属性现在要在控件上维护，所以在控件上复制一份属性值
        if (bindingFieldId) {
            if (!this.designViewModelField) {
                const dgViewModel = this.designViewModelUtils.getDgViewModel(this.viewModelId);
                this.designViewModelField = dgViewModel.fields.find(f => f.id === bindingFieldId);
            }
            propertyData.updateOn = this.designViewModelField?.updateOn;
        }
    }
 
    getBasicPropConfig(propertyData: any): any {
        return {
            description: 'Basic Information',
            title: '基本信息',
            properties: {
                id: {
                    description: '组件标识',
                    title: '标识',
                    type: 'string',
                    readonly: true
                },
                type: {
                    description: '组件类型',
                    title: '控件类型',
                    type: 'select',
                    editor: {
                        type: 'combo-list',
                        textField: 'name',
                        valueField: 'value',
                        editable: false,
                        data: [{ value: propertyData.type, name: DgControl[propertyData.type].name }]
                    }
                }
            }
        };
      
    }


    protected getAppearanceConfig(propertyData = null): any {
        return {
            title: "外观",
            description: "Appearance",
            properties: {
                class: {
                    title: "class样式",
                    type: "string",
                    description: "组件的CSS样式",
                    $converter: "/converter/appearance.converter"
                },
                style: {
                    title: "style样式",
                    type: "string",
                    description: "组件的样式",
                    $converter: "/converter/appearance.converter"
                }
            }
        };
    }

        /**
     * 
     * @param propertyId 
     * @param componentInstance 
     * @returns 
     */
        public updateElementByParentContainer(propertyId:string, componentInstance: DesignerComponentInstance){
            // 1、定位控件父容器
            const parentContainer = componentInstance && componentInstance.parent && componentInstance.parent['schema'];
            if (!parentContainer) {
                return;
            }
            const index = parentContainer.contents.findIndex(c => c.id === propertyId);
            // 通过cloneDeep方式的触发更新
            const controlSchema = cloneDeep(parentContainer.contents[index]);
             // 5、替换控件
            parentContainer.contents.splice(index, 1);
            parentContainer.contents.splice(index, 0, controlSchema);
        }

}
