 
import { BaseControlProperty } from "./base-property";
import { SchemaDOMMapping } from './schema-dom-mapping';
import { canvasChanged } from '../../../../designer-canvas/src/composition/designer-canvas-changed';
import { DesignerComponentInstance } from "../../../../designer-canvas/src/types";
import { FormUnifiedColumnLayout } from "../type";
import {
    ResponseFormLayoutContext,
    UseResponseLayoutEditorSetting,
} from "@farris/ui-vue/components/response-layout-editor";
import { useResponseLayoutEditorSetting } from "../../../../response-layout-editor/src/composition/converter/use-response-layout-editor-setting";
import { FormSchemaEntityFieldType$Type } from "@farris/ui-vue/components/common";

export class InputBaseProperty extends BaseControlProperty {
    public responseLayoutEditorFunction: UseResponseLayoutEditorSetting;
    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
        this.responseLayoutEditorFunction = useResponseLayoutEditorSetting(this.formSchemaUtils);
    }

    public getPropertyConfig(propertyData: any, componentInstance: DesignerComponentInstance) {
        // 基本信息
        this.propertyConfig.categories['basic'] = this.getBasicProperties(propertyData, componentInstance);
        // 外观
        this.propertyConfig.categories['appearance'] = this.getAppearanceProperties(propertyData, componentInstance);
        // 编辑器
        this.propertyConfig.categories['editor'] = this.getEditorProperties(propertyData);
        return this.propertyConfig;
    }

    public getBasicProperties(propertyData, componentInstance): any {
        const self = this;
        this.setDesignViewModelField(propertyData);
        return {
            description: 'Basic Information',
            title: '基本信息',
            properties: {
                id: {
                    description: '组件标识',
                    title: '标识',
                    type: 'string',
                    readonly: true
                },
                type: {
                    description: '编辑器类型',
                    title: '编辑器类型',
                    type: 'string',
                    refreshPanelAfterChanged: true,
                    $converter: '/converter/change-editor.converter',
                    editor: {
                        type: 'combo-list',
                        textField: 'value',
                        valueField: 'key',
                        editable: false,
                        data: self.designViewModelField ? SchemaDOMMapping.getEditorTypesByMDataType(self.designViewModelField.type?.name) : SchemaDOMMapping.getAllInputTypes()
                    }
                },
                label: {
                    title: "标签",
                    type: "string",
                    $converter: '/converter/form-group-label.converter'
                },
                binding: {
                    description: "绑定的表单字段",
                    title: "绑定",
                    editor: {
                        type: "binding-selector",
                        bindingType: { "enable": false },
                        editorParams: {
                            componentSchema: propertyData,
                            needSyncToViewModel: true,
                            viewModelId: this.viewModelId,
                            designerHostService: this.designerHostService,
                            disableOccupiedFields: true
                        },
                        textField: 'bindingField'
                    }
                }
            },
            setPropertyRelates(changeObject, prop) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject && changeObject.propertyID) {
                    case 'type': {
                        self.changeControlType(propertyData, changeObject, componentInstance);
                        break;
                    }
                    case 'label': {
                        changeObject.needRefreshControlTree = true;
                        break;
                    }
                }
            }
        };
    }
    public getAppearanceProperties(propertyData, componentInstance): any {

        const self = this;
        return {
            title: "外观",
            description: "Appearance",
            properties: {
                class: {
                    title: "class样式",
                    type: "string",
                    description: "组件的CSS样式",
                    $converter: "/converter/appearance.converter"
                },
                style: {
                    title: "style样式",
                    type: "string",
                    description: "组件的内联样式",
                    $converter: "/converter/appearance.converter"
                },
                responseLayout: {
                    description: "响应式列宽",
                    title: "响应式列宽",
                    type: "boolean",
                    visible: true,
                    // 这个属性，标记当属性变更得时候触发重新更新属性
                    refreshPanelAfterChanged: true,
                    editor: {
                        type: "response-layout-editor-setting",
                        initialState: self.responseLayoutEditorFunction.checkCanOpenLayoutEditor(propertyData, self.componentId)
                    }
                }
            },
            setPropertyRelates(changeObject, prop) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject && changeObject.propertyID) {
                    case 'responseLayout':
                        self.responseLayoutEditorFunction.changeFormControlsByResponseLayoutConfig(changeObject.propertyValue, self.componentId || propertyData.id);
                        self.updateUnifiedLayoutAfterResponseLayoutChanged(self.componentId);
                        self.updateElementByParentContainer(propertyData.id, componentInstance);
                        delete propertyData.responseLayout;
                        break;
                    case 'class':
                        self.updateUnifiedLayoutAfterControlChanged(changeObject.propertyValue, propertyData.id, this.componentId);
                        self.updateElementByParentContainer(propertyData.id, componentInstance);
                        break;
                }

            }
        };
    };

    public getEditorProperties(propertyData): any {
        return this.getComponentConfig(propertyData);
    }


    /**
     * 卡片控件：切换控件类型后事件
     * @param propertyData 控件DOM属性
     * @param newControlType 新控件类型
     */
    private changeControlType(propertyData, changeObject, componentInstance: DesignerComponentInstance) {
        const newControlType = changeObject.propertyValue;

        // 1、定位控件父容器
        const parentContainer = componentInstance && componentInstance.parent && componentInstance.parent['schema'];
        if (!parentContainer) {
            return;
        }

        const index = parentContainer.contents.findIndex(c => c.id === propertyData.id);
        const oldControl = parentContainer.contents[index];

        let newControl;
        // 2、记录绑定字段viewModel的变更
        if (this.designViewModelField) {
            const dgViewModel = this.designViewModelUtils.getDgViewModel(this.viewModelId);
            dgViewModel.changeField(this.designViewModelField.id, {
                editor: {
                    $type: newControlType
                },
                name: this.designViewModelField.name,
                require: this.designViewModelField.require,
                readonly: this.designViewModelField.readonly
            }, false);
            // 3、创建新控件
            newControl = this.controlCreatorUtils.setFormFieldProperty(this.designViewModelField, newControlType);
        }
        if (!newControl) {
            newControl = this.controlCreatorUtils.createFormGroupWithoutField(newControlType);
        }
        //  4、保留原id样式等属性
        Object.assign(newControl, {
            id: oldControl.id,
            appearance: oldControl.appearance,
            size: oldControl.size,
            label: oldControl.label,
            binding: oldControl.binding,
            visible: oldControl.visible
        });
        Object.assign(newControl.editor, {
            isTextArea: newControl.isTextArea && oldControl.isTextArea,
            placeholder: oldControl.editor?.placeholder,
            holdPlace: oldControl.editor?.holdPlace,
            readonly: oldControl.editor?.readonly,
            required: oldControl.editor?.required,
        });

        // 5、替换控件
        parentContainer.contents.splice(index, 1);
        parentContainer.contents.splice(index, 0, newControl);
        componentInstance.schema = Object.assign(oldControl, newControl);

        // 6、暂时移除旧控件的选中样式（后续考虑更好的方式）
        Array.from(document.getElementsByClassName('dgComponentSelected') as HTMLCollectionOf<HTMLElement>).forEach(
            (element: HTMLElement) => element.classList.remove('dgComponentSelected')
        );

        Array.from(document.getElementsByClassName('dgComponentFocused') as HTMLCollectionOf<HTMLElement>).forEach(
            (element: HTMLElement) => element.classList.remove('dgComponentFocused')
        );
        // 7、触发刷新
        canvasChanged.value++;

    }

    public getComponentConfig(propertyData, info = {}, properties = {}, setPropertyRelates?: any) {
        const editorBasic = Object.assign({
            description: "编辑器",
            title: "编辑器",
            type: "input-group",
            $converter: "/converter/property-editor.converter"
        }, info);

        const editorProperties = Object.assign({
            readonly: {
                description: "",
                title: "只读",
                type: "boolean",
                editor: {
                    enableClear: true,
                    editable: true
                }
            },
            disabled: {
                description: "",
                title: "禁用",
                type: "boolean",
                visible: false
            },
            // required: {
            //     description: "",
            //     title: "必填",
            //     type: "boolean"
            // },
            placeholder: {
                description: "当控件没有值时在输入框中显示的文本",
                title: "提示文本",
                type: "string"
            }
        }, properties);

        return { ...editorBasic, properties: { ...editorProperties }, setPropertyRelates };

    }


    /**
   * 修改某一输入控件的样式后更新Form的统一布局配置
   * @param controlClass 控件样式
   * @param controlId 控件Id
   * @param componentId 控件所在组件id
   */
    private updateUnifiedLayoutAfterControlChanged(controlClass: string, controlId: string, componentId: string) {
        const controlClassArray = controlClass.split(' ');

        let colClass = controlClassArray.find(item => /^col-([1-9]|10|11|12)$/.test(item));
        let colMDClass = controlClassArray.find(item => /^col-md-([1-9]|10|11|12)$/.test(item));
        let colXLClass = controlClassArray.find(item => /^col-xl-([1-9]|10|11|12)$/.test(item));
        let colELClass = controlClassArray.find(item => /^col-el-([1-9]|10|11|12)$/.test(item));

        colClass = colClass || 'col-12';
        colMDClass = colMDClass || 'col-md-' + colClass.replace('col-', '');
        colXLClass = colXLClass || 'col-xl-' + colMDClass.replace('col-md-', '');
        colELClass = colELClass || 'col-el-' + colXLClass.replace('col-xl-', '');

        const latestControlLayoutConfig = {
            id: controlId,
            columnInSM: parseInt(colClass.replace('col-', ''), 10),
            columnInMD: parseInt(colMDClass.replace('col-md-', ''), 10),
            columnInLG: parseInt(colXLClass.replace('col-xl-', ''), 10),
            columnInEL: parseInt(colELClass.replace('col-el-', ''), 10),
        };

        this.updateUnifiedLayoutAfterResponseLayoutChanged(componentId, latestControlLayoutConfig);
    }

    /**
     * 修改控件布局配置后更新Form统一布局配置
     * @param componentId 组件Id
     * @param controlLayoutConfig 某单独变动的控件配置项，FormResponseLayoutContext类型
     */
    private updateUnifiedLayoutAfterResponseLayoutChanged(componentId: string, controlLayoutConfig?: any): FormUnifiedColumnLayout | undefined {
        const { formNode } = this.responseLayoutEditorFunction.checkCanFindFormNode(componentId);
        // 更改form上的统一配置
        if (!formNode || !formNode.unifiedLayout) {
            return;
        }
        const responseLayoutConfig: ResponseFormLayoutContext[] = [];
        this.responseLayoutEditorFunction.getResonseFormLayoutConfig(formNode, responseLayoutConfig, 1);
        if (controlLayoutConfig) {
            const changedControl = responseLayoutConfig.find(c => c.id === controlLayoutConfig.id);
            Object.assign(changedControl || {}, controlLayoutConfig);
        }

        // 收集每种屏幕下的列数
        const columnInSMArray = responseLayoutConfig.map(config => config.columnInSM);
        const columnInMDArray = responseLayoutConfig.map(config => config.columnInMD);
        const columnInLGArray = responseLayoutConfig.map(config => config.columnInLG);
        const columnInELArray = responseLayoutConfig.map(config => config.columnInEL);

        // 只有每个控件的宽度都一样时，才认为form上有统一宽度，否则认为是自定义的控件宽度，此处传递null
        const uniqueColClassInSM = this.checkIsUniqueColumn(columnInSMArray) ? columnInSMArray[0] : null;
        const uniqueColClassInMD = this.checkIsUniqueColumn(columnInMDArray) ? columnInMDArray[0] : null;
        const uniqueColClassInLG = this.checkIsUniqueColumn(columnInLGArray) ? columnInLGArray[0] : null;
        const uniqueColClassInEL = this.checkIsUniqueColumn(columnInELArray) ? columnInELArray[0] : null;


        Object.assign(formNode.unifiedLayout, {
            uniqueColClassInSM,
            uniqueColClassInMD,
            uniqueColClassInLG,
            uniqueColClassInEL
        });
    }
    /**
     * 校验宽度样式值是否一致
     */
    private checkIsUniqueColumn(columnsInScreen: number[]) {
        const keySet = new Set(columnsInScreen);
        const exclusiveKeys = Array.from(keySet);

        if (exclusiveKeys.length === 1) {
            return true;
        }
        return false;
    }
    /**
     * 枚举项编辑器
     * @param propertyData 
     * @param valueField 
     * @param textField 
     * @returns 
     */
    protected getItemCollectionEditor(propertyData, valueField, textField) {
        valueField = valueField || 'value';
        textField = textField || 'name';
        return {
            editor: {
                columns: [
                    { field: valueField, title: '值', dataType: 'string' },
                    { field: textField, title: '名称', dataType: 'string' },
                    { field: 'disabled', title: '禁用', visible: false, dataType: 'boolean', editor: { type: 'switch' } },
                ],
                type: "item-collection-editor",
                valueField: valueField,
                nameField: textField,
                requiredFields: [valueField, textField],
                uniqueFields: [valueField, textField],
                readonly: this.checkEnumDataReadonly(propertyData)
            }
        };
    }
    /**
  * 判断枚举数据是否只读
  * 1、没有绑定信息或者绑定变量，可以新增、删除、修改
  * 2、绑定类型为字段，且字段为枚举字段，则不可新增、删除、修改枚举值。只能从be修改然后同步到表单上。
  * @param propertyData 下拉框控件属性值
  */
    protected checkEnumDataReadonly(propertyData: any): boolean {
        // 没有绑定信息或者绑定变量
        if (!propertyData.binding || propertyData.binding.type !== 'Form') {
            return false;
        }
        if (this.designViewModelField && this.designViewModelField.type &&
            this.designViewModelField.type.$type === FormSchemaEntityFieldType$Type.EnumType) {
            // 低代码、零代码，枚举字段均不可以改
            return true;
        }
        return false;
    }
}
