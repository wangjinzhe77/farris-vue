 
import { ref } from "vue";
import { DesignerItemContext, ComponentSchema, } from '@farris/ui-vue/components/designer-canvas';
import { DesignerHTMLElement, DraggingResolveContext, UseDesignerRules } from '../../../../designer-canvas/src/composition/types';

export function useInputDesignerRules(designItemContext: DesignerItemContext, designerHostService): UseDesignerRules {
    const schema = designItemContext.schema as ComponentSchema;
    /** 组件在拖拽时需要将所属的Component一起拖拽 */
    const triggerBelongedComponentToMoveWhenMoved = ref(true);
    /** 组件在删除时需要将所属的Component一起拖拽 */
    const triggerBelongedComponentToDeleteWhenDeleted = ref(true);
    /** data-grid所属的上级组件控制规则 */
    /**
     * 判断是否可以接收拖拽新增的子级控件
     */
    function canAccepts(draggingContext: DraggingResolveContext): boolean {
        return false;
    }

    /**
     * data-grid是否支持删除，取决于所属组件是否支持删除
     */
    function checkCanDeleteComponent() {
        return false;
    }
    /**
     * data-grid是否支持移动，取决于所属组件是否支持移动
     */
    function checkCanMoveComponent() {
        return false;
    }

    function hideNestedPaddingInDesginerView() {
        return true;
    }

    function onAcceptMovedChildElement(sourceElement: DesignerHTMLElement) {
    }
    /**
     * 判断data-grid上下文
     */
    function resolveComponentContext() {

    }
    // 构造属性配置方法
    function getPropsConfig(componentId: string) {
        return null;
    }

    return {
        canAccepts,
        checkCanDeleteComponent,
        checkCanMoveComponent,
        hideNestedPaddingInDesginerView,
        onAcceptMovedChildElement,
        resolveComponentContext,
        triggerBelongedComponentToMoveWhenMoved,
        triggerBelongedComponentToDeleteWhenDeleted,
        getPropsConfig
    } as UseDesignerRules;

}
