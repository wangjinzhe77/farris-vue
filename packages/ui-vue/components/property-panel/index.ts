/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App } from 'vue';
import FPropertyPanel from './src/property-panel.component';
import { BaseControlProperty } from './src/composition/entity/base-property';
import { SchemaDOMMapping } from './src/composition/entity/schema-dom-mapping';

export * from './src/composition/props/property-panel-item.props';
export * from './src/composition/props/property-panel.props';

export * from './src/composition/type';

export { FPropertyPanel, BaseControlProperty, SchemaDOMMapping };

export default {
    install(app: App): void {
        app.component(FPropertyPanel.name as string, FPropertyPanel);
    }
};
