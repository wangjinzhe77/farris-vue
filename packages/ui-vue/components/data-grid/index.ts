/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App, Plugin } from 'vue';
import FDataGrid from './src/data-grid.component';
import FDataGridDesign from './src/designer/data-grid.design.component';
import FGridFieldEditor from './src/designer/grid-field-editor.component';
import FDataGridColumnDesign from './src/designer/data-grid-column.design.component';
import { columnPropsResolver, treeColumnPropsResolver } from './src/data-grid-column.props';
import { gridFieldEditorPropsResolver } from './src/designer/grid-field-editor.props';
import { bindingResolver, callbackResolver, propsResolver, selectionItemResolver, updateColumnsResolver } from './src/data-grid.props';

export * from './src/data-grid.props';

FDataGrid.install = (app: App) => {
    app.component(FDataGrid.name as string, FDataGrid);
    app.component(FDataGridDesign.name as string, FDataGridDesign);
};

FDataGrid.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap['data-grid'] = FDataGrid;
    propsResolverMap['data-grid'] = propsResolver;
    componentMap['grid-field-editor'] = FGridFieldEditor;
    propsResolverMap['grid-field-editor'] = gridFieldEditorPropsResolver;
    resolverMap['data-grid'] = { bindingResolver, selectionItemResolver, updateColumnsResolver, callbackResolver };
};

FDataGrid.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['data-grid'] = FDataGridDesign;
    propsResolverMap['data-grid'] = propsResolver;
    propsResolverMap['data-grid-column'] = columnPropsResolver;
    componentMap['data-grid-column'] = FDataGridColumnDesign;
    // 树表列和表格列相似
    // jumphere by sagi
    propsResolverMap['tree-grid-column'] = treeColumnPropsResolver;
    componentMap['tree-grid-column'] = FDataGridColumnDesign;

};

export { FDataGrid };
export default FDataGrid as typeof FDataGrid & Plugin;
