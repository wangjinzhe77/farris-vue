 
import { BaseControlProperty } from '../../../property-panel/src/composition/entity/base-property';
import { DgControl } from '../../../designer-canvas/src/composition/dg-control';
import { InputGroupProperty } from '../../../input-group/src/property-config/input-group.property-config';
import { AvatarProperty } from '../../../avatar/src/property-config/avatar.property-config';
import { CheckGroupProperty } from '../../../checkbox/src/property-config/check-group.property-config';
import { CheckBoxProperty } from '../../../checkbox/src/property-config/checkbox.property-config';
import { ComboListProperty } from '../../../combo-list/src/property-config/combo-list.property-config';
import { TextareaProperty } from '../../../textarea/src/property-config/textarea.property-config';
import { DatePickerProperty } from '../../../date-picker/src/property-config/date-picker.property-config';
import { LookupPropertyConfig } from '../../../lookup/src/property-config/lookup.property-config';
import { NumberRangeProperty } from '../../../number-range/src/property-config/number-range.property-config';
import { NumberSpinnerProperty } from '../../../number-spinner/src/property-config/number-spinner.property-config';
import { RadioGroupProperty } from '../../../radio-group/src/property-config/radio-group.property-config';
import { SwitchProperty } from '../../../switch/src/property-config/switch.property-config';
import { TimePickerProperty } from '../../../time-picker/src/property-config/time-picker.property-config';
import { SchemaDOMMapping } from '../../../property-panel/src/composition/entity/schema-dom-mapping';

export class DataGriColumnProperty extends BaseControlProperty {
    constructor(componentId: string, designerHostService: any) {
        super(componentId, designerHostService);
    }
    getPropertyConfig(propertyData: any, gridData: any) {
        // 基本信息
        this.getBasicPropConfig(propertyData);

        // // 外观
        this.getAppearanceProperties(propertyData);

        // // 事件
        // this.getEventPropConfig(propertyData);
        this.getFieldEditorProperties(propertyData, gridData);

        return this.propertyConfig;
    }

    getBasicPropConfig(propertyData: any) {
        this.propertyConfig.categories['basic'] = {
            description: 'Basic Information',
            title: '基本信息',
            properties: {
                id: {
                    description: '组件标识',
                    title: '标识',
                    type: 'string',
                    readonly: true
                },
                title: {
                    description: '标题',
                    title: '标题',
                    type: 'string'
                }
            }
        };
    }
    getAppearanceProperties(propertyData: any) {
        this.propertyConfig.categories['appearance'] = {
            description: '',
            title: '外观',
            properties: {
                resizable: {
                    description: '允许拖拽改变列宽',
                    title: '拖拽改变列宽',
                    type:'boolean'
                },
                halign: {
                    description: '标题对齐方式',
                    title: '标题对齐方式',
                    type: 'enum',
                    editor: {
                        data: [
                            { id: 'left', name: '左对齐' },
                            { id: 'center', name: '居中' },
                            { id: 'right', name: '右对齐' }
                        ]
                    }
                },
                align: {
                    description: '数据水平对齐方式选择',
                    title: '数据水平对齐方式',
                    type: 'enum',
                    editor: {
                        data: [
                            { id: 'left', name: '左对齐' },
                            { id: 'center', name: '居中' },
                            { id: 'right', name: '右对齐' }
                        ]
                    }
                },
                valign: {
                    description: '数据垂直对齐方式选择',
                    title: '数据垂直对齐方式',
                    type: 'enum',
                    editor: {
                        data: [
                            { id: 'top', name: '顶对齐' },
                            { id: 'middle', name: '居中' },
                            { id: 'bottom', name: '底对齐' }
                        ]
                    }
                }
            }
        };
    }
    private getFieldEditorProperties(propertyData: any, gridData: any) {
        if (gridData.fieldEditable && propertyData.editor) {
            this.getFieldEditorProp(propertyData);
        }
    }
    /**
     * 列编辑器属性
     * @param propertyData 列属性值
     * @param viewModelId viewModelId
     */
    getFieldEditorProp(propertyData: any, showPosition = 'gridFieldEditor', isSimpleTable = false) {
        const editorType = this.formMetadataConverter.getRealEditorType(propertyData.editor.type);
        // const editorTypeConfig = propertyConfigSchemaMap[editorType];
        let editorTypeConfig;
        switch (editorType) {
            case 'avatar':
                editorTypeConfig = new AvatarProperty(this.componentId, this.designerHostService);
                break;
            case 'check-group':
                editorTypeConfig = new CheckGroupProperty(this.componentId, this.designerHostService);
                break;
            case 'check-box':
                editorTypeConfig = new CheckBoxProperty(this.componentId, this.designerHostService);
                break;
            case 'combo-list':
                editorTypeConfig = new ComboListProperty(this.componentId, this.designerHostService);
                break;
            case 'date-picker':
                editorTypeConfig = new DatePickerProperty(this.componentId, this.designerHostService);
                break;
            case 'lookup':
                editorTypeConfig = new LookupPropertyConfig(this.componentId, this.designerHostService);
                break;
            case 'number-range':
                editorTypeConfig = new NumberRangeProperty(this.componentId, this.designerHostService);
                break;
            case 'number-spinner':
                editorTypeConfig = new NumberSpinnerProperty(this.componentId, this.designerHostService);
                break;
            case 'radio-group':
                editorTypeConfig = new RadioGroupProperty(this.componentId, this.designerHostService);
                break;
            case 'switch':
                editorTypeConfig = new SwitchProperty(this.componentId, this.designerHostService);
                break;
            case 'textarea':
                editorTypeConfig = new TextareaProperty(this.componentId, this.designerHostService);
                break;
            case 'time-picker':
                editorTypeConfig = new TimePickerProperty(this.componentId, this.designerHostService);
                break;
            default:
                editorTypeConfig = new InputGroupProperty(this.componentId, this.designerHostService);
        }
        this.setDesignViewModelField(propertyData);
        const self = this;
        this.propertyConfig.categories[`${showPosition}_editorType`] = {
            description: '',
            title: '编辑器类型',
            propertyData: propertyData.editor,
            tabId: 'gridFieldEditor',
            // enableCascade: true,
            parentPropertyID: 'editor',
            tabName: '编辑器',
            properties: {
                type: {
                    description: '',
                    title: '编辑器类型',
                    type: 'enum',
                    $converter: '/converter/change-editor.converter',
                    refreshPanelAfterChanged: true,
                    editor: {
                        type: 'combo-list',
                        textField: 'value',
                        valueField: 'key',
                        data: SchemaDOMMapping.getEditorTypesByMDataType(self.designViewModelField.type.name)
                    }
                }
            },
            setPropertyRelates(changeObject, prop) {
                if (!changeObject) {
                    return;
                }
                switch (changeObject && changeObject.propertyID) {
                    case 'type': {
                        self.changeFieldEditorType(propertyData, changeObject.propertyValue);
                        break;
                    }
                }
            }
        };
        const notSupportProperty = ['id', 'type', 'class', 'style', 'binding', 'label', 'responseLayout'];
        const { categories } = editorTypeConfig.getPropertyConfig(propertyData);
        Object.keys(categories).map((categoryId: string) => {
            const propertyCategory = categories[categoryId];
            const properties = {};
            Object.keys(propertyCategory.properties).map((propertyId) => {
                // 此处属性还有visible和通过计算控制的情况--ToDo
                if (notSupportProperty.findIndex((item) => item === propertyId) === -1 && propertyCategory.properties[propertyId] !== false) {
                    properties[propertyId] = propertyCategory.properties[propertyId];
                }
            });
            if (Object.keys(properties).length !== 0) {
                const title = propertyCategory?.title;
                const tabId = 'gridFieldEditor';
                const tabName = '编辑器';
                const description = propertyCategory?.description;
                const $converter = propertyCategory?.$converter || '';
                self.propertyConfig.categories[showPosition + '_' + categoryId] = {
                    title,
                    tabId,
                    tabName,
                    description,
                    $converter,
                    propertyData,
                    properties,
                    setPropertyRelates:propertyCategory?.setPropertyRelates
                };
            }
        });
    }
    /**
     * datagrid field:列编辑器类型切换后事件
     * @param newControlType 新控件类型
     * @param viewModelId 视图模型ID
     */
    changeFieldEditorType(propertyData: any, newControlType: string) {
        // 1、记录viewModel 变更---全量替换editor
        const dgViewModel = this.designViewModelUtils.getDgViewModel(this.viewModelId);
        dgViewModel.changeField(
            this.designViewModelField.id,
            {
                editor: {
                    $type: newControlType
                },
                name: this.designViewModelField.name,
                require: this.designViewModelField.require,
                readonly: this.designViewModelField.readonly
            },
            false
        );

        // 2、定位列表
        const cmp = this.formSchemaUtils.getComponentByViewModelId(this.viewModelId);
        const grid = this.formSchemaUtils.selectNode(
            cmp,
            (item) => item.type === DgControl['data-grid'].type || item.type === DgControl['tree-grid'].type
        );

        if (!grid) {
            return;
        }
        const columnData = dgViewModel.fields.find((item) => item['id'] === this.designViewModelField.id);
        const newField = this.controlCreatorUtils.setGridFieldProperty(newControlType, columnData, null, true);

        // 4、替换列编辑器
        // 兼容新旧表格列属性
        const foundGridColumns = Object.prototype.hasOwnProperty.call(grid, 'columns') ? grid['columns'] : grid['fields'];
        const index = foundGridColumns.findIndex((field) => field.id === propertyData.id);
        if (index > -1 && foundGridColumns[index].editor) {
            foundGridColumns[index].editor = Object.assign(newField.editor, {
                required: foundGridColumns[index].editor.required
            });
        }
        // 更新类型和formatter
        Object.assign(foundGridColumns[index],{dataType:newField.dataType},newField.formatter?{formatter:newField.formatter}:{});
        // 在类型来回切换时，有控件带formatter，有的没有，切换时需要移除
        if(!newField.formatter&&foundGridColumns[index].formatter){
            delete foundGridColumns[index].formatter;
        }
    }
}
