 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType, VNode } from 'vue';
import { EditorConfig } from '../../dynamic-form';
import { createPropsResolver } from '../../dynamic-resolver';
import { columnSchemaMapper } from './schema/schema-mapper';
import { columnSchemaResolver } from "./schema/schema-resolver";
import dataGridColumnSchema from './schema/data-grid-column.schema.json';
import treeGridColumnSchema from '../../tree-grid/src/schema/tree-grid-column.schema.json';
import { VisualData, VisualDataCell } from '../../data-view';


export type DataGridAlignment = 'left' | 'center' | 'right';

export type DataGridVerticalAlignment = 'top' | 'middle' | 'bottom';

export type SortType = 'asc' | 'desc' | 'none';

export interface DataGridColumnCommand {
    type: string;
    text: string;
    command?: string;
    icon?: string;
    hidden?: boolean;
    onClick: (e: MouseEvent, dataIndex: number, visualDataRow: VisualData) => void;
}


export const dataGridColumnProps = {
    id: { type: String, default: '' },
    type: { type: String, default: '' },
    /** 允许分组，默认为 true */
    allowGrouping: { type: Boolean, default: true },
    /** 列合并 */
    colspan: { type: Number, default: 1 },
    dataType: { type: String, default: '' },
    parentId: { type: String, default: '' },
    editor: { type: Object as PropType<EditorConfig> },
    // 字段
    field: { type: String, default: '' },
    /** True to allow the column can be filtered. */
    filterable: { type: Boolean, default: true },
    /** 是否固定 */
    fixed: { type: String, default: '' },

    /** 标题对齐方式 */
    halign: { type: String, default: 'left' },
    // 标题
    title: { type: String, default: '' },
    /** 只读 */
    readonly: { type: Boolean, default: true },
    /** True to allow the column can be resized. */
    resizable: { type: Boolean, default: true },
    rowspan: { type: Boolean, default: true },

    /** 鼠标移动至单元格后，显示悬浮消息 */
    showTips: { type: Boolean, default: true },
    sort: { type: String as PropType<SortType>, default: 'none' },
    /** True to allow the column can be sorted. */
    sortable: { type: Boolean, default: true },
    sortOrder: { type: Number },
    /** 单元格提示模式：
 *  allways： 鼠标滑过即显示
 *  auto: 单元格宽度不够时才会显示
 */
    tipMode: { type: String, default: 'auto' },
    /** 垂直对齐方式 'top' | 'middle' | 'bottom'*/
    valign: { type: String, default: 'middle' },
    /** 是否显示 */
    visible: { type: Boolean, default: true },
    // 宽度
    width: { type: Number },
    /** 记录原始定义宽度 */
    actualWidth: { type: Number },

    /** 文本对齐方式  'left' | 'center' | 'right';*/
    align: { type: String, default: 'left' },
    left: { type: Number },
    sorter: { type: Function as PropType<(preValue: any, postValue: any, sortType: SortType) => number>, default: () => { } },
    /** 列合并原始值  */
    origianlColSpan: { type: Number },
    index: { type: Number },
    /** 是否多语字段 */
    isMultilingualField: { type: Boolean, default: false },
    /** 操作列命令 */
    commands: { type: Array<DataGridColumnCommand>, default: [] },
    filter: { type: String, default: '' },
    showSetting: { type: Boolean, default: true },
    /** 格式化函数 */
    formatter: { type: Function as PropType<(cell: VisualDataCell, visualDataRow: VisualData) => VNode | string>, default: () => { } },
    componentId: { type: String, default: '' }
} as Record<string, any>;

export type DataGridColumnProps = ExtractPropTypes<typeof dataGridColumnProps>;

export const columnPropsResolver = createPropsResolver<DataGridColumnProps>(dataGridColumnProps, dataGridColumnSchema, columnSchemaMapper, columnSchemaResolver);
// jumphere by saig
export const treeColumnPropsResolver = createPropsResolver<DataGridColumnProps>(dataGridColumnProps, treeGridColumnSchema, columnSchemaMapper, columnSchemaResolver);
