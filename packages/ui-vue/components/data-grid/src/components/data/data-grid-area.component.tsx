import { CellMode, DataColumn, VisualData, VisualDataCell } from "@farris/ui-vue/components/data-view";
import { defineComponent, inject, ref, watch } from "vue";
import { DataGridAreaProps, dataGridAreaProps } from "./data-grid-area.props";

export default defineComponent({
    name: 'FDataGridArea',
    props: dataGridAreaProps,
    setup(props: DataGridAreaProps) {

        // const context1: any = inject('FDataGridContext', null);

        // const cellPositionMap = ref(context1?.cellPositionMap.value || { left: {}, primary: {}, right: {} });
        const cellPositionMap = ref(props.cellPositionMap);

        const dataGridContext: any = inject('FDataGridContext');
        const {
            parentProps,
            parentContext: context,
            primaryGridContentRef,
            leftFixedGridContentRef,
            rightFixedGridContentRef,
            useCellContentStyleComposition,
            useCellPositionComposition,
            useColumnComposition,
            useEditComposition,
            useGroupDataComposition,
            useNavigationComposition,
            useRowComposition,
            useVisualDataComposition,
            useVirtualScrollComposition,
            visibleDatas,


        } = dataGridContext;
        const {
            calculateCellPositionInRow,
            cellKey,
            cellPosition,
            rowKey,
            rowPosition,
            rowSpanCellPosition,
            groupCellPosition,
            summaryCellPosition
        } = useCellPositionComposition;

        const { cellContentClass, cellContentPosition, cellContentClassWithEllipsis } = useCellContentStyleComposition;
        const { collpaseGroupIconClass } = useGroupDataComposition;

        const { gridCellClass, gridRowClass, onClickRow, onMouseoverRow, onMouseoutRow } = useRowComposition;

        const { onClickCell } = useEditComposition;

        const { toggleGroupRow } = useVisualDataComposition;

        const { columnContext, hasLeftFixedColumn, hasRightFixedColumn } = useColumnComposition;

        const { navigateOnKeyUp } = useNavigationComposition;

        const {
            gridDataStyle,
            gridMergedDataStyle,
            leftFixedGridDataStyle,
            leftFixedGridMergedDataStyle,
            rightFixedGridDataStyle,
            rightFixedGridMergedDataStyle,
        } = useVirtualScrollComposition;


        watch(() => props.cellPositionMap, (newValue) => {
            cellPositionMap.value = newValue;
        });

        function onToggleGroupRow($event: MouseEvent, dataItem: VisualData) {
            dataItem.collapse = !dataItem.collapse;
            visibleDatas.value = toggleGroupRow(dataItem.collapse ? 'collapse' : 'expand', dataItem, visibleDatas.value);
        }

        function renderGroupRowCells(
            groupRow: VisualData,area:string) {
            const groupRowCells: any[] = [];
            if (groupRow.groupField && cellPositionMap.value[area]?.[groupRow.groupField]) {
                for (let layer = 0; layer <= groupRow.layer; layer++) {
                    groupRowCells.push(
                        <div class="fv-grid-group-row-icon" onClick={(payload: MouseEvent) => onToggleGroupRow(payload, groupRow)}>
                            <span class={collpaseGroupIconClass(groupRow)}></span>
                        </div>
                    );
                }
                const groupCell = groupRow.data[groupRow.groupField];
                groupRowCells.push(
                    <div
                        ref={groupCell.setRef}
                        key={cellKey(groupRow, groupRow.layer + 1)}
                        class="fv-grid-cell"
                        style={groupCellPosition(groupCell, cellPositionMap.value[area], groupRow.layer)}>
                        {groupCell.data}
                    </div>
                );
                Object.values(groupRow.data)
                    .filter((cell: VisualDataCell) => cell.field !== groupRow.groupField && cell.colSpan !== 0)
                    .forEach((cell: VisualDataCell) => {
                        groupRowCells.push(
                            <div
                                ref={cell.setRef}
                                key={cellKey(groupRow, cell.index)}
                                class="fv-grid-cell"
                                style={cellPosition(cell, cellPositionMap.value[area])}>
                                {cell.data}
                            </div>
                        );
                    });
            }
            return groupRowCells;
        }

        function renderGroupRow(groupRow: VisualData,
            // cellPositionMap.value: Record<string, { left: number; width?: number }>,
            area: string,
            columns?: DataColumn[]) {
            return (
                groupRow.layer > -1 && (
                    <div
                        ref={groupRow.setRef}
                        key={rowKey(groupRow)}
                        class={gridRowClass(groupRow)}
                        style={rowPosition(groupRow)}
                        onClick={(payload: MouseEvent) => onClickRow(payload, groupRow)}
                        onMouseover={(payload: MouseEvent) => onMouseoverRow(payload, groupRow)}>
                        {renderGroupRowCells(groupRow,area)}
                    </div>
                )
            );
        }

        function renderSummaryRowCells(summaryRow: VisualData,area:string) {
            const summaryRowCells: any[] = [];
            if (summaryRow.groupField && cellPositionMap.value[area]?.[summaryRow.groupField]) {
                const summaryCell = summaryRow.data[summaryRow.groupField];
                summaryRowCells.push(
                    <div
                        ref={summaryCell.setRef}
                        key={cellKey(summaryRow, summaryRow.layer + 1)}
                        class="fv-grid-cell"
                        style={summaryCellPosition(summaryCell, cellPositionMap.value[area], summaryRow.layer)}>
                        {summaryCell.data}
                    </div>
                );
            }
            return summaryRowCells;
        }

        function renderSummaryRow(summaryRow: VisualData,
            // cellPositionMap.value: Record<string, { left: number; width?: number }>,
            area: string,
            columns?: DataColumn[]) {
            return (
                summaryRow.layer > -1 && (
                    <div
                        ref={summaryRow.setRef}
                        key={rowKey(summaryRow)}
                        class={gridRowClass(summaryRow)}
                        style={rowPosition(summaryRow)}
                        onClick={(payload: MouseEvent) => onClickRow(payload, summaryRow)}
                        onMouseover={(payload: MouseEvent) => onMouseoverRow(payload, summaryRow)}>
                        {renderSummaryRowCells(summaryRow,area)}
                    </div>
                )
            );
        }

        function renderDataRow(
            visualDataRow: VisualData,
            area: string,
            columns?: DataColumn[]
        ) {
            return (
                <div
                    ref={visualDataRow.setRef}
                    key={rowKey(visualDataRow)}
                    class={gridRowClass(visualDataRow)}
                    style={rowPosition(visualDataRow)}
                    onClick={(payload: MouseEvent) => onClickRow(payload, visualDataRow)}
                    onMouseover={(payload: MouseEvent) => onMouseoverRow(payload, visualDataRow)}
                    onMouseout={(payload: MouseEvent) => onMouseoutRow(payload, visualDataRow)}>
                    {Object.values(visualDataRow.data)
                        .filter((cell: VisualDataCell) => cellPositionMap.value[area]?.[cell.field] && cell.rowSpan === 1)
                        .map((cell: VisualDataCell) => {
                            const currentColumn = columns?.find(column => column.field === cell.field);
                            return (
                                <div
                                    ref={cell.setRef}
                                    key={cellKey(visualDataRow, cell.index)}
                                    class={gridCellClass(cell)}
                                    style={cellPosition(cell, cellPositionMap.value[area])}
                                    onClick={(payload: MouseEvent) => onClickCell(payload, cell, visualDataRow, currentColumn as DataColumn)}
                                    onKeyup={(payload: KeyboardEvent) => navigateOnKeyUp(payload, cell)}>
                                    <div class={cellContentClass(cell, currentColumn)} style={cellContentPosition(cell)}>
                                        <div class={cellContentClassWithEllipsis(cell)}>
                                            {
                                                cell.data != null && context.slots.cellTemplate ?
                                                    context.slots.cellTemplate({ cell, row: visualDataRow }) :
                                                    (
                                                        cell.mode === CellMode.editing ?
                                                            cell.getEditor(cell) :
                                                            (
                                                                cell.formatter ?
                                                                    cell.formatter(cell, visualDataRow) :
                                                                    (cell.data != null ? cell.data.toString() : cell.data)
                                                            )
                                                    )
                                            }
                                        </div>
                                    </div>
                                </div>
                            );
                        })}
                </div>
            );
        }

        const visualDataRowRenders = [renderDataRow, renderGroupRow, renderSummaryRow];

        function renderGridData(columns: DataColumn[], area: 'left' | 'primary' | 'right') {
            // const cellPositionMap.value = calculateCellPositionInRow(columns);
            return visibleDatas.value.map((visualDataRow: VisualData) => {
                return visualDataRowRenders[visualDataRow.type](visualDataRow, area, columns);
            });
        }

        function renderMergedGridData(columns: DataColumn[], area: string) {
            // const cellPositionMap.value = calculateCellPositionInRow(columns);
            return visibleDatas.value.map((visualDataRow: VisualData) => {
                return Object.values(visualDataRow.data)
                    .filter((cell) => cellPositionMap.value[area]?.[cell.field] && cell.rowSpan > 1)
                    .map((cell) => {
                        return (
                            <div
                                key={cellKey(visualDataRow, cell.index)}
                                class="fv-grid-cell fv-grid-merged-cell"
                                style={rowSpanCellPosition(visualDataRow, cell, cellPositionMap.value[area])}>
                                {cell.data}
                            </div>
                        );
                    });
            });
        }

        function renderLeftFixedGrid() {
            return (
                <div ref={leftFixedGridContentRef} class="fv-grid-content-left-fixed">
                    <div class="fv-grid-data" style={leftFixedGridDataStyle.value}>
                        {renderGridData(columnContext.value.leftColumns.filter((column: DataColumn) => column.visible), 'left')}
                    </div>
                    <div class="fv-grid-merge-date" style={leftFixedGridMergedDataStyle.value}>
                        {renderMergedGridData(columnContext.value.leftColumns.filter((column: DataColumn) => column.visible), 'left')}
                    </div>
                </div>
            );
        }

        function renderPrimaryFixedDataArea() {
            return (
                <div ref={primaryGridContentRef} class="fv-grid-content-primary">
                    <div class="fv-grid-data" style={gridDataStyle.value}>
                        {renderGridData(columnContext.value.primaryColumns.filter((column: DataColumn) => column.visible), 'primary')}
                    </div>
                    <div class="fv-grid-merge-date" style={gridMergedDataStyle.value}>
                        {renderMergedGridData(columnContext.value.primaryColumns.filter((column: DataColumn) => column.visible), 'primary')}
                    </div>
                </div>
            );
        }

        function renderRightFixedGrid() {
            return (
                <div ref={rightFixedGridContentRef} class="fv-grid-content-right-fixed">
                    <div class="fv-grid-data" style={rightFixedGridDataStyle.value}>
                        {renderGridData(columnContext.value.rightColumns.filter((column: DataColumn) => column.visible), 'right')}
                    </div>
                    <div class="fv-grid-merge-date" style={rightFixedGridMergedDataStyle.value}>
                        {renderMergedGridData(columnContext.value.rightColumns.filter((column: DataColumn) => column.visible), 'right')}
                    </div>
                </div>
            );
        }

        function renderDataArea() {
            const renderResult: any[] = [];
            if (hasLeftFixedColumn.value) {
                renderResult.push(renderLeftFixedGrid());
            }
            renderResult.push(renderPrimaryFixedDataArea());
            if (hasRightFixedColumn.value) {
                renderResult.push(renderRightFixedGrid());
            }
            return renderResult;
        }

        // return { renderDataArea };
        return () => renderDataArea();
    }
});
