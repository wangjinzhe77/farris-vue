import { ExtractPropTypes } from "vue";

export const dataGridAreaProps = {
    cellPositionMap: { type: Object, default: {}}
} as Record<string, any>;

export type DataGridAreaProps = ExtractPropTypes<typeof dataGridAreaProps>;
