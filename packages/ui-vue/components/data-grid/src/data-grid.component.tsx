/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DataGridProps, dataGridProps, PaginatonOptions } from './data-grid.props';
import { computed, defineComponent, onUnmounted, onMounted, ref, nextTick, watch, SetupContext, inject, provide } from 'vue';
import { useResizeObserver } from '@vueuse/core';
import getDataArea from './components/data/data-area.component';
import {
    DataColumn, DataViewOptions, VisualData, getColumnHeader, getDisableMask, getFilterPanel, getGroupPanel, getHorizontalScrollbar,
    getPagination, getSidebar, getSummary, getVerticalScrollbar, useCellPosition, useColumn, useColumnFilter,
    useCommandColumn, useDataView, useDataViewContainerStyle, useDragColumn, useEdit, useFilter, useFilterHistory, useFitColumn,
    useGroupColumn, useGroupData, useHierarchy, useIdentify, useNavigation, useRow, useSelection, useSidebar, useSort, useVirtualScroll,
    useVisualData, useVisualDataBound, useVisualDataCell, useVisualDataRow, useVisualGroupRow, useVisualSummaryRow,
    useCellContentStyle, useLoading, getEmpty, usePagination
} from '@farris/ui-vue/components/data-view';
import './data-grid.css';

export default defineComponent({
    name: 'FDataGrid',
    props: dataGridProps,
    emits: ['changed', 'clickRow', 'doubleClickRow', 'selectionChange', 'enterUpInLastCell', 'pageIndexChanged', 'pageSizeChanged', 'beginEditCell', 'endEditCell'],
    setup(props: DataGridProps, context) {
        const preloadCount = 0;
        const rowHeight = props.rowOption?.height || 28;
        const defaultVisibleCapacity = ref(20);
        const columns = ref(props.columns);
        const useIdentifyComposition = useIdentify(props as DataViewOptions);
        const dataGridRef = ref<any>();
        const gridContentRef = ref<any>();
        const primaryGridContentRef = ref<any>();
        const leftFixedGridContentRef = ref<any>();
        const rightFixedGridContentRef = ref<any>();
        const visibleDatas = ref<VisualData[]>([]);
        const mouseInContent = ref(false);
        const wrapContent = ref(props.rowOption?.wrapContent || false);
        const useGroupDataComposition = useGroupData(props as DataViewOptions, useIdentifyComposition);
        const useFilterComposition = useFilter();
        const useHierarchyComposition = useHierarchy(props as DataViewOptions);
        const { showLoading, renderLoading } = useLoading(props, dataGridRef);
        const dataView = useDataView(props as DataViewOptions, new Map(), useFilterComposition, useHierarchyComposition, useIdentifyComposition);
        const usePaginationComposition = usePagination(props, dataView);
        const { shouldRenderPagination } = usePaginationComposition;
        const useSelectionComposition = useSelection(props as DataViewOptions, dataView, useIdentifyComposition, visibleDatas, context as SetupContext);
        const isDisabled = computed(() => props.disabled);
        const visibleCapacity = computed(() => {
            return Math.min(dataView.dataView.value.length, defaultVisibleCapacity.value);
        });
        const loadingMessage = computed(() => props.loadingConfig.message);

        const { containerStyleObject } = useDataViewContainerStyle(props as DataViewOptions);

        const useCommandColumnComposition = useCommandColumn(props as DataViewOptions);
        const { applyCommands } = useCommandColumnComposition;
        applyCommands(columns);

        const useColumnComposition = useColumn(props as DataViewOptions);
        const useSortComposition = useSort(props as DataViewOptions);
        const { applyColumnSorter, columnContext, updateColumnRenderContext } = useColumnComposition;
        applyColumnSorter(dataView, useSortComposition);

        const useGroupColumnComposition = useGroupColumn(props as DataViewOptions, columnContext);
        const useRowComposition = useRow(props as DataViewOptions, context as SetupContext, useSelectionComposition, useIdentifyComposition);
        const useEditComposition = useEdit(props as DataViewOptions, context as SetupContext, useIdentifyComposition, useRowComposition);

        const useVisualDataBoundComposition = useVisualDataBound(props as DataViewOptions);

        const useVisualDataCellComposition = useVisualDataCell(props as DataViewOptions, useEditComposition, useVisualDataBoundComposition);

        const useVisualDataRowComposition = useVisualDataRow(
            props as DataViewOptions,
            useEditComposition,
            useHierarchyComposition,
            useIdentifyComposition,
            useVisualDataBoundComposition,
            useVisualDataCellComposition
        );

        const useVisualGroupRowComposition = useVisualGroupRow(props as DataViewOptions, useIdentifyComposition, useVisualDataCellComposition, useVisualDataRowComposition);

        const useVisualSummaryRowComposition = useVisualSummaryRow(props as DataViewOptions, useIdentifyComposition, useVisualDataCellComposition, useVisualDataRowComposition);

        const useVisualDataComposition = useVisualData(
            props as DataViewOptions,
            columns,
            dataView,
            visibleCapacity,
            preloadCount,
            useVisualDataRowComposition,
            useVisualGroupRowComposition,
            useVisualSummaryRowComposition
        );
        const { getVisualData } = useVisualDataComposition;

        const useCellContentStyleComposition = useCellContentStyle(columnContext);
        const useCellPositionComposition = useCellPosition(props as DataViewOptions, columnContext);
        const useSidebarComposition = useSidebar(props as DataViewOptions, useSelectionComposition);
        const { sidebarWidth } = useSidebarComposition;

        const useVirtualScrollComposition = useVirtualScroll(
            props as DataViewOptions,
            dataView,
            visibleDatas,
            columnContext,
            useVisualDataComposition,
            visibleCapacity,
            preloadCount,
            sidebarWidth
        );
        const { onWheel, dataGridWidth, viewPortHeight, viewPortWidth, resetScroll, updateVisibleRowsOnLatestVisibleScope } = useVirtualScrollComposition;

        const useFitColumnComposition = useFitColumn(props as DataViewOptions, columnContext, gridContentRef, viewPortWidth, useGroupColumnComposition);
        const { calculateColumnsSize } = useFitColumnComposition;

        const useFilterHistoryComposition = useFilterHistory();

        const useColumnFilterComposition = useColumnFilter(
            gridContentRef,
            rightFixedGridContentRef,
            dataView,
            useFilterHistoryComposition,
            useVirtualScrollComposition
        );


        const useDragColumnComposition = useDragColumn(
            props as DataViewOptions,
            context as SetupContext,
            useColumnComposition,
            dataView,
            useGroupColumnComposition,
            useGroupDataComposition,
            useVirtualScrollComposition
        );

        const gridClass = computed(() => {
            const classObject = {
                'fv-grid': true,
                'fv-grid-bordered': props.showBorder,
                'fv-grid-horizontal-bordered': props.showHorizontalLines,
                'fv-datagrid-strip': props.showStripe,
            } as Record<string, boolean>;
            return classObject;
        });

        const gridContentClass = computed(() => {
            const classObject = {
                'fv-grid-content': true,
                'fv-grid-content-hover': mouseInContent.value,
                'fv-grid-wrap-content': wrapContent.value
            } as Record<string, boolean>;
            return classObject;
        });

        const { renderGridHeader, renderGridColumnResizeOverlay, shouldShowHeader } = getColumnHeader(
            props as DataViewOptions,
            context as SetupContext,
            gridContentRef,
            leftFixedGridContentRef,
            rightFixedGridContentRef,
            useColumnComposition,
            dataView,
            useDragColumnComposition,
            useColumnFilterComposition,
            useFilterComposition,
            useFilterHistoryComposition,
            useFitColumnComposition,
            useGroupColumnComposition,
            useSelectionComposition,
            useSidebarComposition,
            useSortComposition,
            useVirtualScrollComposition,
            viewPortWidth,
            visibleDatas
        );

        const { renderDataGridPagination } = getPagination(props as DataViewOptions, context as SetupContext, dataView,
            useVirtualScrollComposition, usePaginationComposition);

        const { renderDataGridSidebar } = getSidebar(props as DataViewOptions, useRowComposition, useSelectionComposition, useSidebarComposition, useVirtualScrollComposition);

        const { renderDisableMask } = getDisableMask();

        const { renderDataGridSummery } = getSummary(props as DataViewOptions, dataView, useColumnComposition);

        const { renderHorizontalScrollbar } = getHorizontalScrollbar(props as DataViewOptions, gridContentRef, useVirtualScrollComposition);

        const { renderVerticalScrollbar } = getVerticalScrollbar(props as DataViewOptions, gridContentRef, useVirtualScrollComposition);

        const { renderFilterPanel } = getFilterPanel(
            props as DataViewOptions,
            useColumnComposition,
            dataView,
            useFilterComposition,
            useVirtualScrollComposition
        );

        const { renderGroupPanel } = getGroupPanel(
            props as DataViewOptions,
            dataView,
            useDragColumnComposition,
            useGroupDataComposition,
            useVirtualScrollComposition
        );

        function updateGridViewPortWidth($event: UIEvent) {
            viewPortHeight.value = primaryGridContentRef.value?.clientHeight || 0;
            dataGridWidth.value = gridContentRef.value?.clientWidth || 0;
            calculateColumnsSize();
        }

        function onGridContentResize() {
            const newVisibleCapacity = Math.ceil(gridContentRef.value.clientHeight / rowHeight);
            if (newVisibleCapacity > defaultVisibleCapacity.value) {
                defaultVisibleCapacity.value = newVisibleCapacity;
                updateVisibleRowsOnLatestVisibleScope();
            }
            viewPortHeight.value = primaryGridContentRef.value?.clientHeight || 0;
            dataGridWidth.value = gridContentRef.value?.clientWidth || 0;
            calculateColumnsSize();
        }

        onMounted(() => {
            if (gridContentRef.value) {
                defaultVisibleCapacity.value = Math.max(Math.ceil(gridContentRef.value.clientHeight / rowHeight), defaultVisibleCapacity.value);
                visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
                useResizeObserver(gridContentRef.value, onGridContentResize);
                calculateColumnsSize();
                nextTick(() => {
                    if (gridContentRef.value) {
                        dataGridWidth.value = gridContentRef.value.clientWidth;
                    }
                    if (primaryGridContentRef.value) {
                        viewPortWidth.value = primaryGridContentRef.value.clientWidth;
                        viewPortHeight.value = primaryGridContentRef.value.clientHeight;
                    }
                });
            }
            if (showLoading.value) {
                renderLoading();
            }
        });

        onUnmounted(() => {
            // window.removeEventListener('resize', updateGridViewPortWidth);
        });
        // 此处先注释掉该监听，该监听回调会频繁触发导致浏览器内存溢出问题
        // todo: 在合适的时机计算表格尺寸
        watch(viewPortWidth, () => {
            // if (gridContentRef.value) {
            //     calculateColumnsSize();
            // }
        });

        function addNewDataItem() {
            dataView.insertNewDataItem();
            resetScroll();
        }

        function addNewDataItemAtLast() {
            dataView.insertNewDataItem(visibleDatas.value.length);
            resetScroll();
        }

        function editDataItem(visualDataRow: VisualData) {
            useEditComposition.onEditingRow(visualDataRow);
        }

        function removeDataItem(dataIndex: number) {
            dataView.removeDataItem(dataIndex);
            resetScroll();
        }

        function removeDataItemById(dataItemId: string) {
            dataView.removeDataItemById(dataItemId);
            resetScroll();
        }

        function acceptDataItem(visualDataRow: VisualData) {
            useEditComposition.acceptEditingRow(visualDataRow);
        }

        function cancelDataItem(visualDataRow: VisualData) {
            useEditComposition.cancelEditingRow(visualDataRow);
        }

        function updateColumns(newColumns: DataColumn[]) {
            if (newColumns) {
                columns.value = newColumns;
                applyCommands(columns);
                updateColumnRenderContext(columns.value);
                applyColumnSorter(dataView, useSortComposition);
                calculateColumnsSize();
            }
        }

        function updateDataSource(newData: Record<string, any>[]) {
            if (newData) {
                // 重新加载数据时，重置滚动条位置
                resetScroll();
                dataView.load(newData);
                visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);
            }
        }

        function updatePagination(pageOptions: Partial<PaginatonOptions>) {
            usePaginationComposition.updatePagination(pageOptions);
        }

        // 监听传入数据变化，更新视图，此处可能存在性能问题，需要跟changePolicy属性配合使用
        watch(() => props.data, (newDataValue) => {
            if (props.changePolicy === 'default') {
                updateDataSource(newDataValue);
            }
        }, {
            deep: true
        });

        watch(() => props.columns, (latestColumns: DataColumn[]) => {
            updateColumns(latestColumns);
        });

        function selectItemById(dataItemId: string) {
            useSelectionComposition.selectItemById(dataItemId);
        }

        function clickRowItemById(dataItemId: string) {
            const visibleItemToBeSelected = visibleDatas.value.find((visibleData: VisualData) => {
                return visibleData.raw[useIdentifyComposition.idField.value] === dataItemId;
            }) as VisualData;
            if (visibleItemToBeSelected) {
                useRowComposition.clickRowItem(visibleItemToBeSelected);
            }
        }

        function selectItemByIds(dataItemIds: string[]) {
            useSelectionComposition.selectItemByIds(dataItemIds);
        }

        function getVisibleData(): VisualData[] {
            return visibleDatas.value;
        }

        function getVisibleDataByIds(ids: string[]): VisualData[] {
            return visibleDatas.value.filter((visibleData: VisualData) => ids.includes(visibleData.raw[props.idField]));
        }
        function getSelectedItems() {
            return useSelectionComposition.getSelectedItems();
        }
        function getSelectionRow() {
            return useSelectionComposition.getSelectionRow();
        }

        function endEditCell() {
            useEditComposition.endEditCell();
        }

        const dataGridComponentInstance = {
            addNewDataItem, addNewDataItemAtLast, removeDataItem, removeDataItemById, editDataItem,
            acceptDataItem, cancelDataItem, selectItemById, selectItemByIds, updateColumns, updateDataSource,
            updatePagination, getVisibleData, getVisibleDataByIds, getSelectedItems, getSelectionRow,
            endEditCell, clickRowItemById
        };

        context.expose(dataGridComponentInstance);

        const useNavigationComposition = useNavigation(props as DataViewOptions, context as SetupContext, dataGridComponentInstance, useColumnComposition, useEditComposition, visibleDatas);

        const { renderDataArea } = getDataArea(
            props,
            context as SetupContext,
            primaryGridContentRef,
            leftFixedGridContentRef,
            rightFixedGridContentRef,
            useCellContentStyleComposition,
            useCellPositionComposition,
            useColumnComposition,
            useEditComposition,
            useGroupDataComposition,
            useNavigationComposition,
            useRowComposition,
            useVisualDataComposition,
            useVirtualScrollComposition,
            visibleDatas
        );

        const shouldRenderEmptyContent = computed(() => !visibleDatas.value || !visibleDatas.value.length);
        // 渲染空数据提示
        const { renderEmpty } = getEmpty(context as SetupContext);

        return () => {
            return (
                <div ref={dataGridRef} class={gridClass.value} style={containerStyleObject.value} onWheel={onWheel}>
                    {gridContentRef.value && renderFilterPanel()}
                    {gridContentRef.value && renderGroupPanel()}
                    {gridContentRef.value && shouldShowHeader.value && renderGridHeader()}
                    <div ref={gridContentRef} class={gridContentClass.value}
                        onMouseover={() => mouseInContent.value = true}
                        onMouseleave={() => mouseInContent.value = false}>
                        {gridContentRef.value && renderDataGridSidebar(visibleDatas)}
                        {gridContentRef.value && renderDataArea()}
                        {gridContentRef.value && shouldRenderEmptyContent.value && renderEmpty()}
                        {gridContentRef.value && renderHorizontalScrollbar()}
                        {gridContentRef.value && renderVerticalScrollbar()}
                    </div>
                    {gridContentRef.value && renderDataGridSummery()}
                    {shouldRenderPagination.value && renderDataGridPagination()}
                    {renderGridColumnResizeOverlay()}
                    {isDisabled.value && renderDisableMask()}
                </div>
            );
        };
    }
});
