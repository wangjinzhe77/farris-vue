/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, ref } from 'vue';
import { HeaderCell, UseColumn, UseVirtualScroll, DataViewOptions } from '../../../data-view/composition/types';
import FDesignerItem from '../../../designer-canvas/src/components/designer-item.component';
import { DesignerItemContext } from '../../../designer-canvas';
export default function (
    designItemContext: DesignerItemContext,
    props: DataViewOptions,
    useColumnComposition: UseColumn,
    useVirtualScrollComposition: UseVirtualScroll,
    columnType = 'data-grid-column'
) {
    const { columnContext, hasLeftFixedColumn, hasRightFixedColumn } = useColumnComposition;
    const { gridHeaderColumnsStyle, leftFixedGridHeaderColumnsStyle, rightFixedGridHeaderColumnsStyle } = useVirtualScrollComposition;
    const defaultColumnHeight = columnContext.value.defaultColumnWidth;
    const gridHeaderCellHeight = 32;
    const headerCellPositionMap = new Map<number, number>([[0, 0]]);

    function headerCellPosition(headerCell: HeaderCell, columnIndex: number): Record<string, any> {
        const headerCellPosition = headerCellPositionMap.get(columnIndex) || 0;
        const styleObject = {
            height: `${headerCell.depth * gridHeaderCellHeight}px`,
            left: `${headerCell.left}px`,
            top: `${(headerCell.layer - 1) * gridHeaderCellHeight}px`,
            width: `${headerCell.actualWidth}px`,
            padding: 0
        } as Record<string, any>;
        if (columnContext.value.headerDepth > 1) {
            styleObject['line-height'] = `${headerCell.depth * gridHeaderCellHeight}px`;
        }
        headerCellPositionMap.set(columnIndex + 1, headerCellPosition + (headerCell.actualWidth || defaultColumnHeight));
        return styleObject;
    }

    const headerRowClass = computed(() => {
        const classObject = {
            'fv-grid-header': true,
            'fv-grid-header-group-columns': columnContext.value.headerDepth > 1
        } as Record<string, boolean>;
        return classObject;
    });

    const headerRowStyle = computed(() => {
        const styleObject = {
            height: `${columnContext.value.headerDepth * gridHeaderCellHeight}px`
        } as Record<string, any>;
        return styleObject;
    });

    const shouldShowGridSettings = function (headerCell: HeaderCell) {
        return props.showSetting && !!headerCell.showSetting;
    };

    function renderGridSettingsIcon() {
        return (
            <span class="fv-grid-settings-icon d-flex align-items-center">
                <i class="f-icon f-icon-home-setup"></i>
            </span>
        );
    }

    function onSelectionChange(contentType, schema, componentId, componentInstance) {
        designItemContext?.setupContext?.emit('selectionChange', contentType, schema, componentId, componentInstance);
    }
    function getHeaderClass(headerCell: HeaderCell) {
        const direction = headerCell.column?.halign === 'center' ? 'center' : headerCell.column?.halign === 'right' ? 'end pr-2' : 'start pl-2';
        return 'w-100 d-flex align-items-center justify-content-' + direction;
    }
    /**
     * 表格方法精简，不能拖拽，去掉拖拽图标
     * @param headerCell 
     * @param headerCells 
     * @param columnIndex 
     * @returns 
     */
    function renderGridHeaderCell(headerCell: HeaderCell, headerCells: HeaderCell[], columnIndex: number) {
        const columnSchema = ref(headerCell.column);
        return (
            <div
                class="fv-grid-header-cell"
                style={headerCellPosition(headerCell, columnIndex)}>
                <FDesignerItem v-model={columnSchema.value} customClass={getHeaderClass(headerCell)}
                    canAdd={false}
                    canDelete={false}
                    canMove={false}
                    key={columnSchema.value?.id}
                    type={columnType}
                    componentId={props['componentId']}
                    onSelectionChange={onSelectionChange}>
                </FDesignerItem>
                {shouldShowGridSettings(headerCell) && renderGridSettingsIcon()}
                {headerCell.resizable && <span class="fv-column-resize-bar"></span>}
            </div>
        );
    }

    function renderGridHeaderColumns(headerCells: HeaderCell[]): any[] {
        return headerCells.map((headerCell: HeaderCell, columnIndex: number) => {
            const headerCellsNodes: any[] = [];
            headerCellsNodes.push(renderGridHeaderCell(headerCell, headerCells, columnIndex));
            if (headerCell.children && headerCell.children.length) {
                const subHeaderCellNodes = renderGridHeaderColumns(headerCell.children);
                headerCellsNodes.push(...subHeaderCellNodes);
            }
            return headerCellsNodes;
        });
    }

    function renderGridHeaderLeftFixed() {
        return (
            <div class="fv-grid-header-left-fixed">
                <div class="fv-grid-header-columns" style={leftFixedGridHeaderColumnsStyle.value}>
                    {renderGridHeaderColumns(columnContext.value.leftHeaderColumns)}
                </div>
            </div>
        );
    }

    function renderGridHeaderRigthFixed() {
        return (
            <div class="fv-grid-header-right-fixed">
                <div class="fv-grid-header-columns" style={rightFixedGridHeaderColumnsStyle.value}>
                    {renderGridHeaderColumns(columnContext.value.rightHeaderColumns)}
                </div>
            </div>
        );
    }

    function renderGridHeader() {
        return (
            <div class={headerRowClass.value} style={headerRowStyle.value}>
                {hasLeftFixedColumn.value && renderGridHeaderLeftFixed()}
                <div class="fv-grid-header-primary">
                    <div class="fv-grid-header-columns" style={gridHeaderColumnsStyle.value}>
                        {renderGridHeaderColumns(columnContext.value.primaryHeaderColumns)}
                    </div>
                </div>
                {hasRightFixedColumn.value && renderGridHeaderRigthFixed()}
            </div>
        );
    }

    return { renderGridHeader };
}
