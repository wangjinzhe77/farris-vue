 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../../dynamic-resolver';
import { schemaMapper } from '../schema/schema-mapper';
import { schemaResolver } from "../schema/schema-resolver";
import gridFieldEditorSchema from '../schema/grid-field-editor.schema.json';
export const gridFieldEditorProps = {
    modelValue: { type: String, default: '' },
    /** 组件唯一标识 */
    viewModelId: { Type: String, default: '' },
    /** 自定义样式 */
    gridData: { Type: Object, default: null },
    gridType: { Type: String, default: 'data-grid-column' },
    /** 弹出选择窗口前，获取最新的表格数据 */
    getLatestGridData: { Type: Function }
} as Record<string, any>;

export type GridFieldEditorProps = ExtractPropTypes<typeof gridFieldEditorProps>;
export const gridFieldEditorPropsResolver = createPropsResolver<GridFieldEditorProps>(gridFieldEditorProps, gridFieldEditorSchema, schemaMapper, schemaResolver);

