import { SetupContext, defineComponent, inject, onMounted, ref } from 'vue';
import { dataGridColumnProps, DataGridColumnProps } from '../data-grid-column.props';
import { DesignerItemContext } from '../../../designer-canvas';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { useDesignerRulesForDataGridColumn } from './use-column-rules';

export default defineComponent({
    name: 'FDataGridColumnDesign',
    props: dataGridColumnProps,
    emits: [],
    setup(props: DataGridColumnProps, context) {
        const columnRef = ref<any>();
        const designerHostService=inject('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRulesForDataGridColumn(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(columnRef, designItemContext,designerRulesComposition);
        onMounted(() => {
            columnRef.value.componentInstance = componentInstance;
        });
        context.expose(componentInstance.value);
        /**
         * 此处可能抛出tree-grid-column或者data-grid-column
         * @param event 
         */
        function clickHandler(event) {
            designItemContext?.setupContext?.emit('selectionChange', componentInstance.value.schema, designItemContext.schema, props.componentId, componentInstance.value);
        }
        return () => {
            return <span class="fv-column-title" ref={columnRef} onClick={(event) => clickHandler(event)}>{props.title}</span>;
        };
    }
});
