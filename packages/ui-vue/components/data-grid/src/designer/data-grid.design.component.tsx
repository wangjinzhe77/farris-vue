/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DataGridDesignProps, dataGridDesignProps } from '../data-grid.props';
import { computed, defineComponent, onMounted, ref, nextTick, watch, SetupContext, inject } from 'vue';
import { useResizeObserver } from '@vueuse/core';
import { useDesignerFitColumn } from '../../../data-view/designer';
import {
    DataViewOptions, VisualData, getHorizontalScrollbar, useColumn,
    useCommandColumn, useDataView, useDataViewContainerStyle, useEdit, useHierarchy, useIdentify, useSelection,
    useSidebar, useSort, useVirtualScroll, useVisualData, useVisualDataBound, useVisualDataCell,
    useVisualDataRow, useVisualGroupRow, useVisualSummaryRow, usePagination, useGroupColumn,
    useFilter, DataColumn, useRow
} from '../../../data-view';
import getColumnHeader from './column-header.design.component';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import { useDesignerRulesForDataGrid } from './use-designer-rules';
import { DesignerHostService } from '../../../designer-canvas/src/composition/types';
import { canvasChanged } from '../../../designer-canvas/src/composition/designer-canvas-changed';

export default defineComponent({
    name: 'FDataGridDesign',
    props: dataGridDesignProps,
    emits: ['ClickRow', 'DoubleClickRow', 'selectionChange', 'enterUpInLastCell', 'pageIndexChanged', 'pageSizeChanged'],
    setup(props: DataGridDesignProps, context) {
        const dataGridRef = ref<any>();
        const designerHostService = inject<DesignerHostService>('designer-host-service');
        const designItemContext = inject<DesignerItemContext>('design-item-context') as DesignerItemContext;
        const designerRulesComposition = useDesignerRulesForDataGrid(designItemContext, designerHostService);
        const componentInstance = useDesignerComponent(dataGridRef, designItemContext, designerRulesComposition);
        componentInstance.value.styles = 'display:flex;flex:1;';
        onMounted(() => {
            dataGridRef.value.componentInstance = componentInstance;
        });

        context.expose(componentInstance.value);
        const columns = ref(props.columns);
        const defaultVisibleCapacity = 20;
        const useIndentifyComposition = useIdentify(props as DataViewOptions);
        const preloadCount = 0;
        const gridContentRef = ref<any>();
        const primaryGridContentRef = ref<any>();
        const showStripe = ref(props.showStripe);
        const visibleDatas = ref<VisualData[]>([]);
        const mouseInContent = ref(false);
        const wrapContent = ref(props.rowOption?.wrapContent || false);
        const useFilterComposition = useFilter();
        const useHierarchyComposition = useHierarchy(props as DataViewOptions);
        const dataView = useDataView(props as DataViewOptions, new Map(), useFilterComposition, useHierarchyComposition, useIndentifyComposition);
        const useSelectionComposition = useSelection(props as DataViewOptions, dataView, useIndentifyComposition, visibleDatas, context as SetupContext);
        const visibleCapacity = computed(() => {
            return dataView.totalItems.value < defaultVisibleCapacity ? dataView.totalItems.value : defaultVisibleCapacity;
        });

        const { containerStyleObject } = useDataViewContainerStyle(props as DataViewOptions);

        const useCommandColumnComposition = useCommandColumn(props as DataViewOptions);
        const { applyCommands } = useCommandColumnComposition;
        applyCommands(columns);

        const useColumnComposition = useColumn(props as DataViewOptions);
        const useSortComposition = useSort(props as DataViewOptions);
        const { applyColumnSorter, columnContext, updateColumnRenderContext } = useColumnComposition;
        applyColumnSorter(dataView, useSortComposition);

        const useGroupColumnComposition = useGroupColumn(props as DataViewOptions, columnContext);
        const useRowComposition = useRow(props as DataViewOptions, context as SetupContext, useSelectionComposition, useIndentifyComposition);
        const useEditComposition = useEdit(props as DataViewOptions, context as SetupContext, useIndentifyComposition, useRowComposition);

        const useVisualDataBoundComposition = useVisualDataBound(props as DataViewOptions);

        const useVisualDataCellComposition = useVisualDataCell(props as DataViewOptions, useEditComposition, useVisualDataBoundComposition);

        const useVisualDataRowComposition = useVisualDataRow(
            props as DataViewOptions,
            useEditComposition,
            useHierarchyComposition,
            useIndentifyComposition,
            useVisualDataBoundComposition,
            useVisualDataCellComposition
        );

        const useVisualGroupRowComposition = useVisualGroupRow(props as DataViewOptions, useIndentifyComposition, useVisualDataCellComposition, useVisualDataRowComposition);

        const useVisualSummaryRowComposition = useVisualSummaryRow(props as DataViewOptions, useIndentifyComposition, useVisualDataCellComposition, useVisualDataRowComposition);

        const useVisualDataComposition = useVisualData(
            props as DataViewOptions,
            columns,
            dataView,
            visibleCapacity,
            preloadCount,
            useVisualDataRowComposition,
            useVisualGroupRowComposition,
            useVisualSummaryRowComposition
        );
        const { getVisualData } = useVisualDataComposition;

        const useSidebarComposition = useSidebar(props as DataViewOptions, useSelectionComposition);
        const { sidebarWidth } = useSidebarComposition;

        visibleDatas.value = getVisualData(0, visibleCapacity.value + preloadCount - 1);

        const useVirtualScrollComposition = useVirtualScroll(
            props as DataViewOptions,
            dataView,
            visibleDatas,
            columnContext,
            useVisualDataComposition,
            visibleCapacity,
            preloadCount,
            sidebarWidth
        );
        const { onWheel, dataGridWidth, viewPortHeight, viewPortWidth } = useVirtualScrollComposition;

        const useFitColumnComposition = useDesignerFitColumn(props as DataViewOptions, columnContext, gridContentRef, viewPortWidth, useGroupColumnComposition);
        const { calculateColumnsSize } = useFitColumnComposition;

        const gridClass = computed(() => {
            const classObject = {
                'fv-grid': true,
                'fv-datagrid-strip': showStripe.value
            } as Record<string, boolean>;
            return classObject;
        });

        const gridContentClass = computed(() => {
            const classObject = {
                'fv-grid-content': true,
                'fv-grid-content-hover': mouseInContent.value,
                'fv-grid-wrap-content': wrapContent.value
            } as Record<string, boolean>;
            return classObject;
        });

        const { renderGridHeader } = getColumnHeader(
            designItemContext as DesignerItemContext,
            props as DataViewOptions,
            useColumnComposition,
            useVirtualScrollComposition
        );

        const { renderHorizontalScrollbar } = getHorizontalScrollbar(props as DataViewOptions, gridContentRef, useVirtualScrollComposition);

        function onGridContentResize() {
            viewPortHeight.value = primaryGridContentRef.value?.clientHeight || 0;
            dataGridWidth.value = gridContentRef.value?.clientWidth || 0;
            calculateColumnsSize();
        }

        onMounted(() => {
            if (gridContentRef.value) {
                useResizeObserver(gridContentRef.value, onGridContentResize);
                calculateColumnsSize();
                nextTick(() => {
                    if (gridContentRef.value) {
                        dataGridWidth.value = gridContentRef.value.clientWidth;
                    }
                    if (primaryGridContentRef.value) {
                        viewPortWidth.value = primaryGridContentRef.value.clientWidth;
                        viewPortHeight.value = primaryGridContentRef.value.clientHeight;
                    }
                });
            }
        });
        function updateColumns(newColumns: DataColumn[]) {
            if (newColumns) {
                columns.value = newColumns;
                applyCommands(columns);
                updateColumnRenderContext(columns.value);
                applyColumnSorter(dataView, useSortComposition);
                calculateColumnsSize();
            }
        }
        // 此处会更新columns  不能移除
        watch(() => props.columns, (newDatas) => {
            updateColumns(newDatas);

            // 触发更新实体树
            canvasChanged.value++;
        });
        watch(viewPortWidth, () => {
            if (gridContentRef.value) {
                calculateColumnsSize();
            }
        });

        return () => {
            return (
                <div ref={dataGridRef} class={gridClass.value} style={containerStyleObject.value} onWheel={onWheel}>
                    {renderGridHeader()}
                    <div
                        ref={gridContentRef}
                        class={gridContentClass.value}
                        onMouseover={() => {
                            mouseInContent.value = true;
                        }}
                        onMouseleave={() => {
                            mouseInContent.value = false;
                        }}>
                        {renderHorizontalScrollbar()}
                    </div>
                </div>
            );
        };
    }
});
