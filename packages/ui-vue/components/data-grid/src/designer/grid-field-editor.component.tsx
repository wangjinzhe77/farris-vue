/**
* Copyright (c) 2020 - present, Inspur Genersoft selectItem., Ltd.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

import { defineComponent, onMounted, ref, inject, watch } from 'vue';
import FButtonEdit from '../../../button-edit/src/button-edit.component';
import { gridFieldEditorProps, GridFieldEditorProps } from './grid-field-editor.props';
import FTransfer from '../../../transfer/src/transfer.component';
import { FormSchemaEntityField$Type } from '@farris/ui-vue/components/common';

export default defineComponent({
    name: 'FGridFieldEditor',
    props: gridFieldEditorProps,
    emits: ['change'] as (string[] & ThisType<void>),
    setup(props: GridFieldEditorProps, context) {

        const viewModelId = ref(props.viewModelId);
        const designViewModelUtils = inject('designViewModelUtils') as any;
        const formSchemaUtils = inject('useFormSchema') as any;
        const controlCreatorUtils = inject('controlCreatorUtils') as any;
        let fieldColumns = props.gridData['columns'];
        const textValue = ref(`共 ${(fieldColumns || []).length} 项`);
        // 右侧选中
        const selectPanels = ref<any[]>([]);
        // 左侧数据
        const dataSource = ref<any[]>([]);
        // 打平关联字段后的列数据
        let plainColumns = [] as any;
        const rowOptions = {
            customRowStatus: (visualData: any) => {
                const rawData = visualData['raw'];
                visualData['disabled'] = rawData.type && rawData.type.fields && rawData.type.fields.length > 0 ? true : visualData['disabled'];
            }
        };
        /**
        * 获取已选择的列
        */
        function getSelectPanels() {
            selectPanels.value = [];
            if (!fieldColumns || fieldColumns.length === 0) {
                return [];
            }
            fieldColumns.forEach(fieldColumn => {
                if (fieldColumn.binding && fieldColumn.binding.field) {
                    const element = plainColumns.find(el => el.data.id === fieldColumn.binding.field);
                    if (element) {
                        // if (element.data.$type === FormSchemaEntityField$Type.ComplexField) {
                        //     selectPanels.push(Object.assign({ isInValidBinding: true, inValidTip: invalidTip }, element.data));
                        // }
                        selectPanels.value.push(Object.assign({}, element.data));
                    } else {
                        const tip = '绑定字段不存在，请手动移除列';
                        selectPanels.value.push({
                            id: fieldColumn.binding.field,
                            name: fieldColumn.caption,
                            bindingField: null,
                            isInValidBinding: true,
                            inValidTip: tip
                        });
                    }
                }
            });
            textValue.value = `共 ${selectPanels.value.length} 项`;
        }
        function convertTreeNodes2PlainObject(nodes: any[], r: any[] = []): any[] {
            if (nodes) {
                nodes.forEach(n => {
                    r.push(n);
                    if (n.children) {
                        convertTreeNodes2PlainObject(n.children, r);
                    }
                });
            }
            return r;
        }

        /**
         * 更新dataSource
         */
        function resetDataSource() {
            dataSource.value = designViewModelUtils['getAllFields2TreeByVMId'](viewModelId.value);
            plainColumns = convertTreeNodes2PlainObject(dataSource.value);
        }

        function clickConfirm() {
            const dgViewModel = designViewModelUtils.getDgViewModel(viewModelId.value);
            const newFields = [] as any;
            const newFieldIds = [] as any;
            // 组装GridField数据
            selectPanels.value.forEach(selectItem => {
                delete selectItem.inValidTip;
                delete selectItem.isInValidBinding;
                const foundColumn = fieldColumns?.find(item => {
                    if (item.binding && item.binding.field) {
                        return item.binding.field === selectItem.id;
                    }
                });
                if (!foundColumn) {
                    const gridFieldMetadata = controlCreatorUtils.setGridFieldProperty(props.gridType, selectItem.data, null, true);
                    dgViewModel.addField(selectItem); // DgVM增加字段
                    newFields.push(gridFieldMetadata);
                } else {
                    newFields.push(foundColumn);
                }
                newFieldIds.push(selectItem.id);
            });

            // DgVM 删除字段
            const deletedFields = dgViewModel.fields.filter(f => !newFieldIds.includes(f.id));
            if (deletedFields && deletedFields.length > 0) {
                const deletedFieldIds = deletedFields.map(f => f.id);
                dgViewModel.removeField(deletedFieldIds);
                // 若字段配置了表达式，需要删除表达式
                if (formSchemaUtils.getExpressions() && formSchemaUtils.getExpressions().length && deletedFieldIds.length) {
                    const newExpressions = formSchemaUtils.getExpressions().filter(e => !deletedFieldIds.includes(e.fieldId));
                    formSchemaUtils.setExpressions(newExpressions);
                }

            }
            textValue.value = `共 ${newFields.length} 项`;
            context.emit('change', newFields); // 属性编辑器自动将newFields赋值给对应控件value
        }

        const modalOptions = {
            fitContent: false,
            width: 900,
            height: 600,
            title: '列编辑器',
            acceptCallback: () => {
                clickConfirm();
            },
            rejectCallback: () => {
            }
        };
        function onChangeHandler(datas) {
            selectPanels.value = datas ? [...datas] : [];
        }
        function onBeforeOpen() {
            if (props.getLatestGridData) {
                // eslint-disable-next-line vue/no-mutating-props
                props.gridData = props.getLatestGridData();
                resetDataSource();
            }
            fieldColumns = props.gridData['columns'];
            getSelectPanels();
            return Promise.resolve(true);
        }

        onMounted(() => {
            resetDataSource();
        });

        return () => (
            <FButtonEdit
                button-behavior={'Modal'}
                modal-options={modalOptions}
                v-model={textValue.value}
                editable={false}
                beforeOpen={onBeforeOpen}
            >
                <div class="f-utils-absolute-all">
                    <FTransfer
                        selections={selectPanels.value}
                        data-source={dataSource.value}
                        rowOption={rowOptions}
                        class="mx-2"
                        displayType={'Tree'}
                        onChange={(datas) => onChangeHandler(datas)}
                    ></FTransfer>
                </div>
            </FButtonEdit>
        );
    }
});
