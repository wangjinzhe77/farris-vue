import { DesignerHostService } from '../../../designer-canvas/src/composition/types';
import { DynamicResolver } from '../../../../components/dynamic-resolver';
import { ComponentBuildInfo } from '../../../component/src/composition/inner-component-build-info';
import { ComponentSchema } from '../../../../components/designer-canvas';
import { FormSchemaEntityField$Type, FormSchemaEntityFieldTypeName } from '../../../common/entity/entity-schema';
import { cloneDeep } from 'lodash-es';
import { DgControl } from '../../../designer-canvas/src/composition/dg-control';
import { useGuid } from '../../../common';

const ROOT_VIEW_MODEL_ID = 'root-viewmodel';

/**
 * 创建表格组件服务类
 */
export class DataGridComponentCreatorService {

    private formSchemaUtils: any;
    private controlCreatorUtils: any;
    private designViewModelUtils: any;
    private useFormCommand: any;
    private formStateMachineUtils: any;

    constructor(
        private resolver: DynamicResolver,
        private designerHostService: DesignerHostService
    ) {
        this.formSchemaUtils = this.designerHostService.formSchemaUtils;
        this.controlCreatorUtils = this.designerHostService.controlCreatorUtils;
        this.designViewModelUtils = this.designerHostService.designViewModelUtils;
        this.useFormCommand = this.designerHostService.useFormCommand;
        this.formStateMachineUtils = this.designerHostService.formStateMachineUtils;
    }

    public createComponent(buildInfo: ComponentBuildInfo) {
        const componentRefNode = this.createComponentRefNode(buildInfo);

        const componentNode = this.createComponentNode(buildInfo);

        const viewModelNode = this.createViewModeNode(buildInfo);

        const formSchema = this.formSchemaUtils.getFormSchema();
        formSchema.module.viewmodels.push(viewModelNode);
        formSchema.module.components.push(componentNode);

        this.designViewModelUtils.assembleDesignViewModel();

        return this.wrapContainerSectionForComponent(componentRefNode, buildInfo, viewModelNode);
    }
    /**
     * 追加父容器
     * @param componentRefNode 
     * @param buildInfo 
     * @returns 
     */
    private wrapContainerSectionForComponent(componentRefNode: any, buildInfo: ComponentBuildInfo, viewModelNode: any) {
        const parentContainerType = buildInfo?.parentComponentInstance?.schema?.type;

        // 双列表模板、左树右列表模板，拖拽子表时，不生成父标题区域
        const templateId = this.formSchemaUtils.getFormSchema()?.module?.templateId;
        if (parentContainerType === DgControl['splitter-pane'].type && ['double-list-template', 'tree-list-template'].includes(templateId)) {
            return componentRefNode;
        }

        // 1、将表格拖入无标题的目标区域，需要给表格追加Section容器。并给Section添加新增删除按钮
        const parentContainerWithoutTitle = [DgControl['content-container'].type, DgControl['response-layout-item'].type, DgControl['splitter-pane'].type];
        if (parentContainerType && parentContainerWithoutTitle.includes(parentContainerType)) {
            const containerSection = this.resolver.getSchemaByType(
                'section',
                {
                    parentComponentInstance: buildInfo.parentComponentInstance,
                    mainTitle: buildInfo.componentName
                },
                this.designerHostService) as ComponentSchema;
            if (containerSection && containerSection.contents && containerSection.contents.length) {
                const section = containerSection.contents[0];
                section.contents = [componentRefNode];

                this.appendAddDeleteBtnToParentContainer(section, buildInfo, viewModelNode);

                return containerSection;
            }
        }
        // 2、将表格拖入有标题的目标区域，不需要追加父容器。只需要给父容器添加新增删除按钮
        this.appendAddDeleteBtnToParentContainer(buildInfo?.parentComponentInstance?.schema, buildInfo, viewModelNode);

        if (parentContainerType === DgControl['tab-page'].type) {
            // 为解决标签页画布无法更新的问题，手动触发update方法
            if (buildInfo.parentComponentInstance?.parent && buildInfo.parentComponentInstance?.parent['updateToolbarItems']) {
                buildInfo.parentComponentInstance?.parent['updateToolbarItems']();
            }

        }
        return componentRefNode;
    }

    /**
     * 为父容器追加新增、删除按钮
     */
    private appendAddDeleteBtnToParentContainer(resolvedContainerSchema: any, buildInfo: ComponentBuildInfo, viewModelNode: any) {
        const resolvedContainerType = resolvedContainerSchema.type;
        const commandPrefix = viewModelNode.id.replace(/-/g, '').replace(/_/g, '').replace('component', '').replace('viewmodel', '');

        // 限制子表
        if (buildInfo.bindTo === '/') {
            return;
        }
        // 限制列表或卡片
        if (buildInfo.componentType !== 'data-grid' && buildInfo.componentType !== 'form') {
            return;
        }
        const stateMachineRenderState = this.formStateMachineUtils && this.formStateMachineUtils.getRenderStates();

        const btnType = DgControl['tab-page'].type === resolvedContainerType ? 'tab-toolbar-item' : 'section-toolbar-item';
        const btns = [{
            id: `button-add-${buildInfo.componentId}`,
            type: btnType,
            text: '新增',
            disabled: stateMachineRenderState.find(d => d.id === 'canAddDetail') ? `!viewModel.stateMachine['canAddDetail']` : false,
            appearance: {
                class: 'btn btn-secondary f-btn-ml'
            },
            onClick: `root-viewModel.${viewModelNode.id}.${commandPrefix}AddItem1`
        },
        {
            id: `button-remove-${buildInfo.componentId}`,
            type: btnType,
            text: '删除',
            disabled: stateMachineRenderState.find(d => d.id === 'canRemoveDetail') ? `!viewModel.stateMachine['canRemoveDetail']` : false,
            appearance: {
                class: 'btn btn-secondary f-btn-ml'
            },
            onClick: `root-viewModel.${viewModelNode.id}.${commandPrefix}RemoveItem1`
        }];
        if (!resolvedContainerSchema.toolbar) {
            resolvedContainerSchema.toolbar = { buttons: [] };
        }
        if (!resolvedContainerSchema.toolbar.buttons) {
            resolvedContainerSchema.toolbar.buttons = [];
        }
        resolvedContainerSchema.toolbar.buttons = resolvedContainerSchema.toolbar.buttons.concat(btns);

        this.appendAddAndDeleteCommands(viewModelNode);
    }
    /**
     * 向视图模型添加新增删除命令
     */
    private appendAddAndDeleteCommands(viewModelNode: any) {
        const commandPrefix = viewModelNode.id.replace(/-/g, '').replace(/_/g, '').replace('component', '').replace('viewmodel', '');
        const addCommandId = useGuid().guid();
        const deleteCommandId = useGuid().guid();
        const cardControllerId = this.resolveCommandController();

        viewModelNode.commands.push(
            {
                id: addCommandId,
                code: `${commandPrefix}AddItem1`,
                name: '增加一条子表数据',
                params: [],
                handlerName: 'AddItem',
                cmpId: cardControllerId,
                shortcut: {},
                extensions: []
            },
            {
                id: deleteCommandId,
                code: `${commandPrefix}RemoveItem1`,
                name: '删除一条子表数据',
                params: [
                    {
                        name: 'id',
                        shownName: '待删除子表数据的标识',
                        value: `{DATA~${viewModelNode.bindTo}/id}`
                    }
                ],
                handlerName: 'RemoveItem',
                cmpId: cardControllerId,
                shortcut: {},
                extensions: []
            }
        );


        // 3、记录构件命令
        const webCmds = this.formSchemaUtils.getFormSchema().module.webcmds;
        const cardCmd = webCmds.find(webCmd => webCmd.id === cardControllerId);
        cardCmd.refedHandlers.push(
            {
                host: addCommandId,
                handler: 'AddItem'
            },
            {
                host: deleteCommandId,
                handler: 'RemoveItem'
            }
        );
    }
    /**
     * 获取新增删除子表命令可用的控制器id。
     */
    private resolveCommandController(): string {
        const webCmds = this.formSchemaUtils.getFormSchema().module.webcmds;
        // 卡片控制器 / 树卡控制器 / 高级列卡控制器
        const optionalControllerId = [
            '8172a979-2c80-4637-ace7-b13074d3f393',
            '8fe977a1-2b32-4f0f-a6b3-2657c4d03574',
            '45be24f9-c1f7-44f7-b447-fe2ada458a61'
        ];
        const availableController = webCmds.find(cmd => optionalControllerId.includes(cmd.id));
        if (availableController) {
            return availableController.id;
        }

        // 若当前表单没有可用的控制器，默认新增一个高级列卡控制器。这里需要触发load控制器，不然加载不到控制器内的命令
        const listDicControllerId = '45be24f9-c1f7-44f7-b447-fe2ada458a61';
        webCmds.push({
            id: listDicControllerId,
            path: '/projects/packages/Inspur.GS.Gsp.Web.WebCmp/webcmd',
            name: 'AdvancedListCardController.webcmd',
            refedHandlers: []
        });
        if (this.useFormCommand) {
            this.useFormCommand.checkCommands();
        }
        return listDicControllerId;


    }
    createComponentRefNode(buildInfo: ComponentBuildInfo): any {
        const componentRefNode = this.resolver.getSchemaByType('component-ref') as ComponentSchema;
        Object.assign(componentRefNode, {
            id: `${buildInfo.componentId}-component-ref`,
            component: `${buildInfo.componentId}-component`,
        });
        return componentRefNode;
    }

    createComponentNode(buildInfo: ComponentBuildInfo): any {
        const componentNode = this.resolver.getSchemaByType('component') as ComponentSchema;
        const contents = this.createDateGridComponentContents(buildInfo);
        Object.assign(componentNode, {
            id: `${buildInfo.componentId}-component`,
            viewModel: `${buildInfo.componentId}-component-viewmodel`,
            componentType: buildInfo.componentType,
            appearance: {
                class: this.getDataGridComponentClass()
            },
            contents
        });
        return componentNode;
    }
    /**
     * 添加viewModel节点
     */
    createViewModeNode(buildInfo: ComponentBuildInfo): any {
        const viewModelNode = {
            id: `${buildInfo.componentId}-component-viewmodel`,
            code: `${buildInfo.componentId}-component-viewmodel`,
            name: buildInfo.componentName,
            bindTo: buildInfo.bindTo,
            parent: ROOT_VIEW_MODEL_ID,
            fields: this.assembleViewModelFields(buildInfo),
            commands: [],
            states: [],
            enableValidation: true
        };
        return viewModelNode;
    }
    /**
     * 获取表格组件层级的class样式
     */
    private getDataGridComponentClass(): string {
        const { templateId } = this.formSchemaUtils.getFormSchema().module;

        // 双列表标签页、双列表、树列表模板，要求列表填充表单高度
        if (['double-list-in-tab-template', 'double-list-template', 'tree-list-template'].includes(templateId)) {
            return 'f-struct-wrapper f-utils-fill-flex-column';
        }
        return 'f-struct-is-subgrid';

    }
    /**
     * 创建表格组件内层级结构
     */
    private createDateGridComponentContents(buildInfo: ComponentBuildInfo) {
        const { templateId } = this.formSchemaUtils.getFormSchema().module;
        let container;
        // 根据模板不同，创建不同的容器类型和样式
        if (templateId === 'double-list-in-tab-template') {
            // 1、创建setion
            const section = this.resolver.getSchemaByType('section') as ComponentSchema;
            Object.assign(section, {
                id: buildInfo.componentId + '-section',
                appearance: {
                    class: 'f-section-grid f-section-in-main px-0 pt-0'
                },
                fill: true,
                showHeader: false
            });
            container = section;
        } else if (templateId === 'double-list-template' || templateId === 'tree-list-template') {
            // 1、创建setion
            const section = this.resolver.getSchemaByType('section') as ComponentSchema;
            Object.assign(section, {
                id: buildInfo.componentId + '-section',
                appearance: {
                    class: 'f-section-grid f-section-in-main'
                },
                fill: true,
                showHeader: false
            });
            container = section;
        } else {
            // 1、创建contentContainer
            const contentContainer = this.resolver.getSchemaByType('content-container') as ComponentSchema;
            Object.assign(contentContainer, {
                id: buildInfo.componentId + '-container',
                appearance: {
                    class: 'f-grid-is-sub f-utils-flex-column'
                }
            });
            container = contentContainer;
        }

        // 2、创建DataGrid(暂时不开启子表分页)
        const dataGrid = this.resolver.getSchemaByType('data-grid') as ComponentSchema;
        const columns: any[] = [];
        const stateMachineRenderState = this.formStateMachineUtils && this.formStateMachineUtils.getRenderStates();
        const isBindingMainEntity = buildInfo.bindTo === '/';
        Object.assign(dataGrid, {
            id: buildInfo.componentId + '-dataGrid',
            appearance: {
                class: 'f-component-grid'
            },
            columns,
            fieldEditable: buildInfo.editable,
            dataSource: buildInfo.dataSource || '',
            editable: !isBindingMainEntity && stateMachineRenderState.find(d => d.id === 'editable') ? 'viewModel.stateMachine[\'editable\']' : false,
            pagination: {
                enable: buildInfo.editable ? false : true
            }
        });
        container.contents = [dataGrid];
        const { selectedFields } = buildInfo;
        // 3、创建字段
        selectedFields?.forEach(field => {
            if (field.$type === FormSchemaEntityField$Type.ComplexField) {
                return;
            }
            const dgVMField = cloneDeep(field);
            const grieFieldMetadata = this.controlCreatorUtils.setGridFieldProperty('data-grid-column', dgVMField, '', buildInfo.editable);
            if (grieFieldMetadata) {
                columns.push(grieFieldMetadata);
            }
        });
        return [container];
    }


    /**
     * 组装viewModel fields 节点
     */
    private assembleViewModelFields(buildInfo: ComponentBuildInfo) {

        const vmFields: any[] = [];
        const { selectedFields } = buildInfo;
        selectedFields?.forEach(field => {
            let updateOn = 'blur';
            const type = field.type.name;
            if (type === FormSchemaEntityFieldTypeName.Enum || type === FormSchemaEntityFieldTypeName.Boolean) {
                updateOn = 'change';
            }

            vmFields.push({
                type: "Form",
                id: field.id,
                fieldName: field.bindingField,
                groupId: null,
                groupName: null,
                updateOn,
                fieldSchema: {}
            });
        });
        return vmFields;
    }

}
