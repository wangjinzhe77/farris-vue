export function resolveGridColumns(key: string, fields: any, resolvedSchema: any) {

    if (!fields || !fields.length) {
        return { columns: [] };
    }

    fields.forEach((field: any) => {
        field.title = field.caption ? field.caption : field.title;
        field.field = field.binding ? field.binding.path : field.field;
    });

    return { columns: fields };
}
