import { MapperFunction, resolveAppearance } from '../../../dynamic-resolver';
import { resolveGridColumns } from './column-resolver';

export const schemaMapper = new Map<string, string | MapperFunction>([
    ['appearance', resolveAppearance],
    ['column', 'columnOption'],
    // ['fields', resolveGridColumns]
]);

export const columnSchemaMapper = new Map<string, string | MapperFunction>([]);

