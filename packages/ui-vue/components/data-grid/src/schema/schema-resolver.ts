import { ComponentBuildInfo } from "../../../component/src/composition/inner-component-build-info";
import { DesignerHostService } from "../../../designer-canvas/src/composition/types";
import { DesignerComponentInstance } from "../../../designer-canvas/src/types";
import { DynamicResolver } from "../../../dynamic-resolver";
import { DataGridComponentCreatorService } from "../composition/data-grid-component-creator.service";

export function schemaResolver(resolver: DynamicResolver, schema: Record<string, any>, context: Record<string, any>, designerHostService?: DesignerHostService): Record<string, any> {
    const parentComponentInstance = context.parentComponentInstance as DesignerComponentInstance;
    if (parentComponentInstance && designerHostService) {
        const radomNumber = Math.random().toString(36).slice(2, 6);
        const componentBuildInfo: ComponentBuildInfo = {
            componentId: `data-grid-${radomNumber}`,
            componentName: context.bindingSourceContext?.entityTitle || context.bindingSourceContext?.bindingEntity?.name || `表格-${radomNumber}`,
            componentType: 'data-grid',
            parentContainerId: parentComponentInstance.schema.id,
            parentComponentInstance: parentComponentInstance,
            editable: true,
            bindTo: context.bindingSourceContext?.bindTo || '',
            dataSource: context.bindingSourceContext?.bindingEntity?.label,
            selectedFields: context.bindingSourceContext?.bindingEntityFields
        };
        const componentCreator = new DataGridComponentCreatorService(resolver, designerHostService);
        const componentRefNode = componentCreator.createComponent(componentBuildInfo);

        return componentRefNode;
    } else {
        return schema;
    }
}
/**
 *  列
 * @param resolver 
 * @param schema 
 * @param context 
 * @returns 
 */
export function columnSchemaResolver(resolver: DynamicResolver, schema: Record<string, any>, context: Record<string, any>): Record<string, any> {
    return schema;
}

