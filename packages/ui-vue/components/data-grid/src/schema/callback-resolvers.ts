import { DataColumn, VisualData, VisualDataCell } from "@/components/data-view";
import { Caller } from "../../../dynamic-resolver";

export function createDataGridCallbackResolver() {
    function resolve(viewSchema: Record<string, any>, caller: Caller) {
        const callbacks: Record<string, any> = {};
        callbacks.beforeEditCell = (context: { row: VisualData, cell: VisualDataCell, rawData: any, column: DataColumn; }): Promise<boolean> => {
            return caller.call('beforeEditCell', [context]);
        };
        return callbacks;
    }
    return {
        resolve
    };
}
