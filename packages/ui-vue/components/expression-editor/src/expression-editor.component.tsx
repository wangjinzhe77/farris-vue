/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * defination
 */
import { defineComponent, onMounted, ref, SetupContext, watch } from 'vue';
import { ExpressionEditorProps, expressionEditorProps } from './expression-editor.props';

export default defineComponent({
    name: 'FExpressionEditor',
    props: expressionEditorProps,
    emits: [] as (string[] & ThisType<void>) | undefined,
    setup(props: ExpressionEditorProps, context: SetupContext) {
        const elementRef = ref();
        onMounted(() => {
        });

        return () => {
            return (
                <div ref={elementRef} class="d-flex h-100 f-utils-flex-column" style="min-height:450px">
                    <f-layout class="flex-column">
                        <f-layout-pane custom-class="position-relative" position="top" minHeight={120}>
                            <div class="f-utils-absolute-all bg-info" style="opacity:0.2;z-index: -1;">
                            </div>
                            <p>头部区域</p>
                        </f-layout-pane>
                        <f-layout-pane position={"center"}>
                            <f-layout>
                                <f-layout-pane position="left" minWidth={200} width={400}>
                                    <div class="f-utils-absolute-all bg-info" style="opacity:0.2;z-index: -1;" >
                                    </div>
                                    <p>左侧区域</p>
                                </f-layout-pane>
                                <f-layout-pane position={"center"}>
                                    <div class="f-utils-absolute-all bg-danger" style="opacity:0.2;z-index:-1" >
                                    </div>
                                    <p>右侧区域</p>
                                </f-layout-pane>
                            </f-layout>
                        </f-layout-pane>
                    </f-layout>
                </div>
            );
        };
    }
});
