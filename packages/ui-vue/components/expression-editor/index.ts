 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import type { App } from 'vue';
import ExpressionEditor from './src/expression-editor.component';
import ExpressionEditorDesign from './src/designer/expression-editor.design.component';
import { propsResolver } from './src/expression-editor.props';

export * from './src/expression-editor.props';

export { ExpressionEditor };

export default {
    install(app: App): void {
        app.component(ExpressionEditor.name as string, ExpressionEditor);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>): void {
        componentMap['expression-editor'] = ExpressionEditor;
        propsResolverMap['expression-editor'] = propsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
        componentMap['expression-editor'] = ExpressionEditorDesign;
        propsResolverMap['expression-editor'] = propsResolver;
    }
};
