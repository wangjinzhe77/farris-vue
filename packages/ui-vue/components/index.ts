/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { App } from 'vue';
import Accordion from './accordion';
import Avatar from './avatar';
import BorderEditor from './border-editor';
import Button from './button';
import ButtonGroup from './button-group';
import ButtonEdit from './button-edit';
import Calculator from './calculator';
import Calendar from './calendar';
import Capsule from './capsule';
import ColorPicker from './color-picker';
import ListView from './list-view';
import CheckBox from './checkbox';
import ComboList from './combo-list';
import ComboTree from './combo-tree';
import Component from './component';
import ContentContainer from './content-container';
import DataGrid from './data-grid';
import DatePicker from './date-picker';
import DiscussionList from './discussion-list';
import DiscussionEditor from './discussion-editor';
import Dropdown from './dropdown';
import DynamicForm from './dynamic-form';
import DynamicView from './dynamic-view';
import EnumEditor from './enum-editor';
import EventsEditor from './events-editor';
import FieldSelector from './field-selector';
import FilterBar from './filter-bar';
import ImageCropper from './image-cropper';
import InputGroup from './input-group';
import Layout from './layout';
import ListNav from './list-nav';
import Loading from './loading';
import Lookup from './lookup';
import MappingEditor from './mapping-editor';
import Modal from './modal';
import MessageBox from './message-box';
import Nav from './nav';
import Notify from './notify';
import NumberRange from './number-range';
import NumberSpinner from './number-spinner';
import Order from './order';
import PageFooter from './page-footer';
import PageHeader from './page-header';
import Pagination from './pagination';
import Popover from './popover';
import Progress from './progress';
import PropertyEditor from './property-editor';
import PropertyPanel from './property-panel';
import Condition from './condition';
import RadioGroup from './radio-group';
import RadioButton from './radio-button';
import Rate from './rate';
import ResponseLayout from './response-layout';
import ResponseLayoutEditor from './response-layout-editor';
import ResponseToolbar from './response-toolbar';
import SearchBox from './search-box';
import Section from './section';
import SmokeDetector from './smoke-detector';
import SpacingEditor from './spacing-editor';
import Splitter from './splitter';
import Step from './step';
import Switch from './switch';
import Tabs from './tabs';
import Tags from './tags';
import Text from './text';
import TimePicker from './time-picker';
import Tooltip from './tooltip';
import Transfer from './transfer';
import TreeGrid from './tree-grid';
import TreeView from './tree-view';
import Uploader from './uploader';
import VerifyDetail from './verify-detail';
import QuerySolution from './query-solution';
import Video from './video';
import ExpressionEditor from './expression-editor';
import Textarea from './textarea';
import Drawer from './drawer';
import Common from './common';
import EventParemeter from './event-parameter';
import BindingSelector from './binding-selector';
import '../public/assets/farris-all.css';
// 导出所有组件，可以按需加载
export * from './components';
// 导出设计器部分组件及属性
export * from './designer';
export { resolverMap } from './dynamic-view';

export default {
    install(app: App): void {
        app.use(Accordion)
            .use(Avatar)
            .use(BorderEditor)
            .use(Button)
            .use(ButtonGroup)
            .use(ButtonEdit)
            .use(Calendar)
            .use(Calculator)
            .use(Capsule)
            .use(CheckBox)
            .use(ColorPicker)
            .use(ListView)
            .use(ComboList)
            .use(ComboTree)
            .use(Component)
            .use(Condition)
            .use(ContentContainer)
            .use(DataGrid)
            .use(DatePicker)
            .use(DiscussionEditor)
            .use(DiscussionList)
            .use(Dropdown)
            .use(DynamicForm)
            .use(DynamicView)
            .use(EnumEditor)
            .use(EventsEditor)
            .use(FieldSelector)
            .use(FilterBar)
            .use(ImageCropper)
            .use(InputGroup)
            .use(Layout)
            .use(ListNav)
            .use(Loading)
            .use(Lookup)
            .use(MappingEditor)
            .use(Modal)
            .use(MessageBox)
            .use(Nav)
            .use(Notify)
            .use(NumberRange)
            .use(NumberSpinner)
            .use(Order)
            .use(PageFooter)
            .use(PageHeader)
            .use(Pagination)
            .use(Popover)
            .use(Progress)
            .use(PropertyEditor)
            .use(PropertyPanel)
            .use(QuerySolution)
            .use(ResponseLayout)
            .use(ResponseLayoutEditor)
            .use(ResponseToolbar)
            .use(RadioGroup)
            .use(RadioButton)
            .use(Rate)
            .use(SearchBox)
            .use(Section)
            .use(SmokeDetector)
            .use(SpacingEditor)
            .use(Splitter)
            .use(Step)
            .use(Switch)
            .use(Tabs)
            .use(Tags)
            .use(Text)
            .use(TimePicker)
            .use(Tooltip)
            .use(Transfer)
            .use(TreeGrid)
            .use(TreeView)
            .use(Uploader)
            .use(VerifyDetail)
            .use(Video)
            .use(ExpressionEditor)
            .use(Textarea)
            .use(Drawer)
            .use(Common)
            .use(BindingSelector)
            .use(EventParemeter);

    }
};
