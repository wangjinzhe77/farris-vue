 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver/src/props-resolver';
import { schemaMapper } from './schema/schema-mapper';
import { schemaResolver } from './schema/schema-resolver';
import radioButtonSchema from './schema/radio-button.schema.json';
import propertyConfig from './property-config/radio-button.property-config.json';

export const radioButtonProps = {
    /**
     * 单选组枚举数组
     */
    enumData: { type: Array<string>, default: [{ text: '1', active: true }] },
    /**
     * 后缀
     */
    suffixValue: { type: String, default: '' },
    /**
     * 返回值
     */
    onSelectedValueChanged: { type: Function, default: () => { } }
} as Record<string, any>;

export type RadioButtonProps = ExtractPropTypes<typeof radioButtonProps>;
export const propsResolver = createPropsResolver<RadioButtonProps>(radioButtonProps, radioButtonSchema, schemaMapper, schemaResolver, propertyConfig);
