/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, SetupContext, ref, withModifiers, onMounted, watch } from 'vue';
import { RadioButtonProps, radioButtonProps } from './radio-button.props';

import './radio-button.css';

export default defineComponent({
    name: 'FRadioButton',
    props: radioButtonProps,
    emits: ['selectedValueChanged'] as (string[] & ThisType<void>) | undefined,
    setup(props: RadioButtonProps, context: SetupContext) {
        /** 传入值 */
        const enumData = ref(props.enumData);

        function handleClick(e, item) {
            e.currentTarget.classList.value = 'btn btn-secondary active';
            if (e.currentTarget.tagName.toLowerCase() === 'label') {
                const { currentTarget } = e;
                const parent = currentTarget.parentElement;
                const siblings = Array.from(parent.querySelectorAll('label')).filter(label => label !== currentTarget);
                siblings.forEach((item: any) => item.classList.remove('active'));
            }
            context.emit('selectedValueChanged',item);
        }
        function getText(item): any {
            return `${item.text}${props.suffixValue}`;
        }
        watch(() => props.enumData, (newEnumData, oldEnumData) => {
            if (newEnumData !== oldEnumData) {
                enumData.value = props.enumData;
            }
        });
        // height: 22px;
        return () => {
            return (
                <div class='f-radio-button'>
                    <div class={['farris-input-wrap']}>
                        {enumData.value.map((item, index) => (
                            <>
                                <label
                                    class={!item.active ? 'btn btn-secondary' : 'btn btn-secondary active'}
                                    onClick={(e) => handleClick(e, item)}
                                >
                                    <span class='f-radio-button-text'>{getText(item)}</span>
                                </label>
                            </>
                        ))}
                    </div>
                </div>
            );
        };
    }
});

// return (
// <div class="custom-control custom-radio">
// </div>
// <div class="custom-control custom-radio">
//     <label class={[`btn btn-${type.value}`,
//     // active:isGroupModel(),
//         {'disabled': disabled.value }]}>
//     </label>
//     <input
//         type="radio"
//         class="custom-control-input"
//         name={props.name}
//         id={id}
//         value={getValue(item)}
//         disabled={props.disabled}
//         tabindex={props.tabIndex}
//         onClick={(event: MouseEvent) => onClickRadio(item, event)}
//     />}
// </div>
// );
