 
 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import RadioButton from './src/radio-button.component';
import FRadioButtonDesign from './src/designer/radio-button.design.component';
import { propsResolver } from './src/radio-button.props';
import { withInstall } from '@farris/ui-vue/components/common';

export * from './src/radio-button.props';

RadioButton.register = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>) => {
    componentMap['radio-button'] = RadioButton;
    propsResolverMap['radio-button'] = propsResolver;
};
RadioButton.registerDesigner = (componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>) => {
    componentMap['radio-button'] = FRadioButtonDesign;
    propsResolverMap['radio-button'] = propsResolver;
};

export { RadioButton };
export default withInstall(RadioButton);
