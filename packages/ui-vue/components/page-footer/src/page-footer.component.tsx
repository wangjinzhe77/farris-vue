import { computed, defineComponent, ref } from "vue";
import { pageFooterProps, PageFooterProps } from './page-footer.props';

export default defineComponent({
    name: 'FPageFooter',
    props: pageFooterProps,
    emits: [],
    setup(props: PageFooterProps, context) {
        const collapsed = ref(props.collapsed);
        const collapsedText = computed(() => props.collapsedText);
        const contentClass = ref(props.contentClass);
        const enableCollapse = computed(() => props.enableCollapse);
        const expandedText = computed(() => props.expandedText);
        const headerClass = ref(props.headerClass);
        const headerContentClass = ref(props.headerContentClass);
        const headerToolbarClass = ref(props.headerToolbarClass);
        const shouldShowHeader = computed(() => props.showHeader);
        const customClass = ref(props.customClass);

        function toggleCollapse($event: MouseEvent) {
            $event && $event.stopImmediatePropagation();
            collapsed.value = !collapsed.value;
        }

        function renderHeaderToolbarContent() {
            return (
                enableCollapse.value &&
                <div class="toolbar--collapse" onClick={(payload: MouseEvent) => toggleCollapse(payload)}>
                    <span class="toobar--collapse-text">{collapsed.value ? expandedText.value : collapsedText.value}</span>
                    <i class="f-icon toolbar--collapse-icon"></i>
                </div>
            );
        }

        const footerheaderToolbarClass = computed(() => {
            const classObject = {
                'f-toolbar': true
            } as Record<string, boolean>;
            const customClassArray = headerToolbarClass.value.split(' ');
            customClassArray.reduce((result: any, className: any) => {
                result[className] = true;
                return result;
            }, classObject);
            return classObject;
        });

        function renderHeaderToolbar() {
            return (
                <div class={footerheaderToolbarClass.value}>
                    {context.slots.headerToolbar && context.slots.headerToolbar()}
                    {!context.slots.headerToolbar && renderHeaderToolbarContent()}
                </div>
            );
        }

        const footerHeaderContentClass = computed(() => {
            const classObject = {
                'f-content': true
            } as Record<string, boolean>;
            const customClassArray = headerContentClass.value.split(' ');
            customClassArray.reduce((result: any, className: any) => {
                result[className] = true;
                return result;
            }, classObject);
            return classObject;
        });

        function renderHeaderContent() {
            return <>
                {context.slots.headerContent && <div class={footerHeaderContentClass.value}>
                    {context.slots.headerContent()}</div>}
                {renderHeaderToolbar()}
            </>;
        }

        const footerHeaderClass = computed(() => {
            const classObject = {
                'f-cmp-footer-header': true
            } as Record<string, boolean>;
            const customClassArray = headerClass.value.split(' ');
            customClassArray.reduce((result: any, className: any) => {
                result[className] = true;
                return result;
            }, classObject);
            return classObject;
        });

        function renderHeader() {
            return (
                <header class={footerHeaderClass.value}>
                    {context.slots.header && context.slots.header()}
                    {!context.slots.header && renderHeaderContent()}
                </header>
            );
        }

        const footerClass = computed(() => {
            const classObject = {
                'f-cmp-footer': true,
                'f-state-collapse': enableCollapse.value && collapsed.value,
                'f-cmp-footer-accordion': enableCollapse.value
            } as Record<string, boolean>;
            const customClassArray = customClass.value.split(' ');
            customClassArray.reduce((result: any, className: any) => {
                result[className] = true;
                return result;
            }, classObject);
            return classObject;
        });

        const footerContentClass = computed(() => {
            const classObject = {
                'f-cmp-footer-content': true
            } as Record<string, boolean>;
            const customClassArray = contentClass.value.split(' ');
            customClassArray.reduce((result: any, className: any) => {
                result[className] = true;
                return result;
            }, classObject);
            return classObject;
        });

        return () => {
            return (
                <div class={footerClass.value}>
                    <footer class="f-cmp-footer-container">
                        {shouldShowHeader.value && renderHeader()}
                        <div class={footerContentClass.value}>
                            {context.slots.default && context.slots.default()}
                        </div>
                    </footer>
                </div>
            );
        };
    }
});
