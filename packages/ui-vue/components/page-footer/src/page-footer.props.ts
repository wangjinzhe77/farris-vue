 
import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import pageFooterSchema from './schema/page-footer.schema.json';
import { schemaResolver } from './schema/schema-resolver';
import propertyConfig from './property-config/page-footer.property-config.json';

export const pageFooterProps = {

    /** 
     * 组件自定义样式
     */
    customClass: { type: String, default: '' },

    /**
     * 是否启用内容区收折
     */
    enableCollapse: { type: Boolean, default: false },

    /**
     * 是否默认收折内容区
     */
    collapsed: { type: Boolean, default: true },
    /**
     * 收折按钮文本
     */
    collapsedText: { type: String, default: 'More' },

    /** 
     * 展开按钮文本
     */
    expandedText: { type: String, default: 'More' },

    /**
     * 内容区域样式
     */
    contentClass: { type: String, default: '' },

    /** 
     * 页脚组件样式
     */
    headerClass: { type: String, default: '' },

    /**
     * 上方扩展区域左侧内容区样式
     */
    headerContentClass: { type: String, default: '' },
    /**
     * 上方扩展区域右侧按钮区样式
     */
    headerToolbarClass: { type: String, default: '' },
    /**
     * 是否显示上方扩展区域
     */
    showHeader: { type: String, default: false },
} as Record<string, any>;

export type PageFooterProps = ExtractPropTypes<typeof pageFooterProps>;

export const propsResolver = createPropsResolver<PageFooterProps>(pageFooterProps, pageFooterSchema, schemaMapper, schemaResolver, propertyConfig);
