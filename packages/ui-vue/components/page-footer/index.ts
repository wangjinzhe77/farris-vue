 
import { App } from 'vue';
import FPageFooter from './src/page-footer.component';
import FPageFooterDesign from './src/designer/page-footer.design.component';
import { propsResolver } from './src/page-footer.props';

export * from './src/page-footer.props';

export { FPageFooter };

export default {
    install(app: App): void {
        app.component(FPageFooter.name as string, FPageFooter);
    },
    register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>, resolverMap: Record<string, any>): void {
        componentMap['page-footer'] = FPageFooter;
        propsResolverMap['page-footer'] = propsResolver;
    },
    registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
        componentMap['page-footer'] = FPageFooterDesign;
        propsResolverMap['page-footer'] = propsResolver;
    }
};
