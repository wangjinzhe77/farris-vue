/** 默认的第三方npm包声明文件所在的文件夹的地址 */
export const DEFAULT_LIBS_URL = "/platform/dev/common/web/intellisense";

/** 默认的Dts映射清单文件名 */
export const DEFAULT_DTS_MAP_MANIFEST = "dts.map.manifest.json";
