import { fileURLToPath, URL } from 'node:url';

const { BUILD_TYPE } = process.env;
const outDir = BUILD_TYPE === 'app' ? "dist" : 'package';
const externals = [BUILD_TYPE === 'components' ? "@components" : ''];
const externalDependencies = BUILD_TYPE !== 'app';

export default {
  lib: {
    entry: fileURLToPath(new URL('./components/index.ts', import.meta.url)),
    name: "farris-mobile-ui-vue",
    fileName: "index",
    formats: ["esm", 'umd', 'systemjs'],
  },
  outDir: fileURLToPath(new URL(`./${outDir}`, import.meta.url)),
  externals: {
    include: externals,
    filter: (externals)=> {
      return (id) => {
        return externals.find((item) => item && id.indexOf(item) === 0);
      };
    }
  },
  externalDependencies,
  minify: true,
  target: 'es2015',
  alias: [
    { find: '@', replacement: fileURLToPath(new URL('./', import.meta.url)) },
    { find: '@components', replacement: fileURLToPath(new URL('./components', import.meta.url)) },
    { find: '@farris/mobile-ui-vue', replacement: fileURLToPath(new URL('./components', import.meta.url)) }
  ]
};
