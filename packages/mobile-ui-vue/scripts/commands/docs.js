const {
  execSync
} = require('child_process');
const portfinder = require('portfinder');

exports.dev = () => {
  portfinder.getPort({
    port: 8385
  }, (_, port) => {
    process.env.VITE_APP_RUN_MODE = "dev";
    process.env.VITE_APP_DEMO_PORT = port;

    execSync(`concurrently --raw "farris-cli dev --port ${port}" "vitepress dev docs"`, {
      stdio: 'inherit'
    });
  });
};

exports.build = () => {
  const buildCommand = `vitepress build docs` +
    ` && farris-cli build` +
    ` && cp ./docs/assets/farris*.png ./docs/.vitepress/dist/assets` +
    ` && cp ./docs/assets/gitee.svg ./docs/.vitepress/dist/assets` +
    ` && cp -r ./dist ./docs/.vitepress/dist/mobile`;
  execSync(buildCommand, {
    stdio: 'inherit'
  });
};
