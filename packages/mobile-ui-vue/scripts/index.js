#!/usr/bin/env node
const { Command } = require('commander');
// const { build } = require('./commands/build');
const { dev: devDocs, build: buildDocs } = require('./commands/docs');

const program = new Command();

// program.command('build').description('构建 Farris UI Vue 组件库').action(build);

program.command('docs:dev').action(devDocs);

program.command('docs:build').action(buildDocs);

program.parse();
