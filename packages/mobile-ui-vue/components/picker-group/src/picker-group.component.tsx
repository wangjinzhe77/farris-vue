/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent } from 'vue';
import { useBem } from '@components/common';
import { TabBar } from '@components/tab-bar';
import { pickerGroupProps, PickerGroupProps, PICKER_GROUP_NAME } from './picker-group.props';
import { usePickerGroupContext } from './composition/use-picker-group-context';

export default defineComponent({
  name: PICKER_GROUP_NAME,
  props: pickerGroupProps,
  emits: ['confirm', 'cancel', 'update:modelValue'],
  setup(props: PickerGroupProps, context) {
    const { emit, slots } = context;

    const { innerValue, groupItems, getValue, } = usePickerGroupContext();

    const { bem } = useBem(PICKER_GROUP_NAME);

    const onConfirm = () => {
      const value = getValue();
      const text = getValue();
      emit('confirm', {value, text});
      emit('update:modelValue', value);
    };

    const onCancel = () => {
      emit('cancel');
    };

    const renderToolbar = () => (
      <div class={bem('toolbar')}>
        <div class={bem('toolbar-left')} onClick={onCancel}>
          <span>{props.cancelText}</span>
        </div>
        <div class={bem('toolbar-title')}>
          <span>{props.title}</span>
        </div>
        <div class={bem('toolbar-right')} onClick={onConfirm}>
          <span>{props.confirmText}</span>
        </div>
      </div>
    );

    return () => (
      <div class={bem()}>
        <div class={bem('header')}>
          {renderToolbar()}
          <TabBar
            class={bem('tab-bar')}
            v-model={innerValue.value}
            items={groupItems}
            showBottomLine={false}></TabBar>
        </div>
        <div class={bem('content')}>{slots.default && slots.default()}</div>
      </div>
    );
  }
});
