import { onMounted, reactive, ref, watch } from 'vue';
import { useChildrenContexts } from '@components/common';
import { GroupContext, GroupItem, PickerContext, PickerGroupKey } from '@/components/picker';

export const usePickerGroupContext = () => {
  const innerValue = ref();

  const groupItems = reactive<GroupItem[]>([]);

  const addItem = (item: GroupItem) => {
    groupItems.push(item);
  };

  const updateItem = (name: number, title: string) => {
    const item = groupItems.find(item=>item.name === name);
    if (item) {
      item.title = title;
    }
  };

  const context: GroupContext = {
    addItem,
    updateItem
  };

  const { childContexts } = useChildrenContexts<GroupContext, PickerContext>(PickerGroupKey, context);

  const getValue = ()=> {
    return childContexts.map(childContext=> {
      const { instance } = childContext;
      return instance?.exposed?.getValue();
    });
  };
  const getText = ()=> {
    return childContexts.map(childContext=> {
      const { instance } = childContext;
      return instance?.exposed?.getText();
    });
  };

  const hiddenChildToolbar = ()=> {
    childContexts.forEach(childContext=>{
      const { instance } = childContext;
      instance?.props.showToolbar && (instance.props.showToolbar = false);
    });
  };

  const toggleChildVisiable = ()=> {
    childContexts.forEach(childContext=>{
      const { name, instance } = childContext;
      if(name === innerValue.value){
        instance?.vnode.el && (instance.vnode.el.style = 'display: block');
      } else {
        instance?.vnode.el && (instance.vnode.el.style = 'display: none');
      }
    });
  };

  watch(innerValue, toggleChildVisiable);

  onMounted(()=>{
    innerValue.value = groupItems[0]?.name;
    hiddenChildToolbar();
    toggleChildVisiable();
  });

  return {
    innerValue,
    groupItems,
    getValue,
    getText
  };
};
