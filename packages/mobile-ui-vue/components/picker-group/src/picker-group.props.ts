import { ExtractPropTypes } from 'vue';

export const PICKER_GROUP_NAME = 'fm-picker-group';

export const pickerGroupProps = {
  title: { type: String, default: '' },

  cancelText: { type: String, default: '取消' },

  confirmText: { type: String, default: '确定' },
};

export type PickerGroupProps = ExtractPropTypes<typeof pickerGroupProps>;
