import { withInstall } from '@components/common';
import PickerGroupInstallless from "./src/picker-group.component";

const PickerGroup = withInstall(PickerGroupInstallless);

export { PickerGroup };
export default PickerGroup;
