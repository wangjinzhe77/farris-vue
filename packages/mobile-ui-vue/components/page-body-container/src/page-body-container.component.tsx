import { SetupContext, defineComponent, computed } from 'vue';
import { PAGE_BODY_CONTAINER_NAME, PageBodyContainerProps, pageBodyContainerProps } from './page-body-container.props';
import { useBem } from '@components/common';

export default defineComponent({
  name: PAGE_BODY_CONTAINER_NAME,
  props: pageBodyContainerProps,
  emits: [],
  setup(props: PageBodyContainerProps, context: SetupContext) {
    const { bem } = useBem(PAGE_BODY_CONTAINER_NAME);

    const containerClass = computed(() => ({
      [bem()]: true,
      [props.customClass || '']: true,
    }));

    return () => (
      <div class={containerClass.value}>
        {context.slots.default?.()}
      </div>
    );
  }
});
