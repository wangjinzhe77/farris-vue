import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import pageBodyContainerSchema from './schema/page-body-container.schema.json';
import pageBodyContainerPropertyConfig from './property-config/page-body-container.property-config.json';
import { schemaResolver } from './schema/schema-resolver';

export const PAGE_BODY_CONTAINER_NAME = 'fm-page-body-container';

export const pageBodyContainerProps = {
  customClass: { type: String, default: '' },
};

export type PageBodyContainerProps = ExtractPropTypes<typeof pageBodyContainerProps>;

export const propsResolver = createPropsResolver(
  pageBodyContainerProps,
  pageBodyContainerSchema,
  schemaMapper,
  schemaResolver,
  pageBodyContainerPropertyConfig,
);
