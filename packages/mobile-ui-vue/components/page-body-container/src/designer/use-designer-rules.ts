import { nextTick, ref } from "vue";
import { DesignerHTMLElement, DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema } from "../../../designer-canvas/src/types";
import { PAGE_BODY_CONTAINER_NAME } from '../page-body-container.props';

export function useDesignerRules(schema: ComponentSchema, parentSchema?: ComponentSchema): UseDesignerRules {

  const triggerBelongedComponentToMoveWhenMoved = ref(false);

  const triggerBelongedComponentToDeleteWhenDeleted = ref(false);

  const hideNestedPadding = true;

  const isInFixedContextRules = true;

  function canAccepts(draggingContext: DraggingResolveContext): boolean {
    const unAcceptableControlTypes = [
      'page-header-container',
      'page-footer-container',
      'page-body-container',
      'float-container',
      'navbar',
    ];
    const { controlType } = draggingContext;
    if (unAcceptableControlTypes.includes(controlType)) {
      return false;
    }
    return true;
  }


  function checkCanMoveComponent() {
    return !isInFixedContextRules;
  }
  function checkCanDeleteComponent() {
    return !isInFixedContextRules;
  }

  function hideNestedPaddingInDesginerView() {
    return hideNestedPadding;
  }

  function getDesignerClass(): string {
    return PAGE_BODY_CONTAINER_NAME;
  }

  return {
    canAccepts,
    triggerBelongedComponentToMoveWhenMoved,
    triggerBelongedComponentToDeleteWhenDeleted,
    checkCanMoveComponent,
    checkCanDeleteComponent,
    hideNestedPaddingInDesginerView,
    getDesignerClass,
  };
}
