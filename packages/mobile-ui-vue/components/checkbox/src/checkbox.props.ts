import { ExtractPropTypes } from 'vue';
import { checkerProps } from '@components/checker';

export const CHECKBOX_NAME = 'fm-checkbox';

export const checkboxProps = {
  ...checkerProps,

  name: { type: [String, Number], default: '' },
};

export type CheckboxProps = ExtractPropTypes<typeof checkboxProps>;
