/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { computed, defineComponent } from 'vue';
import { useLink } from '@components/common';
import { Checker, CheckerRoleMap, CheckerShapeMap } from '@components/checker';
import { CHECKBOX_GROUP_NAME, CheckboxGroupContext } from '@components/checkbox-group';
import {
  CHECKBOX_NAME,
  CheckboxProps,
  checkboxProps
} from './checkbox.props';

export default defineComponent({
  name: CHECKBOX_NAME,
  props: checkboxProps,
  setup(props: CheckboxProps, context) {
    const { emit } = context;

    const { getParent } = useLink(CHECKBOX_GROUP_NAME);
    const checkboxGroup = getParent<CheckboxGroupContext>();

    const handlerChange = (checked: boolean) => {
      checkboxGroup?.updateChecked(props.name, checked);
      emit('change', checked);
    };
    const handlerUpdateModelValue = (value: boolean) => {
      emit('update:modelValue', value);
    };

    const innerValue = computed(() => {
      return checkboxGroup?.getChecked ? checkboxGroup.getChecked(props.name) : props.modelValue;
    });

    const renderCheckbox = () => {
      return (
        <Checker
          label={props.label}
          modelValue={innerValue.value}
          role={CheckerRoleMap.Checkbox}
          shape={checkboxGroup?.props.shape || props.shape}
          type={checkboxGroup?.props.type || props.type}
          readonly={props.readonly || checkboxGroup?.props.readonly}
          disabled={props.disabled || checkboxGroup?.props.disabled}
          onChange={handlerChange}
          onUpdate:modelValue={handlerChange}
        ></Checker>
      );
    };

    return () => <div class={CHECKBOX_NAME}>{renderCheckbox()}</div>;
  }
});
