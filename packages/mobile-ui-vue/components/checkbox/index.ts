import { withInstall } from '@components/common';
import CheckboxInstallless from './src/checkbox.component';

export * from './src/checkbox.props';

const Checkbox = withInstall(CheckboxInstallless);

export { Checkbox };
export default Checkbox;
