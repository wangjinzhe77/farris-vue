import { withInstall } from '@components/common';
import TimePickerInputInstallless from './src/time-picker-input.component';

const TimePickerInput = withInstall(TimePickerInputInstallless);

export { TimePickerInput };
export default TimePickerInput;
