import { ExtractPropTypes } from 'vue';
import { buttonEditProps } from '@/components/button-edit';
import { timePickerProps } from '@/components/time-picker';

export const TIME_PICKER_INPUT_NAME = 'FmTimePickerInput';

export const timePickerInputProps = {
  ...buttonEditProps,
  ...timePickerProps,

  round: { type: Boolean, default: false }
};

export type TimePickerInputProps = ExtractPropTypes<typeof timePickerInputProps>;
