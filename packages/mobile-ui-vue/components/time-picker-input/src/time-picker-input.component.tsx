import { computed, defineComponent } from 'vue';
import { useBem } from '@components/common';
import TimePicker from '@/components/time-picker';
import ButtonEdit from '@/components/button-edit';
import { usePickerInputState } from '@/components/picker-input';
import { timePickerInputProps, TimePickerInputProps, TIME_PICKER_INPUT_NAME } from './time-picker-input.props';

export default defineComponent({
  name: TIME_PICKER_INPUT_NAME,
  props: timePickerInputProps,
  emits: ['change', 'confirm', 'update:modelValue'],
  setup(props: TimePickerInputProps, context) {

    const { bem } = useBem(TIME_PICKER_INPUT_NAME);

    const { buttonEditProps, inputValue, showPopup, componentRef, handleChange, handleConfirm } = usePickerInputState(props, context);

    const pickerProps = computed(() => {
      return {
        ref: componentRef,
        title: props.title,
        type: props.type,
        format: props.format,
        maxTime: props.maxTime,
        minTime: props.minTime,
        visiableOptionCount: props.visiableOptionCount,
        optionFormatter: props.optionFormatter,
        onChange: handleChange,
        onConfirm: handleConfirm,
        onCancel: () => showPopup.value = false
      };
    });

    return () => (
      <ButtonEdit
        {...buttonEditProps.value}
        v-model={inputValue.value}
        v-model:show={showPopup.value}
        inputClass={bem()}
      >
        <TimePicker {...pickerProps.value} />
      </ButtonEdit>
    );
  }
});
