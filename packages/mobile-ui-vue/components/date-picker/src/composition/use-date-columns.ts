import { computed, watch, ref } from 'vue';
import { Column, Columns, PickerChange } from '@components/picker';
import {
  compareDate,
  formatDate,
  getDateRecord,
  isDate,
  isLeap,
  isString,
  parseDate
} from '@components/common';
import { DatePickerProps, DatePickerTypeMap } from '../date-picker.props';

enum DateColumn {
  Year = 'year',
  Month = 'month',
  Date = 'day'
}

type DateRecord = {
  year: number;
  month: number;
  day: number;
};

type ColumnsMap = ('year' | 'month' | 'day')[];

export const useDateColumns = (props: DatePickerProps) => {
  const textField = 'text';
  const valueField = 'value';

  const getColumn = (begin: number, end: number, type: string) => {
    const column: Column = [];
    let count = begin;
    while (count <= end) {
      let text = String(count);
      if (count < 10) {
        text = `0${count}`;
      }
      text = props.optionFormatter ? props.optionFormatter(type, text) : text;
      column.push({ [textField]: text, [valueField]: count });
      count++;
    }
    return column;
  };

  const NowDate = new Date();

  const beginDate = ref<DateRecord>({ year: 0, month: 0, day: 0 });
  const setBeginDate = () => {
    if (props.minDate) {
      beginDate.value = getDateRecord(props.minDate);
      return;
    }
    const { year, month, day } = getDateRecord(NowDate);
    beginDate.value = { year: year - 10, month, day };
  };

  watch(() => props.minDate, setBeginDate, { immediate: true });

  const endDate = ref<DateRecord>({ year: 0, month: 0, day: 0 });
  const setEndDate = () => {
    if (props.maxDate) {
      endDate.value = getDateRecord(props.maxDate);
      return;
    }
    const { year, month, day } = getDateRecord(NowDate);
    endDate.value = { year: year + 10, month, day };
  };

  watch(() => props.maxDate, setEndDate, { immediate: true });

  const innerValue = ref<number[]>([]);

  const formatValue = (value: string | Date) => {
    let currentDate = new Date();
    if (isString(value) && value) {
      currentDate = parseDate(value, props.format);
    } else if (isDate(value)) {
      currentDate = value;
    }
    if (props.minDate && compareDate(currentDate, props.minDate) < 0) {
      currentDate = props.minDate;
    }
    if (props.maxDate && compareDate(currentDate, props.maxDate) > 0) {
      currentDate = props.maxDate;
    }
    const { year, month, day } = getDateRecord(currentDate);
    innerValue.value = [year, month, day];
  };

  watch(() => props.modelValue, formatValue, { immediate: true });

  const getCurrentDate = () => {
    const [year, month, day] = innerValue.value;
    return { year, month, day };
  };

  const getYearColumn = () => {
    const beginYear = beginDate.value.year;
    const endYear = endDate.value.year;

    return getColumn(beginYear, endYear, DateColumn.Year);
  };

  const getMonthColumn = (year: number) => {
    const _beginDate = beginDate.value;
    const _endDate = endDate.value;

    const monthRecord = { begin: 1, end: 12 };

    if (year <= _beginDate.year) {
      monthRecord.begin = _beginDate.month;
    }
    if (year >= _endDate.year) {
      monthRecord.end = _endDate.month;
    }

    return getColumn(monthRecord.begin, monthRecord.end, DateColumn.Month);
  };

  const getDateColumn = (year: number, month: number) => {
    const _beginDate = beginDate.value;
    const _endDate = endDate.value;

    const februaryDateCount = isLeap(year) && month === 2 ? 29 : 28;

    const date31 = [1, 3, 5, 7, 8, 10, 12];

    const date30 = [4, 6, 9, 11];

    const dateCount = date31.includes(month) ? 31 : date30.includes(month) ? 30 : februaryDateCount;

    const dateRecord = { begin: 1, end: dateCount };

    if (year <= _beginDate.year && month <= _beginDate.month) {
      dateRecord.begin = _beginDate.day;
    }
    if (year >= _endDate.year && month >= _endDate.month) {
      dateRecord.end = _endDate.day;
    }

    return getColumn(dateRecord.begin, dateRecord.end, DateColumn.Date);
  };

  const columnsMap = computed<ColumnsMap>(() => {
    const type =
      props.type === DatePickerTypeMap.Date ? DatePickerTypeMap.YearMonthDay : props.type;
    return type.split('-') as ColumnsMap;
  });

  const dateColumns = computed<Columns>(() => {
    const current = getCurrentDate();

    const yearColumn = getYearColumn();
    const monthColumn = getMonthColumn(current.year);
    const dateColumn = getDateColumn(current.year, current.month);

    return columnsMap.value.map((columnName) => {
      switch (columnName) {
        case DateColumn.Year:
          return yearColumn;
        case DateColumn.Month:
          return monthColumn;
        case DateColumn.Date:
          return dateColumn;
        default:
          return [];
      }
    });
  });

  const getDateValue = (changes?: PickerChange) => {
    const dates = [...innerValue.value];
    if (changes) {
      const { index, value } = changes;
      dates[index] = value[valueField] as number;
    }
    const [year, month = 1, day = 1] = dates;

    const result = new Date(year, month - 1, day);

    if (props.type === DatePickerTypeMap.Date || props.type === DatePickerTypeMap.YearMonthDay) {
      return result;
    }
    return formatDate(result, props.format);
  };

  const getTextValue = () => {
    const value = getDateValue();
    if (isDate(value)) {
      return formatDate(value, props.format);
    }
    return value;
  };

  return {
    textField,
    valueField,
    columnsMap,
    dateColumns,
    innerValue,
    getDateValue,
    getTextValue
  };
};
