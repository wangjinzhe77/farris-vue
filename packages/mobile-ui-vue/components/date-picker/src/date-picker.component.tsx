/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { defineComponent, SetupContext } from 'vue';
import { Picker, PickerChange } from '@components/picker';
import { useBem } from '@components/common';
import { useDateColumns } from './composition';
import { datePickerProps, DatePickerProps, DATE_PICKER_NAME } from './date-picker.props';

export default defineComponent({
  name: DATE_PICKER_NAME,
  inheritAttrs: false,
  props: datePickerProps,
  emits: ['change', 'confirm', 'cancel'],
  setup(props: DatePickerProps, context: SetupContext<("change" | "confirm" | "cancel")[]>) {
    const { emit, expose } = context;

    const { dateColumns, innerValue, textField, valueField, getDateValue, getTextValue } = useDateColumns(props);

    expose({ getTextValue });

    const handleChange = (changes: PickerChange)=>{
      emit('change', getDateValue(changes));
    };

    const handleConfirm = ()=>{
      emit('confirm', getDateValue());
    };

    const handleCanCel = ()=>{
      emit('cancel');
    };

    const { bem } = useBem(DATE_PICKER_NAME);

    return () => (
      <Picker
        class={bem()}
        v-model={innerValue.value}
        columns={dateColumns.value}
        valueField={valueField}
        textField={textField}
        title={props.title}
        showToolbar={props.showToolbar}
        visiableOptionCount={props.visiableOptionCount}
        onChange={handleChange}
        onConfirm={handleConfirm}
        onCancel={handleCanCel}
      ></Picker>
    );
  }
});
