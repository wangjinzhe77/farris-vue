import { ExtractPropTypes, PropType } from 'vue';

export const DATE_PICKER_NAME = 'FmDatePicker';

export const enum DatePickerTypeMap {
  Year = 'year',
  YearMonth = 'year-month',
  YearMonthDay = 'year-month-day',
  Date= 'date',
}

export type DatePickerType = `${DatePickerTypeMap}`;

export const datePickerProps = {
  title: { type: String, default: '' },

  type: { type: String as PropType<DatePickerType>, default: DatePickerTypeMap.Date },

  modelValue: { type: [String, Date], default: '' },

  format: { type: String, default: 'YYYY-MM-DD' },

  maxDate: { type: Date, default: undefined },

  minDate: { type: Date, default: undefined },

  optionFormatter: { type: Function as PropType<(type: string, value: string) => string>, default: undefined },

  visiableOptionCount: { type: Number, default: 5 },

  showToolbar: { type: Boolean, default: true }
};

export type DatePickerProps = ExtractPropTypes<typeof datePickerProps>;
