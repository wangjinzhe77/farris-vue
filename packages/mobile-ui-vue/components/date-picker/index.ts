import { withInstall } from '@components/common';
import DatePickerInstallless from './src/date-picker.component';

export * from './src/date-picker.props';

const DatePicker = withInstall(DatePickerInstallless);

export { DatePicker };
export default DatePicker;
