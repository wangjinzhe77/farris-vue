import { withInstall } from '@components/common';
import PickerInputInstallless from "./src/picker-input.component";

export * from './src/picker-input.props';
export * from './src/composition/use-picker-input-state';

const PickerInput = withInstall(PickerInputInstallless);

export { PickerInput };
export default PickerInput;
