import { ExtractPropTypes } from 'vue';
import { buttonEditProps } from '@/components/button-edit';
import { pickerProps } from '@/components/picker';

export const PICKER_INPUT_NAME = 'FmPickerInput';

export const pickerInputProps = {
  ...buttonEditProps,
  ...pickerProps
};

export type PickerInputProps = ExtractPropTypes<typeof pickerInputProps>;
