import { computed, defineComponent} from 'vue';
import { ButtonEdit } from '@/components/button-edit';
import { useBem } from '@components/common';
import { Picker } from '@/components/picker';
import { pickerInputProps, PickerInputProps, PICKER_INPUT_NAME } from './picker-input.props';
import { usePickerInputState } from './composition/use-picker-input-state';

export default defineComponent({
  name: PICKER_INPUT_NAME,
  props: pickerInputProps,
  emits: ['change', 'confirm', 'update:modelValue'],
  setup(props: PickerInputProps, context) {

    const { bem } = useBem(PICKER_INPUT_NAME);

    const { buttonEditProps, inputValue, showPopup, componentRef, handleChange, handleConfirm } = usePickerInputState(props, context);

    const pickerProps = computed(() => {
      return {
        ref: componentRef,
        modelValue: props.modelValue,
        title: props.title,
        columns: props.columns,
        valueField: props.valueField,
        textField: props.textField,
        childrenField: props.childrenField,
        visiableOptionCount: props.visiableOptionCount,
        onChange: handleChange,
        onConfirm: handleConfirm,
        onCancel: () => showPopup.value = false
      };
    });

    return () => (
      <ButtonEdit
        {...buttonEditProps.value}
        v-model={inputValue.value}
        v-model:show={showPopup.value}
        inputClass={bem()}
      >
        <Picker {...pickerProps.value} />
      </ButtonEdit>
    );
  }
});
