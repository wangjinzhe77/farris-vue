import { SetupContext, onMounted, shallowRef } from 'vue';
import { ButtonEditProps, useButtonEditProps } from '@/components/button-edit';
import { ColumnItem } from '@/components/picker';

export const usePickerInputState = (
  props: Omit<ButtonEditProps, 'modelValue'>,
  context: SetupContext<('change' | 'confirm' | 'update:modelValue')[]>
) => {
  const { emit } = context;

  const { inputValue, showPopup, buttonEditProps } = useButtonEditProps(props);

  const componentRef = shallowRef();
  const updateInputValue = () => {
    const { getTextValue } = componentRef.value;
    inputValue.value = getTextValue();
  };

  onMounted(() => {
    updateInputValue();
  });

  const handleConfirm = (value: any[]) => {
    showPopup.value = false;
    updateInputValue();
    emit('confirm', value);
    emit('update:modelValue', value);
  };

  const handleChange = (value: ColumnItem) => {
    emit('change', value);
  };

  return {
    inputValue,
    showPopup,
    buttonEditProps,
    componentRef,
    handleChange,
    handleConfirm
  };
};
