import { ExtractPropTypes, PropType } from 'vue';
import { ValidationRule } from './validation/index';
import { LabelAlign, ContentAlign } from './types';

export const FORM_ITEM_NAME = 'FmFormItem';

/**
 * 表单项属性
 */
export const formItemProps = {

  /** 名称 */
  name: { type: String, default: '' },

  /** 标签 */
  label: { type: String, default: '' },

  /** 标签排列方式 */
  labelAlign: { type: String as PropType<LabelAlign>, default: undefined },

  /** 标签宽度 */
  labelWidth: { type: [Number, String], default: undefined },

  /** 内容排列方式 */
  contentAlign: { type: String as PropType<ContentAlign>, default: undefined },

  /** 是否显示必填标志 */
  required: { type: Boolean, default: undefined },

  /** 验证规则 */
  rules: { type: Array as PropType<ValidationRule[]>, default: [] },

  /** 是否显示错误信息 */
  showErrorMessage: { type: Boolean, default: undefined },

  /** 错误信息位置 */
  errorMessageAlign: { type: String as PropType<ContentAlign>, deafult: undefined }

};

export type FormItemProps = ExtractPropTypes<typeof formItemProps>;
