import { Ref, ComputedRef } from 'vue';
import { ValidationError, ValidationErrors } from '../validation';
import { LabelAlign } from '../types';

export interface UseFormValidation {
  validate: () => Promise<ValidationErrors | null>;
  clearValidate: () => void;
}

export interface UseFormItemValidation {
  shouldShowRequired: ComputedRef<boolean>;
  shouldShowErrorMessage: ComputedRef<boolean>;
  errorMessage: Ref<string>;
  validate: () => Promise<ValidationError | null>;
  clearValidate: () => void;
}

export interface UseFormItemLabel {
  label: Ref<string>;
  labelAlign: ComputedRef<LabelAlign>;
  labelWidth: ComputedRef<number | string>;
  labelStyle: ComputedRef<string>;
}

export interface UseFormItem {
  formItemClass: ComputedRef<{ [className: string]: boolean }>;
}
