import { reactive, toRefs, SetupContext } from 'vue';
import { FormContext } from '../types';
import { FormProps } from '../form.props';

export function useFormContext(props: FormProps, context: SetupContext): FormContext {
  const { emit } = context;
  const {
    data,
    rules,
    labelAlign,
    labelWidth,
    contentAlign,
    autoShowRequired,
    showErrorMessage,
    errorMessageAlign
  } = toRefs(props);

  const formContext: FormContext = reactive({
    emit,
    data,
    rules,
    labelAlign,
    labelWidth,
    contentAlign,
    autoShowRequired,
    showErrorMessage,
    errorMessageAlign
  });

  return formContext;
}
