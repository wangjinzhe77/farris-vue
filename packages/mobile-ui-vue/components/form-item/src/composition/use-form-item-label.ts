import { computed,  toRefs } from 'vue';
import { addUnit } from '@components/common';
import { FormContext } from '../types';
import { FormItemProps } from '../form-item.props';
import { UseFormItemLabel } from './types';

export function useFormItemLabel(
  props: FormItemProps, formContext: FormContext
): UseFormItemLabel {

  const { label } = toRefs(props);

  const labelAlign = computed(() => {
    return props.labelAlign || formContext.labelAlign || 'left';;
  });

  const labelWidth = computed(() => {
    return props.labelWidth || formContext.labelWidth;
  });

  const labelStyle = computed(() => {
    if (labelAlign.value === 'top') {
      return '';
    }

    return `width: ${addUnit(labelWidth.value)}`;
  });

  return {
    label,
    labelAlign,
    labelWidth,
    labelStyle,
  };
}
