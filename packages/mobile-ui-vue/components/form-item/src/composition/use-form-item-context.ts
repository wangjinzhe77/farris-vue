import { reactive } from 'vue';
import { FormItemContext } from '../types';
import { FormItemProps } from '../form-item.props';

export function useFormItemContext(props: FormItemProps): FormItemContext {
  const formItemContext: FormItemContext = reactive<FormItemContext>({
    name: props.name,
    validate: null,
    clearValidate: null
  });

  return formItemContext;
}
