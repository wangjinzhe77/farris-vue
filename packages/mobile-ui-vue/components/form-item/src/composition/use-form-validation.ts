import { provide } from 'vue';
import { FormItemContext } from '../types';
import { UseFormValidation } from './types';
import { ValidationError, ValidationErrors, ValidationService, VALIDATION_SERVICE } from '../validation/index';

/**
 * 组装验证信息 
 */
function composeValidationErrors(errorPromises: { [key: string]: Promise<ValidationError | null> }): Promise<ValidationErrors | null> {
    const names = Object.keys(errorPromises);
    const promises = Object.values(errorPromises);

    return Promise.all(promises).then((errorArray) => {
        const errors = {};
        errorArray.forEach((error, index) => {
            if (error === null) {
                return;
            }
            const name = names[index];
            errors[name] = error;
        });

        const isValid = Object.keys(errors).length === 0;
        return isValid ? null : errors;
    });
};

export function useFormValidation(childContexts: FormItemContext[]): UseFormValidation {

    // 验证服务实例
    const validationService = new ValidationService();
    provide(VALIDATION_SERVICE, validationService);

    /**
   * 清空验证
   */
    function clearValidate(): void {
        childContexts.forEach((childContext) => {
            childContext.clearValidate();
        });
    }

    // 执行验证
    function validate(): Promise<ValidationErrors | null> {
        clearValidate();
        if (Array.isArray(childContexts) === false) {
            return Promise.resolve(null);
        }

        const validationErrors = {};
        childContexts.forEach((childContext) => {
            const validationError = childContext.validate();
            validationErrors[childContext.name] = validationError;
        });
        return composeValidationErrors(validationErrors);
    }

    return {
        validate,
        clearValidate
    };
}
