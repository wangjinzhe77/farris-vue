import { ref, computed, watch, inject } from 'vue';
import { FormItemProps } from '../form-item.props';
import { FormContext, FormItemContext } from '../types';
import { ValidationService, VALIDATION_SERVICE } from '../validation/index';
import { UseFormItemValidation } from './types';

export function useFormItemValidation(
  props: FormItemProps, formContext: FormContext, formItemContext: FormItemContext
): UseFormItemValidation {
  const validationService = inject<ValidationService>(VALIDATION_SERVICE);
  const errorMessage = ref('');

  // 字段值
  const value = computed(() => {
    const formData = formContext.data;
    return formData && formData[props.name];
  });

  // 验证规则
  const rules = computed(() => {
    const rulesFromForm = formContext.rules ? (formContext.rules[props.name] || []) : [];
    const rulesFromItem = props.rules || [];
    const rules = [...rulesFromForm, ...rulesFromItem];

    return rules;
  });

  // 是否显示错误消息
  const shouldShowErrorMessage = computed(() => {
    const showErrorMessage = props.showErrorMessage || formContext.showErrorMessage || true;
    return showErrorMessage && !!errorMessage.value;
  });

  // 是否显示必填
  const shouldShowRequired = computed(() => {
    if (props.required !== undefined) {
      return props.required;
    }

    if (formContext.autoShowRequired && Array.isArray(rules.value)) {
      const requiredRule = rules.value.find((rule) => {
        return rule.required === true;
      });

      return !!requiredRule;
    }
  });

  /**
   * 执行验证
   */
  let validating = false;
  let validateOnceMore = false;
  function validate() {
    if (!formContext.data || !props.name) {
      return;
    }

    if (validating === true) {
      validateOnceMore = true;
      return;
    }
    validating = true;

    const value = formContext.data[props.name];
    return validationService.validate(value, rules.value).then((error) => {
      validating = false;
      if (validateOnceMore) {
        validateOnceMore = false;
        validate();
      }
      errorMessage.value = error ? error.message : '';
      return error;
    });
  }

  /**
   * 清空验证
   */
  function clearValidate() {
    errorMessage.value = '';
  }

  /**
   * 监听值变化
   */
  function watchValueChange() {
    watch(value, () => {
      validate();
    });
  }

  /**
   * 监听验证规则变化
   */
  function watchRulesChange() {
    watch(
      rules,
      () => {
        validate();
      },
      { deep: true }
    );
  }

  /**
   * 注册校验方法
   */
  function registerMethods() {
    formItemContext.validate = validate;
    formItemContext.clearValidate = clearValidate;
  }

  watchValueChange();
  watchRulesChange();
  registerMethods();

  return {
    shouldShowRequired,
    shouldShowErrorMessage,
    errorMessage,
    validate,
    clearValidate
  };
}
