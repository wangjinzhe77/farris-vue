import { ExtractPropTypes, PropType } from 'vue';
import { LabelAlign, ContentAlign, FormData } from './types';
import { ValidationRules } from './validation';

/**
 * 表单属性
 */
export const formProps = {

  /** 绑定数据 */
  data: { type: Object as PropType<FormData>, default: {} },

  /** 标签排列方式 */
  labelAlign: { type: String as PropType<LabelAlign>, default: 'left' },

  /** 标签宽度 */
  labelWidth: { type: [Number, String], default: undefined },

  /** 输入框排列方式 */
  contentAlign: { type: String as PropType<ContentAlign>, default: 'right' },

  /** 根据验证规则自动显示必填标志 */
  autoShowRequired: { type: Boolean, default: true },

  /** 验证规则 */
  rules: { type: Object as PropType<ValidationRules> },

  /** 显示错误信息 */
  showErrorMessage: { type: Boolean, default: true },

  /** 错误信息排列方式 */
  errorMessageAlign: { type: String as PropType<ContentAlign>, default: 'left' }

};

export type FormProps = ExtractPropTypes<typeof formProps>;
