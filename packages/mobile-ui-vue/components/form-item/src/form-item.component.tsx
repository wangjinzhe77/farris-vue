/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { computed, defineComponent, SetupContext } from 'vue';
import { useParentContext, useBem } from '@components/common';
import FmCell from '@components/cell';

import { FORM_KEY } from './consts';
import { FormContext, FormItemContext } from './types';
import {
  useFormItemContext,
  useFormItemValidation,
  useFormItemLabel
} from './composition';
import { formItemProps, FormItemProps, FORM_ITEM_NAME } from './form-item.props';

export default defineComponent({
  name: FORM_ITEM_NAME,
  props: formItemProps,
  setup(props: FormItemProps, context: SetupContext) {
    const { slots, expose } = context;

    const formItemContext = useFormItemContext(props);
    const { parentContext: formContext } = useParentContext<FormContext, FormItemContext>(
      FORM_KEY,
      formItemContext
    );
    const { errorMessage, shouldShowRequired, shouldShowErrorMessage, validate, clearValidate } =
      useFormItemValidation(props, formContext, formItemContext);

    expose({
      validate,
      clearValidate
    });

    const { label, labelAlign, labelStyle } = useFormItemLabel(props, formContext);

    const { bem } = useBem(FORM_ITEM_NAME);

    const renderLabel = () => {
      if (slots.label) {
        return [slots.label()];
      }
      return <label>{label.value}</label>;
    };

    const contentClass = computed(() => {
      let contentAlign = props.contentAlign || formContext.contentAlign || 'right';
      if (labelAlign.value === 'top') {
        contentAlign = 'left';
      }
      return {
        [bem('content')]: true,
        [bem('content', contentAlign)]: true
      };
    });
    const renderContent = () => {
      return <div class={contentClass.value}>{slots.default()}</div>;
    };

    const errorMessageClass = computed(() => {
      const errorMessageAlign = props.errorMessageAlign || formContext.errorMessageAlign || 'left';
      return {
        [bem('error-message')]: true,
        [bem('error-message', errorMessageAlign)]: true
      };
    });
    const renderErrorMessage = () => {
      if (slots.errorMessage) {
        return [slots.errorMessage()];
      }
      return <div class={errorMessageClass.value}>{errorMessage.value}</div>;
    };

    const formItemClass = computed(() => {
      const direction = labelAlign.value === 'top' ? 'vertical' : 'horizontal';
      return {
        [bem()]: true,
        [bem('', direction)]: true
      };
    });

    const labelClass = computed(() => {
      const labelClasses = [bem('label'), bem('label', labelAlign.value)];
      if (shouldShowRequired.value === true) {
        labelClasses.push(bem('label', 'required'));
      }
      return labelClasses.join(' ');
    });

    return () => {
      const innerSlots = {
        title: renderLabel,
        footer: shouldShowErrorMessage ? renderErrorMessage : undefined,
        default: renderContent,
        leftIcon: slots.leftIcon,
        rightIcon: slots.rightIcon,
        extra: slots.extra
      };

      return (
        <FmCell
          class={formItemClass.value}
          titleClass={labelClass.value}
          titleStyle={labelStyle.value}
          v-slots={innerSlots}></FmCell>
      );
    };
  }
});
