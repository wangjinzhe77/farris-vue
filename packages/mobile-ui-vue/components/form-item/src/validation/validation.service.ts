import { isPromise } from "@components/common";
import { ValidationRule, ValidationError } from "./types";
import { ValidatorFactory } from "./validator.factory";

/**
 * 表单验证服务
 */
export class ValidationService {

  /**
   * 内置验证器集合
   */
  private validatorFactory: ValidatorFactory;

  /**
   * 构造函数
   */
  constructor() {
    this.validatorFactory = new ValidatorFactory();
  }

  /**
   * 验证值
   */
  public async validate(value: any, rules: ValidationRule[]): Promise<ValidationError | null> {
    if (!Array.isArray(rules)) {
      return Promise.resolve(null);
    }

    const rulesLen = rules.length;
    let firstError = null;
    for (let i = 0; i < rulesLen; i++) {
      const error = await this.checkRule(value, rules[i]);
      if (error) {
        firstError = error;
        break;
      }
    }

    return Promise.resolve(firstError);
  }

  /**
   * 检查规则
   */
  private checkRule(value: any, rule: ValidationRule): Promise<ValidationError | null> {
    if (!rule) {
      return null;
    }

    const validator = this.validatorFactory.createValidator(rule);
    if (!validator) {
      return;
    }

    const error = validator.validate(value, rule);
    const errorPromise = isPromise(error) ? error : Promise.resolve(error);

    return errorPromise;
  }
}
