import { ValidationError, ValidationRule, Validator } from '../types';

/**
 * 正则验证器
 */
export class MobilePhoneNumberValidator implements Validator {
  /**
   * 验证器名称
   */
  public static validatorName: string = 'mobilePhoneNumber';

  /**
   * 验证方法
   */
  public validate(value: any, rule: ValidationRule): ValidationError | null {
    const mobilePhoneNumberPattern = /^1[0-9]{10}$/;
    const { message } = rule;
    if (!mobilePhoneNumberPattern.test(String(value))) {
      return { message };
    }

    return null;
  }
}
