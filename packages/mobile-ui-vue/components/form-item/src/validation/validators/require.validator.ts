import { ValidationError, ValidationRule, Validator } from '../types';

/**
 * 必填验证器
 */
export class RequiredValidator implements Validator {

  /**
   * 验证器名称
   */
  public static validatorName: string = 'required';

  /**
   * 验证方法
   */
  public validate(value: any, rule: ValidationRule): ValidationError | null {
    if (rule.required && !value && value !== 0) {
      return { message: rule.message };
    }

    return null;
  }
}
