import { ValidationError, ValidationRule, Validator } from '../types';

/**
 * 正则验证器
 */
export class MinValueValidator implements Validator {
  /**
   * 验证器名称
   */
  public static validatorName = 'minValue';

  /**
   * 验证方法
   */
  public validate(value: any, rule: ValidationRule): ValidationError | null {
    const { message, constraints } = rule;
    const minValue = constraints[0];
    if (value === null || value === undefined || value === '') {
      return null;
    }
    if (minValue) {
      if (value < minValue) {
        return { message };
      }
    }
    return null;
  }
}
