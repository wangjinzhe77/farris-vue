import { ValidationError, ValidationRule, Validator } from '../types';

/**
 * 正则验证器
 */
export class MaxLengthValidator implements Validator {
  /**
   * 验证器名称
   */
  public static validatorName: string = 'maxLength';

  /**
   * 验证方法
   */
  public validate(value: any, rule: ValidationRule): ValidationError | null {
    const { message, constraints } = rule;
    const maxLength = constraints[0];
    if (value.length > maxLength) {
      return { message };
    }

    return null;
  }
}
