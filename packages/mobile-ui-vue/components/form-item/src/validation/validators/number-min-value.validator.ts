import { ValidationError, ValidationRule, Validator } from '../types';

/**
 * 正则验证器
 */
export class NumberMinValueValidator implements Validator {
  /**
   * 验证器名称
   */
  public static validatorName: string = 'numberMinValue';

  /**
   * 验证方法
   */
  public validate(value: any, rule: ValidationRule): ValidationError | null {

    const { message, constraints } = rule;
    const minValue = constraints[0];
    const valueNumber = parseFloat(value);
    if (typeof valueNumber !== 'number') {
      return { message: '' };
    }
    if (minValue) {
      if (valueNumber <= minValue) {
        return { message };
      }
    }
    return null;
  }
}
