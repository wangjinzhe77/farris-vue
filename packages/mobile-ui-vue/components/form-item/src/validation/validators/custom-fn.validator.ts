import { isFunction, isPromise } from "@components/common";
import { ValidationError, ValidationRule, Validator } from '../types';

/**
 * 自定义函数验证器
 */
export class CustomFnValidator implements Validator {

  /**
   * 验证器名称
   */
  public static validatorName = 'validator';

  /**
   * 自定义函数验证
   */
  public validate(value: any, rule: ValidationRule): Promise<ValidationError | null> {
    const validationFn = rule.validator;
    if (!validationFn || !isFunction(validationFn)) {
      return Promise.resolve(null);
    }

    const isValid = validationFn(value);
    const error: ValidationError = {
      message: rule.message
    };
    const promsie = new Promise<ValidationError | null>((resolve) => {
      if (isPromise(isValid) === false) {
        isValid === true ? resolve(null) : resolve(error);
      }

      (isValid as Promise<boolean>).then((isValid) => {
        isValid === true ? resolve(null) : resolve(error);
      });
    });

    return promsie;
  }
}
