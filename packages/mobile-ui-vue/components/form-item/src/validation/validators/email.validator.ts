import { ValidationError, ValidationRule, Validator } from '../types';

/**
 * 正则验证器
 */
export class EmailValidator implements Validator {
  /**
   * 验证器名称
   */
  public static validatorName: string = 'email';

  /**
   * 验证方法
   */
  public validate(value: any, rule: ValidationRule): ValidationError | null {
    const emailPattern = /^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
    const { message } = rule;
    if (!emailPattern.test(String(value))) {
      return { message };
    }

    return null;
  }
}
