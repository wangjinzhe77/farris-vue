import { ValidationError, ValidationRule, Validator } from '../types';

/**
 * 正则验证器
 */
export class MinLengthValidator implements Validator {
  /**
   * 验证器名称
   */
  public static validatorName: string = 'minLength';

  /**
   * 验证方法
   */
  public validate(value: any, rule: ValidationRule): ValidationError | null {
    const { message, constraints } = rule;
    const minLength = constraints[0];
    if (value.length < minLength) {
      return { message };
    }

    return null;
  }
}
