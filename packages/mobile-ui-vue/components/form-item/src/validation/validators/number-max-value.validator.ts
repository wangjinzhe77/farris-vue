import { ValidationError, ValidationRule, Validator } from '../types';

/**
 * 正则验证器
 */
export class NumberMaxValueValidator implements Validator {
  /**
   * 验证器名称
   */
  public static validatorName: string = 'numberMaxValue';

  /**
   * 验证方法
   */
  public validate(value: any, rule: ValidationRule): ValidationError | null {

    const { message, constraints } = rule;
    const maxValue = constraints[0];
    const valueNumber = parseFloat(value);
    if (typeof valueNumber !== 'number') {
      return { message: '' };
    }
    if (maxValue) {
      if (valueNumber >= parseFloat(maxValue)) {
        return { message };
      }
    }
    return null;
  }
}
