import { ValidationError, ValidationRule, Validator } from '../types';
import { compareDate } from '@components/common';

/**
 * 正则验证器
 */
export class DateMinValueValidator implements Validator {
  /**
   * 验证器名称
   */
  public static validatorName = 'dateMinValue';

  /**
   * 验证方法
   */
  public validate(value: any, rule: ValidationRule): ValidationError | null {
    const { message, constraints } = rule;
    const minDateValue = constraints[0];
    const valueDate = value;
    if (valueDate === null || valueDate === undefined || valueDate === '') {
      return null;
    }
    if (minDateValue) {
      let condition = minDateValue;
      if (condition.length === 21) {
        condition = condition.slice(1, 20);
      }
      if (compareDate(new Date(valueDate), new Date(condition)) < 0) {
        return { message };
      }
    }
    return null;
  }
}
