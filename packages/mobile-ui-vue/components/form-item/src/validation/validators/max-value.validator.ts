import { ValidationError, ValidationRule, Validator } from '../types';

/**
 * 正则验证器
 */
export class MaxValueValidator implements Validator {
  /**
   * 验证器名称
   */
  public static validatorName = 'maxValue';

  /**
   * 验证方法
   */
  public validate(value: any, rule: ValidationRule): ValidationError | null {
    const { message, constraints } = rule;
    const maxValue = constraints[0];
    if (value === null || value === undefined || value === '') {
      return null;
    }
    if (maxValue) {
      if (value > maxValue) {
        return { message };
      }
    }
    return null;
  }
}
