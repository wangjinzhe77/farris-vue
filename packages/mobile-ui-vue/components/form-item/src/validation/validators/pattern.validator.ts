import { ValidationError, ValidationRule, Validator } from '../types';

/**
 * 正则验证器
 */
export class PatternValidator implements Validator {

  /**
   * 验证器名称
   */
  public static validatorName: string = 'pattern';

  /**
   * 验证方法
   */
  public validate(value: any, rule: ValidationRule): ValidationError | null {
    const {pattern} = rule;
    if (pattern && !pattern.test(String(value))) {
      return { message: rule.message };
    }

    return null;
  }
}
