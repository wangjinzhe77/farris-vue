import { ValidationError, ValidationRule, Validator } from '../types';

/**
 * 正则验证器
 */
export class IdCardValidator implements Validator {
  /**
   * 验证器名称
   */
  public static validatorName: string = 'idcard';

  /**
   * 验证方法
   */
  public validate(value: any, rule: ValidationRule): ValidationError | null {
    const idCardPattern = /^(?:\d{18}|\d{15}|\d{17}[xX])$/i;
    const { message } = rule;
    if (!idCardPattern.test(String(value))) {
      return { message };
    }

    return null;
  }
}
