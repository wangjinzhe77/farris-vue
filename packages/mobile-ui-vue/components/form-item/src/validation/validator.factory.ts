import { ValidationRule, ValidatorType, Validator } from './types';
import {
  RequiredValidator,
  PatternValidator,
  CustomFnValidator,
  IdCardValidator,
  MobilePhoneNumberValidator,
  EmailValidator,
  NumberMaxValueValidator,
  NumberMinValueValidator,
  MaxValueValidator,
  MinValueValidator,
  DateMaxValueValidator,
  DateMinValueValidator,
  MaxLengthValidator,
  MinLengthValidator
} from './validators/index';

/**
 * 验证器工厂
 */
export class ValidatorFactory {
  /**
   * 验证器类型
   */
  private validatorTypes: Array<ValidatorType<Validator>>;

  /**
   * 验证器集合
   */
  private validators: Map<string, Validator>;

  /**
   * 构造函数
   */
  constructor() {
    this.validatorTypes = [
      RequiredValidator,
      PatternValidator,
      CustomFnValidator,
      IdCardValidator,
      MobilePhoneNumberValidator,
      EmailValidator,
      NumberMaxValueValidator,
      NumberMinValueValidator,
      MaxValueValidator,
      MinValueValidator,
      DateMaxValueValidator,
      DateMinValueValidator,
      MaxLengthValidator,
      MinLengthValidator
    ];
    this.validators = new Map<string, Validator>();
  }

  /**
   * 获取验证器集合
   */
  public createValidators(rules: ValidationRule[]): Validator[] {
    if (Array.isArray(rules) === false) {
      return [];
    }

    const validators = rules
      .map((rule) => {
        const validator = this.createValidator(rule);
        return validator;
      })
      .filter((validator) => {
        return validator as Validator;
      }) as Validator[];

    return validators;
  }

  /**
   * 创建验证器
   */
  public createValidator(rule: ValidationRule): Validator | null {
    const validatorType = this.getValidatorType(rule);
    if (!validatorType) {
      return null;
    }

    const {validatorName} = (validatorType as any);
    if (this.validators.has(validatorName)) {
      return this.validators.get(validatorName) || null;
    }

    const validator = new validatorType();
    if (!this.validators.has(validatorName)) {
      this.validators.set(validatorName, validator);
    }

    return validator;
  }

  /**
   * 获取验证器类型
   */
  public getValidatorType(rule: ValidationRule): ValidatorType<Validator> | undefined {
    const propNames = Object.keys(rule);
    const targetValidatorType = this.validatorTypes.find((validatorType) => {
      const {validatorName} = (validatorType as any);
      return propNames.includes(validatorName);
    });

    return targetValidatorType;
  }
}
