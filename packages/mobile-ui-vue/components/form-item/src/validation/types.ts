/**
 * 验证器类型
 */
export type ValidatorType<T> = new (...args: any[]) => T;

/**
 * 验证结果接口
 */
export interface ValidationError {
  /**
   * 错误消息
   */
  message: string;
}

/**
 * 错误信息集合
 */
export interface ValidationErrors {
  /**
   * 错误信息
   */
  [key: string]: ValidationError;
}

/**
 * 验证函数
 */
export type ValidationFn = (value: any) => boolean | Promise<boolean>;

/**
 * 验证规则
 */
export interface ValidationRule {
  /**
   * 验证类型
   */
  type?: string;

  /**
   * 验证参数
   */
  constraints?: any[];

  /**
   * 错误信息
   */
  message?: string;

  /**
   * 必填验证
   */
  required?: boolean;

  /**
   * 身份证号验证
   */
  idcard?: boolean;

  /**
   * 手机号验证
   */
  mobilePhoneNumber?: boolean;

  /**
   * 邮箱验证
   */
  email?: boolean;

  /**
   * 最大值验证
   */
  maxValue?: boolean;

  /**
   * 最小值验证
   */
  minValue?: boolean;

  /**
   * 日期最大值验证
   */
  dateMaxValue?: boolean;

  /**
   * 日期最小值验证
   */
  dateMinValue?: boolean;

  /**
   * 最大长度验证
   */
  maxLength?: boolean;

  /**
   * 最小长度验证
   */
  minLength?: boolean;

  /**
   * 正则验证
   */
  pattern?: RegExp;

  /**
   * 验证器方法
   */
  validator?: ValidationFn;
}

/**
 * 验证规则集合
 */
export interface ValidationRules {
  [key: string]: ValidationRule[];
}

/**
 * 验证器接口
 */
export interface Validator {
  /**
   * 验证方法
   */
  validate(
    value: any,
    rule: ValidationRule
  ): ValidationError | null | Promise<ValidationError | null>;
}
