
import { InjectionKey } from 'vue';
import { ValidationService } from './validation.service';

export const VALIDATION_SERVICE: InjectionKey<ValidationService> = Symbol('VALIDATION_SERVICE');
