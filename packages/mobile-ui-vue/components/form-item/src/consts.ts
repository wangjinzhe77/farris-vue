import { InjectionKey } from 'vue';
import { ParentProvideValue } from '@components/common';
import { FormContext, FormItemContext } from './types';

export const FORM_KEY: InjectionKey<ParentProvideValue<FormContext, FormItemContext>> = Symbol('FORM');
