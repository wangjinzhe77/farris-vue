import { ValidationRules, ValidationError, ValidationErrors } from './validation';

/**
 * 标签位置
 */
export type LabelAlign = 'left' | 'right' | 'top';

/**
 * 输入框内容位置
 */
export type ContentAlign = 'left' | 'center' | 'right';

/**
 * 表单项上下文
 */
export interface FormItemContext {
  /**
   * 名称
   */
  name: string;

  /**
   * 执行验证
   */
  validate: () => Promise<ValidationError | null>;

  /**
   * 清空验证
   */
  clearValidate: () => void;
}

/**
 * 表单数据
 */
export interface FormData {
  [key: string]: any;
}

/**
 * 表单上下文
 */
export interface FormContext {
  /**
   * 触发事件
   */
  emit: ((event: string, ...args: any[]) => void) | ((event: string, ...args: any[]) => void);

  /**
   * 表单数据
   */
  data: FormData;

  /**
   * 标签宽度
   */
  labelWidth: number | string;

  /**
   * 标签位置
   */
  labelAlign: LabelAlign;

  /**
   * 输入框内容位置
   */
  contentAlign: ContentAlign;

  /**
   * 根据验证规则自动显示必填标志
   */
  autoShowRequired: boolean;

  /**
   * 验证规则
   */
  rules?: ValidationRules;

  /**
   * 显示错误信息
   */
  showErrorMessage?: boolean;

  /**
   * 错误信息位置
   */
  errorMessageAlign?: ContentAlign;
}
