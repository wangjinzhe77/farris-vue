import { withInstall } from '@components/common';
import FormItemInstallless from './src/form-item.component';

export * from './src/form-item.props';

const FormItem = withInstall(FormItemInstallless);

export { FormItem };
export default FormItem;
