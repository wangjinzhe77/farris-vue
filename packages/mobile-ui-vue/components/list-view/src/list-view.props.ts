/* eslint-disable no-use-before-define */
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { ListProps } from '@components/list';
import { PullRefreshProps } from '@components/pull-refresh';
import { CheckboxProps } from '@components/checkbox/src/checkbox.props';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import listViewSchema from './schema/list-view.schema.json';
import listViewPropertyConfig from './property-config/list-view.property-config.json';
import { schemaResolver } from './schema/schema-resolver';

export const LIST_VIEW_NAME = 'fm-list-view';

export const listViewProps = {

  /** 列表数据 */
  data: { type: Array as PropType<any[]>, default: [] },

  /** 是否启用多选功能，默认不启用 */
  enableMultiSelect: { type: Boolean, default: false },

  /** 是否处于多选状态，支持语法糖`v-model:multiSelect`，在非多选状态下且`enableMultiSelect=true`时，用户长按任意列表条目将切换至多选状态 */
  multiSelect: { type: Boolean, default: false },

  /** 多选状态下，如果本属性非空则在列表下方显示一个按钮工具栏 */
  multiSelectToolbarItems: { type: Array as PropType<ListViewToolbarItem[]>, default: [] },

  /** ID字段的编号，ID字段用于唯一标识一条列表数据 */
  idField: { type: String, default: 'id' },

  /** 文本字段的编号 */
  textField: { type: String, default: 'name' },

  /** 禁用字段的编号，如果列表条目数据的禁用字段为真则列表条目禁用勾选 */
  disableField: { type: String, default: 'disabled' },

  /** 子级条目字段的编号，如果一条列表数据的子级条目字段非空，则该条数据以分组的形式渲染 */
  childField: { type: String, default: '' },

  /** 已勾选条目的ID数组 */
  selectedValues: { type: Array as PropType<string[]>, default: [] },

  /** 是否处于加载状态，支持语法糖`v-model:loading`，此状态下将在列表底部显示加载中提示文本 */
  loading: { type: Boolean, default: false },

  /** 是否处于加载失败状态，支持语法糖`v-model:error`，此状态下将在列表底部显示加载失败提示文本，用户点击提示将触发加载事件 */
  error: { type: Boolean, default: false },

  /** 是否加载完成，加载完成后将在列表下方显示加载完成提示文本，且不会再触发加载事件 */
  finished: { type: Boolean, default: false },

  /** 列表属性（除了`loading`、`error`、`finished`之外的其它列表组件属性） */
  listProps: { type: Object as PropType<ListProps>, default: {} },

  /** 是否启用下拉刷新 */
  enablePullRefresh: { type: Boolean, default: false },

  /** 是否正在刷新，支持语法糖`v-model:refreshing`，`enablePullRefresh`为真时有效 */
  refreshing: { type: Boolean, default: false },

  /** 下拉刷新属性（除了`modelValue`之外的其它下拉刷新组件属性），`enablePullRefresh`为真时有效 */
  pullRefreshProps: { type: Object as PropType<PullRefreshProps>, default: {} },

  /** 是否占满高度 */
  fill: { type: Boolean, default: false },

  /** 是否显示分隔线 */
  split: { type: Boolean, default: true },

  /** 列表数据为空时显示的占位文本 */
  emptyMessage: { type: String, default: '' },

  /** 多选状态下，勾选框的属性 */
  checkboxProps: { type: Object as PropType<CheckboxProps>, default: { shape: 'round' } },
};

/** 工具栏按钮 */
export interface ListViewToolbarItem {

  /** 按钮文本 */
  text: string;

  /** 点击事件回调方法 */
  handler: (item: ListViewToolbarItem) => void;

  /** 按钮样式类型 */
  type?: string;

  /** 是否禁用 */
  disabled?: boolean;
}

export type ListViewProps = ExtractPropTypes<typeof listViewProps>;

export const propsResolver = createPropsResolver(
  listViewProps,
  listViewSchema,
  schemaMapper,
  schemaResolver,
  listViewPropertyConfig,
);
