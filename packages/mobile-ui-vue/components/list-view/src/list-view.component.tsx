/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext, computed, withDirectives, toRefs, ref } from 'vue';
import { LIST_VIEW_NAME, listViewProps, ListViewProps } from './list-view.props';
import { useBem, vOnLongPress } from '@components/common';
import List from '@components/list';
import PullRefresh from '@components/pull-refresh';
import Button from '@components/button';
import { useData } from './composition/use-data';
import { useSelection } from './composition/use-selection';
import getListViewItemRender from './components/list-view-item.component';

export default defineComponent({
  name: LIST_VIEW_NAME,

  props: listViewProps,

  emits: [
    'clickItem',
    'update:multiSelect',
    'selectionChange',
    'load',
    'update:loading',
    'update:error',
    'refresh',
    'update:refreshing',
  ],

  setup(props: ListViewProps, context: SetupContext) {
    const { bem } = useBem(LIST_VIEW_NAME);
    const { emit, slots, expose } = context;
    const listComponentRef = ref();

    const useDataComposition = useData(props, context);
    const { listItems, isEmpty } = useDataComposition;

    const useSelectionComposition = useSelection(props, context, useDataComposition);
    const {
      toggleMultiSelect,
      isMultiSelectMode,
      getSelectedItems,
      clearSelection,
    } = useSelectionComposition;

    const { renderItem } = getListViewItemRender(props, context, useDataComposition, useSelectionComposition);

    const {
      enablePullRefresh,
      multiSelectToolbarItems,
      loading: isListLoading,
      error,
      finished,
      refreshing: isListRefreshing,
      pullRefreshProps,
      fill,
      split: shouldShowSplitLine,
    } = toRefs(props);

    const shouldShowEmptyTemplate = computed(() => {
      return isEmpty.value && !isListLoading.value && !slots.default;
    });
    const shouldShowListContent = computed(() => !isEmpty.value);

    const shouldShowToolbar = computed<boolean>(() => {
      return multiSelectToolbarItems.value && multiSelectToolbarItems.value.length > 0 && isMultiSelectMode.value;
    });

    const listProps = computed(() => {
      const shouldShowFinishedTip = !shouldShowEmptyTemplate.value && props.listProps?.showFinishedTip;
      return {
        ...props.listProps,
        showFinishedTip: shouldShowFinishedTip,
      };
    });

    const emptyMessage = computed(() => {
      return props.emptyMessage || '暂无数据';
    });

    const renderEmptyTemplate = () => {
      if (slots.empty) {
        return slots.empty();
      }
      return (
        <span class={bem('empty-message')}>{emptyMessage.value}</span>
      );
    };

    const handleListContentLongPress = () => {
      toggleMultiSelect(true);
    };

    const renderListContent = () => {
      const listContentClass = {
        [bem('content')]: true,
        [bem('content', 'split')]: shouldShowSplitLine.value,
      };
      const listContent = (
        <div class={listContentClass}>
          {listItems.value.map((item, index) => renderItem(item, index))}
        </div>
      );
      return (
        <>
          {withDirectives(listContent, [[
            vOnLongPress, handleListContentLongPress, `700`
          ]])}
        </>
      );
    };

    const renderList = () => {
      const listSlots = {
        finished: slots.finished,
        loading: slots.loading,
        error: slots.error,
        loadMore: slots.loadMore,
      };
      const listEventBinding = {
        'onLoad': () => emit('load'),
        'onUpdate:loading': (value: boolean) => emit('update:loading', value),
        'onUpdate:error': (value: boolean) => emit('update:error', value),
      };
      return (
        <List
          ref={listComponentRef}
          {...listProps.value}
          {...listEventBinding}
          loading={isListLoading.value}
          error={error.value}
          finished={finished.value}
          v-slots={listSlots}
        >
          {shouldShowListContent.value && renderListContent()}
          {shouldShowEmptyTemplate.value && renderEmptyTemplate()}
          {slots.default?.()}
        </List>
      );
    };

    const renderListWithPullRefresh = () => {
      if (!enablePullRefresh.value) {
        return renderList();
      }
      const pullRefreshSlots = {
        pulling: slots.pullRefreshPulling,
        loosing: slots.pullRefreshLoosing,
        loading: slots.pullRefreshLoading,
        complete: slots.pullRefreshComplete,
      };
      const pullRefreshEventBinding = {
        'onUpdate:modelValue': (value: boolean) => emit('update:refreshing', value),
        'onRefresh': () => emit('refresh'),
      };
      return (
        <PullRefresh
          {...pullRefreshProps.value}
          {...pullRefreshEventBinding}
          modelValue={isListRefreshing.value}
          v-slots={pullRefreshSlots}
        >
          {renderList()}
        </PullRefresh>
      );
    };

    const renderToolbar = () => {
      return (
        <div class={bem('toolbar')}>
          <slot name="toolbar" items={multiSelectToolbarItems.value}>
            {multiSelectToolbarItems.value.map((item) => (
              <div class={bem('toolbar-item')}>
                <Button
                  type={(item.type || 'primary') as any}
                  disabled={item.disabled}
                  plain
                  block
                  noBorder
                  {...{
                    'onClick': () => item.handler(item)
                  }}>
                  {item.text}
                </Button>
              </div>
            ))}
          </slot>
        </div>
      );
    };

    const checkListNeedLoadMore = () => {
      listComponentRef.value?.check();
    };

    expose({
      check: checkListNeedLoadMore,
      getSelectedItems,
      clearSelection,
    });

    return () => {
      const listViewClass = {
        [bem()]: true,
        [bem('', 'fill')]: fill.value,
        [bem('', 'multi-select')]: isMultiSelectMode.value,
      };
      return (
        <div class={listViewClass}>
          {slots.header?.()}
          <div class={bem('track')}>
            {renderListWithPullRefresh()}
          </div>
          {shouldShowToolbar.value && renderToolbar()}
          {slots.footer?.()}
        </div>
      );
    };
  }
});
