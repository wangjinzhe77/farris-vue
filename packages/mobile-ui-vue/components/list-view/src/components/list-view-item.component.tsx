import { SetupContext, toRefs } from "vue";
import { LIST_VIEW_NAME, ListViewProps } from '../list-view.props';
import { UseData, UseSelection } from '../composition/types';
import { useBem } from '@components/common';
import { Checkbox } from '@components/checkbox';

export default function (
  props: ListViewProps,
  context: SetupContext,
  useDataComposition: UseData,
  useSelectionComposition: UseSelection,
) {
  const { bem } = useBem(LIST_VIEW_NAME);

  const { emit, slots } = context;

  const { getItemText, getItemChildren, isItemDisabled } = useDataComposition;
  const { isMultiSelectMode, isItemSelected, toggleSelectItem } = useSelectionComposition;

  const { checkboxProps } = toRefs(props);

  const onListItemClick = (item: any, index: number) => {
    emit('clickItem', { data: item, index });
  };

  const onCheckboxClick = (evt: MouseEvent) => {
    evt && evt.stopPropagation();
  };

  const renderCheckbox = (item: any) => {
    return (
      <Checkbox
        {...checkboxProps.value}
        modelValue={isItemSelected(item)}
        disabled={isItemDisabled(item)}
        {...{
          'onUpdate:modelValue': () => toggleSelectItem(item),
        }}
      ></Checkbox>
    );
  };

  const renderItemContent = (item: any, index: number, disabled: boolean) => {
    if (slots.item) {
      return slots.item({ item, index, disabled });
    }
    return (
      <div class={bem('item-text')}>{getItemText(item)}</div>
    );
  };

  const renderSingleItem = (item: any, index: number, isChildItem?: boolean, parentItemIndex?: number) => {
    const itemClass = {
      [bem('item')]: true,
      [bem('item', 'child')]: isChildItem,
    };
    const shouldShowCheckbox = isMultiSelectMode.value;
    const shouldDisableItem = isItemDisabled(item) || isMultiSelectMode.value;
    const rootItemIndex = isChildItem ? parentItemIndex : index;

    return (
      <div class={itemClass} onClick={() => onListItemClick(item, rootItemIndex)}>
        {isMultiSelectMode.value && (
          <div class={bem('item-checker')} onClick={onCheckboxClick}>
            {shouldShowCheckbox && renderCheckbox(item)}
          </div>
        )}
        <div class={bem('item-content')}>
          {renderItemContent(item, index, shouldDisableItem)}
        </div>
      </div>
    );
  };

  const renderItem = (item: any, index: number) => {
    const children = getItemChildren(item);
    const hasChildren = children.length > 0;

    if (!hasChildren) {
      return renderSingleItem(item, index);
    }

    return (
      <div class={[bem('item'), bem('item', 'group')]}>
        <slot name="itemGroupHeader" item={item}>
          <div class={bem('item-group-header')}>{getItemText(item)}</div>
        </slot>
        <div class={bem('child-item-list')}>
          {children.map((subItem, subIndex) => renderSingleItem(subItem, subIndex, true, index))}
        </div>
      </div>
    );
  };

  return { renderItem };
}
