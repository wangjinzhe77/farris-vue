import { SetupContext, computed, Ref, ref, watch } from 'vue';
import { ListViewProps } from '../list-view.props';
import { UseSelection, UseData } from './types';
import { isDef } from '@components/common';

export function useSelection(
  props: ListViewProps,
  context: SetupContext,
  useDataCompostion: UseData,
): UseSelection {

  const { emit } = context;

  const isMultiSelectMode = computed(() => {
    return props.enableMultiSelect && props.multiSelect;
  });

  const toggleMultiSelect = (value: boolean) => {
    emit('update:multiSelect', value);
  };

  const { listItems, getItemID, getItemChildren } = useDataCompostion;

  const getId2ItemMap = () => {
    const id2Item = new Map<string, any>();
    listItems.value.forEach((item) => {
      id2Item.set(getItemID(item), item);

      // only supports one layer of child items
      const children = getItemChildren(item);
      children.forEach((childItem) => {
        id2Item.set(getItemID(childItem), childItem);
      });
    });
    return id2Item;
  };

  const getItemsFromIDs = (ids: string[]): any[] => {
    const items: any[] = [];
    const id2Item = getId2ItemMap();
    ids.forEach((id) => {
      if (!isDef(id)) {
        return;
      }
      const targetItem = id2Item.get(id);
      targetItem && items.push(targetItem);
    });
    return items;
  };

  const selectedItems: Ref<any[]> = ref(getItemsFromIDs(props.selectedValues));

  watch(() => props.selectedValues, (newValues: any[]) => {
    selectedItems.value = ref(getItemsFromIDs(newValues)).value;
  });

  const getItemIndexFromSelectedItems = (item: any): number => {
    const targetID = getItemID(item);
    if (!isDef(targetID)) {
      return -1;
    }
    return selectedItems.value.findIndex(i => getItemID(i) === targetID);
  };

  const isItemSelected = (item: any) => {
    return getItemIndexFromSelectedItems(item) >= 0;
  };

  const getSelectedItems = () => {
    return [...selectedItems.value];
  };

  const emitSelectionChange = () => {
    emit('selectionChange', getSelectedItems());
  };

  const toggleSelectItem = (item: any) => {
    const targetID = getItemID(item);
    if (!isDef(targetID)) {
      return;
    }
    const itemIndex = getItemIndexFromSelectedItems(item);
    if (itemIndex >= 0) {
      selectedItems.value.splice(itemIndex, 1);
    } else {
      selectedItems.value.push(item);
    }
    emitSelectionChange();
  };

  const clearSelection = () => {
    selectedItems.value = [];
    emitSelectionChange();
  };

  return {
    isMultiSelectMode,
    toggleMultiSelect,
    isItemSelected,
    toggleSelectItem,
    getSelectedItems,
    clearSelection,
  };
}
