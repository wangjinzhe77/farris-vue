import { ComputedRef } from 'vue';

export interface UseData {

  listItems: ComputedRef<any[]>;

  isEmpty: ComputedRef<boolean>;

  getItemID: (item: any) => any;

  getItemText: (item: any) => any;

  isItemDisabled: (item: any) => boolean;

  getItemChildren: (item: any) => any[];
}

export interface UseSelection {

  isMultiSelectMode: ComputedRef<boolean>;

  toggleMultiSelect: (selectMode: boolean) => void;

  isItemSelected: (item: any) => boolean;

  toggleSelectItem: (item: any) => void;

  getSelectedItems: () => any[];

  clearSelection: () => void;
}
