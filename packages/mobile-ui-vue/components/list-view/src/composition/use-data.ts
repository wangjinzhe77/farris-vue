import { SetupContext, computed } from 'vue';
import { ListViewProps } from '../list-view.props';
import { UseData } from './types';

export function useData(props: ListViewProps, _context: SetupContext): UseData {

  const getFieldPathArr = (fullPath: string): string[] => {
    if (!fullPath || typeof fullPath !== 'string') {
      return [];
    }
    return fullPath.split('.').filter(path => !!path);
  };

  const idFieldPathArr = computed(() => getFieldPathArr(props.idField));
  const textFieldPathArr = computed(() => getFieldPathArr(props.textField));
  const disableFieldPathArr = computed(() => getFieldPathArr(props.disableField));
  const childFieldPathArr = computed(() => getFieldPathArr(props.childField));

  const getFieldByPathArr = (object: any, fieldPathArr: string[]): any => {
    if (!object || !fieldPathArr || !fieldPathArr.length) {
      return undefined;
    }
    const fieldValue = fieldPathArr.reduce((currentObject, path) => {
      return currentObject ? currentObject[path] : currentObject;
    }, object);
    return fieldValue;
  };

  const setFieldByPathArr = (object: any, fieldPathArr: string[], value: any): any => {
    if (!object || !fieldPathArr || !fieldPathArr.length) {
      return object;
    }
    let currentObject = object;
    for (let i = 0; i < fieldPathArr.length; i++) {
      const path = fieldPathArr[i];
      const isLastPath = i === fieldPathArr.length - 1;
      if (isLastPath && typeof currentObject === 'object' && currentObject) {
        currentObject[path] = value;
      } else {
        currentObject[path] = currentObject[path] || {};
        currentObject = currentObject[path];
      }
    }
    return object;
  };

  const getItemID = (item: any): any => {
    return getFieldByPathArr(item, idFieldPathArr.value);
  };

  const getItemText = (item: any): any => {
    return getFieldByPathArr(item, textFieldPathArr.value) || '';
  };

  const isItemDisabled = (item: any): boolean => {
    return !!getFieldByPathArr(item, disableFieldPathArr.value);
  };

  const getItemChildren = (item: any): any[] => {
    const children = getFieldByPathArr(item, childFieldPathArr.value);
    const isChildrenValid = Array.isArray(children);
    return isChildrenValid ? children : [];
  };

  const listItems = computed(() => {
    const originalItems = props.data || [];
    const safeListItems = originalItems.map((item: any) => {
      if (typeof item !== 'object') {
        const defaultItem = setFieldByPathArr({}, textFieldPathArr.value, item);
        return defaultItem;
      }
      return item;
    });
    return safeListItems;
  });

  const isEmpty = computed(() => {
    return !listItems.value.length;
  });

  return {
    listItems,
    isEmpty,
    getItemID,
    getItemText,
    isItemDisabled,
    getItemChildren,
  };
}
