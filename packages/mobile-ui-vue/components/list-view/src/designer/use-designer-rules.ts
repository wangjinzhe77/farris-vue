import { nextTick, ref } from "vue";
import { DesignerHTMLElement, DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema } from "../../../designer-canvas/src/types";

export function useDesignerRules(schema: ComponentSchema, parentSchema?: ComponentSchema): UseDesignerRules {

  const triggerBelongedComponentToMoveWhenMoved = ref(false);

  const triggerBelongedComponentToDeleteWhenDeleted = ref(false);

  const canAcceptChildContent = false;

  const hideNestedPadding = true;

  const isInFixedContextRules = false;

  function canAccepts(draggingContext: DraggingResolveContext): boolean {
    return canAcceptChildContent;
  }

  function checkCanMoveComponent() {
    return !isInFixedContextRules;
  }
  function checkCanDeleteComponent() {
    return !isInFixedContextRules;
  }

  function hideNestedPaddingInDesginerView() {
    return hideNestedPadding;
  }

  function getStyles(): string {
    if (schema.fill) {
      return 'flex: 1';
    }
    return '';
  }

  return {
    canAccepts,
    triggerBelongedComponentToMoveWhenMoved,
    triggerBelongedComponentToDeleteWhenDeleted,
    checkCanMoveComponent,
    checkCanDeleteComponent,
    hideNestedPaddingInDesginerView,
    getStyles,
  };
}
