import { SetupContext, defineComponent, inject, onMounted, ref, computed } from 'vue';
import { ListViewProps, listViewProps } from '../list-view.props';
import { useDesignerRules } from './use-designer-rules';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';
import ListView from '../list-view.component';

export default defineComponent({
  name: 'FmListViewDesign',
  props: listViewProps,
  emits: [],
  setup(props: ListViewProps, context: SetupContext) {
    const elementRef = ref();
    const designItemContext = inject<DesignerItemContext>('design-item-context');
    const designerRulesComposition = useDesignerRules(designItemContext.schema, designItemContext.parent?.schema);
    const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

    onMounted(() => {
      elementRef.value.componentInstance = componentInstance;
    });

    context.expose(componentInstance.value);

    const listViewProps = computed(() => ({
      ...props,
      finished: true,
    }));

    return () => (
      <div ref={elementRef} class="drag-container" data-dragref={`${designItemContext.schema.id}-container`}>
        <ListView {...listViewProps.value}></ListView>
      </div>
    );
  }
});
