import { withInstall } from '@components/common';
import ListViewInstallless from './src/list-view.component';
import ListViewDesignInstallless from './src/designer/list-view.design.component';
import { propsResolver } from './src/list-view.props';

const COMPONENT_TYPE = 'list-view';
const ListView = withInstall(ListViewInstallless);
const ListViewDesign = withInstall(ListViewDesignInstallless);

export * from './src/list-view.props';

export { ListView, ListViewDesign };

export default {
  register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
    componentMap[COMPONENT_TYPE] = ListView;
    propsResolverMap[COMPONENT_TYPE] = propsResolver;
  },
  registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
    componentMap[COMPONENT_TYPE] = ListViewDesign;
    propsResolverMap[COMPONENT_TYPE] = propsResolver;
  }
};
