/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { defineComponent, SetupContext } from 'vue';
import { Picker, PickerChange } from '@components/picker';
import { useBem } from '@components/common';
import { useTimeColumns } from './composition';
import { timePickerProps, TimePickerProps, TIME_PICKER_NAME } from './time-picker.props';

export default defineComponent({
  name: TIME_PICKER_NAME,
  inheritAttrs: false,
  props: timePickerProps,
  emits: ['change', 'confirm', 'cancel'],
  setup(props: TimePickerProps, context: SetupContext<string[]>) {
    const { emit, expose } = context;

    const { timeColumns, innerValue, textField, valueField, getTimeValue } = useTimeColumns(props);

    expose({ getTextValue: getTimeValue });

    const handleChange = (changes: PickerChange)=>{
      emit('change', getTimeValue(changes));
    };

    const handleConfirm = ()=>{
      emit('confirm', getTimeValue());
    };

    const handleCanCel = ()=>{
      emit('cancel');
    };

    const { bem } = useBem(TIME_PICKER_NAME);

    return () => (
      <Picker
        v-model={innerValue.value}
        title={props.title}
        columns={timeColumns.value}
        class={bem()}
        valueField={valueField}
        textField={textField}
        showToolbar={props.showToolbar}
        visiableOptionCount={props.visiableOptionCount}
        onChange={handleChange}
        onConfirm={handleConfirm}
        onCancel={handleCanCel}
      ></Picker>
    );
  }
});
