import { computed, watch, ref } from 'vue';
import { Columns, Column, PickerChange } from '@components/picker';
import { formatTime, getTimeRecord, } from "@components/common";
import { TimePickerProps, TimePickerTypeMap } from "../time-picker.props";

enum TimeColumn {
  Hours = 'hours',
  Minutes = 'minutes',
  Seconds= 'seconds',
}

export const useTimeColumns = (props: TimePickerProps) => {
  const textField = 'text';
  const valueField = 'value';

  const getColumn = (begin: number, end: number, type: string) => {
    const column: Column = [];
    let count = begin;
    while (count <= end) {
      let text = String(count);
      if (count < 10) {
        text = `0${count}`;
      }
      text = props.optionFormatter ? props.optionFormatter(type, text) : text;
      column.push({ [textField]: text, [valueField]: count });
      count++;
    }
    return column;
  };

  const innerValue = ref([]);

  const formatValue = (value: string)=>{
    const { hours, minutes, seconds } = getTimeRecord(value);
    innerValue.value = [hours, minutes, seconds];
  };

  formatValue(props.modelValue);
  watch(()=> props.modelValue, formatValue);

  const getCurrentTime = ()=>{
    const [hours, minutes, seconds] = innerValue.value;
    return {hours, minutes, seconds};
  };

  const beginTime = ref({hours: 0, minutes: 0, seconds: 0});

  const setBeginTime = ()=>{
    if(props.minTime) {
      beginTime.value = getTimeRecord(props.minTime);;
    }
  };

  watch(()=> props.minTime, setBeginTime, { immediate: true});

  const endTime = ref({hours: 23, minutes: 59, seconds: 59});

  const setEndTime = ()=>{
    if(props.maxTime) {
      endTime.value = getTimeRecord(props.maxTime);
    }
  };

  watch(()=> props.maxTime, setEndTime, { immediate: true});

  const getHoursColumn = () => {
    const beginHours = beginTime.value.hours;
    const endHours = endTime.value.hours;

    return getColumn(beginHours, endHours, 'hours');
  };

  const getMinutesColumn = (hours: number) => {
    const _beginTime = beginTime.value;
    const _endTime = endTime.value;

    const minutesRecord = { begin: 0, end: 59 };

    if(hours <= _beginTime.hours) {
      minutesRecord.begin = _beginTime.minutes;
    }
    if(hours >= _endTime.hours) {
      minutesRecord.end = _endTime.minutes;
    }

    return getColumn(minutesRecord.begin, minutesRecord.end, 'minutes');
  };

  const getSecondsColumn = (hours: number, minutes: number) => {
    const _beginTime = beginTime.value;
    const _endTime = endTime.value;

    const secondsRecord = { begin: 0, end: 59 };

    if(hours <= _beginTime.hours && minutes <= _beginTime.minutes) {
      secondsRecord.begin = _beginTime.seconds;
    }
    if(hours >= _endTime.hours && minutes >= _endTime.minutes) {
      secondsRecord.end = _endTime.seconds;
    }

    return getColumn(secondsRecord.begin, secondsRecord.end, 'seconds');

  };

  const columnsMap = computed(() => {
    const type = props.type === TimePickerTypeMap.Time ? TimePickerTypeMap.HoursMinutesSeconds : props.type;
    return type.split('-');
  });

  const timeColumns = computed<Columns>(() => {
    const current = getCurrentTime();

    const hoursColumn = getHoursColumn();
    const minutesColumn = getMinutesColumn(current.hours);
    const secondsColumn = getSecondsColumn(current.hours, current.minutes);

    return columnsMap.value.map(columnName => {
      switch (columnName) {
        case TimeColumn.Hours:
          return hoursColumn;
        case TimeColumn.Minutes:
          return minutesColumn;
        case TimeColumn.Seconds:
          return secondsColumn;
      }
    });
  });

  const getTimeValue = (changes?: PickerChange)=>{
    const times = [...innerValue.value];
    if(changes) {
      const { index, value } = changes;
      times[index] = value[valueField];
    }
    const [hours, minutes = 0, seconds = 0] = times;

    return formatTime({hours, minutes, seconds}, props.format);
  };

  return {
    textField,
    valueField,
    timeColumns,
    innerValue,
    getTimeValue
  };
};
