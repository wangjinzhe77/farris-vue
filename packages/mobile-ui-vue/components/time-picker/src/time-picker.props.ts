import { ExtractPropTypes, PropType } from 'vue';

export const TIME_PICKER_NAME = 'fm-time-picker';

export const enum TimePickerTypeMap {
  Hours = 'hours',
  HoursMinutes = 'hours-minutes',
  HoursMinutesSeconds = 'hours-minutes-seconds',
  Time= 'time',
}

export type TimePickerType = `${TimePickerTypeMap}`;

export const timePickerProps = {
  title: { type: String, default: '' },

  type: { type: String as PropType<TimePickerType>, default: TimePickerTypeMap.Time },

  modelValue: { type: String, default: '' },

  format: { type: String, default: 'HH:mm:ss' },

  maxTime: { type: String, default: undefined },

  minTime: { type: String, default: undefined },

  optionFormatter: { type: Function as PropType<(type: string, value: string) => string>, default: undefined },

  visiableOptionCount: { type: Number, default: 5 },

  showToolbar: { type: Boolean, default: true }
};

export type TimePickerProps = ExtractPropTypes<typeof timePickerProps>;
