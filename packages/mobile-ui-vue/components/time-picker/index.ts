import { withInstall } from '@components/common';
import TimePickerInstallless from './src/time-picker.component';

export * from './src/time-picker.props';

const TimePicker = withInstall(TimePickerInstallless);

export { TimePicker };
export default TimePicker;
