import { withInstall } from '@components/common';
import DatePickerInputInstallless from './src/date-picker-input.component';

const DatePickerInput = withInstall(DatePickerInputInstallless);

export { DatePickerInput };
export default DatePickerInput;
