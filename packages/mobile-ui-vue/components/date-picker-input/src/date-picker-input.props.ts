import { ExtractPropTypes } from 'vue';
import { buttonEditProps } from '@/components/button-edit';
import { datePickerProps } from '@/components/date-picker';

export const DATA_PICKER_INPUT_NAME = 'FmDatePickerInput';

export const datePickerInputProps = {
  ...buttonEditProps,
  ...datePickerProps,

  round: { type: Boolean, default: false }
};

export type DatePickerInputProps = ExtractPropTypes<typeof datePickerInputProps>;
