/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SetupContext, computed, defineComponent } from 'vue';
import { useBem } from '@components/common';
import { DatePicker } from '@/components/date-picker';
import { ButtonEdit } from '@/components/button-edit';
import { usePickerInputState } from '@/components/picker-input';
import { datePickerInputProps, DatePickerInputProps, DATA_PICKER_INPUT_NAME } from './date-picker-input.props';

export default defineComponent({
  name: DATA_PICKER_INPUT_NAME,
  props: datePickerInputProps,
  emits: ['change', 'confirm', 'update:modelValue'],
  setup(props: DatePickerInputProps, context) {
    const { bem } = useBem(DATA_PICKER_INPUT_NAME);

    const { buttonEditProps, inputValue, showPopup, componentRef, handleChange, handleConfirm } = usePickerInputState(props, context);

    const pickerProps = computed(() => {
      return {
        ref: componentRef,
        title: props.title,
        type: props.type,
        format: props.format,
        maxDate: props.maxDate,
        minDate: props.minDate,
        visiableOptionCount: props.visiableOptionCount,
        optionFormatter: props.optionFormatter,
        onChange: handleChange,
        onConfirm: handleConfirm,
        onCancel: () => showPopup.value = false
      };
    });

    return () => (
      <ButtonEdit
        {...buttonEditProps.value}
        v-model={inputValue.value}
        v-model:show={showPopup.value}
        inputClass={bem()}
      >
        <DatePicker {...pickerProps.value} />
      </ButtonEdit>
    );
  }
});
