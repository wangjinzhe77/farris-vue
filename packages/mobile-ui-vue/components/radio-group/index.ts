import { withInstall } from '@components/common';
import RadioGroupInstallless from './src/radio-group.component';

export * from './src/radio-group.props';

const RadioGroup = withInstall(RadioGroupInstallless);

export { RadioGroup };
export default RadioGroup;
