import { ExtractPropTypes, PropType } from 'vue';
import {CheckerShape, CheckerShapeMap } from '@components/checker';
import { CheckboxGroupContext, checkboxGroupProps } from '@components/checkbox-group';

export const RADIO_GROUP_NAME = 'fm-radio-group';

export const radioGroupProps = {
  ...checkboxGroupProps,

  shape: { type: String as PropType<CheckerShape>, default: CheckerShapeMap.Round },
  
  modelValue: { type: [String, Number] , default: '' }
};

export type RadioGroupProps = ExtractPropTypes<typeof radioGroupProps>;

type Merge<F, S> = {
  [P in  keyof F | keyof S]: P extends keyof S ? S[P] : P extends keyof F ? F[P] : never
};

export type RadioGroupContext = Merge<CheckboxGroupContext, { props: RadioGroupProps; updateChecked: (value: string | number) => void }>;
