/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SetupContext, computed, defineComponent, ref, watch } from 'vue';
import { useBem, useLink } from '@components/common';
import { useGroupItems } from '@components/checkbox-group';
import Radio from '@components/radio';
import {
  RADIO_GROUP_NAME,
  RadioGroupContext,
  RadioGroupProps,
  radioGroupProps
} from './radio-group.props';

export default defineComponent({
  name: RADIO_GROUP_NAME,
  props: radioGroupProps,
  setup(props: RadioGroupProps, context: SetupContext) {
    const { emit, slots } = context;

    const innerValue = ref(props.modelValue);
    watch(
      () => props.modelValue,
      (newValue) => {
        innerValue.value = newValue;
      }
    );

    const updateChecked = (value: string | number) => {
      innerValue.value = value;
      emit('update:modelValue', value);
    };

    const getChecked = (name: any) => {
      return innerValue.value === name;
    };

    const { setParent } = useLink(RADIO_GROUP_NAME);
    setParent<RadioGroupContext>({ props, getChecked, updateChecked });

    const { bem } = useBem(RADIO_GROUP_NAME);

    const { renderGroupItems } = useGroupItems(props, Radio);

    const radioGroupClass = computed(() => {
      return [bem(), bem('', props.direction)];
    });

    return () => (
      <div class={radioGroupClass.value}>
        {slots?.default ? slots.default() : renderGroupItems()}
      </div>
    );
  }
});
