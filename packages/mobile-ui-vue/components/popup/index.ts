import { withInstall } from '@components/common';
import PopupInstallless from './src/popup.component';

const Popup = withInstall(PopupInstallless);

export * from './src/popup.props';
export { Popup };
export default Popup;
