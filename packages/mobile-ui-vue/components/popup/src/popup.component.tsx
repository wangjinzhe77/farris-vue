import {
  CSSProperties,
  defineComponent,
  Transition,
  Teleport,
  watch,
  ref,
  computed,
  SetupContext
} from 'vue';
import { getSlideTransitionName, useBem, useClickAway, useLockScroll } from '@components/common';
import Overlay from '@components/overlay';
import { PopupProps, popupProps, POPUP_NAME } from './popup.props';

export default defineComponent({
  name: POPUP_NAME,
  inheritAttrs: false,
  props: popupProps,
  emits: ['click', 'opened', 'closed', 'update:show'],
  setup(props: PopupProps, context: SetupContext<('click' | 'opened' | 'closed' | 'update:show')[]>) {
    const { emit, slots, attrs } = context;

    const closePopup = () => {
      console.log('closePopup');
      props.show && emit('update:show', false);
    };

    const popupRef = ref<HTMLElement>();
    const clickAwayActive = ref(false);
    props.useClickAway && useClickAway(popupRef, closePopup, { active: clickAwayActive });

    const [lockScroll, unlockScroll] = useLockScroll(() => props.lockScroll);

    const handleShow = () => {
      lockScroll();
      setTimeout(() => {
        clickAwayActive.value = true;
      });
    };
    const handleClose = () => {
      unlockScroll();
      clickAwayActive.value = false;
    };

    watch(
      () => props.show,
      (newValue) => {
        if (newValue) {
          handleShow();
        } else {
          handleClose();
        }
      }
    );

    const { bem } = useBem(POPUP_NAME);

    const popupClass = computed(() => {
      const className = [bem()];
      props.round && className.push(bem('', 'round'));
      props.position && className.push(bem('', props.position));
      props.popupClass && className.push(props.popupClass);
      return className;
    });

    const popupStyle = computed<CSSProperties>(() => {
      return {
        width: props.width ? props.width : undefined,
        height: props.height ? props.height : undefined,
        zIndex: props.zIndex !== undefined ? +props.zIndex : undefined,
        transitionDuration: `${props.duration}s`
      };
    });

    const slideTransitionName = computed(() => {
      return getSlideTransitionName(props.position);
    });

    const renderTransition = () => {
      return (
        <Transition
          name={props.transition || slideTransitionName.value}
          onAfterEnter={() => emit('opened')}
          onAfterLeave={() => emit('closed')}>
          <div v-show={props.show} ref={popupRef} style={popupStyle.value} class={popupClass.value} {...attrs}>
            {slots.default?.()}
          </div>
        </Transition>
      );
    };

    const handleOverlayClick = (event: Event) => {
      event.stopPropagation();
      props.closeOnClickOverlay && closePopup();
    };

    const overlayIndex = computed(() => {
      return props.zIndex !== undefined ? Number(props.zIndex) - 1 : undefined;
    });

    const renderOverlay = () => {
      if (props.overlay) {
        return (
          <Overlay
            style={props.overlayStyle}
            class={props.overlayClass}
            show={props.show}
            zIndex={overlayIndex.value}
            duration={props.duration}
            onClick={handleOverlayClick}
          />
        );
      }
    };

    return () => {
      if (props.teleport) {
        return (
          <Teleport to={props.teleport}>
            {renderOverlay()}
            {renderTransition()}
          </Teleport>
        );
      }
      return (
        <>
          {renderOverlay()}
          {renderTransition()}
        </>
      );
    };
  }
});
