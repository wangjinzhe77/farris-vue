 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType, TeleportProps } from 'vue';

export const POPUP_NAME = 'fm-popup';

export const popupProps = {
  show: { type: Boolean, default: false },

  width: { type: String, default: '' },

  height: { type: String, default: '' },

  position: { type: String, default: 'center' },

  duration: { type: Number, default: undefined },

  zIndex: { type: Number, default: undefined },

  popupClass: { type: String, default: '' },

  overlayClass: { type: String, default: '' },

  overlayStyle: { type: String, default: '' },

  transition: { type: String, default: '' },

  useNativeBack: { type: Boolean, default: false },

  teleport: { type: [String, Object] as PropType<TeleportProps['to']>, default: '' },

  overlay: { type: Boolean, default: true },

  closeOnClickOverlay: { type: Boolean, default: true },

  lockScroll: { type: Boolean, default: true },

  round: { type: Boolean, default: false },

  useClickAway: { type: Boolean, default: true }
};

export type PopupProps = ExtractPropTypes<typeof popupProps>;
