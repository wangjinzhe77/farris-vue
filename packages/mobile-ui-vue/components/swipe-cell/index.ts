import { withInstall } from '@components/common';
import SwipeCellInstallless from './src/swipe-cell.component';

export * from './src/swipe-cell.props';

const SwipeCell = withInstall(SwipeCellInstallless);

export { SwipeCell };
export default SwipeCell;
