/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { SwipeCellButton } from './composition/type';

export const SWIPE_CELL_NAME = 'fm-swipe-cell';

export const swipeCellProps = {

  /** 左侧的按钮列表 */
  leftButtons: { type: Array as PropType<SwipeCellButton[]>, default: [] },

  /** 右侧的按钮列表 */
  rightButtons: { type: Array as PropType<SwipeCellButton[]>, default: [] },

  /** 是否在点击按钮时自动归位，`left`和`right`插槽同样被视为按钮 */
  closeOnClickButton: { type: Boolean, default: true },

  /** 是否在触摸其它区域时自动归位 */
  closeOnTouchOutside: { type: Boolean, default: true },

  /** 是否在点击中间内容区域时自动归位 */
  closeOnClickCell: { type: Boolean, default: true },

  /** 是否禁用滑动 */
  disabled: { type: Boolean, default: false },

  /** 过渡动画的执行时长，单位`ms` */
  transitionDuration: { type: Number, default: 500 },

  /** 触发自动滑出/关闭的滑动距离百分比阈值，取值范围`(0, 1)` */
  threshold: { type: Number, default: 0.2 },

  /** 是否阻止滑动事件冒泡 */
  touchMoveStopPropagation: { type: Boolean, default: false },
};

export type SwipeCellProps = ExtractPropTypes<typeof swipeCellProps>;
