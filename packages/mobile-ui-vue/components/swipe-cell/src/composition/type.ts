import type { VNode } from 'vue';

export interface SwipeCellButton {

  /** 按钮文本 */
  text?: string;

  /** 图标 */
  icon?: string | VNode;

  /** 自定义类名 */
  className?: string;

  /** 自定义样式 */
  style?: string;

  /** 点击事件回调方法 */
  onClick?: (close: (() => void)) => void;

  [key: string]: any;
}

export type SwipeCellSide = 'left' | 'right';

export type SwipeCellClickPosition = 'left' | 'right' | 'cell' | 'outside';
