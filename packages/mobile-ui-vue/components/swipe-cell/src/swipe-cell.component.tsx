/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  defineComponent,
  SetupContext,
  ref,
  Ref,
  reactive,
  isVNode,
  h,
  onMounted,
  computed,
} from 'vue';
import { SWIPE_CELL_NAME, swipeCellProps, SwipeCellProps } from './swipe-cell.props';
import {
  useBem,
  useTouch,
  useRect,
  useEventListener,
  useClickAway,
} from '@components/common';
import { preventDefault } from '@components/common';
import { SwipeCellSide, SwipeCellClickPosition, SwipeCellButton } from './composition/type';
import Icon from '@components/icon';

export default defineComponent({
  name: SWIPE_CELL_NAME,

  props: swipeCellProps,

  emits: ['click', 'open', 'close'],

  setup(props: SwipeCellProps, context: SetupContext) {
    const { bem } = useBem(SWIPE_CELL_NAME);
    const { emit, slots, expose } = context;

    const rootRef = ref<HTMLElement>();
    const leftSideRef = ref<HTMLElement>();
    const rightSideRef = ref<HTMLElement>();

    const touch = useTouch();

    const state = reactive({
      /** 左侧内容的宽度 */
      leftSideWidth: 0,
      /** 右侧内容的宽度 */
      rightSideWidth: 0,
      /** 拖拽开始时的偏移距离 */
      startOffset: 0,
      /** 拖拽的偏移距离 */
      offset: 0,
      /** 是否正在滑动 */
      moving: false,
      /** 是否已打开 */
      opened: false,
    });

    /** 滑动时禁用点击 */
    const forbidClick = ref(false);

    const getElementWidth = (elementRef: Ref<HTMLElement | undefined>) => {
      return elementRef.value ? useRect(elementRef).width : 0;
    };

    const setSideContentWidth = () => {
      state.leftSideWidth = getElementWidth(leftSideRef);
      state.rightSideWidth = getElementWidth(rightSideRef);
    };

    onMounted(() => {
      setSideContentWidth();
    });

    const keepInRange = (num: number, min: number, max: number) => {
      return Math.min(Math.max(num, min), max);
    };

    const open = (side: SwipeCellSide) => {
      state.offset = side === 'left' ? state.leftSideWidth : -state.rightSideWidth;

      if (!state.opened) {
        state.opened = true;
        emit('open', side);
      }
    };

    const close = (clickPosition?: SwipeCellClickPosition) => {
      const side: SwipeCellSide = state.offset > 0 ? 'left' : 'right';
      state.offset = 0;

      if (state.opened) {
        state.opened = false;
        emit('close', { side, clickPosition });
      }
    };

    const handleSwipeEnd = () => {
      const side: SwipeCellSide = state.offset > 0 ? 'left' : 'right';
      const sideWidth = side === 'left' ? state.leftSideWidth : state.rightSideWidth;
      const offset = Math.abs(state.offset);
      const toggleThreshold = props.threshold || 0.2;
      const openThreshold = state.opened ? 1 - toggleThreshold : toggleThreshold;
      const shouldOpen = !!sideWidth && offset > sideWidth * openThreshold;
      if (shouldOpen) {
        open(side);
      } else {
        close();
      }
    };

    const onTouchStart = (event: TouchEvent) => {
      if (props.disabled) {
        return;
      }
      setSideContentWidth();
      state.startOffset = state.offset;
      touch.start(event);
    };

    const onTouchMove = (event: TouchEvent) => {
      if (props.disabled) {
        return;
      }
      touch.move(event);
      if (!touch.isHorizontal()) {
        return;
      }
      state.moving = true;
      forbidClick.value = true;
      const { deltaX } = touch;
      const isOpening = !state.opened;
      const isClosing = deltaX.value * state.startOffset < 0;
      const shouldPreventDefault = isOpening || isClosing;
      if (shouldPreventDefault) {
        preventDefault(event, props.touchMoveStopPropagation);
      }
      const newOffset = state.startOffset + deltaX.value;
      state.offset = keepInRange(newOffset, -state.rightSideWidth, state.leftSideWidth);
    };

    const onTouchEnd = () => {
      if (!state.moving) {
        return;
      }
      state.moving = false;
      handleSwipeEnd();
      // 解决在PC端浏览器松手后触发点击事件的问题
      setTimeout(() => {
        forbidClick.value = false;
      });
    };

    const onClick = (position: SwipeCellClickPosition, button?: SwipeCellButton) => {
      if (forbidClick.value) {
        return;
      }
      if (button && button.onClick) {
        const closeFunc = () => close(position);
        button.onClick(closeFunc);
      }
      emit('click', { position, button });
      const fromSideButton = position === 'left' || position === 'right';
      const fromOutside = position === 'outside';
      const fromCell = position === 'cell';
      if (
        fromSideButton && props.closeOnClickButton
        || fromOutside && props.closeOnTouchOutside
        || fromCell && props.closeOnClickCell
      ) {
        close(position);
      }
    };

    const getClickHandler = (position: SwipeCellClickPosition, button?: SwipeCellButton) => {
      return (event: MouseEvent) => {
        if (
          position === 'left'
          || position === 'right'
          || position === 'cell' && forbidClick.value
        ) {
          event.stopPropagation();
        }
        onClick(position, button);
      };
    };

    const renderButtonIcon = (button: SwipeCellButton) => {
      const { icon } = button;
      if (typeof icon === 'string') {
        return (
          <Icon class={bem('icon')} name={icon} />
        );
      }
      if (isVNode(icon)) {
        return h(icon, { class: bem('icon') });
      }
    };

    const renderSideButtons = (side: SwipeCellSide, buttons: SwipeCellButton[]) => {
      return buttons.map((button, index) => (
        <div
          class={[bem('button'), button.className]}
          style={button.style}
          onClick={getClickHandler(side, button)}
          key={index}
        >
          {renderButtonIcon(button)}
          <span class={bem('text')}>{button.text}</span>
        </div>
      ));
    };

    const renderSideContent = (side: SwipeCellSide, elementRef: Ref<HTMLElement | undefined>) => {
      const sideContentSlot = slots[side];
      const slotParam = { close: () => close(side) };
      const sideButtons = side === 'left' ? props.leftButtons : props.rightButtons;
      const hasSideButtons = !!sideButtons && !!sideButtons.length;
      if (!sideContentSlot && !hasSideButtons) {
        return;
      }
      return (
        <div
          ref={elementRef}
          class={bem(side)}
          onClick={getClickHandler(side)}
        >
          {sideContentSlot && sideContentSlot(slotParam)}
          {!sideContentSlot && renderSideButtons(side, sideButtons)}
        </div>
      );
    };

    expose({
      open,
      close: () => close(),
    });

    useEventListener('touchstart', onTouchStart, { target: rootRef, passive: true });
    useEventListener('touchmove', onTouchMove, { target: rootRef });
    useEventListener('touchend', onTouchEnd, { target: rootRef });
    useEventListener('touchcancel', onTouchEnd, { target: rootRef });

    useEventListener('click', getClickHandler('cell'), { target: rootRef });

    useClickAway(rootRef, () => onClick('outside'), { eventName: 'touchstart' });

    const wrapperStyle = computed(() => {
      return {
        transform: `translate3d(${state.offset}px, 0, 0)`,
        transitionDuration: state.moving ? '0ms' : `${+props.transitionDuration}ms`,
      };
    });

    return () => (
      <div ref={rootRef} class={bem()}>
        <div class={bem('wrapper')} style={wrapperStyle.value}>
          {renderSideContent('left', leftSideRef)}
          {slots.default?.()}
          {renderSideContent('right', rightSideRef)}
        </div>
      </div>
    );
  }
});
