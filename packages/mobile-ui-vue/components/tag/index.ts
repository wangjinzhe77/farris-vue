import { withInstall } from '@components/common';
import TagInstallless from './src/tag.component';

export * from './src/tag.props';

const Tag = withInstall(TagInstallless);

export { Tag };
export default Tag;
