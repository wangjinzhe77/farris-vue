import { ExtractPropTypes, PropType } from 'vue';

export const TAG_NAME = 'fm-tag';

export type TagType = 'primary' | 'success' | 'danger' | 'warning';

export type TagSize = 'large' | 'medium';

export const tagProps = {
  type: { type: String as PropType<TagType>, default: 'primary' },

  color: { type: String, default: undefined },

  textColor: { type: String, default: undefined },

  size: { type: String as PropType<TagSize>, default: undefined },

  show: { type: Boolean, default: true },

  plain: { type: Boolean, default: false },

  round: { type: Boolean, default: false },

  mark: { type: Boolean, default: false },

  closeable: { type: Boolean, default: false },
};

export type TagProps = ExtractPropTypes<typeof tagProps>;
