import { computed, CSSProperties, defineComponent, Transition } from 'vue';
import { TAG_NAME, TagProps, tagProps } from './tag.props';
import { stopPropagation, useBem } from '@/components/common';
import { Icon } from '@/components/icon';

export default defineComponent({
  name: TAG_NAME,
  props: tagProps,
  emits: ['update:modelValue', 'close', 'click'],
  setup(props: TagProps, context) {
    const {emit, slots } = context;

    const handleClick = (event: Event)=> {
      stopPropagation(event);
      emit('click', event);
    };

    const handleClose = (event: Event)=> {
      emit('close', event);
    };

    const { bem } = useBem(TAG_NAME);

    const tgaClass = computed(()=>{
      return [
        bem(),
        bem('', props.type),
        props.size && bem('', props.size),
        props.round && bem('', 'round'),
        props.mark && bem('', 'mark'),
        props.plain && bem('', 'plain'),
      ];
    });
    const tagStyle = computed<CSSProperties>(()=>{
      return {
        color: props.textColor,
        backgroundColor: props.color,
      };
    });

    const renderTag = () => (
      props.show && <span class={tgaClass.value} style={tagStyle.value} onClick={handleClick}>
        {slots.default && slots.default() }
        { props.closeable && <Icon class={bem('close')} name='s-cross' onClick={handleClose}></Icon> }
      </span>
    );

    return () => (
      props.closeable ? <Transition name='fm-fade'>
        {renderTag()}
      </Transition> : renderTag()
    );
  }
});
