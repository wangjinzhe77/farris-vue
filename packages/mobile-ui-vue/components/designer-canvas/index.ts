import type { App } from 'vue';
import FDesignerCanvas from './src/designer-canvas.component';
import FToolbox from './src/components/toolbox.component';
import FControlTreeView from './src/components/control-tree-view.component';

export * from './src/composition/props/designer-canvas.props';

export { FDesignerCanvas };

export default {
  install(app: App): void {
    app.component(FDesignerCanvas.name as string, FDesignerCanvas);
    app.component(FToolbox.name as string, FToolbox);
    app.component(FControlTreeView.name as string, FControlTreeView);
  }
};
