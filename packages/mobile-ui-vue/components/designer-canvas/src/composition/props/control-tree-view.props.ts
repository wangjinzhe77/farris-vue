import { ExtractPropTypes } from 'vue';

export const controlTreeViewProps = {
  /** 树表数据 */
  data: { type: Object, default: [] },
  /** 树节点图标数据 */
  treeNodeIconsData: { type: [Object, String], default: {} },
  /** 树表是否支持可选状态 */
  selectable: { type: Boolean, default: false },
  /** 选中父节点会自动选中子节点 */
  autoCheckChildren: { type: Boolean, default: true },
  /** 返回值展示父节点和子节点 */
  cascade: { type: Boolean, default: true },
  /** 是否显示图标集 */
  showTreeNodeIcons: { type: Boolean, default: false },
  /** 是否显示连接线 */
  showLines: { type: Boolean, default: false },
  /** 新增值 */
  newDataItem: { type: Object, default: {} },
  /** 连接线颜色 */
  lineColor: { type: String, default: '#9399a0' },
  /** 单元格高度 */
  cellHeight: { type: Number, default: 28 },
};

export type ControlTreeViewProps = ExtractPropTypes<typeof controlTreeViewProps>;
