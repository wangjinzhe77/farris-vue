import { ExtractPropTypes } from "vue";

export const designerItemProps = {
  id: { type: String, default: '' },
  componentId: { type: String, default: '' },
  type: { type: String, default: '' },
  canDelete: { type: Boolean, default: true },
  canMove: { type: Boolean, default: true },
  canSelectParent: { type: Boolean, default: true },
  customButtons: { type: Array<any>, default: [] },
  customClass: { type: String, default: '' },
  customStyle: { type: String, default: '' },
  /**
   * 组件值
   */
  modelValue: { type: Object },
  ignore: { type: Boolean, default: false }
} as Record<string, any>;

export type DesignerItemPropsType = ExtractPropTypes<typeof designerItemProps>;
