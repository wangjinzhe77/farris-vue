import { ExtractPropTypes } from "vue";

export const designerPlaceholderProps = {
  id: { type: String }
};

export type DesignerPlaceholderPropsType = ExtractPropTypes<typeof designerPlaceholderProps>;
