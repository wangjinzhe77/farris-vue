 
 
import { SetupContext } from 'vue';
import { ControlTreeViewProps } from '../props/control-tree-view.props';

/** 创建设计器 */
export function createDesigner(props: ControlTreeViewProps, context: SetupContext) {

  function createDesignBuilder(domJson: any) {
    // 画布渲染过程中，可能会在控件上补充属性，这种变更用户不感知，所以不需要通知IDE框架

    const elements = document.getElementsByClassName('editorPanel');
    let rootElement: Element;
    if (elements && elements.length) {
      rootElement = elements[0];
    }

    const childComponents = domJson.module.components.slice(1);
    const rootComponent = domJson.module.components[0];
    const designerSchema = {
      contents: [rootComponent]
    };
  }

  return {};

};
