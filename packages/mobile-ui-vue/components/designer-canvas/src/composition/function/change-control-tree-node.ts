import { SetupContext } from 'vue';
import { ControlTreeViewProps } from '../props/control-tree-view.props';
import { BuilderHTMLElement } from '../entity/builder-element';
import { DesignerItemContext } from '../../types';

/** 变更控件树节点后的操作 */
export function changeTreeNode(props: ControlTreeViewProps, context: SetupContext) {

  /** 点击左侧控件树 */
  function onComponentClicked(e?: PointerEvent) {
    Array.from(document.getElementsByClassName('dgComponentFocused') as HTMLCollectionOf<HTMLElement>).forEach(
      (element: HTMLElement) => element.classList.remove('dgComponentFocused')
    );
    const designId = `${e}-design-item`;
    const curSelectedElement = document.getElementById(designId) as any;

    if (designId) {
      const currentSelectedElements = document.getElementsByClassName('dgComponentSelected') as HTMLCollectionOf<BuilderHTMLElement>;
      const returnValue = curSelectedElement?.componentInstance.value.getPropConfig();

      // 重复点击
      const duplicateClick = currentSelectedElements &&
        currentSelectedElements.length === 1 &&
        currentSelectedElements[0] === curSelectedElement;
      if (!duplicateClick) {
        const array = Array.from(currentSelectedElements);
        array.map((element) => {
          element?.classList.remove('dgComponentSelected');
          if (element.componentInstance && element.componentInstance.afterComponentCancelClicked) {
            element.componentInstance.afterComponentCancelClicked(e);
          }
        });
        curSelectedElement?.classList.add('dgComponentFocused');
        const { designItemContext } = curSelectedElement;
        if (designItemContext) {
          const { schema } = (designItemContext as DesignerItemContext).componentInstance.value;
          context.emit('selectionChanged', schema);
          const draggabledesignerItemElementRef =
            designItemContext.componentInstance.value.getDraggableDesignItemElement(designItemContext);
          if (draggabledesignerItemElementRef && draggabledesignerItemElementRef.value) {
            draggabledesignerItemElementRef.value.classList.add('dgComponentSelected');
          }
        } else {
          curSelectedElement?.classList.add('dgComponentSelected');
        }
      }
      return returnValue;
    }
  }
  return {
    onComponentClicked
  };
}
