import { Ref } from "vue";
import { ComponentSchema, DesignerComponentInstance, ResolveComponentContext } from "../types";

export interface DesignerHTMLElement extends HTMLElement {
  /** 记录各子元素对应的控件schema json的集合，用于container类dom节点 */
  contents?: ComponentSchema[];

  /** 记录element对应的component实例，用于单个component节点 */
  componentInstance: Ref<DesignerComponentInstance>;

  schema: ComponentSchema;
}

export interface UseDragula {
  attachComponents: (element: HTMLElement, component: ComponentSchema) => void;

  attachToolbox: () => void;

  initializeDragula: (containerElement: DesignerHTMLElement) => void;
}

/**
 * 拖拽上下文
 */
export interface DraggingResolveContext {

  sourceElement?: DesignerHTMLElement;
  sourceContainer?: DesignerHTMLElement;
  targetContainer?: DesignerHTMLElement;

  /**
   * 拖拽来源
   * @summary
   * 控件工具箱control/实体树字段field/实体树实体entity/ 现有控件移动位置move
   */
  sourceType?: 'control' | 'field' | 'entity' | 'move';

  /**
   * 拖拽控件类型
   */
  controlType?: string | null;

  /**
   * 拖拽控件的中文类型名称
   */
  controlTypeName?: string | null;
  /**
   * 拖拽控件分类
   */
  controlCategory?: string | null;

  /**
   * 绑定对象类型（字段/实体/小部件）
   */
  bindingType?: 'field' | 'entity' | 'widget';

  /**
   * 目标容器的组件实例
   */
  parentComponentInstance?: any;

  /**
   * 要添加的控件Schema
   */
  componentSchema?: any;

  /** 工具箱中的控件，启用的特性 */
  controlFeature?: any;

}

export interface UseDesignerRules {
  /**
   * 判断是否可以接收拖拽新增的子级控件
   */
  canAccepts(draggingContext: DraggingResolveContext): boolean;

  /**
   * 判断当前容器是否是固定的上下文的中间层级
   */
  checkIsInFixedContextRules?(): boolean;

  /**
   * 判断控件上下文环境，用于后续判断控件是否可移动、可删除、是否有默认间距、线条
   */
  resolveComponentContext?(): void;

  getStyles?(): string;

  getDesignerClass?(): string;

  onResolveNewComponentSchema?: (resolveContext: ResolveComponentContext, compnentSchema: ComponentSchema) => ComponentSchema;

  /**
   * 容器接收新创建的子控件
   */
  onAcceptNewChildElement?: (element: DesignerHTMLElement, targetPosition: number, compnentSchema: ComponentSchema) => ComponentSchema;

  /**
   * 移动控件后事件：在可视化设计器中，容器接收控件后调用此事件
   */
  onAcceptMovedChildElement?: (sourceElement: DesignerHTMLElement) => void;

  /**
   * 判断是否支持移动组件
   */
  checkCanMoveComponent?(): boolean;

  /**
   * 判断是否支持删除组件
   */
  checkCanDeleteComponent?(): boolean;

  /**
   * 判断是否隐藏组件间距和线条
   */
  hideNestedPaddingInDesginerView?(): boolean;

  /** 接收控件属性信息 */

  getPropsConfig?(schema?: any): any;

  /**
   * 组件在拖拽时是否需要将所属的Component一起拖拽，用于form、data-grid等控件的拖拽
   */
  triggerBelongedComponentToMoveWhenMoved?: Ref<boolean>;
  /**
   * 组件在被移除时是否需要将所属的Component一起移除，用于form、data-grid等控件的拖拽
   */
  triggerBelongedComponentToDeleteWhenDeleted?: Ref<boolean>;
}
