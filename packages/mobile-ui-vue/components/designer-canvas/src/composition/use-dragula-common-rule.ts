import { DraggingResolveContext } from "./types";

export function useDragulaCommonRule() {

  /**
   * 容器类控件的基础控制规则
   */
  function basalDragulaRuleForContainer(draggingContext: DraggingResolveContext): boolean {
    if (!draggingContext) {
      return false;
    }

    /** 目标容器的组件实例 */
    const targetCmpInstance = draggingContext.targetContainer?.componentInstance &&
      draggingContext.targetContainer.componentInstance.value;
    if (!targetCmpInstance) {
      return false;
    }

    const targetCmpInstanceClass = targetCmpInstance.schema.appearance?.class;

    // 能够接收输入类控件的只有Form控件和布局容器
    if (draggingContext.controlCategory === 'input' || draggingContext.controlType === 'form-group') {
      const targetContainerType = targetCmpInstance.schema.type;
      const isFormContainer = (targetContainerType === 'response-form' || targetContainerType === 'content-container')
        && targetCmpInstanceClass && targetCmpInstanceClass.includes('farris-form');
      if (targetCmpInstance.schema.type !== 'response-layout-item' && !isFormContainer) {
        return false;
      }
    }

    // 不接收卡片内小分组
    if (draggingContext.controlType === 'field-set') {
      return false;
    }

    return true;
  }

  return {
    basalDragulaRuleForContainer
  };
}
