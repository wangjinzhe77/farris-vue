export const DgControl = {
  Button: { type: 'Button', name: '按钮', icon: 'Button' },

  ButtonGroup: { type: 'ButtonGroup', name: '按钮组', icon: 'ButtonGroup' },

  ToolBar: { type: 'ToolBar', name: '工具栏', icon: 'ButtonGroup' },

  ToolBarItem: { type: 'ToolBarItem', name: '工具栏项', icon: 'Button' },

  ContentContainer: { type: 'ContentContainer', name: '容器', icon: 'ContentContainer' },

  DisplayField: { type: 'DisplayField', name: '标签', icon: 'DisplayField' },

  TextBox: { type: 'TextBox', name: '文本', icon: 'TextBox' },

  MultiTextBox: { type: 'MultiTextBox', name: '多行文本', icon: 'MultiTextBox' },

  LookupEdit: { type: 'LookupEdit', name: '帮助', icon: 'LookupEdit' },

  NumericBox: { type: 'NumericBox', name: '数值', icon: 'NumericBox' },

  DateBox: { type: 'DateBox', name: '日期', icon: 'DateBox' },

  SwitchField: { type: 'SwitchField', name: '开关', icon: 'SwitchField' },

  RadioGroup: { type: 'RadioGroup', name: '单选组', icon: 'RadioGroup' },

  CheckBox: { type: 'CheckBox', name: '复选框', icon: 'CheckBox' },

  CheckGroup: { type: 'CheckGroup', name: '复选框组', icon: 'CheckGroup' },

  EnumField: { type: 'EnumField', name: '下拉列表', icon: 'EnumField' },
  /** 暂时没用 */
  FlexLayout: { type: 'FlexLayout', name: '弹性布局' },
  /** 暂时没用 */
  FlowLayout: { type: 'FlowLayout', name: '流布局' },

  ResponseLayout: { type: 'ResponseLayout', name: '布局容器', icon: 'ResponseLayout3' },

  ResponseLayoutItem: { type: 'ResponseLayoutItem', name: '布局', icon: 'ResponseLayout1' },

  TreeGrid: { type: 'TreeGrid', name: '树表格', icon: 'TreeGrid' },

  TreeGridField: { type: 'TreeGridField', name: '树表格列' },

  FieldSet: { type: 'FieldSet', name: '分组', icon: 'FieldSet' },

  Form: { type: 'Form', name: '卡片面板', icon: 'Form' },

  QueryForm: { type: 'QueryForm', name: '查询面板', icon: 'Form' },

  DataGrid: { type: 'DataGrid', name: '表格', icon: 'DataGrid' },

  GridField: { type: 'GridField', name: '表格列' },

  Panel: { type: 'Panel', name: '面板', icon: 'ContentContainer' },

  Module: { type: 'Module', name: '模块', icon: 'Module' },

  Component: { type: 'Component', name: '组件', icon: 'Component' },

  ExternalContainer: { type: 'ExternalContainer', name: '外部容器', icon: 'ContentContainer' },

  Image: { type: 'Image', name: '图像', icon: 'Image' },

  ImageUpload: { type: 'ImageUpload', name: '图片上传', icon: 'imageupload' },

  HiddenContainer: { type: 'HiddenContainer', name: '隐藏区域', icon: 'ContentContainer' },

  ModalContainer: { type: 'ModalContainer', name: '弹窗容器', icon: 'ContentContainer' },

  RouteContainer: { type: 'RouteContainer', name: '路由区域', icon: 'ContentContainer' },

  Tab: { type: 'Tab', name: '标签页', icon: 'Tab' },

  TabPage: { type: 'TabPage', name: '标签页项', dependentParentControl: 'Tab' },

  TabToolbarItem: { type: 'TabToolbarItem', name: '标签页工具栏按钮', icon: 'Button' },

  Tag: { type: 'Tag', name: 'Tag', icon: 'Tag' },

  Sidebar: { type: 'Sidebar', name: '侧边栏', icon: 'Sidebar' },

  HtmlTemplate: { type: 'HtmlTemplate', name: '模版容器', icon: 'HtmlTemplate' },

  ListView: { type: 'ListView', name: '列表', icon: 'ListView' },

  RichTextBox: { type: 'RichTextBox', name: '富文本', icon: 'RichTextBox' },

  TimeSpinner: { type: 'TimeSpinner', name: '时间调节器', icon: 'TimePicker' },

  TimePicker: { type: 'TimePicker', name: '时间选择', icon: 'TimePicker' },

  Section: { type: 'Section', name: '分组面板', icon: 'Section' },

  SectionToolbar: { type: 'SectionToolbar', name: '分组面板工具栏' },

  SectionToolbarItem: { type: 'SectionToolbarItem', name: '分组面板按钮' },

  QueryScheme: { type: 'QueryScheme', name: '筛选方案', icon: 'QueryScheme' },

  FormHeader: { type: 'FormHeader', name: '翻页' },

  Splitter: { type: 'Splitter', name: '分栏面板', icon: 'Splitter' },

  SplitterPane: { type: 'SplitterPane', name: '分栏面板项', dependentParentControl: 'Splitter' },

  WizardDetail: { type: 'WizardDetail', name: '向导详情页' },

  WizardDetailContainer: { type: 'WizardDetailContainer', name: '向导详情容器' },

  Wizard: { type: 'Wizard', name: '向导', icon: 'Wizard' },

  MultiSelect: { type: 'MultiSelect', name: '数据分配', icon: 'MultiSelect' },

  InputGroup: { type: 'InputGroup', name: '智能输入框', icon: 'InputGroup' },

  Steps: { type: 'Steps', name: '步骤条', icon: 'Steps' },

  Avatar: { type: 'Avatar', name: '头像', icon: 'Avatar' },

  ListFilter: { type: 'ListFilter', name: '筛选条', icon: 'ListFilter' },

  ListNav: { type: 'ListNav', name: '列表导航', icon: 'ListNav' },

  NumberRange: { type: 'NumberRange', name: '数字区间选择', icon: 'NumericBox' },

  Scrollspy: { type: 'Scrollspy', name: '滚动监听', icon: 'Scrollspy' },

  LanguageTextBox: { type: 'LanguageTextBox', name: '多语言输入框', icon: 'LanguageTextBox' },

  ComponentRef: { type: 'ComponentRef', name: '组件引用节点' },

  FileUpload: { type: 'FileUpload', name: '附件上传', icon: 'FileUpload' },

  FilePreview: { type: 'FilePreview', name: '附件预览', icon: 'FilePreview' },

  ViewChange: { type: 'ViewChange', name: '多视图切换', icon: 'Button' },

  MultiViewContainer: { type: 'MultiViewContainer', name: '多视图', icon: 'MultiViewContainer' },

  MultiViewItem: { type: 'MultiViewItem', name: '多视图项', dependentParentControl: 'MultiViewContainer' },

  Footer: { type: 'Footer', name: '页脚' },

  DiscussionEditor: { type: 'DiscussionEditor', name: '评论编辑区', icon: 'DiscussionEditor' },

  DiscussionList: { type: 'DiscussionList', name: '评论列表', icon: 'DiscussionList' },

  NavTab: { type: 'NavTab', name: '标签类导航', icon: 'NavTab' },

  Tags: { type: 'Tags', name: '标记组', icon: 'Tags' },

  Portlet: { type: 'Portlet', name: '小部件', icon: 'dingzhi' },

  Header: { type: 'Header', name: '页头', icon: 'Header' },

  ModalFooter: { type: 'ModalFooter', name: '弹窗页脚', icon: 'ModalFooter' },

  ScrollCollapsibleArea: { type: 'ScrollCollapsibleArea', name: '滚动收折区域', icon: 'ScrollCollapsibleArea' },

  PersonnelSelector: { type: 'PersonnelSelector', name: '人员选择', icon: 'PersonnelSelector' },

  Table: { type: 'Table', name: '表格', icon: 'DataGrid' },

  LoopContainer: { type: 'LoopContainer', name: '循环容器', icon: 'ContentContainer' },

  FileUploadPreview: { type: 'FileUploadPreview', name: '附件上传预览', icon: 'FileUpload' },

  DynamicArea: { type: 'DynamicArea', name: '动态区域', icon: 'ContentContainer' },

  DynamicAreaItem: { type: 'DynamicAreaItem', name: '动态区域项', icon: 'ContentContainer' },

  TabToolbar: { type: 'TabToolbar', name: '标签页工具栏', icon: 'TabToolbar' },

  HeaderToolBar: { type: 'HeaderToolBar', name: '头部组件工具栏', icon: 'HeaderToolBar' },

  ModalFooterToolBar: { type: 'ModalFooterToolBar', name: '底部组件工具栏', icon: 'ModalFooterToolBar' },

  HeaderToolBarItem: { type: 'HeaderToolBarItem', name: '头部组件工具栏按钮', icon: 'HeaderToolBarItem' },

  ModalFooterToolBarItem: { type: 'ModalFooterToolBarItem', name: '底部组件工具栏按钮', icon: 'ModalFooterToolBarItem' },

  OrganizationSelector: { type: 'OrganizationSelector', name: '组织选择', icon: 'OrganizationSelector' },

  AdminOrganizationSelector: { type: 'AdminOrganizationSelector', name: '组织选择', icon: 'OrganizationSelector' },

  EmployeeSelector: { type: 'EmployeeSelector', name: '人员选择', icon: 'PersonnelSelector' },

  OaRelation: { type: 'OaRelation', name: '关联行政审批', icon: 'TextBox' },

  CitySelector: { type: 'CitySelector', name: '城市选择', icon: 'CitySelector' },

  ExtIntergration: { type: 'ExtIntergration', name: '外部服务集成', icon: 'ViewModel' },

  AppointmentCalendar: { type: 'AppointmentCalendar', name: '预约日历', icon: 'DateBox' },

  /** 查询类 start */
  Charts: { type: 'Charts', name: '图表', icon: 'Charts' },

  QdpFramework: { type: 'QdpFramework', name: '查询结果工具栏', icon: 'QdpFramework' },

  SpreadSheet: { type: 'SpreadSheet', name: '查询表格控件', icon: 'Charts' },

  QdpConditionDialog: { type: 'QdpConditionDialog', name: '查询筛选对话框', icon: 'ContentContainer' },

  QdpConditionDialogTab: { type: 'QdpConditionDialogTab', name: '查询筛选标签页', icon: 'Tab' },
  /** 查询类 end */

  /** 审批类 start */
  ApprovalLogs: { type: 'ApprovalLogs', name: '审批记录', icon: 'ApprovalLogs' },

  ApprovalComments: { type: 'ApprovalComments', name: '审批意见', icon: 'shenpiyijian' },
  /** 审批类 end */
};
