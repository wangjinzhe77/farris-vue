import PageContainer from '../../../page-container';
import PageBodyContainer from '../../../page-body-container';
import PageHeaderContainer from '../../../page-header-container';
import PageFooterContainer from '../../../page-footer-container';
import ListView from '../../../list-view';

const componentMap: Record<string, any> = {};
const componentPropsConverter: Record<string, any> = {};
const componentPropertyConfigConverter: Record<string, any> = {};

PageContainer.registerDesigner(componentMap, componentPropsConverter, componentPropertyConfigConverter);
PageBodyContainer.registerDesigner(componentMap, componentPropsConverter, componentPropertyConfigConverter);
PageHeaderContainer.registerDesigner(componentMap, componentPropsConverter, componentPropertyConfigConverter);
PageFooterContainer.registerDesigner(componentMap, componentPropsConverter, componentPropertyConfigConverter);
ListView.registerDesigner(componentMap, componentPropsConverter, componentPropertyConfigConverter);

export { componentMap, componentPropsConverter, componentPropertyConfigConverter };
