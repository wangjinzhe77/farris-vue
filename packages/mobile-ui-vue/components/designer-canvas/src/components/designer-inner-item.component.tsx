import { Ref, SetupContext, computed, defineComponent, inject, onMounted, provide, ref, watch, onBeforeUnmount } from 'vue';
import { DesignerInnerItemPropsType, designerInnerItemProps } from '../composition/props/designer-inner-item.props';
import { UseDragula } from '../composition/types';
import { ComponentSchema, DesignerComponentInstance, DesignerItemContext, ResolveComponentContext } from '../types';
import { canvasChanged, setPositionOfBtnGroup } from '../composition/designer-canvas-changed';
import { useDesignerInnerComponent } from '../composition/function/use-designer-inner-component';

const FDesignerInnerItem = defineComponent({
  name: 'FDesignerInnerItem',
  props: designerInnerItemProps,
  emits: ['selectionChange'],
  setup(props: DesignerInnerItemPropsType, context: SetupContext) {
    const canMove = ref(props.canMove);
    const canAdd = ref(props.canAdd);
    const canDelete = ref(props.canDelete);
    const canNested = ref(false);
    const contentKey = ref(props.contentKey);
    const childType = ref(props.childType);
    const childLabel = ref(props.childLabel);
    const schema = ref(props.modelValue);
    const designComponentStyle = ref('');
    const designerItemElementRef = ref();
    const useDragulaComposition = inject<UseDragula>('canvas-dragula');
    const componentInstance = ref() as Ref<DesignerComponentInstance>;
    const parent = inject<DesignerItemContext>('design-item-context');
    const designItemContext = { designerItemElementRef, componentInstance, schema: schema.value, parent, setupContext: context };
    provide<DesignerItemContext>('design-item-context', designItemContext);

    const designerItemClass = computed(() => {
      const classObject = {
        'farris-component': true,
        'position-relative': canMove.value || canDelete.value,
        'farris-nested': canNested.value,
        'can-move': canMove.value,
        'd-none': designerItemElementRef.value && (designerItemElementRef.value as HTMLElement).classList.contains('d-none')
      } as Record<string, boolean>;
      return classObject;
    });

    const desginerItemStyle = computed(() => {
      const styleObject = {} as Record<string, any>;
      if (designComponentStyle.value) {
        designComponentStyle.value.split(';').reduce((result: Record<string, any>, styleString: string) => {
          const [styleKey, styleValue] = styleString.split(':');
          if (styleKey) {
            result[styleKey] = styleValue;
          }
          return result;
        }, styleObject);
      }
      return styleObject;
    });

    function onClickDeleteButtom(payload: MouseEvent, schemaToRemove: ComponentSchema) {
      if (parent && parent.schema[contentKey.value]) {
        const indexToRemove = parent.schema[contentKey.value].findIndex(
          (contentItem: ComponentSchema) => contentItem.id === schemaToRemove.id
        );
        parent.schema[contentKey.value].splice(indexToRemove, 1);
        parent.componentInstance.value.updateDragAndDropRules();
      }

    }

    function onClickAddButton(payload: MouseEvent) {
      if (componentInstance.value.addNewChildComponentSchema) {
        const resolveContext = {
          componentType: childType.value,
          label: childLabel.value,
          parentComponentInstance: componentInstance.value,
          targetPosition: -1
        } as ResolveComponentContext;
        const childComponentSchema = componentInstance.value.addNewChildComponentSchema(resolveContext);
        componentInstance.value.schema[contentKey.value].push(childComponentSchema);
      }
    }

    function renderAddButton() {
      return (
        canAdd.value && (
          <div
            role="button"
            class="btn component-settings-button"
            title="新增"
            ref="removeComponent"
            onClick={(payload: MouseEvent) => {
              onClickAddButton(payload);
            }}>
            <i class="f-icon f-icon-plus-circle"></i>
          </div>
        )
      );
    }

    function renderDeleteButton(componentSchema: ComponentSchema) {
      return (
        canDelete.value && (
          <div
            role="button"
            class="btn component-settings-button"
            title="删除"
            ref="removeComponent"
            onClick={(payload: MouseEvent) => {
              onClickDeleteButtom(payload, componentSchema);
            }}>
            <i class="f-icon f-icon-yxs_delete"></i>
          </div>
        )
      );
    }

    function renderIconPanel(componentSchema: ComponentSchema) {
      return (
        <div class="component-btn-group" data-noattach="true">
          <div>
            {renderAddButton()}
            {renderDeleteButton(componentSchema)}
          </div>
        </div>
      );
    }

    watch(
      () => props.modelValue,
      (value: any) => {
        schema.value = value;
      }
    );

    function updatePositionOfBtnGroup(e: Event) {
      const targetEl = e.target as any;
      setPositionOfBtnGroup(targetEl);
    }

    function bindingScrollEvent() {
      if (schema.value?.contents?.length && designerItemElementRef.value) {
        designerItemElementRef.value.addEventListener('scroll', updatePositionOfBtnGroup);
      }
    }

    function createDefaultComponentInstance() {
      const designerItemElement = designerItemElementRef.value as HTMLElement;
      const innerComponentElementRef = ref(designerItemElement.children[1] as HTMLElement);
      const innerComponentInstance = useDesignerInnerComponent(innerComponentElementRef, designItemContext);
      return innerComponentInstance.value;
    }

    onMounted(() => {
      if (designerItemElementRef.value) {
        const draggableContainer = designerItemElementRef.value.querySelector(
          `[data-dragref='${schema.value.id}-container']`
        );
        componentInstance.value = (draggableContainer && draggableContainer.componentInstance) ?
          draggableContainer.componentInstance.value : createDefaultComponentInstance();

        if (useDragulaComposition && draggableContainer) {
          useDragulaComposition.attachComponents(draggableContainer, schema.value);
        }
        canNested.value = componentInstance.value.canNested !== undefined ? componentInstance.value.canNested : canNested.value;
        // canDelete.value = componentInstance.value.canDelete !== undefined ? componentInstance.value.canDelete : canDelete.value;
        canMove.value = componentInstance.value.canMove !== undefined ? componentInstance.value.canMove : canMove.value;
        designComponentStyle.value = componentInstance.value.styles || '';
        if (designerItemElementRef.value) {
          designerItemElementRef.value.componentInstance = componentInstance;
          designerItemElementRef.value.designItemContext = designItemContext;
        }
      }
      bindingScrollEvent();

      canvasChanged.value++;
    });

    onBeforeUnmount(() => {
      if (designerItemElementRef.value) {
        designerItemElementRef.value.removeEventListener('scroll', updatePositionOfBtnGroup);
      }
    });

    function onClickDesignerItem(payload: MouseEvent) {
      Array.from(document.getElementsByClassName('dgComponentFocused') as HTMLCollectionOf<HTMLElement>).forEach(
        (element: HTMLElement) => element.classList.remove('dgComponentFocused')
      );
      if (payload) {
        payload.preventDefault();
        payload.stopPropagation();
      }
      const designerItemElement = designerItemElementRef.value as HTMLElement;
      if (designerItemElement) {
        const currentSelectedElements = document.getElementsByClassName('dgComponentSelected') as HTMLCollectionOf<HTMLElement>;
        // 重复点击
        const duplicateClick =
          currentSelectedElements &&
          currentSelectedElements.length === 1 &&
          currentSelectedElements[0] === designerItemElementRef.value;
        if (!duplicateClick) {
          Array.from(currentSelectedElements).forEach((element: HTMLElement) => element.classList.remove('dgComponentSelected'));

          designerItemElement.classList.add('dgComponentFocused');
          context.emit('selectionChange', schema.value.type, schema.value);
          const draggabledesignerItemElementRef = componentInstance.value.getDraggableDesignItemElement(designItemContext);
          if (draggabledesignerItemElementRef && draggabledesignerItemElementRef.value) {
            draggabledesignerItemElementRef.value.classList.add('dgComponentSelected');
          }
        }
      }

      canvasChanged.value++;
    }

    return () => {
      return (
        <div
          id={`${props.id}-design-item`}
          ref={designerItemElementRef}
          class={designerItemClass.value}
          style={desginerItemStyle.value}
          onClick={onClickDesignerItem}>
          {renderIconPanel(schema.value)}
          {context.slots.default && context.slots.default()}
        </div>
      );
    };
  }
});
export default FDesignerInnerItem;
