import { withInstall } from '@components/common';
import TextareaInstallless from './src/textarea.component';

export * from './src/textarea.props';

const Textarea = withInstall(TextareaInstallless);

export { Textarea };
export default Textarea;
