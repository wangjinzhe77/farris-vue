import { ExtractPropTypes } from 'vue';
import { inputCommonProps } from '@/components/input-group';

export const TEXTAREA_NAME = 'FmTextarea';

export const textareaProps = {
  ...inputCommonProps,

  autoSize: { type: Boolean, default: undefined },

  rows: { type: Number, default: undefined },

  maxHeight: { type: Number, default: undefined },

  minHeight: { type: Number, default: undefined },

  showWordLimit: { type: Boolean, default: undefined }
};

export type TextareaProps = ExtractPropTypes<typeof textareaProps>;
