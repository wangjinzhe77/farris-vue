/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { defineComponent } from 'vue';
import { useBem } from '@/components/common';
import InputGroup from "@/components/input-group";
import { TEXTAREA_NAME, textareaProps, TextareaProps } from './textarea.props';

export default defineComponent({
  name: TEXTAREA_NAME,
  props: textareaProps,
  setup(props: TextareaProps) {
    const { bem } = useBem(TEXTAREA_NAME);

    return () => (
      <InputGroup class={bem()} {...props} type="textarea" />
    );
  }
});
