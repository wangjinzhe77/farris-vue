import { withInstall } from '@components/common';
import PickerInstallless from './src/picker.component';

export * from './src/picker.props';
export * from './src/types';
export { PickerGroupKey } from './src/composition';

const Picker = withInstall(PickerInstallless);

export { Picker };
export default Picker;
