import { CSSProperties, SetupContext, computed, defineComponent } from 'vue';
import { addUnit, useBem } from '@components/common';
import { PICKER_NAME } from '../picker.props';
import { useColumnTouch } from '../composition';
import { ColumnProps, columnProps } from './column.props';

export default defineComponent({
  props: columnProps,
  emits: ['change'],
  setup(props: ColumnProps, context: SetupContext<('change')[]>) {
    const { bem } = useBem(PICKER_NAME);

    const {
      translateY,
      transition,
      onTouchStart,
      onTouchMove,
      onTouchEnd,
      onTransitionend
    } = useColumnTouch(props, context);

    const columnStyle = computed<CSSProperties>(() => {
      return {
        transform: `translateY(${addUnit(translateY.value)})`,
        transition: `all ${translateY.active ? transition.duration : 0}s ${transition.timingFunction} 0s`
      };
    });

    return () => {
      return (
        <div
          class={bem('column')}
          style={columnStyle.value}
          onTouchstart={onTouchStart}
          onTouchmove={onTouchMove}
          onTouchend={onTouchEnd}
          onTransitionend={onTransitionend}>
          {props.column.map((item) => (
            <div class={bem('column-item')}>
              <span>{item[props.textField]}</span>
            </div>
          ))}
        </div>
      );
    };
  }
});
