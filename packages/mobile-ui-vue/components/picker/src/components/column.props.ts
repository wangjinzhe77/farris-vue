import { PropType, ExtractPropTypes } from 'vue';
import { Column } from '../types';

export const columnProps = {
  column: { type: Array as PropType<Column>, default: () => [] },

  valueField: { type: String, default: 'value' },

  textField: { type: String, default: 'text' },

  index: { type: Number, default: 0 },

  value: { type: [String, Number, Boolean], default: '' },

  lineHeight: { type: Number, default: 0 },

  visiableOptionCount: { type: Number, default: 5 }
};

export type ColumnProps = ExtractPropTypes<typeof columnProps>;
