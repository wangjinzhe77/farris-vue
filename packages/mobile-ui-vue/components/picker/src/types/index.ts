import { ComponentInternalInstance, ComputedRef, Ref } from 'vue';

export type ValueType = string | number | boolean;

export type ColumnItem = {
  // eslint-disable-next-line no-use-before-define
  [key: string]: ValueType | Column;
};

export type Column = ColumnItem[];

export type Columns = Column[];

export type PickerChange = { index: number; value: ColumnItem };

export interface UsePickerState {
  innerValue: Ref<ValueType[]>;
  lineHeight: number;
  setValueByIndex: (index: number, value: ValueType) => void;
  getValueByIndex: (index: number) => ValueType;
}

export interface UseColumns {
  columns: ComputedRef<Columns>;
  getPickeds: () => ColumnItem[];
  getValue: () => ValueType[];
  getTextValue: () => string;
  setDefaultValue: () => void;
}

export type ColumnTranslate = {
  value: number;
  max: number;
  min: number;
  start: number;
  index: number;
  currentItem: any;
  active: boolean;
};
export type ColumnTransition = {
  property: string;
  duration: number;
  timingFunction: string;
  delay: number;
};

export interface UseColumnTouch {
  translateY: ColumnTranslate;
  transition: ColumnTransition;
  onTouchStart: (event: TouchEvent) => void;
  onTouchMove: (event: TouchEvent) => void;
  onTouchEnd: () => void;
  onTransitionend: () => void;
}

export type PickerContext = {
  name: number;
  instance: ComponentInternalInstance | null;
};

export type GroupItem = {name: number; title: string};

export type GroupContext = {
  addItem:  (item: GroupItem) => void;
  updateItem: (id: number, title: string) => void;
};
