/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CSSProperties, computed, defineComponent } from 'vue';
import { useBem, addUnit } from '@components/common';
import { PickerProps, pickerProps, PICKER_NAME } from './picker.props';
import { useColumns, usePickerState, usePickerContext } from './composition';
import ColumnComponent from './components/column.component';
import { PickerChange, ValueType } from './types';

export default defineComponent({
  name: PICKER_NAME,
  props: pickerProps,
  emits: ['change', 'confirm', 'cancel', 'update:modelValue'],
  setup(props: PickerProps, context) {
    const { emit, expose } = context;

    // add context to picker group
    usePickerContext(props);

    const pickerStateComposition = usePickerState(props);
    const { innerValue, lineHeight, setValueByIndex } = pickerStateComposition;

    const { columns, getPickeds, getValue, getTextValue, setDefaultValue } = useColumns(props, pickerStateComposition);

    expose({ getTextValue, getValue, getPickeds });

    const { bem } = useBem(PICKER_NAME);

    const onChange = (change: PickerChange) => {
      const { value, index } = change;
      emit('change', change);
      setValueByIndex(index, value[props.valueField] as ValueType);
    };

    const onConfirm = () => {
      setDefaultValue();
      emit('confirm', innerValue.value);
      emit('update:modelValue', innerValue.value);
    };

    const onCancel = () => {
      emit('cancel');
    };

    const renderToolbar = () => (
      <div class={bem('toolbar')}>
        <div class={bem('toolbar-left')} onClick={onCancel}>
          <span>{props.cancelText}</span>
        </div>
        <div class={bem('toolbar-title')}>
          <span>{props.title}</span>
        </div>
        <div class={bem('toolbar-right')} onClick={onConfirm}>
          <span>{props.confirmText}</span>
        </div>
      </div>
    );

    const columnsStyle = computed<CSSProperties>(() => {
      const columnsHeight = (props.visiableOptionCount + 1) * lineHeight;
      return {
        height: addUnit(columnsHeight)
      };
    });

    const maskStyle = computed<CSSProperties>(() => {
      const sizeY = (props.visiableOptionCount * lineHeight) / 2;
      return {
        backgroundSize: `100% ${addUnit(sizeY)}`
      };
    });

    const renderColumns = () => (
      <div class={bem('content')}>
        <div class={bem('columns')} style={columnsStyle.value}>
          {columns.value.map((column, index) => (
            <ColumnComponent
              index={index}
              value={innerValue.value[index]}
              column={column}
              textField={props.textField}
              valueField={props.valueField}
              lineHeight={lineHeight}
              visiableOptionCount={props.visiableOptionCount}
              onChange={onChange}></ColumnComponent>
          ))}
          <div class={bem('mask')} style={maskStyle.value}></div>
          <div class={bem('frame')}></div>
        </div>
      </div>
    );

    return () => (
      <div class={bem('')}>
        {props.showToolbar && renderToolbar()}
        {renderColumns()}
      </div>
    );
  }
});
