import { ExtractPropTypes, PropType } from 'vue';
import { Column, ColumnItem, Columns, ValueType } from "./types";

export const PICKER_NAME = 'FmPicker';

export const pickerProps = {
  modelValue: { type: [String, Array<ValueType>], default: '' },

  columns: { type: Array as PropType<Column | Columns>, default: () => [] },

  title: { type: String, default: '' },

  valueField: { type: String, default: 'value' },

  textField: { type: String, default: 'text' },

  childrenField: { type: String, default: 'children' },

  cancelText: { type: String, default: '取消' },

  confirmText: { type: String, default: '确定' },

  optionFormatter: { type: Function as PropType<(index: number, item: ColumnItem) => string>, default: undefined },

  visiableOptionCount: { type: Number, default: 5 },

  showToolbar: { type: Boolean, default: true }
};

export type PickerProps = ExtractPropTypes<typeof pickerProps>;
