import { SetupContext, reactive, watch } from 'vue';
import { getValue, preventDefault, useTouch, useMonmentum } from '@components/common';
import { ColumnProps } from '../components/column.props';
import { ColumnTransition, ColumnTranslate, UseColumnTouch, ValueType } from '../types';

const DefaultTransition: ColumnTransition = {
  property: 'all',
  duration: 0.3,
  timingFunction: 'cubic-bezier(0.17, 0.89, 0.45, 1)',
  delay: 0
};

export const useColumnTouch = (props: ColumnProps, context: SetupContext<('change')[]>): UseColumnTouch => {
  const halfLineHeight = props.lineHeight / 2;

  const momentum = useMonmentum({ maxOverflowY: 44 });

  const transition = reactive<ColumnTransition>({ ...DefaultTransition });

  const changeEmitter = (value: any) => {
    context.emit('change', { value, index: props.index });
  };

  const translateY = reactive<ColumnTranslate>({
    value: 0,
    max: 0,
    min: 0,
    start: 0,
    index: 0,
    currentItem: undefined,
    active: false
  });

  const setTranslateMinAndMax = () => {
    translateY.max = (props.visiableOptionCount / 2) * props.lineHeight;

    const columnHeight = props.column.length * props.lineHeight;
    translateY.min = translateY.max - columnHeight + props.lineHeight;

  };

  const setTranslateByIndex = (index = 0) => {
    setTranslateMinAndMax();
    translateY.index = index;
    translateY.value = translateY.max - index * props.lineHeight;
    translateY.currentItem = props.column[index];
  };

  const getIndexByValue = (value: ValueType) => {
    const maxIndex = props.column.length - 1;
    const defaultIndex = Math.min(translateY.index, maxIndex);
    const index = props.column.findIndex((item) => getValue(props.valueField, item) === value);
    return index >= 0 ? index : defaultIndex;
  };

  const getItemByIndex = (index: number) => {
    const maxIndex = props.column.length - 1;
    return translateY.index > maxIndex ? props.column[maxIndex] : props.column[index];
  };

  const updateTranslate = () => {
    const currentValue = getValue(props.valueField, translateY.currentItem);
    const currentIndex = getIndexByValue(currentValue);
    const realValue = getValue(props.valueField, getItemByIndex(translateY.index));
    setTranslateByIndex(currentIndex);
    if (currentValue !== realValue) {
      changeEmitter(translateY.currentItem);
    }
  };

  setTranslateByIndex(getIndexByValue(props.value));
  watch(() => props.column, updateTranslate);

  const setTranslateOnTouch = (y: number, max = translateY.max, min = translateY.min) => {
    const offsetY = translateY.start + y;

    translateY.value =
      offsetY > halfLineHeight ? Math.min(offsetY, max) : Math.max(offsetY, min);

    translateY.index = Math.round((translateY.max - translateY.value) / props.lineHeight);

    translateY.currentItem = getItemByIndex(translateY.index);
  };

  const touch = useTouch();

  const onTouchStart = (event: TouchEvent) => {
    touch.start(event);
    translateY.active = true;
    translateY.start = translateY.value;
    momentum.start(translateY.start);
    transition.duration = 0;
  };

  const onTouchMove = (event: TouchEvent) => {
    preventDefault(event, true);
    touch.move(event);
    setTranslateOnTouch(
      touch.deltaY.value,
      translateY.max + props.lineHeight,
      translateY.min - props.lineHeight
    );
    momentum.move(touch.deltaY.value);
    transition.duration = DefaultTransition.duration;
  };

  /**
   * 根据参数获得选中行合适y轴偏移量
   * @param translateY y轴偏移量
   * @returns number
   */
  const getRealTranslateY = (translateY: number) => {
    const isNegative = translateY < 0;
    const offset = translateY % props.lineHeight > halfLineHeight ? 1 : 0;
    const index = Math.floor(translateY / props.lineHeight);
    return (isNegative ? index - offset : index + offset) * props.lineHeight + halfLineHeight;
  };

  const onTouchEnd = () => {
    translateY.start = 0;
    const hasMomentum = momentum.end();
    let _translateY = 0;
    if (hasMomentum) {
      const { destination, duration } = momentum.get(translateY.value, translateY.min, translateY.max);
      _translateY = destination;
      transition.duration = duration / 1000;
    } else {
      _translateY = translateY.value;
    }
    setTranslateOnTouch(getRealTranslateY(_translateY));

    changeEmitter(translateY.currentItem);
  };

  const onTransitionend = () => {
    translateY.active = false;
  };

  return {
    translateY,
    transition,
    onTouchStart,
    onTouchMove,
    onTouchEnd,
    onTransitionend
  };
};
