import { getCurrentInstance, watch } from 'vue';
import { random, useParentContext } from '@components/common';
import { PickerProps } from '../picker.props';
import { GroupContext, PickerContext } from '../types';

export const PickerGroupKey = Symbol('PickerGroupKey');

export const usePickerContext = (props: PickerProps) => {
  const name = random(1000, 10000);
  const instance = getCurrentInstance();
  const context: PickerContext = { name, instance };

  const { parentContext: groupContext } = useParentContext<GroupContext, PickerContext>(PickerGroupKey, context);

  groupContext?.addItem({name, title: props.title});
  watch(() => props.title, (newTitle) => {
    groupContext?.updateItem(name, newTitle);
  });

  return {
    groupContext
  };
};
