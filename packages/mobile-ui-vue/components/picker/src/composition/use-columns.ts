import { computed, } from 'vue';
import { isArray, isFunction, isObject } from '@components/common';
import { PickerProps } from '../picker.props';
import { Column, ColumnItem, Columns, UseColumns, UsePickerState, ValueType } from '../types';

export const useColumns = (props: PickerProps, pickerState: UsePickerState): UseColumns => {
  const { innerValue, setValueByIndex, getValueByIndex } = pickerState;

  const getFirstColumnItem = (column: Column) => {
    return column && column[0];
  };

  const isMutiColumns = computed(() => {
    return props.columns?.length > 0 && isArray(props.columns[0]);
  });

  const isCascadeColumn = (column: Column) => {
    if (column?.length === 0) {
      return false;
    }
    const item = getFirstColumnItem(column);
    return item && isArray(item[props.childrenField]);
  };

  const getChildColumns = (column: Column, index: number): Columns => {
    const defaultItem = getFirstColumnItem(column);
    const parentValue = getValueByIndex(index);

    let parentItem = column.find((item) => item[props.valueField] === parentValue);
    parentItem = parentItem || defaultItem;

    const childColumn = (parentItem[props.childrenField] as Column) || [];

    if (isCascadeColumn(childColumn)) {
      const childColumns = getChildColumns(childColumn, index + 1);
      return [childColumn, ...childColumns];
    }
    return [childColumn];
  };

  const getCascadeColumns = () => {
    const column = props.columns as Column;
    const childs = getChildColumns(column, 0);
    return [column, ...childs];
  };

  const getFormatedText = (columnItem: ColumnItem, index: number) => {
    const text = columnItem[props.textField];
    return isFunction(props.optionFormatter) ? props.optionFormatter(index, columnItem) : text;
  };

  const formatColumn = (items: Column, index: number): any => {
    return items.map((item) => {
      const columnItem = isObject(item)
        ? item
        : { [props.valueField]: item, [props.textField]: item };

      columnItem[props.textField] = getFormatedText(columnItem, index);
      return columnItem;
    });
  };

  const columns = computed<Columns>(() => {
    let resultColumns: Columns = [];
    if (isMutiColumns.value) {
      resultColumns = props.columns as Columns;
    } else if (isCascadeColumn(props.columns as Column)) {
      resultColumns = getCascadeColumns();
    } else {
      resultColumns = [props.columns] as Columns;
    }
    return resultColumns.map(formatColumn);
  });

  const getPickeds = () => {
    return columns.value
      .map((column, index) => {
        const itemValue = innerValue.value[index];
        const item = column.find((item) => item[props.valueField] === itemValue);
        return item || getFirstColumnItem(column);
      });
  };

  const getValue = () => {
    return getPickeds()
      .filter(item => item)
      .map((item) => item[props.valueField] as ValueType);
  };

  const getTextValue = () => {
    return getPickeds()
      .filter(item => item)
      .map((item) => item[props.textField]).join('-');
  };

  const setDefaultValue = () => {
    columns.value.map((column, index) => {
      const itemValue = innerValue.value[index];
      const defaultValue = getFirstColumnItem(column)[props.valueField];
      const value = (itemValue || defaultValue) as ValueType;
      setValueByIndex(index, value);
    });
  };

  return {
    columns,
    getPickeds,
    getValue,
    getTextValue,
    setDefaultValue
  };
};
