import { ref, watch } from 'vue';
import { isArray, isDef, isString } from '@components/common';
import { PickerProps } from '../picker.props';
import { UsePickerState, ValueType } from '../types';

export const usePickerState = (props: PickerProps): UsePickerState => {
  const formatValue = (value: string | ValueType[]) =>
    isArray(value) ? value : isString(value) ? value.split(',') : isDef(value) ? [value] : [];

  const innerValue = ref(formatValue(props.modelValue));

  const setValueByIndex = (index: number, value: ValueType) => {
    innerValue.value[index] = value;
  };

  const getValueByIndex = (index: number) => {
    return innerValue.value[index];
  };

  watch(
    () => props.modelValue,
    (newValue) => {
      innerValue.value = formatValue(newValue);
    }
  );

  const lineHeight = 44;

  return {
    innerValue,
    lineHeight,
    setValueByIndex,
    getValueByIndex
  };
};
