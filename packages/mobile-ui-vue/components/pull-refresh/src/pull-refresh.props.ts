/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { LoadingProps } from '@components/loading';

export const PULL_REFRESH_NAME = 'fm-pull-refresh';

export const pullRefreshProps = {

  /** 是否处于加载中状态，支持语法糖`v-model` */
  modelValue: { type: Boolean, default: false },

  /** 顶部内容高度，单位是`px` */
  headHeight: { type: [Number, String], default: 50 },

  /** 触发刷新所需下拉的最小距离，默认值与`headHeight`相等 */
  pullDistance: { type: [Number, String], default: undefined },

  /** 最大下拉距离，小于等于0时不限制 */
  maxPullDistance: { type: [Number, String], default: 0 },

  /** 传递给加载中组件的属性 */
  loadingProps: { type: Object as PropType<LoadingProps>, default: {} },

  /** 下拉时的提示文本 */
  pullingText: { type: String, default: undefined },

  /** 释放时的提示文本 */
  loosingText: { type: String, default: undefined },

  /** 加载时的提示文本 */
  loadingText: { type: String, default: undefined },

  /** 加载完成的提示文本 */
  completeText: { type: String, default: undefined },

  /** 加载完成的提示文本的显示时长，单位`ms` */
  completeDuration: { type: [Number, String], default: 0 },

  /** 是否禁用 */
  disabled: { type: Boolean, default: false },

} as Record<string, any>;

export type PullRefreshProps = ExtractPropTypes<typeof pullRefreshProps>;
