/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext, ref, computed, nextTick, watch, toRef } from 'vue';
import { PULL_REFRESH_NAME, pullRefreshProps, PullRefreshProps } from './pull-refresh.props';
import { useBem, useScrollParent, useTouch, useEventListener } from '@components/common';
import { preventDefault } from '@components/common';
import Loading from '@components/loading';

type PullRefreshStatus = 'initial' | 'pulling' | 'loosing' | 'loading' | 'complete';

export default defineComponent({
  name: PULL_REFRESH_NAME,

  props: pullRefreshProps,

  emits: ['refresh', 'update:modelValue'],

  setup(props: PullRefreshProps, context: SetupContext) {
    const { bem } = useBem(PULL_REFRESH_NAME);
    const { emit, slots } = context;

    const root = ref<HTMLElement>();
    const track = ref<HTMLElement>();
    const scrollParent = useScrollParent(root);

    /** 是否处于加载状态 */
    const loading = toRef(props, 'modelValue');
    const setLoading = (newValue: boolean) => {
      emit('update:modelValue', newValue);
    };

    /** 下拉距离 */
    const distance = ref(0);
    /** 用户当前是否松手，下拉过程中应该取消过渡动画 */
    const isNotPulling = ref(true);
    /** 是否正在展示加载完成提示 */
    const isCompleteTipVisible = ref(false);

    const pullTriggerDistance = computed(() => +(props.pullDistance || props.headHeight));
    const maxPullDistance = computed(() => {
      return +props.maxPullDistance > 0 ? +props.maxPullDistance : Number.MAX_VALUE;
    });

    const status = computed<PullRefreshStatus>(() => {
      if (isCompleteTipVisible.value) {
        return 'complete';
      }
      if (loading.value) {
        return 'loading';
      }
      if (distance.value === 0) {
        return 'initial';
      }
      if (distance.value < pullTriggerDistance.value) {
        return 'pulling';
      }
      return 'loosing';
    });

    const canPullDown = computed<boolean>(() => {
      return !props.disabled && status.value !== 'loading' && status.value !== 'complete';
    });

    const touch = useTouch();

    const easePullDistance = (value: number): number => {
      const pullDistance = pullTriggerDistance.value;

      if (value > pullDistance) {
        if (value < pullDistance * 2) {
          value = pullDistance + (value - pullDistance) / 2;
        } else {
          value = pullDistance * 1.5 + (value - pullDistance * 2) / 4;
        }
      }

      return Math.round(Math.min(value, maxPullDistance.value));
    };

    const setDistance = (value: number, ease?: boolean) => {
      if (ease) {
        distance.value = easePullDistance(value);
      } else {
        distance.value = value;
      }
    };

    const isReachTop = () => {
      const scrollerEl = scrollParent.value;
      if (!scrollerEl) {
        return true;
      }
      const scrollTop = 'scrollTop' in scrollerEl ? scrollerEl.scrollTop : scrollerEl.pageYOffset;
      return scrollTop <= 0;
    };

    let reachTop = false;

    const initTouchState = (event: TouchEvent) => {
      reachTop = isReachTop();

      if (reachTop) {
        isNotPulling.value = false;
        touch.start(event);
      }
    };

    const onTouchStart = (event: TouchEvent) => {
      if (canPullDown.value) {
        initTouchState(event);
      }
    };

    const onTouchMove = (event: TouchEvent) => {
      if (canPullDown.value) {
        if (!reachTop) {
          initTouchState(event);
        }

        const { deltaY } = touch;
        touch.move(event);

        if (reachTop && deltaY.value >= 0 && touch.isVertical()) {
          preventDefault(event);
          setDistance(deltaY.value, true);
        }
      }
    };

    const onTouchEnd = () => {
      isNotPulling.value = true;
      if (reachTop && touch.deltaY.value && canPullDown.value) {
        if (status.value === 'loosing') {
          setDistance(+props.headHeight);
          setLoading(true);
          nextTick(() => emit('refresh'));
        } else {
          setDistance(0);
        }
      }
    };

    const getCustomStatusText = (currentStatus: PullRefreshStatus) => {
      return props[`${currentStatus}Text`];
    };

    const getStatusText = () => {
      const defaultStatusText = {
        pulling: '下拉刷新',
        loosing: '松手刷新',
        loading: '加载中...',
        complete: '刷新完成'
      };
      if (status.value === 'initial') {
        return '';
      }
      return getCustomStatusText(status.value) || defaultStatusText[status.value];
    };

    const renderStatusBar = () => {
      if (slots[status.value]) {
        return slots[status.value]({ distance: distance.value });
      }

      if (status.value === 'loading') {
        return <Loading size="16px" text={getStatusText()} {...props.loadingProps} />;
      }
      if (['pulling', 'loosing', 'complete'].includes(status.value)) {
        return <div class={bem('text')}>{getStatusText()}</div>;
      }
    };

    const trackStyle = computed(() => {
      return {
        transform: distance.value ? `translate3d(0, ${distance.value}px, 0)` : ''
      };
    });

    const headStyle = computed(() => {
      return {
        height: `${+props.headHeight}px`
      };
    });

    const showCompleteTip = () => {
      isCompleteTipVisible.value = true;

      setTimeout(() => {
        isCompleteTipVisible.value = false;
        setDistance(0);
      }, +props.completeDuration);
    };

    watch(
      () => props.modelValue,
      (value) => {
        if (value) {
          setDistance(+props.headHeight);
        } else if (+props.completeDuration) {
          showCompleteTip();
        } else {
          setDistance(0);
        }
      }
    );

    useEventListener('touchstart', onTouchStart, { target: track, passive: true });
    useEventListener('touchmove', onTouchMove, { target: track });
    useEventListener('touchend', onTouchEnd, { target: track });
    useEventListener('touchcancel', onTouchEnd, { target: track });

    return () => (
      <div ref={root} class={bem()}>
        <div
          ref={track}
          class={[bem('track'), isNotPulling.value && bem('track', 'not-pulling')]}
          style={trackStyle.value}>
          <div class={bem('head')} style={headStyle.value}>
            {renderStatusBar()}
          </div>
          {slots.default?.()}
        </div>
      </div>
    );
  }
});
