import { withInstall } from '@components/common';
import PullRefreshInstallless from './src/pull-refresh.component';

export * from './src/pull-refresh.props';

const PullRefresh = withInstall(PullRefreshInstallless);

export { PullRefresh };
export default PullRefresh;
