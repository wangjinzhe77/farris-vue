import { withInstall } from '@components/common';
import SwitchInstallless from "./src/switch.component";

const Switch = withInstall(SwitchInstallless);

export { Switch };
export default Switch;
