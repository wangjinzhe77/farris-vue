/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';

export const switchProps = {
  modelValue: { type: Boolean, default: false },

  disabled: { type: Boolean, default: false },

  readonly: { type: Boolean, default: false },

  loading: { type: Boolean, default: false },

  size: { type: Number, default: 22 },

  activeColor: { type: String, default: '' },

  inactiveColor: { type: String, default: '' },

  activeValue: { type: [Boolean, Number, String], default: true },

  inactiveValue: { type: [Boolean, Number, String], default: false }
};

export type SwitchProps = ExtractPropTypes<typeof switchProps>;
