/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CSSProperties, SetupContext, computed, defineComponent } from 'vue';
import { Loading } from '@components/loading';
import { addUnit, useBem } from '@components/common';
import { SwitchProps, switchProps } from './switch.props';

const name = 'fm-switch';

export default defineComponent({
  name,
  props: switchProps,
  emits: ['click', 'update:modelValue', 'change'] as (string[] & ThisType<void>) | undefined,
  setup(props: SwitchProps, context: SetupContext) {
    const { emit } = context;
    const { bem } = useBem(name);

    const checked = computed(() => {
      return props.modelValue === props.activeValue;
    });

    const onClick = (event: MouseEvent) => {
      emit('click', event);

      const shouldEmitValue = !props.disabled && !props.loading && !props.readonly;

      if (shouldEmitValue) {
        const innerValue = checked.value ? props.inactiveValue : props.activeValue;
        emit('update:modelValue', innerValue);
        emit('change', innerValue);
      }
    };

    const renderLoading = () => {
      return <Loading class={bem('loadding')} size={`${props.size / 2}px`} />;
    };

    const switchClass = computed(() => ({
      [bem()]: true,
      [bem('', 'on')]: checked.value,
      [bem('', 'loading')]: props.loading,
      [bem('', 'disabled')]: props.disabled,
      [bem('', 'readonly')]: props.readonly
    }));

    const switchSize = computed(() => {
      const height = props.size + 2;
      const width = height * 1.66;
      return {
        height,
        width
      };
    });

    const switchStyle = computed<CSSProperties>(() => {
      const { height, width } = switchSize.value;
      return {
        width: addUnit(width),
        height: addUnit(height),
        borderRadius: addUnit(width),
        backgroundColor: checked.value ? props.activeColor : props.inactiveColor
      };
    });

    const nodeStyle = computed<CSSProperties>(() => {
      const { height, width } = switchSize.value;
      const translateX = checked.value ? width - height : 0;
      return {
        width: addUnit(props.size),
        height: addUnit(props.size),
        transform: `translateX(${addUnit(translateX)})`
      };
    });

    return () => (
      <div class={switchClass.value} style={switchStyle.value} onClick={onClick}>
        <div class={bem('node')} style={nodeStyle.value}>
          {props.loading && renderLoading()}
        </div>
      </div>
    );
  }
});
