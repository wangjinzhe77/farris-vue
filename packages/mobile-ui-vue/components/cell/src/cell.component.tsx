/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { computed, defineComponent, SetupContext } from 'vue';
import { isDef, resolveAsset, useBem } from '@components/common';
import { Icon } from '@components/icon';
import { cellProps, CellProps, CELL_NAME } from './cell.props';

export default defineComponent({
  name: CELL_NAME,
  props: cellProps,
  emits: ['click', 'leftIconClick', 'rightIconClick', 'extraClick'],
  setup(props: CellProps, context: SetupContext<string[]>) {
    const { slots, emit } = context;

    const getLeftIconSlot = () => resolveAsset(slots, 'leftIcon');

    const showLeftIcon = computed(() => {
      return props.leftIcon || getLeftIconSlot();
    });

    const showTitle = computed(() => {
      return props.title || slots.title;
    });

    const showValue = computed(() => {
      return props.value || isDef(props.value) || slots.default;
    });

    const showLabel = computed(() => {
      return props.label || slots.label;
    });

    const getRightIconSlot = () => resolveAsset(slots, 'rightIcon');

    const showRightIcon = computed(() => {
      return props.rightIcon || getRightIconSlot() || props.isLink;
    });

    const showExtra = computed(() => {
      return props.extra || slots.extra;
    });

    const showRight = computed(() => {
      return showRightIcon.value || showExtra.value || props.value || slots.default || slots.footer;
    });

    const { bem } = useBem(CELL_NAME);

    const renderLeftIcon = () => {
      const leftIconSlot = getLeftIconSlot();
      return (
        <div
          class={[bem('left-icon'), props.leftIconClass]}
          onClick={(event) => emit('leftIconClick', event)}>
          {leftIconSlot ? (
            leftIconSlot()
          ) : (
            <Icon name={props.leftIcon ? props.leftIcon + '' : ''} />
          )}
        </div>
      );
    };

    const renderTitle = () => {
      return (
        <div class={[bem('title'), props.titleClass]} style={props.titleStyle}>
          <div class={bem('title-text')}>
            {slots.title ? slots.title() : <span>{props.title}</span>}
          </div>
          {showLabel.value && (
            <div class={bem('label')}>
              {slots.label ? slots.label() : <span>{props.label}</span>}
            </div>
          )}
        </div>
      );
    };

    const renderRightIcon = () => {
      const rightIconSlot = getRightIconSlot();
      return (
        <div
          class={[bem('right-icon'), props.rightIconClass]}
          onClick={(event) => emit('rightIconClick', event)}>
          {rightIconSlot ? (
            rightIconSlot()
          ) : (
            <Icon name={props.rightIcon ? props.rightIcon + '' : 's-arrow'} />
          )}
        </div>
      );
    };

    const renderExtra = () => {
      return (
        <div
          class={[bem('extra'), props.extraClass]}
          onClick={(event) => emit('extraClick', event)}>
          {slots.extra
            ? slots.extra()
            : props.extra && <span style="padding-left: 4px">{props.extra}</span>}
        </div>
      );
    };

    const rightClass = computed(() => {
      return {
        [bem('right')]: true,
        [bem('right', 'fill')]: showValue.value
      };
    });

    const valueClass = computed(() => {
      return {
        [bem('value')]: true,
        [bem('value', 'alone')]: !showTitle.value,
        [props.valueClass]: true
      };
    });

    const renderRight = () => {
      return (
        <div class={rightClass.value}>
          <div class={bem('right-content')}>
            {showValue.value && (
              <div class={valueClass.value}>
                {slots.default ? slots.default() : <span>{props.value}</span>}
              </div>
            )}
            {showRightIcon.value && renderRightIcon()}
            {showExtra.value && renderExtra()}
          </div>
          {slots.footer && <div class={bem('right-footer')}>{slots.footer()}</div>}
        </div>
      );
    };

    const cellClass = computed(() => {
      return {
        [bem()]: true,
        [bem('', 'noborder')]: !props.border,
        [bem('', 'required')]: props.required,
        [bem('', 'center')]: props.center,
        [bem('', 'clickable')]: props.clickable,
        [bem('', props.type)]: props.type
      };
    });

    return () => {
      return (
        <div class={cellClass.value} onClick={(event) => emit('click', event)}>
          {showLeftIcon.value && renderLeftIcon()}
          {showTitle.value && renderTitle()}
          {showRight.value && renderRight()}
        </div>
      );
    };
  }
});
