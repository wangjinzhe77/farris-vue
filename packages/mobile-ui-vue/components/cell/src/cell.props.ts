import { ExtractPropTypes } from 'vue';

export const CELL_NAME = 'fm-cell';

export const cellProps = {
  type: { type: String, default: 'form' },

  title: { type: String, default: '' },

  value: { type: [String, Number], default: '' },

  label: { type: String, default: '' },

  valueClass: { type: String, default: '' },

  titleClass: { type: String, default: '' },

  titleStyle: { type: String, default: '' },

  leftIconClass: { type: String, default: '' },

  rightIconClass: { type: String, default: '' },

  extraClass: { type: String, default: '' },

  border: { type: Boolean, default: true },

  required: { type: Boolean, default: false },

  center: { type: Boolean, default: false },

  isLink: { type: Boolean, default: false },

  clickable: { type: Boolean, default: false },

  leftIcon: { type: String, default: '' },

  rightIcon: { type: String, default: '' },

  extra: { type: String, default: '' }
};

export type CellProps = ExtractPropTypes<typeof cellProps>;
