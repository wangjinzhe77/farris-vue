import { withInstall } from '@components/common';
import CellInstallless from './src/cell.component';

const Cell = withInstall(CellInstallless);

export { Cell };
export default Cell;
