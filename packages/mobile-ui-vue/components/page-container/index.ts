 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { withInstall } from '@components/common';
import PageContainerInstallless from './src/page-container.component';
import PageContainerDesignInstallless from './src/designer/page-container.design.component';
import { propsResolver } from './src/page-container.props';

const COMPONENT_TYPE = 'page-container';
const PageContainer = withInstall(PageContainerInstallless);
const PageContainerDesign = withInstall(PageContainerDesignInstallless);

export * from './src/page-container.props';

export { PageContainer, PageContainerDesign };

export default {
  register(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
    componentMap[COMPONENT_TYPE] = PageContainer;
    propsResolverMap[COMPONENT_TYPE] = propsResolver;
  },
  registerDesigner(componentMap: Record<string, any>, propsResolverMap: Record<string, any>, configResolverMap: Record<string, any>): void {
    componentMap[COMPONENT_TYPE] = PageContainerDesign;
    propsResolverMap[COMPONENT_TYPE] = propsResolver;
  }
};
