import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import pageContainerSchema from './schema/page-container.schema.json';
import pageContainerPropertyConfig from './property-config/page-container.property-config.json';
import { schemaResolver } from './schema/schema-resolver';

export const PAGE_CONTAINER_NAME = 'fm-page-container';

export const pageContainerProps = {
  customClass: { type: String, default: '' },
};

export type PageContainerProps = ExtractPropTypes<typeof pageContainerProps>;

export const propsResolver = createPropsResolver(
  pageContainerProps,
  pageContainerSchema,
  schemaMapper,
  schemaResolver,
  pageContainerPropertyConfig,
);
