import { SetupContext, defineComponent, computed } from 'vue';
import { PAGE_CONTAINER_NAME, PageContainerProps, pageContainerProps } from './page-container.props';
import { useBem } from '@components/common';

export default defineComponent({
  name: PAGE_CONTAINER_NAME,
  props: pageContainerProps,
  emits: [],
  setup(props: PageContainerProps, context: SetupContext) {
    const { bem } = useBem(PAGE_CONTAINER_NAME);

    const containerClass = computed(() => ({
      [bem()]: true,
      [props.customClass || '']: true,
    }));

    return () => (
      <div class={containerClass.value}>
        {context.slots.default?.()}
      </div>
    );
  }
});
