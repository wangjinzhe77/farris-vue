import { nextTick, ref } from "vue";
import { DesignerHTMLElement, DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema } from "../../../designer-canvas/src/types";
import { PAGE_CONTAINER_NAME } from '../page-container.props';

export function useDesignerRules(schema: ComponentSchema, parentSchema?: ComponentSchema): UseDesignerRules {

  const triggerBelongedComponentToMoveWhenMoved = ref(false);

  const triggerBelongedComponentToDeleteWhenDeleted = ref(false);

  const hideNestedPadding = true;

  const isInFixedContextRules = true;

  function canAccepts(draggingContext: DraggingResolveContext): boolean {
    const acceptableControlTypes = [
      'page-header-container',
      'page-footer-container',
      'page-body-container',
      'float-container',
    ];
    const uniqueControlTypes = [
      'page-header-container',
      'page-footer-container',
      'page-body-container',
    ];
    const { controlType, parentComponentInstance } = draggingContext;
    if (!acceptableControlTypes.includes(controlType)) {
      return false;
    }
    const shouldBeUnique = uniqueControlTypes.includes(controlType);
    if (shouldBeUnique) {
      const parentComponent = parentComponentInstance?.value;
      const contents: any[] = parentComponent?.contents || [];
      const hasSameTypeControl = !!contents.find((content) => content.type === controlType);
      if (hasSameTypeControl) {
        return false;
      }
    }
    return true;
  }


  function checkCanMoveComponent() {
    return !isInFixedContextRules;
  }
  function checkCanDeleteComponent() {
    return !isInFixedContextRules;
  }

  function hideNestedPaddingInDesginerView() {
    return hideNestedPadding;
  }

  function getStyles(): string {
    return 'border-radius: 12px';
  }

  function getDesignerClass(): string {
    return PAGE_CONTAINER_NAME;
  }

  return {
    canAccepts,
    triggerBelongedComponentToMoveWhenMoved,
    triggerBelongedComponentToDeleteWhenDeleted,
    checkCanMoveComponent,
    checkCanDeleteComponent,
    hideNestedPaddingInDesginerView,
    getStyles,
    getDesignerClass,
  };
}
