import { SetupContext, defineComponent, inject, onMounted, ref } from 'vue';
import { PageContainerProps, pageContainerProps } from '../page-container.props';
import { useDesignerRules } from './use-designer-rules';
import { DesignerItemContext } from '../../../designer-canvas/src/types';
import { useDesignerComponent } from '../../../designer-canvas/src/composition/function/use-designer-component';

export default defineComponent({
  name: 'FmPageContainerDesign',
  props: pageContainerProps,
  emits: [],
  setup(props: PageContainerProps, context: SetupContext) {
    const elementRef = ref();
    const designItemContext = inject<DesignerItemContext>('design-item-context');
    const designerRulesComposition = useDesignerRules(designItemContext.schema, designItemContext.parent?.schema);
    const componentInstance = useDesignerComponent(elementRef, designItemContext, designerRulesComposition);

    onMounted(() => {
      elementRef.value.componentInstance = componentInstance;
    });

    context.expose(componentInstance.value);

    return () => {
      return (
        <div ref={elementRef} class="drag-container" data-dragref={`${designItemContext.schema.id}-container`}>
          {context.slots.default?.()}
        </div>
      );
    };
  }
});
