/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import FMToast from './toast.component';
import { isObject, inBrowser } from '@components/common';
import { mountComponent, usePopupState } from '@components/common';
import { App } from 'vue';

let instance;
let timer;
function defaultOptions() {
  return {
    type: 'default',
    message: '',
    icon: '',
    duration: 3000,
    position: 'middle',
    overlay: false
  };
}

function parseOptions(message) {
  return isObject(message) ? message : { message };
}

const initInstance = () => {
  ({ instance } = mountComponent({
    setup() {
      const { state, toggle } = usePopupState();
      return () => <FMToast {...{ ...state, 'onUpdate:show': toggle }} />;
    }
  }));
};

function Toast(options) {
  if (!inBrowser) {
    return;
  }

  if (!instance) {
    initInstance();
  }

  options = {
    ...Toast.currentOptions,
    ...parseOptions(options)
  };

  instance.open(options);
  clearTimeout(timer);

  if (options.duration > 0) {
    timer = setTimeout(Toast.clear, options.duration);
  }

  return instance;
}

Toast.clear = () => {
  if (instance) {
    instance.toggle(false);
  }
};

Toast.info = (options) => {
  return Toast({ ...parseOptions(options), type: 'info' });
};

Toast.success = (options) => {
  return Toast({ ...parseOptions(options), type: 'success' });
};

Toast.error = (options) => {
  return Toast({ ...parseOptions(options), type: 'error' });
};

Toast.warning = (options) => {
  return Toast({ ...parseOptions(options), type: 'warning' });
};

Toast.loading = (options) => {
  return Toast({ ...parseOptions(options), type: 'loading', duration: 0 });
};

Toast.currentOptions = defaultOptions();

Toast.setDefaultOptions = (options) => {
  Object.assign(Toast.currentOptions, options);
};

Toast.resetDefaultOptions = () => {
  Toast.currentOptions = defaultOptions();
};

Toast.install = (app: App) => {
  app.component(FMToast.name, FMToast);
  app.config.globalProperties.$toast = Toast;
};

Toast.Component = FMToast;

export default Toast;
