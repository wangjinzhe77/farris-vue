/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { SetupContext, computed, defineComponent } from 'vue';
import { ToastProps, toastProps } from './toast.props';
import { useBem } from '@components/common';
import { Icon } from '@components/icon';
import { Overlay } from '@components/overlay';

const name = 'fm-toast';

export default defineComponent({
  name,
  props: toastProps,
  emits: ['click', 'update:modelValue', 'change'],
  setup(props: ToastProps, context: SetupContext) {
    const { bem } = useBem(name);

    const iconName = computed(() => {
      if (props.icon) {
        return props.icon;
      }
      let name = '';
      switch (props.type) {
        case 'success':
          name = 'successfulhints';
          break;
        case 'info':
        case 'warning':
          name = 'warningmessage';
          break;
        case 'error':
          name = 'failureprompt';
          break;
        default:
          name = '';
          break;
      }
      return name;
    });

    const toastClass = computed(() => ({
      [name]: true,
      [bem('', props.type)]: true,
      [bem('', props.position)]: true
    }));

    const isLoadding = computed(() => {
      return props.type === 'loading';
    });

    const renderLoading = () => {
      return (
        <span class="fm-toast--loading-icon">
          <svg
            viewBox="25 25 50 50"
            class="fm-toast--loading-icon-circular"
            style={{ color: '#fff' }}>
            <circle cx="50" cy="50" r="20" fill="none" />
          </svg>
        </span>
      );
    };

    const renderIconContent = () => {
      const iconSize = '48';
      return (
        <div class={`${bem('icon-wrapper')}`}>
          <Icon name={iconName.value} size={iconSize} />
        </div>
      );
    };

    return () => (
      <>
        {props.show && (
          <div class={toastClass.value}>
            {isLoadding.value ? renderLoading() : iconName.value ? renderIconContent() : null}
            <span class="fm-toast--text">{props.message}</span>
          </div>
        )}
        {props.overlay && (
          <Overlay
            show={props.show}
            customStyle="{backgroundColor: 'rgba(255, 255, 255, 0)'}"></Overlay>
        )}
      </>
    );
  }
});
