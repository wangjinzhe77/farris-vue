/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';

type ToastType = 'success' | 'warning' | 'error' | 'info' | 'default' | 'loading';

export const toastProps = {
  show: {
    type: Boolean,
    default: false,
  },
  type: {
    type: String as PropType<ToastType>,
    default: 'info',
  },
  position: {
    type: String,
    default: 'middle',
  },
  message: {
    type: String,
    default: ''
  },
  icon: {
    type: String,
    default: ''
  },
  duration: {
    type: Number,
    default: 3000,
  },
  overlay: {
    type: Boolean,
    default: false,
  }
};

export type ToastProps = ExtractPropTypes<typeof toastProps>;
