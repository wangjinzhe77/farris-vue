import { withInstall } from '@components/common';
import TabBarInstallless from './src/tab-bar.component';

export * from './src/tab-bar.props';
export * from './src/components/tab-bar-item.props';

const TabBar = withInstall(TabBarInstallless);

export { TabBar };
export default TabBar;
