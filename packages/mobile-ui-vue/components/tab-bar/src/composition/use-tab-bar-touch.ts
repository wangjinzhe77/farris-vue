import { useMonmentum, useTouch } from "@components/common";
import { preventDefault } from "@components/common";
import { reactive } from "vue";

export const useTabBarTouch = () => {
  const maxOverflowX = 44;

  const touch = useTouch();

  const translate = reactive({
    x: 0,
    max: 0,
    min: 0,
    start: 0
  });

  const setTranslate = (x: number, min = translate.min, max = translate.max) => {
    translate.x = Math.min(Math.max(x, min), max);
  };

  const onTouchStart = (event: TouchEvent) => {
    touch.start(event);
    translate.start = translate.x;
  };

  const onTouchMove = (event: TouchEvent) => {
    preventDefault(event, true);
    touch.move(event);
    // momentum.move(touch.deltaY.value);
    const translateX = touch.deltaX.value + translate.start;
    setTranslate(translateX, translate.min - maxOverflowX, translate.max + maxOverflowX);
  };

  const onTouchEnd = () => {
    setTranslate(translate.x);
  };

  return {
    translate,
    setTranslate,
    onTouchStart,
    onTouchMove,
    onTouchEnd
  };
};
