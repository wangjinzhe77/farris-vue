/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { SetupContext, computed, defineComponent } from 'vue';
import { preventDefault, useBem } from '@components/common';
import FmIcon from '@components/icon';
import { TAB_BAR_ITEM_NAME, TabBarItemProps, tabBarItemProps } from './tab-bar-item.props';

export default defineComponent({
  name: TAB_BAR_ITEM_NAME,
  props: tabBarItemProps,
  emits: ['click'],
  setup(props: TabBarItemProps, context: SetupContext<string[]>) {
    const { bem } = useBem(TAB_BAR_ITEM_NAME);

    const iconState = computed(() => {
      return {
        name: props.active && props.activeIcon ? props.activeIcon : props.icon,
        color: props.active && props.activeIconColor ? props.activeIconColor : props.iconColor
      };
    });

    const renderIcon = () => {
      return (
        <div class={bem('icon')}>
          <FmIcon
            name={iconState.value.name}
            color={iconState.value.color}></FmIcon>
        </div>
      );
    };

    const renderInk = () => {
      return (
        <div class={bem('ink')}></div>
      );
    };

    const renderBadge = () => {
      return (
        <div class={bem('badge')}>{props.badge}</div>
      );
    };

    const shoudShowInk = computed(() => {
      return props.ink && props.active;
    });

    const renderText = () => {
      return (
        <div class={bem('text')}>
          {props.icon && renderIcon()}
          <span>{props.title}</span>
          {shoudShowInk.value && renderInk()}
          {props.badge && renderBadge()}
        </div>
      );
    };

    const handleClick = (event: MouseEvent) => {
      preventDefault(event, true);
      if (props.disabled) {
        return;
      }
      context.emit('click', event);
    };

    const tabbarItemClass = computed(() => {
      return {
        [bem()]: true,
        [bem('', 'actived')]: props.active,
        [bem('', 'dot')]: props.dot,
        [bem('', 'disabled')]: props.disabled
      };
    });

    return () => (
      <div class={tabbarItemClass.value} onClick={handleClick}>
        {renderText()}
      </div>
    );
  }
});
