/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';

export const TAB_BAR_ITEM_NAME = 'FmTabBarItem';

export const tabBarItemProps = {
  name: { type: [String, Number], default: '' },

  title: { type: String, default: '' },

  active: { type: Boolean, default: undefined },

  icon: { type: String, default: undefined },

  iconColor: { type: String, default: undefined },

  activeIcon: { type: String, default: undefined },

  activeIconColor: { type: String, default: undefined },

  dot: { type: Boolean, default: undefined },

  ink: { type: Boolean, default: undefined },

  badge: { type: [String, Number], default: undefined },

  disabled: { type: Boolean, default: undefined }
};
export type TabBarItemProps = ExtractPropTypes<typeof tabBarItemProps>;
