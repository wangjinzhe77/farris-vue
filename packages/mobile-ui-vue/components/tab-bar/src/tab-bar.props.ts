/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { TabBarItemProps } from './components/tab-bar-item.props';

export const TAB_BAR_NAME = 'fm-tab-bar';

export const tabBarProps = {
  modelValue: { type: [String, Number], default: '' },

  items: { type: Array as PropType<TabBarItemProps[]>, default: () => [] },

  ink: { type: Boolean, default: true },

  activeColor: { type: String, default: undefined },

  inactiveColor: { type: String, default: undefined },

  showBottomLine: { type: Boolean, default: true },

};

export type TabBarProps = ExtractPropTypes<typeof tabBarProps>;

export type { TabBarItemProps };
