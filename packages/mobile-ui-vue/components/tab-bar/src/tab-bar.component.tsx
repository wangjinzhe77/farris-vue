/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { SetupContext, computed, defineComponent, ref, onMounted, watch } from 'vue';
import { addUnit, useBem } from '@components/common';
import { TAB_BAR_NAME, TabBarItemProps, TabBarProps, tabBarProps } from './tab-bar.props';
import TabBarItem from './components/tab-bar-item.component';
import { useTabBarTouch } from './composition';

export default defineComponent({
  name: TAB_BAR_NAME,
  props: tabBarProps,
  setup(props: TabBarProps, context: SetupContext) {
    const innerValue = ref(props.modelValue);
    watch(
      () => props.modelValue,
      (newValue) => {
        innerValue.value = newValue;
      }
    );

    const listEndElement = ref<HTMLElement>();
    const wrapperElement = ref();

    const { translate, setTranslate, onTouchStart, onTouchMove, onTouchEnd } = useTabBarTouch();

    const setTranslateMin = () => {
      const wrapperWidth = wrapperElement.value.offsetWidth;
      const { right } = listEndElement.value.getBoundingClientRect();
      translate.min = wrapperWidth - right;
    };

    onMounted(() => {
      setTranslateMin();
    });

    const setTranslateXOnClick = (event: MouseEvent) => {
      const target = event.currentTarget as HTMLElement;
      const wrapperWidth = wrapperElement.value.offsetWidth;
      const { left, right, width } = target.getBoundingClientRect();
      if (left < 50) {
        setTranslate(translate.x + width);
      } else if(right + 50 > wrapperWidth){
        setTranslate(translate.x - width);
      }
    };

    const handleTabBarItemClick = (item: TabBarItemProps, event: MouseEvent) => {
      setTranslateXOnClick(event);
      innerValue.value = item.name;
      context.emit('update:modelValue', item.name);
      context.emit('change', item.name);
    };

    const checkTabBarItemActived = (item: TabBarItemProps) => {
      return innerValue.value === item.name;
    };

    const listStyle = computed(() => {
      return {
        transform: `translateX(${addUnit(translate.x)})`
      };
    });

    const { bem } = useBem(TAB_BAR_NAME);

    const renderTabBarList = () => {
      return (
        <div
          style={listStyle.value}
          class={bem('list')}
          onTouchstart={onTouchStart}
          onTouchmove={onTouchMove}
          onTouchend={onTouchEnd}>
          {props.items.map((item) => (
            <TabBarItem
              {...item}
              ink={props.ink}
              active={checkTabBarItemActived(item)}
              onClick={(event) => handleTabBarItemClick(item, event)}
            />
          ))}
          <div ref={listEndElement} class={bem('list-end')} />
        </div>
      );
    };

    const tabBarClass = computed(() => {
      return {
        [bem()]: true,
        [bem('', 'bottom-line')]: props.showBottomLine
      };
    });

    return () => (
      <nav class={tabBarClass.value}>
        <div ref={wrapperElement} class={bem('scroll-wrapper')}>
          {renderTabBarList()}
        </div>
      </nav>
    );
  }
});
