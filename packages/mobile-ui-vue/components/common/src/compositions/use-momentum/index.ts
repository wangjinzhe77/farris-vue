type MonmentumOption = {
  maxOverflowY?: number,
  TimeThreshold?: number,
  YThreshold?: number
}

export const useMonmentum = (options: MonmentumOption) => {
  const { maxOverflowY = 0, TimeThreshold = 200, YThreshold = 88 } = options;
  let startTime = 0;
  let momentumStartY = 0;
  let deltaY = 0;
  const start = (y: number) => {
    startTime = new Date().getTime();
    momentumStartY = y;
  };
  const move = (y: number) => {
    deltaY = y;
    const now = new Date().getTime();
    const duration = now - startTime;
    if (duration > TimeThreshold) {
      momentumStartY = deltaY;
      startTime = now;
    }
  };
  const end = () => {
    const now = new Date().getTime();
    const duration = now - startTime;
    const absDeltaY = Math.abs(deltaY - momentumStartY);
    if (duration < TimeThreshold && absDeltaY > YThreshold) {
      return true;
    }
    return false;
  };
  const get = (current: number, minY: number, maxY: number) => {
    const durationMap = {
      noBounce: 2500,
      weekBounce: 800,
      strongBounce: 400
    };
    const bezierMap = {
      noBounce: 'cubic-bezier(.17, .89, .45, 1)',
      weekBounce: 'cubic-bezier(.25, .46, .45, .94)',
      strongBounce: 'cubic-bezier(.25, .46, .45, .94)'
    };
    let type = 'noBounce';
    // 惯性滑动加速度
    const deceleration = 0.003;
    // 回弹阻力
    const bounceRate = 10;
    // 强弱回弹的分割值
    const bounceThreshold = 300;

    let overflowY = 0;
    const now = new Date().getTime();
    const duration = now - startTime;

    const distance = current - momentumStartY;
    const speed = (2 * Math.abs(distance)) / duration;
    let destination = current + (speed / deceleration) * (distance < 0 ? -1 : 1);
    if (destination < minY) {
      overflowY = minY - destination;
      type = overflowY > bounceThreshold ? 'strongBounce' : 'weekBounce';
      destination = Math.max(minY - maxOverflowY, minY - overflowY / bounceRate);
    } else if (destination > maxY) {
      overflowY = destination - maxY;
      type = overflowY > bounceThreshold ? 'strongBounce' : 'weekBounce';
      destination = Math.min(maxY + maxOverflowY, maxY + overflowY / bounceRate);
    }

    return {
      destination,
      duration: durationMap[type],
      bezier: bezierMap[type]
    };
  };
  return {
    start,
    move,
    end,
    get
  };
};
