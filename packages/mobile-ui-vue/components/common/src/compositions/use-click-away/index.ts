import { Ref, unref } from 'vue';
import { useEventListener } from '../use-event-listener';
import { inBrowser, initNodePath } from '../../utils';

export type ClickAwayOptions = {
  eventName?: string
  active?: Ref<boolean> | boolean
}

export function useClickAway(
  target: Element | Ref<Element | undefined>,
  listener: EventListener,
  options: ClickAwayOptions = {},
) {
  if (!inBrowser) {
    return;
  }

  const { eventName = 'click', active = true } = options;

  const isOutsideElement = (event: Event)=>{
    const _target = unref(target);
    if(!_target){
      return false;
    }
    return !_target.contains(event.target as Node);
  };

  const onClick = (event: Event) => {
    const _active = unref(active);

    const enableListener = _active && isOutsideElement(event);
    
    if (enableListener) {
      listener(event);
    }
  };
  useEventListener(eventName, onClick, { target: document });
}
