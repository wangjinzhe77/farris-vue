import { isArray, camelToKebabCase } from '../../utils';

export const useBem = (name: string) => {

  const snakeCaseName = camelToKebabCase(name);

  const getChildClass = (child: string | string[]) => {
    return isArray(child) ? child.join('__') : child;
  };

  const bem = (child?: string | string[], status?: string | string[]) => {
    const childClass = child ? `${snakeCaseName}__${getChildClass(child)}` : snakeCaseName;
    const stateClass = status ? `--${status}` : '';
    return childClass + stateClass;
  };
  return {
    bem
  };
};
