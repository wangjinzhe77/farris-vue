import { inject, provide } from "vue";

export const useLink = (key: string) => {
  const setParent = <T>(context: T)=>{
    provide(key, context);
  };
  const getParent = <T>(defaultValue?: any): T=>{
    return inject(key, defaultValue);
  };
  return {
    setParent,
    getParent
  };
};
