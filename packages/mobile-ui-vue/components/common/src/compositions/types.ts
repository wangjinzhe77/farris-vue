export interface ParentProvideValue<ParentContext, ChildContext> {
  addChild(childContext: ChildContext): void;
  removeChild(childContext: ChildContext): void;
  childContexts: Array<ChildContext>;
  parentContext: ParentContext;
};
