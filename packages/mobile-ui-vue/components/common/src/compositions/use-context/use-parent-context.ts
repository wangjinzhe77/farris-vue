import { inject, onUnmounted, InjectionKey } from 'vue';
import { ParentProvideValue } from '../types';

export function useParentContext<ParentContext, ChildContext>(
  key: InjectionKey<ParentProvideValue<ParentContext, ChildContext>>,
  childContext?: ChildContext
): { parentContext: ParentContext } {
  const parent = inject(key, null);
  if (!parent) {
    return {
      parentContext: null
    };
  }

  const { addChild, removeChild, parentContext } = parent;

  addChild(childContext);
  onUnmounted(() => {
    removeChild(childContext);
  });
  return {
    parentContext
  };
}
