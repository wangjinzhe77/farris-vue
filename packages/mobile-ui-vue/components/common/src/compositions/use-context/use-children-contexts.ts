import { provide, InjectionKey } from 'vue';
import { ParentProvideValue } from '../types';

export function useChildrenContexts<ParentContext, ChildContext>(
  parentKey: InjectionKey<ParentProvideValue<ParentContext, ChildContext>>,
  parentContext?: ParentContext
): { childContexts: Array<ChildContext> } {
  const childContexts: Array<ChildContext> = [];

  /**
   * 注册子上下文
   */
  function addChild(childContext: ChildContext): void {
    childContexts.push(childContext);
  }

  /**
   * 移除子上下文
   */
  function removeChild(childContext: ChildContext): void {
    const childIndex = childContexts.indexOf(childContext);
    if (childIndex === -1) {
      return;
    }
    childContexts.splice(childIndex, 1);
  }

  // 注入父上下文
  const provideValue: ParentProvideValue<ParentContext, ChildContext> = {
    addChild,
    removeChild,
    childContexts,
    parentContext
  };
  provide(parentKey, provideValue);

  return {
    childContexts
  };
}
