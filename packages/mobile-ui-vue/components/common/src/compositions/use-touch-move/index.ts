import { reactive, ref, Ref } from 'vue';
import { throttle } from '../../utils';
import { useEventListener } from '../use-event-listener';

const MIN_DISTANCE = 10;

type TouchDirection = 'horizontal' | 'vertical' | '';

export type TouchOffset = {
  startX: number;
  startY: number;
  endX: number;
  endY: number;
  offsetX: number;
  offsetY: number;
  direction: TouchDirection;
};

type TouchListener = {
  onTouchStart?: (event: Event) => void;
  onTouchMove?: (event: Event) => void;
  onTouchEnd?: (event: Event) => void;
  onTouchCancel?: (event: Event) => void;
};

function getDirection(x: number, y: number): TouchDirection {
  if (x > y && x > MIN_DISTANCE) {
    return 'horizontal';
  }

  if (y > x && y > MIN_DISTANCE) {
    return 'vertical';
  }
  return '';
}

const defaultTouchOffset: () => TouchOffset = () => {
  return {
    startX: 0,
    startY: 0,
    endX: 0,
    endY: 0,
    offsetX: 0,
    offsetY: 0,
    direction: ''
  };
};

export enum DragStatus {
  NotStarted,
  Started,
  Dragging,
  End
}

export function useTouchMove(
  target: Ref<EventTarget | undefined>,
  listener?: TouchListener,
  wait = 0
) {
  let beginOffset = { offsetX: 0, offsetY: 0 };
  const offset: TouchOffset = reactive(defaultTouchOffset());
  const dragStatus = ref(DragStatus.NotStarted);

  const touchStart = (event: any) => {
    if (!event.touches || !event.touches[0]) {
      return;
    }
    offset.startX = event.touches[0].clientX;
    offset.startY = event.touches[0].clientY;
    listener?.onTouchStart && listener.onTouchStart(event);
    dragStatus.value = DragStatus.Started;
  };

  const touchMove = (event: any) => {
    if (!event.touches || !event.touches[0]) {
      return;
    }
    const touch = event.touches[0];
    offset.offsetX = touch.clientX - offset.startX + beginOffset.offsetX;
    offset.offsetY = touch.clientY - offset.startY + beginOffset.offsetY;
    offset.endX = event.touches[0].clientX;
    offset.endY = event.touches[0].clientY;
    offset.direction = getDirection(offset.offsetX, offset.offsetY);
    listener?.onTouchMove && listener.onTouchMove(event);
    dragStatus.value = DragStatus.Dragging;
  };

  const touchEnd = (event: Event) => {
    beginOffset = { offsetX: offset.offsetX, offsetY: offset.offsetY };
    listener?.onTouchEnd && listener.onTouchEnd(event);
    listener?.onTouchCancel && listener.onTouchCancel(event);
    dragStatus.value = DragStatus.End;
  };

  const resetTouchStatus = () => {
    offset.endX = 0;
    offset.endY = 0;
    offset.offsetX = 0;
    offset.offsetY = 0;
    offset.direction = '';
    beginOffset = { offsetX: 0, offsetY: 0 };
    dragStatus.value = DragStatus.NotStarted;
  };

  if (target) {
    useEventListener('touchstart', touchStart, { target });
    useEventListener('touchmove', wait > 0 ? throttle(touchMove, wait) : touchMove, {
      target,
      passive: false
    });
    useEventListener('touchend', touchEnd, { target });
  }

  return {
    offset,
    dragStatus,
    resetTouchStatus
  };
}
