/* eslint-disable no-use-before-define */
import { ObjectDirective } from 'vue';

const DEFAULT_DELAY = 600;
const DEFAULT_THRESHOLD = 10;

export interface OnLongPressModifiers {
  stop?: boolean;
  once?: boolean;
  prevent?: boolean;
  capture?: boolean;
  self?: boolean;
  interceptClick?: boolean;
}

export interface OnLongPressOptions {

  delay?: number;

  modifiers?: OnLongPressModifiers;

  distanceThreshold?: number | false;
}

interface Position {
  x: number;
  y: number;
}

function addEventListener(el: HTMLElement, event: string, listener: any, options: any): () => void {
  el.addEventListener(event, listener, options);
  return () => el.removeEventListener(event, listener, options);
}

export function onLongPress(
  target: HTMLElement,
  handler: (evt: TouchEvent) => void,
  options?: OnLongPressOptions,
) {
  let timeout: ReturnType<typeof setTimeout> | undefined;
  let posStart: Position | undefined;

  function clear() {
    if (timeout) {
      clearTimeout(timeout);
      timeout = undefined;
    }
    posStart = undefined;
  }

  function onStart(ev: TouchEvent) {
    if (options?.modifiers?.self && ev.target !== target) {
      return;
    }

    clear();

    options?.modifiers?.prevent && ev.preventDefault();
    options?.modifiers?.stop && ev.stopPropagation();

    posStart = {
      x: ev.touches[0].pageX,
      y: ev.touches[0].pageY,
    };
    timeout = setTimeout(
      () => {
        timeout = undefined;
        handler(ev);
      },
      options?.delay ?? DEFAULT_DELAY,
    );
  }

  function onMove(ev: TouchEvent) {
    if (options?.modifiers?.self && ev.target !== target) {
      return;
    }

    if (!posStart || options?.distanceThreshold === false) {
      return;
    }

    options?.modifiers?.prevent && ev.preventDefault();
    options?.modifiers?.stop && ev.stopPropagation();

    const dx = ev.touches[0].pageX - posStart.x;
    const dy = ev.touches[0].pageY - posStart.y;
    const distance = Math.sqrt(dx * dx + dy * dy);
    if (distance >= (options?.distanceThreshold ?? DEFAULT_THRESHOLD)) {
      clear();
    }
  }

  function onEnd() {
    if (options?.modifiers?.interceptClick && !timeout) {
      addAutoDisappearMask();
    }
    clear();
  }

  const listenerOptions = {
    capture: options?.modifiers?.capture,
    once: options?.modifiers?.once,
  };
  const cleanup = [
    addEventListener(target, 'touchstart', onStart, listenerOptions),
    addEventListener(target, 'touchmove', onMove, listenerOptions),
    addEventListener(target, 'touchend', onEnd, listenerOptions),
    addEventListener(target, 'touchcancel', clear, listenerOptions),
  ];
  const stop = () => cleanup.forEach(fn => fn());
  return stop;
}

type BindingValue = (evt: TouchEvent) => void;

export const vOnLongPress: ObjectDirective<HTMLElement, BindingValue> = {
  mounted(el, binding) {
    const delayStr = binding.arg || '';
    let delay = parseInt(delayStr, 10) || DEFAULT_DELAY;
    if (delay <= 0) {
      delay = DEFAULT_DELAY;
    }
    onLongPress(el, binding.value, { delay, modifiers: binding.modifiers });
  }
};

function addAutoDisappearMask(duration?: number): void {
  const mask = document.createElement('div');
  mask.style.position = 'fixed';
  mask.style.top = '0';
  mask.style.right = '0';
  mask.style.bottom = '0';
  mask.style.left = '0';
  mask.style.zIndex = '99999';
  document.body.appendChild(mask);
  setTimeout(() => {
    document.body.removeChild(mask);
  }, duration || 25);
}
