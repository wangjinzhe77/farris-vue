import { onMountedOrActivated, inBrowser } from '../../utils';
import { unref, onUnmounted, onDeactivated, Ref } from 'vue';

export const supportsPassive = false;

export type UseEventListenerOptions = {
  target?: EventTarget | Ref<EventTarget | undefined>;
  capture?: boolean;
  passive?: boolean;
  immediate?: boolean;
};

export function useEventListener(type: string, listener: EventListener, options: UseEventListenerOptions = {}) {
  if (!inBrowser) {
    return;
  }

  const { target = window, passive = false, capture = false, immediate = false } = options;
  let attached = false;

  const add = () => {
    const element = unref(target);

    if (element && !attached) {
      element.addEventListener(type, listener, supportsPassive ? {
        capture,
        passive
      } : capture);
      attached = true;
    }
  };

  const remove = () => {
    const element = unref(target);

    if (element && attached) {
      element.removeEventListener(type, listener, capture);
      attached = false;
    }
  };

  immediate ? add() : onMountedOrActivated(add);
  onUnmounted(remove);
  onDeactivated(remove);
}
