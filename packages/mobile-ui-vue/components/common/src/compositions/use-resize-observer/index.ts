/* eslint-disable no-use-before-define */
import { computed, watch } from 'vue';
import { MaybeComputedElementRef, unrefElement, tryOnScopeDispose, defaultWindow } from './utils';

export interface ResizeObserverSize {
  readonly inlineSize: number;
  readonly blockSize: number;
}

export interface ResizeObserverEntry {
  readonly target: Element;
  readonly contentRect: DOMRectReadOnly;
  readonly borderBoxSize?: ReadonlyArray<ResizeObserverSize>;
  readonly contentBoxSize?: ReadonlyArray<ResizeObserverSize>;
  readonly devicePixelContentBoxSize?: ReadonlyArray<ResizeObserverSize>;
}

export type ResizeObserverCallback = (entries: ReadonlyArray<ResizeObserverEntry>, observer: ResizeObserver) => void;

export interface UseResizeObserverOptions {

  /** 监听的盒模型，默认为`content-box` */
  box?: ResizeObserverBoxOptions;

  /** 允许指定一个的`window` */
  window?: Window;
}

declare class ResizeObserver {

  constructor(callback: ResizeObserverCallback);

  disconnect(): void;

  observe(target: Element, options?: UseResizeObserverOptions): void;

  unobserve(target: Element): void;
}

/**
 * 监听元素的大小变化
 */
export function useResizeObserver(
  target: MaybeComputedElementRef | MaybeComputedElementRef[],
  callback: ResizeObserverCallback,
  options: UseResizeObserverOptions = {},
) {
  const { window = defaultWindow, ...observerOptions } = options;
  let observer: ResizeObserver | undefined;

  const cleanup = () => {
    if (observer) {
      observer.disconnect();
      observer = undefined;
    }
  };

  const targets = computed(() =>
    Array.isArray(target)
      ? target.map(el => unrefElement(el))
      : [unrefElement(target)]);

  const stopWatch = watch(
    targets,
    (elements) => {
      cleanup();
      if (window && 'ResizeObserver' in window) {
        observer = new ResizeObserver(callback);
        elements.forEach((element) => {
          element && observer!.observe(element, observerOptions);
        });
      }
    },
    { immediate: true, flush: 'post' },
  );

  const stop = () => {
    cleanup();
    stopWatch();
  };

  tryOnScopeDispose(stop);

  return { stop };
}
