import { isDef, isNumeric, isObject } from './type';

export function noop() { }

export function addUnit(value: string | number) {
  if (!isDef(value)) {
    return undefined;
  }
  value = String(value);
  return isNumeric(value) ? `${value}px` : value;
}

export const escapeHtml = (str: string) => {
  if (str === null || str === undefined || str === '') {
    return '';
  }
  return str
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#39;')
    .replace(/\//g, '&#x2F;');
};

export const unescapeHtml = (str: string) => {
  if (str === null || str === undefined || str === '') {
    return '';
  }
  return str
    .replace(/&amp;/g, '&')
    .replace(/&lt;/g, '<')
    .replace(/&gt;/g, '>')
    .replace(/&quot;/g, '"')
    .replace(/&#39;/g, "'")
    .replace(/&#x2F;/g, '/');
};

export const getValue = (field: string, data: any, safe = false) => {
  if (!data) {
    return '';
  }
  let resultVal = '';
  if (field.indexOf('.') === -1) {
    resultVal = data[field];
  } else {
    resultVal = field.split('.').reduce((obj, key) => {
      if (obj) {
        return obj[key];
      }
      return null;
    }, data);
  }

  if (safe) {
    return escapeHtml(resultVal);
  }
  return resultVal;
};

export const setValue = (obj: Record<string, any>, field: string, val: any) => {
  if(!obj || !field){
    return;
  }
  if (field.indexOf('.') > 0) {
    const fieldMap = field.split('.');
    const target = fieldMap.reduce((record, fieldItem) => {
      record[fieldItem] = isObject(record[fieldItem]) || {};
      return record[fieldItem];
    }, obj);

    if (target) {
      const targetField = fieldMap.pop();
      if (targetField) {
        target[targetField] = val;
      }
    }
  } else {
    obj[field] = val;
  }
};
