export function trimExtraChar(value: string, char: string, regExp: RegExp) {
  const index = value.indexOf(char);

  if (index === -1) {
    return value;
  }

  if (char === '-' && index !== 0) {
    return value.slice(0, index);
  }

  return value.slice(0, index + 1) + value.slice(index).replace(regExp, '');
};

export function camelToKebabCase(camelStr: string) {
  // 首字母转小写，其余每个大写字母前加下划线并转小写
  return camelStr.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
}
