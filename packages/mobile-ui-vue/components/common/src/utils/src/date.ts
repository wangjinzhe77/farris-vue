import { isArray, isString } from './type';

export const getDateRecord = (target: Date) => {
  const year = target.getFullYear();
  const month = target.getMonth() + 1;
  const day = target.getDate();
  const hours = target.getHours();
  const minutes = target.getMinutes();
  const seconds = target.getSeconds();

  return {
    year,
    month,
    day,
    hours,
    minutes,
    seconds
  };
};

export const getTimeRecord = (value: string | number[]) => {
  const [hours, minutes, seconds] = isString(value)
    ? value.split(':').map((item) => Number(item))
    : isArray(value)
      ? value
      : [0, 0, 0];

  return {
    hours,
    minutes,
    seconds
  };
};

export const isLeap = (year: number) => {
  return year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0);
};

export const formatTime = (
  value: { hours: number; minutes: number; seconds: number },
  format: string
) => {
  const { hours, minutes, seconds } = value;
  return format
    .replace('HH', String(hours).padStart(2, '0'))
    .replace('mm', String(minutes).padStart(2, '0'))
    .replace('ss', String(seconds).padStart(2, '0'));
};

export const parseTime = (value: string) => {
  const timeRegex = /^(\d{2}):(\d{2}):(\d{2})$/;
  const match = value.match(timeRegex);

  if (!match) {
    throw new Error('Invalid time format');
  }

  const [_, hours, minutes, seconds] = match;

  return new Date(0, 0, 0, parseInt(hours, 10), parseInt(minutes, 10), parseInt(seconds, 10));
};

export const formatDate = (value: Date, format: string) => {
  const { year, month, day, hours, minutes, seconds } = getDateRecord(value);
  return format
    .replace('YYYY', String(year))
    .replace('MM', String(month).padStart(2, '0'))
    .replace('DD', String(day).padStart(2, '0'))
    .replace('HH', String(hours).padStart(2, '0'))
    .replace('mm', String(minutes).padStart(2, '0'))
    .replace('ss', String(seconds).padStart(2, '0'));
};

export const parseDate = (value: string, format: string) => {
  const [dateValue, timeValue] = value.split(' ');
  const [dateFormat, timeFormat] = format.split(' ');

  const dateRegex = /年|月|日|-|\//g;

  const formatParts = dateFormat.replace(dateRegex, '-').split('-');
  const formatMap = formatParts.reduce((map, part, index) => {
    map[part] = index;
    return map;
  }, {} as Record<string, number>);

  const dateValueParts = dateValue
    .replace(dateRegex, '-')
    .split('-')
    .map((item) => Number(item));

  const year = dateValueParts[formatMap.YYYY];
  const month = dateValueParts[formatMap.MM] ? dateValueParts[formatMap.MM] - 1 : 0;
  const day = dateValueParts[formatMap.DD] || 1;

  const { hours, minutes, seconds } = getTimeRecord(timeValue);

  // 创建Date对象
  const parsedDate = new Date(year, month, day, hours, minutes, seconds);

  // 检查是否有效日期
  if (isNaN(parsedDate as any)) {
    throw new Error('Invalid date format');
  }

  return parsedDate;
};

export const compareDate = (firstDate: Date, secondDate: Date) => {
  return firstDate.getTime() - secondDate.getTime();
};

export const isEqualDate = (firstDate: Date, secondDate: Date) => {
  const {
    year: firstDateYear,
    month: firstDateMonth,
    day: firstDateDay
  } = getDateRecord(firstDate);
  const {
    year: secondDateYear,
    month: secondDateMonth,
    day: secondDateDay
  } = getDateRecord(secondDate);

  return (
    compareDate(
      new Date(firstDateYear, firstDateMonth, firstDateDay),
      new Date(secondDateYear, secondDateMonth, secondDateDay)
    ) === 0
  );
};

export const isEqualDateTime = (firstDate: Date, secondDate: Date) => {
  return compareDate(firstDate, secondDate) === 0;
};
