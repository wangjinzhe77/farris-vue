export const inBrowser = typeof window !== 'undefined';

export const inIOS = () => /(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent);

export const inAndroid = () => /(Android|Adr)/i.test(navigator.userAgent);

export function isDef<T>(val: T): val is NonNullable<T> {
  return val !== undefined && val !== null;
}

export function isNull(val: unknown): val is null {
  return val === null;
}

export function isUndefined(val: unknown): val is undefined {
  return val === undefined;
}

export function isString(val: unknown): val is string {
  return typeof val === 'string';
}

export function isNumber(val: unknown): val is number {
  return typeof val === 'number';
}

export function isBoolean(val: unknown): val is null {
  return typeof val === 'boolean';
}

export const { isArray } = Array;

export function isSymbol(val: unknown): val is symbol {
  return typeof val === 'symbol';
}

export function isFunction(val: unknown): val is FunctionConstructor {
  return typeof val === 'function';
}

export function isObject(val: unknown): val is Record<any, any> {
  return val !== null && typeof val === 'object';
}

export function isPromise<T = any>(val: unknown): val is Promise<T> {
  return isObject(val) && isFunction(val.then) && isFunction(val.catch);
}

export function isNumeric(val: string) {
  return /^-?\d+(\.\d+)?$/.test(val);
}

export function isDate(val: unknown): val is Date {
  return typeof val === 'object' && val instanceof Date;
}

export function getType(val: unknown) {
  let type: string;
  switch (val) {
    case isString(val):{
      type = 'string';
      break;
    }
    case isNumber(val):{
      type = 'number';
      break;
    }
    case isDate(val):{
      type = 'date';
      break;
    }
    case isNull(val):{
      type = 'null';
      break;
    }
    case isUndefined(val):{
      type = 'undefined';
      break;
    }
    case isArray(val):{
      type = 'array';
      break;
    }
    case isSymbol(val):{
      type = 'symbol';
      break;
    }
    case isFunction(val):{
      type = 'function';
      break;
    }
    case isPromise(val):{
      type = 'promise';
      break;
    }
    case isObject(val):{
      type = 'object';
      break;
    }
    default:
      type = typeof val;
      break;
  }
  return type;
}
