export const getSlideTransitionName = (position: string | undefined) => {
  switch (position) {
    case 'bottom':
      return 'fm-slide-up';
    case 'top':
      return 'fm-slide-down';
    case 'left':
      return 'fm-slide-left';
    case 'right':
      return 'fm-slide-right';
    default:
      return 'fm-fade';
  }
};
