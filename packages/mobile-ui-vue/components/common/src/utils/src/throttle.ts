export const throttle = (
  fn: (...args: any)=> any,
  wait: number,
  options: { leading?: boolean; trailing?: boolean } = {}
) => {
  let timeout: any = null;
  let previous = 0;

  const throttled = (...args: any) => {
    const nowDate = +new Date();

    if (!previous && options.leading === false) {
      previous = nowDate;
    }

    const remaining = wait - (nowDate - previous);

    if (remaining <= 0 || remaining > wait) {
      if (timeout) {
        clearTimeout(timeout);
        timeout = null;
      }
      previous = nowDate;
      fn(...args);
    } else if (!timeout && options.trailing !== false) {
      timeout = setTimeout(() => {
        previous = options.leading === false ? 0 : +new Date();
        timeout = null;
        fn(...args);
      }, remaining);
    }
  };

  // 手动取消
  throttled.cancel = function () {
    clearTimeout(timeout);
    previous = 0;
    timeout = null;
  };
  return throttled;
};
