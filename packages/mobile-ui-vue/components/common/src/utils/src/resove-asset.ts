const getKebabCase = (str: string) => {
  return str.replace(/[A-Z]/g, function (i) {
    return "-" + i.toLowerCase();
  });
};
const getCamelCase = (str: string) => {
  return str.replace(/-([a-z])/g, function (all, i) {
    return i.toUpperCase();
  });
};

const hasOwn = (v: object, s: string)=> Object.prototype.hasOwnProperty.call(v, s);

export function resolveAsset (
  assets: Record<string, any>,
  id: string
): any {
  /* istanbul ignore if */
  if (typeof id !== 'string') {
    return;
  }
  // check local registration variations first
  if (hasOwn(assets, id)) {return assets[id];}
  const camelizedId = getKebabCase(id);
  if (hasOwn(assets, camelizedId)) {return assets[camelizedId];}
  const PascalCaseId = getCamelCase(camelizedId);
  if (hasOwn(assets, PascalCaseId)) {return assets[PascalCaseId];}

  // fallback to prototype chain
  const res = assets[id] || assets[camelizedId] || assets[PascalCaseId];
  
  return res;
}
