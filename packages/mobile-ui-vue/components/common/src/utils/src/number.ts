import { trimExtraChar } from './string';
import { isDef } from './type';

export function formatToNumber(value: string, allowDot: boolean) {
  value = isDef(value) ? value : '';
  let regExp = /[^-0-9]/g;
  value = trimExtraChar(value, '-', /-/g);
  if (allowDot) {
    value = trimExtraChar(value, '.', /\./g);
    regExp = /[^-0-9.]/g;
  } else {
    value = value.split('.')[0];
  }
  return value.replace(regExp, '');
}

export function parseFloat(value: string | number, precision = 0) {
  return Number.parseFloat(value ? String(value) : '0').toFixed(precision);
}

export function range(num: number, min: number, max: number): number {
  return Math.min(Math.max(num, min), max);
}
export function random(min: number, max: number) {
  return Math.round(Math.random() * (max - min) + min);
}
