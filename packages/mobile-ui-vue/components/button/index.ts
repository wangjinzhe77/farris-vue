import { withInstall } from '@components/common';
import ButtonInstallless from './src/button.component';

const Button = withInstall(ButtonInstallless);

export { Button };
export default Button;
