import { ExtractPropTypes, PropType } from 'vue';

type ButtonType = 'primary' | 'secondary' | 'success' | 'warning' | 'danger' | 'info';
type SizeType = 'large' | 'normal' | 'small' | 'mini';

export const BUTTON_NAME = "fm-button";

export const buttonProps = {
  type: {
    type: String as PropType<ButtonType>,
    default: 'primary'
  },
  size: {
    type: String as PropType<SizeType>,
    default: ''
  },
  color: {
    type: String,
    default: ''
  },
  block: {
    type: Boolean,
    default: false
  },
  noBorder: {
    type: Boolean,
    default: false
  },
  plain: {
    type: Boolean,
    default: false
  },
  round: {
    type: Boolean,
    default: false
  },
  disabled: {
    type: Boolean,
    default: false
  },
  icon: {
    type: String,
    default: ''
  },
  loading: {
    type: Boolean,
    default: false
  },
  loadingText: {
    type: String,
    default: ''
  }
};

export type ButtonProps = ExtractPropTypes<typeof buttonProps>;
