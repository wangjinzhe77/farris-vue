import { CSSProperties, computed, defineComponent } from 'vue';
import { Icon } from '@components/icon';
import { BUTTON_NAME, ButtonProps, buttonProps } from './button.props';
import { useBem } from '@/components/common';

export default defineComponent({
  name: BUTTON_NAME,
  props: buttonProps,
  emits: ['click'],
  setup(props: ButtonProps, context) {
    const { slots } = context;

    const { bem } = useBem(BUTTON_NAME);

    const buttonClass = computed(() => {
      const classList = [bem(), bem('', props.type)];
      props.disabled && classList.push(bem('', 'disabled'));
      props.block && classList.push(bem('', 'block'));
      props.round && classList.push(bem('', 'round'));
      props.plain && classList.push(bem('', 'plain'));
      props.noBorder && classList.push(bem('', 'noborder'));
      props.loading && classList.push(bem('', 'loading'));
      props.size && classList.push(bem('', props.size));
      return classList;
    });

    const applyGradientBorderBehavior = (style: CSSProperties, color: string) =>{
      const isGradientColor = color.includes('gradient');
      if (isGradientColor) {
        style.border = 0;
      } else {
        style.borderColor = color;
      }
    };

    const buttonStyle = computed(() => {
      const textColor = props.plain ? props.color : '#fff';
      const backgroundColor = !props.plain ? props.color : null;

      const style: CSSProperties = {};
      if (props.color) {
        style.color = textColor;
        style.boxShadow = 'none';
        backgroundColor && (style.background = backgroundColor);
        applyGradientBorderBehavior(style, props.color);
      }
      return style;
    });

    const onClickButton = ($event: Event) =>{
      $event.stopPropagation();
      if (!props.disabled) {
        context.emit('click', $event);
      }
    };

    const renderLoading = () => {
      return (
        props.loading && <>
          <span class={bem('loading')}>
            <svg viewBox="25 25 50 50" class={bem('loading-icon-circular')}>
              <circle cx="50" cy="50" r="20" fill="none" />
            </svg>
          </span>
          {props.loadingText && <span class={bem('loading-text')}>{props.loadingText}</span>}
        </>
      );
    };

    const renderIcon = () => {
      return (
        props.icon && <Icon class={bem('icon')} name={props.icon} />
      );
    };

    const renderText = ()=> {
      return slots.default && <span class={bem('text')}>{slots.default()}</span>;
    };

    return () => (
      <button
        class={buttonClass.value}
        style={buttonStyle.value}
        onClick={onClickButton}
      >
        {renderLoading()}
        {renderIcon()}
        {renderText()}
      </button>
    );
  }
});
