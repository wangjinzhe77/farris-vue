import { ref } from "vue";
import { DesignerHTMLElement, DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema } from "../../../designer-canvas/src/types";
import { PAGE_FOOTER_CONTAINER_NAME } from '../page-footer-container.props';

export function useDesignerRules(schema: ComponentSchema, parentSchema?: ComponentSchema): UseDesignerRules {

  const triggerBelongedComponentToMoveWhenMoved = ref(false);

  const triggerBelongedComponentToDeleteWhenDeleted = ref(false);

  const hideNestedPadding = true;

  function canAccepts(draggingContext: DraggingResolveContext): boolean {
    const acceptableControlTypes = [
      'tab-bar',
      'button',
      'content-container',
      'html-template',
    ];
    const { controlType } = draggingContext;
    if (!acceptableControlTypes.includes(controlType)) {
      return false;
    }
    return true;
  }


  function checkCanMoveComponent() {
    return false;
  }
  function checkCanDeleteComponent() {
    return true;
  }

  function hideNestedPaddingInDesginerView() {
    return hideNestedPadding;
  }

  function getDesignerClass(): string {
    return PAGE_FOOTER_CONTAINER_NAME;
  }

  return {
    canAccepts,
    triggerBelongedComponentToMoveWhenMoved,
    triggerBelongedComponentToDeleteWhenDeleted,
    checkCanMoveComponent,
    checkCanDeleteComponent,
    hideNestedPaddingInDesginerView,
    getDesignerClass,
  };
}
