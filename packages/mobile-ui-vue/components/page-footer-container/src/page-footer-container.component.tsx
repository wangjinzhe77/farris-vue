import { SetupContext, defineComponent, computed } from 'vue';
import { PAGE_FOOTER_CONTAINER_NAME, PageFooterContainerProps, pageFooterContainerProps } from './page-footer-container.props';
import { useBem } from '@components/common';

export default defineComponent({
  name: PAGE_FOOTER_CONTAINER_NAME,
  props: pageFooterContainerProps,
  emits: [],
  setup(props: PageFooterContainerProps, context: SetupContext) {
    const { bem } = useBem(PAGE_FOOTER_CONTAINER_NAME);

    const containerClass = computed(() => ({
      [bem()]: true,
      [props.customClass || '']: true,
    }));

    return () => (
      <div class={containerClass.value}>
        {context.slots.default?.()}
      </div>
    );
  }
});
