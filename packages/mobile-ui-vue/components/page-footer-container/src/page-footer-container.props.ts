import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import pageFooterContainerSchema from './schema/page-footer-container.schema.json';
import pageFooterContainerPropertyConfig from './property-config/page-footer-container.property-config.json';
import { schemaResolver } from './schema/schema-resolver';

export const PAGE_FOOTER_CONTAINER_NAME = 'fm-page-footer-container';

export const pageFooterContainerProps = {
  customClass: { type: String, default: '' },
};

export type PageFooterContainerProps = ExtractPropTypes<typeof pageFooterContainerProps>;

export const propsResolver = createPropsResolver(
  pageFooterContainerProps,
  pageFooterContainerSchema,
  schemaMapper,
  schemaResolver,
  pageFooterContainerPropertyConfig,
);
