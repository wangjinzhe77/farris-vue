/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { SetupContext, computed, defineComponent } from 'vue';
import { NotifyProps, notifyProps } from './notify.props';
import { useBem } from '@components/common';

const name = 'fm-notify';

export default defineComponent({
  name,
  props: notifyProps,
  emits: ['click'],
  setup(props: NotifyProps, context: SetupContext) {
    const { bem } = useBem(name);

    const notifyClass = computed(() => ({
      [name]: true,
      [bem('', props.type)]: true,
      [`${props.className}`]: true
    }));

    const notifyStyle = computed(() => {
      return {
        color: props.color,
        background: props.background
      };
    });

    return () => (
      <>
        {props.show && (
          <div class={notifyClass.value} style={notifyStyle.value}>
            {props.message}
          </div>
        )}
      </>
    );
  }
});
