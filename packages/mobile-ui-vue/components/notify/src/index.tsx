import FMNotify from './notify.component';
import { isObject, inBrowser } from '@components/common';
import { mountComponent, usePopupState } from '@components/common';
import { App } from 'vue';
let instance;
let timer;
function defaultOptions() {
  return {
    type: 'info',
    message: '',
    duration: 3000,
    color: '#fff',
    background: ''
  };
}

function parseOptions(message) {
  return isObject(message) ? message : { message };
}

const initInstance = () => {
  ({ instance } = mountComponent({
    setup() {
      const { state, toggle } = usePopupState();
      return () => <FMNotify {...{ ...state, 'onUpdate:show': toggle }} />;
    }
  }));
};

function Notify(options) {
  if (!inBrowser) {
    return;
  }

  if (!instance) {
    initInstance();
  }

  options = {
    ...Notify.currentOptions,
    ...parseOptions(options)
  };

  instance.open(options);
  clearTimeout(timer);

  if (options.duration > 0) {
    timer = setTimeout(Notify.clear, options.duration);
  }

  return instance;
}

Notify.clear = () => {
  if (instance) {
    instance.toggle(false);
  }
};

Notify.info = (options) => {
  Notify.resetDefaultOptions();
  return Notify({ ...parseOptions(options), type: 'info' });
};

Notify.success = (options) => {
  Notify.resetDefaultOptions();
  return Notify({ ...parseOptions(options), type: 'success' });
};

Notify.error = (options) => {
  Notify.resetDefaultOptions();
  return Notify({ ...parseOptions(options), type: 'error' });
};

Notify.warning = (options) => {
  Notify.resetDefaultOptions();
  return Notify({ ...parseOptions(options), type: 'warning' });
};

Notify.currentOptions = defaultOptions();

Notify.setDefaultOptions = (options) => {
  Object.assign(Notify.currentOptions, options);
};

Notify.resetDefaultOptions = () => {
  Notify.currentOptions = defaultOptions();
};

Notify.install = (app: App) => {
  app.component(FMNotify.name, FMNotify);
  app.config.globalProperties.$notify = Notify;
};

Notify.Component = FMNotify;

export default Notify;
