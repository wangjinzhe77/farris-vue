import { withInstall } from '@components/common';
import TabInstallless from '../tabs/src/components/tab.component';

export * from '../tabs/src/components/tab.props';

const Tab = withInstall(TabInstallless);

export { Tab };
export default Tab;
