import type { App } from 'vue';
import DynamicView from './src/dynamic-view.component';

export * from './src/dynamic-view.props';

export { DynamicView };

export default {
  install(app: App): void {
    app.component(DynamicView.name, DynamicView);
  }
};
