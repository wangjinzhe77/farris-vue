import { ExtractPropTypes } from 'vue';

export const dynamicViewProps = {
  schema: { type: Object, default: {} },
  /**
   * 组件值
   */
  modelValue: { type: Object, default: {} },
};
export type DynamicViewProps = ExtractPropTypes<typeof dynamicViewProps>;
