import PageContainer from '../../../page-container';
import PageBodyContainer from '../../../page-body-container';
import PageHeaderContainer from '../../../page-header-container';
import PageFooterContainer from '../../../page-footer-container';
import ListView from '../../../list-view';

const componentMap: Record<string, any> = {};
const componentPropsConverter: Record<string, any> = {};
const componentPropertyConfigConverter: Record<string, any> = {};

let hasLoaded = false;

function loadRegister() {
  if (!hasLoaded) {
    hasLoaded = true;
    PageContainer.register(componentMap, componentPropsConverter, componentPropertyConfigConverter);
    PageBodyContainer.register(componentMap, componentPropsConverter, componentPropertyConfigConverter);
    PageHeaderContainer.register(componentMap, componentPropsConverter, componentPropertyConfigConverter);
    PageFooterContainer.register(componentMap, componentPropsConverter, componentPropertyConfigConverter);
    ListView.register(componentMap, componentPropsConverter, componentPropertyConfigConverter);
  }
}

export { componentMap, componentPropsConverter, loadRegister };
