import { ExtractPropTypes } from 'vue';
import { buttonEditProps } from '@/components/button-edit';
import { dateTimePickerProps } from '@/components/date-time-picker';

export const DATA_TIME_PICKER_INPUT_NAME = 'FmDateTimePickerInput';

export const dateTimePickerInputProps = {
  ...buttonEditProps,
  ...dateTimePickerProps
};

export type DateTimePickerInputProps = ExtractPropTypes<typeof dateTimePickerInputProps>;
