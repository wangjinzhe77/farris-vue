import { computed, defineComponent } from 'vue';
import { useBem } from '@components/common';
import DateTimePicker from '@/components/date-time-picker';
import { usePickerInputState } from '@/components/picker-input';
import ButtonEdit from '@/components/button-edit';
import { dateTimePickerInputProps, DateTimePickerInputProps, DATA_TIME_PICKER_INPUT_NAME } from './date-time-picker-input.props';

export default defineComponent({
  name: DATA_TIME_PICKER_INPUT_NAME,
  props: dateTimePickerInputProps,
  emits: ['change', 'confirm', 'update:modelValue'],
  setup(props: DateTimePickerInputProps, context) {

    const { bem } = useBem(DATA_TIME_PICKER_INPUT_NAME);

    const { buttonEditProps, inputValue, showPopup, componentRef, handleChange, handleConfirm } = usePickerInputState(props, context);

    const pickerProps = computed(() => {
      return {
        ref: componentRef,
        title: props.title,
        type: props.type,
        format: props.format,
        maxDate: props.maxDate,
        minDate: props.minDate,
        visiableOptionCount: props.visiableOptionCount,
        optionFormatter: props.optionFormatter,
        onChange: handleChange,
        onConfirm: handleConfirm,
        onCancel: () => showPopup.value = false
      };
    });

    return () => (
      <ButtonEdit
        {...buttonEditProps.value}
        v-model={inputValue.value}
        v-model:show={showPopup.value}
        inputClass={bem()}
      >
        <DateTimePicker {...pickerProps.value} />
      </ButtonEdit>
    );
  }
});
