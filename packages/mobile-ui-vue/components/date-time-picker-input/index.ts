import { withInstall } from '@components/common';
import DateTimePickerInputInstallless from './src/date-time-picker-input.component';

const DateTimePickerInput = withInstall(DateTimePickerInputInstallless);

export { DateTimePickerInput };
export default DateTimePickerInput;
