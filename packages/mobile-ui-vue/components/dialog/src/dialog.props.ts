/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';

export type alignType = 'start' | 'end' | 'left' | 'center' | 'right' | 'justify' | 'match-parent';

export const dialogProps = {
  title: { type: String, default: '' },

  message: { type: String, default: '' },

  messageAlign: { type: String as PropType<alignType>, default: 'center' },

  show: { type: Boolean, default: false },

  overlay: { type: Boolean, default: true },

  lockScroll: { type: Boolean, default: true },

  teleport: { type: String, default: '' },

  buttonLayout: { type: String, default: 'row' },

  buttons: { type: Array, default: [] },

  className: { type: String, default: '' },

  showClose: { type: Boolean, default: false },

  content: { type: Function, default: undefined },
};

export type DialogProps = ExtractPropTypes<typeof dialogProps>;
