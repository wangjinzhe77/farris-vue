/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { SetupContext, computed, defineComponent, ref, toRef, watch } from 'vue';
import { DialogProps, dialogProps } from './dialog.props';
import { preventDefault, useBem } from '@components/common';
import Popup from '@components/popup';
import FmButton from '@components/button';

const name = 'fm-dialog';

export default defineComponent({
  name,
  props: dialogProps,
  emits: ['update:show', 'open', 'close'],
  setup(props: DialogProps, context: SetupContext) {
    const { attrs, emit, slots } = context;
    const { bem } = useBem(name);

    const hasShown = ref(props.show);
    watch(
      () => props.show,
      (show) => {
        hasShown.value = show;
        if (show) {
          emit('open');
        } else {
          emit('close');
        }
      }
    );

    const close = () => {
      hasShown.value = false;
      emit('close');
      emit('update:show', false);
    };

    const renderHeader = () => {
      const headerTitle = toRef(props, 'title');
      return <div class={bem('header')}>{slots.header ? slots.header() : headerTitle.value}</div>;
    };

    const dialogContentClass = computed(() => {
      const classObject = {
        [bem(['content', 'message'])]: true,
        [bem(['content', 'message'], 'has-title')]: props.title
      } as Record<string, boolean>;
      return classObject;
    });

    const dialogContentStyle = computed(() => {
      const styleObject = {
        textAlign: props.messageAlign
      } as Record<string, string>;
      return styleObject;
    });

    const getContent = () => {
      if (slots.default) {
        return slots.default();
      }
      if (props.content) {
        return props.content();
      }
      return (
        <div class={dialogContentClass.value} style={dialogContentStyle.value}>
          {props.message}
        </div>
      );
    };

    const renderContent = () => {
      return <div class={['fm-dialog-content', props.className]}>{getContent()}</div>;
    };

    const onButtonClick = (event: MouseEvent, button: any) => {
      preventDefault(event, true);
      if (button.disabled || button.loading) {
        return;
      }
      if (typeof button.handler === 'function') {
        button.handler.call(null, button);
      } else {
        hasShown.value = false;
        emit('update:show', false);
      }
    };

    const dialogFooterClass = computed(() => {
      const classObject = {
        [bem('footer')]: true,
        [bem('footer', 'is-column')]: props.buttonLayout === 'column'
      } as Record<string, boolean>;
      return classObject;
    });

    const renderFooter = () => {
      return (
        <div class={dialogFooterClass.value}>
          {slots.footer
            ? slots.footer()
            : props.buttons?.map((button: any) => (
              <FmButton
                type={button.type ? button.type : 'info'}
                plain
                block
                size="large"
                disabled={!!button.disabled}
                onClick={(event) => {
                  onButtonClick(event, button);
                }}>
                {button.text}
              </FmButton>
            ))}
        </div>
      );
    };
    const renderClose = () => {
      return (
        <div class={bem('close')} onClick={close}>
          <span class="fm-icon fm-icon-s-cross"></span>
        </div>
      );
    };

    const dialogClass = computed(() => {
      const classObject = {
        [bem()]: true,
        [bem('', 'relative')]: props.showClose
      } as Record<string, boolean>;
      return classObject;
    });
    const renderDialog = () => {
      return (
        <div class={dialogClass.value} {...attrs}>
          {renderHeader()}
          {renderContent()}
          {renderFooter()}
          {props.showClose && renderClose()}
        </div>
      );
    };

    return () => {
      const { overlay, lockScroll, teleport = 'body' } = props;
      return (
        <Popup
          show={hasShown.value}
          overlay={overlay}
          teleport={teleport}
          position="center"
          lockScroll={lockScroll}
          round={true}>
          {renderDialog()}
        </Popup>
      );
    };
  }
});
