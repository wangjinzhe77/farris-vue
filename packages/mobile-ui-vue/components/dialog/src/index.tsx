/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { App, ref } from 'vue';
import { mountComponent, usePopupState, isObject, inBrowser } from '@components/common';
import FMDialog from './dialog.component';
import FMInput from '@/components/input-group';

let instance;
function defaultOptions() {
  return {
    title: '',
    message: '',
    messageAlign: 'center',
    overlay: true,
    lockScroll: true,
    teleport: 'body',
    buttonLayout: 'row',
    buttons: [],
    showClose: false,
    content: undefined
  };
}

function parseOptions(message) {
  return isObject(message) ? message : { message };
}

const initInstance = () => {
  ({ instance } = mountComponent({
    setup() {
      const { state, toggle } = usePopupState();
      return () => <FMDialog {...{ ...state, 'onUpdate:show': toggle }} />;
    }
  }));
};

function Dialog(options) {
  if (!inBrowser) {
    return;
  }

  if (!instance) {
    initInstance();
  }

  options = {
    ...Dialog.currentOptions,
    ...parseOptions(options)
  };

  instance.open(options);

  return instance;
}

Dialog.clear = () => {
  if (instance) {
    instance.toggle(false);
  }
};

Dialog.confirm = (options) => {
  const CancelText = '取消';
  const ConfirmText = '确定';

  const {
    cancelText = CancelText,
    confirmText = ConfirmText,
    onConfirm = () => {},
    onCancel = () => {},
    ...others
  } = options;
  const _options = {
    buttons: [
      {
        text: cancelText,
        type: 'info',
        handler: () => {
          onCancel();
          instance.close();
        }
      },
      {
        text: confirmText,
        handler: () => {
          onConfirm();
          instance.close();
        }
      }
    ],
    ...others
  };
  Dialog(_options);
};

Dialog.prompt = (options) => {
  const CancelText = '取消';
  const ConfirmText = '确定';

  const {
    confirmText = ConfirmText,
    cancelText = CancelText,
    onConfirm = () => {},
    onCancel = () => {},
    defaultText = '',
    inputOptions = {},
    ...others
  } = options;
  const inputValue = ref(defaultText);
  const defaultInputOptions = {
    type: 'textarea'
  };
  const _options = {
    buttons: [
      {
        text: cancelText,
        handler: () => {
          onCancel();
          instance.close();
        }
      },
      {
        text: confirmText,
        handler: () => {
          onConfirm(inputValue.value);
          instance.close();
        }
      }
    ],
    content: () => {
      return (
        <FMInput
          v-model={inputValue.value}
          {...{ ...defaultInputOptions, ...inputOptions }}
          withBorder={true}></FMInput>
      );
    },
    ...others
  };
  Dialog(_options);
};
Dialog.alert = (options) => {
  const ConfirmText = '确定';
  const { confirmText = ConfirmText, onConfirm = () => {}, ...others } = options;
  const _options = {
    buttons: [
      {
        text: confirmText,
        handler: () => {
          onConfirm();
          instance.close();
        }
      }
    ],
    ...others
  };
  Dialog(_options);
};

Dialog.currentOptions = defaultOptions();

Dialog.setDefaultOptions = (options) => {
  Object.assign(Dialog.currentOptions, options);
};

Dialog.resetDefaultOptions = () => {
  Dialog.currentOptions = defaultOptions();
};

Dialog.install = (app: App) => {
  app.component(FMDialog.name, FMDialog);
  app.config.globalProperties.$dialog = Dialog;
};

Dialog.Component = FMDialog;

export default Dialog;
