/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { LoadingType, LoadingDirection } from './composition/types';

export const LOADING_NAME = 'fm-loading';

export const loadingProps = {

  /** 加载图标的类型，可选值：`default`（默认）、`spinner`、`ring` */
  type: { type: String as PropType<LoadingType>, default: 'default' },

  /** 文本内容 */
  text: { type: String, default: '' },

  /** 图标与文本的对齐方向，默认水平对齐，可选垂直对齐 */
  direction: { type: String as PropType<LoadingDirection>, default: 'horizontal' },

  /** 图标的大小 */
  size: { type: String, default: '24px' },

  /** 文本的字体大小 */
  textSize: { type: String, default: undefined },

  /** 图标的描边宽度 */
  stroke: { type: Number, default: undefined },

  /** 图标颜色 */
  color: { type: String, default: undefined },

  /** 文本的颜色，默认与图标颜色相同 */
  textColor: { type: String, default: undefined },

  /** 加载动画播放一个周期的时长，单位：ms */
  duration: { type: Number, default: undefined },

} as Record<string, any>;

export type LoadingProps = ExtractPropTypes<typeof loadingProps>;
