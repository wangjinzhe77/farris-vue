/** 加载图标的类型 */
export type LoadingType = 'default' | 'spinner' | 'ring';

/** 加载图标与文本的对齐方向 */
export type LoadingDirection = 'horizontal' | 'vertical';
