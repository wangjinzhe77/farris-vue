/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext, h, computed, toRefs } from 'vue';
import { LOADING_NAME, loadingProps, LoadingProps } from './loading.props';
import { useBem } from '@components/common';

export default defineComponent({
  name: LOADING_NAME,

  props: loadingProps,

  setup(props: LoadingProps, context: SetupContext) {
    const { bem } = useBem(LOADING_NAME);
    const { slots } = context;

    const isPositiveNumber = (value: any): boolean => {
      return typeof value === 'number' && value > 0;
    };

    const {
      text,
      textSize,
      color: iconColor,
      size: iconSize,
    } = toRefs(props);

    const layoutDirection = computed(() => props.direction || 'horizontal');
    const textColor = computed(() => props.textColor ?? props.color);
    const customAnimationDuration = computed(() => {
      return props.duration ? `${+props.duration}ms` : undefined;
    });
    const customPxWidth = computed(() => {
      return isPositiveNumber(+props.stroke) ? `${+props.stroke}px` : undefined;
    });
    const customStrokeWidth = computed(() => {
      return isPositiveNumber(+props.stroke) ? `${+props.stroke}` : undefined;
    });

    const renderCircularIcon = () => {
      const shorterDuration = props.duration ? `${+props.duration * 0.75}ms` : undefined;
      const circleStyle = {
        animationDuration: shorterDuration,
        strokeWidth: customStrokeWidth.value,
      };
      return (
        <svg role="status" class={bem('circular')} viewBox="25 25 50 50">
          <circle style={circleStyle} cx="50" cy="50" r="20" fill="none" />
        </svg>
      );
    };

    const renderSpinnerIcon = () => {
      const lineInnerStyle = {
        width: customPxWidth.value,
      };
      return (
        <div role="status" class={bem('spinner')}>
          {Array(12).fill(null).map((_, index) => (
            <span class={[bem('line'), bem('line', `${index + 1}`)]}>
              <span class={bem('line-inner')} style={lineInnerStyle}></span>
            </span>
          ))}
        </div>
      );
    };

    const renderRingIcon = () => {
      const ringStyle = {
        borderWidth: customPxWidth.value,
      };
      return (
        <div role="status" class={bem('ring')} style={ringStyle}></div>
      );
    };

    const iconRenderers = {
      default: renderCircularIcon,
      spinner: renderSpinnerIcon,
      ring: renderRingIcon,
    };

    const renderIcon = () => {
      if (slots.icon) {
        return slots.icon();
      }
      const iconRenderer: (() => JSX.Element) = iconRenderers[props.type] || iconRenderers.default;
      const icon = h(iconRenderer, {
        style: {
          color: iconColor.value,
          width: iconSize.value,
          height: iconSize.value,
          animationDuration: customAnimationDuration.value,
        }
      });
      return icon;
    };

    const renderText = () => {
      const content = slots.default && slots.default() || text.value;
      if (!content) {
        return;
      }
      const textStyle = {
        fontSize: textSize.value,
        color: textColor.value,
      };
      return (
        <span class={bem('text')} style={textStyle}>
          {content}
        </span>
      );
    };

    return () => (
      <div class={[bem(), bem('', layoutDirection.value)]}>
        {renderIcon()}
        {renderText()}
      </div>
    );
  }

});
