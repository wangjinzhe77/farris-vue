import { withInstall } from '@components/common';
import LoadingInstallless from './src/loading.component';

export * from './src/loading.props';

const Loading = withInstall(LoadingInstallless);

export { Loading };
export default Loading;
