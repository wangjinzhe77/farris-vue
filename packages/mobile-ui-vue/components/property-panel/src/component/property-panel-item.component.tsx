/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext, ref, computed } from 'vue';
import { propertyPanelItemProps, PropertyPanelItemProps } from '../composition/props/property-panel-item.props';
import { PropertyEntity } from '../../../designer-canvas/src/composition/entity/property-entity';
import FDynamicFormGroup from '../../../../../ui-vue/components/dynamic-form/src/component/dynamic-form-group/dynamic-form-group.component';
import '../composition/class/property-panel-item.css';

export default defineComponent({
  name: 'FPropertyPanelItem',
  props: propertyPanelItemProps,
  emits: ['PropertyChange'] as (string[] & ThisType<void>) | undefined,
  setup(props: PropertyPanelItemProps, context: SetupContext) {
    const categoryId = ref(props.category?.categoryId);
    const propertyID = ref(props.elementConfig.propertyID);
    const propertyName = ref(props.elementConfig.propertyName);
    const editor = ref(props.elementConfig.editor);
    const propertyValue = ref(props.elementConfig.propertyValue);
    const propertyItemClass = computed(() => ({
      'farris-group-wrap': true,
      'property-item': true,
      'd-none': !(props.elementConfig as PropertyEntity).visible()
    }));

    function onPropertyChange(newValue: any) {
      (props.elementConfig as PropertyEntity).propertyValue = newValue;
      context.emit('PropertyChange', (props.elementConfig as PropertyEntity).propertyID, newValue);
    }

    return () => {
      return (
        <div class={propertyItemClass.value}>
          <FDynamicFormGroup
            id={propertyID.value}
            customClass={`${categoryId.value}-${propertyID.value}`}
            label={propertyName.value}
            editor={editor.value}
            modelValue={propertyValue.value}
            onChange={onPropertyChange}
          ></FDynamicFormGroup>
        </div>
      );
    };
  }
});
