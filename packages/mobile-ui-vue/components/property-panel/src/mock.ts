export const propertyConfigTemp = [
  {
    categoryId: 'basic',
    categoryName: '基本信息',
    properties: [
      {
        propertyID: 'id',
        propertyName: '标识',
        propertyType: 'string',
        description: '组件的id',
        readonly: true
      },
      {
        propertyID: 'type',
        propertyName: '控件类型',
        propertyType: 'select',
        description: '组件的类型',
      }
    ]
  },
  {
    categoryId: 'appearance',
    categoryName: '样式',
    properties: [
      {
        propertyID: 'fill',
        propertyName: '填充',
        propertyType: 'datetime',
        description: 'flex布局下，填充满剩余部分',
        modelValue: '2023-4-9',
        editor: {
          id: 'd152e48d-13d1-4553-94fa-525fa67d4f2b',
          type: 'date-picker',
          require: false,
          format: 'yyyy-MM-dd',
          weekSelect: false,
          startFieldCode: 'BillDate',
          endFieldCode: 'BillDate'
        },
      },
      {
        propertyID: 'expanded',
        propertyName: '展开',
        propertyType: 'boolean',
        description: '是否展开',
        modelValue: 'false',
        editor: {
          type: 'combo-list',
          require: false,
          valueType: '1',
          multiSelect: false,
          data: [
            {
              value: 'true',
              name: 'true'
            },
            {
              value: 'false',
              name: 'false'
            }
          ]
        }
      },
      {
        propertyID: 'showHeader',
        propertyName: '显示头部区域',
        propertyType: 'boolean',
        description: '是否显示头部区域',
        modelValue: 'true',
        editor: {
          type: 'combo-list',
          require: false,
          valueType: '1',
          multiSelect: false,
          data: [
            {
              value: 'true',
              name: 'true'
            },
            {
              value: 'false',
              name: 'false'
            }
          ]
        }
      },
      {
        propertyID: 'mainTitle',
        propertyName: '主标题',
        propertyType: 'string',
        description: '主标题名称',
        group: 'header'
      },
      {
        propertyID: 'subTitle',
        propertyName: '副标题',
        propertyType: 'string',
        description: '副标题名称',
        group: 'header'
      },
      {
        propertyID: 'enableMaximize',
        propertyName: '显示最大化',
        propertyType: 'boolean',
        description: '是否显示最大化',
        group: 'header'
      },
      {
        propertyID: 'enableAccordion',
        propertyName: '启用收折功能',
        propertyType: 'boolean',
        description: '是否启用收折功能',
        group: 'header'
      },
      {
        propertyID: 'accordionMode',
        propertyName: '收折模式',
        propertyType: 'select',
        description: '收折模式选择',
        iterator: [{ key: 'default', value: '默认收折' }, { key: 'custom', value: '自定义收折' }],
        group: 'header'
      }
    ]
  },
  {
    categoryId: 'toolbar',
    categoryName: '工具栏',
    properties: [
      {
        propertyID: 'toolbarCls',
        propertyName: '工具栏样式',
        propertyType: 'string'
      },
      {
        propertyID: 'toolbarBtnSize',
        propertyName: '按钮尺寸',
        propertyType: 'select',
        iterator: [
          { key: 'default', value: '标准' },
          { key: 'lg', value: '大号' }
        ]
      },
      {
        propertyID: 'toolbarPopDirection',
        propertyName: '弹出方向',
        propertyType: 'select',
        iterator: [
          { key: 'default', value: '自动' },
          { key: 'top', value: '向上' },
          { key: 'bottom', value: '向下' }
        ]
      }
    ]
  }
];

export const propertyDataTemp = {
  id: 'dataGrid',
  testCategoryCascade: {
    showSize2: false
  },
  language1: {
    'zh-CHS': 'check1',
    'en': 'hhhh'
  },
  language2: {
    'en': 'hhhh2',
    'zh-CHS': 'check2',
  },
  language3: {
    'en': 'hhhh3',
    'zh-CHS': 'check3',
  }
};
