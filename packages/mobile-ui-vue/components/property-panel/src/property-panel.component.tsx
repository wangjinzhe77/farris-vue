 
 
/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by ，applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext, ref, watch, onMounted, onBeforeMount } from 'vue';
import { PropertyPanelProps, propertyPanelProps } from './composition/props/property-panel.props';
import { getPropertyConfigBySchema } from '../../dynamic-resolver/src/property-config-resolver';
import FPropertyPanelItemList from '../src/component/property-panel-item-list.component';

import './composition/class/property-panel.css';
import { ElementPropertyConfig } from '../../designer-canvas/src/composition/entity/property-entity';

export default defineComponent({
  name: 'FPropertyPanel',
  props: propertyPanelProps,
  emits: [] as (string[] & ThisType<void>) | undefined,
  setup(props: PropertyPanelProps, context: SetupContext) {
    const width = ref(props.width);
    const isWidePanel = ref(props.isWidePanel);
    /** 是否启用搜索 */
    const enableSearch = ref(props.enableSearch);
    /** 使用模式 */
    const mode = ref(props.mode);
    /** 是否持有面板的隐藏显示状态 */
    const isPersitOpenState = ref(props.isPersitOpenState);
    /** isPersitOpenState=true时，控制面板是否隐藏显示 */
    const isShowPanel = ref(props.isShowPanel);
    /** 属性类型 */
    const propertyConfig = ref();
    /** 属性值 */
    const propertyData = ref();
    /** 是否展示关闭按钮 */
    const showCloseBtn = ref(props.showCloseBtn);
    /** 当前选中的标签页id */
    const selectedTabId = ref(props.selectedTabId);
    /** 当前显示状态 */
    const isOpen = ref(true);
    /** 是否是白色主题 */
    const isWhiteTheme = ref(props.isWhiteTheme);
    /** 外层分类，以标签页形式展示 */
    let categoryTabs: any = [];
    let properties: any = [];
    const keyword = ref('');
    const fPropertyPanel = ref<HTMLDivElement>();
    const propertyPanel = ref<HTMLDivElement>();

    let matchedElementRefs: Array<HTMLElement> = [];

    /** 当前选中的标签页 */
    let selectedTab: any;

    function collectProperties() {
      properties = [];
      if (selectedTab && selectedTab.categoryList && Array.isArray(selectedTab.categoryList) && selectedTab.categoryList.length > 0) {
        const categoryLists = selectedTab.categoryList;
        categoryLists.forEach((category: any) => {
          category.properties.forEach((item: any) => item.categoryId = category.categoryId);
          if (category.properties && Array.isArray(category.properties) && category.properties.length > 0) {
            const props = category.properties.map((item: any) => {
              item.category = category;
              return item;
            });
            properties = properties.concat(props);
          }
        });
      }
    }

    function getElementTop(element: any) {
      let actualTop = element.offsetTop;
      let current = element.offsetParent;
      while (current !== null) {
        actualTop += current.offsetTop;
        current = current.offsetParent;
      }
      return actualTop;
    }
    function setElementStyle(element: any, styles: any) {
      if (element && styles && Object.keys(styles).length > 0) {
        Object.keys(styles).forEach((style: string) => {
          const value = styles[style];
          element.style?.setProperty(style, value);
        });
      }
    }

    /** 比较搜索值与属性面板 */
    function compareSearchValue() {
      const items = properties.filter((item: any) => {
        if (!item.visible) {
          return false;
        }
        if (item.propertyID && item.propertyID.startsWith(keyword.value)) {
          return true;
        }
        if (item.propertyName && (item.propertyName.startsWith(keyword.value) || item.propertyName.includes(keyword.value))) {
          return true;
        }
        return false;
      });
      return items;
    }

    function scrollPanel(index: number, panelElement: HTMLElement, element: HTMLElement) {
      if (index === 0) {
        const panelBodyTop = getElementTop(panelElement);
        const elementTop = getElementTop(element);
        const offsetTop = elementTop - panelBodyTop - 5;
        panelElement?.scroll({
          top: offsetTop,
          behavior: 'smooth'
        });
      }
    }

    /** 搜索框属性值变化 */
    function onValueChangeEvent(searchKey?: string) {
      if (matchedElementRefs && matchedElementRefs.length > 0) {
        matchedElementRefs.forEach((element: any) => {
          element.style.cssText = '';
        });
        matchedElementRefs = [];
      }
      keyword.value = searchKey || keyword.value;
      const panelElement = document.querySelector(".panel-body") as HTMLElement;
      if (!keyword.value) {
        if (fPropertyPanel.value && panelElement) {
          panelElement.scrollTop = 0;
        }

      } else {
        collectProperties();
        if (properties && properties.length > 0) {
          const items = compareSearchValue();
          if (items && items.length > 0) {
            if (items[0].category.status === 'closed') {
              items[0].category.status = 'open';
            }
            setTimeout(() => {
              items.forEach((item: any, index: number) => {
                const selector = item.propertyType === 'cascade' ? `.${item.categoryId}-${item.propertyID} div.col-form-label` :
                  `.property-item .${item.categoryId}-${item.propertyID} label.col-form-label`;
                const element = document.querySelector(selector) as HTMLElement;
                if (element) {
                  scrollPanel(index, panelElement, element);
                  setElementStyle(element, { color: '#5B89FE' });
                  matchedElementRefs.push(element);
                }
              });
            }, 50);
          }
        }
      }
    }

    function search(searchKey?: string) {
      onValueChangeEvent(searchKey);
    }
    /**
     * 回车搜索事件
     * @param event
     */
    function onSearchBoxKeyUpEvent(event: KeyboardEvent) {
      const { value } = event.target as any;
      if (event.key === 'Enter') {
        search(value);
      }
    }
    function onSearchEvent(event: any) {
      const { value } = event.target;
      search(value);
    }

    /**
     *  隐藏面板
     */
    function collapse() {
      // isPersitOpenState=true时,由外部确定状态
      if (!isPersitOpenState.value) {
        isOpen.value = false;
      }
      context.emit('closePropertyPanel');
    }

    function onClearEvent(event: any) {
      keyword.value = '';
      onValueChangeEvent();
    }
    /** 收折 */
    function changeStatus(item: any) {
      if (!item.status || item.status === 'open') {
        item.status = 'closed';
      } else {
        item.status = 'open';
      }
    }

    /**
     * 将属性分类按照标签页进行归类
     */
    function checkShowTabCategory() {
      categoryTabs = [];
      if (!propertyConfig.value || propertyConfig.value.length === 0) {
        categoryTabs = [
          {
            tabId: 'default',
            tabName: '属性',
            categoryList: []
          }
        ];
        selectedTab = null;
        return;
      }
      propertyConfig?.value?.forEach((config: any) => {
        if (config.tabId) {
          const propTab = categoryTabs.find((t: any) => t.tabId === config.tabId) as any;
          if (!propTab) {
            categoryTabs.push({
              tabId: config.tabId,
              tabName: config.tabName,
              categoryList: [config],
              hide: config.hide || config.properties.length === 0
            });
          } else {
            propTab.categoryList.push(config);
            if (propTab.hide) {
              propTab.hide = config.hide || config.properties.length === 0;
            }
          }
        } else {
          const defaultTab = categoryTabs.find((t: any) => t.tabId === 'default') as any;
          if (!defaultTab) {
            categoryTabs.push({
              tabId: 'default',
              tabName: '属性',
              categoryList: [config]
            });
          } else {
            defaultTab.categoryList.push(config);
          }
        }
      });
      // 记录已选的页签
      if (selectedTabId.value) {
        let selectedTab = categoryTabs?.find((tab: any) => tab.tabId === selectedTabId.value && !tab.hide);
        selectedTab = selectedTab || categoryTabs[0];
      } else {
        selectedTab = categoryTabs[0];
      }
      selectedTabId.value = selectedTab?.tabId;
    }

    // 触发属性面板更新的时机
    watch(() => [props.schema, props.isShowPanel], () => {
      propertyData.value = props.schema;
      propertyConfig.value = getPropertyConfigBySchema(propertyData.value);
      checkShowTabCategory();
      onClearEvent(keyword.value);
    });

    onMounted(() => {
      checkShowTabCategory();
    });

    /** 收折属性面板 */
    function onSwitcherClickEvent() {
      mode.value = mode.value === 'panel' ? 'sidebar' : 'panel';
      // 收折时清空搜索框；
      if (mode.value === 'panel') {
        setTimeout(() => {
          search();
        }, 100);
        width.value = '300px';
      } else {
        width.value = '0px';
      }
    }
    /** 关闭按钮 */
    function handleShowCloseBtn() {
      if (showCloseBtn.value) {
        return (
          <div class="title-actions">
            <div class="monaco-toolbar">
              <div class="monaco-action-bar animated">
                <ul class="actions-container" role="toolbar">
                  <li class="action-item" onClick={collapse}>
                    <span class="f-icon f-icon-close"></span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        );
      }
    }
    function refreshPanel() {
      checkShowTabCategory();
    }
    /** 搜索框 */
    function hanleSearchComponent() {
      if (enableSearch.value) {
        return (
          <div class="search">
            <div class="input-group f-state-editable border-left-0 border-right-0">
              <input
                class="form-control f-utils-fill text-left pt-3 pb-3 textbox"
                type="text"
                placeholder="输入属性名称或编号快速定位"
                autocomplete="off"
                onKeyup={(event) => onSearchBoxKeyUpEvent(event)}
                value={keyword.value}></input>
              <div class="input-group-append" style="margin-left: 0px;">
                <span
                  class="input-group-text input-group-clear"
                  style={[{ display: keyword.value && keyword.value.length > 0 ? '' : 'none' }]}
                  onClick={(event) => onClearEvent(event)}>
                  <i class="f-icon modal_close"></i>
                </span>
                <span class="input-group-text input-group-clear" onClick={(event) => onSearchEvent(event)}>
                  <i class="f-icon f-icon-search"></i>
                </span>
              </div>
            </div>
          </div>
        );
      }
    }

    function getCategoryKey(category: ElementPropertyConfig) {
      return `${props.propertyName}_${category.categoryId}`;
    }

    /** 属性面板值 */
    function handlePanelBody() {
      return (
        <div class="panel-body" ref={propertyPanel}>
          <ul class={['property-grid', { 'wide-panel': isWidePanel.value }]}>
            {propertyConfig.value?.map((category: ElementPropertyConfig) => {
              return (
                <li key={getCategoryKey(category)}>
                  {!category.hide && !category.hideTitle && (
                    <span class="group-label" onClick={() => changeStatus(category)}>
                      <span
                        class={[
                          'f-icon  mr-2',
                          { 'f-legend-show': category.status === 'closed' },
                          {
                            'f-legend-collapse': category.status === 'open' || category.status === undefined
                          }
                        ]}></span>
                      {category.categoryName}
                    </span>
                  )}
                  <div hidden={category.status === 'closed'}>
                    <FPropertyPanelItemList
                      category={category}
                      propertyData={propertyData.value}
                      triggerRefreshPanel={refreshPanel}></FPropertyPanelItemList>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      );
    }
    /** 面板展示样式 */
    function handleDisplayMode() {
      if (mode.value === 'sidebar') {
        return (
          <div class="side-panel h-100" onClick={onSwitcherClickEvent}>
            <i class="f-icon f-icon-engineering w-100 icon"></i>
            <span>属性</span>
          </div>
        );
      }
    }
    function handlePropertyPanelStyleObject() {
      const propertyPanelStyleObject = {
        display: isOpen.value ? 'block' : 'none',
        width: width.value
      } as Record<string, any>;
      return propertyPanelStyleObject;
    }
    function handleSwitcher() {
      return (
        <div class="switcher">
          <i
            class="f-icon f-icon-exhale-discount"
            style={[{ transform: mode.value === 'sidebar' ? 'none' : 'rotate(180deg)' }]}
            onClick={onSwitcherClickEvent}></i>
        </div>
      );
    }
    function onChangeSelectedTab(tab: any) {
      selectedTab = tab;
      selectedTabId.value = selectedTab.tabId;
      keyword.value = '';
    }
    function handCategoryTabs() {
      return categoryTabs.map((tab: any) => {
        return (
          <div
            class={['title-label', { active: selectedTab && selectedTab.tabId === tab.tabId }, { hidden: tab.hide }]}
            onClick={(tab: any) => onChangeSelectedTab(tab)}>
            <span>{tab.tabName}</span>
          </div>
        );
      });
    }
    onMounted(() => {
      search(keyword.value);
    });
    onBeforeMount(() => {
      if (isPersitOpenState.value) {
        isOpen.value = isShowPanel.value;
      }
      checkShowTabCategory();
      search(keyword.value);
    });
    return () => {
      return (
        <>
          <div
            ref={fPropertyPanel}
            class={['property-panel', { 'white-theme': isWhiteTheme.value }]}
            style={handlePropertyPanelStyleObject()}>
            <div class='propertyPanel panel flex-column' hidden={mode.value !== 'panel'}>
              <div class={['title d-flex', { 'p-right': showCloseBtn.value }, { only: categoryTabs.length === 1 }]}>
                {handCategoryTabs()}
                {handleShowCloseBtn()}
              </div>
              {hanleSearchComponent()}
              {handlePanelBody()}
            </div>
            {handleSwitcher()}
            {handleDisplayMode()}
          </div >
        </>
      );
    };
  }
});
