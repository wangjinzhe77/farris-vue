import { withInstall } from '@components/common';
import ButtonEditInstallless from './src/button-edit.component';

export * from './src/button-edit.props';
export * from './src/composition/use-button-edit-props';

const ButtonEdit = withInstall(ButtonEditInstallless);

export { ButtonEdit };
export default ButtonEdit;
