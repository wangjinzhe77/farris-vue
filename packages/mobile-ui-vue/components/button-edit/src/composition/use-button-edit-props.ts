import { computed, ref, watch } from "vue";
import { ButtonEditProps } from "../button-edit.props";

export const useButtonEditProps = (props: Omit<ButtonEditProps, 'modelValue'>)=>{

  const showPopup = ref(false);
  watch(()=>props.show, (newValue)=>{
    showPopup.value = newValue;
  });
  const inputValue = ref('');

  const buttonEditProps = computed(() => {
    return {
      // input-group
      modelValue: inputValue.value,
      placeholder: props.placeholder,
      disabled: props.disabled,
      readonly: props.readonly,
      editable: props.editable,
      rightIcon: props.readonly || 's-arrow',
      textAlign: props.textAlign,
      formatter: props.formatter,
      // popup
      show: showPopup.value,
      popupHeight: props.popupHeight,
      popupWidth: props.popupWidth,
      round: props.round,
      zIndex: props.zIndex,
      position: props.position,
      popupClass: props.popupClass,
      overlayClass: props.overlayClass,
      overlayStyle: props.overlayStyle,
      useNativeBack: props.useNativeBack,
      teleport: props.teleport,
      overlay: props.overlay,
      closeOnClickOverlay: props.closeOnClickOverlay,
      useClickAway: props.useClickAway
    };
  });
  return {
    showPopup,
    inputValue,
    buttonEditProps
  };
};
