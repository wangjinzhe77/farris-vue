import { computed, defineComponent, ref, watch } from 'vue';
import InputGroup from '@/components/input-group';
import { useBem } from '@/components/common';
import Popup from '@/components/popup';
import Tag from '@/components/tag';
import { BUTTON_EDIT_NAME, ButtonEditProps, buttonEditProps } from './button-edit.props';

export default defineComponent({
  name: BUTTON_EDIT_NAME,
  props: buttonEditProps,
  emits: ['update:modelValue', 'update:show', 'input'],
  setup(props: ButtonEditProps, context) {
    const { emit, slots } = context;

    const { bem } = useBem(BUTTON_EDIT_NAME);

    const innerValue = ref(props.modelValue);
    watch(() => props.modelValue, (newValue) => {
      innerValue.value = newValue;
    });

    const handleModelChange = (value: string) => {
      emit('update:modelValue', value);
    };

    const tags = computed(() => {
      if (props.multiSelect && innerValue.value) {
        return [];
      }
      return innerValue.value.split(props.separator).filter(tag => !!tag);
    });
    const handleTagClose = (value: string) => {
      innerValue.value = tags.value.filter(tag => tag !== value).join(props.separator);
      handleModelChange(innerValue.value);
    };
    const tagProps = computed(() => {
      return {
        class: bem('tag'),
        type: props.tagType,
        color: props.tagColor,
        textColor: props.tagTextColor,
        size: props.tagSize,
        plain: props.tagPlain,
        round: props.tagRound,
        closeable: props.tagCloseable
      };
    });
    const renderTags = () => {
      return <div class={bem('tags')}>
        {tags.value.map(tag => <Tag {...tagProps.value} onClose={() => handleTagClose(tag)}>{tag}</Tag>)}
      </div>;
    };

    const showPopup = ref(false);
    watch(() => props.show, (newValue) => {
      showPopup.value = newValue;
    });
    const handlePopupShow = (status: boolean, enable = true) => {
      if (props.disabled || props.readonly || !enable) {
        return;
      }
      showPopup.value = enable ? status : false;
      emit('update:show', showPopup.value);
    };

    const inputProps = computed(() => {
      return {
        modelValue: innerValue.value,
        placeholder: props.placeholder,
        disabled: props.disabled,
        readonly: props.readonly,
        editable: props.editable,
        class: props.inputClass,
        rightIcon: props.readonly || 's-arrow',
        textAlign: props.textAlign,
        showPadding: props.showPadding,
        formatter: props.formatter,
        onClick: () => handlePopupShow(true, !props.editable),
        onClickRightIcon: () => handlePopupShow(true),
        onChange: handleModelChange
      };
    });
    const inputSlots = {
      input: props.type === 'tag' ? renderTags : undefined
    };

    const renderButton = () => {
      return <InputGroup {...inputProps.value} v-slots={inputSlots}></InputGroup>;
    };

    const popupProps = computed(() => {
      return {
        show: showPopup.value,
        height: props.popupHeight,
        width: props.popupWidth,
        position: props.position,
        zIndex: props.zIndex,
        popupClass: props.popupClass,
        overlayClass: props.overlayClass,
        overlayStyle: props.overlayStyle,
        useNativeBack: props.useNativeBack,
        teleport: props.teleport,
        overlay: props.overlay,
        closeOnClickOverlay: props.closeOnClickOverlay,
        round: props.round,
        useClickAway: props.useClickAway,
        'onUpdate:show': handlePopupShow
      };
    });
    const renderPopup = () => {
      return <Popup {...popupProps.value}>
        {slots.default && slots.default()}
      </Popup>;
    };

    return () => (
      <div class={bem()}>
        {slots.button ? slots.button() : renderButton()}
        {renderPopup()}
      </div>
    );
  }
});
