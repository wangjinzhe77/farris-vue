import { ExtractPropTypes, PropType } from 'vue';
import { inputCommonProps } from '@/components/input-group';
import { popupProps } from '@/components/popup';
import { tagProps, TagSize, TagType } from '@/components/tag';

export const BUTTON_EDIT_NAME = 'FmButtonEdit';

type Type = 'text' | 'tag';

export const buttonEditProps = {
  ...inputCommonProps,
  ...popupProps,

  modelValue: { type: String, default: '' },

  type: { type: String as PropType<Type>, default: 'text' },

  editable: { type: Boolean, default: false },

  inputClass: { type: String, default: undefined },

  tagType: { type: String as PropType<TagType>, default: 'primary' },

  tagColor: { type: String, default: undefined },

  tagTextColor: { type: String, default: undefined },

  tagSize: { type: String as PropType<TagSize>, default: 'medium' },

  tagPlain: { type: Boolean, default: false },

  tagRound: { type: Boolean, default: false },

  tagCloseable: { type: Boolean, default: false },

  popupWidth: { type: String, default: undefined },

  popupHeight: { type: String, default: undefined },

  position: { type: String, default: 'bottom' },

  multiSelect: { type: Boolean, default: false },

  separator: { type: String, default: ',' }
};

export type ButtonEditProps = ExtractPropTypes<typeof buttonEditProps>;
