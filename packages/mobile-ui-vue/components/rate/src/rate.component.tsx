/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* eslint-disable no-use-before-define */
import { defineComponent, SetupContext, computed, ref } from "vue";
import { RATE_NAME, rateProps, RateProps } from "./rate.props";
import { useTouch, useRefs, useRect, useEventListener, useBem } from "@components/common";
import { RateItem } from "./composition/types";
import { addUnit, preventDefault } from "@components/common";
import { Icon } from "@components/icon";

export default defineComponent({
  name: RATE_NAME,

  props: rateProps,

  emits: ["change", "update:modelValue"],

  setup(props: RateProps, context: SetupContext) {
    const { bem } = useBem(RATE_NAME);
    const { emit } = context;

    const touch = useTouch();
    const [itemRefs, setItemRefs] = useRefs();
    const rootElementRef = ref<Element>();

    const unselectable = computed(() => props.readonly || props.disabled);

    const untouchable = computed(() => unselectable.value || !props.touchable);

    const rateItems = computed(() => convertRatingValue2RateItems(props.modelValue));

    const convertRatingValue2RateItems = (value: number): RateItem[] => {
      const itemCount = +props.count;
      return Array(itemCount)
        .fill('')
        .map((_, index) => getRateItemByIndex(value, index));
    };

    const getRateItemByIndex = (value: number, index: number): RateItem => {
      const score = index + 1;
      const { allowHalf, readonly } = props;
      // 评分大于当前星星所代表的评分时，整个星星被点亮
      if (value >= score) {
        return { status: "full", fillRatio: 1 };
      }

      // 非只读状态下，最小粒度是半颗星，值低于0.5则完全不点亮
      if (value + 0.5 >= score && allowHalf && !readonly) {
        return { status: "half", fillRatio: 0.5 };
      }

      // 只读状态下，支持精确显示
      if (value + 1 >= score && allowHalf && readonly) {
        const cardinal = 10 ** 10;
        return {
          status: "half",
          fillRatio: Math.round((value - score + 1) * cardinal) / cardinal,
        };
      }

      return { status: "void", fillRatio: 0 };
    };

    /** 评分以及评分在屏幕上所对应的区域 */
    let scorePositionRanges: Array<{
      score: number;
      left: number;
      top: number;
      height: number;
    }>;

    let rootElementRect: DOMRect;
    let minRectTop = Number.MAX_SAFE_INTEGER;
    let maxRectTop = Number.MIN_SAFE_INTEGER;

    const updateScorePositionRanges = () => {
      rootElementRect = useRect(rootElementRef);

      const rects = itemRefs.value.map(useRect);

      scorePositionRanges = [];
      rects.forEach((rect, index) => {
        minRectTop = Math.min(rect.top, minRectTop);
        maxRectTop = Math.max(rect.top, maxRectTop);

        if (props.allowHalf) {
          scorePositionRanges.push({
            score: index + 0.5,
            left: rect.left,
            top: rect.top,
            height: rect.height,
          }, {
            score: index + 1,
            left: rect.left + rect.width / 2,
            top: rect.top,
            height: rect.height,
          });
        } else {
          scorePositionRanges.push({
            score: index + 1,
            left: rect.left,
            top: rect.top,
            height: rect.height,
          });
        }
      });
    };

    const getScoreByPosition = (positionX: number, positionY: number) => {
      const x = positionX;
      const y = positionY;
      for (let index = scorePositionRanges.length - 1; index > 0; index--) {
        const isInRateRect = y >= rootElementRect.top && y <= rootElementRect.bottom;
        const rateItemTop = scorePositionRanges[index].top;
        const rateItemBottom = scorePositionRanges[index].top + scorePositionRanges[index].height;
        const isInRateItemRect = y >= rateItemTop && y <= rateItemBottom;
        const isOnRightSideOfRateItem = x > scorePositionRanges[index].left;

        if (isInRateRect && isInRateItemRect && isOnRightSideOfRateItem) {
          return scorePositionRanges[index].score;
        }
        if (!isInRateRect) {
          const currentTop = y < rootElementRect.top ? minRectTop : maxRectTop;
          if (isOnRightSideOfRateItem && scorePositionRanges[index].top === currentTop) {
            return scorePositionRanges[index].score;
          }
        }
      }
      return props.allowHalf ? 0.5 : 1;
    };

    const select = (newValue: number) => {
      const isSameValue = newValue === props.modelValue;
      if (unselectable.value || isSameValue) {
        return;
      }
      emit("update:modelValue", newValue);
      emit("change", newValue);
    };

    const onTouchStart = (event: TouchEvent) => {
      if (untouchable.value) {
        return;
      }

      touch.start(event);
      updateScorePositionRanges();
    };

    const onTouchMove = (event: TouchEvent) => {
      if (untouchable.value) {
        return;
      }

      touch.move(event);

      if (touch.isHorizontal() && !touch.isTap.value) {
        const { clientX, clientY } = event.touches[0];
        preventDefault(event);
        select(getScoreByPosition(clientX, clientY));
      }
    };

    const renderBackgroundIcon = (item: RateItem) => {
      const { size, iconPrefix } = props;
      const isFull = item.fillRatio === 1;
      const iconName = isFull ? props.icon : props.voidIcon;
      const iconClass = {
        [bem('icon')]: true,
        [bem('icon', 'disabled')]: props.disabled,
        [bem('icon', 'full')]: isFull,
      };
      let fillColor = isFull ? props.color : props.voidColor;
      fillColor = props.disabled ? props.disabledColor : fillColor;
      return (
        <Icon
          size={size}
          name={iconName}
          class={iconClass}
          color={fillColor}
          classPrefix={iconPrefix}
        />
      );
    };

    const renderForegroundIcon = (item: RateItem) => {
      const isHalfStar = props.allowHalf && item.fillRatio > 0 && item.fillRatio < 1;
      const shouldRenderForegroundIcon = isHalfStar;
      if (!shouldRenderForegroundIcon) {
        return;
      }
      const { size, iconPrefix, icon } = props;
      const halfIconClass = {
        [bem('icon')]: true,
        [bem('icon', 'half')]: true,
        [bem('icon', 'disabled')]: props.disabled,
      };
      const halfIconStyle = {
        width: `${item.fillRatio}em`,
      };
      const fillColor = props.disabled ? props.disabledColor : props.color;
      return (
        <Icon
          size={size}
          style={halfIconStyle}
          name={icon}
          class={halfIconClass}
          color={fillColor}
          classPrefix={iconPrefix}
        />
      );
    };

    const renderStar = (item: RateItem, index: number) => {
      const { count, gutter, disabled, allowHalf } = props;
      const score = index + 1;

      const onClickItem = (event: MouseEvent) => {
        updateScorePositionRanges();
        let newValue = allowHalf
          ? getScoreByPosition(event.clientX, event.clientY)
          : score;
        const isTap = touch.isTap.value;
        const isSameValue = newValue === props.modelValue;
        const shouldClear = props.clearable && isTap && isSameValue;
        if (shouldClear) {
          newValue = 0;
        }
        select(newValue);
      };

      const notLastStar = score !== +count;
      const shouldShowGutter = gutter && notLastStar;
      const starStyle = {
        paddingRight: shouldShowGutter ? addUnit(gutter) : undefined,
      };
      const itemTabIndex = disabled ? undefined : 0;

      return (
        <div
          key={index}
          ref={setItemRefs(index)}
          style={starStyle}
          class={bem('item')}
          tabindex={itemTabIndex}
          onClick={onClickItem}
        >
          {renderBackgroundIcon(item)}
          {renderForegroundIcon(item)}
        </div>
      );
    };

    useEventListener('touchstart', onTouchStart, {
      target: rootElementRef,
      passive: true,
    });
    useEventListener('touchmove', onTouchMove, {
      target: rootElementRef,
    });

    const rateClass = computed(() => {
      return {
        [bem()]: true,
        [bem('', 'readonly')]: props.readonly,
        [bem('', 'disabled')]: props.disabled,
      };
    });

    const tabIndex = computed(() => {
      return props.disabled ? undefined : 0;
    });

    return () => (
      <div ref={rootElementRef} class={rateClass.value} tabindex={tabIndex.value}>
        {rateItems.value.map(renderStar)}
      </div>
    );
  },
});
