export type RateItemStatus = 'full' | 'half' | 'void';

/** 评分条目 */
export type RateItem = {

  /** 填充状态 */
  status: RateItemStatus;

  /**
   * 具体的填充比例
   * @description 取值范围`[0, 1]`，`1`表示完全填充
   */
  fillRatio: number;
};
