/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';

export const RATE_NAME = 'fm-rate';

export const rateProps = {

  /** 当前评分，支持语法糖`v-model` */
  modelValue: { type: Number, default: 0 },

  /** 图标大小，单位`px` */
  size: { type: [Number, String], default: undefined },

  /** 是否可清空，如果为真，则再次点击相同的分数时清空评分 */
  clearable: { type: Boolean, default: false },

  /** 是否只读 */
  readonly: { type: Boolean, default: false },

  /** 是否禁用 */
  disabled: { type: Boolean, default: false },

  /** 是否可选半颗星 */
  allowHalf: { type: Boolean, default: false },

  /** 选中时的图标名称 */
  icon: { type: String, default: 's-star' },

  /** 未选中时的图标名称 */
  voidIcon: { type: String, default: 's-star-o' },

  /** 选中时的填充颜色 */
  color: { type: String, default: undefined },

  /** 未选中时的填充颜色 */
  voidColor: { type: String, default: undefined },

  /** 禁用时的填充颜色 */
  disabledColor: { type: String, default: undefined },

  /** 图标总数量，默认为5个 */
  count: { type: [Number, String], default: 5 },

  /** 图标之间的间距，单位`px` */
  gutter: { type: [Number, String], default: undefined },

  /** 是否可以通过滑动改变评分 */
  touchable: { type: Boolean, default: true },

  /** 图标名称的前缀 */
  iconPrefix: { type: String, default: undefined },

} as Record<string, any>;

export type RateProps = ExtractPropTypes<typeof rateProps>;
