import { withInstall } from '@components/common';
import RateInstallless from './src/rate.component';

export * from './src/rate.props';

const Rate = withInstall(RateInstallless);

export { Rate };
export default Rate;
