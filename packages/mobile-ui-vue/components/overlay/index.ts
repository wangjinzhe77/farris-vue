import { withInstall } from '@components/common';
import OverlayInstallless from './src/overlay.component';

const Overlay = withInstall(OverlayInstallless);

export { Overlay };
export default Overlay;
