/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { computed, CSSProperties, defineComponent, SetupContext, Transition } from 'vue';
import { preventDefault, noop, useBem, useLazyRender } from '@components/common';
import { OVERLAY_NAME, OverlayProps, overlayProps } from './overlay.props';

export default defineComponent({
  name: OVERLAY_NAME,
  props: overlayProps,
  emits: ['touchend'],
  setup(props: OverlayProps, context: SetupContext<('touchend')[]>) {
    const { slots } = context;

    const lazyRender = useLazyRender(() => props.show);

    const overlayStyle = computed<CSSProperties>(() => {
      return {
        zIndex: props.zIndex,
        animationDuration: props.duration ? `${props.duration}s` : undefined
      };
    });

    const { bem } = useBem(OVERLAY_NAME);

    const preventTouchMove = (event: TouchEvent) => {
      preventDefault(event, true);
    };

    const renderOverlay = lazyRender(() => {
      return (
        <div
          v-show={props.show}
          class={bem()}
          style={overlayStyle.value}
          onTouchmove={props.lockScroll ? preventTouchMove : noop}>
          {slots.default?.()}
        </div>
      );
    });
    return () => <Transition name="fm-fade">{renderOverlay()}</Transition>;
  }
});
