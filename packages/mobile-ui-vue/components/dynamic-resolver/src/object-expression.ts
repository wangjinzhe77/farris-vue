export type ObjectExpressionFunction = (target: string, param: any, value: any, context: Record<string, any>) => boolean;

interface Expression {
  target: string;
  operator: string;
  param: any;
  value: any;
}

/**
 * 解析对象表达式描述的布尔值
 * @param valueSchema 布尔值对象描述
 * @param context 解析表达式对象值所需的上下文对象
 * @returns boolean
 * @example
 * // 简化形式表示数组长度为0
 * {"sibling":0}
 * @example
 * // 数组长度不等于1
 * {
 *  "children":{
 *      "length":{
 *          "not":1
 *      }
 *  }
 * }
 * @example
 * // 存在某元素
 * {
 *  "children":{
 *      "f-page-content":false
 *  }
 * }
 * {
 *  "parent":{
 *      "f-page-main":true
 *  }
 * }
 * @example
 * // 与条件
 * {
 *  "allOf":[
 *      {
 *          "sibling":0,
 *          "parent":{
 *              "f-page-main":true
 *          }
 *      }
 *  ]
 * }
 * // 或条件
 * {
 *  "anyOf":[
 *      {
 *          "children":{
 *              "length":{
 *                  "equal":1
 *              },
 *              "f-struct-like-card":true
 *          }
 *      },
 *      {
 *          "children":{
 *              "scroll-spy":false,
 *              "f-page-content":false,
 *              "f-struct-like-card":false
 *          }
 *      }
 *  ]
 * }
 */
export function useObjectExpression(extendFunctions: Record<string, ObjectExpressionFunction> = {}) {

  function judgingElementCount(target: string, param: any, value: any, context: Record<string, any>) {
    if (typeof value === 'number') {
      return context[target].length === value;
    }
    if (typeof value === 'object') {
      const compare = Object.keys(value)[0];
      const targetValue = value[compare];
      if (compare === 'not') {
        return Number(context[target].length) !== Number(targetValue);
      }
      if (compare === 'moreThan') {
        return Number(context[target].length) >= Number(targetValue);
      }
      if (compare === 'lessThan') {
        return Number(context[target].length) <= Number(targetValue);
      }
    }
    return false;
  }

  function getProperty(target: string, param: any, value: any, context: Record<string, any>) {
    return context[param] && context[param].propertyValue && String(context[param].propertyValue.value) === String(value);
  }

  const expressionCalculateFunctions = new Map<string, ObjectExpressionFunction>([
    ['length', judgingElementCount],
    ['getProperty', getProperty]
  ]);

  Object.keys(extendFunctions).reduce((functionMaps: Map<string, ObjectExpressionFunction>, operator: string) => {
    functionMaps.set(operator, extendFunctions[operator]);
    return functionMaps;
  }, expressionCalculateFunctions);

  function generateExpressions(token: string, valueObject: any): Expression[] {
    const target = token;
    if (typeof valueObject === 'number') {
      return [{ target, operator: 'length', param: null, value: Number(valueObject) }];
    }
    if (typeof valueObject === 'boolean') {
      return [{ target, operator: 'getProperty', param: token, value: Boolean(valueObject) }];
    }
    if (typeof valueObject === 'object') {
      return Object.keys(valueObject).map((key: string) => {
        if (key === 'length') {
          return { target, operator: 'length', param: null, value: valueObject[key] };
        }
        const param = key;
        const value = valueObject[key];
        const operator = token;
        return { target, operator, param, value };
      });
    }
    return [];
  }

  function parseValueObjectToExpression(valueObject: Record<string, any>): Expression[] {
    const parsedExpression = Object.keys(valueObject).reduce((result: Expression[], token: string) => {
      const expressions = generateExpressions(token, valueObject[token]);
      result.push(...expressions);
      return result;
    }, []);
    return parsedExpression;
  }

  function calculateExpression(expression: Expression, context: Record<string, any>) {
    if (expressionCalculateFunctions.has(expression.operator)) {
      const calculateFunction = expressionCalculateFunctions.get(expression.operator);
      return calculateFunction && calculateFunction(expression.target, expression.param, expression.value, context) || false;
    }
    return false;
  }

  function calculate(valueObject: Record<string, any>, context: Record<string, any>): boolean {
    const parsedExpression = parseValueObjectToExpression(valueObject);
    const result = parsedExpression.reduce((parsingResult: boolean, expression: Expression) => {
      return parsingResult && calculateExpression(expression, context);
    }, true);

    return result;
  }

  function parseValueSchema(valueSchema: Record<string, any>, context: Record<string, any>): boolean {
    const schemaKeys = Object.keys(valueSchema);
    const allOf = schemaKeys.includes('allOf');
    const anyOf = schemaKeys.includes('anyOf');
    const hasLogicalOperatorsInSchemaKey = allOf || anyOf;
    const logicalOperator = hasLogicalOperatorsInSchemaKey ? (allOf ? 'allOf' : 'anyOf') : 'allOf';
    const valueObjects = (hasLogicalOperatorsInSchemaKey ? valueSchema[logicalOperator] : [valueSchema]) as Record<string, any>[];
    const expressionValues = valueObjects.map((valueObject: Record<string, any>) => calculate(valueObject, context));
    const result = allOf ? !expressionValues.includes(false) : expressionValues.includes(true);
    return result;
  }

  return { parseValueSchema };
}
