export type MapperFunction = (key: string, value: any, resolvedSchema?: any) => Record<string, any>;

export interface PropertyConverter {

  convertTo: (schema: Record<string, any>, propertyKey: string, propertyValue: any) => void;

  convertFrom: (schema: Record<string, any>, propertyKey: string) => any;
}

export interface DynamicResolver {
  getSchemaByType: (componentType: string) => Record<string, any> | null;
  getPropertyConfigByType?: (componentType: string) => Record<string, any> | null;
};

export type SchemaResolverFunction = (
  dynamicResolver: DynamicResolver,
  schema: Record<string, any>,
  resolveContext: Record<string, any>
) => Record<string, any>;

export type EffectFunction = (source: Record<string, any>) => Record<string, any>;

export type EditorType = 'button-edit' | 'check-box' | 'combo-list' | 'combo-lookup'
  | 'date-picker' | 'date-range' | 'datetime-picker' | 'datetime-range' | 'month-picker' | 'month-range' | 'year-picker' | 'year-range'
  | 'input-group' | 'lookup' | 'number-range' | 'number-spinner' | 'radio-group' | 'text';

export interface EditorConfig {
  /** 编辑器类型 */
  type: EditorType;
  /** 自定义样式 */
  customClass?: string;
  /** 禁用 */
  disable?: boolean;
  /** 只读 */
  readonly?: boolean;
  /** 必填 */
  required?: boolean;
  /** 提示文本 */
  placeholder?: string;
  /** 其他属性 */
  [key: string]: any;
}
