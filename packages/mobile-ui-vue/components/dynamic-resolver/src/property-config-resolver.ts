import { computed } from "vue";
import { ElementPropertyConfig, PropertyEntity } from "../../designer-canvas/src/composition/entity/property-entity";
import { EffectFunction, PropertyConverter, EditorConfig } from './types';
import { useObjectExpression } from './object-expression';
import { ComponentSchema } from "../../designer-canvas/src/types";
import { resolveSchemaWithDefaultValue } from "./schema-resolver";
import propertyEditorConverter from "./converter/property-editor.converter";
import buttonsConverter from "./converter/buttons.converter";

const propertyConfigSchemaMap = {} as Record<string, any>;
const propertyConverterMap = new Map<string, PropertyConverter>([
  ['/converter/property-editor.converter', propertyEditorConverter],
  ['/converter/buttons.converter', buttonsConverter]
]);
const propertyEffectMap = {} as Record<string, EffectFunction>;
const propertyEditorMap = new Map<string, EditorConfig>([
  ['string', { type: 'input-group', enableClear: false }],
  ['boolean', {
    "type": "combo-list",
    "textField": "name",
    "valueField": "value",
    "maxHeight": 64,
    "data": [
      {
        "value": true,
        "name": "是"
      },
      {
        "value": false,
        "name": "否"
      }
    ]
  }],
  ['enum', { "type": "combo-list", "maxHeight": 128, enableClear: false }],
  ['array', { "type": "button-edit" }],
  ['number', { "type": "number-spinner" }]
]);

const useObjectExpressionComponstion = useObjectExpression();

function generateBooleanValue(pvalueSchema: Record<string, any>, propertyConfigMap: Record<string, PropertyEntity>) {
  return () => useObjectExpressionComponstion.parseValueSchema(pvalueSchema, propertyConfigMap);
}

function isVisible(propertySchemaKeys: string[], propertySchema: Record<string, any>, propertyConfigMap: Record<string, PropertyEntity>) {
  if (propertySchemaKeys.includes('visible')) {
    return typeof propertySchema.visible === 'boolean' ?
      () => Boolean(propertySchema.visible) :
      generateBooleanValue(propertySchema.visible, propertyConfigMap);
  }
  return () => true;
}

function isReadonly(propertySchemaKeys: string[], propertySchema: Record<string, any>, propertyConfigMap: Record<string, PropertyEntity>) {
  if (propertySchemaKeys.includes('readonly')) {
    return typeof propertySchema.visible === 'boolean' ?
      () => Boolean(propertySchema.visible) :
      generateBooleanValue(propertySchema.readonly, propertyConfigMap);
  }
  return () => false;
}

function tryGetPropertyConverter(propertySchema: Record<string, any>): PropertyConverter | null {
  const { $converter } = propertySchema;
  if ($converter && propertyConverterMap.has($converter)) {
    return propertyConverterMap.get($converter) || null;
  }
  return null;
}

function getPropertyEntities(
  propertiesInCategory: Record<string, any>, propertyConfigMap: Record<string, PropertyEntity>, editingSchema: ComponentSchema, rawSchema: ComponentSchema
): PropertyEntity[] {
  const propertyEntities = Object.keys(propertiesInCategory).map<PropertyEntity>((propertyKey: string) => {
    const propertyID = propertyKey;
    const propertySchema = propertiesInCategory[propertyKey];
    const propertySchemaKeys = Object.keys(propertySchema);
    const propertyName = propertySchema.title;
    const propertyType = propertySchema.type;
    const defaultEditor = propertyEditorMap.get(propertyType) || { type: 'input-group', enableClear: false };
    const editor = propertySchema.editor ? Object.assign({}, defaultEditor, propertySchema.editor) as EditorConfig : defaultEditor;
    const visible = isVisible(propertySchemaKeys, propertySchema, propertyConfigMap);
    const readonly = isReadonly(propertySchemaKeys, propertySchema, propertyConfigMap);
    const cascadeConfig = propertySchema.type === 'cascade' ? getPropertyEntities(propertySchema.properties, propertyConfigMap, editingSchema, rawSchema) : [];
    const hideCascadeTitle = true;
    const converter = tryGetPropertyConverter(propertySchema);
    const propertyValue = computed({
      get() {
        return (converter && converter.convertFrom) ? converter.convertFrom(editingSchema, propertyKey) : editingSchema[propertyKey];
      },
      set(newValue) {
        if (converter && converter.convertTo) {
          converter.convertTo(rawSchema, propertyKey, newValue);
          converter.convertTo(editingSchema, propertyKey, newValue);
        } else {
          rawSchema[propertyKey] = newValue;
          editingSchema[propertyKey] = newValue;
        }
      }
    });
    const propertyEntity = { propertyID, propertyName, propertyType, propertyValue, editor, visible, readonly, cascadeConfig, hideCascadeTitle };
    propertyConfigMap[propertyID] = propertyEntity;
    return propertyEntity;
  });
  return propertyEntities;
}

function getPropertyConfigByType(schemaType: string, schema = {} as ComponentSchema): ElementPropertyConfig[] {
  const propertyConfigMap = {} as Record<string, PropertyEntity>;
  const propertyConfigSchema = propertyConfigSchemaMap[schemaType];
  if (propertyConfigSchema && propertyConfigSchema.categories) {
    const propertyConfigs = Object.keys(propertyConfigSchema.categories).map<ElementPropertyConfig>((categoryId: string) => {
      const propertyCategory = propertyConfigSchema.categories[categoryId];
      const categoryName = propertyCategory?.title;
      const properties = getPropertyEntities(propertyCategory.properties || {}, propertyConfigMap, {} as ComponentSchema, schema);
      return { categoryId, categoryName, properties };
    });
    return propertyConfigs;
  }
  return [];
}

function tryToResolveReference(categoryId: string, propertyCategory: Record<string, any>, rawSchema: ComponentSchema) {
  const refSchemaPath = propertyCategory.$ref.schema;
  const $converter = propertyCategory.$ref.converter;
  const refSchema = rawSchema[refSchemaPath];

  const schemaType = refSchema.type;
  const editingSchema = resolveSchemaWithDefaultValue(refSchema) as ComponentSchema;
  const propertyConfigMap = {} as Record<string, PropertyEntity>;
  const propertyConfigSchema = propertyConfigSchemaMap[schemaType];
  if (propertyConfigSchema && propertyConfigSchema.categories) {
    const propertyCategory = propertyConfigSchema.categories[categoryId];
    const categoryName = propertyCategory?.title;
    if ($converter) {
      Object.keys(propertyCategory.properties).forEach((propertyKey: any) => {
        propertyCategory.properties[propertyKey].$converter = $converter;
      });
    }
    const propertiesInCategory = propertyCategory?.properties || {};
    const properties = getPropertyEntities(propertiesInCategory, propertyConfigMap, editingSchema, refSchema);
    return { categoryId, categoryName, properties };

  }
  return { categoryId, categoryName: '', properties: [] };
}

function getPropertyConfigBySchema(rawSchema: ComponentSchema): ElementPropertyConfig[] {
  const schemaType = rawSchema.type;
  const editingSchema = resolveSchemaWithDefaultValue(rawSchema) as ComponentSchema;
  const propertyConfigMap = {} as Record<string, PropertyEntity>;
  const propertyConfigSchema = propertyConfigSchemaMap[schemaType];
  if (propertyConfigSchema && propertyConfigSchema.categories) {
    const propertyConfigs = Object.keys(propertyConfigSchema.categories)
      .map<ElementPropertyConfig>((categoryId: string) => {
        const propertyCategory = propertyConfigSchema.categories[categoryId];
        if (propertyCategory.$ref) {
          return tryToResolveReference(categoryId, propertyCategory, rawSchema) as ElementPropertyConfig;
        }
        const categoryName = propertyCategory?.title;
        const properties = getPropertyEntities(propertyCategory.properties || {}, propertyConfigMap, editingSchema, rawSchema);
        return { categoryId, categoryName, properties };
      })
      .filter((propertyConfig: ElementPropertyConfig) => !!propertyConfig);;
    return propertyConfigs;
  }
  return [];
}

export { getPropertyConfigBySchema, getPropertyConfigByType, propertyConfigSchemaMap, propertyConverterMap, propertyEffectMap };
