import { resolveSchemaToProps, schemaMap, schemaResolverMap } from './schema-resolver';
import { DynamicResolver, EffectFunction, MapperFunction, SchemaResolverFunction } from './types';
import { propertyConfigSchemaMap, propertyEffectMap } from './property-config-resolver';

export function createPropsResolver(
  componentPropsObject: Record<string, any>,
  defaultSchema: Record<string, any>,
  schemaMapper: Map<string, string | MapperFunction> = new Map(),
  schemaResolver: SchemaResolverFunction = (
    dynamicResolver: DynamicResolver,
    schema: Record<string, any>,
    resolveContext: Record<string, any>
  ) => schema,
  propertyConfig: Record<string, any> = {},
  propertyEffect: EffectFunction = (properties: Record<string, any>) => properties
) {
  schemaMap[defaultSchema.title] = defaultSchema;
  schemaResolverMap[defaultSchema.title] = schemaResolver;
  propertyConfigSchemaMap[defaultSchema.title] = propertyConfig;
  propertyEffectMap[defaultSchema.title] = propertyEffect;
  return (schemaValue: Record<string, any> = {}) => {
    const resolvedPropsValue = resolveSchemaToProps(schemaValue, defaultSchema, schemaMapper);
    const defaultProps = Object.keys(componentPropsObject).reduce((propsObject: Record<string, any>, propKey: string) => {
      propsObject[propKey] = componentPropsObject[propKey].default;
      return propsObject;
    }, {});

    return Object.assign(defaultProps, resolvedPropsValue);
  };
}
