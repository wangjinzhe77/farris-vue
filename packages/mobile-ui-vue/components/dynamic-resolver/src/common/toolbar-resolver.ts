export function resolveToolbar(key: string, toolbarObject: { buttons: any[] }) {
  return { buttons: toolbarObject.buttons };
}
