import { ComponentSchema } from "../../../designer-canvas/src/types";
import { PropertyConverter } from "../types";

export default {
  convertTo: (schema: ComponentSchema, propertyKey: string, propertyValue: any) => {
    if (schema.editor) {
      schema.editor[propertyKey] = propertyValue;
    }
  },
  convertFrom: (schema: ComponentSchema, propertyKey: string) => {
    return schema.editor ? schema.editor[propertyKey] : schema[propertyKey];
  }
} as PropertyConverter;
