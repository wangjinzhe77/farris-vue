import { ComponentSchema } from "../../../designer-canvas/src/types";
import { PropertyConverter } from "../types";

export default {
  convertFrom: (schema: ComponentSchema, propertyKey: string) => {
    return (schema.buttons && schema.buttons.length) ? `共 ${schema.buttons.length} 项` : '无';
  }
} as PropertyConverter;
