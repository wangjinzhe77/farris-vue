import { SetupContext, defineComponent, computed } from 'vue';
import { PAGE_HEADER_CONTAINER_NAME, PageHeaderContainerProps, pageHeaderContainerProps } from './page-header-container.props';
import { useBem } from '@components/common';

export default defineComponent({
  name: PAGE_HEADER_CONTAINER_NAME,
  props: pageHeaderContainerProps,
  emits: [],
  setup(props: PageHeaderContainerProps, context: SetupContext) {
    const { bem } = useBem(PAGE_HEADER_CONTAINER_NAME);

    const containerClass = computed(() => ({
      [bem()]: true,
      [props.customClass || '']: true,
    }));

    return () => (
      <div class={containerClass.value}>
        {context.slots.default?.()}
      </div>
    );
  }
});
