import { ExtractPropTypes } from 'vue';
import { createPropsResolver } from '../../dynamic-resolver';
import { schemaMapper } from './schema/schema-mapper';
import pageHeaderContainerSchema from './schema/page-header-container.schema.json';
import pageHeaderContainerPropertyConfig from './property-config/page-header-container.property-config.json';
import { schemaResolver } from './schema/schema-resolver';

export const PAGE_HEADER_CONTAINER_NAME = 'fm-page-header-container';

export const pageHeaderContainerProps = {
  customClass: { type: String, default: '' },
};

export type PageHeaderContainerProps = ExtractPropTypes<typeof pageHeaderContainerProps>;

export const propsResolver = createPropsResolver(
  pageHeaderContainerProps,
  pageHeaderContainerSchema,
  schemaMapper,
  schemaResolver,
  pageHeaderContainerPropertyConfig,
);
