import { ref } from "vue";
import { DraggingResolveContext, UseDesignerRules } from "../../../designer-canvas/src/composition/types";
import { ComponentSchema } from "../../../designer-canvas/src/types";
import { PAGE_HEADER_CONTAINER_NAME } from '../page-header-container.props';

export function useDesignerRules(schema: ComponentSchema, parentSchema?: ComponentSchema): UseDesignerRules {

  const triggerBelongedComponentToMoveWhenMoved = ref(false);

  const triggerBelongedComponentToDeleteWhenDeleted = ref(false);

  const hideNestedPadding = true;

  function canAccepts(draggingContext: DraggingResolveContext): boolean {
    const acceptableControlTypes = [
      'navbar',
      'content-container',
      'html-template',
    ];
    const uniqueControlTypes = [
      'navbar',
    ];
    const { controlType, parentComponentInstance } = draggingContext;
    if (!acceptableControlTypes.includes(controlType)) {
      return false;
    }
    const shouldBeUnique = uniqueControlTypes.includes(controlType);
    if (shouldBeUnique) {
      const parentComponent = parentComponentInstance?.value;
      const contents: any[] = parentComponent?.contents || [];
      const hasSameTypeControl = !!contents.find((content) => content.type === controlType);
      if (hasSameTypeControl) {
        return false;
      }
    }
    return true;
  }

  function checkCanMoveComponent() {
    return false;
  }
  function checkCanDeleteComponent() {
    return true;
  }

  function hideNestedPaddingInDesginerView() {
    return hideNestedPadding;
  }

  function getDesignerClass(): string {
    return PAGE_HEADER_CONTAINER_NAME;
  }

  return {
    canAccepts,
    triggerBelongedComponentToMoveWhenMoved,
    triggerBelongedComponentToDeleteWhenDeleted,
    checkCanMoveComponent,
    checkCanDeleteComponent,
    hideNestedPaddingInDesginerView,
    getDesignerClass,
  };
}
