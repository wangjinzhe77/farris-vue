import { withInstall } from '@components/common';
import FormInstallless from '../form-item/src/form.component';

export * from '../form-item/src/form.props';

const Form = withInstall(FormInstallless);

export { Form };
export default Form;
