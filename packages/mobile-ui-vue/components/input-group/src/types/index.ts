export enum InputTextAlignMap {
  left = 'left',
  center = 'center',
  right = 'right'
}

export enum InputUpdateOnMap {
  change = 'change',
  blur = 'blur'
}

export enum InputTypeMap {
  text = 'text',
  textarea = 'textarea',
  number = 'number',
  digit = 'digit',
  password = 'password',
  email = 'email',
  tel = 'tel',
  url = 'url',
  search = 'search'
}

export type InputTextAlign = `${InputTextAlignMap}`;

export type InputUpdateOn = `${InputUpdateOnMap}`;

export type InputType = `${InputTypeMap}`;
