import { ExtractPropTypes, PropType } from 'vue';
import { InputTextAlign, InputTextAlignMap, InputType, InputTypeMap, InputUpdateOn } from "./types";

export const INPUT_NAME = 'FmInputGroup';

export const inputCommonProps = {
  modelValue: { type: [String, Number], default: '' },

  name: { type: String, default: '' },

  type: { type: String as PropType<InputType>, default: InputTypeMap.text },

  placeholder: { type: String, default: '' },

  enableClear: { type: Boolean, default: false },

  disabled: { type: Boolean, default: false },

  readonly: { type: Boolean, default: false },

  editable: { type: Boolean, default: true },

  showBorder: { type: Boolean, default: false },

  showPadding: { type: Boolean, default: true },

  leftIcon: { type: String, default: undefined },

  rightIcon: { type: String, default: undefined },

  textAlign: { type: String as PropType<InputTextAlign>, default: InputTextAlignMap.left },

  formatter: { type: Function as PropType<(v: string | number) => string | number> },

  maxlength: { type: Number, default: undefined }
};

const inputNumberProps = {
  enableNull: { type: Boolean, default: undefined },

  updateOn: { type: String as PropType<InputUpdateOn>, default: undefined },

  precision: { type: Number, default: 2 }
};

const inputTextAreaProps = {
  autoSize: { type: Boolean, default: undefined },

  rows: { type: Number, default: undefined },

  maxHeight: { type: Number, default: undefined },

  minHeight: { type: Number, default: undefined },

  showWordLimit: { type: Boolean, default: undefined }
};

export const inputProps = {
  ...inputCommonProps,
  ...inputNumberProps,
  ...inputTextAreaProps
};

export type InputCommonProps = ExtractPropTypes<typeof inputCommonProps>;
export type InputProps = ExtractPropTypes<typeof inputProps>;
