/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SetupContext, computed, defineComponent } from 'vue';
import { preventDefault, stopPropagation, useBem } from '@components/common';
import { Icon } from '@components/icon';
import { INPUT_NAME, InputProps, inputProps } from './input-group.props';
import { useInputState, useInputEvent, useInputExpose } from './composition';
import { InputTypeMap } from './types';

export default defineComponent({
  name: INPUT_NAME,
  props: inputProps,
  emits: ['update:modelValue', 'change', 'input','clear', 'blur', 'focus', 'keypress', 'click-left-icon', 'click-right-icon'],
  setup(props: InputProps, context: SetupContext) {
    const { emit, slots } = context;

    const { inputElement } = useInputExpose(context);

    const inputStateComposition = useInputState(props, inputElement);

    const inputEvents = useInputEvent(props, context, inputStateComposition);

    const { innerValue, displayValue, placeholder, inputType, inputMode, showClear, updatable, updateValue } =
      inputStateComposition;

    const { bem } = useBem(INPUT_NAME);

    const controlClass = computed(() => {
      return {
        [bem('control')]: true,
        [bem('control', props.textAlign)]: props.textAlign,
        [bem('control', 'readonly')]: props.readonly
      };
    });

    const controlProps = computed(() => {
      return {
        ref: inputElement,
        class: controlClass.value,
        value: displayValue.value,
        placeholder: placeholder.value,
        disabled: props.disabled,
        readonly: props.readonly || !props.editable,
        maxlength: props.maxlength,
        onChange: stopPropagation,
      };
    });

    const isTextArea = computed(() => props.type === InputTypeMap.textarea);

    const renderControl = () => {
      return isTextArea.value ? (
        <textarea {...controlProps.value} {...inputEvents} rows={props.rows}></textarea>
      ) : (
        <input {...controlProps.value} {...inputEvents} type={inputType.value} inputmode={inputMode.value}></input>
      );
    };

    const onClear = (event: Event) => {
      emit('clear');
      updateValue('');
      inputElement.value?.blur();
      preventDefault(event, true);
    };

    const renderClearIcon = () =>
      showClear.value && (
        <Icon
          class={bem('clear')}
          name="cancel"
          onClick={onClear}
        />
      );

    const onClickLeftIcon = (event: Event) => {
      emit('click-left-icon', event);
    };

    const shoudShowLeftIcon = computed(() => props.leftIcon);
    const renderLeft = () =>{
      return slots.left ? slots.left() : shoudShowLeftIcon.value && (
        <Icon class={bem('left-icon')} name={props.leftIcon} onClick={onClickLeftIcon} />
      );
    };

    const onClickRightIcon = (event: Event) => {
      emit('click-right-icon', event);
    };
    const shoudShowRightIcon = computed(() => props.rightIcon && updatable.value);
    const renderRight = () => {
      return slots.right ? slots.right() : shoudShowRightIcon.value && (
        <Icon class={bem('right-icon')} name={props.rightIcon} onClick={onClickRightIcon} />
      );
    };

    const renderWordLimit = () =>
      props.showWordLimit &&
      props.maxlength && (
        <div class={bem('word-limit')}>
          <span class="word-num">{String(innerValue.value).length}</span>/{props.maxlength}
        </div>
      );

    const bodyClass = computed(() => {
      const className = [bem('body')];
      props.showBorder && className.push(bem('body', 'border'));
      return className;
    });

    const inputClass = computed(() => {
      return {
        [bem()]: true,
        [bem('', 'padding')]: props.showPadding
      };
    });

    return () => (
      <div class={inputClass.value}>
        <div class={bodyClass.value}>
          {renderLeft()}
          { slots.input ? slots.input() : renderControl()}
          {renderClearIcon()}
          {renderRight()}
        </div>
        {renderWordLimit()}
      </div>
    );
  }
});
