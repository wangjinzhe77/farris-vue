import { InputUpdateOn } from "../types";

export type InputMode =
  | 'text'
  | 'decimal'
  | 'tel'
  | 'numeric'
  | 'url'
  | 'email'
  | 'search'
  | 'none'
  | undefined;

export type Instance = {
  updateOn: InputUpdateOn;
  placeholder: string;
  inputType: string;
  inputMode: InputMode;
  formatValue: (value: string | number) => any;
  getModelValue: (value: string | number) => any;
  adjustSize?: () => void;
};

export type InputValueEvent = Event & {
  target:  HTMLInputElement;
};
