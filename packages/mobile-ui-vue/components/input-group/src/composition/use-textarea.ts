/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref, onMounted, unref } from 'vue';
import { addUnit } from '@components/common';
import { InputProps } from '../input-group.props';
import { Instance } from './types';
import { useCommon } from './use-common';
import { InputTypeMap } from '../types';

export function useTextarea(props: InputProps, inputRef: Ref<HTMLElement | undefined>): Instance {
  const { placeholder, updateOn, inputMode, formatValue, getModelValue } = useCommon(props);

  const inputType = InputTypeMap.textarea;

  const adjustSize = () => {
    const input = unref(inputRef);
    if (!(props.type === InputTypeMap.textarea && props.autoSize) || !input) {
      return;
    }

    input.style.height = 'auto';

    let height = input.scrollHeight;

    const maxHeight = props.maxHeight ?? 0;
    const minHeight = props.minHeight ?? 0;

    if (maxHeight) {
      height = Math.min(height, maxHeight);
    } else if (minHeight) {
      height = Math.max(height, minHeight);
    }

    if (height) {
      input.style.height = addUnit(height) || '';
    }
  };

  onMounted(() => {
    adjustSize();
  });

  return {
    updateOn,
    placeholder,
    inputType,
    inputMode,
    adjustSize,
    getModelValue,
    formatValue
  };
}
