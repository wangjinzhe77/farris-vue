/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Ref, computed, ref, watch } from 'vue';
import { isFunction } from '@components/common';
import { useCommon } from './use-common';
import { useNumber } from './use-number';
import { useTextarea } from './use-textarea';
import { InputProps } from '../input-group.props';
import { InputMode, Instance } from './types';

export function useInputState(props: InputProps, inputRef: Ref<HTMLElement | undefined>) {
  let instance: Instance;
  switch (props.type) {
    case 'textarea':
      instance = useTextarea(props, inputRef);
      break;
    case 'number':
    case 'digit':
      instance = useNumber(props);
      break;
    default:
      instance = useCommon(props);
      break;
  }

  // const initType = () => {
  // };
  // initType();

  const { getModelValue } = instance;

  const innerValue = ref<string | number>('');
  const displayValue = ref<string | number>('');

  // 处理自定义格式化函数
  const getDisplayValue = (value: string | number) => {
    return isFunction(props.formatter) ? props.formatter(value) : value;
  };

  const updateInnerValue = (value: string | number) => {
    innerValue.value = instance.formatValue(value);
    instance?.adjustSize && instance.adjustSize();
  };

  const updateDisplayValue = (value: string | number) => {
    displayValue.value = getDisplayValue(value);
  };

  const updateValue = (value: string | number) => {
    updateInnerValue(value);
    updateDisplayValue(innerValue.value);
  };

  updateValue(props.modelValue);

  watch(
    () => props.modelValue,
    (newValue) => {
      if (innerValue.value !== newValue) {
        updateValue(newValue);
      }
    }
  );

  const updateOn = computed(() => {
    return props.updateOn ? props.updateOn : instance.updateOn;
  });

  const updatable = computed(() => {
    return !props.readonly && !props.disabled;
  });

  const placeholder = computed(() => {
    if (!updatable.value) {
      return '';
    }
    if (props.placeholder) {
      return props.placeholder;
    }
    return instance.placeholder;
  });

  const inputType = ref('text');
  const inputMode = ref<InputMode>();

  const inputTypeInit = () => {
    inputType.value = instance.inputType;
    inputMode.value = instance.inputMode;
  };

  inputTypeInit();

  const showClear = ref(false);

  return {
    innerValue,
    displayValue,
    placeholder,
    inputType,
    inputMode,
    updatable,
    updateOn,
    showClear,
    getModelValue,
    updateValue,
    updateInnerValue,
    updateDisplayValue
  };
}

export type InputState = ReturnType<typeof useInputState>;
