import { SetupContext, ref } from "vue";

export const useInputExpose = (context: SetupContext) => {
  const { expose } = context;

  const inputElement = ref<HTMLElement | undefined>();

  const focus = () => {
    inputElement.value?.focus();
  };

  const blur = () => {
    inputElement.value?.blur();
  };

  expose({focus, blur});

  return {
    inputElement
  };
};
