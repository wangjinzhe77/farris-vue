/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { SetupContext } from 'vue';
import { InputProps } from '../input-group.props';
import { InputState } from './use-input-state';

export function useInputEvent(props: InputProps, context: SetupContext, state: InputState) {
  const { emit } = context;

  const {
    innerValue,
    displayValue,
    updateOn,
    updatable,
    showClear,
    getModelValue,
    updateValue,
    updateInnerValue,
    updateDisplayValue
  } = state;

  const toggleShowClear = (value?: string | number) => {
    if (props.enableClear) {
      showClear.value = !!value;
    }
  };

  let changed = false;

  const handleValueChange = (value: string | number) => {
    if (changed) {
      updateInnerValue(value);
      const modelValue = getModelValue(innerValue.value);
      updateValue(modelValue);
      emit('change', modelValue);
      emit('update:modelValue', modelValue);
    } else {
      updateValue(value);
    }
  };

  const handleInput = (value: string | number) => {
    changed = true;
    displayValue.value = value;
    if (updateOn.value === 'change') {
      handleValueChange(value);
    } else {
      updateInnerValue(value);
    }
  };

  let compositioning = false;

  const onInput = (event: Event) => {
    if (compositioning) {
      return;
    }
    emit('input', event);
    const target = event.target as HTMLInputElement;
    handleInput(target.value);
    toggleShowClear(target.value);
  };

  const onCompositionstart = () => {
    compositioning = true;
  };

  const onCompositionend = (e: Event) => {
    compositioning = false;
    onInput(e);
  };

  const onBlur = (event: Event) => {
    emit('blur', event);
    if (!updatable.value) {
      return;
    }
    // 解决点击清除按钮时同时触发blur事件导致清除功能失效问题
    setTimeout(toggleShowClear);

    if (updateOn.value === 'blur') {
      handleValueChange(innerValue.value);
    } else {
      updateDisplayValue(innerValue.value);
    }
  };

  const onFocus = (event: FocusEvent) => {
    emit('focus', event);
    if (!updatable.value) {
      return;
    }
    displayValue.value = innerValue.value;
    toggleShowClear(innerValue.value);
  };

  const onKeypress = (event: KeyboardEvent) => {
    const ENTER_CODE = 'Enter';
    const isSearchEnter = event.code === ENTER_CODE && props.type === 'search';
    if (isSearchEnter) {
      const inputElement = event.target as HTMLElement;
      inputElement?.blur();
    }
    emit('keypress', event);
  };

  return {
    onInput,
    onBlur,
    onFocus,
    onKeypress,
    onCompositionstart,
    onCompositionend
  };
}
