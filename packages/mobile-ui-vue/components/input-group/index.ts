import { withInstall } from '@components/common';
import InputGroupInstallless from './src/input-group.component';

export * from './src/input-group.props';
export * from './src/types';

const InputGroup = withInstall(InputGroupInstallless);

export { InputGroup };
export default InputGroup;
