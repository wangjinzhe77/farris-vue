/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { computed, defineComponent, ref, watch } from 'vue';
import { Icon } from '@components/icon';
import { useBem } from '@components/common';
import { CheckerProps, checkerProps, CHECKER_NAME, CheckerTypeMap, CheckerShapeMap, CheckerRoleMap } from './checker.props';

export default defineComponent({
  name: CHECKER_NAME,
  props: checkerProps,
  emits: ['update:modelValue', 'change'],
  setup(props: CheckerProps, context) {
    const { emit } = context;

    const { bem } = useBem(CHECKER_NAME);

    const innerValue = ref(props.modelValue);
    watch(
      () => props.modelValue,
      (newValue) => {
        innerValue.value = newValue;
      }
    );

    const labelClass = computed(() => {
      return [bem('label')];
    });

    const innerLabel = computed(() => {
      const shoudLimit = props.labelLimit && props.labelLimit > 0 && props.label.length > props.labelLimit;
      return shoudLimit ? props.label.slice(0, props.labelLimit) + '...' : props.label;
    });

    const renderLabel = () => {
      return (
        <div class={labelClass.value}>
          <span>{innerLabel.value}</span>
        </div>
      );
    };

    const renderIcon = () => {
      return (
        <div class={bem('icon')}>
          <Icon name="s-success" />
        </div>
      );
    };

    const CheckerClass = computed(() => {
      return {
        [bem()]: true,
        [bem('', 'readonly')]: props.readonly,
        [bem('', 'disabled')]: props.disabled,
        [bem('', 'checked')]: innerValue.value,
        [bem('', 'round')]: props.shape === CheckerShapeMap.Round,
        [bem('', 'button')]: props.type === CheckerTypeMap.Button
      };
    });

    const handlerClick = () => {
      if (props.readonly || props.disabled) {
        return;
      }
      if(props.role === CheckerRoleMap.Radio && innerValue.value) {
        return;
      }
      innerValue.value = !innerValue.value;
      emit('update:modelValue', innerValue.value);
      emit('change', innerValue.value);
    };

    return () => (
      <div class={CheckerClass.value} onClick={handlerClick}>
        {props.type === CheckerTypeMap.Check && renderIcon()}
        {props.label && renderLabel()}
      </div>
    );
  }
});
