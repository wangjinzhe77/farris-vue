import { ExtractPropTypes, PropType } from 'vue';

export const enum CheckerRoleMap {
  Checkbox = 'checkbox',
  Radio = 'radio'
}

export const enum CheckerShapeMap {
  Square = 'square',
  Round = 'round'
}

export const enum CheckerTypeMap {
  Check = 'default',
  Button = 'button'
}

export type CheckerType = `${CheckerTypeMap}`;

export type CheckerShape = `${CheckerShapeMap}`;

export type CheckerRole = `${CheckerRoleMap}`;

export const CHECKER_NAME = 'fm-checker';

export const checkerProps = {
  modelValue: { type: Boolean, default: undefined },

  label: { type: String, default: '' },
  
  disabled: { type: Boolean, default: false },
  
  readonly: { type: Boolean, default: false },
  
  type: { type: String as PropType<CheckerType>, default: CheckerTypeMap.Check },

  role: { type: String as PropType<CheckerRole>, default: CheckerRoleMap.Checkbox },

  shape: { type: String as PropType<CheckerShape>, default: CheckerShapeMap.Square },

  labelLimit: { type: Number, default: undefined },
};

export type CheckerProps = ExtractPropTypes<typeof checkerProps>;
