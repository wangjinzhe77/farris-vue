import { withInstall } from '@components/common';
import CheckerInstallless from './src/checker.component';

export * from './src/checker.props';

const Checker = withInstall(CheckerInstallless);

export { Checker };
export default Checker;
