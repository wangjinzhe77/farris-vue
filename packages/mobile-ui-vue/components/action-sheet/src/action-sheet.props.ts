/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType, TeleportProps } from 'vue';

export const ACTION_SHEET_NAME = "fm-action-sheet";

/** 动作面板选项 */
export interface ActionSheetItem {

  /** 选项标题 */
  title?: string;

  /** 选项副标题，显示在标题下方 */
  subTitle?: string;

  /** 图标名，图标显示在标题左侧 */
  icon?: string;

  /** 自定义颜色 */
  color?: string;

  /** 是否处于禁用状态 */
  disabled?: boolean;

  /** 自定义类名 */
  className?: string;

  [key: string]: any;
}

export const actionSheetProps = {

  /** 是否显示动作面板，支持语法糖`v-model:show` */
  show: { type: Boolean, default: false },

  /** 动作面板的选项列表 */
  items: { type: Array as PropType<ActionSheetItem[]>, default: [] },

  /** 显示在面板顶部的描述信息 */
  description: { type: String, default: '' },

  /** 文本的水平对齐方式 */
  alignment: { type: String as PropType<'center' | 'left'>, default: 'center' },

  /** 是否显示取消按钮 */
  showCancel: { type: Boolean, default: true },

  /** 取消按钮的文本 */
  cancelText: { type: String, default: '' },

  /** 是否在点击遮罩后关闭 */
  closeOnClickOverlay: { type: Boolean, default: true },

  /** 是否在点击选项后关闭 */
  closeOnClickAction: { type: Boolean, default: false },

  /** 是否显示圆角 */
  round: { type: Boolean, default: true },

  /** z-index层级 */
  zIndex: { type: Number, default: undefined },

  /** 指定`Teleport`组件所挂载的节点 */
  teleport: { type: [String, Object] as PropType<TeleportProps['to']>, default: undefined },

  /** 是否显示遮罩层 */
  overlay: { type: Boolean, default: true },

  /** 自定义遮罩层类名 */
  overlayClass: { type: String, default: undefined },

  /** 自定义遮罩层样式 */
  overlayStyle: { type: String, default: undefined },

  /** 是否锁定背景滚动 */
  lockScroll: { type: Boolean, default: true },

  /** 动画执行时长，单位`s`，为`0`时禁用动画 */
  duration: { type: Number, default: undefined },
};

export type ActionSheetProps = ExtractPropTypes<typeof actionSheetProps>;
