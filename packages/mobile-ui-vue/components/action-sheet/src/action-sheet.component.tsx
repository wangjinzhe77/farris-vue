/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext, computed, CSSProperties, nextTick } from 'vue';
import { useBem } from '@components/common';
import { ACTION_SHEET_NAME, actionSheetProps, ActionSheetProps, ActionSheetItem } from './action-sheet.props';
import Popup from '@components/popup';
import Icon from '@components/icon';

export default defineComponent({
  name: ACTION_SHEET_NAME,

  props: actionSheetProps,

  emits: ['update:show', 'select', 'cancel'],

  setup(props: ActionSheetProps, context: SetupContext) {
    const { bem } = useBem(ACTION_SHEET_NAME);
    const { emit, slots } = context;

    const shouldShowHeader = computed(() => {
      return props.description || slots.header;
    });
    const shouldShowFooter = computed(() => {
      return props.showCancel;
    });

    const popupProps = computed(() => ({
      show: props.show,
      closeOnClickOverlay: props.closeOnClickOverlay,
      zIndex: props.zIndex,
      teleport: props.teleport,
      round: false,
      overlay: props.overlay,
      overlayClass: props.overlayClass,
      overlayStyle: props.overlayStyle,
      lockScroll: props.lockScroll,
      duration: props.duration,
    }));

    const updateShow = (show: boolean) => {
      emit('update:show', show);
    };

    const handleCancel = (event: MouseEvent) => {
      event.stopPropagation();
      updateShow(false);
      emit('cancel');
    };

    const handleItemClick = (event: MouseEvent, item: ActionSheetItem, index: number) => {
      event.stopPropagation();
      if (item.disabled) {
        return;
      }
      if (props.closeOnClickAction) {
        updateShow(false);
      }
      nextTick(() => emit('select', { item, index }));
    };

    const renderFooter = () => {
      return (
        <div class={bem('footer')}>
          <div class={bem('footer-gap')}></div>
          <div role="button" class={bem('cancel')} onClick={handleCancel}>
            {props.cancelText || '取消'}
          </div>
        </div>
      );
    };

    const renderActionItemContent = (item: ActionSheetItem) => {
      const { icon, title, subTitle } = item;
      return (
        <>
          {icon && <Icon class={bem('item-icon')} name={icon} />}
          <div class={bem('item-title')}>{title}</div>
          {subTitle && <div class={bem('item-subtitle')}>{subTitle}</div>}
        </>
      );
    };

    const renderActionItem = (item: ActionSheetItem, index: number) => {
      const { color, disabled, className } = item;
      const itemStyle: CSSProperties = { color };
      const itemClass = {
        [bem('item')]: true,
        [bem('item', 'disabled')]: disabled,
        [bem('item', 'left')]: props.alignment === 'left',
        [className]: !!className,
      };
      const hasItemSlot = !!slots.item;
      return (
        <div
          role="button"
          style={itemStyle}
          class={itemClass}
          onClick={(event) => handleItemClick(event, item, index)}
        >
          {hasItemSlot && slots.item({ item, index })}
          {!hasItemSlot && renderActionItemContent(item)}
        </div>
      );
    };

    const renderActionSheet = () => {
      return (
        <div class={bem('list')}>
          {props.items.map(renderActionItem)}
        </div>
      );
    };

    const renderHeader = () => {
      const hasHeaderSlot = !!slots.header;
      const { description } = props;
      const descriptionClass = {
        [bem('description')]: true,
        [bem('description', 'left')]: props.alignment === 'left',
      };
      return (
        <div class={bem('header')}>
          {hasHeaderSlot && slots.header()}
          {!hasHeaderSlot && <div class={descriptionClass}>{description}</div>}
        </div>
      );
    };

    const popupClass = computed(() => ({
      [bem()]: true,
      [bem('', 'round')]: props.round,
    }));

    return () => (
      <Popup
        class={popupClass.value}
        position="bottom"
        {...popupProps.value}
        {...{
          'onUpdate:show': updateShow
        }}
      >
        {shouldShowHeader.value && renderHeader()}
        <div class={bem('content')}>
          {renderActionSheet()}
          {slots.default?.()}
        </div>
        {shouldShowFooter.value && renderFooter()}
      </Popup>
    );
  }
});
