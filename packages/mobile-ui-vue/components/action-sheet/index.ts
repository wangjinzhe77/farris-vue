import { withInstall } from '@components/common';
import ActionSheetInstallless from './src/action-sheet.component';

export * from './src/action-sheet.props';

const ActionSheet = withInstall(ActionSheetInstallless);

export { ActionSheet };
export default ActionSheet;
