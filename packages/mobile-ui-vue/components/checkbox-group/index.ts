import { withInstall } from '@components/common';
import CheckboxGroupInstallless from './src/checkbox-group.component';

export * from './src/composition';
export * from './src/checkbox-group.props';
const CheckboxGroup = withInstall(CheckboxGroupInstallless);

export { CheckboxGroup };
export default CheckboxGroup;
