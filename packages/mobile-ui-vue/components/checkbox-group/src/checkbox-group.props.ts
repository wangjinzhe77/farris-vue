import { ExtractPropTypes, PropType } from 'vue';
import { CheckerShape, CheckerShapeMap, CheckerType, CheckerTypeMap } from '@components/checker';

export type CheckboxItem = {
  disabled: boolean;
  readonly: boolean;
  [key: string]: any;
};

export const enum CheckboxDirectionMap {
  Vertical = 'vertical',
  Horizontal = 'horizontal'
}

export type CheckboxDirection = `${CheckboxDirectionMap}`;

export const CHECKBOX_GROUP_NAME = 'fm-checkbox-group';

export const checkboxGroupProps = {
  modelValue: { type: [String, Array<string>], default: '' },

  disabled: { type: Boolean, default: false },

  readonly: { type: Boolean, default: false },

  shape: { type: String as PropType<CheckerShape>, default: CheckerShapeMap.Square },

  type: { type: String as PropType<CheckerType>, default: CheckerTypeMap.Check },

  direction: { type: String as PropType<CheckboxDirection>, default: CheckboxDirectionMap.Vertical },

  options: { type: Array as PropType<CheckboxItem[]>, default: undefined },

  valueField: { type: String, default: 'value' },

  textField: { type: String, default: 'text' },

  showDisabledItem: { type: Boolean, default: true },
};

export type CheckboxGroupProps = ExtractPropTypes<typeof checkboxGroupProps>;

export type CheckboxGroupContext = {
  props: CheckboxGroupProps;
  getChecked: (name: any) => boolean;
  updateChecked: (value: string | number, checked: boolean) => void;
};
