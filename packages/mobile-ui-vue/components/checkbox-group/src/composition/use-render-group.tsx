/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Component, computed } from 'vue';
import { CheckboxGroupProps, CheckboxItem } from '../checkbox-group.props';

export function useGroupItems(props: Omit<CheckboxGroupProps, 'modelValue'>, component: Component) {
  const groupItems = computed(() => {
    if (!props.options) {
      return [];
    }
    if (props.showDisabledItem) {
      return props.options;
    }
    return props.options.filter((item) => !item.disabled);
  });

  const renderGroupItem = (item: CheckboxItem) => {
    const { textField, valueField } = props;
    const { disabled, readonly } = item;
    const options = {
      label: item[textField],
      name: item[valueField],
      disabled,
      readonly
    };
    return <component {...options}></component>;
  };
  const renderGroupItems = () => {
    return groupItems.value.map((item) => {
      return renderGroupItem(item);
    });
  };
  return {
    renderGroupItems
  };
}
