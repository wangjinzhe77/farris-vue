/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SetupContext, computed, defineComponent, ref, toRaw, watch } from 'vue';
import { isArray, isString, useBem, useLink } from '@components/common';
import Checkbox from '@components/checkbox';
import {
  CHECKBOX_GROUP_NAME,
  CheckboxGroupContext,
  CheckboxGroupProps,
  checkboxGroupProps
} from './checkbox-group.props';
import { useGroupItems } from './composition';

export default defineComponent({
  name: CHECKBOX_GROUP_NAME,
  props: checkboxGroupProps,
  setup(props: CheckboxGroupProps, context: SetupContext) {
    const { emit, slots } = context;

    const checkeds = ref<(string | number)[]>([]);

    const updateChecked = (value: string | number, checked: boolean) => {
      if (checked) {
        checkeds.value.push(value);
      } else {
        checkeds.value = checkeds.value.filter((item) => item !== value);
      }
      const modelValue = isArray(props.modelValue)
        ? toRaw(checkeds.value)
        : checkeds.value.join(',');
      emit('update:modelValue', modelValue);
    };

    const getChecked = (name: any) => {
      return !!checkeds.value.find((checked) => checked === name);
    };

    const { setParent } = useLink(CHECKBOX_GROUP_NAME);
    setParent<CheckboxGroupContext>({ props, getChecked, updateChecked });

    const updateCheckeds = (value: string | string[]) => {
      checkeds.value = isArray(value) ? value : isString(value) ? value.split(',') : [];
    };
    updateCheckeds(props.modelValue);
    watch(
      () => props.modelValue,
      (newValue) => {
        updateCheckeds(newValue);
      }
    );

    const { renderGroupItems } = useGroupItems(props, Checkbox);

    const { bem } = useBem(CHECKBOX_GROUP_NAME);
    const checkboxGroupClass = computed(() => {
      return [bem(), bem('', props.direction)];
    });

    return () => (
      <div class={checkboxGroupClass.value}>
        {slots?.default ? slots.default() : renderGroupItems()}
      </div>
    );
  }
});
