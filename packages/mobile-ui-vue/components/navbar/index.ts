import { withInstall } from '@components/common';
import NavbarInstallless from './src/navbar.component';

const Navbar = withInstall(NavbarInstallless);

export { Navbar };
export default Navbar;
