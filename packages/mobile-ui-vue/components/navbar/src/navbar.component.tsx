/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SetupContext, defineComponent } from 'vue';
import { NavbarProps, navbarProps } from './navbar.props';

export default defineComponent({
  name: 'fm-navbar',
  props: navbarProps,
  emits: ['click-left', 'click-right'],
  setup(props: NavbarProps, context: SetupContext) {
    const { emit, slots } = context;

    const handlerClickLeft = (event) => {
      emit('click-left', event);
    };

    const handlerClickRight = (event) => {
      emit('click-right', event);
    };

    const navbarClass = {
      'fm-navbar': true,
      'fm-navbar-fixed': props.fixed,
      'fm-navbar-border-bottom': props.border
    };

    return () => (
      <>
        <div class={navbarClass}>
          {(props.leftArrow || props.leftText) && (
            <div class="fm-navbar-left" onClick={handlerClickLeft}>
              {slots.left ? (
                slots.left()
              ) : (
                <>
                  {props.leftArrow && (
                    <span class="fm-navbar-left-arrow fm-icon fm-icon-s-arrow-left"></span>
                  )}
                  {props.leftText && <span class="fm-navbar-text">{props.leftText}</span>}
                </>
              )}
            </div>
          )}
          <div class="fm-navbar-title">{slots.title ? slots.title() : props.title}</div>
          <div class="fm-navbar-right" onClick={handlerClickRight}>
            {slots.right ? slots.right() : <span class="fm-navbar-text">{props.rightText}</span>}
          </div>
        </div>
      </>
    );
  }
});
