# fm-navbar
## API

### Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| title | 标题 | string | `''` |
| left-text | 左侧文案 | string | `''` |
| right-text | 右侧文案 | string | `''` |
| left-arrow | 是否显示左侧箭头 | boolean | `false` |
| border | 是否显示下边框 | boolean | `true` |
| fixed | 是否固定在顶部 | boolean | `false` |

### Slots

| 名称  | 说明               |
| ----- | ------------------ |
| title | 自定义标题         |
| left  | 自定义左侧区域内容 |
| right | 自定义右侧区域内容 |

### Events

| 事件名      | 说明               | 回调参数 |
| ----------- | ------------------ | -------- |
| clickLeft  | 点击左侧按钮时触发 | -        |
| clickRight | 点击右侧按钮时触发 | -        |