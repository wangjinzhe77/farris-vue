import { withInstall } from '@components/common';
import RadioInstallless from './src/radio.component';

export * from './src/radio.props';

const Radio = withInstall(RadioInstallless);

export { Radio };
export default Radio;
