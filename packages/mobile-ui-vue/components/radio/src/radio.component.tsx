/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { defineComponent } from 'vue';
import { Checker, CheckerRoleMap, CheckerShapeMap } from '@components/checker';
import { useBem, useLink } from '@components/common';
import { RADIO_GROUP_NAME, RadioGroupContext } from '@components/radio-group';
import { RADIO_NAME, RadioProps, radioProps } from './radio.props';

export default defineComponent({
  name: RADIO_NAME,
  props: radioProps,
  setup(props: RadioProps, context) {
    const { emit } = context;

    const { bem } = useBem(RADIO_NAME);

    const { getParent } = useLink(RADIO_GROUP_NAME);
    const radioGroup = getParent<RadioGroupContext>();

    const handlerChange = (value: boolean) => {
      radioGroup?.updateChecked(props.name);
      emit('change', value);
    };
    const handlerUpdateModelValue = (value: boolean) => {
      emit('update:modelValue', value);
    };

    const renderCheckbox = () => {
      const modelValue = radioGroup?.getChecked
        ? radioGroup.getChecked(props.name)
        : props.modelValue;
      return (
        <Checker
          label={props.label}
          modelValue={modelValue}
          role={CheckerRoleMap.Radio}
          shape={CheckerShapeMap.Round}
          type={radioGroup?.props.type || props.type}
          readonly={props.readonly || radioGroup?.props.readonly}
          disabled={props.disabled || radioGroup?.props.disabled}
          onChange={handlerChange}
          onUpdate:modelValue={handlerUpdateModelValue}
        ></Checker>
      );
    };

    return () => <div class={bem()}>{renderCheckbox()}</div>;
  }
});
