import { ExtractPropTypes, PropType } from 'vue';
import { CheckerRole, CheckerRoleMap } from '@components/checker';
import { checkboxProps } from '@components/checkbox';

export const RADIO_NAME = 'fm-radio';

export const radioProps = {
  ...checkboxProps,
};

export type RadioProps = ExtractPropTypes<typeof radioProps>;
