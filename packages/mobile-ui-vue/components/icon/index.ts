import { withInstall } from '@components/common';
import IconInstallless from './src/icon.component';

const Icon = withInstall(IconInstallless);

export { Icon };
export default Icon;
