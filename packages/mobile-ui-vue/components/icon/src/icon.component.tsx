/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { defineComponent, computed, CSSProperties, ComputedRef, SetupContext } from 'vue';
import { addUnit } from '@components/common';
import { IconProps, iconProps } from './icon.props';

export default defineComponent({
  name: 'FmIcon',
  props: iconProps,
  emits: ['click'],
  setup(props: IconProps, context: SetupContext<string[]>) {
    const { emit } = context;
    const iconClass: ComputedRef<string[]> = computed(() => {
      const { classPrefix, name } = props;
      return [classPrefix, name ? `${classPrefix}-${name}` : ''];
    });

    const iconStyle: ComputedRef<CSSProperties> = computed(() => {
      return { color: props.color, 'font-size': props.size ? addUnit(props.size) : '' };
    });

    return () => (
      <span
        class={iconClass.value}
        style={iconStyle.value}
        onClick={(event) => emit('click', event)}></span>
    );
  }
});
