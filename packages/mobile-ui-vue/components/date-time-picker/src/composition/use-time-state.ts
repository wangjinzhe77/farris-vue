import { computed, ref } from "vue";
import { TimePickerType, TimePickerTypeMap } from "@components/time-picker";
import { DateTimePickerProps } from "../date-time-picker.props";
import { formatDate, isEqualDate } from "@components/common";
import { DateState } from "./use-date-state";

export const useTimeState = (props: DateTimePickerProps, datePropsComposition: DateState) => {
  const { dateValue } = datePropsComposition;

  const timeType = computed(() => {
    return (props.type.split(" ")[1] as TimePickerType) || TimePickerTypeMap.Time;
  });
  const timeFormat = computed(() => {
    return props.format.split(" ")[1] || 'HH:mm:ss';
  });

  const maxTime = computed(() => {
    const date = new Date(dateValue.value);
    if(props.maxDate && isEqualDate(props.maxDate, date)) {
      return formatDate(props.maxDate, 'HH:mm:ss');
    }
    return  '23:59:59';
  });

  const minTime = computed(() => {
    const date = new Date(dateValue.value);
    if(props.minDate && isEqualDate(props.minDate, date)) {
      return formatDate(props.minDate, 'HH:mm:ss');
    }
    return '00:00:00';
  });

  const timeValue = ref();

  return {
    timeValue,
    timeType,
    timeFormat,
    maxTime,
    minTime
  };
};

export type TimeState = ReturnType<typeof useTimeState>;
