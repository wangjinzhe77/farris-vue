import { reactive, watch } from "vue";
import { DateTimePickerProps } from "../date-time-picker.props";
import { compareDate, formatDate, isDate, parseDate } from "@components/common";
import { DateState } from "./use-date-state";
import { TimeState } from "./use-time-state";

export const useDateTimeValue = (props: DateTimePickerProps, dateState: DateState, timeState: TimeState) => {
  const { dateValue, dateFormat } = dateState;
  const { timeValue, timeFormat } = timeState;

  const setInnerValue = (value: string | Date)=>{
    value = value || new Date();
    value = isDate(value) ? value : parseDate(value, props.format);
    if(props.minDate && compareDate(value, props.minDate) < 0) {
      value = props.minDate;
    }
    if(props.maxDate && compareDate(value, props.maxDate) > 0) {
      value = props.maxDate;
    }
    dateValue.value = formatDate(value, dateFormat.value);
    timeValue.value = formatDate(value, timeFormat.value);
  };
  setInnerValue(props.modelValue);
  watch(()=> props.modelValue, setInnerValue);

  const setDateValue = (value: Date)=>{
    dateValue.value = formatDate(value, dateFormat.value);
  };

  const setTimeValue = (value: string)=>{
    timeValue.value = value;
  };

  const getTextValue = () => {
    return `${dateValue.value} ${timeValue.value}`;
  };

  const getDateValue = () => {
    return parseDate(getTextValue(), props.format);
  };

  return {
    getDateValue,
    getTextValue,
    setDateValue,
    setTimeValue
  };
};
