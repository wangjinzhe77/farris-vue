import { computed, ref } from "vue";
import { DatePickerType, DatePickerTypeMap } from "@components/date-picker";
import { DateTimePickerProps } from "../date-time-picker.props";

export const useDateState = (props: DateTimePickerProps) => {
  const dateType = computed(() => {
    return (props.type.split(" ")[0] as DatePickerType) || DatePickerTypeMap.Date;
  });
  const dateFormat = computed(() => {
    return props.format.split(" ")[0] || 'YYYY-MM-DD';
  });

  const dateValue = ref();

  return {
    dateValue,
    dateType,
    dateFormat
  };
};

export type DateState = ReturnType<typeof useDateState>;
