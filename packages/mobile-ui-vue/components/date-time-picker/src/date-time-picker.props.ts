import { ExtractPropTypes, PropType } from 'vue';
import { DatePickerTypeMap } from '@components/date-picker';
import { TimePickerTypeMap } from '@components/time-picker';

export const DATE_TIME_PICKER_NAME = 'FmDateTimePicker';

export const enum DateTimeTypeMap {
  DateTime = `${DatePickerTypeMap.Date} ${TimePickerTypeMap.Time}`,
  DateHour = `${DatePickerTypeMap.Date} ${TimePickerTypeMap.Hours}`,
  DateMinute = `${DatePickerTypeMap.Date} ${TimePickerTypeMap.HoursMinutes}`
}

type DateTimeType = `${DateTimeTypeMap}`;

export const dateTimePickerProps = {
  title: { type: String, default: '' },

  type: { type: String as PropType<DateTimeType>, default: DateTimeTypeMap.DateTime },

  modelValue: { type: [String, Date], default: '' },

  format: { type: String, default: 'YYYY-MM-DD HH:mm:ss' },

  maxDate: { type: Date, default: undefined },

  minDate: { type: Date, default: undefined },

  optionFormatter: { type: Function as PropType<(type: string, value: string) => string>, default: undefined },

  visiableOptionCount: { type: Number, default: 5 },

  showToolbar: { type: Boolean, default: true }
};

export type DateTimePickerProps = ExtractPropTypes<typeof dateTimePickerProps>;
