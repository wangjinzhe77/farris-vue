/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { defineComponent, reactive, ref, SetupContext } from 'vue';
import { DatePicker } from '@components/date-picker';
import { TimePicker } from '@components/time-picker';
import PickerGroup from '@/components/picker-group';
import { useDateState, useDateTimeValue, useTimeState } from './composition';
import {
  dateTimePickerProps,
  DateTimePickerProps,
  DATE_TIME_PICKER_NAME
} from './date-time-picker.props';

export default defineComponent({
  name: DATE_TIME_PICKER_NAME,
  props: dateTimePickerProps,
  emits: ['change', 'confirm', 'update:modelValue'],
  setup(props: DateTimePickerProps, context) {
    const { emit, expose } = context;

    const dateStateComposition = useDateState(props);
    const { dateValue, dateType, dateFormat } = dateStateComposition;
    const timeStateComposition = useTimeState(props, dateStateComposition);
    const { timeValue, timeType, timeFormat, maxTime, minTime } = timeStateComposition;

    const { setDateValue, setTimeValue, getDateValue, getTextValue } = useDateTimeValue(props, dateStateComposition, timeStateComposition);

    expose({ getTextValue });

    const handleDateChange = (value: Date) => {
      setDateValue(value);
      emit('change', getDateValue());
    };
    const handleTimeChange = (value: string) => {
      setTimeValue(value);
      emit('change', getDateValue());
    };

    const handleConfirm = () => {
      const value = getDateValue();
      emit('confirm', value);
      emit('update:modelValue', value);
    };

    return () => (
      <PickerGroup title="选择日期时间" onConfirm={handleConfirm}>
        <DatePicker
          title="选择日期"
          modelValue={dateValue.value}
          type={dateType.value}
          format={dateFormat.value}
          maxDate={props.maxDate}
          minDate={props.minDate}
          optionFormatter={props.optionFormatter}
          visiableOptionCount={props.visiableOptionCount}
          showToolbar={false}
          onChange={handleDateChange}
        ></DatePicker>
        <TimePicker
          title="选择时间"
          modelValue={timeValue.value}
          type={timeType.value}
          format={timeFormat.value}
          maxTime={maxTime.value}
          minTime={minTime.value}
          optionFormatter={props.optionFormatter}
          visiableOptionCount={props.visiableOptionCount}
          showToolbar={false}
          onChange={handleTimeChange}
        ></TimePicker>
      </PickerGroup>
    );
  }
});
