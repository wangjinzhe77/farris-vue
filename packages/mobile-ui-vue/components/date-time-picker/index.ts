import { withInstall } from '@components/common';
import DateTimePickerInstallless from './src/date-time-picker.component';

export * from './src/date-time-picker.props';

const DateTimePicker = withInstall(DateTimePickerInstallless);

export { DateTimePicker };
export default DateTimePicker;
