/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { App } from 'vue';
import Button from './button';
import ButtonEdit from './button-edit';
import Cell from './cell';
import Icon from './icon';
import InputGroup from './input-group';
import Navbar from './navbar';
import Switch from './switch';
import Toast from './toast';
import Notify from './notify';
import Dialog from './dialog';
import Checker from './checker';
import CheckboxGroup from './checkbox-group';
import Checkbox from './checkbox';
import RadioGroup from './radio-group';
import Radio from './radio';
import Rate from './rate';
import Form from './form';
import FormItem from './form-item';
import Popup from './popup';
import Overlay from './overlay';
import Picker from './picker';
import PickerGroup from './picker-group';
import PickerInput from './picker-input';
import DatePicker from './date-picker';
import DatePickerInput from './date-picker-input';
import DateTimePicker from "./date-time-picker";
import DateTimePickerInput from "./date-time-picker-input";
import TimePicker from './time-picker';
import TimePickerInput from './time-picker-input';
import Loading from './loading';
import PullRefresh from './pull-refresh';
import List from './list';
import { ListView } from './list-view';
import TabBar from "./tab-bar";
import SwipeCell from './swipe-cell';
import ActionSheet from './action-sheet';
import Tab from './tab';
import Tabs from './tabs';
import Tag from './tag';
import * as Utils from './common/src/utils';
// import { PageContainer } from './page-container';
// import { PageBodyContainer } from './page-body-container';
// import { PageHeaderContainer } from './page-header-container';
// import { PageFooterContainer } from './page-footer-container';

const components = [
  Button,
  ButtonEdit,
  Cell,
  Checkbox,
  CheckboxGroup,
  Checker,
  DatePicker,
  DatePickerInput,
  DateTimePicker,
  DateTimePickerInput,
  Icon,
  InputGroup,
  Navbar,
  Switch,
  Toast,
  Notify,
  Dialog,
  Radio,
  RadioGroup,
  Rate,
  Form,
  FormItem,
  Popup,
  Picker,
  PickerGroup,
  PickerInput,
  TimePicker,
  TimePickerInput,
  TabBar,
  Loading,
  PullRefresh,
  List,
  ListView,
  SwipeCell,
  ActionSheet,
  Tabs,
  Tab,
  Tag,
  // PageContainer,
  // PageBodyContainer,
  // PageHeaderContainer,
  // PageFooterContainer,
  Overlay
];

const install = (app: App): void => {
  components.forEach((component) => {
    app.use(component);
  });
};
export {
  Button,
  ButtonEdit,
  Cell,
  Checker,
  Checkbox,
  CheckboxGroup,
  DatePicker,
  DatePickerInput,
  DateTimePicker,
  DateTimePickerInput,
  Icon,
  InputGroup,
  Navbar,
  Switch,
  Toast,
  Notify,
  Dialog,
  Radio,
  RadioGroup,
  Rate,
  Form,
  FormItem,
  Popup,
  Picker,
  PickerGroup,
  PickerInput,
  TimePicker,
  TimePickerInput,
  TabBar,
  Loading,
  PullRefresh,
  List,
  ListView,
  SwipeCell,
  ActionSheet,
  Tabs,
  Tab,
  Tag,
  Overlay,
  // PageContainer,
  // PageBodyContainer,
  // PageHeaderContainer,
  // PageFooterContainer,
  Utils
};

export default {
  install
};
