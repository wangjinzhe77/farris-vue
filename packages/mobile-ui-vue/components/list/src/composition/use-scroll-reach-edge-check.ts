import { nextTick, onMounted, Ref, ref, isRef, computed } from 'vue';
import type { ListDirection, UseScrollReachEdgeCheckOptions, UseScrollReachEdgeCheck } from './types';
import { useScrollParent, useEventListener, useResizeObserver } from '@components/common';
import { inBrowser, isNumber } from '@components/common';

const DEFAULT_OFFSET = 30;

export const useScrollReachEdgeCheck = (
  options: UseScrollReachEdgeCheckOptions,
  listener?: () => void,
): UseScrollReachEdgeCheck => {
  const { containerRef, direction, offset } = options;
  const isReachEdge = ref(false);
  const scrollerRef: Ref<Element | Window | undefined> = useScrollParent(containerRef);

  const directionRef = computed<ListDirection>(() => {
    const value = isRef(direction) ? direction.value : direction;
    return value || 'down';
  });

  const offsetRef = computed<number>(() => {
    const value = isRef(offset) ? +offset.value : +offset;
    return value >= 0 ? value : DEFAULT_OFFSET;
  });

  const check = (e?: Event) => {
    nextTick(() => {
      const target = (e && e.target || scrollerRef.value) as HTMLElement;
      if (!target) {
        return;
      }
      const windowHeight = inBrowser ? window.innerHeight : 0;

      const scrollHeight = isNumber(target.scrollHeight)
        ? target.scrollHeight
        : Math.max(document.documentElement.scrollHeight, document.body.scrollHeight);

      const offsetHeight = isNumber(target.offsetHeight)
        ? target.offsetHeight
        : windowHeight;

      const topDistance = isNumber(target.scrollTop)
        ? target.scrollTop
        : (document.documentElement.scrollTop || document.body.scrollTop);

      // 如果容器没有高度，则不进行处理
      if (offsetHeight <= 0) {
        return;
      }

      const bottomDistance = scrollHeight - topDistance - offsetHeight;

      isReachEdge.value = directionRef.value === 'up' ? (topDistance <= offsetRef.value) : (bottomDistance <= offsetRef.value);
      isReachEdge.value && listener && listener();

      if (options.onScroll) {
        options.onScroll({ topDistance, bottomDistance });
      }
    });
  };

  const resizeObservableElRef = computed<HTMLElement>(() => {
    const scroller = scrollerRef.value;
    if (!scroller || scroller instanceof Window || scroller.nodeType !== 1) {
      return document.body;
    }
    return scrollerRef.value as HTMLElement;
  });

  useResizeObserver(resizeObservableElRef, () => {
    check();
  });

  onMounted(() => {
    useEventListener('scroll', check, {
      target: scrollerRef,
      passive: true,
      immediate: true,
    });
  });

  return {
    isReachEdge,
    check: () => check(),
  };
};
