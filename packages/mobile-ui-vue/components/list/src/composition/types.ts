import type { Ref } from 'vue';

/**
 * 列表方向
 * @description 如果为`down`，则滑动到容器底部时触发加载事件
 */
export type ListDirection = "up" | "down";

type MaybeRef<T> = T | Ref<T>;

export interface UseScrollReachEdgeCheckOptions {

  containerRef: Ref<Element | undefined>;

  direction?: MaybeRef<ListDirection>;

  offset?: MaybeRef<number | string>;

  onScroll?: (payload: { topDistance: number; bottomDistance: number }) => void;
}

export interface UseScrollReachEdgeCheck {

  isReachEdge: Ref<boolean>;

  check: () => void;
}
