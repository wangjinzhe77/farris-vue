/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes, PropType } from 'vue';
import { ListDirection } from './composition/types';

export const LIST_NAME = 'fm-list';

export const listProps = {

  /** 是否处于加载状态，支持语法糖`v-model:loading`，此状态下将在列表底部显示加载中提示文本 */
  loading: { type: Boolean, default: false },

  /** 是否处于加载失败状态，支持语法糖`v-model:error`，此状态下将在列表底部显示加载失败提示文本，用户点击提示将触发加载事件 */
  error: { type: Boolean, default: false },

  /** 是否加载完成，加载完成后将在列表下方显示加载完成提示文本，且不会再触发加载事件 */
  finished: { type: Boolean, default: false },

  /** 是否自动触发加载事件，默认为`true`，当用户滑动到列表底部时，自动触发加载事件 */
  autoLoadMore: { type: Boolean, default: true },

  /** 滚动条与底部的距离小于等于`offset`（且`autoLoadMore=true`）时自动触发加载事件 */
  offset: { type: [Number, String], default: 30 },

  /** 是否在初始化时立即检查自动触发加载事件的条件，满足则自动触发加载事件 */
  immediateCheck: { type: Boolean, default: true },

  /** 是否禁用，禁用后不再触发加载事件 */
  disabled: { type: Boolean, default: false },

  /** 默认情况下滚动到底部触发加载，如果将本属性设为`up`则滚动到顶部时触发加载 */
  direction: { type: String as PropType<ListDirection>, default: 'down' },

  /** 加载更多提示文本 */
  loadMoreText: { type: String, default: undefined },

  /** 加载中提示文本 */
  loadingText: { type: String, default: undefined },

  /** 加载完成提示文本 */
  finishedText: { type: String, default: undefined },

  /** 加载失败提示文本 */
  errorText: { type: String, default: undefined },

  /** 是否显示加载完成提示文本，如果为`false`则忽略`finishedText`属性和`finished`插槽 */
  showFinishedTip: { type: Boolean, default: true },

} as Record<string, any>;

export type ListProps = ExtractPropTypes<typeof listProps>;
