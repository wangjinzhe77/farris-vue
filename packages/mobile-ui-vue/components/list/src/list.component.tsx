/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext, ref, watch, onMounted, toRefs, computed } from 'vue';
import { LIST_NAME, listProps, ListProps } from './list.props';
import { useScrollReachEdgeCheck } from './composition/use-scroll-reach-edge-check';
import { useBem } from '@components/common';
import Loading from '@components/loading';

export default defineComponent({
  name: LIST_NAME,

  props: listProps,

  emit: ['load', 'update:error', 'update:loading', 'scroll'],

  setup(props: ListProps, context: SetupContext) {
    const { bem } = useBem(LIST_NAME);
    const { emit, slots, expose } = context;
    const root = ref<HTMLElement>();
    const innerLoading = ref(props.loading);

    const loadMore = () => {
      innerLoading.value = true;
      emit('update:loading', true);
      emit('load');
    };

    const canAutoLoadMore = computed(() => {
      return props.autoLoadMore
        && !innerLoading.value
        && !props.finished
        && !props.disabled
        && !props.error;
    });

    const onReachEdge = () => {
      if (canAutoLoadMore.value) {
        loadMore();
      }
    };

    const {
      direction: listDirection,
      offset: triggerLoadOffset,
    } = toRefs(props);

    const { check } = useScrollReachEdgeCheck(
      {
        containerRef: root,
        direction: listDirection.value,
        offset: triggerLoadOffset.value,
        onScroll: (payload) => { emit('scroll', payload); },
      },
      onReachEdge,
    );

    watch(() => [props.loading, props.finished, props.error], () => {
      innerLoading.value = props.loading;
      if (props.autoLoadMore) {
        check();
      }
    });

    const needImmediateCheck = computed(() => props.autoLoadMore && props.immediateCheck);

    onMounted(() => {
      if (needImmediateCheck.value) {
        check();
      }
    });

    const onLoadMore = () => {
      if (props.disabled) {
        return;
      }
      if (props.error) {
        emit('update:error', false);
      }
      loadMore();
    };

    const renderHeader = () => {
      const header = slots.header?.();
      if (header) {
        return (
          <div class={bem('header')}>{header}</div>
        );
      }
    };

    const renderFooter = () => {
      const footer = slots.footer?.();
      if (footer) {
        return (
          <div class={bem('footer')}>{footer}</div>
        );
      }
    };

    const renderFinishedInfo = () => {
      if (!props.showFinishedTip) {
        return;
      }
      const text = slots.finished ? slots.finished() : props.finishedText;
      if (text) {
        return <div class={bem('finished-info')}>{text}</div>;
      }
    };

    const renderLoading = () => {
      const loading = slots.loading ? slots.loading() : (
        <div class={bem('loading-container')}>
          <Loading size="16px" text={props.loadingText || '加载中...'} />
        </div>
      );
      return (
        <div class={bem('loading')}>{loading}</div>
      );
    };

    const renderErrorInfo = () => {
      const text = slots.error ? slots.error() : props.errorText;
      if (text) {
        return (
          <div role="button" class={bem('error')} tabindex={0} onClick={onLoadMore}>{text}</div>
        );
      }
    };

    const renderLoadMoreButton = () => {
      const text = slots.loadMore ? slots.loadMore() : props.loadMoreText || '加载更多';
      return (
        <div role="button" class={bem('load-more')} tabindex={0} onClick={onLoadMore}>{text}</div>
      );
    };

    const shouldShowFinishedInfo = computed(() => props.finished);
    const shouldShowLoading = computed(() => {
      return innerLoading.value && !props.disabled && !shouldShowFinishedInfo.value;
    });
    const shouldShowErrorInfo = computed(() => {
      return props.error && !shouldShowFinishedInfo.value && !shouldShowLoading.value;
    });
    const shouldShowLoadMoreButton = computed(() => {
      return !props.autoLoadMore && !shouldShowFinishedInfo.value && !shouldShowLoading.value && !shouldShowErrorInfo.value;
    });

    const renderStatusBar = () => {
      return (
        <>
          {shouldShowFinishedInfo.value && renderFinishedInfo()}
          {shouldShowLoading.value && renderLoading()}
          {shouldShowErrorInfo.value && renderErrorInfo()}
          {shouldShowLoadMoreButton.value && renderLoadMoreButton()}
        </>
      );
    };

    expose({ check });

    return () => {
      const content = slots.default?.();
      const shouldShowContentAboveStatus = listDirection.value === 'down';
      const shouldShowContentBelowStatus = listDirection.value === 'up';

      return (
        <div ref={root} class={bem()}>
          {renderHeader()}
          {shouldShowContentAboveStatus && content}
          {renderStatusBar()}
          {shouldShowContentBelowStatus && content}
          {renderFooter()}
        </div>
      );
    };
  }

});
