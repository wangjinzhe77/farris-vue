import { withInstall } from '@components/common';
import ListInstallless from './src/list.component';

export * from './src/list.props';

const List = withInstall(ListInstallless);

export { List };
export default List;
