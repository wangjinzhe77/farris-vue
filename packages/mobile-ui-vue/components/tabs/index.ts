import { withInstall } from '@components/common';
import TabsInstallless from './src/tabs.component';

export * from './src/tabs.props';
export * from './src/components/tab.props';

const Tabs = withInstall(TabsInstallless);

export { Tabs };
export default Tabs;
