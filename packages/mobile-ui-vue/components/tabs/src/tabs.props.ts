/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';

export const TABS_NAME = 'fm-tabs';

export const tabsProps = {

  /** 当前选中标签页的标识，支持语法糖`v-model` */
  modelValue: { type: [String, Number], default: undefined },

  /** 是否在标签页首次被选中后才渲染其内容 */
  lazyRender: { type: Boolean, default: true },

  /** 是否启用切换动画，如果启用则`lazyRender`属性失效 */
  animation: { type: Boolean, default: false },

  /** 切换动画的执行时长，单位`ms` */
  animationDuration: { type: [Number, String], default: 300 },

  /** 是否启用手势左右滑动切换 */
  swipeable: { type: Boolean, default: false },

  /** 是否启用自适应高度 */
  fitHeight: { type: Boolean, default: false },
};

export type TabsProps = ExtractPropTypes<typeof tabsProps>;
