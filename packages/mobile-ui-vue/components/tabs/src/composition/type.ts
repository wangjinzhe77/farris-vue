import type { DeepReadonly, Ref, InjectionKey } from 'vue';
import type { TabsProps } from '../tabs.props';
import type { TabProps } from '../components/tab.props';

export interface TabsContext {

  /** 当前选中标签的标识 */
  currentName: DeepReadonly<Ref<string | number>>;

  /** 标签页属性 */
  props: DeepReadonly<TabsProps>;
}

export const TABS_CONTEXT_KEY: InjectionKey<TabsContext> = Symbol('TabsContext');

export interface UseTabs {

  tabItems: DeepReadonly<Ref<TabProps[]>>;

  currentName: DeepReadonly<Ref<string | number>>;

  currentIndex: DeepReadonly<Ref<number>>;

  updateCurrentName(newValue: string | number): void;

  updateCurrentIndex(newIndex: number): void;

  tabCount: DeepReadonly<Ref<number>>;

  isAtFirstTab: DeepReadonly<Ref<boolean>>;

  isAtLastTab: DeepReadonly<Ref<boolean>>;

  moveToNextTab(): void;

  moveToPreviousTab(): void;
}

export interface UseSwipe {

  swipeState: DeepReadonly<{
    offset: number;
    moving: boolean;
  }>;
}
