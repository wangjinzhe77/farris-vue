import { SetupContext, computed, ref, watch, isVNode, VNode, Fragment } from 'vue';
import { TabsProps } from '../tabs.props';
import Tab from '../components/tab.component';
import { TabProps } from '../components/tab.props';
import { isDef } from '@components/common';
import { UseTabs } from './type';

export function useTabs(props: TabsProps, context: SetupContext): UseTabs {

  const { slots, emit } = context;

  function isTabNode(node: any): boolean {
    return !!node && isVNode(node) && node.type === Tab;
  };

  function filterTabNodes(nodes: VNode[]): VNode[] {
    const tabNodes: VNode[] = [];
    nodes.forEach((node) => {
      if (node.type !== Fragment) {
        tabNodes.push(node);
        return;
      }
      if (!Array.isArray(node.children)) {
        return;
      }
      node.children.forEach((childNode) => {
        if (isVNode(childNode)) {
          tabNodes.push(childNode);
        }
      });
    });
    return tabNodes.filter((node) => isTabNode(node));
  }

  const tabNodes = computed<VNode[]>(() => {
    const children = slots.default?.() || [];
    return filterTabNodes(children);
  });

  const tabItems = computed<TabProps[]>(() => {
    return tabNodes.value.map((node) => {
      const tabProps = node.props as TabProps;
      return { ...tabProps };
    });
  });

  const tabCount = computed(() => {
    return tabItems.value.length;
  });

  const currentName = ref(props.modelValue);

  watch(
    () => props.modelValue,
    (newCurrentName: string | number) => {
      currentName.value = newCurrentName;
    }
  );

  const currentIndex = computed(() => {
    return tabItems.value.findIndex((tab) => (
      isDef(tab.name)
      && tab.name === currentName.value)
    );
  });

  function updateCurrentName(newValue: string | number): void {
    if (!isDef(newValue)) {
      return;
    }
    currentName.value = newValue;
    emit('update:modelValue', newValue);
    emit('change', newValue);
  };

  function updateCurrentIndex(newIndex: number): void {
    if (newIndex < 0 || newIndex >= tabCount.value) {
      return;
    }
    const tabItem = tabItems.value[newIndex];
    const tabName = tabItem && tabItem.name;
    updateCurrentName(tabName);
  }

  function findFirstAvailableTabIndex(startIndex = 0, reverseLookup = false): number {
    const delta = reverseLookup ? -1 : +1;
    let index = startIndex;
    while (index >= 0 && index < tabCount.value) {
      if (!tabItems.value[index].disabled) {
        return index;
      }
      index += delta;
    }
    return -1;
  };

  const isAtFirstTab = computed(() => {
    return currentIndex.value === 0;
  });

  const isAtLastTab = computed(() => {
    return currentIndex.value === tabCount.value - 1;
  });

  function moveToNextTab(): void {
    const nextTabIndex = findFirstAvailableTabIndex(currentIndex.value + 1);
    if (nextTabIndex >= 0) {
      updateCurrentIndex(nextTabIndex);
    }
  }

  function moveToPreviousTab(): void {
    const previousTabIndex = findFirstAvailableTabIndex(currentIndex.value - 1, true);
    if (previousTabIndex >= 0) {
      updateCurrentIndex(previousTabIndex);
    }
  }

  return {
    tabItems,
    currentName,
    currentIndex,
    updateCurrentName,
    updateCurrentIndex,
    tabCount,
    isAtFirstTab,
    isAtLastTab,
    moveToNextTab,
    moveToPreviousTab,
  };
}
