import { reactive, ref, Ref, computed } from 'vue';
import { useTouch, useEventListener } from '@components/common';
import { preventDefault } from '@components/common';
import { TabsProps } from '../tabs.props';
import { UseSwipe, UseTabs } from './type';

const MAX_OFFSET = 85;
const TRIGGER_OFFSET = 35;
const TRIGGER_SPEED = 0.3;

type SwipeDirection = 'vertical' | 'horizontal';

export function useSwipe(
  props: TabsProps,
  useTabsComposition: UseTabs,
  targetElementRef: Ref<HTMLElement>,
): UseSwipe {

  const {
    isAtFirstTab,
    isAtLastTab,
    moveToNextTab,
    moveToPreviousTab,
  } = useTabsComposition;

  const targetElementSizeRef = ref({ width: 0, height: 0 });
  function updateTargetElementSize(): void {
    targetElementSizeRef.value.width = targetElementRef.value?.clientWidth || 0;
    targetElementSizeRef.value.height = targetElementRef.value?.clientHeight || 0;
  };

  const enableSwipe = computed(() => props.swipeable);
  const swipeState = reactive({
    offset: 0,
    moving: false,
  });
  const expectSwipeDirection = computed<SwipeDirection>(() => {
    // @todo 待标签栏支持纵向排列后，根据标签栏的方向返回对应的可滑动方向
    return 'horizontal';
  });

  function limitMaximumAbsoluteValue(value: number, maxAbsoluteValue: number): number {
    maxAbsoluteValue = Math.abs(maxAbsoluteValue);
    if (Math.abs(value) <= maxAbsoluteValue) {
      return value;
    }
    if (value > 0) {
      return maxAbsoluteValue;
    }
    return -maxAbsoluteValue;
  }

  function updateOffset(deltaX: number, deltaY: number): void {
    let newOffset: number;
    if (expectSwipeDirection.value === 'horizontal') {
      newOffset = (-deltaX / targetElementSizeRef.value.width) * 100;
    } else {
      newOffset = (-deltaY / targetElementSizeRef.value.height) * 100;
    }
    newOffset = limitMaximumAbsoluteValue(newOffset, MAX_OFFSET);
    const isMovingToPreviousTab = newOffset < 0;
    const isMovingToNextTab = newOffset > 0;
    const cannotMoveToPreviousTab = isMovingToPreviousTab && isAtFirstTab.value;
    const cannotMoveToNextTab = isMovingToNextTab && isAtLastTab.value;
    const cannotMove = cannotMoveToPreviousTab || cannotMoveToNextTab;
    if (cannotMove) {
      newOffset = 0;
    }
    swipeState.offset = newOffset;
  };

  const touch = useTouch();
  let touchStartTime: number;

  const isCorrectDirection = computed(() => {
    return touch.direction.value === expectSwipeDirection.value;
  });

  function onTouchStart(event: TouchEvent): void {
    if (!enableSwipe.value) {
      return;
    }
    touchStartTime = Date.now();
    swipeState.moving = false;
    updateTargetElementSize();
    touch.start(event);
  };

  function onTouchMove(event: TouchEvent): void {
    if (!enableSwipe.value) {
      return;
    }
    touch.move(event);
    if (!isCorrectDirection.value) {
      return;
    }
    swipeState.moving = true;
    const { deltaX, deltaY } = touch;
    updateOffset(deltaX.value, deltaY.value);
    preventDefault(event, true);
  };

  function onTouchEnd(): void {
    if (!swipeState.moving) {
      return;
    }
    swipeState.moving = false;
    const duration = Date.now() - touchStartTime;
    const delta = touch.deltaX;
    const speed = Math.abs(delta.value / duration);
    const shouldSwipe = Math.abs(swipeState.offset) > TRIGGER_OFFSET || speed > TRIGGER_SPEED;
    if (!shouldSwipe) {
      return;
    }
    if (swipeState.offset > 0) {
      moveToNextTab();
    }
    if (swipeState.offset < 0) {
      moveToPreviousTab();
    }
  };

  useEventListener('touchstart', onTouchStart, { target: targetElementRef, passive: true });
  useEventListener('touchmove', onTouchMove, { target: targetElementRef });
  useEventListener('touchend', onTouchEnd, { target: targetElementRef });
  useEventListener('touchcancel', onTouchEnd, { target: targetElementRef });

  return {
    swipeState,
  };
}
