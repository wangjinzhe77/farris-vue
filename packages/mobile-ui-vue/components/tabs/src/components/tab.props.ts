/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ExtractPropTypes } from 'vue';

export const TAB_NAME = 'fm-tab';

export const tabProps = {

  /** 标签的名称，作为匹配的唯一标识 */
  name: { type: [String, Number], default: '' },

  /** 显示的标题 */
  title: { type: String, default: '' },

  /** 图标，如果非空则显示在标题的左侧 */
  icon: { type: String, default: undefined },

  /** 图标的颜色 */
  iconColor: { type: String, default: undefined },

  /** 选中状态下的图标 */
  activeIcon: { type: String, default: undefined },

  /** 选中状态下的图标的颜色 */
  activeIconColor: { type: String, default: undefined },

  /** 是否在标题右上角显示一个小圆点 */
  dot: { type: Boolean, default: undefined },

  /** 徽标的内容，如果非空则在标题的右上角显示一个徽标 */
  badge: { type: [String, Number], default: undefined },

  /** 是否禁用当前标签页 */
  disabled: { type: Boolean, default: false },

  /** 是否在隐藏时销毁内容 */
  destroyOnHide: { type: Boolean, default: false },
};

export type TabProps = ExtractPropTypes<typeof tabProps>;
