/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  defineComponent,
  SetupContext,
  inject,
  ref,
  computed,
  watch,
  CSSProperties,
} from 'vue';
import { TAB_NAME, tabProps, TabProps } from './tab.props';
import { TABS_CONTEXT_KEY } from '../composition/type';
import { isDef, useBem } from '@components/common';

export default defineComponent({
  name: TAB_NAME,

  props: tabProps,

  emits: [],

  setup(props: TabProps, context: SetupContext) {
    const { bem } = useBem(TAB_NAME);
    const { slots } = context;

    const parent = inject(TABS_CONTEXT_KEY, null);

    if (!parent) {
      throw new Error('FmTab component must be included by FmTabs component');
    }

    const isActive = computed(() => (
      isDef(props.name)
      && parent.currentName.value === props.name
    ));
    const alreadyRendered = ref(false);

    watch(
      () => isActive.value,
      (newValue) => {
        if (newValue) {
          alreadyRendered.value = true;
        }
      },
      { immediate: true }
    );

    const animationDuration = computed(() => {
      const duration = Number(parent.props.animationDuration);
      return isNaN(duration) ? 0 : duration;
    });

    const enableAnimation = computed(() => {
      return parent.props.animation && animationDuration.value > 0;
    });

    const shouldRender = computed(() => {
      const shouldRenderAll = enableAnimation.value;
      const shouldWaitUntilFirstRendered = parent.props.lazyRender && !alreadyRendered.value;
      const shouldDestroyOnHide = props.destroyOnHide && !isActive.value;
      return shouldRenderAll || (!shouldWaitUntilFirstRendered && !shouldDestroyOnHide);
    });

    const shouldCollapseHeight = computed(() => parent.props.fitHeight && !isActive.value);
    const tabClass = computed(() => ({
      [bem()]: true,
      [bem('', 'collapse')]: shouldCollapseHeight.value,
    }));

    const visibleOrReadyToSlideIn = computed(() => isActive.value || enableAnimation.value);
    const tabStyle = computed<CSSProperties>(() => ({
      display: visibleOrReadyToSlideIn.value ? undefined : 'none',
    }));

    return () => (
      <div class={tabClass.value} style={tabStyle.value}>
        {shouldRender.value && slots.default?.()}
      </div>
    );
  }
});
