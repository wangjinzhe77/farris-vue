/**
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { defineComponent, SetupContext, computed, ref, provide, readonly, CSSProperties } from 'vue';
import { TABS_NAME, tabsProps, TabsProps } from './tabs.props';
import { TabsContext, TABS_CONTEXT_KEY } from './composition/type';
import { useBem } from '@components/common';
import { useTabs } from './composition/use-tabs';
import { useSwipe } from './composition/use-swipe';
import { TabBar, TabBarItemProps } from '@components/tab-bar';

export default defineComponent({
  name: TABS_NAME,

  props: tabsProps,

  emits: ['update:modelValue', 'change'],

  setup(props: TabsProps, context: SetupContext) {
    const { bem } = useBem(TABS_NAME);
    const { slots } = context;
    const tabsContentRef = ref<HTMLElement>();

    const useTabsComposition = useTabs(props, context);
    const { tabItems, currentName, updateCurrentName, currentIndex } = useTabsComposition;

    const useSwipeComposition = useSwipe(props, useTabsComposition, tabsContentRef);
    const { swipeState } = useSwipeComposition;

    const tabBarItems = computed<TabBarItemProps[]>(() => {
      return tabItems.value.map((panel) => ({
        name: panel.name,
        title: panel.title,
        icon: panel.icon,
        iconColor: panel.iconColor,
        activeIcon: panel.activeIcon,
        activeIconColor: panel.activeIconColor,
        dot: panel.dot,
        badge: panel.badge,
        disabled: panel.disabled,
        active: false,
      }));
    });

    const tabsContext: TabsContext = {
      currentName,
      props: readonly(props),
    };

    provide(TABS_CONTEXT_KEY, tabsContext);

    function onTabChange(newValue: string | number): void {
      updateCurrentName(newValue);
    };

    const renderTabBar = () => {
      return (
        <TabBar
          class={bem('nav')}
          modelValue={currentName.value}
          items={tabBarItems.value}
          {...{
            'onUpdate:modelValue': onTabChange,
          }}
        />
      );
    };

    const animationDuration = computed(() => {
      const duration = Number(props.animationDuration);
      return isNaN(duration) ? 0 : duration;
    });

    const contentStyle = computed<CSSProperties>(() => {
      const shouldHideContent = currentIndex.value < 0;
      let offsetPercent = currentIndex.value * 100;
      if (swipeState.moving) {
        offsetPercent += swipeState.offset;
      }
      const enableAnimation = props.animation && animationDuration.value > 0;
      const enableTransition = enableAnimation && !swipeState.moving;
      return {
        display: shouldHideContent ? `none` : undefined,
        transform: enableAnimation ? `translate3d(-${offsetPercent}%, 0, 0)` : undefined,
        transitionDuration: enableTransition ? `${animationDuration.value}ms` : undefined,
      };
    });

    const renderContent = () => {
      return (
        <div ref={tabsContentRef} class={bem('content')} style={contentStyle.value}>
          {slots.default?.()}
        </div>
      );
    };

    return () => (
      <div class={bem()}>
        {renderTabBar()}
        {renderContent()}
      </div>
    );
  }
});
