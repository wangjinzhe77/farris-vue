import type { App } from 'vue';
import Designer from './src/designer.component';

export * from './src/designer.props';
export { Designer };

export default {
  install(app: App): void {
    app.component(Designer.name, Designer);
  }
};
