import { SetupContext, defineComponent, ref, computed } from "vue";
import { DesignerProps, designerProps } from "./designer.props";
// @ts-ignore
import { TabHeaderJustifyMode, TabType } from "../../components/tabs";
// @ts-ignore
import { ComponentSchema } from "../../components/designer-canvas/src/types";

export default defineComponent({
  name: 'FDesigner',
  props: designerProps,
  emits: [],
  setup(props: DesignerProps, context: SetupContext) {
    const title = ref('Farris Vue Designer');
    const schema = computed(() => {
      return props.schema && props.schema.contents && props.schema.contents[0];
    });
    const dragulaCompostion = ref();
    const fillTabs = ref(true);
    const tabType = ref<TabType>('pills');
    const justify = ref<TabHeaderJustifyMode>('center');

    const toolbarHandler = {
      save() {
        const schemaString = JSON.stringify(schema.value);
        window.localStorage.setItem('localSchema', schemaString);
      },
      run() {
        window.open(`${window.location.origin}/#dynamic-view/basic`);
      }
    } as Record<string, () => void>;

    function onCanvasInitialized(dragula: any) {
      dragulaCompostion.value = dragula;
    }

    const onClickToolbarItem = (itemId: string) => { };

    function executeMethod($event: MouseEvent, method: string) {
      const methodToBeExecuted = toolbarHandler[method];
      if (methodToBeExecuted) {
        methodToBeExecuted();
      }
    }

    const items = [
      { id: 'save', text: '保存', onClick: executeMethod },
      { id: 'run', text: '运行', onClick: executeMethod, class: 'btn-primary' }
    ];
    const propertyConfig = ref();
    const propertyName = ref();
    const focusingSchema = ref();

    function onSelectionChange(schemaType: string, schemValue: ComponentSchema) {
      propertyName.value = schemaType;
      focusingSchema.value = schemValue;
    }

    return () => {
      return (
        <div class="f-page f-page-navigate f-page-is-grid-card">
          <f-page-header title={title.value} buttons={items} onClick={onClickToolbarItem}></f-page-header>
          <div class="f-page-main">
            <f-splitter class="f-page-content">
              <f-splitter-pane class="f-col-w2 f-page-content-nav" position="left" style="min-width: 240px">
                <div class="f-struct-wrapper f-utils-fill-flex-column">
                  <f-tabs tabType={tabType.value} justify-content={justify.value} fill={fillTabs.value}>
                    <f-tab-page id="tools" title="工具箱">
                      <f-designer-toolbox dragula={dragulaCompostion.value}></f-designer-toolbox>
                    </f-tab-page>
                    <f-tab-page id="outline" title="大纲">
                      <f-control-tree-view
                        data={props.schema}
                        tree-node-icons-data="treeNodeIconsData"
                        show-tree-node-icons={true}
                        onSelectionChanged={(schema: ComponentSchema) => onSelectionChange(schema.type, schema)}
                      ></f-control-tree-view>
                    </f-tab-page>
                  </f-tabs>
                </div>
              </f-splitter-pane>
              <f-splitter-pane class="f-page-content-main" position="center">
                <f-designer-canvas
                  v-model={schema.value}
                  onInit={onCanvasInitialized}
                  onSelectionChange={onSelectionChange}
                ></f-designer-canvas>
                <f-property-panel
                  propertyConfig={propertyConfig.value}
                  propertyName={propertyName.value}
                  schema={focusingSchema.value}
                ></f-property-panel>
              </f-splitter-pane>
            </f-splitter>
          </div>
        </div>
      );
    };
  }
});
