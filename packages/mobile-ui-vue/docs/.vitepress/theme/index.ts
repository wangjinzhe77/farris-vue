import FarrisMobile from '../../../components';
import FarrisTheme from '../farris-theme';
import { registerComponents } from './register-components.js';
import { insertBaiduScript } from './insert-baidu-script';

export default {
  ...FarrisTheme,
  enhanceApp({ app }) {
    app.use(FarrisMobile);
    registerComponents(app);
    insertBaiduScript();
  }
};
