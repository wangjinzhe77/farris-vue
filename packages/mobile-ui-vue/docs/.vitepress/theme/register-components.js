import FExample from '../farris-theme/example/FExample.vue';
import VPDemo from '../farris-theme/components/VPDemo.vue';

export function registerComponents(app) {
    app.component('Demo', FExample).component('VPDemo', VPDemo);
}
