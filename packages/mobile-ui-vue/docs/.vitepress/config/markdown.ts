import type MarkdownIt from 'markdown-it';
import { farrisMarkdownPlugin } from '../plugins/farris-markdown-plugin';
import { highlight as highlightFactory } from '../utils/highlight-shiki';

const markdown = {
    config: async (md: MarkdownIt) => {
        const highlight = await highlightFactory();
        md.use(farrisMarkdownPlugin, { cssPreprocessor: 'scss', highlight });
    }
};

export default markdown;
