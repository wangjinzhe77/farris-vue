const sidebar = [
    {
        text: '介绍',
        items: [
            { text: '快速开始', link: '/guide/quick-start/' }
        ]
    },
    {
        text: '基础组件',
        items: [
            { text: 'Icon 图标', link: '/components/icon/' },
            { text: 'Button 按钮', link: '/components/button/' },
            { text: 'Cell 单元格', link: '/components/cell/' },
            { text: 'Overlay 遮罩', link: '/components/overlay/' },
            { text: 'Popup 弹出框', link: '/components/popup/' },
        ]
    },
    {
        text: '录入数据',
        items: [
            { text: 'Form 表单', link: '/components/form/' },
            { text: 'Input 输入控件', link: '/components/input/' },
            { text: 'Radio 单选框', link: '/components/radio/' },
            { text: 'Checkbox 复选框', link: '/components/checkbox/' },
            { text: 'Picker 选择器', link: '/components/picker/' },
            { text: 'DatePicker 日期选择器', link: '/components/date-picker/' },
            { text: 'DateTimePicker 日期时间选择器', link: '/components/date-time-picker/' },
            { text: 'TimePicker 时间选择器', link: '/components/time-picker/' },
            { text: 'Rate 评分', link: '/components/rate/' },
            { text: 'Switch 开关', link: '/components/switch/' },
        ]
    },
    {
        text: '反馈组件',
        items: [
            { text: 'ActionSheet 动作面板', link: '/components/action-sheet/' },
            { text: 'Loading 加载中', link: '/components/loading/' },
            { text: 'Notify 消息提示框', link: '/components/notify/' },
            { text: 'Toast 轻提示', link: '/components/toast/' },
            { text: 'Dialog 对话框', link: '/components/dialog/' },
            { text: 'PullRefresh 下拉刷新', link: '/components/pull-refresh/' },
            { text: 'SwipeCell 滑动单元格', link: '/components/swipe-cell/' },
        ]
    },
    {
        text: '展示组件',
        items: [
            { text: 'List 列表', link: '/components/list/' },
            { text: 'ListView 列表视图', link: '/components/list-view/' },
        ]
    },
    {
        text: '导航组件',
        items: [
            { text: 'Navbar 导航栏', link: '/components/navbar/' },
            { text: 'Tabs 标签页', link: '/components/tabs/' },
        ]
    },
    {
        text: '个性化',
        items: [
            { text: '自定义打包', link: '/customization/build/' }
        ]
    }
];

export default sidebar;
