import { computed, ComputedRef, ref, SetupContext } from 'vue';
import { UseCollapse } from './type';


export function useCollapse(props: any, setupContext: SetupContext, locale: ComputedRef<any>): UseCollapse {

    const isExpanded = ref(false);


    const controlText = computed(() => {
        return isExpanded.value ? locale.value['hide-text'] : locale.value['show-text'];
    });

    function onClickControl() {
        isExpanded.value = !isExpanded.value;

    };

    return { controlText, isExpanded, onClickControl };

}


