export const handleMessage = () => {

    const inBrowser = typeof window !== 'undefined';
    if (!inBrowser) {
        return;
    }

    const locateTitle = (node: HTMLElement) => {
        window.scroll({
            top: node.offsetTop,
            behavior: 'smooth',
        });
    };

    const getChildAnchorElement = (node: Element): HTMLAnchorElement | null => {
        const {childNodes} = node;
        for (let i = 0; i < childNodes.length; i++) {
            const childNode = childNodes[i];
            const isElement = childNode.nodeType === childNode.ELEMENT_NODE;
            if (!isElement) {
                continue;
            }
            const element = childNode as Element;
            if (element.tagName.toLowerCase() === 'a') {
                return element as HTMLAnchorElement;
            }
        }
        return null;
    };

    const isTitleElement = (node: Element): boolean => {
        const tagName = node.tagName.toLowerCase();
        const isHead = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'].includes(tagName);
        return isHead && !!getChildAnchorElement(node);
    };

    const findTitleNode = (node: Element | null): HTMLElement | null => {
        const MAX_ATTEMPTS = 50;
        let count = 0;
        while (node) {
            if (isTitleElement(node)) {
                return node as HTMLElement;
            }
            count++;
            if (count > MAX_ATTEMPTS) {
                break;
            }
            node = node.previousElementSibling;
        }
        return null;
    };

    const showDemoCodeByID = (demoID: string) => {
        const targetCodeBlock = document.getElementById(demoID);
        if (!targetCodeBlock) {
            return;
        }
        const isDemoCodeBlock = targetCodeBlock.classList.contains('demo-code-container');
        if (!isDemoCodeBlock) {
            return;
        }
        const titleElement = findTitleNode(targetCodeBlock);
        if (titleElement) {
            locateTitle(titleElement);
        }
    };

    window.addEventListener('message', (event) => {
        const { data } = event;
        if (!data || typeof data !== 'object') {
            return;
        }
        const { action, id } = data;
        if (action === 'showDemoCodeByID') {
            setTimeout(() => {
                showDemoCodeByID(id);
            }, 100);
        }
    });
};

export const sendMessageToDemoFrame = (data: any) => {
    const demoFrameName = 'fmDemoFrame';
    const demoFrame: Window = window.frames[demoFrameName];
    if (demoFrame) {
        demoFrame.postMessage(data, '*');
    }
};
