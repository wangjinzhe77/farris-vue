const animateCls = 'inb-animate-slideInUp';
const topDistance = 120;
/**
 * 处理动画
 * @param elements 
 * @param extendfunc 
 */
export function animateFunc(elements, extendfunc,isLargeSize=true) {
    const viewHeight = window.innerHeight,
        viewTop = document.documentElement.scrollTop,
        viewBottom = viewTop + viewHeight;
    const wrapper=document.querySelector('.farris-doc-feature-wrapper');
    if(!wrapper) {return;}
    for (let k = 0; k < elements.length; k++) {
        const ele = elements[k];
        const elementAnimation = ele.getAttribute('animate') ? ele.getAttribute('animate') : animateCls,
            elementHeight = ele.clientHeight,
            elementTop = ele.offsetTop+wrapper['offsetTop'],
            elementBottom = elementTop + elementHeight;
        if (elementBottom >= viewTop && elementTop <= viewBottom) {
            ele.style.visibility = 'visible';
            if (!ele.classList.contains(elementAnimation)) {
                ele.className = ele.className + ' ' + elementAnimation;
                ele.className = ele.className + ' ' + 'inb-animated';
                setTimeout(()=>{
                    ele.className = ele.className + ' ' + 'animate-img';
                },300);
            }
        } else {
            ele.style.visibility = 'hidden';
            ele.className = ele.className.replace(elementAnimation, '');
            ele.className = ele.className.replace('inb-animated', '');
            ele.className = ele.className.replace('animate-img', '');
        }
        
        if (elementTop + 30 < viewHeight * 0.5 + viewTop && elementBottom - 30 > viewHeight * 0.5 + viewTop) {
            extendfunc && extendfunc(k,isLargeSize);
        }

        // if(elementBottom)
    }
}
/**
 * 改变标题
 * @param cur 
 */
export function changeHeader(cur,sLargeSize=true) {
    const headerContainer = document.querySelectorAll('.feature-header');
    if (headerContainer) {
        for (let k = 0; k < headerContainer.length; k++) {
            const ele = headerContainer[k];
            if (k == cur) {
                if (!ele.classList.contains(animateCls)) {
                    ele.className = ele.className + ' ' + animateCls;
                }
            } else {
                ele.className = ele.className.replace(animateCls, '');
            }
        }
    }
}
/**
 * 中间区域左侧的标题导航
 * @returns 
 */
export function changeFeatureCls(isLargeSize=true) {
    const featureEL=document.querySelector('.farris-doc-feature');    
    const wrapper=document.querySelector('.farris-doc-feature-wrapper');
    if(!featureEL||!wrapper) {return;}
    if(!isLargeSize){
        featureEL.className=featureEL.className.replace('fixed','');
        return;
    }
    const viewHeight = window.innerHeight;
    const viewTop = document.documentElement.scrollTop;
    
    if (viewTop + topDistance > wrapper['offsetTop'] && wrapper['offsetTop'] + wrapper['clientHeight'] > viewTop + topDistance + viewHeight) {
            if(!featureEL.classList.contains('fixed')){
                featureEL.className=featureEL.className+' fixed';
            }      
    } else {
        if(featureEL.classList.contains('fixed')){
            featureEL.className=featureEL.className.replace('fixed','');
        }      
    }

}
/**
 * 顶部导航
 * @returns 
 */
export function changePageHeaderFixedState(){
    const viewTop = document.documentElement.scrollTop;
    const navEl=document.querySelector('.VPNav');
    if(!navEl) {return;}
    if(viewTop> navEl['clientHeight']){
        if(!navEl.classList.contains('fixed')){
            navEl.className=navEl.className+' fixed';
        }      
    }else{
        if(navEl.classList.contains('fixed')){
            navEl.className=navEl.className.replace('fixed','');
        }    
       
    }
    
}
/**
 * 改变窗口大小
 * @returns 
 */
export function resizeWindow() {
    const viewSize = { "el": 1600, "xl": 1240, "lg": 1000, "md": 830, "sm": 730,"min":639 };
    const windowWidth = parseInt(document.documentElement.clientWidth+'', 10);
    let matchSize = "";
    // 暂时没有其他用处
    if (windowWidth > viewSize['el']) {
      matchSize = "ex";
    } else if (windowWidth > viewSize['xl']) {
      matchSize = "el";
    } else if (windowWidth > viewSize['lg']) {
      matchSize = "xl";
    } else if (windowWidth > viewSize['md']) {
      matchSize = "lg";
    } else if (windowWidth > viewSize['sm']) {
      matchSize = "md";
    } else if (windowWidth > viewSize['min']) {
      matchSize = "sm";
    }else{
      matchSize = "min";
    }
 return matchSize; 
}
