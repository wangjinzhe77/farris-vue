<script lang="ts" setup>
import { useSidebar } from '../composables/sidebar.js';
import VPNavBarTitle from './VPNavBarTitle.vue';
import VPNavBarSearch from './VPNavBarSearch.vue';
import VPNavBarMenu from './VPNavBarMenu.vue';
import VPNavBarTranslations from './VPNavBarTranslations.vue';
import VPNavBarAppearance from './VPNavBarAppearance.vue';
import VPNavBarSocialLinks from './VPNavBarSocialLinks.vue';
import VPNavBarExtra from './VPNavBarExtra.vue';
import VPNavBarHamburger from './VPNavBarHamburger.vue';

defineProps<{
  isScreenOpen: boolean
}>();

defineEmits<(e: 'toggle-screen') => void>();

const { hasSidebar } = useSidebar();
</script>

<template>
  <div class="VPNavBar" :class="{ 'has-sidebar' : hasSidebar }">
    <div class="container">
      <VPNavBarTitle>
        <template #nav-bar-title-before><slot name="nav-bar-title-before" /></template>
        <template #nav-bar-title-after><slot name="nav-bar-title-after" /></template>
      </VPNavBarTitle>

      <div class="content">
        <slot name="nav-bar-content-before" />
        <VPNavBarSearch class="search" />
        <VPNavBarMenu class="menu" />
        <VPNavBarTranslations class="translations" />
        <VPNavBarAppearance class="appearance" />
        <VPNavBarSocialLinks class="social-links" />
        <VPNavBarExtra class="extra" />
        <slot name="nav-bar-content-after" />
        <VPNavBarHamburger
          class="hamburger"
          :active="isScreenOpen"
          @click="$emit('toggle-screen')"
        />
      </div>
    </div>
  </div>
</template>

<style scoped>
.VPNavBar {
  position: relative;
  padding: 0 8px 0 0;
  transition: border-color 0.5s, background-color 0.5s;
  pointer-events: none;
}
.VPNavBar.has-sidebar .container {
    border-bottom: 1px solid var(--vp-c-divider-light);
  }

@media (min-width: 768px) {
  .VPNavBar {
    padding: 0 8px 0 0;
  }
}

@media (min-width: 960px) {
  .VPNavBar {
    height: var(--vp-nav-height-desktop);
  }

  .VPNavBar.has-sidebar .content {
    margin-right: -32px;
    padding-right: 64px;
  }
}

.container {
  display: flex;
  justify-content: space-between;
  margin: 0 auto;
  width:100%;
  max-width:100%;
  pointer-events: none;
  padding: 0 20px;
}

.container :deep(*) {
  pointer-events: all;
}

.content {
  display: flex;
  justify-content: flex-end;
  align-items: center;
  flex-grow: 1;
}

.menu + .translations::before,
.menu + .appearance::before,
.menu + .social-links::before,
.translations + .appearance::before,
.appearance + .social-links::before {
  margin-right: 8px;
  margin-left: 8px;
  width: 1px;
  height: 24px;
  background-color: var(--vp-c-divider-light);
  content: "";
}

/* no menu for now */
.menu + .appearance::before {
  display: none;
}

.menu + .appearance::before,
.translations + .appearance::before {
  margin-right: 16px;
}

.appearance + .social-links::before {
  margin-left: 16px;
}

.social-links {
  margin-right: -8px;
}
</style>
