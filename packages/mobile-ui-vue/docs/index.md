---
layout: home
hero:
  name: Farris Design Mobile
  text: Farris移动端组件库
  tagline: 基于 Farris Design 具有 Fast Reliable Responsive Intuitive Smart 五大特性。
  actions:
    - theme: brand
      text: 快速开始
      link: /guide/quick-start/
    - theme: Gitee
      text: View on Gitee
      link: https://ubml.atomgit.net/farris-docs-mobile
---

<script lang="ts">
const inBrowser = typeof window !== 'undefined';
inBrowser && location.replace(`/farris-docs-mobile/guide/quick-start/`);
</script>
