# 快速开始

## 开始使用 Farris Mobile UI Vue

### 1. 安装@farris/mobile-ui-vue

```bash
npm install @farris/mobile-ui-vue
```

```bash
yarn add @farris/mobile-ui-vue
```

```bash
pnpm add @farris/mobile-ui-vue
```

### 2. 在应用中引入 Farris Mobile UI Vue

在`main.ts`文件中引入`@farris/mobile-ui-vue`。

#### 全量引入
```ts
import { createApp } from 'vue';
import App from './App.vue';
import Farris from '@farris/mobile-ui-vue';
import '@farris/mobile-ui-vue/style.css'

createApp(App).use(Farris).mount('#app');
```

#### 按需引入
```ts
import { createApp } from 'vue';
import App from './App.vue';
import { Button } from '@farris/mobile-ui-vue';
import '@farris/mobile-ui-vue/button/style.css'

createApp(App).use(Button).mount('#app');
```

### 3. 在应用中使用 Farris Mobile UI Vue

在`App.vue`文件中使用 Farris Mobile UI Vue 组件。

```vue
<template>
    <fm-button>按钮</fm-button>
</template>
```
