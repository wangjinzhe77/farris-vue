---
demo: time-picker
---

# TimePicker 时间选择器

基于Picker 封装，用于选择时间。

## 基础用法


:::vdemo

```vue
{demos/time-picker/basic.vue}
```
:::

## 显示类型

`type`属性用于设置时间选择器的显示类型，支持选择时分秒、时分、时。默认为时分秒选择。

:::vdemo

```vue
{demos/time-picker/type.vue}
```
:::

## 最大最小值

`min-time`用于自定义设置最小值，`max-time`用于自定义设置最大值。

:::vdemo

```vue
{demos/time-picker/min-max.vue}
```
:::

## 自定义显示选项内容

`option-formatter` 属性用来设置列选项的显示内容, 支持传入一个函数，函数参数为当前列及选项值，返回值会作为列选项的显示内容。

:::vdemo

```vue
{demos/time-picker/formatter.vue}
```
:::
## TimePickerInput 日期时间选择器输入框

集成 TimePicker 组件和 Input 组件, 用于在表单中快速使用。

:::vdemo

```vue
{demos/time-picker/input.vue}
```
:::

## 类型

```typescript
type TimePickerType = "hours" | "hours-minutes" | "hours-minutes-seconds" | "time"
```
## TimePicker 属性

| 属性名          | 类型                                | 默认值      | 说明                          |
| :-------------- | :------------------------           | :---------- | :-------------------------------------- |
| modelValue       | `Date`                              | --          | 日期选择器当前值              |
| type             | `TimePickerType`      | 'time'     | 日期选择器显示类型                  |
| min-time         | `string`                              | --          | 日期选择器选择范围最小值            |
| max-time          | `string`                              | --          | 日期选择器选择范围最大值                               |
| option-formatter | `(value: number) => string`         | --          | 自定义显示选项内容          |
| format           | `string`                          | --          | 日期选择器显示格式               |
| visiableOptionCount | `number`                       | 5           | 日期选择器选项可见数量              |
| showToolbar         | `boolean`                           | true        | 日期选择器工具栏是否显示         |
| title             | `string`                           | --          | 日期选择器工具栏标题               |

## TimePickerInput 属性

| 属性名          | 类型                                | 默认值      | 说明                          |
| :-------------- | :------------------------           | :---------- | :-------------------------------------- |
| modelValue       | `Date`                              | --          | 日期选择器当前值              |
| type             | `TimePickerType`      | 'time'     | 日期选择器显示类型                  |
| min-time         | `string`                              | --          | 日期选择器选择范围最小值            |
| max-time          | `string`                              | --          | 日期选择器选择范围最大值                               |
| option-formatter | `(value: number) => string`         | --          | 自定义显示选项内容          |
| format           | `string`                          | --          | 日期选择器显示格式               |
| visiableOptionCount | `number`                       | 5           | 日期选择器选项可见数量              |
| showToolbar         | `boolean`                           | true        | 日期选择器工具栏是否显示         |
| title             | `string`                           | --          | 日期选择器工具栏标题               |
| name                | `string`                                        | --          | 输入框的名称            |
| placeholder         | `string`                                        | --          | 输入框的提示文字        |
| readonly            | `boolean`                                       | false       | 输入框是否只读          |
| disabled            | `boolean`                                       | false       | 输入框是否禁用          |
| formatter           | `(value: string ) => string` | --          | 输入框显示内容格式化函数              |

## TimePicker & TimePickerInput 事件

| 事件名               | 类型                              | 说明              |
| :------------------ | :-------------------------------- | :--------------- |
| update:modelValue   | `EventEmitter<string>`   | 值变化事件        |
| change              | `EventEmitter<string>`   | 切换选择项事件       |
| confirm             | `EventEmitter<string>`        |      确认按钮点击事件 |

