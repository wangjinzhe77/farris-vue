---
demo: pull-refresh
---

# PullRefresh 下拉刷新

PullRefresh 组件提供了通过下拉页面刷新数据的交互方式。

## 基础用法

在`refresh`事件的回调方法中进行更新数据等操作。

:::vdemo

```vue
{demos/pull-refresh/base.vue}
```

:::

## 自定义提示

通过`pullingText`、`loosingText`等属性或者`pulling`、`loosing`等插槽可以对各个状态的提示进行自定义。

:::vdemo

```vue
{demos/pull-refresh/custom.vue}
```

:::

## 属性

| 属性名           | 类型                     | 默认值 | 说明                                                 |
| :--------------- | :----------------------- | :----- | :--------------------------------------------------- |
| modelValue       | `boolean`                | false  | 是否处于加载中状态，支持语法糖`v-model`              |
| headHeight       | `number \| string`       | 50     | 顶部内容高度，单位是`px`                             |
| pullDistance     | `number \| string`       | --     | 触发刷新所需下拉的最小距离，默认值与`headHeight`相等 |
| maxPullDistance  | `number \| string`       | 0      | 最大下拉距离，小于等于 0 时不限制                    |
| loadingProps     | `Object as LoadingProps` | {}     | 传递给加载中组件的属性                               |
| pullingText      | `string`                 | --     | 下拉时的提示文本                                     |
| loosingText      | `string`                 | --     | 释放时的提示文本                                     |
| loadingText      | `string`                 | --     | 加载时的提示文本                                     |
| completeText     | `string`                 | --     | 加载完成的提示文本                                   |
| completeDuration | `number \| string`       | 0      | 加载完成的提示文本的显示时长，单位`ms`               |
| disabled         | `boolean`                | false  | 是否禁用                                             |

## 事件

| 事件名  | 类型                 | 说明                     |
| :------ | :------------------- | :----------------------- |
| refresh | `EventEmitter<void>` | 刷新事件，结束下拉时触发 |

## 插槽

| 名称     | 说明                                                     |
| :------- | :------------------------------------------------------- |
| default  | 内容                                                     |
| pulling  | 下拉状态下的提示（下拉距离尚未达到触发刷新所需要的距离） |
| loosing  | 释放状态下的提示（下拉距离已经达到触发刷新所需要的距离） |
| loading  | 加载状态下的提示                                         |
| complete | 刷新完成后的提示                                         |

## CSS 变量

| 名称                                 | 默认值                     | 说明 |
| :----------------------------------- | :------------------------- | :--- |
| --fm-pull-refresh-head-height        | 50px                       | --   |
| --fm-pull-refresh-head-font-size     | var(--fm-font-size-md)     | --   |
| --fm-pull-refresh-head-text-color    | var(--fm-text-color-light) | --   |
| --fm-pull-refresh-animation-duration | 300ms                      | --   |
