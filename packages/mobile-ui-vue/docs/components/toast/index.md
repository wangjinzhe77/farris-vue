---
demo: toast
---

# Toast 轻提示

Toast 组件用于在页面上方提示出一段信息。

## 基础用法

通过`success`、`warning`、`error`、`info`方法可以设置提示类型。

:::vdemo

```vue
{demos/toast/base.vue}
```

:::

## 自定义

传入提示文本。

:::vdemo

```vue
{demos/toast/custom.vue}
```

:::

## 自定义图标

通过`icon`属性可以设置图标。

:::vdemo

```vue
{demos/toast/icon.vue}
```

:::

## 加载中

通过`loading`方法可以设置加载中状态。

:::vdemo

```vue
{demos/toast/loading.vue}
```

:::

## 类型

```typescript
type ToastType = 'success' | 'warning' | 'error' | 'info' | 'default' | 'loading';
```

## 属性

| 属性名    | 类型                | 默认值   | 说明                                    |
| :-------- | :------------------ | :------- | :-------------------------------------- |
| type      | string as ToastType | `info`   | 提示类型                                |
| position  | string              | `middle` | 位置，可选值为 `top` `bottom` `middle`  |
| message   | string              | --       | 文本内容                                |
| duration  | number              | `3000`   | 展示时长(ms)，值为 0 时，toast 不会消失 |
| className | any                 | --       | 自定义类名                              |

## 方法

| 方法名          | 参数                            | 返回值     | 说明         |
| :-------------- | :------------------------------ | :--------- | :----------- |
| `Toast`         | `options            \| message` | toast 实例 | 展示提示     |
| `Toast.info `   | `options            \| message` | toast 实例 | 展示默认提示 |
| `Toast.success` | `options            \| message` | toast 实例 | 展示成功提示 |
| `Toast.error`   | `options            \| message` | toast 实例 | 展示失败提示 |
| `Toast.warning` | `options            \| message` | toast 实例 | 展示提示     |
| `Toast.clear`   | `clearAll: boolean    \| void`  | --         | 关闭提示     |

## CSS 变量

| 名称                        | 默认值               | 说明                            |
| :-------------------------- | :------------------- | :------------------------------ |
| `--fm-toast-zindex`         | `var(--fm-zindex-5)` | 设置 Toast 组件的层叠顺序       |
| `--fm-toast-color`          | `var(--fm-white)`    | 设置 Toast 组件的文字颜色       |
| `--fm-toast-font-size`      | `16px`               | 设置 Toast 组件的字体大小       |
| `--fm-toast-background`     | `var(--fm-gray-7)`   | 设置 Toast 组件的背景颜色       |
| `--fm-toast-icon-font-size` | `48px`               | 设置 Toast 组件中图标的字体大小 |
