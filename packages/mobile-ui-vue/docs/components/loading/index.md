---
demo: loading
---

# Loading 加载中

Loading 组件用于表示页面或区域的加载中状态，提示用户等待一会儿。

## 图标类型

通过`type`属性可以设置加载中图标的类型。

:::vdemo

```vue
{demos/loading/base.vue}
```

:::

## 自定义颜色

通过`color`属性可以设置图标和文本的颜色。

:::vdemo

```vue
{demos/loading/color.vue}
```

:::

## 自定义动画播放速度

通过`duration`属性可以设置动画播放一个周期的时长。

:::vdemo

```vue
{demos/loading/duration.vue}
```

:::

## 自定义大小/描边宽度

通过`size`属性可以设置图标的大小，通过`stroke`属性可以设置图标的线条粗细。

:::vdemo

```vue
{demos/loading/size.vue}
```

:::

## 自定义文本

通过`text`属性可以设置文本。

:::vdemo

```vue
{demos/loading/text.vue}
```

:::

## 垂直对齐

通过`direction`属性可以设置图标和文本的对齐方式，默认是水平对齐，也可以垂直对齐。

:::vdemo

```vue
{demos/loading/vertical.vue}
```

:::

## 类型

```typescript
type LoadingType = 'default' | 'spinner' | 'ring';
type LoadingDirection = 'horizontal' | 'vertical';
```

## 属性

| 属性名    | 类型                         | 默认值       | 说明                                                         |
| :-------- | :--------------------------- | :----------- | :----------------------------------------------------------- |
| type      | `string as LoadingType`      | 'default'    | 加载图标的类型，可选值：`default`（默认）、`spinner`、`ring` |
| text      | `string`                     | --           | 文本内容                                                     |
| direction | `string as LoadingDirection` | 'horizontal' | 图标与文本的对齐方向，默认水平对齐，可选垂直对齐             |
| size      | `string`                     | '24px'       | 图标的大小                                                   |
| textSize  | `string`                     | --           | 文本的字体大小                                               |
| stroke    | `number`                     | --           | 图标的描边宽度                                               |
| color     | `string`                     | --           | 图标颜色                                                     |
| textColor | `string`                     | --           | 文本的颜色，默认与图标颜色相同                               |
| duration  | `number`                     | --           | 加载动画播放一个周期的时长，单位：ms                         |

## 插槽

| 名称    | 说明       |
| :------ | :--------- |
| default | 文本内容   |
| icon    | 自定义图标 |

## CSS 变量

| 名称                          | 默认值           | 说明 |
| :---------------------------- | :--------------- | :--- |
| --fm-loading-color            | var(--fm-gray-5) | --   |
| --fm-loading-text-font-size   | 14px             | --   |
| --fm-loading-text-line-height | 1.4              | --   |
