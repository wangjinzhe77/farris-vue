---
demo: input
---

# Input 输入框

表单中的输入框组件。

## 基础用法

可以通过 `v-model` 双向绑定输入框的值，通过 `placeholder` 设置占位提示文字。

:::vdemo

```vue
{demos/input/basic.vue}
```
:::

## 输入框状态

`readonly` 、`disabled`属性用来设置只读、禁用。

:::vdemo

```vue
{demos/input/status.vue}
```
:::

## 类型

`type` 属性用来设置 input 类型，默认为`text`, 包括`text`, `digit`, `number`, `textarea`, `search`, `password`。

:::vdemo

```vue
{demos/input/type.vue}
```
:::

## 对齐

`input-align` 属性用来设置对齐方式，默认为`right`, 包括`left`, `center`, `right`。

:::vdemo

```vue
{demos/input/align.vue}
```
:::

## 显示图标

输入框左右两侧支持设置图标。

:::vdemo

```vue
{demos/input/icon.vue}
```
:::

## 显示清空图标

`clearable` 属性用来设置输入框是否显示清空图标（只在输入框不为空并且聚焦时显示）。

:::vdemo

```vue
{demos/input/clearable.vue}
```
:::

## 多行文本

:::vdemo

```vue
{demos/input/text-area.vue}
```
:::

## 自定义格式化函数

`formatter`

:::vdemo

```vue
{demos/input/formatter.vue}
```
:::

## 插槽

:::vdemo

```vue
{demos/input/slot.vue}
```
:::

## 属性

| 属性名          | 类型                                            | 默认值        | 说明                    |
| :-------------- | :------------------------                       | :---------- | :------------------------- |
| modelValue      | `string \| number`                              | `--`          | 输入框的值              |
| name            | `string`                                        | `--`          | 输入框的名称            |
| type            | `string`                                        | `text`        | 输入框的类型            |
| placeholder     | `string`                                        | `--`          | 输入框的提示文字        |
| editable        | `boolean`                                       | `true`        | 输入框是否可编辑        |
| readonly        | `boolean`                                       | `false`       | 输入框是否只读          |
| disabled        | `boolean`                                       | `false`       | 输入框是否禁用          |
| clearable       | `boolean`                                       | `false`       | 输入框是否显示清空图标   |
| input-align     | `string`                                        | `left`       | 输入框对齐方式          |
| left-icon       | `string`                                        | `--`        | 输入框左侧图标名          |
| right-icon      | `string`                                         | `--`        | 输入框右侧图标名          |
| formatter       | `(value: string \| number) => string \| number` | `--`          | 格式化函数              |
| maxlength       | `number`                                        | `--`          | 输入框的最大长度         |
| border          | `boolean`                                        | `false`       | 输入框是否显示边框       |


## 事件

| 事件名               | 类型                              | 说明              |
| :------------------ | :-------------------------------- | :--------------- |
| update:modelValue   | `EventEmitter<string \| nunmber>` | 值变化事件        |
| clear               | `EventEmitter<void>`              | 清空输入框值事件   |
| blur                | `EventEmitter<MouseEvent>`        | 输入框失去焦点事件 |
| focus               | `EventEmitter<MouseEvent>`        | 输入框聚焦事件     |
| input               | `EventEmitter<InputEvent>`        | 输入框输入事件     |
| click-left-icon     | `EventEmitter<MouseEvent>`        | 输入框左侧图标点击事件     |
| click-right-icon    | `EventEmitter<MouseEvent>`        | 输入框右侧图标点击事件     |

## 插槽

| 名称                | 说明                                     |
| :------------------ | :--------------------------------------- |
| left                | 输入框左侧内容                           |
| right               | 输入框右侧内容                           |


## 组件实例

| 名称                | 类型                              | 说明              |
| :------------------ | :-------------------------------- | :--------------- |
| blur                | () => void                        |  输入框失去焦点   |
| focus               | () => void                        |  输入框聚焦       |

## CSS 变量

| 名称                        | 默认值                        | 说明 |
| :-------------------------- | :--------------------------- | :--- |
| --fm-input-size             | var(--fm-font-size)          | --   |
| --fm-input-color            | var(--fm-text-color)         | --   |
| --fm-input-padding          | 10px 16px                    | --   |
| --fm-input-border-color     | var(--fm-gray-2)             | --   |