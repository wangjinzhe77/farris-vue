---
demo: navbar
---

# Navbar 导航栏

用于多个选项中只选取一个结果。

## 基础用法

导航栏基本用法
:::vdemo

```vue
{demos/navbar/base.vue}
```

:::

## 多个按钮

配置多个按钮

:::vdemo

```vue
{demos/navbar/button.vue}
```

:::

## 使用插槽

通过插槽自定义导航栏两侧的内容。
:::vdemo

```vue
{demos/navbar/slot.vue}
```

:::

## 属性

| 参数      | 说明             | 类型      | 默认值  |
| --------- | ---------------- | --------- | ------- |
| title     | 标题             | _string_  | `-`     |
| fixed     | 是否固定在顶部   | _boolean_ | `false` |
| leftText  | 左侧文本         | _string_  | `-`     |
| rightText | 右侧文本         | _string_  | `-`     |
| leftArrow | 是否显示左侧箭头 | _boolean_ | `-`     |
| border    | 是否显示下边框   | _boolean_ | `false` |
| className | 扩展类名         | _string_  | `-`     |

## 事件

| 事件名      | 说明                 | 参数           |
| ----------- | -------------------- | -------------- |
| click-left  | 点击左侧按钮触发事件 | _event: Event_ |
| click-right | 点击右侧按钮触发事件 | _event: Event_ |
## CSS变量
