---
demo: switch
---

# Switch 开关

用于设置一个开关操作。

## 基础用法

通过 `v-model` 来绑定当前值。

:::vdemo

```vue
{demos/switch/base.vue}
```

:::

## 禁用状态

通过设置 `disabled` 属性来禁用组件。

:::vdemo

```vue
{demos/switch/disable.vue}
```

:::

## 只读状态

通过设置 `readonly` 属性来阻止组件状态改变。
:::vdemo

```vue
{demos/switch/readonly.vue}
```

:::

## 加载状态

通过设置 `loading` 属性来显示加载状态。

:::vdemo

```vue
{demos/switch/loading.vue}
```

:::

## 大小

通过设置 `size` 属性可以改变组件的大小。

:::vdemo

```vue
{demos/switch/size.vue}
```

:::

## 指定颜色

通过设置 `active-color` 和 `inactive-color` 属性可以分别定义打开和关闭时的颜色。

:::vdemo

```vue
{demos/switch/color.vue}
```

:::

## 属性

| 参数           | 说明                                       | 类型               | 默认值    |
| -------------- | ------------------------------------------ | ------------------ | --------- |
| v-model        | 绑定值                                     | _string_           | `default` |
| disabled       | 是否禁用                                   | _boolean_          | `false`   |
| readonly       | 是否为只读状态，只读状态下无法修改组件状态 | _boolean_          | `false`   |
| loading        | 是否为加载状态                             | _boolean_          | `false`   |
| size           | 组件尺寸                                   | _string \| number_ | `-`       |
| active-color   | 打开时的颜色                               | _string_           | `-`       |
| inactive-color | 关闭时的颜色                               | _string_           | `-`       |

## 事件

| 事件名 | 说明           | 回调参数 |
| ------ | -------------- | -------- |
| change | 值变化之后触发 | -        |
