---
demo: picker
---

# Picker 选择器

提供多个选项集合供用户选择，支持单列选择、多列选择和级联选择，通常与弹出层组件配合使用。

## 基础用法

Picker 组件通过 columns 属性配置选项数据。

:::vdemo

```vue
{demos/picker/basic.vue}
```
:::

## 多列选择

当 columns 为二维数组时，第二级数组为显示列。

:::vdemo

```vue
{demos/picker/muti-column.vue}
```
:::

## 级联选择

如果配置 children-field 属性 并且 columns 的选项中此字段下存在子级，那么激活级联选择。

:::vdemo

```vue
{demos/picker/child-column.vue}
```
:::

## PickerGroup 选择器组

当需要多个选择器组合使用时，可以使用 PickerGroup 组件。

:::vdemo

```vue
{demos/picker/group.vue}
```
:::

## PickerInput 选择器输入框组件

集成 Picker 组件和 Input 组件, 用于在表单中快速使用。

:::vdemo

```vue
{demos/picker/input.vue}
```
:::
## 类型

```typescript
type ValueType = string | number | boolean
type ColumnItem = {
    [key: string]: ValueType | Column;
}
type Column = ColumnItem[]
type Columns = Column[]
type PickerChange = {
    index: number;
    value: ColumnItem;
}
```
## Picker属性

| 属性名          | 类型                      | 默认值      | 说明                                 |
| :--------------     | :------------------------ | :---------- | :--------------------------------- |
| modelValue          | `string \| Array`                               | --          | 选择器选中值              |
| columns             | `Column \| Columns`                             | []          | 选择器列配置         |
| title               | `string`                                        | --          | 选择器工具栏标题            |
| valueField          | `string`                                        | value       | 选择器选项值字段        |
| textField           | `string`                                        | text       | 选择器选项文本字段        |
| childrenField       | `string`                                        | children    | 选择器选项级联字段          |
| cancelText          | `string`                                        | 取消        | 选择器工具栏取消按钮文本          |
| confirmText         | `string`                                        | 确定        | 选择器工具栏确认按钮文本   |
| optionFormatter     | `Function`                                      | --          | 选择器选项格式化函数          |
| visiableOptionCount | `number`                                        | 5           | 选择器选项可见数量              |
| showToolbar         | `boolean`                                       | true        | 选择器工具栏是否显示         |


## PickerGroup属性

| 属性名          | 类型                      | 默认值      | 说明                                 |
| :--------------     | :------------------------ | :---------- | :--------------------------------- |
| modelValue          | `string \| Array`                               | --          | 选择器选中值              |
| title               | `string`                                        | --          | 选择器工具栏标题            |
| cancelText          | `string`                                        | 取消        | 选择器工具栏取消按钮文本          |
| confirmText         | `string`                                        | 确定        | 选择器工具栏确认按钮文本   |


## PickerInput属性

| 属性名          | 类型                      | 默认值      | 说明                                 |
| :--------------     | :------------------------ | :---------- | :--------------------------------- |
| modelValue          | `string \| Array`                               | --          | 选择器选中值              |
| columns             | `Column \| Columns`                             | []          | 选择器列配置         |
| title               | `string`                                        | --          | 选择器工具栏标题            |
| valueField          | `string`                                        | value       | 选择器选项值字段        |
| textField           | `string`                                        | text       | 选择器选项文本字段        |
| childrenField       | `string`                                        | children    | 选择器选项级联字段          |
| cancelText          | `string`                                        | 取消        | 选择器工具栏取消按钮文本          |
| confirmText         | `string`                                        | 确定        | 选择器工具栏确认按钮文本   |
| optionFormatter     | `Function`                                      | --          | 选择器选项格式化函数          |
| visiableOptionCount | `number`                                        | 5           | 选择器选项可见数量              |
| showToolbar         | `boolean`                                       | true        | 选择器工具栏是否显示         |
| name                | `string`                                        | `--`          | 输入框的名称            |
| placeholder         | `string`                                        | `--`          | 输入框的提示文字        |
| readonly            | `boolean`                                       | `false`       | 输入框是否只读          |
| disabled            | `boolean`                                       | `false`       | 输入框是否禁用          |


## Picker 事件

| 事件名               | 类型                              | 说明              |
| :------------------ | :-------------------------------- | :--------------- |
| update:modelValue   | `EventEmitter<Array>`   | 值变化事件        |
| change              | `EventEmitter<PickerChange>`   | 切换选择项事件   |
| confirm              | `EventEmitter<Array>`        | 确认按钮点击事件 |
| cancel               | `EventEmitter<void>`        | 取消按钮点击事件     |


## PickerGroup 事件

| 事件名               | 类型                              | 说明              |
| :------------------ | :-------------------------------- | :--------------- |
| update:modelValue   | `EventEmitter<Array>`   | 值变化事件        |
| change              | `EventEmitter<PickerChange>`   | 切换选择项事件   |
| confirm              | `EventEmitter<Array>`        | 确认按钮点击事件 |
| cancel               | `EventEmitter<void>`        | 取消按钮点击事件     |

## PickerInput 事件

| 事件名               | 类型                              | 说明              |
| :------------------ | :-------------------------------- | :--------------- |
| update:modelValue   | `EventEmitter<string>`   | 值变化事件        |
| change              | `EventEmitter<PickerChange>`   | 切换选择项事件   |
| confirm              | `EventEmitter<Array>`        | 确认按钮点击事件 |
| cancel               | `EventEmitter<void>`        | 取消按钮点击事件     |

## CSS 变量

| 名称                        | 默认值                       | 说明 |
| :-------------------------- | :--------------------------- | :--- |
| --fm-picker-background      | var(--fm-white)            | --   |
| --fm-picker-toolbar-height  | 44px                       | --   |