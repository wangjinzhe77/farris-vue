---
demo: popup
---

# Popup 弹出框

弹出框在页面中动态弹出，覆盖在现有界面之上，用于展示信息、提示、选项菜单、确认对话框等内容，而不会打断用户的当前任务流。

## 基础用法

通过`show`属性来控制弹出框的显示与隐藏。

:::vdemo

```vue
{demos/popup/base.vue}
```

:::

## 位置

通过`position`属性来设置弹出框的位置。

:::vdemo

```vue
{demos/popup/position.vue}
```

:::

## 无遮罩

通过`overlay`属性来设置是否显示遮罩。

:::vdemo

```vue
{demos/popup/overlay.vue}
```

:::

## 属性

| 属性名              | 类型      | 默认值   | 说明               |
| :------------------ | :-------- | :------- | :----------------- |
| show                | _boolean_ | `false`  | 是否显示           |
| width               | _string_  | `--`     | 宽度               |
| height              | _string_  | `--`     | 高度               |
| position            | _string_  | `center` | 位置               |
| duration            | _number_  | `--`     | 动画持续时间       |
| zIndex              | _number_  | `--`     | 高度               |
| className           | _string_  | `--`     | 类名               |
| overlayClass        | _string_  | `--`     | 遮罩类名           |
| overlayStyle        | _string_  | `--`     | 遮罩样式           |
| transition          | _string_  | `--`     | 过渡效果           |
| useNativeBack       | _boolean_ | `false`  | 使用原生返回       |
| teleport            | _string_  | `--`     | 动画位置           |
| overlay             | _boolean_ | `true`   | 是否启用遮罩       |
| closeOnClickOverlay | _boolean_ | `true`   | 点击遮罩时是否关闭 |
| lockScroll          | _boolean_ | `true`   | 是否锁定滚动       |
| round               | _boolean_ | `false`  | 是否启用圆角       |
| useClickAway        | _boolean_ | `true`   | 外部区域事件件     |

## 事件

| 事件名      | 参数           | 说明     |
| :---------- | :------------- | :------- |
| click       | _event: Event_ | 点击事件 |
| opened      | _event: Event_ | 打开事件 |
| closed      | _event: Event_ | 关闭事件 |
| update:show | _event: Event_ | 变化事件 |

## 插槽

| 名称    | 说明 |
| :------ | :--- |
| default | 内容 |

## CSS 变量

| 名称                  | 默认值             | 说明 |
| :-------------------- | :----------------- | :--- |
| --fm-popup-background | var(--fm-white)    | `--` |
| --fm-popup-zindex     | var(--fm-zindex-3) | `--` |
| --fm-popup-radius     | 16px               | `--` |
