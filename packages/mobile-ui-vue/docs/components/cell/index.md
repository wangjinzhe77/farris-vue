---
demo: cell
---

# Cell 单元格

单元格展示一行文字标题或者描述。

## 基础用法

:::vdemo

```vue
{demos/cell/base.vue}
```

:::

## 辅助标题

通过 `label` 属性来设置辅助标题。

:::vdemo

```vue
{demos/cell/label-slot.vue}
```

:::

## 无标题

:::vdemo

```vue
{demos/cell/no-title.vue}
```

:::

## 链接样式

通过 `link` 设置链接样式。

:::vdemo

```vue
{demos/cell/link.vue}
```

:::

## 点击事件

通过 `clickable` 属性可以设置点击。

:::vdemo

```vue
{demos/cell/clickable.vue}
```

:::

## 左侧插槽

:::vdemo

```vue
{demos/cell/left-icon.vue}
```

:::

## 右侧插槽

:::vdemo

```vue
{demos/cell/right-icon.vue}
```

:::

## 垂直居中

通过 `center` 属性可以设置垂直居中。

:::vdemo

```vue
{demos/cell/center.vue}
```

:::

## 属性

| 属性名           | 类型               | 默认值   | 说明 |
| :--------------- | :----------------- | :------- | :--- |
| `type`           | `String`           | `'form'` | `--` |
| `title`          | `String`           | `--`     | `--` |
| `value`          | `[String, Number]` | `--`     | `--` |
| `label`          | `String`           | `--`     | `--` |
| `valueClass`     | `String`           | `--`     | `--` |
| `titleClass`     | `String`           | `--`     | `--` |
| `titleStyle`     | `String`           | `--`     | `--` |
| `leftIconClass`  | `String`           | `--`     | `--` |
| `rightIconClass` | `String`           | `--`     | `--` |
| `extraClass`     | `String`           | `--`     | `--` |
| `border`         | `Boolean`          | `true`   | `--` |
| `required`       | `Boolean`          | `false`  | `--` |
| `center`         | `Boolean`          | `false`  | `--` |
| `isLink`         | `Boolean`          | `false`  | `--` |
| `clickable`      | `Boolean`          | `false`  | `--` |
| `leftIcon`       | `String`           | `--`     | `--` |
| `rightIcon`      | `String`           | `--`     | `--` |
| `extra`          | `String`           | `--`     | `--` |

## 事件

| 事件名         | 说明             | 回调参数 |
| :------------- | :--------------- | :------- |
| click          | 点击单元格触发   | --       |
| leftIconClick  | 点击左侧插槽触发 | --       |
| rightIconClick | 点击左侧插槽触发 | --       |
| extraClick     | 点击额外插槽触发 | --       |

## 插槽

| 名称       | 说明           |
| :--------- | :------------- |
| default    | 单元格文本内容 |
| label      | 副标题插槽     |
| left-icon  | 左侧插槽       |
| right-icon | 右侧插槽       |
| extra      | 额外插槽       |

## CSS 变量

| 名称                          | 默认值                       | 说明 |
| :---------------------------- | :--------------------------- | :--- |
| `--fm-cell-padding`           | `10px 16px`                  | `--` |
| `--fm-cell-margin`            | `5px`                        | `--` |
| `--fm-cell-line-height`       | `24px`                       | `--` |
| `--fm-cell-font-size`         | `var(--fm-font-size)`        | `--` |
| `--fm-cell-background`        | `var(--fm-background-white)` | `--` |
| `--fm-cell-color`             | `var(--fm-text-color)`       | `--` |
| `--fm-cell-label-font-size`   | `12px`                       | `--` |
| `--fm-cell-label-line-height` | `18px`                       | `--` |
| `--fm-cell-label-color`       | `var(--fm-text-color-light)` | `--` |
| `--fm-cell-required-color`    | `var(--fm-danger-color)`     | `--` |
