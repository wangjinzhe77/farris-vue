---
demo: notify
---

# Notify 消息提示框

Notify 组件用于在页面中提示出一段信息。

## 基本用法

通过`info` `success` `danger` `warning`方法设置提示类型。

:::vdemo

```vue
{demos/notify/base.vue}
```

:::

## 自定义

传入提示文本。

:::vdemo

```vue
{demos/notify/custom.vue}
```

:::

## 类型

```typescript
type NotifyType = 'info' | 'success' | 'danger' | 'warning';
```

## 属性

| 属性名     | 类型                 | 默认值 | 说明                                               |
| :--------- | :------------------- | :----- | :------------------------------------------------- |
| type       | string as NotifyType | `info` | 类型，可选值为 `info` `success` `danger` `warning` |
| message    | string               | --     | 展示文案                                           |
| duration   | number               | `3000` | 展示时长(ms)，值为 0 时，notify 不会消失           |
| color      | string               | `#fff` | 字体颜色                                           |
| background | string               | --     | 背景颜色                                           |
| className  | any                  | --     | 自定义类名                                         |

## 方法

| 方法名           | 参数                            | 返回值      | 说明         |
| :--------------- | :------------------------------ | :---------- | :----------- |
| `Notify`         | `options            \| message` | notify 实例 | 展示提示     |
| `Notify.info `   | `options            \| message` | notify 实例 | 展示默认提示 |
| `Notify.success` | `options            \| message` | notify 实例 | 展示成功提示 |
| `Notify.error`   | `options            \| message` | notify 实例 | 展示失败提示 |
| `Notify.warning` | `options            \| message` | notify 实例 | 展示提示     |
| `Notify.clear`   | `clearAll: boolean    \| void`  | --          | 关闭提示     |

## CSS 变量

| 名称                         | 默认值           | 说明 |
| :--------------------------- | :--------------- | :--- |
| --fm-notify-color            | var(--fm-gray-5) | --   |
| --fm-notify-text-font-size   | 14px             | --   |
| --fm-notify-text-line-height | 1.4              | --   |
