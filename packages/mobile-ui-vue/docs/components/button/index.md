---
demo: button
---

# Button 按钮

按钮用于触发一个操作，如提交表单。

## 按钮类型

通过`type`设置按钮类型。按钮支持 `primary`、`secondary`、`success`、`warning`、`danger`、`info`，默认为 `primary`。

:::vdemo

```vue
{demos/button/base.vue}
```

:::

## 线框按钮

通过 `plain` 属性将按钮设置为线框按钮，线框按钮的文字为按钮颜色，背景为白色。

:::vdemo

```vue
{demos/button/plain.vue}
```

:::

## 禁用状态

通过 `disabled` 属性来禁用按钮，禁用状态下按钮不可点击。

:::vdemo

```vue
{demos/button/disabled.vue}
```

:::

## 加载中

通过 `loading` 属性来设置加载中状态。

:::vdemo

```vue
{demos/button/loading.vue}
```

:::

## 按钮形状

通过 `round` 设置圆形按钮。

:::vdemo

```vue
{demos/button/round.vue}
```

:::

## 按钮尺寸

通过 `size` 设置圆形大小。支持 `large`、`normal`、`small`、`mini` 四种尺寸，默认为 `normal`。

:::vdemo

```vue
{demos/button/size.vue}
```

:::

## 块级按钮

按钮在默认情况下为行内块级元素，通过 `block` 属性可以将按钮的元素类型设置为块级元素。

:::vdemo

```vue
{demos/button/block.vue}
```

:::

## 添加按钮

:::vdemo

```vue
{demos/button/add.vue}
```

:::

## 图标按钮

:::vdemo

```vue
{demos/button/icon.vue}
```

:::

## 自定义颜色

通过 `color` 属性可以自定义按钮的颜色。

:::vdemo

```vue
{demos/button/color.vue}
```

:::

## 类型

```typescript
type ButtonType = 'primary' | 'secondary' | 'success' | 'warning' | 'danger' | 'info';
type SizeType = 'large' | 'normal' | 'small' | 'mini';
```

## 属性

| 属性名    | 类型                   | 默认值    | 说明                                               |
| :-------- | :--------------------- | :-------- | :------------------------------------------------- |
|           |                        |           |                                                    |
| type      | _string as ButtonType_ | `default` | 类型，可选值为 `primary` `info` `warning` `danger` |
| size      | _string as SizeType_   | `normal`  | 尺寸，可选值为 `large` `small` `mini`              |
| color     | _string_               | --        | 按钮颜色，支持传入`linear-gradient`渐变色          |
| block     | _boolean_              | `false`   | 是否为块级元素                                     |
| plain     | _boolean_              | `false`   | 是否为朴素按钮                                     |
| round     | _boolean_              | `false`   | 是否为圆形按钮                                     |
| disabled  | _boolean_              | `false`   | 是否禁用按钮                                       |
| className | _string_               | --        | 自定义类名                                         |
| no-border | _boolean_              | --        | 是否显示描边                                       |

## 事件

| 事件名  | 说明         | 回调参数 |
| :------ | :----------- | :------- |
| onClick | 点击按钮触发 | --       |

## 插槽

| 名称    | 说明         |
| :------ | :----------- |
| default | 按钮文本内容 |

## CSS 变量

| 名称                           | 默认值                  | 说明 |
| :----------------------------- | :---------------------- | :--- |
| `--fm-button-height`           | `42px`                  | --   |
| `--fm-button-padding`          | `0 15px`                | --   |
| `--fm-button-radius`           | `2px`                   | --   |
| `--fm-button-border-width`     | `1px`                   | --   |
| `--fm-button-color`            | `var(--fm-white)`       | --   |
| `--fm-button-line-height`      | `var(--fm-line-height)` | --   |
| `--fm-button-font-size`        | `var(--fm-font-size)`   | --   |
| `--fm-button-plain-background` | `var(--fm-white)`       | --   |
| `--fm-button-lg-height`        | `46px`                  | --   |
| `--fm-button-lg-font-size`     | `18px`                  | --   |
| `--fm-button-md-height`        | `38px`                  | --   |
| `--fm-button-md-font-size`     | `14px`                  | --   |
| `--fm-button-sm-height`        | `34px`                  | --   |
| `--fm-button-sm-padding`       | `0 8px`                 | --   |
| `--fm-button-sm-font-size`     | `12px`                  | --   |
| `--fm-button-xs-height`        | `28px`                  | --   |
| `--fm-button-xs-font-size`     | `10px`                  | --   |
| `--fm-button-secondary-color`  | `var(--fm-blue-light)`  | --   |
