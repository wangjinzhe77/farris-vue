---
demo: list-view
---

# ListView 列表视图

ListView 组件用于以摘要信息的形式展示列表数据。

## 基础用法

通过`data`属性传入列表数据，可以通过`item`插槽定制每条数据的显示效果，如果没有`item`插槽则默认显示数据的文本字段的内容。<br />
当列表滚动到底部时，将触发`load`事件，用于加载更多数据，可以通过`listProps`透传其它的列表组件属性。<br />
通过`enable-pull-refresh`属性可以启用下拉刷新，然后通过`refresh`事件和`v-model:refreshing`属性来监听下拉刷新事件并控制刷新状态。<br />
通过`enable-multi-select`属性可以启用多选功能，`v-model:multiSelect`属性表示当前是否处于多选状态，用户长按列表条目时将使得列表切换到多选状态。<br />
通过`multi-select-toolbar-items`属性可以定制多选状态下的按钮工具栏。<br />
通过`fill`属性可以使列表内容占满高度。

:::vdemo

```vue
{demos/list-view/base.vue}
```

:::

## 层级列表

通过`child-field`属性可以指定列表数据的子级条目字段，需要注意的是，只支持一层子级。

:::vdemo

```vue
{demos/list-view/children.vue}
```

:::

## 类型

```typescript
interface ListViewToolbarItem {
  /** 按钮文本 */
  text: string;

  /** 点击事件回调方法 */
  handler: (item: ListViewToolbarItem) => void;

  /** 按钮样式类型 */
  type?: string;

  /** 是否禁用 */
  disabled?: boolean;
}
```

## 属性

| 属性名                  | 类型                             | 默认值               | 说明                                                                                                                                |
| :---------------------- | :------------------------------- | :------------------- | :---------------------------------------------------------------------------------------------------------------------------------- |
| data                    | `any[]`                          | []                   | 列表数据                                                                                                                            |
| enableMultiSelect       | `boolean`                        | false                | 是否启用多选功能，默认不启用                                                                                                        |
| multiSelect             | `boolean`                        | false                | 是否处于多选状态，支持语法糖`v-model:multiSelect`，在非多选状态下且`enableMultiSelect=true`时，用户长按任意列表条目将切换至多选状态 |
| multiSelectToolbarItems | `Array as ListViewToolbarItem[]` | []                   | 多选状态下，如果本属性非空则在列表下方显示一个按钮工具栏                                                                            |
| idField                 | `string`                         | 'id'                 | ID 字段的编号，ID 字段用于唯一标识一条列表数据                                                                                      |
| textField               | `string`                         | 'name'               | 文本字段的编号                                                                                                                      |
| disableField            | `string`                         | 'disabled'           | 禁用字段的编号，如果列表条目数据的禁用字段为真则列表条目禁用勾选                                                                    |
| childField              | `string`                         | ''                   | 子级条目字段的编号，如果一条列表数据的子级条目字段非空，则该条数据以分组的形式渲染                                                  |
| selectedValues          | `string[]`                       | []                   | 已勾选条目的 ID 数组                                                                                                                |
| loading                 | `boolean`                        | false                | 是否处于加载状态，支持语法糖`v-model:loading`，此状态下将在列表底部显示加载中提示文本                                               |
| error                   | `boolean`                        | false                | 是否处于加载失败状态，支持语法糖`v-model:error`，此状态下将在列表底部显示加载失败提示文本，用户点击提示将触发加载事件               |
| finished                | `boolean`                        | false                | 是否加载完成，加载完成后将在列表下方显示加载完成提示文本，且不会再触发加载事件                                                      |
| listProps               | `Object as ListProps`            | {}                   | 列表属性（除了`loading`、`error`、`finished`之外的其它列表组件属性）                                                                |
| enablePullRefresh       | `boolean`                        | false                | 是否启用下拉刷新                                                                                                                    |
| refreshing              | `boolean`                        | false                | 是否正在刷新，支持语法糖`v-model:refreshing`，`enablePullRefresh`为真时有效                                                         |
| pullRefreshProps        | `Object as PullRefreshProps`     | {}                   | 下拉刷新属性（除了`modelValue`之外的其它下拉刷新组件属性），`enablePullRefresh`为真时有效                                           |
| fill                    | `boolean`                        | false                | 是否占满高度                                                                                                                        |
| split                   | `boolean`                        | false                | 是否显示分隔线                                                                                                                      |
| emptyMessage            | `string`                         | ''                   | 列表数据为空时显示的占位文本                                                                                                        |
| checkboxProps           | `Object as CheckboxProps`        | \{ shape: 'round' \} | 多选状态下，勾选框的属性                                                                                                            |

## 事件

| 事件名          | 类型                                         | 说明                                                   |
| :-------------- | :------------------------------------------- | :----------------------------------------------------- |
| load            | `EventEmitter<void>`                         | 加载更多事件，列表滚动到底部时触发                     |
| clickItem       | `EventEmitter<{ data: any; index: number }>` | 列表条目点击事件，点击列表条目时触发                   |
| selectionChange | `EventEmitter<any[]>`                        | 多选变化事件，多选状态下，多选或取消勾选列表条目时触发 |
| refresh         | `EventEmitter<void>`                         | 下拉刷新事件，下拉刷新时触发                           |

## 插槽

| 名称                | 说明                                     |
| :------------------ | :--------------------------------------- |
| item                | 列表条目                                 |
| itemGroupHeader     | 层级列表的分组标题                       |
| toolbar             | 多选状态下，下方的按钮工具栏             |
| empty               | 列表内容为空时的提示                     |
| header              | 列表头部，显示在列表内容和各种提示的上方 |
| footer              | 列表尾部，显示在列表内容和各种提示的下方 |
| finished            | 加载完成提示                             |
| loading             | 加载中提示                               |
| error               | 加载失败提示                             |
| loadMore            | 加载更多提示                             |
| pullRefreshPulling  | 下拉刷新，下拉状态下的提示               |
| pullRefreshLoosing  | 下拉刷新，释放状态下的提示               |
| pullRefreshLoading  | 下拉刷新，加载状态下的提示               |
| pullRefreshComplete | 下拉刷新，刷新完成后的提示               |
| default             | 默认内容                                 |

## 组件实例

| 名称             | 类型        | 说明                                                                             |
| :--------------- | :---------- | :------------------------------------------------------------------------------- |
| check            | () => void  | 如果列表已经滚动到底部或者因为内容太少而没有滚动条，则调用本方法将触发`load`事件 |
| getSelectedItems | () => any[] | 获取当前被勾选的数据                                                             |
| clearSelection   | () => void  | 取消全部数据的勾选                                                               |

## CSS 变量

| 名称                                   | 默认值                       | 说明 |
| :------------------------------------- | :--------------------------- | :--- |
| --fm-listview-checkbox-container-width | 40px                         | --   |
| --fm-listview-toolbar-height           | 42px                         | --   |
| --fm-listview-split-line-color         | rgb(230, 235, 235)           | --   |
| --fm-listview-empty-text-color         | var(--fm-gray-5)             | --   |
| --fm-listview-empty-text-font-size     | var(--fm-font-size-md, 14px) | --   |
| --fm-listview-empty-text-line-height   | 50px                         | --   |
