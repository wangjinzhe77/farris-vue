---
demo: date-picker
---

# DatePicker 日期选择器

基于Picker 封装，用于选择日期、年月、年份。

## 基础用法


:::vdemo

```vue
{demos/date-picker/basic.vue}
```
:::

## 显示类型

日期选择器`type`属性用于设置日期选择器的显示类型，支持选择年月日、年月、年份。默认为年月日选择。

:::vdemo

```vue
{demos/date-picker/type.vue}
```
:::

## 最大最小值

默认情况下，日期选择器可以选择当前值前十年到未来十年时间。`min-date`用于自定义设置最小值，`max-date`用于自定义设置最大值。

:::vdemo

```vue
{demos/date-picker/min-max.vue}
```
:::

## 自定义显示选项内容

`option-formatter` 属性用来设置列选项的显示内容, 支持传入一个函数，函数参数为当前列及选项值，返回值会作为列选项的显示内容。

:::vdemo

```vue
{demos/date-picker/formatter.vue}
```
:::

## DatePickerInput 日期选择器输入框

集成 DatePicker 组件和 Input 组件, 用于在表单中快速使用。

:::vdemo

```vue
{demos/date-picker/input.vue}
```
:::

## 类型

```typescript
type DatePickerType = "year" | "year-month" | "year-month-day" | "date"
```

## DatePicker 属性

| 属性名          | 类型                                | 默认值      | 说明                          |
| :-------------- | :------------------------           | :---------- | :-------------------------------------- |
| modelValue       | `Date`                              | --          | 日期选择器当前值              |
| type             | `DatePickerType`      | 'date'     | 日期选择器显示类型                  |
| min-date         | `Date`                              | --          | 日期选择器选择范围最小值            |
| max-date          | `Date`                              | --          | 日期选择器选择范围最大值                               |
| option-formatter | `(value: number) => string`         | --          | 自定义显示选项内容          |
| format           | `string`                          | --          | 日期选择器显示格式               |
| visiableOptionCount | `number`                       | 5           | 日期选择器选项可见数量              |
| showToolbar         | `boolean`                           | true        | 日期选择器工具栏是否显示         |
| title             | `string`                           | --          | 日期选择器工具栏标题               |

## DatePickerInput 属性

| 属性名          | 类型                                | 默认值      | 说明                          |
| :-------------- | :------------------------           | :---------- | :-------------------------------------- |
| modelValue       | `Date`                              | --          | 日期选择器当前值              |
| type             | `DatePickerType`      | 'date'     | 日期选择器显示类型                  |
| min-date         | `Date`                              | --          | 日期选择器选择范围最小值            |
| max-date          | `Date`                              | --          | 日期选择器选择范围最大值                               |
| option-formatter | `(value: number) => string`         | --          | 自定义显示选项内容          |
| format           | `string`                          | --          | 日期选择器显示格式               |
| visiableOptionCount | `number`                       | 5           | 日期选择器选项可见数量              |
| showToolbar         | `boolean`                           | true        | 日期选择器工具栏是否显示         |
| title             | `string`                           | --          | 日期选择器工具栏标题               |
| name                | `string`                                        | --         | 输入框的名称            |
| placeholder         | `string`                                        | --          | 输入框的提示文字        |
| readonly            | `boolean`                                       | false       | 输入框是否只读          |
| disabled            | `boolean`                                       | false       | 输入框是否禁用          |
| formatter           | `(value: string \| number) => string \| number` | --          | 格式化函数              |

## DatePicker & DatePickerInput 事件

| 事件名               | 类型                              | 说明              |
| :------------------ | :-------------------------------- | :--------------- |
| update:modelValue   | `EventEmitter<Date>`   | 值变化事件        |
| change              | `EventEmitter<Date>`   | 切换选择项事件       |
| confirm             | `EventEmitter<Date>`        |      确认按钮点击事件 |

