---
demo: icon
---

# Icon 图标

基于字体的图标集，可以通过 Icon 组件使用，也可以在其他组件中通过 icon 属性引用。

## 基础用法

Icon 的 `name` 属性支持传入图标名称或图片链接，所有可用的图标名称见`图标库`。

:::vdemo

```vue
{demos/icon/base.vue}
```

:::

## 图标颜色

Icon 的 `color` 属性用来设置图标的颜色。

:::vdemo

```vue
{demos/icon/color.vue}
```

:::

## 图标大小

Icon 的 `size` 属性用来设置图标的尺寸大小，默认单位为 px。

:::vdemo

```vue
{demos/icon/size.vue}
```

:::

## 图标库

所有可用的图标名称见右侧示例。
:::vdemo

```vue
{demos/icon/icon-factory.vue}
```

:::

## 属性

| 属性名       | 类型               | 默认值    | 说明                         |
| :----------- | :----------------- | :-------- | :--------------------------- |
| name         | _string_           | --        | 图标名称                     |
| color        | _string_           | `inherit` | 图标颜色                     |
| size         | _number \| string_ | `inherit` | 图标大小                     |
| class-prefix | _string_           | `fm-icon` | 类名前缀，用于使用自定义图标 |

## 事件

| 事件名 | 参数           | 说明           |
| :----- | :------------- | :------------- |
| click  | _event: Event_ | 点击图标时触发 |

## CSS 变量

| 名称                | 默认值 | 说明     |
| :------------------ | :----- | :------- |
| --fm-icon-font-size | 14px   | 图标大小 |
