---
demo: rate
---

# Rate 评分

### 介绍

Rate 组件用来对事物进行打分。

## 基础用法

通过`v-model`双向绑定评分值。

:::vdemo

```vue
{demos/rate/base.vue}
```

:::

## 自定义样式

通过`size`属性可以设置图标大小，通过`color`和`void-color`属性可以分别设置选中时和未选中时的颜色，通过`icon`和`void-icon`属性可以分别设置选中时和未选中时的图标。

:::vdemo

```vue
{demos/rate/custom.vue}
```

:::

## 可选半星

设置`allow-half`属性，可以选择半颗星。

:::vdemo

```vue
{demos/rate/half.vue}
```

:::

## 自定义数量

通过`count`属性可以自定义星星的总数。

:::vdemo

```vue
{demos/rate/count.vue}
```

:::

## 可清空

设置`clearable`属性后，重复选中相同的评分将清空评分。

:::vdemo

```vue
{demos/rate/clearable.vue}
```

:::

## 禁用状态

通过`disabled`属性可以使组件处于禁用状态，不可编辑。

:::vdemo

```vue
{demos/rate/disabled.vue}
```

:::

## 只读状态

通过`readonly`属性可以使组件处于只读状态，不可编辑。

:::vdemo

```vue
{demos/rate/readonly.vue}
```

:::

## 在只读状态显示小数

如果`readonly`且`allow-half`，则组件可以显示小数评分。

:::vdemo

```vue
{demos/rate/decimal.vue}
```

:::

## 属性

| 属性名        | 类型               | 默认值     | 说明                                                 |
| :------------ | :----------------- | :--------- | :--------------------------------------------------- |
| modelValue    | `number`           | 0          | 当前评分，支持语法糖`v-model`                        |
| size          | `number \| string` | --         | 图标大小，单位`px`                                   |
| clearable     | `boolean`          | false      | 是否可清空，如果为真，则再次点击相同的分数时清空评分 |
| readonly      | `boolean`          | false      | 是否只读                                             |
| disabled      | `boolean`          | false      | 是否禁用                                             |
| allowHalf     | `boolean`          | false      | 是否可选半颗星                                       |
| icon          | `string`           | 's-star'   | 选中时的图标名称                                     |
| voidIcon      | `string`           | 's-star-o' | 未选中时的图标名称                                   |
| color         | `string`           | --         | 选中时的填充颜色                                     |
| voidColor     | `string`           | --         | 未选中时的填充颜色                                   |
| disabledColor | `string`           | --         | 禁用时的填充颜色                                     |
| count         | `number \| string` | 5          | 图标总数量，默认为 5 个                              |
| gutter        | `number \| string` | --         | 图标之间的间距，单位`px`                             |
| touchable     | `boolean`          | true       | 是否可以通过滑动改变评分                             |
| iconPrefix    | `string`           | --         | 图标名称的前缀                                       |

## 事件

| 事件名 | 类型                   | 说明                           |
| :----- | :--------------------- | :----------------------------- |
| change | `EventEmitter<number>` | 评分变更事件，评分值变化时触发 |

## CSS 变量

| 名称                          | 默认值                 | 说明 |
| :---------------------------- | :--------------------- | :--- |
| --fm-rate-icon-size           | 20px                   | --   |
| --fm-rate-icon-gutter         | var(--fm-padding-base) | --   |
| --fm-rate-icon-void-color     | var(--fm-gray-4)       | --   |
| --fm-rate-icon-full-color     | var(--fm-orange-3)     | --   |
| --fm-rate-icon-disabled-color | var(--fm-gray-4)       | --   |
