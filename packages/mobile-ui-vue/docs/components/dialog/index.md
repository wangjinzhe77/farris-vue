---
demo: dialog
---

# Dialog 对话框

用于显示关键信息、警告、确认操作或是提供额外的用户交互空间，同时暂时阻断用户与界面其他部分的交互，以确保用户聚焦于当前的对话内容。

## 基础用法

:::vdemo

```vue
{demos/dialog/base.vue}
```

:::

## 含关闭按钮

:::vdemo

```vue
{demos/dialog/buttons.vue}
```

:::

## 组件调用-带头像

:::vdemo

```vue
{demos/dialog/avatar.vue}
```

:::

## 组件调用-按钮竖向排列

:::vdemo

```vue
{demos/dialog/button-layout.vue}
```

:::

## 组件调用-使用插槽

:::vdemo

```vue
{demos/dialog/slots.vue}
```

:::

## 属性

| 属性名              | 类型      | 默认值   | 说明             |
| :------------------ | :-------- | :------- | :--------------- |
| show                | _boolean_ | `false`  | 显示             |
| title               | string    | `--`     | 标题             |
| message             | string    | `--`     | 消息文本         |
| messageAlign        | string    | `center` | 消息方向         |
| overlay             | _boolean_ | `true`   | 是否遮罩         |
| closeOnClickOverlay | _boolean_ | `true`   | 点击遮罩时关闭   |
| lockScroll          | _boolean_ | `true`   | 是否锁定滚动     |
| teleport            | string    | `--`     | 位置             |
| buttonLayout        | string    | `--`     | 按钮边框         |
| buttons             | _array_   | `[]`     | 按钮组           |
| className           | string    | `--`     | 样式名           |
| showClose           | _boolean_ | `false`  | 显示关闭按钮     |
| content             | Function  | `--`     | 内容             |
| useClickAway        | _boolean_ | `true`   | 点击外部区域关闭 |

## 事件

| 事件名      | 参数           | 说明     |
| :---------- | :------------- | :------- |
| open        | _event: Event_ | 打开事件 |
| close       | _event: Event_ | 关闭事件 |
| update:show | _event: Event_ | 变化事件 |

## 插槽

| 名称    | 说明     |
| :------ | :------- |
| default | 内容     |
| header  | 头部内容 |
| footer  | 尾部内容 |

## CSS 变量

| 名称                            | 默认值           | 说明 |
| :------------------------------ | :--------------- | :--- |
| --fm-dialog-background          | 24px             | `--` |
| --fm-dialog-padding-top         | 16px             | `--` |
| --fm-dialog-padding-left        | 17px             | `--` |
| --fm-dialog-header-font-size    | 24px             | `--` |
| --fm-dialog-header-line-height  | 15px             | `--` |
| --fm-dialog-content-line-height | 20px             | `--` |
| --fm-dialog-footer-height       | 50px             | `--` |
| --fm-border-color               | var(--fm-gray-4) | `--` |
