---
demo: overlay
---

# Overlay 遮罩

弹出遮罩区域,阻止用户与界面元素进行交互。

## 基础用法

通过 `show` 属性控制显示或隐藏。

:::vdemo

```vue
{demos/overlay/base.vue}
```

:::

## 动画持续时间

通过 `duration` 属性设置动画持续时间。

:::vdemo

```vue
{demos/overlay/duration.vue}
```

:::

## 高度

通过 `zIndex` 属性设置高度。
:::vdemo

```vue
{demos/overlay/z-index.vue}
```

:::

## 属性

| 属性名     | 类型      | 默认值  | 说明         |
| :--------- | :-------- | :------ | :----------- |
| show       | _boolean_ | `false` | 是否显示     |
| zIndex     | _number_  | `--`    | 高度         |
| duration   | _number_  | `--`    | 动画持续时间 |
| className  | _string_  | `--`    | 类名         |
| style      | _string_  | `false` | 样式         |
| lockScroll | _boolean_ | `--`    | 是否锁定滚动 |

## 事件

| 事件名   | 参数           | 说明 |
| :------- | :------------- | :--- |
| touchend | _event: Event_ | `--` |

## 插槽

| 名称    | 说明 |
| :------ | :--- |
| default | 内容 |

## CSS 变量

| 名称                    | 默认值             | 说明 |
| :---------------------- | :----------------- | :--- |
| --fm-overlay-background | rgba(0, 0, 0, 0.4) | `--` |
| --fm-overlay-zindex     | 98                 | `--` |
