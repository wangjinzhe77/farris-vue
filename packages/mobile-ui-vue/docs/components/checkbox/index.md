---
demo: checkbox
---

# Checkbox Group 复选框

Checkbox 组件为不同使用场景提供了多种展示样式。

## 基础用法

:::vdemo

```vue
{demos/checkbox/basic.vue}
```
:::

## 只读

通过`readonly` 属性设置复选框的是否只读。默认为否。

:::vdemo

```vue
{demos/checkbox/readonly.vue}
```
:::

## 禁用

通过`disabled` 属性设置复选框的是否禁用。默认为否。

:::vdemo

```vue
{demos/checkbox/disabled.vue}
```
:::

## 复选组

:::vdemo

```vue
{demos/checkbox/group.vue}
```
:::

## 复选组-横向排列

通过`direction` 属性设置复选组的排列方向。

:::vdemo

```vue
{demos/checkbox/group-direction.vue}
```
:::

## 复选组-按钮模式

:::vdemo

```vue
{demos/checkbox/group-button.vue}
```
:::

## Checkbox属性

| 属性名       | 类型                                       | 默认值      | 说明       |
| :----------  | :------------------------------------------ | :-------- | :------------ |
| label        | _string_                                    | --        | 标签名         |
| disabled     | _boolean_                                   | false     | 禁用        |
| readonly     | _boolean_                                   | false     | 只读          |
| shape        | _CheckboxShape_                             | square    | 选择框形状    |
| type         | _CheckboxType_                              | check     | 选择框类型      |
| labelLimit   | _number_                                    | --        | 标签最大长度      |

## Checkbox事件

| 事件名               | 类型                              | 说明              |
| :------------------ | :-------------------------------- | :--------------- |
| update:modelValue   | `EventEmitter<boolean>`           | 值变化事件        |
| change              | `EventEmitter<boolean>`           | 值变化事件   |

## CheckboxGroup属性

| 属性名       | 类型                                       | 默认值      | 说明       |
| :----------  | :------------------------------------------ | :-------- | :------------ |
| modelValue   |  _string \| string[]_                       | --        | 绑定值         |
| disabled     | _boolean_                                   | false     | 禁用        |
| readonly     | _boolean_                                   | false     | 只读          |
| shape        | _CheckboxShape_                             | square    | 复选组形状    |
| type         | _CheckboxType_                              | check     | 复选组类型      |
| direction    | _string_                                    | vertical  | 复选组排列方向      |
| items        | _CheckboxItem[]_                            | --        | 支持使用对象动态渲染复选框      |
| valueField   | _string_                                    | value     | 动态渲染复选框时数组对象值字段      |
| textField    | _string_                                    | text      | 动态渲染复选框时数组对象文本字段      |

## CheckboxGroup事件

| 事件名               | 类型                              | 说明              |
| :------------------ | :-------------------------------- | :--------------- |
| update:modelValue   | `EventEmitter<string \| any[]>`           | 值变化事件        |


## CheckboxGroupItem属性

| 属性名       | 类型                                       | 默认值      | 说明       |
| :----------  | :------------------------------------------ | :-------- | :------------ |
| name         | _string \| number_                          | --        | 标识，配合CheckboxGroup时使用         |
| label        | _string_                                    | --        | 标签名         |
| disabled     | _boolean_                                   | false     | 禁用        |
| readonly     | _boolean_                                   | false     | 只读          |
| shape        | _CheckboxShape_                             | square    | 选择框形状    |
| type         | _CheckboxType_                              | check     | 选择框类型      |
| labelLimit   | _number_                                    | --        | 标签最大长度      |

## CheckboxGroupItem事件

| 事件名               | 类型                              | 说明              |
| :------------------ | :-------------------------------- | :--------------- |
| update:modelValue   | `EventEmitter<boolean>`           | 值变化事件        |

## CSS 变量

```css
:root {
  --fm-checkbox-color: var(--fm-text-color);
  --fm-checkbox-font-size: var(--fm-font-size);
  --fm-checkbox-icon-color: var(--fm-text-color-light);
  --fm-checkbox-icon-background: var(--fm-background-white);
  --fm-checkbox-icon-radius: var(--fm-radius-md);
  --fm-checkbox-checked-icon-color: var(--fm-primary-color);
  --fm-checkbox-button-background: var(--fm-gray-2);
  --fm-checkbox-disabled-color: var(--fm-disabled-color);
  --fm-checkbox-disabled-icon-color: var(--fm-gray-1);
}
```