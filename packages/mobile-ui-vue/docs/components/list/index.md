---
demo: list
---

# List 列表

Loading 组件用于展示长列表，当列表滚动到底部时，可以触发加载事件。

## 基础用法

当列表滚动到底部时，将触发`load`事件并将`loading`设置为`true`。`loading=true`表示列表处于加载中状态，此时将在列表底部显示加载中提示且不会再重复触发`load`事件，所以当数据更新完毕后，还需要将`loading`设为`false`。如果已加载完全部数据，则应该将`finished`设为`true`，`finished=true`时将不再触发`load`事件并在列表底部显示加载完成提示。

:::vdemo

```vue
{demos/list/base.vue}
```

:::

## 错误提示

如果加载时出现了错误，可以将`error`设置为`true`，此时将在列表底部显示加载错误提示，如果用户点击错误提示则将触发`load`事件并将`error`设置为`false`。

:::vdemo

```vue
{demos/list/error.vue}
```

:::

## 手动触发加载

如果将`auto-load-more`设置为`false`，则不会在列表滚动到底部时触发`load`事件了，而是会在列表底部显示一个加载更多提示，用户点击加载更多提示后才会触发`load`事件。

:::vdemo

```vue
{demos/list/manual.vue}
```

:::

## 类型

```typescript
type ListDirection = 'up' | 'down';
```

## 属性

| 属性名          | 类型                      | 默认值      | 说明                                                                                                                  |
| :-------------- | :------------------------ | :---------- | :-------------------------------------------------------------------------------------------------------------------- |
| loading         | `boolean`                 | false       | 是否处于加载状态，支持语法糖`v-model:loading`，此状态下将在列表底部显示加载中提示文本                                 |
| error           | `boolean`                 | false       | 是否处于加载失败状态，支持语法糖`v-model:error`，此状态下将在列表底部显示加载失败提示文本，用户点击提示将触发加载事件 |
| finished        | `boolean`                 | false       | 是否加载完成，加载完成后将在列表下方显示加载完成提示文本，且不会再触发加载事件                                        |
| autoLoadMore    | `boolean`                 | true        | 是否自动触发加载事件，默认为`true`，当用户滑动到列表底部时，自动触发加载事件                                          |
| offset          | `number`                  | 30          | 滚动条与底部的距离小于等于`offset`（且`autoLoadMore=true`）时自动触发加载事件                                         |
| immediateCheck  | `boolean`                 | true        | 是否在初始化时立即检查自动触发加载事件的条件，满足则自动触发加载事件                                                  |
| disabled        | `boolean`                 | false       | 是否禁用，禁用后不再触发加载事件                                                                                      |
| direction       | `string as ListDirection` | 'down'      | 默认情况下滚动到底部触发加载，如果将本属性设为`up`则滚动到顶部时触发加载                                              |
| loadMoreText    | `string`                  | '加载更多'  | 加载更多提示文本                                                                                                      |
| loadingText     | `string`                  | '加载中...' | 加载中提示文本                                                                                                        |
| finishedText    | `string`                  | --          | 加载完成提示文本                                                                                                      |
| errorText       | `string`                  | --          | 加载失败提示文本                                                                                                      |
| showFinishedTip | `boolean`                 | true        | 是否显示加载完成提示文本，如果为`false`则忽略`finishedText`属性和`finished`插槽                                       |

## 事件

| 事件名 | 类型                                                            | 说明                                                                                                                            |
| :----- | :-------------------------------------------------------------- | :------------------------------------------------------------------------------------------------------------------------------ |
| load   | `EventEmitter<void>`                                            | 加载更多事件，滚动条与底部的距离小于等于`offset`（且`autoLoadMore=true`）时自动触发，或者在用户点击错误提示或加载更多提示时触发 |
| scroll | `EventEmitter<{ topDistance: number; bottomDistance: number }>` | 列表滚动事件，列表滚动时触发，`topDistance`是与滚动容器顶部的距离，`bottomDistance`是与滚动容器底部的距离                       |

## 插槽

| 名称     | 说明                                     |
| :------- | :--------------------------------------- |
| default  | 列表内容                                 |
| header   | 列表头部，显示在列表内容和各种提示的上方 |
| footer   | 列表尾部，显示在列表内容和各种提示的下方 |
| finished | 加载完成提示                             |
| loading  | 加载中提示                               |
| error    | 加载失败提示                             |
| loadMore | 加载更多提示                             |

## 组件实例

| 名称  | 类型       | 说明                                                                                                                                                             |
| :---- | :--------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| check | () => void | 如果列表已经滚动到底部或者因为内容太少而没有滚动条，则调用本方法将触发`load`事件，前提是：`autoLoadMore=true`且`finished=false`且`disabled=false`且`error=false` |

## CSS 变量

| 名称                        | 默认值                       | 说明 |
| :-------------------------- | :--------------------------- | :--- |
| --fm-list-text-color        | var(--fm-gray-5)             | --   |
| --fm-list-text-font-size    | var(--fm-font-size-md, 14px) | --   |
| --fm-list-text-line-height  | 50px                         | --   |
| --fm-list-loading-icon-size | 16px                         | --   |
