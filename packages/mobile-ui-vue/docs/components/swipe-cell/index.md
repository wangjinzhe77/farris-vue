---
demo: swipe-cell
---

# SwipeCell 滑动单元格

SwipeCell 组件通过左右滑动来展示单元格的更多操作按钮。

## 基础用法

通过`left-buttons`和`right-buttons`属性可以定义左右两侧滑动区域中的按钮组。按钮的高度与内容的高度相等。

:::vdemo

```vue
{demos/swipe-cell/base.vue}
```

:::

## 自定义两侧内容

通过`left`和`right`插槽可以自定义左右两侧滑动区域的内容。

:::vdemo

```vue
{demos/swipe-cell/custom.vue}
```

:::

## 异步关闭

如果将`close-on-click-button`属性设置为`false`，则点击两侧区域后不会自动归位，可以在异步操作完成之后通过组件的实例方法`close`使单元格归位。

:::vdemo

```vue
{demos/swipe-cell/async-close.vue}
```

:::

## 类型

```typescript
interface SwipeCellButton {
  /** 按钮文本 */
  text?: string;

  /** 图标 */
  icon?: string | VNode;

  /** 自定义类名 */
  className?: string;

  /** 自定义样式 */
  style?: string;

  /** 点击事件回调方法 */
  onClick?: (close: () => void) => void;

  [key: string]: any;
}

type SwipeCellSide = 'left' | 'right';

type SwipeCellClickPosition = 'left' | 'right' | 'cell' | 'outside';
```

## 属性

| 属性名                   | 类型                         | 默认值 | 说明                                                        |
| :----------------------- | :--------------------------- | :----- | :---------------------------------------------------------- |
| leftButtons              | `Array as SwipeCellButton[]` | []     | 左侧的按钮列表                                              |
| rightButtons             | `Array as SwipeCellButton[]` | []     | 右侧的按钮列表                                              |
| closeOnClickButton       | `boolean`                    | true   | 是否在点击按钮时自动归位，`left`和`right`插槽同样被视为按钮 |
| closeOnTouchOutside      | `boolean`                    | true   | 是否在触摸其它区域时自动归位                                |
| closeOnClickCell         | `boolean`                    | true   | 是否在点击中间内容区域时自动归位                            |
| disabled                 | `boolean`                    | false  | 是否禁用滑动                                                |
| transitionDuration       | `number`                     | 500    | 过渡动画的执行时长，单位`ms`                                |
| threshold                | `number`                     | 0.2    | 触发自动滑出/关闭的滑动距离百分比阈值，取值范围`(0, 1)`     |
| touchMoveStopPropagation | `boolean`                    | false  | 是否阻止滑动事件冒泡                                        |

## 事件

| 事件名 | 类型                                                                           | 说明                                                              |
| :----- | :----------------------------------------------------------------------------- | :---------------------------------------------------------------- |
| click  | `EventEmitter<{ position: SwipeCellClickPosition; button: SwipeCellButton }>`  | 点击事件，点击时触发，如果被点击的是两侧的按钮则`button`非空      |
| open   | `EventEmitter<SwipeCellSide>`                                                  | 打开事件，打开时触发                                              |
| close  | `EventEmitter<{ side: SwipeCellSide; clickPosition: SwipeCellClickPosition }>` | 关闭事件，归位时触发，如果关闭是由点击导致的则`clickPosition`非空 |

## 插槽

| 名称    | 说明               |
| :------ | :----------------- |
| default | 自定义内容         |
| left    | 左侧滑动区域的内容 |
| right   | 右侧滑动区域的内容 |

## 组件实例

| 名称  | 类型                          | 说明                 |
| :---- | :---------------------------- | :------------------- |
| open  | (side: SwipeCellSide) => void | 打开指定侧的滑动区域 |
| close | () => void                    | 收起滑动区域         |

## CSS 变量

| 名称                              | 默认值               | 说明 |
| :-------------------------------- | :------------------- | :--- |
| --fm-swipe-cell-button-text-color | var(--fm-white)      | --   |
| --fm-swipe-cell-button-bg-color   | var(--fm-gray-4)     | --   |
| --fm-swipe-cell-button-font-size  | var(--fm-font-size)  | --   |
| --fm-swipe-cell-button-icon-size  | var(--fm-font-size)  | --   |
| --fm-swipe-cell-button-padding    | var(--fm-padding-md) | --   |
