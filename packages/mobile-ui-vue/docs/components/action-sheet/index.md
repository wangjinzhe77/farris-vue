---
demo: action-sheet
---

# ActionSheet 动作面板

ActionSheet 组件是从页面底部弹出的模态框，为用户呈现一组与当前情境相关的选项。

## 基础用法

通过`v-model:show`控制弹框的显示隐藏，通过`items`属性定义一组选项。<br />
如果`close-on-click-action`属性为真，则用户点击选项后自动关闭弹框。<br />
通过选项的`icon`属性可以设置选项的图标。<br />
通过`description`属性可以在面板上方显示一行描述文本，通过选项的`subTitle`属性可以为选项设置副标题。<br />
如果将`alignment`属性设置为`left`则文本居左对齐。<br/>
如果将`show-cancel`属性设置为`false`则可以将面板下方的取消按钮隐藏。

:::vdemo

```vue
{demos/action-sheet/base.vue}
```

:::

## 选项状态

当选项的`disabled`属性为真时，选项被禁用。

:::vdemo

```vue
{demos/action-sheet/status.vue}
```

:::

## 自定义插槽

通过`item`插槽可以自定义选项的显示效果，通过默认插槽可以自定义整个面板的显示效果。

:::vdemo

```vue
{demos/action-sheet/custom.vue}
```

:::

## 类型

```typescript
/** 动作面板选项 */
interface ActionSheetItem {
  /** 选项标题 */
  title?: string;

  /** 选项副标题，显示在标题下方 */
  subTitle?: string;

  /** 图标名，图标显示在标题左侧 */
  icon?: string;

  /** 自定义颜色 */
  color?: string;

  /** 是否处于禁用状态 */
  disabled?: boolean;

  /** 自定义类名 */
  className?: string;

  [key: string]: any;
}
```

## 属性

| 属性名              | 类型                         | 默认值   | 说明                                       |
| :------------------ | :--------------------------- | :------- | :----------------------------------------- |
| show                | `boolean`                    | false    | 是否显示动作面板，支持语法糖`v-model:show` |
| items               | `Array as ActionSheetItem[]` | []       | 动作面板的选项列表                         |
| description         | `string`                     | --       | 显示在面板顶部的描述信息                   |
| alignment           | `'center' \| 'left'`         | 'center' | 文本的水平对齐方式                         |
| showCancel          | `boolean`                    | true     | 是否显示取消按钮                           |
| cancelText          | `string`                     | --       | 取消按钮的文本                             |
| closeOnClickOverlay | `boolean`                    | true     | 是否在点击遮罩后关闭                       |
| closeOnClickAction  | `boolean`                    | false    | 是否在点击选项后关闭                       |
| round               | `boolean`                    | true     | 是否显示圆角                               |
| zIndex              | `number`                     | --       | z-index 层级                               |
| teleport            | `string \| Element`          | --       | 指定`Teleport`组件所挂载的节点             |
| overlay             | `boolean`                    | true     | 是否显示遮罩层                             |
| overlayClass        | `string`                     | --       | 自定义遮罩层类名                           |
| overlayStyle        | `string`                     | --       | 自定义遮罩层样式                           |
| lockScroll          | `boolean`                    | true     | 是否锁定背景滚动                           |
| duration            | `number`                     | 0.3      | 动画执行时长，单位`s`，为`0`时禁用动画     |

## 事件

| 事件名 | 类型                                                     | 说明                                 |
| :----- | :------------------------------------------------------- | :----------------------------------- |
| select | `EventEmitter<{ item: ActionSheetItem; index: number }>` | 选中事件，点击非禁用状态的选项时触发 |
| cancel | `EventEmitter<void>`                                     | 取消事件，点击取消按钮时触发         |

## 插槽

| 名称    | 说明     |
| :------ | :------- |
| item    | 选项     |
| header  | 面板头部 |
| default | 面板内容 |

## CSS 变量

| 名称                                        | 默认值                     | 说明 |
| :------------------------------------------ | :------------------------- | :--- |
| --fm-action-sheet-max-height                | 80%                        | --   |
| --fm-action-sheet-border-radius             | 12px                       | --   |
| --fm-action-sheet-description-color         | var(--fm-gray-5)           | --   |
| --fm-action-sheet-description-line-height   | 22px                       | --   |
| --fm-action-sheet-description-font-size     | var(--fm-font-size-md)     | --   |
| --fm-action-sheet-description-padding       | 12px 16px                  | --   |
| --fm-action-sheet-item-default-background   | var(--fm-background-white) | --   |
| --fm-action-sheet-item-active-background    | #f2f3f5                    | --   |
| --fm-action-sheet-item-padding              | 13px 16px                  | --   |
| --fm-action-sheet-item-text-color           | #1a1a1a                    | --   |
| --fm-action-sheet-item-disabled-text-color  | #bdbdbd                    | --   |
| --fm-action-sheet-item-subtitle-color       | var(--fm-gray-5)           | --   |
| --fm-action-sheet-item-icon-size            | 18px                       | --   |
| --fm-action-sheet-item-icon-margin-right    | 8px                        | --   |
| --fm-action-sheet-item-title-font-size      | var(--fm-font-size-lg)     | --   |
| --fm-action-sheet-item-title-line-height    | 24px                       | --   |
| --fm-action-sheet-item-subtitle-font-size   | var(--fm-font-size-sm)     | --   |
| --fm-action-sheet-item-subtitle-line-height | 18px                       | --   |
| --fm-action-sheet-item-subtitle-margin-top  | 2px                        | --   |
| --fm-action-sheet-footer-gap-color          | #f5f5f5                    | --   |
| --fm-action-sheet-divider-color             | #e7e7e7                    | --   |
| --fm-action-sheet-cancel-height             | 48px                       | --   |
| --fm-action-sheet-cancel-color              | #1a1a1a                    | --   |
| --fm-action-sheet-cancel-font-size          | var(--fm-font-size-lg)     | --   |
| --fm-action-sheet-cancel-font-weight        | 500                        | --   |
