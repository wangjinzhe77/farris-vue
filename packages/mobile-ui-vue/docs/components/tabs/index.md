---
demo: tabs
---

# Tabs 标签页

Tabs 组件用于对平级内容分类后的展示切换。

## 基础用法

通过`v-model`绑定当前选中标签的`name`。

:::vdemo

```vue
{demos/tabs/base.vue}
```

:::

## 切换动画

如果将`animation`属性设置为真，则启用切换动画，此时`lazyRender`“懒渲染”失效。

:::vdemo

```vue
{demos/tabs/animation.vue}
```

:::

## 滑动切换

如果将`swipeable`属性设置为真，则可以通过左右滑动页签内容来切换标签页。

:::vdemo

```vue
{demos/tabs/swipeable.vue}
```

:::

## 标签栏滚动

当标签较多时，标签栏可滚动。

:::vdemo

```vue
{demos/tabs/tab-scroll.vue}
```

:::

## 带徽标的标签

通过`Tab`组件的`dot`属性可以在标签右上角显示一个圆点，通过`badge`属性可以在标签右上角显示一个徽标。

:::vdemo

```vue
{demos/tabs/badge.vue}
```

:::

## 禁用标签

通过`Tab`组件的`disabled`属性可以将标签设置为禁用，此时标签无法被选中。

:::vdemo

```vue
{demos/tabs/disabled.vue}
```

:::

## 带图标的标签

通过`Tab`组件的`icon`属性可以为标签添加一个图标。

:::vdemo

```vue
{demos/tabs/icon.vue}
```

:::

## 自适应高度

如果将`fit-height`属性设置为真，则`Tabs`组件的高度随着当前选中`Tab`组件的高度变化。

:::vdemo

```vue
{demos/tabs/fit-height.vue}
```

:::

## 切换标签事件

通过`change`事件可以监听当前标签切换事件。

:::vdemo

```vue
{demos/tabs/change.vue}
```

:::

## Tabs 属性

| 属性名            | 类型               | 默认值 | 说明                                             |
| :---------------- | :----------------- | :----- | :----------------------------------------------- |
| modelValue        | `string \| number` | --     | 当前选中标签页的标识，支持语法糖`v-model`        |
| lazyRender        | `boolean`          | true   | 是否在标签页首次被选中后才渲染其内容             |
| animation         | `boolean`          | false  | 是否启用切换动画，如果启用则`lazyRender`属性失效 |
| animationDuration | `number \| string` | 300    | 切换动画的执行时长，单位`ms`                     |
| swipeable         | `boolean`          | false  | 是否启用手势左右滑动切换                         |
| fitHeight         | `boolean`          | false  | 是否启用自适应高度                               |

## Tab 属性

| 属性名          | 类型               | 默认值 | 说明                                             |
| :-------------- | :----------------- | :----- | :----------------------------------------------- |
| name            | `string \| number` | --     | 必填，标签的名称，作为匹配的唯一标识             |
| title           | `string`           | --     | 显示的标题                                       |
| icon            | `string`           | --     | 图标，如果非空则显示在标题的左侧                 |
| iconColor       | `string`           | --     | 图标的颜色                                       |
| activeIcon      | `string`           | --     | 选中状态下的图标                                 |
| activeIconColor | `string`           | --     | 选中状态下的图标的颜色                           |
| dot             | `boolean`          | --     | 是否在标题右上角显示一个小圆点                   |
| badge           | `string \| number` | --     | 徽标的内容，如果非空则在标题的右上角显示一个徽标 |
| disabled        | `boolean`          | false  | 是否禁用当前标签页                               |
| destroyOnHide   | `boolean`          | false  | 是否在隐藏时销毁内容                             |

## Tabs 事件

| 事件名 | 类型                             | 说明                                                       |
| :----- | :------------------------------- | :--------------------------------------------------------- |
| change | `EventEmitter<string \| number>` | 切换标签事件，切换到新的标签时触发，事件参数是页签的`name` |

## Tab 插槽

| 名称    | 说明 |
| :------ | :--- |
| default | 内容 |
