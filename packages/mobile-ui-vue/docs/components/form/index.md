---
demo: form
---

# Form 表单

表单（Form）用于数据的收集、校验、提交，表单内部可以包含输入框、单选组、多选组、选择器等各种类型的输入组件，每个输入组件需要放到一个表单项（FormItem）容器下。


## 基础用法

实现一个简单的表单，我们仅需要为输入组件添加标签和绑定字段值：
- 通过FormItem的labe属性指定标签文本；
- 通过输入组件的v-model属性绑定字段值；

如果表单需要进行数据校验，还需进行以下配置：
- 通过Form的data属性指定表单绑定的数据；
- 通过Form的rules属性指定表单的验证规则；
- 通过FormItem的name属性指定输入组件绑定的字段名称，需要和data中的属性名保持一致；

:::vdemo
```vue
{demos/form/basic.vue}
```
:::

## 输入组件类型

在表单中我们可以添加各种类型的输入组件，目前支持的输入控件类型包括：输入框、开关、单选框、单选组、复选框、复选组、评分、选择器。其他输入组件（比如日期选择器、附件上传等）后续提供。

:::vdemo
```vue
{demos/form/control-type.vue}
```
:::

## 表单校验

我们可以通过为Form的rules属性指定表单级的校验规则，也可以通过为FormItem的rules指定单个表单项的校验规则。提交按钮点击时，可以通过Form组件实例上的validate方法执行验证，当输入组件绑定的字段值发生变化或校验规则发生变化时会自动触发校验。

Form组件目前预置了最基本的3种验证形式：必填校验、正则校验、自定义校验器（支持异步）。其他的常见验证规则（最大值、最小值、邮箱、电话等）后续提供：

:::vdemo
```vue
{demos/form/validation.vue}
```
:::

## 标签对齐

我们可以通过Form的labelAlign指定所有下级表单项的标签对齐方式，也可以通过FormItem的labelAlign设置单个FormItem的标签对齐方式。

:::vdemo
```vue
{demos/form/label-align.vue}
```
:::

## 标签宽度

我们可以通过Form的labelWidth指定所有下级表单项的标签宽度，也可以通过FormItem的labelWidth设置单个FormItem的标签宽度。

:::vdemo
```vue
{demos/form/label-width.vue}
```
:::

## 必填标志

Form默认会根据校验规则中是否存在必填校验自动显示必填星号，可以通过FormItem的required属性设置单个FormItem是否显示必填星号。

:::vdemo
```vue
{demos/form/required.vue}
```
:::

## Form属性

| 属性名             | 类型               | 默认值      | 说明  |
| :------------------| :---------------- | :---------- | :--- |
| data               | `object`  | null  | 表单绑定的数据，使用数据校验时必传，数据必须是一个响应式对象 |
| labelAlign         | `string`  | left  | 标签对齐方式，可选值：center、right、top |
| labelWidth         | `number`  | 105px | 标签宽度 |
| contentAlign       | `string`  | left  | 表单项内输入组件的对齐方式，可选值：center、right |
| autoShowRequired   | `boolean` | true  | 是否根据必填校验规则自动添加必填星号 |
| rules              | `object`  | --    | 表单校验规则，形如：`{ name: [{required: true, message: '姓名为必填'}]}` ，key要和data中的属性名保持一致 |
| show-error-message | `boolean` | true  | 是否显示校验错误信息 |
| errorMessageAlign  | `string`  | left  | 校验错误信息排列方式，可选值：center、right，建议和contentAlign保持一致 |


## Form事件


| 事件名    | 说明                     | 回调参数 |
| -------- | ------------------------ | ---------|
| validate | FormItem执行校验时触发    | 参数包括表单项的名称和校验错误消息，形如：`{ name: 'age', error: '年龄必须为数字'} ` |


## Form插槽

| 名称    | 说明       |
| :------ | :--------- |
| default | 自定义内容  |


## Form组件实例

| 名称          | 类型                                      | 说明  |
| :------------ | :--------------------------------------- | :---- |
| validate      | `() => Promise<ValidationErrors\|null>`  | 对整个表单进行验证，验证通过时返回`Promise<null>`，验证不通过时返回`Promise<ValidatorErrors>`，ValidatorErrors中的key和data中的字段属性名一致
| clearValidate | `() => void`                             | 清空整个表单的验证状态 |


## FormItem属性

| 属性名             | 类型               | 默认值      | 说明-  |
| :------------------| :---------------- | :---------- | :---- |
| name               | `string`  | --    | 表单项的名称，一个表单中不允许重复，启用表单验证时必填，需要和data中的属性名保持一致 |
| label              | `string`  | --    | 标签文本 |
| labelAlign         | `string`  | left  | 标签对齐方式，可选值：center、right、top|
| labelWidth         | `number`  | 105px | 标签宽度  |
| contentAlign       | `string`  | left  | 表单项内输入组件的对齐方式，可选值：center、right |
| required           | `boolean` | true  | 是否显示必填星号 |
| rules              | `array`   | --    | 表单校验规则，形如：`[{required: true, message: '年龄为必填'}, { pattern: /^\d+$/, message: '年龄必须为数字' }]` |
| show-error-message | `boolean` | true  | 是否显示校验错误信息 |
| errorMessageAlign  | `string`  | left  | 校验错误信息排列方式，可选值：center、right，建议和contentAlign保持一致|


## FormItem插槽

| 名称         | 说明       |
| :----------- | :-------- |
| default      | 自定义内容 |
| label        | 自定义标签区域 |
| errorMessage | 自定义校验错误信息显示区域 |

## FormItem组件实例

| 名称          | 类型                                      | 说明  |
| :------------ | :--------------------------------------- | :---- |
| validate      | `() => Promise<ValidationError\|null>`   | 验证表单项，验证通过时返回`Promise<null>`，验证不通过时返回`Promise<ValidationError>` |
| clearValidate | `() => void`                             | 清空表单项的验证状态 |

## ValidationRule

| 属性名            | 类型                                        | 说明                                                         |
| :---------------- | :------------------------------------------ | :----------------------------------------------------------- |
| message           | `string`                                    | 校验错误信息                                                 |
| required          | `boolean`                                   | 必填验证规则                                                 |
| pattern           | `RegExp`                                    | 正则表达式                                                   |
| validator         | `(value: any)=> boolean \|Promise<boolean>` | 自定义验证函数，支持异步校验，异步校验时需要返回一个 Promise |
| idcard            | `boolean`                                   | 身份证号验证                                                 |
| mobilePhoneNumber | `boolean`                                   | 手机号验证                                                   |
| email             | `boolean`                                   | 邮箱验证                                                     |
| maxValue          | `boolean`                                   | 最大值                                                       |
| minValue          | `boolean`                                   | 最小值                                                       |
| dateMaxValue      | `boolean`                                   | 日期最大值                                                   |
| dateMinValue      | `boolean`                                   | 日期最小值                                                   |
| maxLength         | `boolean`                                   | 最大长度                                                     |
| minLength         | `boolean`                                   | 最小长度                                                     |
