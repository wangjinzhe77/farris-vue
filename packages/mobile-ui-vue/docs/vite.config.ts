import { defineConfig } from 'vite';
import { resolve } from 'path';
import vueJsx from '@vitejs/plugin-vue-jsx';
import svgLoader from 'vite-svg-loader';
import { MarkdownTransform } from './.vitepress/plugins/markdown-transform';

export default defineConfig({
  plugins: [vueJsx({}), svgLoader(), MarkdownTransform()],
  server: {
    fs: {
      strict: false
    }
  },
  resolve: {
    alias: [
      { find: '@', replacement: resolve(__dirname, '../') },
      { find: '@components', replacement: resolve(__dirname, '../components') },
      { find: '@farris/mobile-ui-vue', replacement: resolve(__dirname, '../components') }
    ]
  },
});
