export default {
  views: [
    {
      title: "基础组件",
      subMenu: [
        {
          title: "图标",
          name: "icon",
          url: "/demos/icon",
          component: "/icon",
        },
        {
          title: "按钮",
          name: "button",
          url: "/demos/button",
          component: "/button",
        },
        {
          title: "单元格",
          name: "cell",
          url: "/demos/cell",
          component: "/cell",
        },
        {
          title: "导航栏",
          name: "navbar",
          url: "/demos/navbar",
          component: "/navbar",
        },
        {
          title: "弹窗",
          name: "popup",
          url: "/demos/popup",
          component: "/popup",
        },
        {
          title: "遮罩",
          name: "overlay",
          url: "/demos/overlay",
          component: "/overlay",
        }
      ],
    },
    {
      title: "表单组件",
      subMenu: [
        {
          title: "表单",
          name: "form",
          url: "/demos/form",
          component: "/form",
        },
        {
          title: "输入框",
          name: "input",
          url: "/demos/input",
          component: "/input",
        },
        {
          title: "多选框",
          name: "checkbox",
          url: "/demos/checkbox",
          component: "/checkbox",
        },
        {
          title: "单选框",
          name: "radio",
          url: "/demos/radio",
          component: "/radio",
        },
        {
          title: "评分",
          name: "rate",
          url: "/demos/rate",
          component: "/rate",
        },
        {
          title: "选择器",
          name: "picker",
          url: "/demos/picker",
          component: "/picker",
        },
        {
          title: "日期选择器",
          name: "date-picker",
          url: "/demos/date-picker",
          component: "/date-picker",
        },
        {
          title: "时间选择器",
          name: "time-picker",
          url: "/demos/time-picker",
          component: "/time-picker",
        },
        {
          title: "日期时间选择器",
          name: "date-time-picker",
          url: "/demos/date-time-picker",
          component: "/date-time-picker",
        },
        {
          title: "开关",
          name: "switch",
          url: "/demos/switch",
          component: "/switch",
        },
        {
          title: "按钮输入框",
          name: "button-edit",
          url: "/demos/button-edit",
          component: "/button-edit",
        }
      ],
    },
    {
      title: "反馈组件",
      subMenu: [
        {
          title: "动作面板",
          name: "action-sheet",
          url: "/demos/action-sheet",
          component: "/action-sheet",
        },
        {
          title: "加载中",
          name: "loading",
          url: "/demos/loading",
          component: "/loading",
        },
        {
          title: "下拉刷新",
          name: "pull-refresh",
          url: "/demos/pull-refresh",
          component: "/pull-refresh",
        },
        {
          title: "滑动单元格",
          name: "swipe-cell",
          url: "/demos/swipe-cell",
          component: "/swipe-cell",
        },
        {
          title: "轻提示",
          name: "toast",
          url: "/demos/toast",
          component: "/toast",
        },
        {
          title: "消息通知",
          name: "notify",
          url: "/demos/notify",
          component: "/notify",
        },
        {
          title: "对话框",
          name: "dialog",
          url: "/demos/dialog",
          component: "/dialog",
        }
      ],
    },
    {
      title: "展示组件",
      subMenu: [
        {
          title: "列表",
          name: "list",
          url: "/demos/list",
          component: "/list",
        },
        {
          title: "列表视图",
          name: "list-view",
          url: "/demos/list-view",
          component: "/list-view",
        },
        {
          title: "标签栏",
          name: "tab-bar",
          url: "/demos/tab-bar",
          component: "/tab-bar",
        },
        {
          title: "标签页",
          name: "tabs",
          url: "/demos/tabs",
          component: "/tabs",
        },
        {
          title: "标签",
          name: "tag",
          url: "/demos/tag",
          component: "/tag",
        },
      ],
    },
  ]
};
