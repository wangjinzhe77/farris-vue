export const blinkText = (element: HTMLElement): void => {
  if (!element) {
    return;
  }
  const blinkingClass = 'blinking-item';
  const isBlinking = element.classList.contains(blinkingClass);
  if (isBlinking) {
    return;
  }
  element.classList.add(blinkingClass);
  setTimeout(() => {
    element.classList.remove(blinkingClass);
  }, 1000);
};
