import { createApp } from 'vue';
import './style.css';
import App from './app.vue';
import router from './router';
import FarrisMobile from '@farris/mobile-ui-vue';
import FarrisMobileDemo from './components';

import '@components/index.scss';

/* -------------------------------- 临时的外部依赖 - 开始 -------------------------------- */
// @todo 临时引入的外部依赖，待其独立后统一移除
// import Designer from '../designer';
// import PageHeader from '../../ui-vue/components/page-header';
// import Splitter from '../../ui-vue/components/splitter';
// import Tabs from '../../ui-vue/components/tabs';
// import PropertyPanel from '../components/property-panel';
// import DynamicView from '../components/dynamic-view';
// import DesignerCanvas from '../components/designer-canvas';
// /* -------------------------------- 临时的外部依赖 - 结束 -------------------------------- */

// const temporaryExternalDependencies = [
//   Designer,
//   PageHeader,
//   Splitter,
//   Tabs,
//   PropertyPanel,
//   DynamicView,
//   DesignerCanvas,
// ];

const app = createApp(App);

// temporaryExternalDependencies.forEach((component) => {
//   app.use(component);
// });

app.use(router)
  .use(FarrisMobile)
  .use(FarrisMobileDemo)
  .mount('#app');
