import { blinkText } from '../composables/blink';

export const handleMessage = () => {

  const inBrowser = typeof window !== 'undefined';
  if (!inBrowser) {
    return;
  }

  const isDemoInBlock = (element: HTMLElement): boolean => {
    return !!element && element.classList.contains('fm-demo-block');
  };

  const isDemoInTab = (element: HTMLElement): boolean => {
    return !!element && element.classList.contains('fm-demo-tabs__tab');
  };

  const handleDemoInDifferentContainer = (element: HTMLElement) => {
    if (isDemoInBlock(element)) {
      element.scrollIntoView({ block: 'nearest' });
      setTimeout(() => {
        blinkText(element);
      }, 50);
    }
    if (isDemoInTab(element)) {
      element.click();
      element.scrollIntoView({ block: 'nearest' });
    }
  };

  const scrollDemoIntoView = (demoID: string) => {
    if (!demoID) {
      return;
    }
    const targetDemoElement = document.querySelector(`[data-id=${demoID}]`) as HTMLElement;
    if (targetDemoElement) {
      handleDemoInDifferentContainer(targetDemoElement);
    }
  };

  const changeTheme = (theme: string) => {
    const documentElement = document.documentElement || document.body;
    const DARK_THEME = 'dark';
    if (theme === DARK_THEME) {
      documentElement.classList.add(DARK_THEME);
    } else {
      documentElement.classList.remove(DARK_THEME);
    }
    const APPEARANCE_KEY = 'vitepress-theme-appearance';
    localStorage.setItem(APPEARANCE_KEY, theme);
  };

  window.addEventListener('message', (event) => {
    const { data } = event;
    if (!data || typeof data !== 'object') {
      return;
    }
    const { action } = data;
    if (action === 'scrollDemoIntoView') {
      setTimeout(() => {
        scrollDemoIntoView(data.id);
      }, 100);
    }
    if (action === 'changeTheme') {
      changeTheme(data.theme);
    }
  });
};
