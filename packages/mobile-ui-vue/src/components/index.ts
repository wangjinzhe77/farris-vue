import type { App } from 'vue';
import { handleMessage } from './message';
import DemoBlock from './demo-block/demo-block.component';
import DemoTabs from './demo-tabs/demo-tabs.component';
import DemoTab from './demo-tabs/demo-tab.component';

import './index.scss';

const components = [
  DemoBlock,
  DemoTabs,
  DemoTab,
];

const install = (app: App): void => {
  components.forEach((component) => {
    app.component(component.name, component);
  });
};

handleMessage();

export default {
  install
};
