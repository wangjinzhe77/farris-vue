import { defineComponent, SetupContext, computed, ref } from 'vue';
import { useBem } from '@components/common';

export default defineComponent({
  name: 'FmDemoBlock',

  props: {
    title: { type: String, default: '' },
    description: { type: String, default: '' },
    noPadding: { type: Boolean, default: false },
    noBackgroundColor: { type: Boolean, default: false },
  },

  setup(props, context: SetupContext) {
    const blockName = 'fm-demo-block';
    const { bem } = useBem(blockName);
    const { slots } = context;

    const root = ref();

    const demoBlockClass = computed(() => {
      return {
        [bem()]: true,
        [bem('', 'with-padding')]: !props.noPadding,
        [bem('', 'no-bg-color')]: props.noBackgroundColor,
      };
    });

    const showDemoCodeByID = () => {
      const data = {
        action: 'showDemoCodeByID',
        id: root.value?.id,
      };
      window.parent.postMessage(data, '*');
    };

    return () => (
      <div class={demoBlockClass.value} ref={root} data-id={root.value?.id}>
        {props.title && (
          <div class={bem('title')}>
            <span class={[bem('title-text'), 'blinkable-text']} onClick={showDemoCodeByID}>{props.title}</span>
          </div>
        )}
        {props.description && (
          <div class={bem('description')}>{props.description}</div>
        )}
        <div class={bem('container')}>
          {slots.default?.()}
        </div>
      </div>
    );
  },
});
