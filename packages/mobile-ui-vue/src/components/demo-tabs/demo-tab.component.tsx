import { computed, defineComponent, SetupContext } from 'vue';
import { useBem } from '@components/common';

export default defineComponent({
  name: 'FmDemoTab',

  props: {
    title: { type: String, default: '' },
    padding: { type: Boolean, default: false },
    fill: { type: Boolean, default: false },
  },

  setup(props, context: SetupContext) {
    const blockName = 'fm-demo-tab';
    const { bem } = useBem(blockName);
    const { slots } = context;

    const tabClass = computed(() => {
      return {
        [bem()]: true,
        [bem('', 'with-padding')]: props.padding,
        [bem('', 'fill')]: props.fill,
      };
    });

    return () => (
      <div class={tabClass.value}>
        {slots.default?.()}
      </div>
    );
  },
});
