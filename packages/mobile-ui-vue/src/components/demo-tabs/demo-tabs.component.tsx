/* eslint-disable no-use-before-define */
import { defineComponent, SetupContext, VNode, ref, h, onActivated, onMounted, nextTick } from 'vue';
import { inBrowser, useBem } from '@components/common';
import DemoTab from './demo-tab.component';

export default defineComponent({
  name: 'FmDemoTabs',

  props: {
    defaultIndex: { type: [Number, String], default: 0 },
  },

  setup(props, context: SetupContext) {
    const blockName = 'fm-demo-tabs';
    const { bem } = useBem(blockName);
    const { slots } = context;

    const isTab = (node: VNode) => {
      return !!node && node.type === DemoTab;
    };

    const defaults = slots.default();
    const tabItems = defaults.filter(node => isTab(node));
    const tabTitles = tabItems.map(item => item.props.title as string);
    const titleRefs = ref([]);
    const navRef = ref();

    const activeIndex = ref<number>(-1);
    const trackStyle = ref({});

    const getChildSpanElement = (node: HTMLElement): HTMLElement => {
      const children = node.childNodes;
      for (let i = 0; i < children.length; i++) {
        const childNode = children[i] as HTMLElement;
        const isElement = childNode.nodeType === childNode.ELEMENT_NODE;
        if (isElement && childNode.tagName.toLowerCase() === 'span') {
          return childNode;
        }
      }
      return null;
    };

    const resetTrack = (immediate?: boolean) => {
      nextTick(() => {
        const titles = titleRefs.value;
        const title = titles[activeIndex.value] as HTMLElement;
        if (!title) {
          return;
        }
        const left = title.offsetLeft + title.offsetWidth / 2;
        const textNode = getChildSpanElement(title);
        const textNodeWidth = textNode ? textNode.offsetWidth : 0;
        const newWidth = Math.max(textNodeWidth, 20);
        trackStyle.value = {
          width: `${newWidth}px`,
          transform: `translateX(${left}px) translateX(-50%)`,
          transitionDuration: !immediate ? `300ms` : undefined,
        };
      });
    };

    let cancelScrollIntoViewAnimation: (() => void) | undefined;

    const scrollIntoView = (immediate?: boolean) => {
      const nav = navRef.value as HTMLElement;
      const titles = titleRefs.value;
      const title = titles[activeIndex.value] as HTMLElement;
      const to = title.offsetLeft - (nav.offsetWidth - title.offsetWidth) / 2;
      if (cancelScrollIntoViewAnimation) {
        cancelScrollIntoViewAnimation();
      };
      cancelScrollIntoViewAnimation = scrollLeftTo(nav, to, immediate ? 0 : 300);
    };

    const setActiveIndex = (index: number, immediate?: boolean) => {
      if (activeIndex.value === index) {
        return;
      }
      activeIndex.value = index;
      setTimeout(() => {
        scrollIntoView(immediate);
        resetTrack(immediate);
      });
    };

    const init = () => {
      setActiveIndex(+props.defaultIndex || 0, true);
    };

    const handleResize = () => {
      resetTrack(true);
      scrollIntoView(true);
    };

    onActivated(resetTrack);
    onMounted(init);
    window.addEventListener('resize', handleResize, { passive: true });
    window.addEventListener('orientationchange', handleResize, { passive: true });

    const showDemoCodeByID = (id: string) => {
      const data = { action: 'showDemoCodeByID', id };
      window.top.postMessage(data, '*');
    };

    const getTabIdByIndex = (index: number): string => {
      return tabItems[index].props.id;
    };

    const handleTabClick = (event: MouseEvent, index: number) => {
      setActiveIndex(index);
      if (event.isTrusted) {
        const tabId = getTabIdByIndex(index);
        showDemoCodeByID(tabId);
      }
    };

    const renderTabItemTitle = (title: string, index: number) => {
      const titleClass = {
        [bem('tab')]: true,
        [bem('tab', 'active')]: activeIndex.value === index,
      };
      const dataId = tabItems[index].props.id;
      return (
        <div ref={(el) => { titleRefs.value[index] = el; }}
          class={titleClass}
          data-id={dataId}
          onClick={(event) => handleTabClick(event, index)}>
          <span>{title}</span>
        </div>
      );
    };

    const renderHead = () => {
      return (
        <div class={bem('head')}>
          <div class={bem('nav')} ref={navRef}>
            {tabTitles.map((title, index) => renderTabItemTitle(title, index))}
            <div class={bem('track')} style={trackStyle.value}></div>
          </div>
        </div>
      );
    };

    const renderTabItemContent = (item: VNode, index: number) => {
      return h(item, { class: { 'active': index === activeIndex.value } });
    };

    const renderContent = () => {
      return (
        <div class={bem('content')}>
          {tabItems.map((item, index) => renderTabItemContent(item, index))}
        </div>
      );
    };

    return () => (
      <div class={bem()}>
        {renderHead()}
        {renderContent()}
      </div>
    );
  },
});

function scrollLeftTo(scroller: HTMLElement, to: number, duration: number) {
  if (!inBrowser) {
    return;
  }
  let animationId: number;
  let currentFrame = 0;
  const from = scroller.scrollLeft;
  const frames = duration === 0 ? 1 : Math.round(duration / 16);

  function cancelAnimation() {
    cancelAnimationFrame(animationId);
  }

  function doAnimation() {
    scroller.scrollLeft += (to - from) / frames;
    if (++currentFrame < frames) {
      animationId = requestAnimationFrame(doAnimation);
    }
  }

  doAnimation();

  return cancelAnimation;
}
