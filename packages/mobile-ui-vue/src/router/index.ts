import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router';
import menuData from '../menu-data';

// import DesignerCanvasBasicDemo from '../../demos/designer-canvas/basic.vue';
// import DynamicViewBasicDemo from '../../demos/dynamic-view/basic.vue';

// const designTimeDemoRoutes: RouteRecordRaw[] = [
//   { path: '/designer-canvas/basic', component: DesignerCanvasBasicDemo },
//   { path: '/dynamic-view/basic', component: DynamicViewBasicDemo },
// ];

// @ts-ignore
const modules = import.meta.glob([`../../demos/**/index.vue`, `../../demos/index.vue`]);

const allDemos: RouteRecordRaw[] = [];
menuData.views.forEach(view => {
  if (!view.subMenu) {
    return;
  }
  view.subMenu.forEach((menuItem) => {
    allDemos.push({
      name: menuItem.name,
      path: menuItem.name,
      component: modules[`../../demos/${menuItem.name}/index.vue`]
    });
  });
});

const routes: RouteRecordRaw[] = [
  // ...designTimeDemoRoutes,
  {
    name: 'home',
    path: '/home',
    component: modules["../../demos/home/index.vue"]
  },
  {
    name: 'demos',
    path: '/demos',
    component: modules["../../demos/index.vue"],
    children: allDemos
  },
  {
    path: '/',
    redirect: '/home'
  }
];

export default createRouter({
  history: createWebHashHistory(),
  routes,
});
