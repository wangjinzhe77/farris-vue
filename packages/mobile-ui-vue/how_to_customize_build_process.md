# 自定义打包

我们可以通过自定义打包来定制npm包中的内容，以减少npm包的体积。假设我们要定制一个只包含Button组件的npm包，可以按一下步骤实现。

## 步骤1：添加自定义入口文件

在packages/mobile-ui-vue/components目录下添加index-custom.ts和index-custom.scss。
在入口文件中仅导出按钮组件和它的样式文件。

**index-custom.ts**
```ts
import { App } from 'vue';
import Button from './button';

import './index-custom.scss';

const components = [
  Button
];

const install = (app: App): void => {
  components.forEach((component) => {
    app.use(component);
  });
};
export {
  Button
};

export default {
  install
};
```

**index-custom.scss**
```scss
@import './button/src/button.scss';
```


## 步骤2：添加自定义打包命令

在packages/mobile-ui-vue/scripts/commands目录下添加build-custom.js，将入口文件设置为index-custom.ts

```js
const { resolve } = require('path');
const fsExtra = require('fs-extra');
const { omit } = require('lodash');
const { build } = require('vite');
const dts = require('vite-plugin-dts');
const replace = require('../plugins/replace');
const getViteConfig = require('./vite.config');

const CWD = process.cwd();

const package = require('../../package.json');

const getVersion = () => {
  const versionNums = package.version.split('.');
  return versionNums
    .map((num, index) => (index === versionNums.length - 1 ? +num + 1 : num))
    .join('.');
};

const createPackageJson = async (version) => {
  package.version = getVersion(version);
  package.main = './index.umd.js';
  package.module = './index.esm.js';
  package.style = './style.css';
  package.dependencies = omit(package.dependencies, '');
  package.types = './types/index.d.ts';
  package.private = false;
  const fileStr = JSON.stringify(omit(package, 'scripts', 'devDependencies'), null, 2);
  await fsExtra.outputFile(resolve(CWD, './package', `package.json`), fileStr, 'utf-8');
};

exports.build = async () => {
  const lib = {

    // 指定入口文件
    entry: resolve(CWD, './components/index-custom.ts'),
    name: 'FarrisVue',
    fileName: 'index',
    formats: ['esm', 'umd']
  };

  const outDir = resolve(CWD, './package');

  const plugins = [
    dts({
      entryRoot: './components',
      outputDir: './package/types',
      include: ['./components/**/*.ts', './components/**/*.tsx', './components/**/*.vue'],
      noEmitOnError: false,
      skipDiagnostics: true
    }),
    replace({ path: (format, args) => `.${args[1]}/index.${format}.js` })
  ];
  const config = getViteConfig({ lib, outDir, plugins });

  await build(config);
  await createPackageJson();
};

```


## 步骤3：注册打包命令

在packages/mobile-ui-vue/scripts/index.js中注册自定义打包命令

```js
const { Command } = require('commander');
const { build } = require('./commands/build');
const { dev: devDocs, build: buildDocs } = require('./commands/docs');

// 导入自定义打包命令
const { build: buildCustom } = require('./commands/build-custom');

const program = new Command();

program.command('build').description('构建 Farris UI Vue 组件库').action(build);

// 注册自定义打包命令
program.command('build:custom').description('构建 自定义Farris UI Vue 组件库').action(buildCustom);

program.command('docs:dev').action(devDocs);

program.command('docs:build').action(buildDocs);

program.parse();
```

## 步骤4：在packages.json添加命令执行入口

在packages/mobile-ui-vue/packages.json的scripts中添加build:custom命令的入口

```json
{
  "name": "@farris/mobile-ui-vue",
  "private": true,
  "version": "0.0.1-beta.1",
  "scripts": {
    "dev": "vite",
    "build": "vue-tsc && vite build",
    "build:lib": "node ./scripts/index.js build",
    "build:custom": "node ./scripts/index.js build:custom",
    "preview": "vite preview",
    "docs:dev": "node ./scripts/index.js docs:dev",
    "docs:build": "node ./scripts/index.js docs:build"
  }
}
```

在命令行中执行打包命令
```bash
npm run build:custom
```