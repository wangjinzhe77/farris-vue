const { resolve } = require("path");
const { build } = require("vite");
const dts = require("vite-plugin-dts");

const replace = require("../plugins/replace");
const buildViteConfig = require("./build-vite-config");
const createPackageJson = require("./create-package");

const CWD = process.cwd();

exports.build = async () => {

    // 类库配置
    const lib = {
        entry: resolve(CWD, "./lib/index.ts"),
        name: "FarrisMobileCommandServicesVue",
        fileName: "mobile-command-services-vue",
        formats: ["esm", "umd"],
    };

    // 输出配置
    const outDir = resolve(CWD, "./dist/mobile-command-services-vue");

    // 插件配置
    const plugins = [
        dts({
            entryRoot: "./lib",
            outputDir: resolve(CWD, "./dist/mobile-command-services-vue/types"),
            include: [
                "./lib/**/*.ts",
                "./lib/**/*.tsx",
                "./lib/**/*.vue",
            ],
            noEmitOnError: false,
            skipDiagnostics: true,
        }),
        replace({ path: (format, args) => `.${args[1]}/mobile-command-services-vue.${format}.js` })
    ];

    // 执行打包
    const config = buildViteConfig({ lib, outDir, plugins });
    await build(config);

    // 输出package.json
    await createPackageJson();
};
