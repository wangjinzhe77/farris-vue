#!/usr/bin/env node
const { Command } = require('commander');
const { build } = require('./commands/build');

const program = new Command();
program.command('build').description('构建 Farris Mobile Command Services For Vue').action(build);
program.parse();
