// / <reference types="vitest" />
import path from 'path';
import { defineConfig } from 'vite';
import type { InlineConfig } from 'vitest';
import type { UserConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';

interface VitestConfigExport extends UserConfig {
    test: InlineConfig;
}

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [vue(), vueJsx()],
    test: {
        globals: true,
        environment: 'happy-dom',
        include: ['**/*.test.tsx']
    },
    server: {
        proxy: {
            "/api": {
                target: "http://127.0.0.1:5200",
                changeOrigin: true,
                secure: false
            },
            "/apps": {
                target: "http://127.0.0.1:5200",
                changeOrigin: true,
                secure: false
            }
        }
    },
    resolve: {
        alias: {
            "@farris/devkit-vue": path.resolve(__dirname, "../devkit/lib/index"),
            "@farris/bef-vue": path.resolve(__dirname, "../bef/lib/index")
        }
    }
} as VitestConfigExport);
