import { ViewModelState, ViewModel, Entity, EntityState, EntityStore } from '@farris/devkit-vue';

/**
 * 实体状体服务
 */
class EntityStateService {

    /**
     * 视图模型
     */
    private viewModel: ViewModel<ViewModelState>;

    /**
     * 实体状态
     */
    private entityStore: EntityStore<EntityState<Entity>>;

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        this.viewModel = viewModel;
        this.entityStore = viewModel.entityStore as EntityStore<EntityState<Entity>>;
    }

    /**
     * 改变当前行
     */
    public changeCurrentEntity(id: string): void {
        this.entityStore.changeCurrentEntityById(id);
    }

    /**
     * 设置当前实体
     */
    public changeCurrentEntityByPath(path: string, id: string): void {
        this.entityStore.changeCurrentEntityByPath(path, id);
    }
}

export { EntityStateService };
