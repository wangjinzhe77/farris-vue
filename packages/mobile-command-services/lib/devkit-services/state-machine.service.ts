import { StateMachineState, StateMachine, ViewModelState, ViewModel } from '@farris/devkit-vue';

/**
 * 实体状体服务
 */
class StateMachineService {

    /**
     * 视图模型
     */
    private viewModel: ViewModel<ViewModelState>;

    /**
     * 实体状态
     */
    private stateMachine: StateMachine<StateMachineState>;

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        this.viewModel = viewModel;
        this.stateMachine = viewModel.stateMachine as StateMachine<StateMachineState>;
    }

    /**
     * 设置当前实体
     */
    public executeAction(actionName: string): void {
        if (!actionName) {
            return;
        }

        if (!this.stateMachine) {
            return;
        }

        (this.stateMachine as any)[actionName]();
    }
}

export { StateMachineService };
