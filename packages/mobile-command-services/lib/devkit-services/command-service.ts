import { ViewModelState, ViewModel } from '@farris/devkit-vue';

/**
 * 命令服务
 */
class CommandService {

    /**
     * 视图模型
     */
    private viewModel: ViewModel<ViewModelState>;


    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        this.viewModel = viewModel;
    }

    /**
     * 设置当前实体
     */
    public execute(commandName: string, viewModelId: string): any {
        let viewModel = this.viewModel;
        if (viewModelId) {
            const module = viewModel.getModule();
            const targetViewModel = module.getViewModel(viewModelId);
            if (!targetViewModel) {
                throw new Error(`ViewModel(id=${viewModelId}) does not exist`);
            }

            viewModel = targetViewModel;
        }
        return viewModel[commandName]();
    }
}

export { CommandService };
