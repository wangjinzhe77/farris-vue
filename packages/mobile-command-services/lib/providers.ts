import { ViewModel, StaticProvider, EXCEPTION_HANDLER_TOKEN } from '@farris/devkit-vue';
import {
    EntityStateService, StateMachineService, CommandService
} from './devkit-services/index';
import {
    LoadDataService, CreateDataService, RemoveDataService, SaveDataService,
    CancelDataService, UpdateDataService, ValidatorService
} from './data-services/index';
import { ExceptionService } from './exception/index';
import { ROUTER_TOKEN, RouterService} from './router.service';

const commandServiceModuleProviders: StaticProvider[] = [
    { provide: EXCEPTION_HANDLER_TOKEN, useClass: ExceptionService, deps: []}
];

const commandServiceViewModelProviders: StaticProvider[] = [
    { provide: LoadDataService, useClass: LoadDataService, deps: [ViewModel] },
    { provide: CreateDataService, useClass: CreateDataService, deps: [ViewModel] },
    { provide: RemoveDataService, useClass: RemoveDataService, deps: [ViewModel] },
    { provide: SaveDataService, useClass: SaveDataService, deps: [ViewModel] },
    { provide: CancelDataService, useClass: CancelDataService, deps: [ViewModel] },
    { provide: SaveDataService, useClass: SaveDataService, deps: [ViewModel] },
    { provide: UpdateDataService, useClass: UpdateDataService, deps: [ViewModel] },
    { provide: ValidatorService, useClass: ValidatorService, deps: [ViewModel]},
    { provide: EntityStateService, useClass: EntityStateService, deps: [ViewModel] },
    { provide: StateMachineService, useClass: StateMachineService, deps: [ViewModel] },
    { provide: CommandService, useClass: CommandService, deps: [ViewModel] },
    { provide: RouterService, useClass:RouterService, deps: [ViewModel, ROUTER_TOKEN] }

];

export { commandServiceModuleProviders, commandServiceViewModelProviders };
