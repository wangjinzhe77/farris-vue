import { Router } from 'vue-router';
import { ViewModelState, ViewModel } from '@farris/devkit-vue';

const ROUTER_TOKEN = 'ROUTER_TOKEN';

/**
 * 路由方法
 */
class RouterService {


    private viewModel: ViewModel<ViewModelState>;

    /**
     * 路由方法
     */
    private router: Router;

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>, router: any) {
        this.viewModel = viewModel;
        this.router = router;
    }
    

    /**
     * 导航方法
     */
    public navigate(path: string, queryParams: any) {
        const parsedQueryParams = this.parseQueryParams(queryParams);
        this.router.push({
            path: path,
            query: parsedQueryParams
          });
    }

    /**
     * 解析参数
     */
    public parseQueryParams(queryParams: any) {
        if (!queryParams || typeof queryParams === 'object') {
            return queryParams;
        }
       
        const parsedQueryParams = JSON.parse(queryParams);
        return parsedQueryParams;
    }

}

export { RouterService, ROUTER_TOKEN };