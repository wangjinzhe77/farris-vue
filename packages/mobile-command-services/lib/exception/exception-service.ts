import { EXCEPTION_HANDLER_TOKEN, ExceptionHandler} from '@farris/devkit-vue';

/**
 * 异常处理服务
 */
class ExceptionService implements ExceptionHandler {
    
    /**
     * 处理异常
     */
    public handle(error: any) {
        console.error(error);
    }

}

export { ExceptionService };
