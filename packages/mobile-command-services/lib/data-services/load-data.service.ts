import { ViewModelState, ViewModel, Entity } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';

/**
 * 数据加载服务
 */
class LoadDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     * 加载数据
     */
    public loadForList(filters: any[], sorts: [], pageSize?: number, pageIndex?: number): Promise<Entity[]> {
        filters = filters || [];
        sorts = sorts || [];
        pageSize = pageSize || 20;
        pageIndex = pageIndex || 1;

        if (typeof filters === 'string') {
            filters = JSON.parse(filters);
        }

        if (typeof sorts === 'string') {
            sorts = JSON.parse(sorts);
        }

        const loadPromise = this.repository.getEntities(filters, sorts, pageSize, pageIndex).then((entities: Entity[]) => {
            this.entityStore.loadEntities(entities);

            return entities;
        });

        return loadPromise;
    }

    /**
     *加载实体
     */
    public loadForCard(id: string): Promise<Entity> {
        const loadPromise = this.repository.getEntityById(id).then((entity: Entity) => {
            this.entityStore.loadEntities([entity]);
            return entity;
        });

        return loadPromise;
    }

    /**
     * 合并过滤条件
     */
    public mergeFilters(filters: any[], autoMerge: boolean) {
        return filters;
    }

}

export { LoadDataService };
