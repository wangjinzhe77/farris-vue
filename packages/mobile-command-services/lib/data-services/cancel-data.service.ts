import { ViewModelState, ViewModel } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';
import { UpdateDataService } from './update-data.service';

/**
 * 数据取消服务
 */
class CancelDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     * 取消方法
     */
    public cancel(): Promise<void> {
        return this.repository.cancelEntityChanges().then(() => {
            const updateService = this.getService<UpdateDataService>(UpdateDataService);
            const currentEntity = this.entityStore.getCurrentEntity();
            return updateService.update(currentEntity.idValue);
        });
    }

}

export { CancelDataService };
