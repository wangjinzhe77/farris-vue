import { ViewModelState, ViewModel, Entity } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';

/**
 * 数据验证服务
 */
class ValidatorService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     * 重置验证状态
     */
    public resetFieldsValidate(): void {
        return;
    }

    /**
     * 清空子表验证状态
     */
    public clearValidationChildResult(): void {
        return;
    }
}

export { ValidatorService };
