import { ViewModelState, ViewModel, Entity } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';

/**
 * 数据新增服务
 */
class CreateDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     * 新增数据
     */
    public create(): Promise<Entity> {
        const createPromise = this.repository.createEntity().then((entity: Entity) => {
            this.entityStore.loadEntities([entity]);
            return entity;
        });
        return createPromise;
    }

    /**
     * 追加新数据
     */
    public append(): Promise<Entity> {
        const createPromise = this.repository.createEntity().then((entity: Entity) => {
            this.entityStore.appendEntities([entity]);
            return entity;
        });
        return createPromise;
    }

}

export { CreateDataService }