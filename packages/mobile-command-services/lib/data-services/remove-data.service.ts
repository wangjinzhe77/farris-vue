import { ViewModelState, ViewModel } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';

/**
 * 数据删除服务
 */
class RemoveDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     * 删除方法
     */
    public remove() {
        const currentEntity = this.entityStore.getCurrentEntity();
        const id = currentEntity.idValue;
        const removePromsie = this.repository.removeAndSaveEntityById(id).then(() => {
            this.entityStore.removeEntityById(id);
        });

        return removePromsie;
    }

}

export { RemoveDataService };
