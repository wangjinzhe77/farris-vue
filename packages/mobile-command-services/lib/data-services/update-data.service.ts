import { ViewModelState, ViewModel, Entity } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';

/**
 * 更新数据服务
 */
class UpdateDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     *加载实体
     */
    public update(id: string): Promise<void> {
        const loadPromise = this.repository.getEntityById(id).then((newEntity: Entity) => {
            const newEntityData = newEntity.toJSON();
            this.entityStore.updateEntityById(id, newEntityData);
        });

        return loadPromise;
    }

}

export { UpdateDataService };
