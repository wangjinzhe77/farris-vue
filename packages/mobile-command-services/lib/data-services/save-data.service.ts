import { ViewModelState, ViewModel } from '@farris/devkit-vue';
import { BaseDataService } from './base-data.service';

/**
 * 数据保存服务
 */
class SaveDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     * 保存成功
     */
    public save() {
        return this.repository.saveEntityChanges();
    }


}

export { SaveDataService }