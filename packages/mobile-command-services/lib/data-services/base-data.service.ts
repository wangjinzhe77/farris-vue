import { Entity, EntityState, EntityStore, ViewModelState, ViewModel } from '@farris/devkit-vue';
import { BefRepository } from '@farris/bef-vue';

/**
 * 基础数据服务
 */
class BaseDataService {

    /**
     * 视图模型
     */
    protected viewModel: ViewModel<ViewModelState>;

    /**
     * 数据仓库
     */
    protected repository: BefRepository<Entity>;

    /**
     * 实体状态
     */
    protected entityStore: EntityStore<EntityState<Entity>>;


    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        this.viewModel = viewModel;
        this.repository = viewModel.repository as BefRepository<Entity>;
        this.entityStore = viewModel.entityStore as  EntityStore<EntityState<Entity>>;
    }

    /**
     * 获取服务实例
     */
    public getService<T>(token: any, defaultValue?: any): T {
        const injector = this.viewModel.getInjector();
        return injector.get<T>(token, defaultValue);
    }
}

export { BaseDataService }