import {
    StaticProvider,
    EntityStoreConfig, UIStateConfig, StateMachineConfig, RepositoryConfig,
    ViewModelConfig, ModuleConfig,
    UIStoreConfig,
} from '@farris/devkit-vue';
import { EntityStoreConfigBuilder } from './entity-store-config-builder';
import { RepositoryConfigBuilder } from './repository-config-builder';
import { UIStoreConfigBuilder } from './ui-store-config-builder';
import { StateMachineConfigBuilder } from './state-machine-config-builder';
import { ViewModelConfigBuilder } from './viewmodel-config-builder';

/**
 * 模块配置构造器
 */
class ModuleConfigBuilder {

    /**
     * 上下文
     */
    private context: any;

    /**
     * 构造函数
     */
    constructor(context: any) {
        this.context = context;
    }

    /**
     * 构造模块配置
     */
    public build() {
        const formMeta = this.context.form;
        const webCmdMetas = this.context.webcmds;
        const smMetas = this.context.stateMachines;

        const id = formMeta.module.code;
        const providers: StaticProvider[] = this.context.moduleProviders;
        const entityStoreConfigs = this.buildEntityStoreConfigs(formMeta);
        const uiStoreConfigs = this.buildUIStoreConfigs(formMeta);
        const stateMachineConfigs = this.buildStateMachineConfigs(formMeta, smMetas);
        const repositoryConfigs = this.buildRepositoryConfigs(formMeta);
        const viewModelConfigs = this.buildViewModelConfigs(formMeta, webCmdMetas, this.context.viewModelProviders);

        const moduleConfig: ModuleConfig = {
            id,
            providers,
            entityStores: entityStoreConfigs,
            uiStores: uiStoreConfigs,
            stateMachines: stateMachineConfigs,
            repositories: repositoryConfigs,
            viewModels: viewModelConfigs
        };

        return moduleConfig;
    }

    /**
     * 构造实体仓库配置
     */
    private buildEntityStoreConfigs(formMeta: any): EntityStoreConfig[] {
        const builder = new EntityStoreConfigBuilder(formMeta);
        const configs: EntityStoreConfig[] = [];

        const schemaNodes = formMeta.module.schemas;
        schemaNodes.forEach((schema: any) => {
            const config = builder.build(schema);
            configs.push(config);
        });

        return configs;
    }

    /**
     * 构造远程实体仓库配置
     */
    private buildRepositoryConfigs(formMeta: any): RepositoryConfig[] {
        const builder = new RepositoryConfigBuilder(formMeta);
        const configs: RepositoryConfig[] = [];
        const schemaNodes = formMeta.module.schemas as any[];
        schemaNodes.forEach((schemaNode: any) => {
            const config = builder.build(schemaNode);
            configs.push(config);
        });

        return configs;
    }

    /**
     * 构造UI仓库配置
     */
    private buildUIStoreConfigs(formMeta: any): UIStoreConfig[] {
        const builder = new UIStoreConfigBuilder(formMeta);
        const configs: UIStoreConfig[] = [];
        const viewModelNodes = formMeta.module.viewmodels as any[];
        viewModelNodes.forEach((viewModelNode) => {
            const config = builder.build(viewModelNode);
            configs.push(config);
        });

        return configs;
    }

    /**
     * 构造状态机配置
     */
    private buildStateMachineConfigs(formMeta: any, smMetas: any): StateMachineConfig[] {
        const builder = new StateMachineConfigBuilder(formMeta);
        const configs: StateMachineConfig[] = [];
        smMetas.forEach((smMeta: any) => {
            const config = builder.build(smMeta);
            if (!config) {
                return;
            }
            configs.push(config);
        });

        return configs;
    }

    /**
     * 构造视图模型配置
     */
    public buildViewModelConfigs(formMeta: any, webCmdMetas: any[], viewModelProviders: any[]): ViewModelConfig[] {
        const builder = new ViewModelConfigBuilder(formMeta, webCmdMetas, viewModelProviders);
        const configs: ViewModelConfig[] = [];
        const viewModelNodes = formMeta.module.viewmodels as any[];
        viewModelNodes.forEach((viewModelNode) => {
            const config = builder.build(viewModelNode);
            if (!config) {
                return;
            }
            configs.push(config);
        });

        return configs;
    }
}

export { ModuleConfigBuilder };
