import { CommandTaskConfig, CommandTaskParamConfig, CommandHandlerConfig, ViewModel } from '@farris/devkit-vue';

/**
 * 命令编排配置构造器
 */
class CommandHandlerConfigBuilder {

    /**
     * 表单元数据
     */
    private formMeta: any;

    /**
     * 表单元数据
     */
    private webCmdMetas: any[];

    /**
     * 构造函数
     */
    constructor(formMeta: any, webCmdMetas: any[]) {
        this.formMeta = formMeta;
        this.webCmdMetas = webCmdMetas;
    }

    /**
     * 构造命令处理方法
     */
    public build(commandNode: any): CommandHandlerConfig {
        const commandHandlerNode = this.getCommandHandlerNode(commandNode.cmpId, commandNode.id);
        const itemNodes = commandHandlerNode.Items;
        this.formatItemNodes(null, itemNodes);

        const taskConfigs: CommandTaskConfig[] = [];
        this.buildTaskConfigs(itemNodes, taskConfigs);

        const commandHandlerConfig: CommandHandlerConfig = {
            commandName: commandNode.code,
            tasks: taskConfigs
        };

        return commandHandlerConfig;
    }

    /**
     * 创建任务配置
     */
    private buildTaskConfigs(itemNodes: any[], taskConfigs: CommandTaskConfig[]) {
        itemNodes.forEach((itemNode: any, commandItemIndex: number) => {

            // 1=分支 2=分支集合
            if (itemNode.Type === 1 || itemNode.Type === 2) {
                return;
            }

            // 普通节点
            const paramConfigs = this.buildTaskParamConfigs(itemNode.ParamConfigs);

            const taskConfig: CommandTaskConfig = {
                name: itemNode.itemCode,
                service: itemNode.ComponentCode,
                method: itemNode.MethodCode,
                params: paramConfigs,
                links: []
            };

            // 自定义服务需要特殊处理
            const isCustomService = this.isCustomService(itemNode.ComponentPath);
            if (isCustomService === true) {
                taskConfig.service = this.getCustomServiceName(itemNode.ComponentCode);
                taskConfig.serviceUrl = this.getCustomServiceUrl(itemNode.ComponentPath, itemNode.ComponentCode);
                taskConfig.serviceDeps = this.getCustomServiceDeps();
            }

            taskConfigs.push(taskConfig);
            const nextItemNode = itemNodes[commandItemIndex + 1] || null;
            this.buildTaskLinkConfigs(itemNode, nextItemNode, taskConfig, taskConfigs);
        });

        return taskConfigs;
    }

    /**
     * 是否是自定义服务
     */
    public isCustomService(componentPath: string): boolean {
        if (!componentPath.startsWith('Gsp')) {
            return true;
        }
        return false;
    }

    /**
     * 构造自定义服务名
     */
    private getCustomServiceName(componentCode: string): string {
        // JiwtSimpleListCard_mfrm_Controller
        // JiwtSimpleListCardMfrmControllerService
        const codeSegs = componentCode.split('_');
        const pascalCaseParts = codeSegs.map((codeSeg) => {
            return codeSeg.charAt(0).toUpperCase() + codeSeg.slice(1);
        });
        const pascalCaseStr = pascalCaseParts.join('');
        return pascalCaseStr + 'Service';
    }

    /**
     * 构造自定义服务地址
     */
    private getCustomServiceUrl(componentPath: string, componentCode: string): string {
        // Jiwt/JiwtSimpleModule/JiwtSimpleApp/bo-jiwtsimpleapp-front/metadata/components
        // /apps/jiwt/jiwtsimplemodule/mob/bo-jiwtsimpleapp-front
        const pathSegs = componentPath.split('/');
        const app = pathSegs[0];
        const su = pathSegs[1];
        const bo = pathSegs[2];
        const proj = pathSegs[3];

        const serviceUrl = `/apps/${app}/${su}/web/${proj}/${componentCode}.js`.toLowerCase();
        return serviceUrl;
    }

    /**
     * 获取自定义服务的依赖
     */
    private getCustomServiceDeps(): any[] {
        const serviceDeps = [ViewModel];
        return serviceDeps;
    }

    /**
     * 创建任务参数配置
     */
    public buildTaskParamConfigs(paramNodes: any[]): CommandTaskParamConfig[] {
        const paramConfigs: CommandTaskParamConfig[] = [];
        paramNodes.forEach((paramNode: any) => {
            const paramConfig: CommandTaskParamConfig = {
                name: paramNode.ParamCode,
                value: paramNode.ParamExpress
            };
            paramConfigs.push(paramConfig);
        });

        return paramConfigs;
    }

    /**
     * 构造任务连接配置
     */
    private buildTaskLinkConfigs(currentItemNode: any, nextItemNode: any, currentTaskConfig: any, taskConfigs: CommandTaskConfig[]) {
        if (!nextItemNode) {
            nextItemNode = this.getNextItemNodeFromParent(currentItemNode);
            if (!nextItemNode) {
                return [];
            }
        }

        // 任务节点
        if (nextItemNode.Type === 0) {
            currentTaskConfig.links.push({
                nextTaskName: nextItemNode.itemCode,
                conditions: true
            });
            return;
        }

        // 分支节点
        if (nextItemNode.Type === 2) {
            const caseItemNodes = nextItemNode.Items as any[];
            caseItemNodes.forEach((caseItemNode) => {
                if (!caseItemNode || !caseItemNode.Items || !caseItemNode.Items[0]) {
                    return;
                }
                currentTaskConfig.links.push({
                    nextTaskName: caseItemNode.Items[0].itemCode,
                    conditions: caseItemNode.Express
                });

                // 递归处理分支下的任务节点
                this.buildTaskConfigs(caseItemNode.Items, taskConfigs);
            });
        }
    }

    /**
     * 从父层级中获取下一节点
     */
    private getNextItemNodeFromParent(itemNode: any) {
        let nextItemNode = null;
        let parentItemNode = itemNode.__parent__;
        while(!nextItemNode && parentItemNode) {
            if (parentItemNode.__nextSlibing__ && parentItemNode.__nextSlibing__.Type === 0) {
                nextItemNode = parentItemNode.__nextSlibing__;
            }
            parentItemNode = parentItemNode.__parent__;
        }

        return nextItemNode;
    }

    /**
     * 从命令构件元数据中获取命令编排信息
     */
    private getCommandHandlerNode(webCmdId: string, commandId: string) {
        const commandHandlerName = this.getCommandHandlerName(webCmdId, commandId);
        const targetWebCmdMeta = this.webCmdMetas.find((webCmdMeta) => {
            return webCmdMeta.Content.Id === webCmdId;
        });

        if (!targetWebCmdMeta) {
            throw new Error(`WebCmdMeta(Id=${webCmdId}) does not exist`);
        }

        const targetCommandHandler = targetWebCmdMeta.Content.Commands.find((commandHandler: any) => {
            return commandHandler.Code === commandHandlerName;
        });
        if (!targetCommandHandler) {
            throw new Error(`Command(Code=${commandHandlerName}) does not exist in WebCmdMeta(Id=${webCmdId})`);
        }

        return targetCommandHandler;
    }

    /**
     * 获取命令处理器名称
     */
    private getCommandHandlerName(webCmdId: string, commandName: string) {
        const webCmds = this.formMeta.module.webcmds as any[];
        const targetWebCmd = webCmds.find((webCmd) => {
            return webCmd.id === webCmdId;
        });

        const refedHandlers = targetWebCmd.refedHandlers as any[];
        const targetRefHandler = refedHandlers.find((refedHandler: any) => {
            return refedHandler.host === commandName;
        });

        return targetRefHandler.handler;
    }

    /**
     * 加工节点，建立父子关系
     */
    private formatItemNodes(parentItemNode: any, itemNodes: any[]) {
        if (!Array.isArray(itemNodes)) {
            return [];
        }

        itemNodes.forEach((itemNode, itemIndex) => {
            itemNode.__parent__ = parentItemNode;
            itemNode.__preSlibing__ = itemNodes[itemIndex -1] || null;
            itemNode.__nextSlibing__ = itemNodes[itemIndex + 1] || null;
            if (Array.isArray(itemNode.Items)) {
                this.formatItemNodes(itemNode, itemNode.Items);
            }
        });
    }

}

export { CommandHandlerConfigBuilder };
