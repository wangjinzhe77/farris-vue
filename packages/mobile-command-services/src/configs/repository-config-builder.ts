import { Module, RepositoryConfig } from '@farris/devkit-vue';
import { BefRepository } from '@farris/bef-vue';

/**
 * 远程实体仓库配置构造器
 */
class RepositoryConfigBuilder {

    /**
     * 表单元数据
     */
    private formMeta: any;

    /**
     * 构造函数
     */
    constructor(formMeta: any) {
        this.formMeta = formMeta;
    }

    /**
     * 构造配置
     */
    public build(schema: any): RepositoryConfig {
        const config =  {
            id: this.formMeta.module.code + '-repository',
            type: BefRepository,
            deps: [ Module ],
            isDynamic: true,
            entityStore: this.formMeta.module.code + '-entitystore',
            baseUrl: '/' + schema.sourceUri.toLowerCase()
        };

        return config;
    }
}

export { RepositoryConfigBuilder };
