import './assets/main.css';

import { createApp } from 'vue';
import FarrisMobileUI from '@farris/mobile-ui-vue';
import '@farris/mobile-ui-vue/style.css';
import { createDevkit } from '@farris/devkit-vue';
import { befRootProviders } from '@farris/bef-vue';
import App from './app.vue';
import router from './router';

const app = createApp(App);
app.use(router);

// Farris Mobile UI
app.use(FarrisMobileUI);

// devkit
const devkit = createDevkit({
    providers: [
        befRootProviders
    ]
});
app.use(devkit);

app.mount('#app');
