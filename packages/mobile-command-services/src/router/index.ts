import { createRouter, createWebHistory } from 'vue-router';
import { dynamicRoutes} from '../views/dynamic-demo/routes';
import { listcardRoutes } from '../views/list-card-demo/routes';

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            redirect: '/dynamic'
        },
        dynamicRoutes,
        listcardRoutes
    ]
});

export default router;
