import Index from './Index.vue';

const dynamicRoutes = {
    path: '/dynamic',
    component: Index
};

export { dynamicRoutes };
