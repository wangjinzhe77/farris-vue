import { StaticProvider, ViewModel } from '@farris/devkit-vue';
import { CustomLoadDataService } from './custom-load-data.service';
import { CustomSaveDataService } from './custom-save-data.service';
import { CustomTestService } from './custom-test.service';

const customServiceProviders: StaticProvider[] = [
    { provide: CustomLoadDataService, useClass: CustomLoadDataService, deps: [ ViewModel ] },
    { provide: CustomSaveDataService, useClass: CustomSaveDataService, deps: [ ViewModel ] },
    { provide: CustomTestService, useClass: CustomTestService, deps: [ ViewModel ] }
];

export { customServiceProviders };
