import { ViewModelState, ViewModel } from '@farris/devkit-vue';
import { BaseDataService } from '../../../../lib/index';

/**
 * 自定义测试服务
 */
class CustomTestService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     * 测试方法
     */
    public test() {
        const oldName = this.viewModel.entityStore?.getValueByPath('/name').split('-')[0];
        const random = Math.round(Math.random() * 10000);
        const newName = oldName + '-' + random;
        this.viewModel.entityStore?.setValueByPath('/name', newName);
    }
}

export { CustomTestService };
