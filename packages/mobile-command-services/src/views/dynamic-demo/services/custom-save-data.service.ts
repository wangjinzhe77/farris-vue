import { ViewModelState, ViewModel } from '@farris/devkit-vue';
import { BaseDataService } from '../../../../lib/index';

/**
 * 自定义数据保存服务
 */
class CustomSaveDataService extends BaseDataService {

    /**
     * 构造函数
     */
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     * 保存
     */
    public save(): Promise<boolean> {
        const savePromise = this.repository.saveEntityChanges().then((result) => {
            return result;
        });

        return savePromise;
    }
}

export { CustomSaveDataService };
