import { ModuleConfig, Module } from '@farris/devkit-vue';
import { befProviders, BefRepository } from '@farris/bef-vue';
import { commandServiceModuleProviders, commandServiceViewModelProviders} from '../../../lib/index';
import { customServiceProviders } from './services/index';

export const dynamicModuleConfig: ModuleConfig = {

    id: 'dynamic-module',

    providers: [
        ...befProviders,
        ...commandServiceModuleProviders
    ],

    viewModels: [
        {
            id: 'dynamic-viewmodel',
            entityStore: 'dynamic-entity-store',
            uiStore: 'dynamic-ui-store',
            repository: 'dynamic-repository',
            providers: [
                ...commandServiceViewModelProviders,
                ...customServiceProviders
            ],
            commands: [
                {
                    name: 'load',
                    params: [],

                },
                {
                    name: 'save',
                    params: []
                },
                {
                    name: 'test',
                    params: []
                }
            ],
            commandHandlers: [
                {
                    commandName: 'load',
                    tasks: [
                        {
                            name: 'load',
                            service: 'CustomLoadDataService',
                            method: 'load',
                            params: [],
                        }
                    ]
                },
                {
                    commandName: 'save',
                    tasks: [
                        {
                            name: 'save',
                            service: 'CustomSaveDataService',
                            method: 'save',
                            params: []
                        }
                    ]
                },
                {
                    commandName: 'test',
                    tasks: [
                        {
                            name: 'test',
                            service: 'CustomTestService',
                            method: 'test',
                            params: []
                        }
                    ]
                }
            ]
        }
    ],

    repositories: [
        {
            id: 'dynamic-repository',
            type: BefRepository,
            deps: [ Module ],
            isDynamic: true,
            entityStore: 'dynamic-entity-store',
            baseUrl: '/api/jiwt/jiwtsimplemodule/v1.0/jiwtsimplelistcard_mfrm'
        }
    ],

    entityStores: [
        {
            id: "dynamic-entity-store",
            state: {
                entity: {
                    idKey: 'id',
                    fields: [
                        { name: 'id', type: 'Primitive' },
                        { name: 'code', type: 'Primitive' },
                        { name: 'name', type: 'Primitive' },
                    ]
                }
            }
        }
    ],

    uiStores: [
        {
            id: 'dynamic-ui-store',
            state: {
                props: [
                    { name: 'action' },
                ]
            }
        },
    ],

    stateMachines: []
};
