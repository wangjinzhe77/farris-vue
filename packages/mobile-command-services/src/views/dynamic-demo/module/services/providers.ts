import { StaticProvider, ViewModel } from '@farris/devkit-vue';
import { LoadDataService } from './load-data.service';
import { SaveDataService } from './save-data.service';
import { TestService } from './test.service';

const serviceProviders: StaticProvider[] = [
    { provide: LoadDataService, useClass: LoadDataService, deps: [ ViewModel ] },
    { provide: SaveDataService, useClass: SaveDataService, deps: [ ViewModel ] },
    { provide: TestService, useClass: TestService, deps: [ ViewModel ] }
];

export { serviceProviders };
