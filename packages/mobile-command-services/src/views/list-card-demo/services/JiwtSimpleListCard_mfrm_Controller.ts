 import { ViewModelState, ViewModel } from '@farris/devkit-vue';
import { BaseDataService } from '../../../../lib/index';

export class JiwtSimpleListCard_mfrm_Controller extends BaseDataService {
    constructor(viewModel: ViewModel<ViewModelState>) {
        super(viewModel);
    }

    /**
     * 方法A
     */
    public methodA() {
        console.log('methodA');
    }

    /**
     * 方法B
     */
    public methodB() {
        console.log('methodB');
    }

    /**
     * 方法C
     */
    public methodC() {
        console.log('methodC');
    }

    /**
     * 方法D
     */
    public methodD() {
        console.log('methodD');
    }

    /**
     * 方法E
     */
    public methodE() {
        console.log('methodE');
    }

    /**
     * 方法F
     */
    public methodF() {
        console.log('methodF');
    }

    /**
     * 方法G
     */
    public methodG() {
        console.log('methodG');
    }

    /**
     * 方法H
     */
    public methodH() {
        console.log('methodH');
    }

    /**
     * 方法I
     */
    public methodI() {
        console.log('methodI');
    }

    /**
     * 方法J
     */
    public methodJ() {
        console.log('methodJ');
    }

    /**
     * 方法K
     */
    public methodK() {
        console.log('methodK');
    }
}
