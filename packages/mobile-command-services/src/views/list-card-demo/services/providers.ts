 import { StaticProvider, ViewModel } from '@farris/devkit-vue';
import { JiwtSimpleListCard_mfrm_Controller } from './JiwtSimpleListCard_mfrm_Controller';

const customServiceProviders: StaticProvider[] = [
    { provide: JiwtSimpleListCard_mfrm_Controller, useClass: JiwtSimpleListCard_mfrm_Controller, deps: [ViewModel] }
];

export { customServiceProviders };
