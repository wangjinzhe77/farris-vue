import formMeta from './list-card-form.json';
import listSmMeta from './list-statemachine.json';
import cardSmMeta from './card-statemachine.json';
import customWebCmdMeta from './list-card-controller-webcmd.json';

const stateMachineMetas = [
    listSmMeta,
    cardSmMeta,
];

export { formMeta, stateMachineMetas, customWebCmdMeta };
