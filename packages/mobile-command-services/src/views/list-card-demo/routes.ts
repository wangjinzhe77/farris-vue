import Index from './Index.vue';
import ListPage from './ListPage.vue';
import CardPage from './CardPage.vue';

const listcardRoutes = {
    path: '/listcard',
    component: Index,
    children: [
        {
            path: '',
            component: ListPage,
        },
        {
            path: 'list',
            component: ListPage,
        },
        {
            path: 'card',
            component: CardPage
        }
    ]
};

export { listcardRoutes };
