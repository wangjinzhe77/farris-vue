import addCommands from './webcmds/AddCommands.json';
import cancelCommands from './webcmds/CancelCommands.json';
import discussionGroupCommands from './webcmds/DiscussionGroupCommands.json';
import editCommands from './webcmds/EditCommands.json';
import loadCmdsMeta from './webcmds/LoadCommands.json';
import loadPageCmdsMeta from './webcmds/LoadPageCommands.json';
import navigateCommands from './webcmds/NavigateCommands.json';
import removeCommands from './webcmds/RemoveCommands.json';
import saveCommands from './webcmds/SaveCommands.json';
import stateMachineCommands from './webcmds/StateMachineCommands.json';
import uiCommands from './webcmds/UICommands.json';
import updateCommands from './webcmds/UpdateCommands.json';
import viewCommands from './webcmds/ViewCommands.json';
import voVariableService from './webcmds/VoVariableService.json';

import mobileAttachmentCmd from './webcmds/MobileAttachmentCmd.json';
import approveService from './webcmds/ApproveService.json';

const webCmdMetas = [
    addCommands,
    cancelCommands,
    discussionGroupCommands,
    editCommands,
    loadPageCmdsMeta,
    loadCmdsMeta,
    navigateCommands,
    removeCommands,
    saveCommands,
    stateMachineCommands,
    uiCommands,
    updateCommands,
    viewCommands,
    voVariableService,
    mobileAttachmentCmd,
    approveService
];

export { webCmdMetas };
