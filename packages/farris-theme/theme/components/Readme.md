# Farris 主题

### components 文件夹

| 文件夹               | 组件                                   | 使用状态 | 依赖                   |
| :------------------- | :------------------------------------- | :------- | :--------------------- |
| accordion            | 手风琴                                 |          | Icon                   |
| appointment-calendar | 安排日历                               | -----    |                        |
| avatar               | 头像                                   |          | Icon                   |
| badge                | 徽标                                   | 界面     |                        |
| button               | 按钮                                   |          |                        |
| button-group         | 按钮组                                 |          | Button、Icon           |
| button-edit          | Button Edit                            |          | InputGroup、Tags、Icon |
| calculator           | 计算器                                 |          |                        |
| calendar             | 日历                                   |          | Button、Icon           |
| capsule              | 胶囊按钮                               |          |                        |
| checkbox             | 多选                                   |          | Icon                   |
| collapse-scroll      | 滚动收折                               | ---      |                        |
| color-picker         | 颜色选择器                             |          | Input                  |
| combolist            | 下拉选择输入框,未被使用                |          |                        |
|                      | 依赖 Button Edit、Listview 组件        |          |                        |
| condition-list       | 条件列表                               |          |                        |
| context-menu         | 右键菜单                               | ----     |                        |
| data-grid            | 表格                                   |          |                        |
| datagrid-settings    | 表格配置                               | -----    |                        |
| datepicker           | 日期选择器                             |          |                        |
| dial                 | 仪表盘                                 |          |                        |
| discussion           | 评论区                                 | -----    |                        |
| dropdown             | 下拉菜单                               |          |                        |
| filter               | Filter Bar 筛选条                      |          |                        |
| footer               | Page Footer 页脚                       |          |                        |
| form                 | 表单布局                               |          |                        |
| icon                 | 部分图标定义、图标按钮                 |          |                        |
| image-cropper        | 图像裁剪                               |          |                        |
| input                | 输入框                                 |          |                        |
| input-group          | 输入框组                               |          | Input、Icon            |
| input-append         | 输入控件扩展                           | -----    |                        |
| layout               | Layout 布局                            |          |                        |
| list-nav             | ListNav 列表导航 实际依赖 页面模版样式 |          |                        |
| listview             | 列表视图                               |          |                        |
| loading              | 加载                                   |          |                        |
| message              | Message Box 消息弹窗                   |          |                        |
| modal                | Modal 弹出窗口                         |          |                        |
| nav                  | Nav 导航条                             |          |                        |
| notify               | Notify 通知消息                        |          |                        |
| number-spinner       | 数字输入框                             |          |                        |
| order                | 排序                                   |          |                        |
| pagination           | 分页                                   |          |                        |
| popover              | 冒泡提示                               |          |                        |
| process              | 流程进度                               | -----    |                        |
| progress             | 进度条                                 |          |                        |
| progress-step        | 步骤条                                 |          |                        |
| rate                 | 评分                                   |          |                        |
| radio                | 单选                                   |          | Checkbox、Icon         |
| response-toolbar     | 响应式工具栏                           |          |                        |
| scheme               | 条件列表                               |          |                        |
| scrollspy            | 滚动监听                               | -----    |                        |
| searchbox-panel      | 查询面板                               | -----    |                        |
| section              | 面板                                   |          |                        |
| sidebar              | 侧边栏                                 | -----    |                        |
| static-text          | 静态文本                               |          |                        |
| switch               | 开关                                   |          |                        |
| table                | 基础表格                               |          |                        |
| tabs                 | 标签页                                 |          |                        |
| tags                 | 标签                                   |          |                        |
| time-picker          | 时间选择器                             |          |                        |
| tooltip              | 提示信息                               |          |                        |
| transfer             | 穿梭框                                 |          |                        |
| upload               | 附件上传                               |          |                        |
| utils                | 通用样式                               |          |                        |
| verify-detail        | 验证信息                               |          |                        |
| view-change          | 视图切换                               | -----    |                        |
