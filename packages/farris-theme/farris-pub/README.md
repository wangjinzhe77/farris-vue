# 公共样式概述
1. 公共样式，主要是提供给第三方使用，使项目上的页面结构不变，界面效果随主题变化。
2. 在不同主题下变量名、样式名恒定，色值跟随主题变化。样式对应的颜色依据展示分为边线色、文本色、图标色、背景色、语义色，在不同分类下又会有不同的状态色、交互色。
3. index.html作为展示界面，提供不同主题下的变量名、样式名与色值的对应关系，支持主题切换、查询功能。
4. index.html作为站点的入口页，通过命令gulp pubsite 可以生成带默认主题公共样式的一套站点文件

## 一、命令
### 生成公共样式命令
- 命令：gulp pubcss --code 主题的编号
- 生成目录：farris-pub/css/theme/主题的编号/farris-pub.css
### 生成已有主题样式
- cmd 到当前目录
- 在cmd中输入 ./farris-pub.sh 回车运行
- 生成所有主题的公共样式

### 生成站点样式命令
- 命令：gulp pubdocs
- 生成目录：dist/farris-pub/css/docs.css

### 更新站点命令
- 命令：gulp pubsite
- 生成目录：dist/farris-pub
- 把公共样式、站点样式、站点脚本、站点图片拷贝到生成目录
- 其中公共样式只包含默认主题、站点脚本未压缩

### 发布站点命令
- 命令：gulp pubsite:publish
- 生成目录：dist/farris-pub
- 把版本、说明、公共样式、站点样式、站点脚本、站点图片拷贝到生成目录
- 其中公共样式只包含默认主题、站点脚本已压缩
- 定位到dist/farris-pub,然后执行npm publish --access=public

## 二、目录
```
farris-pub
|
├── docs/ 站点样式文件
|   │
|   ├── ...样式定义
|   └── docs.scss 站点样式统一出口文件
|
├── public/ 公共样式定义
|   │
│   ├── themes/ 不同主题
│   │   │ 
|   |   ├── default 默认商务风
│   │   │   └──_index.scss 引入商务风相关变量
│   │   │  
|   |   ├── ... 样式主题
│   │   |   └──_index.scss 引入样式主题变量
│   │    
│   ├── _mixins.scss 样式函数定义
│   ├── _variables.scss 追加的变量定义
│   ├── farris-pub.scss 公共样式入口
│   └── setting.json 公共样式支持主题的配置
│   
├── site/ 公共样式站点
|   |
│   ├── imgs/ 图片资源
|   |
│   ├── js/ 站点脚本
│   │   │ 
│   │   ├── preset.js 预置脚本变量
│   │   ├── docs.js 站点交互逻辑定义
│   │   └──jquery.min.js
│      
├── package.json 公共样式离线包版本    
├── README.md 公共样式离线包说明

```
## 三、public/settings.json 详解
主题配置文件，配置不同主题。里面的配置节点被用于生成不同主题公共样式的命令

```
[
    ...
   {
    "theme": "mimicry",
    "types": ["default"],
    "dist": "dist/farris-pub/css/themes/mimicry-default"
  }
    ...
]
```
属性说明
- theme 主题的编号
    - 这个编号与主题样式定义的文件夹一致
    - 对应生成主题公共样式命令中的code参数
    - 比如生成拟物风蓝色主题，gulp pubcss --code mimicry 

- types 因为同一主题不同尺寸对应的的公共样式是一致的，所以此处只有default一个值

- dist 指定该主题公共样式文件的生成目录


## 四、如何新增一个主题的公共样式？
1. 假定新增主题编号 test-green，指定生成目录是test-green
2. 修改 public/setting.json，追加节点
```
 {
    "theme": "test-green",
    "types": ["default"],
    "dist": "dist/farris-pub/css/themes/test-green"
}
```
3. 拷贝themes/green 文件夹，重名为test-green
    - 在_index.scss 中可通过import引入颜色变量定义 或者直接写变量定义

4. 执行命令 gulp pubcss --code test-green 
    - 生成文件在dist/farris-pub/css/themes/test-green/farris-pub.css

5. 可修改farris-pub.sh，追加该主题的生成，方便使用

## 五、如何调整公共样式？
1. [与设计组约定的变量](https://docs.qq.com/sheet/DRG1zcXFCa2RCa25y?newPad=1&newPadType=clone&tab=erp3e3)
2. 公共样式的入口在public/farris-pub.scss中 
3. 根据变量的生成规则，在public/_mixins.scss定义对应的函数
4. 定义变量$fp-border，通过样式函数 common 指定的规则生成样式
 ![avatar](./imgs/pub-css.png)
5. 可以在现有变量的基础上增加 序号和对应的颜色变量。
6. 如果觉得这个逻辑麻烦，也可以追加样式。要注意！！在index.html是要显示和查询所有样式的，同步修改site/js里脚本的逻辑。

## ToDOList
1. 切换暗黑的时候文档样式应该也变成暗黑的，这样好识别下面的色块
