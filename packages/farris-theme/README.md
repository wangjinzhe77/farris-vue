<p align="center">
  <a href="#" target="_blank" rel="noopener noreferrer">
    <img alt="Farris UI Logo" src="../../farris_design.jpg"  style="max-width:50%;">
  </a>
</p>

<h1 align="center">Farris Theme</h1>

<p align="center">Farris Theme 是基于 Farris Design 的前端主题库。</p>

# Farris 主题

    Farris提供了拟物风格的界面，颜色：蓝色，尺寸：紧凑版

-   ToDoList 支持三种颜色:蓝色（默认颜色）、绿色、红色；
-   ToDoList 两种尺寸:紧凑版（默认尺寸）、宽松版
-   ToDoList 部分组件样式颜色没有使用定义的变量

## 1. 主题文件

### 1.1 主题文件生成

1. 安装本工程的包:

    ```
    npm install
    ```

2. 生成主题:

    ```
    npm run theme
    ```

    会在当前目录生成/dist/themes/theme-default 主题

3. 用于 ui-vue 打包组件生成
    ```
    npm run theme
    npm run uipack
    ```
    会在当前目录生成/dist/themes/theme-default 主题，并且拷贝到目录 ui-vue/package 下

4. 打包ui-vue组件
   ```
    npm run uilib
    ```
会在packages/ui-vue/生成文件夹package,包含组件定义和样式定义
#### 1.2 生成内容结构

1. dist/themes/theme-default theme-default 是默认主题生成的文件夹，后续会支持 theme-green 等其他颜色主题的生成

2. 生成目录含义

    ````
    theme-default/
    │
    ├── components/ 组件样式，比如基础样式、标签页、按钮、树组件等
    │
    ├── interface/ 非组件类的界面展示样式，比如页面模版、面包屑导航、输入控件等
    │
    ├── farris-all.css 所有样式合集
    │
    ├── farris-components.css 所有components下样式的合并
    │
    ├── farris-interface.css 所有interface下样式的合并

     ```
    ````

3. 文件生成后的路径在:

    ```
    packages/farris-theme/src/assets/themes/主题风格-颜色
    ```

### 1.3 样式定义

1. 样式目录含义

    ````
    theme/
    │
    ├── assets/ 通用的资源
    │
    ├── common/ 定义公共变量、组件变量和通用的方法
    │
    ├── commponents/ 组件定义
    │
    ├── entrance/ 入口文件定义
    │
    ├── interface/ 非组件类的界面展示样式定义
    │
    ├── iteration 依赖的第三方组件
    │
    ├── farris-interface.css 所有interface下样式的合并

     ```

    ````

2. 主题目录含义

    ````
    themes/
    │
    ├── default/ 拟物风蓝色主题
    │     │
    │     ├── assets 图片类资源
    │     │
    │     ├── base 变量相关
    │     │     │
    │     │     ├──color 颜色变量
    │     │     │
    │     │     ├──size 尺寸变量
    │     │
    │     ├── default 默认紧凑尺寸
    │     │     │
    │     │     ├──extend 扩展样式，自定义某些样式的入口文件
    │     │     │
    │     │     ├──index 所有样式的入口文件
    │     │
    │     ├── loose 宽松尺寸
    │
    ├── default-dark/ 拟物风黑色主题
    │
    ├── default-green/ 拟物风绿色主题
    │
    ├── default-red/ 拟物风红色主题
    │
    ├── interface/ 非组件类的界面展示样式定义
    │
    ├── iteration 依赖的第三方组件
    │
    ├── farris-interface.css 所有interface下样式的合并

     ```
    ````


### 1.4 切换运行时主题
1. 跟主题颜色相关的变量，定义在文件farris-theme/theme/common/base/_color.scss
2. 颜色变量生成在:root下，定义在文件farris-theme/theme/iteration/_root.scss
3. 运行时追加样式文件，重置之前的颜色变量，更改主题
```
:root{
    变量名称:颜色定义,
    --f-theme-01:red,
    --f-theme-02:green,
}
```

### 1.5 Vue 组件引入样式的方式

1. 以 button-group 组件为例，目录 ui-vue/components/button-group
2. 增加 style 文件夹，增加 style/css.ts 文件
3. 定义 css.ts 文件

```
import "@farris/ui-vue/button/style/css";
import "@farris/ui-vue/theme-default/components/button-group.css";
import "@farris/ui-vue/dependent-icon/style/css";
```
- 依赖组件button、dependent-icon的样式
- 依赖button-group.css的样式定义

4. 特殊的样式空组件
- 有以dependent-开头的组件，内部只有style/css.ts
- 这些是为了细化拆分大样式文件，而拆分出的只有样式定义的组件

## 2. 公共样式文件

### 2.1 公共样式界面示例

当前可生成的公共样式包括:商务风-蓝色、绿色、红色主题，拟物风-蓝色、绿色、红色主题；暗黑主题将在后续迭代更新

<p align="center">
    <img src="./img/public.png"  style="max-width:60%;">
</p>

### 2.2 公共样式生成

#### 2.2.1 初次生成

1. 生成默认公共样式

    在终端输入如下命令来生成默认公共样式（即商务风下的蓝色主题公共样式）:

    ```
    gulp pubsite
    ```

#### 2.2.2 后续生成

1. 生成所有公共样式

    在终端输入如下命令来生成所有主题样式:

    ```
    ./farris-pub.sh
    ```

2. 生成特定公共样式
    ```
    gulp  pubcss --code 主题风格
    ```
    例:（具体示例可参考 farris-pub.sh）
    ```
    gulp pubcss --code mimicry-red 生成拟物风红色主题公共样式
    ```
3. 文件生成后的路径在:

    ```
    packages/farris-theme/dist/farris-pub
    ```

    双击 `index.html` 文件，可在浏览器中查看所有生成的公共样式若某个风格下的样式还没有生成，则该风格下的色值展示将全部置灰。

4. 重新运行如下命令，刷新页面，即可查看

    ```
    ./farris-pub.sh
    ```

## 3. 字体图标文件

#### 3.1 字体文件目录

字体图标相关定义都在文件夹 `packages/farris-theme/farris-icon` 下，其目录如下:

```
farris-icon/
│
├── commpatible/ 处理图标兼容
│
├── dist/ 生成的图标NPM包文件，用于发布
│
├── entrance/ 入口文件
|   |
│   ├──farrisicon.html NPM包中用于说明、展示和查询图标的页面
|   └──farrisicon.scss 图标样式文件的入口，生成命令会调用此文件
│
├──scss
|   |
│   ├── extend/
|   |   |
│   │   ├── _icons.scss 字体图标的样式
│   │   ├── _varriables.scss 相关变量
│   │   └── farrisicon-extend.ttf 字体图标文件
|   |
│   ├── farris/ farris字体图标
|   |   |
│   │   ├── _basic.scss 字体family的定义
│   │   ├── _new.scss 新追加的Farris字体图标样式
│   │   ├── _rewrite.scss Farris字体图标库
│   │   ├── _variables.scss 相关变量
│   │   └── farrisicon.ttf farris字体图标文件
│   │
│   ├──package.json NPM包中的版本号
|   └──readme.md NPM包中的说明文件
```

#### 3.2 使用已有图标

1. 字体图标文件介绍：

    Farris 主题内置了字体图标文件,其中包括三个主要文件: `farrisicon.css`,`farrisicon-extend.ttf`,`farrisicon.ttf`。

2. 如何使用字体图标文件

    双击`packages/farris-theme/farris-icon/dist`文件下的`farrisicon.html`可以查看目前已有的 farris 图标并进行使用。

    具体使用步骤可以在`farrisicon.html`文件中查看。

#### 3.3 新增图标

若用户想要`新增图标`，则可以按照如下步骤生成:

1. 提前确定好新增的`样式名`和`图标编号`

2. 由设计组同事提供最新的图标文件，覆盖现有的 `farrisicon.ttf`。文件具体位置在:`packages/farris-theme/farris-icon/scss/farris/farrisicon.ttf`

3. 在 `packages/farris-theme/farris-icon/scss/farris/_new.scss` 中参照之前的写法新增样式定义。此次需要重新命名图标，并增加 `font-family`。

4. 在 `packages/farris-theme/farris-icon/entrance/farrisicon.html` 的底部`icon`数组中，增加样式名称；

5. 处理图标兼容

    处理 `farrisicon.ttf`文件在 IE 浏览器上的兼容问题，需下载`ttfpatch.exe`，经转化文件后的`xx.ttf`文件可支持 IE 浏览器。

    具体操作方式如下:

    a. 在`ttfpatch.exe`上右击`属性`-> `兼容性` ->勾选`以兼容模式运行这个程序`，点击`应用`

    b. `cmd` 打开命令窗口，定位到 `ttfpatch.exe` 所在的目录

    c. 执行如下命令

    ```
    ttfpatch.exe ../scss/farris/farrisicon.ttf
    ```

    d. `farris/farrisicon.ttf`的修改日期变化，说明生成成功

6. 执行命令

    ```
    gulp farrisicon
    ```

    生成目录:`farris-icon/dist`

    在 `dist` 文件的 `html` 中验证是否无误

7. 因为图标的变动影响所有主题，需要执行如下命令重新生成所有主题:

    ```
    themes.sh
    ```

## 4. 更新日志

见[log](log.md)文件
