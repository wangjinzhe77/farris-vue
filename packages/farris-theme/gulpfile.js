const gulp = require('gulp'); // 引入 gulp
const minifycss = require('gulp-minify-css'); // 引入 gulp-uglify 插件
const uglify = require('gulp-uglify');
const clean = require('gulp-clean');
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const magicImporter = require('node-sass-magic-importer');
const autoprefixer = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const through2 = require('through2');
const minimist = require('minimist');
const path = require('path');
const fs = require('fs');
const inlineBase64 = require('gulp-inline-base64');
const sourceDir = path.resolve('./');
// 决定是否把图片、字体改成dataurl的写法
// 用于格式化命令参数
const knownOptions = {
  default: {
    code: 'default',
    type: 'default'
  }
};

/** *****************************
如果命令是 gulp theme --code mimicry --type default --base64 true
process.argv:[node路径,gulp路径,'theme','--code','mimicry','--type','default','--base64','true']
process.argv.slice(2): 执行结果是 ['theme','--code','mimicry','--type','default','--base64','true']
minimist:执行结果{_:['theme'],code:'mimicry','type':'default','base64':'true'}
*******************************/
const options = minimist(process.argv.slice(2), knownOptions);
const showInlineBase64 = options.hasOwnProperty('base64') ? options['base64'] == 'true' : true;

/** *****************************
    farris icon 图标库文件  ***********************************************开始****************************
*******************************/
function transferToBase64(stream, fileBaseDir) {
  if (showInlineBase64) {
    return stream.pipe(inlineBase64({
      baseDir: fileBaseDir,
      maxFileSize: 1024 * 200,
      debug: true
    }));
  }
  return stream;
}
const iconSourceDir = sourceDir + '/farris-icon/';
gulp.task('icon-css', function (param) {
  let stream = gulp
    .src(`${iconSourceDir}entrance/farrisicon.scss`)
    .pipe(
      sass({
        importer: magicImporter(),
        'output-style': 'expanded',
        precision: 5
      }).on('error', sass.logError)
    );
  // 路径是否转base64
  stream = transferToBase64(stream, iconSourceDir + '/farris');
  return stream.pipe(minifycss()).pipe(gulp.dest(`${iconSourceDir}dist/`));
});

gulp.task('icon-copy', function () {
  const sourceFiles = [
    `${iconSourceDir}farris/farrisicon.ttf`,
    `${iconSourceDir}entrance/farrisicon.html`
  ];
  if (showInlineBase64) {
    sourceFiles.shift();
  }
  return gulp
    .src(sourceFiles)
    .pipe(gulp.dest(`${iconSourceDir}dist/`));
});

gulp.task('icon-assets', function (done) {
  if (showInlineBase64) {
    return done();
  } else {
    return gulp
      .src([
        `${iconSourceDir}farris/farrisicon.ttf`
      ])
      .pipe(gulp.dest(sourceDir + '/theme/assets')).on('end', done);
  }
});
gulp.task('icon-clean', function () {
  return gulp.src([sourceDir + 'farris-icon/dist/*']).pipe(clean({ force: true }));
});
// 生成样式
gulp.task(
  'farrisicon',
  gulp.series('icon-clean', 'icon-css', 'icon-copy', 'icon-assets')
);

/** *****************************
    farris icon 图标库文件  ***********************************************结束****************************
*******************************/

/** *****************************
    Farris 主题样式生成  ***********************************************开始****************************
*******************************/
// 定义主题
let themeDetail = {};

/** 控制参数 ****************************开始***************/
const srcDir = path.join(sourceDir, 'theme/entrance');
const baseDistDir = path.join(sourceDir, 'dist/themes');
let distDir = baseDistDir;
const baseThemes = "themes/";
/** 控制参数 *******************************结束*****************/

// 读取配置文件，更新当前主题
gulp.task('getthemes', () => {
  return gulp.src(path.join(sourceDir, 'themes/setting.json')).pipe(
    modifyContent(function (file) {
      const themes = JSON.parse(file.contents);
      themeDetail = findThemeSetting(options.code, themes);
      // distDir = themeDetail['dist'] + '/' + options.type;
      distDir = path.join(distDir, themeDetail['dist']);
    })
  );
});

// 清空
gulp.task('theme-cleanall', function () {
  return gulp.src(distDir, { read: false, allowEmpty: true }).pipe(clean({ force: true }));
});

// farris 自定义样式 相关
gulp.task('theme-css', function () {
  let stream = gulp
    .src([path.join(srcDir, 'all.scss'), path.join(srcDir, 'components.scss'), path.join(srcDir, 'interface.scss')])
    .pipe(modifyContent(modifySCSSAll))
    .pipe(
      sass({
        importer: magicImporter(),
        'output-style': 'expanded',
        precision: 5
      }).on('error', sass.logError)
    )
    .pipe(autoprefixer());
  stream = transferToBase64(stream, path.join(distDir, '/'));
  return stream.pipe(minifycss())
    .pipe(rename(function (path) {
      path.basename = "farris-" + path.basename;
    }))
    .pipe(modifyContent(addDate))
    .pipe(gulp.dest(distDir));
});

gulp.task('theme-components', function () {
  let stream = gulp
    .src(path.join(sourceDir, '/theme/components/*.scss'))
    .pipe(modifyContent(modifySCSSForComponents))
    .pipe(
      sass({
        importer: magicImporter(),
        'output-style': 'expanded',
        precision: 5
      }).on('error', sass.logError)
    )
    .pipe(autoprefixer());
  stream = transferToBase64(stream, path.join(distDir, '/'));
  return stream.pipe(minifycss())
    .pipe(modifyContent(addDate))
    .pipe(gulp.dest(distDir + "/components"));
});

gulp.task('theme-interface', function () {
  let stream = gulp
    .src(path.join(sourceDir, '/theme/interface/*.scss'))
    .pipe(modifyContent(modifySCSSForComponents))
    .pipe(
      sass({
        importer: magicImporter(),
        'output-style': 'expanded',
        precision: 5
      }).on('error', sass.logError)
    )
    .pipe(autoprefixer());
  stream = transferToBase64(stream, path.join(distDir, '/'));
  return stream.pipe(minifycss())
    .pipe(modifyContent(addDate))
    .pipe(gulp.dest(distDir + "/interface"));
});

const parentPath = path.resolve(sourceDir, '..');
// 清空发布的文件夹
gulp.task('ui-clean', function () {
  return gulp.src(path.join(parentPath, "ui-vue/package/theme*")).pipe(clean({ force: true }));
});

// 拷贝主题
gulp.task('theme-ui', function () {
  return gulp.src(path.join(distDir, "**/*")).pipe(gulp.dest(path.join(parentPath, "ui-vue/package")));
});
// 更新到ui-vue/public/assets
gulp.task('copy-to-ui', function () {
  return gulp.src(path.join(distDir, "/farris-all.css")).pipe(gulp.dest(path.join(parentPath, "ui-vue/public/assets")));
});
// 更新到designer/public/assets
gulp.task('copy-to-designer', function () {
  return gulp.src(path.join(distDir, "/farris-all.css")).pipe(gulp.dest(path.join(parentPath, "designer/public/assets")));
});


// 拷贝资源到目录下
gulp.task('theme-assets', () => {
  const themePath = baseThemes + options.code + '/';
  return gulp.src([path.join(sourceDir, 'theme/assets/**/*'), path.join(sourceDir, themePath + 'assets/**/*')]).pipe(gulp.dest(distDir));
});

// 删除之前临时生成的资源文件
gulp.task('theme-clean', (done) => {
  if (showInlineBase64) {
    return gulp.src([path.join(distDir, "/imgs"), path.join(distDir, "*.ttf")], { read: false, allowEmpty: true }).pipe(clean({ force: true })).on('end', done);
  } else {
    return done();
  }
});

// 生成主题 
gulp.task('theme', gulp.series('getthemes', 'theme-cleanall', 'theme-assets', 'theme-css', 'theme-components', 'theme-interface', 'theme-clean'));
gulp.task('themeupdate', gulp.series('getthemes', 'theme-cleanall', 'theme-assets', 'theme-css', 'theme-components', 'theme-interface', 'theme-clean','copy-to-ui', 'copy-to-designer'));
// 生成主题并拷贝到ui-vue下
gulp.task('themeui', gulp.series('ui-clean', 'theme-ui'));
// 修改内容
function modifyContent(detailFunc) {
  return through2.obj(function (chunk, encoding, done) {
    detailFunc(chunk);
    this.push(chunk);
    done();
  });
}
function modifySCSSForComponents(chunk) {
  let content = String(chunk.contents);
  const themePath = baseThemes + options.code + '/' + options.type + '/';

  let contentPre = '@import "../common/index";\r\n';
  // 追加变量定义
  if (fs.existsSync(themePath + 'index.scss')) {
    contentPre = "@import '" + themePath + "index.scss';\r\n" + contentPre;
  }
  // 追加扩展定义
  if (chunk['history'][0].indexOf('extend.scss') > -1) {
    if (fs.existsSync(themePath + 'extend.scss')) {
      content += "@import '" + themePath + "extend.scss';\r\n";
    }
  }
  content = contentPre + content;
  chunk.contents = Buffer.from(content);
}

function modifySCSSAll(chunk) {
  let content = String(chunk.contents);
  const themePath = baseThemes + options.code + '/' + options.type + '/';
  // // 追加变量定义
  let contentPre = '@import "../common/index";';
  // 追加变量定义
  if (fs.existsSync(path.join(themePath, 'index.scss'))) {
    contentPre = "@import '../../" + themePath + "index.scss';\r\n" + contentPre;
  }
  const needExtendIndex = ["all.scss", "components.scss"].findIndex(item => chunk['history'][0].indexOf(item) > -1);
  if (needExtendIndex > -1) {
    // 追加扩展定义
    if (fs.existsSync(path.join(themePath, 'extend.scss'))) {
      content += "@import '../../" + themePath + "extend.scss';\r\n";
    }
  }
  content = contentPre + content;
  chunk.contents = Buffer.from(content);
}

function findThemeSetting(themeName, themes) {
  const findTheme = themes.find(item => item.theme == themeName);
  return findTheme;
}
function addDate(chunk) {
  let content = String(chunk.contents);
  content = "/**" + formaData() + "**/\r\n" + content;
  chunk.contents = Buffer.from(content);
}

// 定义格式化封装函数
function formaData() {
  const timer = new Date();
  const year = timer.getFullYear();
  const month = timer.getMonth() + 1; // 由于月份从0开始，因此需加1
  const day = timer.getDate();
  const hour = timer.getHours();
  const minute = timer.getMinutes();
  const second = timer.getSeconds();
  return `${pad(year, 4)}-${pad(month)}-${pad(day)} ${pad(hour)}:${pad(minute)}:${pad(second)}`;
}
// 定义具体处理标准
// timeEl 传递过来具体的数值：年月日时分秒
// total 字符串总长度 默认值为2
// str 补充元素 默认值为"0"
function pad(timeEl, total = 2, str = '0') {
  return timeEl.toString().padStart(total, str);
}
/** *****************************
    Farris 主题样式生成  ***********************************************结束****************************
*******************************/


/** *****************************
      Farris 公共样式生成 ***********************************************开始****************************
*******************************/
const pubOptions = minimist(process.argv.slice(2), { default: { code: 'default' } });
let pubDistDir = '';
const pubSrcDir = sourceDir + 'farris-pub/public/';
// 更新
gulp.task('pubGetSetting', () => {
  return gulp.src(pubSrcDir + 'setting.json').pipe(
    modifyContent(function (file) {
      const themes = JSON.parse(file.contents);
      themeDetail = findThemeSetting(pubOptions.code, themes);
      console.log(themeDetail);
      pubDistDir = sourceDir + themeDetail['dist'];
    })
  );
});
// 生成文件
gulp.task('pubMain', () => {
  return (
    gulp
      .src(pubSrcDir + 'farris-pub.scss')
      .pipe(modifyContent(modifyPubSCSS))
      .pipe(
        sass({
          importer: magicImporter(),
          'output-style': 'expanded',
          precision: 5
        }).on('error', sass.logError)
      )
      .pipe(autoprefixer())
      .pipe(minifycss())
      .pipe(gulp.dest(pubDistDir))
  );
});


function modifyPubSCSS(chunk) {
  let content = String(chunk.contents);
  const themePath = pubSrcDir + 'themes/' + pubOptions.code + '/';
  // 追加变量定义
  if (fs.existsSync(themePath + '_index.scss')) {
    content = content.replace("//@theme", "@import './themes/" + pubOptions.code + "/index.scss';\r\n");
  }
  chunk.contents = Buffer.from(content);
}
// 生成公共样式
gulp.task('pubcss', gulp.series('pubGetSetting', 'pubMain'));

const pubSiteSrc = sourceDir + 'farris-pub/site/';
const pubDocsSrc = sourceDir + 'farris-pub/docs/';
const pubDocsDist = sourceDir + 'dist/farris-pub/';
// 文档样式
gulp.task('pubdocs', function (param) {
  return (
    gulp
      .src([pubDocsSrc + 'docs.scss', pubDocsSrc + 'dark.scss', pubDocsSrc + 'light.scss'])
      .pipe(
        sass({
          importer: magicImporter(),
          'output-style': 'expanded',
          precision: 5
        }).on('error', sass.logError)
      )
      .pipe(autoprefixer())
      .pipe(concat('docs.css'))
      .pipe(minifycss())
      .pipe(gulp.dest(pubDocsDist + 'css/'))
  );
});

gulp.task('pubsiteCopy', function () {
  return gulp.src([pubSiteSrc + '**/*']).pipe(gulp.dest(pubDocsDist));
});
gulp.task('pubsiteMinJS', function () {
  return gulp.src([pubSiteSrc + 'js/*']).pipe(uglify()).pipe(gulp.dest(pubDocsDist + 'js'));
});
gulp.task('pubsiteSettings', function () {
  return gulp.src(['farris-pub/package.json', 'farris-pub/README.md']).pipe(gulp.dest(pubDocsDist));
});

gulp.task('pubsite', gulp.series('pubcss', 'pubdocs', 'pubsiteCopy'));
// 增加了 js压缩、配置文件
gulp.task('pubsite:publish', gulp.series('pubcss', 'pubdocs', 'pubsiteCopy', 'pubsiteMinJS', 'pubsiteSettings'));
/** *****************************
      Farris 公共样式生成 ***********************************************结束****************************
*******************************/
