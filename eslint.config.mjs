import globals from "globals";
import pluginJs from "@eslint/js";
import tseslint from "typescript-eslint";
import pluginVue from "eslint-plugin-vue";
// import farrislint from "@farris/eslint-config";

/** @type {import('eslint').Linter.Config[]} */
export default [
  { files: ["**/*.{js,mjs,cjs,ts,vue}"] },
  { languageOptions: { globals: globals.browser } },
  pluginJs.configs.recommended,
  ...tseslint.configs.recommended,
  ...pluginVue.configs["flat/essential"],
  { files: ["**/*.vue"], languageOptions: { parserOptions: { parser: tseslint.parser } } },
  {
    ignores: [
      "node_modules/*",
      "packages/**/node_modules/*",
      "packages/**/dist/*",
      "packages/**/build/*",
      "packages/**/lib/*",
      "packages/**/dist-rollup/*",
      "packages/**/package/*",
      "packages/ui-vue/docs/*",
      "packages/ui-vue/demos/*",
      "packages/farris-theme/*",
      "packages/f-theme-editor-project/*",
      "packages/mobile-ui-vue/docs/*",
      "packages/devkit/public/*",
      "packages/mobile-command-services/public/*"
    ]
  },
  {
    rules: {
      'accessor-pairs': 'off',
      'array-callback-return': 'off',
      'arrow-body-style': 'off',
      curly: 'error',
      'class-methods-use-this': 'off',
      complexity: [
        'error',
        {
          max: 40
        }
      ],
      'consistent-return': 'off',
      'default-case': 'off',
      'eol-last': 'warn',
      eqeqeq: ['error', 'smart'],
      'func-names': 'off',
      'import/order': 'off',
      'import/extensions': 'off',
      'import/no-unresolved': 'off',
      'import/prefer-default-export': 'off',
      'import/no-extraneous-dependencies': 'off',
      indent: 0,
      'max-depth': 'off',
      'max-len': 'off',
      'max-nested-callbacks': ['error', 6],
      'max-params': 'off',
      'no-new': 'off',
      'no-bitwise': 'off',
      'no-console': [
        'error',
        {
          allow: [
            'log',
            'warn',
            'dir',
            'timeLog',
            'assert',
            'clear',
            'count',
            'countReset',
            'group',
            'groupEnd',
            'table',
            'dirxml',
            'error',
            'groupCollapsed',
            'Console',
            'profile',
            'profileEnd',
            'timeStamp',
            'context'
          ]
        }
      ],
      'no-multiple-empty-lines': 'error',
      'no-restricted-globals': 'off',
      'no-shadow': 'off',
      'no-trailing-spaces': 'off',
      'no-param-reassign': 'off',
      'no-plusplus': 'off',
      'no-nested-ternary': 'off',
      'no-undef': 'off',
      'no-underscore-dangle': 'off',
      'no-unused-expressions': 'off',
      'no-unused-labels': 'error',
      'no-use-before-define': 'error',
      'no-useless-constructor': 'off',
      'no-useless-concat': 'off',
      'no-var': 'error',
      'prefer-const': 'error',
      'prefer-destructuring': ['error', { object: true, array: false }],
      'prefer-promise-reject-errors': 'off',
      'prefer-template': 'off',
      'space-in-parens': ['error', 'never'],
      'spaced-comment': ['error', 'always'],
      '@typescript-eslint/camelcase': 'off',
      '@typescript-eslint/ban-ts-comment': 'off',
      '@typescript-eslint/no-unused-expressions': 'off',
      '@typescript-eslint/no-unused-vars': 'off',
      '@typescript-eslint/no-var-requires': 'off',
      '@typescript-eslint/no-explicit-any': 'off',
      '@typescript-eslint/no-empty-function': 'off',
      '@typescript-eslint/no-non-null-assertion': 'off',
      '@typescript-eslint/explicit-function-return-type': 'off',
      '@typescript-eslint/explicit-module-boundary-types': 'off',
      '@typescript-eslint/no-useless-constructor': 'off',
      '@typescript-eslint/no-parameter-properties': 'off',
      '@typescript-eslint/no-require-imports': 'off',
      '@typescript-eslint/dot-notation': 'off',
      '@typescript-eslint/no-misused-new': 'error',
      '@typescript-eslint/prefer-function-type': 'error',
      '@/semi': ['warn', 'always'],
      '@typescript-eslint/unified-signatures': 'error',
      '@typescript-eslint/no-shadow': 'off',
      '@typescript-eslint/member-ordering': 'off',
      '@typescript-eslint/no-this-alias': 'off',
      'vue/no-v-html': 'off',
      'vue/attributes-order': 'off',
      'vue/require-v-for-key': 'off',
      'vue/require-default-prop': 'off',
      'vue/no-unused-components': 'off',
      'vue/multi-word-component-names': 'off',
      'vue/return-in-computed-property': 'off'
    }
  },
  // { plugins: { farrislint }, rules: { "prefer-object-spread": "off" } }
];
