import { defineWorkspace } from 'vitest/config';

export default defineWorkspace([
    './packages/ui-vue/vite.config.ts',
    './packages/ui-vue/docs/vite.config.ts',
    './packages/mobile-ui-vue/vite.config.ts',
    './packages/mobile-ui-vue/docs/vite.config.ts'
]);
