# 回退到上一级目录
# cd ..
# 生成所有主题

gulp theme --code default --type default
echo -e "生成默认商务风蓝色-紧凑版"
gulp theme --code default --type loose
echo -e "生成默认商务风蓝色-宽松版"

gulp theme --code green --type default
echo -e "生成默认商务风绿色-紧凑版"
gulp theme --code green --type loose
echo -e "生成默认商务风绿色-宽松版"

gulp theme --code red --type default
echo -e "生成默认商务风红色-紧凑版"
gulp theme --code red --type loose
echo -e "生成默认商务风红色-宽松版"


gulp theme --code mimicry --type default
echo -e "生成拟物风蓝色-紧凑版"
gulp theme --code mimicry --type loose
echo -e "生成拟物风蓝色-宽松版"

gulp theme --code mimicry-green --type default
echo -e "生成拟物风绿色-紧凑版"
gulp theme --code mimicry-green --type loose
echo -e "生成拟物风绿色-宽松版"

gulp theme --code mimicry-red --type default
echo -e "生成拟物风红色-紧凑版"
gulp theme --code mimicry-red --type loose
echo -e "生成拟物风绿色-宽松版"


echo -e "所有主题生成完毕"


# 回到当前目录
# cd build